<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A single assert test
     *
     * @return void
     */
    public function testSingleAssertTest()
    {
     	$response = $this->get('/test-page');

        $response->assertStatus(200);
    }

    /**
     * A no if else test
     *
     * @return void
     */
    public function testNoIfsTest()
    {
     	$response = $this->post('/test-page', 
            [
                'unique_string' => str_random(32), //not hard coded
                '_token' => csrf_token()
            ]
        );
        
        //this is straight forward, no if and else
        $response->assertRedirect('success-page');
    }


    /**
     * Using new straight forward
     *
     * @return void
     */
    public function testNewTest()
    {
     	//This is a straight forward use of new, no other complicated methods being used
    	$test = new Test();
    	$test->name = str_random(10);
    	$test->unique_string = str_random(32);
    	$test->save();

    	$response = $this->post('test','/test-page',$test->id);
    	$response->assertStatus(200);
    }

    /**
     * An exemption of multiple assert
     *
     * @return void
     */
    public function testJsonTest()
    {
        $response = $this->json('POST', '/user', ['name' => str_random(6)]);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'created' => true,
            ]);
    }

}
