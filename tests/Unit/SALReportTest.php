<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use CreditBPO\Models\FinancialReport;
use DB;

class SALReportTest extends TestCase
{
    /**
     * A basic feature test posting distributekey test.
     * Use vendor\bin\phpunit --stderr {path_to/}PostDistributeKeyTest.php
     *
     * @return void
     */
    public function testSALReportTest()
    {
        Auth::loginUsingId(4);

        $financialReport = FinancialReport::latest('updated_at')->first();

        $response = $this->get('financial-report/download/sal/'.$financialReport->id);

        $filename = sprintf(
            'Structure of the Assets and Liabilities %s',
            $financialReport->title.'-'.date('Y-m-d'));

        $response->assertRedirect('/financial_analysis/'.$filename.'.pdf');

    }
}