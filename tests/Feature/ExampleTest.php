<?php

namespace Tests\Feature;

use Tests\TestCase;
use CreditBPO\Models\Entity;
use CreditBPO\Models\MajorCustomer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    protected $preserveGlobalState = false;
    protected $runTestInSeparateProcess = true;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * @test Database Columns
     */
    public function testDatabaseColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('tbllogin', [
                'loginid', 'email', 'password', 'job_title', 'firstname', 'lastname', 'name', 'activation_code',
                'activation_date', 'role', 'status', 'remember_token', 'created_at', 'updated_at', 'bank_id',
                'parent_user_id', 'permissions', 'can_view_free_sme', 'tutorial_flag', 'transactionid', 'product_plan',
                'trial_flag', 'bypass_subscriptions_checks', 'street_address', 'city', 'province', 'zipcode', 'phone'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('tblentity', [
                'entityid', 'loginid', 'companyname', 'company_tin', 'industry_main_id', 'industry_sub_id',
                'industry_row_id', 'total_assets', 'website', 'email', 'address1', 'address2', 'cityid',
                'province', 'zipcode', 'phone', 'former_address1', 'former_address2', 'former_cityid',
                'former_province', 'former_zipcode', 'former_phone', 'no_yrs_present_address', 'date_established',
                'number_year', 'description', 'employee_size', 'updated_date', 'number_year_management_team',
                'related_companies', 'tin_num', 'sec_num', 'is_agree', 'status', 'current_bank', 'is_independent',
                'completed_date', 'created_at', 'updated_at', 'submitted_at', 'sec_reg_date', 'sec_cert',
                'sec_generation', 'entity_type', 'is_paid', 'transaction_id', 'discount_code', 'permit_to_operate',
                'authority_to_borrow', 'authorized_signatory', 'tax_registration', 'longitude', 'latitude',
                'total_asset_grouping', 'value_of_outstanding_contracts', 'potential_credit_quality_issues',
                'income_tax', 'cbpo_pdf', 'pdf_grdp', 'admin_review_flag', 'completion', 'anonymous_report',
                'approval_status', 'approval_date', 'export_DSCR', 'is_premium','negative_findings', 'utility_bill'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('tblindustry_main', [
                'industry_main_id', 'main_title', 'business_group', 'management_group', 'financial_group',
                'industry_trend', 'industry_score', 'main_status', 'gross_revenue_growth', 'net_income_growth',
                'gross_profit_margin', 'net_profit_margin', 'net_cash_margin', 'current_ratio', 'debt_equity_ratio',
                'industry_capacity_utilization', 'net_cash', 'created_at', 'updated_at', 'q1', 'q2', 'q3', 'q4',
                'q5', 'q6', 'q7', 'q8', 'slope', 'gf_year1', 'gf_year2', 'gf_year3'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('tblindustry_row', [
                'industry_row_id', 'industry_sub_id', 'row_code', 'row_title', 'row_status', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('tblindustry_sub', [
                'industry_sub_id', 'industry_main_id', 'sub_code', 'sub_status', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('activity_logs', [
                'id', 'entity_id', 'first_activity', 'last_activity', 'first_submission', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('analyst_account', [
                'id', 'name', 'email', 'is_deleted', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('auditlogs', [
                'auditLogId', 'entity_id', 'login_id', 'activity', 'error_code', 'stack_trace',
                'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('automate_standalone_rating', [
                'id', 'entity_id', 'status', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('automate_standalone_rating_logs', [
                'id', 'data', 'entity_id', 'status', 'error', 'created_at', 'updated_at', 'is_deleted'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_custom_docs', [
                'document_id', 'bank_id', 'document_label', 'bank_required', 'status', 'created_date', 'updated_date'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_doc_options', [
                'id', 'bank_id', 'name', 'is_active', 'corp_sec_reg', 'corp_sec_gen', 'corp_business_license',
                'corp_board_resolution', 'corp_board_resolution_pres', 'corp_tax_reg', 'corp_itr',
                'sole_business_license', 'sole_business_owner', 'sole_tax_reg', 'sole_itr', 'boi', 'peza',
                'fs_template', 'balance_sheet', 'income_statement', 'cashflow_statement', 'bank_statement',
                'utility_bill', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_formula', [
                'id', 'bank_id', 'bcc', 'mq', 'fa', 'boost', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_formula_history', [
                'id', 'name', 'bank_id', 'bcc', 'mq', 'fa', 'boost', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_interest_rates', [
                'id', 'low', 'mid', 'high', 'type', 'executed'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bank_keys', [
                'id', 'bank_id', 'serial_key', 'is_used', 'login_id', 'entity_id', 'created_at', 'updated_at'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('browser_statistics', [
                'id', 'browser', 'version', 'major_version', 'operating_system', 'platform', 'is_mobile'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bsp_business_expectations', [
                'bsp_expectation_id', 'industry_id', 'filename', 'status', 'date_uploaded'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('bsp_expectation_downloads', [
                'bsp_file_id', 'filename', 'status', 'date_uploaded'
            ])
        );

        $this->assertTrue(
            Schema::hasColumns('business_outlook_update',[
                'id', 'quarter', 'year', 'status', 'executed'
            ])
        );
    }

    /**
     * @test Entity Model
     */
    public function testDatabaseEntityModel()
    {
        $user = new Entity();

        $user->companyname = 'Company XYZ';
        $user->company_tin = '999-999-999-999';
        $user->entity_type = 1;
        $user->tin_num     = 45899655;
        $user->sec_reg_date = 2019-11-06;
        $user->industry_main_id = 20;
        $user->industry_row_id = 644;
        $user->total_assets = 117878031.00;
        $user->total_assets_grouping = 2;
        $user->website = 'www.compnyxyz.com';
        $user->email   = 'leoleo@creditbpo.com';
        $user->address1 = 'Champaca Street';
        $user->province = 'Camarines Sur';
        $user->cityid   = 23;
        $user->zipcode  = 1809;
        $user->phone    = 905689635;
        $user->no_yrs_present_address = 20;
        $user->date_established  = 2019-10-11;
        $user->number_year = 19;
        $user->description = 'Provider of modern transportation services since 1991, has offered a full range of transportation and logistic services to meet all the needs of modern businesses.';
        $user->employee_size = 50;
        $user->updated_date = 2019-04-01;
        $user->number_year_management_team = 15;
        $user->current_bank = 743;

        $this->assertEquals($user->companyname, 'Company XYZ');
        $this->assertEquals($user->company_tin, '999-999-999-999');
        $this->assertEquals($user->entity_type, 1);
        $this->assertEquals($user->tin_num, 45899655);
        $this->assertEquals($user->sec_reg_date, 2019-11-06);
        $this->assertEquals($user->industry_main_id, 20);
        $this->assertEquals($user->industry_row_id, 644);
        $this->assertEquals($user->total_assets, 117878031.00);
        $this->assertEquals($user->total_assets_grouping, 2);
        $this->assertEquals($user->website, 'www.compnyxyz.com');
        $this->assertEquals($user->email, 'leoleo@creditbpo.com');
        $this->assertEquals($user->address1, 'Champaca Street');
        $this->assertEquals($user->province, 'Camarines Sur');
        $this->assertEquals($user->cityid, 23);
        $this->assertEquals($user->zipcode, 1809);
        $this->assertEquals($user->phone, 905689635);
        $this->assertEquals($user->no_yrs_present_address, 20);
        $this->assertEquals($user->date_established, 2019-10-11);
        $this->assertEquals($user->number_year, 19);
        $this->assertEquals($user->description, 'Provider of modern transportation services since 1991, has offered a full range of transportation and logistic services to meet all the needs of modern businesses.');
        $this->assertEquals($user->employee_size, 50);
        $this->assertEquals($user->updated_date, 2019-04-01);
        $this->assertEquals($user->number_year_management_team, 15);
        $this->assertEquals($user->current_bank, 743);
    }

    /**
     * @test MajorCustomer Model
     */
    public function testDatabaseMajorCustomerModel()
    {
        $customer = new MajorCustomer();

        $customer->customer_name = 'Leonor Rivera';
        $customer->customer_share_sales = 0.5;
        $customer->customer_address = 'San Mateo Rizal';
        $customer->customer_contact_person = 'Dave Sales';
        $customer->customer_email = 'www.customeremail.com';
        $customer->customer_phone = 65932584736;
        $customer->customer_started_years = 1990;
        $customer->customer_years_doing_business = 29;
        $customer->customer_settlement = 4;
        $customer->customer_order_frequency = 'mcof_regularly';
        $customer->relationship_satisfaction = 1;

        $this->assertEquals($customer->customer_name, 'Leonor Rivera');
        $this->assertEquals($customer->customer_share_sales, 0.5);
        $this->assertEquals($customer->customer_address, 'San Mateo Rizal');
        $this->assertEquals($customer->customer_contact_person, 'Dave Sales');
        $this->assertEquals($customer->customer_email, 'www.customeremail.com');
        $this->assertEquals($customer->customer_phone, 65932584736);
        $this->assertEquals($customer->customer_started_years, 1990);
        $this->assertEquals($customer->customer_years_doing_business, 29);
        $this->assertEquals($customer->customer_settlement, 4);
        $this->assertEquals($customer->customer_order_frequency, 'mcof_regularly');
        $this->assertEquals($customer->relationship_satisfaction, 1);
    }
}
