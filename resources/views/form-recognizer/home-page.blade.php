@extends('layouts.form-recognizer', ['route' => 'resource.view', 'name' => 'Edit API key'])

@section('content')
<div class="container">
  <h3>Upload form to analyze</h3>
  <form enctype="multipart/form-data" style="margin-bottom: 90px;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="upload">
        <div id="upload-border">
          <nobr>
            <input type="text" id="upload-name" disabled="true" />
            <button type="button" id="upload-button">Upload</button>
          </nobr>
        </div>
        <input type="file" id="hidden-upload" name="document"  style="display:none"/>
        <button type="submit" class="sub"> Submit </button>
      </div>
      <div class="msg"></div>
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(session()->has('messages'))
      <div class="alert alert-success">
        <ul>
          @foreach (session()->get('messages') as $message)
          <li>{{$message}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div id="form-errors"></div>
      @empty($secrets)
      <div class="alert alert-info">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        There is no API secret for Form Recognizer. Please click 'Edit API key' to add the secret.
      </div>
      @endempty
    </form>
    @if(count($files))
    <div class="tbl-res-cs">
      <table class="table table-striped table-bordered">
        <thead >
          <tr>
            <th scope="col">#</th>
            <th scope="col" class="file">File</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($files as $key => $file)
          <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$file->name}}</td>
            <td>{{$file->status ? "Completed" : "Pending" }}</td>
            <td>
              <button type="button" class="btn btn-success"  onclick="window.location='{{ route("result",[$file->id]) }}'" @if($file->status) disabled @endif>
                <i class="fa-solid fa-arrows-rotate"></i>
              </button>
              <button type="button" class="btn btn-info" onclick="window.location='{{ route("file.get",[$file->id]) }}'">
                Download file
              </button>
              <button type="button" class="btn btn-danger" onclick="window.location='{{ route("report",[$file->id]) }}'" @if(!$file->status) disabled @endif>
                <!-- <i class="fa-solid fa-download"></i>  -->
                Download report
              </button>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
      @if ($files->hasPages())
      <div class="form-recognizer-pagination">
        {{ $files->links() }}
      </div>
      @endif
    </div>
    @endif
</div>
@endsection

@push('scripts')
<script>
var spinner = $('#loader');
var form = $('form')[0];
var formData = new FormData(form);

$('form').submit(function(e) {

  var files = $('#hidden-upload')[0].files;

  if(files.length > 0 ){
    formData.append('file',files[0]);
  }

  e.preventDefault();
  spinner.show();

  $.ajax({
    url: 'form-recognizer',
    data: formData,
    type: "post",
    contentType: false,
    processData: false,
    success:function(response) {
      successHtml = '<div class="alert alert-success"><ul>';

      successHtml += '<li>' + response.message + '</li>';

      successHtml += '</ul></div>';

      $( '#form-errors' ).html( successHtml );

      spinner.hide();

      setTimeout(function() {
        window.location = "{{ URL::route('form.home')}}"
      }, 2000);
    },
    error:function (response ) {

      // if (response.status == 500) {
      //     errors = {'error': {'0':'Internal Server Error'}};
      // } else {
      // }
      errors = response.responseJSON.errors;

      errorsHtml = '<div class="alert alert-danger"><ul>';

      $.each(errors, function(key, value) {
        errorsHtml += '<li>' + value[0] + '</li>';
      });

      errorsHtml += '</ul></div>';

      $( '#form-errors' ).html( errorsHtml );

      spinner.hide();
    }
  });

});
</script>
@endpush
