<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}"  media="screen" />
    <!--    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}"></link>-->

    <!--new-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/resource-management.css') }}" media="screen" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--new end-->

</head>
<body>
  <div class="text-right">
      <button type="button" onclick="window.location='{{ route("form.home") }}'" style="margin :4px; background: #348998; border: none; color: #fff; text-transform: uppercase;font-size: 16px; letter-spacing: 1px;" name="button">
         Form recognizer
       </button>
  </div>
  <div class="container">
    <form class="form-horizontal resource-management-form" method="post" action="{{route("resource.update")}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <div class="col-sm-8">
            <h4><span>Manage API Key</span></h4>
          </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="apiKey">API Key:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="apiKey" value="{{$secret->api_key ?? ''}}" name="apiKey" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="endPoint">End Point:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="endPoint" value="{{$secret->end_point ?? ''}}" name="endPoint" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="modelId">Model Id:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="modelId" value="{{$secret->model_id ?? ''}}" name="modelId" required>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-8">
          <h4><span>Shared Access Signature (SAS)</span></h4>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="modelid">Container Name:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" value="{{$secret->container_name ?? ''}}" name="containerName" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="modelid">Container Url:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="containerUrl" value="{{$secret->container_url ?? ''}}" required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="modelid">SAS Token:</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="sasToken" value="{{$secret->sas_token ?? ''}}" required>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-6">
          <button type="submit" class="btn btn-default">Update</button>
        </div>
      </div>
    </form>
    @empty($secret)
    <div class="alert alert-info">
      <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
      If above field doesn't contain data please run that command "php artisan db:seed --class=ApiSecretSeeder"
    </div>
    @endempty
  </div>
</body>
</html>
<script src="http://code.jquery.com/jquery.js"></script>
