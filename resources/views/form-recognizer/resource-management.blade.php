@extends('layouts.form-recognizer', ['route' => 'form.home', 'name' => 'Form recognizer'])
@section('content')
<div class="container">
  <form class="form-horizontal resource-management-form" method="post" action="{{route("resource.update")}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
        <div class="col-sm-8">
          <h4><span>Manage API Key</span></h4>
        </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="apiKey">API Key:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="apiKey" value="{{$secret->api_key ?? ''}}" name="apiKey" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="endPoint">End Point:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="endPoint" value="{{$secret->end_point ?? ''}}" name="endPoint" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="modelId">Model Id:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="modelId" value="{{$secret->model_id ?? ''}}" name="modelId" required>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-8">
        <h4><span>Shared Access Signature (SAS)</span></h4>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="modelid">Container Name:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" value="{{$secret->container_name ?? ''}}" name="containerName" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="modelid">Container Url:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="containerUrl" value="{{$secret->container_url ?? ''}}" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="modelid">SAS Token:</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="sasToken" value="{{$secret->sas_token ?? ''}}" required>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-6">
        <button type="submit" class="btn btn-default">Update</button>
      </div>
    </div>
  </form>
  @if(session()->has('messages'))
  <div class="alert alert-success">
    <ul>
      @foreach (session()->get('messages') as $message)
      <li>{{$message}}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @empty($secret)
  <div class="alert alert-info">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    If above field doesn't contain data please run that command "php artisan db:seed --class=ApiSecretSeeder"
    <br>
    Or you can add it manually.
  </div>
  @endempty
</div>
@endsection
