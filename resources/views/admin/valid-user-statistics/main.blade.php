@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">

    <div class="row filters">
        <div class="col-sm-4" style="border-right: 1px solid #e8e8e8;">
            {{Form::open(array('route'=>'adminValidUserStatistics', 'method'=>'get', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
            <div class="form-group">
                {{Form::label('sme', 'Company Name', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    <select id="select-sme" class="form-control" name="sme[]" multiple="multiple">
                        @foreach($sme_list as $key=>$value)
                            @if(in_array($key, $sme_selected))
                                <option value="{{$key}}" selected="selected">{{$value}}</option>
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('industry1', 'Industry Tier 1', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('industry1', array(''=>'All') + $main_industry_list, $main_industry_selected, array('class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('industry2', 'Industry Tier 2', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('industry2', array(''=>'All') + $sub_industry_list, $sub_industry_selected, array('class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('industry3', 'Industry Tier 3', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('industry3', array(''=>'All') + $industry_list, $industry_selected, array('class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('region', 'Region', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('region', array(''=>'All') + $region_list, '', array('class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('bcc', 'Business Considerations and Conditions (BCC)', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{Form::select('bcc_from', array(
                            '' => ' - ',
                            226 => 'AAA',
                            211 => 'AA',
                            196 => 'A',
                            181 => 'BBB',
                            166 => 'BB',
                            151 => 'B',
                            136 => 'CCC',
                            121 => 'CC',
                            101 => 'C',
                            81 => 'D',
                            0 => 'E',
                        ), $bcc_from, array('class'=>'form-control'))}}
                        <label>to</label>
                        {{Form::select('bcc_to', array(
                            '' => ' - ',
                            240 => 'AAA',
                            225 => 'AA',
                            210 => 'A',
                            195 => 'BBB',
                            180 => 'BB',
                            165 => 'B',
                            150 => 'CCC',
                            135 => 'CC',
                            120 => 'C',
                            100 => 'D',
                            80 => 'E',
                        ), $bcc_to, array('class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('mq', 'Management Quality (MQ)', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{Form::select('mq_from', array(
                            '' => ' - ',
                            141 => 'AAA',
                            131 => 'AA',
                            121 => 'A',
                            111 => 'BBB',
                            101 => 'BB',
                            91 => 'B',
                            81 => 'CCC',
                            71 => 'CC',
                            61 => 'C',
                            51 => 'D',
                            0 => 'E',
                        ), $mq_from, array('class'=>'form-control'))}}
                        <label>to</label>
                        {{Form::select('mq_to', array(
                            '' => ' - ',
                            150 => 'AAA',
                            140 => 'AA',
                            130 => 'A',
                            120 => 'BBB',
                            110 => 'BB',
                            100 => 'B',
                            90 => 'CCC',
                            80 => 'CC',
                            70 => 'C',
                            60 => 'D',
                            50 => 'E',
                        ), $mq_to, array('class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('fc', 'Financial Condition (FC)', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{Form::select('fc_from', array(
                            '' => ' - ',
                            200 => 'AAA',
                            180 => 'AA',
                            160 => 'A',
                            140 => 'BBB',
                            120 => 'BB',
                            100 => 'B',
                            80 => 'CCC',
                            60 => 'CC',
                            40 => 'C',
                            20 => 'D',
                            0 => 'E',
                        ), $fc_from, array('class'=>'form-control'))}}
                        <label>to</label>
                        {{Form::select('fc_to', array(
                            '' => ' - ',
                            300 => 'AAA',
                            199 => 'AA',
                            179 => 'A',
                            159 => 'BBB',
                            139 => 'BB',
                            119 => 'B',
                            99 => 'CCC',
                            79 => 'CC',
                            59 => 'C',
                            39 => 'D',
                            19 => 'E',
                        ), $fc_to, array('class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('rating', 'CreditBPO Rating', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{Form::select('rating_from', array(
                            '' => ' - ',
                            177 => 'AA',
                            150 => 'A',
                            123 => 'BBB',
                            96 => 'BB',
                            68 => 'B',
                            0 => 'CCC',
                        ), $rating_from, array('class'=>'form-control'))}}
                        <label>to</label>
                        {{Form::select('rating_to', array(
                            '' => ' - ',
                            250 => 'AA',
                            176 => 'A',
                            149 => 'BBB',
                            122 => 'BB',
                            95 => 'B',
                            67 => 'CCC',
                        ), $rating_to, array('class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('brating', 'Supervisor Rating', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{Form::select('brating_from', array(
                            '' => ' - ',
                            177 => 'AA',
                            150 => 'A',
                            123 => 'BBB',
                            96 => 'BB',
                            68 => 'B',
                            0 => 'CCC',
                        ), $brating_from, array('class'=>'form-control'))}}
                        <label>to</label>
                        {{Form::select('brating_to', array(
                            '' => ' - ',
                            250 => 'AA',
                            176 => 'A',
                            149 => 'BBB',
                            122 => 'BB',
                            95 => 'B',
                            67 => 'CCC',
                        ), $brating_to, array('class'=>'form-control'))}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('pdefault', 'Preliminary Probability of Default', array('class'=>'col-sm-6'))}}
                <div class="col-sm-6 form-inline">
                    <div class="form-group">
                        {{ Form::text('pdefault_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }} %
                        <label>and</label>
                        {{ Form::text('pdefault_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }} %
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{Form::label('type', 'Graph Type', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('type', array('1'=>'Industry','2'=>'Composite Final Ratings','3'=>'Composite Rating Breakdown','4'=>'Industry Comparison: Report vs Industry'), $type, array('class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group filter4">
                {{Form::label('selector', 'Data Shown', array('class'=>'col-sm-4'))}}
                <div class="col-sm-8">
                    {{Form::select('selector', array(
                        '1'=>'Gross Revenue Growth',
                        '2'=>'Net Income Growth',
                        '3'=>'Gross Profit Margin',
                        '4'=>'Current Ratio',
                        '5'=>'Debt-to-Equity Ratio'
                    ), (isset($selector)) ? $selector : 1, array('class'=>'form-control'))}}
                </div>
            </div>

            <div class="form-group">
                <a href="#" class="create_graph btn btn-primary">Create New Graph</a>
                <a href="#" class="update_graph btn btn-primary">Update Current Graph</a>
            </div>
            <div class="form-group">
                <a href="#" class="delete_graph btn btn-primary">Delete Selected Graph</a>
                <a href="#" class="delete_all_graph btn btn-primary">Clear All Graphs</a>
                <a href="#" onclick="window.print(); return false;" class="btn btn-primary">Print</a>
            </div>

            {{ Form::close() }}
        </div>
        <div class="col-sm-8">
            <div id="chartcontainer"><div></div></div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/jquery.form.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart1.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart2.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart3.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart4.js"></script>
@stop
@section('style')
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
<link rel="stylesheet" href="{{ URL::asset('css/statistics-print.css') }}" media="print"></link>
<style>
#chartcontainer div.highcharts-container {
    min-height: 500px !important;
}
</style>
@stop
