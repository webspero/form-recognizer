@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
@stop

@section('content')
    <div id="page-wrapper" data-valid-user-statistics style="margin-left:0px;"></div>
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            require([
                '{{ URL::asset("static/js/modules/admin/valid-user-statistics/index.js")}}'
            ], function(mod) {
                mod.initialize();
            });
        });
    </script>
@stop
