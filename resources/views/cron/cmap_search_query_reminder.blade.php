<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> We have reached 40+ CMAP search query for the month of {{ $month }}. </h2>
				<div>
					<p>Total number of queries in cmap search for this month: {{ $num_of_queries }}</p>
					<p>The CreditBPO Team<br>
						CreditBPO
					</p>
					<p><i>Paving the way</i></p>
				</div>
			</div>
		</div>
	</body>
</html>