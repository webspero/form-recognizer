<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
            <header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
                <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
            </header>
            <div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
                <h2 style="color:#00697A;">
                    Automate Standalone Rating Result as of {{ $date_processed }}
                </h2>

                @if (@count($success) > 0)
                    <div>
                        <h3 style="color:#00697A;">
                            Successful Reports
                        </h3>

                        <p>
                            <ul>
                                @foreach ($success as $item)
                                    <li>{{ $item }}</li>
                                @endforeach
                            </ul>
                        </p>
                    </div>
                @endif

                @if (@count($failed) > 0)
                    <div>
                        <h3 style="color:#00697A;">
                            Failed Reports
                        </h3>

                        <p>
                            <ul>
                                @foreach ($failed as $item)
                                    <li>{{ $item }}</li>
                                @endforeach
                            </ul>
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>
