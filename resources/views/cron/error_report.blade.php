<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
            <header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
                <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
            </header>
            <div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
                <h2 style="color:#00697A;">
                    Automate Standalone Rating Error for Entity Id {{$entity_id}} as of {{$created_at}}.
                </h2>


                <div>
                    <h3 style="color:#00697A;">
                        Failed Report for {{$entity_id}}
                    </h3>

                    <p>
                        With error message: <i><strong>{{$error}}</strong></i>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
