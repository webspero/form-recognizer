@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
	<style>.ui-dialog-titlebar-close .ui-button-text { height: 14px !important; }</style>

    <script src="https://www.google.com/recaptcha/api.js?render=6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M"></script>

@stop

@section('nav')

@show
@section('content')
	<div id="loginwrapper" class="container">
        {{ Form::open(array('url' => 'login', 'id' => 'login')) }}
        <div class="form-group">
            <input type="button" value="New Users Click Here to Sign Up" onclick="javascript: location.href='{{ URL::to('/') }}/signup'" class="btn btn-large btn-primary" style="font-weight: normal;font-size: 21px;width: 100%;" />

            <input type="hidden" name="cbpo_honey" id="cbpo_honey" value="" />
        </div>

        <h3>Member Login</h3>

        @if($errors->has('error'))
            <div class="form-group">
                <span class="errormessage">{{ $errors->first('error') }}</span>
            </div>
        @endif

        @if (Session::has('inactivity'))
            <div class="form-group">
                <span class="errormessage">{{ Session::get('inactivity') }}</span>
            </div>
        @endif

        <div class="form-group{{ ($errors->has('email')) ? ' has-error' : '' }}">
            {{ Form::label('email', 'Email Address') }}
            {{ Form::text('email', Input::old('email'), array('class' => 'form-control login-field','id' => 'email')) }}

            @if($errors->has('email'))
                <span class="errormessage">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group{{ ($errors->has('username')) ? ' has-error' : '' }}">
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('class' => 'form-control login-field','id' => 'password')) }}
            
            @if($errors->has('password'))
                <span class="errormessage">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="row">
			<div class="col-sm-6">
				
				<div style="margin-top: 10px">
                    {{ Form::token() }}

					<input type="submit" id="submit_btn" class="btn btn-large btn-primary" value="Submit">
				</div>
			</div>
		</div>

        <!-- <div class="form-group">
            {{ Form::button('Log In',array('class'=>'btn btn-large btn-primary login-btn')) }}
        </div> -->

        <div style="margin-top: 10px">
            <a id = 'forgot-pass-link' href = 'forgot-pword-form'> Reset Password </a>
            <p id = 'forgot-pass-error' class="errormessage"></p>
        </div>

        {{ Form::close() }}

    </div>

    @include('forgot-password-modal')
    <div class="modal" id="browser-detection" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3> <i class = "icon-more-info glyphicon glyphicon-exclamation-sign"> </i> CAUTION!</h3>
                </div>

                <div class="modal-body">
                    <p>
                        It appears that you are using an outdated browser.
                        In order to streamline your experience
                        and ensure proper data security from malicious access,
                        we strongly recommend that you use a different browser.
                    </p>

                    <p>
                        The most secure, compatible browsers are listed below.
                        You may access and download these by clicking their respective links.
                        <br/><br/>
                        <ul>
                            <li><a href = 'https://www.google.com/chrome/browser'> Google Chrome <img src = "{{ URL::To('images/chrome-icon.png') }}" style = 'width: 30px; height: 30px;'/></a></li>
                            <li><a href = 'https://www.mozilla.org/'> Mozilla Firefox <img src = "{{ URL::To('images/firefox-icon.png') }}" style = 'width: 25px; height: 25px; margin-left: 12px;'/></a></li>
                        </ul>
                    </p>
                </div>

                <div class="modal-footer"></div>

            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>

        $(document).ready(function(){
            $('.login-field').keypress(function(e){
              if(e.keyCode==13)
              $('.login-btn').click();
            });

            if (window.attachEvent && !window.addEventListener) {
                /** Configure the Modal Pop-up  */
                $('#browser-detection').modal('show');
            }

            $('#login').submit(function(e){

                e.preventDefault();

                    grecaptcha.ready(function() {
                      grecaptcha.execute('6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M', {action: 'submit'}).then(function(token) {
                          
                          $('#login').prepend('<input type="hidden" name="recaptcha" value="' + token + '">');

                          e.currentTarget.submit();
                      });
                });

            });
        });
    </script>
    <script src="{{ URL::asset('js/forgot-password.js') }}"></script>
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>

@stop
