@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>PDF/Image to Excel Converter</h2>
				<hr/>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(['route'=>'postPDFtoCSV', 'method'=>'post', 'files'=>true])}}
					<div class="form-group">
						{{ Form::label('imagefile', 'Select the image or pdf you want to convert to excel') }}
						{{ Form::file('imagefile') }}
					</div>
					<p>Warning: Processing may take 1-5 minutes per page/image.  Please click on "Convert" button only once and wait for the result.</p>
					{{ Form::submit('Convert', array('class'=>'btn btn-primary')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
