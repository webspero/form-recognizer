
	<div class="row">
        {{ Form::open( array('route' => array('execSeleniumWebdrvr'))) }}
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $e)
                <p>{{$e}}</p>
            @endforeach
        </div>
        @endif
        
        <div class="form-group">
            <div class="col-md-2">
            {{ Form::label('username', 'Username') }}
            {{ Form::text('username', Input::old('username'), array('class' => 'form-control')) }}
            </div>	
            <div class="col-md-4">
            {{ Form::label('password', 'Password') }}
            {{ Form::text('password', Input::old('password'), array('class' => 'form-control')) }}
            </div>							
            <div class="col-md-4">
            {{ Form::label('report_id', 'Report ID') }}
            {{ Form::text('report_id', Input::old('report_id'), array('class' => 'form-control')) }}
            </div>								
            <div class="col-md-4">
            {{ Form::label('browser', 'Browser') }}
            {{ Form::select('browser', array('1' => 'Firefox', '2' => 'Chrome'), array('id' => 'browser', 'class' => 'form-control')) }}
            </div>	
            
            <div class="col-md-12"><hr/></div>
        </div>

        <div class="spacewrapper">
            {{ Form::token() }}
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="Execute Automated Test">
        </div>

		{{ Form::close() }}
	</div>
