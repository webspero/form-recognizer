
    <style>
		#show_results {
			min-height: 150px !important;
			max-height: 300px !important;
			overflow-y: auto !important;
			box-shadow: inset 0 0 1px 1px #143048;
		}
		ul, li, ul>li {
			list-style : none !important;
		}

		.search_icon {
			background-image: url('/images/icon_search.png') !important;
			height: 20px;
			width: 20px;
			display: block;
		}

		.input-container {
			display: flex !important;
			width: 100%;
		}

        .modal-overlay {
            z-index: 1100 !important;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .navbar {
            z-index:500 !important;
        }

	</style>
{{ Form::open( array('route' => array('postEditableField', $field_id, $field_name, $arr_table), 'role'=>'form', 'id' => 'editable-field-form', 'class' => 'form-inline')) }}

    @if (isset($industry['main_val']))
        {{ Form::select('industry_main_id', $industry['main_lst'], $industry['main_val'], array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table, 'id' => 'industrymain', 'onchange' =>  'get_industry("sub")')) }}
        <div style="clear: both"></div> 
        {{ Form::select('industry_sub_id', $industry['sub_lst'], $industry['sub_val'], array('class' => 'form-control', 'id' => 'industrysub', 'onchange' =>  'get_industry("row")')) }}
        <div style="clear: both"></div>
        {{ Form::select('industry_row_id', $industry['row_lst'], $industry['row_val'], array('class' => 'form-control', 'id' => 'industryrow')) }}
        <div style="clear: both"></div>
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["industry_txt"] }}' />
        <script src="{{ URL::asset('js/editable-industry.js') }}"></script>
    @elseif (isset($conv_data['bank_list']))
        <div class = 'row'>
            <div class = 'col-md-12'>
            @if($login->trial_flag == 0)
                {{ Form::select($field_name, array('' => trans('messages.choose_here'), '0' => trans('business_details1.requesting_bank_independent'),'1' => trans('business_details1.requesting_bank_with_bank')) , $field_value, array('id' => $field_name, 'class' => 'form-control', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table)) }}
            @endif
            </div>
            <div class = 'col-md-12 bank-ctrl'>
            @if($login->trial_flag == 0)
            {{ Form::select('bank', $conv_data['bank_list'], $conv_data['current_bank'], array('id' => 'bank', 'class' => 'form-control')) }}
            @else
            {{ Form::select('bank', $conv_data['bank_list'], $conv_data['current_bank'], array('id' => 'bank', 'class' => 'form-control', 'disabled' => 'disabled')) }}
            @endif
            </div>
            @if (0 != $conv_data['current_bank'])
                <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["bank_name"] }}' />
            @else
                <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = 'Applying Independently' />
            @endif
        </div>
        <script src="{{ URL::asset('js/editable-bank.js') }}"></script>

    @elseif (isset($province['province_val']))
        {{ Form::select('province', $province['province_list'], $province['province_val'], array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table,'id' => 'province','onchange' => 'getCities()')) }}
        {{ Form::select('cityid', $province['city_list'], $province['city_val'], array('class' => 'form-control','id' => 'city')) }}
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["province_txt"] }}' />
        <script src="{{ URL::asset('js/editable-province.js') }}"></script>

    @elseif (isset($affiliates_province['province_val']))
        {{ Form::select('related_company_province', $affiliates_province['province_list'], $affiliates_province['province_val'], array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table)) }}
        {{ Form::select('related_company_cityid', $affiliates_province['city_list'], $affiliates_province['city_val'], array('class' => 'form-control')) }}
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["province_txt"] }}' />
        <script src="{{ URL::asset('js/editable-related-province.js') }}"></script>

	@elseif (isset($keymanager_province['province_val']))
        {{ Form::select('province', $keymanager_province['province_list'], $keymanager_province['province_val'], array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table)) }}
        {{ Form::select('cityid', $keymanager_province['city_list'], $keymanager_province['city_val'], array('class' => 'form-control')) }}
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["province_txt"] }}' />
        <script src="{{ URL::asset('js/editable-province.js') }}"></script>
		
	@elseif (isset($owner_province['province_val']))
        {{ Form::select('province', $owner_province['province_list'], $owner_province['province_val'], array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table)) }}
        {{ Form::select('cityid', $owner_province['city_list'], $owner_province['city_val'], array('class' => 'form-control')) }}
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $conv_data["province_txt"] }}' />
        <script src="{{ URL::asset('js/editable-province.js') }}"></script>

    @elseif (EDITABLE_PARAGRAPH == $fld_typ)
        <script>
            $(document).ready(function() {
                $('#editable-field-form').parent().css('display', 'inline');
            });
        </script>
        
        <textarea name = '{{ $field_name }}' rows = '7' style = 'width: 100%; resize: none' class = 'form-control' data-field-id =' {{ $field_id }}' data-arr-tb = '{{ $arr_table }}'>{{ $field_value }}</textarea>
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $prev_value }}' />
    @elseif (STS_OK == $conv_sts)
        {{
            Form::select($field_name, $conv_data, (isset($conv_data[$field_value])) ? $field_value : '', array('class' => 'form-control edit-input-field', 'data-field-id' => $field_id, 'data-arr-tb' => $arr_table))
        }}
        <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ (isset($conv_data[$field_value])) ? $conv_data[$field_value] : $conv_data['']  }}' />
    @else
        @if($field_name == 'company_tin' || $field_name == 'id_no')
            <input class = 'edit-input-field form-control' type = 'text' onkeydown="addHyphen(this)" id="companyTin" maxlength="15" value = '{{ $field_value }}' name = '{{ $field_name }}' data-field-id = '{{ $field_id }}' data-arr-tb = '{{ $arr_table }}'/>
        @else
            <input class = 'edit-input-field form-control' type = 'text' id="editInputField" value = '{{ $field_value }}' name = '{{ $field_name }}' data-field-id = '{{ $field_id }}' data-arr-tb = '{{ $arr_table }}'/>
        @endif
            <input type = 'hidden' id = 'prev_val' name = 'previous_val' value = '{{ $prev_value }}' />
    @endif
    <span id="loading" hidden> 
        <img  src="{{ URL::asset('images/flowing_gradient.gif')}}" width="20px" height="20px">
    </span>
    <span id="addCancel">
        <button class = 'btn btn-default' id = 'submit-editable' {{ isset($conv_data['bank_list']) && $login->trial_flag == 1 ? 'style="display: none"' : ''}}> <i class = 'glyphicon glyphicon-ok'> </i></button>
        <button type = 'button' class = 'btn btn-danger cancel-editables' id = 'close-editable'> <i class = 'glyphicon glyphicon-remove'> </i></button>
    </span>
    @if (isset($industry['main_val']))
        <button type = 'button' class = 'btn btn-default' id = 'live-search' > <i class = 'glyphicon glyphicon-search' style="color:green" > </i></button>
    @endif
    
    <p class = 'editable-error-msg' style = 'display: none;'> </p>
    
{{ Form::close() }}

@if (isset($industry['main_val']))


<!-- modal should have min-height and max-height body and must be scrollable. -->
<div id="industry_live_search" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" id="search_industry" class="form-control" placeholder="search ... "/>
                </div>
                <!-- <input type="hidden" id="token" value=/> -->
                @csrf
                <div class="show_results" id="show_results">
                
                </div>
            </div>
            <div class="modal-footer">
                <span class="save_status"></span>
                <button type="button" id="modalClose" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnSave" class="btn btn-primary btn-setting-save">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- <script src="{{ URL::asset('js/1.11.2.jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script> -->
<script>
	var search_row = null;
    var curr_row = null;
    var curRow = null;
    var curSub = null;
// $(document).ready(function(){

    $("#live-search").on("click", function(e) {
        curr_row = $("#industryrow").val();
        // $("#industryrow").val("");
        $("#industry_live_search").modal({
            keyboard: false,
            backdrop: 'static'
        });
    });

    $("#search_industry").on("keyup",function(ths){
        let str = $(this).val()
        if(str.length >= 3) {
            $.ajax({
                url: BaseURL + '/industry_search',
                type: 'get',
                data: {
                    'search' : str
                },
                beforeSend: function(){
                    //  do before send page load
                },
                success: function(resp){
                    if(resp.html) {
                        $("#show_results").html(resp.html);
                    } else {
                        $("#show_results").html("<ul><li><i>No data found!</i></li></ul>");
                    }

                }
            });
        }
    });

    // // Do thing after close
    $('#industry_live_search').on('hidden.bs.modal', function () {
        $("#show_results").html(" ");
        $("#search_industry").val("");
        curRow = null;
        curSub = null;
    });

    // Do things after search selection
    $("#btnSave").on("click",function(){
        let search = $("input[name=row_title]:checked").val();
        $.ajax({
            url: BaseURL + '/process_search',
            type: 'get',
            data: {
                industry: search
            },
            success: function(resp){
                if(resp.success) {
                    $("#industry_live_search").modal("hide");
                    $("#industrymain").val(resp.main);
                    get_industry("sub", resp.sub, resp.row);
                }
            }
        }); 
    })
// });

function get_industry(tier, sub, row) 
{
    if(row) {
        search_row = row;
    }
    var url = ""
    var index = 0;
    if(tier == "main") {
        url = 'signup/getIndustryMain';
    } else if (tier == "sub") {
        index = $("#industrymain").val();
        url = 'signup/getIndustrySub/' + index;
    } else {
        index = $("#industrysub").val();
        url = 'signup/getIndustryRow/' + index;
    }

    $.ajax({
        type: 'get',
        url: BaseURL + '/' +  url,
        success: function(data){
             var industry  = "";
            for (var i = 0; i < data.length; i++){
                if(tier == "main") {
                    industry += '<option value="'+data[i].main_code+'">'+data[i].main_title+'</option>';
                } else if (tier == "sub") {
                    industry += '<option value="'+data[i].sub_code+'">'+data[i].sub_title+'</option>';
                } else {
                    industry += '<option value="'+data[i].group_code+'">'+data[i].group_description+'</option>';
                }
            }
            $('#industry' + tier).html(" ");
            $('#industry' + tier).append(industry);
            if(sub && tier == "sub") {
                $("#industrysub").val(sub).change();
            } else if(search_row && tier == "row"){
                $("#industryrow").val(search_row);
                search_row = null;
            }else{
                if(tier != "row") {
                    $("#industry" + tier).change();
                }
            }
        },
        error: function(){
        }
    });
}
    // var search_row = null;
    // var curr_row = null;
    // $(document).ready(function(){
    //     $("#live-search").on("click", function(e) {
    //         curr_row = $("#industryrow").val();
    //         $("#industryrow").val("");
    //         $("#industry_live_search").modal({
    //         keyboard: false,
    //         backdrop: 'static'
    //         });
    //     });
    //     $("#search_industry").on("keyup",function(ths){
    //         let str = $(this).val()
    //         if(str.length >= 3) {
    //             $.ajax({
    //                 url: BaseURL + '/industry_search',
    //                 type: 'post',
    //                 data: {
    //                     search: str
    //                 },
    //                 beforeSend: function(){
    //                     //  do before send page load
    //                 },
    //                 success: function(resp){
    //                     if(resp.html) {
    //                         $("#show_results").html(resp.html);
    //                     } else {
    //                         $("#show_results").html("<ul><li><i>No data found!</i></li></ul>");
    //                     }

    //                 }
    //             });
    //         }
    //     });
    //     // Do things after search selection
    //     $("#btnSave").on("click",function(){
    //         let search = $("input[name=row_title]:checked").val();
    //         $.ajax({
    //             url: BaseURL + '/process_search',
    //             type: 'post',
    //             data: {
    //                 industry: search
    //             },
    //             success: function(resp){
    //                 if(resp.success) {
    //                     $("#industry_live_search").modal("hide");
    //                     $("#industrymain").val(resp.main).change();
    //                     curRow = resp.row;
    //                     curSub = resp.sub;
    //                     $("#editable-field-form").submit();
    //                 }
    //             }
    //         }); 
    //     });	
    //     // Do thing after close
    //     $('#industry_live_search').on('hidden.bs.modal', function () {
    //         $("#show_results").html(" ");
    //         $("#search_industry").val("");
    //         $("#industryrow").val(curr_row);
    //         curr_row = null;
    //     });
    // });
</script>

@endif