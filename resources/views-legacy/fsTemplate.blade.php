@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')

@if(!Session::has('tql') || (Session::has('tql') && in_array('financial-statement-cntr', $tql->data)))
@if (Auth::user()->role != 2)
<div id="fs-template-cntr" class="spacewrapper print-none">
  <div class="read-only">
    <h4>{{ trans('business_details1.financial_statement_template') }}</h4>
    <div class="read-only-text">
      <div class="col-md-12">
        <p style="text-align: justify;">
          {{ trans('business_details1.financial_statement_template_hint') }}
        </p>
        <br />
        <!-- <a href="https://creditbpo.com/s/FS-Input-Template.xls" target="_blank"> -->
        <a  class='btn btn-link' id='download_fs_link' href="{{ URL::to('/') }}/getFsTemplateFile/{{ $entity->entityid }}">
          {{ trans('business_details1.financial_statement_template_link') }}
        </a>
      </div>

      @if(Auth::user()->role == 2 || Auth::user()->role == 3)
      <div class="completeness_check">{{ (count($fs_template) >= 1) ? "true" : "" }}</div>
      @endif

      <div id = 'fs-template-list-cntr' class="details-row">
        <div class = 'reload-fs-template'>

          @if ($doc_req)
          @if (STS_NG == $doc_req->fs_template)
          <span class="progress-field-counter" min-value="1">
            @else
            <span class="progress-required-counter" min-value="1">
              @endif
              @else
              <span class="progress-required-counter" min-value="1">
                @endif
                @if (0 >= count($fs_template))
                0
                @else
                1
                @endif
              </span>

              @if (count($fs_template) != 0)
              @foreach ($fs_template as $documents2)
              <div class="grouping">
                <div class="col-md-2">
                  <span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents2->documentid }}'>
                    @if($documents2->document_type == 1)
                    {{trans('business_details1.unaudited')}}
                    @elseif($documents2->document_type == 2)
                    {{trans('business_details1.interim')}}
                    @elseif($documents2->document_type == 4)
                    {{trans('business_details1.recasted')}}
                    @else
                    {{trans('business_details1.audited')}}
                    @endif
                  </span>
                </div>
                <div class="col-md-8">
                  <a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
                </div>
                <div class="col-md-2">
                  @if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
                  <a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents2->documentid }}" class="navbar-link delete-info" data-reload = '.reload-fs-template' data-cntr = '#fs-template-list-cntr'>{{ trans('messages.delete') }}</a>
                  @endif
                  <span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents2->document_orig }}" group="documents" item="balance_sheet_{{$documents2->documentid}}"></span>
                </div>
              </div>
              @endforeach
              @endif

              <div id = 'expand-fs-template-form' class = 'spacewrapper'></div>

              @if ($entity->status == 0 or $entity->status == 3)
              <span class="details-row" style="text-align: right;">
                @if ($entity->loginid == Auth::user()->loginid)
                @if (0 >= count($fs_template))
                <a id = 'show-fs-template-expand' href="{{ URL::to('/') }}/sme/upload_fs_template_premium/{{ $entity->entityid }}" class="navbar-link">Upload {{ trans('business_details1.financial_statement_template') }}</a>
                @endif
                @endif
              </span>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif

    @if($bank_standalone==0)
    @if (Auth::user()->role != 2)
    <div id="financial-statement-cntr" class="spacewrapper print-none">
     <div class="read-only">
       <h4>{{trans('business_details1.financial_statement')}}</h4>
       <div class="read-only-text">
        <div class="col-md-12">
         <p>{{trans('business_details1.document_upload_hint')}}</p>
         <p>{{trans('business_details1.upload_balance_sheet')}}</p>
         <p>{{trans('business_details1.upload_income_statement')}}</p>
         <p>E.g.:</p>
         <p>{{trans('business_details1.balance_sheet_eg')}}</p>
         <p>{{trans('business_details1.income_statement_eg')}}</p>
       </div>

       <div class="col-md-12">
        <p><b>Financial Statements</b></p>
      </div>
      <div id = 'financials-list-cntr' class="details-row">
        <div class = 'reload-financials'>

          @if (count($fs_supporting) != 0)
          @foreach ($fs_supporting as $documents2)
          <div class="grouping">
            <div class="col-md-2">
              <span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents2->documentid }}'>
                @if($documents2->document_type == 1)
                {{trans('business_details1.unaudited')}}
                @elseif($documents2->document_type == 2)
                {{trans('business_details1.interim')}}
                @elseif($documents2->document_type == 4)
                {{trans('business_details1.recasted')}}
                @else
                {{trans('business_details1.audited')}}
                @endif
              </span>
            </div>
            <div class="col-md-8">
              <a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
            </div>
            <div class="col-md-2">
              @if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
              <a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents2->documentid }}" class="navbar-link delete-info" data-reload = '.reload-financials' data-cntr = '#financials-list-cntr'>{{ trans('messages.delete') }}</a>
              @endif
              <span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents2->document_orig }}" group="documents" item="balance_sheet_{{$documents2->documentid}}"></span>
            </div>
          </div>
          @endforeach
          @endif
          @if (count($documents3) != 0)
          @foreach ($documents3 as $documents3)
          <div class="grouping">
            <div class="col-md-2">
              <span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents3->documentid }}'>
                @if($documents3->document_type == 1)
                {{trans('business_details1.unaudited')}}
                @elseif($documents3->document_type == 2)
                {{trans('business_details1.interim')}}
                @elseif($documents2->document_type == 4)
                {{trans('business_details1.recasted')}}
                @else
                {{trans('business_details1.audited')}}
                @endif
              </span>
            </div>
            <div class="col-md-8">
              <a href="{{ URL::to('/') }}/download/documents/{{ $documents3->document_rename }}" target="_blank">{{ $documents3->document_orig }}</a>
            </div>
            <div class="col-md-2">
              @if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
              <a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents3->documentid }}" class="navbar-link delete-info" data-reload = '.reload-financials' data-cntr = '#financials-list-cntr'>delete</a>
              @endif
              <span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.income_statement',[],'en')}}" value="{{ $documents3->document_orig }}" group="documents" item="income_statement_{{$documents3->documentid}}"></span>
            </div>
          </div>
          @endforeach
          @endif

          <div id = 'expand-financials-form' class = 'spacewrapper'></div>
        </div>
      </div>
      @if ($entity->status == 0 or $entity->status == 3)
      <span class="details-row" style="text-align: right;">
       @if ($entity->loginid == Auth::user()->loginid)
       <a id = 'show-financials-expand' href="{{ URL::to('/') }}/sme/upload_documents/{{ $entity->entityid }}" class="navbar-link">Upload {{trans('business_details1.financial_statement')}}</a>
       @endif
     </span>
     @endif
     @if ($entity->status == 0 or $entity->status == 3)
     <span class="details-row">
      <div class="col-md-12">
        @if(count($documents2)==0 && count($documents3)==0)
        <p style="color: RED;">{{ trans('business_details1.upload_fs_reminder') }}</p>
        @endif
      </div>
    </span>
    @endif
  </div>
</div>
</div>
@endif
@endif
@endif

@include('user-2fa-form')
@include('modal-add-info')
@include('modal-upload-file')
@include('modal-refresh-warning')
@if(Auth::user()->role == 1 && $entity->status == 0)
@include('modal-external-link')
@include('modal-external-link-notification')
@endif
@include('modal-confirm-sec-override')
@stop

@section('javascript')
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/busidentification.js') }}"></script>
<script src="{{ URL::asset('js/creditfacilities.js') }}"></script>
<script src="{{ URL::asset('js/smesummary.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/index.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>

@if($entity->status == 7)
<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/dscrexport.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>
@endif

@if(Session::has('redirect_tab'))
<script type="text/javascript">
  $(document).ready(function(){
   ClickTab("{{Session::get('redirect_tab')}}");
 });
</script>
@endif
@if(Auth::user()->role == 1)
<script src="{{ URL::asset('js/smesummary_progressbar.js') }}"></script>
@endif
@if(Auth::user()->role == 2 || Auth::user()->role == 3)
<script src="{{ URL::asset('js/smesummary_tab_indicator.js') }}"></script>
@endif
@if ((Auth::user()->role == 1) && ($entity->status == 0))
<script type="text/javascript" src="{{ URL::asset('js/cmn-lib/cmn-upd-info.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz1.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-ecf.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-owner.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-bcs.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-rcl.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/summary-ccc.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/toggle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/external-link.js') }}"></script>
@endif
<script src="{{ URL::asset('js/smesummary_investigator.js') }}"></script>
<script src="{{ URL::asset('js/chart-min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.printarea.js') }}"></script>
<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison2.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison3.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison4.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison5.js') }}"></script>
<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
<script src="{{ URL::asset('js/graphs-strategic-forecast.js') }}"></script>
<script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
<script src="{{ URL::asset('js/next-tab-msg.js') }}"></script>
<script src="{{ URL::asset('js/jquery.form.min.js') }}"></script>

@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
<script src="{{ URL::asset('js/tutorial.js') }}"></script>
<script type="text/javascript">
 $(window).load(function(){
  Tutorial.init('step3');
});
</script>
@endif
@if (1 == $bank_ci_view)
<script type="text/javascript">
  $(document).ready(function(){
    $('.investigator-docs input[type="checkbox"').each(function(){
     $(this).prop('disabled', true);
   });

    $('textarea[name="notes"]').prop('disabled', true);
    $('input[name="investigator_others_docs"]').prop('disabled', true);
  });
</script>
@endif
<script>
function disableDSCRExportButton(){
  setTimeout( function(){
    $('#export-dscr-btn').attr('disabled', 'disabled');
    $('#export-dscr-btn').text("Exported to Dashboard");
  }, 2000);
}
</script>

<script type="text/javascript">
  $(window).bind("load", function(){
    if({{ $export_dscr }} == 1){
      disableDSCRExportButton();
    }
  });
</script>
@stop
@section('style')
@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag!=2)
<link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"/>
@endif
@if(Session::has('tql'))
<link rel="stylesheet" href="{{ URL::asset('css/external-link.css') }}" media="screen"/>
@endif
<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="print"/>
<link rel="stylesheet" href="{{ URL::asset('css/debt-service-calculator.css') }}" media="print"/>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>
@stop
