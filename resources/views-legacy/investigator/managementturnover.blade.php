@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Management Turnover</h1>
		    {{ Form::model($entity, ['route' => ['putManagementturnoverUpdate', $entity->mturnoverid], 'method' => 'put']) }}
		@else
			<h1>Add: Management Turnover</h1>
		    {{ Form::open( array('route' => 'postManagementturnover', 'role'=>'form')) }}
			{{ Form::hidden('ownerid', $ownerid, array('class' => 'form-control')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('executive_team')) ? ' has-error' : '' }}">
							{{ Form::label('executive_team', 'Executive Team') }} *
							{{ Form::select('executive_team',
							array(
							'' => 'Choose here',
							'1' => 'High',
							'2' => 'Medium',
							'3' => 'Low'
							), Input::old('executive_team'), array('id' => 'executive_team', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('middle_management')) ? ' has-error' : '' }}">
							{{ Form::label('middle_management', 'Middle Management') }} *
							{{ Form::select('middle_management',
							array(
							'' => 'Choose here',
							'1' => 'High',
							'2' => 'Medium',
							'3' => 'Low'
							), Input::old('middle_management'), array('id' => 'middle_management', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('staff')) ? ' has-error' : '' }}">
							{{ Form::label('staff', 'Staff') }} *
							{{ Form::select('staff',
							array(
							'' => 'Choose here',
							'1' => 'High',
							'2' => 'Medium',
							'3' => 'Low'
							), Input::old('staff'), array('id' => 'staff', 'class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
