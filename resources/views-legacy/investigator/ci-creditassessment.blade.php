@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Creditlines Assessment</h1>
		    {{ Form::model($entity, ['route' => ['putCreditassessmentUpdate', $entity->ccinvestigationid], 'method' => 'put']) }}
		@else
			<h1>Add: Creditlines Assessment</h1>
		    {{ Form::open( array('route' => 'postCreditassessment', 'role'=>'form')) }}
			{{ Form::hidden('ownerid', $ownerid, array('class' => 'form-control')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('banking_credit_experience')) ? ' has-error' : '' }}">
							{{ Form::label('banking_credit_experience', 'Banking and Credit Experience') }} *
							{{ Form::select('banking_credit_experience',
							array(
							'' => 'Choose here',
							'1' => 'Has clean legal record with banks and other Fis and up to date with his payments',
							'2' => 'Has adverse records but have been resolved and creditor was issued clearance',
							'3' => 'Has adverse records not yet resolved or cleared by creditors'
							), Input::old('banking_credit_experience'), array('id' => 'banking_credit_experience', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('credit_facility_ready')) ? ' has-error' : '' }}">
							{{ Form::label('credit_facility_ready', 'Existing Credit Facilities') }} *
							{{ Form::select('credit_facility_ready',
							array(
							'' => 'Choose here',
							'1' => 'Has Clean Bank/FI credit facility',
							'2' => 'Has Collateralized Bank/FI credit facility',
							'3' => 'Has no etablished credit facility '
							), Input::old('credit_facility_ready'), array('id' => 'credit_facility_ready', 'class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
