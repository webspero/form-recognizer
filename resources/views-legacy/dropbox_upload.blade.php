@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
	<style>
		.modal-body{
			word-wrap: break-word;
		}
		.mb-3 {
			margin-bottom: 10px;
		}
	</style>
@stop

@section('content')
	<div class="container">
		<h1>{{trans('messages.file_storage')}}</h1>
			{{ Form::open( array('route' => 'postDropboxUpload', 'role'=>'form', 'enctype' => 'multipart/form-data')) }}
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('messages.choose_file')}}</h4>
				<div class="clear" style="clear: both"></div>
				@if (is_array($errors))
				<br/>
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				   
				@endif

				<div class="clear" style="clear: both"></div>
				
				<div class="read-only-text">
					@csrf
					<div class="row">
					         <div class = 'col-md-12'>
					            <div class="alert alert-danger pop-error-msg"
					               role="alert" style = 'display: none;'>
					            </div>
					         </div>
					   </div>
					<div class="form-group row">
						 <label for="dropboxfile" class="col-sm-2 col-form-label text-md-right">{{ trans('messages.upload_file') }}</label>

						 <div class="col-md-4">
							  <input id="dropboxfile" type="file" class="form-control" name="dropbox_file[]" value="{{ old('dropboxfile') }}" required autofocus>
						 </div>
						 <div class="col-md-1">
							  <button type="submit" class="btn btn-primary" id="file-storage-btn">
									{{ trans('messages.upload') }}
							  </button>
						 </div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
		
		<div class="spacewrapper">
				<div class="read-only">
					<h4>{{trans('messages.files_list')}}</h4>
					<div class="read-only-text">
						@if (session('delete'))
						<div class="col-md-12 text-center alert-danger showMessage">
							<span class="alert-danger">
									{{ trans('messages.file_deleted') }}</span>
						 </div>
						 @elseif(session('status'))
						 <div class="col-md-12 text-center alert-success showMessage">
							 <span class="alert-success">
									 {{ trans('messages.file_uploaded') }}</span>
						  </div>
						 @endif
						 
						@if ($folderUrl != '')
							<!-- <span data-toggle="modal" data-target="#shareLinkModal">
								<a title="Share" data-toggle="tooltip" data-placement="top" class="share-link btn btn-primary btn-md mb-3" data-id="{{ $folderUrl->folder_url }}"> {{ trans('messages.share_folder') }} <span class="glyphicon glyphicon-share"></span></a>
							</span> -->

							 <div class="col-md-12 text-center alert-success copied hidden">
							 <span class="alert-success">{{ trans('messages.link_copied') }}</span>
							  </div>
								<table class="table table-hover table-bordered">
										<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">{{ trans('messages.file_name') }}</th>
											<th scope="col" class="text-center">{{ trans('messages.action') }}</th>
										</tr>
										</thead>
										<tbody>
										
									@foreach($userFiles as $key => $filename)
										<tr>
											<th scope="row" class="text-center">{{ $key+1 }}</th>
											<td><a href="@php echo !filter_var($filename->file_download, FILTER_VALIDATE_URL) ? url('sme/download?directory=user-files/'.$filename->login_id.'&filename='.$filename->file_name) : $filename->file_download @endphp">{{ $filename->file_name }}</a></td>
											<td class="text-center">
											<a title="{{ trans('messages.download')}}" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="@php echo !filter_var($filename->file_download, FILTER_VALIDATE_URL) ? url('sme/download?directory=user-files/'.$filename->login_id.'&filename='.$filename->file_name) : $filename->file_download @endphp"><span class="glyphicon glyphicon-download-alt"></span></a>
											<!-- <span data-toggle="modal" data-target="#shareLinkModal">
												<a title="{{ trans('messages.share')}}" data-toggle="tooltip" data-placement="top" class="share-link btn btn-primary btn-sm" data-id="{{ $filename->file_url }}"><span class="glyphicon glyphicon-share"></span></a>
											</span> -->
											<span data-toggle="modal" data-target="#deleteModal">
													<a title="{{ trans('messages.delete')}}" data-toggle="tooltip" data-placement="top" class="delete-file btn btn-danger btn-sm" data-id="{{ $filename->id }}" data-filename="{{ $filename->file_name }}"><span class="glyphicon glyphicon-trash"></span> </a>
											</span>
											</td>
										</tr>
									@endforeach
										</tbody>
								</table>
							@else
								<div class="col-md-12 text-center alert-warning">
									<span class="alert-warning">{{ trans('messages.no_files') }}</span>
								</div>
							@endif 
					</div>
				</div>
			</div>
	</div>
	<!-- Share Link Modal-->
	<div class="modal fade" id="shareLinkModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
			  <div class="modal-content">
				 <div class="modal-header">
					<h4 class="modal-title">{{ trans('messages.share_link') }}</h4>
				 </div>
				 <div class="modal-body">
					 <div>
							<a href="" target="_blank" id="shareLink" value=""></a>
					 </div>
					<div class="text-center">
							<button type="button" class="btn btn-success" data-dismiss="modal" id="copyButton">{{ trans('messages.copy_link') }}</button>
					</div>
				 </div>
				 <div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.back') }}</button>
				 </div>
			  </div>
			</div>
	</div>
	<!-- Delete Modal-->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
			{{ Form::open( array('route' => 'postDropboxDelete', 'role'=>'form')) }}
			<div class="modal-dialog modal-dialog-centered" role="document">
			  <div class="modal-content">
				 <div class="modal-header">
					<h4 class="modal-title">{{ trans('messages.delete_confirmation') }}</h4>
				 </div>
				 <div class="modal-body">
					 <p type="text" id="fileName" value=""></p>
					 <input type="hidden" name="fileId" id="fileId" value="">
				 </div>
				 <div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
					<button type="submit" class="btn btn-danger">{{ trans('messages.delete') }}</button>
				 </div>
			  </div>
			</div>
			{{ Form::close() }}
	</div>
@stop
@section('javascript')
	
	<script type="text/javascript">
      $('#dropboxfile').bind('change', function() {
         var fileSize = this.files[0].size/1024/1024;
         
         if(fileSize > 40){
            $('.pop-error-msg').show();
            $('.pop-error-msg').html('Maximum file upload is 40MB');
            $('#file-storage-btn').prop('disabled',true);

         }else{
            $('.pop-error-msg').hide();
            $('.pop-error-msg').html('');
            $('#file-storage-btn').prop('disabled',false);
         }
     });
   </script> 

	<script src="{{ URL::asset('js/signup.js') }}"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.showMessage').show();
			setTimeout(function() {
				$('.showMessage').fadeOut();
			}, 3000);
		});

		$(function () {
		$('[data-toggle="tooltip"]').tooltip()
		});
	</script>
	<script>
		$(document).on("click", ".delete-file", function () {
     var fileName = $(this).data('filename');
     var fileId = $(this).data('id');
     $(".modal-body #fileName").text( fileName );
     $(".modal-body #fileId").val( fileId );
		});
	</script>
	<script>
		$(document).on("click", ".share-link", function () {
     var link = $(this).data('id');
     $(".modal-body #shareLink").text(link);
     $(".modal-body #shareLink").attr("href", link);
		});
	</script>
	<script>
		$(document).ready(function(){
		$('#copyButton').click(function(){

			var text = $("#shareLink").get(0)
			var selection = window.getSelection();
			var range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
			//add to clipboard.
			document.execCommand('copy');
			
			$('.copied').removeClass('hidden');
			$('.copied').show();
			setTimeout(function() {
				$('.copied').fadeOut();
			}, 3000);
		})

		});
	</script>
@stop
@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>.toggle{ width:100% !important; }</style>
@stop
