@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')

<div class="container">

	<div class="spacewrapper">
		<div class="read-only">
			<h4> BSP Bank List Extraction </h4>

			<div class="read-only-text">
				<div id = 'bank_list'> </div>
				<div id = 'extraction_container' style = 'display: none;'> </div>
			</div>
		</div>

	</div>

</div>
@stop

@section('javascript')
<script src="{{ URL::asset('js/bsp-bank-extraction.js') }}"></script>
@stop
