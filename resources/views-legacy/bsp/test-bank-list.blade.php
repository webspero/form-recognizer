<table>
	<tr>
		<th> Name </th>
		<th> Contact Person </th>
		<th> Designation </th>
		<th> Address </th>
		<th> No. of offices </th>
		<th> Phone </th>
		<th> Fax </th>
		<th> Email </th>
		<th> Website </th>
	</tr>
	
	@foreach ($banks as $bank)
	<tr>
		<td> {{ $bank->bank_name }} </td>
		<td> {{ $bank->contact_person }} </td>
		<td> {{ $bank->contact_person_designation }} </td>
		<td> {{ $bank->bank_street_address1 }} </td>
		<td> {{ $bank->bank_no_of_offices }} </td>
		<td> {{ $bank->bank_phone }} </td>
		<td> {{ $bank->bank_fax }} </td>
		<td> {{ $bank->bank_email }} </td>
		<td> {{ $bank->bank_website }} </td>
	</tr>
	@endforeach
</table>