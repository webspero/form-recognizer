

    <div id = 'forgot-password-dialog' title = 'Reset Password' style = 'display:none;'>
        {{ Form::open(array('id' => 'forgot-password-form')) }}
            <div id = 'password-error' class="errormessage"> </div>
            <div class="form-group">
                {{ Form::label('notif-code', 'Notification Code') }}
                {{ Form::text('notif-code', '', array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('new-password', 'Password') }}
                {{ Form::password('new-password', array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('confirm-password', 'Confirm Password') }}
                {{ Form::password('confirm-password', array('class' => 'form-control')) }}
            </div>
        {{ Form::close() }}
    </div>