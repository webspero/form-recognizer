@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<div class="spacewrapper">
		<div class="read-only">
		<h4>Payment Selection</h4>
			<div class="read-only-text">
				<h3>Choose your preferred payment option:</h3>
				<hr />
				<div class="row">
					<div class="col-sm-6 text-center">
						<p>For Paypal and Credit Card transactions use:</p>
						<a href="{{URL::to('/')}}/payment_step2/{{$code}}/paypal">
							<img src="{{URL::to('/')}}/images/paypal_option.jpg" style="margin-top: 35px;"/>
						</a>
					</div>
					<div class="col-sm-6 text-center">
						<p>For Online Banking and Over-the-counter transactions use:</p>
						<a href="{{URL::to('/')}}/payment_step2/{{$code}}/dragonpay">
							<img src="{{URL::to('/')}}/images/dragonpay_option.jpg" />
						</a>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
@stop
