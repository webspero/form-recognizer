<div class="modal" id="modal-file-uploader" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1> Upload: <span id = 'pop-upload-hdr-lbl'> </span> </h1> </span></h4>
            </div>
            
            <div class="modal-body" id = 'file-uploader-body'>
                <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
                
                {{ Form::open( array('url' => '', 'role'=>'form', 'id' => 'file-uploader-form', 'files' => true)) }}
                    <input type="file" name="editable-file-uploader" id="editable-file-uploader" class="form-control"/>
                    <small class="small_text accepted-format-1">Accepted file formats: pdf,zip,rar,jpeg,png,csv,docx &nbsp;&nbsp;&nbsp;</small>
                {{ Form::close() }}
				<div class="clearfix"></div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
                <button type="submit" class="btn btn-large btn-primary pop-uploader-btn"> {{ trans('messages.save') }} </button>
            </div>
                
        </div>
    </div>
</div>