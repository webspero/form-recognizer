@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
                <h1>Unlock Email</h1>
                {{ Form::open( array('route' => 'postEmailUnlock', 'role'=>'form')) }}
                @if($errors->has('email'))
                    {{ $errors->first('email', '<span class="errormessage">:message</span>') }}
                @endif
                @if(Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                @elseif(Session::has('fail'))
                        <div class="alert alert-danger">{{ Session::get('fail') }}</div>
                @endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
                                            <div class="col-md-12{{ ($errors->has('email')) ? ' has-error' : '' }}">
                                            {{ Form::label('email', 'Email') }}
                                            {{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
                                            </div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
