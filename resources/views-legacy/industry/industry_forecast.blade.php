@extends('layouts.master', array('hide' => true))

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">	
		<div class="spacewrapper">
			<h1>CreditBPO Industry Comparison Data</h1>
			<div class="read-only">
				<div class="read-only-text">
					@if($errors->any())
						<div class="alert alert-danger" role="alert">
							@foreach($errors->all() as $e)
								<p>{{$e}}</p>
							@endforeach
						</div>
					@endif

					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif
					<a href="{{ URL::asset('documents/industry/Template.xlsx') }}">Download Industry Comparison Template</a>
					<br> <br>

					{{ Form::open( array('route' => array('postIndustryForecast'), 'role'=>'form', 'files' => true)) }}
					<div>
					{{ Form::file('industry_file', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'id' => 'industry_file'], array('multiple'=>true)) }}
						<br>
						<input type="submit" class="btn btn-large btn-primary" value="Upload File">
					</div>
					{{ Form::close() }}
			</div>
		</div>
	</div>
@stop
@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>.toggle{ width:100% !important; }</style>
@stop
