<!DOCTYPE html>
<html lang="en">
<head>
   
      <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
    
            <title>results</title>
    
            <!-- Fonts -->
            <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    
            <!-- Styles -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        </head>
</head>
<body>

      <div class="row text-center">
            <div class="col-md-6">
               <table class="table table-bordered thead-dark">
                  <thead>
                     <tr>
                        <th>DATE</th>
                        <th>BALANCE</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach ($dateBalances as $key => $dateBalance)
                     <tr>
                           <td>{{ array_key_exists("date", $dateBalance) ? $dateBalance['date'] : '' }}</td>
                           <td>{{ array_key_exists("balance", $dateBalance) ? $dateBalance['balance'] : '' }}</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            <div class="col-md-6">
                  <span><h3>Average Daily Balance: </h3></span>
                  <p><b>{{ $averageBalance }}</b></p>
            </div>
        </div>
   
</body>
</html>

{{-- @foreach ($results->text() as $result)
<span>{{ $text= $result->description() }}</span><br>
@endforeach --}}