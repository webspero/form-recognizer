@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
			        <h3>You have cancelled your payment.</h3>
			    </div>
			</div>
		</div>
	</div>
@stop
