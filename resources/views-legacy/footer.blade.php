@if(Request::path() != "signup")
<div id="footer">
	<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
	<tr>
	<td width="135" align="center" valign="top">
    <!-- <script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=business.creditbpo.com&size=S&use_flash=NO&use_transparent=NO&lang=en"></script>
    <br />
	<a href="http://www.symantec.com/ssl-certificates" target="_blank" style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>

	<div style="clear: both"></div> -->

	<!-- Place HTML on your site where the seal should appear -->
	<div id="DigiCertClickID_FVNZFcCM"></div>
	<!-- DigiCert Seal Code -->
	<!-- Place with DigiCert Seal HTML or with other scripts -->
	<script type="text/javascript">
		var __dcid = __dcid || [];
		__dcid.push({"cid":"DigiCertClickID_FVNZFcCM","tag":"FVNZFcCM"});
		(function(){var cid=document.createElement("script");cid.async=true;cid.src="//seal.digicert.com/seals/cascade/seal.min.js";var s = document.getElementsByTagName("script");var ls = s[(s.length - 1)];ls.parentNode.insertBefore(cid, ls.nextSibling);}());
	</script>
	</tr>
	</table>
	<p>Copyright &copy; {{ date('Y') }} CreditBPO Tech, Inc. All rights reserved</p>
	<p class="small">CreditBPO and the CreditBPO marks used herein are registered trademarks of CreditBPO Tech, Inc.</p>
</div>
@endif

@section('requirejs')
    <script type="text/javascript">
        // require(['/static/js/modules/norton-seal/index.js'], function(nortonInstance) {
        //   nortonInstance.initialize();
        // });
    </script>
@stop
