@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<a href="{{URL::to('/')}}/signup/editreport/{{$entity_id}}">Go Back</a>
	<div class="spacewrapper">
		<div class="read-only">
		<h4>Payment Selection</h4>
			<div class="read-only-text">
				<h2>{{$display_amount}}</h2>
				<p>Reminder:
					This report is a preliminary tool on financial condition analysis for general licensing, accreditation and procurement analysis purposes. Any decision on this report’s use is completely determined by the requesting organization as it is only one of a multitude of factors being considered by the requesting organization in compliance with their own processes and objectives. Further, the accuracy of the generated report is dependent solely on the information provided. The user is therefore advised to upload only accurate information.
				</p>
				<p>
					<b>Please note that after payment, you will not be able to modify any information indicated on the report creation page, so make sure that all information provided are correct. You can still use the back button to return to the previous page and confirm all the information needed if necessary.</b>
				</p>
				<p>
					<b><span class="errormessage">Please do not close this page, otherwise you will not be able to pay for this report and will have to create a new report.</span></b>
				</p>
				<p>
					<b>Thank you.</p>
				</p>
				<h3>Choose your preferred payment option:</h3>
				<hr />
				<div class="row">
					<div class="col-sm-6 text-center">
						<p>For Paypal and Credit Card transactions use:</p>
						<a href="{{URL::to('/')}}/payment/checkout_paypal/{{$entity_id}}">
							<img src="{{URL::to('/')}}/images/paypal_option.jpg" style="margin-top: 35px;"/>
						</a>
					</div>
					<div class="col-sm-6 text-center">
						<p>For Online Banking and Over-the-counter transactions use:</p>
						<a href="{{URL::to('/')}}/payment/checkout_dragonpay/{{$entity_id}}">
							<img src="{{URL::to('/')}}/images/dragonpay_option.jpg" />
						</a>
					</div>
				</div>
				<hr/>
				<h6><em>*All prices are Tax-inclusive</em></h6>

			</div>
		</div>
	</div>
</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
