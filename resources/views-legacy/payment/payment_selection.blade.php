@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<a href="{{URL::to('/')}}/signup/editreport/{{$entity_id}}">{{ trans('messages.go_back') }}</a>
	<div class="spacewrapper">
		@if(Session::get('catch_error'))
			<span class="errormessage"><h1>For CreditBPO Personnel: </h1>{!! Session::get("catch_error", '') !!}</span>
			<?php Session::forget('catch_error') ?>
		@endif
		<div class="read-only">
			<div class="read-only-text">
				<div class="row payment-invoice">
					
					<div class="col-md-3 invoice-address">
						Level 10-1 One Global Place, 25th Street and 5th Ave.,<br/>
						BGC Taguig, MM 1634 PH
						{{ trans('messages.invoice_email') }}					
					</div>
					<div class="col-md-5"></div>
					<div class="col-md-4 invoice-logo">
						<img src="{{ asset('images/creditbpo-logo.png') }}" class="invoice-logo-img" />
					</div>

				</div>
				<div class="clear"></div>

				<h1 class="invoice-label">{{ trans('messages.invoice_label') }}</h1>

				<div class="clear"></div>

				<div class="row">
					<div class="col-md-3 invoice-bill-to">
						BILL TO<br/>
						{{ $user->street_address }}<br/>
						{{ $user->city.' '.$user->province.' '.$user->zipcode }}
					</div>				

					<div class="col-md-5"></div>

					<div class="col-md-3 invoice-no-details">
						
						<div class="invoice-no-details-div">
							<span class="inv-details-left">
								<strong>INVOICE NO.</strong>
							</span>
							<span class="inv-details-right"></span>
						</div>
						
						<div class="clear"></div>

						<div class="invoice-no-details-div">
							<span class="inv-details-left">
								<strong>DATE</strong>
							</span>
							<span class="inv-details-right">{{ date('m/d/Y') }}</span>
						</div>

						<div class="clear"></div>

						<div class="invoice-no-details-div">
							<span class="inv-details-left">
								<strong>DUE DATE</strong>
							</span>
							<span class="inv-details-right">{{ date('m/d/Y') }}</span>
						</div>

						<div class="clear"></div>

						<div class="invoice-no-details-div">
							<span class="inv-details-left">
								<strong>TERMS</strong>
							</span>
							<span class="inv-details-right">Due on receipt</span>
						</div>

					</div>
				</div>

				<div class="clear"></div>

				<div class="invoice-table-div">
					<table class="invoice-table">
						<thead>
							<th>ACTIVITY</th>
							<th>QTY</th>
							<th>RATE</th>
							<th>AMOUNT</th>
						</thead>
						<tr>
							<td>
								<strong>Financial Condition Analysis and Benchmarking<br/> Report: CreditBPO Rating Report&reg;</strong><br/>CreditBPO Rating Report&reg; ({{ $report_details['report_type'] }})<br/>Financial Condition Analysis and Benchmarking Report
							</td>
							<td class="text-center">1</td>
							<td class="text-right">{{ number_format($report_details['amount'],2,".",",") }}</td>
							<td class="text-right">{{ number_format($report_details['amount'],2,".",",") }}</td>
						</tr>

						<!-- <tr>
							<td>Tax</td>
							<td class="text-center">--</td>
							<td class="text-center">{{ $report_details['tax_rate'] }}</td>
							<td class="text-right">{{ number_format($report_details['tax_amount'],2,".",",") }}</td>
						</tr> -->

						<tr>
							<td>Discount</td>
							<td class="text-center">--</td>
							<td class="text-center">{{ $report_details['discount'] }}</td>
							<td class="text-right">{{ number_format($report_details['discount'],2,".",",") }}</td>
						</tr>

						<tr class="td-balance-due">
							<td>&nbsp;</td>
							<td class="text-center">BALANCE DUE</td>
							<td class="text-right" colspan="2">
								<h2><strong>PHP {{ number_format($report_details['total_amount'],2,".",",") }}</strong></h2>
							</td>
						</tr>

					</table>

				</div>

				<div class="payment-details-message">
					{{ trans('messages.payment_details') }}
				</div>

				<h3>{{ trans('messages.payment_option') }}</h3>
				<hr />
				<div class="row">
					<div class="col-sm-6 text-center">
						<p>{{ trans('messages.banks_available') }}</p>
						<a href="{{URL::to('/')}}/payment/checkout_paypal/{{$entity_id}}">
							<img src="{{URL::to('/')}}/images/paypal_option.jpg" style="margin-top: 35px;"/>
						</a>
					</div>
					<div class="col-sm-6 text-center">
						<p>{{ trans('messages.online_banking') }}</p>
						<a href="{{URL::to('/')}}/payment/checkout_dragonpay/{{$entity_id}}">
							<img src="{{URL::to('/')}}/images/dragonpay_option.jpg" />
						</a>
					</div>
				</div>
				<hr/>
				<h6><em>{{ trans('messages.tax_inclusive') }}</em></h6>
			</div>
		</div>
	</div>
</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
