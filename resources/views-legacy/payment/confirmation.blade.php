@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
        @if (1 == Auth::user()->role && 2 == Auth::user()->status)
		<h1>New Report Created</h1>
        @else
        <h1>Successful Transaction</h1>
        @endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
                    @if (1 == Auth::user()->role && 2 == Auth::user()->status)
			        <h1>Please take note of the following requirements.</h1>
                    @else
                    <h1>Congratulations! You have successfully activated your account.</h1>
                    @endif
					<h3>What you should know:</h3>
					<p><b>a.</b> It will take you roughly 2 hours to answer the questionnaire.</p>
					<p><b>b.</b> Your replies to these questions will be the basis for generating your business credit risk rating. Inaccurate and incomplete replies will result in an inaccurate rating.</p>
					<p>
                        <b>c.</b> The following documents are required in digital format:
                        <ul>
                            <li> SEC Registration Certificate, Articles of Incorporation and By-Laws (in pdf, jpg, or png format) </li>
                            <li>
                                4 periods of Consecutive Balance Sheets encoded into the FS Template (choice of audited, interim or unaudited)
                                <ul>
                                    <li> Minimum: 3 periods </li>
                                </ul>
                            </li>
                            <li>
                                3 periods of Consecutive Income Statements encoded into the FS Template (choice of audited, interim or unaudited)
                                <ul>
                                    <li> Minimum: 2 periods </li>
                                </ul>
                            </li>
                        </ul>
                    </p>
					<p><b>d.</b> Your data will be run through our risk rating algorithm.</p>
					<p><b>e.</b> It will take 24-48 hours after submission to email you your Credit Risk Rating report.</p>
					<p><b>f.</b> Please indicate your business email address. We may have to get in touch with you for some clarification.</p>
					<p><b>g.</b> All data you submit will be treated with the strictest confidentiality and non-disclosure restriction to third parties.</p>
                    @if (1 == Auth::user()->role && 2 == Auth::user()->status)
					<p>Now that you have read all of the above: <br/>
                    <a href="{{ URL::to('/') }}/payment/selection?id={{Input::get('id')}}" class="btn btn-primary">Click here to pay for the newly created report.</a></p>
                    @else
                    <p>Now that you have read all of the above: <a href="{{ URL::to('dashboard/index') }}" class="navbar-link">Click here to proceed.</a></p>
                    @endif
			    </div>
			</div>
		</div>
	</div>
@stop
@section('javascript')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}
,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-59609039-1', 'auto');
ga('send', 'pageview');
</script>
@stop
