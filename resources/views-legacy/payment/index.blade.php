@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<div class="spacewrapper">
		<div class="read-only">
		<h4>Read the Mandated Agreement</h4>
			<div class="read-only-text">
				<div class="form-group">
				<h3>DEAR SUBSCRIBER:</h3>
				<div style="text-align: justify;">
				<p>Your subscription to CreditBPO’s rating  service grants CreditBPO the express authority to conduct a confirmation and validation of the financial, management, and business  information supplied to it by your company  through our online data collection platform.</p>
				<p>Further, all the information collected through our website and related analyses and reports generated thereof in conformity with the aforementioned subscription will be treated as strictly CONFIDENTIAL and subject to a full NON-DISCLOSURE restriction to third parties.</p>
				<p>We trust that you have a full understanding of the intent of our company to fulfill our commitment to undertake the services as agreed upon using reliable and accurate data. The data you provide will be the basis for generating your CreditBPO Rating Report, a self risk assessment tool that meets best industry credit practices and regulatory standards.</p>
				<p>We're on your side,</p>
				<p>The CreditBPO Team</p>
				<p><img src="{{ URL::to('/') }}/images/logo_0.png" /></p>
				@if (1 == Auth::user()->role && 2 == Auth::user()->status)
                <a href="{{ URL::to('/') }}/payment/show_requirments?id={{$entity_id}}" class="btn btn-primary">I AGREE!</a>
                @elseif ($entity_id)
				<a href="{{ URL::to('/') }}/payment/selection?id={{$entity_id}}" class="btn btn-primary">I AGREE!</a>
				@else
				<a href="{{ URL::to('/') }}/payment/selection" class="btn btn-primary">I AGREE!</a>
				@endif
				<span style="font-size: 80%; color: #888;">Note: Do not proceed with this page if you have PENDING TRANSACTION (over-the-counter), unless you want to go through with a different payment option.</span>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
