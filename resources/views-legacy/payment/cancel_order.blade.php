@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
			        <h3>You have cancelled your payment.</h3>
					<p><a href="{{ URL::to('/') }}/logout" class="navbar-link">Click here to logout</a></p>
			    </div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
