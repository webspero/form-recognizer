@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
			        <h3>There was an error with the payment gateway.</h3>
					<p>Click <a href="{{ URL::to('/') }}" class="navbar-link">here</a> to try again.</p>
			    </div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
