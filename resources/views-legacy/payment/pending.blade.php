@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Pending Transaction</h1>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
			        <h1>Your transaction is awaiting your payment!</h1>

					<h1>Reference number: <span style="color: RED;">{{ $refno }}</span></h1>

					<h3>What you should know:</h3>
					<p>a. Please check your email for payment instructions.</p>
					<p>b. Pay the exact amount indicated. Excess portion of your payment is forfeited. Payments less than the amount due will not be processed.</p>
					<p>c. If you are paying for multiple Dragonpay reference numbers, pay separately for each reference number. Do not lump them into a single transaction.</p>
					<p>d. Make sure to get a reference number first before paying. A Dragonpay reference number can only be used once.</p>
					<p>e. If you made a short payment by mistake, do not try to correct it by making another bills payment with the same reference no.</p>
				</div>
			</div>
		</div>
	</div>
@stop

@section('javascript')
    <script src="{{ URL::asset('js/googletrack-login-payment.js') }}"></script>
@stop
