<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">CreditBPO Business Site Error Notification</h2>
				<div>
					<p>An error has occurred.</p>
					<br>
					<table cellspacing="0" cellpadding="5px" border="1px" width="100%">
						<tr>
							<td>Server</td>
							<td>{{$server}}</td>
						</tr>
						<tr>
							<td>DateTime</td>
							<td>{{$date}}</td>
						</tr>
						<tr>
							<td>User</td>
							<td>{{$user}}</td>
						</tr>
						<tr>
							<td>URL</td>
							<td>{{URL::previous()}}</td>
						</tr>
						<tr>
							<td>Error Code</td>
							<td>{{$code}}</td>
						</tr>
						<tr>
							<td>Exception</td>
							<td>{{$exception}}</td>
						</tr>
					</table>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>