<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">CreditBPO Analysis: Additional Documents Needed</h2>
				<div>
					<p>Hello  {{ $username }},</p><br/><br/>
					<p>{!! $message_body !!}</p>
					<br/>
					<p>Sincerely,</p>
					<p><img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150"></p>
					<p>Your Rating Solution.</p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
