<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{{ $subject }}</title>
        <style>
            .ExternalClass{
                width:100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div{
                line-height: 100%;
            }

            table {
                mso-table-lspace:0pt;
                mso-table-rspace:0pt;
            }

            img {
                -ms-interpolation-mode:bicubic;
            }

        </style>
    </head>
    <body>
        <div class="body-container"
            style="
                font-family: arial,
                sans-serif;
                background-color: #fafafa;
                padding: 20px;">
            <table cellpadding="0" cellspacing="0" bgcolor="white"
                style="
                    width: 100%;
                    height: 100%;
                    border: 1px solid rgba(0,0,0,0.1);">
                <tr>
                    <td style="
                            text-align: left;
                            padding: 20px 30px;
                            width: 50%;
                            vertical-align: center;">
                        <img src="https://business.creditbpo.com/images/creditbpo-logo.jpg"
                            alt="CreditBPO" width="200" />
                    </td>
                    <td
                        style="
                            font-size: 30px;
                            letter-spacing: 5px;
                            font-weight: 700;
                            color: #ada0a0;
                            vertical-align: center;
                            width: 50%;
                            text-align: right;
                            padding: 20px 30px;">
                        INVOICE
                    </td>
                </tr>
                <tr>
                    <td class="row">
                        <div class="col-md-4 col-sm-12" style="padding-left: 50px; width: 40%; float: left;">
                            <div class="row">
                                <h3 style="margin: 0; font-weight: bold;">CreditBPO Tech, Inc </h3>
                            </div>
                            <div class="row">
                                <p style="margin: 1em 0; padding: 0;">
                                    One Global Place<br/>
                                    25th Street and 5th Avenue<br/>
                                    Bonifacio Global City, Taguig<br/>
                                    1634 MM<br/>
                                    Philippines<br/>
                                    <br/> www.creditbpo.com
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12" style="margin-left: 50px;">
                            <div class="row">
                                @if(isset($user->name) || $user->name != null)
                                    <h3 style="margin: 0; font-weight: bold;">Bill to: {{ $user->name }}</h3>
                                @else
                                <h3 style="margin: 0; font-weight: bold;">Bill to: {{ $user->firstname }} {{ $user->lastname }}</h3>
                                @endif
                            </div>
                            <div class="row">
                                @if(isset($user->street_address) && isset($user->city) && isset($user->province) && isset($user->zipcode) && isset($user->phone))
                                <p style="margin: 1em 0; padding: 0;">
                                    {{ $user->street_address }}<br>
                                    {{ $user->city }}<br>
                                    {{ $user->province }}<br>
                                    {{ $user->zipcode }}<br>
                                    {{ $user->phone }}<br>
                                </p>
                                @endif
                            </div>
                        </div>
                    </td>
                    <td style="text-align: right; padding: 20px 30px; vertical-align: top;">
                        <h3 style="margin: 0;">&nbsp;</h3>
                        <p style="margin: 1em 0; padding: 0;">
                            Invoice #: {{ $receipt->reference_id }} <br/>
                            Payee:
                            Invoice Date: {{ date('d M Y') }} <br/>
                            Due Date: {{ date('d M Y') }} <br/>
                        </p>
                        <br/>
                        <table cellspacing="0" cellpadding="0" align="right">
                            <tr>
                                <td style="
                                        border: solid 2px #c1c1c1;
                                        padding: 10px 20px;
                                        border-collapse: collapse;
                                        text-align: center;">
                                    <span>Amount Due:</span><br/>
                                    <span style="font-weight: bold; font-size: 18px;">
                                        PHP {{ number_format($receipt->total_amount, 2) }}
                                    <span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 20px 30px;">
                        <table width="100%" cellspacing="0" cellpadding="0"
                            style="width: 100%; border-collapse: collapse;">
                            <thead>
                                <th style="
                                        border: 1px solid #c1c1c1;
                                        border-collapse: collapse;
                                        padding: 7px;
                                        width: 50%;">
                                    Description
                                </th>
                                <th style="
                                        border: 1px solid #c1c1c1;
                                        border-collapse: collapse;
                                        padding: 7px;">
                                    Quantity
                                </th>
                                <th style="
                                        border: 1px solid #c1c1c1;
                                        border-collapse: collapse;
                                        padding: 7px;">
                                    Price
                                </th>
                                <th style="
                                        border: 1px solid #c1c1c1;
                                        border-collapse: collapse;
                                        padding: 7px;">
                                    Amount
                                </th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;">
                                        @if (1 == $receipt->item_type)
                                            Filing Fee
                                        @elseif (2 == $receipt->item_type)
                                            Monthly Subsrcription to CreditBPO
                                        @else
                                            CreditBPO Keys
                                        @endif
                                    </td>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;">
                                        {{ $receipt->item_quantity }}
                                    </td>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;">
                                        PHP {{ number_format($receipt->price, 2) }}
                                    </td>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;">
                                        PHP {{ number_format($receipt->amount, 2) }}
                                    </td>
                                </tr>
                                
                                    <tr>
                                        <td colspan="3"
                                            style="
                                                border: 1px solid #c1c1c1;
                                                border-collapse: collapse;
                                                padding: 7px;
                                                text-align: right;
                                                border-bottom: 0;
                                                border-top: 0;">
                                            Discount
                                        </td>
                                        <td style="
                                                border: 1px solid #c1c1c1;
                                                border-collapse: collapse;
                                                padding: 7px;
                                                text-align: right;
                                                border-bottom: 0;
                                                border-top: 0;">
                                            @if (isset($receipt->discount) && $receipt->discount != 0)
                                                - ( PHP {{ number_format($receipt->discount, 2) }} )
                                            @else
                                                PHP 0.00
                                            @endif
                                        </td>
                                    <tr>
                                    
                                <!-- @if($receipt['item_type'] == 3)
                                <tr>
                                    <td colspan="3"
                                        style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;
                                            border-bottom: 0;">
                                        Subtotal
                                    </td>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;
                                            border-bottom: 0;">
                                        @if (isset($subtotal))
                                            PHP {{ number_format($subtotal, 2) }}
                                        @else
                                            PHP {{ number_format($receipt->subtotal, 2) }}
                                        @endif
                                    </td>
                                <tr>

                                <tr>
                                    <td colspan="3"
                                        style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;
                                            border-bottom: 0;">
                                        Tax Amount (1%)
                                    </td>
                                    <td style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;
                                            border-bottom: 0;">
                                        PHP {{ number_format($receipt->vat_amount, 2) }}
                                    </td>
                                <tr>
                                @endif -->

                                <tr>
                                    <td colspan="3"
                                        style="
                                            border: 1px solid #c1c1c1;
                                            border-collapse: collapse;
                                            padding: 7px;
                                            text-align: right;">
                                        <b>Total</b>
                                    </td>
                                    <td style="
                                        border: 1px solid #c1c1c1;
                                        border-collapse: collapse;
                                        padding: 7px;
                                        text-align: right;
                                        font-weight: bold;">
                                        PHP {{ number_format($receipt->total_amount, 2) }}
                                    </td>
                                <tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 26px 30px 10px;" colspan="2">
                        <hr style="margin: 0; padding: 0; border: 1px solid #c1c1c1;" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding: 20px 30px; vertical-align: top;">
                        <b>Notes</b>
                        <p>
                            Thank you for your business. You will receive a message in the email you provided on important next steps and FAQ primer. We look forward to being your preferred rating solution partner.
                        </p>
                    </td>
                    <td style="text-align: left; padding: 20px 30px; vertical-align: top;">
                        <b>Terms and Condition</b>
                        <p>
                            Except in conjunction with this purchase, the purchaser and the corporation he represents agree not to use CreditBPO's Rating Report® and online platform, or modified versions of said platform or database setup unless expressly approved in writing by CreditBPO. The trademark-registered CreditBPO Rating Report® is copyright-registered with the U.S. Copyright Office.
                        </p>
                    </td>
                </tr>
            </table>
            <br/>
        </div>
    
    </body>
</html>
