<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">To better serve you, we would like to ask for a short feedback, please click the link below</h2>
				<div>
                    <p> Please log in to <a href = "{{ URL::to('/login') }}"> Credit BPO </a> </p>
					<p><a href = "{{ URL::to('/feedback/final_rating/').'/'.$trans_id }}" > Click here after logging in to give your feedback </a></p>
					<br>
					<p>The CreditBPO Team<br>
						CreditBPO
					</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>