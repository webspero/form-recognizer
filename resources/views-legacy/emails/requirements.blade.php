<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Document Request from CreditBPO</h2>
				<div>
					<p>A validation process is currently being run on {{$company_name}}. Please reply to this email and attach the following documents in order to proceed:</p>
					@if(@count($docs)>0)
						<p>&nbsp;</p>
						<ul>
						@foreach($docs as $d)
							<li>{{ $d }}</li>
						@endforeach
						</ul>
					@endif
					<p>&nbsp;</p>
					<p><a href="{{ URL::to('/') }}" style="background-color: #00697A; padding:10px 30px; color:white; text-decoration:none;">Login screen is available here if desired.</a></p>
					<br>
					<p>The CreditBPO Team<br>
						CreditBPO
					</p>
					<p><i>Paving the way</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>