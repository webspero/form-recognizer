<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">CreditBPO Risk Rating Completed</h2>
				<div>
					<p>A CreditBPO Rating Report has been made accessible. Click below to log in and view results.</p>
                </div>
                
                <div style = 'padding-top: 15px; padding-bottom: 15px; line-height: 150%; mso-line-height-rule: exactly; mso-line-height: 150%;'>
					<p><a href="{{ URL::to('/') }}" style="background-color: #00697A; padding:10px 30px; color:white; text-decoration:none;">CreditBPO</a></p>
                </div>
                
                <div>
					<p>
                        We recommend that the CreditBPO Rating Report®, your financial technology tool,  be included as an integral part of your Risk Management processes.
                        <br/>
                        It will capacitate the financial condition analytical capability of your organization.<br/>
                        <br/>
						The CreditBPO Rating Report® is not to be used as the single basis for making final credit decision but merely as a pre-clearance tool.
						<br/>
						Work Smarter!
                    </p>
                </div>
                
                <div>
                    <br/>
					<p>The CreditBPO Team</p>
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
