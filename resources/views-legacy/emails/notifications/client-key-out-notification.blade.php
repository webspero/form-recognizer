<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Client Keys Used </h2>
				<div>
					<p>Hello, {{ $user->firstname }} {{ $user->lastname }}</p>
                    <p>
                        
                        Thank you very much for choosing our product! <br/>
                        This is to notify you that all of your Client Keys have been used. <br/>
						To avail more, please log in to our system and purchase additional keys.
                        <br/><br/>
                        We’re on your side.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
