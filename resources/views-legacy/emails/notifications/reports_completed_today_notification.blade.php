<!DOCTYPE html>
<html lang="en-US">
	<head>
        <meta charset="utf-8">
        <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 70%;
            }
            
            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
        </style>
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Reports Completed Today </h2>
				<div>
					<p>Hello, {{ $user->firstname }} {{ $user->lastname }}</p>
                    <p>
                        
                        Thank you very much for choosing our product! <br/>
                        This is to notify you of the reports completed this day.<br/><br/>
                        Here are the reports:<br/>
                        <table>
                            <thead>
                                <tr>
                                    <th class="text-danger">Company ID</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reportsCompletedToday as $report)
                                <tr>
                                    <td width="15%">{{ $report->entityid }}</td>
                                    <td width="50%">{{ $report->companyname }}</td>
                                    <td>{{ $report->email }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br/><br/>
                        We’re on your side.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
                    <img src="https://business.creditbpo.com/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
