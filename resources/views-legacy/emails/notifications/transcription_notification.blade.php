
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>

    <body>
        <div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
            <div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
                <h2 style="color:#00697A;">Reminder</h2>

                <p><strong><i>Hello,</i></strong> </p>
                <div class="container">
                        </p>
                        <table id="entitydata" class="table table-bordered" cellspacing="20">
                            <thead>
                                <tr>
                                    <th style="font-weight: bold;">Company ID</th>
                                    <th style="font-weight: bold;">Company Name</th>
                                    <th style="font-weight: bold;">Report Type</th>
                                    <th style="font-weight: bold;">Time and Date</th>
                                </tr>
                            </thead>
                            <tbody>
                
                                    <tr role="row">
                                        <td>{{$report_number}}</td>
                                        <td>{{$company_name}}</td>
                                        <td>
                                          <?php
                                          if($report_type == 0){
                                             echo 'Standalone';
                                          }else if($report_type == 1){
                                            echo 'Premium';
                                          }else{
                                            echo 'Simplified';
                                          }
                                          
                                          ?>
                                        </td>
                                        <td>{{date('M d, Y H:i:A', strtotime($time))}}</td>
                                    </tr>
                            </tbody>
                        </table>
           
                </div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
            </div>
        </div>
    </body>
</html>
