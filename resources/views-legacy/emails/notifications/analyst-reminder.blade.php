<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
            <div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
                <h2 style="color:#00697A;">Reminder</h2>

                <p><strong><i>Hello,</i></strong> </p>
            <p>
                Below is a list of submitted questionnaires for Risk Rating as of {{$currentDate}}
            </p>
                <div class="container">
                    <table id="entitydata" class="table table-bordered" cellspacing="20" >
                        <thead>
                            <tr>
                                <th style="font-weight: bold;">Company ID</th>
                                <th style="font-weight: bold;">Company Name</th>
                                <th style="font-weight: bold;">Report Type</th>
                                <th style="font-weight: bold;">Email</th>
                                <th style="font-weight: bold;">Queue Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($entity)

                            	@php
                            		$reportTypes = [
                            			0 => 'Simplified',
                            			1 => 'Standalone',
                            			2 => 'Premium'
                            		];
                            	@endphp
                            	@foreach ($entity as $info)
                                    <tr role="row">
                                        <td>{{$info->entityid}}</td>
                                        <td>{{$info->companyname}}</td>
                                        <td>{{$reportTypes[$info->is_premium]}}</td>
                                        <td>{{$info->email}}</td>
                                        <td>{{$info->date_diff}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <p>No submitted reports!</p>
                            @endif
                        </tbody>
                    </table>
                </div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
            </div>
        </div>
    </body>
</html>
