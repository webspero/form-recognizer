<!DOCTYPE html>
<html lang="en-US">
	<head>
        <meta charset="utf-8">
        <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 70%;
            }
            
            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
        </style>
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Document Denied - {{ $entity->companyname }} </h2>
				<div>
					<p>Hello, {{ $entity->name }}</p>
                    <p>
                        This is to notify you that some documents were denied for company, {{ $entity->companyname }}.<br/><br/>
                        Here are the details of the report:<br/>
                        <div>
                            <p><b>Report number:</b> {{ $entity->entityid }}</p>
                            <p><b>Company name:</b> {{ $entity->companyname }}</p>
                        </div>
                        <b>Denied Documents:</b> <br>
                        @foreach($denied_documents as $denied_document)
                            <p>{{ $denied_document->file_name }}</p>
                        @endforeach
                        Click <a href="{{ URL::to('summary/smesummary/'.$entity->entityid) }}">here</a> to see the report. <br/>
                        <br/><br/>
                        Thank you.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
                    <img src="https://business.creditbpo.com/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
