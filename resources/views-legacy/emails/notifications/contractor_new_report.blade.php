<!DOCTYPE html>
<html lang="en-US">
	<head>
        <meta charset="utf-8">
        <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 70%;
            }
            
            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
        </style>
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Renewal Request - {{ $company_name }} </h2>
				<div>
					<p>Hello, {{ $reviewer->name }}</p>
                    <p>
                        This is to notify you that a renewal request for {{ $company_name }} has been submitted.<br/><br/>
                        Here are the details of the report:<br/>
                        <div>
                            <p><b>Report number:</b> {{ $report_number }}</p>
                            <p><b>Company name:</b> {{ $company_name }}</p>
                            <p><b>Email:</b> {{ $basic_user_email }}</p>
                        </div>
                        Click <a href="{{ URL::to('summary/smesummary/'.$report_number) }}">here</a> to see the report. <br/>
                        <br/><br/>
                        Thank you.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
                    <img src="https://business.creditbpo.com/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
