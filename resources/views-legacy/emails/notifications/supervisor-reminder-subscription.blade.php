<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Reminder</h2>
				<div>
					<p>Hi {{$supervisor->name}}, </p>
                    <p>
						We would like to inform you that your supervisor subscription is due this coming {{$supervisor->expiry_date}}. You still have 10 days to process your bills. <br>
						Click the button below to pay your subscription. <br>
	
						<div style = 'padding-top: 10px; padding-bottom: 10px; line-height: 150%; mso-line-height-rule: exactly; mso-line-height: 150%;'>
							<p><a href="https://business.creditbpo.com" style="background-color: #00697A; padding:10px 30px; color:white; text-decoration:none;">Pay Subscripton</a></p>
						</div>
					
                    </p>
                        
					<br/>
					<p>Sincerely,</p>
						<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">	
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>