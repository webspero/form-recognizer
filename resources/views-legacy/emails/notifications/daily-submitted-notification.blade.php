<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>

    <body>
        <div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
            <div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
                <h2 style="color:#00697A;">Reminder</h2>

                <p><strong><i>Hello,</i></strong> </p>
                <div class="container">

                    @if(count($report) > 0)
                        <!-- LIST OF REPORTS -->
                        <p>
                            List of  reports as of {{$currentDate}}
                        </p>
                        <table id="entitydata" class="table table-bordered" cellspacing="20">
                            <thead>
                                <tr>
                                    <th style="font-weight: bold;">Company ID</th>
                                    <th style="font-weight: bold;">Company Name</th>
                                    <th style="font-weight: bold;">Report Type</th>
                                    <th style="font-weight: bold;">Notification</th>
                                    <th style="font-weight: bold;">Time and Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $reportTypes = [
                                        0 => 'Standalone',
                                        1 => 'Premium',
                                        2 => 'Simplified'
                                    ];

                                    $reportStatus = [
                                        '0' => 'Started',
                                        '5' => 'Awaiting Transcription',
                                        '7' => 'Generated'
                                    ];

                                @endphp
                                @foreach ($report as $info)
                                    <tr role="row">
                                        <td>{{$info->entityid}}</td>
                                        <td>{{$info->companyname}}</td>
                                        <td>{{$reportTypes[$info->is_premium]}}</td>
                                        <td>
                                            {{$reportStatus[$info->status]}}
                                        </td>
                                        <td>{{date('M d, Y H:i:A', strtotime($info->created_at))}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        There are no reports generated as of {{$currentDate}}
                    @endif
                </div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
            </div>
        </div>
    </body>
</html>
