<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">File Upload Notification</h2>

				<p><strong>Hello Innodata Recipient,</strong> </p>
				<p>File listed below has been uploaded to the <strong>{{ $to_folder }}</strong> folder.</p>
				@if(!empty($files))
					<ul>
					@foreach ($files as $file)
                        <li>{{ $file }}</li>
					@endforeach
					</ul>
				@endif
                @if(!empty($creditbpo))
                    <p>Kindly notify the email/s listed below once the transcribed Financial Statement Input Template file has been uploaded into the <strong>{{ $from_folder}}</strong> folder following the agreed upon format : <strong>{{ $file_name }}</strong></p>
                    @foreach ($creditbpo as $recipient)
                        <p>{{ $recipient->email }}</p>
                    @endforeach
                @endif
                <br />
                <p>Best Regards,</p>
                <p>The CreditBPO Team</p>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>