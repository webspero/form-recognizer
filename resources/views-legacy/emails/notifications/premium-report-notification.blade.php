<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> New Premium Report </h2>
				<div>
					<p>Hello, </p>
                    <p>
						Please start the Business Profiling for {{ $entity->companyname }}.
						@if(isset($loa))
							You can download letter of authorization on this <a href="{{ URL::to('/') }}/letterofauthorization/{{$entity->entityid}}" target="_blank">link</a>.
						@endif
                    </p>
					<p>
						Kindly fill up the required fields here: {{ $url }}
					</p>
                    
					<br/>
					<p>Sincerely,</p>
					<p>The CreditBPO Team</p>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
				</div>

				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
