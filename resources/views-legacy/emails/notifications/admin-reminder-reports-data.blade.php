<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Weekly Reports </h2>
				<div>
					<p>Hello, </p>
                    <p>
						File attached below are the reports generated last week from {{$from}} to {{$to}}.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
					<p>The CreditBPO Team</p>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
				</div>

				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
