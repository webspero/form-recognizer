<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif; width: 80%; margin: 0 auto">
			<div style="background-color: #fff; padding: 30px; border: 1px solid rgba(0,0,0,0.1); color: black;">
                <div>
                    <img src = '{{ URL::to("images/creditbpo-logo-3.png") }}' alt="CreditBPO" />
                    <a href="https://www.facebook.com/CreditBPO"><img src='{{ URL::to("images/facebook_icon.png") }}' alt="CreditBPO-Facebook" width=50px style="float:right;"></a>
                </div>
                <br/>
                <h1 style="color:#00697A;">Free trial access details</h1>
				<div>
               <p>Hello, <br/>
                  As part of the free trial accorded to {{ $org_name }}, the following basic user account access details are available to you:
               </p>
               <p>
                  Login: {{ $basic_email }} <br/>
                  Password: {{ $basic_password }}
               </p>
               <p>
                  To get started, go to the login page <a href="business.creditbpo.com">HERE</a> and use the access details above. <br/>
               </p>
               <p>
                  For any additional questions, please reach out to us at info@creditbpo.com.
               </p>
               <p>
                  Best Regards, <br/>
                  The CreditBPO Team
               </p>
               <p style="font-size: 13px;"><i>This email was sent to {{$basic_email}} by CreditBPO in accordance with our <a href="{{ URL::route('getPrivacyPolicy') }}">Privacy Policy</a>.</i></p>
				</div>
            <br/>
			   <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
