<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Attached list of email leads.</h2>
				<div>
					@if (0 >= $ctr)
						<p>No new leads today...</p>
						<br>
						@else
						<p>There are {{ $ctr }} new leads today...</p>
						<br><br/>
						
						@if ($drupal_leads)
						<p>
							Website Feedbacks
							<br/>
							@foreach ($drupal_leads as $druper)
							{{ $druper }}
							@endforeach
						</p>
						@endif
						
						@if ($mchimp_leads)
						<p>
							Request Sample Report
							<br/>
							@foreach ($mchimp_leads as $chimp)
							{{ $chimp }}
							@endforeach
						</p>
						@endif
					@endif
					
					<p>The CreditBPO Team<br>
						CreditBPO
					</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>