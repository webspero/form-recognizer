<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif; width: 80%; margin: 0 auto">
			<div style="background-color: #fff; padding: 30px; border: 1px solid rgba(0,0,0,0.1); color: black;">
                <div>
                    <img src = '{{ URL::to("images/creditbpo-logo-3.png") }}' alt="CreditBPO" />
                    <a href="https://www.facebook.com/CreditBPO"><img src='{{ URL::to("images/facebook_icon.png") }}' alt="CreditBPO-Facebook" width=50px style="float:right;"></a>
                </div>
				<div>
                    <p>Hi {{$user}} </p>
                    <p>Thank you for working with us! Your access details are as follows:</p>
                    <p>Email Address: {{$email}}</p>
                    <p>Password: {{$password}}</p>
                    To get started, please go to this webpage to log in using the access details above: <a href="https://business.creditbpo.com" >https://business.creditbpo.com</a>

                    <br/>
					<p>Sincerely,</p>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
