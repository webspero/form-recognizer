<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa;
					font-family: arial,sans-serif;
					text-align: center;
					padding: 50px 0px">
			<div style="width: 50%; 
						margin: 0 auto; 
						background: white;
						border-radius: 10px;
						padding: 20px;
						border: 1px solid rgba(0,0,0,0.1)">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="200" style="margin-bottom: 15px">
				<div style="background-color: #ff3c3c;
							padding: 10px 0px;
							color: white;
							margin-bottom: 25px;
							letter-spacing: 1px;
							font-size: 15px;
							font-weight: 700;">
				<h2>Security Alert</h2>
				</div>
				<div style="text-align: left;color: black;">
					<p>Hi {{$firstname}} {{$lastname}},</p>
					<br/>
					<p>Please note that the account registered with an email {{$email}} has been locked due to multiple incorrect access attempts.</p>
					<p>Please contact info@creditbpo.com for support.</p>
					<br/>
					<p>The CreditBPO Team</p>
					<p>CreditBPO</p>
					<p><i>Paving the way</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
