<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-size: 12px; font-family: arial, sans-serif;">
			
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<p>
                    Hello,
                    <br/>
                    
                    <br/> This is a follow up from the free trial scheduling for {{ $company_name }}
                    
                    <br/>
                    <br/> Username: {{ $username }}
                    <br/> Password: {{ $password }}
                    <br/>
                    
                    <br/> Account was tested 5 minutes before sending this email and verified functional.
                    <br/>
                    
                    <br/> Login page is located at <a href = '{{ URL::To("/") }}'> {{ URL::To("/") }} </a>
                    <br/>
                    
                    <br/> Let us know the date and time you intend to commence the trial. Please read the attached Trial FAQs for your information and preparation.
                    <br/>
                    
                    <br/> Live chat is available on the website for assistance.
                    <br/>
                    
                    <br/> For sake of clarification, there is no cost associated with this trial and all Questionnaire and Report features are fully available.
                    <br/>
                    
                    <br/> Let us know if you have additional questions or comments.
                    
                    <br/> Regards,
                </p>
				<div>
					<p>This email is auto-generated.  Download the attachment to view the list.</p>
					<br>
					
				</div>
			</div>
            
            <div style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
                <p>
                    The CreditBPO Team<br>
					CreditBPO
                </p>
                <p><i>Your Rating Solution</i></p>
                
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>