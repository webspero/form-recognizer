<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif; width: 80%; margin: 0 auto">
			<div style="background-color: #fff; padding: 30px; border: 1px solid rgba(0,0,0,0.1); color: black;">
                <div>
                    <img src = '{{ URL::to("images/creditbpo-logo-3.png") }}' alt="CreditBPO" />
                    <a href="https://www.facebook.com/CreditBPO"><img src='{{ URL::to("images/facebook_icon.png") }}' alt="CreditBPO-Facebook" width=50px style="float:right;"></a>
                </div>
                <br/>
                <h1 style="color:#00697A;">Activate your CreditBPO account</h1>
				<div>
					<p>To confirm your email address, just click the button below.</p>
                    @if (!Request::wantsJson())
                    <div style = 'padding-top: 10px; padding-bottom: 10px; line-height: 150%; mso-line-height-rule: exactly; mso-line-height: 150%;'>
                        <p><a href="{{ URL::to('/') }}/activation/{{ $activationcode }}" style="background-color: #00697A; padding:10px 30px; color:white; text-decoration:none;">Account Activation</a></p>
                    </div>
                    @endif
                    <p style="font-size: 13px;">If you can't click on the link, just copy and paste this URL into a web browser.<br/>
                    <span style="color:#00697A">{{ URL::to('/') }}/activation/{{ $activationcode }}</span></p>
                    <p style="font-size: 13px;"><i>This email was sent to {{$email}} by CreditBPO in accordance with our <a href="{{ URL::route('getPrivacyPolicy') }}">Privacy Policy</a>.</i></p>
				</div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
