<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif; width: 80%; margin: 0 auto">
			<div style="background-color: #fff; padding: 30px; border: 1px solid rgba(0,0,0,0.1); color: black;">
                <div>
                    <img src = '{{ URL::to("images/creditbpo-logo-3.png") }}' alt="CreditBPO" />
                    <a href="https://www.facebook.com/CreditBPO"><img src='{{ URL::to("images/facebook_icon.png") }}' alt="CreditBPO-Facebook" width=50px style="float:right;"></a>
                </div>
                <br/>
                <h1 style="color:#00697A;">Free trial accounts details</h1>
				<div>
               <p>Hello, <br/>
                  As part of the free trial accorded to {{ $org_name }}, the following free trial accounts are available to you:
               </p>
               <p>
                  <strong>Basic User: </strong>{{ $basic_email }} <br/>
                  <strong>Supervisor: </strong>{{ $sup_email }} <br/>
               </p>
               <p>
                  Login details and instructions were sent to each user's respective email. <br/>
               </p>
               <p>
                  For any additional questions, please reach out to us at info@creditbpo.com.
               </p>
               <p>
                  Best Regards, <br/>
                  The CreditBPO Team
               </p>
               <p style="font-size: 13px;"><i>This email was sent to {{$email}} by CreditBPO in accordance with our <a href="{{ URL::route('getPrivacyPolicy') }}">Privacy Policy</a>.</i></p>
				</div>
            <br/>
			   <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
