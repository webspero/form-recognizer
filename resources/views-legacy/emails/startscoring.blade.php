<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>New Basic User Report has been submitted!</h2>
		<div>
			<p>Company Name: {{$company_name}}</p>
			<p>Company Email: {{$company_email}}</p>
			<p>Company Phone: {{$phone}}</p>
			<p>Company Address: {{$address1}} {{$address2}}, {{$city}}, {{$province}} {{$zipcode}}</p>
			<br>
			<p>The CreditBPO Team</p>
			<p><a href="{{ URL::to('/') }}">CreditBPO</a></p>
			<p><i>Paving the way</i></p>
		</div>
		<br/>
		<p><small>*** This is a system generated email. Do not reply. ***<small></p>
	</body>
</html>
