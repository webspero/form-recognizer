<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
        <style>
            .right {
                position: absolute;
                padding: 10px;
                right: 30px;
            }

            .invoice-hdr {
                font-size: 30px;
                letter-spacing: 5px;
                font-weight: 700;
                color: #ada0a0;
            }

            .amount-due {
                width: 130px;
                line-height: 25px;
                border: solid 2px #c1c1c1;
                padding: 10px;
                text-align: center;
            }

            .amount-price {
                font-weight: bold;
                font-size: 18px;
            }

            .summary-tb {
                width: 100%;
                border: 1px solid #c1c1c1;
            }

            .summary-tb td,
            .summary-tb th {
                border: 1px solid #c1c1c1;
                padding: 7px;
            }
        </style>
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<div style = 'width: 75%; display: inline-block;'>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="200">
                </div>
                <div style = 'width: 15%; display: inline-block;'>
                    <span class = 'invoice-hdr'> INVOICE </span>
                </div>
			</div>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1); border-top: 0;">
				<div style = 'width: 45%; display: inline-block;'>
                    <h3>CreditBPO Tech, Inc </h3>
                    <div>
                        <p>
                            One Global Place<br/>
                            25th Street and 5th Avenue<br/>
                            Bonifacio Global City, Taguig<br/>
                            1634 MM<br/>
                            Philippines
                        </p>
                        <p>
                            <br/> www.creditbpo.com
                        </p>
                    </div>
                </div>

                <div style = 'width: 45%; display: inline-block;'>
                    <div style = 'min-height: 120px;'>
                        <p style = 'text-align: right; line-height: 30px;'>
                            Invoice #: {{ $receipt->reference_id }}<br/>
                            Invoice Date: {{ date('d M Y') }}<br/>
                            Due Date: {{ date('d M Y') }}
                        </p>
                    </div>
                </div>

                <div style = 'width: 75%; display: inline-block;'>

                </div>

                <div style = 'width: 15%; display: inline-block; min-height: 100px;'>
                    <div class = 'amount-due'>
                        Amount Due:
                        <span class = 'amount-price'> PHP {{ number_format($receipt->vatable_amount, 2) }} <span>
                    </div>
                </div>

                <div>
                    <table class = 'summary-tb' cellspacing = '0'>
                        <thead>
                            <tr>
                                <th style = 'width: 50%;'> Description </th>
                                <th> Quantity </th>
                                <th> Price </th>
                                <th> Amount </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @if (1 == $receipt->item_type)
                                        CreditBPO Rating Report
                                    @elseif (2 == $receipt->item_type)
                                        Monthly Subsrcription to CreditBPO
                                    @else
                                        CreditBPO Keys
                                    @endif
                                </td>
                                <td style = 'text-align: right;'> 1 </td>
                                <td style = 'text-align: right;'> PHP {{ number_format($receipt->vatable_amount, 2) }} </td>
                                <td style = 'text-align: right;'> PHP {{ number_format($receipt->vatable_amount, 2) }} </td>
                            </tr>

                            <tr>
                                <td colspan = '3' style = 'text-align: right; border-bottom: 0px;'> Subtotal </td>
                                <td style = 'text-align: right; border-bottom: 0px;'> PHP {{ number_format($receipt->vatable_amount, 2) }} </td>
                            <tr>

                            <tr>
                                <td colspan = '3' style = 'text-align: right; border-top: 0px;'> Shipping </td>
                                <td style = 'text-align: right; border-top: 0px;'> PHP 0.00 </td>
                            <tr>

                            <tr>
                                <td colspan = '3' style = 'text-align: right;'> <b>Total</b> </td>
                                <td style = 'text-align: right;'> <b>PHP {{ number_format($receipt->vatable_amount, 2) }}</b> </td>
                            <tr>
                        </tbody>
                    </table>
                </div>

                <div style = 'margin: 30px 0px;'> <hr/> </div>
                <div style = 'width: 50%; display: inline-block; margin: 5px; vertical-align:top; line-height: 25px;'>
                    <b>Notes</b>
                    <p>
                        Thank you for your business. You will receive a message in the email you provided on important next steps and FAQ primer. We look forward to being your preferred rating solution partner.
                    </p>
                </div>

                <div style = 'width: 45%; display: inline-block; margin: 5px; vertical-align:top; line-height: 25px;'>
                    <b>Terms and Condition</b>
                    <p>
                        Except in conjunction with this purchase, the purchaser and the corporation he represents agree not to use CreditBPO's Rating Report® and online platform, or modified versions of said platform or database setup unless expressly approved in writing by CreditBPO. The trademark-registered CreditBPO Rating Report® is copyright-registered with the U.S. Copyright Office.
                    </p>
                </div>

			</div>
		</div>
	</body>
</html>
