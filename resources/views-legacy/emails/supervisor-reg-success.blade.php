<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Registration Successful </h2>
				<div>
					<p>
                        Your application will now be approved by our staff. Please send any inquiries to info@creditbpo.com or notify.user@creditbpo.com.
                    </p>
                    <p>
                        <b>Email:</b> {{ $input_data['email'] }}<br/>
                        <b>Name:</b> {{ $input_data['firstname'] }} {{ $input_data['lastname'] }}
                    </p>
					<br/>
					<p>The CreditBPO Team</p>
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
