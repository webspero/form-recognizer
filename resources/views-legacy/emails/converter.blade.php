<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Change in Converter Key</h2>
				<div>
					<p>The converter key has been changed.</p>
					<p>&nbsp;</p>
					<p>New Key: <span style="font-weight:bold;">{{$converter_key}}</span></p>
					<p>&nbsp;</p>
					<p>New link to public converter: <a href="{{ URL::to('/converter') }}/{{$converter_key}}">{{ URL::to('/converter') }}/{{$converter_key}}</a></p>
					<br>
					<p>The CreditBPO Team<br>
						CreditBPO
					</p>
					<p><i>Paving the way</i></p>
				</div>
				<br/>
				<p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>