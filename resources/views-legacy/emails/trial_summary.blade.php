<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<header style="background-color:white; border-top: 1px solid rgba(0,0,0,0.1); border-right: 1px solid rgba(0,0,0,0.1); border-left: 1px solid rgba(0,0,0,0.1); padding:20px;">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
			</header>
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;">Trial Account Activity Summary</h2>
				
                @foreach ($email as $mail)
                <div>
					<p><b>Account:</b> {{ $mail['account'] }}</p>
                    
                    @foreach ($mail['report'] as $report)
					<div>
                        <p><b>Company Name: {{ $report['company_name'] }}</b></p>
                        <p><b> Summary: </b> </p>
                        @foreach ($report['answered'] as $questionnaire)
                            @foreach ($questionnaire as $answers)
                                @if ('business_details1.fs_template' == $answers)
                                    <span> FS Template</span>
                                @else
                                    <span> {{ $answers }}</span>
                                @endif
                                <br/>
                            @endforeach
                        @endforeach
                        
                    </div>
					@endforeach
				</div>
                @endforeach
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>