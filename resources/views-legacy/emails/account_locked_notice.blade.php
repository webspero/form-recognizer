<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa;
					font-family: arial,sans-serif;
					text-align: center;
					padding: 50px 0px">
			<div style="width: 50%; 
						margin: 0 auto; 
						background: white;
						border-radius: 10px;
						padding: 20px;
						border: 1px solid rgba(0,0,0,0.1)">
				<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="200" style="margin-bottom: 15px">
				<div style="background-color: #ff3c3c;
							padding: 10px 0px;
							color: white;
							margin-bottom: 25px;
							letter-spacing: 1px;
							font-size: 15px;
							font-weight: 700;">
				<h2>Account Locked</h2>
				</div>
				<div style="text-align: left;color: black;">
					<p>This is to notify the team that {{$email}} account has been locked with the given information.</p>
					<br/>
					<p>Transaction ID: {{$attemptid}}</p>
					<p>Country Code: {{$countrycode}}</p>
					<p>Attempts: {{$attempts}}</p>
					<p>Last Attempt: {{ $lastlogin }}</p>
				</div>
			</div>
			<br/>
			<p><small>*** This is a system generated email. Do not reply. ***<small></p>
		</div>
	</body>
</html>
