<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="background-color: #fafafa; padding: 10px; font-family: arial, sans-serif;">
			<div style="background-color: #fff; padding: 20px; border: 1px solid rgba(0,0,0,0.1);">
				<h2 style="color:#00697A;"> Reminder </h2>
				<div>
					<p>Hello, {{ $user->firstname }} {{ $user->lastname }}</p>
                    <p>
                        Thanks very much for choosing our product! <br/>
                        We look forward to helping you achieve your goals for this report.<br/>
                        Unfortunately, we cannot do so until you finish filling out the questionnaire to the best of your ability.
                        It has been
                        @if (USER_LOG_3DAY == $log_type)
                        3 days
                        @elseif (USER_LOG_1WIK == $log_type)
                        a week
                        @elseif (USER_LOG_2WIK == $log_type)
                        2 weeks
                        @elseif (USER_LOG_1MON == $log_type)
                        1 month
                        @else
                        a day
                        @endif
                        since your last action and we would like to remind you to not give up and keep going!
                        <br/><br/>
                        We’re on your side.
                    </p>
                    
					<br/>
					<p>Sincerely,</p>
                    <img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" width="150">
					<p>CreditBPO</p>
					<p><i>Your Rating Solution</i></p>
				</div>
                <br/>
			    <p><small>*** This is a system generated email. Do not reply. ***<small></p>
			</div>
		</div>
	</body>
</html>
