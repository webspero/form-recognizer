    <div class="modal" id="modal-external-link">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">
                        Create External Link
                    </h4>
                </div>    
                <div class="modal-body">
                </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
					<button type="submit" class="btn btn-large btn-primary external-link-btn"> Link </button>
				</div>
            </div>
        </div>
    </div>