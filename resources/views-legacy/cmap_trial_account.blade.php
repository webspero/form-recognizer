@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('style')
 	<style>
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			font-size: 14px !important;
		}
		ul#select2-bank_id-results{
			font-size: 14px !important;
		}
		.centered-modal.in {
			display: flex !important;
		}
		.centered-modal .modal-dialog {
			margin: auto;
		}
    </style>
@stop

@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Create CMAP Free Trial Account</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postCmapCreateTrialAccounts')) }}
					<!-- <div class="form-group">
						{{-- {{ Form::label('current_bank', "Organization Name") }}
						{{ Form::text('current_bank', '', array('class'=>'form-control typeahead', 'autocomplete'=>'off', 'required' => 'required')) }} --}}
					</div> -->

					<div class="form-group">
						{{Form::label('name', 'Name')}}
						{{ Form::text('name', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{Form::label('cmap_organization', 'Organization')}}
						{{Form::select('cmap_organization', $cmapOrganizations, '', array('class'=>'form-control'))}}
					</div>

					<div class="form-group">
                        {{Form::label('bank_type', 'Organization type')}}
                        {{Form::select('bank_type', array(
                            'Bank'=>'Bank',
                            'Non-Bank Financing'=>'Non-Bank Financing',
                            'Corporate'=>'Corporate',
                            'Government'=>'Government'
                        ), 'Bank', array('class' => 'form-control'))}}
					</div>

					<div class="form-group">
						{{ Form::label('phone', "Phone No.") }}
						{{ Form::text('phone', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('email', "Email") }}
						{{ Form::email('email', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('basic_username', "Basic User Name") }}
						{{ Form::text('basic_username', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('basic_email', "Basic User Email") }}
						{{ Form::email('basic_email', '', array('class'=>'form-control', 'required' => 'required')) }}
						<p><em><small>Basic user password will be sent via email</small></em></p>
					</div>

					<div class="form-group">
						{{ Form::label('supervisor_username', "Supervisor User Name") }}
						{{ Form::text('supervisor_username', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('supervisor_email', "Supervisor User Email") }}
						{{ Form::email('supervisor_email', '', array('class'=>'form-control', 'required' => 'required')) }}
						<p><em><small>Supervisor user password will be sent via email</small></em></p>
					</div>

					<div class="form-group">
						{{ Form::submit('Create', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap3-typeahead.js') }}"></script>
@stop
