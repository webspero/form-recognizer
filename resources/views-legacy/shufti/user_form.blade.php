@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        i {
            margin-right: 10px;
        }
        input:focus, button:focus, .form-control:focus {
            outline: none;
            box-shadow: none;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #fff;
        }
        .d-flex {
            display: flex;
        }
        .justify-content-center {
            justify-content: center;
        }
        .align-items-center {
            align-items: center;
        }
        .bg-color {
            background-color: #333;
        }
        .signup-step-container {
            padding: 15px 0px;
            padding-bottom: 20px;
        }
        .wizard .nav-tabs {
            position: relative;
            margin-bottom: 0;
            border-bottom-color: transparent;
        }
        .wizard>div.wizard-inner {
            position: relative;
            margin-bottom: 100px;
            text-align: center;
        }
        .connecting-line {
            height: 2px;
            background: #e0e0e0;
            position: absolute;
            width: 65%;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: 35px;
            z-index: 1;
        }
        .wizard .nav-tabs>li.active>a, .wizard .nav-tabs>li.active>a:hover, .wizard .nav-tabs>li.active>a:focus {
            color: #555555;
            cursor: default;
            border: 0;
            border-bottom-color: transparent;
        }
        span.round-tab {
            width: 30px;
            height: 30px;
            line-height: 30px;
            display: inline-block;
            border-radius: 50%;
            background: #fff;
            z-index: 2;
            position: absolute;
            left: 0;
            text-align: center;
            font-size: 16px;
            color: #0e214b;
            font-weight: 500;
            border: 1px solid #ddd;
        }
        span.round-tab i {
            color: #555555;
        }
        .wizard li.active span.round-tab {
            background: #025283;
            color: #fff;
            border-color: #025283;
        }
        .wizard li.active span.round-tab i {
            color: #5bc0de;
        }
        .wizard .nav-tabs>li.active>a i {
            color: #025283;
        }
        .wizard .nav-tabs>li {
            width: 33.33%;
        }
        .wizard li:after {
            content: " ";
            position: absolute;
            left: 46%;
            opacity: 0;
            margin: 0 auto;
            bottom: 0px;
            border: 5px solid transparent;
            border-bottom-color: red;
            transition: 0.1s ease-in-out;
        }
        .wizard .nav-tabs>li a {
            width: 30px;
            height: 30px;
            margin: 20px auto;
            border-radius: 100%;
            padding: 0;
            background-color: transparent;
            position: relative;
            top: 0;
            opacity: 1;
        }
        .wizard .nav-tabs>li a i {
            position: absolute;
            top: 45px;
            font-style: normal;
            font-weight: 400;
            white-space: nowrap;
            left: 15px;
            transform: translate(-50%, -50%);
            font-size: 12px;
            font-weight: 700;
            color: #000;
        }
        .wizard .nav-tabs>li a:hover {
            background: transparent;
        }
        .wizard .tab-pane {
            position: relative;
            padding-top: 0px;
            float: left;
            width: 100%;
            overflow-x: hidden;
        }
        .wizard h3 {
            margin-top: 0;
        }
        .prev-step, .next-step {
            font-size: 13px;
            padding: 8px 24px;
            border: none;
            border-radius: 4px;
            margin-top: 30px;
        }
        .save {
            font-size: 13px;
            padding: 8px 24px;
            border: none;
            border-radius: 4px;
            margin-top: 30px;
            color: #ffff;
            background-color: #025283;
        }
        .next-step {
            color: #ffff;
            background-color: #025283;
        }
        .skip-btn {
            background-color: #cec12d;
        }
        .step-head {
            font-size: 20px;
            text-align: center;
            font-weight: 500;
            margin-bottom: 20px;
        }
        .term-check {
            font-size: 14px;
            font-weight: 400;
        }
        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: 40px;
            margin-bottom: 0;
        }
        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: 40px;
            margin: 0;
            opacity: 0;
        }
        .custom-file-label {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            z-index: 1;
            height: 40px;
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 2;
            color: #495057;
            background-color: #fff;
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }
        .custom-file-label::after {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            z-index: 3;
            display: block;
            height: 38px;
            padding: .375rem .75rem;
            line-height: 2;
            color: #495057;
            content: "Browse";
            background-color: #e9ecef;
            border-left: inherit;
            border-radius: 0 .25rem .25rem 0;
        }
        .footer-link {
            margin-top: 30px;
        }
        .all-info-container {}
        .list-content {
            margin-bottom: 10px;
        }
        .list-content a {
            padding: 10px 15px;
            width: 100%;
            display: inline-block;
            background-color: #f5f5f5;
            position: relative;
            color: #565656;
            font-weight: 400;
            border-radius: 4px;
        }
        .list-content a[aria-expanded="true"] i {
            transform: rotate(180deg);
        }
        .list-content a i {
            text-align: right;
            position: absolute;
            top: 15px;
            right: 10px;
            transition: 0.5s;
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fdfdfd;
        }
        .list-box {
            padding: 10px;
        }
        .signup-logo-header .logo_area {
            width: 200px;
        }
        .signup-logo-header .nav>li {
            padding: 0;
        }
        .signup-logo-header .header-flex {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .list-inline li {
            display: inline-block;
        }
        .pull-right {
            float: right;
        }
        
        @media (max-width: 767px) {
            .sign-content h3 {
                font-size: 40px;
            }
            .wizard .nav-tabs>li a i {
                display: none;
            }
            .signup-logo-header .navbar-toggle {
                margin: 0;
                margin-top: 8px;
            }
            .signup-logo-header .logo_area {
                margin-top: 0;
            }
            .signup-logo-header .header-flex {
                display: block;
            }
        }
        .login-box {
            box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.2);
        }
        .custom-container {
            display: inline-block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            margin-right: 15px;
            margin-top: 5px;
            font-size: 14px;
        }
        .custom-container:hover input~.checkmark {
            background-color: #ccc;
        }
        .list-inline {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 25px
        }
        .step-text {
            margin: 0px;
            padding: 0px;
            font-size: 16px;
            font-weight: 700;
        }
        .row.margin {
            margin: 25px 10px 10px;
        }
        .brouse-img {
            background: #e3e3e3;
            padding: 10px 20px;
            color: #000;
        }
        .row.border-img {
            border-bottom: 1px solid #e3e3e3;
            align-items: center;
            margin: 15px 0px;
            padding: 0px 0px 15px;
        }
        .row.border-img .form-group {
            margin: 0px;
        }
        .msg {
            color: #a12727;
            font-size: 12px;
        }
        .prev-step {
            margin-right: 15px;
        }
        label {
            font-size: 14px;
        }
        label>.has-error{
            color: #a94442;
        }
    </style>
@stop

@section('content')
    <div id="content">
		<section class="step-main-sec">
			<div class="container">
				<div class="col-12">
					<section class="signup-step-container">
						<div class="container">
							<div class="row d-flex justify-content-center">
								<div class="col-md-12">
									<div class="wizard">
										<div class="wizard-inner">
											<div class="connecting-line"></div>
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active">
													<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab"><i>Personal Information</i><span class="round-tab">1</span> </a>
												</li>
												<li role="presentation" class="disabled">
													<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" class="btn disabled"><i>Attachment</i><span class="round-tab">2</span> </a>
												</li>
												<li role="presentation" class="disabled">
													<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" class="btn disabled"><i>Finish</i> <span class="round-tab">3</span> </a>
												</li>
											</ul>
										</div>
						
										{{ Form::open( array('route' => 'postUserForm', 'role'=>'form', 'id' => 'formbpo', 'enctype' => 'multipart/form-data')) }}
											<div class="tab-content" id="main_form">
												<div class="tab-pane active" role="tabpanel" id="step1">
                                                    <div class="row margin border-img">
														<div class="col-lg-6 col-md-6 col-12">
                                                            <h4 class="step-text">Step 1 - Personal Information</h4>
														</div>
														<div class="col-lg-6 col-md-6 col-12">	
															<h4 class="step-text text-right">Note: <b>* Fields Are Mandatory</b></h4>
														</div>
													</div>
                                                    
                                                    <div class="form-group row">
                                                        <div class="col-md-12 {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                                            {{ Form::label('email', 'Email') }} *
                                                            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'required' => 'required')) }}
                                                            @if($errors->has('email'))
                                                                <span class="errormessage">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <div class="col-md-4 {{ ($errors->has('last_name')) ? ' has-error' : '' }}">
                                                            {{ Form::label('last_name', 'Last Name') }}
                                                            {{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control')) }}
                                                            @if($errors->has('last_name'))
                                                            <span class="errormessage">{{ $errors->first('last_name') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('first_name')) ? ' has-error' : '' }}">
                                                            {{ Form::label('first_name', 'First Name') }}
                                                            {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control')) }}
                                                            @if($errors->has('first_name'))
                                                            <span class="errormessage">{{ $errors->first('first_name') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('middle_name')) ? ' has-error' : '' }}">
                                                            {{ Form::label('middle_name', 'Middle Name') }}
                                                            {{ Form::text('middle_name', Input::old('middle_name'), array('class' => 'form-control')) }}
                                                            @if($errors->has('middle_name'))
                                                            <span class="errormessage">{{ $errors->first('middle_name') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-1 {{ ($errors->has('sufix')) ? ' has-error' : '' }}">
                                                            {{ Form::label('sufix', 'Sufix') }}
                                                            {{ Form::text('sufix', Input::old('sufix'), array('class' => 'form-control')) }}
                                                            @if($errors->has('sufix'))
                                                            <span class="errormessage">{{ $errors->first('sufix') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-3 {{ ($errors->has('birth_date')) ? ' has-error' : '' }}">
                                                            {{ Form::label('birth_date', 'Birth Date') }}
                                                            {{ Form::text('birth_date', Input::old('birth_date'), array('class' => 'form-control datepicker')) }}
                                                            @if($errors->has('birth_date'))
                                                            <span class="errormessage">{{ $errors->first('birth_date') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('gender')) ? ' has-error' : '' }}">
                                                            {{ Form::label('gender', 'Gender') }} *
                                                            <br />
                                                            {{ Form::radio('gender', 'M', Input::old('gender'), array('class' => '', 'required' => 'required')) }} Male
                                                            {{ Form::radio('gender', 'F', Input::old('gender'), array('class' => '', 'required' => 'required')) }} Female
                                                            @if($errors->has('gender'))
                                                            <span class="errormessage">{{ $errors->first('gender') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('civil_status')) ? ' has-error' : '' }}">
                                                            {{ Form::label('civil_status', 'Civil Status') }} *
                                                            {{ Form::select('civil_status', ['Single' => 'Single', 'Married' => 'Married', 'Divorced' => 'Divorced'], Input::old('civil_status'), array('class' => 'form-control', 'placeholder' => 'Select Civil Status', 'required' => 'required')) }}
                                                            @if($errors->has('civil_status'))
                                                            <span class="errormessage">{{ $errors->first('civil_status') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-4 {{ ($errors->has('birth_province')) ? ' has-error' : '' }}">
                                                            {{ Form::label('birth_province', 'Place of Birth') }} *
                                                            {{-- <i  data-toggle="modal" data-target="#modalMap"><img height="25px" src="{{ URL::asset('images/location.png') }}" /></i> --}}
                                                            {{ Form::select('birth_province', $province, '', array('id' => 'birth_province', 'class' => 'form-control', 'placeholder' => 'Select Province', 'required' => 'required')) }}
                                                            @if($errors->has('birth_province'))
                                                            <span class="errormessage">{{ $errors->first('birth_province') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('birth_city')) ? ' has-error' : '' }}" style="padding-top: 3.3rem">
                                                            {{ Form::select('birth_city', $city, '', array('id' => 'birth_city', 'class' => 'form-control', 'placeholder' => 'Select City', 'required' => 'required')) }}
                                                            @if($errors->has('birth_city'))
                                                            <span class="errormessage">{{ $errors->first('birth_city') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('citizenship')) ? ' has-error' : '' }}">
                                                            {{ Form::label('citizenship', 'Citizenship') }} *
                                                            {{ Form::text('citizenship', Input::old('citizenship'), array('class' => 'form-control', 'required' => 'required')) }}
                                                            @if($errors->has('citizenship'))
                                                            <span class="errormessage">{{ $errors->first('citizenship') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-4 {{ ($errors->has('tin')) ? ' has-error' : '' }}">
                                                            {{ Form::label('tin', 'Tax Identification Number (TIN)') }}
                                                            {{ Form::text('tin', Input::old('tin'), array('class' => 'form-control')) }}
                                                            @if($errors->has('tin'))
                                                            <span class="errormessage">{{ $errors->first('tin') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('family_members')) ? ' has-error' : '' }}">
                                                            {{ Form::label('family_members', 'No. of Family Members (including loan applicant)') }} *
                                                            {{ Form::number('family_members', Input::old('family_members'), array('class' => 'form-control', 'required' => 'required')) }}
                                                            @if($errors->has('family_members'))
                                                            <span class="errormessage">{{ $errors->first('family_members') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('monthly_income')) ? ' has-error' : '' }}">
                                                            {{ Form::label('monthly_income', 'Total Monthly Income') }} *
                                                            {{ Form::number('monthly_income', Input::old('monthly_income'), array('class' => 'form-control', 'required' => 'required')) }}
                                                            @if($errors->has('monthly_income'))
                                                            <span class="errormessage">{{ $errors->first('monthly_income') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-4 {{ ($errors->has('cards_owned')) ? ' has-error' : '' }}">
                                                            {{ Form::label('cards_owned', 'No. of Credit Cards Owned') }}
                                                            {{ Form::number('cards_owned', Input::old('cards_owned'), array('class' => 'form-control')) }}
                                                            @if($errors->has('cards_owned'))
                                                            <span class="errormessage">{{ $errors->first('cards_owned') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('cars_owned')) ? ' has-error' : '' }}">
                                                            {{ Form::label('cars_owned', 'No. of Cars Owned') }}
                                                            {{ Form::number('cars_owned', Input::old('cars_owned'), array('class' => 'form-control')) }}
                                                            @if($errors->has('cars_owned'))
                                                            <span class="errormessage">{{ $errors->first('cars_owned') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 {{ ($errors->has('cbpo_user')) ? ' has-error' : '' }}">
                                                            {{ Form::label('cbpo_user', 'Existing CreditBPO Client') }} *
                                                            <br />
                                                            {{ Form::radio('cbpo_user', 'Yes', Input::old('cbpo_user'), array('required' => 'required')) }} Yes
                                                            {{ Form::radio('cbpo_user', 'No', Input::old('cbpo_user'), array('required' => 'required')) }} No
                                                            @if($errors->has('cbpo_user'))
                                                            <span class="errormessage">{{ $errors->first('cbpo_user') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
													<ul class="list-inline pull-right">
														<li><button type="button" class="default-btn next-step text-right">Continue</button></li>
													</ul>
												</div>
												<div class="tab-pane" role="tabpanel" id="step2">
													<div class="row margin border-img">
														<div class="col-lg-6 col-md-6 col-12">	
															<h4 class="step-text">Step 2 - Attachement</h4>
														</div>
													</div>	
													
													<div class="row margin border-img">
														<div class="col-lg-6 col-md-6 col-12">	
															<h4 class="step-text">Documentery Requirements</h4>
														</div>

														<div class="col-lg-6 col-md-6 col-12">	
															<h4 class="step-text">Attachment<span class="msg"> (I.e Maximum File size 2mb Only) </span></h4>
														</div>
													</div>

													<div class="row margin border-img">
														<div class="col-lg-6 col-md-6 col-12">
															<div class="form-group">
																<label>Personal Identification Document </label> 
															</div>
														</div>

														<div class="col-lg-6 col-md-6 col-12">
															<div class="form-group brouse-img">
                                                                <input type="file" id="document" name="document" accept="image/jpeg, image/png, image/jpg">
                                                                @if($errors->has('document'))
                                                                    <span class="errormessage">{{ $errors->first('document') }}</span>
                                                                @endif
															</div>
														</div>
													</div>
													<div class="row margin border-img">
														<div class="col-lg-6 col-md-6 col-12">
															<div class="form-group">
                                                                <label>Webcam Video </label> 
															</div>
														</div>

														<div class="col-lg-6 col-md-6 col-12">
															<div class="form-group brouse-img">
                                                                <input type="file" id="video" name="video" accept="video/mp4, video/mpeg">
                                                                @if($errors->has('video'))
                                                                    <span class="errormessage">{{ $errors->first('video') }}</span>
                                                                @endif
															</div>
														</div>
                                                    </div>
                                                    
													<ul class="list-inline pull-right">
														<li><button type="button" class="default-btn prev-step">Prev Step</button></li>
														<li><button type="button" class="default-btn next-step">Next Step </button></li>
													</ul>
												</div>

												<div class="tab-pane" role="tabpanel" id="step3">

													<div class="row margin border-img">
														<div class="col-lg-12 col-md-5 col-12">
															<h4 class="step-text">Finish</h4>
														</div>	
													</div>
													
													 <div class="row margin">
												        <p style="padding-left: 20px; margin: 0;">
                                                            <label>
                                                                <input id="agree" name="agree" style="display: inline-block;" type="checkbox" value="true" required class="validate[required] checkbox" data-errormessage-value-missing="Please read the Terms of Use and Privacy Policy, and then check this box" />
                                                                {{ trans('business_details1.iHaveRead') }}
                                                            <a href="{{ URL::to('/termsofuse') }}" class="btn btn-primary" target="_blank">{{ trans('business_details1.termsOfUse') }}</a>
                                                                {{ trans('business_details1.and') }}
                                                            <a href="{{ URL::to('/privacypolicy') }}" class="btn btn-primary" target="_blank">{{ trans('business_details1.privacyPolicy') }}</a>
                                                                <img src="{{ URL::asset('images/thumb_12601.png') }}" />
                                                            </label>
                                                        </p>
													 </div>

													<ul class="list-inline pull-right">
														<li><button type="button" class="default-btn prev-step">Prev Step</button></li>
														<li><button type="submit" class="default-btn next-step">Submit</button></li>
													</ul>
												</div>
                                                <div class="clearfix"></div>
											</div>
											{{ Form::token() }}
										{{ Form::close() }}
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
@stop
@section('javascript')
<script>
    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0
    });

    $(document).ready(function() {
        $('.nav-tabs > li a[title]').tooltip();
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target);
            if (target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {
            var curStep = $(this).closest(".tab-pane");
            var curInputs = curStep.find("input, select");
            var isValid = true;
            $('div').removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest("div").addClass("has-error");
                    $(curInputs[i]).closest("label").addClass("has-error");
                }
            }
            if (isValid) {
                var active = $('.wizard .nav-tabs li.active');
                active.next().removeClass('disabled');
                nextTab(active);
            }
        });
        $(".prev-step").click(function (e) {
            var active = $('.wizard .nav-tabs li.active');
            prevTab(active);
        });

        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            var active = $('.wizard .nav-tabs li');
            active.find('a').addClass('btn disabled');
        })

        $('#birth_province').change(function() {
            var newProvince = $('#birth_province').find(":selected").val();
            populateCity2(newProvince);
        });
    });

    function populateProvince2() {
        $.ajax({
            type: 'get',
            url: BaseURL + '/getProvinceLists',
            success: function(data) {
                var provinceList = "";
                for (var i = 0; i < data.length; i++) {
                    provinceList += '<option value="' + data[i].provCode + '">' + data[i].provDesc + '</option>';
                }

                $('#birth_province').html(" ");
                $('#birth_province').append(provinceList);
                $('#birth_province').change();
            },
            error: function() {
                console.log('error!');
            }

        });
    }

    function populateCity2($provCode = 1401) {
        $.ajax({
            type: 'get',
            url: BaseURL + '/getCitiesLists/' + $provCode,
            success: function(data) {
                var cityList = "";
                for (var i = 0; i < data.length; i++) {
                    cityList += '<option value="' + data[i].citymunCode + '">' + data[i].citymunDesc + '</option>';
                }

                $('#birth_city').html(" ");
                $('#birth_city').append(cityList);
                $('#birth_city').change();
            },
            error: function() {
                console.log('error!');
            }
        });
    }

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>
@stop