@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
@stop

@section('content')
<div class="container text-center">
    <h1 class="px-4">Request Submitted</h1>
    <h4>We have received your request and it's under process. We will update you soon!</h4>
@stop