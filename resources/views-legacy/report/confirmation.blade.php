@extends('layouts.master')
@section('nav')
@show
@section('header')
	@parent
	<title>CreditBPO</title>
@stop


@section('content')
	<div class="container">
		<h1>Form Completed</h1>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					@if(Session::has('success'))
			        <p>{!! Session::get('success') !!}<p>
			    	@elseif(Session::has('fail'))
			    	<p>{!! Session::get('fail') !!}<p>
					@elseif($entity->is_premium == 1)
					<p>Transaction Completed</p><p>Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 days.</p><p><a href="../../logout">Click here to logout</a></p>
                @else
					 <p>Transaction Completed</p><p><a href="../../logout">Click here to logout</a></p>
					 @endif
			    </div>
			</div>
		</div>
	</div>
@stop
