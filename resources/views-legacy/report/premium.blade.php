@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')
@if(Auth::user()->role == 1)
    @show
@endif

@include('dashboard.nav')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="buttonwrapper">
        </div>

        {{ Form::hidden('entityids', $entity->entityid, array('id' => 'entityids')) }}
        <div class="panel-body">
            <div class="bs-tab">
                <div class="tab-content">
                @if ($entity->status == 7)
                    @if (Auth::user()->role != 3 && Auth::user()->role != 2)
                    @if ($bank_standalone==0)
                    <div id="ssc" class="tab-pane fade">
                    <div class="row col-md-12">
                        <a id="dev-ref-link" class = 'hidden-xs hidden-sm'>{{trans('messages.api_request_code')}}</a>
                        @if(!empty($fa_report))
                          <a id="print_sal" href = '{{ URL::To("financial-report/download/sal/".$fa_report->id) }}' target = '_blank'>{{trans('messages.sal_print')}}</a>
                        @endif
                        <a id="privatelink">{{trans('messages.private_link')}}</a>
                        <a id="printthis" href = '{{ URL::To("show/cbpo_pdf/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}}</a>
                    </div>
                    <div class="row col-md-4">
                    <img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
                    </div>
                    <div class="col-md-7" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
                    <div class="col-md-12"><hr/></div>

                    <div class="spacewrapper break-page">
                    @if ($entity->status == 7)
                    @if(isset($readyratiodata[0]))
                    @if (0 != $readyratiodata[0]->gross_revenue_growth1)
                    {{ Form::hidden('gross_revenue_growth2', $companyIndustry['gross_revenue_growth'], array('id' => 'gross_revenue_growth22')) }}
                    @else
                    {{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
                    @endif

                    @if (0 != $readyratiodata[0]->net_income_growth1)
                    {{ Form::hidden('net_income_growth2', $companyIndustry['net_income_growth'], array('id' => 'net_income_growth22')) }}
                    @else
                    {{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
                    @endif
                    {{ Form::hidden('gross_profit_margin2', $companyIndustry['gross_profit_margin'], array('id' => 'gross_profit_margin22')) }}
                    {{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
                    {{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
                    {{ Form::hidden('current_ratio2', $companyIndustry['current_ratio'], array('id' => 'current_ratio22')) }}
                    {{ Form::hidden('debt_equity_ratio2', $companyIndustry['debt_to_equity'], array('id' => 'debt_equity_ratio22')) }}
                    @endif
                    <div class="row chart-remove-padding">
                        <div class="col-lg-12">
                        @if($show_bank_rating)
                        <input type="hidden" id="bank_rating_score" value="{{$grandscore}}" />
                        <div class="rating_panel_switcher">
                        <span tab="original" class="active">CreditBPO Rating</span>
                        <span tab="bank">Bank Rating</span>
                        </div>
                        @endif
                        <div class="chart-container original_rating_panel">
                        <div style="padding: 5px 0px; float: left; width: 100%;">
                        <div id="title3_3" style="float: left; width: 100%;">
                            <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.credit_bpo_rating')}}</b></div>
                            <div style="float: right; padding-right: 10px;">
                            </div>
                        </div>
                        <div id="description3_3" style="float: left; width: 100%; display: none;">
                            <div style="padding: 10px;">
                            <span class="description-bar"></span>
                        </div>
                        </div>
                    </div>
                    <div style="float: left; width: 100%;">
                        <div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
                        <div id="graphs3_3text0000" class="gcircle-container">
                        <span id="graphs3_3text000" style="font-size: 35px;"></span>
                        </div>
                    </div>
                    <div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
                        <div style="float: left; width: 100%;">
                        <p><b>Result: <span id="graphs3_3text0"></span><br/>
                        <span id="graphs3_3text00" style = 'font-size: 14px;'></span></b></p>
                        </div>
                        <p style="display: none;"><b>Preliminary Probability of Default: <span class="prelimdefault">0%</span></b></p>
                    </div>
                    </div>
                    </div>
                    @if($show_bank_rating)
                    <div class="chart-container bank_rating_panel">
                    <div style="padding: 5px 0px; float: left; width: 100%;">
                    <div style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Bank Rating</b></div>
                        <div style="float: right; padding-right: 10px;">
                        </div>
                    </div>
                    <div style="float: left; width: 100%; display: none;">
                        <div style="padding: 10px;">
                        <span class="description-bar"></span>
                    </div>
                    </div>
                    </div>
                    <div style="float: left; width: 100%;">
                    <div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
                    <div id="graphs3_3text00001" class="gcircle-container">
                    <span id="graphs3_3text0001" style="font-size: 35px;"></span>
                    </div>
                    </div>
                    <div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
                    <div style="float: left; width: 100%;">
                    <p><b>Result: <span id="graphs3_3text01"></span><br/>
                        <span id="graphs3_3text001" style = 'font-size: 14px;'></span></b></p>
                    </div>
                    </div>
                    </div>
                    </div>
                    @endif

                    <div class="clearfix"></div>

                    </div>
                    </div>
                    <div class="row chart-remove-padding break-page">
                    <div class="col-lg-6">
                    <!-- chart box wrapper -->
                    <div class="chart-container">
                        <div style="margin:0 auto; width: 97%;">
                        <div style="float: left; width: 100%;">
                        <div id="title1" style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.business_considerations')}}</b></div>
                        <div style="float: right; padding-right: 10px;">
                            <b>
                            <span id="show1" style="display: block; cursor: pointer;">Expand +</span>
                            </b>
                        </div>
                        </div>
                    </div>
                    <div id="description1" style="display: none;">
                        <div style="padding: 10px;">
                        @foreach ($finalscore as $finalscoreinfo)
                        {{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
                        {{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
                        {{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
                        {{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
                        @if ($finalscoreinfo->score_rm == 90)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.rm_high')}}
                        </p>
                        {{ Form::hidden('score_rm_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_rm_value')) }}
                        @elseif ($finalscoreinfo->score_rm == 30)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.rm_low')}}
                        </p>
                        {{ Form::hidden('score_rm_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_rm_value')) }}
                        @endif

                        {{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
                        @if ($finalscoreinfo->score_cd == 30)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.cd_high')}}
                        </p>
                        {{ Form::hidden('score_cd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_cd_value')) }}
                        @elseif ($finalscoreinfo->score_cd == 20)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                        <br/>{{trans('risk_rating.cd_moderate')}}
                        </p>
                        {{ Form::hidden('score_cd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_cd_value')) }}
                        @else
                        <p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.cd_low')}}
                        </p>
                        {{ Form::hidden('score_cd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_cd_value')) }}
                        @endif

                        {{ Form::hidden('score_sd', $finalscoreinfo->score_sd, array('id' => 'score_sd')) }}
                        @if ($finalscoreinfo->score_sd == 30)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.sd_high')}}
                        </p>
                        {{ Form::hidden('score_sd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sd_value')) }}
                        @elseif ($finalscoreinfo->score_sd == 20)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                        <br/>{{trans('risk_rating.sd_moderate')}}
                        </p>
                        {{ Form::hidden('score_sd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sd_value')) }}
                        @else
                        <p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.sd_low')}}
                        </p>
                        {{ Form::hidden('score_sd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sd_value')) }}
                        @endif

                        {{ Form::hidden('score_bois', $finalscoreinfo->score_bois, array('id' => 'score_bois')) }}
                        @if ($finalscoreinfo->score_bois == 90)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.bo_high')}}
                        </p>
                        {{ Form::hidden('score_bois_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bois_value')) }}
                        @elseif ($finalscoreinfo->score_bois == 60)
                        <p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                        <br/>{{trans('risk_rating.bo_moderate')}}
                        </p>
                        {{ Form::hidden('score_bois_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_bois_value')) }}
                        @else
                        <p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.bo_low')}}
                        </p>
                        {{ Form::hidden('score_bois_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bois_value')) }}
                        @endif

                        {{ Form::hidden('score_agroup_total', 0, array('id' => 'score_agroup_total')) }}
                        {{ Form::hidden('score_agroup_sum', 240, array('id' => 'score_agroup_sum')) }}

                        @endforeach
                    </div>
                    </div>
                    <div style="float: left; width: 98%;">
                    <div id="container-pie1" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0 0 10px;"><img id = 'test-chart-export'></div>
                    </div>
                    </div>
                    </div><!-- #End chart box wrapper -->
                    </div>
                    <div class="col-lg-6">
                    <!-- chart box wrapper -->
                    <div class="chart-container">
                    <div style="margin:0 auto; width: 97%;">
                    <div style="float: left; width: 100%;">
                        <div id="title1" style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.management_quality')}}</b></div>
                        <div style="float: right; padding-right: 10px;">
                        <b>
                            <span id="show2" style="display: block;cursor: pointer;">Expand +</span>
                        </b>
                        </div>
                    </div>
                    </div>
                    <div id="description2" style="display: none;">
                    <div style="padding: 10px;">
                    @foreach ($finalscore as $finalscoreinfo)
                    {{ Form::hidden('score_sf', $finalscoreinfo->score_sf, array('id' => 'score_sf')) }}
                    {{ Form::hidden('score_bdms', $finalscoreinfo->score_bdms, array('id' => 'score_bdms')) }}
                    {{ Form::hidden('score_boe', $finalscoreinfo->score_boe, array('id' => 'score_boe')) }}
                    {{ Form::hidden('score_mte', $finalscoreinfo->score_mte, array('id' => 'score_mte')) }}
                    @if ($finalscoreinfo->score_boe == 21)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                    {{ Form::hidden('score_owner_exp', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_owner_exp')) }}
                    @elseif ($finalscoreinfo->score_boe == 14)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                    {{ Form::hidden('score_owner_exp', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_owner_exp')) }}
                    @elseif ($finalscoreinfo->score_boe == 7)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                    {{ Form::hidden('score_owner_exp', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_owner_exp')) }}
                    @endif
                    @if ($finalscoreinfo->score_mte == 21)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                    {{ Form::hidden('score_mng_exp', '<span style="color: green;"> '.trans('risk_rating.high').'</span>', array('id' => 'score_mng_exp')) }}
                    @elseif ($finalscoreinfo->score_mte == 14)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                    {{ Form::hidden('score_mng_exp', '<span style="color: orange;"> '.trans('risk_rating.moderate').'</span>', array('id' => 'score_mng_exp')) }}
                    @elseif ($finalscoreinfo->score_mte == 7)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                    {{ Form::hidden('score_mng_exp', '<span style="color: red;"> '.trans('risk_rating.low').'</span>', array('id' => 'score_mng_exp')) }}
                    @endif
                    @if ($finalscoreinfo->score_bdms == 36)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.bd_high')}}
                    </p>
                    {{ Form::hidden('score_bdms_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bdms_value')) }}
                    @elseif ($finalscoreinfo->score_bdms == 12)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.bd_low')}}
                    </p>
                    {{ Form::hidden('score_bdms_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bdms_value')) }}
                    @endif

                    {{ Form::hidden('score_sp', $finalscoreinfo->score_sp, array('id' => 'score_sp')) }}
                    @if ($finalscoreinfo->score_sp == 36)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.sp_high')}}
                    </p>
                    {{ Form::hidden('score_sp_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sp_value')) }}
                    @elseif ($finalscoreinfo->score_sp == 24)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                        <br/>{{trans('risk_rating.sp_moderate')}}
                    </p>
                    {{ Form::hidden('score_sp_value', '<span style="color: orange;"><br/>'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sp_value')) }}
                    @else
                    <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.sp_low')}}
                    </p>
                    {{ Form::hidden('score_sp_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sp_value')) }}
                    @endif

                    {{ Form::hidden('score_ppfi', $finalscoreinfo->score_ppfi, array('id' => 'score_ppfi')) }}
                    @if ($finalscoreinfo->score_ppfi == 36)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                        <br/>{{trans('risk_rating.pp_high')}}
                    </p>
                    {{ Form::hidden('score_ppfi_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_ppfi_value')) }}
                    @elseif ($finalscoreinfo->score_ppfi == 12)
                    <p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                        <br/>{{trans('risk_rating.pp_low')}}
                    </p>
                    {{ Form::hidden('score_ppfi_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_ppfi_value')) }}
                    @endif

                    {{ Form::hidden('score_bgroup_total', 0, array('id' => 'score_bgroup_total')) }}
                    {{ Form::hidden('score_bgroup_sum', 150, array('id' => 'score_bgroup_sum')) }}

                    @endforeach
                    </div>
                    </div>
                    <div style="float: left; width: 98%;">
                    <div id="container-pie2" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0px 0 10px;"></div>
                    </div>
                    </div>
                    </div><!-- #End chart box wrapper -->
                    </div>
                    </div>
                    <div class="row chart-remove-padding">
                    <div class="col-lg-6 col-sm-6">
                    <div class="chart-container">
                        <div style="padding: 5px 0px; width: 100%; height: 35px">
                        <div id="title4" style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
                        <div style="float: right; padding-right: 10px;">
                        </div>
                        </div>
                        <div id="description3" style="float: left; width: 100%; display: none;">
                        <div style="padding: 10px;">
                            <span class="description-bar"></span>
                        </div>
                        </div>
                    </div>
                    <div style="width: 100%; height: 390px; overflow: hidden;">

                    <div style="font-size: 11px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                        <td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
                        <td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
                        </tr>
                        <tr>
                        <td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
                        <td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
                        </tr>
                    </table>
                    </div>
                    <div style="text-align: center; font-size: 14px;">
                        <div style = 'margin-top:30px;display:block;min-height: 80px; width: 100%; text-align: center;'>
                            <img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style="width:250px;margin-bottom: 50px" />
                        </div>
                        <div style="clear:both"></div>
                    <!-- <div id="container-semi" style="min-width: 150px; height: 250px; max-width: 100%; margin: 0 auto"></div> -->
                    <div 'fc-description' style="font-size: 14px; min-width: 150px; ">
                        @if ($finalscoreinfo->score_facs >= 162)
                        Capacity to meet financial commitments on financial obligations are very strong.
                        @elseif ($finalscoreinfo->score_facs >= 145)
                        Capacity to meet financial commitments on financial obligations are very strong.
                        @elseif ($finalscoreinfo->score_facs >= 127)
                        Capacity to meet financial obligations is still strong but somewhat more susceptible to the adverse effects of changes in business and economic conditions.
                        @elseif ($finalscoreinfo->score_facs >= 110)
                        Exhibits adequate protection parameters. However, adverse economic conditions or changing circumstances are more likely to lead to a weakened capacity to meet debt payments or loan covenants.
                        @elseif ($finalscoreinfo->score_facs >= 92)
                        Currently has the capacity to meet financial commitment on loans. However, adverse business, financial, or economic conditions will likely impair the capacity or willingness to meet debt obligations.
                        @elseif ($finalscoreinfo->score_facs >= 75)
                        Faces major ongoing uncertainties or exposure to adverse business, financial, or economic conditions which could lead to the obligor's inadequate capacity to meet debt obligations.​
                        @elseif ($finalscoreinfo->score_facs >= 57)
                        Currently vulnerable to nonpayment, and is dependent upon favorable business, financial, and economic conditions in order to meet financial obligations. Not likely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                        @elseif ($finalscoreinfo->score_facs >= 40)
                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                        @elseif ($finalscoreinfo->score_facs >= 22)
                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                        @elseif ($finalscoreinfo->score_facs >= 4)
                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                        @else
                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                        @endif

                        @if (USER_ROLE_BANK == Auth::user()->role && $m_score >= -2.22)
                        <br/><br/><b>{{trans('risk_rating.fs_earnings_manipulator')}}</b>
                        @endif
                    </div>
                    </div>
                    </div>
                    </div>


                    </div>

                    <div class="col-lg-6 col-sm-6">
                    <div class="chart-container">

                    <div style="width: 100%; height: 429px; font-size: 14px;">
                    <table class="rating-summary-padding-table" cellpadding="0" cellspacing="0" border="0" width="100%">

                        <tr>
                        <td>&nbsp;</td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cc_year1 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cc_year2 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cc_year3 : "" }}</span></td>
                    </tr>
                    <tr>
                        <td>{{trans('risk_rating.fc_receivable')}}</td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->receivables_turnover1 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->receivables_turnover2 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->receivables_turnover3 : "" }}</span></td>
                    </tr>
                    <tr>
                        <td>+ {{trans('risk_rating.fc_inventory')}}</td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->inventory_turnover1 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->inventory_turnover2 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->inventory_turnover3 : "" }}</span></td>
                    </tr>
                    <tr>
                        <td>- {{trans('risk_rating.fc_payable')}}</td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->accounts_payable_turnover1 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->accounts_payable_turnover2 : "" }}</span></td>
                        <td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->accounts_payable_turnover3 : "" }}</span></td>
                    </tr>
                    <tr>
                        <td><b>= {{trans('risk_rating.fc_ccc')}}</b></td>
                        <td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cash_conversion_cycle1 : "" }}</span></b></td>
                        <td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cash_conversion_cycle2 : "" }}</span></b></td>
                        <td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cash_conversion_cycle3 : "" }}</span></b></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>{{trans('risk_rating.adb_1')}}</td>
                        <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($finalscore[0]->average_daily_balance, 2, '.', ',') }}</span></td>
                    </tr>
                    <tr>
                        <td>{{trans('risk_rating.adb_2')}}</td>
                        <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_margin }} %</span></td>
                    </tr>
                    <tr>
                        <td>{{trans('risk_rating.adb_3')}}</td>
                        <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_ratio }}</span></td>
                    </tr>
                    <tr>
                        <td>{{trans('risk_rating.adb_4')}}</td>'
                        <?php
                        $loan_amount = ( $finalscore[0]->average_daily_balance * 0.3 * 360 ) / ( 181 * 0.04900416667 );
                        ?>
                        <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($loan_amount, 2, '.', ',') }}</span></td>
                    </tr>
                    </table>
                    </div>

                    </div>
                    </div>
                    @if ((STR_EMPTY != $finalscoreinfo->positive_points) || (STR_EMPTY != $finalscoreinfo->negative_points))
                    <div class="col-lg-12 col-sm-12">
                    <div class="chart-container">

                    <div style="width: 100%; font-size: 11px;">
                    <?php
                    $pos_exp    = explode('|', $finalscoreinfo->positive_points);
                    $neg_exp    = explode('|', $finalscoreinfo->negative_points);
                    ?>

                    @if (STR_EMPTY != $finalscoreinfo->positive_points)
                    @if (STR_EMPTY != $finalscoreinfo->negative_points)
                    <div class = 'col-md-6'>
                        @else
                        <div class = 'col-md-12'>
                        @endif
                        <span class="divider1"></span>
                        <div id="positive-points">
                            <b style = 'font-size: 16px;'> The Good </b>
                            <ul>
                            @foreach ($pos_exp as $positive)
                            <li> {{ $positive }} </li>
                            @endforeach
                            </ul>
                        </div>
                        </div>
                        @endif

                        @if (STR_EMPTY != $finalscoreinfo->negative_points)
                        @if (STR_EMPTY != $finalscoreinfo->positive_points)
                        <div class = 'col-md-6'>
                        @else
                        <div class = 'col-md-12'>
                            @endif
                            <span class="divider1"></span>
                            <div id="negative-points">
                            <b style = 'font-size: 16px;'> The Bad </b>
                            <ul>
                                @foreach ($neg_exp as $negative)
                                <li> {{ $negative }} </li>
                                @endforeach
                            </ul>
                            </div>
                        </div>
                        @endif
                        </div>

                    </div>
                    </div>
                    @endif
                    </div>
                    <div class="clearfix"></div>

                    <div class="row chart-remove-padding">
                    <div class="col-lg-6 col-sm-6">
                    <div class="chart-container" style = 'min-height: 375px;'>
                        <div style="padding: 5px 0px; float: left; width: 100%;">
                        <div id="title4" style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
                        <div style="float: right; padding-right: 10px; text-align: right;">
                        <b>
                            <span id="show4" style="display: block;cursor: pointer;">Expand +</span>
                        </b>
                                            <!-- <div class="grdp_display">Region: {{ $entity->regDesc }}</div> -->
                                        </div>
                                        </div>
                                    </div>
                                    <div id="description4" style="display: none;">
                                    <div style="padding: 10px;">
                                        <span class="description-bar"></span>
                                    </div>
                                    </div>
                                    <div style="float: left; width: 98%;">
                                    <div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                    <div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="chart-container" style = 'min-height: 375px;'>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div class="legend">
                                <div class="legend-item">
                                    <span class="legend-info"></span>
                                    <span>Company</span>
                                </div>
                                <div class="legend-item">
                                    <span class="legend-info industry"></span>
                                    <span>Industry</span>
                                </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
                        <br/>
                        <p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
                        <p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
                        <p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

                        @endif

                        <input type="button" id="finananalysis_next" value="Next" />

                        </div>
                    </div>
                    @else
                    <div id="ssc" class="tab-pane fade">
                        @foreach ($finalscore as $finalscoreinfo)
                        {{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
                        {{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
                        {{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
                        {{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
                        {{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
                        @endforeach
                        <div class="row col-md-12">
                        <a id="dev-ref-link" class = 'hidden-xs hidden-sm'>{{trans('messages.api_request_code')}}</a>
                        <a id="privatelink">{{trans('messages.private_link')}}</a>
                        @if (4 == Auth::user()->role)
                        <a id="printdscr" href = '{{ URL::To("show/dscr_print/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}} DSCR</a>
                        @endif
                        <a id="printthis" href = '{{ URL::To("show/cbpo_pdf/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}} Rating Report</a>
                        </div>
                        <div class="row col-md-4">
                        <img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
                    </div>
                    <div class="col-md-7" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
                    <div class="col-md-12"><hr/></div>

                    <div class="spacewrapper break-page">
                        @if ($entity->status == 7)
                        @if(isset($readyratiodata[0]))
                        @if (0 != $readyratiodata[0]->gross_revenue_growth1)
                        {{ Form::hidden('gross_revenue_growth2', $companyIndustry['gross_revenue_growth'], array('id' => 'gross_revenue_growth22')) }}
                        @else
                        {{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
                        @endif

                        @if (0 != $readyratiodata[0]->net_income_growth1)
                        {{ Form::hidden('net_income_growth2', $companyIndustry['net_income_growth'], array('id' => 'net_income_growth22')) }}
                        @else
                        {{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
                        @endif
                        {{ Form::hidden('gross_profit_margin2', $companyIndustry['gross_profit_margin'], array('id' => 'gross_profit_margin22')) }}
                        {{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
                        {{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
                        {{ Form::hidden('current_ratio2', $companyIndustry['current_ratio'], array('id' => 'current_ratio22')) }}
                        {{ Form::hidden('debt_equity_ratio2', $companyIndustry['debt_to_equity'], array('id' => 'debt_equity_ratio22')) }}
                        @endif
                        <div class="row chart-remove-padding">
                        <div class="col-lg-12 col-sm-12">
                        <div class="chart-container" >
                            <div style="padding: 5px 0px; width: 100%; height: 35px">
                            <div id="title4" style="float: left; width: 100%;">
                            <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
                            <div style="float: right; padding-right: 10px;">
                            </div>
                            </div>
                            <div id="description3" style="float: left; width: 100%; display: none;">
                            <div style="padding: 10px;">
                                <span class="description-bar"></span>
                            </div>
                            </div>
                        </div>
                        <div style="width: 100%; overflow: hidden;">

                        <div style="font-size: 11px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                            <td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
                            <td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
                            </tr>
                            <tr>
                            <td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
                            <td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
                            </tr>
                        </table>
                        </div>
                        
                        <div style="text-align: center; font-size: 14px;">
                        <div style = 'margin-top:30px;display:block;min-height: 80px; width: 100%; text-align: center;'>
                            <img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style="width:250px;margin-bottom: 50px" />
                        </div>
                        <div style="clear:both"></div>
                        <!-- <div id="container-semi" style="min-width: 150px; height: 300px; max-width: 100%; margin: 0 auto"></div> -->
                        <div 'fc-description' style="min-width: 150px; max-width: 100%; margin: 0 auto; padding-left: 75px; padding-right: 75px; padding-bottom: 40px;">
                            @if ($finalscoreinfo->score_facs >= 162)
                            Capacity to meet financial commitments on financial obligations are very strong.
                            @elseif ($finalscoreinfo->score_facs >= 145)
                            Capacity to meet financial commitments on financial obligations are very strong.
                            @elseif ($finalscoreinfo->score_facs >= 127)
                            Capacity to meet financial obligations is still strong but somewhat more susceptible to the adverse effects of changes in business and economic conditions.
                            @elseif ($finalscoreinfo->score_facs >= 110)
                            Exhibits adequate protection parameters. However, adverse economic conditions or changing circumstances are more likely to lead to a weakened capacity to meet debt payments or loan covenants.
                            @elseif ($finalscoreinfo->score_facs >= 92)
                            Currently has the capacity to meet financial commitment on loans. However, adverse business, financial, or economic conditions will likely impair the capacity or willingness to meet debt obligations.
                            @elseif ($finalscoreinfo->score_facs >= 75)
                            Faces major ongoing uncertainties or exposure to adverse business, financial, or economic conditions which could lead to the obligor's inadequate capacity to meet debt obligations.​
                            @elseif ($finalscoreinfo->score_facs >= 57)
                            Currently vulnerable to nonpayment, and is dependent upon favorable business, financial, and economic conditions in order to meet financial obligations. Not likely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscoreinfo->score_facs >= 40)
                            High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscoreinfo->score_facs >= 22)
                            High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscoreinfo->score_facs >= 4)
                            High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @else
                            High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @endif

                            @if (USER_ROLE_BANK == Auth::user()->role && $m_score >=  -2.22)
                            <br/><br/><b>{{trans('risk_rating.fs_earnings_manipulator')}}</b>
                            @endif
                        </div>
                    </div>
                    @if(!empty($keySummary))
					<div style = 'font-size: 11px;' class = 'col-md-12'>
                        @if($bad_points == false && $good_points == true)
                            <div class = 'col-md-12 positive_points_parent'>
                        @elseif(($bad_points == false && $good_points == false))
                            <div class = 'col-md-12 positive_points_parent'>
                        @else
                            <div class = 'col-md-6 positive_points_parent'>
                        @endif
							<span class="divider1"></span>
							<div id="positive-points">
								<b style = 'font-size: 16px;'> The Good </b>
								<ul class="positive_points">
							        {{-- Noncurrent assets --}}
							        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
										<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
							        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
                                        <li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Debt-to-equity Ratio --}}
							        @if($keySummary[0]->DebtRatio < 0.30)
										<li>{!! trans('financial_analysis/conclusion.the_debt_to_equity', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
										<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
										<li>{!! trans('financial_analysis/conclusion.the_percentage_of_liabilities', ['debtratioPercentage' => number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Working Capital and Inventories --}}
							        @if($keySummary[0]->NWC > 0)
							            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
							                <li>{{trans('financial_analysis/conclusion.long_term_resources')}}</li>
							            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))
											<li>{{trans('financial_analysis/conclusion.working_capital')}}</li>
							            @endif
							        @endif

							        {{-- Current Ratio --}}
							        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->CurrentRatio >= 2.1)
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio_criteria', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Quick Ratio --}}
							        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
										<li>{!! trans('financial_analysis/conclusion.a_good_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->QuickRatio >= 1.1)
										<li>{!! trans('financial_analysis/conclusion.an_outstanding_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Cash Ratio --}}
							        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->CashRatio > 0.25)
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Return of Equity --}}
							        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
										<li>{!! trans('financial_analysis/conclusion.return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year ]) !!}</li>
							        @elseif($keySummary[0]->ROE > 0.2)
										<li>{!! trans('financial_analysis/conclusion.high_return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',',')]) !!}</li>
							        @endif

							        {{-- ROA --}}
							        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
										<li>{!! trans('financial_analysis/conclusion.good_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',',')]) !!}</li>
							        @elseif($keySummary[0]->ROA > 0.1)
										<li>{!! trans('financial_analysis/conclusion.excellent_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
							        @endif

							        {{-- Net Worth --}}
							        @if($fa_report)
							            @if($balance_sheets[0]->IssuedCapital > 0)
							                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
							                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
													<li>{!! trans('financial_analysis/conclusion.net_worth_net_assets', ['netAsset' => number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
							                    @else
							                        <li>{{trans('financial_analysis/conclusion.the_net_worth_is_higher')}}</li>
							                    @endif
							                @endif
							            @endif
							        @endif

							        {{-- Equity --}}
							        @if($fa_report)
							            @if($balance_sheets)
							                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
							                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
													<li>{!! trans('financial_analysis/conclusion.equity_value_grew', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' =>  $balance_sheets[0]->Year]) !!}</li>
							                    @else
													<li>{!! trans('financial_analysis/conclusion.the_equity_growth_for_the', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' => $balance_sheets[0]->Year ]) !!}</li>
							                    @endif
							                @endif
							            @endif
							        @endif

							        {{-- EBIT --}}
							        @if($keySummary[0]->EBIT > 0)
							            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
							                @if($keySummary[0]->EBIT > 0)
												<li>{!! trans('financial_analysis/conclusion.during_the_entire_period', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
							                @else
												<li>{!! trans('financial_analysis/conclusion.earnings_before_interest', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
							                @endif
							            @endif
							        @endif

							        {{-- Comprehensive Income --}}
							        @if($fa_report)
							            @if($income_statements[0]->ComprehensiveIncome > 0)
											<li>{!! trans('financial_analysis/conclusion.the_income_from_financial', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}</li>
							            @endif
							        @endif

							    </ul>
							</div>
							</div>
                            @if($bad_points == false && $good_points == true)
                                <div class = 'col-md-6 negative_points_parent' hidden>
                            @elseif(($bad_points == false && $good_points == falses))
                                <div class = 'col-md-12 negative_points_parent'>
                            @else
                                <div class = 'col-md-6 negative_points_parent'>
                            @endif
                                <span class="divider1"></span>
                                <div id="negative-points" >
                                    <b style = 'font-size: 16px;'> The Bad </b>
                                    <ul class="negative_points">
                                        {{-- Debt-to-equity Ratio --}}
                                        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
                                            <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage' => number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}</li>
                                        @elseif($keySummary[0]->DebtRatio > 1)
                                            <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_critical', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
                                        @endif

                                        {{-- Working Capital and Inventories --}}
                                        @if($keySummary[0]->NWC > 0)
                                            @if($keySummary[0]->InventoryNWC > 1.0)
                                                <li>{{trans('financial_analysis/conclusion.available_working_capital') }}</li>
                                            @endif
                                        @else
                                            <li>{{trans('financial_analysis/conclusion.not_enough_long_term')}}</li>
                                        @endif

                                        {{-- Current Ratio --}}
                                        @if($keySummary[0]->CurrentRatio < 1)
                                            <li>{!! trans('financial_analysis/conclusion.the_current_ratio_standard', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
                                            <li>{!! trans('financial_analysis/conclusion.the_current_ratio_normal', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                        @endif

                                        {{-- Quick Ratio --}}
                                        @if($keySummary[0]->QuickRatio < 0.5)
                                            <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
                                            <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                        @endif

                                        {{-- Cash Ratio --}}
                                        @if($keySummary[0]->CashRatio < 0.05)
                                            <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
                                            <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                        @endif

                                        {{-- Return of Equity --}}
                                        @if($keySummary[0]->ROE < 0)
                                            <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe_critical', ['roe' => number_format($keySummary[0]->ROE,2,'.',',') ]) !!}</li>
                                        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
                                            <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}</li>
                                        @endif

                                        {{-- ROA --}}
                                        @if($keySummary[0]->ROA < 0)
                                            <li>{!! trans('financial_analysis/conclusion.critical_return_on_assets', ['year' => $keySummary[0]->Year ]) !!}</li>
                                        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
                                            <li>{!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets', ['roa' => number_format($keySummary[0]->ROA*100,1,'.', ','), 'year'=>$keySummary[0]->Year]) !!}</li>
                                        @endif

                                        {{-- Net Worth --}}
                                        @if($fa_report)
                                            @if($balance_sheets[0]->IssuedCapital > 0)
                                                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                                                    <li>{!! trans('financial_analysis/conclusion.net_worth_is_less_than', ['issuedCapital' => number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
                                                @endif
                                            @endif
                                        @endif

                                        {{-- EBIT --}}
                                        @if($keySummary[0]->EBIT < 0)
                                            <li>{!! trans('financial_analysis/conclusion.during_the_entire_period_ebit', ['ebit' => $keySummary[0]->EBIT ]) !!}</li>
                                        @endif

                                        {{-- comprehensive income --}}
                                        @if($fa_report)
                                            @if($income_statements[0]->ComprehensiveIncome < 0)
                                                <li>{!! trans('financial_analysis/conclusion.the_income_from_financial_operational', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',') ]) !!}</li>
                                            @endif
                                        @endif

                                    </ul>
                                </div>
						    </div>
						  
						</div>
						@endif
					</div>

                            </div>
                    </div>
                </div>

                <div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container">
					<p><strong>Cash Conversion Cycle</strong></p>
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col" class="col-md-2 text-center" rowspan="2" ></td>
								<th scope="col" class="col-md-2 text-center" colspan="{{count($turnoverRatios)}}" style="font-size: 15px;">Value, days</td>
								<th scope="col" class="col-md-2 text-center" rowspan ="2" style="font-size: 15px;">{!! trans('financial_analysis/turnover_ratios.change',['colCount' => count($turnoverRatios)+1]) !!}</th>
							</tr>
							@foreach($turnoverRatios as $turnover)
								<th style="text-align:center; font-size:15px;" >{{$turnover->year}}</th>
							@endforeach
						<thead>
						<tbody>
                            <tr>
								<td scope="col" class="col-md-2 text-center">1</td>
								@for($x=2; $x<=(count($turnoverRatios) + 1); $x++)
									<td scope="col" class="col-md-2 text-center" style="vertical-align: middle;">{{$x}}</td>
								@endfor
								<td scope="col" class="col-md-2 text-center">{{$x}}</td>
							</tr>
							<tr>
								<td style="font-size: 15px;" class="text-center">Days Sales Outstanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="font-size: 15px;">{{$turnover->ReceivablesTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="font-size: 15px;" class="text-center">Days Payable Oustanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="font-size: 15px;">{{$turnover->PayableTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="font-size: 15px;" class="text-center">Days Inventory Outstanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="font-size: 15px;">{{$turnover->InventoryTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="font-size: 15px;" class="text-center"><strong>Cash Conversion Cycle</strong></td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="font-size: 15px;">{{$turnover->CCC}}</td>
								@endforeach
								<td scope="col" class="text-center" style="font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
					<!-- <p style="font-size: 11px;" class = 'print-none'>
						@php
						$totalDays = 0;
						$totalPayableDays = 0;
						$cnt = 0;
						foreach($turnoverRatios as $turnover){
							$totalDays += $turnover->AssetTurnover;
							$totalPayableDays += $turnover->PayableTurnover;
							$cnt += 1;
						}
						$arr = [
							'receivablesTurnover' => $turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover,
							'payableTurnover' => $turnoverRatios[count($turnoverRatios)-1]->PayableTurnover,
							'financialReportTitle' => $entity->companyname,
							'totalDays'            => round(($totalDays/$cnt),0),
							'payableDays'          => round(($totalPayableDays/$cnt),0)
						];
					@endphp
					{!! trans('financial_analysis/turnover_ratios.during_the_last_year',$arr) !!} -->
					</p>
				</div>
			</div>
		</div>

        <div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container">
					
					<p><strong>Return on Equity Drivers</strong></p>

					<table class="table">
						<thead class="thead-dark">
						<tr>
							<th scope="col" class="col-md-2 text-center">    </th>
							@foreach($ROEDrivers as $drivers)
								<th scope="col" class="text-center" style="font-size: 15px;">{{$drivers['year']}}</th>
							@endforeach
						</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Net Income</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['net_income'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Profit Margin</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['profit_margin'],2,".",",")}}</strong></td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Ave. Assets</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Asset Turnover</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['turnover_asset'],2,".",",")}}</strong></td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Ave. Assets</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Financial Leverage</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['financial_leverage'],2,".",",")}}</strong></td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Return on Equity</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format(($drivers['roe']*100),2,".",",")}}%</strong></td>
								@endforeach
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

                        
                    <div class="clearfix"></div>

                    @if($forecast_data != null)
                    <div class="row chart-remove-padding" id="growth_forecasting_chart" hidden>
                    <div class="col-lg-12 col-sm-12">
                    <div class="chart-container">
                        <p>
                        <strong>Growth Forecast</strong>
                        <a style = "color: #056fc4;" href="#" class="popup-hint popup-hint-header" data-hint="The Growth Forecast graph is meant to present CreditBPO’s own
                        forecast for expected industry growth alongside expected company
                        growth 1, 2 and 3 years forward. Calculation emphasis was placed
                        on keeping the base numbers used for both trendlines raw, and
                        pushing multivariable series analysis into the trend line itself.
                        <br/><br/>
                        The purpose of the Industry Comparison Graph is to compare the Industry
                        and the Company using metrics that are calculated from current (as opposed to forecast)
                        figures. This is important because the metrics used must be exactly the
                        same in terms of both source and derivation. In perspective, it is a
                        static measure of how the company of the data-specific period compares
                        to the greater industry of the same period.
                        <br/><br/>
                        Given the aims for both graphs explained above, the figures used differ
                        by their very nature. For example, Revenue Growth is used in both graphs but the
                        Industry figure in Growth Forecast is very different from the one in
                        Industry Comparison. This is because the Industry Comparison figure is
                        averaged out over thousands of companiesp for which we have individual
                        figures. The overall Growth Forecast is based on the entire Industry
                        indicator which includes companies for which there is no sufficient granular data.">
                        More info...
                        </a>
                    </p>
                    <script>var forecast_data = {!!json_encode($forecast_data)!!};</script>
                    <div id="forecast_graph"></div>
                    <div>
                    <p style = 'font-size: 14px;'>{!! $forecast_data['description'] !!}</p>
                    </div>
                    </div>
                    </div>
                    </div>
                    @endif

                    @if ($export_dscr == 1)
                    <div class="clearfix"></div>
                    <div class="row chart-remove-padding">
                    <div class="col-lg-12 col-sm-12">
                        <div class="chart-container">
                        <strong>User Exported Parameters</strong>
                        <div id="debt_service_calculator_rs_container"></div>
                        </div>
                    </div>
                    </div>
                    @endif

                    <div class="clearfix"></div>
                    <div class="row chart-remove-padding" id="industry_composition_chart" hidden>
                    <div class="col-lg-6 col-sm-6">
                    <div class="chart-container" style = 'min-height: 375px;'>
                        <div style="padding: 5px 0px; float: left; width: 100%;">
                        <div id="title4" style="float: left; width: 100%;">
                        <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
                        <div style="float: right; padding-right: 10px; text-align: right;">
                            <small><b><span id="show4" style="display: block;cursor: pointer;">Expand +</span></b></small>
                            <div class="grdp_display"><small><strong>Region:</strong> {{ $entity->regDesc }}</small></div>
                            <div class="grdp_value"><small><strong>GRDP:</strong> {{$industryGrdp->grdp * 100}} <strong>Year:</strong> {{$industryComparisonYear}}</small></div>
                                        </div>
                                        </div>
                                    </div>
                                    <div id="description4" style="display: none;">
                                    <div style="padding: 10px;">
                                        <span class="description-bar"></span>
                                    </div>
                                    </div>
                                    <div style="float: left; width: 98%;">
                                    <div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                    <div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="chart-container" style = 'min-height: 375px;'>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div style="float: left; width: 98%;">
                                <div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
                                </div>
                                <div class="legend">
                                <div class="legend-item">
                                    <span class="legend-info"></span>
                                    <span>You</span>
                                </div>
                                <div class="legend-item">
                                    <span class="legend-info industry"></span>
                                    <span>Industry</span>
                                </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
                        <br/>
                        <p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
                        <p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
                        <p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

                        @endif

                        <input type="button" id="finananalysis_next" value="Next" />

                        </div>
                    </div>
                    @endif
                    <div id="fianalysis" class="tab-pane fade">
                        <div class="spacewrapper">
                        <div class="col-md-4">
                        <img src="{{ URL::asset('images/creditbpo-logo-big.png') }}" width="100%" style="margin-bottom:5px;" />
                        </div>
                        <div class="col-md-8" style="margin-top:60px;padding-left:60px;font-size:18px;">{{ trans('messages.financial_analysis_for') }} {{ $entity->companyname }}</div>
                        <div class="col-md-12"><hr/></div>
                        @php
					    $fname_count = 0;
					    $fname_exp  = [];
					    if(!empty($documents9->document_orig)){
					      $fname_exp = explode('.', $documents9->document_orig);
					      $fname_count = @count($fname_exp);
					    }
					    
					    $useIndex = 0;
					    if($fname_count > 2){
					      $useIndex = $fname_count - 1;
					    }else{
					      $useIndex = 1;
					    }

					    $lang = app()->getLocale();
					    ($lang == 'fil') ? $suffix = 'tl':$suffix = 'en';
					    
					    @endphp

					    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'test')

					      @if(!empty($fname_exp[$useIndex]))
					        @if ('pdf' == $fname_exp[$useIndex])
					        <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
					          <div class="col-md-12">
					            <p>
					              It appears you don't have a PDF plugin for this browser.
					              No biggie... you can
					              <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}">click here to download the PDF file.</a>
					            </p>
					          </div>
					        </object>
					        @else
					        <div class="col-md-12">
					          <p>
					            It appears you don't have a PDF plugin for this browser.
					            No biggie... you can
					            <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}">click here to download the PDF file.</a>
					          </p>
					        </div>
					        @endif

					      @else
					        
					        <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
					          <div class="col-md-12">
					            <p>
					              It appears you don't have a PDF plugin for this browser.
					              No biggie... you can
					              <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" target="_blank">click here to download the PDF file.</a>
					            </p>
					          </div>
					        </object>

					      @endif

					    @else

					      <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
					          <div class="col-md-12">
									<p>
										It appears you don't have a PDF plugin for this browser.
										No biggie... you can
										<a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" target="_blank">click here to download the PDF file.</a>
									</p>
					          </div>
					        </object>

					    @endif    

                        <div class="col-md-12">
                        <p style="font-size: 11px;">{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
                        <p style="font-size: 12px;"><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
                        <p style="font-size: 11px;">{{trans('risk_rating.copyright')}}</p>

                        <input type="button" id="sscore_prev" value="Previous" />

                        @if($bank_standalone==0)
                        <input type="button" id="sscore_next" value="Next" />
                        @else
                        <input type="button" id="strategicprofile_next" value="Next" />
                        @endif
                        </div>

                    </div>
                    </div>
                        {{ Form::hidden('entityids', $entity->entityid, array('id' => 'entityids')) }}
                        {{ Form::hidden('loginid', $entity->loginid, array('id' => 'loginid')) }}
                        {{ Form::hidden('status', $entity->status, array('id' => 'status')) }}
                        {{ Form::hidden('entity_type', $entity->entity_type, array('id' => 'entity_type')) }}
                        {{ Form::hidden('clogidid', Auth::user()->loginid, array('id' => 'clogidid')) }}
                        {{ Form::hidden('userid', Auth::user()->role, array('id' => 'userid')) }}
                        {{ Form::hidden('bindex', $entity->industry_score, array('id' => 'bindex')) }}
                        {{ Form::hidden('tindex', $entity->industry_trend, array('id' => 'tindex')) }}
                        {{ Form::hidden('bgroup', $entity->business_group / 100, array('id' => 'bgroup')) }}
                        {{ Form::hidden('mgroup', $entity->management_group / 100, array('id' => 'mgroup')) }}
                        {{ Form::hidden('fgroup', $entity->financial_group / 100, array('id' => 'fgroup')) }}
                        {{ Form::hidden('nymteam', $entity->number_year_management_team, array('id' => 'nymteam')) }}
                        {{ Form::hidden('gross_revenue_growth', $entity->gross_revenue_growth, array('id' => 'gross_revenue_growth')) }}
                        {{ Form::hidden('net_income_growth', $entity->net_income_growth, array('id' => 'net_income_growth')) }}
                        {{ Form::hidden('gross_profit_margin', $entity->gross_profit_margin, array('id' => 'gross_profit_margin')) }}
                        {{ Form::hidden('net_profit_margin', $entity->net_profit_margin, array('id' => 'net_profit_margin')) }}
                        {{ Form::hidden('net_cash_margin', $entity->net_cash_margin, array('id' => 'net_cash_margin')) }}
                        {{ Form::hidden('current_ratio', $entity->current_ratio, array('id' => 'current_ratio')) }}
                        {{ Form::hidden('debt_equity_ratio', $entity->debt_equity_ratio, array('id' => 'debt_equity_ratio')) }}
                        {{ Form::hidden('bank_ci_view', $bank_ci_view, array('id' => 'bank_ci_view')) }}
                        {{ Form::hidden('sme_supervisor', $entity->current_bank, array('id' => 'sme_supervisor')) }}
                        {{ Form::hidden('trial_flag', Auth::user()->trial_flag, array('id' => 'trial_flag')) }}
                        {{ Form::hidden('base_year', $base_year, array('id' => 'base_year')) }}
                        {{ Form::hidden('report_anonymous', $entity->anonymous_report, array('id' => 'report_anonymous')) }}
                        {{ Form::hidden('local_rate', $local_rate->value, array('id' => 'local_rate')) }}
                        {{ Form::hidden('foreign_rate', $foreign_rate->value, array('id' => 'foreign_rate')) }}
                        @if ($financialChecker)
                            {{ Form::hidden('financialChecker', $financialChecker, array('id' => 'financialChecker')) }}
                            {{ Form::hidden('standalone_bank', $finalRatingScore[3], array('id' => 'standalone_bank')) }}
                            {{ Form::hidden('fpr', $finalRatingScore[0], array('id' => 'fpr')) }}
                            {{ Form::hidden('fpm', $finalRatingScore[1], array('id' => 'fpm')) }}
                            {{ Form::hidden('financialTotal', $finalRatingScore[2], array('id' => 'financialTotal')) }}
                        @endif
                    @endif
                    @endif
                    <div id="registration1" class="tab-pane fade in active">
                        <div id = 'biz-details-cntr' class="spacewrapper noTab">
                            <div class="read-only reload-biz_details">
                                <h4>{{trans('business_details1.business_details')}}</h4>
                                <div class="page-title clearfix">
                                    <div class="">
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.companyname') }}:</span>
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field anonymous-info' data-name = 'biz_details.companyname' data-field-id = '{{ $entity->entityid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Company - {{ $entity->entityid }}
                                                @else
                                                {{ $entity->companyname }}
                                                @endif
                                            </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="Company Name" value="{{ $entity->companyname }}" group="business_details" item="companyname"></span>
                                        </span>


                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.company_tin') }}(e.g. 111-111-111 or 111-111-111-111):  *</span>
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field anonymous-info' data-name = 'biz_details.company_tin' data-field-id = '{{ $entity->entityid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $entity->company_tin }}
                                                @endif
                                            </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="Company Name" value="{{ $entity->companyname }}" group="business_details" item="companyname"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.entity_type') }}:  *</span>
                                            @if (BIZ_ENTITY_CORP == $entity->entity_type)
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field' data-name = 'biz_details.entity_type' data-field-id = '{{ $entity->entityid }}'>
                                                {{ trans('business_details1.corporation') }}
                                            </span>
                                            </span>
                                            @else
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field' data-name = 'biz_details.entity_type' data-field-id = '{{ $entity->entityid }}'>
                                                {{ trans('business_details1.sole_proprietorship') }}
                                            </span>
                                            </span>
                                            @endif
                                            <span class="investigator-control" group_name="Business Details" label="Business Entity Type" value="{{ $entity->entity_type }}" group="business_details" item="entity_type"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.sec_company_reg_no') }}:  *</span>
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field anonymous-info' data-name = 'biz_details.tin_num' data-field-id = '{{ $entity->entityid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $entity->tin_num }}
                                                @endif
                                            </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="SEC Company Reg. No." value="{{ $entity->tin_num }}" group="business_details" item="tin_num"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.sec_reg_date') }}:  *</span>
                                            <span class="detail-value progress-required">
                                            <span class = 'editable-field date-input' data-name = 'biz_details.sec_reg_date' data-field-id = '{{ $entity->entityid }}'>
                                                @if ('0000-00-00' != $entity->sec_reg_date)
                                                {{ date("F d, Y", strtotime($entity->sec_reg_date)) }}
                                                @endif
                                            </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="SEC Reg. Date." value="{{ $entity->sec_reg_date }}" group="business_details" item="sec_reg_date"></span>
                                        </span>
                                        @if($bank_standalone==0)
                                            
                                           
                                            @if (BIZ_ENTITY_SOLE == $entity->entity_type)
                                                <span class="details-row">
                                                    <span class="details-label">
                                                        @if (BIZ_ENTITY_CORP == $entity->entity_type)
                                                            {{ trans('business_details1.authorized_signatory_board') }}:
                                                        @else
                                                            {{ trans('business_details1.authorized_signatory_owner') }}:
                                                        @endif
                                                        @if ($doc_req)
                                                            @if ((STS_OK == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_OK == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
                                                                *
                                                            @endif
                                                        @else
                                                            *
                                                        @endif
                                                    </span>
                                                    @if (STR_EMPTY != $entity->authorized_signatory)
                                                        <span class="detail-value">
                                                            @if ($doc_req)
                                                                @if ((STS_NG == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
                                                                    <span class="progress-field-counter" min-value="1"> 1 </span>
                                                                @else
                                                                    <span class="progress-required-counter" min-value="1"> 1 </span>
                                                                @endif
                                                            @else
                                                                <span class="progress-required-counter" min-value="1"> 1 </span>
                                                            @endif
                                                            <span id = 'auth-sign-{{ $entity->entityid }}'>
                                                                <a href="{{ URL::to('/') }}/download/documents/{{ $entity->authorized_signatory }}" target="_blank">
                                                                    @if (BIZ_ENTITY_CORP == $entity->entity_type)
                                                                        Download {{ trans('business_details1.authorized_signatory_board') }}
                                                                    @else
                                                                        Download {{ trans('business_details1.authorized_signatory_owner') }}
                                                                    @endif
                                                                </a>
                                                            </span>
                                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                                                | <a href="#" class="pop-file-uploader" data-header = 'Authorized Signatory' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/authorized_signatory/biz_details") }}'>
                                                                    @if (BIZ_ENTITY_CORP == $entity->entity_type)
                                                                        Upload {{ trans('business_details1.authorized_signatory_board') }}:
                                                                    @else
                                                                        Upload {{ trans('business_details1.authorized_signatory_owner') }}:
                                                                    @endif
                                                                </a>
                                                            @endif
                                                        </span>
                                                    @else
                                                        <span class="detail-value">
                                                            @if ($doc_req)
                                                                @if ((STS_NG == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
                                                                    <span class="progress-field-counter" min-value="1"> 0 </span>
                                                                @else
                                                                    <span class="progress-required-counter" min-value="1"> 0 </span>
                                                                @endif
                                                            @else   
                                                                <span class="progress-required-counter" min-value="1"> 0 </span>
                                                            @endif
                                                            <span id = 'auth-sign-{{ $entity->entityid }}'>
                                                                You have not yet uploaded. Please upload.
                                                            </span>
                                                            @if ((Auth::user()->role == 1) && ($entity->status == 0))
                                                                | <a href="#" class="pop-file-uploader" data-header = 'Authorized Signatory' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/authorized_signatory/biz_details") }}'>
                                                                        @if (BIZ_ENTITY_CORP == $entity->entity_type)
                                                                            Upload {{ trans('business_details1.authorized_signatory_board') }}:
                                                                        @else
                                                                            Upload {{ trans('business_details1.authorized_signatory_owner') }}:
                                                                        @endif
                                                                </a>
                                                            @endif
                                                        </span>
                                                    @endif
                                                    <span class="investigator-control" group_name="Business Details" label="Authorized Signatory" value="<a href='{{ URL::to('/') }}/download/documents/{{ $entity->authorized_signatory }}' target='_blank'> Uploaded Authorized Signatory</a>" group="business_details" item="authorized_signatory"></span>
                                                </span>
                                            @endif
                                        @endif
                                        <span class="details-row">
                                          <span class="details-label">{{ trans('business_details1.industry_main') }}: *</span>
                                          <span class="detail-value progress-required">
                                            <span class = 'editable-field' data-name = 'biz_details.industry_main_id' data-field-id = '{{ $entity->entityid }}'>
                                              {{ $entity->main_title }}
                                            </span>
                                          </span>
                                          <span class="investigator-control" group_name="Business Details" label="Industry Main" value="{{ $entity->main_title }}" group="business_details" item="main_title"></span>
                                        </span>
                                        <span class="details-row">
                                          <span class="details-label">{{ trans('business_details1.industry_sub') }}: *</span>
                                          <span class="detail-value progress-field">
                                            <span class = 'industry-sub-title'> {{ $entity->sub_title }} </span>
                                          </span>
                                          <span class="investigator-control" group_name="Business Details" label="Industry Sub" value="{{ $entity->sub_title }}" group="business_details" item="sub_title"></span>
                                        </span>
                                        <span class="details-row">
                                          <span class="details-label">{{ trans('business_details1.industry') }}: *</span>
                                          <span class="detail-value progress-field">
                                            <span class = 'industry-row-title'> {{ $entity->row_title }} </span>
                                          </span>
                                          <span class="investigator-control" group_name="Business Details" label="Industry" value="{{ $entity->row_title }}" group="business_details" item="row_title"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.company_website')}}:</span>
                                                <span class="detail-value progress-field">
                                                    <span class = 'editable-field' data-name = 'biz_details.website' data-field-id = '{{ $entity->entityid }}'>
                                                        {{ $entity->website }}
                                                    </span>
                                                </span>
                                            <span class="investigator-control" group_name="Business Details" label="Website" value="{{ $entity->website }}" group="business_details" item="website"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.company_email')}}: *</span>
                                                <span class="detail-value progress-required">
                                                    <span class = 'editable-field anonymous-info' data-name = 'biz_details.email' data-field-id = '{{ $entity->entityid }}'>
                                                        @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                            Classified Information
                                                        @else
                                                            {{ $entity->email }}
                                                        @endif
                                                    </span>
                                                </span>
                                            <span class="investigator-control" group_name="Business Details" label="Email Address" value="{{ $entity->email }}" group="business_details" item="email"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.primary_business_address') }}: *</span>
                                            <span class="detail-value progress-required">
                                                <span class = 'editable-field anonymous-info' data-name = 'biz_details.address1' data-field-id = '{{ $entity->entityid }}'>
                                                    @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                        Classified Information
                                                    @else
                                                        {{ $entity->address1 }}
                                                    @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="Primary Business Address, Room Building Street" value="{{ $entity->address1 }}" group="business_details" item="address1"></span>
                                        </span>

                                        @if($entity->countrycode == 'PH')

                                        	<span class="details-row">
	                                            <span class="details-label">{{ trans('business_details1.primary_business_province') }}: *</span>
	                                            <span class="detail-value progress-required">
	                                                <span class = 'editable-field' data-name = 'biz_details.province' data-field-id = '{{ $entity->entityid }}'>
	                                                    {{ $entity->province }}
	                                                </span>
	                                            </span>
	                                            <span class="investigator-control" group_name="Business Details" label="Primary Business Address, Province" value="{{ $entity->province }}" group="business_details" item="province"></span>
	                                        </span>
	                                        <span class="details-row">
	                                            <span class="details-label">{{ trans('business_details1.primary_business_city') }}: *</span>
	                                            <span class="detail-value progress-required">
	                                                <span class = 'city-name'>{{ $entity->citymunDesc }}</span>
	                                            </span>
	                                            <span class="investigator-control" group_name="Business Details" label="Primary Business Address, City" value="{{ $entity->citymunDesc }}" group="business_details" item="city"></span>
	                                        </span>
	                                        <span class="details-row">
	                                            <span class="details-label">{{ trans('business_details1.primary_business_zipcode') }}: *</span>
	                                                <span class="detail-value progress-required">
	                                                    <span class = 'biz-zipcode'>{{ $entity->zipcode }}</span>
	                                                </span>
	                                            <span class="investigator-control" group_name="Business Details" label="Primary Business Address, Zipcode" value="{{ $entity->zipcode }}" group="business_details" item="zipcode"></span>
	                                        </span>
                                        @else
                                            <span class="details-row">
                                                <span class="details-label">{{ trans('business_details1.primary_business_zipcode') }}: *</span>
                                                    <span class="detail-value progress-required">
                                                        @if($entity->countrycode == 'PH')
                                                            <span class = 'biz-zipcode'>{{ $entity->zipcode }}</span>
                                                        @else
                                                            <span class = 'biz-zipcode editable-field' data-name = 'biz_details.zipcode' data-field-id = '{{ $entity->entityid }}' >{{ $entity->zipcode }}</span>
                                                        @endif
                                                    </span>
                                                <span class="investigator-control" group_name="Business Details" label="Primary Business Address, Zipcode" value="{{ $entity->zipcode }}" group="business_details" item="zipcode"></span>
                                            </span>
                                        @endif
                                        
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.primary_business_telephone') }}: *</span>
                                                <span class="detail-value progress-required">
                                                    <span class = 'editable-field anonymous-info' data-name = 'biz_details.phone' data-field-id = '{{ $entity->entityid }}'>
                                                        @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                            Classified Information
                                                        @else
                                                            {{ $entity->phone }}
                                                        @endif
                                                    </span>
                                                    <div id="phone_validation" class="text-danger validation_text" hidden> Phone format should be Area Code, and phone number. e.g (02) 1234567 </div> 
                                                </span>
                                            <span class="investigator-control" group_name="Business Details" label="Primary Business Address, Telephone" value="{{ $entity->phone }}" group="business_details" item="phone"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.no_yrs_present_address') }}: </span>
                                            <span class="detail-value progress-required">
                                                <span class = 'editable-field anonymous-info' data-name = 'biz_details.no_yrs_present_address' data-field-id = '{{ $entity->entityid }}'>
                                                    @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                        Classified Information
                                                    @else
                                                        {{ $entity->no_yrs_present_address }}
                                                    @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.no_yrs_present_address',[],'en')}}" value="{{ $entity->no_yrs_present_address }}" group="business_details" item="no_yrs_present_address"></span>
                                        </span>
                                        <!-- <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.date_established')}}: </span>
                                                <span class="detail-value progress-required">
                                                    <span class = 'editable-field date-input anonymous-info' data-name = 'biz_details.date_established' data-field-id = '{{ $entity->entityid }}'>
                                                        @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                            Classified Information
                                                        @else
                                                            @if ('0000-00-00' != $entity->date_established)
                                                                {{ date("F d, Y", strtotime($entity->date_established)) }}
                                                            @endif
                                                        @endif
                                                    </span>
                                                </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.date_established',[],'en')}}" value="{{ $entity->date_established }}" group="business_details" item="date_established"></span>
                                        </span> -->
                                        <div class="hidden">
                                        <span class="details-row">
                                            <span class="details-label">{{ trans('business_details1.no_yrs_in_business') }}:</span>
                                            <span class="detail-value progress-field">
                                                <span class = 'anonymous-info' data-name = 'biz_details.number_year' data-field-id = '{{ $entity->entityid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $entity->number_year }}
                                                @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="Years in Business" value="{{ $entity->number_year }}" group="business_details" item="number_year"></span>
                                        </span>
                                        </div>
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.company_description')}}:</span>
                                            <span class="detail-value progress-field">
                                                <span class = 'editable-field' data-name = 'biz_details.description' data-field-id = '{{ $entity->entityid }}'>
                                                @if (STR_EMPTY != $entity->description)
                                                {{ $entity->description }}
                                                @else
                                                {{ trans('business_details1.company_description_data') }}
                                                @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.company_description',[],'en')}}" value="{{ $entity->description }}" group="business_details" item="description"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.employee_size')}}: *</span>
                                            <span class="detail-value progress-required">
                                                <span class = 'editable-field' data-name = 'biz_details.employee_size' data-field-id = '{{ $entity->entityid }}'>
                                                
                                                @if(empty($entity->employee_size))
													@if ($entity->total_asset_grouping == 4)
													{{trans('business_details1.employee_size_0')}}
													@elseif ($entity->total_asset_grouping == 1)
														{{trans('business_details1.employee_size_1')}}
													@elseif ($entity->total_asset_grouping == 2)
														{{trans('business_details1.employee_size_2')}}
													@else
														{{trans('business_details1.employee_size_3')}}
													@endif
												@else

													@if (1 == $entity->employee_size)
													{{trans('business_details1.employee_size_1')}}
													@elseif (2 == $entity->employee_size)
														{{trans('business_details1.employee_size_2')}}
													@elseif (3 == $entity->employee_size)
														{{trans('business_details1.employee_size_3')}}
													@else
														{{trans('business_details1.employee_size_0')}}
													@endif

												@endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.employee_size',[],'en')}}" value="{{ $entity->employee_size }}" group="business_details" item="employee_size"></span>
                                        </span>
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.updated_date')}}:</span>
                                            <span class="detail-value progress-field">
                                                <span class = 'editable-field date-input' data-name = 'biz_details.updated_date' data-field-id = '{{ $entity->entityid }}'>
                                                @if (STR_EMPTY != $entity->updated_date)
                                                {{ date("F d, Y", strtotime($entity->updated_date)) }}
                                                @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.updated_date',[],'en')}}" value="{{ $entity->updated_date }}" group="business_details" item="updated_date"></span>
                                        </span>
                                        <!-- <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.number_year_management_team')}}: </span>
                                            <span class="detail-value progress-required">
                                                <span class = 'editable-field anonymous-info' data-name = 'biz_details.number_year_management_team' data-field-id = '{{ $entity->entityid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $entity->number_year_management_team }}
                                                @endif
                                                </span>
                                            </span>
                                            <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.number_year_management_team',[],'en')}}" value="{{ $entity->number_year_management_team }}" group="business_details" item="number_year_management_team"></span>
                                        </span> -->
                                        <span class="details-row">
                                            <span class="details-label">{{trans('business_details1.requesting_bank')}}:</span>
                                            <span class="detail-value progress-field">
                                            @if($isClientKey == 1)
                                                <span class = '' data-name = 'biz_details.is_independent' data-field-id = '{{ $entity->entityid }}'>
                                            @else
                                                <span class = '{{Auth::user()->is_contractir != 1 ? "editable-field" : ""}}' data-name = 'biz_details.is_independent' data-field-id = '{{ $entity->entityid }}'>
                                            @endif
                                                    {{ (1 == $entity->is_independent) ? $entity->bank_name : "Applying Independently" }}</span>
                                                </span>
                                                <span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.requesting_bank',[],'en')}}" value="{{ ($entity->bank_name) ? $entity->bank_name : 'Applying Independently' }}" group="business_details" item="bank_name"></span>
                                            </span>
                                            @if ($entity->status == 0 or $entity->status == 3)
                                                <span class="details-row" style="text-align: right;">
                                                    @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                                        <a id = 'edit-biz-details' style = 'display: none;' href="{{ URL::to('/') }}/sme/busdetails/{{ $entity->entityid }}/edit" class="navbar-link">{{trans('business_details1.edit_business_details')}}</a>
                                                    @endif
                                                </span>
                                            @endif
                                        </span>
                                        @if($entity->negative_findings && Auth::user()->role == 4)
                                            <span class="details-row">
                                                <span class="details-label">{{trans('business_details1.negative_findings')}}:</span>
                                                    <span class="detail-value">
                                                        <span class = 'editable-field' data-name = 'biz_details.negative_findings' data-field-id = '{{ $entity->entityid }}'>
                                                            {!! $entity->negative_findings !!}
                                                        </span>
                                                    </span>
                                                <span class="investigator-control" group_name="Business Details" label="Findings" value="{{ $entity->negative_findings }}" group="business_details" item="findings"></span>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="fspdf-upload-cntr" class="spacewrapper print-none">
								<div class="read-only">
									<h4>{{ trans('messages.fs_pdf') }}</h4>
									<div class="read-only-text">
										<p class="col-md-12">{{trans('messages.fspdf_hint')}}</p>

										<div id='fspdf-upload-list-cntr' class="details-row">
											<div class='reload-fspdf-upload'>
											    <span class="progress-required-counter" min-value="1">
													@if (empty($fsPdfFiles))
														0
													@else
														1
													@endif
											    </span>
												@if (!empty($fsPdfFiles))

													<table class="table table-hover table-bordered">
													    <thead>
													    <tr>
															<th scope="col" class="col-md-1 text-center">#</th>
															<th scope="col" class="col-md-7">{{ trans('messages.file_name') }}</th>
															<th scope="col" class="col-md-4 text-center">{{ trans('messages.action') }}</th>
													    </tr>
													    </thead>
													    <tbody>
													    
														@foreach($fsPdfFiles as $key => $filename)
														<tr>
															<th scope="row" class="text-center">{{ $key+1 }}</th>

															<td><a target="_blank">{!! $filename->file_name !!}</a></td>

															<td class="text-center">
																<a title="{{ trans('messages.download')}}" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ URL::to('/') }}/sme/download_fspdfFile/{{ $filename->id }}"><span class="glyphicon glyphicon-download-alt"></span></a>
																@if ($entity->status == 0 or $entity->status == 3)
																<a href="{{ URL::to('/') }}/sme/delete_fspdfFile/{{ $filename->id }}"  title="{{ trans('messages.delete')}}" class="navbar-link delete-file btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-fspdf-upload' data-cntr = '#fspdf-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
															@endif
															</td>
														</tr>
														@endforeach
													    </tbody>
													</table>
												@endif

												<div id = 'expand-fspdf-upload-form' class = 'spacewrapper'></div>

												@if ($entity->status == 0 or $entity->status == 3)
													<span class="details-row" style="text-align: right;">
												@if ($entity->loginid == Auth::user()->loginid  || Auth::user()->role == 0)
													<a id = 'show-fspdf-upload-expand' href="{{ URL::to('/') }}/sme/fspdf_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.upload_file') }}</a>
												@endif
													</span>
												@endif
										  	</div>
										</div>
									</div>
								</div>
							</div>

                        <div id="fs-template-cntr" class="spacewrapper print-none">
                            <div class="read-only">
                                <h4>{{ trans('business_details1.financial_statement_template') }}</h4>
                                <div class="read-only-text">
                                    <div class="col-md-12">
                                        <p style="text-align: justify;">
                                            {{ trans('business_details1.financial_statement_template_hint') }}
                                        </p>
                                        <br />
                                        
                                        <a  class='btn btn-link' id='download_fs_link' href="{{ URL::to('/') }}/getFsTemplateFile/{{ $entity->entityid }}">
                                            {{ trans('business_details1.financial_statement_template_link') }}
                                        </a>
                                    </div>

                                    @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">{{ (@count($fs_template) >= 1) ? "true" : "" }}</div>
                                    @endif

                                    <div id = 'fs-template-list-cntr' class="details-row">
                                        <div class = 'reload-fs-template'>

                                        @if ($doc_req)
                                            @if (STS_NG == $doc_req->fs_template)
                                                <span class="progress-field-counter" min-value="1">
                                            @else
                                                <span class="progress-required-counter" min-value="0">
                                            @endif
                                        @else
                                            <span class="progress-required-counter" min-value="0">
                                        @endif

                                        @if (0 >= @count($fs_template))
                                            0
                                        @else
                                            1
                                        @endif

                                        </span>

                                        @if (@count($fs_template) != 0)
                                            @foreach ($fs_template as $documents2)
                                            <div class="grouping">
                                                <div class="col-md-2">
                                                    <span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents2->documentid }}'>
                                                        @if($documents2->document_type == 1)
                                                            {{trans('business_details1.unaudited')}}
                                                        @elseif($documents2->document_type == 2)
                                                            {{trans('business_details1.interim')}}
                                                        @elseif($documents2->document_type == 4)
                                                            {{trans('business_details1.recasted')}}
                                                        @else
                                                            {{trans('business_details1.audited')}}
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-md-8">
                                                    <a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
                                                </div>
                                                <div class="col-md-2">
                                                    @if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
                                                    @if(@count($fs_template) === 1)
                                                    <input type="hidden" value="1" id="count_fs_template">
                                                    @else
                                                    <input type="hidden" value="2" id="count_fs_template">
                                                    @endif
                                                    <a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents2->documentid }}" class="navbar-link delete-info" data-reload = '.reload-fs-template' data-cntr = '#fs-template-list-cntr'>{{ trans('messages.delete') }}</a>
                                                    @endif
                                                    <span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents2->document_orig }}" group="documents" item="balance_sheet_{{$documents2->documentid}}"></span>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif

                                        <div id = 'expand-fs-template-form' class = 'spacewrapper'></div>
                                        <div id = 'expand-quickbooks-form' class = 'spacewrapper'></div>

                                        @if ($entity->status == 0 or $entity->status == 3)
                                            <span class="details-row" style="text-align: right;">
                                            @if ($entity->loginid == Auth::user()->loginid)
                                                @if (0 >= @count($fs_template))
                                                    <a id = 'show-fs-template-expand' href="{{ URL::to('/') }}/sme/upload_fs_template/{{ $entity->entityid }}" class="navbar-link">Upload {{ trans('business_details1.financial_statement_template') }}</a>
                                                    <a id = 'show-quickbooks-expand' href="{{ URL::to('/') }}/sme/upload_quickbooks/{{ $entity->entityid }}" class="navbar-link">Upload {{ trans('business_details1.quickbooks_reports') }}</a>
                                                @endif
                                            @endif
                                        </span>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="registration2" class="tab-pane fade">
                        <div>
                            <div id='affiliates-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-affiliates">
                                    <span class="progress-required-counter" min-value="1"> {{ count($relatedcompanies) > 0 ? 1 : 0 }} </span>
                                    <h4>{{trans('business_details1.related_company')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.related_company_info')}}</i>">More info...</a></h4>
                                    <div class="read-only-text">
                                        @if(!$relatedcompanies) 
                                        <small><i>Skip if not applicable</i></small>
                                        @endif
                                        
                                        @foreach ($relatedcompanies as $relatedcompanieinfo)
                                            <span class="details-row">
                                                <div class="col-md-3 col-sm-3"><b>{{ trans('messages.reg_name') }}</b></div>
                                                <div class="col-md-9 col-sm-9">
                                                    <span class = 'editable-field' data-name = 'affiliates.related_company_name' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
                                                        {{ $relatedcompanieinfo->related_company_name }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Related Company" label="Name" value="{{ $relatedcompanieinfo->related_company_name }}" group="related_company" item="related_company_name_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
                                                </div>
                                            </span>
                                            <span class="details-row">
                                                <div class="col-md-3 col-sm-3"><b>{{ trans('messages.reg_address') }}</b></div>
                                                <div class="col-md-9 col-sm-9">
                                                    <span class = 'editable-field' data-name = 'affiliates.related_company_address1' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
                                                        {{ $relatedcompanieinfo->related_company_address1 }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Related Company" label="Address 1" value="{{ $relatedcompanieinfo->related_company_address1 }}" group="related_company" item="related_company_address1_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
                                                </div>
                                            </span>     
                                            @if($relatedcompanieinfo->related_company_phone && $relatedcompanieinfo->related_company_phone != ' ')
                                                <span class="details-row">
                                                    <div class="col-md-3 col-sm-3"><b>{{ trans('messages.phone_number') }}</b></div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <span class = 'editable-field' data-name = 'affiliates.related_company_phone' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
                                                            {{ $relatedcompanieinfo->related_company_phone }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Related Company" label="Phone Number" value="{{ $relatedcompanieinfo->related_company_phone }}" group="related_company" item="related_company_phone_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
                                                    </div>
                                                </span>
                                            @endif
                                            @if($relatedcompanieinfo->related_company_email && $relatedcompanieinfo->related_company_email != ' ')
                                                <span class="details-row">
                                                    <div class="col-md-3 col-sm-3"><b>{{ trans('messages.email') }}</b></div>
                                                    <div class="col-md-9 col-sm-9">
                                                        <span class = 'editable-field' data-name = 'affiliates.related_company_email' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
                                                            {{ $relatedcompanieinfo->related_company_email }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Related Company" label="Email" value="{{ $relatedcompanieinfo->related_company_email }}" group="related_company" item="related_company_email_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
                                                    </div>
                                                </span>
                                            @endif
                                            @if ($entity->status == 0 or $entity->status == 3)
                                                <span class="details-row">
                                                    @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                                        <div class="col-md-12">
                                                            <a href="{{ URL::to('/') }}/sme/relatedcompanies/{{ $relatedcompanieinfo->relatedcompaniesid }}/delete" class="navbar-link delete-info" data-reload = '.reload-affiliates' data-cntr = '#affiliates-list-cntr' >{{trans('messages.delete')}}</a>
                                                        </div>
                                                    @endif
                                                </span>
                                            @endif
                                        @endforeach
                                        @if ($entity->status == 0 or $entity->status == 3)
                                            <span class="details-row" style="text-align: right; width: 100%;">
                                                @if ($entity->loginid == Auth::user()->loginid  || Auth::user()->role == 0)
                                                    <a id = 'show-affiliates2-expand' href="{{ URL::to('/') }}/sme/relatedcompaniespremium/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.related_company')}}</a>
                                                @endif
                                            </span>
                                        @endif

                                        <div id = 'expand-affiliates2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='cap-details-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-cap-details">
                                    @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->capital_details)
                                    <span class="progress-required-counter" min-value="5"> {{ count($capital) > 0 ? 5 : 0 }} </span>
                                    @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->capital_details)
                                    <span class="progress-field-counter" min-value="5"> {{ count($capital) > 0 ? 5 : 0 }} </span>
                                    @else

                                    @endif

                                    <h4>{{trans('business_details1.capital_details')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ Lang::get('business_details1.capital_details_hint', array(), 'en') }} <hr/> {{Lang::get('business_details1.capital_details_hint', array(), 'fil')}}">More info...</a></h4>
                                    <div class="read-only-text">
                                    @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                    <div class="completeness_check">{{ count($capital)>0 ? "true" : "" }}</div>
                                    @endif

                                    @foreach ($capital as $capitalinfo)
                                    <span class="details-row">
                                        <div class="col-md-4 col-sm-4"><b>{{trans('business_details1.authorized_capital')}}</b></div>
                                        <div class="col-md-8 col-sm-8">
                                        <span class = 'editable-field' data-name = 'capital_details.capital_authorized' data-field-id = '{{ $capitalinfo->capitalid }}'>
                                            {{ number_format($capitalinfo->capital_authorized, 2) }}
                                        </span>
                                        <span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.authorized_capital',[],'en')}}" value="{{ $capitalinfo->capital_authorized }}" group="capital_details" item="capital_authorized_{{$capitalinfo->capitalid}}"></span>
                                        </div>
                                    </span>
                                    <span class="details-row">
                                    <div class="col-md-4 col-sm-4"><b>{{trans('business_details1.issued_capital')}}</b></div>
                                    <div class="col-md-8 col-sm-8">
                                        <span class = 'editable-field' data-name = 'capital_details.capital_issued' data-field-id = '{{ $capitalinfo->capitalid }}'>
                                        {{ number_format($capitalinfo->capital_issued, 2) }}
                                        </span>
                                        <span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.issued_capital',[],'en')}}" value="{{ $capitalinfo->capital_issued }}" group="capital_details" item="capital_issued_{{$capitalinfo->capitalid}}"></span>
                                    </div>
                                    </span>
                                    <span class="details-row">
                                    <div class="col-md-4 col-sm-4"><b>{{trans('business_details1.paid_up_capital')}}</b></div>
                                    <div class="col-md-8 col-sm-8">
                                    <span class = 'editable-field' data-name = 'capital_details.capital_paid_up' data-field-id = '{{ $capitalinfo->capitalid }}'>
                                        {{ number_format($capitalinfo->capital_paid_up, 2) }}
                                    </span>
                                    <span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.paid_up_capital',[],'en')}}" value="{{ $capitalinfo->capital_paid_up }}" group="capital_details" item="capital_paid_up_{{$capitalinfo->capitalid}}"></span>
                                    </div>
                                    </span>
                                    <span class="details-row">
                                    <div class="col-md-4 col-sm-4"><b>{{trans('business_details1.ordinary_shares')}}</b></div>
                                    <div class="col-md-8 col-sm-8">
                                        <span class = 'editable-field' data-name = 'capital_details.capital_ordinary_shares' data-field-id = '{{ $capitalinfo->capitalid }}'>
                                        {{ number_format($capitalinfo->capital_ordinary_shares, 2) }}
                                        </span>
                                        <span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.ordinary_shares',[],'en')}}" value="{{ $capitalinfo->capital_ordinary_shares }}" group="capital_details" item="capital_ordinary_shares_{{$capitalinfo->capitalid}}"></span>
                                    </div>
                                    </span>
                                    <span class="details-row">
                                    <div class="col-md-4 col-sm-4"><b>{{trans('business_details1.par_value')}}</b></div>
                                    <div class="col-md-8 col-sm-8">
                                    <span class = 'editable-field' data-name = 'capital_details.capital_par_value' data-field-id = '{{ $capitalinfo->capitalid }}'>
                                        {{ number_format($capitalinfo->capital_par_value, 2) }}
                                    </span>
                                    <span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.par_value',[],'en')}}" value="{{ $capitalinfo->capital_par_value }}" group="capital_details" item="capital_par_value_{{$capitalinfo->capitalid}}"></span>
                                    </div>
                                    </span>
                                    @endforeach
                                    <small><i>Par Value is the total par values of Issued Capital, Paid Up Capital and Ordinary Shares</i></small><br>
                                    @if ($entity->status == 0 or $entity->status == 3)
                                    <span class="details-row" style="text-align: right; width: 100%;">
                                    @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                    @if (count($capital) == 0)
                                    <a id = 'show-cap-details2-expand' href="{{ URL::to('/') }}/sme/capitalpremium/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.capital_details')}}</a>
                                    @else
                                    <a href="{{ URL::to('/') }}/sme/capital/{{ $capitalinfo->capitalid }}/delete" class="navbar-link delete-info" data-reload = '.reload-cap-details' data-cntr = '#cap-details-list-cntr'>{{trans('messages.delete')}} </a>
                                    @endif
                                    @endif
                                    </span>
                                    @endif

                                    <div id = 'expand-cap-details2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='main-loc-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-main-loc">
                                    <h4>{{trans('business_details1.main_locations')}}</h4>
                                    <div class="read-only-text">
                                    <span class="details-row hidden-xs hidden-sm">
                                        <div class="col-md-3 col-sm-3"><b>{{trans('business_details1.size_of_premises')}}</b></div>
                                        <div class="col-md-3 col-sm-3"><b>{{trans('business_details1.premises_owned_leased')}}</b></div>
                                        <div class="col-md-3 col-sm-3"><b>{{trans('business_details1.location')}}</b></div>
                                        <div class="col-md-3 col-sm-3"><b>{{trans('business_details1.premise_used_as')}}</b></div>
                                    </span>

                                    @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                    <div class="completeness_check">{{ count($slocation)>0 ? "true" : "" }}</div>
                                    @endif

                                    @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->main_locations)
                                    <span class="progress-required-counter" min-value="4"> {{ count($slocation) > 0 ? 4 : 0 }} </span>
                                    @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->main_locations)
                                    <span class="progress-field-counter" min-value="4">{{ count($slocation)>0 ? 4 : 0 }}</span>
                                    @else

                                    @endif

                                    @foreach ($slocation as $slocationinfo)
                                    <div class="details-row">
                                        <div class="col-md-3">
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <span class="hidden-xs hidden-sm">
                                            <a href="{{ URL::to('/') }}/sme/locations/{{ $slocationinfo->locationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-main-loc' data-cntr = '#main-loc-list-cntr'>{{trans('messages.delete')}}</a>
                                        </span>
                                        @endif
                                        @endif
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.size_of_premises')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'main_loc.location_size' data-field-id = '{{ $slocationinfo->locationid }}'>
                                            {{ $slocationinfo->location_size }} sqm
                                        </span>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.premises_owned_leased')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'main_loc.location_type' data-field-id = '{{ $slocationinfo->locationid }}'>
                                            {{ LanguageHelper::convert('business_details1', $slocationinfo->location_type) }}
                                        </span>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.location')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'main_loc.location_map' data-field-id = '{{ $slocationinfo->locationid }}'>
                                            {{ LanguageHelper::convert('business_details1', $slocationinfo->location_map) }}
                                        </span>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.premise_used_as')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'main_loc.location_used' data-field-id = '{{ $slocationinfo->locationid }}'>
                                            {{ LanguageHelper::convert('business_details1', $slocationinfo->location_used) }}
                                        </span>
                                        <span class="investigator-control" group_name="Main Location/s" label="{{trans('business_details1.size_of_premises',[],'en')}}" value="{{ $slocationinfo->location_size }}" group="capital_details" group="main_location" item="location_{{$slocationinfo->locationid}}"></span>
                                        </div>
                                        <div class="col-xs-12 visible-xs-block visible-sm-block">
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <a href="{{ URL::to('/') }}/sme/locations/{{ $slocationinfo->locationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-main-loc' data-cntr = '#main-loc-list-cntr'>{{trans('messages.delete')}}</a>
                                        @endif
                                        @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    {{-- @if ($entity->status == 0 or $entity->status == 3) --}}
                                    <span class="details-row" style="text-align: right; width: 100%;">
                                    {{-- @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && !$slocation ) --}}

                                    @if($entity->status != 7)
                                    	<a id = 'show-main-loc2-expand' href="{{ URL::to('/') }}/sme/locationsPremium/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.main_locations')}}</a>
                                    @endif

                                    {{-- @endif --}}
                                    </span>
                                    {{-- @endif --}}

                                    <div id = 'expand-main-loc2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='certifications-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-certifications">
                                    <h4>{{trans('business_details1.certifications')}}</h4>
                                    <div class="read-only-text">
                                        <span class="details-row hidden-xs hidden-sm">
                                        <div class="col-md-6 col-sm-6"><b></b></div>
                                        <div class="col-md-2 col-sm-2"><b>{{ trans('messages.registration_num') }}</b></div>
                                        <div class="col-md-2 col-sm-2"><b>{{ trans('messages.registration_date') }}</b></div>
                                        <div class="col-md-2 col-sm-2 print-none"><b>Uploaded Document</b></div>
                                        </span>

                                        @if ($doc_req)
                                        @if ((STS_OK == $doc_req->boi) || (STS_OK == $doc_req->peza))
                                        <span class="progress-required-counter" min-value="1">
                                        @else
                                        <span class="progress-field-counter" min-value="1">
                                            @endif
                                            @else
                                            <span class="progress-field-counter" min-value="1">
                                            @endif

                                            @if (($boi_cert) || ($peza_cert))
                                            1
                                            @else
                                            0
                                            @endif
                                            </span>

                                            @foreach ($certifications as $certificationinfo)
                                            <span class="details-row">
                                            <div class="col-md-6 col-sm-6">
                                            @if ($entity->status == 0 or $entity->status == 3)
                                            @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                            <a href="{{ URL::to('/') }}/sme/certification/{{ $certificationinfo->certificationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-certifications' data-cntr = '#certifications-list-cntr'>{{trans('messages.delete')}}</a>
                                            @endif
                                            @endif
                                            @if (($certificationinfo->certification_details != 'Board of Investments (BOI)') || ($certificationinfo->certification_details != 'Philippine Export Processing Zone Authority (PEZA)'))
                                            <span class = 'editable-field' data-name = 'certifications.certification_details' data-field-id = '{{ $certificationinfo->certificationid }}'>
                                                {{ $certificationinfo->certification_details }}
                                            </span>
                                            @else
                                            {{ $certificationinfo->certification_details }}
                                            @endif
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                            <b class="visible-xs-block visible-sm-block"> {{ trans('messages.registration_num') }} </b>
                                            <span class = 'editable-field' data-name = 'certifications.certification_reg_no' data-field-id = '{{ $certificationinfo->certificationid }}'>
                                                {{ $certificationinfo->certification_reg_no }}
                                            </span>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                            <b class="visible-xs-block visible-sm-block"> {{ trans('messages.registration_date') }} </b>
                                            <span class = 'editable-field date-input' data-name = 'certifications.certification_reg_date' data-field-id = '{{ $certificationinfo->certificationid }}'>
                                                {{ date("F d, Y", strtotime($certificationinfo->certification_reg_date)) }}
                                            </span>
                                            </div>
                                            @if($certificationinfo->certification_doc)
                                                <div class="col-md-2 col-sm-2 print-none">
                                                <b class="visible-xs-block visible-sm-block"> Uploaded Document </b>
                                                    <span id = 'certification-docs{{ $certificationinfo->certificationid }}'>
                                                        <a href="{{ URL::to('/') }}/download/certification-docs/{{ $certificationinfo->certification_doc }}" target="_blank"> Download </a>
                                                    </span>
                                                    @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                                    | <a href="#" class="pop-file-uploader" data-header = 'Certification' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$certificationinfo->certificationid."/certification_doc/certifications") }}'> Upload </a>
                                                    @endif
                                                </div>
                                            @else
                                                <div class="col-md-2 col-sm-2 print-none">
                                                    <b class="visible-xs-block visible-sm-block"> Uploaded Document </b>
                                                    
                                                    @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                                    <a href="#" class="pop-file-uploader" data-header = 'Certification' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$certificationinfo->certificationid."/certification_doc/certifications") }}'> Upload </a>
                                                    @endif
                                                </div>
                                            @endif
                                            <span class="investigator-control" group_name="Certifications, Accreditations, and Tax Exemptions" label="Certifications, Accreditations, and Tax Exemptions" value="{{ $certificationinfo->certification_details }}" group="certifications" item="certification_details_{{$certificationinfo->certificationid}}"></span>
                                        </span>
                                        @endforeach
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <span class="details-row" style="text-align: right; width: 100%;">
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <a id = 'show-certifications2-expand' href="{{ URL::to('/') }}/sme/certification/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.certifications')}}</a>
                                        @endif
                                        </span>
                                        @endif

                                        <div id = 'expand-certifications2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='insurance-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-insurance">
                                    <h4>{{trans('business_details1.insurance_coverage')}} <a href="#" class="popup-hint popup-hint-header" data-hint=" {{trans('business_details1.insurance_coverage_hint')}}">More info...</a></h4>
                                    <div class="read-only-text">
                                        @if ($insuarance)
                                        <span class="details-row hidden-xs hidden-sm"><div class="col-md-6 col-sm-6"><b>{{trans('business_details1.insurance_type')}}</b></div><div class="col-md-6 col-sm-6"><b>{{trans('business_details1.insured_amount')}}</b></div></span>
                                        @endif

                                        @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">{{ count($insuarance)>0 ? "true" : "" }}</div>
                                        @endif

                                        @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->insurance)
                                        <span class="progress-required-counter" min-value="2"> {{ count($insuarance) > 0 ? 2 : 0 }} </span>
                                        @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->insurance)
                                        <span class="progress-field-counter" min-value="2"> {{ count($insuarance) > 0 ? 2 : 0 }} </span>
                                        @else

                                        @endif

                                        @foreach ($insuarance as $insuaranceinfo)
                                        <span class="details-row">
                                        <div class="col-md-6 col-sm-6">
                                            @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                            @if ($entity->status == 0 or $entity->status == 3)
                                            <span class="hidden-xs hidden-sm">
                                            <a href="{{ URL::to('/') }}/sme/insurance/{{ $insuaranceinfo->insuranceid }}/delete" class="navbar-link delete-info" data-reload = '.reload-insurance' data-cntr = '#insurance-list-cntr'>{{trans('messages.delete')}}</a>
                                        </span>
                                        @endif
                                        @endif
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.insurance_type')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'insurance.insurance_type' data-field-id = '{{ $insuaranceinfo->insuranceid }}'>
                                            {{ $insuaranceinfo->insurance_type }}
                                        </span>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                        <b class="visible-xs-block visible-sm-block">{{trans('business_details1.insured_amount')}}<br /></b>
                                        <span class = 'editable-field' data-name = 'insurance.insured_amount' data-field-id = '{{ $insuaranceinfo->insuranceid }}'>
                                            {{ number_format($insuaranceinfo->insured_amount, 2) }}
                                        </span>
                                        <span class="investigator-control" group_name="Insurance Coverage" label="Insurance Type" value="{{ $insuaranceinfo->insurance_type }}" group="insurance_coverage" item="insurance_{{$insuaranceinfo->insuranceid}}"></span>
                                        </div>
                                        <div class="col-xs-12 visible-xs-block visible-sm-block">
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <a href="{{ URL::to('/') }}/sme/insurance/{{ $insuaranceinfo->insuranceid }}/delete" class="navbar-link delete-info" data-reload = '.reload-insurance' data-cntr = '#insurance-list-cntr'>{{trans('messages.delete')}}</a>
                                        @endif
                                        @endif
                                    </div>
                                    </span>
                                    @endforeach
                                    @if ($entity->status == 0 or $entity->status == 3)
                                    <span class="details-row" style="text-align: right; width: 100%;">
                                    @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                    <a id = 'show-insurance2-expand' href="{{ URL::to('/') }}/sme/insurance/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.insurance_coverage')}}</a>
                                    @endif
                                    </span>
                                    @endif
                                    <div id = 'expand-insurance2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='major-cust-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-major-cust">
                                    <h4>{{trans('business_details2.major_customer')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.major_customer_hint_premium')}}">More info...</a></h4>
                                    <div class="read-only-text">
                                        <div class="majorcustomer">
                                            @foreach ($majorcustomers as $customer)
                                            <input type="hidden" value="{{ $customer->customer_share_sales }}" name="totalsalevalue" class="totalsalevalue">
                                            <input type="hidden" value="{{ $customer->customer_experience }}" name="cexperience" class="texperience">

                                            <div class="details-row">
                                            <div class="col-md-6 col-sm-6">
                                                <b>{{trans('messages.name')}}</b>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field anonymous-info' data-name = 'major_cust.customer_name' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $customer->customer_name }}
                                                @endif
                                                </span>
                                                <span class="investigator-control" group_name="Major Customer/s" label = 'Name' group="major_customer" value="{{ $customer->customer_name }}" item="customer_name_{{ $customer->majorcustomerrid }}"></span>
                                            </div>
                                            </div>

                                            <div class="details-row">
                                            <div class="col-md-6 col-sm-6"><b>Address</b></div>
                                            <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_cust.customer_address' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                {{ $customer->customer_address }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Customer/s" label = 'Address' group="major_customer" value="{{ $customer->customer_address }}" item="customer_address_{{ $customer->majorcustomerrid }}"></span>
                                            </div>
                                            </div>

                                            @if($customer->customer_contact_person)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <b>{{trans('messages.contact_person')}}</b>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_contact_person' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ $customer->customer_contact_person }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Contact Person' group="major_customer" value="{{ $customer->customer_contact_person }}" item="customer_contact_person_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_phone)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_phone')}}</b></div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_phone' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ $customer->customer_phone }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Contact Phone Number' group="major_customer" value="{{ $customer->customer_phone }}" item="customer_phone_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_email )
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_email')}}</b></div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_email' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ $customer->customer_email }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Contact Email' group="major_customer" value="{{ $customer->customer_email }}" item="customer_email_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_started_years )
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_year_started') }}</b></div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_started_years' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ $customer->customer_started_years }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Year Started Doing Business With' group="major_customer" value="{{ $customer->customer_started_years }}" item="customer_started_years_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_years_doing_business)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_year_doing_business') }}</b></div>
                                                    <div class="col-md-6 col-sm-6"> <span id = '{{ $customer->majorcustomerrid }}-auto-calc-years'> {{ $customer->customer_years_doing_business }} </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Years Doing Business With' group="major_customer" value="{{ $customer->customer_years_doing_business }}" item="customer_years_doing_business_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->relationship_satisfaction)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <b>{{ trans('business_details2.mc_relationship_satisfaction') }}</b>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class="editable-field"
                                                        data-name="major_cust.relationship_satisfaction"
                                                        data-field-id="{{ $customer->majorcustomerrid }}">
                                                        @if ($customer->relationship_satisfaction == 1)
                                                            {{ trans('business_details2.mc_relation_good') }}
                                                        @elseif ($customer->relationship_satisfaction == 2)
                                                            {{ trans('business_details2.mc_relation_satisfactory') }}
                                                        @elseif ($customer->relationship_satisfaction == 3)
                                                            {{ trans('business_details2.mc_relation_unsatisfactory') }}
                                                        @elseif ($customer->relationship_satisfaction == 4)
                                                            {{ trans('business_details2.mc_relation_bad') }}
                                                        @else
                                                            {{ trans('business_details2.mc_relation_not_set') }}
                                                        @endif
                                                        </span>
                                                        <span
                                                        class="investigator-control"
                                                        group_name="Major Customer/s"
                                                        label="{{ trans('mc_relationship_satisfaction') }}"
                                                        group="major_customer"
                                                        value="{{ $customer->relationship_satisfaction }}"
                                                        item="typename_{{ $customer->majorcustomerrid }}">
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_settlement)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_payment_behavior') }}</b></div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_settlement' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        @if ($customer->customer_settlement == 1)
                                                        {{ trans('business_details2.mc_ahead') }}
                                                        @elseif ($customer->customer_settlement == 2)
                                                        {{ trans('business_details2.mc_ondue') }}
                                                        @elseif ($customer->customer_settlement == 3)
                                                        {{ trans('business_details2.mc_portion') }}
                                                        @elseif ($customer->customer_settlement == 4)
                                                        {{ trans('business_details2.mc_beyond') }}
                                                        @else
                                                        {{ trans('business_details2.mc_delinquent') }}
                                                        @endif
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Customer payment behavior in last 3 payments' group="major_customer" value="{{ $customer->customer_settlement }}" item="typename_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            @if($customer->customer_share_sales)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <b>{{ trans('business_details2.mc_percent_share') }}</b>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_share_sales' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ number_format($customer->customer_share_sales, 2) }}%
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = '% Share of Your Sales to this Company vs. Your Total Sales' group="major_customer" value="{{ $customer->customer_share_sales }}" item="customer_share_sales_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($customer->customer_order_frequency)
                                                <div class="details-row">
                                                    <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_order_frequency') }}</b></div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <span class = 'editable-field' data-name = 'major_cust.customer_order_frequency' data-field-id = '{{ $customer->majorcustomerrid }}'>
                                                        {{ trans('business_details2.'.$customer->customer_order_frequency) }}
                                                        </span>
                                                        <span class="investigator-control" group_name="Major Customer/s" label = 'Order Frequency' group="major_customer" value="{{ $customer->customer_order_frequency }}" item="customer_order_frequency_{{ $customer->majorcustomerrid }}"></span>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="details-row">
                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                            <div class="col-md-12">
                                                <a href = "{{ URL::To('/sme/majorcustomer/'.$customer->majorcustomerrid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-cust' data-cntr = '#major-cust-list-cntr'>{{ trans('messages.delete') }}</a>
                                            </div>
                                            @endif
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="details-row no-major-cust-cntr" style="text-align: left; width: 100%;">
                                            <div class = 'reload-no-major-cust'>
                                            @if (isset($majorcustomeraccount[0]->is_agree))
                                            {{ Form::hidden('is_agree', $majorcustomeraccount[0]->is_agree, array('id' => 'is_agree')) }}
                                            @if (1 == $majorcustomeraccount[0]->is_agree)
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">true</div>
                                            @endif
                                            <span class="progress-required-counter" min-value="8">8</span>
                                            <label>
                                                <input class = 'no-major-customer' type="checkbox" value="1" checked = "checked" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
                                            </label>
                                            @else
                                            @if($bank_standalone==0)
                                            @if (0 == $progress['majorcustomer'])
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">false</div>
                                            @endif
                                            <span class="progress-required-counter" min-value="8">0</span>
                                            @else
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">true</div>
                                            @endif
                                            <span class="progress-required-counter" min-value="8">8</span>
                                            @endif
                                            @else
                                            <div class="completeness_check">true</div>
                                            <span class="progress-required-counter" min-value="8">8</span>
                                            @endif
                                            <label>
                                                <input class = 'no-major-customer' type="checkbox" value="1" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
                                            </label>
                                            @endif
                                            @else
                                            {{ Form::hidden('is_agree', 0, array('id' => 'is_agree')) }}
                                            @if($bank_standalone==0)
                                            @if (0 == $progress['majorcustomer'])
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">false</div>
                                            @endif
                                            <span class="progress-required-counter" min-value="8">0</span>
                                            @else
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">true</div>
                                            @endif
                                            <span class="progress-required-counter" min-value="8">8</span>
                                            @endif
                                            @else
                                            <div class="completeness_check">true</div>
                                            <span class="progress-required-counter" min-value="8">8</span>
                                            @endif
                                            <label>
                                                <input class = 'no-major-customer' type="checkbox" value="1" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
                                            </label>
                                            @endif
                                            <span class="investigator-control" group_name="Major Customer/s" label="No Individual Customer" value="No Individual Customer" group="major_customer" item="is_agree"></span>
                                            </div>
                                        </div>
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <span class="details-row" style="text-align: right; width: 100%;">
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <a id = 'show-major-cust-expand' href="{{ URL::to('/') }}/sme/majorcustomer/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_customer')}}</a>
                                        @endif
                                        </span>
                                        @endif
                                        <div id = 'expand-major-cust-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='major-supp-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-major-supp">
                                    <h4>{{trans('business_details2.major_supplier')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.major_supplier_hint_premium')}}">More info...</a></h4>
                                    <div class="read-only-text">
                                        <div class="majorsupplier">
                                        @foreach($majorsuppliers as $supplier)
                                        <input type="hidden" value="{{ $supplier->supplier_share_sales }}" name="totalsuppliersalevalue" class="totalsuppliersalevalue">
                                        <input type="hidden" value="{{ $supplier->supplier_experience }}" name="sexperience" class="texperience">

                                        <div class="details-row">
                                            <div class="col-md-6 col-sm-6"><b>{{trans('messages.name')}}</b></div>
                                            <div class="col-md-6 col-sm-6">
                                            <span class = 'editable-field anonymous-info' data-name = 'major_supp.supplier_name' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
                                                Classified Information
                                                @else
                                                {{ $supplier->supplier_name }}
                                                @endif
                                            </span>
                                            <span class="investigator-control" group_name="Major Supplier/s" label = 'Name' group="major_supplier" value="{{ $supplier->supplier_name }}" item="supplier_name_{{ $supplier->majorsupplierid }}"></span>
                                            </div>
                                        </div>

                                        <div class="details-row">
                                            <div class="col-md-6 col-sm-6"><b>Address</b></div>
                                            <div class="col-md-6 col-sm-6">
                                            <span class = 'editable-field' data-name = 'major_supp.supplier_address' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                {{ $supplier->supplier_address }}
                                            </span>
                                            <span class="investigator-control" group_name="Major Supplier/s" label = 'Address' group="major_supplier" value="{{ $supplier->supplier_address }}" item="supplier_address_{{ $supplier->majorsupplierid }}"></span>
                                            </div>
                                        </div>

                                        @if($supplier->supplier_contact_person)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_person')}}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_contact_person' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->supplier_contact_person }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Person' group="major_supplier" value="{{ $supplier->supplier_contact_person }}" item="supplier_contact_person_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_phone)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_phone')}}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_phone' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->supplier_phone }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Phone Number' group="major_supplier" value="{{ $supplier->supplier_phone }}" item="supplier_phone_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_email)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_email')}}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_email' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->supplier_email }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Email' group="major_supplier" value="{{ $supplier->supplier_email }}" item="supplier_email_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif


                                        @if($supplier->supplier_started_years )
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_year_started') }}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_started_years' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->supplier_started_years }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Year Started Doing Business With' group="major_supplier" value="{{ $supplier->supplier_started_years }}" item="supplier_started_years_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_years_doing_business)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_year_doing_business') }}</b></div>
                                                <div class="col-md-6 col-sm-6"> <span id = '{{ $supplier->majorsupplierid }}-auto-calc-years'> {{ $supplier->supplier_years_doing_business }} </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Years Doing Business With' group="major_supplier" value="{{ $supplier->supplier_years_doing_business }}" item="supplier_years_doing_business_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->relationship_satisfaction)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6">
                                                    <b>{{ trans('business_details2.ms_relationship_satisfaction') }}</b>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <span class="editable-field"
                                                    data-name="major_supp.relationship_satisfaction"
                                                    data-field-id="{{ $supplier->majorsupplierid }}">
                                                    @if ($supplier->relationship_satisfaction == 1)
                                                        {{ trans('business_details2.ms_relation_good') }}
                                                    @elseif ($supplier->relationship_satisfaction == 2)
                                                        {{ trans('business_details2.ms_relation_satisfactory') }}
                                                    @elseif ($supplier->relationship_satisfaction == 3)
                                                        {{ trans('business_details2.ms_relation_unsatisfactory') }}
                                                    @elseif ($supplier->relationship_satisfaction == 4)
                                                        {{ trans('business_details2.ms_relation_bad') }}
                                                    @else
                                                        {{ trans('business_details2.ms_relation_not_set') }}
                                                    @endif
                                                    </span>
                                                    <span
                                                    class="investigator-control"
                                                    group_name="Major Supplier/s"
                                                    label="{{ trans('ms_relationship_satisfaction') }}"
                                                    group="major_supplier"
                                                    value="{{ $supplier->relationship_satisfaction }}"
                                                    item="typename_{{ $supplier->majorsupplierid }}">
                                                    </span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_settlement)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_payment') }}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_settlement' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    @if ($supplier->supplier_settlement == 1)
                                                    {{ trans('business_details2.ms_ahead') }}
                                                    @elseif ($supplier->supplier_settlement == 2)
                                                    {{ trans('business_details2.ms_ondue') }}
                                                    @elseif ($supplier->supplier_settlement == 3)
                                                    {{ trans('business_details2.ms_portion') }}
                                                    @elseif ($supplier->supplier_settlement == 4)
                                                    {{ trans('business_details2.ms_beyond') }}
                                                    @else
                                                    {{ trans('business_details2.ms_delinquent') }}
                                                    @endif
                                                </span>

                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Your payment to supplier in last 3 payments' group="major_supplier" value="{{ $supplier->supplier_settlement }}" item="typename_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_share_sales)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_percentage_share') }}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_share_sales' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ number_format($supplier->supplier_share_sales, 2) }}%
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = '% Share of Total Purchases' group="major_supplier" value="{{ $supplier->supplier_share_sales }}" item="supplier_share_sales_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->supplier_order_frequency)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_frequency') }}</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                <span class = 'editable-field' data-name = 'major_supp.supplier_order_frequency' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ trans('business_details2.'.$supplier->supplier_order_frequency) }}
                                                </span>
                                                <span class="investigator-control" group_name="Major Supplier/s" label = 'Payment Frequency' group="major_supplier" value="{{ $supplier->supplier_order_frequency }}" item="supplier_order_frequency_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->average_monthly_volume)
                                            <div class="details-row">
                                                <div class="col-md-6 col-sm-6"><b>Average Monthly Volume</b></div>
                                                <div class="col-md-6 col-sm-6">
                                                    <span class = 'editable-field' data-name = 'major_supp.average_monthly_volume' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->average_monthly_volume }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Major Supplier/s" label = 'Average Monthly Volume' group="major_supplier" value="{{ $supplier->average_monthly_volume }}" item="average_monthly_volume_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        @if($supplier->item_1 || $supplier->item_2 || $supplier->item_3)
                                            <div class="details-row">
                                                <div class="col-md-12"><b>Items Purchased (List of three distinct items)</b></div>
                                                <div class="col-md-12">
                                                    <span class = 'editable-field' data-name = 'major_supp.item_1' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->item_1 }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 1' group="major_supplier" value="{{ $supplier->item_1 }}" item="item_1_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class = 'editable-field' data-name = 'major_supp.item_2' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->item_2 }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 2' group="major_supplier" value="{{ $supplier->item_2 }}" item="item_2_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class = 'editable-field' data-name = 'major_supp.item_3' data-field-id = '{{ $supplier->majorsupplierid }}'>
                                                    {{ $supplier->item_3 }}
                                                    </span>
                                                    <span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 3' group="major_supplier" value="{{ $supplier->item_3 }}" item="item_3_{{ $supplier->majorsupplierid }}"></span>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="details-row">
                                        @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                        <div class="col-md-12">
                                            <a href = "{{ URL::To('/sme/majorsupplier/'.$supplier->majorsupplierid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-supp' data-cntr = '#major-supp-list-cntr'>{{ trans('messages.delete') }}</a>
                                        </div>
                                        @endif
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="details-row no-major-supp-cntr" style="text-align: left; width: 100%;">
                                        <div class = 'reload-no-major-supp'>
                                        @if($bank_standalone==0)
                                        @if (0 == $progress['majorsupplier'])
                                        @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">false</div>
                                        @endif
                                        <span class="progress-required-counter" min-value="8">0</span>
                                        @else
                                        @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">true</div>
                                        @endif
                                        <span class="progress-required-counter" min-value="8">8</span>
                                        @endif
                                        @else
                                        <div class="completeness_check">true</div>
                                        <span class="progress-required-counter" min-value="8">8</span>
                                        @endif
                                    </div>
                                    </div>
                                    @if ($entity->status == 0 or $entity->status == 3)
                                    <span class="details-row" style="text-align: right;">
                                    @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                    <a id = 'show-major-supp-expand' href="{{ URL::to('/') }}/sme/majorsupplier/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_supplier')}}</a>
                                    @endif
                                    </span>
                                    @endif

                                    <div id = 'expand-major-supp-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='major-loc-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-major-loc">
                                    <h4>{{trans('business_details2.major_market_location')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ Lang::get('business_details2.major_market_location_hint', array(), 'en') }}<hr/>{{ Lang::get('business_details2.major_market_location_hint', array(), 'fil') }}">More info...</a></h4>
                                    <div class="read-only-text">
                                        @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">{{ $progress['majormarketlocation']==1 ? "true" : "" }}</div>
                                        @endif

                                        @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->major_market)
                                        <span class="progress-required-counter" min-value="1"> {{ count($majorlocations) > 0 ? 1 : 0 }} </span>
                                        @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->major_market)
                                        <span class="progress-field-counter" min-value="1"> {{ count($majorlocations) > 0 ? 1 : 0 }} </span>
                                        @else

                                        @endif


                                        <div class="majorlocation">
                                        @foreach ($majorlocations as $major_loc)
                                        <div class="details-row">
                                            <div class="col-md-12">
                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                            <a href = "{{ URL::To('/sme/majorlocation/'.$major_loc->marketlocationid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-loc' data-cntr = '#major-loc-list-cntr'>{{ trans('messages.delete') }}</a>
                                            @endif
                                            <span class = 'editable-field' data-name = 'major_loc.marketlocation' data-field-id = '{{ $major_loc->marketlocationid }}'>
                                                {{ $major_loc->citymunDesc }}
                                            </span>
                                            <span class="investigator-control" group_name="Major Market Location"  label="Major Market Location" value="{{ $major_loc->citymunDesc }}" group="major_market_location" item="location_{{ $major_loc->marketlocationid }}"></span>
                                            </div>
                                        </div>
                                        @endforeach
                                        </div>
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <span class="details-row" style="text-align: right;">
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <a id = 'show-major-loc-expand' href="{{ URL::to('/') }}/sme/majorlocation/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_market_location')}}</a>
                                        @endif
                                    </span>
                                    @endif

                                    <div id = 'expand-major-loc-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='competitor-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-competitor">
                                    <h4>{{trans('business_details2.competitors')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ Lang::get('business_details2.competitors_hint', array(), 'en') }}<hr/>{{ Lang::get('business_details2.competitors_hint', array(), 'fil') }}">More info...</a></h4>
                                    <div class="read-only-text">
                                        @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                        <div class="completeness_check">{{ $progress['competitor']==1 ? "true" : "" }}</div>
                                        @endif

                                        @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->competitors)
                                        <span class="progress-required-counter" min-value="3"> {{ count($competitors) > 0 ? 3 : 0 }} </span>
                                        @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->competitors)
                                        <span class="progress-field-counter" min-value="3"> {{ count($competitors) > 0 ? 3 : 0 }} </span>
                                        @else

                                        @endif

                                        <div class="competitor">
                                        <div class="details-row hidden-xs hidden-sm">
                                            <div class="col-md-3 col-sm-3"><b>Name</b></div>
                                            <div class="col-md-3 col-sm-3"><b>Address</b></div>
                                            <div class="col-md-3 col-sm-3"><b> Contact Phone Number</b></div>
                                            <div class="col-md-3 col-sm-3"><b> Contact Email</b></div>
                                        </div>

                                        @foreach ($competitors as $competitor)
                                        <div class="details-row hidden-xs hidden-sm">
                                            <div class="col-md-3 col-sm-3">
                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                            <a href = "{{ URL::To('/sme/competitor/'.$competitor->competitorid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-competitor' data-cntr = '#competitor-list-cntr'>{{ trans('messages.delete') }}</a>
                                            @endif
                                            <span class = 'editable-field' data-name = 'competitor.competitor_name' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_name }}
                                            </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                            <span class = 'editable-field' data-name = 'competitor.competitor_address' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_address }}
                                            </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                            <span class = 'editable-field' data-name = 'competitor.competitor_phone' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_phone }}
                                            </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                            <span class = 'editable-field' data-name = 'competitor.competitor_email' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_email }}
                                            </span>
                                            <span class="investigator-control" group_name="Competitors"  label="Name" value="{{ $competitor->competitor_name }}" group="competitor" item="competitor_{{ $competitor->competitorid }}"></span>
                                            </div>
                                        </div>

                                        <div class="details-row visible-sm-block visible-xs-block">
                                            <div class="col-md-3 col-sm-3">
                                            <b class="visible-sm-block visible-xs-block">{{trans('messages.name')}}<br /></b>
                                            <span class = 'editable-field' data-name = 'competitor.competitor_name' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_name }}
                                            </span>
                                            </div>

                                            <div class="col-md-3 col-sm-3">
                                            <b class="visible-sm-block visible-xs-block">Address<br /></b>
                                            <span class = 'editable-field' data-name = 'competitor.competitor_address' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_address }}
                                            </span>
                                            </div>

                                            <div class="col-md-3 col-sm-3">
                                            <b class="visible-sm-block visible-xs-block">{{trans('messages.contact_phone')}}<br /></b>
                                            <span class = 'editable-field' data-name = 'competitor.competitor_phone' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_phone }}
                                            </span>
                                            </div>

                                            <div class="col-md-3 col-sm-3">
                                            <b class="visible-sm-block visible-xs-block">{{trans('messages.contact_email')}}<br /></b>
                                            <span class = 'editable-field' data-name = 'competitor.competitor_email' data-field-id = '{{ $competitor->competitorid }}'>
                                                {{ $competitor->competitor_email }}
                                            </span>
                                            <span class="investigator-control" group_name="Competitors"  label="Name" value="{{ $competitor->competitor_name }}" group="competitor" item="competitor_{{ $competitor->competitorid }}"></span>
                                            </div>

                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                            <div class="col-xs-12 visible-xs-block visible-sm-block">
                                            <a href = "{{ URL::To('/sme/competitor/'.$competitor->competitorid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-competitor' data-cntr = '#competitor-list-cntr'>{{ trans('messages.delete') }}</a>
                                            </div>
                                            @endif

                                        </div>
                                        @endforeach
                                        </div>
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <span class="details-row" style="text-align: right;">
                                        @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                        <a id = 'show-competitor-expand' href="{{ URL::to('/') }}/sme/competitor/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}}  {{trans('business_details2.competitors')}}</a>
                                        @endif
                                    </span>
                                    @endif

                                    <div id = 'expand-competitor-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                 <div id= 'bizsegment-list-cntr' class='spacewrapper'>
                                    <div class="read-only reload-bizsegment">
                                        <h4>{{trans('business_details2.customer_segment')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.cs_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.cs_hint', array(), 'fil')}}</i>">More info...</a></h4>
                                        <div class="read-only-text">
                                            @if(Auth::user()->role == 2 || Auth::user()->role == 3)
                                            <div class="completeness_check">{{ $progress['businesssegment']==1 ? "true" : "" }}</div>
                                            @endif

                                            @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->customer_segments)
                                            <span class="progress-required-counter" min-value="1"> {{ count($bizsegments) > 0 ? 1 : 0 }} </span>
                                            @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->customer_segments)
                                            <span class="progress-field-counter" min-value="1"> {{ count($bizsegments) > 0 ? 1 : 0 }} </span>
                                            @else

                                            @endif

                                            <div class="businesssegment">
                                            @foreach ($bizsegments as $bizsegment)
                                            <div class="details-row">
                                                <div class="col-md-12">
                                                @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                                <a href = "{{ URL::To('/sme/businesssegment/'.$bizsegment->businesssegmentid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-bizsegment' data-cntr = '#bizsegment-list-cntr'>{{ trans('messages.delete') }}</a>
                                                @endif
                                                <span class = 'editable-field' data-name = 'bizsegment.businesssegment_name' data-field-id = '{{ $bizsegment->businesssegmentid }}'>
                                                    {{ $bizsegment->businesssegment_name }}
                                                </span>
                                                </div>
                                            </div>
                                            @endforeach
                                            </div>
                                            @if ($entity->status == 0 || $entity->status == 3)
                                            <span class="details-row" style="text-align: right;">
                                            @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                                <a id = 'show-bizsegment-expand' href="{{ URL::to('/') }}/sme/businesssegment/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.customer_segment')}}</a>
                                            @endif
                                        </span>
                                        @endif

                                        <div id = 'expand-bizsegment-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='keymanager-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-key-manager">
                                    <h4>{{trans('owner_details.key_manager')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.cs_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.cs_hint', array(), 'fil')}}</i>">More info...</a></h4>
                                    
                                    <div class="read-only-text">
                                        <div class="details-row hidden-xs hidden-sm">
                                                <div class="col-md-2 col-sm-2"><b>First Name</b></div>
                                                <div class="col-md-2 col-sm-2"><b>Middle Name</b></div>
                                                <div class="col-md-2 col-sm-2"><b>Last Name</b></div>
                                                <div class="col-md-2 col-sm-2"><b>Address</b></div>
                                                <div class="col-md-2 col-sm-2"><b> Nationality</b></div>
                                                <div class="col-md-2 col-sm-2"><b> Position</b></div>
                                        </div>
                                        @foreach ($keymanagers as $keyinfo)
                                            <div class="details-row hidden-xs hidden-sm">
                                                <div class="col-md-2 col-sm-2">
                                                @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                                    <a href = "{{ URL::To('/sme/keymanager2/'.$keyinfo->keymanagerid).'/delete' }}" class="navbar-link delete-info" data-reload = '.reload-key-manager' data-cntr = '#keymanager-list-cntr'>{{ trans('messages.delete') }}</a>
                                                @endif
                                                <span class = 'editable-field' data-name = 'key_managers.firstname'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->firstname }}
                                                </span>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                <span class = 'editable-field' data-name = 'key_managers.middlename'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->middlename }}
                                                </span>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                <span class = 'editable-field' data-name = 'key_managers.lastname'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->lastname }}
                                                </span>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                <span class = 'editable-field' data-name = 'key_managers.address1'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->address1 }}
                                                </span>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                <span class = 'editable-field' data-name = 'key_managers.nationality'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->nationality }}
                                                </span>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                <span class = 'editable-field' data-name = 'key_managers.position'  data-field-id = '{{ $keyinfo->keymanagerid }}'>
                                                    {{ $keyinfo->position }}
                                                </span>
                                                </div>
                                            </div>
                                        @endforeach

                                        @if ($entity->status == 0 || $entity->status == 3)
                                            <span class="details-row" style="text-align: right;">
                                                @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                                    <a id="show-keymanager2-expand" href="{{ URL::to('/') }}/sme/keymanager2/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('owner_details.key_manager')}}</a>
                                                @endif
                                            </span>
                                        @endif
                                        <div id = 'expand-keymanager2-form' class = 'spacewrapper'></div>
                                    </div>
                                </div>
                            </div>
                            <div id='management-list-cntr' class='spacewrapper'>
                                <div class="read-only reload-management">

                                        <h4>{{trans('business_details1.management')}}</h4>
                                    <div class="read-only-text">

                                        @if(count($shareholders)>0)
                                            <h5><b>{{trans('business_details1.shareholder')}}</b></h5>
                                            <div class="details-row hidden-xs hidden-sm row7">
                                                    <div class="col7"><b>First Name</b></div>
                                                    <div class="col7"><b>Middle Name</b></div>
                                                    <div class="col7"><b>Last Name</b></div>
                                                    <div class="col7"><b>Address</b></div>
                                                    <div class="col7"><b> Nationality</b></div>
                                                    <div class="col7"><b> TIN</b></div>
                                                    <div class="col7"><b> % of Ownership</b></div>
                                            </div>
                                        @endif
                                        @foreach ($shareholders as $shinfo)
                                        <div class="details-row hidden-xs hidden-sm row7">
                                            <div class="col7">
                                            @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
	                                            @if($entity->status != 7)
	                                                <a href = "{{ URL::to('/') }}/sme/shareholders/{{ $shinfo->id }}/delete" class="navbar-link delete-info" data-reload = '.reload-management' data-cntr = '#management-list-cntr'>{{ trans('messages.delete') }}</a>
                                            	@endif   	
                                            @endif
                                                <span class = 'editable-field' data-name = 'shareholders.firstName'  data-field-id = '{{ $shinfo->id }}'>
                                                    {{ $shinfo->firstname }}
                                                </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.middleName'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->middlename }}
                                            </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.lastName'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->lastname }}
                                            </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.address'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->address }}
                                            </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.nationality'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->nationality }}
                                            </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.id_no'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->id_no }}
                                            </span>
                                            </div>
                                            <div class="col7">
                                            <span class = 'editable-field' data-name = 'shareholders.id_no'  data-field-id = '{{ $shinfo->id }}'>
                                                {{ $shinfo->percentage_share }}
                                            </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        <span class="details-row" style="text-align: right; width: 100%;">
                                        	@if($entity->status != 7)
                                            	<a id = 'show-shareholders2-expand' href="{{ URL::to('/') }}/sme/shareholders/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{trans('business_details1.shareholder')}}</a>
                                            @endif
                                        </span>
                                        <div id = 'expand-shareholders2-form' class = 'spacewrapper'></div>
                                    </div>
                                        
                                    <div class="read-only-text">
                                        @if(count($directors)>0)
                                            <h5><b>{{trans('business_details1.director')}}</b></h5>
                                            <div class="details-row hidden-xs hidden-sm">
                                                    <div class="col-md-2 col-sm-2"><b>First Name</b></div>
                                                    <div class="col-md-2 col-sm-2"><b>Middle Name</b></div>
                                                    <div class="col-md-2 col-sm-2"><b>Last Name</b></div>
                                                    <div class="col-md-2 col-sm-2"><b> Address</b></div>
                                                    <div class="col-md-2 col-sm-2"><b> Nationality</b></div>
                                                    <div class="col-md-2 col-sm-2"><b> TIN</b></div>
                                            </div>
                                        @endif

                                    @foreach ($directors as $dinfo)
                                    <div class="details-row hidden-xs hidden-sm">
                                        <div class="col-md-2 col-sm-2">
                                        @if (($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0) && ($entity->status == 0))
                                            <a href = "{{ URL::to('/') }}/sme/directors/{{ $dinfo->id }}/delete" class="navbar-link delete-info" data-reload = '.reload-management' data-cntr = '#management-list-cntr'>{{ trans('messages.delete') }}</a>
                                        @endif
                                            <span class = 'editable-field' data-name = 'directors.firstName'  data-field-id = '{{ $dinfo->id }}'>
                                                {{ $dinfo->firstname }}
                                            </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        <span class = 'editable-field' data-name = 'directors.middleName'  data-field-id = '{{ $dinfo->id }}'>
                                            {{ $dinfo->middlename }}
                                        </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        <span class = 'editable-field' data-name = 'directors.lastName'  data-field-id = '{{ $dinfo->id }}'>
                                            {{ $dinfo->lastname }}
                                        </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        <span class = 'editable-field' data-name = 'directors.address'  data-field-id = '{{ $dinfo->id }}'>
                                            {{ $dinfo->address }}
                                        </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        <span class = 'editable-field' data-name = 'directors.nationality'  data-field-id = '{{ $dinfo->id }}'>
                                            {{ $dinfo->nationality }}
                                        </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        <span class = 'editable-field' data-name = 'directors.id_no'  data-field-id = '{{ $dinfo->id }}'>
                                            {{ $dinfo->id_no }}
                                        </span>
                                        </div>
                                    </div>
                                    @endforeach
                                    <span class="details-row" style="text-align: right; width: 100%;">
                                        @if($entity->status != 7)
                                        	<a id = 'show-directors2-expand' href="{{ URL::to('/') }}/sme/directors/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{trans('business_details1.director')}}</a>
                                        @endif
                                    </span>

                                    <div id = 'expand-directors2-form' class = ''></div>
                                </div>
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        <div id="registration3" class="tab-pane fade">

                            <div id="dropbox-upload-cntr" class="spacewrapper print-none">
                              <div class="read-only">
                                <h4>{{ trans('messages.files_list') }}</h4>
                                <div class="read-only-text">
                            
                                  <div id='dropbox-upload-list-cntr' class="details-row">
                                    <div class='reload-dropbox-upload'>
                                                       <div class="col-md-12 text-center alert-success copied hidden">
                                            <span class="alert-success">Link Copied to Clipboard</span>
                                         </div>
                                          @if ($folderUrl == '')
                                                @if($loa)
                                                	@if(!empty($loa->document_orig))
                                                    <table class="table table-hover table-bordered">
                                                        <thead>
                                                        <tr>
                                                        <th scope="col" class="text-center">#</th>
                                                        <th scope="col">File Name</th>
                                                        <th scope="col" class="text-center">Type</th>
                                                        <th scope="col" class="text-center">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td scope="row" class="text-center">{{$count + 1}}</td>
                                                            <td>{{$loa->document_orig}}</td>
                                                            <td class="text-center">
                                                                <a title="Download" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ URL::to('/') }}/letterofauthorization/{{ $entity->entityid}}"><span class="glyphicon glyphicon-download-alt"></span></a>
                                                            </td>
                                                            <td class="text-center">Authorization Letter</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                	@endif
                                                @endif
                                          @else
                                            <!-- <span data-toggle="modal" data-target="#shareLinkModal" class="toShareModal">
                                              <a title="Share" data-toggle="tooltip" data-placement="top" class="share-link btn btn-primary btn-md mb-3" data-id="{{ $folderUrl->folder_url }}">Share Folder <span class="glyphicon glyphicon-share"></span></a>
                                            </span> -->
                          
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                  <th scope="col" class="text-center">#</th>
                                                  <th scope="col">File Name</th>
                                                  <th scope="col" class="text-center">Type</th>
                                                  <th scope="col" class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                
                                              @if($loa)
                                                	@if(!empty($loa->document_orig))
		                                                <tr>
		                                                    <td scope="row" class="text-center"><strong>{{$count - ($count - 1)}}</strong></td>
		                                                    <td>{{$loa->document_orig}}</td>
		                                                    <td class="text-center">Authorization Letter</td>
		                                                    <td class="text-center">
		                                                        <a title="Download" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ URL::to('/') }}/download/letterofauthorization/{{ $entity->entityid}}"><span class="glyphicon glyphicon-download-alt"></span></a>
		                                                    </td>
		                                                   
		                                                </tr>
	                                              	@endif
                                              @endif
                                                
                                              @foreach($entityFiles as $key => $filename)
                                                <tr>
                                                  @if(empty($loa))
                                                  <th scope="row" class="text-center">{{ $key+1}}</th>
                                                  @else
                                                  <th scope="row" class="text-center">{{ $key+2}}</th>
                                                  @endif
                          
                                                  <td><a href="{{ $filename->file_url }}" target="_blank">{{ $filename->file_name }}</a></td>
                                                  <td></td>
                                                  <td class="text-center">
                                                  <a title="Download" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ $filename->file_download }}"><span class="glyphicon glyphicon-download-alt"></span></a>
                          
                                                  <span data-toggle="modal" data-target="#shareLinkModal" class="toShareModal">
                                                    <a title="Share" data-toggle="tooltip" data-placement="top" class="share-link btn btn-primary btn-sm" data-id="{{ $filename->file_url }}"><span class="glyphicon glyphicon-share"></span></a>
                                                  </span>
                                                  @if ($entity->status == 0 or $entity->status == 3)
                                                  <a href="{{ URL::to('/') }}/sme/delete_assocfile/{{ $filename->id }}"  title="Delete" class="navbar-link delete-file btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-dropbox-upload' data-cntr = '#dropbox-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
                                                  @endif
                                                  </td>
                                                  
                                                </tr>
                                              @endforeach
                                                </tbody>
                                            </table>
                                          @endif
                            
                                          <div id = 'expand-dropbox-upload-form' class = 'spacewrapper'></div>
                            
                                          @if ($entity->status == 0 || $entity->status == 3)
                                          <span class="details-row" style="text-align: right;">
                                            @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                            <a id = 'show-dropbox-upload-expand' href="{{ URL::to('/') }}/sme/dropbox_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.upload_files') }}</a>
                                            @endif
                                          </span>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                          
                          <!-- Share Link Modal-->
                          <div class="modal fade centered-modal" id="shareLinkModal" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                 <div class="modal-header">
                                  <h4 class="modal-title">Share Link</h4>
                                 </div>
                                 <div class="modal-body">
                                   <div>
                                      <a href="" target="_blank" id="shareLink" value=""></a>
                                   </div>
                                  <div class="text-center">
                                      <button type="button" class="btn btn-success" data-dismiss="modal" id="copyButton">Copy Link to Clipboard</button>
                                  </div>
                                 </div>
                                 <div class="modal-footer">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                 </div>
                                </div>
                              </div>
                          </div>
                          
                          <div id="ubill-upload-cntr" class="spacewrapper print-none hidden">
                            <div class="read-only">
                              <h4>Utility Bills</h4>
                              <div class="read-only-text">
                          
                                <div id='ubill-upload-list-cntr' class="details-row">
                                  <div class='reload-ubill-upload'>
                                        @if (count($ubillFiles) == 0)
                                        <div class="col-md-12 text-center alert-warning">
                                          <span class="alert-warning">No Utility Bill</span>
                                        </div>
                                        @else
                                          <table class="table table-hover table-bordered">
                                              <thead>
                                              <tr>
                                                <th scope="col" class="text-center">#</th>
                                                <th scope="col">File Name</th>
                                                <th scope="col" class="text-center">Action</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                              
                                            @foreach($ubillFiles as $key => $filename)
                                              <tr>
                                                <th scope="row" class="text-center">{{ $key+1 }}</th>
                          
                                              <td><a href="{{ URL::to('/') }}/sme/download_ubill/{{ $filename->entity_id }}/{{ $filename->utility_bill_file }}" target="_blank">{{ $filename->utility_bill_file }}</a></td>
                          
                                                <td class="text-center">
                                                {{-- <a title="Download" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ $filename->file_download }}"><span class="glyphicon glyphicon-download-alt"></span></a> --}}
                          
                                                {{-- <span data-toggle="modal" data-target="#shareLinkModal" class="toShareModal">
                                                  <a title="Share" data-toggle="tooltip" data-placement="top" class="share-link btn btn-primary btn-sm" data-id="{{ $filename->file_url }}"><span class="glyphicon glyphicon-share"></span></a>
                                                </span> --}}
                                                @if ($entity->status == 0 or $entity->status == 3)
                                                <a href="{{ URL::to('/') }}/sme/delete_ubill_premium/{{ $filename->id }}"  title="Delete" class="navbar-link delete-ubill btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-ubill-upload' data-cntr = '#ubill-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
                                                @endif
                                                </td>
                                              </tr>
                                            @endforeach
                                              </tbody>
                                          </table>
                                        @endif
                          
                                        <div id = 'expand-ubill-upload-form' class = 'spacewrapper'></div>
                          
                                        @if ($entity->status == 0 or $entity->status == 3)
                                        <span class="details-row" style="text-align: right;">
                                          @if ($entity->loginid == Auth::user()->loginid || Auth::user()->role == 0)
                                          @if (count($ubillFiles) == 0)
                                          <a id = 'show-ubill-upload-expand' href="{{ URL::to('/') }}/sme/ubill_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ 'Upload Utility Bill' }}</a>
                                          @endif
                                          @endif
                                        </span>
                                        @endif
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                          
                          </div>
                        @if ($entity->status == 0 && !Session::has('tql'))
                            <div id="sci" class="tab-pane fade">

                                 <div id = 'incomplete-summary-premium' class="spacewrapper"> </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('user-2fa-form')
@include('modal-add-info')
@include('modal-upload-file')
@include('modal-refresh-warning')
@if(Auth::user()->role == 1 && $entity->status == 0)
    @include('modal-external-link')
    @include('modal-external-link-notification')
@endif
@include('modal-confirm-sec-override')
@stop

@section('javascript')
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/busidentification.js') }}"></script>
<script src="{{ URL::asset('js/creditfacilities.js') }}"></script>
<script src="{{ URL::asset('js/smesummary.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


<script>
    var curSub = null, curRow = null;
    $(document).ready(function() {
        $('.showMessage').show();
        setTimeout(function() {
            $('.showMessage').fadeOut();
        }, 3000);
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
<script>
    $(document).on("click", ".delete-file", function () {
        var fileName = $(this).data('id');
        $(".modal-body #fileName").text( fileName );
        $(".modal-body #filenameHidden").val( fileName );
    });
</script>
<script>
    $(document).on("click", ".share-link", function () {
        var link = $(this).data('id');
        $(".modal-body #shareLink").text(link);
        $(".modal-body #shareLink").attr("href", link);
        });
</script>
<script>
    $(document).ready(function(){
        $('#copyButton').click(function(){

        var text = $("#shareLink").get(0)
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
        //add to clipboard.
        document.execCommand('copy');
        
        $('.copied').removeClass('hidden');
        $('.copied').show();
        setTimeout(function() {
        $('.copied').fadeOut();
        }, 3000);
    })

    $('.toShareModal').click(function(){
        $(".navbar").addClass('zindex');
    });

    $("#shareLinkModal").on("hidden.bs.modal", function () {
        $('.navbar').removeClass('zindex');
    });

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        require(['/static/js/modules/smesummary/index.js'], function(smesummary) {
            smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
        });
        $("#submit_exit").on("click", function(){
            $("#form_submit").submit();
        }) 
    });
</script>
@if(Auth::user()->role == 4 || Auth::user()->role == 0)
<script>
    $(document).ready(function(){
        /**Check empty deliverables */
        if( ({{$deliverables->financial_rating}} == 0) && ({{$deliverables->growth_forecast}} == 0) && ({{$deliverables->industry_forecasting}} == 0)  ){
            //$('#financial_condition_chart').show();
            $('#growth_forecasting_chart').show();
            $('#industry_composition_chart').show();
            $('#printthis').show();
        }else{
            if( {{$deliverables->financial_rating}} == 1 ){
                //$('#financial_condition_chart').show();
            }
            if( {{$deliverables->growth_forecast}} == 1 ){
                $('#growth_forecasting_chart').show();
            }
            if( {{$deliverables->industry_forecasting}} == 1 ){
                $('#industry_composition_chart').show();
            }
        }
    });
</script>
@endif

@if(Session::has('redirect_tab'))
<script type="text/javascript">
  $(document).ready(function(){
   ClickTab("{{Session::get('redirect_tab')}}");
 });
</script>
@endif
@if($entity->status == 0)
	<script type="text/javascript" src="{{ URL::asset('js/cmn-lib/cmn_upd_premium_info.js') }}"></script>
@endif
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz1.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-ecf.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-owner.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-bcs.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-rcl.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/summary-ccc.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/toggle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/external-link.js') }}"></script>
<script src="{{ URL::asset('js/smesummary_investigator.js') }}"></script>
<script src="{{ URL::asset('js/chart-min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.printarea.js') }}"></script>
<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison2.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison3.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison4.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison5.js') }}"></script>
<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
<script src="{{ URL::asset('js/graphs-strategic-forecast.js') }}"></script>
<script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
<script src="{{ URL::asset('js/next-tab-msg.js') }}"></script>
<script src="{{ URL::asset('js/jquery.form.min.js') }}"></script>

@if (1 == $bank_ci_view)
<script type="text/javascript">
  $(document).ready(function(){
    $('.investigator-docs input[type="checkbox"').each(function(){
     $(this).prop('disabled', true);
   });

    $('textarea[name="notes"]').prop('disabled', true);
    $('input[name="investigator_others_docs"]').prop('disabled', true);
  });
</script>
@endif
<script type="text/javascript">

  $('#debt_service_calculator_container').bind('DOMSubtreeModified', function(){
	  console.log('changed');
	  	if({{ $export_dscr }} == 1){

	      	$('#export-dscr-btn').prop('disabled', true);
    		$('#export-dscr-btn').text("Exported to Rating Summary");
	    }
	});

  
</script>
@stop
@section('style')
@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag!=2)
    <link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"/>
@endif
@if(Session::has('tql'))
    <link rel="stylesheet" href="{{ URL::asset('css/external-link.css') }}" media="screen"/>
@endif
<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="print"/>
<link rel="stylesheet" href="{{ URL::asset('css/debt-service-calculator.css') }}" media="print"/>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>

<style>
  .centered-modal.in {
    display: flex !important;
}
.centered-modal .modal-dialog {
    margin: auto;
}
.modal-body{
  word-wrap: break-word;
}
.zindex {
  z-index:500 !important;
}

.col7{
	width: 14.28%;
	float: left;
}
.row7{
	padding: 5px 10px;
}
</style>
@stop
