@extends(STS_NG == $_COOKIE['formdata_support'] ? 'layouts.master' : 'layouts.empty')
@if (STS_NG == $_COOKIE['formdata_support'])

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>{{trans('messages.add')}}: {{trans('business_details1.certifications')}}</h1>
        <div class="spacewrapper">
            <div class="read-only">

                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="read-only-text">
@endif

	<div class="row">
        {{ Form::open(array('id' => 'certifications-expand-form', 'route' => array('postCertification', $entity_id), 'role'=>'form', 'files' => true)) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            @if (STS_NG == $_COOKIE['formdata_support'])
                <?php $max = 3; ?>
            @else
                <?php $max = 1; ?>
            @endif
            @for($i = 0; $i < 1; $i++)
            <div class="form-group">
                <div class="col-md-12{{ ($errors->has('certification_details.'.$i.'')) ? ' has-error' : '' }}">

                    @if (($i >= 1) || ($boi_peza >= 1))
                        {{ Form::label('certification_details'.$i.'', trans('business_details1.certifications')) }} *
                        {{ Form::text('certification_details['.$i.']', Input::old('certification_details.'.$i.''), array('class' => 'form-control')) }}
                    @else
                        {{ Form::label('certification_details'.$i.'', trans('business_details1.certifications')) }} *
                        {{ Form::select('certification_details['.$i.']',
                            array('' => trans('messages.choose_here'),
                                  'Board of Investments (BOI)' => 'Board of Investments (BOI)',
                                  'Philippine Export Processing Zone Authority (PEZA)' => 'Philippine Export Processing Zone Authority (PEZA)'
                                ), Input::old('certification_details.'.$i.''), array('id' => 'rate-tab', 'class' => 'form-control')) }}
                    @endif
                </div>

                <div class="col-md-12{{ ($errors->has('certification_reg_no.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('certification_reg_no'.$i.'', trans('messages.registration_num')) }} *
                    {{ Form::text('certification_reg_no['.$i.']', Input::old('certification_reg_no.'.$i.''), array('class' => 'form-control')) }}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6{{ ($errors->has('certification_reg_date.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('certification_reg_date'.$i.'', trans('messages.registration_date')) }} *
                    {{ Form::text('certification_reg_date['.$i.']', Input::old('certification_reg_date.'.$i.''), array('class' => 'form-control datepicker')) }}
                </div>

                <div class="col-md-6{{ ($errors->has('certification_doc.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('certification_doc'.$i.'', trans('messages.upload')) }} 
                    {{ Form::file('certification_doc['.$i.']', array('class' => 'form-control')) }}
                    <small>{{trans('messages.upload_hint1')}} &nbsp;&nbsp;&nbsp;</small>
                </div>
            </div>

            <div class = 'col-md-12'> <hr> </div>
            @endfor


            <div class="spacewrapper">
                {{ Form::token() }}
                @if (STS_NG == $_COOKIE['formdata_support'])
                <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @else
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @endif
            </div>
		{{ Form::close() }}
	</div>
@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
<script>
$('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    yearRange: '-30:+0',
    onChangeMonthYear: function(y, m, i) {
        var d = i.selectedDay;
        $(this).datepicker('setDate', new Date(y, m - 1, d));
    }
});
</script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
