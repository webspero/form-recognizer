    <div class="modal fade" id="user-2fa-form-modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> User Verification </h4>
                </div>
                {{ Form::open(array('id' => 'user-2fa-test-form')) }}
                <div class="modal-body">
                    <div class="container-fluid user-2fa-body">
                        <div class = 'row'>
                            <div class = 'col-md-12'>
                                <p> You are presented with this challenge because <span class = 'user-2fa-message'> </span>. </p>
                                <p> Please check your email for the answer. </p>
                                <p class = 'errormessage'> </p>
                            </div>
                        </div>
                        
                        
                        <div class = 'row'>
                            <div class="col-md-3">
                                <img src = '{{ URL::to("images/house.jpg") }}' class = 'img-responsive'>
                            </div>
                            <div class="col-md-3">
                                <img src = '{{ URL::to("images/car.jpg") }}' class = 'img-responsive'>
                            </div>
                            <div class="col-md-3">
                                <img src = '{{ URL::to("images/airplane.jpg") }}' class = 'img-responsive'>
                            </div>
                            <div class="col-md-3">
                                <img src = '{{ URL::to("images/coffee.jpg") }}' class = 'img-responsive'>
                            </div>
                        </div>
                        
                        <div class = 'row'>
                            <div class="col-md-3">
                                {{ Form::radio('image-test', 'house', false) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::radio('image-test', 'car', false) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::radio('image-test', 'airplane', false) }}
                            </div>
                            <div class="col-md-3">
                                {{ Form::radio('image-test', 'coffee', false) }}
                            </div>
                        </div>
                        
                        <div class = 'row'>
                            <div class="col-md-12">
                                {{ Form::label('passcode-test', 'Passcode') }}
                                {{ Form::text('passcode-test', '', array('class' => 'form-control')) }}
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                
                <div class="modal-footer user-2fa-footer">
                    <button id = 'btn-upload-bsp' type="submit" class="btn btn-primary">Verify</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>