@extends('layouts.master', array('hide' => true))

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'> (function(){ var widget_id = 'EPm8g31tw4'; var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<script type = 'text/javascript'>
    function jivo_onIntroduction()
    {
        var client_data = jivo_api.getContactInfo();
        var ajax_url    = BaseURL+'/ajax/save_jivo_leads';

        $.post(ajax_url, client_data, function() {
            /** No Processing   */
        });
    }
</script>
<!-- {/literal} END JIVOSITE CODE -->
<?php /*
<!-- live2support.com tracking codes starts --><div id="l2s_trk" style="z-index:99;">website live chat</div><script type="text/javascript"> var l2slhight=400; var l2slwdth=350; var l2slay_mnst="#l2snlayer {}";var l2slv=3; var l2slay_hbgc="#0097c2"; var l2slay_bcolor="#0097c2"; var l2sdialogofftxt="Live Chat Offline"; var l2sdialogontxt="Live Chat Online"; var l2sminimize=true;    var l2senblyr=true; var l2slay_pos="R"; var l2s_pht=escape(location.protocol); if(l2s_pht.indexOf("http")==-1) l2s_pht='http:'; (function () { document.getElementById('l2s_trk').style.visibility='hidden';
var l2scd = document.createElement('script'); l2scd.type = 'text/javascript'; l2scd.async = true; l2scd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's01.live2support.com/js/lsjs1.php?stid=26893 &jqry=Y&l2stxt='; var l2sscr = document.getElementsByTagName('script')[0]; l2sscr.parentNode.insertBefore(l2scd, l2sscr); })(); </script><!-- live2support.com tracking codes closed -->
*/ ?>
	<div class="container">
		<h1>Contractor Signup Page</h1>
		{{ Form::open( array('route' => 'postSignupContractor', 'role'=>'form', 'id' => 'formbpo')) }}
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Registration Details</h4>
				<div class="read-only-text">
					@if($errors->has('error'))
						{{ $errors->first("error", '<span class="errormessage">:message</span>') }}
					@endif
					<div class="form-group row">
						<div class="col-md-12{{ ($errors->has('email')) ? ' has-error' : '' }}">
							{{ Form::label('email', 'User Email') }} *
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control validate[required]')) }}
							@if($errors->has('email'))
								<span class="errormessage">{{ $errors->first('email') }}</span>
							@endif
							{{ Form::hidden('recaptcha','', array("id"=>"recaptcha"))}}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6{{ ($errors->has('password')) ? ' has-error' : '' }}">
							{{ Form::label('password', 'Password') }} *
							{{ Form::password('password', array('class' => 'form-control validate[required]')) }}

							@if($errors->has('password'))
								<span class="errormessage">{{ $errors->first('password') }}</span>
							@endif
						</div>
						<div class="col-md-6{{ ($errors->has('password_confirmation')) ? ' has-error' : '' }}">
							{{ Form::label('password_confirmation', 'Re-type Password') }} *
							{{ Form::password('password_confirmation', array('class' => 'form-control validate[required]')) }}

							@if($errors->has('password_confirmation'))
							<span class="errormessage">{{ $errors->first('password_confirmation') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12{{ ($errors->has('name')) ? ' has-error' : '' }}">
							{{ Form::label('name', "Full Name") }} *
							{{ Form::text('name', Input::old('name'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('name'))
							<span class="errormessage">{{ $errors->first('name') }}</span>
							@endif

							{{ Form::hidden('recaptcha','', array("id"=>"recaptcha"))}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-xs-12">
				<p style="padding-left: 20px; margin: 0;">
					<label>
						<input id="agree" name="agree" style="display: inline-block;" type="checkbox" value="true" class="validate[required] checkbox" data-errormessage-value-missing="Please read the Terms of Use and Privacy Policy, and then check this box" />
						I have read and agree to the
						<a href="{{ URL::to('/termsofuse') }}" class="btn btn-primary" target="_blank">Terms of Use</a>
						and
						<a href="{{ URL::to('/privacypolicy') }}" class="btn btn-primary" target="_blank">Privacy Policy</a>
						<img src="{{ URL::asset('images/thumb_12601.png') }}" />
					</label>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div style="padding: 10px;">
					@if($errors->has('captcha'))
						{{ $errors->first('captcha', '<span class="errormessage">:message</span>') }}
					@endif
				</div>

				<div style="padding: 10px;">
					{{ Form::token() }}
					<input type="button" id="submit_btn" class="btn btn-large btn-primary" value="Register">
				</div>
			</div>
			<div class="col-sm-6 falsefooter">
				<div title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
					<script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=business.creditbpo.com&size=S&use_flash=NO&use_transparent=NO&lang=en"></script><br />
					<a href="http://www.symantec.com/ssl-certificates" target="_blank" style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a>
				</div>
				<p>Copyright &copy; {{ date('Y') }} CreditBPO Tech, Inc. All rights reserved</p>
				<p class="small">CreditBPO and the CreditBPO marks used herein are registered trademarks of CreditBPO Tech, Inc.</p>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
		<script>
			$(document).ready(function(){
				grecaptcha.ready(function() {
					grecaptcha.execute('6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M', {action: 'home'}).then(function(token) {
						if(token) $("#recaptcha").val(token);
					});
				});
			});
		</script>
	<!-- <script src="{{ URL::asset('js/signup.js') }}"></script> -->
	<script src="{{ URL:: asset('js/register.js') }}"></script>
	<script src="https://www.google.com/recaptcha/api.js?render=6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@stop
@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>.toggle{ width:100% !important; }</style>
@stop
