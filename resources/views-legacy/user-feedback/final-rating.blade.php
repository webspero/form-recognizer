@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
    <div class="container">
        @if (0 >= $feedback_count)
		<h1>Feedback</h1>
		{{ Form::model(['route' => ['putSMEOverallFeedback', $entity_id], 'method' => 'post', 'id' => 'overall-feedback']) }}
		<div class="spacewrapper">
			<div class="read-only">
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $err)
						<p>{{$err}}</p>
					@endforeach
				</div>
				@endif
				<h4>Based on your overall experience</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('ease-experience', 'How easy was it to engage our service?') }} *
                        </div>
                        <div class="col-md-6">
                            {{ Form::select('ease-experience',
                                array('' => '1 (Lowest) to 5 (Highest)',
                                      '1' => 1,
                                      '2' => 2,
                                      '3' => 3,
                                      '4' => 4,
                                      '5' => 5
                                    ), null, array('id' => 'rate-tab', 'class' => 'form-control')) }}
						</div>
                    </div>

                    <div class="form-group">
						<div class="col-md-6">
							{{ Form::label('report-satisfaction', 'How happy are you with the report?') }} *
                        </div>
                        <div class="col-md-6">
                            {{ Form::select('report-satisfaction',
                                array('' => '1 (Lowest) to 5 (Highest)',
                                      '1' => 1,
                                      '2' => 2,
                                      '3' => 3,
                                      '4' => 4,
                                      '5' => 5
                                    ), null, array('id' => 'rate-tab', 'class' => 'form-control')) }}
						</div>
                    </div>

                    <div class="form-group">
						<div class="col-md-6">
							{{ Form::label('our-understanding', 'How well did we understand what you want?') }} *
                        </div>
                        <div class="col-md-6">
                            {{ Form::select('our-understanding',
                                array('' => '1 (Lowest) to 5 (Highest)',
                                      '1' => 1,
                                      '2' => 2,
                                      '3' => 3,
                                      '4' => 4,
                                      '5' => 5
                                    ), null, array('id' => 'rate-tab', 'class' => 'form-control')) }}
						</div>
                    </div>

                    <div class="form-group">
						<div class="col-md-6">
							{{ Form::label('will-recommend', 'How likely is it that you would recommend us to a peer or colleague?') }} *
                        </div>
                        <div class="col-md-6">
                            {{ Form::select('will-recommend',
                                array('' => '1 (Lowest) to 5 (Highest)',
                                      '1' => 1,
                                      '2' => 2,
                                      '3' => 3,
                                      '4' => 4,
                                      '5' => 5
                                    ), null, array('id' => 'rate-tab', 'class' => 'form-control')) }}
						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
        @else
        <h1> You have already given us your feedback. Thanks. </h1>
        @endif
    </div>
@stop

@section('javascript')

@stop
