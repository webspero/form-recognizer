                <div class="modal-body">
                    
                    <div class="container-fluid">
                        {{ Form::open(array('method' => 'post', 'id' => 'tab-user-feedback')) }}
                        <div class = 'row'>
                            <div class = 'col-md-12'>
                                <div id = 'feedback-rating-msg' class = 'errormessage'> </div>
                            </div>
                        </div>
                        <div class = 'row'>
                            {{ Form::hidden('tab-type', $tab_id, array('id' => 'tab-type')) }}
                            {{ Form::hidden('rating-trans-id', '', array('id' => 'rating-trans-id')) }}
                            <div class="form-group">
                                <div class='col-md-12'>
                                    <label>{{trans('messages.question_1')}}</label>
                                    <select name = 'difficulty-question' id = 'difficulty-question' class = 'form-control'>
                                        <option value = ''> {{ trans('messages.choose_here') }} </option>
                                        @foreach ($tab_questions[$tab_id] as $question)
                                        <option value = '{{ $question }}'> {{ trans($lang_file[$tab_id].'.'.$question) }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class = 'row'>
                            <div class="form-group">
                                <div class='col-md-12'>
                                <label>{{trans('messages.question_2')}}</label>
                                </div>
                                <div class='col-md-12'>
                                    {{ Form::textarea('recommendation-question', '', array('class' => 'form-control', 'style' => 'resize: none')) }}
                                </div>
                            </div>
                        </div>
                        
                        <div class = 'row'>        
                            <div class="form-group">
                                <div class='col-md-12'>
                                <label>{{trans('messages.rate_tab_overall')}}</label>
                                    {{ Form::select('rate-tab-question', 
                                        array('' => '1 (Lowest) to 5 (Highest)',
                                              '1' => 1,
                                              '2' => 2,
                                              '3' => 3,
                                              '4' => 4,
                                              '5' => 5
                                            ), null, array('id' => 'rate-tab', 'class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>