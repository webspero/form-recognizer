<div class="modal" id="modal-upd-approval" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1> Update Status of <span id = 'upd-approval-company'> </span> </h1>
            </div>
            
            <div class="modal-body">
                <div class = 'col-md-12'> <div class="alert alert-danger approval-error-msg" role="alert" style = 'display: none;'> </div> </div>
                
                {{ Form::open( array('url' => 'bank/upd-approval', 'role'=>'form', 'id' => 'upd-approval-form')) }}
                    <input type = 'hidden' id = 'upd-approval-id' name = 'upd-approval-id' value = '0' />
                    <select name="upd-sts-select" id="upd-sts-select" class="form-control">
                        <option value = ""> None </option>
                        <option value = "1"> Loan Approved </option>
                        <option value = "2"> Project Approved </option>
                        <option value = "3"> Loan Denied </option>
                        <option value = "4"> Project Denied </option>
                        <option value = "5"> In Process </option>
                    </select>
                {{ Form::close() }}
                
                <div class="clearfix"></div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }} </button>
                <button type="submit" class="btn btn-large btn-primary upd-approval-submit"> {{ trans('messages.save') }} </button>
            </div>
            
        </div>
    </div>
</div>