<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

</head>
<body style="margin: 12pt; font-family: helvetica,georgia,serif; color: #777777;">
	<table style="width: 100%; margin-bottom: 10px;">
		<tr>
			<td><h1  style="color: #026A7E; margin-bottom: 0; margin-top: 42px; font-size: 29px;">YOUR CreditBPO RATING REPORT</h1></td>
			<td style="text-align: right;"><img src="../public/images/CreditBPO_logo_pdf.png" style="width: 240px;" /></td>
		</tr>
	</table>
	<div style="text-align: center; background-color: #6DACDF; border: 2px solid #026A7E;">
		<table style="width: 100%;">
			<tr>
				<td align="center"><h1 style="color: #fff; font-size: 30px;">Compliments of: {{$key->name}}</h1></td>
			</tr>
		</table>
	</div>
	<div style="width: 80%; margin: 0 auto;">
		<p style="margin: 5px 0; font-size: 16px; margin-top: 10px; margin-bottom: 10px;">You need a partner who will tie in the loose ends for you and help you see how your
		business stands, how your strengths can be optimized and your weaknesses downplayed.
		Your CreditBPO Rating Report is the answer for a company like you.</p>
		<div style="margin: 0; color: #fff; background-color: #81B958; font-size: 26px; text-align: center; padding: 10px;" >
			<p style="margin: 5px 0; font-size: 24px;">YOUR CREDITBPO CLIENT KEY: <span style="font-family: monospace; color: #fff">{{$key->serial_key}}</span></p>
		</div>
		<h3 style="color: #13B36B; margin: 25px 0 10px; color: #13B36B; font-weight: normal; font-size: 23px;">YOUR CreditBPO RATING REPORT BENEFITS:</h3>
		<h4 style="font-weight: normal; font-size: 14px; margin: 0 0 14px 0;">
			<img src="../public/images/know-how-banks.png" style="vertical-align: middle; width: 20px;">
			Know how the banks will rate your business BEFORE you send them a loan application.
		</h4>
		<h4 style="font-weight: normal; font-size: 14px; margin: 0 0 14px 0;">
			<img src="../public/images/benchmark.png" style="vertical-align: middle; width: 20px;">
			Benchmark your Financial Performance against other companies in your Industry.
		</h4>
		<h4 style="font-weight: normal; font-size: 14px; margin: 0 0 14px 0;">
			<img src="../public/images/actionable.png" style="vertical-align: middle; width: 20px;">
			Get actionable recommendations on how to improve your credit risk rating and grow your business.
		</h4>
	</div>
	<br><br>
	<p style="margin: 5px 0; font-size: 12px;">To claim your CreditBPO rating report:</p>
	<p style="margin: 5px 0; font-size: 12px;">New users without logins register at http://business.creditbpo.com and use the Client Key above OR</p>
	<p style="margin: 5px 0; font-size: 12px;">Existing users with logins log into their account and order a New Report to use the Client Key.</p>
	<p style="margin: 5px 0; font-size: 12px;">Excited at knowing how financial analysts would rate your business? At increasing your chances of getting a loan? At getting ahead of the competition?</p>
	<p style="margin: 5px 0; font-size: 12px;">Visit us at http://www.creditbpo.com</p>
</body>
</html>
