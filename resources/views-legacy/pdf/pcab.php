<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  </head>
  <body>
    <div style="margin-right: 0px;">
        <div>
          <p style="font-size:11px; float:right">PCAB-F-SVD-031b Revision No.00, 11/19/2018</p>
          <br>
          <table>
              <tbody>
                  <tr>
                    <td style="font-size:11px; width:130px"><b>Subject: Summary Evaluation of the application of:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                    <td style="font-size:11px; width:50px"><b>OR Number:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                  </tr>
                  <tr>
                    <td style="font-size:11px; width:130px"></td>
                    <td style="font-size:11px; width:50px"></td>
                    <td style="font-size:11px; width:50px"><b>Amount:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                  </tr>
                  <tr>
                    <td style="font-size:11px; width:130px"><b>Address:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                    <td style="font-size:11px; width:50px"><b>Reference Number:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                  </tr>
                  <tr>
                    <td style="font-size:11px; width:130px"></td>
                    <td style="font-size:11px; width:50px"></td>
                    <td style="font-size:11px; width:50px"><b>Date Filed:</b></td>
                    <td style="font-size:11px; width:50px"></td>
                  </tr>
              </tbody>
          </table>
        </div>
        <div style="position: relative; padding-right:50px; margin:1em 0; min-height:120px;">
          <div style="font-size: 48px; -webkit-transform: rotate(-90deg); -moz-transform: rotate(-90deg); -ms-transform: rotate(-90deg); -o-transform: rotate(-90deg); filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3); right: -30px; top: 27px; position: absolute; text-transform: uppercase; font-size:25pt; font-weight:bold;">00000</div>
          <table style="border: 1px solid black; border-collapse: collapse; margin: 1px;">
            <tbody>
              <tr>
                <td style="font-size:11px; width:170px;"><b>Name of AMO:</b></td>
                <td style="font-size:11px; width:170px;"></td>
                <td style="font-size:11px; width:170px;"><b>Designation:</b></td>
                <td style="font-size:11px; width:170px;"></td>
              </tr>
              <tr>
                <td style="font-size:11px; width:170px"><b>Application for:</b> </td>
                <td style="font-size:11px; width:170px"><input type="checkbox" checked> Renewal CFY:</td>
                <td style="font-size:11px; width:170px"><b>Classification(s) applied for:</b></td>
                <td style="font-size:11px; width:170px"></td>
              </tr>
              <tr>
                <td style="font-size:11px; width:170px"><b>License Number:</b></td>
                <td style="font-size:11px; width:170px"></td>
                <td style="font-size:11px; width:170px"><b>Principal:</b></td>
                <td style="font-size:11px; width:170px"></td>
              </tr>
              <tr>
                <td style="font-size:11px; width:170px"><b>Date Issued:</b></td>
                <td style="font-size:11px; width:170px"></td>
                <td style="font-size:11px; width:170px"><b>Others:</b></td>
                <td style="font-size:11px; width:170px"></td>
              </tr>
              <tr>
                <td style="font-size:11px; width:170px"><b>Present Category:</b></td>
                <td style="font-size:11px; width:170px"></td>
                <td style="font-size:11px; width:170px"></td>
                <td style="font-size:11px; width:170px"></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div>
          <p style="font-size:11px;">1. The Evalutaion Commitee submits its findings and recommendation as follows:</p>
          <ol>
            <li style="list-style-type: none; font-size:11px;">1.1 LEGAL REQUIREMENTS</li>
            <li style="list-style-type: none; font-size:11px;">1.2 TECHNICAL REQUIREMENTS</li>
            <br>
            <table  style="border: 1px solid black; border-collapse: collapse; margin: 1px; text-align: center;">
              <thead>
                <tr>
                  <th style="font-size:11px; border: 1px solid black;" colspan="2"></th>
                  <th style="font-size:11px; border: 1px solid black;">No. of STE</th>
                  <th style="font-size:11px; border: 1px solid black;">Man-Yrs</th>
                  <th style="font-size:11px; border: 1px solid black;">Credit Pts</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="font-size:11px; width:270px; border: 1px solid black; text-align: left;">a.1) Full-time STE(s) for principal classification</td>
                  <td style="font-size:11px; width:50px; border: 1px solid black; "></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:11px; width:270px; border: 1px solid black; text-align: left;">a.2) Full-time STE(s) for other classification</td>
                  <td style="font-size:11px; width:50px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:11px; width:320px; border: 1px solid black; text-align: left;" colspan="2">a.3) Total</td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:11px; width:320px; border: 1px solid black; text-align: left;" colspan="2">b) Average Annual Work Volume for the past three (3) years</td>
                  <td style="font-size:11px; width:200px; border: 1px solid black;" colspan="2"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:11px; width:320px; border: 1px solid black; text-align: left;" colspan="2">c) No. of years of active operation as a Licencse Contractors</td>
                  <td style="font-size:11px; width:200px; border: 1px solid black;" colspan="2"></td>
                  <td style="font-size:11px; width:100px; border: 1px solid black;"></td>
                </tr>
              </tbody>
            </table>
            <li style="list-style-type: none; font-size:11px;">1.3 FINANCIAL REQUIREMENTS</li>
            <br>
            <table style="border: 1px solid black; border-collapse: collapse; margin: 1px;">
              <tbody>
                <tr>
                  <td style="width:200px; font-size:11px; border: 1px solid black;  text-align: left;">a)    Networth/Stockholder's Equity as of</td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;">PHP</td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="width:200px; font-size:11px; border: 1px solid black; text-align: left;">b)    Book Value of construction equipment as of</td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;">PHP</td>
                  <td style="width:200px; font-size:11px; border: 1px solid black;"></td>
                </tr>
              </tbody>
            </table>
          </ol>
        </div>

        <div>
          <p style="font-size:11px;">TOTAL POINTS</p>
          <div>
            <table>
              <tbody>
                <tr>
                  <td style="font-size:11px; width:170px;">RECOMMENDED CATEGORY:</td>
                  <td style="font-size:11px; width:170px; display: table-cell; border-bottom: 1 px solid black;" ></td>
                  <td style="font-size:11px; width:170px;">PRINCIPAL CLASSIFICATION:</td>
                  <td style="font-size:11px; width:170px; display: table-cell; border-bottom: 1px solid black;"></td>
                </tr>
                  <td style="font-size:11px; width:170px;">OTHER CLASSIFICATION(S):</td>
                  <td colspan="3" style="font-size:11px; width:510px; display: table-cell; border-bottom: 1px solid black;"></td>
                <tr>
                </tr>
                  <td style="font-size:11px; width:170px; color: #FFFFFF" >:</td>
                  <td colspan="3" style="font-size:11px; width:510px; display: table-cell; border-bottom: 1px solid black;"></td>
                <tr>
                </tr>
                  <td style="font-size:11px; width:170px; color: #FFFFFF" >:</td>
                  <td colspan="3" style="font-size:11px; width:510px; display: table-cell; border-bottom: 1px solid black;"></td>
                <tr>
                </tr>
                  <td style="font-size:11px; width:450px;" colspan="3">In view of the foregoing, the Evaluation Committee recommends that subject application be</td>
                  <td style="font-size:11px; width:150px; display: table-cell; border-bottom: 1px solid black;"></td>
                <tr>
            </table>
          </div>
          <br>  
        </div>
        <div style="padding:2% 0 2% 0; border-top:1px solid black; border-bottom: 1px solid black; width:685px; height: 200px;">
          <div style="width: 50%;  float: left;">
              <p style="font-size:11px;">To pay:</p>
              <table>
                <tr>
                  <td style="font-size:11px; width: 30px;" colspan="2">Renewal ______ balance</td>
                  <td style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;"><u>PHP______</u></td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;">Additional Lic. Fee</td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;">Prev. Renewals -CFY</td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                  <td style="font-size:11px;">__________</td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                  <td style="font-size:11px; width: 15px;">__________</td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;"><u>PHP______</u></td>
                </tr>
                <tr>
                  <td colspan="2" style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;"></td>
                  <td style="font-size:11px; width: 15px;"></td>
                </tr>
              </table>
              <div style="font-size:11px; position: relative; left:120px; top: 30;"><strong><i>March 3, 2020</i></strong></div>
          </div>
          <div style="width: 50%; float: left;">
            <p style="font-size:11px;">Subject to:</p>
              <ol>
                <li style="list-style-type: none; font-size:11px;">1. Signature of Prop. AMO on</li>
                <div width="365px"><hr style="background-color:black;"></div>
                <div width="365px"><hr style="background-color:black;"></div>
                <li style="list-style-type: none; font-size:11px;">2. Submission of</li>
                <div width="365px"><hr style="background-color:black;"></div>
                <div width="365px"><hr style="background-color:black;"></div>
              </ol>
          </div>
        </div>
        </div>
      </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>