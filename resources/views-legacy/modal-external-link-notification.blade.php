    <div class="modal" id="modal-external-link-notification">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">
                        External Link Submitted
                    </h4>
                </div>    
                <div class="modal-body">
					The following list of questions has just been externally answered:<br/><br/>
					<ul class="list">
					</ul>
					Please click yes to reload this page and update the questions above.
                </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary meln-yes" >Yes</button>
					<button type="submit" class="btn btn-default" data-dismiss="modal">Not Yet</button>
				</div>
            </div>
        </div>
    </div>