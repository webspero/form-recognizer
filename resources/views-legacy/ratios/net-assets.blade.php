@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
    <style>
        td {
            vertical-align: middle !important;
            text-align: center;
        }
    </style>
@stop

@section('content')
    <div class="container">
        @if(!empty($financialReport))
            <div class="text-center">
                <h3>1.2. Net Assets (Net Worth)</h3>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td rowspan="3">Indicator</td>
                    <td colspan="{{ count($balanceSheets)+2 }}">Value</td>
                    <td colspan="2">Change</td>
                </tr>
                <tr>
                    <td colspan="2">in PHP</td>
                    <td colspan="{{ count($balanceSheets) }}">% of the balance total</td>
                    <td rowspan="2">PHP (col.3-col.2)</td>
                    <td rowspan="2">% ((col.3-col.2):col.2)</td>
                </tr>
                <tr>
                    <td>at the beginning of the period analyzed(12/31/{{ $balanceSheets[0]->Year}})</td>
                    <td>at the end of the period analyzed(12/31/{{ $balanceSheets[count($balanceSheets)-1]->Year}})</td>
                    @foreach ($balanceSheets as $balanceSheet)
                        <td>12/31/{{ $balanceSheet->Year}}</td>
                    @endforeach
                </tr>
                <tr>
                    @for($indexCount = 1; $indexCount <= (count($balanceSheets)+5); $indexCount++)
                        <td> {{ $indexCount }}</td>
                    @endfor
                </tr>
                </thead>
                <tbody>
                    @php 
                        // beginning of the period
                        $b_intangible_assets = $balanceSheets[0]->Goodwill+$balanceSheets[0]->IntangibleAssetsOtherThanGoodwill;
                        $b_net_tangible_assets = $balanceSheets[0]->Equity-$b_intangible_assets;
                        $b_net_assets = $balanceSheets[0]->Equity;
                        $b_issued_capital = $balanceSheets[0]->IssuedCapital;
                        $b_difference = $b_net_assets-$b_issued_capital;

                        // end of the period
                        $e_intangible_assets = $balanceSheets[count($balanceSheets)-1]->Goodwill+$balanceSheets[count($balanceSheets)-1]->IntangibleAssetsOtherThanGoodwill;
                        $e_net_tangible_assets = $balanceSheets[count($balanceSheets)-1]->Equity-$e_intangible_assets;
                        $e_net_assets = $balanceSheets[count($balanceSheets)-1]->Equity;
                        $e_issued_capital = $balanceSheets[count($balanceSheets)-1]->IssuedCapital;
                        $e_difference = $e_net_assets-$e_issued_capital;

                        // change column
                        $net_tangible_assets_change = $e_net_tangible_assets - $b_net_tangible_assets;
                        if($e_net_tangible_assets > 0) {
                            $net_tangible_assets_change_prec = ($net_tangible_assets_change/$e_net_tangible_assets)*100;
                        } else {
                            $net_tangible_assets_change_prec = 0;
                        }
                        $net_assets_change = $e_net_assets - $b_net_assets;
                        if($e_net_assets > 0) {
                            $net_assets_change_prec = ($net_assets_change/$e_net_assets)*100;
                        } else {
                            $net_assets_change_prec = 0;
                        }
                        $issued_capital_change = $e_issued_capital - $b_issued_capital;
                        if($e_issued_capital > 0) {
                            $issued_capital_change_prec = ($issued_capital_change/$e_issued_capital)*100;
                        } else {
                            $issued_capital_change_prec = 0;
                        }
                        $difference_change = $net_assets_change - $issued_capital_change;
                        if($e_difference > 0) {
                            $difference_change_prec = ($difference_change/$e_difference)*100;
                        } else {
                            $difference_change_prec = 0;
                        }
                        @endphp
                    
                    <tr>
                        <td>1. Net Tangible assets</td>
                        <td> {{ ($b_net_tangible_assets != 0) ? number_format($b_net_tangible_assets) : '-' }}</td>
                        <td> {{ ($e_net_tangible_assets != 0) ? number_format($e_net_tangible_assets) : '-' }}</td>
                        @foreach ($balanceSheets as $balanceSheet)
                            @php
                                $i_intangible_assets = $balanceSheet->Goodwill+$balanceSheet->IntangibleAssetsOtherThanGoodwill;
                                $i_net_tangible_assets = $balanceSheet->Equity-$i_intangible_assets;
                                $i_assets = $balanceSheet->Assets;
                                if($i_assets > 0) {
                                    $i_net_tangible_assets_prec = ($i_net_tangible_assets/$i_assets)*100;
                                } else {
                                    $i_net_tangible_assets_prec = 0;
                                }
                            @endphp
                            <td> {{ ($i_net_tangible_assets_prec != 0) ? round($i_net_tangible_assets_prec,1) : '-' }}</td>
                        @endforeach
                        <td>
                            @if($net_tangible_assets_change == 0)
                                {{ '-' }}
                            @elseif($net_tangible_assets_change > 0)
                                {{ '+'.number_format($net_tangible_assets_change) }}
                            @else
                                {{ '-'.number_format(abs($net_tangible_assets_change)) }}
                            @endif
                        </td>
                        <td>
                            @if($net_tangible_assets_change_prec == 0)
                                {{ '-' }}
                            @elseif($net_tangible_assets_change_prec > 0)
                                {{ round($net_tangible_assets_change_prec, 1) }}
                            @else
                                {{ round($net_tangible_assets_change_prec, 1) }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>2. Net assets(Net Worth)</td>
                        <td> {{ ($b_net_assets != 0) ? number_format($b_net_assets) : '-' }}</td>
                        <td> {{ ($e_net_assets != 0) ? number_format($e_net_assets) : '-' }}</td>
                        @foreach ($balanceSheets as $balanceSheet)
                            @php
                                $i_net_assets = $balanceSheet->Equity;
                                $i_assets = $balanceSheet->Assets;
                                if($i_assets > 0) {
                                    $i_net_assets_prec = ($i_net_assets/$i_assets)*100;
                                } else {
                                    $i_net_assets_prec = 0;
                                }
                            @endphp
                            <td> {{ ($i_net_assets_prec != 0) ? round($i_net_assets_prec,1) : '-' }}</td>
                        @endforeach
                        <td>
                            @if($net_assets_change == 0)
                                {{ '-' }}
                            @elseif($net_assets_change > 0)
                                {{ '+'.number_format($net_assets_change) }}
                            @else
                                {{ '-'.number_format(abs($net_assets_change)) }}
                            @endif
                        </td>
                        <td>
                            @if($net_assets_change_prec == 0)
                                {{ '-' }}
                            @elseif($net_assets_change_prec > 0)
                                {{ round($net_assets_change_prec, 1) }}
                            @else
                                {{ round($net_assets_change_prec, 1) }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>3. Issued (share) capital</td>
                        <td> {{ ($b_issued_capital != 0) ? number_format($b_issued_capital) : '-' }}</td>
                        <td> {{ ($e_issued_capital != 0) ? number_format($e_issued_capital) : '-' }}</td>
                        @foreach ($balanceSheets as $balanceSheet)
                            @php
                                $i_issued_capital = $balanceSheet->IssuedCapital;
                                $i_assets = $balanceSheet->Assets;
                                if($i_assets > 0) {
                                    $i_issued_capital_prec = ($i_issued_capital/$i_assets)*100;
                                } else {
                                    $i_issued_capital_prec = 0;
                                }
                            @endphp
                            <td> {{ ($i_issued_capital_prec != 0) ? round($i_issued_capital_prec,1) : '-' }}</td>
                        @endforeach
                        <td>
                            @if($issued_capital_change == 0)
                                {{ '-' }}
                            @elseif($issued_capital_change > 0)
                                {{ '+'.number_format($issued_capital_change) }}
                            @else
                                {{ '-'.number_format(abs($issued_capital_change)) }}
                            @endif
                        </td>
                        <td>
                            @if($issued_capital_change_prec == 0)
                                {{ '-' }}
                            @elseif($issued_capital_change_prec > 0)
                                {{ round($issued_capital_change_prec, 1) }}
                            @else
                                {{ round($issued_capital_change_prec, 1) }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>4. Difference between net assets and Issued (share) capital (line 2 - line 3)</td>
                        <td> {{ ($b_difference != 0) ? number_format($b_difference) : '-' }}</td>
                        <td> {{ ($e_difference != 0) ? number_format($e_difference) : '-' }}</td>
                        @foreach ($balanceSheets as $balanceSheet)
                            @php
                                $i_retained_earnings = $balanceSheet->RetainedEarnings;
                                $i_assets = $balanceSheet->Assets;
                                if($i_assets > 0) {
                                    $i_difference_prec = ($i_retained_earnings/$i_assets)*100;
                                } else {
                                    $i_difference_prec = 0;
                                }
                            @endphp
                            <td> {{ ($i_difference_prec != 0) ? round($i_difference_prec,1) : '-' }}</td>
                        @endforeach
                        <td>
                            @if($difference_change == 0)
                                {{ '-' }}
                            @elseif($difference_change > 0)
                                {{ '+'.number_format($difference_change) }}
                            @else
                                {{ '-'.number_format(abs($difference_change)) }}
                            @endif
                        </td>
                        <td>
                            @if($difference_change_prec == 0)
                                {{ '-' }}
                            @elseif($difference_change_prec > 0)
                                {{ round($difference_change_prec, 1) }}
                            @else
                                {{ round($difference_change_prec, 1) }}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <canvas id="canvas"></canvas>
        @else
            <div class="text-center" style="min-height: 70vh;">
                <h4> Invalid Entity! Please select valid Entity.</h4>
            </div>
        @endif
    </div>
@stop
@section('javascript')
    @if(!empty($financialReport))
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-annotation/0.5.7/chartjs-plugin-annotation.min.js"></script>
        <script>
            var years = {{ $balanceSheets->map(function ($value) {
                return $value->Year;
                })
            }};
            var net_worth = {{ $balanceSheets->map(function ($value) {
                return (double)$value->Equity;
                })
            }};
            var issued_capital = {{ $balanceSheets->map(function ($value) {
                return (double)$value->IssuedCapital;
                })
            }};
            
            new Chart(document.getElementById('canvas').getContext('2d'), {
                type: 'bar',
                data: {
                    labels: years,
                    datasets: [{
                        type: 'bar',
                        label: 'Net worth (net assets)',
                        backgroundColor: 'blue',
                        barThickness: 50,
                        data: net_worth,
                    }, {
                        type: 'bar',
                        label: 'Issued (share) capital',
                        backgroundColor: 'green',
                        barThickness: 50,
                        data: issued_capital,
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Dynamics of net assets and issued capital',
                        fontSize: 16,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                    },
                    scales: {
                        xAxes: [{
                            gridLines : {
                                display : false,
                            },
                        }],
                        yAxes: [{
                            gridLines : {
                                display : true,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PHP'
                            },
                            ticks: {
                                beginAtZero:true,
                            }
                        }]
                    },
                    annotation: {
                        annotations: [{
                            type: 'line',
                            mode: 'horizontal',
                            scaleID: 'y-axis-0',
                            value: {{ $balanceSheets[count($balanceSheets)-1]->IssuedCapital }},
                            borderColor: 'red',
                            borderDash: [5, 5],
                            borderWidth: 1,
                            label: {
                                enabled: true,
                                content: "{{ $balanceSheets[count($balanceSheets)-1]->IssuedCapital }}",
                                position: 'right',
                                fontColor: 'red',
                                backgroundColor: 'rgba(0,0,0,0)',
                                yAdjust: -15,
                            }
                        }]
                    },
                }
            });        
        </script>
    @endif
@stop