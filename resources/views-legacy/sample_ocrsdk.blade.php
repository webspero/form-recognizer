<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Average Daily Balance OCR Check</title>
<style>
	table { border-collapse: collapse; }
	table td, table th { border: 1px solid #CCC; padding: 3px; }
</style>
</head>
<body>
	<div>
		<h3>Average Daily Balance OCR Check</h3>
		{{Form::open(['route'=>'postOCRSDK', 'method'=>'post', 'files'=>true])}}
			<label>File: </label><input type="file" name="imagefile" /><br/><br/>
			<input type="submit" value="OCR convert" />
		{{Form::close()}}
	</div>
	<div>
		<h2>RESULT</h2>
		<textarea rows="20" style="width: 100%;" wrap="soft">{{ $response }}</textarea>
		<table>
			<tr><th>Date</th><th>Amount</th></tr>
			@foreach($transactions as $k => $t)
				<tr><td>{{$k}}</td><td>{{$t}}</td></tr>
			@endforeach
			<tr><td><b>Average</b></td><td>{{number_format($average, 2, '.', ',')}}</td></tr>
		</table>
	</div>
</body>
</html>