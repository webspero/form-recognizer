
    <div class = 'balance-sheet col-md-12'>
        
        <div class = 'row'> <b>Balance Sheet</b> </div>
        
        <div class = 'col-md-6'> <b>Current Assets</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->current_asset, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Total Assets</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->total_asset, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Current Liabilities</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->current_liabilities, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Total Liabilities</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->total_liabilities, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Retained Earnings/(Deficit)</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->retained_earnings, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Stockholders' Equity</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->stockholders_equity, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Stockholders' Equity - Parent</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$bal_sheet->parent_stockholders_equity, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Book Value</b> </div>
        <div class = 'col-md-6'> {{ number_format($bal_sheet->book_value, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Current ratio</b> </div>
        <div class = 'col-md-6'> {{ number_format($bal_sheet->current_ratio, 2) }} <b>({{ number_format($bal_sheet->current_ratio_change, 2) }}%)</b></div>
        
        <div class = 'col-md-6'> <b>Debt-equity ratio</b> </div>
        <div class = 'col-md-6'> {{ number_format($bal_sheet->debt_equity_ratio, 2) }} <b>({{ number_format($bal_sheet->debt_equity_change, 2) }}%)</b></div>
    </div>
    
    <div class = 'income-statement col-md-12'>
        
        <div class = 'row'> <b>Income Statement</b> </div>
        
        <div class = 'col-md-6'> <b>Gross Revenue</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$income->gross_revenue, 2) }} <b>({{ number_format($income->gross_revenue_change, 2) }}%)</b></div>
        
        <div class = 'col-md-6'> <b>Gross Expense</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$income->gross_expense, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Net Income/(Loss) Before Tax</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$income->net_income_before_tax, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Net Income/(Loss) After Tax</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$income->net_income_after_tax, 2) }} <b>({{ number_format($income->net_income_change, 2) }}%)</b></div>
        
        <div class = 'col-md-6'> <b>Net Income/(Loss) Attributable to Parent</b> </div>
        <div class = 'col-md-6'> {{ number_format((float)$income->net_income_parent_attrib, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Earnings/(Loss) Per Share (Basic)</b> </div>
        <div class = 'col-md-6'> {{ number_format($income->basic_earnings_per_share, 2) }} </div>
        
        <div class = 'col-md-6'> <b>Earnings/(Loss) Per Share (Diluted)</b> </div>
        <div class = 'col-md-6'> {{ number_format($income->diluted_earnings_per_share, 2) }} </div>
    </div>