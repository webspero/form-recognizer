@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<p style="margin-top: 5px; margin-bottom:  0px;">
				<span style="font-weight: bold; font-size: 16px;">PSE Companies Data</span>
			</p>
		</div>
		@if (@count($companies) > 0)
        <div class="col-sm-4 col-md-4">
            <a data-toggle="modal" data-target="#modal-extract-report" class = 'btn btn-primary pull-right'> <span class="glyphicon glyphicon-save"></span> Extract Annual Reports </a>
            <a data-toggle="modal" data-target="#modal-initial-extract" class = 'btn btn-primary' id = 'initial-extraction-btn' style = 'display: none;'> <span class="glyphicon glyphicon-save"></span> Extract PSE Companies </a>
        </div>
        @endif
	</div>
	<div class="clearfix"></div>
	<hr />
	<div class="table-responsive">
        <input type = 'hidden' value = '{{ $init_data }}' id = 'initialize_complete' />
        <input type = 'hidden' value = '{{ $new_list_exist }}' id = 'company_list_complete' />
        <input type = 'hidden' value = '{{ $total_companies }}' id = 'total_companies' />
        @if (0 >= @count($companies))
            <div class = 'col-md-12'>
                <div> The PSE Companies has not yet been extracted. Click on the button to begin extraction </div>
                <a data-toggle="modal" data-target="#modal-initial-extract" class = 'btn btn-primary' id = 'initial-extraction-btn'> <span class="glyphicon glyphicon-save"></span> Extract PSE Companies </a>
            </div>
        @else
            <table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th style="background-color: #055688;color: #fff; font-weight: bold;">Code</th>
                        <th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
                        <th style="background-color: #055688;color: #fff; font-weight: bold;">Reports</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($companies as $company)
                    <tr class="odd" role="row">
                        <td>{{ $company->pse_code }}</td>
                        <td class = 'pse-name-id' data-value = '{{ $company->pse_id }}'>{{ $company->company_name }}</td>
                        <td> <a href = '{{ URL::To("pse/get_reports/") }}/{{ $company->pse_id }}' class = 'annual-report-link' data-company-name = '{{ $company->company_name }}'> Annual Report </a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
	</div>
</div>

<div class="modal" id="modal-extract-report" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Annual Report Extraction</h4>
            </div>
            <div class="modal-body">
                <div class = 'row'>
                    <div id = 'extracting-data-ongoing' class = 'col-md-12' style = 'display: none;'>
                        <img src = '{{ URL::To("images/preloader.gif") }}' class = 'extraction-loader'/>
                        <span class = 'extraction-progress-notes'> Initializing Extraction Interface... </span>
                        <div class = 'extraction-progress'> </div>
                    </div>
                    <div id = 'extract-data-initialize' class = 'col-md-12'>
                        The Annual reports for all PSE Listed Companies will be extracted when the "Begin Extraction" button is clicked.
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id = 'close-extract-btn'>Close</button>
                <button type="button" class="btn btn-primary" id = 'extract-report-btn'>Begin Extraction</button>
            </div>

        </div>
    </div>
</div>

<div class="modal" id="modal-initial-extract" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Initial Extraction</h4>
            </div>
            <div class="modal-body">
                <div class = 'row'>
                    <div id = 'initial-extraction-ongoing' class = 'col-md-12' style = 'display: none;'>
                        <img src = '{{ URL::To("images/preloader.gif") }}' class = 'initial-extraction-loader'/>
                        <span class = 'initial-extraction-notes'> Initializing Extraction Interface... </span>
                        <div class = 'initial-extraction-progress'> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-show-report" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Annual Report <span class = 'report-company-name'> </span></h4>
            </div>
            <div class="modal-body">
                <div class = 'row'>
                    <div id = 'report-picker' class = 'col-md-12'>
                        <input type = 'hidden' value = '0' id = 'report-pse-id'/>
                        <select id = 'report-selector'> </select>
                    </div>

                    <div id = 'report-view-deck' class = 'col-md-12'>

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<div id = 'company_list'> </div>
<div id = 'extraction_container' style = 'display:none;'> </div>
@stop

@section('javascript')
<script src="{{ URL::asset('js/pse-extraction.js') }}"></script>
@stop
