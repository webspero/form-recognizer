@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h2 class="text-center text-uppercase">Terms of Use</h2>
				<p class="text-justify">This website (“Site”) is owned and operated by CreditBPO Tech Inc. (“CreditBPO”). These
                    Terms of Use (“Terms of Use”) contain the terms, covenants, conditions, and provisions upon
                    which you (also referred to herein as “User”) may access and use this Site and the content,
                    including (without limitation) the ratings, opinions, and other materials, tools, products,
                    services, publications, and information (collectively, the “Materials”) displayed on the Site..</p>
                <p class="lead">CREDITBPO RESERVES THE RIGHT TO CHANGE THE TERMS AND CONDITIONS OF THESE TERMS OF USE UPON NOTICE, WHICH MAY BE GIVEN BY CREDITBPO POSTING SUCH CHANGE (OR REVISED TERMS OF USE) ON THE SITE, BY E-MAIL, OR BY ANY OTHER REASONABLE WAY. IF A CHANGE IS NOTIFIED BY A POSTING ON THE SITE, IT SHALL BE DEEMED TO TAKE EFFECT WHEN POSTED; IF A CHANGE IS NOTIFIED BY E-MAIL, IT SHALL BE DEEMED TO TAKE EFFECT WHEN THE E-MAIL IS SENT; AND IF A MODIFICATION IS NOTIFIED IN ANY OTHER WAY, IT SHALL BE DEEMED TO TAKE EFFECT WHEN THE RELEVANT NOTICE IS SENT OR ISSUED BY OR ON BEHALF OF CREDITBPO. YOUR CONTINUED USE OF THIS SITE FOLLOWING NOTICE OF SUCH MODIFICATIONS WILL BE CONCLUSIVELY DEEMED YOUR ACCEPTANCE OF ANY CHANGES TO THESE TERMS OF USE. YOU AGREE THAT NOTICE OF CHANGES TO THESE TERMS OF USE ON THE SITE, DELIVERED BY EMAIL, OR PROVIDED IN ANY OTHER REASONABLE WAY CONSTITUTES REASONABLE AND SUFFICIENT NOTICE. </p>
                <p class="text-justify">These Terms of Use shall apply to any CreditBPO mobile application or CreditBPO content on third-party social media services (e.g., a CreditBPO Facebook® page) that reference these Terms of Use. In such instances, the term “Site” shall include the applicable mobile application or CreditBPO content whenever that term is used herein. </p>
                <p class="text-justify">By using this Site, you acknowledge that you have read these Terms of Use, understand them, and agree to be bound by their terms and conditions. If you do not agree to these Terms of Use, you shall neither access nor use this Site. If you register with this Site, the user agreement you enter into as part of that registration process will, rather than these Terms of Use, govern your use of this Site. </p>
                <h3 class="text-center text-uppercase"><strong>Terms and Conditions</strong></h3>
                <div class="terms-section text-justify">
                    <ol class="ol-no-padding">
                        <li>
                            <strong class="text-uppercase"><u>DEFINITIONS AND INTERPRETATION:
                            </u></strong>
                            <ol>
                                <li>"Agreement" means the terms and conditions as detailed herein including all Exhibits, privacy policy, other policies mentioned on the website and will include the references to this agreement as amended, negated, supplemented, varied or replaced from time to time</li>
                                <li>CreditBPO is an online platform that provides a venue to its registered users purchase the detailed credit report from the website at reasonable rates, help banks in loan prequalification, corporate procurement and government procurement.</li>
                                <li>“Account” means the accounts created by the customers on our website in order to use the Services provided by us and require information such as name, email address, password, contact number etc. </li>
                                <li>“Content” means text, graphics, images, music, audio, video, information or other materials.</li>
                                <li>“User Content” means all content that a posts, uploads, publishes, submits or
                                    transmits to be made available through our website.
                                    </li>
                                <li>“SNS” means Social Networking Site such as Facebook, twitter etc.</li>
                                <li>The official language of these terms shall be English. </li>
                                <li>The headings and sub-headings are merely for convenience purpose and shall not be used for interpretation.</li>
                                <li>“Copyright Policy” means the copyright policy of the website which is incorporated into these terms.</li>
                                <li>“Philippine Intellectual Property Code” shall mean the Intellectual Property laws of Philippines.</li>
                                <li>“RA 876” refers to Republic Act No. 876 also known as “The Arbitration Act.”</li>
                                <li>“RA 10173” shall refer to the Republic Act no. 10173 known as the Data Privacy Act of 2012.</li>
                                <li>“RA 8792” shall mean the Republic Act No 8792 also known as the Electronic Commerce Act of 2000.</li>
                                <li>“RA 9184”  shall mean the Republic Act No 9184 also known as the Government Procurement Reform Act.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>ELIGIBILITY OF MEMBERSHIP</u></strong>
                            <ol>
                                <li>Use of the Site is available only to persons who can form legally binding contracts under applicable law. If you are a minor i.e. under the age of 18 years you are not allowed to use the website.</li>
                                <li>Our website reserves the right to terminate your membership and refuse to provide you with access to the Site if we discover that you are under the age of 18 years. The Site is not available to persons whose membership has been suspended or terminated by us for any reason whatsoever. If you are registering as a business entity, you represent that you have the authority to bind the entity to this User Agreement.</li>
                                <li>Except where additional terms and conditions are provided which are product
                                    specific, these terms and conditions supersede all previous representations,
                                    understandings, or agreements and shall prevail notwithstanding any variance with
                                    any other terms of any order submitted. By using the services of our website you
                                    agree to be bound by the Terms and Conditions.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>REGISTRATION</u></strong>
                            <ol>
                                <li>It is mandatory for the users to register on the website if they wish to avail the services provided on the website.</li>
                                <li>If you wish to register with us then you must register for an account with us (an
                                    "Account"). You can register with us by clicking on the relevant link on the website.
                                    </li>
                                <li>You represent and warrant that all required registration information you submit is truthful and accurate, and you will maintain the accuracy of such information. You are responsible for maintaining the confidentiality of your Account login information and are fully responsible for all activities that occur under your Account. You agree to immediately notify us of any unauthorized use, or suspected unauthorized use of your Account or any other breach of security. The website cannot and will not be liable for any loss or damage arising from your failure to comply with the above requirements. You must not share your password or other access credentials with any other person or entity that is not authorized to access your account. Without limiting the foregoing, you are solely responsible for any activities or actions that occur under your website account access credentials. We encourage you to use a “strong” password (a password that includes a combination of upper and lower case letters, numbers, and symbols) with your account. We cannot and will not be liable for any loss or damage arising from your failure to comply with any of the above.</li>
                                <li>You agree to provide and maintain accurate, current and complete information about your Account. Without limiting the foregoing, in the event you change any of your personal information as mentioned above in this Agreement, you will update your Account information promptly.</li>
                                <li>
                                When creating an Account, do not:
                                    <ol>
                                        <li>Provide any false personal information to us (including without limitation a false username) or create any Account for anyone other than yourself without such other person’s permission;</li>
                                        <li>Use a username that is the name of another person with the intent to impersonate that person;</li>
                                        <li>Use a username that is subject to rights of another person without appropriate authorization; or</li>
                                        <li>Use a username that is offensive, vulgar or obscene or otherwise in bad taste.</li>
                                    </ol>
                                </li>
                                <li>We reserve the right to suspend or terminate your Account if any information provided during the registration process or thereafter proves to be inaccurate, false or misleading or to reclaim any username that you create through the Service that violates our Terms. If you have reason to believe that your Account is no longer secure, then you must immediately notify us at <a href="mailto:info@creditbpo.com?Subject=Privacy%20Inquiry">info@creditbpo.com</a>.</p></li>
                                <li>You may not transfer or sell your website account and User ID to another party. If you are registering as a business entity, you personally guarantee that you have the authority to bind the entity to this Agreement.</li>
                                <li>Our Services are not available to temporarily or indefinitely suspended members. Our website reserves the right, in its sole discretion, to cancel unconfirmed or inactive accounts. Our website reserves the right to refuse service to anyone, for any reason, at any time.</li>
                                <li>One individual/entity can own only one account in his/her name.</li>
                                <li>You agree to comply with all local laws regarding online conduct and acceptable content. You are responsible for all applicable taxes. In addition, you must abide by our website’s policies as stated in the Agreement and the website policy documents published on the Website as well as all other operating rules, policies and procedures that may be published from time to time on the Website by Company.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>SCOPE OF SERVICES</u></strong>
                            <p>Our website provides a platform to its registered users to purchase their detailed credit report, helping banks, corporates and government entities in loan prequalification, Corporate Procurement and Government Procurement.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>GRANT OF LICENSE</u></strong>
                            <p>These Terms of Use provide you with a personal, revocable, non-exclusive, non-transferable license to use the Site conditioned on your continued compliance with the terms and conditions of these Terms of Use. You may, on an occasional and irregular basis, print and download Materials on the Site solely for personal and non-commercial use. Only to the extent permitted by an Other Agreement (defined below), you may, on an occasional and irregular basis, print and download Materials on the Site solely for internal business use. In each case, you may not obscure, alter, remove or delete any copyright or other proprietary notices contained in such Materials. With the exception of the foregoing and except as otherwise expressly permitted herein, you may not modify, create derivatives of, copy, distribute, repackage, redistribute, disseminate, broadcast, transmit, reproduce, publish, license, transfer, sell or re-sell, mirror, frame, “deep link”, “scrape”, data mine, or otherwise use or store for subsequent use for any such purpose, any information or Materials obtained from or through this Site, without CreditBPO prior written consent. Further, you may not post any Materials from this Site to forums, newsgroups, list serves, mailing lists, electronic bulletin boards, or other websites, without the prior written consent of CreditBPO. You warrant to CreditBPO that you will not use this Site for any purpose that is unlawful or prohibited by these Terms of Use, including but not limited to attempting or actually (i) disrupting, impairing or interfering with this Site, (ii) collecting any information about other users of this Site, including passwords, accounts or other information, or (iii) systematically extracting data contained on this Site to populate databases for internal or external business use.</p>   
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>INTELLECTUAL PROPERTY RIGHTS</u></strong>
                            <p>All Materials contained on the Site, unless otherwise indicated, are protected by law including, but not limited to, United States copyright, trade secret, and trademark law, as well as Philippine Intellectual Property Code emphasizing but not limited to RA 8792, RA 9184, RA 10173 and international laws and regulations. The Site, its Materials, layout and design are the exclusive property of CreditBPO or its licensors and, except as expressly provided herein, CreditBPO does not grant any express or implied right in any such Materials to you. In particular and without limitation, CreditBPO owns the copyright in the Site as a collective work and/or compilation, any and all databases accessible on the Site, and in the selection, coordination, arrangement, and enhancement of the Materials on the Site. CreditBPO, CreditBPO Investors Service, CreditBPO Analytics, and all other names, logos, and icons identifying CreditBPO and/or CreditBPO products and services are proprietary marks of CreditBPO or its licensors. Third-party trademarks displayed on the Site are the property of their respective owners.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>PRIVACY POLICY</u></strong>
                            <p>Please see CreditBPO Privacy Policy for a summary of CreditBPO personal data collection and use practices with respect to the Site.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>PASSWORD POLICY</u></strong>
                            <p>Your use of certain portions of the Site requires a password. As part of the registration process, you must select a login email address and password and provide CreditBPO with accurate, complete and up-to-date information. Anyone with knowledge of your password can gain access to the restricted portions of the Site and the information available to you. Accordingly, you must keep your password secret. By agreeing to these Terms of Use, you agree to be solely responsible for the confidentiality and use of your respective password, as well as for any communications entered through this Site using your password. You will also immediately notify CreditBPO if you become aware of any loss or theft of your password or any unauthorized use of your password. CreditBPO reserves the right to delete or change a password at any time and for any reason.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>ASSUMPTION OF RISK</u></strong>
                            <p>You use the Internet solely at your own risk and subject to all applicable local, state, national, and international laws and regulations. While CreditBPO has endeavored to create a secure and reliable website, please be advised that the confidentiality of any communication or material transmitted to/from this Site over the Internet cannot be guaranteed. Accordingly, CreditBPO and CreditBPO licensors and suppliers are not responsible for the security of any information transmitted via the Internet, the accuracy of the information contained on the Site, or for the consequences of any reliance on such information. CreditBPO and CreditBPO licensors and suppliers shall have no liability for interruptions or omissions in Internet, network or hosting services. You assume the sole and complete risk of using the Site.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>PAYMENTS</u></strong>
                            <ol>
                                <li>In order to purchase the Credit Reports and other services the user will have to pay
                                    the requisite fees by using payment options presented within
                                    business.creditbpo.com;
                                    </li>
                                <li>Our website uses third party payment providers to receive payments for from users. We are not responsible for delays or erroneous transaction execution or cancellation of orders due to payment issues.</li>
                                <li>We take utmost care to work with 3rd party payment providers, but do not control their systems, processes, technology and work flows, hence cannot be held responsible for any fault at the end of payment providers.</li>
                                <li>Our website reserves the right to refuse to process transactions by users with a prior history of questionable charges including without limitation breach of any agreements by Buyer with us or breach/violation of any law or any charges imposed by Issuing Bank or breach of any policy.</li>
                                <li>The users acknowledge that we will not be liable for any damages, interests or claims etc. resulting from not processing a Transaction/Transaction Price or any delay in processing a Transaction/Transaction Price which is beyond our control.</li>
                                <li>Our website reserves the right to recover the cost of goods, collection charges and lawyers' fees from persons using the Site fraudulently. We reserve the right to initiate legal proceedings against such persons for fraudulent use of the Site and any other unlawful act or acts or omissions in breach of these terms and conditions.</li>
                                <li>We as a merchant shall be under no liability whatsoever in respect of any loss or damage arising directly or indirectly out of the decline of authorization for any Transaction, on Account of the Cardholder having exceeded the preset limit mutually agreed by us with our acquiring bank from time to time.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>REFUND</u></strong>
                            <p>The payments once made for the avail of services shall not be refunded thus the users are advised to exercise due diligence and prudent judgment before making for any of services listed on the website. No request for refund shall be entertained by us.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>YOU AGREE AND CONFIRM:</u></strong>
                            <ol>
                                <li>That you will use the services provided by our website, its affiliates and contracted companies, for lawful purposes only and comply with all applicable laws and regulations while using the Site and transacting on the Site.</li>
                                <li>You will provide authentic and true information in all instances where such information is requested of you. We reserve the right to confirm and validate the information and other details provided by you at any point of time. If upon confirmation your details are found not to be true (wholly or partly), we have the right in our sole discretion to reject the registration and debar you from using the Services of our website and / or other affiliated websites without prior intimation whatsoever.</li>
                                <li>That you are accessing the services available on this Site and transacting at your sole risk and are using your best and prudent judgment before entering into any transaction through this Site.</li>
                                <li>It is possible that the other users (including unauthorized/unregistered users or "hackers") may post or transmit offensive or obscene materials on the Website and that you may be involuntarily exposed to such offensive and obscene materials. It also is possible for others to obtain personal information about you due to your use of the website, and that the recipient may use such information to harass or injure you. We do not approve of such unauthorized uses, but by using the website you acknowledge and agree that we are not responsible for the use of any personal information that you publicly disclose or share with others on the website. Please carefully select the type of information that you publicly disclose or share with others on the Website.</li>
                                <li>
                                You agree that you will not:
                                    <ol>
                                        <li>Restrict or inhibit any other user from using and enjoying the Interactive Features;</li>
                                        <li>Post or transmit any unlawful, threatening, abusive, libelous, defamatory, obscene, vulgar, pornographic, profane, or indecent information of any kind, including without limitation any transmissions constituting or encouraging conduct that would constitute a criminal offense, give rise to civil liability or otherwise violate any local, state, national, or international law;</li>
                                        <li>Post or transmit any information, software, or other material which violates or infringes in the rights of others, including material which is an invasion of privacy or publicity rights or which is protected by copyright, trademark or other proprietary right, or derivative works with respect thereto, without first obtaining permission from the owner or right holder.</li>
                                        <li>Post or transmit any information, software or other material which contains a virus or other harmful component;</li>
                                        <li>Alter, damage or delete any Content or other communications that are not your own Content or to otherwise interfere with the ability of others to access our website</li>
                                        <li>Disrupt the normal flow of communication in an Interactive Area;</li>
                                        <li>Claim a relationship with or to speak for any business, association, institution or other organization for which you are not authorized to claim such a relationship;</li>
                                        <li>Violate any operating rule, policy or guideline of your Internet access provider or online service.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>YOU MAY NOT USE THE SITE FOR ANY OF THE FOLLOWING PURPOSES:</u></strong>
                            <ol>
                                <li>Disseminating any unlawful, harassing, libelous, abusive, threatening, harmful, vulgar, obscene, or otherwise objectionable material.</li>
                                <li>Transmitting material that encourages conduct that constitutes a criminal offense, results in civil liability or otherwise breaches any relevant laws, regulations or code of practice.</li>
                                <li>Interfering with any other person's use or enjoyment of the Site.</li>
                                <li>Breaching any applicable laws;</li>
                                <li>Interfering or disrupting networks or web sites connected to the Site.</li>
                                <li>Making, transmitting or storing electronic copies of materials protected by copyright without the permission of the owner.</li>
                                <li>Without limiting other remedies, we may, in our sole discretion, limit, suspend, or terminate our services and user accounts, prohibit access to our sites, services, applications, and tools, and their content, delay or remove hosted content, and take technical and legal steps to keep users from using our sites, services, applications, or tools, if we think that they are creating problems or possible legal liabilities, infringing the intellectual property rights of third parties, or acting inconsistently with the letter or spirit of our policies. We also reserve the right to cancel unconfirmed accounts or accounts that have been inactive for a period of months, or to modify or discontinue our site, services</li>
                                <li>Further we prohibits the transmission, distribution or posting of any matter which
                                    discloses personal or private information concerning any person or entity, including
                                    without limitation phone number(s) or addresses, credit debit cards, calling card,
                                    User account numbers/ passwords or similar financial information, and home
                                    phone numbers or addresses. Even though all of this is strictly prohibited, there is a
                                    small chance that you might become exposed to such items and you further waive
                                    your right to any damages (from any party) related to such exposure.
                                    </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>NOT A CREDIT REPAIR ORGANIZATION OR CONTRACT</u></strong>
                            <p>We are not a credit repair organization, or similarly regulated organization under other applicable law and does not provide any form of credit repair advice or counselling. We offer access to your credit report and other credit-related information Products, but we do not offer, provide, or furnish any Products, or any advice, counselling, or assistance, for the express or implied purpose of improving your credit record, credit history, or credit rating. By this we mean that we do not claim we can "clean up" or "improve" your credit record, credit history, or credit rating and you acknowledge and agree that you will not purchase, use, or access any of Our Products or the Site for such purposes. These items (credit record, history, and rating) are based on your past or historical credit behavior, and accurate and timely adverse credit information cannot be changed. If you believe that your consumer file contains inaccurate, non-fraudulent information, it is your responsibility to contact the relevant consumer reporting agency, and follow the procedures established by the various consumer reporting agencies related to the removal of such information.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>ENFORCING SECURITY</u></strong>
                            <p>Actual or attempted unauthorized use of any of the Site may result in criminal and/or civil prosecution. To maintain the security and integrity of the Site, CreditBPO reserves the right to review and record activity on the Site to the extent permitted by law and consistent with CreditBPO Privacy Policy. Any information obtained by such reviewing or recording is subject to review by law enforcement organizations in connection with the investigation or prosecution of possible criminal activity on any of the Site. CreditBPO will also comply with all court orders involving requests for such information.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>LINKS TO OTHER WEBSITES</u></strong>
                            <p>CreditBPO may provide links, in its sole discretion, to other websites on the World Wide Web for your convenience in locating related information and services. These websites have not necessarily been reviewed by CreditBPO and are maintained by third parties over which CreditBPO exercises no control. Accordingly, CreditBPO and its directors, officers, employees, agents, representatives, licensors and suppliers (together, the “CreditBPO Parties”) expressly disclaims any responsibility for the content, the accuracy of the information, the quality of products or services provided by or advertised on and/or software downloaded from these third-party websites. Moreover, these links do not imply an endorsement of any third party or any website or the products or services provided by any third party.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>EVENTS BEYOND CREDITBPO CONTROL</u></strong>
                            <p>You expressly absolve and release CreditBPO and CreditBPO licensors and suppliers from any claim of harm resulting from a cause beyond their control, including, but not limited to, the failure of electronic or mechanical equipment or communication lines, telephone or other interconnect problems, computer viruses, unauthorized access, theft, operator errors, severe weather, earthquakes, or natural disasters, strikes or other labor problems, wars, terrorism or governmental restrictions.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>DISCLAIMERS</u></strong>
                            <p>CreditBPO obtains all Materials furnished on the Site from sources believed by it to be
                                accurate and reliable. You expressly agree that (a) the credit ratings and other opinions
                                provided via the Site are, and will be construed solely as, statements of opinion of the
                                relative future credit risk (as defined below) of entities, credit commitments, or debt or
                                debt-like securities and not statements of current or historical fact as to credit worthiness,
                                investment or financial advice, recommendations regarding credit decisions or decisions to
                                purchase, hold or sell any securities, endorsements of the accuracy of any of the data or
                                conclusions, or attempts to independently assess or vouch for the financial condition of any
                                company; (b) the credit ratings and other financial opinions provided via the Site do not
                                address any other risk, including but not limited to liquidity risk, market value risk or price
                                volatility; (c) the credit ratings and other opinions provided via the Site do not take into
                                account your personal objectives, financial situations or needs; (d) each credit rating or
                                other opinion will be weighed, if at all, solely as one factor in any organizational or lending
                                decision made by or on behalf of you; and (e) you will accordingly, with due care, make
                                your own study and evaluation of each lending or procurement decision, and of each issuer
                                and guarantor of, and each provider of credit support for, each loan beneficiary or supplier
                                with which you may consider involvement. For the avoidance of doubt, CreditBPO
                                Materials and opinions may also include quantitative model-based estimates of credit risk
                                and related opinions or commentary published by CreditBPO. Further, you expressly agree
                                that any tools or information made available on the Site are not a substitute for the exercise
                                of core organizational judgment and expertise. You should always seek the assistance of
                                organizational assets for advice on loans, suppliers, the law, or other professional matters.
                                For purposes of this paragraph, CreditBPO defines “credit risk” as the risk that an entity
                                may not meet its contractual, financial obligations as they come due and any estimated
                                financial loss in the event of default.
                                </p>
                        </li>
                        <li>
                            CreditBPO adopts all necessary measures so that the information it uses in assigning a
                            credit rating is of sufficient quality and from sources CreditBPO considers to be reliable,
                            including, when appropriate, independent third-party sources. However, CreditBPO is not
                            an auditor and cannot in every instance independently verify or validate information
                            received in the rating process or in preparing CreditBPO Materials made available on the
                            Site. Because of the possibility of human or mechanical error as well as other factors, the
                            Site and all related Materials are provided on an "AS IS" and “AS AVAILABLE” basis
                            without representation or warranty of any kind, and THE CREDITBPO PARTIES MAKE NO
                            REPRESENTATION OR WARRANTY, EXPRESS OR IMPLIED, TO YOU OR ANY OTHER
                            PERSON OR ENTITY AS TO THE ACCURACY, RESULTS, TIMELINESS, COMPLETENESS,
                            MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE WITH RESPECT TO
                            THE SITE OR ANY RELATED MATERIALS.
                            
                        </li>
                        <li>
                            <p>CreditBPO makes no representation or warranty that any Materials on the Site are
                                appropriate or available for use in any particular locations, and access to them from
                                territories where any of the contents of this Site are illegal is prohibited. If you choose to
                                access this Site from such locations, you do so on your own volition and are responsible for
                                compliance with any applicable local laws, rules, and regulations. CreditBPO may limit the
                                Site’s availability, in whole or in part, to any person, geographic area or jurisdiction we
                                choose, at any time and in our sole discretion. You agree and acknowledge that no oral or
                                written information or advice is given by CreditBPO or any of its employees or agents in
                                respect to the Site shall constitute a representation or a warranty unless such information
                                or advice is incorporated into these Terms of Use by a written agreement. FURTHER, THE
                                MATERIALS, MADE AVAILABLE ON THIS SITE MAY INCLUDE INACCURACIES OR
                                TYPOGRAPHICAL ERRORS, AND THERE MAY BE TIMES WHEN THIS SITE OR ITS
                                MATERIALS ARE UNAVAILABLE. MOREOVER, CREDITBPO MAY MAKE MODIFICATIONS
                                AND/OR CHANGES TO THE SITE OR TO THE MATERIALS DESCRIBED OR MADE
                                AVAILABLE ON THE SITE AT ANY TIME, FOR ANY REASON. YOU ASSUME THE SOLE
                                RISK OF MAKING USE AND/OR RELYING ON THE MATERIALS MADE AVAILABLE ON
                                THE SITE.
                                </p>
                            <p>CREDIT RATINGS AND CREDITBPO MATERIALS ARE NOT INTENDED FOR USE BY ANY PERSON AS A BENCHMARK AS THAT TERM IS DEFINED FOR REGULATORY PURPOSES, AND MUST NOT BE USED IN ANY WAY THAT COULD RESULT IN THEM BEING CONSIDERED A BENCHMARK.</p>
                            <p>CREDITBPO CREDIT RATINGS AND CREDITBPO PUBLICATIONS ARE NOT INTENDED FOR USE BY RETAIL INVESTORS AND IT WOULD BE RECKLESS AND INAPPROPRIATE FOR RETAIL INVESTORS TO USE CREDITBPO CREDIT RATINGS OR CREDITBPO PUBLICATIONS WHEN MAKING AN INVESTMENT DECISION. IF IN DOUBT YOU SHOULD CONTACT YOUR FINANCIAL OR OTHER PROFESSIONAL ADVISER.</p>
                            <p>TO THE EXTENT PERMITTED BY LAW, THE CREDITBPO PARTIES DISCLAIM LIABILITY TO ANY PERSON OR ENTITY FOR ANY INDIRECT, SPECIAL, CONSEQUENTIAL, OR INCIDENTAL LOSSES OR DAMAGES WHATSOEVER ARISING FROM OR IN CONNECTION WITH YOUR ACCESS TO OR USE OF THIS SITE AND ITS MATERIALS OR THE USE OF OR INABILITY TO USE THE SITE OR ANY OF ITS MATERIALS, EVEN IF ANY OF THE CREDITBPO PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSSES OR DAMAGES, INCLUDING, BUT NOT LIMITED TO:</p>
                                <ol>
                                    <li>COMPENSATION, REIMBURSEMENT OR DAMAGES ON ACCOUNT OF THE LOSS OF PRESENT OR PROSPECTIVE PROFITS;</li>
                                    <li>EXPENDITURES, INVESTMENTS OR COMMITMENTS, WHETHER MADE IN THE ESTABLISHMENT, DEVELOPMENT OR MAINTENANCE OF BUSINESS REPUTATION OR GOODWILL;</li>
                                    <li>ANY LOSS OR DAMAGE ARISING WHERE THE RELEVANT BUSINESS ENTITY IS NOT THE SUBJECT OF A PARTICULAR CREDIT RATING ASSIGNED BY CREDITBPO;</li>
                                    <li>LOSS OF DATA;</li>
                                    <li>COST OF SUBSTITUTE MATERIALS;</li>
                                    <li>COST OF CAPITAL;</li>
                                    <li>THE CLAIMS OF ANY THIRD PARTY; OR</li>
                                    <li>ANY SUCH DAMAGES ARISING OUT OF OR IN CONNECTION WITH ANY OTHER REASON WHATSOEVER.</li>
                                </ol>
                            <br>
                            <p>TO THE EXTENT PERMITTED BY LAW, THE CREDITBPO PARTIES DISCLAIM LIABILITY FOR ANY DIRECT OR COMPENSATORY LOSSES OR DAMAGES CAUSED TO ANY PERSON OR ENTITY, INCLUDING BUT NOT LIMITED TO BY ANY NEGLIGENCE (BUT EXCLUDING FRAUD, WILLFUL MISCONDUCT OR ANY OTHER TYPE OF LIABILITY THAT, FOR THE AVOIDANCE OF DOUBT, BY LAW CANNOT BE EXCLUDED) ON THE PART OF, OR ANY CONTINGENCY WITHIN OR BEYOND THE CONTROL OF, THE CREDITBPO PARTIES ARISING FROM OR IN CONNECTION WITH YOUR USE OF OR INABILITY TO USE THIS SITE AND ITS MATERIALS. </p><br>
                            <p>THE USER MUST USE ALL REASONABLE ENDEAVORS TO MITIGATE ANY LOSS OR DAMAGE WHATSOEVER (AND HOWSOEVER ARISING) AND NOTHING IN THESE TERMS OF USE SHALL BE DEEMED TO RELIEVE OR ABROGATE THE USER OF ANY SUCH DUTY TO MITIGATE ANY LOSS OR DAMAGE. </p><br>
                            <p>IN ANY EVENT, TO THE EXTENT PERMITTED BY LAW, THE AGGREGATE LIABILITY OF THE CREDITBPO PARTIES FOR ANY REASON WHATSOEVER RELATED TO ACCESS TO OR USE OF THIS SITE AND ITS MATERIALS SHALL NOT EXCEED THE GREATER OF (A) THE TOTAL AMOUNT PAID BY THE USER FOR SERVICES PROVIDED VIA THIS SITE PURSUANT TO THESE TERMS OF USE DURING THE TWELVE (12) MONTHS IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO LIABILITY, OR (B) Php 5000.00.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>INDEMNITY</u></strong>
                            <p>You agree to indemnify and hold harmless CreditBPO, its licensors and suppliers, all of their
                                affiliates, and all of their respective officers, directors, employees, shareholders,
                                representatives, agents, successors and assigns, from and against any damages, liabilities,
                                costs and expenses (including reasonable attorneys’ and professionals' fees and court
                                costs) arising out of any third-party claims based on or related to your use of the Site or any
                                breach by you of these Terms of Use.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>SUBMISSIONS</u></strong>
                            <p>CreditBPO welcomes your feedback and suggestions, including about how to improve this
                                Site. We and/or our service providers may make available through the Site certain services
                                to which you are able to post comments, information and/or feedback (for example,
                                message boards, blogs, chat features, messaging and/or comment functionalities). Any
                                ideas, suggestions, information, feedback, know-how, material, and any other content
                                (collectively, “Submissions”) posted and/or received through this Site, will be deemed to
                                include a worldwide, royalty-free, fully paid-up, perpetual, irrevocable, nonexclusive,
                                transferable and fully sublicensable (through multiple tiers) right and license for CreditBPO
                                to adopt, publish, reproduce, disseminate, transmit, distribute, copy, use, create derivative
                                works, display, (in whole or part) worldwide, or act on such Submissions without additional
                                approval or consideration, in any form, media, or technology now known or later developed
                                for the full term of any rights that may exist in such Submissions. You hereby waive (a) any
                                claim to the contrary; and (b) any “moral rights” associated with your Submissions. You
                                represent and warrant that you have all rights necessary for you to grant the foregoing
                                license and that each Submission you provide to the Site complies with all applicable laws,
                                rules, and regulations. You are and remain responsible and liable for the content of any
                                Submission. IF YOU DO NOT WISH TO GRANT THE RIGHTS GRANTED IN THIS SECTION,
                                PLEASE DO NOT POST, TRANSMIT OR OTHERWISE MAKE ANY SUBMISSION. ANY
                                SUBMISSIONS MADE ARE DONE SO AT YOUR OWN RISK. Please note, CreditBPO does
                                not control any of the User-submitted Submissions, they do not reflect the opinion of
                                CreditBPO, and CreditBPO does not guarantee their accuracy or endorse any of the
                                opinions expressed. The CreditBPO Parties are not responsible or liable for (i) any
                                Submissions, including, without limitation, any errors or omissions in such Submissions,
                                links or images embedded therein, or results obtained by using any of the same; or (ii) any
                                loss or damage caused by the Submissions or resulting from the use (including without
                                limitation republication) or misuse thereof by any third party, including your reliance
                                thereon.
                                .</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>EXPORT RESTRICTIONS</u></strong>
                            <p>No software or any other materials associated with this Site may be downloaded or otherwise
                                exported or re-exported to countries or persons prohibited under relevant export control
                                laws, including, without limitation, countries against which the Republic of the Philippines
                                has embargoed goods, or to any entity lacking full export approval of the Republic of the
                                Philippines. You are responsible for compliance with the laws of your local jurisdiction
                                regarding the import, export, or re-export of any such materials. By using and/or
                                downloading any such materials from this Site, you represent and warrant that you are not
                                located in, under the control of, or a national or resident of any such country to which such
                                import, export, or re-export is prohibited or are not a person or entity to which such export
                                is prohibited.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>GOVERNING LAW</u></strong>
                            <p>These Terms of Use, including (without limitation) any disputes relating to the Materials on the
                                Site, whether sounding in contract, tort, statute or otherwise, is governed by the laws of the
                                Republic of Philippines , without reference to its conflict of law principles and without
                                regard to the U.N. Convention on Contracts for the International Sale of Goods.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>DISPUTE RESOLUTION</u></strong>
                            <p>THIS PARAGRAPH CONTAINS ARBITRATION AND CLASS ACTION WAIVER PROVISIONS
                                THAT WAIVE THE RIGHT TO A COURT HEARING OR TRIAL OR TO PARTICIPATE IN A
                                CLASS ACTION. PLEASE REVIEW CAREFULLY. Any dispute, controversy or claim arising
                                out of or relating to these Terms of Use, including any disputes relating to the Materials on
                                Site, whether sounding in contract, tort, statute or otherwise, shall be finally resolved by
                                arbitration. Any claim must be brought in the claimant’s individual capacity, and not as a
                                plaintiff or class member in any purported class, collective, representative, multiple
                                plaintiffs, or similar proceeding (“Class Action”). The parties expressly waive any ability to
                                maintain any Class Action in any forum. The arbitration shall be conducted by one arbitrator
                                in English and in accordance with the Philippines Republic Act No 876 known as “The
                                Arbitration Act”. The place of Arbitration shall be the Republic of the Philippines. The
                                decision of the arbitrators shall be binding upon the parties hereto, and the expense of the
                                arbitration (including without limitation the award of attorneys’ fees to the prevailing party)
                                shall be paid as the arbitrators determine. The decision of the arbitrator may be entered by
                                any court of competent jurisdiction. You agree to submit to the jurisdiction of the Republic
                                of the Philippines for the purposes of any judicial proceedings to obtain interim relief and in
                                aid of the arbitration or judicial proceedings to confirm or enforce the award.
                                Notwithstanding the foregoing, CreditBPO may seek preliminary injunctive relief from a
                                court of law in the event of a breach by you.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Term, Termination</u></strong>
                            <p>These Terms of Use will take effect when you first commence using the Site. CreditBPO reserves the right at any time and for any reason to deny you access to the Site or any portion thereof. Termination will be effective without notice.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Waiver</u></strong>
                            <p>Failure to insist on strict performance of any of the terms and conditions of these Terms of Use will not operate as a waiver of any subsequent default or failure of performance. No waiver by CreditBPO of any right under these Terms of Use will be deemed to be either a waiver of any other right or provision or a waiver of that same right or provision at any other time.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Nature of Relationship</u></strong>
                            <p>No joint venture, partnership, employment, or agency relationship exists between you and CreditBPO as a result of these Terms of Use or your utilization of this Site.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Severability</u></strong>
                            <p>The provisions of these Terms of Use are severable. If any provision (or part of any provision) shall be determined to be void or unenforceable, the relevant provision or part of any provision shall be deemed deleted and these Terms of Use, and the validity and enforceability of all remaining provisions (and parts of any provisions) of these Terms of Use, shall not be affected.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Entire Agreement/Reservation of Rights</u></strong>
                            <p>Your rights to use certain Materials available on or through this Site may be subject to separate written agreements with CreditBPO (“Other Agreements”). Particular pages or features of this Site with content supplied by CreditBPO or its licensors may have different or additional terms ("Special Terms"), which will be disclosed to you when you access those pages or features, and by accessing or using such pages and features, you will be deemed to have agreed to the applicable Special Terms. If there is a conflict between these Terms of Use and the Special Terms, the Special Terms will govern with respect to such pages or features or content. In the event of a conflict between these Terms of Use and one or more Other Agreements, the terms of such Other Agreement(s) shall govern and control. With the exception of any Special Terms and Other Agreements, these Terms of Use represent the entire agreement between you and CreditBPO with respect to your use of and material available on or through the Site, and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral, or written between you and CreditBPO with respect to the Site. Any rights not expressly granted herein are reserved. Notwithstanding the foregoing, CreditBPO data collection and usage practices in connection with the Site are as set forth in the Privacy Policy.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Third Party Beneficiaries</u></strong>
                            <p>CreditBPO licensors set forth in these Terms of Use are intended to be third-party beneficiaries of the terms and conditions set forth herein, as applicable to their respective products and services.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Assignment</u></strong>
                            <p>You may not assign your rights and obligations under these Terms of Use without the prior written consent of CreditBPO. Any assignment made in violation of the foregoing prohibition shall be null and void. These Terms of Use shall be binding upon the parties hereto and their respective successors and permitted assigns.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>Contact Information</u></strong>
                            <p>If you would like to contact CreditBPO regarding these Terms of Use or the Privacy Policy, please contact: </p>
                            <p>CreditBPO Tech Inc.</p>
                            <p>Level 10-01 One Global Place</p>
                            <p>Taguig, Philippines 1632</p>
                            <p>E-mail: <a href="mailto:info@creditbpo.com?Subject=Terms%20of%20Use%20Inquiry" target="_top">info@creditbpo.com</a></p>
                            <p>If you choose to contact CreditBPO via e-mail regarding these Terms of Use or the Privacy Policy, respectively, please mark the subject heading of your e-mail “Terms of Use Inquiry” or “Privacy Inquiry.”</p>
                        </li>
                    </ol>
                </div>
				<br/><br/>
				<p align="center"><a class="btn btn-primary" onclick="window.close(); return false;">Close this window</a></p>
			</div>
		</div>
    </div>

</div>

@stop
@section('javascript')
@stop
