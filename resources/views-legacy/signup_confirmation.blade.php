@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Account Activation and New Report Created</h1>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
                    <h1>Congratulations for purchasing your report!</h1>
					<h3>What you should know:</h3>
					<p><b>a.</b> It will take you roughly 30 minutes to answer the questionnaire.</p>
					<p><b>b.</b> Your replies to these questions will be the basis for generating your business credit risk rating. Inaccurate and incomplete replies will result in an inaccurate rating.</p>
					<p><b>c.</b> Your data will be run through our risk rating algorithm.</p>
					<p><b>d.</b> It will take 24-48 hours after submission to email you your Credit Risk Rating report.</p>
					<p><b>e.</b> Please indicate	 your business email address. We may have to get in touch with you for some clarification.</p>
					<p><b>f.</b> All data you submit will be treated with the strictest confidentiality and non-disclosure restriction to third parties.</p>
                    <a href="{{ URL::to('dashboard/index') }}" class="btn btn-primary" style="font-size: 18px; margin-top: 10px;padding: 5px 25px;">Click here to proceed</a></p>
			    </div>
			</div>
		</div>
	</div>
@stop
@section('javascript')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}
,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-59609039-1', 'auto');
ga('send', 'pageview');
</script>
@stop
