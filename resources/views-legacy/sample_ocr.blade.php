<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Test Tesseract Application</title>
<style type="text/css">
#parsed_result table { width: 100%; border-collapse: collapse; }
#parsed_result table { border-top: 1px solid #DDD; border-bottom: 1px solid #DDD; border-left: 1px solid #DDD; }
#parsed_result table tr td { border-right: 1px solid #DDD; text-align: right; }
#parsed_result table tr td:first-child { text-align: left; }
#parsed_result table tr td:nth-child(2) { text-align: left; }
#parsed_result table tr th { font-weight: bold; }
</style>
</head>
<body>
	<div>
		{{Form::open(['route'=>'postTest', 'method'=>'post', 'files'=>true])}}
			<label>File: </label><input type="file" name="imagefile" /><br />
			<p>ImageMagick Settings</p>
			<label>Resolution: </label><input type="text" name="resolution" value="{{ (isset($resolution)) ? $resolution : 300 }}" /><br />
			<label>Quality: </label><input type="text" name="quality" value="{{ (isset($quality)) ? $quality : 100 }}" /><br />
			<p>Textcleaner Settings</p>
			<label><input type="checkbox" name="noclean" checked="checked" /> Use Textcleaner</label><br />
			<label>Options: </label><input type="text" name="options" value="{{ (isset($options)) ? $options : '-g -e stretch -f 25 -o 10 -s 1' }}" /><br />
			<label><input type="checkbox" name="thumbs" /> Create Thumbnails for processed images</label><br />
			<br/>
			<input type="submit" value="OCR convert" />
		{{Form::close()}}
	</div>
	<div>
		@if(isset($content))
		<p>Filesize: {{ $filesize }} bytes</p>
		<p>Benchmark: {{ $benchmark }} seconds </p>
		<br />
			@if($is_pdf && $has_thumbs)
			<div>
				<p>Processed Files:</p>
				<p>After <b>ImageMagick</b> pdf to jpg conversion </p>
				<img src="{{URL::to('/')}}/temp/rasterized_crop.jpg" />
				@if($used_textcleaner)
					<p>After <b>Textcleaner</b> processing on Image </p>
					<img src="{{URL::to('/')}}/temp/rasterized2_crop.jpg" />
				@endif
			</div>
			@endif
		<p><b>After Tesseract OCR - Result</b></p>
		<hr />
		<div style="width:50%; float: left;">
			<textarea id="result" rows="20" cols="90">{{ (isset($content)) ? $content : "" }}</textarea>
		</div>
		<div style="width:50%; float: left;">
			<input type="button" value="Parse Result (Bank Statement)" class="parse" />
			<div id="parsed_result" style="font-size: 12px; font-family: Arial;"></div>
		</div>
		@endif
	</div>
	@if(isset($content))
	<script src="{{ URL::asset('js/1.11.2.jquery.min.js') }}"></script>
	<script type="text/javascript">
		$('.parse').on('click', function(){
			$.ajax({
				url: '{{ URL::to("/") }}' + '/ocr_parsed',
				type: 'post',
				dataType: 'json',
				data: {
					content: $('#result').val()
				},
				success: function(oReturn){
					if(oReturn.length > 0){
						$("#parsed_result").html('<table><tr><th>DATE</th><th>TRANSACTION DESCRIPTION</th><th>CHECK NO.</th><th>DEBIT AMOUNT</th><th>CREDIT AMOUNT</th><th>BALANCE</th></tr></table>');
						for(x in oReturn){
							var sHtml = '<td>' + oReturn[x].date + '</td>';
							sHtml += '<td>' + oReturn[x].desc + '</td>';
							sHtml += '<td>' + oReturn[x].check + '</td>';
							sHtml += '<td>' + oReturn[x].debit + '</td>';
							sHtml += '<td>' + oReturn[x].credit + '</td>';
							sHtml += '<td>' + oReturn[x].balance + '</td>';
							$("#parsed_result table").append('<tr>' + sHtml + '</tr>');
						}
					}
				}
			});
		});
	</script>
	@endif
</body>
</html>