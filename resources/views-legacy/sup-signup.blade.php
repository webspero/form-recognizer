@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('style')
    <style>
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        font-size: 14px !important;
    }
    ul#select2-bank_id-results{
        font-size: 14px !important;
    }
    #question_type{
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
    }
    #question_type option {
        display: none;
    }
    </style>
@stop
    

@section('content')
	<!-- live2support.com tracking codes starts --><div id="l2s_trk" style="z-index:99;">website live chat</div><script type="text/javascript"> var l2slhight=400; var l2slwdth=350; var l2slay_mnst="#l2snlayer {}";var l2slv=3; var l2slay_hbgc="#0097c2"; var l2slay_bcolor="#0097c2"; var l2sdialogofftxt="Live Chat Offline"; var l2sdialogontxt="Live Chat Online"; var l2sminimize=true;    var l2senblyr=true; var l2slay_pos="R"; var l2s_pht=escape(location.protocol); if(l2s_pht.indexOf("http")==-1) l2s_pht='http:'; (function () { document.getElementById('l2s_trk').style.visibility='hidden';
	var l2scd = document.createElement('script'); l2scd.type = 'text/javascript'; l2scd.async = true; l2scd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's01.live2support.com/js/lsjs1.php?stid=26893 &jqry=Y&l2stxt='; var l2sscr = document.getElementsByTagName('script')[0]; l2sscr.parentNode.insertBefore(l2scd, l2sscr); })(); </script><!-- live2support.com tracking codes closed -->

	<div class="container">
		<h1>Supervisor Sign Up Page</h1>
		{{ Form::open( array('route' => 'postSupervisorSignup', 'role'=>'form', 'files' => true)) }}
		<div class="spacewrapper">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
            </div>
            @endif
			<div class="read-only">
				<h4>Registration Details</h4>
				<div class="read-only-text">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 {{$errors->has('firstname') ? 'has-error' : ''}}">
                                {{Form::label('firstname', 'First Name')}}
                                {{Form::text('firstname', $errors->has('firstname') ? Input::old('firstname') : "", array('class' => 'form-control'))}}
                                {{ Form::hidden('recaptcha','', array("id"=>"recaptcha"))}}
                            </div>
                            <div class="col-sm-6 {{$errors->has('lastname') ? 'has-error' : ''}}">
                                {{Form::label('lastname', 'Last Name')}}
                                {{Form::text('lastname', $errors->has('lastname') ? Input::old('lastname') : "", array('class' => 'form-control'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                        {{Form::label('email', 'Email Address')}}
                        {{Form::text('email', $errors->has('email') ? Input::old('email') : "", array('class' => 'form-control'))}}
                        <small>This authorized user email address will receive all updates and reports. It can also be different from the official company email.</small><br/>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 {{$errors->has('password') ? 'has-error' : ''}}">
                                {{Form::label('password', 'Password')}}
                                {{Form::password('password', array('class' => 'form-control'))}}
                            </div>

                            <div class="col-sm-6 {{$errors->has('confirm_password') ? 'has-error' : ''}}">
                                {{Form::label('confirm_password', 'Confirm Password')}}
                                {{Form::password('confirm_password', array('class' => 'form-control'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('role', 'Organization type')}}
                        {{Form::select('role', array(
                            'Bank'=>'Bank',
                            'Non-Bank Financing'=>'Non-Bank Financing',
                            'Corporate'=>'Corporate',
                            'Government'=>'Government',
                            'Contractor'=>'Contractor'
                        ), 'Bank', array('class' => 'form-control'))}}
                    </div>
                    <div class="form-group">
                        {{Form::label('bank_id', 'Organization')}}
                        {{Form::select('bank_id', array(
                        ), null , array('class' => 'form-control'))}}
                    </div>

                    <!-- <div class="form-group">
                        {{Form::label('question_type', 'Questionnaire Type')}}
                        {{Form::select('question_type', array(
                            '' => 'Not Set',
                            '0'=>'Bank',
                            '1'=>'Corporate',
                            '2'=>'Government'
                        ), '' , array('class' => 'form-control', 'readonly'=>'true'))}}
                    </div> -->

                    <div class="form-group">
                        {{Form::label('valid_id_picture', 'Upload Government Issued ID')}}
                        {{Form::file('valid_id_picture', ['style' => 'border: 1px solid #ccc', 'class' => 'form-control']) }}
                    </div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-sm-6">
				<div class="spacewrapper">
					@if($errors->has('captcha'))
						{{ $errors->first('captcha', '<span class="errormessage">:message</span>') }}
					@endif
				</div>

				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" id="submit_btn" class="btn btn-large btn-primary" value="Register">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop

@section('javascript')
    <script src="https://www.google.com/recaptcha/api.js?render=6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M', {action: 'home'}).then(function(token) {
                if(token) $("#recaptcha").val(token);
            });
        });
        $('select#role').change(function(){
            var type = $(this).val();
            $.ajax({
                method: 'GET',
                url: BaseURL+'/admin/organization-type/'+ type,
                success: function(response){
                    var len = response.length;

                    $("#bank_id").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['bank_name'];
                        $("#bank_id").append("<option value='"+id+"'>"+name+"</option>");
                    }
                    $('#bank_id').change();
                }
            })
        });
        $('select#role').change();

        $('#bank_id').change(function(){
            var bank = $(this).val();
            if (bank == null){
                $('#question_type').val('');
            } else{
                $.ajax({
                    method: 'GET',
                    url: BaseURL+'/admin/questionnaire-type/'+ bank,
                    success: function(response){
                        $('#question_type').val(response);
                    }
                })
            }
        });

        $('#bank_id').select2({placeholder: "Please select an organization"});

        $('#other').autocomplete({
            source: BaseURL+'/search_companies',
            minLength: 3,
            select: function(evt, ui) {

            }
        });

    });
    </script>
@stop
