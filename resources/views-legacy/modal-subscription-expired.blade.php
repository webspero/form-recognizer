<!-- alert modal -->
<div class="modal centered-modal" id="modal-subscription-expired" tabindex="-1" role="dialog" id="alertModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  	<div class="text-center">
			<p class="error-text">Your subscription has expired</p> <br>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>