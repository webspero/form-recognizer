<div class="modal" id="modal-add-info" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1> {{ trans('messages.add') }}: <span id = 'pop-hdr-lbl'> </span> </h1>
            </div>
            
            <div class="modal-body" id = 'add-info-body'> </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }} </button>
                <button type="submit" class="btn btn-large btn-primary pop-upd-btn"> {{ trans('messages.save') }} </button>
            </div>
                
        </div>
    </div>
</div>