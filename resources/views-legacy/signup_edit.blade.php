@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
	<style>
		#show_results {
			min-height: 150px !important;
			max-height: 300px !important;
			overflow-y: auto !important;
			box-shadow: inset 0 0 1px 1px #143048;
		}
		ul, li, ul>li {
			list-style : none !important;
		}

		.search_icon {
			background-image: url('/images/icon_search.png') !important;
			height: 20px;
			width: 20px;
			display: block;
		}

		.input-container {
			display: flex !important;
			width: 100%;
		}

		.select2-container--default .select2-selection--single .select2-selection__rendered{
			font-size: 14px !important;
		}
		ul#select2-bank_id-results{
			font-size: 14px !important;
		}
		.centered-modal.in {
			display: flex !important;
		}
		.centered-modal .modal-dialog {
			margin: auto;
		}
		#ui-id-1 {
			height: auto;
			max-height: 300px;
			overflow-x: hidden;
		}
		.simplifiedToggle, .standAloneToggle, .premiumToggle {
			font-size: 13px;
		}

		/* Important part */
		.modal-dialog{
			overflow-y: initial !important
		}
		.modal-body{
			height: 40vh;
			overflow-y: auto;
		}

	</style>
@stop

@section('content')
	<div class="container">
		<h1>{{trans('messages.new_report_title')}}</h1>
			{{ Form::open( array('route' => 'postUpdateNewReport', 'role'=>'form', 'id' => 'formbpo', 'files'=>'true')) }}
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('messages.registration_details')}}</h4>
				<div class="read-only-text">
					@if($errors->has('error'))
						{{ $errors->first('error', '<span class="errormessage">:message</span>') }}
					@endif

					<div class="form-group row">
						<div class="col-md-6{{ ($errors->has('companyname')) ? ' has-error' : '' }}">
							{{ Form::label('companyname', trans('messages.reg_name')) }} *
							<i id="pse_search" name="pse_search"><img height="25px" src="{{ URL::asset('images/search.png') }}" title="Search PSE Company"/></i>
							{{ Form::text('companyname', $entity->companyname, array('class' => 'form-control validate[required] typeahead')) }}
							{{ Form::hidden('pse_id', Input::old('pse_id'), array('class' => 'pseId'))}}

							@if($errors->has('companyname'))
								{{ $errors->first('companyname', '<span class="errormessage">:message</span>') }}
							@endif
						</div>

						<div class="col-md-6">
							{{ Form::label('country', trans('business_details1.primary_business_country')) }} * 
							<!-- &nbsp
							<i  data-toggle="modal" data-target="#modalMap"><img height="25px" src="{{ URL::asset('images/location.png') }}" /></i> -->
							{{ Form::select('country', $countries, 'PH', array('id' => 'country', 'class' => 'form-control validate[required]')) }}
						</div>
					</div>

					<div class="form-group row" id="province_city" >
						<div class="col-md-6">
							{{ Form::label('province', trans('business_details1.primary_business_province'))}}&nbsp
							<i  data-toggle="modal" data-target="#modalMap"><img height="25px"src="{{ URL::asset('images/location.png') }}" /></i>
							{{ Form::select('province',$province, $currentProvince,  array('id' => 'province', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::label('city', trans('business_details1.primary_business_city')) }} *
							{{ Form::select('city', $city, $currentCity, array('id' => 'city', 'class' => 'form-control')) }}
						</div>
					</div>
					
					<div class="form-group row"> 
							<div class ="col-md-4 col-lg-4">
								<div class="input-container">
								{{ Form::label('industrymain', trans('business_details1.industry_main'))}}&nbsp
								<i id="live_search" name="live_search"><img height="25px" src="{{ URL::asset('images/search.png') }}" /></i>
								</div>
								{{ Form::select('industry_main', $industry_main , $entity->industry_main_id, array('id' => 'industry_main', 'class' => 'form-control')) }}
							</div>
							<div class ="col-md-4 col-lg-4">
								{{ Form::label('industrysub', trans('business_details1.industry_sub') ) }} *
								{{ Form::select('industry_sub', $industrySub, $entity->industry_sub_id, array('id' => 'industry_sub', 'class' => 'form-control')) }}
							</div>
							<div class ="col-md-4 col-lg-4">
								{{ Form::label('industryrow', trans('business_details1.industry') ) }} *
								{{ Form::select('industry_row', $industryRow , $entity->industry_row_id, array('id' => 'industry_row', 'class' => 'form-control')) }}
							</div>
					</div>
					

					<div class="form-group row">
						<div class="col-md-6{{ ($errors->has('bank')) ? ' has-error' : '' }}">
								@if(Auth::user()->is_contractor == 1)
									{{ Form::label('who', trans('business_details1.processingFee')) }} *
									<div class="input-group">
										<div id="radioBtn" class="btn-group  btn-group-justified">
											<a class="btn btn-primary btn active standAloneToggle" data-toggle="who" data-title="0">Standalone: Php {{$reportPrices['standalone']}}.00 </a>
								@else
									{{ Form::label('who', trans('business_details1.typeOfReport')) }} *
									<div class="input-group">
										<div id="radioBtn" class="btn-group  btn-group-justified">
											<a class="btn btn-primary btn active simplifiedToggle" data-toggle="who" data-title="2">Simplified Rating: <br> Php {{number_format($reportPrices['simplified'], 2, '.', ',')}} </a>
											<a class="btn btn-primary btn notActive standAloneToggle" data-toggle="who" data-title="0">Standalone: <br> Php {{number_format($reportPrices['standalone'], 2, '.', ',')}} </a>
											<a class="btn btn-primary btn notActive premiumToggle" data-toggle="who" data-title="1">Premium: <br> Php {{number_format($reportPrices['premium'], 2, '.', ',')}}</a>
								@endif
								<input type="hidden" name="who" id="who">
								{{ Form::hidden('simplifiedType', $reportType->simplified_type,  array('id' => 'simplifiedType')) }}
								{{ Form::hidden('standaloneType', $reportType->standalone_type,  array('id' => 'standaloneType')) }}
								{{ Form::hidden('premiumType', $reportType->premium_type,  array('id' => 'premiumType')) }}
								</div>
							</div>
						</div>
						<div class="col-md-6{{ ($errors->has('entity_type')) ? ' has-error' : '' }}">
							{{ Form::label('entity_type', trans('business_details1.businessEntityType')) }} *
							{{-- <input type="checkbox" name="entity_type" data-toggle="toggle" data-on="Sole Proprietorship" data-off="Corporation" data-onstyle="success" data-offstyle="primary" /> --}}
							<div class="input-group">
								<div id="radioBtn" class="btn-group  btn-group-justified">
									<a class="btn btn-primary btn notActive soleProprietorship" data-toggle="entity_type" data-title="Sole Proprietorship">Sole Proprietorship</a>
									@if(Auth::user()->is_contractor == 1)
									<a class="btn btn-primary btn active" data-toggle="entity_type" data-title="Corporation">{{trans('business_details1.corporationPartnership')}}</a>
									@else
									<a class="btn btn-primary btn active corporation" data-toggle="entity_type" data-title="Corporation">Corporation</a>
									@endif
								</div>
								<input type="hidden" name="entity_type" id="entity_type">
							</div>
                        </div>
					</div>

					<div class="form-group row">
                        <div class="col-md-6">
							{{ Form::label('serial_key', trans('messages.reg_key')) }}
							{{ Form::text('serial_key', Input::old('serial_key'), array('class' => 'form-control', 'id' => 'serial_key')) }}
							{{ Form::hidden('requested_by', 1) }}

							{{ Form::hidden('entity', $entity, array('id' => 'entity')) }}
							{{ Form::hidden('entity_id', $entity->entityid, array('id' => 'entity_id')) }}

							@if($errors->has('serial_key'))
							<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">{{ $errors->first('serial_key') }}</span>
							@endif
						</div>
						<div class="col-md-6{{ ($errors->has('discount_code')) ? ' has-error' : '' }}">
							{{ Form::label('discount_code', trans('messages.reg_discount')) }}
							{{ Form::text('discount_code', Input::old('discount_code'), array('class' => 'form-control', 'id' => 'discount_code')) }}

							@if($errors->has('discount_code'))
							<span class="errormessage">{{ $errors->first('discount_code') }}</span>
							@endif
						</div>
					</div>

					@if(Auth::user()->is_contractor != 1)
					<div class="form-group bank-control row">
						<div class="col-md-12{{ ($errors->has('bank')) ? ' has-error' : '' }}">
							{{ Form::label('bank', trans('business_details1.requesting_bank')) }} *
							{{ Form::select('bank_dd', $bankList, Input::old('bank'), array('id' => 'bank_dd', 'class' => 'form-control')) }}
							{{ Form::hidden('bank', Input::old('bank', 1)) }}

							@if($errors->has('bank'))
								{{ $errors->first('bank', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					@else
						<input type="hidden"  id="bank_contractor" value="{{Auth::user()->is_contractor}}">
					@endif

					<div class="form-group bank-control row" id="loa" hidden>
						<div class="col-md-4">
							{{ Form::label('bank', 'Upload Letter of authorization (optional)') }}
						</div>
						<div class="col-md-8">
							{{ Form::file('letter_of_auth', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control', 'id' => 'letter_of_auth'], array('multiple'=>false)) }}
						</div>
					</div>

					
					<div class="form-group row standAlone">
						<div class="col-md-12">
							<label><input type="checkbox" name="use_previous" value="1"> {{trans('messages.reg_use_prev')}}</label>
							<select name="use_previous_entity">
								@foreach($reportList as $r)
								<option value="{{ $r->entityid }}">{{ $r->companyname }} - {{ date('F j, Y', strtotime($r->created_at)) }}</option>
								@endforeach
							</select>
                            <br/>
                            <small class="small_text accepted-format-1">{{trans('messages.reg_hint_one')}} <b>{{trans('messages.reg_hint_two')}}</b>{{trans('messages.reg_hint_three')}}</small>
						</div>

						
					</div>
					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<p style="padding-left: 20px; margin: 0;">
					<label>
						<input id="agree" name="agree" style="display: inline-block;" type="checkbox" value="true" class="validate[required] checkbox" data-errormessage-value-missing="Please read the Terms of Use and Privacy Policy, and then check this box" />
						{{ trans('business_details1.iHaveRead') }}
						<a href="{{ URL::to('/termsofuse') }}" class="btn btn-primary" target="_blank">Terms of Use</a>
						{{ trans('business_details1.and') }}
						<a href="{{ URL::to('/privacypolicy') }}" class="btn btn-primary" target="_blank">Privacy Policy</a>
						<img src="{{ URL::asset('images/thumb_12601.png') }}" />
					</label>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="button"  id="submit_btn"  class="btn btn-large btn-primary" value="{{trans('messages.confirm_btn')}}">
				</div>
			</div>
		</div>

		{{ Form::close() }}
		<div id="loading_screen"></div>
	</div>

	<!-- MODAL FOR PSE COMPANY SEARCH -->
	<div id="pse_search_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">PSE Company Search</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" id="search_company" class="form-control" placeholder="search ... "/>
					</div>
					@csrf
					<div class="show_pse_results" id="show_pse_results">
					</div>
				</div>
				<div class="modal-footer">
					<span class="save_status"></span>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="btnSavePse" class="btn btn-primary btn-setting-save">Save</button>
				</div>
			</div>
		</div>
	</div>

	<!-- modal should have min-height and max-height body and must be scrollable. -->
	<div id="industry_live_search" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Industry Search</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" id="search_industry" class="form-control" placeholder="search ... "/>
					</div>
					<!-- <input type="hidden" id="token" value=/> -->
					@csrf
					<div class="show_results" id="show_results">
					
					</div>
				</div>
				<div class="modal-footer">
					<span class="save_status"></span>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="btnSave" class="btn btn-primary btn-setting-save">Save</button>
				</div>
			</div>
		</div>
	</div>
		<!-- MODAL FOR MAP GEOLOCATION -->
		<div class="modal fade" id="modalMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body mb-0 p-0">
						<div class="pac-card" id="pac-card">
					      <div>
					        
					        <div id="type-selector" class="pac-controls">
					          <input
					            type="radio"
					            name="type"
					            id="changetype-all"
					            checked="checked"
					          />
					          <label for="changetype-all">All</label>

					          <input type="radio" name="type" id="changetype-establishment" />
					          <label for="changetype-establishment">Establishments</label>

					          <input type="radio" name="type" id="changetype-address" />
					          <label for="changetype-address">Addresses</label>

					          <input type="radio" name="type" id="changetype-geocode" />
					          <label for="changetype-geocode">Geocodes</label>
					        </div>
					        <div id="strict-bounds-selector" class="pac-controls">
					          <input type="checkbox" id="use-strict-bounds" value="" />
					          <label for="use-strict-bounds">Strict Bounds</label>
					        </div>
					      </div>
					      <div id="pac-container">
					        <input id="pac-input" type="text" placeholder="Enter a location" />
					      </div>
				    	</div>
				    
				    <div id="map" style="height: 500px;"></div>

				    <div id="infowindow-content">
				      <img src="" width="16" height="16" id="place-icon" />
				      <span id="place-name" class="title"></span><br />
				      <span id="place-address"></span>
				    </div>
					</div>
					<div class="modal-footer justify-content-center">
					<button type="button" class="btn btn-info btn-md" onclick="saveLocation()"> Save Location</button>
						<button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Close <i class="fas fa-times ml-1"></i></button>
					</div>
				</div>
		</div>
	  </div>
@stop
@section('javascript')
	
	<script type="text/javascript">
		
		var URL = "{{ URL::to('/') }}";
		
		$('#discount_code').keyup(function(){
			getDiscountCode();
		});

		$('#serial_key').change(function(){
			var serial_key = $(this).val();
			if(serial_key.trim() != ''){
				$('#discount_code').prop( "disabled", true );
			}else{
				$('#discount_code').prop( "disabled", false );
			}
		});
		
		function getDiscountCode(){
			$("#bank_dd").prop("disabled", false);
			$.ajax({
				url: URL + '/get_discount_organization',
				type: 'POST',
				data: {
					discount_code: $('#discount_code').val()
				},
				success: function(response){
					if(response != ''){
						$('#bank_dd').val(response);
						$('#bank_dd').val(response).trigger('change');
						$("#bank_dd").prop("disabled", true);
					}	
				}
			});
		}

		
	</script>

	<script>
		var select_city_code = null;
		var siteUrl = "{{ URL::to('/') }}";
	</script>
	<script src="{{ URL::asset('js/signup_map.js')}}"></script>
	<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCsVwIax_SHupaPq7goAvxZPYe-U_1_zE&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
	<script src="{{ URL::asset('js/signup2.js') }}"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#country').change(function(){
				if($(this).val() == 'PH'){
					$('#province_city').show();
				}else{
					$('#province_city').hide();
				}
			});
			
			$('.typeahead').autocomplete({
				showHintOnFocus: true,
				source: BaseURL+'/company_search',
				minLength: 2,
				select: function (event,ui) {
					var item = ui.item;
					if (item) {
						$('.typeahead').val(item.label);
						$('.pseId').val(item.id);
						if(item.province && item.city){
							select_city_code= item.city;
							$('#province').val(item.province);
							$('#province').change();
						}
					}
				}
			});

			$(".premiumToggle").on("click",function(){
				$("#loa").show();
				$("#pse_search").hide();
			});

			$(".standAloneToggle").on("click",function(){
				$("#loa").hide();
				$("#pse_search").show();
			});

			$(".simplifiedToggle").on("click",function(){
				$("#loa").hide();
				$("#pse_search").show();
			});

			$(".premiumToggle").on("click",function(){
			$("#loa").show();
			$("#pse_search").hide();
		});

		$(".standAloneToggle").on("click",function(){
			$("#loa").hide();
			$("#pse_search").show();
		});

		$(".simplifiedToggle").on("click",function(){
			$("#loa").hide();
			$("#pse_search").show();
		});

		// Available report type
		if($("#premiumType").val() == 1 && $("#standaloneType").val() == 0 && $("#simplifiedType").val() == 0){
			$('#country').val("PH").change();
            $('#country').prop('disabled', true);
		}

		if($("#premiumType").val() == 0){
			$('.premiumToggle').hide();
		}else{
			$('.premiumToggle').click();
		}

		if($("#standaloneType").val() == 0){
			$('.standAloneToggle').hide();
		}else{
			$('.standAloneToggle').click();
		}

		if($("#simplifiedType").val() == 0){
			$('.simplifiedToggle').hide();
		}else{
			$('.simplifiedToggle').click();
		}

			$(document).ready(function() {
				$('.typeahead').autocomplete({
					showHintOnFocus: true,
					source: BaseURL+'/company_search',
					minLength: 2,
					select: function (event,ui) {
						var item = ui.item;
						if (item) {
							$('.typeahead').val(item.label);
							$('.pseId').val(item.id);
							if(item.province && item.city){
								select_city_code = item.city;
								$('#province').val(item.province);
								$('#province').change();
								if(item.industry_main_id)
								{
									$('#industry_main option[value="'+item.industry_main_id+'"]').prop('selected', true)
									$('#industry_main').change();								
								}
							}
						}
					}
				});
			});
		});
	</script>
@stop
@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
	.toggle{ 
		width:100% !important; 
	}
	#pac-input{
		background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 600px;
	}
	#infowindow-content .title {
        font-weight: bold;
      }
      #infowindow-content {
        display: none;
      }
      #map #infowindow-content {
        display: inline;
      }
      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }
      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }
      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }
      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #pac-input:focus {
        border-color: #4d90fe;
      }
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
</style>
@stop
