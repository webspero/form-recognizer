
	<div class="row">
        {{ Form::open( array('route' => array('postBranches', $entity_id), 'role'=>'form', 'id' => 'branches-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
            @for($i = 0; $i < 1; $i++)						
            <div class="col-md-8{{ ($errors->has('branch_address.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('branch_address'.$i.'', 'Address') }}
            {{ Form::text('branch_address['.$i.']', Input::old('branch_address.'.$i.''), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-3{{ ($errors->has('branch_owned_leased.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('branch_owned_leased'.$i.'', trans('business_details1.branches_owned_leased')) }}
            {{ Form::select('branch_owned_leased['.$i.']', 
                array('' => trans('messages.choose_here'), 
                'br_owned' => trans('business_details1.br_owned'),
                'br_leased' => trans('business_details1.br_leased')
                ), Input::old('branch_owned_leased.'.$i.''), array('id' => 'branch_owned_leased'.$i.'', 'class' => 'form-control'))
            }}
            </div>
            <div class="col-md-3{{ ($errors->has('branch_contact_person.'.$i.'')) ? ' has-error' : '' }}">
            
            {{ Form::label('branch_contact_person'.$i.'', trans('business_details1.b_contact_person')) }}
            {{ Form::text('branch_contact_person['.$i.']', Input::old('branch_contact_person.'.$i.''), array('class' => 'form-control')) }}
            </div>							
            <div class="col-md-3{{ ($errors->has('branch_phone.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('branch_phone'.$i.'', trans('business_details1.b_contact_phone')) }}
            {{ Form::text('branch_phone['.$i.']', Input::old('branch_phone.'.$i.''), array('class' => 'form-control')) }}
            </div>	
            <div class="col-md-3{{ ($errors->has('branch_email.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('branch_email'.$i.'', trans('business_details1.b_contact_email')) }}
            {{ Form::text('branch_email['.$i.']', Input::old('branch_email.'.$i.''), array('class' => 'form-control')) }}
            </div>								
            <div class="col-md-2{{ ($errors->has('branch_job_title.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('branch_job_title'.$i.'', trans('business_details1.b_job_title')) }}
            {{ Form::text('branch_job_title['.$i.']', Input::old('branch_job_title.'.$i.''), array('id' => 'branch_job_title'.$i.'', 'class' => 'form-control')) }}
            </div>                          
            <div class="clearfix"></div>
            <hr />
            @endfor
        </div>

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

        {{ Form::close() }}
	</div>
