@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Spouse Details</h1>
		    {{ Form::model($entity, ['route' => ['putSpouseUpdate', $entity->spouseid], 'method' => 'put']) }}
		@else
			<h1>Add: Spouse Details</h1>
			{{ Form::open( array('route' => 'postReputation', 'role'=>'form')) }}
		@endif
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Personal Details</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('firstname')) ? ' has-error' : '' }}">
							{{ Form::label('firstname', 'First Name') }} *
							{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('middlename')) ? ' has-error' : '' }}">
							{{ Form::label('middlename', 'Middle Name') }} *
							{{ Form::text('middlename', Input::old('middlename'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('lastname')) ? ' has-error' : '' }}">
							{{ Form::label('lastname', 'Last Name') }} *
							{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('nationality')) ? ' has-error' : '' }}">
							{{ Form::label('nationality', 'Nationality') }} *
							{{ Form::text('nationality', Input::old('nationality'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('birthdate')) ? ' has-error' : '' }}">
							{{ Form::label('birthdate', 'Birthdate') }} *
							{{ Form::text('birthdate', Input::old('birthdate'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('profession')) ? ' has-error' : '' }}">
							{{ Form::label('profession', 'Profession') }} *
							{{ Form::text('profession', Input::old('profession'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('email')) ? ' has-error' : '' }}">
							{{ Form::label('email', 'E-mail Address') }} *
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('phone')) ? ' has-error' : '' }}">
							{{ Form::label('phone', 'Phone Number') }} *
							{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
							{{ Form::label('tin_num', 'TIN (e.g. 111-111-111 or 111-111-111-111A)') }} *
							{{ Form::text('tin_num', Input::old('tin_num'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
					<input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/busowner.js') }}"></script>
@stop
