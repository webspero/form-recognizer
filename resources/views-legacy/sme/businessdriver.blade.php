	<div class="row">
        {{ Form::open( array('route' => array('postBusinessdriver', $entity_id), 'role'=>'form', 'id' => 'bizdriver-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                @for($i = 0; $i < 1; $i++)
                <div class="col-md-5{{ ($errors->has('businessdriver_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('businessdriver_name'.$i.'', trans('business_details2.bdriver_name')) }}
                {{ Form::text('businessdriver_name['.$i.']', Input::old('businessdriver_name.'.$i.''), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-5{{ ($errors->has('businessdriver_total_sales.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('businessdriver_total_sales'.$i.'', trans('business_details2.bdriver_share')) }}
                {{ Form::text('businessdriver_total_sales['.$i.']', Input::old('businessdriver_total_sales.'.$i.''), array('class' => 'form-control')) }}
                </div>
                @endfor
            </div>

            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>
		{{ Form::close() }}
	</div>