{{ Form::open( array('route' => array('postUploadUbill', $entity_id), 'role'=>'form', 'files' => true, 'id'=>'ubill-upload-expand-form')) }}
   <div class="row">
         <div class = 'col-md-12'>
            <div class="alert alert-danger pop-error-msg"
               role="alert" style = 'display: none;'>
            </div>
         </div>
   </div>

   <div class="row file-container" id="upload-file-container">
         <div class="form-group">
            <div class="col-md-12">
               <label for="ubillFile">{{ __('Upload File:') }}</label>
               {{ Form::file('ubill_file[]', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'required', "multiple" => "multiple"]) }}
            </div>
         </div>
   </div>

   <div class="row">
         <div class="col-md-11" style="margin-top: 2px;">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
         </div>
   </div>
{{ Form::close() }}

@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
    <script type="text/javascript" src="{{URL::asset('js/bsis_upload.js')}}"></script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
