
	<div class="row">
        {{ Form::open( array('route' => array('postSMEPlanFacilityCreate', $entity_id), 'role'=>'form', 'id' => 'ecf-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                <div class="col-md-12"><h3>{{trans('ecf.credit_facilities_title')}} <a href="#" class="popup-hint popup-hint-content" data-hint="{{ Lang::get('ecf.ecf', array(), 'en') }}<hr/>{{ Lang::get('ecf.ecf', array(), 'fil') }}">More info...</a></h3></div>
                <div class="col-md-6{{ ($errors->has('lending_institution')) ? ' has-error' : '' }}">
                    {{ Form::label('lending_institution', trans('ecf.lending_institution')) }}
                    {{ Form::select('lending_institution', array(
                    '' => trans('messages.choose_here'), 
                    trans('ecf.banks')          => trans('ecf.banks'), 
                    trans('ecf.suppliers')      => trans('ecf.suppliers'), 
                    trans('ecf.guarantors')     => trans('ecf.guarantors'), 
                    trans('ecf.other_lenders')  => trans('ecf.other_lenders')
                    ), Input::old('lending_institution'), array('id' => 'lending_institution', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('plan_credit_availment')) ? ' has-error' : '' }}">
                    {{ Form::label('plan_credit_availment', trans('ecf.amount_of_line')) }} *
                    {{ Form::text('plan_credit_availment', Input::old('plan_credit_availment'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('purpose_credit_facility')) ? ' has-error' : '' }}">
                    {{ Form::label('purpose_credit_facility', trans('ecf.purpose_of_cf')) }} *
                    {{ Form::select('purpose_credit_facility', array(
                    '' => trans('messages.choose_here'),
                    '1' => trans('ecf.working_capital'),
                    '2' => trans('ecf.expansion'),
                    '3' => trans('ecf.takeout'),
                    '5' => trans('ecf.supplier_credit'),
                    '6' => trans('ecf.equipment_purchase'),
                    '7' => trans('ecf.guarantee'),
                    '4' => trans('ecf.other_specify')
                    ), Input::old('purpose_credit_facility'), array('id' => 'purpose_credit_facility', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-12" style = 'display: none;'>
                    {{ Form::label('purpose_credit_facility_others', trans('ecf.purpose_of_cf_others')) }}
                    {{ Form::text('purpose_credit_facility_others', Input::old('purpose_credit_facility_others'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('credit_terms')) ? ' has-error' : '' }}" style = 'display: none;'>
                    {{ Form::label('credit_terms', trans('ecf.credit_terms')) }} *
                    {{ Form::text('credit_terms', Input::old('credit_terms'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('outstanding_balance')) ? ' has-error' : '' }}">
                    {{ Form::label('outstanding_balance', trans('ecf.outstanding_balance')) }}
                    {{ Form::text('outstanding_balance', Input::old('outstanding_balance'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('experience')) ? ' has-error' : '' }}">
                    {{ Form::label('experience', trans('ecf.experience')) }} *
                    {{ Form::select('experience', array(
                    '' => trans('messages.choose_here'),
                    '1' => trans('messages.poor'),
                    '2' => trans('messages.fair'),
                    '3' => trans('messages.prompt'),
                    ), Input::old('experience'), array('id' => 'experience', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('other_remarks')) ? ' has-error' : '' }}">
                    {{ Form::label('other_remarks', trans('ecf.other_remarks')) }}
                    {{ Form::text('other_remarks', Input::old('other_remarks'), array('class' => 'form-control')) }}
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-12"><h3>{{trans('ecf.existing_collateral')}} <a href="#" class="popup-hint popup-hint-content" data-hint="{{ Lang::get('ecf.existing_collateral_hint', array(), 'en') }}<hr/>{{ Lang::get('ecf.existing_collateral_hint', array(), 'fil') }}">More info...</a></h3></div>
                <div class="col-md-6{{ ($errors->has('cash_deposit_details')) ? ' has-error' : '' }}">
                    {{ Form::label('cash_deposit_details', trans('ecf.type_of_deposit')) }}
                    {{ Form::text('cash_deposit_details', Input::old('cash_deposit_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('cash_deposit_amount')) ? ' has-error' : '' }}">
                    {{ Form::label('cash_deposit_amount', trans('ecf.deposit_amount')) }}
                    {{ Form::text('cash_deposit_amount', Input::old('cash_deposit_amount'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('securities_details')) ? ' has-error' : '' }}">
                    {{ Form::label('securities_details', trans('ecf.marketable_securities')) }}
                    {{ Form::text('securities_details', Input::old('securities_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('securities_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('securities_estimated_value', trans('ecf.marketable_securities_value')) }}
                    {{ Form::text('securities_estimated_value', Input::old('securities_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('property_details')) ? ' has-error' : '' }}">
                    {{ Form::label('property_details', trans('ecf.property')) }}
                    {{ Form::text('property_details', Input::old('property_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('property_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('property_estimated_value', trans('ecf.property_value')) }}
                    {{ Form::text('property_estimated_value', Input::old('property_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('chattel_details')) ? ' has-error' : '' }}">
                    {{ Form::label('chattel_details', trans('ecf.chattel')) }}  <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('ecf.chattel_hint')}}">More info...</a>
                    {{ Form::text('chattel_details', Input::old('chattel_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('chattel_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('chattel_estimated_value', trans('ecf.chattel_value')) }}
                    {{ Form::text('chattel_estimated_value', Input::old('chattel_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('others_details')) ? ' has-error' : '' }}">
                    {{ Form::label('others_details', trans('ecf.others')) }}
                    {{ Form::text('others_details', Input::old('others_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('others_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('others_estimated_value', trans('ecf.others_value')) }}
                    {{ Form::text('others_estimated_value', Input::old('others_estimated_value'), array('class' => 'form-control')) }}
                </div>
            </div>
            
            {{ Form::token() }}
        
		{{ Form::close() }}
	</div>
    
<script type="text/javascript">
    $('#purpose_credit_facility').change(function() {
        
        $('#purpose_credit_facility_others').parent().hide();
        $('#credit_terms').parent().hide();
        
        if (4 == $(this).val()) {
            $('#purpose_credit_facility_others').parent().show();
        }
        else if (5 == $(this).val()) {
            $('#credit_terms').parent().show();
        }
        else {
            
        }
    });
</script>
