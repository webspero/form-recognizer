@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>Financial Statements</h1>
    <div class="spacewrapper">
        <div class="read-only">
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
            @endif
            <div class="read-only-text" id="fs_upload">
                <div class="col-md-12">
                    <p style="text-align: justify;">{{ trans('business_details1.financial_statement_template_hint') }}</p>
                    <br />
                    <!-- <a href="https://creditbpo.com/s/FS-Input-Template.xls" target="_blank"> -->
                    <a  class='btn btn-link' id='download_fs_link' href="{{ URL::to('/') }}/getFsTemplateFile/{{ $entity->entityid }}">
                    {{ trans('business_details1.financial_statement_template_link') }}
                    </a>
                </div>
                <div id = 'fs-template-list-cntr' class="details-row">
                    <div class = 'reload-fs-template'>
                        <div id = 'expand-fs-template-form' class = 'spacewrapper'></div>
                            @if ($entity->status == 0 or $entity->status == 3)
                                <span class="details-row" style="text-align: right;">
                                    @if ($entity->loginid == Auth::user()->loginid)
                                        <a id = 'show-fs-template-expand' href="{{ URL::to('/') }}/sme/upload_fs_template/{{ $entity->entityid }}" class="navbar-link">Upload {{ trans('business_details1.financial_statement_template') }}</a>
                                    @endif
                                </span>
                                {{-- <span class="details-row" style="text-align: right;">
                                    @if ($entity->loginid == Auth::user()->loginid)
                                        @if ($entity->is_paid == 1 || $entity->discount_code)
                                        <a href="{{ URL::to('/') }}/sme/premiumsuccess/{{ $entity->entityid }}">Skip this step</a>
                                        @else
                                        <a href="{{ URL::to('/') }}/payment/selection?id={{ $entity->entityid }}">Skip this step</a>
                                        @endif
                                    @endif
                                </span> --}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ URL::asset('js/cmn-lib/cmn-upd-info.js') }}"></script>
    <script type="text/javascript" > 
        function expandCompForm()
        {
            /** Variable Definition    */
            var tab_cntr    = $('#fs_upload');
            var expand_form = '#expand-fs-template-form';
            var upd_form    = '#fs-template-expand-form';
            var upd_btn     = '#show-fs-template-expand';
            
            //-----------------------------------------------------
            //  Binding S560.12: #show-fs-template-expand
            //      Launch the Expander Form for FS Template
            //-----------------------------------------------------
            tab_cntr.on('click', upd_btn, function() {
                initExpandForm(expand_form, upd_form, $(this).attr('href'));
                return false;
            });
        }
        $(document).ready(function(){
            expandCompForm();
        })
    </script>
@stop
