	<div class="row">
        {{ Form::open( array('route' => array('postPhpm', $entity_id), 'role'=>'form', 'id' => 'rev-phpm-form')) }}
            {{ trans('condition_sustainability.future_growth_relation') }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                    @for($i = 0; $i < 1; $i++)
                    <div class="col-md-6{{ ($errors->has('current_year.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('current_year'.$i.'', trans('condition_sustainability.within_12_mons')) }} *
                    @if(isset($entity))
                        {{ Form::text('current_year['.$i.']', ($errors->has('current_year.'.$i.'')) ? Input::old('current_year.'.$i.'') : $entity[$i]->current_year, array('class' => 'form-control')) }}
                    @else
                        {{ Form::text('current_year['.$i.']', Input::old('current_year.'.$i.''), array('class' => 'form-control')) }}
                    @endif
                    </div>					
                    <div class="col-md-6{{ ($errors->has('new_year.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('new_year'.$i.'', trans('condition_sustainability.within_24_mons')) }} *
                    @if(isset($entity))
                        {{ Form::text('new_year['.$i.']', ($errors->has('new_year.'.$i.'')) ? Input::old('new_year.'.$i.'') : $entity[$i]->new_year, array('class' => 'form-control')) }}
                    @else
                        {{ Form::text('new_year['.$i.']', Input::old('new_year.'.$i.''), array('class' => 'form-control')) }}
                    @endif
                    </div>
                    @endfor
            </div>	


            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>