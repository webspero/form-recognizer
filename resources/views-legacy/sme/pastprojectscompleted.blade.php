
	<div class="row">
        {{ Form::open( array('route' => array('postPastproject', $entity_id), 'role'=>'form', 'id' => 'past-proj-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                @for($i = 0; $i < 1; $i++)
                <div class="col-md-12{{ ($errors->has('projectcompleted_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('projectcompleted_name'.$i.'', trans('business_details2.ppc_purpose')) }}
                {{ Form::text('projectcompleted_name['.$i.']', Input::old('projectcompleted_name.'.$i.''), array('class' => 'form-control validate[required]')) }}
                </div>												
                <div class="col-md-6{{ ($errors->has('projectcompleted_cost.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('projectcompleted_cost'.$i.'', trans('business_details2.ppc_cost')) }}
                {{ Form::text('projectcompleted_cost['.$i.']', Input::old('projectcompleted_cost.'.$i.''), array('class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('projectcompleted_source_funding.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('projectcompleted_source_funding'.$i.'', trans('business_details2.ppc_source')) }}
                {{ Form::select('projectcompleted_source_funding['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        'ppc_source_organic' => trans('business_details2.ppc_source_organic'), 
                        'ppc_source_family' => trans('business_details2.ppc_source_family'), 
                        'ppc_source_partners' => trans('business_details2.ppc_source_partners'), 
                        'ppc_source_venture' => trans('business_details2.ppc_source_venture'), 
                        'ppc_source_ipo' => trans('business_details2.ppc_source_ipo'),
                        'ppc_source_bank_loan' => trans('business_details2.ppc_source_bank_loan'),
                        'ppc_source_informal' => trans('business_details2.ppc_source_informal')
                    ), Input::old('projectcompleted_source_funding.'.$i.''), array('id' => 'projectcompleted_source_funding'.$i.'', 'class' => 'form-control validate[required]')) }}
                </div>							
                <div class="col-md-6{{ ($errors->has('projectcompleted_year_began.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('projectcompleted_year_began'.$i.'', trans('business_details2.ppc_year')) }}
                {{ Form::text('projectcompleted_year_began['.$i.']', Input::old('projectcompleted_year_began.'.$i.''), array('class' => 'form-control validate[required]', 'placeholder' => 'YYYY')) }}
                </div>	
                <div class="col-md-6{{ ($errors->has('projectcompleted_result.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('projectcompleted_result'.$i.'', trans('business_details2.ppc_result')) }}
                {{ Form::select('projectcompleted_result['.$i.']', 
                    array('' => trans('messages.choose_here'),
                    'ppc_result_vs' => trans('business_details2.ppc_result_vs'),
                    'ppc_result_s' => trans('business_details2.ppc_result_s'),
                    'ppc_result_u' => trans('business_details2.ppc_result_u')
                    ), Input::old('projectcompleted_result.'.$i.''), array('id' => 'projectcompleted_result'.$i.'', 'class' => 'form-control validate[required]')) }}
                </div>
                @endfor
            </div>
		
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>
		{{ Form::close() }}
	</div>