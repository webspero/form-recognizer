
	<div class="row">
        {{ Form::open( array('route' => array('postKeymanager2', $entity_id), 'role'=>'form', 'id' => 'keymanager-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

        <div class="form-group">
            <div class="col-md-4{{ ($errors->has('firstname')) ? ' has-error' : '' }}">
                {{ Form::label('firstname', 'First Name') }} *
                {{ Form::text('firstname', isset($entity) ? $entity->firstname : Input::old('firstname'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('middlename')) ? ' has-error' : '' }}">
                {{ Form::label('middlename', 'Middle Name') }} 
                {{ Form::text('middlename', isset($entity) ? $entity->middlename : Input::old('middlename'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('lastname')) ? ' has-error' : '' }}">
                {{ Form::label('lastname', 'Last Name') }} *
                {{ Form::text('lastname', isset($entity) ? $entity->lastname : Input::old('lastname'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2{{ ($errors->has('nationality')) ? ' has-error' : '' }}">
                {{ Form::label('nationality', 'Nationality') }}
                {{ Form::select('nationality', $nationalities, Input::old('nationality'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2{{ ($errors->has('civilstatus')) ? ' has-error' : '' }}">
                {{ Form::label('civilstatus', 'Civil Status') }} 
                {{ Form::select('civilstatus', array('' => 'Choose here', '1' => 'Married', '2' => 'Single', '3' => 'Separated'), isset($entity) ? $entity->civilstatus : Input::old('civilstatus'), array('id' => 'civilstatus', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-2{{ ($errors->has('profession')) ? ' has-error' : '' }}">
                {{ Form::label('profession', 'Profession') }} 
                {{ Form::text('profession', isset($entity) ? $entity->profession : Input::old('profession'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-6{{ ($errors->has('phone')) ? ' has-error' : '' }}">
                {{ Form::label('phone', 'Phone Number') }} 
                {{ Form::text('phone', isset($entity) ? $entity->phone : Input::old('phone'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('birthdate')) ? ' has-error' : '' }}">
                {{ Form::label('birthdate', 'Birthdate') }}
                {{ Form::text('birthdate', isset($entity) ? $entity->birthdate : Input::old('birthdate'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('address1')) ? ' has-error' : '' }}">
                {{ Form::label('address1', 'Address') }}
                {{ Form::text('address1', isset($entity) ? $entity->address1 : Input::old('address1'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
                {{ Form::label('tin_num', 'TIN') }} 
                {{ Form::text('tin_num', isset($entity) ? $entity->tin_num : Input::old('tin_num'), array('class' => 'form-control', 'onkeydown' => 'addHyphen(this)', 'maxlength' => '15')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('percent_of_ownership')) ? ' has-error' : '' }}">
                {{ Form::label('percent_of_ownership', trans('owner_details.percent_ownership')) }} 
                {{ Form::text('percent_of_ownership', isset($entity) ? $entity->percent_of_ownership : Input::old('percent_of_ownership'), array('class' => 'form-control', 'placeholder' => 'Number Only')) }}
            </div>
            <div class="col-md-5{{ ($errors->has('number_of_year_engaged')) ? ' has-error' : '' }}">
                {{ Form::label('number_of_year_engaged', trans('owner_details.years_exp')) }} 
                {{ Form::text('number_of_year_engaged', isset($entity) ? $entity->number_of_year_engaged : Input::old('number_of_year_engaged'), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-3{{ ($errors->has('position')) ? ' has-error' : '' }}">
                {{ Form::label('position', trans('owner_details.position')) }} *
                {{ Form::text('position', isset($entity) ? $entity->position : Input::old('position'), array('class' => 'form-control')) }}
            </div>
        </div>

		
        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>
<script>
    $(document).ready(function() {
        $("#birthdate").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0',
            onChangeMonthYear: function(y, m, i) {
                    var d = i.selectedDay;
                    $(this).datepicker('setDate', new Date(y, m - 1, d));
                }
        });
      });
</script>
<script>
    function addHyphen (element) {
              let ele = document.getElementById(element.id);
              ele = ele.value.split('-').join('');    // Remove dash (-) if mistakenly entered.
              
              finalVal = ele.match(/.{3}(?=.{2,3})|.+/g).join('-');
              document.getElementById(element.id).value = finalVal;
          }
</script>