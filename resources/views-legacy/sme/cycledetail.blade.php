@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Compute {{ trans('business_details1.cash_conversion_cyle') }} (days)</h1>
        <p> {{ trans('business_details1.cash_conversion_cyle_hint') }} </p>
		<p>{{ trans('business_details1.cash_conversion_instruct') }}</p>
		@if($errors->any())
		<div class="alert alert-danger" role="alert">
			All items should be a number - please do not use any signs ($, %, #, etc).
		</div>
		@endif
		{{ Form::open( array('route' => array('postCycleDetail', $entity_id), 'role'=>'form', 'autocomplete'=>'off')) }}
		<div class="spacewrapper">
			<div class="table-responsive">
				<table class="table table-striped table-bordered ccc-table" cellspacing="0">
					<tr>
						<td></td>
						<td>Month 1</td>
						<td>Month 2</td>
						<td>Month 3</td>
					</tr>
					<tr class>
						<td>{{ trans('business_details1.ccc_credit_sales') }} (PHP)</td>
						<td style="padding: 0;"><input name="credit_sales[0]" type="text" class="credit_sales_0 cycle-input {{($errors->has('credit_sales.0'))?'cycle-error':''}}" value="{{ isset($entity[0]) ? $entity[0]->credit_sales : ((Input::old('credit_sales.0')) ? Input::old('credit_sales.0') : '')}}" /></td>
						<td style="padding: 0;"><input name="credit_sales[1]" type="text" class="credit_sales_1 cycle-input {{($errors->has('credit_sales.1'))?'cycle-error':''}}" value="{{ isset($entity[1]) ? $entity[1]->credit_sales : ((Input::old('credit_sales.1')) ? Input::old('credit_sales.1') : '')}}" /></td>
						<td style="padding: 0;"><input name="credit_sales[2]" type="text" class="credit_sales_2 cycle-input {{($errors->has('credit_sales.2'))?'cycle-error':''}}" value="{{ isset($entity[2]) ? $entity[2]->credit_sales : ((Input::old('credit_sales.2')) ? Input::old('credit_sales.2') : '')}}" /></td>
					</tr>
					<tr>
						<td>{{ trans('business_details1.ccc_accounts_receivables') }} (PHP)</td>
						<td style="padding: 0;"><input name="accounts_receivable[0]" type="text" class="accounts_receivable_0 cycle-input {{($errors->has('accounts_receivable.0'))?'cycle-error':''}}" value="{{ isset($entity[0]) ? $entity[0]->accounts_receivable : ((Input::old('accounts_receivable.0')) ? Input::old('accounts_receivable.0') : '')}}" /></td>
						<td style="padding: 0;"><input name="accounts_receivable[1]" type="text" class="accounts_receivable_1 cycle-input {{($errors->has('accounts_receivable.1'))?'cycle-error':''}}" value="{{ isset($entity[1]) ? $entity[1]->accounts_receivable : ((Input::old('accounts_receivable.1')) ? Input::old('accounts_receivable.1') : '')}}" /></td>
						<td style="padding: 0;"><input name="accounts_receivable[2]" type="text" class="accounts_receivable_2 cycle-input {{($errors->has('accounts_receivable.2'))?'cycle-error':''}}" value="{{ isset($entity[2]) ? $entity[2]->accounts_receivable : ((Input::old('accounts_receivable.2')) ? Input::old('accounts_receivable.2') : '')}}" /></td>
					</tr>
					<tr>
						<td>{{ trans('business_details1.ccc_inventory') }}  (PHP)</td>
						<td style="padding: 0;"><input name="inventory[0]" type="text" class="inventory_0 cycle-input {{($errors->has('inventory.0'))?'cycle-error':''}}" value="{{ isset($entity[0]) ? $entity[0]->inventory : ((Input::old('inventory.0')) ? Input::old('inventory.0') : '')}}" /></td>
						<td style="padding: 0;"><input name="inventory[1]" type="text" class="inventory_1 cycle-input {{($errors->has('inventory.1'))?'cycle-error':''}}" value="{{ isset($entity[1]) ? $entity[1]->inventory : ((Input::old('inventory.1')) ? Input::old('inventory.1') : '')}}" /></td>
						<td style="padding: 0;"><input name="inventory[2]" type="text" class="inventory_2 cycle-input {{($errors->has('inventory.2'))?'cycle-error':''}}" value="{{ isset($entity[2]) ? $entity[2]->inventory : ((Input::old('inventory.2')) ? Input::old('inventory.2') : '')}}" /></td>
					</tr>
					<tr>
						<td>{{ trans('business_details1.ccc_cost_of_sales') }} (PHP)</td>
						<td style="padding: 0;"><input name="cost_of_sales[0]" type="text" class="cost_of_sales_0 cycle-input {{($errors->has('cost_of_sales.0'))?'cycle-error':''}}" value="{{ isset($entity[0]) ? $entity[0]->cost_of_sales : ((Input::old('cost_of_sales.0')) ? Input::old('cost_of_sales.0') : '')}}" /></td>
						<td style="padding: 0;"><input name="cost_of_sales[1]" type="text" class="cost_of_sales_1 cycle-input {{($errors->has('cost_of_sales.1'))?'cycle-error':''}}" value="{{ isset($entity[1]) ? $entity[1]->cost_of_sales : ((Input::old('cost_of_sales.1')) ? Input::old('cost_of_sales.1') : '')}}" /></td>
						<td style="padding: 0;"><input name="cost_of_sales[2]" type="text" class="cost_of_sales_2 cycle-input {{($errors->has('cost_of_sales.2'))?'cycle-error':''}}" value="{{ isset($entity[2]) ? $entity[2]->cost_of_sales : ((Input::old('cost_of_sales.2')) ? Input::old('cost_of_sales.2') : '')}}" /></td>
					</tr>
					<tr>
						<td>{{ trans('business_details1.ccc_accounts_payable') }} (PHP)</td>
						<td style="padding: 0;"><input name="accounts_payable[0]" type="text" class="accounts_payable_0 cycle-input {{($errors->has('accounts_payable.0'))?'cycle-error':''}}" value="{{ isset($entity[0]) ? $entity[0]->accounts_payable : ((Input::old('accounts_payable.0')) ? Input::old('accounts_payable.0') : '')}}" /></td>
						<td style="padding: 0;"><input name="accounts_payable[1]" type="text" class="accounts_payable_1 cycle-input {{($errors->has('accounts_payable.1'))?'cycle-error':''}}" value="{{ isset($entity[1]) ? $entity[1]->accounts_payable : ((Input::old('accounts_payable.1')) ? Input::old('accounts_payable.1') : '')}}" /></td>
						<td style="padding: 0;"><input name="accounts_payable[2]" type="text" class="accounts_payable_2 cycle-input {{($errors->has('accounts_payable.2'))?'cycle-error':''}}" value="{{ isset($entity[2]) ? $entity[2]->accounts_payable : ((Input::old('accounts_payable.2')) ? Input::old('accounts_payable.2') : '')}}" /></td>
					</tr>
					<tr>
						<td style="border-top: 2px solid; font-size: 14px; font-weight: bold;">{{ trans('business_details1.cash_conversion_cyle') }} (days)</td>
						<td style="border-top: 2px solid; font-size: 14px; font-weight: bold; text-align: right;" class="ccc_0">{{ isset($entity[0]) ? $entity[0]->ccc : '' }}</td>
						<td style="border-top: 2px solid; font-size: 14px; font-weight: bold; text-align: right;" class="ccc_1">{{ isset($entity[1]) ? $entity[1]->ccc : '' }}</td>
						<td style="border-top: 2px solid; font-size: 14px; font-weight: bold; text-align: right;" class="ccc_2">{{ isset($entity[2]) ? $entity[2]->ccc : '' }}</td>
					</tr>
				</table>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
					<input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
<script type="text/javascript">
	$('.cycle-input').on('change', function(){
		for (i = 0; i < 3; i++) {
			var dio = ($('.ccc-table .inventory_'+i).val() / $('.ccc-table .cost_of_sales_'+i).val()) * 30;
			var dso = ($('.ccc-table .accounts_receivable_'+i).val() / $('.ccc-table .credit_sales_'+i).val()) * 30;
			var dpo = $('.ccc-table .accounts_payable_'+i).val() / ($('.ccc-table .cost_of_sales_'+i).val() / 30);
			var ccc = dio + dso - dpo;
			if(ccc){
				$('.ccc-table .ccc_'+i).text(ccc.toFixed(2));
			} else {
				$('.ccc-table .ccc_'+i).text('');
			}
		}
	});
</script>
@stop
