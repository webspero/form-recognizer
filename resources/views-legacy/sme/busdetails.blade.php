@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>{{trans('messages.edit')}}: {{trans('business_details1.business_details')}}</h1>
		{{ Form::model($entity, array('route' => array('putSMEBusDetailsUpdate', $entity->entityid), 'method' => 'put', 'id' => 'formbpo', 'files' => true)) }}
		<div class="spacewrapper">
			<div class="read-only">
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				<h4>Registration Details</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('companyname')) ? ' has-error' : '' }}">
							{{ Form::label('companyname', 'Company Name') }} *
							{{ Form::text('companyname', Input::old('companyname'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('companyname'))
								{{ $errors->first('companyname', '<span class="errormessage">:message</span>') }}
							@endif
						</div>

						<div class="col-md-6{{ ($errors->has('entity_type')) ? ' has-error' : '' }}">
							{{ Form::label('entity_type', 'Business Entity Type') }} *
                            {{ Form::select('entity_type', array('' => trans('messages.choose_here'),
                                  '0' => 'Corporation',
                                  '1' => 'Sole Proprietorship'
                                ), Input::old('entity_type'), array('id' => 'entity_type', 'class' => 'form-control validate[required]'))
                            }}

							@if($errors->has('entity_type'))
								{{ $errors->first('entity_type', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
                    <div class="form-group">
                        <div class="col-md-6{{ ($errors->has('company_tin')) ? ' has-error' : '' }}">
							{{ Form::label('company_tin', 'Company Tin', array('class' => 'company_tin_lbl')) }} *
							{{ Form::text('company_tin', Input::old('company_tin'), array('class' => 'form-control validate[required]')) }}
							@if($errors->has('company_tin'))
								{{ $errors->first('company_tin', '<span class="errormessage">:message</span>') }}
							@endif
						</div>

                        <div class="col-md-6{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
							{{ Form::label('tin_num', 'SEC Company Reg. No.', array('class' => 'tin_num_lbl')) }} * <a href="#" class="popup-hint popup-hint-content" data-hint="<img src='{{URL::to('/')}}/images/ss_company_reg_no.jpg' />">More info...</a>
							{{ Form::text('tin_num', Input::old('tin_num'), array('class' => 'form-control validate[required]')) }}
							@if($errors->has('tin_num'))
								{{ $errors->first('tin_num', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
                    <div class="form-group">
						<div class="col-md-6{{ ($errors->has('sec_reg_date')) ? ' has-error' : '' }}">
							{{ Form::label('sec_reg_date', 'SEC Registration Date', array('class' => 'sec_reg_date_lbl')) }} *
							{{ Form::text('sec_reg_date', Input::old('sec_reg_date'), array('class' => 'form-control datepicker')) }}

							@if($errors->has('sec_reg_date'))
								{{ $errors->first('sec_reg_date', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>

                    <div class="form-group only-corp-field">

                        <div class="col-md-12{{ ($errors->has('sec_cert')) ? ' has-error' : '' }}">
							{{ Form::label('sec_cert', 'SEC Registration Certificate, Articles of Incorporation and By-Laws', array('class' => 'sec_cert_lbl')) }} *
							@if($entity->sec_cert!='')
								<p class="sec_cert_label">{{$entity->sec_cert}} <a href="#" class="sec_cert_edit"> Edit</a></p>
								<input type="file" name="sec_cert" id="sec_cert" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="sec_cert" id="sec_cert" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('sec_cert'))
								{{ $errors->first('sec_cert', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12{{ ($errors->has('permit_to_operate')) ? ' has-error' : '' }}">
							{{ Form::label('permit_to_operate', "Mayor's / Business Permit", array('class' => 'permit_to_operate_lbl')) }} *
							@if($entity->permit_to_operate!='')
								<p class="permit_to_operate_label">{{$entity->permit_to_operate}} <a href="#" class="business_permit_edit"> Edit</a></p>
								<input type="file" name="permit_to_operate" id="permit_to_operate" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="permit_to_operate" id="permit_to_operate" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('permit_to_operate'))
								{{ $errors->first('permit_to_operate', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12{{ ($errors->has('authority_to_borrow')) ? ' has-error' : '' }}">
							{{ Form::label('authority_to_borrow', "Board Resolution", array('class' => 'authority_to_borrow_lbl')) }}
							@if($entity->authority_to_borrow!='')
								<p class="authority_to_borrow_label">{{$entity->authority_to_borrow}} <a href="#" class="authority_to_borrow_edit"> Edit</a></p>
								<input type="file" name="authority_to_borrow" id="authority_to_borrow" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="authority_to_borrow" id="authority_to_borrow" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('authority_to_borrow'))
								{{ $errors->first('authority_to_borrow', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

                    <div class="form-group only-corp-field">

                        <div class="col-md-12{{ ($errors->has('authorized_signatory')) ? ' has-error' : '' }}">
							{{ Form::label('authorized_signatory', "Board Resolution (Pres, VP Financ, Treasurer)", array('class' => 'authorized_signatory_lbl')) }}
							@if($entity->authorized_signatory!='')
								<p class="authorized_signatory_label">{{$entity->authorized_signatory}} <a href="#" class="authorized_signatory_edit"> Edit</a></p>
								<input type="file" name="authorized_signatory" id="authorized_signatory" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="authorized_signatory" id="authorized_signatory" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('authorized_signatory'))
								{{ $errors->first('authorized_signatory', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12{{ ($errors->has('tax_registration')) ? ' has-error' : '' }}">
							{{ Form::label('tax_registration', "BIR Registration", array('class' => 'tax_registration_lbl')) }}
							@if($entity->tax_registration!='')
								<p class="tax_registration_label">{{$entity->tax_registration}} <a href="#" class="tax_registration_edit"> Edit</a></p>
								<input type="file" name="tax_registration" id="tax_registration" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="tax_registration" id="tax_registration" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('tax_registration'))
								{{ $errors->first('tax_registration', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12{{ ($errors->has('tax_registration')) ? ' has-error' : '' }}">
							{{ Form::label('income_tax', "Income Tax Return", array('class' => 'income_tax_lbl')) }}
							@if($entity->income_tax!='')
								<p class="income_tax_label">{{$entity->income_tax}} <a href="#" class="tax_registration_edit"> Edit</a></p>
								<input type="file" name="income_tax" id="income_tax" class="form-control" style="display: none;" />
								<small class="small_text" style="display: none;">Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@else
								<input type="file" name="income_tax" id="income_tax" class="form-control" />
								<small>Accepted file formats: pdf,zip,rar,jpeg,png,csv &nbsp;&nbsp;&nbsp;</small>
							@endif
							@if($errors->has('income_tax'))
								{{ $errors->first('income_tax', '<span class="errormessage">:message</span>') }}
							@endif

						</div>
                    </div>

					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('industrymain')) ? ' has-error' : '' }}">
							{{ Form::label('industrymain', trans('business_details1.industry')) }} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('business_details1.industry_choose')}}">More info...</a>
							{{ Form::select('industrymain', array('' => 'Choose your industry group') + $industrymainList, Input::old('industrymain'), array('id' => 'industrymain', 'class' => 'form-control validate[required]')) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('industrysub')) ? ' has-error' : '' }}">
                            <img class = 'sub-ind-load' style = 'float: left; top: 20%; left: -5px; position: absolute; display: none;' src = '<?php echo URL::To('images/preloader.gif'); ?>' />
							{{ Form::select('industrysub', array('' => 'Choose your sub-industry') + $industrysubList, Input::old('industrysub'), array('id' => 'industrysub', 'class' => 'form-control validate[required]')) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('industryrow')) ? ' has-error' : '' }}">
                            <img class = 'ind-load' style = 'float: left; top: 20%; left: -5px; position: absolute; display: none;' src = '<?php echo URL::To('images/preloader.gif'); ?>' />
							{{ Form::select('industryrow', array('' => 'Choose your industry') + $industryrowList, Input::old('industryrow'), array('id' => 'industryrow', 'class' => 'form-control validate[required]')) }}

							@if($errors->has('industryrow'))
								{{ $errors->first('industryrow', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('total_assets')) ? ' has-error' : '' }}">
							{{ Form::label('total_assets', trans('business_details1.total_assets')) }}
							{{ Form::text('total_assets', Input::old('total_assets'), array('class' => 'form-control')) }}
							@if($errors->has('total_assets'))
								{{ $errors->first('total_assets', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{{ Form::label('website', 'Company Website') }}
							{{ Form::text('website', Input::old('website'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('emailalt')) ? ' has-error' : '' }}">
							{{ Form::label('emailalt', 'Company Email') }} *
							{{ Form::text('emailalt', Input::old('emailalt'), array('class' => 'form-control validate[required]', 'readonly'=>'readonly')) }}

							@if($errors->has('emailalt'))
								{{ $errors->first('emailalt', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('street')) ? ' has-error' : '' }}">
							{{ Form::label('street', trans('business_details1.primary_business_address')) }} *
							{{ Form::text('street', Input::old('street'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('street'))
								{{ $errors->first('street', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
						<div class="col-md-6{{ ($errors->has('province')) ? ' has-error' : '' }}">
							{{ Form::label('province', trans('business_details1.primary_business_province')) }} *
							{{ Form::select('province', array('' => 'Choose your province') + $provinceList, Input::old('province'), array('id' => 'province', 'class' => 'form-control validate[required]')) }}

							@if($errors->has('province'))
								{{ $errors->first('province', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('city')) ? ' has-error' : '' }}">
							<img class='city-preloader' style='top: 36px; left: 24px; position: absolute; display: none;' src='<?php echo URL::to('images/preloader.gif'); ?>' />
							{{ Form::label('city', trans('business_details1.primary_business_city')) }} *
							{{ Form::select('city', array('' => 'Choose your city') + $cityList, Input::old('city'), array('id' => 'city', 'class' => 'form-control validate[required]')) }}

							@if($errors->has('city'))
								{{ $errors->first('city', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
						<div class="col-md-6{{ ($errors->has('zipcode')) ? ' has-error' : '' }}">
							{{ Form::label('zipcode', trans('business_details1.primary_business_zipcode')) }} *
							{{ Form::text('zipcode', Input::old('zipcode'), array('class' => 'form-control', 'readonly'=>'readonly')) }}

							@if($errors->has('zipcode'))
								{{ $errors->first('zipcode', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('phone')) ? ' has-error' : '' }}">
							{{ Form::label('phone', trans('business_details1.primary_business_telephone')) }} *
							{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control validate[required]', 'placeholder' => '(XXX) XXX-XXXX')) }}

							@if($errors->has('phone'))
								{{ $errors->first('phone', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('no_yrs_present_address')) ? ' has-error' : '' }}">
							{{ Form::label('no_yrs_present_address', trans('business_details1.no_yrs_present_address')) }} *
							{{ Form::text('no_yrs_present_address', Input::old('no_yrs_present_address'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('no_yrs_present_address'))
								{{ $errors->first('no_yrs_present_address', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
						<div class="col-md-3{{ ($errors->has('dateestablished')) ? ' has-error' : '' }}">
							{{ Form::label('dateestablished', trans('business_details1.date_established')) }} *
							{{ Form::text('dateestablished', Input::old('dateestablished'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('dateestablished'))
								{{ $errors->first('dateestablished', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
						<div class="col-md-3{{ ($errors->has('numberyear')) ? ' has-error' : '' }}">
							{{ Form::label('numberyear', trans('business_details1.no_yrs_in_business')) }} *
							{{ Form::text('numberyear', Input::old('numberyear'), array('class' => 'form-control', 'readonly' => 'readyonly')) }}

							@if($errors->has('numberyear'))
								{{ $errors->first('numberyear', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('description')) ? ' has-error' : '' }}">
							{{ Form::label('description', trans('business_details1.company_description_fill')) }}
							@if(isset($entity) && $entity->description!="")
								{{ Form::textarea('description', Input::old('description'), array(
								    'id'      => 'description',
								    'rows'    => 5,
								    'class'	  => 'form-control',
									));
								}}
							@else
							    {{ Form::textarea('description', trans('business_details1.company_description_data'), array(
								    'id'      => 'description',
								    'rows'    => 5,
								    'class'	  => 'form-control',
									));
								}}
							@endif
							@if($errors->has('description'))
								{{ $errors->first('description', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('employeesize')) ? ' has-error' : '' }}">
							{{ Form::label('employeesize', trans('business_details1.employee_size')) }} *
							{{ Form::text('employeesize', Input::old('employeesize'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('employeesize'))
								{{ $errors->first('employeesize', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
						<div class="col-md-6{{ ($errors->has('updated_date')) ? ' has-error' : '' }}">
							{{ Form::label('updated_date', trans('business_details1.updated_date')) }} *
							{{ Form::text('updated_date', Input::old('updated_date'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('updated_date'))
								{{ $errors->first('updated_date', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('number_year_management_team')) ? ' has-error' : '' }}">
							{{ Form::label('number_year_management_team', trans('business_details1.number_year_management_team')) }} *
							{{ Form::text('number_year_management_team', Input::old('number_year_management_team'), array('class' => 'form-control validate[required]')) }}

							@if($errors->has('number_year_management_team'))
								{{ $errors->first('number_year_management_team', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('bank')) ? ' has-error' : '' }}">
							{{ Form::label('requested_by', 'Association Options') }} *
							{{ Form::select('requested_by', array('0' => trans('business_details1.requesting_bank_independent'),'1' => trans('business_details1.requesting_bank_with_bank')) , Input::old('requested_by'), array('id' => 'requested_by', 'class' => 'form-control validate[required]')) }}

							@if($errors->has('requested_by'))
								{{ $errors->first('requested_by', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
					<div class="form-group bank-control">
						<div class="col-md-6{{ ($errors->has('bank')) ? ' has-error' : '' }}">
							{{ Form::label('bank', trans('business_details1.requesting_bank')) }} *
							{{ Form::select('bank', $bankList, Input::old('current_bank'), array('id' => 'current_bank', 'class' => 'form-control')) }}

							@if($errors->has('bank'))
								{{ $errors->first('bank', '<span class="errormessage">:message</span>') }}
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
					<input type="button" id="formbpo_submit" class="btn btn-large btn-primary" value="{{ trans('messages.save') }}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
	<div style="height:150px; width: 100%;"></div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/busdetails.js') }}"></script>
	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<script src="{{ URL::asset('js/tutorial.js') }}"></script>
		<script type="text/javascript">
			$(window).load(function(){
				Tutorial.init('step4');
			});
		</script>
	@endif
@stop
@section('style')
	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"></link>
	@endif
@stop
