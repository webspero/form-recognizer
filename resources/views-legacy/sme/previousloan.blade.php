@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Previous Loan Applications </h1>
		    {{ Form::model($entity, ['route' => ['putPreviousloanUpdate', $entity[0]->ploanapplicationid], 'method' => 'put']) }}
		@else
			<h1>Add: Previous Loan Applications </h1>
			{{ Form::open( array('route' => 'postPreviousloan', 'role'=>'form')) }}
		@endif
                The credit lines you've applied for or availed of in the past, that are not currently active, are:
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-3{{ ($errors->has('ploan_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_name'.$i.'', 'Lender') }}
							@if(isset($entity))
								{{ Form::text('ploan_name['.$i.']', ($errors->has('ploan_name.'.$i.'')) ? Input::old('ploan_name.'.$i.'') : $entity[$i]->ploan_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('ploan_name['.$i.']', Input::old('ploan_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_products.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_products'.$i.'', 'Product') }}
							@if(isset($entity))
								{{ Form::text('ploan_products['.$i.']', ($errors->has('ploan_products.'.$i.'')) ? Input::old('ploan_products.'.$i.'') : $entity[$i]->ploan_products, array('class' => 'form-control')) }}
							@else
								{{ Form::text('ploan_products['.$i.']', Input::old('ploan_products.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_date.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_date'.$i.'', 'Date Applied') }}
							@if(isset($entity))
								{{ Form::text('ploan_date['.$i.']', ($errors->has('ploan_date.'.$i.'')) ? Input::old('ploan_date.'.$i.'') : $entity[$i]->ploan_date, array('class' => 'form-control withdate')) }}
							@else
								{{ Form::text('ploan_date['.$i.']', Input::old('ploan_date.'.$i.''), array('class' => 'form-control withdate')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_status.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_status'.$i.'', 'Status') }}
							@if(isset($entity))
								{{ Form::select('ploan_status['.$i.']',
									array('' => 'Choose here',
									'Rejected' => 'Rejected',
									'Fully Paid' => 'Fully Paid',
									), ($errors->has('ploan_status.'.$i.'')) ? Input::old('ploan_status.'.$i.'') : $entity[$i]->ploan_status, array('id' => 'ploan_status'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('ploan_status['.$i.']',
									array('' => 'Choose here',
									'Rejected' => 'Rejected',
									'Fully Paid' => 'Fully Paid',
									), Input::old('ploan_status.'.$i.''), array('id' => 'ploan_status'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							@endfor
						@else
							@for($i = 0; $i < 3; $i++)
							<div class="col-md-3{{ ($errors->has('ploan_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_name'.$i.'', 'Lender') }}
							{{ Form::text('ploan_name['.$i.']', Input::old('ploan_name.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_products.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_products'.$i.'', 'Product') }}
							{{ Form::text('ploan_products['.$i.']', Input::old('ploan_products.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_date.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_date'.$i.'', 'Date Applied') }}
							{{ Form::text('ploan_date['.$i.']', Input::old('ploan_date.'.$i.''), array('class' => 'form-control withdate')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('ploan_status.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('ploan_status'.$i.'', 'Status') }}
							{{ Form::select('ploan_status['.$i.']',
								array('' => 'Choose here',
								'Rejected' => 'Rejected',
								'Fully Paid' => 'Fully Paid',
							), Input::old('ploan_status.'.$i.''), array('id' => 'ploan_status'.$i.'', 'class' => 'form-control')) }}
							</div>
							@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
@stop
