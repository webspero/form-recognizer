@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Form Completed</h1>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					@if(Session::has('success'))
			        <p>{!! Session::get('success') !!}<p>
			    	@elseif(Session::has('fail'))
			    	<p>{!! Session::get('fail') !!}<p>
					@elseif($data == "NCR")
						<p>Transaction Completed</p><p>Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 business days.</p>
					@elseif($data !== "NCR")
						<p>Transaction Completed</p><p>Premium Report processing will now begin. We will email you upon completion of your report in approximately 10 business days.</p>
					@else
					<p>Transaction Completed</p>
					 @endif
			    </div>
			</div>
		</div>
	</div>
@stop
