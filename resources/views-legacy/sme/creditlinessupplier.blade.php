@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Credit Lines with Suppliers </h1>
		    {{ Form::model($entity, ['route' => ['putCreditlinesupplierUpdate', $entity[0]->creditlineid], 'method' => 'put',  'class'=>'majorsupplier']) }}
		@else
			<h1>Add: Credit Lines with Suppliers </h1>
			{{ Form::open( array('route' => 'postCreditlinesupplier', 'role'=>'form',  'class'=>'majorsupplier')) }}
		@endif
                You are currently using the ff credit lines.
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-2{{ ($errors->has('bank_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('bank_name['.$i.']', ($errors->has('bank_name.'.$i.'')) ? Input::old('bank_name.'.$i.'') : $entity[$i]->bank_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_name['.$i.']', Input::old('bank_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-2{{ ($errors->has('bank_product.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_product'.$i.'', 'Product') }}
							@if(isset($entity))
								{{ Form::text('bank_product['.$i.']', ($errors->has('bank_product.'.$i.'')) ? Input::old('bank_product.'.$i.'') : $entity[$i]->bank_product, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_product['.$i.']', Input::old('bank_product.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3">
							{{ Form::label('bank_collateral'.$i.'', 'Collateral') }}
							@if(isset($entity))
								{{ Form::select('bank_collateral['.$i.']',
									array('' => 'Choose here',
									'Clean' => 'Clean',
									'Cash' => 'Cash',
									'Securities' => 'Securities',
									'Property' => 'Property',
									'Chattel' => 'Chattel',
									'PDCs' => 'PDCs',
									), ($errors->has('bank_collateral.'.$i.'')) ? Input::old('bank_collateral.'.$i.'') : $entity[$i]->bank_collateral, array('id' => 'bank_collateral'.$i.'', 'class' => 'form-control colreadonly')) }}
							@else
								{{ Form::select('bank_collateral['.$i.']',
									array('' => 'Choose here',
									'Clean' => 'Clean',
									'Cash' => 'Cash',
									'Securities' => 'Securities',
									'Property' => 'Property',
									'Chattel' => 'Chattel',
									'PDCs' => 'PDCs',
									), Input::old('bank_collateral.'.$i.''), array('id' => 'bank_collateral'.$i.'', 'class' => 'form-control colreadonly')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('bank_initial_availment.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_initial_availment'.$i.'', 'Credit Line (PHP)') }}
							@if(isset($entity))
								{{ Form::text('bank_initial_availment['.$i.']', ($errors->has('bank_initial_availment.'.$i.'')) ? Input::old('bank_initial_availment.'.$i.'') : $entity[$i]->bank_initial_availment, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_initial_availment['.$i.']', Input::old('bank_initial_availment.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-2{{ ($errors->has('bank_outstanding_date.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_outstanding_date'.$i.'', 'Date Opened') }}
							@if(isset($entity))
								{{ Form::text('bank_outstanding_date['.$i.']', ($errors->has('bank_outstanding_date.'.$i.'')) ? Input::old('bank_outstanding_date.'.$i.'') : $entity[$i]->bank_outstanding_date, array('class' => 'form-control withdate')) }}
							@else
								{{ Form::text('bank_outstanding_date['.$i.']', Input::old('bank_outstanding_date.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-4{{ ($errors->has('bank_outstanding_amount.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_outstanding_amount'.$i.'', 'Outstanding Amount (PHP)') }}
							@if(isset($entity))
								{{ Form::text('bank_outstanding_amount['.$i.']', ($errors->has('bank_outstanding_amount.'.$i.'')) ? Input::old('bank_outstanding_amount.'.$i.'') : $entity[$i]->bank_outstanding_amount, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_outstanding_amount['.$i.']', Input::old('bank_outstanding_amount.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('bank_credit_term.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_credit_term'.$i.'', 'Credit Terms') }}
							@if(isset($entity))
								{{ Form::select('bank_credit_term['.$i.']',
									array('' => 'Choose here',
									'COD' => 'COD',
									'30 days ' => '30 days ',
									'60 days' => '60 days',
									'90 days and above ' => '90 days and above ',
									), ($errors->has('bank_credit_term.'.$i.'')) ? Input::old('bank_credit_term.'.$i.'') : $entity[$i]->bank_credit_term, array('id' => 'bank_credit_term'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('bank_credit_term['.$i.']',
									array('' => 'Choose here',
									'COD' => 'COD',
									'30 days ' => '30 days ',
									'60 days' => '60 days',
									'90 days and above ' => '90 days and above ',
									), Input::old('bank_credit_term.'.$i.''), array('id' => 'bank_credit_term'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-5{{ ($errors->has('bank_volume_trade.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_volume_trade'.$i.'', 'Monthly Trade Volume (PHP)') }}
							@if(isset($entity))
								{{ Form::text('bank_volume_trade['.$i.']', ($errors->has('bank_volume_trade.'.$i.'')) ? Input::old('bank_volume_trade.'.$i.'') : $entity[$i]->bank_volume_trade, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_volume_trade['.$i.']', Input::old('bank_volume_trade.'.$i.''), array('class' => 'form-control withdate')) }}
							@endif
							</div>
							@endfor
						@else
							<div class="col-md-12">
							{{ Form::radio('formtype', 1, true, ['class' => 'formtype']) }} None
							{{ Form::radio('formtype', 2, false, ['class' => 'formtype']) }} Add Credit Lines with Suppliers
							</div>
							<div class="reasonsform">
								<div class="col-md-12{{ ($errors->has('credit_facilities_reasons')) ? ' has-error' : '' }}">
								{{ Form::label('credit_facilities_reasons', 'Reason(s)') }}
								{{ Form::textarea('credit_facilities_reasons', Input::old('credit_facilities_reasons'), array(
								    'id'      => 'credit_facilities_reasons',
								    'rows'    => 5,
								    'class'	  => 'form-control',
									));
								}}
								</div>
							</div>
							<div class="addform hideme">
								@for($i = 0; $i < 3; $i++)
								<div class="col-md-2{{ ($errors->has('bank_name.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_name'.$i.'', 'Name') }}
								{{ Form::text('bank_name['.$i.']', Input::old('bank_name.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-2{{ ($errors->has('bank_product.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_product'.$i.'', 'Product') }}
								{{ Form::text('bank_product['.$i.']', Input::old('bank_product.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-3">
								{{ Form::label('bank_collateral'.$i.'', 'Collateral') }}
								{{ Form::select('bank_collateral['.$i.']',
									array('' => 'Choose here',
									'Clean' => 'Clean',
									'Cash' => 'Cash',
									'Securities' => 'Securities',
									'Property' => 'Property',
									'Chattel' => 'Chattel',
									'PDCs' => 'PDCs',
								), Input::old('bank_collateral.'.$i.''), array('id' => 'bank_collateral'.$i.'', 'class' => 'form-control colreadonly')) }}
								</div>
								<div class="col-md-3{{ ($errors->has('bank_initial_availment.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_initial_availment'.$i.'', 'Credit Line (PHP)') }}
								{{ Form::text('bank_initial_availment['.$i.']', Input::old('bank_initial_availment.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-2{{ ($errors->has('bank_outstanding_date.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_outstanding_date'.$i.'', 'Date Opened') }}
								{{ Form::text('bank_outstanding_date['.$i.']', Input::old('bank_outstanding_date.'.$i.''), array('class' => 'form-control withdate')) }}
								</div>
								<div class="col-md-4{{ ($errors->has('bank_outstanding_amount.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_outstanding_amount'.$i.'', 'Outstanding Amount (PHP)') }}
								{{ Form::text('bank_outstanding_amount['.$i.']', Input::old('bank_outstanding_amount.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-3">
								{{ Form::label('bank_credit_term'.$i.'', 'Credit Terms') }}
								{{ Form::select('bank_credit_term['.$i.']',
									array('' => 'Choose here',
									'COD' => 'COD',
									'30 days ' => '30 days ',
									'60 days' => '60 days',
									'90 days and above ' => '90 days and above ',
								), Input::old('bank_credit_term.'.$i.''), array('id' => 'bank_credit_term'.$i.'', 'class' => 'form-control')) }}
								</div>
								<div class="col-md-5{{ ($errors->has('bank_volume_trade.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_volume_trade'.$i.'', 'Monthly Trade Volume (PHP)') }}
								{{ Form::text('bank_volume_trade['.$i.']', Input::old('bank_volume_trade.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-12"><hr/></div>
								@endfor
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
@stop
