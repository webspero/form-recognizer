@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Company Employee </h1>
		    {{ Form::model($entity, ['route' => ['putEmployeeUpdate', $entity[0]->employeeid], 'method' => 'put']) }}
		@else
			<h1>Add: Company Employee </h1>
			{{ Form::open( array('route' => 'postEmployee', 'role'=>'form')) }}
		@endif
                Your employees who can be interviewed are:
		<!--
		<p>For us to be able to provide an accurate rating, please fill in X number of employees below:</p>
		<p>X = (Employee Size * 10%) rounded up to the nearest integer</p>
		<p>if X > 10, Just put in 10.</p>
		<p>10 employees will be the maximum we'll ask for.</p>
		-->
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-4{{ ($errors->has('employee_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('employee_name['.$i.']', ($errors->has('employee_name.'.$i.'')) ? Input::old('employee_name.'.$i.'') : $entity[$i]->employee_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('employee_name['.$i.']', Input::old('employee_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-4{{ ($errors->has('employee_address.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_address'.$i.'', 'Address') }}
							@if(isset($entity))
								{{ Form::text('employee_address['.$i.']', ($errors->has('employee_address.'.$i.'')) ? Input::old('employee_address.'.$i.'') : $entity[$i]->employee_address, array('class' => 'form-control')) }}
							@else
								{{ Form::text('employee_address['.$i.']', Input::old('employee_address.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-4{{ ($errors->has('employee_contact_details.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_contact_details'.$i.'', 'Contact Details (Phone/Email)') }}
							@if(isset($entity))
								{{ Form::text('employee_contact_details['.$i.']', ($errors->has('employee_contact_details.'.$i.'')) ? Input::old('employee_contact_details.'.$i.'') : $entity[$i]->employee_contact_details, array('class' => 'form-control')) }}
							@else
								{{ Form::text('employee_contact_details['.$i.']', Input::old('employee_contact_details.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							@endfor
						@else
							@for($i = 0; $i < 10; $i++)
							<div class="col-md-4{{ ($errors->has('employee_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_name'.$i.'', 'Name') }}
							{{ Form::text('employee_name['.$i.']', Input::old('employee_name.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4{{ ($errors->has('employee_address.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_address'.$i.'', 'Address') }}
							{{ Form::text('employee_address['.$i.']', Input::old('employee_address.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4{{ ($errors->has('employee_contact_details.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('employee_contact_details'.$i.'', 'Contact Details (Phone/Email)') }}
							{{ Form::text('employee_contact_details['.$i.']', Input::old('employee_contact_details.'.$i.''), array('class' => 'form-control')) }}
							</div>
							@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
