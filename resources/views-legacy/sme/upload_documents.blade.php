@extends(STS_NG == $_COOKIE['formdata_support'] ? 'layouts.master' : 'layouts.empty')
@if (STS_NG == $_COOKIE['formdata_support'])

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>{{trans('messages.add')}}: Financial Statements</h1>
        <div class="spacewrapper">
            <div class="read-only">

                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="read-only-text">
                    <div class="col-md-12">
                        <p>{{ trans('business_details1.upload_recommended') }}</p>
                        <p>{{ trans('business_details1.upload_balance_sheet') }}</p>
                        <p>{{ trans('business_details1.upload_income_statement') }}</p>
                        <p>{{ trans('business_details1.upload_file_hint') }}</p>
                    </div>
@endif

	<div class="row">
		{{ Form::open( array('route' => array('postUploadDocuments', $entity_id), 'role'=>'form', 'files' => true, 'id'=>'financials-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div id="upload-file-container" class="file-container">
                <div class="col-md-11 form-inline upload_template" style="margin-bottom: 20px;">
                    <div class="form-group">
                        {{ Form::select('document_type[]',
                        array(
                        '1' => trans('business_details1.unaudited'),
                        '2' => trans('business_details1.interim'),
                        '3' => trans('business_details1.audited')
                        ), Input::old('document_type'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group" style="position: relative;">
                        {{ Form::file('fs_file[]', ['style' => 'width: 350px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader'], array('multiple'=>true)) }}
                        <small style="position: absolute; top: 35px; left: 10px;">{{trans('messages.upload_hint2')}}</small>
                    </div>
                </div>

                <div class="col-md-11 form-inline upload_template" style="margin-bottom: 20px;">
                    <div class="form-group">
                        {{ Form::select('upload_type[]',
                        array(
                        '1' => 'Balance Sheet',
                        '2' => 'Income Statement',
                        '4' => 'Cashflow Statement',
                        '3' => 'BS + IS + CS'
                        ), Input::old('upload_type'), array('class' => 'form-control upload_type')) }}
                    </div>
                    <div class="form-group bs_add"  style="display: inline-block;">
                        <label> as of: </label>
                        <input type="text" name="as_of[]" value="" class="form-control datepicker" />
                    </div>
                    <div class="form-group is_add" style="display: none;">
                        <label> for the period of: </label>
                        <input type="text" name="period_from[]" value="" class="form-control datepicker dp1" />
                        <input type="text" name="period_to[]" value="" class="form-control datepicker dp2" />
                    </div>
                </div>
				@for($x=1; $x<3; $x++)
                <div class="col-md-11 form-inline upload_template" style="margin-bottom: 20px;">
                    <div class="form-group">
                        {{ Form::select('document_type[]',
                        array(
                        '1' => trans('business_details1.unaudited'),
                        '2' => trans('business_details1.interim'),
                        '3' => trans('business_details1.audited')
                        ), Input::old('document_type'), array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group" style="position: relative;">
                        {{ Form::file('fs_file[]', ['style' => 'width: 350px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader'], array('multiple'=>true)) }}
                        <small style="position: absolute; top: 35px; left: 10px;">{{trans('messages.upload_hint2')}}</small>
                    </div>
                </div>

                <div class="col-md-11 form-inline upload_template" style="margin-bottom: 20px;">
                    <div class="form-group">
						{{ Form::select('upload_type[]',
						array(
						'1' => 'Balance Sheet',
						'2' => 'Income Statement',
						'4' => 'Cashflow Statement',
						'3' => 'BS + IS + CS'
						), Input::old('upload_type'), array('class' => 'form-control upload_type')) }}
                    </div>
                    <div class="form-group bs_add" style="display: inline-block;">
                        <label> as of: </label>
                        <input type="text" name="as_of[]" value="" class="form-control datepicker" />
                    </div>
                    <div class="form-group is_add" style="display: none;">
                        <label> for the period of: </label>
                        <input type="text" name="period_from[]" value="" class="form-control datepicker dp1" />
                        <input type="text" name="period_to[]" value="" class="form-control datepicker dp2" />
                    </div>
                </div>
				@endfor
            </div>

            <div class="spacewrapper">
                {{ Form::token() }}
                @if (STS_NG == $_COOKIE['formdata_support'])
                <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @else
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @endif
            </div>
		{{ Form::close() }}
	</div>
@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
    <script type="text/javascript" src="{{URL::asset('js/bsis_upload.js')}}"></script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
