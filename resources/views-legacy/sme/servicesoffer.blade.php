
	<div class="row">
        {{ Form::open( array('route' => array('postSMEservicesoffer', $entity_id), 'role'=>'form', 'id' => 'offers-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
            @for($i = 0; $i < 1; $i++)
            <div class="col-md-3{{ ($errors->has('servicesoffer_name.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('servicesoffer_name'.$i.'', trans('messages.name')) }}
            {{ Form::text('servicesoffer_name['.$i.']', Input::old('servicesoffer_name.'.$i.''), array('class' => 'form-control')) }}
            </div>	
            <div class="col-md-3{{ ($errors->has('servicesoffer_targetmarket.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('servicesoffer_targetmarket'.$i.'', trans('business_details1.target_market')) }}
            {{ Form::text('servicesoffer_targetmarket['.$i.']', Input::old('servicesoffer_targetmarket.'.$i.''), array('class' => 'form-control')) }}
            </div>					
            <div class="col-md-3{{ ($errors->has('servicesoffer_share_revenue.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('servicesoffer_share_revenue'.$i.'', trans('business_details1.share_to_total_sales')) }}
            {{ Form::text('servicesoffer_share_revenue['.$i.']', Input::old('servicesoffer_share_revenue.'.$i.''), array('class' => 'form-control')) }}
            <a href="#" class="popup-hint popup-hint-content" data-hint="Share to Total Sales % can be estimated – this is used as an indicator of how important a specific product is to your business">More info...</a>
            </div>						
            <div class="col-md-2{{ ($errors->has('servicesoffer_seasonality.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('servicesoffer_seasonality'.$i.'', trans('business_details1.seasonality')) }}
            {{ Form::select('servicesoffer_seasonality['.$i.']', 
                array('' => trans('messages.choose_here'), 
                    '1' => trans('business_details1.seasonality_year_round'),
                    '2' => trans('business_details1.seasonality_6_mo'),
                    '3' => trans('business_details1.seasonality_3_mo'),
                    '4' => trans('business_details1.seasonality_less_than_3_mo')
                ), Input::old('servicesoffer_seasonality.'.$i.''), array('id' => 'servicesoffer_seasonality_'.$i.'', 'class' => 'form-control')) }}
            </div>
            @endfor
        </div>

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>