	<div class="row">
        {{ Form::open( array('route' => array('postDirectors', $entity_id), 'role'=>'form', 'id' => 'directors-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
            @for($i=0; $i < 1; $i++)
            <div class="col-md-4{{ ($errors->has('firstName.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('firstName', 'First Name *') }} 
                {{ Form::text('firstName['.$i.']', Input::old('firstName.'.$i), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('middleName.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('middleName', 'Middle Name') }} 
                {{ Form::text('middleName['.$i.']', Input::old('middleName.'.$i), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('lastName.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('lastName', 'Last Name *') }} 
                {{ Form::text('lastName['.$i.']', Input::old('lastName.'.$i), array('class' => 'form-control')) }}
            </div>

            <div class="col-md-6{{ ($errors->has('birthDate')) ? ' has-error' : '' }}">
                {{ Form::label('birthDate', 'Birthdate') }}
                {{ Form::text('birthDate['.$i.']', Input::old('birthDate.'.$i), array('class' => 'form-control birthdate')) }}
            </div>
            <div class="col-md-6{{ ($errors->has('address.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('address', 'Address') }} 
                {{ Form::text('address['.$i.']', Input::old('address.'.$i), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-5{{ ($errors->has('id_no.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('id_no', 'TIN') }} 
                {{ Form::text('id_no['.$i.']', Input::old('id_no.'.$i), array('class' => 'form-control', 'onkeydown' => 'addHyphen(this)', 'maxlength' => '15')) }}
            </div>
            <div class="col-md-5{{ ($errors->has('nationality.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('nationality', ($entity->is_premium == 1)? trans('business_details1.nationality') . ' *':  trans('business_details1.nationality')  ) }}
                {{ Form::select('nationality['.$i.']', $nationalities, Input::old('nationality'.$i), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-6{{ ($errors->has('percentage_share.'.$i)) ? ' has-error' : '' }}">
                {{ Form::label('percentage_share', trans('business_details1.share_to_total_stockholders_ety')) }} 
                {{ Form::text('percentage_share['.$i.']', Input::old('percentage_share.'.$i), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-12"><hr /></div>
            @endfor
        </div>	

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>
    <script>
        $(document).ready(function() {
            $(".birthdate").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: '-100:+0',
                onChangeMonthYear: function(y, m, i) {
                        var d = i.selectedDay;
                        $(this).datepicker('setDate', new Date(y, m - 1, d));
                    }
            });
          });
    </script>
    
    <script>
        function addHyphen (element) {
                let ele = element;
                ele = ele.value.split('-').join('');    // Remove dash (-) if mistakenly entered.
                
                finalVal = ele.match(/.{3}(?=.{2,3})|.+/g).join('-');
                element.value = finalVal;
            }
    </script>