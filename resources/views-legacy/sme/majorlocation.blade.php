
	<div class="row">
        {{ Form::open( array('route' => array('postMajorlocation', $entity_id), 'role'=>'form', 'id' => 'major-loc-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
    

        <div class="form-group">
                @for($i = 0; $i < 1; $i++)				
                <div class="col-md-10{{ ($errors->has('marketlocation')) ? ' has-error' : '' }}">
                {{ Form::label('marketlocation'.$i.'', trans('business_details2.major_market_location')) }}
                <select id="marketlocation" name="marketlocation" class="form-control validate[required]">
                    <option>{{trans('messages.choose_here')}}</option>
                    @foreach($cityList as  $key => $ct)
                        <option value="{{$key}}" data-zipcode="{{$ct}}">{{$ct}}</option>
                    @endforeach
                </select>
                @endfor
        </div>
	
        <div class="spacewrapper">
            {{ Form::token() }}	
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>
		{{ Form::close() }}
	</div>