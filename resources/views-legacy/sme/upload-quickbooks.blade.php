@extends(STS_NG == $_COOKIE['formdata_support'] ? 'layouts.master' : 'layouts.empty')
@if (STS_NG == $_COOKIE['formdata_support'])

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>{{trans('messages.add')}}: Financial Statements</h1>
        <div class="spacewrapper">
            <div class="read-only">

                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="read-only-text">
                    <div class="col-md-12">
                        <p>{{ trans('business_details1.upload_recommended') }}</p>
                        <p>{{ trans('business_details1.upload_balance_sheet') }}</p>
                        <p>{{ trans('business_details1.upload_income_statement') }}</p>
                        <p>{{ trans('business_details1.upload_file_hint') }}</p>
                    </div>
@endif

    @if (!$entity->industry_main_id)
        <div class="row">
            <div class = 'col-md-12'>
                <div class="alert alert-danger pop-error-msg"
                    role="alert">
                    Please select an Industry on Business Details before uploading your Financial Statement
                </div>
            </div>
        </div>
    @else
        
        {{ Form::open( array('route' => array('postUploadQuickbooks', $entity_id), 'role'=>'form', 'files' => true, 'id'=>'quickbooks-expand-form')) }}
            <div class="row">
                <div class = 'col-md-12'>
                    <div class="alert alert-danger pop-error-msg"
                        role="alert" style = 'display: none;'>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-md-3" style="margin-top: 8px;">Currency *</label>
                    <div class="col-md-4">
                        {{ Form::select('currency[]',
                            $currency,
                            'PHP',
                            array('class' => 'form-control'))
                        }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-md-3" style="margin-top: 8px;">Unit *</label>
                    <div class="col-md-4">
                        {{ Form::select('unit[]',
                            array(
                                1 => 'Default',
                                1000 => 'Thousands',
                                1000000 => 'Millions',
                            ),
                            Input::old('unit'),
                            array('class' => 'form-control'))
                        }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-md-3" style="margin-top: 8px;">Type *</label>
                    <div class="col-md-4">
                        {{ Form::select('document_type[]',
                        array(
                        '1' => trans('business_details1.unaudited'),
                        '4' => trans('business_details1.recasted'),
                        '2' => trans('business_details1.interim'),
                        '3' => trans('business_details1.audited')
                        ), Input::old('document_type'), array('class' => 'form-control')) }}
                    </div>
                </div>
            </div>

            <div class="row file-container" id="upload-file-container">
                <div class="form-group">
                    <div class="col-md-3">
                        <label style="margin-top: 8px;">Balance Sheet *</label>
                    </div>
                    <div class="col-md-4">
                        {{ Form::file('qb_balance_sheet', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'required'], array('multiple'=>false)) }}
                        
                    </div>
                    <br>
                </div>
                
            </div>

            <div class="row file-container" id="upload-file-container">
                <div class="form-group">
                    <div class="col-md-3">
                        <label style="margin-top: 8px;">Profit and Loss *</label>
                    </div>
                    <div class="col-md-4">
                        {{ Form::file('qb_profit_loss', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'required'], array('multiple'=>true)) }}
                        
                    </div>
                    <br>
                </div>
                
            </div>

            <div class="row file-container" id="upload-file-container">
                <div class="form-group">
                    <div class="col-md-3">
                        <label style="margin-top: 8px;">Statement of Cash Flows *</label>
                    </div>
                    <div class="col-md-4">
                        {{ Form::file('qb_cash_flows', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'required'], array('multiple'=>true)) }}
                        
                    </div>
                    <br>
                </div>
                
            </div>

            {{ Form::hidden('upload_type[]', 6) }}
            {{ Form::hidden('industry_id[]', $entity->industry_main_id) }}
            <div class="row">
                <div class="col-md-11" style="margin-top: 2px;">
                    {{ Form::token() }}
                    @if (STS_NG == $_COOKIE['formdata_support'])
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
                    <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                    @else
                    <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                    <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                    @endif
                </div>
            </div>
        {{ Form::close() }}

    @endif

@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
    <script type="text/javascript" src="{{URL::asset('js/bsis_upload.js')}}"></script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
