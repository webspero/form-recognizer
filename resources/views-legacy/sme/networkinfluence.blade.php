@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Network of Influence</h1>
		    {{ Form::model($entity, ['route' => ['putNetworkinfluenceUpdate', $entity[0]->networkinfluenceid], 'method' => 'put']) }}
		@else
			<h1>Add: Network of Influence</h1>
			{{ Form::open( array('route' => 'postNetworkinfluence', 'role'=>'form')) }}
			{{ Form::hidden('givenid', $dataowner) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-6{{ ($errors->has('network_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('network_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('network_name['.$i.']', ($errors->has('network_name.'.$i.'')) ? Input::old('network_name.'.$i.'') : $entity[$i]->network_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('network_name['.$i.']', Input::old('network_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-6{{ ($errors->has('network_type.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('network_type'.$i.'', 'Type') }}
							@if(isset($entity))
								{{ Form::select('network_type['.$i.']',
									array('' => 'Choose here',
									'1' => 'Political',
									'2' => 'Business',
									'3' => 'Professional'
									), ($errors->has('network_type.'.$i.'')) ? Input::old('network_type.'.$i.'') : $entity[$i]->network_type, array('id' => 'network_type'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('network_type['.$i.']',
									array('' => 'Choose here',
									'1' => 'Political',
									'2' => 'Business',
									'3' => 'Professional'
									), Input::old('network_type.'.$i.''), array('id' => 'network_type'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							@endfor
						@else
							@for($i = 0; $i < 3; $i++)
							<div class="col-md-6{{ ($errors->has('network_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('network_name'.$i.'', 'Name') }}
							{{ Form::text('network_name['.$i.']', Input::old('network_name.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-6{{ ($errors->has('network_type.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('network_type'.$i.'', 'Type') }}
							{{ Form::select('network_type['.$i.']',
								array('' => 'Choose here',
								'1' => 'Political',
								'2' => 'Business',
								'3' => 'Professional'
								), Input::old('network_type.'.$i.''), array('id' => 'network_type'.$i.'', 'class' => 'form-control')) }}
							</div>
							@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
