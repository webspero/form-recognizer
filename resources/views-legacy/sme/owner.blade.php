@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>{{trans('messages.edit')}}: {{trans('owner_details.owner_and_key_management')}}</h1>
		    {{ Form::model($entity, ['route' => ['putOwnerUpdate', $entity->ownerid], 'method' => 'put', 'id' => 'formbpo']) }}
		@endif
		@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Business Owner Details</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('firstname')) ? ' has-error' : '' }}">
							{{ Form::label('firstname', 'First Name') }} *
							{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('middlename')) ? ' has-error' : '' }}">
							{{ Form::label('middlename', 'Middle Name') }} *
							{{ Form::text('middlename', Input::old('middlename'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('lastname')) ? ' has-error' : '' }}">
							{{ Form::label('lastname', 'Last Name') }} *
							{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('nationality')) ? ' has-error' : '' }}">
							{{ Form::label('nationality', 'Nationality') }} *
							{{ Form::text('nationality', Input::old('nationality'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('birthdate')) ? ' has-error' : '' }}">
							{{ Form::label('birthdate', 'Birthdate') }} *
							{{ Form::text('birthdate', Input::old('birthdate'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('civilstatus')) ? ' has-error' : '' }}">
							{{ Form::label('civilstatus', 'Civil Status') }} *
							{{ Form::select('civilstatus', array('' => 'Choose here', '1' => 'Married', '2' => 'Single', '3' => 'Separated'), Input::old('civilstatus'), array('id' => 'civilstatus', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('profession')) ? ' has-error' : '' }}">
							{{ Form::label('profession', 'Profession') }} *
							{{ Form::text('profession', Input::old('profession'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('email')) ? ' has-error' : '' }}">
							{{ Form::label('email', 'E-mail Address') }} *
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('address1')) ? ' has-error' : '' }}">
							{{ Form::label('address1', 'Address Line 1 (Street Address)') }} *
							{{ Form::text('address1', Input::old('address1'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('address2')) ? ' has-error' : '' }}">
							{{ Form::label('address2', 'Address Line 2 (Apartment No., Complex No. Block, etc.)') }}
							{{ Form::text('address2', Input::old('address2'), array('class' => 'form-control')) }}
						</div>
                        <div class="col-md-6{{ ($errors->has('city')) ? ' has-error' : '' }}">
							{{ Form::label('cityid', 'City') }} *
							{{ Form::select('cityid', array('' => 'Choose your city') + $cityList, Input::old('cityid'), array('id' => 'cityid', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('province')) ? ' has-error' : '' }}">
							{{ Form::label('province', 'Province/Region') }} *
							{{ Form::select('province', array('' => 'Choose your province') + $provinceList, Input::old('province'), array('id' => 'province', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('zipcode')) ? ' has-error' : '' }}">
							{{ Form::label('zipcode', 'Zipcode') }}
							{{ Form::text('zipcode', Input::old('zipcode'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('present_address_status')) ? ' has-error' : '' }}">
							{{ Form::label('present_address_status', trans('owner_details.present_address_status')) }} *
							{{ Form::select('present_address_status', array('' => trans('messages.choose_here'), '1' => trans('owner_details.pas_owned'), '2' => trans('owner_details.pas_mortgaged'), '3' => trans('owner_details.pas_rented')), Input::old('present_address_status'), array('id' => 'present_address_status', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('no_yrs_present_address')) ? ' has-error' : '' }}">
							{{ Form::label('no_yrs_present_address', trans('owner_details.no_years_present')) }} *
							{{ Form::text('no_yrs_present_address', Input::old('no_yrs_present_address'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('phone')) ? ' has-error' : '' }}">
							{{ Form::label('phone', 'Phone Number') }} *
							{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
							{{ Form::label('tin_num', 'TIN (e.g. 111-111-111 or 111-111-111-111A)') }} *
							{{ Form::text('tin_num', Input::old('tin_num'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('percent_of_ownership')) ? ' has-error' : '' }}">
							{{ Form::label('percent_of_ownership', trans('owner_details.percent_ownership')) }} *
							{{ Form::text('percent_of_ownership', Input::old('percent_of_ownership'), array('class' => 'form-control validate[required,custom[onlyNumberSp]]', 'placeholder' => 'Number Only')) }}
						</div>
						<div class="col-md-5{{ ($errors->has('number_of_year_engaged')) ? ' has-error' : '' }}">
							{{ Form::label('number_of_year_engaged', trans('owner_details.years_exp')) }} *
							{{ Form::text('number_of_year_engaged', Input::old('number_of_year_engaged'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3{{ ($errors->has('position')) ? ' has-error' : '' }}">
							{{ Form::label('position', trans('owner_details.position')) }} *
							{{ Form::text('position', Input::old('position'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
					<input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/busowner.js') }}"></script>
@stop
