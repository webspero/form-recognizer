@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Bank History</h1>
		    {{ Form::model($entity, ['route' => ['putbankHistoryupdate', $entity->bankruphistoryid], 'method' => 'put']) }}
		@else
			<h1>Add: Bank History</h1>
		    {{ Form::open( array('route' => 'postbankHistory', 'role'=>'form')) }}
		    {{ Form::hidden('givenid', $refererid) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Bankruptcy History</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('bank_history')) ? ' has-error' : '' }}">
							{{ Form::label('bank_history', 'History') }} *
							{{ Form::select('bank_history',
								array('' => 'Choose here',
								'1' => 'None',
								'2' => 'With Recovery',
								'3' => 'Without Recovery'
							), Input::old('bank_history'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
