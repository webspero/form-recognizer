@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit:Character Management</h1>
		    {{ Form::model($entity, ['route' => ['putCharacterUpdate', $entity->trackrecordid], 'method' => 'put']) }}
		@else
			<h1>Add: Character Management</h1>
		    {{ Form::open( array('route' => 'postCharacter', 'role'=>'form')) }}
			{{ Form::hidden('givenid', $dataowner, array('class' => 'form-control')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Number of Years of Engagement in the Business</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('bus_owner')) ? ' has-error' : '' }}">
							{{ Form::label('bus_owner', 'Business Owner') }} *
							{{ Form::text('bus_owner', Input::old('bus_owner'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('bus_spouse', 'Spouse') }}
							{{ Form::text('bus_spouse', Input::old('bus_spouse'), array('id' => 'bus_spouse', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('bus_relative')) ? ' has-error' : '' }}">
							{{ Form::label('bus_relative', 'Relative') }} *
							{{ Form::text('bus_relative', Input::old('bus_relative'), array('id' => 'bus_relative', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('bus_profmanager')) ? ' has-error' : '' }}">
							{{ Form::label('bus_profmanager', 'Professional Manager') }} *
							{{ Form::text('bus_profmanager', Input::old('bus_profmanager'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('bus_other', 'Others (Please specify)') }}
							{{ Form::text('bus_other', Input::old('bus_other'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4">
							{{ Form::label('bus_other_specify', 'Others') }}
							{{ Form::text('bus_other_specify', Input::old('bus_other_specify'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Political Exposure</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-6{{ ($errors->has('exposure_position')) ? ' has-error' : '' }}">
							{{ Form::label('exposure_position', 'Business Owner and Immediate Family Members: Position') }}
							{{ Form::text('exposure_position', Input::old('exposure_position'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('exposure_type')) ? ' has-error' : '' }}">
							{{ Form::label('exposure_type', 'Business Owner and Immediate Family Members: Type') }}
							{{ Form::select('exposure_type',
								array('' => 'Choose here',
								'Elected' => 'Elected',
								'Appointed' => 'Appointed'
							), Input::old('exposure_type'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('cr_position')) ? ' has-error' : '' }}">
							{{ Form::label('cr_position', 'Close Relative: Position') }}
							{{ Form::text('cr_position', Input::old('cr_position'), array('id' => 'cr_position', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('cr_type')) ? ' has-error' : '' }}">
							{{ Form::label('cr_type', 'Close Relative: Type') }}
							{{ Form::select('cr_type',
								array('' => 'Choose here',
								'Elected' => 'Elected',
								'Appointed' => 'Appointed'
							), Input::old('cr_type'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('dr_position')) ? ' has-error' : '' }}">
							{{ Form::label('dr_position', 'Distant Relative: Position') }}
							{{ Form::text('dr_position', Input::old('dr_position'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('dr_type')) ? ' has-error' : '' }}">
							{{ Form::label('dr_type', 'Distant Relative: Type') }}
							{{ Form::select('dr_type',
								array('' => 'Choose here',
								'Elected' => 'Elected',
								'Appointed' => 'Appointed'
							), Input::old('dr_type'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Household Management Responsibility</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('hmr_status')) ? ' has-error' : '' }}">
							{{ Form::label('hmr_status', 'Status') }} *
							{{ Form::select('hmr_status',
								array('' => 'Choose here',
								'Stable Marriage' => 'Stable Marriage',
								'Separated with Custody of Children' => 'Separated with Custody of Children',
								'Single with Marriage Intent (With Business Partners)' => 'Single with Marriage Intent (With Business Partners)',
								'Single with no Business Partners' => 'Single with no Business Partners'
							), Input::old('hmr_status'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/fiedetails.js') }}"></script>
@stop
