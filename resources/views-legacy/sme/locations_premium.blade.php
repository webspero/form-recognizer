
	<div class="row">
			{{ Form::open( array('route' => array('postLocations', $entity_id), 'role'=>'form', 'id' => 'main-loc2-expand-form', 'files' => true)) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                @for($i = 0; $i < 1; $i++)						
                <div class="col-md-6{{ ($errors->has('location_size.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('location_size'.$i.'', trans('business_details1.size_of_premises')) }} *
                {{ Form::text('location_size['.$i.']', Input::old('location_size.'.$i.''), array('class' => 'form-control')) }}
                </div>	
                <div class="col-md-6{{ ($errors->has('location_type.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('location_type'.$i.'', trans('business_details1.premises_owned_leased')) }} *
                {{ Form::select('location_type['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        'ml_owned' => trans('business_details1.ml_owned'),
                        'ml_leased' => trans('business_details1.ml_leased')
                    ), Input::old('location_type.'.$i.''), array('id' => 'location_type'.$i.'', 'class' => 'form-control')) }}
                </div>							
                <div class="col-md-6{{ ($errors->has('location_map.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('location_map'.$i.'', trans('business_details1.location')) }} *
                {{ Form::select('location_map['.$i.']', 
                    array('' => trans('messages.choose_here'),
                        'ml_commercial_area' => trans('business_details1.ml_commercial_area'),
                        'ml_residential_area' => trans('business_details1.ml_residential_area'),
                        'ml_industrial_area' => trans('business_details1.ml_industrial_area')
                    ), Input::old('location_map.'.$i.''), array('id' => 'location_map'.$i.'', 'class' => 'form-control')) }}
                </div>							
                <div class="col-md-6{{ ($errors->has('location_used.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('location_used'.$i.'', trans('business_details1.premise_used_as')) }} *
                {{ Form::select('location_used['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        'ml_admin_office' => trans('business_details1.ml_admin_office'),
                        'ml_sales_office' => trans('business_details1.ml_sales_office'),
                        'ml_warehouse' => trans('business_details1.ml_warehouse'),
                        'ml_retail_outlet' => trans('business_details1.ml_retail_outlet'),
                        'ml_production_facility' => trans('business_details1.ml_production_facility')
                    ), Input::old('location_used.'.$i.''), array('id' => 'location_used'.$i.'', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('floor_count.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('floor_count'.$i.'', '# of Floors') }} *
                {{ Form::text('floor_count['.$i.']', Input::old('floor_count.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('address.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('address'.$i.'', 'Location Address') }} *
                {{ Form::text('address['.$i.']', Input::old('address.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('material.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('material'.$i.'', 'Material') }} *
                {{ Form::text('material['.$i.']', Input::old('material.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('color.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('color'.$i.'', 'Color') }} *
                {{ Form::text('color['.$i.']', Input::old('color.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('color.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('map'.$i.'', 'Map') }} *
                    <div class="col-md-12 form-inline upload-fields" style="margin-bottom: 20px;">
                        <div class="form-group" style="position: relative;">
                            {{ Form::file('map[]', ['style' => 'width: 450px; border: 1px solid #ccc', 'class' => 'form-control'], array('multiple'=>false)) }}
                            <small style="position: absolute; top: 35px; left: 10px;">Accepted file formats: png, jpg, jpeg, gif</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-6{{ ($errors->has('color.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('location_photo'.$i.'', 'Actual Location Photo') }} *
                    <div class="col-md-12 form-inline upload-fields" style="margin-bottom: 20px;">
                        <div class="form-group" style="position: relative;">
                            {{ Form::file('location_photo[]', ['style' => 'width: 450px; border: 1px solid #ccc', 'class' => 'form-control'], array('multiple'=>false)) }}
                            <small style="position: absolute; top: 35px; left: 10px;">Accepted file formats: png, jpg, jpeg, gif</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-6{{ ($errors->has('other.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('other'.$i.'', 'Note') }}
                    {{ Form::text('other['.$i.']', Input::old('other.'.$i.''), array('class' => 'form-control')) }}
                </div>
                @endfor
            </div>
            
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>