@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Upload requested documents</h1>
		@if($errors->any())
		<div class="alert alert-danger" role="alert">
			@foreach($errors->all() as $e)
				<p>{{$e}}</p>
			@endforeach
		</div>
		@endif
		{{ Form::open( array('route' => 'postUploadRequestedDocument', 'role'=>'form', 'files' => true)) }}
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="file-container">
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','', array('class'=>'form-control', 'multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','', array('class'=>'form-control', 'multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','', array('class'=>'form-control', 'multiple'=>true)) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					<input id="submit" type="submit" class="btn btn-large btn-primary" value="Submit">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
