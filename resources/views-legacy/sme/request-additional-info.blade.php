@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
                <h1>Request Additional Information from Company</h1>
                {{ Form::open( array('route' => 'postRequestAdditionalInfo', 'role'=>'form')) }}
                @if($errors->has('email'))
                    {{ $errors->first('email', '<span class="errormessage">:message</span>') }}
                @endif
                @if(Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                @elseif(Session::has('fail'))
                        <div class="alert alert-danger">{{ Session::get('fail') }}</div>
                @endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
                                            <div class="col-md-12{{ ($errors->has('message_body')) ? ' has-error' : '' }}">
                                            {{ Form::hidden('entityid', $entityid, array('name' => 'entityid', 'id' => 'entityid')) }}
                                            {{ Form::label('message_body', 'Message') }}
                                            <?php
												$default_text = "We started processing your application as soon as you submitted it. For verification purposes, we need accurate copies of the documents listed here:

-	[Item1]
-	[Item2]
-	[Item3]

We cannot continue your application unless these items are provided. Thank you very much for your understanding and cooperation.
"
											?>
											{{ Form::textarea('message_body', Input::old('message_body', $default_text), array('class' => 'form-control')) }}
                                            </div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Send Message">
					<a href="{{URL::to('/')}}/summary/smesummary/{{$entityid}}" class="btn btn-default">Back</a>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
