@extends(STS_NG == $_COOKIE['formdata_support'] ? 'layouts.master' : 'layouts.empty')
@if (STS_NG == $_COOKIE['formdata_support'])

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>{{trans('messages.add')}}: {{trans('business_details1.bank_statement')}}</h1>
        <div class="spacewrapper">
            <div class="read-only">

                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="read-only-text">
@endif

	<div class="row">
		{{ Form::open( array('id' => 'bank-statements-expand-form', 'route' => array('postBankStatements', $entity_id), 'role'=>'form', 'files' => true)) }}

            <div class="file-container">
                <div class="col-md-12">
                    <p>{{ trans('business_details1.upload_bank_statements') }}</p>
                    <p>{{ trans('business_details1.upload_multiple_files') }}</p>
                    {!! trans('business_details1.zip_file_win') !!}

                    <br />

                    {!! trans('business_details1.zip_file_mac') !!}

                </div>

                <div id = 'upload-field-cntr' class = 'row spacewrapper'>
                    <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
                    @if (STS_NG == $_COOKIE['formdata_support'])
                        <?php $max = 3; ?>
                    @else
                        <?php $max = 1; ?>
                    @endif

                    @for($i = 0; $i < 3; $i++)
                    <div class="col-md-12 form-inline upload-fields" style="margin-bottom: 20px;">
                        <div class="form-group" style="position: relative;">
                            {{ Form::file('bank_statements[]', ['style' => 'width: 450px; border: 1px solid #ccc', 'class' => 'form-control'], array('multiple'=>true)) }}
                            <small style="position: absolute; top: 35px; left: 10px;">{{trans('messages.upload_hint1')}}</small>
                        </div>
                        <div class="form-group">
                            <label> as of: </label>
                            <input type="text" name="as_of[]" value="" class="form-control datepicker" />
                        </div>
                    </div>
                    @endfor
                </div>
            </div>

            <div class="spacewrapper">
                {{ Form::token() }}
                @if (STS_NG == $_COOKIE['formdata_support'])
                <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @else
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @endif
            </div>

		{{ Form::close() }}
	</div>
@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
    <script>
        $(document).ready(function(){
            $('input[name="as_of[]"]').datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '-30:+0',
                onChangeMonthYear: function(y, m, i) {
                        var d = i.selectedDay;
                        $(this).datepicker('setDate', new Date(y, m - 1, d));
                    }
            });
        });
    </script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
