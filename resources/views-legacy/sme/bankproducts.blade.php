@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Bank Products </h1>
		    {{ Form::model($entity, ['route' => ['putBankproductsUpdate', $entity[0]->bankproductid], 'method' => 'put']) }}
		@else
			<h1>Add: Bank Products </h1>
			{{ Form::open( array('route' => 'postBankproducts', 'role'=>'form')) }}
		@endif
                You are currently using the ff banks and bank products for your business.
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-2{{ ($errors->has('bank_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('bank_name['.$i.']', ($errors->has('bank_name.'.$i.'')) ? Input::old('bank_name.'.$i.'') : $entity[$i]->bank_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_name['.$i.']', Input::old('bank_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-2{{ ($errors->has('bank_product.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_product'.$i.'', 'Product') }}
							@if(isset($entity))
								{{ Form::text('bank_product['.$i.']', ($errors->has('bank_product.'.$i.'')) ? Input::old('bank_product.'.$i.'') : $entity[$i]->bank_product, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_product['.$i.']', Input::old('bank_product.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-2{{ ($errors->has('bank_account_number.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_account_number'.$i.'', 'Account Number') }}
							@if(isset($entity))
								{{ Form::text('bank_account_number['.$i.']', ($errors->has('bank_account_number.'.$i.'')) ? Input::old('bank_account_number.'.$i.'') : $entity[$i]->bank_account_number, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_account_number['.$i.']', Input::old('bank_account_number.'.$i.''), array('class' => 'form-control withdate')) }}
							@endif
							</div>
							<div class="col-md-2{{ ($errors->has('bank_initial_availment.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_initial_availment'.$i.'', 'Date Opened') }}
							@if(isset($entity))
								{{ Form::text('bank_initial_availment['.$i.']', ($errors->has('bank_initial_availment.'.$i.'')) ? Input::old('bank_initial_availment.'.$i.'') : $entity[$i]->bank_initial_availment, array('class' => 'form-control withdate')) }}
							@else
								{{ Form::text('bank_initial_availment['.$i.']', Input::old('bank_initial_availment.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-4{{ ($errors->has('bank_outstanding_amount.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('bank_outstanding_amount'.$i.'', 'Average Daily Balance Amount (PHP)') }}
							@if(isset($entity))
								{{ Form::text('bank_outstanding_amount['.$i.']', ($errors->has('bank_outstanding_amount.'.$i.'')) ? Input::old('bank_outstanding_amount.'.$i.'') : $entity[$i]->bank_outstanding_amount, array('class' => 'form-control')) }}
							@else
								{{ Form::text('bank_outstanding_amount['.$i.']', Input::old('bank_outstanding_amount.'.$i.''), array('class' => 'form-control withdate')) }}
							@endif
							</div>
							@endfor
						@else
							<div class="col-md-12">
							{{ Form::radio('formtype', 1, true, ['class' => 'formtype']) }} None
							{{ Form::radio('formtype', 2, false, ['class' => 'formtype']) }} Add Bank Products
							</div>
							<div class="reasonsform">
								<div class="col-md-12{{ ($errors->has('credit_facilities_reasons')) ? ' has-error' : '' }}">
								{{ Form::label('credit_facilities_reasons', 'Reason(s)') }}
								{{ Form::textarea('credit_facilities_reasons', Input::old('credit_facilities_reasons'), array(
								    'id'      => 'credit_facilities_reasons',
								    'rows'    => 5,
								    'class'	  => 'form-control',
									));
								}}
								</div>
							</div>
							<div class="addform hideme">
								@for($i = 0; $i < 3; $i++)
								<div class="col-md-2{{ ($errors->has('bank_name.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_name'.$i.'', 'Name') }}
								{{ Form::text('bank_name['.$i.']', Input::old('bank_name.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-2{{ ($errors->has('bank_product.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_product'.$i.'', 'Product') }}
								{{ Form::text('bank_product['.$i.']', Input::old('bank_product.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-2{{ ($errors->has('bank_account_number.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_account_number'.$i.'', 'Account Number') }}
								{{ Form::text('bank_account_number['.$i.']', Input::old('bank_account_number.'.$i.''), array('class' => 'form-control')) }}
								</div>
								<div class="col-md-2{{ ($errors->has('bank_initial_availment.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_initial_availment'.$i.'', 'Date Opened') }}
								{{ Form::text('bank_initial_availment['.$i.']', Input::old('bank_initial_availment.'.$i.''), array('class' => 'form-control withdate')) }}
								</div>
								<div class="col-md-4{{ ($errors->has('bank_outstanding_amount.'.$i.'')) ? ' has-error' : '' }}">
								{{ Form::label('bank_outstanding_amount'.$i.'', 'Average Daily Balance Amount (PHP)') }}
								{{ Form::text('bank_outstanding_amount['.$i.']', Input::old('bank_outstanding_amount.'.$i.''), array('class' => 'form-control')) }}
								</div>
								@endfor
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
@stop
