	<div class="row">
        {{ Form::open( array('url' => '/sme/busconandsus/' . $type .'/'. $entity_id, 'role'=>'form', 'id' => 'bcs-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
		@if($type == "succession")
            
            <div class="form-group">
                <div class="col-md-5{{ ($errors->has('succession_plan')) ? ' has-error' : '' }}">
                    {{ Form::label('succession_plan', trans('condition_sustainability.plan')) }} *
                    {{ Form::select('succession_plan', 
                    array(
                    '' => trans('condition_sustainability.choose_plan'), 
                    '1' => trans('condition_sustainability.mngmt_relative'), 
                    '2' => trans('condition_sustainability.trusted_employee'), 
                    '3' => trans('condition_sustainability.none_less_five'),
                    ), Input::old('succession_plan'), array('id' => 'succession_plan', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('succession_timeframe')) ? ' has-error' : '' }}">
                    {{ Form::label('succession_timeframe', trans('condition_sustainability.time_frame')) }} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('condition_sustainability.succession_time')}}">More info...</a> *
                    {{ Form::select('succession_timeframe', array('' => trans('condition_sustainability.choose_succession_time'), '0' => trans('condition_sustainability.succession_plan_none'), '1' => trans('condition_sustainability.succession_within_five'), '2' => trans('condition_sustainability.succession_over_five')), Input::old('succession_timeframe'), array('id' => 'succession_timeframe', 'class' => 'form-control validate[required]')) }}
                </div>
            </div>
                    
		@elseif($type == "competitionlandscape")

            <div class="form-group">
                <div class="col-md-10{{ ($errors->has('competition_landscape')) ? ' has-error' : '' }}">
                    {{ Form::label('competition_landscape', trans('condition_sustainability.landscape')) }} *
                    {{ Form::select('competition_landscape', array('' => trans('condition_sustainability.choose_landscape'), '1' => trans('condition_sustainability.market_pioneer'), '2' => trans('condition_sustainability.limited_market_entry'), '3' => trans('condition_sustainability.high_market_penetration')), Input::old('succession_plan'), array('id' => 'competition_landscape', 'class' => 'form-control validate[required]')) }}
                </div>
                <input type="hidden" name="competition_timeframe" value="1" />
            </div>
    
		@elseif($type == "economicfactors")

            <div class="form-group">
                <div class="col-md-5{{ ($errors->has('ev_susceptibility_economic_recession')) ? ' has-error' : '' }}">
                    {{ Form::label('ev_susceptibility_economic_recession', trans('condition_sustainability.recession_susceptibility')) }} *
                    {{ Form::select('ev_susceptibility_economic_recession', array('' => trans('messages.choose_here'), '1' => trans('messages.low'), '2' => trans('messages.medium'), '3' => trans('messages.high')), Input::old('ev_susceptibility_economic_recession'), array('id' => 'ev_susceptibility_economic_recession', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('ev_foreign_exchange_interest_sensitivity')) ? ' has-error' : '' }}">
                    {{ Form::label('ev_foreign_exchange_interest_sensitivity', trans('condition_sustainability.forex_sensitivity')) }} *
                    {{ Form::select('ev_foreign_exchange_interest_sensitivity', array('' => trans('messages.choose_here'), '1' => trans('messages.low'), '2' => trans('messages.medium'), '3' => trans('messages.high')), Input::old('ev_foreign_exchange_interest_sensitivity'), array('id' => 'ev_foreign_exchange_interest_sensitivity', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('ev_commodity_price_volatility')) ? ' has-error' : '' }}">
                    {{ Form::label('ev_commodity_price_volatility', trans('condition_sustainability.price_volatility')) }} *
                    {{ Form::select('ev_commodity_price_volatility', array('' => trans('messages.choose_here'), '1' => trans('messages.low'), '2' => trans('messages.medium'), '3' => trans('messages.high')), Input::old('ev_commodity_price_volatility'), array('id' => 'ev_commodity_price_volatility', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('ev_governement_regulation')) ? ' has-error' : '' }}">
                    {{ Form::label('ev_governement_regulation', trans('condition_sustainability.government_regulation')) }} *
                    {{ Form::select('ev_governement_regulation', array('' => trans('messages.choose_here'), '1' => trans('messages.low'), '2' => trans('messages.medium'), '3' => trans('messages.high')), Input::old('ev_governement_regulation'), array('id' => 'ev_governement_regulation', 'class' => 'form-control validate[required]')) }}
                </div>	
            </div>	
            
		@elseif($type == "taxpayments")
            
            <div class="form-group">
                <div class="col-md-10{{ ($errors->has('tax_payments')) ? ' has-error' : '' }}">
                    {{ Form::label('tax_payments', trans('condition_sustainability.tax_payments')) }} *
                    {{ Form::select('tax_payments', array('' => trans('messages.choose_here'), '1' => trans('condition_sustainability.taxes_current'), '2' => trans('condition_sustainability.taxes_arrears'), '3' => trans('condition_sustainability.taxes_bad')), Input::old('tax_payments'), array('id' => 'tax_payments', 'class' => 'form-control validate[required]')) }}
                </div>	
            </div>
            
		@endif

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>
