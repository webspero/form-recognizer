@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Score: Company Employee</h1>
		{{ Form::model($entity, ['route' => ['putScoreEmployeeUpdate', $entity[0]->loginid], 'method' => 'put']) }}
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<span class="details-row">
							<div class="col-md-3"><b>Name</b></div>
							<div class="col-md-3"><b>Address</b></div>
							<div class="col-md-3"><b>Contact Details (Phone/Email)</b></div>
							<div class="col-md-3"><b>Score</b></div>
						</span>
						@for($i = 0; $i < @count($entity); $i++)
							{{ Form::hidden('employeeid['.$i.']', $entity[$i]->employeeid, array('id' => 'employeeid')) }}
							<span class="details-row">
								<div class="col-md-3">{{ $entity[$i]->employee_name }}</div>
								<div class="col-md-3">{{ $entity[$i]->employee_address }}</div>
								<div class="col-md-3">{{ $entity[$i]->employee_contact_details }}</div>
								<div class="col-md-3{{ ($errors->has('employee_score.'.$i.'')) ? ' has-error' : '' }}">
								@if(isset($entity))
									{{ Form::select('employee_score['.$i.']',
										array('' => 'Score here',
										'1' => '1',
										'2' => '2',
										'3' => '3',
										'4' => '4',
										'5' => '5'
										), ($errors->has('employee_score.'.$i.'')) ? Input::old('employee_score.'.$i.'') : $entity[$i]->employee_score, array('id' => 'employee_score'.$i.'', 'class' => 'form-control')) }}
								@else
									{{ Form::select('employee_score['.$i.']',
										array('' => 'Score here',
										'1' => '1',
										'2' => '2',
										'3' => '3',
										'4' => '4',
										'5' => '5'
										), Input::old('employee_score.'.$i.''), array('id' => 'employee_score'.$i.'', 'class' => 'form-control')) }}
								@endif
								</div>
							</span>
						@endfor
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
