
	<div class="row">
        {{ Form::open( array('route' => array('postBusinesssegment', $entity_id), 'role'=>'form' , 'id' => 'bizsegment-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                @for($i = 0; $i < 1; $i++)
                <div class="col-md-10{{ ($errors->has('businesssegment_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('businesssegment_name'.$i.'', trans('messages.name')) }}
                {{ Form::text('businesssegment_name['.$i.']', Input::old('businesssegment_name.'.$i.''), array('class' => 'form-control validate[required]')) }}
                </div>						
                @endfor
            </div>

	
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>