@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Family Income and Expenditure Details</h1>
		    {{ Form::model($entity, ['route' => ['putSMEFIEUpdate', $entity->incomeandexpenditureid], 'method' => 'put']) }}
		@else
			<h1>Add: Family Income and Expenditure Details </h1>
		    {{ Form::open( array('route' => 'postSMEFIECreate', 'role'=>'form')) }}
			{{ Form::hidden('ownerid', $owner, array('class' => 'form-control')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('total_income')) ? ' has-error' : '' }}">
							{{ Form::label('total_income', 'Total Income') }} *
							{{ Form::text('total_income', Input::old('total_income'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('living_expenses')) ? ' has-error' : '' }}">
							{{ Form::label('living_expenses', 'Living Expenses') }} *
							{{ Form::text('living_expenses', Input::old('living_expenses'), array('id' => 'living_expenses', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('education_medical')) ? ' has-error' : '' }}">
							{{ Form::label('education_medical', 'Education / Medical') }} *
							{{ Form::text('education_medical', Input::old('education_medical'), array('id' => 'education_medical', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('insurance_pension')) ? ' has-error' : '' }}">
							{{ Form::label('insurance_pension', 'Insurance / Pension') }} *
							{{ Form::text('insurance_pension', Input::old('insurance_pension'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('rent_amortization')) ? ' has-error' : '' }}">
							{{ Form::label('rent_amortization', 'Rent / Amortization') }} *
							{{ Form::text('rent_amortization', Input::old('rent_amortization'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('transportation_utilities')) ? ' has-error' : '' }}">
							{{ Form::label('transportation_utilities', 'Transportation / Utilities') }} *
							{{ Form::text('transportation_utilities', Input::old('transportation_utilities'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('credit_cards')) ? ' has-error' : '' }}">
							{{ Form::label('credit_cards', 'Credit Cards') }} *
							{{ Form::text('credit_cards', Input::old('credit_cards'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('other_loans')) ? ' has-error' : '' }}">
							{{ Form::label('other_loans', 'Other Loans') }} *
							{{ Form::text('other_loans', Input::old('other_loans'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('miscellaneous')) ? ' has-error' : '' }}">
							{{ Form::label('miscellaneous', 'Miscellaneous') }} *
							{{ Form::text('miscellaneous', Input::old('miscellaneous'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('total_expenses')) ? ' has-error' : '' }}">
							{{ Form::label('total_expenses', 'Total Expenses') }} *
							{{ Form::text('total_expenses', Input::old('total_expenses'), array('id' => 'total_expenses' ,'class' => 'form-control', 'readonly' => 'readonly')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('net_monthly_income')) ? ' has-error' : '' }}">
							{{ Form::label('net_monthly_income', 'Net Monthly Income') }} *
							{{ Form::text('net_monthly_income', Input::old('net_monthly_income'), array('id' => 'net_monthly_income' ,'class' => 'form-control', 'readonly' => 'readonly')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/fiedetails.js') }}"></script>
@stop
