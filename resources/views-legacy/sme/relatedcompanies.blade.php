
	<div class="row">
        {{ Form::open( array('route' => array('postRelatedcompanies', $entity_id), 'role'=>'form', 'class'=>'majorsupplier', 'id' => 'affiliates-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

        @for($i = 0; $i < 1; $i++)
            <div class="form-group">
                <div class="col-md-2{{ ($errors->has('related_company_name.'.$i.'')) ? 'has-error' : '' }}">
                {{ Form::label('related_company_name'.$i.'', trans('messages.name')) }}
                {{ Form::text('related_company_name['.$i.']', Input::old('related_company_name.'.$i.''), array('class' => 'form-control')) }}
                </div>	
                <div class="col-md-4{{ ($errors->has('related_company_address1.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_address1'.$i.'', trans('messages.address_1')) }}
                {{ Form::text('related_company_address1['.$i.']', Input::old('related_company_address1.'.$i.''), array('class' => 'form-control')) }}
                </div>							
                <div class="col-md-4{{ ($errors->has('related_company_address2.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_address2'.$i.'', trans('messages.address_2')) }}
                {{ Form::text('related_company_address2['.$i.']', Input::old('related_company_address2.'.$i.''), array('class' => 'form-control')) }}
                </div>								
                <div class="col-md-4{{ ($errors->has('related_company_province.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_province'.$i.'', trans('messages.province_region')) }}
                {{ Form::select('related_company_province['.$i.']', array('' => 'Choose here') + $provinceList, Input::old('related_company_province.'.$i.''), array('id' => 'related_company_province'.$i.'', 'class' => 'form-control province_change')) }}
                </div>	
                <div class="col-md-4{{ ($errors->has('related_company_cityid.'.$i.'')) ? ' has-error' : '' }}">
                <img class='city-preloader' style='top: 36px; left: 24px; position: absolute; display: none;' src='<?php echo URL::to('images/preloader.gif'); ?>' />
                {{ Form::label('related_company_cityid'.$i.'', trans('messages.city')) }}
                {{ Form::select('related_company_cityid['.$i.']', array('' => 'Choose here') + $cityList[$i], Input::old('related_company_cityid.'.$i.''), array('id' => 'related_company_cityid'.$i.'', 'class' => 'form-control city_change')) }}
                </div>							
                <div class="col-md-2{{ ($errors->has('related_company_zipcode.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_zipcode'.$i.'', trans('messages.zipcode')) }}
                {{ Form::text('related_company_zipcode['.$i.']', Input::old('related_company_zipcode.'.$i.''), array('id' => 'related_company_zipcode'.$i.'', 'class' => 'form-control zipcode_change')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('related_company_phone.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_phone'.$i.'', trans('messages.phone_number')) }}
                {{ Form::text('related_company_phone['.$i.']', Input::old('related_company_phone.'.$i.''), array('id' => 'related_company_phone'.$i.'', 'class' => 'form-control')) }}
                </div>		
                <div class="col-md-3{{ ($errors->has('related_company_email.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('related_company_email'.$i.'', trans('messages.email')) }}
                {{ Form::text('related_company_email['.$i.']', Input::old('related_company_email.'.$i.''), array('id' => 'related_company_email'.$i.'', 'class' => 'form-control')) }}
                </div>							
                <div class="col-md-12"><hr/></div>
            </div>
        @endfor

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.province_change').on('change', function(){
		var sProvince = $(this).val();
		var uiCity = $(this).parent().parent().find('.city_change');
		
		$.ajax({
			url: BaseURL + '/api/cities',
			type: 'get',
			dataType: 'json',
			data: {
				province: sProvince
			},
			beforeSend: function() {
				uiCity.parent().find('.city-preloader').show();
				uiCity.html('<option>&nbsp;&nbsp;&nbsp;Loading cities...</option>');
			},
			success: function(oData)	{
				uiCity.parent().find('.city-preloader').hide();
				uiCity.html('<option value="">Choose here</option>');
				for(x in oData){
					uiCity.append('<option value="'+oData[x].city+'" zipcode="'+oData[x].zip_code+'">'+oData[x].city+'</option>');
				}
			}
		});
	});
	
	$('.city_change').on('change', function(){
		var uiZipcode = $(this).parent().parent().find('.zipcode_change');
		if($(this).val()!=''){
			uiZipcode.val($(this).find('option:selected').attr('zipcode'));
		}
	});
});
</script>
