
	<div class="row">
		    {{ Form::open( array('route' => array('postCapital', $entity_id), 'role'=>'form', 'id' => 'cap-details-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                <div class="col-md-12{{ ($errors->has('capital_authorized')) ? ' has-error' : '' }}">
                    {{ Form::label('capital_authorized', trans('business_details1.authorized_capital')) }} *
                    {{ Form::text('capital_authorized', Input::old('capital_authorized'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('capital_issued')) ? ' has-error' : '' }}">
                    {{ Form::label('capital_issued', trans('business_details1.issued_capital')) }} *
                    {{ Form::text('capital_issued', Input::old('capital_issued'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('capital_paid_up')) ? ' has-error' : '' }}">
                    {{ Form::label('capital_paid_up', trans('business_details1.paid_up_capital')) }} *
                    {{ Form::text('capital_paid_up', Input::old('capital_paid_up'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('capital_ordinary_shares')) ? ' has-error' : '' }}">
                    {{ Form::label('capital_ordinary_shares', trans('business_details1.ordinary_shares')) }} *
                    {{ Form::text('capital_ordinary_shares', Input::old('capital_ordinary_shares'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('capital_par_value')) ? ' has-error' : '' }}">
                    {{ Form::label('capital_par_value', trans('business_details1.par_value')) }} *
                    {{ Form::text('capital_par_value', Input::old('capital_par_value'), array('class' => 'form-control')) }}
                </div>
            </div>	
		
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>
