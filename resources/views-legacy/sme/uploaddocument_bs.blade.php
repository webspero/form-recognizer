@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Add: Documents</h1>
		@if($errors->any())
		<div class="alert alert-danger" role="alert">
			@foreach($errors->all() as $e)
				<p>{{$e}}</p>
			@endforeach
		</div>
		@endif
		{{ Form::open( array('route' => 'postDocumentsBS', 'role'=>'form', 'files' => true)) }}
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="file-container">
						<div class="col-md-12">
						<p>Upload at least two (2) years of consecutive Balance Sheets. Four (4) years is ideal. Please upload files in excel format.</p>
						{{ Form::select('document_type',
							array(
							'1' => 'Un-audited',
							'2' => 'Interim',
							'3' => 'Audited',
							'4' => 'Recasted'
							), Input::old('document_type'), array('id' => 'document_type', 'class' => 'form-control')) }}

						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
						<div class="col-md-12 form-inline">
							<div class="form-group">
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
							<div class="form-group">
								<label> as of: </label>
								<input type="text" name="as_of[]" value="" class="form-control datepicker" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input id="submit" type="submit" class="btn btn-large btn-primary" disabled="disabled" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
			dateFormat: 'mm/dd/yy',
			onSelect: function(){
				check_container();
			}
		});

		$('.file-container input[type=file]').on('change', function(){
			check_container();
		});
		$('.datepicker').on('change', function(){
			check_container();
		});

	});

	function check_container(){
		var comp = true;
		$('.file-container input[type=file]').each(function(){
			if($(this).val()!=''){
				if($(this).parent().parent().find('.datepicker').val()==''){
						comp = false;
				}
			}
		});
		if(comp){
			$('#submit').removeAttr('disabled');
		} else {
			$('#submit').attr('disabled', 'disabled');
		}
	}
</script>
@stop
