@extends('layouts.master')
@section('nav')
@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<p>Verification Submitted!</p><p>  We will process report after all documents are verified. Thank you!</p><p><a href="/">Go to Dashboard</a></p>
			    </div>
			</div>
		</div>
	</div>
@stop
