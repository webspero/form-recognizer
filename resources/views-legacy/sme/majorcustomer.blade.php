	<div class="row">

        {{ Form::open( array('route' => array('postMajorcustomer', $entity_id), 'role'=>'form', 'class'=>'majorsupplier', 'id' => 'major-cust-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                @for($i = 0; $i < 1; $i++)
                    <div class="col-md-3{{ ($errors->has('customer_name.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_name'.$i.'', ($entity->is_premium == 1)? trans('messages.reg_name') . ' *':trans('messages.name') . ' *') }} 
                    {{ Form::text('customer_name['.$i.']', Input::old('customer_name.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_address.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_address'.$i.'', ($entity->is_premium == 1)? trans('messages.reg_address') . ' *':'Address *') }} 
                    {{ Form::text('customer_address['.$i.']', Input::old('customer_address.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_contact_person.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_contact_person'.$i.'',($entity->is_premium == 1)? trans('messages.contact_person') :trans('messages.contact_person').' *') }}
                    {{ Form::text('customer_contact_person['.$i.']', Input::old('customer_contact_person.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_phone.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_phone'.$i.'', ($entity->is_premium == 1)? trans('messages.contact_phone') : trans('messages.contact_phone').' *') }}
                    {{ Form::text('customer_phone['.$i.']', Input::old('customer_phone.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_email.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_email'.$i.'',($entity->is_premium == 1)?  trans('messages.contact_email'): trans('messages.contact_email').' *') }}
                    {{ Form::text('customer_email['.$i.']', Input::old('customer_email.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_started_years.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_started_years'.$i.'',($entity->is_premium == 1)? trans('business_details2.mc_year_started'): trans('business_details2.mc_year_started').' *') }}
                    {{ Form::text('customer_started_years['.$i.']', Input::old('customer_started_years.'.$i.''), array('id' => 'customer_started_years'.$i.'', 'class' => 'customer_started_years form-control', 'data-idnumber' => ''.$i.'', 'placeholder' => 'YYYY', )) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('customer_years_doing_business.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_years_doing_business'.$i.'', ($entity->is_premium == 1)? trans('business_details2.mc_year_doing_business') : trans('business_details2.mc_year_doing_business').' *') }}
                    {{ Form::text('customer_years_doing_business['.$i.']', Input::old('customer_years_doing_business.'.$i.''), array('id' => 'customer_years_doing_business'.$i.'', 'class' => 'form-control', 'readonly' => 'readonly')) }}
                    </div>
                    <div class="col-md-3{{ ($errors->has('relationship_satisfaction.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label(
                        'relationship_satisfaction'.$i.'',
                        ($entity->is_premium == 1)? trans('business_details2.mc_relationship_satisfaction') : trans('business_details2.mc_relationship_satisfaction').'*')
                    }}
                    {{ Form::select(
                        'relationship_satisfaction['.$i.']',
                        ['' => trans('messages.choose_here')] + $relationship_satisfaction,
                        Input::old('relationship_satisfaction.'.$i),
                        [
                            'id' => 'relationship_satisfaction'.$i,
                            'class' => 'form-control'
                        ])
                    }}
                    </div>
                    <div class="col-md-4{{ ($errors->has('customer_share_sales.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_share_sales'.$i.'', ($entity->is_premium == 1)? trans('business_details2.mc_percent_share') : trans('business_details2.mc_percent_share').' *') }}
                    {{ Form::text('customer_share_sales['.$i.']', Input::old('customer_share_sales.'.$i.''), array('class' => 'form-control')) }}
                    </div>
                    <div class="col-md-4{{ ($errors->has('customer_order_frequency.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_order_frequency'.$i.'', ($entity->is_premium == 1)? trans('business_details2.mc_order_frequency'): trans('business_details2.mc_order_frequency').' *') }}
                    {{ Form::select('customer_order_frequency['.$i.']',
                        array('' => trans('messages.choose_here'),
                            'mcof_regularly' => trans('business_details2.mcof_regularly'),
                            'mcof_often' => trans('business_details2.mcof_often'),
                            'mcof_rarely' => trans('business_details2.mcof_rarely')
                        ), Input::old('customer_order_frequency.'.$i.''), array('id' => 'customer_order_frequency'.$i.'', 'class' => 'form-control')) }}
                    </div>
                    <div class="col-md-4{{ ($errors->has('customer_settlement.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('customer_settlement'.$i.'', ($entity->is_premium == 1)?  trans('business_details2.mc_payment_behavior') :  trans('business_details2.mc_payment_behavior').' *') }} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('business_details2.mcpb_hint')}}">More info...</a>
                    {{ Form::select('customer_settlement['.$i.']',
                        array('' => trans('messages.choose_here'),
                        '1' => trans('business_details2.mc_ahead'),
                        '2' => trans('business_details2.mc_ondue'),
                        '3' => trans('business_details2.mc_portion'),
                        '4' => trans('business_details2.mc_beyond'),
                        '5' => trans('business_details2.mc_delinquent')
                        ), Input::old('customer_settlement.'.$i.''), array('id' => 'customer_settlement_'.$i.'', 'class' => 'form-control')) }}
                    </div>

                    <div class="col-md-12"><hr/></div>
                @endfor
            </div>

            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>

    <script>
        var report = $('#anonymous-report');

        if (report.prop('checked')) {

            $('[name="customer_name[0]"]').prop('disabled', true).val('Major Customer');
            $('.submit-expander-form').click(function() {
                $('[name="customer_name[0]"]').prop('disabled', false);
            });
        }

    </script>
