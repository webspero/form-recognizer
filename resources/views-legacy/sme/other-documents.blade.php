@extends(STS_NG == $_COOKIE['formdata_support'] ? 'layouts.master' : 'layouts.empty')
@if (STS_NG == $_COOKIE['formdata_support'])

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
    <h1>{{trans('messages.add')}}: Other Documents Requested by Bank </h1>
        <div class="spacewrapper">
            <div class="read-only">

                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                        <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="read-only-text">
@endif

	<div class="row">
        {{ Form::open(array('id' => 'other-documents-expand-form', 'route' => array('postOtherDocuments', $entity_id), 'role'=>'form', 'files' => true)) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            @if (STS_NG == $_COOKIE['formdata_support'])
                <?php $max = 3; ?>
            @else
                <?php $max = 1; ?>
            @endif
            @for($i = 0; $i < 1; $i++)
            <div class="form-group">
                <div class = 'col-md-12'>{{ Form::label('doc_lbl'.$i.'', 'Upload Other Documents') }}</div>
                <div class="col-md-5{{ ($errors->has('doc_lbl.'.$i.'')) ? ' has-error' : '' }}">

                    <select name = 'doc_lbl[{{ $i }}]' class = 'form-control'>
                        <option value = ''> {{ trans('messages.choose_here') }} </option>
                        @foreach ($custom_docs as $docs)
                        <option value = '{{ $docs->document_label }}'> {{ $docs->document_label }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-5{{ ($errors->has('other_document.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::file('other_document['.$i.']', array('class' => 'form-control')) }}
                </div>
            </div>

            <div class = 'col-md-12'> <hr> </div>
            @endfor


            <div class="spacewrapper">
                {{ Form::token() }}
                @if (STS_NG == $_COOKIE['formdata_support'])
                <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @else
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
                @endif
            </div>
		{{ Form::close() }}
	</div>
@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
<script>
$('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    yearRange: '-30:+0',
    onChangeMonthYear: function(y, m, i) {
        var d = i.selectedDay;
        $(this).datepicker('setDate', new Date(y, m - 1, d));
    }
});
</script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
