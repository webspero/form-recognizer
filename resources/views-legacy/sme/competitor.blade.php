<div class="row">
    {{ Form::open( array('route' => array('postCompetitor', $entity_id), 'role'=>'form', 'id' => 'competitor-expand-form')) }}
    <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
    
    <div class="form-group">
        @for($i = 0; $i < 1; $i++)
        <div class="col-md-5{{ ($errors->has('competitor_name.'.$i.'')) ? ' has-error' : '' }}">
        {{ Form::label('competitor_name'.$i.'', trans('messages.name')  .' *') }}
        {{ Form::text('competitor_name['.$i.']', Input::old('competitor_name.'.$i.''), array('class' => 'form-control')) }}
        </div>												
        <div class="col-md-5{{ ($errors->has('competitor_address.'.$i.'')) ? ' has-error' : '' }}">
        {{ Form::label('competitor_address'.$i.'', 'Address *' )  }}
        {{ Form::text('competitor_address['.$i.']', Input::old('competitor_address.'.$i.''), array('class' => 'form-control')) }}
        </div>
        <div class="col-md-5{{ ($errors->has('competitor_phone.'.$i.'')) ? ' has-error' : '' }}">
        {{ Form::label('competitor_phone'.$i.'', trans('messages.contact_phone') .' *' ) }}
        {{ Form::text('competitor_phone['.$i.']', Input::old('competitor_phone.'.$i.''), array('class' => 'form-control')) }}
        </div>	
        <div class="col-md-5{{ ($errors->has('competitor_email.'.$i.'')) ? ' has-error' : '' }}">
        {{ Form::label('competitor_email'.$i.'', trans('messages.contact_email')) }}
        {{ Form::text('competitor_email['.$i.']', Input::old('competitor_email.'.$i.''), array('class' => 'form-control')) }}
        </div>	
        @endfor
    </div>

    <div class="spacewrapper">
        {{ Form::token() }}
        <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
        <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
    </div>

    {{ Form::close() }}
</div>