
	<div class="row">
        {{ Form::open( array('route' => array('postMachineEquipment', $entity_id), 'role'=>'form', 'id' => 'machine-equip-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> 
        
        <div class="form-group">
                @for($i = 0; $i < 1; $i++)						
                <div class="col-md-5{{ ($errors->has('machine_equip_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('machine_equip_name'.$i.'', trans('business_details2.machine_equip_name')) }}
                {{ Form::text('machine_equip_name['.$i.']', Input::old('machine_equip_name.'.$i.''), array('class' => 'form-control')) }}
                </div>
                
                <div class="col-md-5{{ ($errors->has('machine_equip_serial.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('machine_equip_serial'.$i.'', trans('business_details2.machine_equip_serial')) }}
                {{ Form::text('machine_equip_serial['.$i.']', Input::old('machine_equip_serial.'.$i.''), array('class' => 'form-control')) }}
                </div>
                @endfor
        </div>
        
        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>