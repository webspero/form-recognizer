@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Add: Documents</h1>
		{{ Form::open( array('route' => 'postDocuments', 'role'=>'form', 'files' => true)) }}
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12">
						<p>Submit 3 years of consecutive Financial Statements (Balance Sheet and Income Statement).</p>
						<p>What Financial Statements will you upload? Please note that they will be the basis for our Financial Analysis Rating.</p>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">Un-audited</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[0]" value="11" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">Interrim</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[1]" value="12" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">Audited</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[2]" value="13" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>

						<div class="col-md-12">
						<p>Balance Sheet</p>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 1</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[3]" value="21" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 2</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[4]" value="22" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 3</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[5]" value="23" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
                                                <div class="col-md-12">
							<div class="col-md-2">YEAR 4</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[9]" value="24" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>

						<div class="col-md-12">
						<p>Income Statement</p>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 1</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[6]" value="31" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 2</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[7]" value="32" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2">YEAR 3</div>
							<div class="col-md-8">
								<input type="hidden" name="documenttype[8]" value="33" />
								{{ Form::file('file[]','',array('id'=>'documents','class'=>'form-control','multiple'=>true)) }}
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
