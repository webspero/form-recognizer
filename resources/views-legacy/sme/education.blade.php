	<div class="row">
        {{ Form::open( array('route' => array('postEducation', $entity_id, $owner_id, $type), 'role'=>'form', 'id' => 'educ-bg-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
		
        <div class="form-group">
            @for($i = 0; $i < 4; $i++)
            <input type="hidden" value="{{ $i + 1 }}" name="educ_type[{{ $i }}]">
            <div class="col-md-6{{ ($errors->has('school_name.'.$i.'')) ? ' has-error' : '' }}">
            @if($i + 1 == 1)
                {{ Form::label('school_name'.$i.'', 'High School: Name') }}
            @elseif($i + 1 == 2)
                {{ Form::label('school_name'.$i.'', 'College: Name') }}
            @elseif($i + 1 == 3)
                {{ Form::label('school_name'.$i.'', 'Post Graduate: Name') }}
            @else
                {{ Form::label('school_name'.$i.'', 'Others: Name') }}
            @endif						
            {{ Form::text('school_name['.$i.']', Input::old('school_name.'.$i.''), array('class' => 'form-control')) }}
            </div>												
            <div class="col-md-6{{ ($errors->has('educ_degree.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('educ_degree'.$i.'', 'Level of Completion') }}
            {{ Form::select('educ_degree['.$i.']', array(
                'Not Taken'=>'Not Taken', 
                'Graduated'=>'Graduated', 
                'Ongoing'=>'Ongoing', 
                'Delayed'=>'Delayed'
            ), Input::old('educ_degree.'.$i), array('class' => 'form-control'))}}
            </div>
            @endfor
        </div>	

        {{ Form::token() }}

		{{ Form::close() }}
	</div>