
	<div class="row">
        {{ Form::open( array('route' => array('postBusinesstype', $entity_id), 'role'=>'form', 'id' => 'biztyp-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
            <div class="col-md-3">{{ Form::label('is_import', trans('business_details1.ie_imports')) }}</div>
            <div class="col-md-8">
            {{ Form::select('is_import', 
                array('' => trans('messages.choose_here'), 
                '1' => trans('business_details1.ie_yes'),
                '2' => trans('business_details1.ie_no')
            ), Input::old('is_import'), array('id' => 'is_import', 'class' => 'form-control')) }}
            </div>
            <div class="col-md-3">{{ Form::label('is_export', trans('business_details1.ie_exports')) }}</div>
            <div class="col-md-8">
            {{ Form::select('is_export', 
                array('' => trans('messages.choose_here'), 
                '1' => trans('business_details1.ie_yes'),
                '2' => trans('business_details1.ie_no')
            ), Input::old('is_export'), array('id' => 'is_export', 'class' => 'form-control')) }}
            </div>
        </div>	

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>