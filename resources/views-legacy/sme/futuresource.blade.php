@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($planfacility))
			<h1>Edit: Future Source Of Capital </h1>
		    {{ Form::model($planfacility, ['route' => ['putSMEFutureSourceUpdate', $planfacility->loginid], 'method' => 'put']) }}
		@else
			<h1>Add: Future Source Of Capital </h1>
		    {{ Form::open( array('route' => 'postSMEFutureSourceCreate', 'role'=>'form')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-12{{ ($errors->has('opg_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('opg_per_year', 'Organic Profit Growth (internally generated profit) PHP Per Year') }}
							{{ Form::text('opg_per_year', Input::old('opg_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('opg_horizontal_year')) ? ' has-error' : '' }}">
							{{ Form::label('opg_horizontal_year', 'Organic Profit Growth (internally generated profit) Horizon Years') }}
							{{ Form::text('opg_horizontal_year', Input::old('opg_horizontal_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('family_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('family_per_year', 'Family PHP Per Year') }}
							{{ Form::text('family_per_year', Input::old('family_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('family_horizon_year')) ? ' has-error' : '' }}">
							{{ Form::label('family_horizon_year', 'Family Horizon Years') }}
							{{ Form::text('family_horizon_year', Input::old('family_horizon_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('bp_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('bp_per_year', 'Business partners PHP Per Year') }}
							{{ Form::text('bp_per_year', Input::old('bp_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('bp_horizon_year')) ? ' has-error' : '' }}">
							{{ Form::label('bp_horizon_year', 'Business partners Horizon Years') }}
							{{ Form::text('bp_horizon_year', Input::old('bp_horizon_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('vcc_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('vcc_per_year', 'Venture capital companies PHP Per Year') }}
							{{ Form::text('vcc_per_year', Input::old('vcc_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('vcc_horizon_year')) ? ' has-error' : '' }}">
							{{ Form::label('vcc_horizon_year', 'Venture capital companies Horizon Years') }}
							{{ Form::text('vcc_horizon_year', Input::old('vcc_horizon_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('ipo_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('ipo_per_year', 'IPO PHP Per Year') }}
							{{ Form::text('ipo_per_year', Input::old('ipo_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('ipo_horizon_year')) ? ' has-error' : '' }}">
							{{ Form::label('ipo_horizon_year', 'IPO Horizon Years') }}
							{{ Form::text('ipo_horizon_year', Input::old('ipo_horizon_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('others_per_year')) ? ' has-error' : '' }}">
							{{ Form::label('others_per_year', 'Others PHP Per Year') }}
							{{ Form::text('others_per_year', Input::old('others_per_year'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('others_horizon_year')) ? ' has-error' : '' }}">
							{{ Form::label('others_horizon_year', 'Others Horizon Years') }}
							{{ Form::text('others_horizon_year', Input::old('others_horizon_year'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/busdetails.js') }}"></script>
@stop
