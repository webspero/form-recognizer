@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>{{trans('messages.add')}}: {{trans('owner_details.owner_and_key_management')}} </h1>
		{{ Form::open( array('route' => array('postSMEBusOwnerStore', $entity_id), 'role'=>'form', 'id' => 'formbpo')) }}
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $e)
                <p>{{$e}}</p>
            @endforeach
        </div>
        @endif
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('owner_details.biz_owner_details')}}</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('firstname')) ? ' has-error' : '' }}">
							{{ Form::label('firstname', trans('owner_details.firstname')) }} *
							{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('middlename')) ? ' has-error' : '' }}">
							{{ Form::label('middlename', trans('owner_details.middlename')) }} *
							{{ Form::text('middlename', Input::old('middlename'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('lastname')) ? ' has-error' : '' }}">
							{{ Form::label('lastname', trans('owner_details.lastname')) }} *
							{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('nationality')) ? ' has-error' : '' }}">
							{{ Form::label('nationality', trans('owner_details.nationality')) }} *
							{{ Form::text('nationality', Input::old('nationality'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('birthdate')) ? ' has-error' : '' }}">
							{{ Form::label('birthdate', trans('owner_details.birthdate')) }} *
							{{ Form::text('birthdate', Input::old('birthdate'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('civilstatus')) ? ' has-error' : '' }}">
							{{ Form::label('civilstatus', trans('owner_details.civilstatus')) }} *
							{{ Form::select('civilstatus', array('' => trans('messages.choose_here'), '1' => trans('owner_details.married'), '2' => trans('owner_details.single'), '3' => trans('owner_details.separated')), Input::old('civilstatus'), array('id' => 'civilstatus', 'class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('profession')) ? ' has-error' : '' }}">
							{{ Form::label('profession', trans('owner_details.profession')) }} *
							{{ Form::text('profession', Input::old('profession'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('email')) ? ' has-error' : '' }}">
							{{ Form::label('email', trans('owner_details.email')) }} *
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('address1')) ? ' has-error' : '' }}">
							{{ Form::label('address1', trans('owner_details.address1')) }} *
							{{ Form::text('address1', Input::old('address1'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('address2')) ? ' has-error' : '' }}">
							{{ Form::label('address2', trans('owner_details.address2')) }}
							{{ Form::text('address2', Input::old('address2'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('province')) ? ' has-error' : '' }}">
							{{ Form::label('province', 'Province/Region') }} *
							<select id="province" name="province" class="form-control">
								<option>Choose your province</option>
								@foreach($provinceList2 as $pt)
									<option value="{{$pt}}" data-zipcode="{{$pt}}" >{{$pt}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-6{{ ($errors->has('city')) ? ' has-error' : '' }}">
							{{ Form::label('cityid', trans('owner_details.city')) }} *

							<select id="cityid" name="cityid" class="form-control">
								<option>Choose your city</option>
								@foreach($cityList2 as $ct)
									<option value="{{$ct->id}}" data-zipcode="{{$ct->zip_code}}" {{ (Input::old('cityid')==$ct->id) ? "selected='selected'" : '' }}>{{$ct->city}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-6{{ ($errors->has('zipcode')) ? ' has-error' : '' }}">
							{{ Form::label('zipcode', 'Zipcode') }}
							{{ Form::text('zipcode', Input::old('zipcode'), array('class' => 'form-control', 'readonly' => 'readonly')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('present_address_status')) ? ' has-error' : '' }}">
							{{ Form::label('present_address_status', trans('owner_details.present_address_status')) }} *
							{{ Form::select('present_address_status', array('' => trans('messages.choose_here'), '1' => trans('owner_details.pas_owned'), '2' => trans('owner_details.pas_mortgaged'), '3' => trans('owner_details.pas_rented')), Input::old('present_address_status'), array('id' => 'present_address_status', 'class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('no_yrs_present_address')) ? ' has-error' : '' }}">
							{{ Form::label('no_yrs_present_address', trans('owner_details.no_years_present')) }} *
							{{ Form::text('no_yrs_present_address', Input::old('no_yrs_present_address'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('phone')) ? ' has-error' : '' }}">
							{{ Form::label('phone', trans('owner_details.phone')) }} *
							{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
							{{ Form::label('tin_num', 'TIN (e.g. 111-111-111 or 111-111-111-111A)') }} *
							{{ Form::text('tin_num', Input::old('tin_num'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('percent_of_ownership')) ? ' has-error' : '' }}">
							{{ Form::label('percent_of_ownership', trans('owner_details.percent_ownership')) }} *
							{{ Form::text('percent_of_ownership', Input::old('percent_of_ownership'), array('class' => 'form-control validate[required,custom[onlyNumberSp]]', 'placeholder' => 'Number Only')) }}
						</div>
						<div class="col-md-5{{ ($errors->has('number_of_year_engaged')) ? ' has-error' : '' }}">
							{{ Form::label('number_of_year_engaged', trans('owner_details.years_exp')) }} *
							{{ Form::text('number_of_year_engaged', Input::old('number_of_year_engaged'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="col-md-3{{ ($errors->has('position')) ? ' has-error' : '' }}">
							{{ Form::label('position', trans('owner_details.position')) }} *
							{{ Form::text('position', Input::old('position'), array('class' => 'form-control validate[required]')) }}
						</div>
						<div class="busownwrapper">
							<div class="col-md-12"><h3>{{trans('owner_details.other_business')}}</h3></div>
							<div class="col-md-12"><hr/></div>
							<div class="col-md-4">
								{{ Form::label('bus_name', trans('messages.name')) }}
								{{ Form::text('bus_name[0]', Input::old('bus_name.0'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_type', trans('owner_details.business_type')) }}
								{{ Form::text('bus_type[0]', Input::old('bus_type.0'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_location', trans('owner_details.business_location')) }}
								<select id="bus_location_0" name="bus_location_0" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-4">
								{{ Form::label('bus_name', trans('messages.name')) }}
								{{ Form::text('bus_name[1]', Input::old('bus_name.1'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_type', trans('owner_details.business_type')) }}
								{{ Form::text('bus_type[1]', Input::old('bus_type.1'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_location', trans('owner_details.business_location')) }}
								<select id="bus_location_1" name="bus_location_1" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-4">
								{{ Form::label('bus_name', trans('messages.name')) }}
								{{ Form::text('bus_name[2]', Input::old('bus_name.2'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_type', trans('owner_details.business_type')) }}
								{{ Form::text('bus_type[2]', Input::old('bus_type.2'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('bus_location', trans('owner_details.business_location')) }}
								<select id="bus_location_2" name="bus_location_2" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('owner_details.key_manager')}}</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('km_firstname')) ? ' has-error' : '' }}">
							{{ Form::label('km_firstname', trans('owner_details.firstname')) }}
							{{ Form::text('km_firstname', Input::old('km_firstname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('km_middlename')) ? ' has-error' : '' }}">
							{{ Form::label('km_middlename', trans('owner_details.middlename')) }}
							{{ Form::text('km_middlename', Input::old('km_middlename'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('km_lastname')) ? ' has-error' : '' }}">
							{{ Form::label('km_lastname', trans('owner_details.lastname')) }}
							{{ Form::text('km_lastname', Input::old('km_lastname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('km_nationality')) ? ' has-error' : '' }}">
							{{ Form::label('km_nationality', trans('owner_details.nationality')) }}
							{{ Form::text('km_nationality', Input::old('km_nationality'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('km_birthdate')) ? ' has-error' : '' }}">
							{{ Form::label('km_birthdate', trans('owner_details.birthdate')) }}
							{{ Form::text('km_birthdate', Input::old('km_birthdate'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('km_civilstatus')) ? ' has-error' : '' }}">
							{{ Form::label('km_civilstatus', trans('owner_details.civilstatus')) }}
							{{ Form::select('km_civilstatus', array('' => trans('messages.choose_here'), '1' => trans('owner_details.married'), '2' => trans('owner_details.single'), '3' => trans('owner_details.separated')), Input::old('km_civilstatus'), array('id' => 'km_civilstatus', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-2{{ ($errors->has('km_profession')) ? ' has-error' : '' }}">
							{{ Form::label('km_profession', trans('owner_details.profession')) }}
							{{ Form::text('km_profession', Input::old('km_profession'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('km_email')) ? ' has-error' : '' }}">
							{{ Form::label('km_email', trans('owner_details.email')) }}
							{{ Form::text('km_email', Input::old('km_email'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('km_address1')) ? ' has-error' : '' }}">
							{{ Form::label('km_address1', trans('owner_details.address1')) }}
							{{ Form::text('km_address1', Input::old('km_address1'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('km_address2')) ? ' has-error' : '' }}">
							{{ Form::label('km_address2', trans('owner_details.address2')) }}
							{{ Form::text('km_address2', Input::old('km_address2'), array('class' => 'form-control')) }}
						</div>

						<div class="col-md-6{{ ($errors->has('km_province')) ? ' has-error' : '' }}">
							{{ Form::label('km_province', trans('owner_details.province')) }}
							<select id="km_province" name="km_province" class="form-control">
								<option>Choose your province</option>
								@foreach($provinceList2 as $ct)
									<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-6{{ ($errors->has('km_city')) ? ' has-error' : '' }}">
							{{ Form::label('km_cityid', trans('owner_details.city')) }}

							<select id="km_cityid" name="km_cityid" class="form-control">
								<option>Choose your city</option>
							</select>
						</div>

						<div class="col-md-6{{ ($errors->has('km_zipcode')) ? ' has-error' : '' }}">
							{{ Form::label('km_zipcode', 'Zipcode') }}
							{{ Form::text('km_zipcode', Input::old('km_zipcode'), array('class' => 'form-control', 'readonly' => 'readonly')) }}
						</div>

						<div class="col-md-6{{ ($errors->has('km_present_address_status')) ? ' has-error' : '' }}">
							{{ Form::label('km_present_address_status', trans('owner_details.present_address_status')) }}
							{{ Form::select('km_present_address_status', array('' => trans('messages.choose_here'), '1' => trans('owner_details.pas_owned'), '2' => trans('owner_details.pas_mortgaged'), '3' => trans('owner_details.pas_rented')), Input::old('km_present_address_status'), array('id' => 'km_present_address_status', 'class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('km_no_yrs_present_address')) ? ' has-error' : '' }}">
							{{ Form::label('km_no_yrs_present_address', trans('owner_details.no_years_present')) }}
							{{ Form::text('km_no_yrs_present_address', Input::old('km_no_yrs_present_address'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6{{ ($errors->has('km_phone')) ? ' has-error' : '' }}">
							{{ Form::label('km_phone', trans('owner_details.phone')) }}
							{{ Form::text('km_phone', Input::old('km_phone'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-12{{ ($errors->has('km_tin_num')) ? ' has-error' : '' }}">
							{{ Form::label('km_tin_num', 'TIN (e.g. 111-111-111 or 111-111-111-111A)') }}
							{{ Form::text('km_tin_num', Input::old('km_tin_num'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('km_percent_of_ownership')) ? ' has-error' : '' }}">
							{{ Form::label('km_percent_of_ownership', trans('owner_details.percent_ownership')) }}
							{{ Form::text('km_percent_of_ownership', Input::old('km_percent_of_ownership'), array('class' => 'form-control', 'placeholder' => 'Number Only')) }}
						</div>
						<div class="col-md-5{{ ($errors->has('km_number_of_year_engaged')) ? ' has-error' : '' }}">
							{{ Form::label('km_number_of_year_engaged', trans('owner_details.years_exp')) }}
							{{ Form::text('km_number_of_year_engaged', Input::old('km_number_of_year_engaged'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3{{ ($errors->has('km_position')) ? ' has-error' : '' }}">
							{{ Form::label('km_position', trans('owner_details.position')) }}
							{{ Form::text('km_position', Input::old('km_position'), array('class' => 'form-control')) }}
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('owner_details.educational_background')}}</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.highschool')) }}
								{{ Form::text('school_name[0]', Input::old('school_name.0'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[0]', '1') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[0]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.0'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.college')) }}
								{{ Form::text('school_name[1]', Input::old('school_name.1'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[1]', '2') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[1]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.1'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.postgrad')) }}
								{{ Form::text('school_name[2]', Input::old('school_name.2'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[2]', '3') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[2]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.2'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.others1')) }}
								{{ Form::text('school_name[3]', Input::old('school_name.3'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[3]', '4') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[3]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.3'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.others2')) }}
								{{ Form::text('school_name[4]', Input::old('school_name.4'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[4]', '5') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[4]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.4'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('school_name', trans('owner_details.others3')) }}
								{{ Form::text('school_name[5]', Input::old('school_name.5'), array('class' => 'form-control')) }}
								{{ Form::hidden('educ_type[5]', '6') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('educ_degree[5]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('educ_degree.5'), array('class' => 'form-control'))}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('owner_details.spouse_details')}}</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="col-md-4{{ ($errors->has('sfirstname')) ? ' has-error' : '' }}">
							{{ Form::label('sfirstname', trans('owner_details.firstname')) }}
							{{ Form::text('sfirstname', Input::old('sfirstname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('smiddlename')) ? ' has-error' : '' }}">
							{{ Form::label('smiddlename', trans('owner_details.middlename')) }}
							{{ Form::text('smiddlename', Input::old('smiddlename'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('slastname')) ? ' has-error' : '' }}">
							{{ Form::label('slastname', trans('owner_details.lastname')) }}
							{{ Form::text('slastname', Input::old('slastname'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('snationality')) ? ' has-error' : '' }}">
							{{ Form::label('snationality', trans('owner_details.nationality')) }}
							{{ Form::text('snationality', Input::old('snationality'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('sbirthdate')) ? ' has-error' : '' }}">
							{{ Form::label('sbirthdate', trans('owner_details.birthdate')) }}
							{{ Form::text('sbirthdate', Input::old('sbirthdate'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('sprofession')) ? ' has-error' : '' }}">
							{{ Form::label('sprofession', trans('owner_details.profession')) }}
							{{ Form::text('sprofession', Input::old('sprofession'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('emailspouse')) ? ' has-error' : '' }}">
							{{ Form::label('emailspouse', trans('owner_details.email')) }}
							{{ Form::text('emailspouse', Input::old('emailspouse'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('sphone')) ? ' has-error' : '' }}">
							{{ Form::label('sphone', trans('owner_details.phone')) }}
							{{ Form::text('sphone', Input::old('sphone'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-4{{ ($errors->has('stin_num')) ? ' has-error' : '' }}">
							{{ Form::label('stin_num', 'TIN (e.g. 111-111-111 or 111-111-111-111A)') }}
							{{ Form::text('stin_num', Input::old('stin_num'), array('class' => 'form-control')) }}
						</div>
						<div class="busownwrapper">
							<div class="col-md-4">
								{{ Form::label('sbus_name', trans('owner_details.sbusiness_name')) }}
								{{ Form::text('sbus_name[0]', Input::old('sbus_name.0'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_type', trans('owner_details.sbusiness_type')) }}
								{{ Form::text('sbus_type[0]', Input::old('sbus_type.0'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_location', trans('owner_details.sbusiness_location')) }}
								<select id="sbus_location_0" name="sbus_location_0" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-4">
								{{ Form::label('sbus_name', trans('owner_details.sbusiness_name')) }}
								{{ Form::text('sbus_name[1]', Input::old('sbus_name.1'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_type', trans('owner_details.sbusiness_type')) }}
								{{ Form::text('sbus_type[1]', Input::old('sbus_type.1'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_location', trans('owner_details.sbusiness_location')) }}
								<select id="sbus_location_1" name="sbus_location_1" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-4">
								{{ Form::label('sbus_name', trans('owner_details.sbusiness_name')) }}
								{{ Form::text('sbus_name[2]', Input::old('sbus_name.2'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_type', trans('owner_details.sbusiness_type')) }}
								{{ Form::text('sbus_type[2]', Input::old('sbus_type.2'), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-4">
								{{ Form::label('sbus_location', trans('owner_details.sbusiness_location')) }}
								<select id="sbus_location_2" name="sbus_location_2" class="form-control">
									<option>Choose your location</option>
									@foreach($cityList as $ct)
										<option value="{{$ct}}" data-zipcode="{{$ct}}" >{{$ct}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<h4>{{trans('owner_details.spouse_educ_bg')}}</h4>
				<div class="read-only-text">
					<div class="form-group">
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.highschool')) }}
								{{ Form::text('sschool_name[0]', Input::old('sschool_name.0'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[0]', '1') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('seduc_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[0]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.0'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.college')) }}
								{{ Form::text('sschool_name[1]', Input::old('sschool_name.1'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[2]', '2') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('seduc_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[1]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.1'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.postgrad')) }}
								{{ Form::text('sschool_name[2]', Input::old('sschool_name.2'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[2]', '3') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('seduc_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[2]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.2'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.others1')) }}
								{{ Form::text('sschool_name[3]', Input::old('sschool_name.3'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[3]', '4') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('seduc_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[3]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.3'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.others2')) }}
								{{ Form::text('sschool_name[4]', Input::old('sschool_name.4'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[4]', '5') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('seduc_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[4]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.4'), array('class' => 'form-control'))}}
							</div>
						</div>
						<div class="busownwrapper">
							<div class="col-md-6">
								{{ Form::label('sschool_name', trans('owner_details.others3')) }}
								{{ Form::text('sschool_name[5]', Input::old('sschool_name.5'), array('class' => 'form-control')) }}
								{{ Form::hidden('seduc_type[5]', '6') }}
							</div>
							<div class="col-md-6">
								{{ Form::label('educ_degree', trans('owner_details.level')) }}
								{{ Form::select('seduc_degree[5]', array(
									'Not Taken'=>trans('owner_details.lvl_not_taken'),
									'Graduated'=>trans('owner_details.lvl_graduated'),
									'Ongoing'=>trans('owner_details.lvl_ongoing'),
									'Delayed'=>trans('owner_details.lvl_delayed')
								), Input::old('seduc_degree.5'), array('class' => 'form-control'))}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
                    <a href = '#' class = 'btn btn-large btn-primary form-back-btn'> <i class = 'glyphicon glyphicon-chevron-left'> </i> {{ trans('messages.back') }} </a>
					<input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/busowner.js') }}"></script>
	<script>
	$(document).ready(function(){
		$('#province').on('change', function(){
			var sProvince = $(this).val();
			var uiCity = $('#cityid');
			$.ajax({
				url: BaseURL + '/api/cities',
				type: 'get',
				dataType: 'json',
				data: {
					province: sProvince
				},
				beforeSend: function() {
					uiCity.html('<option>Loading cities...</option>');
				},
				success: function(oData)	{
					let cities = oData.data.cities;
					uiCity.html('<option value="">Choose your city</option>');
					for(x in cities){
						uiCity.append('<option value="'+cities[x].id+'" data-zipcode="'+cities[x].zipcode+'">'+cities[x].city+'</option>');
					}
				}
			});
		});

		$('#cityid').on('change', function(){
			var uiCity = $(this);
			$('#zipcode').val(uiCity.find('option:selected').data('zipcode'));
		});

		$('#km_province').on('change', function(){
			var sProvince = $(this).val();
			var uiCity = $('#km_cityid');
			$.ajax({
				url: BaseURL + '/api/cities',
				type: 'get',
				dataType: 'json',
				data: {
					province: sProvince
				},
				beforeSend: function() {
					uiCity.html('<option>Loading cities...</option>');
				},
				success: function(oData)	{
					let cities = oData.data.cities;
					uiCity.html('<option value="">Choose your city</option>');
					for(x in cities){
						uiCity.append('<option value="'+cities[x].id+'" data-zipcode="'+cities[x].zipcode+'">'+cities[x].city+'</option>');
					}
				}
			});
		});

		$('#km_cityid').on('change', function(){
			var uiCity = $(this);
			$('#km_zipcode').val(uiCity.find('option:selected').data('zipcode'));
		});
	});

	</script>
@stop
