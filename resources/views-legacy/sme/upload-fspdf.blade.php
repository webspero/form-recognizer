{{ Form::open( array('route' => array('postUploadFspdfFiles', $entity_id), 'role'=>'form', 'files' => true, 'id'=>'fspdf-upload-expand-form')) }}
   <div class="row">
         <div class = 'col-md-12'>
            <div class="alert alert-danger pop-error-msg"
               role="alert" style = 'display: none;'>
            </div>
         </div>
   </div>

   <div class="row file-container" id="upload-file-container">
         <div class="form-group">
            <div class="col-md-12">
               <label for="fspdf_file">{{ trans('messages.upload_file') }}</label>
               {{ Form::file('fspdf_file[]', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'required', 'id' => 'fs-raw-file']) }}
            </div>
         </div>
   </div>

   <div class="row">
         <div class="col-md-11" style="margin-top: 2px;">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" id="btn-submit-fs-raw" value="{{ trans('messages.upload') }}">
         </div>
   </div>
{{ Form::close() }}

@if (STS_NG == $_COOKIE['formdata_support'])
</div></div></div></div>
@stop
@section('javascript')
@endif
   <script type="text/javascript">
      $('#fs-raw-file').bind('change', function() {
         var fileSize = this.files[0].size/1024/1024;
         if(fileSize > 40){
            $('.pop-error-msg').show();
            $('.pop-error-msg').html('Maximum file upload is 40MB');
            $('#btn-submit-fs-raw').prop('disabled',true);

         }else{
            $('.pop-error-msg').hide();
            $('.pop-error-msg').html('');
            $('#btn-submit-fs-raw').prop('disabled',false);
         }
     });
   </script>
   <script type="text/javascript" src="{{URL::asset('js/bsis_upload.js')}}"></script>
@if (STS_NG == $_COOKIE['formdata_support'])
@stop
@endif
