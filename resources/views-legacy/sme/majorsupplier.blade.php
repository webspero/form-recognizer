	<div class="row">
			{{ Form::open( array('route' => array('postMajorsupplier', $entity_id), 'role'=>'form', 'class'=>'majorsupplier', 'id' => 'major-supp-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">

                @for($i = 0; $i < 1; $i++)
                <div class="col-md-3{{ ($errors->has('supplier_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_name'.$i.'', ($entity->is_premium == 1)? trans('messages.reg_name').' *':trans('messages.name').' *') }}
                {{ Form::text('supplier_name['.$i.']', Input::old('supplier_name.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_address.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_address'.$i.'', ($entity->is_premium == 1)? trans('messages.reg_address').' *': 'Address *') }}
                {{ Form::text('supplier_address['.$i.']', Input::old('supplier_address.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_contact_person.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_contact_person'.$i.'', ($entity->is_premium == 1)? trans('messages.contact_person') : trans('messages.contact_person').' *') }}
                {{ Form::text('supplier_contact_person['.$i.']', Input::old('supplier_contact_person.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_phone.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_phone'.$i.'', ($entity->is_premium == 1)? trans('messages.contact_phone') : trans('messages.contact_phone').' *') }}
                {{ Form::text('supplier_phone['.$i.']', Input::old('supplier_phone.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_email.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_email'.$i.'', ($entity->is_premium == 1)? trans('messages.contact_email') : trans('messages.contact_email').' *') }}
                {{ Form::text('supplier_email['.$i.']', Input::old('supplier_email.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_started_years.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_started_years'.$i.'', ($entity->is_premium == 1)? trans('business_details2.ms_year_started'):  trans('business_details2.ms_year_started').' *') }}
                {{ Form::text('supplier_started_years['.$i.']', Input::old('supplier_started_years.'.$i.''), array('id' => 'supplier_started_years'.$i.'', 'class' => 'supplier_started_years form-control', 'data-idnumber' => ''.$i.'', 'placeholder' => 'YYYY')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('supplier_years_doing_business.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_years_doing_business'.$i.'', ($entity->is_premium == 1)? trans('business_details2.ms_year_doing_business'): trans('business_details2.ms_year_doing_business').' *') }}
                {{ Form::text('supplier_years_doing_business['.$i.']', Input::old('supplier_years_doing_business.'.$i.''), array('id' => 'supplier_years_doing_business'.$i.'', 'class' => 'form-control', 'readonly' => 'readonly')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('relationship_satisfaction.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label(
                        'supplier_relationship_satisfaction'.$i.'',
                        ($entity->is_premium == 1)? trans('business_details2.mc_relationship_satisfaction') : trans('business_details2.mc_relationship_satisfaction').' *')
                    }}
                    {{ Form::select(
                        'relationship_satisfaction['.$i.']',
                        ['' => trans('messages.choose_here')] + $relationship_satisfaction,
                        Input::old('relationship_satisfaction.'.$i),
                        [
                            'id' => 'supplier_relationship_satisfaction'.$i,
                            'class' => 'form-control'
                        ])
                    }}
                </div>
                <div class="col-md-4{{ ($errors->has('supplier_share_sales.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_share_sales'.$i.'', ($entity->is_premium == 1)?  trans('business_details2.ms_percentage_share'): trans('business_details2.ms_percentage_share').' *') }}
                {{ Form::text('supplier_share_sales['.$i.']', Input::old('supplier_share_sales.'.$i.''), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-4{{ ($errors->has('supplier_order_frequency.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_order_frequency'.$i.'', ($entity->is_premium == 1)? trans('business_details2.ms_frequency') : trans('business_details2.ms_frequency').' *') }}
                {{ Form::select('supplier_order_frequency['.$i.']',
                    array('' => trans('messages.choose_here'),
                        'ms_cod' => trans('business_details2.ms_cod'),
                        'ms_monthly' => trans('business_details2.ms_monthly'),
                        'ms_bimonthly' => trans('business_details2.ms_bimonthly'),
                        'ms_weekly' => trans('business_details2.ms_weekly'),
                        'ms_daily' => trans('business_details2.ms_daily')
                    ), Input::old('supplier_order_frequency.'.$i.''), array('id' => 'supplier_order_frequency'.$i.'', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-4{{ ($errors->has('supplier_settlement.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('supplier_settlement'.$i.'', ($entity->is_premium == 1)?  trans('business_details2.ms_payment') : trans('business_details2.ms_payment').' *') }} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('business_details2.ms_payment_hint')}}">More info...</a>
                {{ Form::select('supplier_settlement['.$i.']',
                    array('' => trans('messages.choose_here'),
                    '1' => trans('business_details2.ms_ahead'),
                    '2' => trans('business_details2.ms_ondue'),
                    '3' => trans('business_details2.ms_portion'),
                    '4' => trans('business_details2.ms_beyond'),
                    '5' => trans('business_details2.ms_delinquent')
                    ), Input::old('supplier_settlement.'.$i.''), array('id' => 'supplier_settlement_'.$i.'', 'class' => 'form-control')) }}
                </div>
				<div class="col-md-12{{ ($errors->has('average_monthly_volume.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('average_monthly_volume'.$i.'', ($entity->is_premium == 1)? 'Average monthly volume' : 'Average monthly volume *') }}
                {{ Form::text('average_monthly_volume['.$i.']', Input::old('average_monthly_volume.'.$i.''), array('class' => 'form-control')) }}
				<small>* derived from 3 months’ figure input (Your payment to supplier in last 3 payments)</small>
                </div>
                <div class="col-md-12"><hr/></div>
                <div class="col-md-12">
					{{ Form::label('items'.$i.'',  'Items Purchased (List of three distinct items)') }}
					<p>
                        {{ Form::text('item_1['.$i.']', Input::old('item_1.'.$i.''), array('class' => 'form-control')) }}
                    </p>
					<p>
                        {{ Form::text('item_2['.$i.']', Input::old('item_2.'.$i.''), array('class' => 'form-control')) }}
                    </p>
					<p>
                        {{ Form::text('item_3['.$i.']', Input::old('item_3.'.$i.''), array('class' => 'form-control')) }}
                    </p>
				</div>
				<div class="col-md-12">
				</div>
                @endfor
            </div>

            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>

    <script>
        var report = $('#anonymous-report');

        if (report.prop('checked')) {

            $('[name="supplier_name[0]"]').prop('disabled', true).val('Major Supplier');
            $('.submit-expander-form').click(function() {
                $('[name="supplier_name[0]"]').prop('disabled', false);
            });
        }

    </script>
