@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Score: Major Supplier </h1>
		{{ Form::model($entity, ['route' => ['putScoreMajorSupplierUpdate', $id], 'method' => 'put']) }}
                Your major suppliers are:
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<?php $x=0; ?>
						@foreach ($entity as $entityinfo)
						<?php $i = $x++; ?>
						<span class="details-row">
							{{ Form::hidden('majorsupplierid['.$i.']', $entityinfo->majorsupplierid, array('id' => 'majorsupplierid')) }}
							<div class="col-md-4"><b>Name</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_name }}</div>
							<div class="col-md-4"><b>Address</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_address }}</div>
							<div class="col-md-4"><b>Contact Person</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_contact_person }}</div>
							<div class="col-md-4"><b>Contact Details (Phone/Email)</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_contact_details }}</div>
							<div class="col-md-4"><b>Year Started Doing Business With</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_started_years }}</div>
							<div class="col-md-4"><b>Years Doing Business With</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_years_doing_business }}</div>
							<div class="col-md-4"><b>Settlement in last 3 months</b></div>
							<div class="col-md-8">
							@if ($entityinfo->supplier_settlement == 1)
				              	Ahead of Due Date
				            @elseif ($entityinfo->supplier_settlement == 2)
				              	On Due Date
				            @elseif ($entityinfo->supplier_settlement == 3)
				              	Portion settled On Due Date
				            @elseif($entityinfo->supplier_settlement == 4)
				              	Settled beyond Due Date
				            @else
				              	Delinquent
				            @endif
							</div>
							<div class="col-md-4"><b>Order Frequency</b></div>
							<div class="col-md-8">{{ $entityinfo->supplier_order_frequency }}</div>
							<div class="col-md-4"><b>Experience</b></div>
							<div class="col-md-8">
							{{ Form::select('supplier_experience['.$i.']',
								array('' => 'Choose here',
								'40' => 'Very Satisfactory',
								'27' => 'Satisfactory',
								'13' => 'Unsatisfactory',
								'0' => 'With Intention to terminate relationship'
								), Input::old('supplier_experience.'.$i.''), array('id' => 'supplier_experience'.$i.'', 'class' => 'form-control')) }}
							</div>
							</span>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
