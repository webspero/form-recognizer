@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Community Reputation</h1>
		    {{ Form::model($entity, ['route' => ['putReputationUpdate', $entity[0]->communityreputationid], 'method' => 'put']) }}
		@else
			<h1>Add: Community Reputation</h1>
			{{ Form::open( array('route' => 'postReputation', 'role'=>'form')) }}
			{{ Form::hidden('givenid', $dataowner) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-3{{ ($errors->has('assoc_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('assoc_name['.$i.']', ($errors->has('assoc_name.'.$i.'')) ? Input::old('assoc_name.'.$i.'') : $entity[$i]->assoc_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('assoc_name['.$i.']', Input::old('assoc_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_jurisdiction.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_jurisdiction'.$i.'', 'Jurisdiction') }}
							@if(isset($entity))
								{{ Form::select('assoc_jurisdiction['.$i.']',
									array('' => 'Choose here',
									'Local' => 'Local',
									'National International' => 'National International',
									), ($errors->has('assoc_jurisdiction.'.$i.'')) ? Input::old('assoc_jurisdiction.'.$i.'') : $entity[$i]->assoc_jurisdiction, array('id' => 'assoc_jurisdiction'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('assoc_jurisdiction['.$i.']',
									array('' => 'Choose here',
									'Local' => 'Local',
									'National International' => 'National International'
									), Input::old('assoc_jurisdiction.'.$i.''), array('id' => 'assoc_jurisdiction'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_type.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_type'.$i.'', 'Type') }}
							@if(isset($entity))
								{{ Form::select('assoc_type['.$i.']',
									array('' => 'Choose here',
									'Civic' => 'Civic',
									'Religious' => 'Religious',
									'Business' => 'Business'
									), ($errors->has('assoc_type.'.$i.'')) ? Input::old('assoc_type.'.$i.'') : $entity[$i]->assoc_type, array('id' => 'assoc_type'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('assoc_type['.$i.']',
									array('' => 'Choose here',
									'Civic' => 'Civic',
									'Religious' => 'Religious',
									'Business' => 'Business'
									), Input::old('assoc_type.'.$i.''), array('id' => 'assoc_type'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_dues.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_dues'.$i.'', 'Payment of Association Dues') }}
							@if(isset($entity))
								{{ Form::select('assoc_dues['.$i.']',
									array('' => 'Choose here',
									'Current' => 'Current',
									'Delinquent' => 'Delinquent'
									), ($errors->has('assoc_dues.'.$i.'')) ? Input::old('assoc_dues.'.$i.'') : $entity[$i]->assoc_dues, array('id' => 'assoc_dues'.$i.'', 'class' => 'form-control')) }}
							@else
								{{ Form::select('assoc_dues['.$i.']',
									array('' => 'Choose here',
									'Current' => 'Current',
									'Delinquent' => 'Delinquent'
									), Input::old('assoc_dues.'.$i.''), array('id' => 'assoc_dues'.$i.'', 'class' => 'form-control')) }}
							@endif
							</div>
							@endfor
						@else
							@for($i = 0; $i < 3; $i++)
							<div class="col-md-3{{ ($errors->has('assoc_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_name'.$i.'', 'Name') }}
							{{ Form::text('assoc_name['.$i.']', Input::old('assoc_name.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_jurisdiction.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_jurisdiction'.$i.'', 'Jurisdiction') }}
							{{ Form::select('assoc_jurisdiction['.$i.']',
								array('' => 'Choose here',
								'Local' => 'Local',
								'National International' => 'National International'
								), Input::old('assoc_jurisdiction.'.$i.''), array('id' => 'assoc_jurisdiction'.$i.'', 'class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_type.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_type'.$i.'', 'Type') }}
							{{ Form::select('assoc_type['.$i.']',
								array('' => 'Choose here',
								'Civic' => 'Civic',
								'Religious' => 'Religious',
								'Business' => 'Business'
								), Input::old('assoc_type.'.$i.''), array('id' => 'assoc_type'.$i.'', 'class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('assoc_dues.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('assoc_dues'.$i.'', 'Payment of Association Dues') }}
							{{ Form::select('assoc_dues['.$i.']',
								array('' => 'Choose here',
								'Current' => 'Current',
								'Delinquent' => 'Delinquent'
								), Input::old('assoc_dues.'.$i.''), array('id' => 'assoc_dues'.$i.'', 'class' => 'form-control')) }}
							</div>
							@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
