
	<div class="row">
			{{ Form::open(array('route' => ['postKeyManager', $entity_id, $ownerid], 'method' => 'post', 'id' => 'biz-manager-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                <div class="col-md-4{{ ($errors->has('firstname')) ? ' has-error' : '' }}">
                    {{ Form::label('firstname', 'First Name') }} *
                    {{ Form::text('firstname', isset($entity) ? $entity->firstname : Input::old('firstname'), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-4{{ ($errors->has('middlename')) ? ' has-error' : '' }}">
                    {{ Form::label('middlename', 'Middle Name') }} *
                    {{ Form::text('middlename', isset($entity) ? $entity->middlename : Input::old('middlename'), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-4{{ ($errors->has('lastname')) ? ' has-error' : '' }}">
                    {{ Form::label('lastname', 'Last Name') }} *
                    {{ Form::text('lastname', isset($entity) ? $entity->lastname : Input::old('lastname'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-2{{ ($errors->has('nationality')) ? ' has-error' : '' }}">
                    {{ Form::label('nationality', 'Nationality') }} *
                    {{ Form::text('nationality', isset($entity) ? $entity->nationality : Input::old('nationality'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-2{{ ($errors->has('birthdate')) ? ' has-error' : '' }}">
                    {{ Form::label('birthdate', 'Birthdate') }} *
                    {{ Form::text('birthdate', isset($entity) ? $entity->birthdate : Input::old('birthdate'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-2{{ ($errors->has('civilstatus')) ? ' has-error' : '' }}">
                    {{ Form::label('civilstatus', 'Civil Status') }} *
                    {{ Form::select('civilstatus', array('' => 'Choose here', '1' => 'Married', '2' => 'Single', '3' => 'Separated'), isset($entity) ? $entity->civilstatus : Input::old('civilstatus'), array('id' => 'civilstatus', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-2{{ ($errors->has('profession')) ? ' has-error' : '' }}">
                    {{ Form::label('profession', 'Profession') }} *
                    {{ Form::text('profession', isset($entity) ? $entity->profession : Input::old('profession'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-4{{ ($errors->has('email')) ? ' has-error' : '' }}">
                    {{ Form::label('email', 'E-mail Address') }} *
                    {{ Form::text('email', isset($entity) ? $entity->email : Input::old('email'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('address1')) ? ' has-error' : '' }}">
                    {{ Form::label('address1', 'Address Line 1 (Street Address)') }} *
                    {{ Form::text('address1', isset($entity) ? $entity->address1 : Input::old('address1'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('address2')) ? ' has-error' : '' }}">
                    {{ Form::label('address2', 'Address Line 2 (Apartment No., Complex No. Block, etc.)') }} 
                    {{ Form::text('address2', isset($entity) ? $entity->address2 : Input::old('address2'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('province')) ? ' has-error' : '' }}">
					{{ Form::label('province', 'Province/Region') }} *
					{{ Form::select('province', array('' => 'Choose your province') + $provinceList, isset($entity) ? $entity->province : Input::old('province'), array('id' => 'province', 'class' => 'form-control')) }}
				</div>
				<div class="col-md-6{{ ($errors->has('city')) ? ' has-error' : '' }}">
					{{ Form::label('cityid', 'City') }} *
					
					<select id="cityid" name="cityid" class="form-control">
						<option>Choose your city</option>
						@foreach($cityList as $ct)
							<option value="{{$ct->id}}" data-zipcode="{{$ct->zip_code}}" {{ (Input::old('cityid')==$ct->id) ? "selected='selected'" : '' }}>{{$ct->city}}</option>
						@endforeach
					</select>
				</div>
                <div class="col-md-6{{ ($errors->has('zipcode')) ? ' has-error' : '' }}">
                    {{ Form::label('zipcode', 'Zipcode') }}
                    {{ Form::text('zipcode', isset($entity) ? $entity->zipcode : Input::old('zipcode'), array('class' => 'form-control', 'readonly' => 'readonly')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('present_address_status')) ? ' has-error' : '' }}">
                    {{ Form::label('present_address_status', trans('owner_details.present_address_status')) }} *
                    {{ Form::select('present_address_status', array(
                        '' => trans('messages.choose_here'), 
                        '1' => trans('owner_details.pas_owned'), 
                        '2' => trans('owner_details.pas_mortgaged'), 
                        '3' => trans('owner_details.pas_rented')
                    ), isset($entity) ? $entity->present_address_status : Input::old('present_address_status'), array('id' => 'present_address_status', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('no_yrs_present_address')) ? ' has-error' : '' }}">
                    {{ Form::label('no_yrs_present_address', trans('owner_details.no_years_present')) }} *
                    {{ Form::text('no_yrs_present_address', isset($entity) ? $entity->no_yrs_present_address : Input::old('no_yrs_present_address'), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-6{{ ($errors->has('phone')) ? ' has-error' : '' }}">
                    {{ Form::label('phone', 'Phone Number') }} *
                    {{ Form::text('phone', isset($entity) ? $entity->phone : Input::old('phone'), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-12{{ ($errors->has('tin_num')) ? ' has-error' : '' }}">
                    {{ Form::label('tin_num', 'TIN') }} * 
                    {{ Form::text('tin_num', isset($entity) ? $entity->tin_num : Input::old('tin_num'), array('class' => 'form-control', 'onkeydown' => 'addHyphen(this)')) }}
                </div>						
                <div class="col-md-4{{ ($errors->has('percent_of_ownership')) ? ' has-error' : '' }}">
                    {{ Form::label('percent_of_ownership', trans('owner_details.percent_ownership')) }} *
                    {{ Form::text('percent_of_ownership', isset($entity) ? $entity->percent_of_ownership : Input::old('percent_of_ownership'), array('class' => 'form-control', 'placeholder' => 'Number Only')) }}
                </div>						
                <div class="col-md-5{{ ($errors->has('number_of_year_engaged')) ? ' has-error' : '' }}">
                    {{ Form::label('number_of_year_engaged', trans('owner_details.years_exp')) }} *
                    {{ Form::text('number_of_year_engaged', isset($entity) ? $entity->number_of_year_engaged : Input::old('number_of_year_engaged'), array('class' => 'form-control')) }}
                </div>						
                <div class="col-md-3{{ ($errors->has('position')) ? ' has-error' : '' }}">
                    {{ Form::label('position', trans('owner_details.position')) }} *
                    {{ Form::text('position', isset($entity) ? $entity->position : Input::old('position'), array('class' => 'form-control')) }}
                </div>
            </div>

            {{ Form::token() }}

		{{ Form::close() }}
	</div>
    
    <script>
        $(document).ready(function() {
            $("#birthdate").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                yearRange: '-100:+0',
                onChangeMonthYear: function(y, m, i) {
                        var d = i.selectedDay;
                        $(this).datepicker('setDate', new Date(y, m - 1, d));
                    }
            });
			
			$('#province').on('change', function(){
				var sProvince = $(this).val();
				var uiCity = $('#cityid');
				$.ajax({
					url: BaseURL + '/api/cities',
					type: 'get',
					dataType: 'json',
					data: {
						province: sProvince
					},
					beforeSend: function() {
						uiCity.html('<option>Loading cities...</option>');
					},
					success: function(oData)	{
						uiCity.html('<option value="">Choose your city</option>');
						for(x in oData){
							uiCity.append('<option value="'+oData[x].id+'" data-zipcode="'+oData[x].zip_code+'">'+oData[x].city+'</option>');
						}
					}
				});
			});
			
			$('#cityid').on('change', function(){
				var uiCity = $(this);
				$('#zipcode').val(uiCity.find('option:selected').data('zipcode'));
			});
        });
    </script>