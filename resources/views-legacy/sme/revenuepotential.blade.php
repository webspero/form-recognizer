
	<div class="row">
        {{ Form::open( array('route' => array('postRevenuePotential', $entity_id), 'role'=>'form', 'id' => 'rev-growth-expand-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                    @for($i = 0; $i < 1; $i++)
                    <div class="col-md-6{{ ($errors->has('current_market.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('current_market'.$i.'', trans('condition_sustainability.under_current_market')) }}
                    {{ Form::select('current_market['.$i.']', 
                        array('' => trans('messages.choose_here'), 
                        '1' => '0%',
                        '2' => trans('condition_sustainability.less_3_percent'),
                        '3' => trans('condition_sustainability.between_3to5_percent'),
                        '4' => trans('condition_sustainability.greater_5_percent')
                        ), Input::old('current_market.'.$i.''), array('id' => 'current_market'.$i.'', 'class' => 'form-control')) }}
                    </div>					
                    <div class="col-md-6{{ ($errors->has('new_market.'.$i.'')) ? ' has-error' : '' }}">
                    {{ Form::label('new_market'.$i.'', trans('condition_sustainability.expected_future_market')) }}
                    {{ Form::select('new_market['.$i.']', 
                        array('' => trans('messages.choose_here'), 
                        '1' => '0%',
                        '2' => trans('condition_sustainability.less_3_percent'),
                        '3' => trans('condition_sustainability.between_3to5_percent'),
                        '4' => trans('condition_sustainability.greater_5_percent')
                        ), Input::old('new_market.'.$i.''), array('id' => 'new_market'.$i.'', 'class' => 'form-control')) }}
                    </div>
                    @endfor
            </div>	
	
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>
            
		{{ Form::close() }}
	</div>