@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>Score: Major Customer </h1>
		{{ Form::model($entity, ['route' => ['putScoreMajorCustomerUpdate', $id], 'method' => 'put']) }}
                Your main customers who account for 80% of your sales are:
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
					<div class="form-group">
						<?php $x=0; ?>
						@foreach ($entity as $entityinfo)
						<?php $i = $x++; ?>
						<span class="details-row">
							{{ Form::hidden('majorcustomerrid['.$i.']', $entityinfo->majorcustomerrid, array('id' => 'majorcustomerrid')) }}
							<div class="col-md-4"><b>Name</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_name }}</div>
							<div class="col-md-4"><b>Address</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_address }}</div>
							<div class="col-md-4"><b>Contact Person</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_contact_person }}</div>
							<div class="col-md-4"><b>Contact Details (Phone/Email)</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_contact_details }}</div>
							<div class="col-md-4"><b>Year Started Doing Business With</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_started_years }}</div>
							<div class="col-md-4"><b>Years Doing Business With</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_years_doing_business }}</div>
							<div class="col-md-4"><b>Settlement in last 3 months</b></div>
							<div class="col-md-8">
							@if ($entityinfo->customer_settlement == 1)
				              	Ahead of Due Date
				            @elseif ($entityinfo->customer_settlement == 2)
				              	On Due Date
				            @elseif ($entityinfo->customer_settlement == 3)
				              	Portion settled On Due Date
				            @elseif($entityinfo->customer_settlement == 4)
				              	Settled beyond Due Date
				            @else
				              	Delinquent
				            @endif
							</div>
							<div class="col-md-4"><b>Order Frequency</b></div>
							<div class="col-md-8">{{ $entityinfo->customer_order_frequency }}</div>
							<div class="col-md-4"><b>Experience</b></div>
							<div class="col-md-8">
							{{ Form::select('customer_experience['.$i.']',
								array('' => 'Choose here',
								'40' => 'Very Satisfactory',
								'27' => 'Satisfactory',
								'13' => 'Unsatisfactory',
								'0' => 'With Intention to terminate relationship'
								), Input::old('customer_experience.'.$i.''), array('id' => 'customer_experience'.$i.'', 'class' => 'form-control')) }}
							</div>
							</span>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
