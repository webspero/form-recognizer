@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		@if(isset($entity))
			<h1>Edit: Adverse Record</h1>
		    {{ Form::model($entity, ['route' => ['putAdverserecordUpdate', $entity[0]->adverserecordid], 'method' => 'put']) }}
		@else
			<h1>Add: Adverse Record</h1>
			{{ Form::open( array('route' => ['postAdverserecord', $entity_id], 'role'=>'form')) }}
		@endif
		<div class="spacewrapper">
			<div class="read-only">
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				<div class="read-only-text">
					<div class="form-group">
						@if(isset($entity))
							@for($i = 0; $i < 1; $i++)
							<div class="col-md-3{{ ($errors->has('adverserecord_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_name'.$i.'', 'Name') }}
							@if(isset($entity))
								{{ Form::text('adverserecord_name['.$i.']', ($errors->has('adverserecord_name.'.$i.'')) ? Input::old('adverserecord_name.'.$i.'') : $entity[$i]->adverserecord_name, array('class' => 'form-control')) }}
							@else
								{{ Form::text('adverserecord_name['.$i.']', Input::old('adverserecord_name.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_products.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_products'.$i.'', 'Product') }}
							@if(isset($entity))
								{{ Form::text('adverserecord_products['.$i.']', ($errors->has('adverserecord_products.'.$i.'')) ? Input::old('adverserecord_products.'.$i.'') : $entity[$i]->adverserecord_products, array('class' => 'form-control')) }}
							@else
								{{ Form::text('adverserecord_products['.$i.']', Input::old('adverserecord_products.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_pastdue.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_pastdue'.$i.'', 'Date Applied') }}
							@if(isset($entity))
								{{ Form::text('adverserecord_pastdue['.$i.']', ($errors->has('adverserecord_pastdue.'.$i.'')) ? Input::old('adverserecord_pastdue.'.$i.'') : $entity[$i]->adverserecord_pastdue, array('class' => 'form-control withdate')) }}
							@else
								{{ Form::text('adverserecord_pastdue['.$i.']', Input::old('adverserecord_pastdue.'.$i.''), array('class' => 'form-control withdate')) }}
							@endif
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_dueamount.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_dueamount'.$i.'', 'Status') }}
							@if(isset($entity))
								{{ Form::text('adverserecord_dueamount['.$i.']', ($errors->has('adverserecord_dueamount.'.$i.'')) ? Input::old('adverserecord_dueamount.'.$i.'') : $entity[$i]->adverserecord_dueamount, array('class' => 'form-control')) }}
							@else
								{{ Form::text('adverserecord_dueamount['.$i.']', Input::old('adverserecord_dueamount.'.$i.''), array('class' => 'form-control')) }}
							@endif
							</div>
							@endfor
						@else
							@for($i = 0; $i < 3; $i++)
							<div class="col-md-3{{ ($errors->has('adverserecord_name.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_name'.$i.'', 'Name') }}
							{{ Form::text('adverserecord_name['.$i.']', Input::old('adverserecord_name.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_products.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_products'.$i.'', 'Product') }}
							{{ Form::text('adverserecord_products['.$i.']', Input::old('adverserecord_products.'.$i.''), array('class' => 'form-control')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_pastdue.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_pastdue'.$i.'', 'Past Due') }}
							{{ Form::text('adverserecord_pastdue['.$i.']', Input::old('adverserecord_pastdue.'.$i.''), array('class' => 'form-control withdate')) }}
							</div>
							<div class="col-md-3{{ ($errors->has('adverserecord_dueamount.'.$i.'')) ? ' has-error' : '' }}">
							{{ Form::label('adverserecord_dueamount'.$i.'', 'Due Amount') }}
							{{ Form::text('adverserecord_dueamount['.$i.']', Input::old('adverserecord_dueamount.'.$i.''), array('class' => 'form-control')) }}
							</div>
							@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" class="btn btn-large btn-primary" value="Save">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
@stop
@section('javascript')
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
@stop
