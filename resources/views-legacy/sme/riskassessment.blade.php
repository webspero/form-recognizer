
<div class="row">
    {{ Form::open( array('route' => array('postRiskassessment', $entity_id), 'role'=>'form', 'id' => 'ra-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

        <div class="form-group">
            @for($i = 0; $i < 1; $i++)
            <div class="col-md-5{{ ($errors->has('risk_assessment_name.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('risk_assessment_name'.$i.'', trans('condition_sustainability.threat')) }}
            {{ Form::select('risk_assessment_name['.$i.']', 
                array('' => trans('messages.choose_here'), 
                    'threat_of_new_entrants' => trans('condition_sustainability.threat_of_new_entrants'),
                    'threat_of_substitute' => trans('condition_sustainability.threat_of_substitute'),
                    'buyer_bargainer_pow' => trans('condition_sustainability.buyer_bargainer_pow'),
                    'supplier_bargainer_pow' => trans('condition_sustainability.supplier_bargainer_pow'),
                    'rivalry_intensity' => trans('condition_sustainability.rivalry_intensity')
                ), Input::old('risk_assessment_name.'.$i.''), array('id' => 'risk_assessment_name'.$i.'', 'class' => 'form-control validate[required]')) }}
            </div>
            <div class="col-md-5{{ ($errors->has('risk_assessment_solution.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('risk_assessment_solution'.$i.'', trans('condition_sustainability.counter_measures')) }} <a href="#" class="popup-hint popup-hint-content" data-hint="

                {{ trans('condition_sustainability.cost_leadership') }}<br />

                {{ trans('condition_sustainability.product_differentiation') }}<br />

                {{ trans('condition_sustainability.focus_market') }}">More info...</a>
            {{ Form::select('risk_assessment_solution['.$i.']', 
                array('' => trans('messages.choose_here'), 
                    'cost_leadership_sel' => trans('condition_sustainability.cost_leadership_sel'),
                    'product_innovation' => trans('condition_sustainability.product_innovation'),
                    'focus' => trans('condition_sustainability.focus')
                ), Input::old('risk_assessment_solution.'.$i.''), array('id' => 'risk_assessment_solution'.$i.'', 'class' => 'form-control validate[required]')) }}
            </div>
        </div>
        @endfor

        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

    {{ Form::close() }}
</div>