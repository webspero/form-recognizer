
	<div class="row">
        {{ Form::open( array('route' => array('postInsurance', $entity_id), 'role'=>'form', 'id' => 'insurance-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
                @for($i = 0; $i < 1; $i++)						
                <div class="col-md-5{{ ($errors->has('insurance_type.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('insurance_type'.$i.'', trans('business_details1.insurance_type')) }}
                {{ Form::text('insurance_type['.$i.']', Input::old('insurance_type.'.$i.''), array('class' => 'form-control')) }}
                </div>
                
                <div class="col-md-5{{ ($errors->has('insured_amount.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('insured_amount'.$i.'', trans('business_details1.insured_amount')) }}
                {{ Form::text('insured_amount['.$i.']', Input::old('insured_amount.'.$i.''), array('class' => 'form-control')) }}
                </div>
                @endfor
        </div>
        
        <div class="spacewrapper">
            {{ Form::token() }}
            <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
            <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
        </div>

		{{ Form::close() }}
	</div>