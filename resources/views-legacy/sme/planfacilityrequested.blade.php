	<div class="row">

        {{ Form::open( array('route' => array('postSMEPlanFacilityCreateRequested', $entity_id), 'role'=>'form', 'id' => 'rcl-form')) }}
        
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
                <div class="col-md-12"><h3>{{trans('rcl.requested_cf_title')}} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.rcl_hint')}}">More info...</a></h3></div>
                <div class="col-md-4{{ ($errors->has('plan_credit_availment')) ? ' has-error' : '' }}">
                    {{ Form::label('plan_credit_availment', trans('rcl.amount_of_line')) }} *
                    {{ Form::text('plan_credit_availment', Input::old('plan_credit_availment'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-4{{ ($errors->has('purpose_credit_facility')) ? ' has-error' : '' }}">
                    {{ Form::label('purpose_credit_facility', trans('rcl.purpose_of_cf')) }} *
                    {{ Form::select('purpose_credit_facility', array(
                    '' => trans('messages.choose_here'),
                    '1' => trans('rcl.working_capital'),
                    '2' => trans('rcl.expansion'),
                    '3' => trans('rcl.takeout'),
                    '5' => trans('rcl.supplier_credit'),
                    '6' => trans('rcl.equipment_purchase'),
                    '7' => trans('rcl.guarantee'),
                    '4' => trans('rcl.other_specify')
                    ), Input::old('purpose_credit_facility'), array('id' => 'purpose_credit_facility', 'class' => 'form-control')) }}
                </div>
                <div class="col-md-12" style = 'display: none;'>
                    {{ Form::label('purpose_credit_facility_others', trans('rcl.purpose_of_cf_others')) }}
                    {{ Form::text('purpose_credit_facility_others', Input::old('purpose_credit_facility_others'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('credit_terms')) ? ' has-error' : '' }}" style = 'display: none;'>
                    {{ Form::label('credit_terms', trans('rcl.credit_terms')) }} *
                    {{ Form::text('credit_terms', Input::old('credit_terms'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-12{{ ($errors->has('other_remarks')) ? ' has-error' : '' }}">
                    {{ Form::label('other_remarks', trans('rcl.other_remarks')) }}
                    {{ Form::text('other_remarks', Input::old('other_remarks'), array('class' => 'form-control')) }}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12"><h3>{{trans('rcl.offered_collateral')}} <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.offered_collateral_hint')}}">More info...</a></h3></div>
                <div class="col-md-6{{ ($errors->has('cash_deposit_details')) ? ' has-error' : '' }}">
                    {{ Form::label('cash_deposit_details', trans('rcl.type_of_deposit')) }}
                    {{ Form::text('cash_deposit_details', Input::old('cash_deposit_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('cash_deposit_amount')) ? ' has-error' : '' }}">
                    {{ Form::label('cash_deposit_amount', trans('rcl.deposit_amount')) }}
                    {{ Form::text('cash_deposit_amount', Input::old('cash_deposit_amount'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('securities_details')) ? ' has-error' : '' }}">
                    {{ Form::label('securities_details', trans('rcl.marketable_securities')) }}
                    {{ Form::text('securities_details', Input::old('securities_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('securities_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('securities_estimated_value', trans('rcl.marketable_securities_value')) }}
                    {{ Form::text('securities_estimated_value', Input::old('securities_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('property_details')) ? ' has-error' : '' }}">
                    {{ Form::label('property_details', trans('rcl.property')) }}
                    {{ Form::text('property_details', Input::old('property_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('property_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('property_estimated_value', trans('rcl.property_value')) }}
                    {{ Form::text('property_estimated_value', Input::old('property_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('chattel_details')) ? ' has-error' : '' }}">
                    {{ Form::label('chattel_details', trans('rcl.chattel')) }}  <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.chattel_hint')}}">More info...</a>
                    {{ Form::text('chattel_details', Input::old('chattel_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('chattel_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('chattel_estimated_value', trans('rcl.chattel_value')) }}
                    {{ Form::text('chattel_estimated_value', Input::old('chattel_estimated_value'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('others_details')) ? ' has-error' : '' }}">
                    {{ Form::label('others_details', trans('rcl.others')) }}
                    {{ Form::text('others_details', Input::old('others_details'), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-6{{ ($errors->has('others_estimated_value')) ? ' has-error' : '' }}">
                    {{ Form::label('others_estimated_value', trans('rcl.others_value')) }}
                    {{ Form::text('others_estimated_value', Input::old('others_estimated_value'), array('class' => 'form-control')) }}
                </div>
            </div>	

            {{ Form::token() }}

            <div style="clear:both"></div>

            <div class="col-md-12">
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close </button>
	                <button type="button" class="btn btn-large btn-primary" id="save-rcl"> Save </button>
	            </div>
            </div>
		{{ Form::close() }}
	</div>

<script type="text/javascript">
    $('#purpose_credit_facility').change(function() {
        
        $('#purpose_credit_facility_others').parent().hide();
        $('#credit_terms').parent().hide();
        
        if (4 == $(this).val()) {
            $('#purpose_credit_facility_others').parent().show();
        }
        else if (5 == $(this).val()) {
            $('#credit_terms').parent().show();
        }
        else {
            
        }
    });

    $('#save-rcl').click(function(){
    	formData = $('#rcl-form').serialize();
    	$.ajax({
    		url: "{{ url('sme/planfacilityrequested/'.$entity_id) }}",
    		type: "POST",
    		dataType: 'json',
    		data: formData,
    		success: function(data){
    			console.log(data);
    			if(data.sts == 0){
    				var msg = data.messages;
    				$('.pop-error-msg').html(msg[0]);
    				$('.pop-error-msg').show();
    				return false;
    			}else{
    				window.location = window.location;
    			}
    		}
    	});
    });
    
</script>