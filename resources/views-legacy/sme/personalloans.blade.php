
	<div class="row">
        {{ Form::open( array('route' => ['postPersonalLoans', $entity_id, $owner_id], 'role'=>'form', 'id' => 'personal-loan-form')) }}
            <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
            
            <div class="form-group">
            @for($i=0; $i < 3; $i++)
                <div class="col-md-2 {{ ($errors->has('purpose.'.$i)) ? 'has-error' : '' }}">
                    {{ Form::label('purpose', 'Purpose') }}
                    @if($i==0 && $auto==0)
                        {{ Form::text('purpose['.$i.']', 'Auto', array('class' => 'form-control', 'readonly'=>'readonly')) }}
                    @elseif($i==1 && $housing==0)
                        {{ Form::text('purpose['.$i.']', 'Housing', array('class' => 'form-control', 'readonly'=>'readonly')) }}
                    @else
                        {{ Form::select('purpose['.$i.']', array(
                            '' => trans("messages.choose_here"),
                            'Home Renovation / Upgrades'=>'Home Renovation / Upgrades',
                            'Tuition / Education'=>'Tuition / Education',
                            'Furniture'=>'Furniture',
                            'Appliances / Electronic Gadgets'=>'Appliances / Electronic Gadgets',
                            'Vacation / Travel'=>'Vacation / Travel',
                            'Balance Transfer / Debt Consolidation'=>'Balance Transfer / Debt Consolidation',
                            'Special Events'=>'Special Events',
                            'Health and Wellness'=>'Health and Wellness',
                            'Medical Emergencies'=>'Medical Emergencies'
                        ), '', array('class' => 'form-control')) }}
                    @endif
                </div>
                <div class="col-md-3 {{ ($errors->has('balance.'.$i)) ? 'has-error' : '' }}">
                    {{ Form::label('balance', 'Outstanding Balance') }}
                    {{ Form::text('balance['.$i.']', Input::old('balance.'.$i) , array('class' => 'form-control')) }}
                </div>
                <div class="col-md-3 {{ ($errors->has('monthly_amortization.'.$i)) ? 'has-error' : '' }}">
                    {{ Form::label('monthly_amortization', 'Monthly Amortization') }}
                    {{ Form::text('monthly_amortization['.$i.']', Input::old('monthly_amortization.'.$i), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-2 {{ ($errors->has('creditor.'.$i)) ? 'has-error' : '' }}">
                    {{ Form::label('creditor', 'Creditor') }}
                    {{ Form::text('creditor['.$i.']', Input::old('creditor.'.$i), array('class' => 'form-control')) }}
                </div>
                <div class="col-md-2 {{ ($errors->has('due_date.'.$i)) ? 'has-error' : '' }}">
                    {{ Form::label('due_date', 'Due Date') }}
                    {{ Form::text('due_date['.$i.']', Input::old('due_date.'.$i), array('class' => 'form-control datepicker')) }}
                </div>
                
                <div class = 'col-md-12'> <hr> </div>
                
                @if (($auto==1) && ($housing==1))
                    <?php break; ?>
                @endif
            @endfor
            </div>
            
            {{ Form::token() }}	
        
		{{ Form::close() }}
	</div>
    
    <script>
        $(document).ready(function(){
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                onChangeMonthYear: function(y, m, i) {
                        var d = i.selectedDay;
                        $(this).datepicker('setDate', new Date(y, m - 1, d));
                    }
            });
        });
    </script>