
	<div class="row">
        {{ Form::open( array('route' => array('postFuturegrowth', $entity_id), 'role'=>'form', 'id' => 'future-growth-expand-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>

            <div class="form-group">
                @for($i = 0; $i < 1; $i++)
                <div class="col-md-5{{ ($errors->has('futuregrowth_name.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('futuregrowth_name'.$i.'', trans('business_details2.fgi_purpose')) }}
                {{ Form::text('futuregrowth_name['.$i.']', Input::old('futuregrowth_name.'.$i.''), array('class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('futuregrowth_estimated_cost.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('futuregrowth_estimated_cost'.$i.'', trans('business_details2.fgi_cost')) }}
                {{ Form::text('futuregrowth_estimated_cost['.$i.']', Input::old('futuregrowth_estimated_cost.'.$i.''), array('class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('futuregrowth_implementation_date.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('futuregrowth_implementation_date'.$i.'', trans('business_details2.fgi_date')) }}
                {{ Form::text('futuregrowth_implementation_date['.$i.']', Input::old('futuregrowth_implementation_date.'.$i.''), array('class' => 'form-control withdate')) }}
                </div>
                <div class="col-md-5{{ ($errors->has('futuregrowth_source_capital.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::label('futuregrowth_source_capital'.$i.'', trans('business_details2.fgi_source')) }}
                {{ Form::select('futuregrowth_source_capital['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        'fgi_source_organic' => trans('business_details2.fgi_source_organic'), 
                        'fgi_source_family' => trans('business_details2.fgi_source_family'), 
                        'fgi_source_partners' => trans('business_details2.fgi_source_partners'), 
                        'fgi_source_venture' => trans('business_details2.fgi_source_venture'), 
                        'fgi_source_ipo' => trans('business_details2.fgi_source_ipo'),
                        'fgi_source_bank' => trans('business_details2.fgi_source_bank')
                    ), Input::old('futuregrowth_source_capital.'.$i.''), array('id' => 'futuregrowth_source_capital'.$i.'', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('planned-proj-goal'.$i.'', trans('business_details2.fgi_proj_goal')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('planned-proj-goal.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::select('planned-proj-goal['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        'fgi_increase_sales' => trans('business_details2.fgi_increase_sales'), 
                        'fgi_increase_profit' => trans('business_details2.fgi_increase_profit'),
                    ), Input::old('planned-proj-goal.'.$i.''), array('id' => 'planned-proj-goal'.$i.'', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class = 'col-md-2'>
                    {{ Form::label('planned-goal-increase'.$i.'', trans('business_details2.fgi_goal_increase')) }}
                </div>
                <div class="col-md-2{{ ($errors->has('planned-goal-increase.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::select('planned-goal-increase['.$i.']', 
                    array('' => trans('messages.choose_here'), 
                        '0-5' => '0 - 5 %', 
                        '6-10' => '6 - 10 %',
                        '10-15' => '10 - 15 %',
                        '15-20' => '15 - 20 %',
                        'Over 20' => 'Over 20 %',
                    ), Input::old('planned-goal-increase.'.$i.''), array('id' => 'planned-goal-increase'.$i.'', 'class' => 'form-control validate[required]')) }}
                </div>
                <div class = 'col-md-3'>
                    {{ Form::label('proj-benefit-date'.$i.'', trans('business_details2.fgi_benefit_date')) }}
                </div>
                <div class="col-md-3{{ ($errors->has('proj-benefit-date.'.$i.'')) ? ' has-error' : '' }}">
                {{ Form::text('proj-benefit-date['.$i.']', Input::old('proj-benefit-date.'.$i.''), array('class' => 'form-control datepicker-mon-yir validate[required]')) }}
                <a href="#" class="popup-hint popup-hint-content" data-hint="{{Lang::get('business_details2.beginning_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.beginning_hint', array(), 'fil')}}</i>">More info...</a>
                </div>
                
                <div class = 'col-md-12'> <hr> </div>
            </div>	
            @endfor
            
            <div class="spacewrapper">
                {{ Form::token() }}
                <a href = '#' class = 'btn btn-large btn-default close-expanders'> {{ trans('messages.close') }} </a>
                <input type="submit" class="btn btn-large btn-primary submit-expander-form" value="{{ trans('messages.save') }}">
            </div>

		{{ Form::close() }}
	</div>
    
    <style id = 'dp-style'></style>
    
    <script>
        $(document).ready(function() {
            $(".withdate").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-30:+10',
            onChangeMonthYear: function(y, m, i) {
                    var d = i.selectedDay;
                    $(this).datepicker('setDate', new Date(y, m - 1, d));
                }
            });
            
            $('.datepicker-mon-yir').datepicker( {
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy-mm',
                beforeShow: function(dateText, inst) {
                    $('#dp-style').html('<style> .ui-datepicker-calendar { display: none; } </style>');
                },
                onClose: function(dateText, inst) { 
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month));
                    $('#dp-style').html('');
                },
                onChangeMonthYear: function(y, m, i) {
                        var d = i.selectedDay;
                        $(this).datepicker('setDate', new Date(y, m - 1, d));
                    }
            });
        });
    </script>