
	<div class="row">
        {{ Form::open( array('route' => array('postManageBusiness', $entity_id, $owner_id, $type), 'role'=>'form', 'id' => 'biz-mng-form')) }}
        <div class = 'col-md-12'> <div class="alert alert-danger pop-error-msg" role="alert" style = 'display: none;'> </div> </div>
        
        <div class="form-group">
            @for($i = 0; $i < 1; $i++)
            <div class="col-md-4{{ ($errors->has('bus_name.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('bus_name'.$i.'', 'Name') }}
            {{ Form::text('bus_name['.$i.']', Input::old('bus_name.'.$i.''), array('class' => 'form-control')) }}
            </div>												
            <div class="col-md-4{{ ($errors->has('bus_type.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('bus_type'.$i.'', trans('owner_details.business_type')) }}
            {{ Form::text('bus_type['.$i.']', Input::old('bus_type.'.$i.''), array('class' => 'form-control')) }}
            </div>
            <div class="col-md-4{{ ($errors->has('bus_location.'.$i.'')) ? ' has-error' : '' }}">
            {{ Form::label('bus_location'.$i.'', trans('owner_details.business_location')) }}
            {{ Form::select('bus_location['.$i.']', 
                array('' => trans('messages.choose_here')) + $cityList, Input::old('bus_location.'.$i.''), array('id' => 'bus_location'.$i.'', 'class' => 'form-control')) }}
            </div>
            @endfor
        </div>

        {{ Form::token() }}

		{{ Form::close() }}
	</div>