@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>Excel to ReadyRatios (vfa) file</h2>
				<hr/>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route' => array('postConverterPublic', $hash), 'files' => true)) }}
					<div class="form-group">
						{{ Form::label('file', 'Select the Excel template that you want to convert to vfa') }}
						{{ Form::file('file') }}
					</div>
					{{ Form::submit('Convert', array('class'=>'btn btn-primary')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
