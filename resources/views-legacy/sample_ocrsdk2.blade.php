<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>SEC Certificate OCR Check</title>
<style>
	table { border-collapse: collapse; }
	table td, table th { border: 1px solid #CCC; padding: 3px; }
</style>
</head>
<body>
	<div>
		<h3>SEC Certificate OCR Check</h3>
		{{Form::open(['route'=>'postOCRSDK2', 'method'=>'post', 'files'=>true])}}
			<label>File: </label><input type="file" name="imagefile" /><br/><br/>
			<input type="submit" value="OCR convert" />
		{{Form::close()}}
	</div>
	<div>
		<h2>RESULT</h2>
		<textarea rows="20" style="width: 100%;" wrap="soft">{{ $response }}</textarea>
	</div>
	<pre>
		<?php print_r($parsed); ?>
	</pre>
</body>
</html>