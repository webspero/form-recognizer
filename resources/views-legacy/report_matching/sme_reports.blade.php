@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO - Reports List</title>
@stop
@section('style')
<style>
    @media print{
   .noprint{
       display:none;
   }
}
</style>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<h3>Company Matching</h3>
	<div class="row">
		
		<div class="col-sm-4" style="border-right: 1px solid #e8e8e8;">
			{{Form::open(array('route'=>'getSMEReports', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
			<div class="form-group form_row">
				{{Form::label('email', 'Email', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{ Form::text('email',$email, array('class'=>'form-control'))}}
				</div>
			</div>

			<div class="form-group form_row">
				{{Form::label('sme', 'Organization', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					<select id="select-sme" class="form-control" name="sme[]" multiple="multiple">
						@foreach($entity as $sme)
							@if(in_array($sme->entityid, $smeSelected))
								<option value="{{ $sme->entityid }}" selected="selected">{{$sme->companyname}}</option>
							@else
								<option value="{{ $sme->entityid }}">{{$sme->companyname}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="form-group form_row">
				{{Form::label('bcc', 'Rating', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('bcc_from', array(
							'' => ' - ',
							'AAA' => 'AAA',
							'AA' => 'AA',
							'A' => 'A',
							'BBB' => 'BBB',
							'BB' => 'BB',
							'B' => 'B',
							'CCC' => 'CCC',
							'CC' => 'CC',
							'C' => 'C',
							'D' => 'D',
							'E' => 'E',
						), $bccFrom, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('bcc_to', array(
							'' => ' - ',
							'AAA' => 'AAA',
							'AA' => 'AA',
							'A' => 'A',
							'BBB' => 'BBB',
							'BB' => 'BB',
							'B' => 'B',
							'CCC' => 'CCC',
							'CC' => 'CC',
							'C' => 'C',
							'D' => 'D',
							'E' => 'E',
						), $bccTo, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>

			<!-- <div class="form-group form_row">
				{{Form::label('region', 'Region', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('region',[''=>'All'] + $provinces, $region, array('class'=>'form-control'))}}
				</div>
			</div> -->

			<div class="form-group form_row">
				{{Form::label('industry_section', 'Industry Section', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry_section',[''=>'All'] + $industries, $industry_section, array('class'=>'form-control','id' => 'industry_section'))}}

					<input type="hidden" name="s_industry_section" id="s_industry_section" value="{{ $industry_section }}">
				</div>
			</div>

			<div class="form-group form_row">
				{{Form::label('industry_division', 'Industry Division', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry_division',[''=>'All'], $industry_division, array('class'=>'form-control','id' => 'industry_division'))}}

					<input type="hidden" name="s_industry_division" id="s_industry_division" value="{{ $industry_division }}">
				</div>
			</div>

			<div class="form-group form_row">
				{{Form::label('industry_group', 'Industry Group', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry_group',[''=>'All'], $industry_group, array('class'=>'form-control','id' => 'industry_group'))}}

					<input type="hidden" name="s_industry_group" value="{{ $industry_group }}">
				</div>
			</div>

			<div class="form-group form_row">
				{{Form::label('earnings_manipulator', 'Earnings Manipulator', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('earnings_manipulator',[
						''=>'All',
						'Likely'=>'Likely',
						'Unlikely'=>'Unlikely'
					],$earningsManipulator, array('class'=>'form-control'))}}
				</div>
			</div>

			<div class="form-group form_row">
				{{Form::label('preliminary_probability', 'Preliminary Probability', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('preliminary_probability', array(
							'' => ' - ',
							'1% - Very Low Risk' => '1% - Very Low Risk',
							'2% - Very Low Risk' => '2% - Very Low Risk',
							'3% - Low Risk' => '3% - Low Risk',
							'4% - Moderate Risk' => '4% - Moderate Risk',
							'6% - Moderate Risk' => '6% - Moderate Risk',
							'8% - Moderate Risk' => '8% - Moderate Risk',
							'15% - High Risk' => '15% - High Risk',
							'20% - High Risk' => '20% - High Risk',
							'25% - Very High Risk' => '25% - Very High Risk',
							'32% - Very High Risk' => '32% - Very High Risk'
						), $preliminary_probability, array('class'=>'form-control'))}}
						
					</div>
				</div>
			</div>

			<div class="form-group">
				<button class="update_table_list btn btn-primary">Update Table List</button>
			</div>

			{{ Form::close() }}
		</div>

	<div class="col-xs-8">
        <table id="entitydata" class="table table-striped table-bordered" style="table-layout: fixed; width:100%; overflow: hidden;">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Basic User Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Revenue Growth</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Years in Business</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Province</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Latest CreditBPO Rating</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Rating Date</th>
               
          @if(Auth::user()->role != 1)
            <th style="display:none; background-color: #055688;color: #fff; font-weight: bold;">Beneish M-Score</th>
            <th style="background-color: #055688;color: #fff; font-weight: bold;">Earnings Manipulator</th>
          @endif
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Preliminary Probability of Default</th>
					
                   

				</tr>
			</thead>
			<tbody>
            @foreach ($entity as $info)

            	@php
            		if($info->hideRating == 1) continue;
            		if($info->earnings_manipulator_hidden == 1) continue;

            		if(!empty($preliminary_probability)){
            			if($info->pdefault != $preliminary_probability) continue;
					}
            	@endphp
				<tr class="odd" role="row">
                    
					<td style="overflow: hidden; word-break: break-word;">
                        <a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}" target="_blank">
                            @if (1 == $info->anonymous_report)
                                Company - {{ $info->entityid }}
                            @else
                                {{ $info->entityid }} - {{ $info->companyname }}
                            @endif
                        </a>
                    </td>
					<td style="overflow: hidden; word-break: break-word;">{{ $info->basic_email }}</td>
					<td>
                        @if (!empty($info->forecast_data["gf_icon"]))
                        <img src = '{{ $info->forecast_data["gf_icon"] }}' style = 'height: 50px; width: 50px;'/>
                        @endif
                    </td>
					<td>{{ $info->number_year }}</td>
					<td>{{ $info->province }}</td>

					<td><span style="color: {{ $info->cbpo_score_color }}; font-weight: bold;">{{$info->val == "In Progress" ? '' : $info->score_sf}}</span></td>
					
					<td>
                        @if($info->val== "In Progress")
                        @else
                            {{ ($info->completed_date!=null) ? date('m/d/Y', strtotime($info->completed_date)) : ""}}
                        @endif
                    </td>
                    
                    @if(Auth::user()->role != 1)
                        <td style="display:none">{{$info->val == "In Progress" ? '' :  number_format($info->m_score, 2)}}</td>
                        <td>
                            {{ $info->earnings_manipulator }}
                        </td>
                    @endif    
					<td>{{ $info->main_title }}</td>
					<td><span style="color: red; font-weight: bold;">{{$info->val == "In Progress" ? '' :  $info->pdefault}}</span></td>
                   
				</tr>
			@endforeach
			</tbody>
        </table>

	</div>
</div>

<!-- spinner -->
<!-- 
<div class="modal" role="dialog" id="spinnerModal" hidden style="top: 50% !important; position:absolute; left: 30% !important; transform: traslate(-50%,-50%) !important; margin: 0px !important;">
    <div class="modal-dialog spinnerClass" role="document" >
        <div class="modal-content">
        <img src="{{URL::asset('images/email.gif')}}" width="80px" height="35px">
        </div>
    </div>
</div> -->



@include('user-2fa-form')

@stop
@section('javascript')
<script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script type="text/javascript" scr="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">

	$('#select-sme').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#industry_section').change(function(){
		let industrySection = $(this).val();

		$.ajax({
			url: "{{ url('signup/getIndustrySub') }}/" + industrySection,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';

				$.each(data, function(key,value){
					options += '<option value="'+value.sub_code+'">'+value.sub_title+'</option>';
				});

				$('#industry_division').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	});

	$('#industry_division').change(function(){
		let industryDivision = $(this).val();
		if(industryDivision == ''){
			industryDivision = false;
		}

		$.ajax({
			url: "{{ url('signup/getIndustryRow') }}/" + industryDivision,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';

				$.each(data, function(key,value){
					options += '<option value="'+value.group_code+'">'+value.group_description+'</option>';
				});

				$('#industry_group').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	});

	getIndustryDivision();

	function getIndustryDivision(){
		let industrySection = $('#s_industry_section').val();
		if(industrySection == ''){
			industrySection = false;
		}

		$.ajax({
			url: "{{ url('signup/getIndustrySub') }}/" + industrySection,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';
				let s_industry_division = $('#s_industry_division').val();

				$.each(data, function(key,value){
					options += '<option value="'+value.sub_code+'"';

					if(s_industry_division != '')
						options += ' selected ';

					options += '>'+value.sub_title+'</option>';
				});

				$('#industry_division').find('option')
				    .remove()
				    .end().append(options);
			}
		});

	}

	getIndustryGroup();

	function getIndustryGroup(){
		let industryDivision = $('#s_industry_division').val();
		if(industryDivision == ''){
			industryDivision = false;
		}

		$.ajax({
			url: "{{ url('signup/getIndustryRow') }}/" + industryDivision,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';
				let s_industry_group = $('#s_industry_group').val();

				$.each(data, function(key,value){
					options += '<option value="'+value.group_code+'"';
					if(s_industry_group != ''){
						options += ' selected ';	
					}
					options += '>'+value.group_description+'</option>';
				});

				$('#industry_group').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	}

</script>

<script>

$(document).ready(function(){
    $('#entitydata').DataTable({
        columnDefs: [{
            orderable: false
        }],
        order: [[ 0, 'asc' ]]
    });

});
</script>

<script type="text/javascript">
	$(document).ready(function(){
       
		$('.datepicker').datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
		});
		
	});
</script>
<style>
    .control-set {
        text-align: right;
    }

    #entitydata {
        margin-bottom: 10px;
    }

</style>

<link rel="stylesheet" href="{{ URL::asset('css/supervisor-overview-print.css') }}" media="print"></link>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
@stop