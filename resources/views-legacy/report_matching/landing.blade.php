@extends('layouts.master', array('hide' => true))

@section('header')
	@parent
	<title>CreditBPO SME-Bank Vertical Matching</title>
@stop

@section('content')

	<div class="container">
		<h1>CreditBPO Vertical Matching</h1>
		
		<div class="col-md-12">
			<p>Discover Ways to Grow your Business</p>
			<ol>
			<li>Get an indicator of how a bank may possibly view your creditworthiness before you send them a loan application.*</li>
			<li>Gain actionable insights on how to improve your creditworthiness and grow your business.</li>
			<li>Benchmark your financial performance against other SMEs in your industry.</li>
			<li>Have the opportunity to get matched as a business borrower to a bank or other financial institution or as a supplier to a conglomerate.</li>

			<p>It all starts with the click of a button ...</p>
			<p><a href="{{ url('signup') }}" class="btn btn-primary">Sign up</a>
		</div>
		
		
	</div>
@stop
@section('javascript')
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@stop
@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>.toggle{ width:100% !important; }</style>
@stop
