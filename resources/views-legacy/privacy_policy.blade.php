@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
                <h2 class="text-center text-uppercase">Privacy Policy</h2>
                <p class="text-justify">CreditBPO respects the privacy of its online visitors and users of its products and services and
                        complies with Republic Act No. 10173 also known as the Data Privacy Act of 2012, its
                        implementing rules and regulations, and other issuances of the National Privacy Commission.
                        </p>
                <p>CreditBPO recognizes the importance of protecting information collected from users and has
                        adopted this privacy policy to inform users about how it collects, uses, stores,
                        transfers/discloses and disposes of information derived from their use of CreditBPO services,
                        sites, and online portals. Please note that this privacy policy may be amended from time to
                        time to reflect changes and additions.</p>
                <p>“Personally identifiable information” is information that can be used to uniquely identify a user
                        such as full name, password, age, civil status, gender, mailing address, email address,
                        telephone number, mobile number, and other personal details. For clarity, we also consider
                        Personal Data as defined under the Data Privacy Act of 2012 as personally identifiable
                        information.</p>
                <div class="terms-section text-justify">
                    <ol class="ol-no-padding">
                        <li>
                            <strong class="text-uppercase"><u>INFORMATION WE COLLECT:</u></strong>
                            <ol>
                                <p>You provide us information which may or may not be
                                        about yourself – Full Name, E-mail Address, Business Name, Business Owners Name, Job
                                        Title, Company Name, Company website, Business Entity Type, Client Key, Discount Code,
                                        etc. If you correspond with us by email, we may retain the content of your email messages,
                                        your email address, and our responses. Additionally, we store information about users’
                                        contacts when users manually enter contact e-mail addresses or transfer contact
                                        information from other online social networks. We also collect general information about
                                        your use of our services.
                                        </p>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>INFORMATION WE COLLECT AUTOMATICALLY WHEN YOU USE OUR SERVICES:</u></strong>
                            <ol>
                                <li>
                                    When you access or use our Services, we automatically collect information about you, including:
                                    <ol>
                                        <li>
                                            <p>Usage Information: If you choose to post messages on our message boards,
                                                online/ offline chats or other message areas or leave feedback for other
                                                users, we retain this information as necessary to resolve disputes, provide
                                                customer support and troubleshoot problems as permitted by law. If you
                                                send us personal correspondence, such as emails or letters, or if other users
                                                or third parties send us correspondence about your activities on the
                                                Website, we may collect such information into a file specific to you.
                                                </p> 
                                        </li>
                                        <li>
                                            <p>Log Information: We log information about your use of our Website,
                                                including your browser type and language, access times, pages viewed,
                                                your IP address and the Website you viewed before navigating to our
                                                Website.
                                                </p>
                                        </li>
                                        <li>
                                            <p>Device Information: We may collect information about the device you use to
                                                access our Services, including the hardware model, operating system and
                                                version, unique device identifier, phone number, International Mobile
                                                Equipment Identity ("IMEI") and mobile network information.
                                                </p>
                                        </li>
                                        <li>
                                            <p>Location Information: With your consent, we may collect information about
                                                the location of your device to facilitate your use of certain features of our
                                                Services, determine the speed at which your device is traveling, add
                                                location-based filters (such as local weather), and for any other purposes.
                                                </p>
                                        </li>
                                        <li>
                                            <p>Information Collected by Cookies and Other Tracking Technologies: We use
                                                various technologies to collect information, and this may include sending
                                                cookies to you. A "cookie" is a small data file transferred to your computer’s
                                                hard drive that allows a Website to respond to you as an individual,
                                                gathering and remembering information about your preferences in order to
                                                tailor its operation to your needs, likes and dislikes. Overall, cookies are safe,
                                                as they only identify your computer to customize your Web experience.
                                                Accepting a cookie does not provide us access to your computer or any
                                                Personally Identifiable Information about you, other than the information you
                                                choose to share. Other servers cannot read them, nor can they be used to
                                                deliver a virus. Most browsers automatically accept cookies, but you can
                                                usually adjust yours (Microsoft Internet Explorer, Firefox or Google Chrome)
                                                to notify you of cookie placement requests, refuse certain cookies, or decline
                                                cookies completely. If you turn off cookies completely, there may be some
                                                Website features that will not be available to you, and some Web pages may
                                                not display properly. To support the personalized features of our Website
                                                (such as your country and language codes and browsing functions) we must
                                                send a cookie to your computer’s hard drive and/or use cookie-based
                                                authentication to identify you as a registered Website user. We do not,
                                                however, use so-called "surveillance" cookies that track your activity
                                                elsewhere on the Web. We may also collect information using web beacons
                                                (also known as "tracking pixels").
                                                </p>
                                        </li>
                                        <li>
                                                “Web beacons” or clear .gifs are small pieces of code placed on a Web page
                                                to monitor user behavior and collect data about the visitors viewing a Web
                                                page. For example, Web beacons or similar technology can be used to
                                                count the users who visit a Website or to deliver a cookie to the browser of a
                                                visitor viewing that page. We may use Web beacons or similar technology on
                                                our Services from time to time for this and other purposes.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>HOW WE USE YOUR INFORMATION:</u></strong>
                            <ol>
                                <li>We use the personal information we collect to fulfill your requests for services, improve our services, contact you, conduct research, and provide anonymous reporting for internal and external clients.</li>
                                <li>By providing us your e-mail address, you consent to us using the e-mail address to
                                        send you our Website and services related notices, including any notices required by
                                        law, in lieu of communication by postal mail. You also agree that we may send
                                        notifications of activity on our Website to the e-mail address you give us, in
                                        accordance with any applicable privacy settings. We may use your e-mail address to
                                        send you other messages, such as newsletters, changes to our features, new
                                        services, events or other information. If you do not want to receive optional e-mail
                                        messages, you may modify your settings to opt-out.
                                        </li>
                                <li>Our settings may also allow you to adjust your communications preferences. If you do
                                        not wish to receive promotional email messages from us, you may opt-out by
                                        following the unsubscribe instructions in those emails. If you opt-out, you will still
                                        receive non-promotional emails from us about our Services.</li>
                                <li>Following termination or deactivation of your services, we may (but are under no obligation to) retain your information for archival purposes. We will not publicly disclose any of your personally identifiable information other than as described in this Privacy Policy.</li>
                                <li>At our sole discretion, for any reason or no reason at all, we reserve the right to remove any content or messages, if we believe that such action is necessary (a) to conform to the law, comply with legal process served on us or our affiliates, or investigate, prevent, or take action regarding suspected or actual illegal activities; to enforce this policy, to take precautions against liability, to investigate and defend ourselves against any third-party claims or allegations, to assist government enforcement agencies, or to protect the security or integrity of our Website; or (c) to exercise or protect the rights, property, or personal safety of the Website, our users, or others.</li>
                                <li>
                                    
                                    <p><strong>Business Transfers: </strong>As our businesses continue to evolve, we might sell one or more
                                            of our companies, subsidiaries or business units. In such transactions, personally
                                            identifiable information generally is one of the transferred business assets. In such an
                                            event, this Privacy Policy may be amended as set forth below or the collection and
                                            uses of your personally identifiable information may be governed by a different
                                            privacy policy.
                                            </p>
                                </li>
                                <li>
                                    
                                    <p><strong>Protection of Our Services and Others: </strong>We reserve the right to release personally identifiable information to unaffiliated third parties when we believe its release is appropriate to comply with the law, enforce or apply our Terms and Conditions and other agreements, or protect the rights, property or safety of Trader Interactive, our users or others. This includes exchanging information with other unaffiliated third parties in connection with fraud protection and credit risk reduction.</p>
                                </li>
                                <li>
                                    <p><strong>With Your Consent: </strong>Other than as set out above, you will receive a notice and have
                                            the opportunity to withhold consent when personally identifiable information about
                                            you might be shared with unaffiliated third parties.</p>
                                </li>
                                <li>We may transmit the user data across the various Websites of the Company.</li>
                                <li>
                                    <p><strong>User Submissions: </strong>If you participate in any online forum/communities, chat sessions and/or blogs, or otherwise post in any user comment field visible to other users of our Services, any information that you submit or post will be visible to and may be read, collected or used by others who use our Services. We are not responsible for any information, including personally identifiable information, you choose to submit in any such user comment field.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>HOW WE SHARE YOUR INFORMATION:</u></strong>
                            <ol>
                                <li>As a matter of policy, we will not sell or rent information about you and we will not
                                        disclose information about you in a manner inconsistent with this Privacy Policy
                                        except as required by law or government regulation. We cooperate with law
                                        enforcement inquiries, as well as other third parties, to enforce laws such as those
                                        regarding intellectual property rights, fraud, and other personal rights.
                                        </li>
                                <li>We will not share the personal information we collect about you with any third party for their own marketing purposes without your consent. We may share your data with our services providers who process your personal information to provide services to us or on our behalf. We have contracts with our service providers that prohibit them from sharing the information about you that they collect or that we provide to them with anyone else, or using it for other purposes.</li>
                                <li>You may decline to submit Personally Identifiable Information through the Services, in
                                        which case we may not be able to provide certain services to you. If you do not agree
                                        with our Privacy Policy or Terms of Service, please discontinue the use of our Service;
                                        your continued usage of the Service will signify your assent to and acceptance of our
                                        Privacy Policy and Terms of Use.
                                        </li>
                                <li>WE CAN (AND YOU AUTHORIZE US TO) DISCLOSE ANY INFORMATION ABOUT YOU TO LAW ENFORCEMENT, OTHER GOVERNMENT OFFICIALS, ANY LAWSUIT OR ANY OTHER THIRD PARTY THAT WE, IN OUR SOLE DISCRETION, BELIEVE NECESSARY OR WEBSITEROPRIATE IN CONNECTION WITH AN INVESTIGATION OF FRAUD, INTELLECTUAL PROPERTY INFRINGEMENT, OR OTHER ACTIVITY THAT IS ILLEGAL OR MAY EXPOSE US, OR YOU, TO LIABILITY.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>ENSURING INFORMATION IS ACCURATE AND UP-TO-DATE</u></strong>
                            <ol>
                                <li>We take reasonable precautions to ensure that the Personal Information we Collect,
                                        Use and Disclose are complete, relevant and up-to-date. However, the accuracy of
                                        that information depends to a large extent on the information you provide. That's why
                                        we recommend that you:
                                    <ol>
                                        <li>Let us know if there are any errors in your Personal Information; and</li>
                                        <li>Keep us up-to-date with changes to your Personal Information such as your name or address.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>HOW WE PROTECT YOUR INFORMATION:</u></strong>
                            <ol>
                                <li>We are very concerned about safeguarding the confidentiality of your personally identifiable information. We employ administrative, physical and electronic measures designed to protect your information from unauthorized access.</li>
                                <li>By using this Website or the Services or providing Personal Information to us, you agree that we can communicate with you electronically regarding security, privacy, and administrative issues relating to your use of this Website or Services.</li>
                                <li>We use commercially reasonable physical, managerial, and technical safeguards to preserve the integrity and security of your Personal Information. Your sensitive information, such as credit card information, is encrypted when it is transmitted to us. Our employees are trained and required to safeguard your information. We cannot, however, ensure or warrant the security of any information you transmit to us and you do so at your own risk. Using unsecured wi-fi or other unprotected networks to submit messages through the Service is never recommended. Once we receive your transmission of information, we make commercially reasonable efforts to ensure the security of our systems. However, please note that this is not a guarantee that such information may not be accessed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards. If we learn of a security systems breach, then we may attempt to notify you electronically so that you can take appropriate protective steps.</li>
                                <li>Notwithstanding anything to the contrary in this Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation or legal request; to protect the safety of any person; to address fraud, security or technical issues; or to protect our rights or property. However, nothing in this Policy is intended to limit any legal defenses or objections that you may have to a third party, including a government’s, request to disclose your information.</li>
                                <li>However, no data transmission over the Internet or data storage system can be guaranteed to be 100% secure. Please do not send us credit card information and/or other sensitive information through email. If you have reason to believe that your interaction with us is not secure (for example, if you feel that the security of any account you might have with us has been compromised), you must immediately notify us of the problem by contacting us in accordance with the "Contacting Us" section available on our Website.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>YOUR CHOICES ABOUT YOUR INFORMATION:</u></strong>
                            <ol>
                                <li>
                                    <p>You have several choices regarding the use of information on our Services:</p>
                                    <ol>
                                        <li>Email Communications: We will periodically send you free newsletters and
                                                e-mails that directly promote the use of our Website or Services. When you
                                                receive newsletters or promotional communications from us, you may
                                                indicate a preference to stop receiving further communications from us and
                                                you will have the opportunity to “opt-out” by following the unsubscribe
                                                instructions provided in the e-mail you receive or by contacting us directly.
                                                Despite your indicated e-mail preferences, we may send you service-related
                                                communications, including notices of any updates to our Terms of Use or
                                                Privacy Policy.
                                                </li>
                                        <li>You may, of course, decline to submit personally identifiable information through the Website, in which case, we will not be able to provide you our service.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>STORING PERSONAL DATA</u></strong>
                            <p>We retain your information only for as long as is necessary for the purposes for which we process the information as set out in this policy. However, we may retain your Personal Data for a longer period of time where such retention is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>CROSS BORDER TRANSFER OF PERSONAL DATA</u></strong>
                            <p>Your Personal Data may be stored and processed in any country where we have facilities or in which we engage service providers. In certain circumstances, courts, law enforcement agencies, regulatory agencies, or security authorities in those other countries may be entitled to access your Personal Data.</p>
                            <p>If you are located in the European Economic Area (“EEA”): Some of the non-EEA countries are
                                    recognized by the European Commission as providing an adequate level of data protection
                                    according to EEA standards (the full list of these countries is available on the EU
                                    Commission’s adequacy list online at</p> 
                            <p><a href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/adequacy-protection-personal-data-non-eu-countries_en" target="_blank">https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/adequacy-protection-personal-data-non-eu-countries_en</a>).</p>
                            <p> For transfers from the EEA to countries not considered adequate by the European Commission, we have put in place adequate measures, such as standard contractual clauses adopted by the European Commission, to protect your Personal Data.
                            </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>CHILDREN’S PRIVACY:</u></strong>
                            <p>Our services are available only to persons who can form a legally binding contract with us as per the applicable laws. Protecting the privacy of young children is especially important. Thus, we do not knowingly collect or solicit personal information from anyone under the age of 18 or knowingly allow such persons to register. If you are under 18, please do not attempt to register for the Service or send any information about yourself to us, including your name, address, telephone number, or email address. No one under the age of 18 may provide any personal information for listing any service.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>MERGERS AND ACQUISITIONS:</u></strong>
                            <p>In case of a merger or acquisition, we reserve the right to transfer all the information, including personally identifiable information, stored with us to the new entity or company thus formed. Any change in the Website’s policies and standing will be notified to you through email.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>LINKS TO THIRD PARTY WEBSITE:</u></strong>
                            <p>Our website contains links to other websites. The fact that we link to a website is not an endorsement, authorization or representation of our affiliation with that third party. We do not exercise control over third party websites. These other websites may place their own cookies or other files on your computer, collect data or solicit personally identifiable information from you. Other websites follow different rules regarding the use or disclosure of the personally identifiable information you submit to them. We encourage you to read the privacy policies or statements of the other websites you visit.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>NOTIFICATION PROCEDURES:</u></strong>
                            <p>It is our policy to provide notifications, whether such notifications are required by law or are for
                                    marketing or other business-related purposes, to you via e-mail notice, written or hard copy
                                    notice, or through conspicuous posting of such notice on the Website, as determined by us
                                    in our sole discretion. We reserve the right to determine the form and means of providing
                                    notifications to you, provided that you may opt-out of certain means of notification as
                                    described in this Privacy Policy.
                                    </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>OPTING OUT OF INFORMATION SHARING:</u></strong>
                            <ol>
                                <li>We understand and respect that not all users may want to allow us to share their
                                        information with other select users or companies. If you do not want us to share your
                                        information, please contact us through the Contact Us page, and we will remove
                                        your name from lists we share with other users or companies as soon as reasonably
                                        practicable or you can also click on unsubscribe. When contacting us, please clearly
                                        state your request, including your name, mailing address, email address, and phone
                                        number. In case you do not want your name to be displayed on the Website, you
                                        may opt for the same by choosing a Username other than your original name, you
                                        may also hide details of Social Networking Accounts by opting for the same when
                                        you are creating an account with us.
                                        </li>
                                <li>
                                    However, under the following circumstances, we may still be required to share your personal information:
                                        <ol>
                                            <li>If we respond to court orders or legal process, or if we need to establish or exercise our legal rights or defend against legal claims.</li>
                                            <li>If we believe it is necessary to share information in order to investigate, prevent or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Terms of Use or as otherwise required by law.</li>
                                            <li>If we believe it is necessary to restrict or inhibit any user from using any of
                                                our websites, including, without limitation, by means of "hacking" or
                                                defacing any portion thereof.
                                                </li>
                                        </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            
                            <p><strong class="text-uppercase"><u>USER SUBMISSIONS</u></strong> You understand that when using the Platform, you will be exposed
                                to Content from a variety of sources and that we are not responsible for the accuracy,
                                usefulness, safety, or intellectual property rights of or relating to such Content and you
                                agree and assume all liability for your use. You further understand and acknowledge that
                                you may be exposed to Content that is inaccurate, offensive, indecent, or objectionable,
                                defamatory or libelous and you agree to waive, and hereby do waive, any legal or equitable
                                rights or remedies you have or may have against us with respect thereto. If you find any
                                content to be libelous, objectionable, defamatory, and indecent or infringing your
                                Intellectual Property Rights, you may contact us directly through the “Contact Us” page
                                and we shall take appropriate action to remove such content from the Website.
                                </p>
                        </li>
                        <li>
                            
                            <p><strong class="text-uppercase"><u>PHISHING OR FALSE EMAILS:</u></strong> If you receive an unsolicited email that appears to be from us or one of our members that requests personal information (such as your credit card, login, or password), or that asks you to verify or confirm your account or other personal information by clicking on a link, that email was likely to have been sent by someone trying to unlawfully obtain your information, sometimes referred to as a "phisher" or "spoofer." We do not ask for this type of information in an email. Do not provide the information or click on the link. Please contact us at notify.user@creditbpo.com if you get an email like this.</p>
                        </li>
                        <li>
                            
                            <p><strong class="text-uppercase"><u>CHANGES TO OUR PRIVACY POLICY:</u></strong> We may update this Privacy Policy and
                                information security procedures from time to time. If these privacy and/or information
                                security procedures materially change at any time in the future, we will post the new
                                changes conspicuously on the Website to notify you and provide you with the ability to
                                opt-out in accordance with the provisions set forth above. Continued use of our Website
                                and Service, following notice of such changes, shall indicate your acknowledgment of such
                                changes and agreement to be bound by the terms and conditions of such changes.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>COLLECTION OF INFORMATION BY THIRD-PARTY WEBSITES, SERVICES, AD SERVERS AND SPONSORS</u></strong>
                            <ol>
                                <li>Our Services may contain links to other Websites or services whose information
                                    practices may be different than ours. For example, while using one or more of our
                                    Websites, you may link to a third party’s Website via a window opened within (or on
                                    top of) our Website. Some of our Services may allow users to interface with
                                    third-party Websites or services, such as Facebook and Twitter. You will remain
                                    logged into those third-party Websites or services until you actively log off. By
                                    interfacing with those third-party Websites or services, you are allowing our Services
                                    to access your information that is or becomes available via such third party Websites
                                    or services, and you are agreeing to those third party’s applicable terms and
                                    conditions. Once you log onto any such third party Websites or services, the content
                                    you post there may also post to our Services. Our Privacy Policy and procedures
                                    may or may not be consistent with the policies and procedures of such third party
                                    Websites or services, and when you visit such Websites or services our Privacy
                                    Policy does not apply to personally identifiable information and other data collected
                                    by the third party. You should consult, read and understand the privacy notices of
                                    such third parties before choosing to provide personally identifiable information on
                                    any such Websites or services.
                                    </li>
                                <li>Our Services may also use a third-party ad server to present the advertisements that
                                    you may see on our Services. These third-party ad servers may use cookies, Web
                                    beacons, clear .gifs or similar technologies to help present such advertisements, and
                                    to help measure and research the advertisements’ effectiveness. The use of these
                                    technologies by these third party ad servers is subject to their own privacy policies
                                    and is not covered by our Privacy Policy.</li>
                            </ol>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>BREACH OF PRIVACY POLICY:</u></strong>
                            <p>We reserve the right to terminate or suspend your usage of this Website immediately if you are
                                found to be in violation of our privacy policy. We sincerely request you to respect the
                                privacy and secrecy concerns of others. The jurisdiction of any breach or dispute shall be
                                the Republic of the Philippines.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>NO RESERVATIONS:</u></strong>
                            <p>We do not accept any reservation or any type of limited acceptance of our privacy policy. You expressly agree to each and every term and condition as stipulated in this policyv without any exception whatsoever.</p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>NO CONFLICT:</u></strong>
                            <p>The policy constitutes a part of the user terms. We have taken the utmost care to avoid any
                                inconsistency or conflict of this policy with any other terms, agreements or guidelines
                                available on our Website. In case there exists a conflict, we request you to kindly contact
                                us for the final provision and interpretation.
                                </p>
                        </li>
                        <li>
                            <strong class="text-uppercase"><u>CONTACT US:</u></strong>
                            <p>If you have any questions about this Privacy Policy, our practices relating to the website, or your dealings with us, please contact the Privacy Team at <a href="mailto:info@creditbpo.com?Subject=Privacy%20Inquiry">info@creditbpo.com</a></p>
                        </li>
                    </ol>
                </div>
                <p align="center"><a class="btn btn-primary" onclick="window.close(); return false;">Close this window</a></p>
            </div>   
		</div>
    </div>

</div>

@stop
@section('javascript')
@stop
