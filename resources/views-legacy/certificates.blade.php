@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<div class="spacewrapper">
		<div class="read-only">
			<h3>Client Keys and Certificates</h3>
			<table class="table table-striped table-bordered" cellspacing="0">
				<tr>
					<th>Client Key</th>
					<th>Name on Certificate</th>
					<th>Claimed/Used</th>
					<th>Action</th>
				</tr>
				@foreach($keys as $k)
				<tr>
					<td>{{$k->serial_key}}</td>
					<td>{{$k->name}}</td>
					<td>{{$k->status}}</td>
					<td>
					@if($k->is_used == 1)
						&nbsp;
					@else
						<a class="download_certificate_btn" cert_id="{{$k->id}}" href="#">Download Certificate</a>
					@endif
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
<div class="modal" id="certificate-modal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Download Certificate</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" id="cert_id" />
				<label>Name on Certificate</label>
				<input type="text" id="name_on_cert" class="form-control"/>
			</div>
			<div class="modal-footer">
				<button type="button" class="cert_download_btn btn btn-primary">Download</button>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
	$('.download_certificate_btn').on('click', function(e){
		e.preventDefault();
		$('#cert_id').val($(this).attr('cert_id'));
		$('#name_on_cert').val('');
		$('#certificate-modal').modal();
		$('#certificate-modal').on('hide.bs.modal', function () {
			window.location.reload(true);
		});
	});


	$('.cert_download_btn').on('click', function(e){
		if($.trim($('#name_on_cert').val())!=''){
			$.ajax({
				url: BaseURL + '/update_certificate/' + '{{$code}}',
				type: 'post',
				data: {
					cert_id: $('#cert_id').val(),
					name: $('#name_on_cert').val()
				},
				beforeSend: function(){
					$('.cert_download_btn').text('Processing...').prop('disabled', true);
				},
				success: function(sUrl){
					if(sUrl!='error'){
						window.location = sUrl;
					} else {
						window.location.reload(true);
					}
				}
			});
		}
	});
});
</script>
@stop
