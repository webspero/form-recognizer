<div class="modal" id="modal-approval-process" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1> Status Update </h1>
            </div>
            
            <div class="modal-body row">
                                
                <div class = 'col-md-12'>
                    <p> <b>The following companies have been In Process for more than a week. An update maybe necessary </b></p>
                    <hr/>
                    <div id = 'companies-list'></div>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }} </button>
            </div>
            
        </div>
    </div>
</div>