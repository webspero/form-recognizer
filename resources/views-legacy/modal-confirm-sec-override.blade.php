    <div class="modal" id="modal-confirm-sec-override">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">
                        Confirm Change
                    </h4>
                </div>    
                <div class="modal-body">
					<p>This General Information Sheet has information that differs from edited fields. Would you like to override these fields with the data from this new file?</p>
                </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary confirm-yes">Yes</button>
					<button type="submit" class="btn btn-default" data-dismiss="modal">No</button>
				</div>
            </div>
        </div>
    </div>