    <div class="modal" id="trial-resend-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <b>Send Login Information</b>
                    </h4>
                </div>
                
                
                <div class="modal-body">
                    <div class = 'container-fluid'>
                        <div class = 'col-md-12'> <div class="alert alert-danger trial-send-error" role="alert" style = 'display: none;'> </div> </div>
                        
                        {{ Form::open(array('id' => 'send-trial-login')) }}
                        <div class = 'col-md-12'>
                            {{ Form::label('trial-email', 'Enter the Email Address where the Login Details will be sent') }}
                        </div>
                        
                        <div class = 'col-md-12'>
                            {{ Form::text('trial-email', '', array('id' => 'trial-email', 'class' => 'form-control')) }}
                        </div>
                        
                        <div class = 'col-md-12'>
                            Enter the Password of for <span id = 'password-lbl' style = 'font-weight: bold;'> </span> Account
                        </div>
                        
                        <div class = 'col-md-12'>
                            {{ Form::text('trial-password', '', array('id' => 'trial-password', 'class' => 'form-control')) }}
                        </div>
                        
                        {{ Form::close() }}
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
                    <input id = 'trial-login-send-btn' type="submit" class="btn btn-large btn-primary" data-ajax-url = '' value="SEND">
                </div>
                
            </div>
        </div>
    </div>