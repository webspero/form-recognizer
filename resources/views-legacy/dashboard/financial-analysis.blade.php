@extends('layouts.master')

@section('title', 'Dashboard')

@section('nav')

    @include('dashboard.nav')

@stop

@section('content')

  <div id="page-wrapper">
      <div class="row">
      <div class="col-lg-12">
        <div class="cont-box">



	<div class="row">
	<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Company ID</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Established</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Years in Business</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">City</th>
				<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($entity as $info)
			<tr class="odd" role="row">
				<td>{{ $info->entityid }}</td>
				<td><a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">{{ $info->companyname }}</a></td>
				<td>{{ $info->email }}</td>
				<td>{{ $info->date_established }}</td>
				<td>{{ $info->number_year }}</td>
				<td>{{ $info->row_title }}</td>
				<td>{{ $info->city }}</td>
				<td>
					@if ($info->status == 0)
						Active
					@elseif ($info->status == 1)
						Ready for investigation
					@elseif ($info->status == 2)
						Start investigation
					@elseif ($info->status == 3)
						On hold
					@elseif ($info->status == 4)
						Resume Investigator
					@elseif ($info->status == 5)
						Approved by investigator
					@elseif ($info->status == 6)
						Start Risk Scoring
					@elseif ($info->status == 7)
						Incomplete Documents by the Bank
					@elseif ($info->status == 8)
						Approved by the Bank
					@elseif ($info->status == 9)
						Deleted
					@else
						INVALID STATUS
					@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	</div>


        </div>
      </div>
      </div>
  </div>

@stop
