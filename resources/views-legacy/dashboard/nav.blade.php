@if(Auth::user()->role != 0)
	<div class="navbar-default sidebar"  role="navigation">
@else
	<div class="navbar-default sidebar" style="margin-top: 150px;"  role="navigation">
@endif
    <div class="sidebar-nav navbar-collapse collapse">
      <ul class="nav" id="side-menu">
        @if ($entity->status == 7)
            @if (Auth::user()->role != 3 && Auth::user()->role != 2 )
                @if(Auth::user()->role == 1 && $entity->is_premium == 1)
                @else
                    <li class="ressum">
                        <a data-toggle="tab" href="#ssc" class="nav-result pop-next-tab-msg" id="sscore" data-prompt-msg = 'sscore'>{{trans('messages.rating_summary')}}
                            <div class="ribbon-wrapper-green"><div class="ribbon-green">Result</div></div>
                        </a>
                    </li>
                    @if($entity->is_premium != 2)
                    <li class="finanal">
                        <a data-toggle="tab" href="#fianalysis" class="nav-result pop-next-tab-msg" id="finananalysis" data-prompt-msg = 'finananalysis'>{{trans('messages.financial_analysis')}}
                            <div class="ribbon-wrapper-green"><div class="ribbon-green">Result</div></div>
                        </a>
                    </li>
                    @endif
                  
                @endif
            @endif
        @endif
        @if(Auth::user()->role != 5)
        <li class="busdet pr"><a data-toggle="tab" href="#registration1" class="pop-next-tab-msg cancel-editables" id="nav_registration1" data-prompt-msg = 'registration1'>{{trans('messages.business_details')}} |</a></li>
        <li class="busdet pr2"><a data-toggle="tab" href="#registration2" class="pop-next-tab-msg cancel-editables" id="nav_registration2" data-prompt-msg = 'registration2'>{{trans('messages.business_details')}} ||</a></li>
        @endif
        <li class="busdet assocFiles"><a data-toggle="tab" href="#registration3" class="pop-next-tab-msg cancel-editables" id="nav_assocFiles">{{trans('messages.assoc_files')}}</a></li>

        @if ((QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->rcl || $bank_standalone==1) && $entity->is_premium != 1  && Auth::user()->is_contractor != 1 && Auth::user()->role !=5)
            <li class="planfac pr5"> <a data-toggle="tab" href="#pfs" class="pop-next-tab-msg cancel-editables"  id="nav_pfs" data-prompt-msg = 'pfs'>{{trans('messages.required_credit_line')}}</a> </li>
        @endif

        @if (($entity->status == 0) && (Auth::user()->role!=4))
            <li class="owndet" id="submitcreditrating"><a data-toggle="tab" href="#sci" class="pop-next-tab-msg cancel-editables" id="nav_sci">{{trans('messages.submit_for_credit_risk_rating')}}</a></li>
        @endif

		@if ($entity->status == 6 and Auth::user()->role == 3)
            <li class="ressum"> <a data-toggle="tab" href="#ssc" class="nav-result" id="sscore">{{trans('messages.rating_summary')}}</a> </li>
        @endif
		
	  </ul>
    </div>
  </div>