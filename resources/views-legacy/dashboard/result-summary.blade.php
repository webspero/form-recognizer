@extends('layouts.master')

@section('title', 'Dashboard')

@section('nav')

    @include('dashboard.nav')
    
@stop

@section('content')

  <div id="page-wrapper">
      <div class="row">
      <div class="col-lg-6">
        <div class="cont-box">
        <img src="{{ URL::asset('images/business-consideration.png') }}" class="img-responsive" alt="">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="cont-box">
        <img src="{{ URL::asset('images/management-quality.png') }}" class="img-responsive" alt="">
        </div>
      </div>
      </div>
      <div class="row margin-top-15">
      <div class="col-lg-6">
        <div class="cont-box">
        <img src="{{ URL::asset('images/financial-analysis-img.png') }}" class="img-responsive" alt="">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="cont-box">
        <img src="{{ URL::asset('images/industry-comparison-img.png') }}" class="img-responsive" alt="">
        </div>
      </div>
      </div>
  </div>

@stop