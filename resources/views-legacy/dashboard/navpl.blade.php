<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse collapse">
      <ul class="nav" id="side-menu">
        <li class="ressum">
			<a data-toggle="tab" href="#ssc" class="nav-result pop-next-tab-msg" id="sscore" data-prompt-msg = 'sscore'>{{trans('messages.rating_summary')}}
				<div class="ribbon-wrapper-green"><div class="ribbon-green">Result</div></div>
			</a>
		</li>
        <li class="finanal"> 
			<a data-toggle="tab" href="#fianalysis" class="nav-result pop-next-tab-msg" id="finananalysis" data-prompt-msg = 'finananalysis'>Financial Analysis
				<div class="ribbon-wrapper-green"><div class="ribbon-green">Result</div></div>
			</a> 
		</li>
		@if($bank_standalone==0)
		<li class="stratpro"> 
			<a data-toggle="tab" href="#strategicprofile" class="nav-result pop-next-tab-msg" id="nav_strategicprofile" data-prompt-msg = 'finananalysis'>{{trans('messages.sme_strategic_profile')}}
				<div class="ribbon-wrapper-green"><div class="ribbon-green">Prospects</div></div>
			</a> 
		</li>
		@endif
	  </ul>
    </div>
  </div>