@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	
	<input type="hidden" name="rm_notify" id="rm_notify" value="{{ session('rm_notify') }}" />
    @if(Auth::user()->role==1 || Auth::user()->role==4)

	@if(Auth::user()->role==1 && count($entity) == 0)
	
		<div class="text-right">
			<a href="{{ URL::to('/dropbox_upload') }}" class="btn btn-primary">{{trans('messages.file_storage')}}</a>
		</div>
		<div class="container">
			<div class="spacewrapper">
				<div class="read-only" style="text-align: center;">
					<br> <br><br>
					<a href="{{ URL::to('/create_new_report') }}" class="btn btn-primary btn-lg btn-xl" style="padding: 10px 20px; font-size: 40px;">Start {{trans('messages.new_report')}}</a>
					<br><br><br>
				</div>
			</div>
		</div>
    @elseif(Auth::user()->role==1 && Auth::user()->trial_flag == 0)
	<div class="text-right">
		<a href="{{ URL::to('/dropbox_upload') }}" class="btn btn-primary">{{trans('messages.file_storage')}}</a>
		<a href="{{ URL::to('/create_new_report') }}" class="btn btn-primary">{{trans('messages.new_report')}}</a>
	</div>
	@endif

    <?php $new_row = 1; ?>

    @include('dashboard.entity-thumb')

    @elseif(Auth::user()->role==3)
		<div class="table-responsive {{(Auth::user()->role==1) ? 'hidden-xs' : ''}}">
			<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
				<thead>
					<tr>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Company ID</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Established</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Years in Business</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">City</th>
						@if(Auth::user()->role==2)
						<th style="background-color: #055688;color: #fff; font-weight: bold;">CI Verification Status</th>
						@endif
						@if(Auth::user()->role!=2)
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
						<th style="background-color: #055688;color: #fff; font-weight: bold;">Queue Duration</th>
						@endif
					</tr>
				</thead>
				<tbody>
				@foreach ($entity as $info)
					{{ Form::hidden('gross_revenue_growth', $info->gross_revenue_growth, array('id' => 'gross_revenue_growth')) }}
					{{ Form::hidden('net_income_growth', $info->net_income_growth, array('id' => 'net_income_growth')) }}
					{{ Form::hidden('gross_profit_margin', $info->gross_profit_margin, array('id' => 'gross_profit_margin')) }}
					{{ Form::hidden('net_profit_margin', $info->net_profit_margin, array('id' => 'net_profit_margin')) }}
					{{ Form::hidden('net_cash_margin', $info->net_cash_margin, array('id' => 'net_cash_margin')) }}
					{{ Form::hidden('current_ratio', $info->current_ratio, array('id' => 'current_ratio')) }}
					{{ Form::hidden('debt_equity_ratio', $info->debt_equity_ratio, array('id' => 'debt_equity_ratio')) }}

					<tr class="odd" role="row">
						<td>{{ $info->entityid }}</td>
						<td><a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">
                            @if (1 == $info->anonymous_report)
                                Company - {{ $info->entityid }}
                            @else
                                {{ $info->entityid }} - {{ $info->companyname }}
                            @endif
                        </a></td>
						<td>{{ $info->email }}</td>
						<td>{{ $info->date_established }}</td>
						<td>{{ $info->number_year }}</td>
						<td>{{ $info->class_description }}</td>
						<td>{{ $info->citymunDesc }}</td>
						@if(Auth::user()->role==2)
						<td>
							{!!$info->ci_verification!!}
						</td>
						@endif
						@if(Auth::user()->role!=2)
						<td>
							@if ($info->status == 0)
								Active
							@elseif ($info->status == 1)
								Ready for investigation
							@elseif ($info->status == 2)
								Start investigation
							@elseif ($info->status == 3)
								On hold
							@elseif ($info->status == 4)
								Resume Investigator
							@elseif ($info->status == 5)
								<!-- Approved by investigator -->
								Ready for Risk Scoring
							@elseif ($info->status == 6)
								Risk Scoring in Progress
							@elseif ($info->status == 7)
								Risk Scoring Completed {{ ($info->completed_date!=null) ? date('m/d/Y', strtotime($info->completed_date)) : ""}}
							@elseif ($info->status == 8)
								Approved by the Bank
							@elseif ($info->status == 9)
								Deleted
							@else
								INVALID STATUS
							@endif
						</td>
						@if(Auth::user()->role==3)
						<td id="timer{{ $info->entityid }}" class="timer">{{ date('c', strtotime($info->submitted_at)) }}</td>
						@endif
						@endif
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	@elseif(Auth::user()->role == 5) 
		<div class="table-responsive {{(Auth::user()->role==1) ? 'hidden-xs' : ''}}">
			<table id="pendingData" class="table table-striped table-bordered" cellspacing="0">
				<thead>
					<tr>
						<th class="thead_pendingData" >Report ID</th>
						<th class="thead_pendingData" >Submitted By</th>
						<th class="thead_pendingData"  >Company Name</th>
						<th class="thead_pendingData thead_hide" >Company Email</th>
						<th class="thead_pendingData thead_hide" >Industry</th>
						<th class="thead_pendingData thead_hide" >City</th>
						<th class="thead_pendingData" >Date Submitted</th>
						<th class="thead_pendingData" >Status</th>
						<th class="thead_pendingData" >Queue Duration</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($entity as $info)
					<tr>
						<td> <a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}" ttarget="_blank"> {{$info->entityid}} </a> </td>
						<td> {{$info->submitted_by}} </td>
						<td> <a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}" target="_blank"> {{$info->companyname}} </a> </td>
						<td class="thead_hide"> {{$info->email}} </td>
						<td class="thead_hide"> {{$info->row_title}}  </td>
						<td class="thead_hide"> {{$info->citymunDesc}} </td>
						<td> {{$info->submitted_at}} </td>
						<td> {{$info->status_desc}} </td>
						<td id="timer{{ $info->entityid }}" class="timer">{{ date('c', strtotime($info->submitted_at)) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endif
</div>

<div class="modal" id="unpaid-link-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class = "glyphicon glyphicon-exclamation-sign"> </i> Notice </h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                <p>This report has not yet been started. There is no information to display.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@if(session()->has('success'))
<input type="hidden" name="success" id="success" value="{{ session()->get('success') }}">
<div class="modal" id="matching-success-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class = "glyphicon glyphicon-exclamation-sign"> </i> Success Notice </h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                
			    <div class="alert alert-success">
			        {{ session()->get('success') }}
			    </div>
				
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@endif

@include('user-2fa-form')

<input type="hidden" id="server_time" value="{{ date('c') }}" />
@stop

@section('javascript')

	<script type="text/javascript">

		if($('#success').val()){
			$('#matching-success-modal').modal('show');
		}
		
		$(".matching").change(function() {

			var matching = 'No';
			if(this.checked){
				var matching = 'Yes';
			}
		     
		    window.location = "{{ URL::to('report_matching/optin/') }}/" + matching + '/' + $(this).attr('rel');
		});
	</script>

	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<script src="{{ URL::asset('js/tutorial.js') }}"></script>
		<script type="text/javascript">
			$(window).load(function(){
				Tutorial.init('step0');
			});
		</script>
	@endif
    <script>
    $(document).ready(function(){
        $('.unpaid-sme').click(function() {
            /** Open the Rating form modal */
            $('#unpaid-link-modal').modal('show');

            return false;
        });
    });
	</script>
	<script type="text/javascript">
	var servertime = new Date($('#server_time').val());
	var clientime = new Date();
	var servertime_distance = servertime.getTime() - clientime.getTime();

	$(document).ready(function(){
		$('.timer').each(function(){
			CountDownTimer($(this).text(), $(this).attr('id'));
		});
	});

	function CountDownTimer(dt, id)
    {
        var start = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = now.getTime() - start.getTime() + servertime_distance;

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

			var display = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
            document.getElementById(id).innerHTML = display;

        }

        timer = setInterval(showRemaining, 1000);
    }
</script>
    <script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>

    <script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
	<script src="{{ URL::asset('js/chart-min.js') }}"></script>
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>

@stop
@section('style')
	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"></link>
	@endif
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard/entity.css') }}" media="screen"></link>
@stop
