@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h1>{{trans('billing-details.billing_details')}}</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif

				<div class="form-group-row">
                  
                        {{ Form::open(array('route'=>'postAdminSupBankBillingDetails', 'method'=>'post')) }}
                        <div class="form-group">
                            @if($user->role == 4)
                                {{ Form::label('name', 'Company Name/Biller Name') }}
                                @if($user->name != "")
                                    {{ Form::text('name', $user->name, array('class'=>'form-control', 'required' => 'required',)) }}
                                @else
                                    {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'required' => 'required')) }}
                                @endif
                            @endif

                            {{ Form::label('province', trans('billing-details.province')) }}
                            @if($user->province != "")
                                {{ Form::text('province', $user->province, array('class'=>'form-control', 'required' => 'required',)) }}
                            @else
                                {{ Form::text('province', Input::old('province'), array('class'=>'form-control', 'required' => 'required')) }}
                            @endif
                            
                            {{ Form::label('city', trans('billing-details.city')) }}
                            @if($user->city != "")
                                {{ Form::text('city', $user->city, array('class'=>'form-control', 'required' => 'required')) }}
                            @else
                                {{ Form::text('city', Input::old('city'), array('class'=>'form-control', 'required' => 'required')) }}
                            @endif

                            {{ Form::label('street', trans('billing-details.street_address')) }}
                            @if($user->street_address != "")
                                {{ Form::text('street', $user->street_address, array('class'=>'form-control', 'required' => 'required')) }}
                            @else
                                {{ Form::text('street', Input::old('street'), array('class'=>'form-control', 'required' => 'required')) }}
                            @endif
                            
                            {{ Form::label('zipcode', trans('billing-details.zipcode')) }}
                            @if($user->zipcode != "")
                                {{ Form::text('zipcode', $user->zipcode, array('class'=>'form-control', 'required' => 'required')) }}
                            @else
                                {{ Form::text('zipcode', Input::old('zipcode'), array('class'=>'form-control', 'required' => 'required')) }}
                            @endif

                            {{ Form::label('phone', trans('billing-details.phone')) }}
                            @if($user->phone != "")
                                {{ Form::text('phone', $user->phone, array('class'=>'form-control', 'required' => 'required')) }}
                            @else
                                {{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'required' => 'required')) }}
                            @endif

                        </div>
                        {{ Form::submit(trans('billing-details.save_details'), array('class'=>'btn btn-success')) }}
                    {{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@include('modal-subscription-expired');

<input type="hidden" name="unsubscribe" id="unsubscribe" value="{{ empty($subscription) && Auth::user()->role != 1 ? 0:1 }}" />
@stop
@section('javascript')

<script type="text/javascript">
	
	if($('#unsubscribe').val() == 0)
		$('#modal-subscription-expired').modal('show');
	
</script>
@stop
