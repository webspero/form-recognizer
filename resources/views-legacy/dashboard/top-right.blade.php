@if(Auth::check())
@if ( (Auth::user()->status == 2 OR (Auth::user()->role == 1 && Auth::user()->status == 1))  or Auth::user()->role != 1)
<!--<a class="navmenu-brand" href="#">Risk Assesment System</a>-->
<style>
    @media (max-width: 769px) {
    .navbar-header {
        float: none;
    }
    .navbar-left,.navbar-right {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
		top: 0;
		border-width: 0 0 1px;
	}
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
		margin-top: 7.5px;
	}
	.navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
  		display:block !important;
	}
}

@if (Auth::user()->role == 0  && Session::has('supervisor') != true)
.nav.nav.navbar-top-links > li > a,
.nav.nav.navbar-top-links > li > span {
	padding: 20px 3px;
	font-size: 11px;
}
@else
.nav.nav.navbar-top-links > li > a,
.nav.nav.navbar-top-links > li > span {
	font-size: 14px;
}
.nav > li {
	margin-left: 10px;
	text-align: center;
}

.dropdown-menu {
	margin-top: 20px !important;
}

.dropdown-menu li>a  {
	font-size: 14px;
	padding: 3px;
}

.nav.navbar-top-links > li > ul {
	width: 200px;
}

.dropdown-menu a:active {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
	background-color: transparent !important;
}

.dropdown-menu a:active {
	box-shadow: none !important;
}

@endif
</style>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-top-links navbar-right">
				 @if (Auth::user()->role == 0 && Session::has('supervisor') != true)
                <li> <a href="{{ URL::to('/') }}/admin" class="navbar-link" id="nav-link-accounts">Accounts</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/reports" class="navbar-link" id="nav-link-accounts">Reports</a></li>
                <li> <a href="{{ URL::to('/') }}/supervisor/list" class="navbar-link" id="nav-link-supervisor-accounts">Supervisor Accounts</a></li>
                <li>
                  <a href="{{ URL::to('/') }}/admin/valid-user-statistics" class="navbar-link" id="nav-link-valid-user-statistics">
                    Valid User Statistics
                  </a>
                </li>
                <li> <a href="{{ URL::to('/') }}/developers/list" class="navbar-link" id="nav-link-developer-api-users">Developer API Users</a></li>
                <li> <a href="{{ URL::to('/') }}/admin/industry" class="navbar-link" id="nav-link-industry-weights">Industry Weights</a></li>
                <!-- <li> <a href="{{ URL::to('/') }}/admin/user-feedbacks" class="navbar-link" id="nav-link-user-feedbacks">User Feedbacks</a></li> -->
				<li> <a href="{{ URL::to('/') }}/admin/clientkeys" class="navbar-link" id="nav-link-independent-keys">Independent Keys</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/supervisor_clientkeys" class="navbar-link" id="nav-link-clienyt-keys">Client Keys</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/discounts" class="navbar-link" id="nav-link-discounts">Discounts</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/transactions" class="navbar-link" id="nav-link-transactions">Transactions</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/reviews" class="navbar-link" id="nav-link-admin-review">Admin Review</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/premium_reports" class="navbar-link" id="nav-link-premium-report-process">Premium Report Process</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/trials" class="navbar-link" id="nav-link-trial-accounts">Trial Accounts</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/organizations" class="navbar-link" id="nav-link-organizations">Organizations</a></li>
				<li> <a href="{{ URL::to('/') }}/admin/security" class="navbar-link" id="nav-link-security">Security</a></li>
				<li><span id="nav-link-tools">Tools</span>
					<ul>
						{{-- <li> <a href="{{ URL::to('/') }}/pse/extraction_point" class="navbar-link" id="nav-link-pse-data">PSE Data</a></li> --}}
						<li> <a href="{{ URL::to('/') }}/admin/converter" class="navbar-link" id="nav-link-excel-to-vfa">Excel to vfa</a></li>
                        <li> <a href="{{ URL::to('/') }}/admin/ocr_options" class="navbar-link" id="nav-link-ocr-options">OCR Options</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/leads" class="navbar-link" id="nav-link-leads">Leads</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/scraper" class="navbar-link" id="nav-link-bsp-directory-scraper">BSP Directory Scraper</a><li>
						<li> <a href="{{ URL::to('/') }}/fr" class="navbar-link" id="nav-link-financial-report-viewer">Financial Report Viewer</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/upload_psa_forecasting" class="navbar-link" id="nav-link-psa-forecasting-data">PSA Forecasting Data</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/analyst_report" class="navbar-link" id="nav-link-analyst-report">Analyst Report</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/third_party" class="navbar-link" id="nav-link-third-party">Third Party</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/cmap_search_count" class="navbar-link" id="nav-link-cmap-search-count">CMAP Search Count</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/notifications" class="navbar-link" id="nav-link-custom-notifications">Custom Notifications</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/innodata" class="navbar-link" id="nav-link-innodata-recipients">Innodata Recipients</a></li>
						<li> <a href="{{ URL::to('/logs') }}" class="navbar-link" id="nav-link-logs" target="_blank">Logs</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/industry/comparison" class="navbar-link" id="navbar-link-industry-comparison">Industry Comparison</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/transcribe_files" class="navbar-link" id="navbar-link-transcribe-fs">Transcribe FS</a></li>
						<li> <a href="{{ URL::to('/') }}/admin/form-recognizer" class="navbar-link">Form Recognizer</a></li>
					</ul>
				</li>
                @endif

                @if (Auth::user()->role == 1)
					@if(Auth::user()->trial_flag == 1)
						<li> <a href="{{ URL::to('/') }}/upgrade_to_full" onClick="if(!confirm('Confirm upgrade to Full Account Status?')) return false;" id="nav-link-upgrade-to-full">Upgrade to Full</a> </li>
					@endif
				<li> <a href="{{ URL::to('/') }}/lang/en" id="nav-link-english">{{trans('messages.english')}}</a> </li>
				<li> <a href="{{ URL::to('/') }}/lang/fil" id="nav-link-filipino">{{trans('messages.filipino')}}</a> </li>
                @if(!Session::has('tql'))
				<li> <a href="{{ URL::to('/') }}/dashboard/index" id="nav-link-dashboard"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Dashboard </a> </li>

                @if(!empty(Auth::user()->name))
				<li> <span id="nav-link-user-name">{{ Auth::user()->name }}</span> </li>
				@else
				<li> <a href="{{ URL::to('/') }}/dashboard/index" id="nav-link-user-fullname">{{ Auth::user()->firstname.' '.Auth::user()->lastname }} </a></li>
				@endif
                @endif
				@endif

				@if (Auth::user()->role == 2)
                <li> <a href="{{ URL::to('/') }}/dashboard/index" id="nav-link-dashboard"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Dashboard </a> </li>
				@endif



                @if (Auth::user()->role == 3)
                <li><a href="{{ URL::to('/') }}/dashboard/index" class="navbar-link" id="nav-link-submitted-reports">Submitted Reports for Risk Rating</a></li>
                <li> <a href="{{ URL::to('/') }}/dashboard/smebucket" id="nav-link-my-report-rating"><img src="{{ URL::asset('images/business-details.png') }}" alt="">My Report Rating Bucket </a> </li>
                <li><a href="{{ URL::to('/') }}/dashboard/smescored" class="navbar-link" id="nav-link-risk-rating">Risk Rating Completed</a></li>
                @endif

				@if (Auth::user()->role == 5)
                <li><a href="{{ URL::to('/') }}/dashboard/index" class="navbar-link" id="nav-link-submmitted-reports">Submitted Reports for Document Approval</a></li>
                <li> <a href="{{ URL::to('/') }}/dashboard/approved" id="nav-link-completed-report"><img src="{{ URL::asset('images/business-details.png') }}" alt="">Completed Report Approval </a> </li>
				@endif

				@if (Auth::user()->role == 4)
					@if(Auth::user()->product_plan==3)
						<li> <a href="{{ URL::to('/') }}/lang/en" id="nav-link-english">{{trans('messages.english')}}</a> </li>
						<li> <a href="{{ URL::to('/') }}/lang/fil" id="nav-link-filipino">{{trans('messages.filipino')}}</a> </li>
						<li> <a href="{{ URL::to('/') }}/bank/dashboard" id="nav-link-basic-report-listing"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Basic Report Listing</a> </li>
						<li><a href="{{ URL::to('/') }}/bank/keys" class="navbar-link" id="nav-link-client-keys">Client Keys</a></li>
					@else
						<li> <a href="{{ URL::to('/') }}/lang/en" id="nav-link-english">{{trans('messages.english')}}</a> </li>
						<li> <a href="{{ URL::to('/') }}/lang/fil" id="nav-link-filipino">{{trans('messages.filipino')}}</a> </li>
						<li> <a href="{{ URL::to('/') }}/bank/dashboard" id="nav-link-dashboard"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Dashboard</a> </li>

						@if(!empty(session('report_matching_bank')) || !empty(session('rm_bank_id')))

							<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/sme_reports" id="nav-link-company-matching">Company Matching</a> </li>

						@else
						@if(Permissions::check(11,false))
							<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/bank_signup/{{ Auth::id() }}" id="nav-link-company-matching">Company Matching</a> </li>
						@endif
						@endif

						<li> <a href="{{ URL::to('/') }}/bank/reportlist" class="navbar-link" id="nav-link-report-listing">Report Listing</a> </li>

						@if(Permissions::check(3,false))
						<li><a href="{{ URL::to('/') }}/bank/keys" class="navbar-link" id="nav-link-client-keys">Client Keys</a></li>
						@endif
						@if(Permissions::check(4,false))
						<li><a href="{{ URL::to('/') }}/bank/statistics" class="navbar-link" id="nav-link-portfolio-statistics">Portfolio Statistics</a></li>
						@endif

						@if(Permissions::check(7,false))
						<li><a href="{{ URL::to('/') }}/view_map" class="navbar-link" id="nav-link-map">Map</a></li>
						@endif

						@if (Auth::user()->parent_user_id == 0)
						<li><a href="{{ URL::to('/') }}/bank/nfcc" class="navbar-link" id="nav-link-nfcc">NFCC</a></li>
						@if (Auth::user()->parent_user_id == 0)
							<li ><a href="{{ URL::to('/') }}/bank/sub_users" class="navbar-link"  id="nav-users">Users</a></li>
							@if(Auth::user()->getAssociatedBank(Auth::user()->bank_id)->bank_name == "Contractors Platform")
							    <li ><a href="{{ URL::to('/') }}/bank/contractors" class="navbar-link" id="nav-link-contractors">Contractors</a></li>
							@endif
						@endif
						<li class="dropdown" style="display:none">
							<a href="#" class="drop-down" data-toggle="dropdown"  id="nav-link-accounts">Accounts</a>
							<ul class="dropdown-menu">
								@if(Permissions::check(5,false))
									<li ><a href="{{ URL::to('/') }}/bank/ci_users" class="navbar-link" id="nav-link-CI">CI</a></li>
								@endif
								@if (Auth::user()->parent_user_id == 0)
									<li ><a href="{{ URL::to('/') }}/bank/sub_users" class="navbar-link" id="nav-link-users">Users</a></li>
								@endif
							</ul>
						</li>
						@endif
						@if(Permissions::check(10,false))
							<li ><a href="{{ URL::to('/') }}/bank/notifications" class="navbar-link" id="nav-link-notifications">Notifications</a></li>
						@endif
						<li class="dropdown" style="display:none">
							<a href="#" class="drop-down" data-toggle="dropdown" id="nav-link-settings">Settings</a>
							<ul class="dropdown-menu">
								@if(Permissions::check(2,false))
									<li ><a href="{{ URL::to('/') }}/bank/industry" class="navbar-link" id="nav-link-industry-weights">Industry Weights</a></li>
								@endif
								@if(Permissions::check(6,false))
									<li ><a href="{{ URL::to('/') }}/bank/formula" class="navbar-link" id="nav-link-rating-formula">Rating Formula</a></li>
								@endif
								@if(Permissions::check(8,false))
									<li ><a href="{{ URL::to('/') }}/bank/document_options" class="navbar-link" id="nav-link-document-options">Document Options</a></li>
								@endif
								@if(Permissions::check(9,false))
									<li ><a href="{{ URL::to('/') }}/bank/questionnaire_config" class="navbar-link" id="nav-link-questionnaire-config">Questionnaire Config</a></li>
								@endif
								@if(Permissions::check(10,false))
									<li ><a href="{{ URL::to('/') }}/bank/notifications" class="navbar-link" id="nav-link-notifications">Notifications</a></li>
								@endif

							</ul>
						</li>
					@endif
				@endif
				@if (Auth::user()->role == 0 && Session::has('supervisor'))
					<li>
						<a href="{{ URL::to('/') }}/supervisor/list" id="nav-button"  style="display: inline-block">
						<img src="{{ URL::asset('images/back.png') }}" alt="back" id="back-img">Back
						</a>
						<span>{{Session::get('supervisor.email')}}</span>
						<span id="nav-line">|</span>
					</li>
					<li> <a href="{{ URL::to('/') }}/lang/en" id="nav-link-english">{{trans('messages.english')}}</a> </li>
					<li> <a href="{{ URL::to('/') }}/lang/fil" id="nav-link-filipino">{{trans('messages.filipino')}}</a> </li>


					@if(Auth::user()->product_plan==3)
						<li> <a href="{{ URL::to('/') }}/bank/dashboard" id="nav-link-basic-report-listing"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Basic Report Listing</a> </li>
						<li> <a href="{{ URL::to('/') }}/lang/fil" id="nav-link-filipino">{{trans('messages.filipino')}}</a> </li>

						@if(!empty(session('report_matching_bank')) || !empty(session('rm_bank_id')))

							<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/sme_reports" id="nav-link-company-matching">Company Matching</a> </li>

						@else
							@if(Permissions::check(11,false))
								<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/bank_signup/{{ Session::get('supervisor.loginid') }}" id="nav-link-company-matching">Company Matching</a> </li>
							@endif
						@endif
						<li><a href="{{ URL::to('/') }}/bank/keys" class="navbar-link" id="nav-link-client-keys">Client Keys</a></li>
					@else
						<li> <a href="{{ URL::to('/') }}/bank/dashboard" id="nav-link-dashboard"><img src="{{ URL::asset('images/dashboard-icon.png') }}" alt="">Dashboard</a> </li>

						@if(!empty(session('report_matching_bank')) || !empty(session('rm_bank_id')))

							<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/sme_reports" id="nav-link-company-matching">Company Matching</a> </li>

						@else
							@if(Permissions::check(11,false))
								<li> <a class="navbar-link" href="{{ URL::to('/') }}/report_matching/bank_signup/{{ Auth::id() }}" id="nav-link-company-matching">Company Matching</a> </li>
							@endif
						@endif

						<li> <a href="{{ URL::to('/') }}/bank/reportlist" class="navbar-link" id="nav-link-report-listing">Report Listing</a> </li>

					@if(Permissions::check(3,false))
						<li><a href="{{ URL::to('/') }}/bank/keys" class="navbar-link" id="nav-link-client-keys">Client Keys</a></li>
						@endif

						@if(Permissions::check(4,false))
						<li><a href="{{ URL::to('/') }}/bank/statistics" class="navbar-link" id="nav-link-portfolio-statistics">Portfolio Statistics</a></li>
						@endif

						@if(Permissions::check(7,false))
						<li><a href="{{ URL::to('/') }}/view_map" class="navbar-link" id="nav-link-map">Map</a></li>
						@endif

						@if (Auth::user()->parent_user_id == 0)
							<li ><a href="{{ URL::to('/') }}/bank/sub_users" class="navbar-link" id="nav-link-users">Users</a></li>
						@endif

						@if(Permissions::check(10,false))
							<li ><a href="{{ URL::to('/') }}/bank/notifications" class="navbar-link" id="nav-link-notifications">Notifications</a></li>
						@endif

						<li class="dropdown" style="display:none">
							<a href="#" class="drop-down" data-toggle="dropdown"  id="nav-link-accounts">Accounts</a>
							<ul class="dropdown-menu">
								@if(Permissions::check(5,false))
									<li ><a href="{{ URL::to('/') }}/bank/ci_users" class="navbar-link" id="nav-link-CI">CI</a></li>
								@endif
								@if (Auth::user()->parent_user_id == 0)
									<li ><a href="{{ URL::to('/') }}/bank/sub_users" class="navbar-link" id="nav-link-users">Users</a></li>
								@endif
							</ul>
						</li>
					@endif
				@endif

				@if(!Session::has('tql'))
					@if(Auth::user()->role == 0 && Session::has('supervisor'))
						<li class="dropdown">
							<a href="#" class="drop-down" data-toggle="dropdown" id="nav-link-profile" >Profile</a>
							<ul class="dropdown-menu">
								<li> <a href="{{ URL::to('/') }}/admin/supervisor/bankbillingdetails" class="navbar-link" id="nav-link-billing-details">{{ trans('billing-details.billing_details') }}</a></li>
								<li> <a href="{{ URL::to('/') }}/admin/supervisor/bank/subscription" class="navbar-link" id="nav-link-subscription">Subscription</a></li>

							</ul>
						</li>
					@else
						@if(Auth::user()->role == 0 || Auth::user()->role == 3 )
							<li><a href="{{ URL::to('/') }}/logout" class="navbar-link" id="nav-link-logout">Logout</a></li>
						@else
							<li class="dropdown">
								<a href="#" class="drop-down" data-toggle="dropdown" id="nav-link-profile" >Profile</a>
								<ul class="dropdown-menu">
									@if(Auth::user()->role == 1)
									<li> <a href="{{ URL::to('/') }}/billingdetails" class="navbar-link" id="nav-link-billing-details">{{ trans('billing-details.billing_details') }}</a></li>
									@endif
									@if(Auth::user()->role == 4)
									<li> <a href="{{ URL::to('/') }}/bankbillingdetails" class="navbar-link" id="nav-link-billing-details">{{ trans('billing-details.billing_details') }}</a></li>
									<li> <a href="{{ URL::to('/') }}/bank/subscription" class="navbar-link" id="nav-link-subscription">Subscription</a></li>
									@endif
									<li><a href="{{ URL::to('/') }}/logout" class="navbar-link" id="nav-link-logout">Logout</a></li>
								</ul>
							</li>
						@endif
					@endif
				@endif
        </ul>
</div>
@endif
@endif
