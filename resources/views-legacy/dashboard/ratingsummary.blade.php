@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div class="container">
	<div class="row">
		<p class="navbar-text navbar-right-text">
		Welcome {{ Auth::user()->firstname.' '.Auth::user()->lastname }}
		<a href="{{ URL::to('/') }}/logout" class="navbar-link">Logout</a>
		</p>
		<div class="page-header">
			@if (Auth::user()->role == 1)
			<h1>Rating Summary</h1>
			@endif
			@if (Auth::user()->role == 3)
			<h1>Submitted Reports for Risk Rating</h1>
			@endif
		</div>
	</div>
	@if(Auth::user()->role == 1)
	@if($entity[0]->status == 7)
	@foreach ($readyratiodata as $readyratiodata)
		{{ Form::hidden('gross_revenue_growth2', $readyratiodata->gross_revenue_growth2, array('id' => 'gross_revenue_growth2')) }}
		{{ Form::hidden('net_income_growth2', $readyratiodata->net_income_growth2, array('id' => 'net_income_growth2')) }}
		{{ Form::hidden('gross_profit_margin2', $readyratiodata->gross_profit_margin2, array('id' => 'gross_profit_margin2')) }}
		{{ Form::hidden('net_profit_margin2', $readyratiodata->net_profit_margin2, array('id' => 'net_profit_margin2')) }}
		{{ Form::hidden('net_cash_margin2', $readyratiodata->net_cash_margin2, array('id' => 'net_cash_margin2')) }}
		{{ Form::hidden('current_ratio2', $readyratiodata->current_ratio2, array('id' => 'current_ratio2')) }}
		{{ Form::hidden('debt_equity_ratio2', $readyratiodata->debt_equity_ratio2, array('id' => 'debt_equity_ratio2')) }}

		{{ Form::hidden('receivables_turnover2', $readyratiodata->receivables_turnover2, array('id' => 'receivables_turnover2')) }}
		{{ Form::hidden('inventory_turnover2', $readyratiodata->inventory_turnover2, array('id' => 'inventory_turnover2')) }}
		{{ Form::hidden('accounts_payable_turnover2', $readyratiodata->accounts_payable_turnover2, array('id' => 'accounts_payable_turnover2')) }}
	@endforeach

	<div class="row">
	<div style="float: left; width: 100%;">
	<div style="float: left; width: 50%; padding: 10px 0px;">
		<div style="float: left; width: 98%; border: 1px solid grey;">
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title1" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Business Considerations and Conditions</b></div>
					<div style="float: right; padding-right: 10px;">
					<b>
					<span id="show1" style="display: block;">+</span>
					<span id="hide1" style="display: none;">-</span>
					</b>
					</div>
				</div>
				<div id="description1" style="float: left; width: 100%; display: none;">
					<div style="padding: 10px;">
						@foreach ($finalscore as $finalscoreinfo)
							{{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
							{{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
							{{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
							{{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
							@if ($finalscoreinfo->score_rm == 90)
								<p style="font-size:11px;"><b>Risk Management Rating <span style="color: green;">HIGH</span></b>
								<br/>You have an excellent understanding of the major risks your company faces and a good plan to address them.
								</p>
								{{ Form::hidden('score_rm_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_rm_value')) }}
							@elseif ($finalscoreinfo->score_rm == 30)
								<p style="font-size:11px;"><b>Risk Management Rating <span style="color: red;">LOW</span></b>
								<br/>Your company will benefit from a Risk Analysis and Planning session.
								</p>
					    		{{ Form::hidden('score_rm_value', '<span style="color: red;">LOW</span>', array('id' => 'score_rm_value')) }}
					    	@endif

							{{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
							@if ($finalscoreinfo->score_cd == 30)
								<p style="font-size:11px;"><b>Customer Dependency <span style="color: green;">HIGH</span></b>
								<br/>You have a well diversified customer base. Excellent.
								</p>
								{{ Form::hidden('score_cd_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_cd_value')) }}
							@elseif ($finalscoreinfo->score_cd == 20)
								<p style="font-size:11px;"><b>Customer Dependency <span style="color: orange;">MODERATE</span></b>
								<br/>The business tends to be reliant on a few of the potential customer base. Consider dedicating time and resources to getting new customers as soon as possible.
								</p>
					    		{{ Form::hidden('score_cd_value', '<span style="color: orange;">MODERATE</span>', array('id' => 'score_cd_value')) }}
					    	@else
								<p style="font-size:11px;"><b>Customer Dependency <span style="color: red;">LOW</span></b>
								<br/>The business is heavily reliant on a few of the potential customer base. Dedicate time and resources to getting new customers as soon as possible.
								</p>
					    		{{ Form::hidden('score_cd_value', '<span style="color: red;">LOW</span>', array('id' => 'score_cd_value')) }}
					       	@endif

							{{ Form::hidden('score_sd', $finalscoreinfo->score_sd, array('id' => 'score_sd')) }}
							@if ($finalscoreinfo->score_sd == 30)
								<p style="font-size:11px;"><b>Supplier Dependency <span style="color: green;">HIGH</span></b>
								<br/>You have a well diversified supplier base. Excellent.
								</p>
								{{ Form::hidden('score_sd_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_sd_value')) }}
							@elseif ($finalscoreinfo->score_sd == 20)
								<p style="font-size:11px;"><b>Supplier Dependency <span style="color: orange;">MODERATE</span></b>
								<br/>Your business is at risk because it is heavily dependent on a few suppliers. Consider dedicating time and resources to finding new suppliers as soon as possible.
								</p>
					    		{{ Form::hidden('score_sd_value', '<span style="color: orange;">MODERATE</span>', array('id' => 'score_sd_value')) }}
					    	@else
								<p style="font-size:11px;"><b>Supplier Dependency <span style="color: red;">LOW</span></b>
								<br/>Your business is at risk because it is heavily dependent on a few suppliers. Dedicate time and resources to finding new suppliers as soon as possible.
								</p>
					    		{{ Form::hidden('score_sd_value', '<span style="color: red;">LOW</span>', array('id' => 'score_sd_value')) }}
					       	@endif

							{{ Form::hidden('score_bois', $finalscoreinfo->score_bois, array('id' => 'score_bois')) }}
							@if ($finalscoreinfo->score_bois == 90)
								<p style="font-size:11px;"><b>Business Outlook Index Score <span style="color: green;">HIGH</span></b>
								<br/>Given business expectation data over the last 2 years, your industry is on an upward trend. This is ideal.
								</p>
								{{ Form::hidden('score_bois_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_bois_value')) }}
							@elseif ($finalscoreinfo->score_bois == 60)
								<p style="font-size:11px;"><b>Business Outlook Index Score <span style="color: orange;">MODERATE</span></b>
								<br/>Given business expectation data over the last 2 years, your industry has hit a plateau. To remain viable, try product line extension, incremental product improvement, and explore new markets.
								</p>
					    		{{ Form::hidden('score_bois_value', '<span style="color: orange;">MODERATE</span>', array('id' => 'score_bois_value')) }}
					    	@else
								<p style="font-size:11px;"><b>Business Outlook Index Score <span style="color: red;">LOW</span></b>
								<br/>Given business expectation data over the last 2 years, your industry is on a downward trend.  To remain viable, seriously study opportunities in new markets, doing product line extension, incremental product improvement.  Don't wait.
								</p>
					    		{{ Form::hidden('score_bois_value', '<span style="color: red;">LOW</span>', array('id' => 'score_bois_value')) }}
					       	@endif

							{{ Form::hidden('score_agroup_total', 0, array('id' => 'score_agroup_total')) }}
							{{ Form::hidden('score_agroup_sum', 240, array('id' => 'score_agroup_sum')) }}

						@endforeach
					</div>
				</div>
			</div>
			<div style="float: left; width: 100%;">
			<div id="container-pie1" style="min-width: 310px; height: 300px; max-width: 100%; margin: 0 auto"></div>
			</div>
		</div>
	</div>
	<div style="float: left; width: 50%; padding: 10px 0px;">
		<div style="float: left; width: 98%; border: 1px solid grey;">
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title1" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Management Quality</b></div>
					<div style="float: right; padding-right: 10px;">
					<b>
					<span id="show2" style="display: block;">+</span>
					<span id="hide2" style="display: none;">-</span>
					</b>
					</div>
				</div>
				<div id="description2" style="float: left; width: 100%; display: none;">
					<div style="padding: 10px;">
						@foreach ($finalscore as $finalscoreinfo)
							{{ Form::hidden('score_sf', $finalscoreinfo->score_sf, array('id' => 'score_sf')) }}
							{{ Form::hidden('score_bdms', $finalscoreinfo->score_bdms, array('id' => 'score_bdms')) }}
							@if ($finalscoreinfo->score_bdms == 36)
								<p style="font-size:11px;"><b>Business Driver and Main Business Segment Rating <span style="color: green;">HIGH</span></b>
								<br/>You have an excellent understanding of the major business drivers and segments that contribute to your revenue.
								</p>
								{{ Form::hidden('score_bdms_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_bdms_value')) }}
							@elseif ($finalscoreinfo->score_bdms == 12)
								<p style="font-size:11px;"><b>Business Driver and Main Business Segment Rating <span style="color: red;">LOW</span></b>
								<br/>Your company may benefit from benchmarking your last 2 years worth of transactions and determining your major business drivers and customer segments. Identifying them is critical to boosting profitability.
								</p>
					    		{{ Form::hidden('score_bdms_value', '<span style="color: red;">LOW</span>', array('id' => 'score_bdms_value')) }}
					    	@endif

							{{ Form::hidden('score_sp', $finalscoreinfo->score_sp, array('id' => 'score_sp')) }}
							@if ($finalscoreinfo->score_sp == 36)
								<p style="font-size:11px;"><b>Succession Plan <span style="color: green;">HIGH</span></b>
								<br/>You have an excellent succession plan in place for the mission critical leadership positions in your company.
								</p>
								{{ Form::hidden('score_sp_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_sp_value')) }}
							@elseif ($finalscoreinfo->score_sp == 24)
								<p style="font-size:11px;"><b>Succession Plan <span style="color: orange;">MODERATE</span></b>
								<br/>You have a good succession plan in place for the mission critical leadership positions in your company.
								</p>
					    		{{ Form::hidden('score_sp_value', '<span style="color: orange;">MODERATE</span>', array('id' => 'score_sp_value')) }}
					    	@else
								<p style="font-size:11px;"><b>Succession Plan <span style="color: red;">LOW</span></b>
								<br/>Your company may benefit from a good Succession Planning session.
								</p>
					    		{{ Form::hidden('score_sp_value', '<span style="color: red;">LOW</span>', array('id' => 'score_sp_value')) }}
					       	@endif

							{{ Form::hidden('score_ppfi', $finalscoreinfo->score_ppfi, array('id' => 'score_ppfi')) }}
							@if ($finalscoreinfo->score_ppfi == 36)
								<p style="font-size:11px;"><b>Past Projects and Future Initiatives <span style="color: green;">HIGH</span></b>
								<br/>You have an excellent Business Planning practice that allows you to consistently set initiatives and complete projects.
								</p>
								{{ Form::hidden('score_ppfi_value', '<span style="color: green;">HIGH</span>', array('id' => 'score_ppfi_value')) }}
							@elseif ($finalscoreinfo->score_ppfi == 12)
								<p style="font-size:11px;"><b>Past Projects and Future Initiatives <span style="color: red;">LOW</span></b>
								<br/>Your company will benefit from good quarterly or semi-annual Business Planning sessions to strategize for growth.
								</p>
					    		{{ Form::hidden('score_ppfi_value', '<span style="color: red;">LOW</span>', array('id' => 'score_ppfi_value')) }}
					    	@endif

							{{ Form::hidden('score_bgroup_total', 0, array('id' => 'score_bgroup_total')) }}
							{{ Form::hidden('score_bgroup_sum', 108, array('id' => 'score_bgroup_sum')) }}

						@endforeach
					</div>
				</div>
			</div>
			<div style="float: left; width: 100%;">
				<div id="container-pie2" style="min-width: 310px; height: 300px; max-width: 100%; margin: 0 auto"></div>
			</div>
		</div>
	</div>
	<div style="float: left; width: 100%;">
	<div style="float: left; width: 50%; padding: 10px 0px;">
		<div style="float: left; width: 98%; border: 1px solid grey;">
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title4" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Financial Conditions</b></div>
					<div style="float: right; padding-right: 10px;">
					</div>
				</div>
				<div id="description3" style="float: left; width: 100%; display: none;">
					<div style="padding: 10px;">
						<span class="description-bar"></span>
					</div>
				</div>
			</div>
			<div style="float: left; width: 100%;">
				<div style="float: left; width: 60%;">
				<div id="container-semi" style="min-width: 210px; height: 200px; max-width: 100%; margin: 0 auto"></div>
				</div>
				<div style="float: left; width: 40%; font-size: 11px;">
					<div style="float: left; width: 50%;">
						<p><b>Financial<br/>Position</b></p>
						<p><span id="fposition_value"></span></p>
						Days<br/>
						Average<br/>
						Receivables Turnover<br/>
						+ Inventory Turnover<br/>
						- Payable Turnover<br/>
						<b>Cash Conversion Cycle</b>

					</div>
					<div style="float: left; width: 50%; text-align: center;">
						<p><b>Financial<br/>Performance</b></p>
						<p><span id="fperformance_value"></span></p>
						<br/>
						Normal<br/>
						<span id="grap3-text1"></span><br/>
						<span id="grap3-text2"></span><br/>
						<span id="grap3-text3"></span><br/>
						<b><span id="grap3-text4"></span></b>

					</div>
				</div>
			</div>
		</div>
		<div style="padding:5px; float: left; width: 100%;"></div>
		<div style="float: left; width: 98%; border: 1px solid grey;">
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title3_3" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Credit Risk Rating</b></div>
					<div style="float: right; padding-right: 10px;">
					</div>
				</div>
				<div id="description3_3" style="float: left; width: 100%; display: none;">
					<div style="padding: 10px;">
						<span class="description-bar"></span>
					</div>
				</div>
			</div>
			<div style="float: left; width: 100%;">
				<div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
					<div id="graphs3_3text0000" class="gcircle-container">
						<span id="graphs3_3text000" style="font-size: 30px;"></span>
					</div>
				</div>
				<div style="float: left; width: 50%; font-size: 12px;">
					<div style="float: left; width: 100%;">
						<p><b>Result: <span id="graphs3_3text0"></span><br/>
						<span id="graphs3_3text00"></span></b></p>

					</div>
				</div>
			</div>
		</div>

	</div>
	<div style="float: left; width: 50%; padding: 10px 0px;">
		<div style="float: left; width: 98%; border: 1px solid grey;">
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title4" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Industry Comparison</b></div>
					<div style="float: right; padding-right: 10px;">
					<b>
					<span id="show4" style="display: block;">+</span>
					<span id="hide4" style="display: none;">-</span>
					</b>
					</div>
				</div>
				<div id="description4" style="float: left; width: 100%; display: none;">
					<div style="padding: 10px;">
						<span class="description-bar"></span>
					</div>
				</div>
			</div>
			<div style="float: left; width: 100%;">
				<div id="container-bar" style="min-width: 310px; height: 300px; max-width: 100%; margin: 0 auto"></div>
			</div>
		</div>
	</div>
	</div>
	@endif
	@endif
	</div>
</div>
@foreach ($entity as $info)
	{{ Form::hidden('gross_revenue_growth', $info->gross_revenue_growth, array('id' => 'gross_revenue_growth')) }}
	{{ Form::hidden('net_income_growth', $info->net_income_growth, array('id' => 'net_income_growth')) }}
	{{ Form::hidden('gross_profit_margin', $info->gross_profit_margin, array('id' => 'gross_profit_margin')) }}
	{{ Form::hidden('net_profit_margin', $info->net_profit_margin, array('id' => 'net_profit_margin')) }}
	{{ Form::hidden('net_cash_margin', $info->net_cash_margin, array('id' => 'net_cash_margin')) }}
	{{ Form::hidden('current_ratio', $info->current_ratio, array('id' => 'current_ratio')) }}
	{{ Form::hidden('debt_equity_ratio', $info->debt_equity_ratio, array('id' => 'debt_equity_ratio')) }}
@endforeach
@stop
@section('javascript')
	<script src="{{ URL::asset('js/chart-min.js') }}"></script>
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
@stop
