@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="table-responsive">
				<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Company</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Established</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Years in Business</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">City</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Analyst Responsible</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Processing Time</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($entity as $info)
						<tr class="odd" role="row">
							<td>{{ $info->entityid }}</td>
							<td>
                                <a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">
                                    @if (1 == $info->anonymous_report)
                                        Company - {{ $info->entityid }}
                                    @else
                                        {{ $info->companyname }}
                                    @endif
                                </a>
                            </td>
							<td>{{ $info->email }}</td>
							<td>{{ $info->date_established }}</td>
							<td>{{ $info->number_year }}</td>
							<td>{{ $info->row_title }}</td>
							<td>{{ $info->city }}</td>
							<td>
								@if ($info->status == 0)
									Active
								@elseif ($info->status == 1)
									Ready for investigation
								@elseif ($info->status == 2)
									Start investigation
								@elseif ($info->status == 3)
									On hold
								@elseif ($info->status == 4)
									Resume Investigator
								@elseif ($info->status == 5)
									<!-- Approved by investigator -->
									Ready for Risk Scoring
								@elseif ($info->status == 6)
									Risk Scoring in Progress
								@elseif ($info->status == 7)
									Risk Scoring Completed
								@elseif ($info->status == 8)
									Approved by the Bank
								@elseif ($info->status == 9)
									Deleted
								@else
									INVALID STATUS
								@endif
							</td>
							<td>{{ $info->firstname }} {{ $info->lastname }}</td>
							<td id="timer{{ $info->entityid }}" class="timer">{{ date('c', strtotime($info->created_at)) }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="server_time" value="{{ date('c') }}" />
@stop
@section('javascript')
<script type="text/javascript">
	var servertime = new Date($('#server_time').val());
	var clientime = new Date();
	var servertime_distance = servertime.getTime() - clientime.getTime();

	$(document).ready(function(){
		$('.timer').each(function(){
			CountDownTimer($(this).text(), $(this).attr('id'));
		});
	});

	function CountDownTimer(dt, id)
    {
        var start = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = now.getTime() - start.getTime() + servertime_distance;

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

			var display = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
            document.getElementById(id).innerHTML = display;

        }

        timer = setInterval(showRemaining, 1000);
    }
</script>
@stop
