@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="table-responsive">
				<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Company</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Established</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">City</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Score</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Completed</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Analyst Responsible</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Processing Time</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($entity as $info)
						<tr class="odd" role="row">
							<td>{{ $info->entityid }}</td>
							<td>{{ $info->companyname }}</td>
							<td>{{ $info->email }}</td>
							<td>{{ $info->date_established }}</td>
							<td>{{ $info->row_title }}</td>
							<td>{{ $info->city }}</td>
							<td><span style="color: {{ $info->rating_color }}; font-weight: bold;">{{ $info->score_sf }}</span></td>
							<td>{{ ($info->end_date!=null) ? date('m/d/Y', strtotime($info->end_date)) : ""}}</td>
							<td>{{ $info->firstname }} {{ $info->lastname }}</td>
							<td>{{ $info->process_time }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
