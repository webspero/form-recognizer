<div class="EntityThumb">
    <?php
        $columnCount = @count($entity);

        if ($columnCount > 12) {
            $mod = 4;
            $col = 'col-lg-3 col-md-6 col-sm-6';
            $leftCol = 'col-md-6 col-sm-6';
            $rightCol = 'col-md-6 col-sm-6';
            $hideIcons = 'hidden-xs';
            $showTable = 'visible-xs-block';
        } else if ($columnCount >= 6) {
            $mod = 3;
            $col = 'col-lg-4 col-md-6 col-sm-6';
            $leftCol = 'col-md-6 col-sm-6';
            $rightCol = 'col-md-6 col-sm-6';
            $hideIcons = 'hidden-xs';
            $showTable = 'visible-xs-block';
        } else {
            $mod = 2;
            $col = 'col-lg-5 col-md-6 col-sm-6';
            $leftCol = 'col-md-6 col-sm-6';
            $rightCol = 'col-md-6 col-sm-6';
            $hideIcons = 'hidden-xs';
            $showTable = 'visible-xs-block';
        }
    ?>

    @foreach ($entity as $row => $info)
        @if (($row + 1) % $mod === 1)
            <div class="row EntityThumb-row hidden-xs">
        @endif

        <!-- {{ Auth::user()->role == 4 && $info->is_paid == 1? 'unpaid-sme' : '' }} -->

        <!-- <div class="{{ $col }}" {{($info->is_premium == 1)?'style="pointer-events:none"':''}}> -->
        <div class="{{ $col }}" 
            @if($info->is_premium == 1 && $info->is_paid !=0 )
                style = "pointer-events: none;"
            @endif >
            
            <div class="row EntityThumb-item"
                
            >
                <div class="{{ $leftCol }} EntityThumb-item-pane">
                    <div class="EntityThumb-item-container">
                        @if($info->is_premium == 1)
                        <div>
                            <span id="p_link" value="{{$info->url}}"><span>
                        </div>
                        @endif
                        <div class="EntityThumb-item-summary">
                            @if ($info->is_paid != STS_OK)
                                New Report
                            @elseif ($info->status != 7)
                                Ongoing Report
                            @else
                                Report Finished
                                <?php
                                    if ($info->completed_date != null) {
                                        $date = new DateTime($info->completed_date);
                                        $date->modify('+8 hours');

                                        echo $date->format('m/d/Y - h:ia');
                                    }
                                ?>
                            @endif
                        </div>

                        <a @if (Auth::user()->role==1)
		                    href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}"
		                @elseif (Auth::user()->role == 4)
		                    href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}/ci_view"
		                @endif class="a-entity" >
                        <img class="img-responsive EntityThumb-item-icon"
                            @if ($info->status == 7)
                                src="{{ URL::to('images/sme_report_complete.png') }}"
                            @else
                                src="{{ URL::to('images/sme_report_editing.png') }}"
                            @endif
                        />
                    	</a>
                        
                        <div class="matching_checkbox">
							<input type="checkbox" class="matching" rel="{{ $info->entityid }}" name="matching" @php echo ($info->matching_optin == 'Yes') ? 'checked': '';  @endphp /> Matching
						</div>
						
                    </div>
                </div>

                <div class="{{ $rightCol }} EntityThumb-item-pane">
                    <div class="EntityThumb-detail">
                        <div class="EntityThumb-detail-row">
                            Company ID - Company Name
                        </div>
                        <div class="EntityThumb-detail-value">
                            @if (1 == $info->anonymous_report)
                                Company - {{ $info->entityid }}
                            @else
                                {{ $info->entityid }} - {{ $info->companyname }}
                            @endif
                        </div>
                        <div class="EntityThumb-detail-row">
                            SEC Registration Date
                        </div>
                        <div class="EntityThumb-detail-value">
                            {{ $info->sec_reg_date }}
                        </div>
                        <div class="EntityThumb-detail-row">
                            City
                        </div>
                        <div class="EntityThumb-detail-value">
                            {{ $info->citymunDesc }}
                        </div>
                        <div class="EntityThumb-detail-row">
                            Industry
                        </div>
                        <div class="EntityThumb-detail-value">
                            {{ $info->class_description }}
                        </div>
                        @if (Auth::user()->role != 1)
                            <div class="EntityThumb-detail-row">
                                Beneish M-Score
                            </div>
                            <div class="EntityThumb-detail-value">
                                {{ $info->m_score }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (((($row + 1) % $mod) == 0) || (@count($entity) === ($row + 1)))
            </div>
        @endif

    @endforeach
    @foreach($entity as $info)
    <a class="EntityPanel panel-link visible-xs-block"
        href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">
        <div class="panel panel-default">
            <div class="panel-body EntityPanel-body">
                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Company Name
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        @if (1 == $info->anonymous_report)
                            Company - {{ $info->entityid }}
                        @else
                            {{ $info->entityid }} - {{ $info->companyname }}
                        @endif
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Email
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        {{ $info->email }}
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Date Established
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        {{ $info->date_established }}
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Years in Business
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        {{ $info->number_year }}
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Industry
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        {{ $info->class_description }}
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        City
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        {{ $info->citymunDesc }}
                    </div>
                </div>

                <div class="row EntityPanel-row">
                    <div class="col-xs-5 EntityPanel-title">
                        Status
                    </div>
                    <div class="col-xs-7 EntityPanel-value">
                        @if ($info->status == 0)
                            Active
                        @elseif ($info->status == 1)
                            Ready for investigation
                        @elseif ($info->status == 2)
                            Start investigation
                        @elseif ($info->status == 3)
                            On hold
                        @elseif ($info->status == 4)
                            Resume Investigator
                        @elseif ($info->status == 5)
                            <!-- Approved by investigator -->
                            Ready for Risk Scoring
                        @elseif ($info->status == 6)
                            Risk Scoring in Progress
                        @elseif ($info->status == 7)
                            Risk Scoring Completed {{ ($info->completed_date!=null) ? date('m/d/Y', strtotime($info->completed_date)) : ""}}
                        @elseif ($info->status == 8)
                            Approved by the Bank
                        @elseif ($info->status == 9)
                            Deleted
                        @else
                            INVALID STATUS
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </a>
    @endforeach
</div>
