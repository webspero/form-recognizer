@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	
	<input type="hidden" name="rm_notify" id="rm_notify" value="{{ session('rm_notify') }}" />
    
    <div class="container center-error-message">
    	<br/>
    	<h2>We're sorry for the interruption! Please click the option below to log out. See you soon!</h2>
    	<div class="clear"></div>
    	<br/><br/>
    	<a href="{{ url('logout') }}" class="btn btn-primary big-btn">LOGOUT</a>
    </div>

</div>

@include('user-2fa-form')

<input type="hidden" id="server_time" value="{{ date('c') }}" />
@stop

@section('javascript')

	<script type="text/javascript">

		if($('#success').val()){
			$('#matching-success-modal').modal('show');
		}
		
		$(".matching").change(function() {

			var matching = 'No';
			if(this.checked){
				var matching = 'Yes';
			}
		     
		    window.location = "{{ URL::to('report_matching/optin/') }}/" + matching + '/' + $(this).attr('rel');

		});
	</script>

	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<script src="{{ URL::asset('js/tutorial.js') }}"></script>
		<script type="text/javascript">
			$(window).load(function(){
				Tutorial.init('step0');
			});
		</script>
	@endif
    <script>
    $(document).ready(function(){
        $('.unpaid-sme').click(function() {
            /** Open the Rating form modal */
            $('#unpaid-link-modal').modal('show');

            return false;
        });
    });
	</script>
	<script type="text/javascript">
	var servertime = new Date($('#server_time').val());
	var clientime = new Date();
	var servertime_distance = servertime.getTime() - clientime.getTime();

	$(document).ready(function(){
		$('.timer').each(function(){
			CountDownTimer($(this).text(), $(this).attr('id'));
		});
	});

	function CountDownTimer(dt, id)
    {
        var start = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = now.getTime() - start.getTime() + servertime_distance;

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

			var display = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
            document.getElementById(id).innerHTML = display;

        }

        timer = setInterval(showRemaining, 1000);
    }
</script>
    <script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>

    <script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
	<script src="{{ URL::asset('js/chart-min.js') }}"></script>
	<script src="{{ URL::asset('js/creditlines.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>

@stop
@section('style')
	@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
		<link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"></link>
	@endif
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard/entity.css') }}" media="screen"></link>
@stop
