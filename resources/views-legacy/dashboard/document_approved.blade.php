@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="table-responsive">
				<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Report ID</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Completed</th>
						</tr>
					</thead>
					<tbody>
						@foreach($entity as $info)
						<tr>
                        	<td> {{$info->entityid}} </td>
                        	<td> {{$info->companyname}}</td>
							@if($info->status==0)
								<td> Denied </td>
							@else
								<td> Approved </td>
							@endif
                        	<td> {{date("m-d-Y", strtotime($info->created_at))}} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="server_time" value="{{ date('c') }}" />
@stop
@section('javascript')
<script type="text/javascript">
	var servertime = new Date($('#server_time').val());
	var clientime = new Date();
	var servertime_distance = servertime.getTime() - clientime.getTime();

	$(document).ready(function(){
		$('.timer').each(function(){
			CountDownTimer($(this).text(), $(this).attr('id'));
		});
	});

	function CountDownTimer(dt, id)
    {
        var start = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = now.getTime() - start.getTime() + servertime_distance;

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

			var display = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
            document.getElementById(id).innerHTML = display;

        }

        timer = setInterval(showRemaining, 1000);
    }
</script>
@stop
