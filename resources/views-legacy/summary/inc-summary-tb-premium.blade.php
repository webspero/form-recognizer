
<div class="spacewrapper">
 
    <span class="details-row" style="text-align: center; border: 0px;">
    {{ Form::hidden('entityid', $entity->is_premium, array('id' => 'report_type')) }}
    @if ($entity->status == 0 && $entity->status != 7)

    	{{ Form::open(['route' => ['postFinancialAnalysis', $entity->entityid], 'method' => 'post']) }}

    	@php
    		$ec = 0;
	    	foreach ($biz1['counter'] as $key => $val){
	    		
		       if (0 >= $val){
		            if(in_array($key, $req)){
		           		$ec++; 		
		            
		        	}
		        }
		           
		  	}
	    @endphp
			<button id="credit_risk_rating_submit" type="submit" 
			@php
				if($ec > 0){
					echo 'disabled="disabled"';
				}
			@endphp
			 >{{trans('messages.submit_for_financial_analysis')}}</button>
			<!-- <button id="credit_financial_analysis" type="submit">{{trans('messages.submit_for_financial_analysis')}}</button> -->
		{{ Form::close() }}
        <!-- <form id="form_submit" method="post" action="{{
         url('summary/smesummary/'.$entity->entityid.'/pci/fa') }}">
            @if ($msgOk == 1)
            <input type="submit"  class="btn btn-success" id="submit_exit" value="{{trans('messages.submit_for_credit_risk_rating')}}"/>
            @else
            <input type="button"  class="btn btn-danger" id="submit_exit" value="Fill in Required Fields" disabled="disabled"/>
            @endif
        </form> -->
    @endif
    </span>
</div>

<div class="spacewrapper">
    @if ($msgOk == 0)
    <h4 class = 'errormessage'> {{ $msg }} </h4>
    @else
    <h4 class = 'successmessage'> {{ $msg }} </h4>
    @endif

</div>
@if (0 >= $comp)
<div class = 'read-only' id="errordivpremium">
    <h4> {{ trans('business_details1.business_details') }} I </h4>

    <div class = 'read-only-text'>
    @foreach ($biz1['counter'] as $key => $val)

        @if (!in_array($key, $hide))
        @if (0 >= $val)
            @if (in_array($key, $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
            @else
            <span class = 'details-row'>
            @endif
        @endif
            @if (0 >= $val)
                {{ trans('business_details1.'.$key) }}
            @endif

            </span>
        @endif
    @endforeach
    </div>
</div>
@endif

<script src="{{ URL::asset('js/smesummary_progressbar.js') }}"></script>
<script type="text/javascript">
	
    $(document).ready(function() {
        require(['/static/js/modules/smesummary/index.js'], function(smesummary) {
            smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
        });
        // $("#submit_exit").on("click", function(){
        //     $("#form_submit").submit();
        // }) 
    });
</script>