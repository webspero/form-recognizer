<div class="spacewrapper">

    <h4 class = 'errormessage'> {{ $msg }} </h4>

</div>
@if (0 >= $comp)
<div class = 'read-only'>
    <h4> {{ trans('business_details1.business_details') }} I </h4>

    <div class = 'read-only-text'>
    @foreach ($biz1['counter'] as $key => $val)

        @if (!in_array($key, $hide))
        @if (0 >= $val)
            @if (in_array($key, $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
            @else
            <span class = 'details-row'>
            @endif
        @endif

            @if ($key == 'fs_template')
                @if (0 >= $val)
                {{ trans('business_details1.fs_must_be_uploaded') }}
                @endif
            @elseif ($key == 'balance_sheet')
                @if (4 > $val)
                @if ($val > 0)
                <span class = 'details-row'>
                @endif
                {{ 4 - $val }} more {{ trans('business_details1.'.$key) }} Period(s) is ideal
                @endif
            @elseif ($key == 'income_statement')
                @if (3 > $val)
                @if ($val > 0)
                <span class = 'details-row'>
                @endif
                {{ 3 - $val }} more {{ trans('business_details1.'.$key) }} Period(s) is ideal
                @endif
            @elseif (($key == 'bank_statement') || ($key == 'utility_bill'))
                @if (3 > $val)
                @if ($val > 0)
                @if (in_array($key, $req))
                <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
                @else
                <span class = 'details-row'>
                @endif
                @endif
                {{ 3 - $val }} more {{ trans('business_details1.'.$key) }}
                @endif
            @elseif (0 >= $val)
                {{ trans('business_details1.'.$key) }}
            @endif

            </span>
        @endif
    @endforeach

    @foreach ($custom_docs as $custom)
        <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ $custom }} </span>
    @endforeach
    </div>
</div>

@if (Auth::user()->is_contractor == 1)
@if($is_major == 0 || $machineEquipCheck == 0)
    <div class='read-only'>
        <h4>{{trans('business_details1.business_details')}} II</h4>
        <div class='read-only-text'>
            @foreach($biz2['counter'] as $key => $val)
                @if(!in_array($key, $hide))
                    @if( 0 >= $val)
                        @if(in_array($key, $req))
                            <span class='details-row' style='color:red; font-weight: bold;'> *
                            @if($key == 'major_supplier')
                                @if( 0 >= $val)
                                    Major Supplier
                                @endif
                            @elseif($key == 'major_customer')
                                @if( 0 >= 'major_customer')
                                    Major Customer
                                @endif 
                            @elseif($key == 'machine_equipment')
                                @if( 0 >= 'machine_equipment')
                                    Machine and Equipments
                                @endif 
                            @endif
                        @endif
                    @endif
                @endif
            @endforeach
        </div>
    </div>
@endif
@if (!$assocFilesCheck)
<div class = 'read-only'>
    <h4> Associated Files </h4>

    <div class = 'read-only-text'>
    @foreach ($assocFiles['counter'] as $key => $val)
        @if (0 >= $val)
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * Section 

            @switch ($key)
                @case("a1")
                A.1.
                @break
                @case("a1_1")
                A.1.1.
                @break
                @case("a2")
                A.2.
                @break
                @case("b1")
                B.1.
                @break
                @case("c1_1")
                C.1.1.
                @break
                @case("c1_1_1")
                C.1.1.1.
                @break
                @case("c1_1_2")
                C.1.1.2.
                @break
                @case("c1_1_3")
                C.1.1.3.
                @break
                @case("c1_1_4")
                C.1.1.4.
                @break
                @case("c1_1_5")
                C.1.1.5.
                @break
                @case("c1_2_1")
                C.1.2.1.
                @break
                @case("c1_2_2")
                C.1.2.2.
                @break
                @case("c1_2_3")
                C.1.2.3.
                @break
                @case("c1_2")
                C.1.2
                @break
                @case("c2")
                C.2.
                @break
                @case("c2_1_1")
                C.2.1.1.
                @break
                @case("c2_1_2")
                C.2.1.2.
                @break
                @case("c2_1_3")
                C.2.1.3.
                @break
                @case("c2_1_4")
                C.2.1.4.
                @break
                @case("c2_1_5")
                C.2.1.5.
                @break
                @case("c2_2_1")
                C.2.2.1.
                @break
                @case("c2_2_2")
                C.2.2.2.
                @break
                @case("c2_2_3")
                C.2.2.3.
                @break
                @case("d1")
                D.1.
                @break
                @case("d2")
                D.2.
                @break
                @case("d3")
                D.3.
                @break
                @case("e1")
                E.1.
                @break
                @case("e2")
                E.2.
                @break
                @default
                @break
            @endswitch

            </span>
        @endif
    @endforeach

    @foreach ($custom_docs as $custom)
        <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ $custom }} </span>
    @endforeach
    </div>
</div>
@endif
@endif

@if($bank_standalone==0)
<div class = 'read-only'>
    <h4> {{ trans('business_details1.business_details') }} II </h4>

    <div class = 'read-only-text'>
    @foreach ($biz2['counter'] as $key => $val)
        @if (!in_array($key, $hide))
            @if (0 >= $val)
                @if (in_array($key, $req))
                <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
                @else
                <span class = 'details-row'>
                @endif
                @if ('major_supplier' == $key)
                    3 more top {{ trans('business_details2.'.$key) }} is required
                @else
                    {{ trans('business_details2.'.$key) }}
                @endif
                </span>
            @else
                @if ('major_supplier' == $key)
                    @if (3 > $val)
                        <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
                            {{ 3 - $val }} more top {{ trans('business_details2.'.$key) }} is required
                        </span>
                    @endif
                @endif
            @endif
        @endif
    @endforeach
    </div>
</div>

<div class = 'read-only'>
    <h4> {{ trans('messages.owner_details') }} / Key Managers </h4>

    <div class = 'read-only-text'>
        @if (0 >= $owner['counter']['owner_inf'])
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * Owner Details </span>
        @endif
        @if (0 >= $owner['counter']['key_manager'])
            @if (in_array('key_manager', $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * Key Manager </span>
            @else
            <span class = 'details-row'> Key Manager </span>
            @endif
        @endif
        @if (0 >= $owner['counter']['owner_educ'])
            <span class = 'details-row'> Owner Education Background </span>
        @endif
        @if (0 >= $owner['counter']['owner_wife'])
            <span class = 'details-row'> Spouse of Owner </span>
        @endif
        @if (0 >= $owner['counter']['wife_educ'])
            <span class = 'details-row'> Spouse Educational Background </span>
        @endif
        @if (!in_array('shareholder', $hide))
        @if (0 >= $owner['counter']['shareholder'])
            @if (in_array('shareholder', $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ trans('owner_details.shareholder') }} </span>
            @else
            <span class = 'details-row'> {{ trans('owner_details.shareholder') }} </span>
            @endif
        @endif
        @endif
        @if (!in_array('director', $hide))
        @if (0 >= $owner['counter']['director'])
            @if (in_array('director', $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ trans('owner_details.director') }} </span>
            @else
            <span class = 'details-row'> {{ trans('owner_details.director') }} </span>
            @endif
        @endif
        @endif
    </div>
</div-->

@if (!in_array('credit_facilities', $hide))
@if (NULL != $ecf)
<div class = 'read-only'>
    <h4> {{ trans('ecf.ecf') }} </h4>
    <div class = 'read-only-text'>
    @foreach ($ecf['counter'] as $key => $val)
        @if (0 >= $val)
            @if (in_array('credit_facilities', $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ trans('ecf.'.$key) }} </span>
            @else
            <span class = 'details-row'> {{ trans('ecf.'.$key) }} </span>
            @endif
        @endif
    @endforeach
    </div>
</div>
@endif
@endif

<div class = 'read-only'>
    <h4> {{ trans('condition_sustainability.condition_sustainability') }} </h4>

    <div class = 'read-only-text'>
    @foreach ($cns['counter'] as $key => $val)
        @if (!in_array($key, $hide))
        @if (0 >= $val)
            @if (in_array($key, $req))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
            @else
            <span class = 'details-row'>
            @endif
            {{ trans('condition_sustainability.'.$key) }}
            </span>
        @endif
        @endif
    @endforeach
    </div>
</div>

@if (!in_array('requested_cf', $hide))
@if (NULL != $rcl)
<div class = 'read-only'>
	<h4> {{ trans('rcl.rcl') }} </h4>

    <div class = 'read-only-text'>
		@foreach ($rcl['counter'] as $key => $val)
			@if (0 >= $val)
                @if (in_array('requested_cf', $req))
                <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ trans('rcl.'.$key) }} </span>
                @else
                <span class = 'details-row'> {{ trans('rcl.'.$key) }} </span>
                @endif
			@endif
		@endforeach
    </div>
</div-->
@endif
@endif

@endif
@endif
