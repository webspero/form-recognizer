<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

<style>
    body { margin: 14pt; font-size: 13px; color: #000000; font-family: Arial, Helvetica, sans-serif; }
    h1 { font-size: 20pt; text-align: center; }
    h2 { font-size: 17pt; text-align: center; }
    h3 { font-size: 14pt; text-align: center; }
    table { width: 100%; border-collapse: collapse; border-top: 1px solid #000; border-left: 1px solid #000;  }
    table td { border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 2px 5px; }
    .center { text-align: center; }
    .green { color: GREEN; }
    .red { color: RED; }
    .black { color: #000; }
    
    
    #graphs0value00000 {
        font-size: 20px !important;
    }
    
    .circle-container {
        border: 10px solid red;
        border-radius: 62px;
        color: red;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }
    
    .gcircle-container {
        border: 10px solid green;
        border-radius: 62px;
        color: green;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }
    
    .ocircle-container {
        border: 10px solid orange;
        border-radius: 62px;
        color: orange;
        height: 80px;
        line-height: 0;        
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }
    
    .hundred-percent {
        width: 100%;
        display: block;
        margin: 20px
    }
    
    .seventy-percent {
        width: 60%;
        display: inline-block;
        margin: 20px;
    }
    
    .fifty-percent {
        width: 45%;
        display: inline-block;
        margin: 10px;
    }
    
    .thirty-percent {
        width: 20%;
        display: inline-block;
        margin: 20px;
    }
    
    .break-page {
        page-break-after: always;
    }
    
    .last-break-page {
        page-break-inside: avoid !important;
        float: none !important;
    }
    
    .pdf-hdr-logo {
        width: auto;
        height: 60px;
    }
    
    table td.tb_hdr {
        background-color: #055688;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }
    
    .ind-legend {
        text-align: center;
        margin-bottom: 10px;
    }
    
    .ind-legend .legend-item {
        display: inline-block;
        width: 70px;
        margin: 0 5px;
    }
    
    .ind-legend .legend-item .legend-info {
        height: 10px;
        width: 10px;
        background-color: #BDCF8E;
        display: inline-block;
    }
    
    .ind-legend .legend-item .industry {
        background-color: #F9967C;
    }
    
    .title3_3 {
        font-size: 20px;
    }
</style>

</head>

<body>
    
	<div id  = 'rating-summary-main' class = 'container'>
        
        <div class = 'break-page'>
            <div>
                <img src="{{ URL::asset('images/creditbpo-logo-big.png') }}" width="100%" style="margin-bottom:5px;" />
            </div>   
            <div style="margin-top:60px;font-size:18px; text-align: center;"> Debt Service Capacity for {{ $entity->companyname }}</div>
            <div><hr/></div>
        </div>


            <div id = 'debt-service-standalone' class="break-page">
                <div class="col-lg-12 col-sm-12">
                    <div class="chart-container">
                        <div style="padding: 5px 0px; width: 100%; height: 35px">
                            <div id="title4" style="float: left; width: 100%;">
                                <div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>If you do decide to grant a loan to this company, we have safe (Debt Service Capacity) estimated safe loan terms here:</b></div>
                            </div>
                        </div>
                        
                        <div>
                            <div style="width: 45%; display: inline-block;">
                                <label> DSCR: 1.20x</label>
                            </div>
                            
                            <div style="width: 45%; display: inline-block;">
                                <label> Total Loan Duration: {{$dscr_standalone['months_cntr']}} months</label>
                            </div>
                        </div>
                        
                        <div>
                            <div style="width: 45%; display: inline-block;">
                                <label>NOI (Net Operating Income): {{ number_format($dscr_standalone['noi'], 2) }}</label>
                            </div>
                            
                            <div style="width: 45%; display: inline-block;">
                                <label> {{ trans('rcl.loan_amount') }}: {{ number_format($dscr_standalone['la'], 2) }}</label>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <table class="table table-striped table-bordered" style="margin-top: 20px;">
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="3" align="center"><b>{{ trans('rcl.working_capital_2') }}</b></td>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>{{ trans('rcl.interest_rate') }}</td>
                                <td>{{ trans('rcl.period') }}</td>
                                <td>{{ trans('rcl.payment_per_period') }}</td>
                            </tr>

                            <tr>
                                <td><b>{{ trans('rcl.local_banks') }}</b></td>
                                <td>{{ number_format($dscr_standalone['dscr_data'][0], 2, '.', ',') }}</td>
                                <td>{{ $dscr_standalone['dscr_result'][0] }}</td>
                                <td>{{ $dscr_standalone['dscr_result'][1] }}</td>
                            </tr>
                            
                            <tr>
                                <td><b>{{ trans('rcl.foreign_banks') }}</b></td>
                                <td>{{ number_format($dscr_standalone['dscr_data'][1], 2, '.', ',') }}</td>
                                <td>{{ $dscr_standalone['dscr_result'][2] }}</td>
                                <td>{{ $dscr_standalone['dscr_result'][3] }}</td>
                            </tr>
                            
                        </table>
                        
                        <small>*{{ trans('rcl.dscr_hint') }}</small>
                        
                        <table class="table table-striped table-bordered" style="margin-top: 20px;">
                            <tr>
                                <td>&nbsp;</td>
                                <td>{{ $dscr_standalone['fr_data']['financial_report']->year }}</td>
                                <td>{{ $dscr_standalone['fr_data']['financial_report']->year-1 }}</td>
                            </tr>
                            
                            <tr>
                                <td>Trade and other Current Receivables</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->TradeAndOtherCurrentReceivables, 2) }} </td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->TradeAndOtherCurrentReceivables, 2) }} </td>
                            </tr>
                            
                            <tr>
                                <td>Trade and other Current Payables</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->TradeAndOtherCurrentPayables, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->TradeAndOtherCurrentPayables, 2) }}</td>
                            </tr>
                            
                            <tr>
                                <td>Current Provisions for Employee Benefits</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->CurrentProvisionsForEmployeeBenefits, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->CurrentProvisionsForEmployeeBenefits, 2) }}</td>
                            </tr>
                            
                            <tr>
                                <td>Cost of Sales</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->CostOfSales, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->CostOfSales, 2) }}</td>
                            </tr>
                            
                            <tr>
                                <td>Inventories</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->Inventories, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->Inventories, 2) }}</td>
                            </tr>
                            
                            <tr>
                                <td>Revenue</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->Revenue, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->Revenue, 2) }}</td>
                            </tr>
                            
                            <tr>
                                <td>Interest Expense</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][0]->InterestExpense, 2) }}</td>
                                <td>{{ number_format($dscr_standalone['fr_data']['balance_sheets'][1]->InterestExpense, 2) }}</td>
                            </tr>
                        </table>
                        
                        <div>
                            <p>
                                <small>
                                    Notes: <br/><br/>
                                    This is entirely dependant on the accuracy of provided financial statements. Feel free to use them as a starting point.
                                    <br/><br/>
                                    These terms are ideal for Working Capital loan as they are based off of the company's ability to service debt within a single operational period.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>