
@if (STS_NG == $sme_form_complete)
<h4 class = 'errormessage'> {!! $msg !!} </h4>

<div class = 'read-only'>

    <div class = 'read-only-text'>
    @foreach ($unfilled as $key => $val)
        @if (!in_array($key, $hidden))
        @if (0 >= $val)
            @if (in_array($key, $required))
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
            @else
            <span class = 'details-row'>
            @endif
        @else
            @if ('major_supplier' == $key)
                @if (3 > $val)
                    <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
                        {{ 3 - $val }} more top {{ trans('business_details2.'.$key) }} is required
                    </span>
                @endif
            @endif
        @endif
                @if ($key == 'fs_template')
                    @if (0 >= $val)
                    {{ trans('business_details1.fs_must_be_uploaded') }}
                    @endif
                @elseif ($key == 'balance_sheet')
                    @if (4 > $val)
                    @if ($val > 0)
                    <span class = 'details-row'>
                    @endif
                    {{ 4 - $val }} more {{ trans($trans_file.'.'.$key) }} Period(s) is ideal
                    @endif
                @elseif ($key == 'income_statement')
                    @if (3 > $val)
                    @if ($val > 0)
                    <span class = 'details-row'>
                    @endif
                    {{ 3 - $val }} more {{ trans('business_details1.'.$key) }} Period(s) is ideal
                    @endif
                @elseif (($key == 'bank_statement') || ($key == 'utility_bill'))
                    @if (3 > $val)
                    @if ($val > 0)
                    @if (in_array($key, $required))
                    <span class = 'details-row' style = 'color: red; font-weight: bold;'> *
                    @else
                    <span class = 'details-row'>
                    @endif
                    @endif
                    {{ 3 - $val }} more {{ trans('business_details1.'.$key) }}
                    @endif
                @elseif ('major_supplier' == $key)
                    @if (0 >= $val)
                    3 more top {{ trans('business_details2.'.$key) }} is required
                    @endif
                @elseif (0 >= $val)
                    {{ trans($trans_file.'.'.$key) }}
                @endif
            </span>
        @endif
    @endforeach

    @if (@count($custom_docs) > 0)
        @foreach ($custom_docs as $custom)
            <span class = 'details-row' style = 'color: red; font-weight: bold;'> * {{ $custom }} </span>
        @endforeach
    @endif
    </div>
	<div class="clearfix"></div>
</div>
@endif
