<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>

<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

<style>
    @page { margin: 100 25px; }
    body { margin: 14pt; font-size: 13px; color: #000000; font-family: Arial, Helvetica, sans-serif; }
    h1 { font-size: 20pt; text-align: center; }
    h2 { font-size: 17pt; text-align: center; }
    h3 { font-size: 14pt; text-align: center; }
    table { width: 100%; border-collapse: collapse; border-top: 1px solid #000; border-left: 1px solid #000;}
    table td { border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 2px 5px; }
    table th { border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 2px 5px; }
    .center { text-align: center; }
    .green { color: GREEN; }
    .red { color: RED; }
    .black { color: #000; }


    #graphs0value00000 {
        font-size: 20px !important;
    }
    
    .header { position: fixed; top: -80px; left: 20px; right: 15px; bottom: 10px }

    .annexA>td,th {
        width: 20%;
    }

    .avoidPageBreak {
        page-break-inside: avoid;
    }
    
    .mainLocation {
        margin-bottom: 25px;
    }

    .mainLocation table {
        width: 100%;
    }

    .mainLocation tr td:first-child {
        width: 75%;
        white-space: normal;
    }

    .negativeFindings {
        padding-top: 5px;
    }
    
    .negativeFindings table {
        white-space: normal!important;
    }
    
    .negativeFindings td, th{
        word-wrap: break-word;
    }

    .cmapSearch tr td:nth-child(1) {
        width: 30%;
    }

    .cmapSearch tr td:nth-child(2) {
        width: 30%;
    }

    .keyPerson td:nth-child(1) {
        text-align: center;
    }

    .keyPerson tr td:nth-child(2) {
        width: 80%;
        white-space: normal;
    }

    .grid-container {
        display: grid;
        grid-template-columns: auto auto;
        background-color: #2196F3;
        padding: 10px;
}

    .grid-item-label {
        text-align: center;
    }

    .grid-item {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid rgba(0, 0, 0, 0.8);
        padding: 20px;
        font-size: 30px;
        text-align: center;
     }

    /* .grid-item {
        text-align: left;
    } */

    .circle-container {
        border: 10px solid red;
        border-radius: 62px;
        color: red;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .gcircle-container {
        border: 10px solid green;
        border-radius: 62px;
        color: green;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .ocircle-container {
        border: 10px solid orange;
        border-radius: 62px;
        color: orange;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .hundred-percent {
        width: 100%;
        display: block;
        margin: 20px
    }

    .seventy-percent {
        width: 60%;
        display: inline-block;
        margin: 20px;
    }

    .fifty-percent {
        width: 45%;
        display: inline-block;
        margin: 10px;
    }

    .thirty-percent {
        width: 20%;
        display: inline-block;
        margin: 20px;
    }

    .break-page {
        page-break-after: always;
    }

    .last-break-page {
        page-break-inside: avoid !important;
        float: none !important;
    }

    .pdf-hdr-logo {
        width: auto;
        height: 60px;
    }

    table td.tb_hdr {
        background-color: #055688;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }

    .ind-legend {
        text-align: center;
        margin-bottom: 10px;
    }

    .ind-legend .legend-item {
        display: inline-block;
        width: 70px;
        margin: 0 5px;
    }

    .ind-legend .legend-item .legend-info {
        height: 10px;
        width: 10px;
        background-color: #98d027;
        display: inline-block;
    }

    .ind-legend .legend-item .industry {
        background-color: #aaaaaa;
    }

    .title3_3 {
        font-size: 20px;
    }
    
    .title3_4 {
        font-size:15px;
    }

    .hideChart {
        display : none;
    }
</style>
</head><body>
   
   @php

   $is_deliverables = 1;

   @endphp
   
    <div class="break-page">
        <?php
            if($bank->isLabel == 1){
                $organization_name = $bank->white_label_name;
            }else{
                $organization_name = "CreditBPO";
            }
        ?>
        @if($bank->isLabel == 1)
            <div>
                <img src="{{ URL::to('/') }}/images/brand_images/{{$bank->brand_img}}" width="100%" style="margin-bottom:5px;" />
                <br/>
                <p style="text-align:center;"><img src="{{ URL::to('/') }}/images/powered-by-cbpo-logo.png" alt=".CreditBPO" style="width: auto; height: 40px; margin-bottom:5px;"/></p>
            </div>
        @else
            <div>
                <img src="{{ URL::asset('images/creditbpo-logo-big.png') }}" width="100%" style="margin-bottom:5px;" />
            </div>
        @endif
        <div style="margin-top:60px;font-size:18px; text-align: center;"> {{trans('rating_summary.ratingsummary')}} {{ $entity->companyname }}</div>
        <div><hr/></div>
        <div style="text-align: center; font-size: 16px;">
            {{trans('rating_summary.report_generated')}}: {{ $date_created }}
        </div>
    </div>
    
    <div class="header">
        <div>
            @if($bank->isLabel == 1)
                <img class = 'pdf-hdr-logo' src="{{ URL::to('/') }}/images/brand_images/{{$bank->brand_img}}" style="margin-bottom:5px;" /><br>
                <img src="{{ URL::to('/') }}/images/powered-by-cbpo-logo.png" alt=".CreditBPO" style="width: auto; height: 20px; margin-bottom:5px;"/>
            @else
                <img class = 'pdf-hdr-logo' src="{{ URL::asset('images/creditbpo-logo-2.jpg') }}" style="margin-bottom:5px;" />
            @endif
        </div>
        <div> <hr/> </div>
    </div>

	<div id  = 'rating-summary-main' class = 'container'>
        <div class = 'break-page'>
            <table style="border: 0px; margin-left: -3px;">
                @if($bank->bank_name == "Matching Platform")
                @else
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.sec_company_reg_no')}}: </b></td>
                        <td style="text-align: left; border: 0px;">
                                {{ $entity->tin_num }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.sec_reg_date')}}: </b></td>
                        <td style="text-align: left; border: 0px;">
                            @php  
                                $sec_reg_date = date_create($entity->sec_reg_date);
                                echo date_format($sec_reg_date,"m/d/Y");
                            @endphp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.company_tin')}}: </b></td>
                        <td style="text-align: left; border: 0px;">
                            {{  $entity->company_tin }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.business_address_pr')}}: </b></td>
                        <td style="text-align: left; border: 0px;">
                            {{  $entity->address1 }}, @php
                                echo !empty($entityCity) ? $entityCity.', ':''; 
                                echo !empty($entity->province) ? $entity->province.', ':'';
                                echo !empty($zipcode) ? $zipcode:'';
                            @endphp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.telephone_number')}}:</b></td>
                        <td style="text-align: left; border: 0px;">
                                {{ $entity->phone }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.company_email')}}:</b></td>
                        <td style="text-align: left; border: 0px;">
                                {{  $entity->email }}
                        </td>
                    </tr>
                    @if ($entity->website)
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.company_website')}}:</b> </td>
                        <td style="text-align: left; border: 0px;">
                                {{  $entity->website }}
                        </td>
                    </tr>
                    @endif
                @endif
            </table><br>
          
            
            <span><b>{{trans('rating_summary.company_profile')}}: </b></span><br>
            @if($entity->description)
           <div style="margin-left: 31px; text-align: justify;"><span>{{ $entity->description }}</span><br><br></div>
           @else
           <div style="margin-left: 31px; text-align: justify;"><span>{!! trans('rating_summary.company_description_data') !!}</span><br><br></div>
           @endif

            <span><b>{{trans('rating_summary.industry_classification')}}:</b></span><br>
                <table style="border: 0px; width: 80%; margin-left: 26px;">
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;">{{trans('rating_summary.industry_main')}}: </td>
                        <td style="text-align: left; border: 0px;">{{ $industry->main_title }} </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px; width: 25%;"Industry >{{trans('rating_summary.industry_sub')}}: </td>
                        <td style="text-align: left; border: 0px;">{{ !empty($ind_sub->sub_title) ? $ind_sub->sub_title: ''  }} </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;">{{trans('rating_summary.industry')}}: </td>
                        <td style="text-align: left; border: 0px;">{{ !empty($ind_row->group_description) ? $ind_row->group_description: ''  }}</td>
                    </tr>
                </table><br>

            
                <table style="border: 0px; margin-left: -3px;">
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.asset_size')}}:</b></td>
                            <td style="text-align: left; border: 0px;">PHP {{ number_format((float)$entity->total_assets, 2, '.', ',') }} (@php echo !empty($assetClassification) ? $assetClassification:''; @endphp)</td>
                        </tr>

                        @if(!empty($entity->number_year_management_team))
	                        <tr>
	                            <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.years_of_managerial')}}:</b></td>
	                            <td style="text-align: left; border: 0px;">{{  $entity->number_year_management_team }}</td>
	                        </tr>
                        @endif
                        
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.employee_size')}}:</b></td>
                            <td style="text-align: left; border: 0px;">
                            	
                            	@if(!empty($employee_size))
	                            	@if(empty($entity->employee_size))
										@if ($entity->total_asset_grouping == 4)
										{{trans('business_details1.employee_size_0')}}
										@elseif ($entity->total_asset_grouping == 1)
											{{trans('business_details1.employee_size_1')}}
										@elseif ($entity->total_asset_grouping == 2)
											{{trans('business_details1.employee_size_2')}}
										@else
											{{trans('business_details1.employee_size_3')}}
										@endif
									@else

										@if (1 == $entity->employee_size)
										{{trans('business_details1.employee_size_1')}}
										@elseif (2 == $entity->employee_size)
											{{trans('business_details1.employee_size_2')}}
										@elseif (3 == $entity->employee_size)
											{{trans('business_details1.employee_size_3')}}
										@else
											{{trans('business_details1.employee_size_0')}}
										@endif

									@endif
								@endif
                            </td>
                        </tr>
                        {{-- @if(!$entity->negative_findings and !$cmapSearch)
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>{{trans('rating_summary.negative_findings')}}:</b></td>
                            <td style="text-align: left; border: 0px;">No Negative Finding Found.</td>
                        </tr>
                        @endif --}}
                </table>
            
                <div class="avoidPageBreak">

                    @if(!empty($majorLocations))
                        @if (count($majorLocations) != 0) 
                        <span><b>{{trans('rating_summary.major_market_location')}}: </b></span><br>
                                <ul style="list-style-type:none; margin-top: 2px; margin-left: -9px;">
                                @foreach($majorLocations as $majorLocation)
                                 <li>{{ $majorLocation->citymunDesc }}</li>
                                @endforeach
                                </ul>
                        @endif
                    @endif
                </div>
                @if(!empty($majorLocations))
                    @if(count($customerSegment) != 0)
                    <span><b>{{trans('rating_summary.customer_segment')}}:</b></span><br>
                        <ul style="list-style-type:none; margin-top: 2px; margin-left: -9px;">
                        @foreach($customerSegment as $custSeg)
                         <li>{{ $custSeg->businesssegment_name }}</li>
                        @endforeach
                        </ul>
                    @endif
                @endif
        </div>
        @if (!empty($cmapSearch) || $entity->negative_findings)
        <div class="break-page">
            @if(!empty($cmapSearch))
                <div class="negativeFindings">
                    @foreach ($cmapSearch as $tables)
                <div class="avoidPageBreak {{ $tables[0] == "court_cases" ? "cmapSearch" : "" }}">
                        <b>
                            @switch($tables[0])
                                @case("court_cases")
                                    {{trans('rating_summary.court_cases')}}:
                                @break
                                @case("accounts_referred_to_lawyers")
                                    {{trans('rating_summary.account_reffered_lawyer')}}:
                                @break
                                @case("returned_cheques")
                                    {{trans('rating_summary.return_cheques')}}
                                @break
                                @case("telecom_data")
                                    {{trans('rating_summary.telecom_data')}}:
                                @break
                                @default
                            @endswitch
                        </b>
                            <table>
                                <thead>
                                    <tr>
                                        @foreach($tables[1] as $key => $th)
                                            <th>{{ $th }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                    @foreach($tables[2] as $cases)
                                    <tr>
                                        @foreach($cases as $key2 => $td)
                                            <td> {{ $td == "&nbsp;" ? '' : $td }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                            </table>
                        </div>
                        <br>
                    @endforeach
                </div>
            @endif
            @if($entity->negative_findings)
                <div class="avoidPageBreak negativeFindings keyPerson">
                    <b>{{trans('rating_summary.key_person')}}:</b>
                    {!! $entity->negative_findings !!}<br><br></div>
            @endif
        </div>
        @endif
        @if ($entity->is_premium == 1)
        @if (!empty($capitalDetails) || (!empty($certifications)) || ( !empty($insurances)) || (!empty($competitors)) || (!empty($relatedCompanies)))
        <div class="break-page">
            @if ($capitalDetails != null) 
            <span><b>{{trans('rating_summary.capital_details')}}:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.authorized_capital')}}</th>
                    <th>{{trans('rating_summary.issued_capital')}}</th>
                    <th>{{trans('rating_summary.paid_up_capital')}}</th>
                    <th>{{trans('rating_summary.ordinary_shares')}}</th>
                    <th>{{trans('rating_summary.par_value')}}</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        
                            <td> {{ number_format((float)$capitalDetails->capital_authorized, 2, '.', ',') }} </td>
                            @if ($capitalDetails->capital_issued != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_issued, 2, '.', ',') }}  </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_paid_up != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_paid_up, 2, '.', ',') }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_ordinary_shares != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_ordinary_shares, 2, '.', ',') }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_par_value != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_par_value, 2, '.', ',') }} </td>
                            
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                </tbody>
                
              </table><br>
            @endif

            @if (count($certifications) != 0) 
            <span><b>{{trans('rating_summary.cert_accre_tax')}}:</b> </span> 
            <table class="table">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.cert_details')}}</th>
                    <th>{{trans('rating_summary.reg_num')}}</th>
                    <th>{{trans('rating_summary.reg_date')}}</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($certifications as $certification)
                    <tr>
                            <td> {{ $certification->certification_details }} </td>
                            <td> {{ $certification->certification_reg_no }}  </td>
                            <td> {{ $certification->certification_reg_date }} </td>
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($insurances) != 0) 
            <span><b>{{trans('rating_summary.insurance_coverage')}}:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.insurance_type')}}</th>
                    <th>{{trans('rating_summary.insurance_amt')}}</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($insurances as $insurance)
                    <tr>
                            <td> {{ $insurance->insurance_type }} </td>
                            <td>PHP {{ number_format((float)$insurance->insured_amount, 2, '.', ',') }}  </td>
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($competitors) != 0)
            <span><b>Competitors:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.name')}}</th>
                    <th>{{trans('rating_summary.address')}}</th>
                    <th>{{trans('rating_summary.phone_number')}}</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($competitors as $competitor)
                    <tr>
                        
                            <td> {{ $competitor->competitor_name }} </td>
                            <td> {{ $competitor->competitor_address }}  </td>
                            <td> {{ $competitor->competitor_phone }} </td>
                            @if ($competitor->competitor_email)
                            <td> {{ $competitor->competitor_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($relatedCompanies) != 0)
            <span><b>{{trans('rating_summary.affiliate_sub')}}:</b> </span>
            <table style="width: 100%">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.name')}}</th>
                    <th>{{trans('rating_summary.address')}}</th>
                    <th style="max-width: 100px">{{trans('rating_summary.phone_number')}}</th>
                    <th style="max-width: 120px">Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($relatedCompanies as $relatedCompany)
                    <tr>
                        <td> {{ $relatedCompany->related_company_name }} </td>
                        @if($entity->is_premium == 1)
                            <td>{{ $relatedCompany->related_company_address1 }}</td>
                        @else
                            <td> {{ $relatedCompany->related_company_address1 }}, 
                                    {{ $relatedCompany->related_company_cityid }},
                                    {{ $relatedCompany->related_company_province }},
                                    {{ $relatedCompany->related_company_zipcode }}  </td>
                        @endif
                        @if ($relatedCompany->related_company_phone)
                        <td> {{ $relatedCompany->related_company_phone }} </td>
                        @else
                        <td> - </td>
                        @endif
                        @if ($relatedCompany->related_company_email)
                            <td> {{ $relatedCompany->related_company_email }} </td>
                        @else
                            <td> - </td>
                        @endif
                        
                    </tr>
                    @endforeach
                </tbody>
                
              </table>
            @endif
        </div>
        @endif
        @endif
        
        @if(!empty($is_deliverables) || !empty($deliverables->financial_rating))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->financial_rating))
        <div class = 'hideChart last-break-page'>
        @endif
			@if($bank_standalone==0)
            <div style = 'margin-bottom: 30px;'>
                <div>
                    <div class="title3_3">
                        <div><b>{{trans('risk_rating.credit_bpo_rating')}}</b></div>
                    </div>
                </div>

                <div class = 'fifty-percent'>
                    <div class="{{ $cbpo_rate['rating_class'] }}">
                        <div style="font-size: 30px; position: relative; float:left; top: 30px;">
                            {{ $cbpo_rate['rating_txt'] }}
                        </div>
                    </div>
                </div>

                <div class = 'fifty-percent'>
                    <p>
                        <b>Result: </b>
                        <span style="font-size: 16px; color: {{ $cbpo_rate['rating_color'] }}">
                            {{ $cbpo_rate['rating_label'] }}
                        </span>
                        <br/>
                        <span>
                            {{ $cbpo_rate['rating_desc'] }}
                        </span>
                    </p>
                </div>
            </div>

            <div> <hr/> </div>
			@endif

            <div>
                <div>
                    <div class="title3_3">
                        <div>
                            <b>{{ trans('risk_rating.financial_condition') }}</b>&nbsp;({{ $readyratiodata->cc_year1 }} - {{ $readyratiodata->cc_year3 }})
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                    	<div style = 'display:block;min-height: 300px; width: 630px; text-align: center;'>
                        	<img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style="width:250px;margin-bottom: 50px" />
                    	</div>
                        <div class = 'center' style = 'position: relative; top: -50px;'>
                            {{ trans('risk_rating.financial_position_rating') }}: <b> {{ $financial['fin_position'] }} </b>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            {{ trans('risk_rating.financial_performance_rating') }}:<b> {{ $financial['fin_performance'] }} </b>
                        </div>
                        <div class = 'center' style = 'position: relative;'>
                            <span> <b>{{trans('rating_summary.rating_significance')}}</b></span><br/>
                            @if ($finalscore->score_facs >= 162)
                                {{trans('risk_rating.finalscore_one')}}
                            @elseif ($finalscore->score_facs >= 145)
                                {{trans('risk_rating.finalscore_one')}}
                            @elseif ($finalscore->score_facs >= 127)
                                {{trans('risk_rating.finalscore_two')}}
                            @elseif ($finalscore->score_facs >= 110)
                                {{trans('risk_rating.finalscore_three')}}
                            @elseif ($finalscore->score_facs >= 92)
                                {{trans('risk_rating.finalscore_four')}}
                            @elseif ($finalscore->score_facs >= 75)
                                {{trans('risk_rating.finalscore_five')}}
                            @elseif ($finalscore->score_facs >= 57)
                                {!! trans('risk_rating.finalscore_six') !!}
                            @elseif ($finalscore->score_facs >= 40)
                                {{trans('risk_rating.finalscore_seven')}}
                            @elseif ($finalscore->score_facs >= 22)
                                {{trans('risk_rating.finalscore_seven')}}
                            @elseif ($finalscore->score_facs >= 4)
                                {{trans('risk_rating.finalscore_seven')}}
                            @else
                                {{trans('risk_rating.finalscore_seven')}}
                            @endif

                            @if ($m_score >= -2.22)
                                <br/><b>{{trans('risk_rating.fs_earnings_manipulator')}}</b>
                            @endif
                        </div>
                    </div>
                </div>

                @if ($bank_standalone == 1)
                    @if (!empty($keySummary))
                        
                            <div class = 'hundred-percent'>
                            	
                                <b class="positive_points_title"> The Good </b>
                             

                                <ul class="positive_points">
					        {{-- Noncurrent assets --}}
					        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
					        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}<li>
					        @endif

					        {{-- Debt-to-equity Ratio --}}
                            @if($keySummary[0]->DebtRatio < 0.30)
								<li>{!! trans('financial_analysis/conclusion.the_debt_to_equity', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
								<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
								<li>{!! trans('financial_analysis/conclusion.the_percentage_of_liabilities', ['debtratioPercentage' => number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Working Capital and Inventories --}}
                            @if($keySummary[0]->NWC > 0)
					            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
									<li>{{trans('financial_analysis/conclusion.long_term_resources')}}</li>
					            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))
									<li>{{trans('financial_analysis/conclusion.working_capital')}}</li>
					            @endif
					        @endif

					        {{-- Current Ratio --}}
					        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CurrentRatio >= 2.1)
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio_criteria', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Quick Ratio --}}
					        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
								<li>{!! trans('financial_analysis/conclusion.a_good_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->QuickRatio >= 1.1)
								<li>{!! trans('financial_analysis/conclusion.an_outstanding_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Cash Ratio --}}
					        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CashRatio > 0.25)
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Return of Equity --}}
					        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
								<li>{!! trans('financial_analysis/conclusion.return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year ]) !!}</li>
					        @elseif($keySummary[0]->ROE > 0.2)
								<li>{!! trans('financial_analysis/conclusion.high_return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}	</li>
					        @endif

					        {{-- ROA --}}
					        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
								<li>{!! trans('financial_analysis/conclusion.good_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					        @elseif($keySummary[0]->ROA > 0.1)
								<li>{!! trans('financial_analysis/conclusion.excellent_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					        @endif

					        {{-- Net Worth --}}
					        @if($fa_report)
					            @if($balance_sheets[0]->IssuedCapital > 0)
					                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
					                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
											<li>{!! trans('financial_analysis/conclusion.net_worth_net_assets', ['netAsset' => number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					                    @else
											<li>{{trans('financial_analysis/conclusion.the_net_worth_is_higher')}}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- Equity --}}
					        @if($fa_report)
					            @if($balance_sheets)
					                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
					                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
											<li>{!! trans('financial_analysis/conclusion.equity_value_grew', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' =>  $balance_sheets[0]->Year]) !!}</li>
					                    @else
											<li>{!! trans('financial_analysis/conclusion.the_equity_growth_for_the', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' => $balance_sheets[0]->Year ]) !!}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- EBIT --}}
					        @if($keySummary[0]->EBIT > 0)
					            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
					                @if($keySummary[0]->EBIT > 0)
										<li>{!! trans('financial_analysis/conclusion.during_the_entire_period', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @else
										<li>{!! trans('financial_analysis/conclusion.earnings_before_interest', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @endif
					            @endif
					        @endif

					        {{-- Comprehensive Income --}}
                            @if($fa_report)
					            @if($income_statements[0]->ComprehensiveIncome > 0)
									<li>{!! trans('financial_analysis/conclusion.the_income_from_financial', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}</li>
					            @endif
					        @endif

					    </ul>
                            </div>
                        
                            <div class = 'hundred-percent'>
                            	@php $thebad = 0 @endphp
                            	@if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
						           @php $thebad++; 0 @endphp
						        @elseif($keySummary[0]->DebtRatio > 1)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Working Capital and Inventories --}}
						        @if($keySummary[0]->NWC > 0)
						            @if($keySummary[0]->InventoryNWC > 1.0)
						                @php $thebad++; 0 @endphp
						            @endif
						        @else
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Current Ratio --}}
						        @if($keySummary[0]->CurrentRatio < 1)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Quick Ratio --}}
						        @if($keySummary[0]->QuickRatio < 0.5)
						            @php $thebad++; 0 @endphp
						        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Cash Ratio --}}
						        @if($keySummary[0]->CashRatio < 0.05)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Return of Equity --}}
						        @if($keySummary[0]->ROE < 0)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- ROA --}}
						        @if($keySummary[0]->ROA < 0)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Net Worth --}}
						        @if($financial_report)
						            @if($balance_sheets[0]->IssuedCapital > 0)
						                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
						                    @php $thebad++; 0 @endphp
						                @endif
						            @endif
						        @endif

						        {{-- EBIT --}}
						        @if($keySummary[0]->EBIT < 0)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- comprehensive income --}}
						        @if($financial_report)
						            @if($income_statements[0]->ComprehensiveIncome < 0)
						                @php $thebad++; 0 @endphp
						            @endif
						        @endif

                            	@if($thebad > 0)
                                <b class="positive_points_title"> The Bad </b>
                                @endif
                                
                                <ul class="negative_points">
						        {{-- Debt-to-equity Ratio --}}
						        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
                                    <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage' => number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}</li>
                                @elseif($keySummary[0]->DebtRatio > 1)
                                    <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_critical', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Working Capital and Inventories --}}
						        @if($keySummary[0]->NWC > 0)
                                    @if($keySummary[0]->InventoryNWC > 1.0)
                                        <li>{{trans('financial_analysis/conclusion.available_working_capital') }}</li>
                                    @endif
                                @else
                                    <li>{{trans('financial_analysis/conclusion.not_enough_long_term')}}</li>
                                @endif

						        {{-- Current Ratio --}}
						        @if($keySummary[0]->CurrentRatio < 1)
                                    <li>{!! trans('financial_analysis/conclusion.the_current_ratio_standard', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
                                    <li>{!! trans('financial_analysis/conclusion.the_current_ratio_normal', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Quick Ratio --}}
                                @if($keySummary[0]->QuickRatio < 0.5)
                                    <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
                                    <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Cash Ratio --}}
						        @if($keySummary[0]->CashRatio < 0.05)
                                    <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
                                    <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                @endif

						        {{-- Return of Equity --}}
                                @if($keySummary[0]->ROE < 0)
                                    <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe_critical', ['roe' => number_format($keySummary[0]->ROE,2,'.',',') ]) !!}</li>
                                @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
                                    <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}</li>
                                @endif

						        {{-- ROA --}}
						        @if($keySummary[0]->ROA < 0)
                                    <li>{!! trans('financial_analysis/conclusion.critical_return_on_assets', ['year' => $keySummary[0]->Year ]) !!}</li>
                                @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
                                    <li>{!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets', ['roa' => number_format($keySummary[0]->ROA*100,1,'.', ',')]) !!}</li>
                                @endif

						        {{-- Net Worth --}}
						        @if($fa_report)
                                    @if($balance_sheets[0]->IssuedCapital > 0)
                                        @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                                            <li>{!! trans('financial_analysis/conclusion.net_worth_is_less_than', ['issuedCapital' => number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
                                        @endif
                                    @endif
                                @endif

						        {{-- EBIT --}}
						        @if($keySummary[0]->EBIT < 0)
                                    <li>{!! trans('financial_analysis/conclusion.during_the_entire_period_ebit', ['ebit' => $keySummary[0]->EBIT ]) !!}</li>
                                @endif

						        {{-- comprehensive income --}}
						        @if($fa_report)
                                    @if($income_statements[0]->ComprehensiveIncome < 0)
                                        <li>{!! trans('financial_analysis/conclusion.the_income_from_financial_operational', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',') ]) !!}</li>
                                    @endif
                                @endif

						    </ul>
                            </div>
                        
                    @endif
                @endif

                @if($bank_standalone == 0)
                <div class = 'hundred-percent'>
                    <table style="border:1px solid #000;">
                        <tr>
                            <td>&nbsp;</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->cc_year1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->cc_year2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->cc_year3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>{{trans('risk_rating.fc_receivable')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->receivables_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->receivables_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->receivables_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>+ {{trans('risk_rating.fc_inventory')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->inventory_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->inventory_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->inventory_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>- {{trans('risk_rating.fc_payable')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->accounts_payable_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->accounts_payable_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->accounts_payable_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td><b>= {{trans('risk_rating.fc_ccc')}}</b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->cash_conversion_cycle1 : "" }}</span></b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->cash_conversion_cycle2 : "" }}</span></b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->cash_conversion_cycle3 : "" }}</span></b></td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>{{trans('rating_summary.average_daily_balance')}}</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($finalscore->average_daily_balance, 2, '.', ',') }}</span></td>
                        </tr>
                        <tr>
                            <td>Operating Cashflow Margin</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore->operating_cashflow_margin }} %</span></td>
                        </tr>
                        <tr>
                            <td>Operating Cashflow Ratio</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore->operating_cashflow_ratio }}</span></td>
                        </tr>
                    </table>
                </div>
                @endif
            </div>

        </div>

        @if ((($bank_standalone == 0) && (STR_EMPTY != $finalscore->positive_points)) || ($bank_standalone == 1))

        @if(!empty($is_deliverables) || !empty($deliverables->growth_forecast))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->growth_forecast))
        <div class = 'hideChart blast-break-page'>
        @endif
            @if ($bank_standalone == 0)
                @if ($keySummary)

                        <div class = 'hundred-percent'>
                           
                               <b class="positive_points_title"> The Good </b>
                               

                                <ul class="positive_points">
					        {{-- Noncurrent assets --}}
					        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
					        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}<li>
					        @endif

					        {{-- Debt-to-equity Ratio --}}
					        @if($keySummary[0]->DebtRatio < 0.30)
								<li>{!! trans('financial_analysis/conclusion.the_debt_to_equity', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
								<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
								<li>{!! trans('financial_analysis/conclusion.the_percentage_of_liabilities', ['debtratioPercentage' => number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Working Capital and Inventories --}}
					        @if($keySummary[0]->NWC > 0)
					            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
									<li>{{trans('financial_analysis/conclusion.long_term_resources')}}</li>
					            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))
									<li>{{trans('financial_analysis/conclusion.working_capital')}}</li>
					            @endif
					        @endif

					        {{-- Current Ratio --}}
					        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CurrentRatio >= 2.1)
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio_criteria', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Quick Ratio --}}
					        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
								<li>{!! trans('financial_analysis/conclusion.a_good_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->QuickRatio >= 1.1)
								<li>{!! trans('financial_analysis/conclusion.an_outstanding_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Cash Ratio --}}
					        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CashRatio > 0.25)
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Return of Equity --}}
					        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
								<li>{!! trans('financial_analysis/conclusion.return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year ]) !!}</li>
					        @elseif($keySummary[0]->ROE > 0.2)
								<li>{!! trans('financial_analysis/conclusion.high_return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',',')]) !!}	</li>
					        @endif

					        {{-- ROA --}}
					        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
								<li>{!! trans('financial_analysis/conclusion.good_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',',')]) !!}</li>
					        @elseif($keySummary[0]->ROA > 0.1)
								<li>{!! trans('financial_analysis/conclusion.excellent_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					        @endif

					        {{-- Net Worth --}}
					        @if($fa_report)
					            @if($balance_sheets[0]->IssuedCapital > 0)
					                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
					                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
											<li>{!! trans('financial_analysis/conclusion.net_worth_net_assets', ['netAsset' => number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					                    @else
											<li>{{trans('financial_analysis/conclusion.the_net_worth_is_higher')}}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- Equity --}}
					        @if($fa_report)
					            @if($balance_sheets)
					                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
					                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
											<li>{!! trans('financial_analysis/conclusion.equity_value_grew', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' =>  $balance_sheets[0]->Year]) !!}</li>
					                    @else
											<li>{!! trans('financial_analysis/conclusion.the_equity_growth_for_the', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' => $balance_sheets[0]->Year ]) !!}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- EBIT --}}
					        @if($keySummary[0]->EBIT > 0)
					            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
					                @if($keySummary[0]->EBIT > 0)
										<li>{!! trans('financial_analysis/conclusion.during_the_entire_period', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @else
										<li>{!! trans('financial_analysis/conclusion.earnings_before_interest', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @endif
					            @endif
					        @endif

					        {{-- Comprehensive Income --}}
					        @if($fa_report)
					            @if($income_statements[0]->ComprehensiveIncome > 0)
									<li>{!! trans('financial_analysis/conclusion.the_income_from_financial', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}</li>
					            @endif
					        @endif

					    </ul>
                        </div>
                                       
                        <div class = 'hundred-percent'>
                   				
                   				@php $thebad = 0 @endphp
                            	@if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
						           @php $thebad++; 0 @endphp
						        @elseif($keySummary[0]->DebtRatio > 1)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Working Capital and Inventories --}}
						        @if($keySummary[0]->NWC > 0)
						            @if($keySummary[0]->InventoryNWC > 1.0)
						                @php $thebad++; 0 @endphp
						            @endif
						        @else
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Current Ratio --}}
						        @if($keySummary[0]->CurrentRatio < 1)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Quick Ratio --}}
						        @if($keySummary[0]->QuickRatio < 0.5)
						            @php $thebad++; 0 @endphp
						        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Cash Ratio --}}
						        @if($keySummary[0]->CashRatio < 0.05)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Return of Equity --}}
						        @if($keySummary[0]->ROE < 0)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- ROA --}}
						        @if($keySummary[0]->ROA < 0)
						            @php $thebad++; 0 @endphp
						        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- Net Worth --}}
						        @if($financial_report)
						            @if($balance_sheets[0]->IssuedCapital > 0)
						                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
						                    @php $thebad++; 0 @endphp
						                @endif
						            @endif
						        @endif

						        {{-- EBIT --}}
						        @if($keySummary[0]->EBIT < 0)
						            @php $thebad++; 0 @endphp
						        @endif

						        {{-- comprehensive income --}}
						        @if($financial_report)
						            @if($income_statements[0]->ComprehensiveIncome < 0)
						                @php $thebad++; 0 @endphp
						            @endif
						        @endif

                            	@if($thebad > 0)
                                <b class="positive_points_title"> The Bad </b>
                                @endif
                                
                                <ul class="negative_points">
						        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
                                    <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage' => number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}</li>
                                @elseif($keySummary[0]->DebtRatio > 1)
                                    <li>{!! trans('financial_analysis/conclusion.the_debt_ratio_critical', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Working Capital and Inventories --}}
						        @if($keySummary[0]->NWC > 0)
					            @if($keySummary[0]->InventoryNWC > 1.0)
									<li>{{trans('financial_analysis/conclusion.available_working_capital') }}</li>
                                    @endif
                                @else
                                    <li>{{trans('financial_analysis/conclusion.not_enough_long_term')}}</li>
                                @endif

						        {{-- Current Ratio --}}
						        @if($keySummary[0]->CurrentRatio < 1)
                                    <li>{!! trans('financial_analysis/conclusion.the_current_ratio_standard', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
                                    <li>{!! trans('financial_analysis/conclusion.the_current_ratio_normal', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Quick Ratio --}}
						        @if($keySummary[0]->QuickRatio < 0.5)
                                    <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
                                    <li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
                                @endif

						        {{-- Cash Ratio --}}
						        @if($keySummary[0]->CashRatio < 0.05)
                                    <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
                                    <li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
                                @endif

						        {{-- Return of Equity --}}
						        @if($keySummary[0]->ROE < 0)
                                    <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe_critical', ['roe' => number_format($keySummary[0]->ROE,2,'.',',') ]) !!}</li>
                                @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
                                    <li>{!! trans('financial_analysis/conclusion.return_on_equity_roe', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}</li>
                                @endif

						        {{-- ROA --}}
						        @if($keySummary[0]->ROA < 0)
                                    <li>{!! trans('financial_analysis/conclusion.critical_return_on_assets', ['year' => $keySummary[0]->Year ]) !!}</li>
                                @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
                                    <li>{!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets', ['roa' => number_format($keySummary[0]->ROA*100,1,'.', ',')]) !!}</li>
                                @endif

						        {{-- Net Worth --}}
						        @if($fa_report)
								@if($balance_sheets[0]->IssuedCapital > 0)
                                        @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                                            <li>{!! trans('financial_analysis/conclusion.net_worth_is_less_than', ['issuedCapital' => number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
                                        @endif
                                    @endif
                                @endif

						        {{-- EBIT --}}
						        @if($keySummary[0]->EBIT < 0)
                                    <li>{!! trans('financial_analysis/conclusion.during_the_entire_period_ebit', ['ebit' => $keySummary[0]->EBIT ]) !!}</li>
                                @endif

						        {{-- comprehensive income --}}
						        @if($fa_report)
								@if($income_statements[0]->ComprehensiveIncome < 0)
                                        <li>{!! trans('financial_analysis/conclusion.the_income_from_financial_operational', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',') ]) !!}</li>
                                    @endif
                                @endif

						    </ul>
                        </div>
                   
                @endif
            @endif

            @if ($bank_standalone == 0)
            </div>
            <div class = 'break-page'>
            @endif

            <!-- <div>
                <div class = 'hundred-percent'>
                    <div>
                        <div class="title3_3">
                            <div style = 'margin-left: -15px;'>
                                <b>Statement of Cash Flows</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                        <table>
                            <tr>
                                <td>Net Operating Income</td>
                                <td>{{ number_format($cash_flows[0]->NetOperatingIncome,2,".",",") }}</td>
                            </tr>
                            <tr>
                                <td>Net Cashflow from Operations</td>
                                <td>{{ number_format($cash_flows[0]->NetCashFlowFromOperations,2,".",",") }}</td>
                            </tr>
                            <tr>
                                <td>Net Profit Margin</td>
                                <td>{{ $cf_net_profit_margin_percentage }}%</td>
                            </tr>
                            <tr>
                                <td>Asset Turnover</td>
                                <td>{{ $cf_asset_turnover_percentage }}%</td>
                            </tr>
                            <tr>
                                <td>Equity Multiplier</td>
                                <td>{{ $cf_equity_multiplier_percentage }}%</td>
                            </tr>
                            <tr>
                                <td>DuPont Analysis</td>
                                <td>{{ $cf_dupont_analysis_percentage }}%</td>
                            </tr>

                        </table>
                    </div>
                </div>

            </div> -->

            <div>
                <div class = 'hundred-percent'>
                    <div>
                        <div class="title3_3">
                            <div style = 'margin-left: -15px;'>
                                <b>Cash Conversion Cycle</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <td style="text-align:center;width: 150px" rowspan="2" >{{ trans('financial_analysis/turnover_ratios.turnover_ratio') }}</td>
                                    <td style="text-align:center;" colspan="{{count($turnoverRatios)}}">{{ trans('financial_analysis/turnover_ratios.value_days') }}</td>
                                    <td style="text-align:center;width: 80px" rowspan ="2">{!! trans('financial_analysis/turnover_ratios.change',['colCount' => count($turnoverRatios)+1]) !!}</td>
                                </tr>
                                <tr>
                                    @php
                                        $w = intval(170/(count($turnoverRatios)));
                                    @endphp
                                    @foreach($turnoverRatios as $turnover)
                                        <td style="text-align:center;width: {{ $w }}px">{{$turnover->year}}</td>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align:center;width: 150px">1</td>

                                    @php
                                    $i = 1;
                                    foreach($turnoverRatios as $turnover){
                                        $i++;
                                        echo '<td style="text-align:center;width: '.$w.'px">'.$i.'</td>';
                                    }
                                    @endphp
                                    <td style="text-align:center;width: 80px">{{ $i++ }}</td>
                                </tr>
                                <tr>
                                    <td> Days Sales Outstanding</td>
                                    @foreach($turnoverRatios as $turnover)
                                        <td style="text-align:center;">{{$turnover->ReceivablesTurnover}}</td>
                                    @endforeach
                                    <td style="text-align:center;">
                                        @if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) == 0)
                                            -
                                        @elseif(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) > 0)
                                            <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
                                        @else
                                            <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Days Payable Outstanding</td>
                                    @foreach($turnoverRatios as $turnover)
                                        <td style="text-align:center;">{{$turnover->PayableTurnover}}</td>
                                    @endforeach
                                    <td style="text-align:center;">
                                        @if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) == 0)
                                            -
                                        @elseif(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) > 0)
                                            <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
                                        @else
                                            <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td> Days Inventory Oustanding</td>
                                    @foreach($turnoverRatios as $turnover)
                                        <td style="text-align:center;">{{$turnover->InventoryTurnover}}</td>
                                    @endforeach
                                    <td style="text-align:center;">
                                        @if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) == 0)
                                            -
                                        @elseif(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) > 0)
                                            <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
                                        @else
                                            <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cash Conversion Cycle</td>
                                    @foreach($turnoverRatios as $turnover) <td style="text-align:center;">{{$turnover->CCC}}</td> @endforeach
                                    <td style="text-align:center;">
                                        @if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) == 0)
                                            -
                                        @elseif(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) > 0)
                                            <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
                                        @else
                                            <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            
            <div>
                <div class = 'hundred-percent'>
                    <div>
                        <div class="title3_3">
                            <div style = 'margin-left: -15px;'>
                                <b>Return on Equity Drivers</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th></th>
                                    @foreach($ROEDrivers as $drivers)
                                        <th style="text-align:center;">{{$drivers['year']}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align:left;">Net Income</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['net_income'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;">Revenue</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['revenue'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;"><strong>Profit Margin</strong></td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;"><strong>{{number_format($drivers['profit_margin'],2,".",",")}}</strong></td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;">Revenue</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['revenue'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;">Ave. Assets</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;"><strong>Asset Turnover</strong></td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;"><strong>{{number_format($drivers['turnover_asset'],2,".",",")}}</strong></td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;">Revenue</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['revenue'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;">Ave. Assets</td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;"><strong>Financial Leverage</strong></td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;"><strong>{{number_format($drivers['financial_leverage'],2,".",",")}}</strong></td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td style="text-align:left;"><strong>ROE</strong></td>
                                    @foreach($ROEDrivers as $drivers)
                                        <td style="text-align:center;"><strong>{{number_format(($drivers['roe']*100),2,".",",")}}%</strong></td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div>
                <br><br><br><br>
				<div class = 'hundred-percent' >
                        <div class="title3_3">
                            <div style = 'margin-left: -15px;'>
                                <b>Growth Forecast</b>
                            </div>
                        </div>
                        <img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["forecast"]) }}' style = 'height: auto; width: 630px; vertical-align: middle;'/>
                </div>

				<div class = 'hundred-percent' style="font-size:12px;">
                    <p>
						{!! $growth_desc !!}
					</p>
                </div>

                <div class = 'hundred-percent' style="margin-bottom: 0; padding-bottom: 0;">
                    <p style="margin-bottom: 0; padding-bottom: 0; font-size:12px;">
                        <b>{{trans('rating_summary.looking_at')}} </b>
                        <br>
                        {{trans('rating_summary.gf_desc_1',['organization' => $organization_name])}}
                        <br/><br/>
                        {{trans('rating_summary.gf_desc_2')}}
                        <br/><br/>
                        {{trans('rating_summary.gf_desc_3')}}
                        <br>
					</p>
                </div>
            </div>

        </div>
        @endif

        @if($entity->export_DSCR == 1)
        <div>
            <div class = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div style = 'margin-left: -15px;'><b>{{trans('rating_summary.export_parameters')}}</b></div>
                    </div>
                </div>
            </div>
            @if(!empty($is_financing))
            <table style="border:1px solid #000;">
                <tr>
                    <td><b>DSCR</b></td>
                    <td><b>{{ $dscr_details->dscr }}x</b></td>
                </tr>
                @if($dscr_details->source == "NetOperatingIncome")
                <tr>    
                    <td><b>{{trans('rating_summary.source')}} (NOI (Net Operating Income))</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b></td>
                </tr>
                @elseif($dscr_details->source == "NetCashByOperatingActivities")
                <tr>    
                    <td><b>{{trans('rating_summary.source')}} (Net Cash Flow)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b></td>
                </tr>
                @elseif($dscr_details->source == "ADB")
                <tr>
                    <td><b>{{trans('rating_summary.source')}} (Average Daily Balance)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b>
                </tr>
                @else
                <tr>
                    <td><b>{{trans('rating_summary.source')}} (Custom)</b></td> 
                    <td></b>{{ number_format($dscr_details->net_operating_income, 2) }} </b></td>
                </tr>              
                @endif
                <tr>
                    <td><b>{{trans('rating_summary.t_loan_duration')}} </b></td>
                    <td><b>{{ $dscr_details->loan_duration }} </b> </td>
                </tr>
                <tr>
                    <td><b>{{trans('rating_summary.loan_ammount')}}</b></td>
                    <td><b>{{ number_format($dscr_details->loan_amount, 2) }} </b> </td>
                </tr>
            </table>
            @else
            <table style="border:1px solid #000;">
                <tr>
                    <td><b>{{trans('rating_summary.coverage_ratio')}}</b></td>
                    <td> {{ $dscr_details->dscr }}x</b> </td>
                </tr>
                @if($dscr_details->source == "NetOperatingIncome")
                <tr>
                    <td><b>{{trans('rating_summary.funds_to_cover')}}: (NOI (Net Operating Income))</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @elseif($dscr_details->source == "NetCashByOperatingActivities")
                <tr>
                    <td><b>{{trans('rating_summary.funds_to_cover')}}: (Net Cash Flow)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b></td><br/>
                </tr>
                @elseif($dscr_details->source == "ADB")
                <tr>
                    <td><b>{{trans('rating_summary.funds_to_cover')}}: (Average Daily Balance)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @else
                <tr>
                    <td><b>{{trans('rating_summary.funds_to_cover')}}: (Custom)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @endif
                <tr>
                    <td><b>{{trans('rating_summary.suggested_contract_base')}}</b></td>
                    <td><b> {{ $dscr_details->loan_duration }} </b> </td>
                </tr>
                <tr>
                    <td><b>{{trans('rating_summary.awards_fund_source')}} </b></td> <!-- Mungkahing Gawad sa Kontrata Batay sa Pinagmulan ng Mga Pondo -->
                    <td><b> {{ number_format($dscr_details->loan_amount, 2) }} </b> </td>
                </tr>
            </table>
            @endif
        </div>
        @endif

        @if($bank_standalone==0)
        <div class = 'break-page'>
            <div class = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div><b>{{ trans('risk_rating.business_considerations') }}</b></div>
                    </div>
                </div>

                @if ($finalscore->score_rm == 90)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.risk_management')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.rm_high')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.risk_management')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.rm_low')}}
                    </p>
                @endif

                @if ($finalscore->score_cd == 30)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.cd_high')}}
                    </p>
                @elseif ($finalscore->score_cd == 20)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.cd_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.cd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_sd == 30)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.sd_high')}}
                    </p>
                @elseif ($finalscore->score_sd == 20)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.sd_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.sd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_bois == 90)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.bo_high')}}
                    </p>
                @elseif ($finalscore->score_bois == 60)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.bo_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.bo_low')}}
                    </p>
                @endif
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["bcc_graph"]) }}' style = 'max-width: 100%;'/>
            </div>
        </div>

        <div class = 'break-fpage'>
            <div id = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div><b>{{ trans('risk_rating.management_quality') }}</b></div>
                    </div>
                </div>

                @if ($finalscore->score_boe == 21)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                @elseif ($finalscore->score_boe == 14)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                @elseif ($finalscore->score_boe == 7)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                @endif

                @if ($finalscore->score_mte == 21)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                @elseif ($finalscore->score_mte == 14)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                @elseif ($finalscore->score_mte == 7)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                @endif

                @if ($finalscore->score_bdms == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_driver')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.bd_high')}}
                    </p>
                @elseif ($finalscore->score_bdms == 12)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_driver')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.bd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_sp == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.sp_high')}}
                    </p>
                @elseif ($finalscore->score_sp == 24)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.sp_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.sp_low')}}
                    </p>
                @endif

                @if ($finalscore->score_ppfi == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.past_project')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.pp_high')}}
                    </p>
                @elseif ($finalscore->score_ppfi == 12)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.past_project')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.pp_low')}}
                    </p>
                @endif
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["mq_graph"]) }}' style = 'max-width: 100%;'/>
            </div>
        </div>
        @endif

        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->industry_forecasting))
        <div class = 'hideChart last-break-page'>
        @endif
            <div>
                <div class="title3_4">
                    <div>
                        <b> {{ trans('risk_rating.industry_comparison') }} </b> <br/>
                        > {{ $industry->main_title }} <br/>
                        >> {{ !empty($ind_sub->sub_title) ? $ind_sub->sub_title: ''  }} <br/>
                        >>> @php echo !empty($ind_row->group_description) ? $ind_row->group_description:''; @endphp
                    </div>
					
                </div>
            </div>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.gross_growth_vs_industry') }}
                    <span style = 'color: #98d027;'> {{ number_format($ind_cmp['grg'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #aaaaaa;"> {{ number_format($boost['grg'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['grg'] > $boost['grg'])
                    {{ trans('risk_rating.growth_high') }}
                @elseif ($ind_cmp['grg'] == $boost['grg'])
                    {{ trans('risk_rating.growth_moderate') }}
                @elseif ($boost['grg'] > $ind_cmp['grg'])
                    {{ trans('risk_rating.growth_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.net_growth_vs_industry') }}
                    <span style = 'color: #98d027;'> {{ number_format($ind_cmp['nig'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #aaaaaa;"> {{ number_format($boost['nig'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['nig'] > $boost['nig'])
                    {{ trans('risk_rating.net_high') }}
                @elseif ($ind_cmp['nig'] == $boost['nig'])
                    {{ trans('risk_rating.net_moderate') }}
                @elseif ($boost['nig'] > $ind_cmp['nig'])
                    {{ trans('risk_rating.net_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.profit_vs_industry') }}
                    <span style = 'color: #98d027;'> {{ number_format($ind_cmp['gpm'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #aaaaaa;"> {{ number_format($boost['gpm'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['gpm'] > $boost['gpm'])
                    {{ trans('risk_rating.profit_high') }}
                @elseif ($ind_cmp['gpm'] == $boost['gpm'])
                    {{ trans('risk_rating.profit_moderate') }}
                @elseif ($boost['gpm'] > $ind_cmp['gpm'])
                    {{ trans('risk_rating.profit_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.ratio_vs_industry') }}
                    <span style = 'color: #98d027;'> {{ number_format($ind_cmp['cr_ind'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #aaaaaa;"> {{ number_format($boost['cr'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['cr_ind'] > $boost['cr'])
                    {{ trans('risk_rating.ratio_high') }}
                @elseif ($ind_cmp['cr_ind'] == $boost['cr'])
                    {{ trans('risk_rating.ratio_moderate') }}
                @elseif ($boost['cr'] > $ind_cmp['cr_ind'])
                    {{ trans('risk_rating.ratio_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.debt_vs_industry') }}
                    <span style = 'color: #98d027;'> {{ number_format($ind_cmp['der_ind'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #aaaaaa;"> {{ number_format($boost['der'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['der_ind'] > $boost['der'])
                    {{ trans('risk_rating.debt_high') }}
                @elseif ($ind_cmp['der_ind'] == $boost['der'])
                    {{ trans('risk_rating.debt_moderate') }}
                @elseif ($boost['der'] > $ind_cmp['der_ind'])
                    {{ trans('risk_rating.debt_low') }}
                @endif
            </p>

            <div class="ind-legend hundred-percent">
                <div class="legend-item">
                    <span class="legend-info legend-company"></span>
                    <span>Company</span>
                </div>
                <div class="legend-item">
                    <span class="legend-info industry"></span>
                    <span>Industry</span>
                </div>
            </div>

            <div class = 'hundred-percent'>
                <span style="font-size:10px; color: #8a8a5c; display:inline; text-align:center; width: 15%;">Gross Revenue Growth %</span><img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["reg1_graph"]) }}' style = 'display:inline; max-height: 66px; width:75%; vertical-align:middle; float:right;'/>
            </div>
            <br>

            <div class = 'hundred-percent'>
                <span style="font-size:10px; color: #8a8a5c; display:inline; text-align:center; width: 15%;">Net Income Growth %</span><img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["reg2_graph"]) }}' style = 'display:inline; max-height: 66px; width:75%; vertical-align:middle; float:right;'/>
            </div>
            <br>

            <div class = 'hundred-percent'>
                <span style="font-size:10px; color: #8a8a5c; display:inline; text-align:center; width: 15%;">Gross Profit Margin %</span><img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["reg3_graph"]) }}' style = 'display:inline; max-height: 66px; width:75%; vertical-align:middle; float:right;'/>
            </div>
            <br>

            <div class = 'hundred-percent'>
                <span style="font-size:10px; color: #8a8a5c; display:inline; text-align:center; width: 15%;">Current Ratio</span><img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["reg4_graph"]) }}' style = 'display:inline; max-height: 66px; width:75%; vertical-align:middle; float:right;'/>
            </div>
            <br>

            <div class = 'hundred-percent'>
                <span style="font-size:10px; color: #8a8a5c; display:inline; text-align:center; width: 15%;">Debt-to-Equity</span><img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["reg5_graph"]) }}' style = 'display:inline; max-height: 66px; width:75%; vertical-align:middle; float:right;'/>
            </div>

        </div>

        <br> <br><br><br>

        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
            <div class = 'hundred-percent'>
        @elseif(empty($deliverables->industry_forecasting))
            <div class = 'hideChart hundred-percent'>
        @endif
        </div>
		@if($bank_standalone==0)
        <div class = 'break-page'>
            <div>
                <div class="title3_3">
                    <div><b>{{trans('rating_summary.strategic_profile')}}</b></div>
                </div>
            </div>

            <div class = 'hundred-percent' style = 'margin: 10px;'>
                @if ($businessdriver)
                <div style = 'width: 35%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'>{{ trans('business_details2.bdriver') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{trans('rating_summary.driver_name')}}</b> </td>
                                <td> <b>{{trans('rating_summary.percent_share')}}</b> </td> 
                            </tr> 

                            @foreach ($businessdriver as $driver)
                            <tr>
                                <td> {{ $driver->businessdriver_name }} </td>
                                <td> {{ $driver->businessdriver_total_sales }}% </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif

                @if ($bizsegment)
                <div style = 'width: 30%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr'>{{ trans('business_details2.customer_segment') }} </td>
                            </tr>
                            @foreach ($bizsegment as $segment)
                            <tr>
                                <td> {{ $segment->businesssegment_name }} </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif

                @if ($revenuepotential)
                <div style = 'width: 30%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'> {{ trans('rating_summary.revenue_growth_3_yrs') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('condition_sustainability.under_current_market') }}</b> </td>
                                <td> <b>{{ trans('condition_sustainability.expected_future_market') }}</b> </td>
                            </tr>

                            @foreach ($revenuepotential as $potential)
                            <tr>
                                <td>
                                    @if ($potential->current_market == 1)
                                        0%
                                    @elseif ($potential->current_market == 2)
                                        {{ trans('condition_sustainability.less_3_percent') }}
                                    @elseif ($potential->current_market == 3)
                                        {{ trans('condition_sustainability.between_3to5_percent') }}
                                    @else
                                        {{ trans('condition_sustainability.greater_5_percent') }}
                                    @endif
                                </td>

                                <td>
                                    @if ($potential->new_market == 1)
                                        0%
                                    @elseif ($potential->new_market == 2)
                                        {{ trans('condition_sustainability.less_3_percent') }}
                                    @elseif ($potential->new_market == 3)
                                        {{ trans('condition_sustainability.between_3to5_percent') }}
                                    @else
                                        {{ trans('condition_sustainability.greater_5_percent') }}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif
            </div>

            @if ($past_projs)
            <div class = 'hundred-percent' style = 'margin: 10px;'>
                <div class="read-only">
                    <table style = 'width: 96%;'>
                        <tr>
                            <td class = 'tb_hdr' colspan = '5'> {{ trans('business_details2.past_proj_completed') }} </td>
                        </tr>

                        <tr>
                            <td> <b>{{ trans('business_details2.ppc_purpose') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_cost_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_source_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_year_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_result_short') }}</b> </td>
                        </tr>

                        @foreach ($past_projs as $proj)
                        <tr>
                            <td> {{ $proj->projectcompleted_name }} </td>
                            <td> {{ number_format($proj->projectcompleted_cost, 2) }} </td>
                            <td> {{ trans('business_details2.'.$proj->projectcompleted_source_funding) }} </td>
                            <td> {{ $proj->projectcompleted_year_began }} </td>
                            <td> {{ trans('business_details2.'.$proj->projectcompleted_result) }} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif

            @if ($future_growths)
            <div class = 'hundred-percent' style = 'margin: 10px;'>
                <div class="read-only">
                    <table style = 'width: 96%;'>
                        <tr>
                            <td class = 'tb_hdr' colspan = '5'> {{ trans('business_details2.future_growth_initiatives') }} </td>
                        </tr>

                        <tr>
                            <td> <b>{{ trans('business_details2.fgi_purpose') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_cost') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_date') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_source') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_proj_goal') }}</b> </td>
                        </tr>

                        @foreach ($future_growths as $future)
                        <tr>
                            <td> {{ $future->futuregrowth_name }} </td>
                            <td> {{ number_format($future->futuregrowth_estimated_cost, 2) }} </td>
                            <td> {{ $future->futuregrowth_implementation_date }} </td>
                            <td> {{ trans('business_details2.'.$future->futuregrowth_source_capital) }} </td>
                            <td> {{ trans('business_details2.'.$future->planned_proj_goal) }} by {{ $future->planned_goal_increase }}% beginning {{ substr($future->proj_benefit_date, 5, 2) }} {{ substr($future->proj_benefit_date, 0, 4) }} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif

            @if ($planfacilities)
            @foreach ($planfacilities as $planfacility)
            <div class = 'hundred-percent' style = 'margin: 10px; margin-right: 12px;'>
                <div class = 'fifty-percent' style = 'margin: 0px;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '3'> {{ trans('rcl.rcl') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.amount_of_line') }}</b> </td>
                                @if ($planfacility->purpose_credit_facility == 5)
                                <td> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                <td> <b>{{ trans('rcl.supplier_credit') }}</b> </td>
                                @elseif ($planfacility->purpose_credit_facility == 4)
                                <td> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                <td> <b>{{ trans('rcl.purpose_of_cf_others') }}</b> </td>
                                @else
                                <td colspan = '2'> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                @endif
                            </tr>

                            <tr>
                                <td> {{ number_format($planfacility->plan_credit_availment, 2) }} </td>
                                @if (($planfacility->purpose_credit_facility == 4) || ($planfacility->purpose_credit_facility == 5))
                                <td>
                                @else
                                <td colspan = '2'>
                                @endif
                                    @if ($planfacility->purpose_credit_facility == 1)
                                            {{trans('rcl.working_capital')}}
                                    @elseif ($planfacility->purpose_credit_facility == 2)
                                            {{trans('rcl.expansion')}}
                                    @elseif ($planfacility->purpose_credit_facility == 3)
                                            {{trans('rcl.takeout')}}
                                    @elseif ($planfacility->purpose_credit_facility == 5)
                                            {{trans('rcl.supplier_credit')}}
                                    @else
                                            {{trans('rcl.other_specify')}}
                                    @endif
                                </td>

                                @if (($planfacility->purpose_credit_facility == 4) || ($planfacility->purpose_credit_facility == 5))
                                <td>
                                    @if ($planfacility->purpose_credit_facility == 5)
                                        {{ $planfacility->credit_terms }}
                                    @elseif ($planfacility->purpose_credit_facility == 4)
                                        {{ $planfacility->purpose_credit_facility_others }}
                                    @endif
                                </td>
                                @endif
                            </tr>

                            @if ('' != $planfacility->other_remarks)
                            <tr>
                                <td class = 'tb_hdr' colspan = '3'> {{ trans('rcl.other_remarks') }} </td>
                            </tr>

                            <tr>
                                <td colspan = '3'> <b>{{ $planfacility->other_remarks }}</b> </td>
                            </tr>
                            @endif
                        </table>
                    </div>

                </div>

                <div class = 'fifty-percent' style = 'margin: 0px; width: 51%;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'> {{ trans('rcl.offered_collateral') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.type_of_deposit') }}</b> </td>
                                <td> {{ $planfacility->cash_deposit_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.deposit_amount') }}</b> </td>
                                <td> {{ number_format($planfacility->cash_deposit_amount, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.marketable_securities') }}</b> </td>
                                <td> {{ $planfacility->securities_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.marketable_securities_value') }}</b> </td>
                                <td> {{ number_format($planfacility->securities_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.property') }}</b> </td>
                                <td> {{ $planfacility->property_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.property_value') }}</b> </td>
                                <td> {{ number_format($planfacility->property_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.chattel') }}</b> </td>
                                <td> {{ $planfacility->chattel_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.chattel_value') }}</b> </td>
                                <td> {{ number_format($planfacility->chattel_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.others') }}</b> </td>
                                <td> {{ $planfacility->others_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.others_value') }}</b> </td>
                                <td> {{ number_format($planfacility->others_estimated_value, 2) }} </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            @endforeach
            @endif

        </div>
        @endif

        
        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
        <div><hr/></div>
        @elseif(empty($deliverables->industry_forecasting))
        <div class="hideChart"><hr/></div>
        @endif
        
        {{-- @if ($bank_standalone==0) --}}
        @if($entity->is_premium == 1)
            @if ((empty($keyManagers)) && (empty($shareHolders)) && (empty($boardOfDirectors)) && (empty($majorCustomers)) && (empty($majorSuppliers)) && empty($location))
            <div class="last-break-page">
            @else
            <div class="break-page">
            @endif
        @else
            <div class = 'last-break-page'>
        @endif
        {{-- @else --}}
            {{-- <div class='last-break-page'> --}}
        {{-- @endif --}}
            @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
            <div class = 'hundred-percent'>
            @elseif(empty($deliverables->industry_forecasting))
            <div class = 'hideChart hundred-percent'>
            @endif
                <div id = 'creditbpo-disc'>
                    <p>
                        {{trans('rating_summary.rating_summary_disclaimer1', ['organization' => strtoupper($organization_name)])}}
                        <br />
                        {{trans('rating_summary.rating_summary_disclaimer2', ['organization' => $organization_name])}}
                        <br/>
                        {{trans('rating_summary.copyright', ['organization' => strtoupper($organization_name)])}}
                    </p>
                </div>
            </div>
            </div>
        </div>

    @if ($entity->is_premium == 1)
    <div>
    @if ((empty($keyManagers)) && (empty($shareHolders)) && (empty($boardOfDirectors)))
    @else
    @if(!empty($location) && ((empty($majorCustomers)) && (empty($majorSuppliers))))
    <div class="last-break-page annexA"></div>
    @else
    <div class="break-page annexA">
    @endif
        @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0))
        @elseif ((count($keyManagers) != 0) || (count($shareHolders) != 0) || (count($boardOfDirectors) != 0))
        <h3>Annex A: Associated Persons</h3>
        @endif
        @if (count($keyManagers) != 0)
        <div class="avoidPageBreak">
            <p><b>Key Managers</b></p>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col"> {{trans('rating_summary.name')}}</th>
                    <th scope="col"> {{trans('rating_summary.address')}}</th>
                    <th scope="col"> {{trans('rating_summary.nationality')}}</th>
                    <th scope="col"> {{trans('rating_summary.position')}}</th>
                    <th scope="col"> {{trans('rating_summary.percent_of_ownership')}}</th> 
                    
                    
                </tr>
                </thead>
                <tbody>
                    @foreach ($keyManagers as $keyManager)
                    <tr>
                        <td> {{ $keyManager->firstname }} {{ $keyManager->middlename }} {{ $keyManager->lastname }} </td>
                        @if ($keyManager->address1)
                        <td> {{ $keyManager->address1 }}  </td>
                        @else
                        <td> - </td>
                        @endif
                        @if ($keyManager->nationality)
                        <td style="text-align: center;"> {{ $keyManager->nationality }}  </td>
                        @else
                        <td> - </td>
                        @endif
                        <td style="text-align: center;"> {{ $keyManager->position }}  </td>
                        @if ($keyManager->percent_of_ownership)
                        <td style="text-align: center;"> {{ $keyManager->percent_of_ownership }}  </td>
                        @else
                        <td> - </td>
                        @endif
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
                
        </div><br>
        @endif
        @if (count($shareHolders) != 0)
        <div class="avoidPageBreak">
                <p><b>Shareholders</b></p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{trans('rating_summary.name')}}</th>
                        <th scope="col">{{trans('rating_summary.address')}}</th>
                        <th scope="col">{{trans('rating_summary.nationality')}}</th>
                        <th scope="col">TIN</th>
                        <th scope="col">{{trans('rating_summary.percent_of_ownership')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($shareHolders as $shareHolder)
                        <tr>
                            <td> {{ $shareHolder->name }} </td>
                            <td> {{ $shareHolder->address ? $shareHolder->address : ' - ' }}  </td>
                            <td style="text-align: center;"> {{ $shareHolder->nationality }}  </td>
                            @if ($shareHolder->id_no)
                            <td style="text-align: center;"> {{ $shareHolder->id_no }}  </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($shareHolder->percentage_share)
                            <td style="text-align: center;"> {{ $shareHolder->percentage_share }}  </td>
                            @else
                            <td> - </td>
                            @endif
                        </tr>
                            
                        @endforeach
                    </tbody>
                </table>
                    
            </div><br>
            @endif
            @if (count($boardOfDirectors) != 0)
            <div class="avoidPageBreak">
                <p><b>Board of Directors</b></p>
                <table class="table">
                    <thead>
                      <tr>
                        <th>{{trans('rating_summary.name')}}</th>
                        <th>{{trans('rating_summary.address')}}</th>
                        <th>{{trans('rating_summary.nationality')}}</th>
                        <th>TIN</th>
                        <th>{{trans('rating_summary.percent_of_ownership')}}</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($boardOfDirectors as $boardOfDirector)
                        <tr>
                            
                                <td> {{ $boardOfDirector->name }} </td>
                                <td> {{ $boardOfDirector->address ? $boardOfDirector->address : ' - ' }}  </td>
                                <td style="text-align: center;"> {{ $boardOfDirector->nationality }} </td>
                                @if ($boardOfDirector->id_no)
                                <td style="text-align: center;"> {{ $boardOfDirector->id_no }} </td>
                                @else
                                <td> - </td>
                                @endif
                                @if ($boardOfDirector->percentage_share != 0)
                                <td style="text-align: center;"> {{ (int)$boardOfDirector->percentage_share }} </td>
                                @else
                                <td> - </td>
                                @endif
                            
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                
            </div><br>
            @endif
    </div>
    @endif

    @if ((empty($majorCustomers)) && (empty($majorSuppliers)))
    @else
    @if(!empty($location))
    <div class="break-page">
    @else
    <div class="last-break-page">
    @endif
        @if (empty($keyManagers) && empty($shareHolders) && empty($boardOfDirectors) && (empty($majorCustomers) || empty($majorSuppliers)))
            <h3>Annex A: {{trans('rating_summary.customer_and_supplier')}}</h3>
        @elseif ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0))
        @else
            <h3>Annex B: {{trans('rating_summary.customer_and_supplier')}}</h3> 
        @endif
        
        @if (count($majorCustomers) != 0) 
        <div>
            <p><b>{{trans('rating_summary.major_customer')}}</b></p>
            <table>
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.name')}}</th>
                    <th>{{trans('rating_summary.address')}}</th>
                    <th>{{trans('rating_summary.phone_number')}}</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($majorCustomers as $majorCustomer)
                    <tr>
                        
                            <td> {{ $majorCustomer->customer_name }} </td>
                            <td> {{ $majorCustomer->customer_address }}  </td>
                            @if ($majorCustomer->customer_phone)
                            <td> {{ $majorCustomer->customer_phone }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($majorCustomer->customer_email)
                            <td> {{ $majorCustomer->customer_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
              </table>
            
        </div><br>
        @endif
        @if (count($majorSuppliers) != 0)  
        <div>
            <p><b>{{trans('rating_summary.major_supplier')}}</b></p>
            <table>
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.name')}}</th>
                    <th>{{trans('rating_summary.address')}}</th>
                    <th>{{trans('rating_summary.phone_number')}}</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($majorSuppliers as $majorSupplier)
                    <tr>
                        
                            <td> {{ $majorSupplier->supplier_name }} </td>
                            <td> {{ $majorSupplier->supplier_address }}  </td>
                            @if ($majorSupplier->supplier_phone)
                            <td> {{ $majorSupplier->supplier_phone }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($majorSupplier->supplier_email)
                            <td> {{ $majorSupplier->supplier_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
              </table>
            
        </div>
        @endif
    </div>
    @endif


    @if (!empty($location))
        @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && ((count($majorCustomers) != 0) || (count($majorSuppliers) != 0)))
        <h3>Annex B: {{trans('rating_summary.site_inspection')}}</h3> 
        @elseif (((count($keyManagers) != 0) || (count($shareHolders) != 0) || (count($boardOfDirectors) != 0)) && ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0)))
        <h3>Annex B: {{trans('rating_summary.site_inspection')}}</h3>
        @elseif((count($keyManagers) == 0) && ( count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && (count($majorCustomers) == 0) && (count($majorSuppliers) == 0))
        <h3>Annex A: {{trans('rating_summary.site_inspection')}}</h3>
        @else
        <h3>Annex C:{{trans('rating_summary.site_inspection')}}</h3>
        @endif
        <div class="mainLocation">
            <p><b>{{trans('rating_summary.main_location')}}</b></p>
            <table class="table">
                <thead>
                  <tr>
                    <th>{{trans('rating_summary.address')}}</th>
                    <th>{{trans('rating_summary.size_of_premises')}}</th>
                    <th>{{trans('rating_summary.no_of_floors')}}</th> Blg. ng Palapag
                    <th>{{trans('rating_summary.premises_owned_leased')}}</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        
                            <td> {{ $entity->address1 }} </td>
                            <td style="text-align: center;"> {{ $location->location_size }} sq.m</td>
                            <td style="text-align: center;"> {{ $location->floor_count }} </td>
                            <td style="text-align: center;"> {{ trans('business_details1.' . $location->location_type) }} </td>
                        
                    </tr>
                </tbody>
              </table>
            
        </div><br>

        <div class="hundred-percent" style="margin: auto;">
            <div class="break-page">
            @if (empty($location->map))
            	<p>Location map not available.</p>
            @else
                <img src="{{ URL::asset('images/locations/'.$location->map) }}" alt="map" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @endif
            
                <div class = 'center' style = 'position: relative; top: -10px;'>
                    @if ($location->other)
                    <p style="text-align:center; padding-top: 5px;">{{ $location->other }}</p>
                    @else
                    <br /><br />
                    @endif
                </div>
            </div>

            @if (empty($location->location_photo))
            	<p>Location photo not available.</p>
            @else
                <img src="{{ URL::asset('images/locations/'.$location->location_photo) }}" alt="map" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @endif

                <div class = 'center' style = 'position: relative; top: -10px;'>
                    <p style="text-align:center;"> {{trans('rating_summary.main_office_of')}} {{ $entity->companyname }}.</p>
                </div>
        </div>
    @endif
    </div>
    @endif

    <script type="text/javascript">

    	if ($('.positive_points li').length == 0){
			$('.positive_points_title').hide();
			

		if ($('.negative_points li').length == 0){
			$('.negative_points_title').hide();
			
		}

		Lang = {!!LanguageHelper::getAll()!!};
		function trans(sData){
			sCommand = sData.split('.');
			return Lang[sCommand[0]][sCommand[1]];
		}
		function convert(sGroup, sKey){
			if(sKey!=null && sKey!=''){
				if(Lang[sGroup][sKey]){
					return Lang[sGroup][sKey];
				}
			}
			return sKey;
		}
	</script>

</body></html>
