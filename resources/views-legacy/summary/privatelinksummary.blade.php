@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')
    @parent
    @include('dashboard.navpl')
@stop

@section('content')
	<div id="page-wrapper">
		{{ Form::hidden('entityids', $entity->entityid, array('id' => 'entityids')) }}
		{{ Form::hidden('loginid', $entity->loginid, array('id' => 'loginid')) }}
		{{ Form::hidden('status', $entity->status, array('id' => 'status')) }}
		{{ Form::hidden('bindex', $entity->industry_score, array('id' => 'bindex')) }}
		{{ Form::hidden('tindex', $entity->industry_trend, array('id' => 'tindex')) }}
		{{ Form::hidden('bgroup', $entity->business_group / 100, array('id' => 'bgroup')) }}
		{{ Form::hidden('mgroup', $entity->management_group / 100, array('id' => 'mgroup')) }}
		{{ Form::hidden('fgroup', $entity->financial_group / 100, array('id' => 'fgroup')) }}
		{{ Form::hidden('nymteam', $entity->number_year_management_team, array('id' => 'nymteam')) }}
		{{ Form::hidden('gross_revenue_growth', $entity->gross_revenue_growth, array('id' => 'gross_revenue_growth')) }}
		{{ Form::hidden('net_income_growth', $entity->net_income_growth, array('id' => 'net_income_growth')) }}
		{{ Form::hidden('gross_profit_margin', $entity->gross_profit_margin, array('id' => 'gross_profit_margin')) }}
		{{ Form::hidden('net_profit_margin', $entity->net_profit_margin, array('id' => 'net_profit_margin')) }}
		{{ Form::hidden('net_cash_margin', $entity->net_cash_margin, array('id' => 'net_cash_margin')) }}
		{{ Form::hidden('current_ratio', $entity->current_ratio, array('id' => 'current_ratio')) }}
		{{ Form::hidden('debt_equity_ratio', $entity->debt_equity_ratio, array('id' => 'debt_equity_ratio')) }}
        {{ Form::hidden('base_year', $base_year, array('id' => 'base_year')) }}

		<div class="panel-body">
			<div class="bs-tab">
				<div class="tab-content">
					@if($bank_standalone==0)
					<div id="ssc" class="tab-pane fade">
						<div class="col-md-3">
							<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
						</div>
						<div class="col-md-8" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
						<div class="col-md-12"><hr/></div>

						<div class="spacewrapper break-page">
						@if(isset($readyratiodata[0]))
                            @if (0 != $readyratiodata[0]->gross_revenue_growth1)
                                {{ Form::hidden('gross_revenue_growth2', (($readyratiodata[0]->gross_revenue_growth2/$readyratiodata[0]->gross_revenue_growth1) - 1) * 100, array('id' => 'gross_revenue_growth22')) }}
                            @else
                                {{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
                            @endif

                            @if (0 != $readyratiodata[0]->net_income_growth1)
                                {{ Form::hidden('net_income_growth2', (($readyratiodata[0]->net_income_growth2/$readyratiodata[0]->net_income_growth1) - 1) * 100, array('id' => 'net_income_growth22')) }}
                            @else
                                {{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
                            @endif
							{{ Form::hidden('gross_profit_margin2', $readyratiodata[0]->gross_profit_margin2, array('id' => 'gross_profit_margin22')) }}
							{{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
							{{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
							{{ Form::hidden('current_ratio2', $readyratiodata[0]->current_ratio2, array('id' => 'current_ratio22')) }}
							{{ Form::hidden('debt_equity_ratio2', $readyratiodata[0]->debt_equity_ratio2, array('id' => 'debt_equity_ratio22')) }}
						@endif
							<div class="row chart-remove-padding">
								<div class="col-lg-12">
									@if($show_bank_rating)
									<input type="hidden" id="bank_rating_score" value="{{$grandscore}}" />
									<div class="rating_panel_switcher">
										<span tab="original" class="active">CreditBPO Rating</span>
										<span tab="bank">Bank Rating</span>
									</div>
									@endif
									<div class="chart-container original_rating_panel">
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div id="title3_3" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.credit_bpo_rating')}}</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div id="description3_3" style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
													<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="float: left; width: 100%;">
											<div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
												<div id="graphs3_3text0000" class="gcircle-container">
													<span id="graphs3_3text000" style="font-size: 35px;"></span>
												</div>
											</div>
											<div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
												<div style="float: left; width: 100%;">
													<p><b>Result: <span id="graphs3_3text0"></span><br/>
													<span id="graphs3_3text00" style = 'font-size: 14px;'></span></b></p>
												</div>
											</div>
										</div>
									</div>
									@if($show_bank_rating)
									<div class="chart-container bank_rating_panel">
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Bank Rating</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
													<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="float: left; width: 100%;">
											<div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
												<div id="graphs3_3text00001" class="gcircle-container">
													<span id="graphs3_3text0001" style="font-size: 35px;"></span>
												</div>
											</div>
											<div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
												<div style="float: left; width: 100%;">
													<p><b>Result: <span id="graphs3_3text01"></span><br/>
													<span id="graphs3_3text001" style = 'font-size: 14px;'></span></b></p>
												</div>
											</div>
										</div>
									</div>
									@endif

									<div class="clearfix"></div>

								</div>
							</div>
							<div class="row chart-remove-padding break-page">
								<div class="col-lg-6">
									<!-- chart box wrapper -->
									<div class="chart-container">
										<div style="margin:0 auto; width: 97%;">
											<div style="float: left; width: 100%;">
												<div id="title1" style="float: left; width: 100%;">
													<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.business_considerations')}}</b></div>
													<div style="float: right; padding-right: 10px;">
														<b>
														<span id="show1" style="display: block; cursor: pointer;">Expand +</span>
														</b>
													</div>
												</div>
											</div>
											<div id="description1" style="display: none;">
												<div style="padding: 10px;">
													@foreach ($finalscore as $finalscoreinfo)
															{{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
															{{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
															{{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
															{{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
															@if ($finalscoreinfo->score_rm == 90)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.rm_high')}}
																	</p>
																	{{ Form::hidden('score_rm_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_rm_value')) }}
															@elseif ($finalscoreinfo->score_rm == 30)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.rm_low')}}
																	</p>
															{{ Form::hidden('score_rm_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_rm_value')) }}
													@endif

															{{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
															@if ($finalscoreinfo->score_cd == 30)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.cd_high')}}
																	</p>
																	{{ Form::hidden('score_cd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_cd_value')) }}
															@elseif ($finalscoreinfo->score_cd == 20)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
																	<br/>{{trans('risk_rating.cd_moderate')}}
																	</p>
															{{ Form::hidden('score_cd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_cd_value')) }}
													@else
																	<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.cd_low')}}
																	</p>
															{{ Form::hidden('score_cd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_cd_value')) }}
													@endif

															{{ Form::hidden('score_sd', $finalscoreinfo->score_sd, array('id' => 'score_sd')) }}
															@if ($finalscoreinfo->score_sd == 30)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.sd_high')}}
																	</p>
																	{{ Form::hidden('score_sd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sd_value')) }}
															@elseif ($finalscoreinfo->score_sd == 20)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
																	<br/>{{trans('risk_rating.sd_moderate')}}
																	</p>
															{{ Form::hidden('score_sd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sd_value')) }}
													@else
																	<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.sd_low')}}
																	</p>
															{{ Form::hidden('score_sd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sd_value')) }}
													@endif

															{{ Form::hidden('score_bois', $finalscoreinfo->score_bois, array('id' => 'score_bois')) }}
															@if ($finalscoreinfo->score_bois == 90)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.bo_high')}}
																	</p>
																	{{ Form::hidden('score_bois_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bois_value')) }}
															@elseif ($finalscoreinfo->score_bois == 60)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
																	<br/>{{trans('risk_rating.bo_moderate')}}
																	</p>
															{{ Form::hidden('score_bois_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_bois_value')) }}
													@else
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.bo_low')}}
																	</p>
															{{ Form::hidden('score_bois_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bois_value')) }}
													@endif

															{{ Form::hidden('score_agroup_total', 0, array('id' => 'score_agroup_total')) }}
															{{ Form::hidden('score_agroup_sum', 240, array('id' => 'score_agroup_sum')) }}

													@endforeach
												</div>
											</div>
											<div style="float: left; width: 98%;">
												<div id="container-pie1" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0 0 10px;"></div>
											</div>
										</div>
									</div><!-- #End chart box wrapper -->
								</div>
								<div class="col-lg-6">
									<!-- chart box wrapper -->
									<div class="chart-container">
										<div style="margin:0 auto; width: 97%;">
											<div style="float: left; width: 100%;">
												<div id="title1" style="float: left; width: 100%;">
													<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.management_quality')}}</b></div>
													<div style="float: right; padding-right: 10px;">
														<b>
														<span id="show2" style="display: block;cursor: pointer;">Expand +</span>
														</b>
													</div>
												</div>
											</div>
											<div id="description2" style="display: none;">
												<div style="padding: 10px;">
													@foreach ($finalscore as $finalscoreinfo)
															{{ Form::hidden('score_sf', $finalscoreinfo->score_sf, array('id' => 'score_sf')) }}
															{{ Form::hidden('score_bdms', $finalscoreinfo->score_bdms, array('id' => 'score_bdms')) }}
															{{ Form::hidden('score_boe', $finalscoreinfo->score_boe, array('id' => 'score_boe')) }}
															{{ Form::hidden('score_mte', $finalscoreinfo->score_mte, array('id' => 'score_mte')) }}
															@if ($finalscoreinfo->score_boe == 21)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
																	{{ Form::hidden('score_owner_exp', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_owner_exp')) }}
															@elseif ($finalscoreinfo->score_boe == 14)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
																	{{ Form::hidden('score_owner_exp', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_owner_exp')) }}
															@elseif ($finalscoreinfo->score_boe == 7)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
																	{{ Form::hidden('score_owner_exp', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_owner_exp')) }}
															@endif
															@if ($finalscoreinfo->score_mte == 21)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
																	{{ Form::hidden('score_mng_exp', '<span style="color: green;"> '.trans('risk_rating.high').'</span>', array('id' => 'score_mng_exp')) }}
															@elseif ($finalscoreinfo->score_mte == 14)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
																	{{ Form::hidden('score_mng_exp', '<span style="color: orange;"> '.trans('risk_rating.moderate').'</span>', array('id' => 'score_mng_exp')) }}
															@elseif ($finalscoreinfo->score_mte == 7)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
																	{{ Form::hidden('score_mng_exp', '<span style="color: red;"> '.trans('risk_rating.low').'</span>', array('id' => 'score_mng_exp')) }}
															@endif
															@if ($finalscoreinfo->score_bdms == 36)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.bd_high')}}
																	</p>
																	{{ Form::hidden('score_bdms_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bdms_value')) }}
															@elseif ($finalscoreinfo->score_bdms == 12)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.bd_low')}}
																	</p>
															{{ Form::hidden('score_bdms_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bdms_value')) }}
													@endif

															{{ Form::hidden('score_sp', $finalscoreinfo->score_sp, array('id' => 'score_sp')) }}
															@if ($finalscoreinfo->score_sp == 36)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.sp_high')}}
																	</p>
																	{{ Form::hidden('score_sp_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sp_value')) }}
															@elseif ($finalscoreinfo->score_sp == 24)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
																	<br/>{{trans('risk_rating.sp_moderate')}}
																	</p>
															{{ Form::hidden('score_sp_value', '<span style="color: orange;"><br/>'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sp_value')) }}
													@else
																	<p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.sp_low')}}
																	</p>
															{{ Form::hidden('score_sp_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sp_value')) }}
													@endif

															{{ Form::hidden('score_ppfi', $finalscoreinfo->score_ppfi, array('id' => 'score_ppfi')) }}
															@if ($finalscoreinfo->score_ppfi == 36)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
																	<br/>{{trans('risk_rating.pp_high')}}
																	</p>
																	{{ Form::hidden('score_ppfi_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_ppfi_value')) }}
															@elseif ($finalscoreinfo->score_ppfi == 12)
																	<p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
																	<br/>{{trans('risk_rating.pp_low')}}
																	</p>
															{{ Form::hidden('score_ppfi_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_ppfi_value')) }}
													@endif

															{{ Form::hidden('score_bgroup_total', 0, array('id' => 'score_bgroup_total')) }}
															{{ Form::hidden('score_bgroup_sum', 150, array('id' => 'score_bgroup_sum')) }}

													@endforeach
												</div>
											</div>
											<div style="float: left; width: 98%;">
													<div id="container-pie2" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0px 0 10px;"></div>
											</div>
										</div>
									</div><!-- #End chart box wrapper -->
								</div>
							</div>
							<div class="row chart-remove-padding">
                                <div class="col-lg-6 col-sm-6">
									<div class="chart-container">
										<div style="padding: 5px 0px; width: 100%; height: 35px">
											<div id="title4" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div id="description3" style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
														<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="width: 100%; height: 390px; overflow: hidden;">

											<div style="font-size: 14px;">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
														<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
													</tr>
													<tr>
														<td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
														<td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
													</tr>
												</table>
											</div>
											<div style="text-align: center;">
												<div id="container-semi" style="min-width: 150px; height: 250px; max-width: 100%; margin: 0 auto"></div>
                                                <div 'fc-description' style="font-size: 14px;; min-width: 150px; ">
                                                    @if ($finalscoreinfo->score_facs >= 162)
                                                        Capacity to meet financial commitments on financial obligations are very strong.
                                                    @elseif ($finalscoreinfo->score_facs >= 145)
                                                        Capacity to meet financial commitments on financial obligations are very strong.
                                                    @elseif ($finalscoreinfo->score_facs >= 127)
                                                        Capacity to meet financial obligations is still strong but somewhat more susceptible to the adverse effects of changes in business and economic conditions.
                                                    @elseif ($finalscoreinfo->score_facs >= 110)
                                                        Exhibits adequate protection parameters. However, adverse economic conditions or changing circumstances are more likely to lead to a weakened capacity to meet debt payments or loan covenants.
                                                    @elseif ($finalscoreinfo->score_facs >= 92)
                                                        Currently has the capacity to meet financial commitment on loans. However, adverse business, financial, or economic conditions will likely impair the capacity or willingness to meet debt obligations.
                                                    @elseif ($finalscoreinfo->score_facs >= 75)
                                                        Faces major ongoing uncertainties or exposure to adverse business, financial, or economic conditions which could lead to the obligor's inadequate capacity to meet debt obligations.​
                                                    @elseif ($finalscoreinfo->score_facs >= 57)
                                                        Currently vulnerable to nonpayment, and is dependent upon favorable business, financial, and economic conditions in order to meet financial obligations. Not likely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                    @elseif ($finalscoreinfo->score_facs >= 40)
                                                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                    @elseif ($finalscoreinfo->score_facs >= 22)
                                                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                    @elseif ($finalscoreinfo->score_facs >= 4)
                                                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                    @else
                                                        High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                    @endif
                                                </div>
											</div>
										</div>
									</div>


								</div>

								<div class="col-lg-6 col-sm-6">
									<div class="chart-container">


										<div style="width: 100%; height: 429px; font-size: 14px;">
											<table class="rating-summary-padding-table" cellpadding="0" cellspacing="0" border="0" width="100%">

												<tr>
													<td>&nbsp;</td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cc_year1 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cc_year2 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cc_year3 : "" }}</span></td>
												</tr>
												<tr>
													<td>{{trans('risk_rating.fc_receivable')}}</td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->receivables_turnover1 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->receivables_turnover2 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->receivables_turnover3 : "" }}</span></td>
												</tr>
												<tr>
													<td>+ {{trans('risk_rating.fc_inventory')}}</td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->inventory_turnover1 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->inventory_turnover2 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->inventory_turnover3 : "" }}</span></td>
												</tr>
												<tr>
													<td>- {{trans('risk_rating.fc_payable')}}</td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->accounts_payable_turnover1 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->accounts_payable_turnover2 : "" }}</span></td>
													<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->accounts_payable_turnover3 : "" }}</span></td>
												</tr>
												<tr>
													<td><b>= {{trans('risk_rating.fc_ccc')}}</b></td>
													<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cash_conversion_cycle1 : "" }}</span></b></td>
													<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cash_conversion_cycle2 : "" }}</span></b></td>
													<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cash_conversion_cycle3 : "" }}</span></b></td>
												</tr>
												<tr>
													<td colspan="4">&nbsp;</td>
												</tr>
												<tr>
													<td>Average Daily Balance</td>
													<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($finalscore[0]->average_daily_balance, 2, '.', ',') }}</span></td>
												</tr>
												<tr>
													<td>Operating Cashflow Margin</td>
													<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_margin }} %</span></td>
												</tr>
												<tr>
													<td>Operating Cashflow Ratio</td>
													<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_ratio }}</span></td>
												</tr>
												<tr>
													<td>ADB Derived Loan Amount</td>'
													<?php
														$loan_amount = ( $finalscore[0]->average_daily_balance * 0.3 * 360 ) / ( 181 * 0.04900416667 );
													?>
													<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($loan_amount, 2, '.', ',') }}</span></td>
												</tr>
											</table>
										</div>

									</div>
								</div>

                                @if ((STR_EMPTY != $finalscoreinfo->positive_points) || (STR_EMPTY != $finalscoreinfo->negative_points))
                                <div class="col-lg-12 col-sm-12">
									<div class="chart-container">

										<div style="width: 100%; font-size: 11px;">
											<?php
                                                $pos_exp    = explode('|', $finalscoreinfo->positive_points);
                                                $neg_exp    = explode('|', $finalscoreinfo->negative_points);
                                            ?>

                                            @if (STR_EMPTY != $finalscoreinfo->positive_points)
                                                @if (STR_EMPTY != $finalscoreinfo->negative_points)
                                                <div class = 'col-md-6'>
                                                @else
                                                <div class = 'col-md-12'>
                                                @endif
                                                    <span class="divider1"></span>
                                                    <div id="positive-points">
                                                        <b style = 'font-size: 16px;'> The Good </b>
                                                        <ul>
                                                        @foreach ($pos_exp as $positive)
                                                            <li> {{ $positive }} </li>
                                                        @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif

                                            @if (STR_EMPTY != $finalscoreinfo->negative_points)
                                                @if (STR_EMPTY != $finalscoreinfo->positive_points)
                                                <div class = 'col-md-6'>
                                                @else
                                                <div class = 'col-md-12'>
                                                @endif
                                                    <span class="divider1"></span>
                                                    <div id="negative-points">
                                                        <b style = 'font-size: 16px;'> The Bad </b>
                                                        <ul>
                                                        @foreach ($neg_exp as $negative)
                                                            <li> {{ $negative }} </li>
                                                        @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
										</div>

									</div>
								</div>
                                @endif

							</div>
                            <div class="clearfix"></div>

							<div class="row chart-remove-padding">
								<div class="col-lg-6 col-sm-6">
									<div class="chart-container" style = 'min-height: 375px;'>
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div id="title4" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
												<div style="float: right; padding-right: 10px; text-align: right;">
													<b>
													<span id="show4" style="display: block;cursor: pointer;">Expand +</span>
													</b>
													<label>Regional GDP Modifier
													{{Form::select('grdp_select', array(''=>'None') + $grdp_year, '', array('id'=>'grdp_select'))}}
													</label>
													<div class="grdp_display"></div>
												</div>
											</div>
										</div>
										<div id="description4" style="display: none;">
											<div style="padding: 10px;">
												<span class="description-bar"></span>
											</div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>

									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-6 col-sm-6">
									<div class="chart-container" style = 'min-height: 375px;'>
										<div style="float: left; width: 98%;">
											<div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div class="legend">
											<div class="legend-item">
												<span class="legend-info"></span>
												<span>You</span>
											</div>
											<div class="legend-item">
												<span class="legend-info industry"></span>
												<span>Industry</span>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
                            <p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
							<br/>
							<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
							<p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
							<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

							<input type="button" id="finananalysis_next" value="Next" />

						</div>
					</div>
					@else
					<div id="ssc" class="tab-pane fade">
						@foreach ($finalscore as $finalscoreinfo)
						{{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
						{{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
						{{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
						{{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
						{{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
                        @endforeach

						<div class="row col-md-4">
							<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
						</div>
						<div class="col-md-7" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
						<div class="col-md-12"><hr/></div>

						<div class="spacewrapper break-page">
							@if ($entity->status == 7)
								@if(isset($readyratiodata[0]))
									@if (0 != $readyratiodata[0]->gross_revenue_growth1)
										{{ Form::hidden('gross_revenue_growth2', (($readyratiodata[0]->gross_revenue_growth2/$readyratiodata[0]->gross_revenue_growth1) - 1) * 100, array('id' => 'gross_revenue_growth22')) }}
									@else
										{{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
									@endif

									@if (0 != $readyratiodata[0]->net_income_growth1)
										{{ Form::hidden('net_income_growth2', (($readyratiodata[0]->net_income_growth2/$readyratiodata[0]->net_income_growth1) - 1) * 100, array('id' => 'net_income_growth22')) }}
									@else
										{{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
									@endif
									{{ Form::hidden('gross_profit_margin2', $readyratiodata[0]->gross_profit_margin2, array('id' => 'gross_profit_margin22')) }}
									{{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
									{{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
									{{ Form::hidden('current_ratio2', $readyratiodata[0]->current_ratio2, array('id' => 'current_ratio22')) }}
									{{ Form::hidden('debt_equity_ratio2', $readyratiodata[0]->debt_equity_ratio2, array('id' => 'debt_equity_ratio22')) }}
								@endif
							<div class="row chart-remove-padding">
                                <div class="col-lg-12 col-sm-12">
									<div class="chart-container">
										<div style="padding: 5px 0px; width: 100%; height: 35px">
											<div id="title4" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div id="description3" style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
														<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="width: 100%; overflow: hidden;">

											<div>
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style = 'font-size: 11px;'>
													<tr>
														<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
														<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
													</tr>
													<tr>
														<td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
														<td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
													</tr>
												</table>
											</div>
											<div style="text-align: center; font-size: 14px;">
												<div id="container-semi" style="min-width: 150px; height: 300px; max-width: 100%; margin: 0 auto"></div>
                                                <div 'fc-description' style="min-width: 150px; max-width: 100%; margin: 0 auto; padding-left: 75px; padding-right: 75px; padding-bottom: 40px;">
                                                @if ($finalscoreinfo->score_facs >= 162)
                                                    Capacity to meet financial commitments on financial obligations are very strong.
                                                @elseif ($finalscoreinfo->score_facs >= 145)
                                                    Capacity to meet financial commitments on financial obligations are very strong.
                                                @elseif ($finalscoreinfo->score_facs >= 127)
                                                    Capacity to meet financial obligations is still strong but somewhat more susceptible to the adverse effects of changes in business and economic conditions.
                                                @elseif ($finalscoreinfo->score_facs >= 110)
                                                    Exhibits adequate protection parameters. However, adverse economic conditions or changing circumstances are more likely to lead to a weakened capacity to meet debt payments or loan covenants.
                                                @elseif ($finalscoreinfo->score_facs >= 92)
                                                    Currently has the capacity to meet financial commitment on loans. However, adverse business, financial, or economic conditions will likely impair the capacity or willingness to meet debt obligations.
                                                @elseif ($finalscoreinfo->score_facs >= 75)
                                                    Faces major ongoing uncertainties or exposure to adverse business, financial, or economic conditions which could lead to the obligor's inadequate capacity to meet debt obligations.​
                                                @elseif ($finalscoreinfo->score_facs >= 57)
                                                    Currently vulnerable to nonpayment, and is dependent upon favorable business, financial, and economic conditions in order to meet financial obligations. Not likely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                @elseif ($finalscoreinfo->score_facs >= 40)
                                                    High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                @elseif ($finalscoreinfo->score_facs >= 22)
                                                    High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                @elseif ($finalscoreinfo->score_facs >= 4)
                                                    High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                @else
                                                    High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                                                @endif
                                                </div>
											</div>

                                            @if ((STR_EMPTY != $finalscoreinfo->positive_points) || (STR_EMPTY != $finalscoreinfo->negative_points))
                                            <div style = 'font-size: 14px;' class = 'col-md-12'>
                                                <?php
                                                    $pos_exp    = explode('|', $finalscoreinfo->positive_points);
                                                    $neg_exp    = explode('|', $finalscoreinfo->negative_points);
                                                ?>

                                                @if (STR_EMPTY != $finalscoreinfo->positive_points)
                                                    @if (STR_EMPTY != $finalscoreinfo->negative_points)
                                                    <div class = 'col-md-6'>
                                                    @else
                                                    <div class = 'col-md-12'>
                                                    @endif
                                                        <span class="divider1"></span>
                                                        <div id="positive-points">
                                                            <b style = 'font-size: 16px;'> The Good </b>
                                                            <ul>
                                                            @foreach ($pos_exp as $positive)
                                                                <li> {{ $positive }} </li>
                                                            @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if (STR_EMPTY != $finalscoreinfo->negative_points)
                                                    @if (STR_EMPTY != $finalscoreinfo->positive_points)
                                                    <div class = 'col-md-6'>
                                                    @else
                                                    <div class = 'col-md-12'>
                                                    @endif
                                                        <span class="divider1"></span>
                                                        <div id="negative-points">
                                                            <b style = 'font-size: 16px;'> The Bad </b>
                                                            <ul>
                                                            @foreach ($neg_exp as $negative)
                                                                <li> {{ $negative }} </li>
                                                            @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
											</div>
                                            @endif
										</div>
									</div>
								</div>
							</div>
                            <div class="clearfix"></div>
							@if($forecast_data != null)
							<div class="row chart-remove-padding">
                                <div class="col-lg-12 col-sm-12">
									<div class="chart-container">
										<p><strong>Growth Forecast</strong></p>
										<script>var forecast_data = {{json_encode($forecast_data)}};</script>
										<div id="forecast_graph"></div>
										<div>
											<p style = 'font-size: 14px;'> {{ $forecast_data['description'] }} </p>
										</div>
									</div>
								</div>
							</div>
							@endif
                            <div class="clearfix"></div>
							<div class="row chart-remove-padding">
								<div class="col-lg-6 col-sm-6">
									<div class="chart-container" style = 'min-height: 375px;'>
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div id="title4" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
												<div style="float: right; padding-right: 10px; text-align: right;">
													<b>
													<span id="show4" style="display: block;cursor: pointer;">Expand +</span>
													</b>
													<!--label>{{trans('risk_rating.gdp')}}
													{{Form::select('grdp_select', array(''=>'None') + $grdp_year, '', array('id'=>'grdp_select'))}}
													</label-->
													<div class="grdp_display">Region: {{ $entity-> region }}</div>
												</div>
											</div>
										</div>
										<div id="description4" style="display: none;">
											<div style="padding: 10px;">
												<span class="description-bar"></span>
											</div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>

									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-6 col-sm-6">
									<div class="chart-container" style = 'min-height: 375px;'>
										<div style="float: left; width: 98%;">
											<div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div style="float: left; width: 98%;">
											<div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
										</div>
										<div class="legend">
											<div class="legend-item">
												<span class="legend-info"></span>
												<span>You</span>
											</div>
											<div class="legend-item">
												<span class="legend-info industry"></span>
												<span>Industry</span>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
                            <p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
							<br/>
							<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
							<p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
							<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

							@endif

							<input type="button" id="finananalysis_next" value="Next" />

						</div>
					</div>
					@endif
					<div id="fianalysis" class="tab-pane fade">
						<div class="spacewrapper">
							<div class="col-md-4">
								<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" width="100%" style="margin-bottom:5px;" />
							</div>
							<div class="col-md-8" style="margin-top:60px;padding-left:60px;font-size:18px;">Financial Analysis Report for {{ $entity->companyname }}</div>
							<div class="col-md-12"><hr/></div>

                            @foreach ($documents9 as $documents9info)
                            <?php
                                $fname_exp = explode('.', $documents9info->document_rename);

                            ?>

                            @if ('pdf' == $fname_exp[1])
							<object data="{{ URL::to('/') }}/financial_analysis/{{ $documents9info->document_rename }}" type="application/pdf" width="100%" height="1000px">
                                <div class="col-md-12">
                                    <p>
                                        It appears you don't have a PDF plugin for this browser.
                                        No biggie... you can
                                        <a href="{{ URL::to('/') }}/download/financial_analysis/{{ $documents9info->document_rename }}">click here to download the PDF file.</a>
                                    </p>
                                </div>
							</object>
                            @else
                            <div class="col-md-12">
                                <p>
                                    It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can
                                    <a href="{{ URL::to('/') }}/download/financial_analysis/{{ $documents9info->document_rename }}">click here to download the PDF file.</a>
                                </p>
                            </div>
                            @endif
							@endforeach

							<div class="col-md-12">
                                <p style="font-size: 11px;">{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
                                <p style="font-size: 12px;"><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
                                <p style="font-size: 11px;">{{trans('risk_rating.copyright')}}</p>
                            </div>

                            <div class="col-md-12">
                                <input type="button" id="sscore_prev" value="Previous" />
                                @if($bank_standalone==0)
                                <input type="button" id="sscore_next" value="Next" />
                                @endif
                            </div>

						</div>
					</div>
					@if($bank_standalone==0)
					<div id="strategicprofile" class="tab-pane fade">
						<div class="spacewrapper">
							<div class="row">
								@if($forecast_data != null)
								<script>var forecast_data = {{json_encode($forecast_data)}};</script>
								<div class="col-md-12">
									<div class="read-only">
										<h4>Growth Forecast</h4>
										<div class="read-only-text">
											<div class="row">
												<div class="col-sm-8">
													<div id="forecast_graph"></div>
												</div>
												<div class="col-sm-4">
													<p style = 'font-size: 14px;'> {{$forecast_data['description']}} </p>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif
								<div class="col-md-4">
									<div class="read-only">
										<h4>{{trans('business_details2.bdriver')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.bdriver_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.bdriver_hint', array(), 'fil')}}</i>">More info...</a></h4>
										<div class="read-only-text">
											<div class="businessdriver">
												<div class="details-row hidden-xs hidden-sm">
													<div class="col-md-4 col-sm-4">
														<b>{{ trans('business_details2.bdriver_name') }}</b>
													</div>

													<div class="col-md-8 col-sm-8">
														<b>{{ trans('business_details2.bdriver_share') }}</b>
													</div>
												</div>

												@foreach ($bizdrivers as $driver)
												<div class="details-row hidden-xs hidden-sm">
													<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
															{{ $driver->businessdriver_name }}
														</span>
													</div>

													<div class="col-md-8 col-sm-8">
														<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
															{{ $driver->businessdriver_total_sales }}%
														</span>
													</div>
												</div>

												<div class="details-row visible-sm-block visible-xs-block">
													<div class="col-md-4">
														<b>{{ trans('business_details2.bdriver_name') }}</b><br/>
														<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
															{{ $driver->businessdriver_name }}
														</span>
													</div>

													<div class="col-md-8">
														<b>{{ trans('business_details2.bdriver_share') }}</b><br />
														<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
															{{ $driver->businessdriver_total_sales }}%
														</span>
													</div>

												</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="read-only">
										<h4>{{trans('business_details2.customer_segment')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.cs_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.cs_hint', array(), 'fil')}}</i>">More info...</a></h4>
										<div class="read-only-text">
											<div class="businesssegment">
												@foreach ($bizsegments as $bizsegment)
												<div class="details-row">
													<div class="col-md-12">
														<span class = 'editable-field' data-name = 'bizsegment.businesssegment_name' data-field-id = '{{ $bizsegment->businesssegmentid }}'>
															{{ $bizsegment->businesssegment_name }}
														</span>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="read-only">
										<h4>{{ trans('condition_sustainability.revenue_growth') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ Lang::get('condition_sustainability.expected_revenue_3years', array(), 'en') }}<hr/>{{ Lang::get('condition_sustainability.expected_revenue_3years', array(), 'fil') }}">More info...</a></h4>
										<div class="read-only-text">
											@foreach ($revenuepotential as $revenuepotentialinfo)
												<span class="details-row">
													<div class="col-md-8">{{ trans('condition_sustainability.under_current_market') }}</div>
													<div class="col-md-4">
													@if ($revenuepotentialinfo->current_market == 1)
														0%
													@elseif ($revenuepotentialinfo->current_market == 2)
														{{ trans('condition_sustainability.less_3_percent') }}
													@elseif ($revenuepotentialinfo->current_market == 3)
														{{ trans('condition_sustainability.between_3to5_percent') }}
													@else
														{{ trans('condition_sustainability.greater_5_percent') }}
													@endif
													</div>
												</span>
												<span class="details-row">
													<div class="col-md-8">Expected Future Market Conditions</div>
													<div class="col-md-4">
													@if ($revenuepotentialinfo->new_market == 1)
														0%
													@elseif ($revenuepotentialinfo->new_market == 2)
														{{ trans('condition_sustainability.less_3_percent') }}
													@elseif ($revenuepotentialinfo->new_market == 3)
														{{ trans('condition_sustainability.between_3to5_percent') }}
													@else
														{{ trans('condition_sustainability.greater_5_percent') }}
													@endif
													</div>
												</span>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-7">

									<div class="read-only">
										<h4>{{trans('business_details2.past_proj_completed')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.ppc_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.ppc_hint', array(), 'fil')}}</i>">More info...</a></h4>
										<div class="read-only-text">
											<div class="pastprojectscompleted">
												<div class="details-row hidden-sm hidden-xs">
													<div class="col-md-3 col-sm-4">
														<b>{{ trans('business_details2.ppc_purpose') }}</b>
													</div>

													<div class="col-md-2 col-sm-2">
														<b>{{ trans('business_details2.ppc_cost_short') }}</b>
													</div>

													<div class="col-md-3 col-sm-2">
														<b>{{ trans('business_details2.ppc_source_short') }}</b>
													</div>

													<div class="col-md-2 col-sm-1">
														<b>{{ trans('business_details2.ppc_year_short') }}</b>
													</div>

													<div class="col-md-2 col-sm-3">
														<b>{{ trans('business_details2.ppc_result_short') }}</b>
													</div>
												</div>

												@foreach ($pastprojects as $past_proj)
												<div class="details-row hidden-sm hidden-xs">
													<div class="col-md-3 col-sm-4">
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ $past_proj->projectcompleted_name }}
														</span>
													</div>

													<div class="col-md-2 col-sm-2">
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ number_format($past_proj->projectcompleted_cost, 2) }}
														</span>
													</div>
													<div class="col-md-3 col-sm-2">
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
														</span>
													</div>
													<div class="col-md-2 col-sm-1">
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ $past_proj->projectcompleted_year_began }}
														</span>
													</div>
													<div class="col-md-2 col-sm-3">
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
														</span>
														<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="{{ $past_proj->projectcompleted_name }}" group="past_projects" item="past_project_{{ $past_proj->projectcompletedid }}"></span>
													</div>
												</div>

												<div class="details-row visible-sm-block visible-xs-block">
													<div class="col-md-5">
														<b>{{ trans('business_details2.ppc_purpose') }}</b><br/>
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ $past_proj->projectcompleted_name }}
														</span>
													</div>

													<div class="col-md-1">
														<b>{{ trans('business_details2.ppc_cost') }}</b><br/>
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ number_format($past_proj->projectcompleted_cost, 2) }}
														</span>
													</div>

													<div class="col-md-2">
														<b>{{ trans('business_details2.ppc_source') }}</b><br/>
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
														</span>
													</div>

													<div class="col-md-1">
														<b>{{ trans('business_details2.ppc_year') }}</b><br/>
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ $past_proj->projectcompleted_year_began }}
														</span>
													</div>

													<div class="col-md-3">
														<b>{{ trans('business_details2.ppc_result') }}</b><br/>
														<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
															{{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
														</span>
														<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="{{ $past_proj->projectcompleted_name }}" group="past_projects" item="past_project_{{ $past_proj->projectcompletedid }}"></span>
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>

									<div class="read-only">
										<h4>{{trans('business_details2.future_growth_initiatives')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{Lang::get('business_details2.fgi_hint', array(), 'en')}}<hr/><i>{{Lang::get('business_details2.fgi_hint', array(), 'fil')}}</i>">More info...</a></h4>
										<div class="read-only-text">
											<div class="futuregrowthinitiative">
												<div class="details-row hidden-sm hidden-xs">
													<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_purpose') }}</b></div>
													<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_cost') }}</b></div>
													<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_proj_goal') }}</b></div>
												</div>

												@foreach ($futuregrowths as $future_init)
												<div class="details-row hidden-sm hidden-xs">
													<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'fgi.futuregrowth_name' data-field-id = '{{ $future_init->futuregrowthid }}'>
															{{ $future_init->futuregrowth_name }}
														</span>
													</div>

													<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'fgi.futuregrowth_estimated_cost' data-field-id = '{{ $future_init->futuregrowthid }}'>
															{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
														</span>
													</div>
													<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'fgi.planned_proj_goal' data-field-id = '{{ $future_init->futuregrowthid }}'>
															{{ trans('business_details2.'.$future_init->planned_proj_goal) }}
														</span>
														by
														<span class = 'editable-field' data-name = 'fgi.planned_goal_increase' data-field-id = '{{ $future_init->futuregrowthid }}'>
															{{ $future_init->planned_goal_increase }}%
														</span>
														beginning
														<span class = 'editable-field' data-name = 'fgi.proj_benefit_date' data-field-id = '{{ $future_init->futuregrowthid }}'>
															{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
														</span>
														<style id = 'dp-style'></style>
													</div>
												</div>

												<div class="details-row visible-sm-block visible-xs-block">
													<div class="col-md-5">
														<b>{{ trans('business_details2.fgi_purpose') }}</b><br/>
														{{ $future_init->futuregrowth_name }}
													</div>

													<div class="col-md-2">
														<b>{{ trans('business_details2.fgi_cost') }}</b><br/>
														{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
													</div>

													<div class="col-md-2">
														<b>{{ trans('business_details2.fgi_date') }}</b><br/>
														{{ date("F d, Y", strtotime($future_init->futuregrowth_implementation_date)) }}
													</div>

													<div class="col-md-3">
														<b>{{ trans('business_details2.fgi_source') }}</b><br/>
														{{ trans('business_details2.'.$future_init->futuregrowth_source_capital) }}
													</div>

													<div class="col-md-3 col-sm-3">
														{{ trans('business_details2.'.$future_init->planned_proj_goal) }} by
														{{ $future_init->planned_goal_increase }}% beginning
														{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
													</div>

												</div>
												@endforeach
											</div>
										</div>
									</div>

								</div>
								<div class="col-md-5">
									<div class="read-only">
										<h4>{{trans('rcl.rcl')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('rcl.rcl_hint')}}">More info...</a></h4>
										<div class="read-only-text">
											@foreach ($planfacilityrequested as $planfacilityinfo)
												<p><b>{{trans('rcl.requested_cf')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.rcl_hint')}}">More info...</a></p>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.amount_of_line')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->plan_credit_availment }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.purpose_of_cf')}}</div>
													<div class="col-md-8">
													@if ($planfacilityinfo->purpose_credit_facility == 1)
															{{trans('rcl.working_capital')}}
													@elseif ($planfacilityinfo->purpose_credit_facility == 2)
															{{trans('rcl.expansion')}}
													@elseif ($planfacilityinfo->purpose_credit_facility == 3)
															{{trans('rcl.takeout')}}
													@else
															{{trans('rcl.other_specify')}}
													@endif
													</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.purpose_of_cf_others')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->purpose_credit_facility_others }}</div>
												</span>
												<p><b>{{trans('rcl.offered_collateral')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.offered_collateral_hint')}}">More info...</a></p>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.type_of_deposit')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->cash_deposit_details }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.deposit_amount')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->cash_deposit_amount }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.marketable_securities')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->securities_details }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.marketable_securities_value')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->securities_estimated_value }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.property')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->property_details }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.property_value')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->property_estimated_value }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.chattel')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->chattel_details }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.chattel_value')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->chattel_estimated_value }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.others')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->others_details }}</div>
												</span>
												<span class="details-row">
													<div class="col-md-4">{{trans('rcl.others_value')}}</div>
													<div class="col-md-8">{{ $planfacilityinfo->others_estimated_value }}</div>
												</span>
											@endforeach
										</div>
									</div>
								</div>
							</div>

							<input type="button" id="strategicprofile_prev" value="Previous" />

						</div>
					</div>
					@endif
			</div>
		</div>
	</div>

@section('javascript')
    <script src="{{ URL::asset('js/jquery.js') }}"></script>
	<script src="{{ URL::asset('js/privatelink.js') }}"></script>
	<script src="{{ URL::asset('js/chart-min.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison2.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison3.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison4.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-industry-comparison5.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
	<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
    <script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
	<script src="{{ URL::asset('js/graphs-strategic-forecast.js') }}"></script>
@stop
