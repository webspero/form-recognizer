@extends('layouts.master')

@section('header')
@parent
	<title>CreditBPO - Summary</title>
@stop



@section('content')

<div id="page-wrapper">

	@section('nav')
	@parent
	@include('dashboard.nav')
	@stop
	
	<input type="hidden" name="type_report" id="type_report" value="{{$entity->is_premium}}">
	{{-- if the user is a trial, the report is finished and it is older than 2 weeks - dont show it --}}
	@if (
	$login->trial_flag == 1 &&
	$entity->status == 7 &&
	$entity->completed_date &&
	\Carbon\Carbon::parse($entity->completed_date) < Carbon\Carbon::today()->subWeeks(2) &&
	Auth::user()->role != 0
	)
		<h1>Trial account</h1>
		<p>You may no longer load this report while in Trial Mode. You must become a customer by purchasing a report in order to see reports created during Trial Mode.</p>
	@else
	<div class="row">

		<input type="hidden" name="role" id="role" value="{{ Auth::user()->role }}">
		<div class="text-right matching_checkbox">
			<!-- <input type="checkbox" name="matching" id="matching" @php echo ($entity->matching_optin == 'Yes') ? 'checked': '';  @endphp /> Matching -->
			<input type="hidden" name="entityid" id="entityid" value="{{ $entity->entityid}}" />
		</div>
		
		<div class="buttonwrapper">
			 <?php $url = explode("/", URL::previous()); ?>
			 @if (is_countable($url) && count($url) >= 6)
			 	<input type="hidden" id="tabname" value="{{ $url[5] }}" />
			 @else
				@if ($entity->status == 7)
					<input type="hidden" id="tabname" value="tab9" />
				@else
					<input type="hidden" id="tabname" value="tab1" />
				@endif
			 @endif
			 
			 @if ($entity->status == 1 and Auth::user()->role == 2)
				 {{ Form::open(['route' => ['postCreditStartInvestigation', $entity->entityid], 'method' => 'post']) }}
				 <button type="submit">Start Investigation </button>
				 {{ Form::close() }}
			 @endif

			 @if ($entity->status == 5 and Auth::user()->role == 3)
				 {{ Form::open(['route' => ['postStartScoring', $entity->entityid], 'method' => 'post']) }}
				 <button type="submit">Start Risk Scoring</button>
				 {{ Form::close() }}
			 @endif
			 @if ($entity->status == 6 and Auth::user()->role == 3)
				 {{ Form::open(['route' => ['postReleaseScoring', $entity->entityid], 'method' => 'delete']) }}
				 <button type="submit">Release</button>
				 {{ Form::close() }}
			 @endif
			 @if (Auth::user()->role == 3)
				 <a id="emailthis" href="{{ URL::to('/') }}/sme/request-additional-info/{{ $entity->entityid }}">Request Additional Information from Company</a>
				 @if ($entity->status == 6)
					 @if($fa_report!=null)
						 <p style="text-align: left;">
						  <a id="generate-fspdf" href="{{ URL::to('/') }}/financial-report/download/{{ $fa_report->id }}">Generate Financial Condition Report</a>
						</p>
						@if(Session::has('fs_gen_err'))
							<div style = 'position: relative; float: left; color: red; top: 32px; left: 38%'>
							  <small>{{ Session::get('fs_gen_err') }}</small>
							</div>
						@endif
					@endif
				@endif
			@endif
		</div>
</div>

<div class="page-title clearfix">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12">
		@if (1 == $entity->anonymous_report)
			@if (1 == Auth::user()->role)
				<span style="font-size: 17px;font-weight: bold; display: none;" class = 'hdr-company-name'>{{ $entity->companyname }}</span>
			@else
				<span style="font-size: 17px;font-weight: bold;" class = 'hdr-company-name'>Company - {{ $entity->entityid }}</span>
			@endif
		@else
		<span style="font-size: 17px;font-weight: bold;" class = 'hdr-company-name'>{{ $entity->companyname }}</span>
		@endif
		@if(!Session::has('tql'))
			@if(Auth::user()->role == 1)
				<div class="progressbar-container">
					<div class="progressbar-count">100% of Questionnaire is Complete:</div>
					<div class="progressbar-wrap">
						<div class="progressbar-empty"></div>
						<div class="progressbar-full"></div>
					</div>
				</div>
			@endif
		@endif

		@if(Auth::user()->role == 0 && $entity->status == 7 && $entity->admin_review_flag == 1)
			<a href="{{ URL::to('admin/publish/'.$entity->entityid) }}" class="bluebutton">Publish</a>
			<a href="{{ URL::to('admin/redact/'.$entity->entityid) }}" class="bluebutton">Redact</a>
		@endif
		</div>
	</div>
</div>

{{ Form::hidden('auth_role', Auth::user()->role, array('id' => 'auth_role')) }}
{{ Form::hidden('entityids', $entity->entityid, array('id' => 'entityids')) }}
{{ Form::hidden('loginid', $entity->loginid, array('id' => 'loginid')) }}
{{ Form::hidden('status', $entity->status, array('id' => 'status')) }}
{{ Form::hidden('entity_type', $entity->entity_type, array('id' => 'entity_type')) }}
{{ Form::hidden('clogidid', Auth::user()->loginid, array('id' => 'clogidid')) }}
{{ Form::hidden('userid', Auth::user()->role, array('id' => 'userid')) }}
{{ Form::hidden('bindex', $entity->industry_score, array('id' => 'bindex')) }}
{{ Form::hidden('tindex', $entity->industry_trend, array('id' => 'tindex')) }}
{{ Form::hidden('bgroup', $entity->business_group / 100, array('id' => 'bgroup')) }}
{{ Form::hidden('mgroup', $entity->management_group / 100, array('id' => 'mgroup')) }}
{{ Form::hidden('fgroup', $entity->financial_group / 100, array('id' => 'fgroup')) }}
{{ Form::hidden('nymteam', $entity->number_year_management_team, array('id' => 'nymteam')) }}
{{ Form::hidden('gross_revenue_growth', $entity->gross_revenue_growth, array('id' => 'gross_revenue_growth')) }}
{{ Form::hidden('net_income_growth', $entity->net_income_growth, array('id' => 'net_income_growth')) }}
{{ Form::hidden('gross_profit_margin', $entity->gross_profit_margin, array('id' => 'gross_profit_margin')) }}
{{ Form::hidden('net_profit_margin', $entity->net_profit_margin, array('id' => 'net_profit_margin')) }}
{{ Form::hidden('net_cash_margin', $entity->net_cash_margin, array('id' => 'net_cash_margin')) }}
{{ Form::hidden('current_ratio', $entity->current_ratio, array('id' => 'current_ratio')) }}
{{ Form::hidden('debt_equity_ratio', $entity->debt_equity_ratio, array('id' => 'debt_equity_ratio')) }}
{{ Form::hidden('bank_ci_view', $bank_ci_view, array('id' => 'bank_ci_view')) }}
{{ Form::hidden('sme_supervisor', $entity->current_bank, array('id' => 'sme_supervisor')) }}
{{ Form::hidden('trial_flag', Auth::user()->trial_flag, array('id' => 'trial_flag')) }}
{{ Form::hidden('base_year', $base_year, array('id' => 'base_year')) }}
{{ Form::hidden('report_anonymous', $entity->anonymous_report, array('id' => 'report_anonymous')) }}
{{ Form::hidden('local_rate', $local_rate->value, array('id' => 'local_rate')) }}
{{ Form::hidden('foreign_rate', $foreign_rate->value, array('id' => 'foreign_rate')) }}

@if ($financialChecker)
	{{ Form::hidden('financialChecker', $financialChecker, array('id' => 'financialChecker')) }}
	{{ Form::hidden('standalone_bank', $finalRatingScore[3], array('id' => 'standalone_bank')) }}
	{{ Form::hidden('fpr', $finalRatingScore[0], array('id' => 'fpr')) }}
	{{ Form::hidden('fpm', $finalRatingScore[1], array('id' => 'fpm')) }}
	{{ Form::hidden('financialTotal', $finalRatingScore[2], array('id' => 'financialTotal')) }}
@endif

@if ($entity->status != 7)
	{{ Form::hidden('score_sf', '0 - No Value', array('id' => 'score_sf')) }}
@endif

@if ($investigator_notes != null && $investigator_notes->status == 1)
	{{ Form::hidden('done_with_investigation', 'true', array('id' => 'done_with_investigation')) }}
@endif

@for ($i = 1; $i <= 6; $i++)
	{{ Form::hidden('fback_tab'.$i, $feedback_tab_val[$i], array('data-tab-type' => $i, 'data-tab-name' => $feedback_tab_name[$i])) }}
@endfor

<div class="panel-body">
 @if(Session::has('tql'))
	<?php $tql = json_decode(Session::get('tql_data', true)); ?>
	<span style="color: RED">Please make sure to answer all questions thoroughly in one session. Once exiting, you will not have the ability to change them.</span>
 @else
	 @if(Auth::user()->role == 1 && $entity->status == 0)
		<p style="text-align: right; margin: 0;">
			<a href="#" class="create-external-link">{{ trans('business_details1.q_pass') }}</a><br>
			<small><a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('messages.external_link_help')}}">{{ trans('business_details1.more_info') }}</a></small>
		</p>
	@endif
@endif

<div class="bs-tab">
	<div class="tab-content">
	    @if($errors->has('catch_error'))
	      <span class="errormessage"><h1>For CreditBPO Personnel: </h1>{{ $errors->first('catch_error') }}</span>
	    @endif
		@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'registration1'))
			<div id="registration1" class="tab-pane fade in active">
				@if(!Session::has('tql') || (Session::has('tql') && in_array('biz-details-cntr', $tql->data)))
					<div id = 'biz-details-cntr' class="spacewrapper noTab">
						<div class="read-only reload-biz_details">
							<h4>{{trans('business_details1.business_details')}}</h4>
							<div class="read-only-text">
								<!-- <div class="details-row">
								</div> -->
								<span class="details-row" id="span-companyname">
									<span class="details-label">{{ trans('business_details1.companyname') }}:  *</span>
									<span class="detail-value progress-required">
										<span id="edit-span-companyname" class = 'editable-field' data-name = 'biz_details.companyname' data-field-id = '{{ $entity->entityid }}'>
											@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
												Company - {{ $entity->entityid }}
											@else
												{{ $entity->companyname }}
											@endif
										</span>
									</span>
									<span class="investigator-control" group_name="Business Details" label="Company Name" value="{{ $entity->companyname }}" group="business_details" item="companyname"></span>
								</span>
								<span class="details-row" id="span-company_tin">
									<span class="details-label">{{ trans('business_details1.company_tin') }} (e.g. 111-111-111 or 111-111-111-111):*</span>
									<span class="detail-value progress-required">
									  
										<span id="edit-span-company_tin" class = 'editable-field' data-name = 'biz_details.company_tin' data-field-id = '{{ $entity->entityid }}'>
										@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
											Classified Information
										@else
											{{ $entity->company_tin }}
										@endif
										</span>

									</span>
									<span class="investigator-control" group_name="Business Details" label="Company Name" value="{{ $entity->companyname }}" group="business_details" item="companyname"></span>
								</span>
								<span class="details-row" id="span-entity_type">
									<span class="details-label">{{ trans('business_details1.entity_type') }}:  *</span>
									@if (BIZ_ENTITY_CORP == $entity->entity_type)
										<span class="detail-value progress-required">
											<span id="edit-span-entity_type" class = 'editable-field' data-name = 'biz_details.entity_type' data-field-id = '{{ $entity->entityid }}'>
												{{ trans('business_details1.corporation') }}
											</span>
										</span>
									@else
									<span class="detail-value progress-required">
										<span id="edit-span-entity_type" class = 'editable-field' data-name = 'biz_details.entity_type' data-field-id = '{{ $entity->entityid }}'>
										{{ trans('business_details1.sole_proprietorship') }}
										</span>
									</span>
									@endif
									<span class="investigator-control" group_name="Business Details" label="Business Entity Type" value="{{ $entity->entity_type }}" group="business_details" item="entity_type"></span>
								</span>
								<span class="details-row" id="span-tin_num">
									@if (BIZ_ENTITY_CORP == $entity->entity_type)
										<span class="details-label">{{ trans('business_details1.sec_company_reg_no') }}:  *</span>
									@else
										<span class="details-label">{{ trans('business_details1.dti_company_reg_no') }}:  *</span>
									@endif
									<span class="detail-value progress-required">
										<span id="edit-span-tin_num" class = 'editable-field' data-name = 'biz_details.tin_num' data-field-id = '{{ $entity->entityid }}'>
											@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
												Classified Information
											@else
												{{ $entity->tin_num }}
											@endif
										</span>
									</span>
									<span class="investigator-control" group_name="Business Details" label="SEC Company Reg. No." value="{{ $entity->tin_num }}" group="business_details" item="tin_num"></span>
								</span>
								<span class="details-row" id="span-sec_reg_date">
									@if (BIZ_ENTITY_CORP == $entity->entity_type)
										<span class="details-label">{{ trans('business_details1.sec_reg_date') }}:  *</span>
									@else
										<span class="details-label">{{ trans('business_details1.dti_reg_date') }}:  *</span>
									@endif
									<span class="detail-value progress-required">
										<span id="edit-span-sec_reg_date" class = 'editable-field date-input' data-name = 'biz_details.sec_reg_date' data-field-id = '{{ $entity->entityid }}'>
											@if ('0000-00-00' != $entity->sec_reg_date)
											{{ date("F d, Y", strtotime($entity->sec_reg_date)) }}
											@endif
										</span>
									</span>
									<span class="investigator-control" group_name="Business Details" label="SEC Reg. Date." value="{{ $entity->sec_reg_date }}" group="business_details" item="sec_reg_date"></span>
								</span>

								@if($bank_standalone == 0)
									@if (BIZ_ENTITY_CORP == $entity->entity_type)
										<span class="details-row">

										<span class="details-label">
											{{ trans('business_details1.sec_cert') }}:
											@if ($doc_req)
												@if (STS_OK == $doc_req->corp_sec_reg)
													*
												@endif
											@else
												*
											@endif

										</span>

								@if (STR_EMPTY != $entity->sec_cert)
									<span class="detail-value">
										@if ($doc_req)
											@if (STS_NG == $doc_req->corp_sec_reg)
												<span class="progress-field-counter" min-value="1"> 1 </span>
											@else
												<span class="progress-required-counter" min-value="1"> 1 </span>
											@endif
										@else
											<span class="progress-required-counter" min-value="1"> 1 </span>
										@endif

										<span id = 'inc-doc-{{ $entity->entityid }}'>
										<a href="{{ URL::to('/') }}/download/incorporation-docs/{{ $entity->sec_cert }}" target="_blank">
											Download Registration Document
										</a>
										</span>
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											| <a href="#" class="pop-file-uploader" data-header = 'Registration Document' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/sec_cert/biz_details") }}'> Upload Registration Document </a>
										@endif
									</span>
								@else
									<span class="detail-value">
										@if ($doc_req)
											@if (STS_NG == $doc_req->corp_sec_reg)
											<span class="progress-field-counter" min-value="1"> 0 </span>
											@else
											<span class="progress-required-counter" min-value="1"> 0 </span>
											@endif
											@else
											<span class="progress-required-counter" min-value="1"> 0 </span>
											@endif

									  <span id = 'inc-doc-{{ $entity->entityid }}'>
									    You have not yet uploaded. Please upload.
									  </span>
									  @if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									  	| <a href="#" class="pop-file-uploader" data-header = 'Registration Document' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/sec_cert/biz_details") }}'> Upload Registration Document </a>
									  @endif
									</span>
								@endif
								<span class="investigator-control" group_name="Business Details" label="Articles of Incorporation" value="<a href='{{ URL::to('/') }}/download/incorporation-docs/{{ $entity->sec_cert }}' target='_blank'> Uploaded Registration Document</a>" group="business_details" item="sec_cert"></span>
								</span>
							@endif

							<span class="details-row" id="span-permit_to_operate">
								<span class="details-label">
									{{ trans('business_details1.permit_to_operate') }}:
									@if ($doc_req)
										@if ((STS_OK == $doc_req->corp_business_license && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_OK == $doc_req->sole_business_license && BIZ_ENTITY_SOLE == $entity->entity_type))
											*
										@endif
									@else
										*
									@endif
								</span>

								@if (STR_EMPTY != $entity->permit_to_operate)
								<span class="detail-value">
									@if ($doc_req)
										@if ((STS_NG == $doc_req->corp_business_license && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_license && BIZ_ENTITY_SOLE == $entity->entity_type))
											<span class="progress-field-counter" min-value="1"> 1 </span>
										@else
											<span class="progress-required-counter" min-value="1"> 1 </span>
										@endif
									@else
										<span class="progress-required-counter" min-value="1"> 1 </span>
									@endif

									<span id = 'permit-operate-{{ $entity->entityid }}'>
									<a href="{{ URL::to('/') }}/download/documents/{{ $entity->permit_to_operate }}" target="_blank">
										Download Permit to Operate
									</a>
									</span>
									@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
										| <a href="#" class="pop-file-uploader" data-header = 'Permit to Operate' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/permit_to_operate/biz_details") }}'> Upload Permit to Operate </a>
									@endif
									</span>
								@else
									<span class="detail-value">
										@if ($doc_req)
											@if ((STS_NG == $doc_req->corp_business_license && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_license && BIZ_ENTITY_SOLE == $entity->entity_type))
												<span class="progress-field-counter" min-value="1"> 0 </span>
											@else
												<span class="progress-required-counter" min-value="1"> 0 </span>
											@endif
										@else
											<span class="progress-required-counter" min-value="1"> 0 </span>
										@endif

										<span id = 'permit-operate-{{ $entity->entityid }}'>
										You have not yet uploaded. Please upload.
										</span>
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											| <a href="#" class="pop-file-uploader" data-header = 'Permit to Operate' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/permit_to_operate/biz_details") }}'> Upload Permit to Operate </a>
										@endif
									</span>
								@endif
								<span class="investigator-control" group_name="Business Details" label="Business Permit" value="<a href='{{ URL::to('/') }}/download/documents/{{ $entity->permit_to_operate }}' target='_blank'> Uploaded Business Permit</a>" group="business_details" item="permit_to_operate"></span>
							</span>

							@if (BIZ_ENTITY_SOLE == $entity->entity_type)
								<span class="details-row">
								<span class="details-label">
									@if (BIZ_ENTITY_CORP == $entity->entity_type)
										{{ trans('business_details1.authorized_signatory_board') }}:
									@else
										{{ trans('business_details1.authorized_signatory_owner') }}:
									@endif
									@if ($doc_req)
										@if ((STS_OK == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_OK == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
											*
										@endif
									@else
										*
									@endif
								</span>
							@if (STR_EMPTY != $entity->authorized_signatory)
								<span class="detail-value">

									@if ($doc_req)
										@if ((STS_NG == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
											<span class="progress-field-counter" min-value="1"> 1 </span>
										@else
											<span class="progress-required-counter" min-value="1"> 1 </span>
										@endif
									@else
										<span class="progress-required-counter" min-value="1"> 1 </span>
									@endif

									<span id = 'auth-sign-{{ $entity->entityid }}'>
										<a href="{{ URL::to('/') }}/download/documents/{{ $entity->authorized_signatory }}" target="_blank">
											@if (BIZ_ENTITY_CORP == $entity->entity_type)
												Download {{ trans('business_details1.authorized_signatory_board') }}
											@else
												Download {{ trans('business_details1.authorized_signatory_owner') }}
											@endif
										</a>
									</span>
									@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
										| <a href="#" class="pop-file-uploader" data-header = 'Authorized Signatory' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/authorized_signatory/biz_details") }}'>
										@if (BIZ_ENTITY_CORP == $entity->entity_type)
											Upload {{ trans('business_details1.authorized_signatory_board') }}:
										@else
											Upload {{ trans('business_details1.authorized_signatory_owner') }}:
										@endif
										</a>
									@endif
								</span>
							@else
								<span class="detail-value">

									@if ($doc_req)
										@if ((STS_NG == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == $entity->entity_type))
											<span class="progress-field-counter" min-value="1"> 0 </span>
										@else
											<span class="progress-required-counter" min-value="1"> 0 </span>
										@endif
										@else
											<span class="progress-required-counter" min-value="1"> 0 </span>
										@endif

										<span id = 'auth-sign-{{ $entity->entityid }}'>
										You have not yet uploaded. Please upload.
										</span>
										@if ((Auth::user()->role == 1) && ($entity->status == 0))
											| <a href="#" class="pop-file-uploader" data-header = 'Authorized Signatory' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/authorized_signatory/biz_details") }}'>
											@if (BIZ_ENTITY_CORP == $entity->entity_type)
												Upload {{ trans('business_details1.authorized_signatory_board') }}:
											@else
												Upload {{ trans('business_details1.authorized_signatory_owner') }}:
										@endif
										</a>
								@endif

							</span>
						@endif
						<span class="investigator-control" group_name="Business Details" label="Authorized Signatory" value="<a href='{{ URL::to('/') }}/download/documents/{{ $entity->authorized_signatory }}' target='_blank'> Uploaded Authorized Signatory</a>" group="business_details" item="authorized_signatory"></span>
						</span>
					@endif

					<span class="details-row" id="span-tax_registration">
						<span class="details-label">
							{{ trans('business_details1.tax_registration') }}:
							@if ($doc_req)
								@if ((STS_OK == $doc_req->corp_tax_reg && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_OK == $doc_req->sole_tax_reg && BIZ_ENTITY_SOLE == $entity->entity_type))
								*
							@endif
							@else
								*
							@endif
						</span>
						@if (STR_EMPTY != $entity->tax_registration)
						<span class="detail-value">

							@if ($doc_req)
								@if ((STS_NG == $doc_req->corp_tax_reg && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_tax_reg && BIZ_ENTITY_SOLE == $entity->entity_type))
									<span class="progress-field-counter" min-value="1"> 1 </span>
								@else
									<span class="progress-required-counter" min-value="1"> 1 </span>
								@endif
								@else
									<span class="progress-required-counter" min-value="1"> 1 </span>
								@endif

							<span id = 'tax-reg-{{ $entity->entityid }}'>
							  <a href="{{ URL::to('/') }}/download/documents/{{ $entity->tax_registration }}" target="_blank">
							    Download BIR Registration
							  </a>
							</span>
							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								| <a href="#" class="pop-file-uploader" data-header = 'BIR Registration' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/tax_registration/biz_details") }}'> Upload BIR Registration: </a>
							@endif
						</span>
						@else
							<span class="detail-value">
								@if ($doc_req)
									@if ((STS_NG == $doc_req->corp_tax_reg && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_tax_reg && BIZ_ENTITY_SOLE == $entity->entity_type))
										<span class="progress-field-counter" min-value="1"> 1 </span>
									@else
										<span class="progress-required-counter" min-value="1"> 0 </span>
									@endif
									@else
										<span class="progress-required-counter" min-value="1"> 0 </span>
									@endif

								<span id = 'tax-reg-{{ $entity->entityid }}'>
								  You have not yet uploaded. Please upload.
								</span>

								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									| <a href="#" class="pop-file-uploader" data-header = 'BIR Registration' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/tax_registration/biz_details") }}'> Upload BIR Registration: </a>
								@endif
							</span>
						@endif
							<span class="investigator-control" group_name="Business Details" label="Tax Registration" value="<a href='{{ URL::to('/') }}/download/documents/{{ $entity->tax_registration }}' target='_blank'> Uploaded Tax Registration</a>" group="business_details" item="tax_registration"></span>
						</span>

						<span class="details-row" id="span-income_tax">
							<span class="details-label">
							{{ trans('business_details1.income_tax_return') }}:
							@if ($doc_req)
								@if ((STS_OK == $doc_req->corp_itr && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_OK == $doc_req->sole_itr && BIZ_ENTITY_SOLE == $entity->entity_type))
									*
								@endif
							@else
								*
							@endif
							</span>

							@if (STR_EMPTY != $entity->income_tax)
							<span class="detail-value">

							@if ($doc_req)
								@if ((STS_NG == $doc_req->corp_itr && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_itr && BIZ_ENTITY_SOLE == $entity->entity_type))
									<span class="progress-field-counter" min-value="1"> 1 </span>
								@else
									<span class="progress-required-counter" min-value="1"> 1 </span>
								@endif
							@else
								<span class="progress-required-counter" min-value="1"> 1 </span>
							@endif

							<span id = 'income-tax-{{ $entity->entityid }}'>
							  <a href="{{ URL::to('/') }}/download/documents/{{ $entity->income_tax }}" target="_blank">
							    Download {{ trans('business_details1.income_tax_return') }}
							  </a>
							</span>
							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								| <a href="#" class="pop-file-uploader" data-header = 'Income Tax Return' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/income_tax/biz_details") }}'> Upload {{ trans('business_details1.income_tax_return') }} </a>
							@endif
								</span>
							@else
							<span class="detail-value">

							@if ($doc_req)
								@if ((STS_NG == $doc_req->corp_itr && BIZ_ENTITY_CORP == $entity->entity_type) || (STS_NG == $doc_req->sole_itr && BIZ_ENTITY_SOLE == $entity->entity_type))
									<span class="progress-field-counter" min-value="1"> 0 </span>
								@else
									<span class="progress-required-counter" min-value="1"> 0 </span>
								@endif
							@else
								<span class="progress-required-counter" min-value="1"> 0 </span>
							@endif

							<span id = 'income-tax-{{ $entity->entityid }}'>
							 	You have not yet uploaded. Please upload.
							</span>
							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								| <a href="#" class="pop-file-uploader" data-header = 'Income Tax Return' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$entity->entityid."/income_tax/biz_details") }}'> Upload {{ trans('business_details1.income_tax_return') }} </a>
							@endif
							</span>
							@endif
							<span class="investigator-control" group_name="Business Details" label="Income Tax Return" value="<a href='{{ URL::to('/') }}/download/documents/{{ $entity->sec_generation }}' target='_blank'> Uploaded {{ trans('business_details1.income_tax_return') }}</a>" group="business_details" item="income_tax"></span>
						</span>
					@endif
					<span class="details-row" id="span-industry_main">
						<span class="details-label">{{ trans('business_details1.industry_main') }}: *</span>
						<span class="detail-value progress-required">
						<span class = 'editable-field' data-name = 'biz_details.industry_main_id' data-field-id = '{{ $entity->entityid }}'>
							{{ $entity->main_title }}
						</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Industry Main" value="{{ $entity->main_title }}" group="business_details" item="main_title"></span>
					</span>
					<span class="details-row" id="span-industry_sub">
						<span class="details-label">{{ trans('business_details1.industry_sub') }}: *</span>
						<span class="detail-value progress-required">
							<span class = 'industry-sub-title'> {{ $entity->sub_title }} </span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Industry Sub" value="{{ $entity->sub_title }}" group="business_details" item="sub_title"></span>
					</span>
					<span class="details-row">
						<span class="details-label">{{ trans('business_details1.industry') }}: *</span>
						<span class="detail-value progress-required">
							<span class = 'industry-row-title'> {{ $entity->row_title }} </span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Industry" value="{{ $entity->row_title }}" group="business_details" item="row_title"></span>
					</span>

					@if ($entity->total_assets == 0 && Auth::user()->role == 1)
						<span class="details-row" id="totalAsset" hidden>
					@else
						<span class="details-row" id="totalAsset">
					@endif

					<span class="details-label">{{trans('business_details1.total_assets')}}:</span>
						<span class="detail-value progress-field">
						<span data-name = 'biz_details.total_assets' data-field-id = '{{ $entity->entityid }}' id="total_assets_bizdetails">
							{{ number_format($entity->total_assets, 2) }}
						</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="{{Lang::get('business_details1.total_assets', [], 'en')}}" value="{{ $entity->total_assets }}" group="business_details" item="total_assets"></span>
					</span>

					@if ($entity->total_assets == 0 && Auth::user()->role == 1)
						<span class="details-row"  id="totalAssetGrouping" hidden >
					@else
						<span class="details-row"  id="totalAssetGrouping" >
					@endif

					<span class="details-label">{{trans('business_details1.total_asset_grouping')}}: *</span>

						<!-- @if ($entity->total_asset_grouping == 4)
							<span id="total_asset_grouping" class="detail-value">
							<span data-name = 'biz_details.total_asset_grouping' data-field-id = '{{ $entity->entityid }}' id="total_assets_grouping_bizdetails">
								{{trans('business_details1.tag_micro')}}
							</span>
							</span>
						@elseif ($entity->total_asset_grouping == 1)
							<span id="total_asset_grouping" class="detail-value">
							<span data-name = 'biz_details.total_asset_grouping' data-field-id = '{{ $entity->entityid }}' id="total_assets_grouping_bizdetails">
								{{trans('business_details1.tag_small')}}
							</span>
							</span>
						@elseif ($entity->total_asset_grouping == 2)
							<span id="total_asset_grouping" class="detail-value">
							<span data-name = 'biz_details.total_asset_grouping' data-field-id = '{{ $entity->entityid }}' id="total_assets_grouping_bizdetails">
								{{trans('business_details1.tag_med')}}
							</span>
							</span>
						@elseif ($entity->total_asset_grouping == 3)
							<span id="total_asset_grouping" class="detail-value">
							<span data-name = 'biz_details.total_asset_grouping' data-field-id = '{{ $entity->entityid }}' id="total_assets_grouping_bizdetails">
								{{trans('business_details1.tag_large')}}
							</span>
							</span>
						@else                                                                               
							<span id="total_asset_grouping" class="detail-value">
							<span data-name = 'biz_details.total_asset_grouping' data-field-id = '{{ $entity->entityid }}' id="total_assets_grouping_bizdetails">       

							</span>
							</span>
						@endif -->

						@if(empty($entity->employee_size))
							@if ($entity->total_asset_grouping == 4)
							{{trans('business_details1.employee_size_0')}}
							@elseif ($entity->total_asset_grouping == 1)
								{{trans('business_details1.employee_size_1')}}
							@elseif ($entity->total_asset_grouping == 2)
								{{trans('business_details1.employee_size_2')}}
							@else
								{{trans('business_details1.employee_size_3')}}
							@endif
						@else

							@if (1 == $entity->employee_size)
							{{trans('business_details1.employee_size_1')}}
							@elseif (2 == $entity->employee_size)
								{{trans('business_details1.employee_size_2')}}
							@elseif (3 == $entity->employee_size)
								{{trans('business_details1.employee_size_3')}}
							@else
								{{trans('business_details1.employee_size_0')}}
							@endif

						@endif
						<span class="investigator-control" group_name="Business Details" label="Total Asset Grouping" value="{{ $entity->total_asset_grouping }}" group="business_details" item="total_asset_grouping"></span>
					</span>
					 
					<span class="details-row" id="span-value_of_outstanding_contracts">
						<span class="details-label">{{trans('business_details1.value_of_outstanding_contracts')}}:</span>
						<span class="detail-value progress-field">
						<span class = 'editable-field' data-name = 'biz_details.value_of_outstanding_contracts' data-field-id = '{{ $entity->entityid }}'>
							{{ $entity->value_of_outstanding_contracts }}
						</span>
						</span>
						<span class="investigator-control"
						group_name="Business Details"
						label="Value of Outstanding Contracts"
						value="{{ $entity->value_of_outstanding_contracts }}"
						group="business_details"
						item="value_of_outstanding_contracts">
						</span>
					</span>

					<span class="details-row" id="span-company_website">
						<span class="details-label">{{trans('business_details1.company_website')}}:</span>
						<span class="detail-value progress-field">
							<span class = 'editable-field' data-name = 'biz_details.website' data-field-id = '{{ $entity->entityid }}'>
								{{ $entity->website }}
							</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Website" value="{{ $entity->website }}" group="business_details" item="website"></span>
					</span>
					<span class="details-row" id="span-company_email">
						<span class="details-label">{{trans('business_details1.company_email')}}: *</span>
						<span class="detail-value progress-required">
							<span class = 'editable-field' data-name = 'biz_details.email' data-field-id = '{{ $entity->entityid }}'>
								@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
									Classified Information
								@else
									{{ $entity->email }}
								@endif
							</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Email Address" value="{{ $entity->email }}" group="business_details" item="email"></span>
					</span>
					<span class="details-row" id="span-address1">
						<span class="details-label">{{ trans('business_details1.primary_business_address') }}: *</span>
						<span class="detail-value progress-required">
							<span class = 'editable-field' data-name = 'biz_details.address1' data-field-id = '{{ $entity->entityid }}'>
								@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
									Classified Information
								@else
									{{ $entity->address1 }}
								@endif
							</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Primary Business Address, Room Building Street" value="{{ $entity->address1 }}" group="business_details" item="address1"></span>
					</span>

					@if($entity->countrycode == 'PH')
						<span class="details-row" id="span-primary_business_province">
							<span class="details-label">{{ trans('business_details1.primary_business_province') }}: *</span>
							<span class="detail-value progress-required">
								<span class = 'editable-field' data-name = 'biz_details.province' data-field-id = '{{ $entity->entityid }}'>
									{{ $entity->province }}
								</span>
							</span>
							<span class="investigator-control" group_name="Business Details" label="Primary Business Address, Province" value="{{ $entity->province }}" group="business_details" item="province"></span>
						</span>
						<span class="details-row" id="span-primary_business_city">
							<span class="details-label">{{ trans('business_details1.primary_business_city') }}: *</span>
								<span class="detail-value progress-required">
									<span class = 'city-name'>{{ $entity->citymunDesc }}</span>
								</span>
							<span class="investigator-control" group_name="Business Details" label="Primary Business Address, City" value="{{ $entity->citymunDesc }}" group="business_details" item="city"></span>
						</span>
					@endif

					<span class="details-row" id="span-primary_business_zipcode">
						<span class="details-label">{{ trans('business_details1.primary_business_zipcode') }}: *</span>
							<span class="detail-value progress-required">
								@if($entity->countrycode == 'PH')
									<span class = 'biz-zipcode'>{{ $entity->zipcode }}</span>
								@else
									<span class = 'biz-zipcode editable-field' data-name = 'biz_details.zipcode' data-field-id = '{{ $entity->entityid }}' >{{ $entity->zipcode }}</span>
								@endif
							</span>
						<span class="investigator-control" group_name="Business Details" label="Primary Business Address, Zipcode" value="{{ $entity->zipcode }}" group="business_details" item="zipcode"></span>
					</span>
					<span class="details-row" id="span-primary_business_telephone">
						<span class="details-label">{{ trans('business_details1.primary_business_telephone') }}: *</span>
						<span class="detail-value progress-required">
							<span class = 'editable-field' data-name = 'biz_details.phone' data-field-id = '{{ $entity->entityid }}'>
								@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
									Classified Information
								@else
									{{ $entity->phone }}
								@endif
							</span>
						</span>
						<span class="investigator-control" group_name="Business Details" label="Primary Business Address, Telephone" value="{{ $entity->phone }}" group="business_details" item="phone"></span>
					</span>

					<div class="hidden">
						<span class="details-row" id="span-no_yrs_in_business">
						  <span class="details-label">{{ trans('business_details1.no_yrs_in_business') }}:</span>
						  <span class="detail-value progress-field">
						    <span class = 'anonymous-info' data-name = 'biz_details.number_year' data-field-id = '{{ $entity->entityid }}'>
						      @if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
						      Classified Information
						      @else
						      {{ $entity->number_year }}
						      @endif
						    </span>
						  </span>
						  <span class="investigator-control" group_name="Business Details" label="Years in Business" value="{{ $entity->number_year }}" group="business_details" item="number_year"></span>
						</span>
						</div>
						<span class="details-row" id="span-company_description">
							<span class="details-label">{{trans('business_details1.company_description')}}:</span>
							<span class="detail-value progress-field">
								<span class = 'editable-field' data-name = 'biz_details.description' data-field-id = '{{ $entity->entityid }}'>
									@if (STR_EMPTY != $entity->description)
									{!! nl2br($entity->description) !!}
									@else
									{!! nl2br(trans('business_details1.company_description_data')) !!}
									@endif
								</span>
							</span>
							<span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.company_description',[],'en')}}" value="{{ $entity->description }}" group="business_details" item="description"></span>
						</span>
						<span class="details-row" id="span-employee_size">
							<span class="details-label">{{trans('business_details1.employee_size')}}: *</span>
							<span class="detail-value progress-required">
								<span class = 'editable-field' data-name = 'biz_details.employee_size' data-field-id = '{{ $entity->entityid }}'>

									@if(empty($entity->employee_size))
										@if ($entity->total_asset_grouping == 4)
										{{trans('business_details1.employee_size_0')}}
										@elseif ($entity->total_asset_grouping == 1)
											{{trans('business_details1.employee_size_1')}}
										@elseif ($entity->total_asset_grouping == 2)
											{{trans('business_details1.employee_size_2')}}
										@else
											{{trans('business_details1.employee_size_3')}}
										@endif
									@else

										@if (1 == $entity->employee_size)
										{{trans('business_details1.employee_size_1')}}
										@elseif (2 == $entity->employee_size)
											{{trans('business_details1.employee_size_2')}}
										@elseif (3 == $entity->employee_size)
											{{trans('business_details1.employee_size_3')}}
										@else
											{{trans('business_details1.employee_size_0')}}
										@endif

									@endif


								</span>
							</span>
							<span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.employee_size',[],'en')}}" value="{{ $entity->employee_size }}" group="business_details" item="employee_size"></span>
						</span>
						<span class="details-row" id="span-updated_date">
							<span class="details-label">{{trans('business_details1.updated_date')}}:</span>
							<span class="detail-value progress-field">
								<span class = 'editable-field date-input' data-name = 'biz_details.updated_date' data-field-id = '{{ $entity->entityid }}'>
									@if (STR_EMPTY != $entity->updated_date)
										{{ date("F d, Y", strtotime($entity->updated_date)) }}
									@endif
								</span>
							</span>
							<span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.updated_date',[],'en')}}" value="{{ $entity->updated_date }}" group="business_details" item="updated_date"></span>
						</span>
						<span class="details-row" id="span-requesting_bank">
							<span class="details-label">{{trans('business_details1.requesting_bank')}}:</span>
							<span class="detail-value progress-field">
	
							@if($isClientKey == 1)
									<span class = '' data-name = 'biz_details.is_independent' data-field-id = '{{ $entity->entityid }}'>
								@else
									<span class = '{{Auth::user()->is_contractir != 1 ? "editable-field" : ""}}' data-name = 'biz_details.is_independent' data-field-id = '{{ $entity->entityid }}'>
								@endif
									{{ (1 == $entity->is_independent) ? $entity->bank_name : "Applying Independently" }}</span>
									</span>
							<span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.requesting_bank',[],'en')}}" value="{{ ($entity->bank_name) ? $entity->bank_name : 'Applying Independently' }}" group="business_details" item="bank_name"></span>
						</span>
						<span class="details-row" id="span-fs_input_who">
							<span class="details-label">{{trans('business_details1.fs_input_who')}} *</span>
							<span class="detail-value progress-required">
								<span class = 'editable-field' data-name = 'biz_details.fs_input_who' data-field-id = '{{ $entity->entityid }}'>
									@if (0 === $entity->fs_input_who)
										{{trans('business_details1.fs_input_who_0')}}
									@elseif (1 == $entity->fs_input_who)
										{{trans('business_details1.fs_input_who_1')}}
									@else
										Choose here
									@endif
								</span>
							</span>
							<span class="investigator-control" group_name="Business Details" label="{{trans('business_details1.employee_size',[],'en')}}" value="{{ $entity->employee_size }}" group="business_details" item="employee_size"></span>
						</span>
						@if($bank_standalone == 0)
							<span class="details-row">
							<span class="details-label">{{trans('business_details1.potential_credit_quality_issues')}}:</span>
							<span class="detail-value progress-field potential_credit_quality_issues">
								<p>{{trans('business_details1.potential_credit_quality_issues_hint')}}</p>
								<?php
								$pcqi_array = explode(',', $entity->potential_credit_quality_issues);
								?>
								<label><input type="checkbox" value="1" {{ (in_array('1', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.existing_labor_disputes')}}</label><br/>
								<label><input type="checkbox" value="2" {{ (in_array('2', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.tax_tariff_change')}}</label><br/>
								<label><input type="checkbox" value="3" {{ (in_array('3', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.company_fraud')}}</label><br/>
								<label><input type="checkbox" value="4" {{ (in_array('4', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.loss_of_key_management')}}</label><br/>
								<label><input type="checkbox" value="5" {{ (in_array('5', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.existing_litigation')}}</label><br/>
								<label><input type="checkbox" value="6" {{ (in_array('6', $pcqi_array)) ? 'checked="checked"' : '' }} {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{trans('business_details1.large_maturity')}}</label><br/>
							</span>
							<span class="investigator-control" group_name="Business Details" label="Potential Credit Quality Issues" value="{{ $entity->potential_credit_quality_issues }}" group="business_details" item="potential_credit_quality_issues"></span>
							</span>
						@endif
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
								@if ($entity->loginid == Auth::user()->loginid)
									<a id = 'edit-biz-details' style = 'display: none;' href="{{ URL::to('/') }}/sme/busdetails/{{ $entity->entityid }}/edit" class="navbar-link">{{trans('business_details1.edit_business_details')}}</a>
								@endif
							</span>
						@endif
					</div>
				</div>
			</div>
		@endif

		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->affiliates)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('affiliates-list-cntr', $tql->data)))
				<div id = 'affiliates-list-cntr' class="spacewrapper">
					<div class="read-only reload-affiliates">
						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->affiliates)
						<span class="progress-required-counter" min-value="1"> {{ @count($relatedcompanies) > 0 ? 1 : 0 }} </span>
						@endif

				  		<h4>{{trans('business_details1.related_company')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.related_company_info')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							@if ((@count($relatedcompanies)==0) && (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->affiliates))
								<small><i>{{ trans('business_details1.skip_not_available') }}</i></small>
							@endif
							@foreach ($relatedcompanies as $relatedcompanieinfo)
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.name') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_name' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
											{{ $relatedcompanieinfo->related_company_name }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Name" value="{{ $relatedcompanieinfo->related_company_name }}" group="related_company" item="related_company_name_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.address_1') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_address1' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
											{{ $relatedcompanieinfo->related_company_address1 }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Address 1" value="{{ $relatedcompanieinfo->related_company_address1 }}" group="related_company" item="related_company_address1_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.address_2') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_address2' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
											{{ $relatedcompanieinfo->related_company_address2 }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Address 2" value="{{ $relatedcompanieinfo->related_company_address2 }}" group="related_company" item="related_company_address2_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.province_region') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_province' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
											{{ $relatedcompanieinfo->related_company_province }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Province/Region" value="{{ $relatedcompanieinfo->related_company_province }}" group="related_company" item="related_company_province_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.city') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'affiliates-city-{{ $relatedcompanieinfo->relatedcompaniesid }}'>{{ $relatedcompanieinfo->related_company_cityid }}</span>
										<span class="investigator-control" group_name="Related Company" label="City" value="{{ $relatedcompanieinfo->related_company_cityid }}" group="related_company" item="related_company_cityid_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.zipcode') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'affiliates-zip-{{ $relatedcompanieinfo->relatedcompaniesid }}'> {{ $relatedcompanieinfo->related_company_zipcode }} </span>
										<span class="investigator-control" group_name="Related Company" label="Zipcode" value="{{ $relatedcompanieinfo->related_company_zipcode }}" group="related_company" item="related_company_zipcode_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.phone_number') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_phone' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
										{{ $relatedcompanieinfo->related_company_phone }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Phone Number" value="{{ $relatedcompanieinfo->related_company_phone }}" group="related_company" item="related_company_phone_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-3 col-sm-3"><b>{{ trans('messages.email') }}</b></div>
									<div class="col-md-9 col-sm-9">
										<span class = 'editable-field' data-name = 'affiliates.related_company_email' data-field-id = '{{ $relatedcompanieinfo->relatedcompaniesid }}'>
										{{ $relatedcompanieinfo->related_company_email }}
										</span>
										<span class="investigator-control" group_name="Related Company" label="Email" value="{{ $relatedcompanieinfo->related_company_email }}" group="related_company" item="related_company_email_{{$relatedcompanieinfo->relatedcompaniesid}}"></span>
									</div>
								</span>
								@if ($entity->status == 0 or $entity->status == 3)
									<span class="details-row">
										@if ($entity->loginid == Auth::user()->loginid)
											<div class="col-md-12">
											<a href="{{ URL::to('/') }}/sme/relatedcompanies/{{ $relatedcompanieinfo->relatedcompaniesid }}/delete" class="navbar-link delete-info" data-reload = '.reload-affiliates' data-cntr = '#affiliates-list-cntr' >{{trans('messages.delete')}}</a>
											</div>
										@endif
									</span>
								@endif
							@endforeach
							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right; width: 100%;">
									@if ($entity->loginid == Auth::user()->loginid)
										<a id = 'show-affiliates-expand' href="{{ URL::to('/') }}/sme/relatedcompanies/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.related_company')}}</a>
									@endif
								</span>
							@endif

							<div id = 'expand-affiliates-form' class = 'spacewrapper'></div>
						</div>
					</div>
				</div>
			@endif
		@endif

		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->products)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('offers-list-cntr', $tql->data)))
				<div id = 'offers-list-cntr' class="spacewrapper">
					<div class="read-only reload-offers">
						<h4>{{trans('business_details1.products_services_offered')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.products_services_offered_hint')}}">{{trans('messages.more_info')}}</a></h4>
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ $progress['servicesoffer']==1 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->products)
							<span class="progress-required-counter" min-value="4"> {{ @count($prod_services) > 0 ? 4 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->products)
							<span class="progress-field-counter" min-value="4"> {{ @count($prod_services) > 0 ? 4 : 0 }} </span>
						@else

						@endif

						<div class="read-only-text">
							<div class="servicesoffers">
								<div class="details-row hidden-xs hidden-sm">
									<div class="col-md-3 col-sm-3">
										<b>{{ trans('business_details1.products_services_offered') }}</b>
									</div>

									<div class="col-md-3 col-sm-3">
										<b>{{ trans('business_details1.target_market') }}</b>
									</div>

									<div class="col-md-3 col-sm-3">
										<b>{{ trans('business_details1.share_to_total_sales') }}</b>
									</div>

									<div class="col-md-3 col-sm-3">
										<b>{{ trans('business_details1.seasonality') }}</b>
									</div>
								</div>

								@foreach ($prod_services as $offers)
									<div class="details-row hidden-xs hidden-sm">
										<div class="col-md-3 col-sm-3">
											@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
												<a href = "{{ URL::To('/sme/servicesoffer/'.$offers->servicesofferid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-offers' data-cntr = '#offers-list-cntr'>{{trans('messages.delete')}}</a>
											@endif
												<span class = 'editable-field' data-name = 'offers.servicesoffer_name' data-field-id = '{{ $offers->servicesofferid }}'>
													{{ $offers->servicesoffer_name }}
												</span>
										</div>

										<div class="col-md-3 col-sm-3">
											<span class = 'editable-field' data-name = 'offers.servicesoffer_targetmarket' data-field-id = '{{ $offers->servicesofferid }}'>
										    	{{ $offers->servicesoffer_targetmarket }}
										  	</span>
										</div>

										<div class="col-md-3 col-sm-3">
											<span class = 'editable-field' data-name = 'offers.servicesoffer_share_revenue' data-field-id = '{{ $offers->servicesofferid }}'>
												{{ number_format($offers->servicesoffer_share_revenue, 2) }}%
											</span>
										</div>
										<div class="col-md-3 col-sm-3">
											<span class = 'editable-field' data-name = 'offers.servicesoffer_seasonality' data-field-id = '{{ $offers->servicesofferid }}'>
												@if ($offers->servicesoffer_seasonality == 1)
													{{ trans('business_details1.seasonality_year_round') }}
												@elseif ($offers->servicesoffer_seasonality == 2)
													{{ trans('business_details1.seasonality_6_mo') }}
												@elseif ($offers->servicesoffer_seasonality == 3)
													{{ trans('business_details1.seasonality_3_mo') }}
												@else
													{{ trans('business_details1.seasonality_less_than_3_mo') }}
												@endif
											</span>

											<span class="investigator-control" group_name="Products/Services Offered" label="{{ trans('business_details1.products_services_offered') }}" value="{{ $offers->servicesoffer_name }}" group="services_offer" item="services_offer_{{ $offers->servicesofferid }}"></span>
										</div>
										</div>

										<div class="details-row visible-xs-block visible-sm-block">
											<div class="col-xs-12 col-sm-3">
												<b>{{ trans('business_details1.products_services_offered') }}</b><br />
												<span class = 'editable-field' data-name = 'offers.servicesoffer_name' data-field-id = '{{ $offers->servicesofferid }}'>
												{{ $offers->servicesoffer_name }}
												</span>
											</div>

											<div class="col-xs-12 col-sm-3">
												<b>{{ trans('business_details1.target_market') }}</b><br />
												<span class = 'editable-field' data-name = 'offers.servicesoffer_targetmarket' data-field-id = '{{ $offers->servicesofferid }}'>
												{{ $offers->servicesoffer_targetmarket }}
												</span>
											</div>

											<div class="col-xs-12 col-sm-3">
												<b>{{ trans('business_details1.share_to_total_sales') }}</b><br />
												<span class = 'editable-field' data-name = 'offers.servicesoffer_targetmarket' data-field-id = '{{ $offers->servicesofferid }}'>
												{{ $offers->servicesoffer_share_revenue }}%
												</span>
											</div>

											<div class="col-xs-12 col-sm-3">
												<b>{{ trans('business_details1.seasonality') }}</b><br/>
												<span class = 'editable-field' data-name = 'offers.servicesoffer_seasonality' data-field-id = '{{ $offers->servicesofferid }}'>
												@if ($offers->servicesoffer_seasonality == 1)
													{{ trans('business_details1.seasonality_year_round') }}
												@elseif ($offers->servicesoffer_seasonality == 2)
													{{ trans('business_details1.seasonality_6_mo') }}
												@elseif ($offers->servicesoffer_seasonality == 3)
													{{ trans('business_details1.seasonality_3_mo') }}
												@else
													{{ trans('business_details1.seasonality_less_than_3_mo') }}
												@endif
												</span>
												<span class="investigator-control" group_name="Products/Services Offered" label="{{ trans('business_details1.products_services_offered') }}" value="{{ $offers->servicesoffer_name }}" group="services_offer" item="services_offer_{{ $offers->servicesofferid }}"></span>
											</div>

											<div class="col-xs-12">
												<a href = "{{ URL::To('/sme/servicesoffer/'.$offers->servicesofferid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-offers' data-cntr = '#offers-list-cntr'>{{trans('messages.delete')}}</a>
											</div>
									</div>
								@endforeach
						    </div>
							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-offers-expand' href="{{ URL::to('/') }}/sme/servicesoffer/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.products_services_offered')}}</a>
							@endif
								</span>
							@endif

						   <div id = 'expand-offers-form' class = 'spacewrapper'></div>
						</div>
					</div>
				</div>
			@endif
		@endif

		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->capital_details)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('cap-details-list-cntr', $tql->data)))
				@if (BIZ_ENTITY_CORP == $entity->entity_type)
					<div id = 'cap-details-list-cntr' class="spacewrapper">
				@else
					<div id = 'cap-details-list-cntr' class="spacewrapper" style = 'display: none;'>
				@endif
			    <div class="read-only reload-cap-details">
					@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->capital_details)
						<span class="progress-required-counter" min-value="5"> {{ @count($capital) > 0 ? 5 : 0 }} </span>
					@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->capital_details)
						<span class="progress-field-counter" min-value="5"> {{ @count($capital) > 0 ? 5 : 0 }} </span>
					@else

					@endif

					<h4>{{trans('business_details1.capital_details')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.capital_details_hint')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ @count($capital)>0 ? "true" : "" }}</div>
						@endif

						@foreach ($capital as $capitalinfo)
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{trans('business_details1.authorized_capital')}}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'capital_details.capital_authorized' data-field-id = '{{ $capitalinfo->capitalid }}'>
										{{ number_format($capitalinfo->capital_authorized, 2) }}
									</span>
									<span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.authorized_capital',[],'en')}}" value="{{ $capitalinfo->capital_authorized }}" group="capital_details" item="capital_authorized_{{$capitalinfo->capitalid}}"></span>
								</div>
							</span>
							<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{trans('business_details1.issued_capital')}}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'capital_details.capital_issued' data-field-id = '{{ $capitalinfo->capitalid }}'>
									{{ number_format($capitalinfo->capital_issued, 2) }}
								</span>
								<span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.issued_capital',[],'en')}}" value="{{ $capitalinfo->capital_issued }}" group="capital_details" item="capital_issued_{{$capitalinfo->capitalid}}"></span>
							</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{trans('business_details1.paid_up_capital')}}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'capital_details.capital_paid_up' data-field-id = '{{ $capitalinfo->capitalid }}'>
										{{ number_format($capitalinfo->capital_paid_up, 2) }}
									</span>
									<span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.paid_up_capital',[],'en')}}" value="{{ $capitalinfo->capital_paid_up }}" group="capital_details" item="capital_paid_up_{{$capitalinfo->capitalid}}"></span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{trans('business_details1.ordinary_shares')}}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'capital_details.capital_ordinary_shares' data-field-id = '{{ $capitalinfo->capitalid }}'>
										{{ number_format($capitalinfo->capital_ordinary_shares, 2) }}
									</span>
									<span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.ordinary_shares',[],'en')}}" value="{{ $capitalinfo->capital_ordinary_shares }}" group="capital_details" item="capital_ordinary_shares_{{$capitalinfo->capitalid}}"></span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{trans('business_details1.par_value')}}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'capital_details.capital_par_value' data-field-id = '{{ $capitalinfo->capitalid }}'>
										{{ number_format($capitalinfo->capital_par_value, 2) }}
									</span>
									<span class="investigator-control" group_name="Capital Details" label="{{trans('business_details1.par_value',[],'en')}}" value="{{ $capitalinfo->capital_par_value }}" group="capital_details" item="capital_par_value_{{$capitalinfo->capitalid}}"></span>
								</div>
							</span>
						@endforeach
						<small><i>{{trans('messages.par_value_shares')}}</i></small><br>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right; width: 100%;">
							@if ($entity->loginid == Auth::user()->loginid)
								@if (count($capital) == 0)
									<a id = 'show-cap-details-expand' href="{{ URL::to('/') }}/sme/capital/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.capital_details')}}</a>
								@else
									<a href="{{ URL::to('/') }}/sme/capital/{{ $capitalinfo->capitalid }}/delete" class="navbar-link delete-info" data-reload = '.reload-cap-details' data-cntr = '#cap-details-list-cntr'>{{trans('messages.delete')}} </a>
								@endif
							@endif
							</span>
						@endif

						<div id = 'expand-cap-details-form' class = 'spacewrapper'></div>
						</div>
					</div>
				</div>
			@endif
		@endif

		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->main_locations)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('main-loc-list-cntr', $tql->data)))
				<div id = 'main-loc-list-cntr' class="spacewrapper">
					<div class="read-only reload-main-loc">
						<h4>{{trans('business_details1.main_locations')}}</h4>
						<div class="read-only-text">
							<span class="details-row hidden-xs hidden-sm">
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.size_of_premises')}}</b></div>
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.premises_owned_leased')}}</b></div>
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.location')}}</b></div>
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.premise_used_as')}}</b></div>
							</span>

						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ @count($slocation)>0 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->main_locations)
							<span class="progress-required-counter" min-value="4"> {{ @count($slocation) > 0 ? 4 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->main_locations)
							<span class="progress-field-counter" min-value="4">{{ @count($slocation)>0 ? 4 : 0 }}</span>
						@else

						@endif

						@foreach ($slocation as $slocationinfo)
							<div class="details-row">
								<div class="col-md-3">
									@if ($entity->status == 0 or $entity->status == 3)
										@if ($entity->loginid == Auth::user()->loginid)
											<span class="hidden-xs hidden-sm">
											<a href="{{ URL::to('/') }}/sme/locations/{{ $slocationinfo->locationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-main-loc' data-cntr = '#main-loc-list-cntr'>{{trans('messages.delete')}}</a>
											</span>
										@endif
									@endif
									<b class="visible-xs-block visible-sm-block">{{trans('business_details1.size_of_premises')}}<br /></b>
									<span class = 'editable-field' data-name = 'main_loc.location_size' data-field-id = '{{ $slocationinfo->locationid }}'>
										{{ $slocationinfo->location_size }} sqm
									</span>
								</div>
								<div class="col-md-3 col-sm-3">
									<b class="visible-xs-block visible-sm-block">{{trans('business_details1.premises_owned_leased')}}<br /></b>
									<span class = 'editable-field' data-name = 'main_loc.location_type' data-field-id = '{{ $slocationinfo->locationid }}'>
										{{ LanguageHelper::convert('business_details1', $slocationinfo->location_type) }}
									</span>
								</div>
								<div class="col-md-3 col-sm-3">
									<b class="visible-xs-block visible-sm-block">{{trans('business_details1.location')}}<br /></b>
									<span class = 'editable-field' data-name = 'main_loc.location_map' data-field-id = '{{ $slocationinfo->locationid }}'>
										{{ LanguageHelper::convert('business_details1', $slocationinfo->location_map) }}
									</span>
								</div>
								<div class="col-md-3 col-sm-3">
									<b class="visible-xs-block visible-sm-block">{{trans('business_details1.premise_used_as')}}<br /></b>
									<span class = 'editable-field' data-name = 'main_loc.location_used' data-field-id = '{{ $slocationinfo->locationid }}'>
										{{ LanguageHelper::convert('business_details1', $slocationinfo->location_used) }}
									</span>
									<span class="investigator-control" group_name="Main Location/s" label="{{trans('business_details1.size_of_premises',[],'en')}}" value="{{ $slocationinfo->location_size }}" group="capital_details" group="main_location" item="location_{{$slocationinfo->locationid}}"></span>
								</div>
								<div class="col-xs-12 visible-xs-block visible-sm-block">
									@if ($entity->status == 0 or $entity->status == 3)
										@if ($entity->loginid == Auth::user()->loginid)
											<a href="{{ URL::to('/') }}/sme/locations/{{ $slocationinfo->locationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-main-loc' data-cntr = '#main-loc-list-cntr'>{{trans('messages.delete')}}</a>
										@endif
									@endif
								</div>
							</div>
						@endforeach
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right; width: 100%;">
								@if ($entity->loginid == Auth::user()->loginid)
									<a id = 'show-main-loc-expand' href="{{ URL::to('/') }}/sme/locations/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.main_locations')}}</a>
								@endif
							</span>
						@endif

						<div id = 'expand-main-loc-form' class = 'spacewrapper'></div>
						</div>
					</div>
				</div>
			@endif
		@endif

		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->branches)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('branches-list-cntr', $tql->data)))
				<div id = 'branches-list-cntr' class="spacewrapper">
					 <div class="read-only reload-branches">
						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->branches)
							<span class="progress-required-counter" min-value="1"> {{ @count($branches) > 0 ? 1 : 0 }} </span>
						@endif

						<h4>{{trans('business_details1.branches')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.branches_hint')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							@if ((@count($branches)==0) && (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->branches))
								<small><i>{{ trans('business_details1.skip_not_available') }}</i></small>
							@else
							<span class="details-row hidden-xs hidden-sm">
							 <div class="col-md-2 col-sm-3"><b>Address</b></div>
							 <div class="col-md-2 col-sm-1"><b>{{trans('business_details1.branches_owned_leased_short')}}</b></div>
							 <div class="col-md-2 col-sm-1"><b>{{trans('business_details1.b_contact_phone')}}</b></div>
							 <div class="col-md-2 col-sm-3"><b>{{trans('business_details1.b_contact_email')}}</b></div>
							 <div class="col-md-2 col-sm-2"><b>{{trans('business_details1.b_contact_person')}}</b></div>
							 <div class="col-md-2 col-sm-2"><b>{{trans('business_details1.b_job_title')}}</b></div>
							</span>
							@endif
							@foreach ($branches as $branchesinfo)
								<span class="details-row">
									<div class="col-md-2">
										@if ($entity->status == 0 or $entity->status == 3)
											@if ($entity->loginid == Auth::user()->loginid)
												<span class="hidden-xs hidden-sm">
												<a href="{{ URL::to('/') }}/sme/branches/{{ $branchesinfo->branchid }}/delete" class="navbar-link delete-info" data-reload = '.reload-branches' data-cntr = '#branches-list-cntr'>{{trans('messages.delete')}}</a>
												</span>
											@endif
										@endif
										<b class="visible-xs-block visible-sm-block">Address<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_address' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ $branchesinfo->branch_address }}
										</span>
									</div>
									<div class="col-md-2 col-sm-1">
										<b class="visible-xs-block visible-sm-block">{{trans('business_details1.branches_owned_leased_short')}}<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_owned_leased' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ LanguageHelper::convert('business_details1', $branchesinfo->branch_owned_leased) }}
										</span>
									</div>
									<div class="col-md-2 col-sm-1"><b class="visible-xs-block visible-sm-block">{{trans('business_details1.b_contact_phone')}}<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_phone' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ $branchesinfo->branch_phone }}
										</span>
									</div>
									<div class="col-md-2 col-sm-2"><b class="visible-xs-block visible-sm-block">{{trans('business_details1.b_contact_email')}}<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_email' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ $branchesinfo->branch_email }}
										</span>
									</div>
									<div class="col-md-2 col-sm-2"><b class="visible-xs-block visible-sm-block">{{trans('business_details1.b_contact_person')}}<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_contact_person' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ $branchesinfo->branch_contact_person }}
										</span>
									</div>
									<div class="col-md-2 col-sm-2"><b class="visible-xs-block visible-sm-block">{{trans('business_details1.b_job_title')}}<br /></b>
										<span class = 'editable-field' data-name = 'branches.branch_job_title' data-field-id = '{{ $branchesinfo->branchid }}'>
											{{ $branchesinfo->branch_job_title }}
										</span>
										<span class="investigator-control" group_name="Branch(es)" label="Address" value="{{ $branchesinfo->branch_address }}" group="branches" item="branch_{{$branchesinfo->branchid}}"></span>
									</div>
									<div class="col-xs-12 visible-xs-block visible-sm-block">
									 @if ($entity->status == 0 or $entity->status == 3)
										 @if ($entity->loginid == Auth::user()->loginid)
										 	<a href="{{ URL::to('/') }}/sme/branches/{{ $branchesinfo->branchid }}/delete" class="navbar-link delete-info" data-reload = '.reload-branches' data-cntr = '#branches-list-cntr'>{{trans('messages.delete')}}</a>
										 @endif
									 @endif
									</div>
								</span>
							@endforeach
							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right; width: 100%;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-branches-expand' href="{{ URL::to('/') }}/sme/branches/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.branches')}}</a>
							@endif
							</span>
						@endif

						<div id = 'expand-branches-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif

	@if($bank_standalone == 0)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('certifications-list-cntr', $tql->data)))
			<div id = 'certifications-list-cntr'  class="spacewrapper">
				<div class="read-only reload-certifications">
					<h4>{{trans('business_details1.certifications')}}</h4>
						<div class="read-only-text">
							<span class="details-row hidden-xs hidden-sm">
							  <div class="col-md-6 col-sm-6"><b></b></div>
							  <div class="col-md-2 col-sm-2"><b>{{ trans('messages.registration_num') }}</b></div>
							  <div class="col-md-2 col-sm-2"><b>{{ trans('messages.registration_date') }}</b></div>
							  <div class="col-md-2 col-sm-2 print-none"><b>{{trans('business_details1.uploaded_document')}}</b></div>
							</span>

						@if ($doc_req)
							@if ((STS_OK == $doc_req->boi) || (STS_OK == $doc_req->peza))
								<span class="progress-required-counter" min-value="1">
							@else
								<span class="progress-field-counter" min-value="1">
							@endif
						@else
					    	<span class="progress-field-counter" min-value="1">
					    @endif

						@if (($boi_cert) || ($peza_cert))
							1
						@else
							0
						@endif
						</span>

					    @foreach ($certifications as $certificationinfo)
						    <span class="details-row">
								<div class="col-md-6 col-sm-6">
									@if ($entity->status == 0 or $entity->status == 3)
										@if ($entity->loginid == Auth::user()->loginid)
											<a href="{{ URL::to('/') }}/sme/certification/{{ $certificationinfo->certificationid }}/delete" class="navbar-link delete-info" data-reload = '.reload-certifications' data-cntr = '#certifications-list-cntr'>{{trans('messages.delete')}}</a>
										@endif
									@endif
									@if (($certificationinfo->certification_details != 'Board of Investments (BOI)') || ($certificationinfo->certification_details != 'Philippine Export Processing Zone Authority (PEZA)'))
										<span class = 'editable-field' data-name = 'certifications.certification_details' data-field-id = '{{ $certificationinfo->certificationid }}'>
										{{ $certificationinfo->certification_details }}
										</span>
									@else
										{{ $certificationinfo->certification_details }}
									@endif
								</div>

						    <div class="col-md-2 col-sm-2">
								<b class="visible-xs-block visible-sm-block"> {{ trans('messages.registration_num') }} </b>
								<span class = 'editable-field' data-name = 'certifications.certification_reg_no' data-field-id = '{{ $certificationinfo->certificationid }}'>
								{{ $certificationinfo->certification_reg_no }}
								</span>
						    </div>
						    <div class="col-md-2 col-sm-2">
								<b class="visible-xs-block visible-sm-block"> {{ trans('messages.registration_date') }} </b>
								<span class = 'editable-field date-input' data-name = 'certifications.certification_reg_date' data-field-id = '{{ $certificationinfo->certificationid }}'>
								{{ date("F d, Y", strtotime($certificationinfo->certification_reg_date)) }}
								</span>
						    </div>
						    <div class="col-md-2 col-sm-2 print-none">
								<b class="visible-xs-block visible-sm-block"> {{trans('business_details1.uploaded_document')}} </b>
								<span id = 'certification-docs{{ $certificationinfo->certificationid }}'>
								<a href="{{ URL::to('/') }}/download/certification-docs/{{ $certificationinfo->certification_doc }}" target="_blank"> Download </a>
								</span>
								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									| <a href="#" class="pop-file-uploader" data-header = 'Certification' data-type = '1' data-target = '{{ URL::To("rest/post_editable_upload/".$certificationinfo->certificationid."/certification_doc/certifications") }}'> Upload </a>
								@endif
						    </div>
						    <span class="investigator-control" group_name="Certifications, Accreditations, and Tax Exemptions" label="Certifications, Accreditations, and Tax Exemptions" value="{{ $certificationinfo->certification_details }}" group="certifications" item="certification_details_{{$certificationinfo->certificationid}}"></span>
						  	</span>
					  	@endforeach
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right; width: 100%;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-certifications-expand' href="{{ URL::to('/') }}/sme/certification/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.certifications')}}</a>
							@endif
							</span>
						@endif

						<div id = 'expand-certifications-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif

	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->import_export)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('biztyp-list-cntr', $tql->data)))
			<div id = 'biztyp-list-cntr' class="spacewrapper">
				<div class="read-only reload-biztyp">
					<h4>{{trans('business_details1.import_export')}}</h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ @count($businesstype)>0 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->import_export)
							<span class="progress-required-counter" min-value="2"> {{ @count($businesstype) > 0 ? 2 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->import_export)
							<span class="progress-field-counter" min-value="2">{{ @count($businesstype) > 0 ? 2 : 0 }}</span>
						@else

						@endif

						@foreach ($businesstype as $businesstypeinfo)
							<span class="details-row">
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.ie_imports')}}</b></div>
								<div class="col-md-9 col-sm-9">
									<input type="checkbox" class="cb_import" {{ ($entity->status == 0 && $entity->loginid == Auth::user()->loginid) ? '' : 'disabled' }} {{ ($businesstypeinfo->is_import == 1) ? "checked='checked'" : "" }} data-toggle="toggle" data-on="Yes" data-off="No" data-size="mini" />
									<span class="investigator-control" group_name="Import/Export Business" label="Imports" value="{{($businesstypeinfo->is_import == 1) ? 'YES' : 'NO'}}" group="import_export" item="import"></span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-3 col-sm-3"><b>{{trans('business_details1.ie_exports')}}</b></div>
								<div class="col-md-9 col-sm-9">
									<input type="checkbox" class="cb_export" {{ ($entity->status == 0 && $entity->loginid == Auth::user()->loginid) ? '' : 'disabled' }} {{ ($businesstypeinfo->is_export == 1) ? "checked='checked'" : "" }} data-toggle="toggle" data-on="Yes" data-off="No" data-size="mini" />
									<span class="investigator-control" group_name="Import/Export Business" label="Exports" value="{{($businesstypeinfo->is_export == 1) ? 'YES' : 'NO'}}" group="import_export" item="export"></span>
								</div>
							</span>
						@endforeach
					</div>
				</div>
			</div>
		@endif
	@endif

	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->insurance)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('insurance-list-cntr', $tql->data)))
			<div id = 'insurance-list-cntr' class="spacewrapper">
				<div class="read-only reload-insurance">
					<h4>{{trans('business_details1.insurance_coverage')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.insurance_coverage_hint')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
					 @if ($insuarance)
					 	<span class="details-row hidden-xs hidden-sm"><div class="col-md-6 col-sm-6"><b>{{trans('business_details1.insurance_type')}}</b></div><div class="col-md-6 col-sm-6"><b>{{trans('business_details1.insured_amount')}}</b></div></span>
					 @endif

					 @if(Auth::user()->role == 2 || Auth::user()->role == 3)
					 	<div class="completeness_check">{{ @count($insuarance)>0 ? "true" : "" }}</div>
					 @endif

					 @if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->insurance)
					 	<span class="progress-required-counter" min-value="2"> {{ @count($insuarance) > 0 ? 2 : 0 }} </span>
					 @elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->insurance)
					 	<span class="progress-field-counter" min-value="2"> {{ @count($insuarance) > 0 ? 2 : 0 }} </span>
					 @else

					 @endif

				 	@foreach ($insuarance as $insuaranceinfo)
						<span class="details-row">
							<div class="col-md-6 col-sm-6">
							@if ($entity->loginid == Auth::user()->loginid)
								@if ($entity->status == 0 or $entity->status == 3)
									<span class="hidden-xs hidden-sm">
									 <a href="{{ URL::to('/') }}/sme/insurance/{{ $insuaranceinfo->insuranceid }}/delete" class="navbar-link delete-info" data-reload = '.reload-insurance' data-cntr = '#insurance-list-cntr'>{{trans('messages.delete')}}</a>
									</span>
								@endif
							@endif
							<b class="visible-xs-block visible-sm-block">{{trans('business_details1.insurance_type')}}<br /></b>
							<span class = 'editable-field' data-name = 'insurance.insurance_type' data-field-id = '{{ $insuaranceinfo->insuranceid }}'>
								{{ $insuaranceinfo->insurance_type }}
							</span>
							</div>
							<div class="col-md-6 col-sm-6">
								<b class="visible-xs-block visible-sm-block">{{trans('business_details1.insured_amount')}}<br /></b>
								<span class = 'editable-field' data-name = 'insurance.insured_amount' data-field-id = '{{ $insuaranceinfo->insuranceid }}'>
									{{ number_format($insuaranceinfo->insured_amount, 2) }}
								</span>
								<span class="investigator-control" group_name="Insurance Coverage" label="Insurance Type" value="{{ $insuaranceinfo->insurance_type }}" group="insurance_coverage" item="insurance_{{$insuaranceinfo->insuranceid}}"></span>
							</div>
							<div class="col-xs-12 visible-xs-block visible-sm-block">
							 @if ($entity->loginid == Auth::user()->loginid)
								 @if ($entity->status == 0 or $entity->status == 3)
								 	<a href="{{ URL::to('/') }}/sme/insurance/{{ $insuaranceinfo->insuranceid }}/delete" class="navbar-link delete-info" data-reload = '.reload-insurance' data-cntr = '#insurance-list-cntr'>{{trans('messages.delete')}}</a>
								 @endif
							 @endif
							</div>
							</span>
						@endforeach
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right; width: 100%;">
						@if ($entity->loginid == Auth::user()->loginid)
							<a id = 'show-insurance-expand' href="{{ URL::to('/') }}/sme/insurance/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details1.insurance_coverage')}}</a>
						@endif
						</span>
					@endif

					<div id = 'expand-insurance-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif

	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->cash_conversion)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('cash-conversion-cntr', $tql->data)))
			@if (Auth::user()->role != 2)
				<div id="cash-conversion-cntr" class="spacewrapper">
					<div class="read-only">
						<h4>{{trans('business_details1.cash_conversion_cyle')}} (days)<a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details1.cash_conversion_cyle_hint')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							@if(Auth::user()->role == 2 || Auth::user()->role == 3)
								<div class="completeness_check">{{ @count($cycledetails)>0 ? "true" : "" }}</div>
							@endif

							@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->cash_conversion)
								<span class="progress-required-counter" min-value="5"> {{ @count($cycledetails) > 0 ? 5 : 0 }} </span>
							@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->cash_conversion)
								<span class="progress-field-counter" min-value="5"> {{ @count($cycledetails) > 0 ? 5 : 0 }} </span>
							@else

							@endif

							@if ($entity->status == 0 AND $entity->loginid == Auth::user()->loginid)
								{{ Form::open( array('route' => array('postCycleDetail', $entity->entityid), 'role'=>'form', 'autocomplete'=>'off', 'id'=>'ccc-form')) }}
							@endif
							<div class="alert alert-danger ccc-message" role="alert" style="display: none;"></div>

							<div class="table-responsive">
								<table class="table table-bordered table-striped ccc-table" cellspacing="0">
									<tr>
										<td></td>
										<td>Month 1</td>
										<td>Month 2</td>
										<td>Month 3</td>
									</tr>
									<tr>
										<td>{{ trans('business_details1.ccc_credit_sales') }} (PHP)</td>
										<td style="padding: 0;"><input readonly="readonly" name="credit_sales[0]" type="text" class="credit_sales_0 cycle-input" value="{{ isset($cycledetails[0]) ? $cycledetails[0]->credit_sales : '' }}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="credit_sales[1]" type="text" class="credit_sales_1 cycle-input" value="{{ isset($cycledetails[1]) ? $cycledetails[1]->credit_sales : '' }}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="credit_sales[2]" type="text" class="credit_sales_2 cycle-input" value="{{ isset($cycledetails[2]) ? $cycledetails[2]->credit_sales : '' }}" /></td>
									</tr>
									<tr>
										<td>{{ trans('business_details1.ccc_accounts_receivables') }} (PHP)</td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_receivable[0]" type="text" class="accounts_receivable_0 cycle-input" value="{{ isset($cycledetails[0]) ? $cycledetails[0]->accounts_receivable : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_receivable[1]" type="text" class="accounts_receivable_1 cycle-input" value="{{ isset($cycledetails[1]) ? $cycledetails[1]->accounts_receivable : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_receivable[2]" type="text" class="accounts_receivable_2 cycle-input" value="{{ isset($cycledetails[2]) ? $cycledetails[2]->accounts_receivable : ''}}" /></td>
									</tr>
									<tr>
										<td>{{ trans('business_details1.ccc_inventory') }}  (PHP)</td>
										<td style="padding: 0;"><input readonly="readonly" name="inventory[0]" type="text" class="inventory_0 cycle-input" value="{{ isset($cycledetails[0]) ? $cycledetails[0]->inventory : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="inventory[1]" type="text" class="inventory_1 cycle-input" value="{{ isset($cycledetails[1]) ? $cycledetails[1]->inventory : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="inventory[2]" type="text" class="inventory_2 cycle-input" value="{{ isset($cycledetails[2]) ? $cycledetails[2]->inventory : ''}}" /></td>
									</tr>
									<tr>
										<td>{{ trans('business_details1.ccc_cost_of_sales') }} (PHP)</td>
										<td style="padding: 0;"><input readonly="readonly" name="cost_of_sales[0]" type="text" class="cost_of_sales_0 cycle-input" value="{{ isset($cycledetails[0]) ? $cycledetails[0]->cost_of_sales : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="cost_of_sales[1]" type="text" class="cost_of_sales_1 cycle-input" value="{{ isset($cycledetails[1]) ? $cycledetails[1]->cost_of_sales : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="cost_of_sales[2]" type="text" class="cost_of_sales_2 cycle-input" value="{{ isset($cycledetails[2]) ? $cycledetails[2]->cost_of_sales : ''}}" /></td>
									</tr>
									<tr>
										<td>{{ trans('business_details1.ccc_accounts_payable') }} (PHP)</td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_payable[0]" type="text" class="accounts_payable_0 cycle-input" value="{{ isset($cycledetails[0]) ? $cycledetails[0]->accounts_payable : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_payable[1]" type="text" class="accounts_payable_1 cycle-input" value="{{ isset($cycledetails[1]) ? $cycledetails[1]->accounts_payable : ''}}" /></td>
										<td style="padding: 0;"><input readonly="readonly" name="accounts_payable[2]" type="text" class="accounts_payable_2 cycle-input" value="{{ isset($cycledetails[2]) ? $cycledetails[2]->accounts_payable : ''}}" /></td>
									</tr>
									<tr>
										<td style="border-top: 2px solid; font-size: 15px; font-weight: bold;">{{ trans('business_details1.cash_conversion_cyle') }} (days)</td>
										<td style="border-top: 2px solid; font-size: 15px; font-weight: bold; text-align: right;" class="ccc_0">{{ isset($cycledetails[0]) ? $cycledetails[0]->ccc : '' }}</td>
										<td style="border-top: 2px solid; font-size: 15px; font-weight: bold; text-align: right;" class="ccc_1">{{ isset($cycledetails[1]) ? $cycledetails[1]->ccc : '' }}</td>
										<td style="border-top: 2px solid; font-size: 15px; font-weight: bold; text-align: right;" class="ccc_2">{{ isset($cycledetails[2]) ? $cycledetails[2]->ccc : '' }}</td>
									</tr>
								</table>
							</div>
							<p><i>{{ trans('business_details1.note_three_months') }}</i></p>

							@if ($entity->status == 0 AND $entity->loginid == Auth::user()->loginid)
								<span class="details-row" style="text-align: right; width: 100%;">
								<input type="button" class="btn btn-primary submit-ccc-form" value="{{ trans('messages.save') }}">
								</span>
								{{ Form::close() }}
							@endif
						</div>
					</div>
				</div>
			@endif
		@endif
	@endif

	@if(!Session::has('tql') || (Session::has('tql') && in_array('financial-statement-cntr', $tql->data)))
		@if (Auth::user()->role != 2)

			<div id="fspdf-upload-cntr" class="spacewrapper print-none">
				<div class="read-only">
					<h4>{{ trans('messages.fs_pdf') }}</h4>
					<div class="read-only-text">
						<p class="col-md-12">{{trans('messages.fspdf_hint')}}</p>

						<div id='fspdf-upload-list-cntr' class="details-row">
							<div class='reload-fspdf-upload'>
							    <span class="progress-required-counter" min-value="1">
									@if (0 >= @count($fsPdfFiles))
										0
									@else
										1
									@endif
							    </span>
								@if (count($fsPdfFiles) != 0)

									<table class="table table-hover table-bordered">
									    <thead>
									    <tr>
											<th scope="col" class="col-md-1 text-center">#</th>
											<th scope="col" class="col-md-7">{{ trans('messages.file_name') }}</th>
											<th scope="col" class="col-md-4 text-center">{{ trans('messages.action') }}</th>
									    </tr>
									    </thead>
									    <tbody>
									    
										@foreach($fsPdfFiles as $key => $filename)
										<tr>
											<th scope="row" class="text-center">{{ $key+1 }}</th>

											<td><a target="_blank">{!! $filename->file_name !!}</a></td>

											<td class="text-center">
												<a title="{{ trans('messages.download')}}" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ URL::to('/') }}/sme/download_fspdfFile/{{ $filename->id }}"><span class="glyphicon glyphicon-download-alt"></span></a>
												@if ($entity->status == 0 or $entity->status == 3)
												<a href="{{ URL::to('/') }}/sme/delete_fspdfFile/{{ $filename->id }}"  title="{{ trans('messages.delete')}}" class="navbar-link delete-file btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-fspdf-upload' data-cntr = '#fspdf-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
											@endif
											</td>
										</tr>
										@endforeach
									    </tbody>
									</table>
								@endif

								<div id = 'expand-fspdf-upload-form' class = 'spacewrapper'></div>

								@if ($entity->status == 0 or $entity->status == 3)
									<span class="details-row" style="text-align: right;">
								@if ($entity->loginid == Auth::user()->loginid)
									<a id = 'show-fspdf-upload-expand' href="{{ URL::to('/') }}/sme/fspdf_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.upload_file') }}</a>
								@endif
									</span>
								@endif
						  	</div>
						</div>
					</div>
				</div>
			</div>

			<div id="fs-template-cntr" class="spacewrapper print-none">
				<div class="read-only">
					<h4>{{ trans('business_details1.financial_statement_template') }}</h4>
					<div class="read-only-text">
						<div class="col-md-12">
							<p style="text-align: justify;">
								{{ trans('business_details1.financial_statement_template_hint') }}
							</p>
							<br />
							
							<a  class='btn btn-link' id='download_fs_link' href="{{ URL::to('/') }}/getFsTemplateFile/{{ $entity->entityid }}">
								{{ trans('business_details1.financial_statement_template_link') }}
							</a>
						</div>

						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ (@count($fs_template) >= 1) ? "true" : "" }}</div>
						@endif

						<div id = 'fs-template-list-cntr' class="details-row">
							<div class = 'reload-fs-template'>

							@if ($doc_req)
								@if (STS_NG == $doc_req->fs_template)
									<span class="progress-field-counter" min-value="1">
								@else
									<span class="progress-required-counter" min-value="0">
								@endif
							@else
								<span class="progress-required-counter" min-value="0">
							@endif

							@if (0 >= @count($fs_template))
								0
							@else
								1
							@endif

							</span>

							@if (@count($fs_template) != 0)
								@foreach ($fs_template as $documents2)
								<div class="grouping">
									<div class="col-md-2">
										<span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents2->documentid }}'>
											@if($documents2->document_type == 1)
												{{trans('business_details1.unaudited')}}
											@elseif($documents2->document_type == 2)
												{{trans('business_details1.interim')}}
											@elseif($documents2->document_type == 4)
												{{trans('business_details1.recasted')}}
											@else
												{{trans('business_details1.audited')}}
											@endif
										</span>
									</div>
									<div class="col-md-8">
										<a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
									</div>
									<div class="col-md-2">
										@if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
										@if(@count($fs_template) === 1)
										<input type="hidden" value="1" id="count_fs_template">
										@else
										<input type="hidden" value="2" id="count_fs_template">
										@endif
										<a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents2->documentid }}" class="navbar-link delete-info" data-reload = '.reload-fs-template' data-cntr = '#fs-template-list-cntr'>{{ trans('messages.delete') }}</a>
										@endif
										<span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents2->document_orig }}" group="documents" item="balance_sheet_{{$documents2->documentid}}"></span>
									</div>
								</div>
								@endforeach
							@endif

							<!-- @if(!empty($quickbooks_reports))
								@foreach($quickbooks_reports as $documents3)
								<div class="grouping">
									<div class="col-md-2">
										<span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents3->documentid }}'>
										
											{{trans('business_details1.audited')}}
										
										</span>
									</div>
									<div class="col-md-8">
										<a href="{{ URL::to('/') }}/download/documents/quickbooks/{{ $documents3->document_rename }}" target="_blank">{{ $documents3->document_orig }}</a>
									</div>
									<div class="col-md-2">
										@if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
										@if(@count($quickbooks) === 1)
										<input type="hidden" value="1" id="count_quickbooks">
										@else
										<input type="hidden" value="2" id="count_quickbooks">
										@endif
										<a href="{{ URL::to('/') }}/sme/delete_quickbooks/{{ $documents3->documentid }}" class="navbar-link delete-info" data-reload = '.reload-fs-template' data-cntr = '#fs-template-list-cntr'>{{ trans('messages.delete') }}</a>
										@endif
										<span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents3->document_orig }}" group="documents" item="balance_sheet_{{$documents3->documentid}}"></span>
									</div>
								</div>

								@php
									break;
								@endphp
								@endforeach
							@endif -->

							<div id = 'expand-fs-template-form' class = 'spacewrapper'></div>
							<div id = 'expand-quickbooks-form' class = 'spacewrapper'></div>

							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right;">
								@if ($entity->loginid == Auth::user()->loginid)
									@if (0 >= @count($fs_template))
										<a id = 'show-fs-template-expand' href="{{ URL::to('/') }}/sme/upload_fs_template/{{ $entity->entityid }}" class="navbar-link" style="margin-right: 10px">Upload {{ trans('business_details1.financial_statement_template') }}</a>
										<a id = 'show-quickbooks-expand' href="{{ URL::to('/') }}/sme/upload_quickbooks/{{ $entity->entityid }}" class="navbar-link">Upload {{ trans('business_details1.quickbooks_reports') }}</a>
									@endif
								@endif
							</span>
							@endif

							
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

	@if($bank_standalone == 0)
		@if (Auth::user()->role != 2)
			<div id="financial-statement-cntr" class="spacewrapper print-none">
				<div class="read-only">
					<h4>{{trans('business_details1.financial_statement')}}</h4>
					<div class="read-only-text">
						<div class="col-md-12">
							<p>{{trans('business_details1.document_upload_hint')}}</p>
							<p>{{trans('business_details1.upload_balance_sheet')}}</p>
							<p>{{trans('business_details1.upload_income_statement')}}</p>
							<p>E.g.:</p>
							<p>{{trans('business_details1.balance_sheet_eg')}}</p>
							<p>{{trans('business_details1.income_statement_eg')}}</p>
						</div>

						<div class="col-md-12">
							<p><b>Financial Statements</b></p>
						</div>
						<div id = 'financials-list-cntr' class="details-row">
							<div class = 'reload-financials'>

								@if (@count($fs_supporting) != 0)
									@foreach ($fs_supporting as $documents2)
										<div class="grouping">
											<div class="col-md-2">
												<span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents2->documentid }}'>
													@if($documents2->document_type == 1)
														{{trans('business_details1.unaudited')}}
													@elseif($documents2->document_type == 2)
														{{trans('business_details1.interim')}}
													@elseif($documents2->document_type == 4)
														{{trans('business_details1.recasted')}}
													@else
														{{trans('business_details1.audited')}}
													@endif
												</span>
											</div>
											<div class="col-md-8">
												<a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
											</div>
											<div class="col-md-2">
												@if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
												<a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents2->documentid }}" class="navbar-link delete-info" data-reload = '.reload-financials' data-cntr = '#financials-list-cntr'>{{ trans('messages.delete') }}</a>
												@endif
												<span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.balance_sheet',[],'en')}}" value="{{ $documents2->document_orig }}" group="documents" item="balance_sheet_{{$documents2->documentid}}"></span>
											</div>
										</div>
									@endforeach
								@endif

								@if (@count($documents3) != 0)
									@foreach ($documents3 as $documents3)
										<div class="grouping">
											<div class="col-md-2">
												<span class = 'editable-field pull-right text-right' data-name = 'financials.document_type' data-field-id = '{{ $documents3->documentid }}'>
													@if($documents3->document_type == 1)
													{{trans('business_details1.unaudited')}}
													@elseif($documents3->document_type == 2)
													{{trans('business_details1.interim')}}
													@elseif($documents2->document_type == 4)
													{{trans('business_details1.recasted')}}
													@else
													{{trans('business_details1.audited')}}
													@endif
												</span>
											</div>
											<div class="col-md-8">
												<a href="{{ URL::to('/') }}/download/documents/{{ $documents3->document_rename }}" target="_blank">{{ $documents3->document_orig }}</a>
											</div>
											<div class="col-md-2">
												@if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
												<a href="{{ URL::to('/') }}/sme/delete_document/{{ $documents3->documentid }}" class="navbar-link delete-info" data-reload = '.reload-financials' data-cntr = '#financials-list-cntr'>delete</a>
												@endif
												<span class="investigator-control" group_name="Document Upload" label="{{trans('business_details1.income_statement',[],'en')}}" value="{{ $documents3->document_orig }}" group="documents" item="income_statement_{{$documents3->documentid}}"></span>
											</div>
										</div>
									@endforeach
								@endif

								<div id = 'expand-financials-form' class = 'spacewrapper'></div>
							</div>
						</div>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-financials-expand' href="{{ URL::to('/') }}/sme/upload_documents/{{ $entity->entityid }}" class="navbar-link">Upload {{trans('business_details1.financial_statement')}}</a>
							@endif
							</span>
						@endif
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row">
								<div class="col-md-12">
									@if(@count($documents2)==0 && @count($documents3)==0)
									<p style="color: RED;">{{ trans('business_details1.upload_fs_reminder') }}</p>
									@endif
								</div>
							</span>
						@endif
						</div>
					</div>
				</div>
			@endif
		@endif
	@endif

	@if(!Session::has('tql') || (Session::has('tql') && in_array('cash-proxy-cntr', $tql->data)))
	<div id="cash-proxy-cntr" class="spacewrapper print-none">
		<div class="read-only">
			<h4>{{ trans('business_details1.cash_proxy') }}</h4>
			<div class="read-only-text">
				<div class="col-md-12">
					<p> {{ trans('business_details1.cash_proxy_hint') }} </p>
				</div>
				<div id = 'bank-statements-list-cntr'>
					<div class = 'reload-bank-statements'>
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
						<div class="completeness_check">{{ @count($documents5)>=3 ? "true" : "" }}</div>
					@endif

					@if ($doc_req)
						@if (STS_NG == $doc_req->bank_statement)
							<span class="progress-field-counter" min-value="1">
						@else
							<span class="progress-required-counter" min-value="1">
						@endif
					@else
						<span class="progress-required-counter" min-value="1">
					@endif
					@if (@count($documents5) >= 3 || $bank_standalone == 1)
						1
					@else
						0
					@endif
					</span>

					@if (@count($documents5) != 0)
						<div class="col-md-12">
							<p><b>{{trans('business_details1.bank_statement')}} </b></p>
						</div>
						<div class="details-row">
						@foreach ($documents5 as $document)
							<div class="grouping">
								<div class="col-md-10">
									<a href="{{ URL::to('/') }}/download/documents/{{ $document->document_rename }}" target="_blank">{{ $document->document_orig }}</a>
									<span class="investigator-control" group_name="Cash Proxy" label="Bank Statements" value="{{ $document->document_orig }}" group="documents" item="bank_statement_{{$document->documentid}}"></span>
								</div>
								@if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
									<div class = 'col-md-2'>
										<a href="{{ URL::to('/') }}/sme/delete_cash_proxy/{{ $document->documentid }}" class="navbar-link delete-info" data-reload = '.reload-bank-statements' data-cntr = '#bank-statements-list-cntr'> {{ trans('messages.delete') }} </a>
									</div>
								@endif
							</div>
						@endforeach
						</div>
					@endif
					<div id = 'expand-bank-statements-form' class = 'spacewrapper'></div>
					</div>
					</div>
					<div id = 'utility-bills-list-cntr'>
					<div class = 'reload-utility-bills'>
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
						<div class="completeness_check">{{ @count($documents6)>=3 ? "true" : "" }}</div>
					@endif
					@if ($doc_req)
						@if (STS_NG == $doc_req->utility_bill)
						<span class="progress-field-counter" min-value="1">
						@else
							<span class="progress-required-counter" min-value="1">
						@endif
					@else
						<span class="progress-required-counter" min-value="1">
					@endif
					@if (@count($documents6) >= 3 || $bank_standalone == 1)
						1
					@else
						0
				@endif
				</span>

				@if (@count($documents6) != 0)
					<div class="col-md-12">
					<p> <b>{{trans('business_details1.utility_bill')}}</b> </p>
					</div>
					<div class="details-row">
						@foreach ($documents6 as $document)
							<div class="grouping">
							  <div class="col-md-10">
							    <a href="{{ URL::to('/') }}/download/documents/{{ $document->document_rename }}" target="_blank">{{ $document->document_orig }}</a>
							    <span class="investigator-control" group_name="Cash Proxy" label="Utility Bills" value="{{ $document->document_orig }}" group="documents" item="utility_bills_{{$document->documentid}}"></span>
							  </div>
							  @if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
							  <div class = 'col-md-2'>
							    <a href="{{ URL::to('/') }}/sme/delete_cash_proxy/{{ $document->documentid }}" class="navbar-link delete-info" data-reload = '.reload-utility-bills' data-cntr = '#utility-bills-list-cntr'> {{ trans('messages.delete') }} </a>
							  </div>
							  @endif
							</div>
						@endforeach
					</div>
				@endif
				<div id = 'expand-utility-bills-form' class = 'spacewrapper'></div>
				</div>
				</div>

				@if ($entity->status == 0 or $entity->status == 3)
					<span class="details-row" style="text-align: right;">
						@if ($entity->loginid == Auth::user()->loginid)
							<a id = 'show-bank-statements-expand' href="{{ URL::to('/') }}/sme/upload-bank-statements/{{ $entity->entityid }}" class="navbar-link"> {{ trans('messages.bank_statement') }} </a> |
							<a id = 'show-utility-bills-expand' href="{{ URL::to('/') }}/sme/upload-utility-bills/{{ $entity->entityid }}" class="navbar-link"> Utility Bills </a>
						@endif
					</span>
				@endif
				@if (($entity->status == 0 or $entity->status == 3) && $bank_standalone == 0)
					<span class="details-row">
						<div class="col-md-12">
							@if(@count($documents5)==0 && @count($documents6)==0)
								<p style="color: RED;">{{ trans('business_details1.upload_cash_proxy_reminder') }}</p>
							@else
								@if(@count($documents6)==0)
									<p style="color: RED;">{{ trans('business_details1.upload_utility_bill_reminder') }}</p>
								@endif
								@if(@count($documents5)==0)
									<p style="color: RED;">{{ trans('business_details1.upload_bank_doc_reminder') }}</p>
								@endif
							@endif
						</div>
					</span>
				@endif
				</div>
			</div>
		</div>
	@endif

	@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'registration1'))
		@if (1 == $entity->is_independent && @count($custom_docs) > 0)
			<div id="other-docs-cntr" class="spacewrapper print-none">
				<div class="read-only">
					<h4> Other Documents Requested by Bank </h4>
					<div class="read-only-text">
						<div class="col-md-12">
							<p></p>
						</div>
						<div id = 'other-documents-list-cntr'>
							<div class = 'reload-other-documents'>
								@if(Auth::user()->role == 2 || Auth::user()->role == 3)
									<div class="completeness_check">{{ 0 > @count($custom_doc) ? "true" : "" }}</div>
								@endif
								<span class="progress-required-counter" min-value="1">
									@if (0 >= @count($custom_doc))
										1
									@else
										0
									@endif
								</span>

								@if (@count($documents7) != 0)
									<div class="col-md-12">
										<p> <b> Other Documents Requested by Bank </b></p>
									</div>
									<div class="details-row">
										@foreach ($documents7 as $document)
											<div class="grouping">
												<div class="col-md-10">
													<a href="{{ URL::to('/') }}/download/documents/{{ $document->document_rename }}" target="_blank">{{ $document->document_orig }}</a>
													<span class="investigator-control" group_name="Other Documents" label="Other Document" value="{{ $document->document_orig }}" group="documents" item="other_documents_{{$document->documentid}}"></span>
												</div>
												@if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
													<div class = 'col-md-2'>
														<a href="{{ URL::to('/') }}/sme/delete_other_document/{{ $document->documentid }}" class="navbar-link delete-info" data-reload = '.reload-other-documents' data-cntr = '#other-documents-list-cntr'> {{ trans('messages.delete') }} </a>
													</div>
												@endif
											</div>
										@endforeach
									</div>
								@endif
								<div id = 'expand-other-documents-form' class = 'spacewrapper'></div>
							</div>
						</div>

						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-other-documents-expand' href="{{ URL::to('/') }}/sme/upload-other-documents/{{ $entity->entityid }}" class="navbar-link"> Upload Other Documents </a>
							@endif
							</span>
						@endif
					</div>
				</div>
			</div>
		@endif
	@endif

	<p style="font-size: 11px;margin-top:15px;margin-left:10px;" class = 'print-none'>{{trans('messages.disclaimer')}}</p>
	@if(!Session::has('tql'))
		<input type="button" id="nav_registration1_next" value="{{trans('messages.next')}}" />
	@endif
	</div>
@endif

@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'registration2'))
	<div id="registration2" class="tab-pane fade">
		@if(!Session::has('tql') || (Session::has('tql') && in_array('major-cust-list-cntr', $tql->data)))
			<div id = 'major-cust-list-cntr' class="spacewrapper">
				<div class="read-only reload-major-cust">
					<h4>{{trans('business_details2.major_customer')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.major_customer_hint')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							<div class="majorcustomer">
								@foreach ($majorcustomers as $customer)
									<input type="hidden" value="{{ $customer->customer_share_sales }}" name="totalsalevalue" class="totalsalevalue">
									<input type="hidden" value="{{ $customer->customer_experience }}" name="cexperience" class="texperience">

									<div class="details-row">
										<div class="col-md-6 col-sm-6">
											<b>{{trans('messages.name')}}</b>
										</div>
										<div class="col-md-6 col-sm-6">
										
											<span class = 'editable-field' data-name = 'major_cust.customer_name' data-field-id = '{{ $customer->majorcustomerrid }}'>
												@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
												Classified Information
												@else
												{{ $customer->customer_name }}
												@endif
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Name' group="major_customer" value="{{ $customer->customer_name }}" item="customer_name_{{ $customer->majorcustomerrid }}"></span>
										</div>
									</div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>Address</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_address' data-field-id = '{{ $customer->majorcustomerrid }}'>
												{{ $customer->customer_address }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Address' group="major_customer" value="{{ $customer->customer_address }}" item="customer_address_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6">
											<b>{{trans('messages.contact_person')}}</b>
										</div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_contact_person' data-field-id = '{{ $customer->majorcustomerrid }}'>
												{{ $customer->customer_contact_person }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Contact Person' group="major_customer" value="{{ $customer->customer_contact_person }}" item="customer_contact_person_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_phone')}}</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_phone' data-field-id = '{{ $customer->majorcustomerrid }}'>
												{{ $customer->customer_phone }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Contact Phone Number' group="major_customer" value="{{ $customer->customer_phone }}" item="customer_phone_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_email')}}</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_email' data-field-id = '{{ $customer->majorcustomerrid }}'>
												{{ $customer->customer_email }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Contact Email' group="major_customer" value="{{ $customer->customer_email }}" item="customer_email_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_year_started') }}</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_started_years' data-field-id = '{{ $customer->majorcustomerrid }}'>
												{{ $customer->customer_started_years }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Year Started Doing Business With' group="major_customer" value="{{ $customer->customer_started_years }}" item="customer_started_years_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_year_doing_business') }}</b></div>
										<div class="col-md-6 col-sm-6">
											<span id = '{{ $customer->majorcustomerrid }}-auto-calc-years'> {{ $customer->customer_years_doing_business }} </span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Years Doing Business With' group="major_customer" value="{{ $customer->customer_years_doing_business }}" item="customer_years_doing_business_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6">
											<b>{{ trans('business_details2.mc_relationship_satisfaction') }}</b>
										</div>
										<div class="col-md-6 col-sm-6">
											<span class="editable-field"
											  data-name="major_cust.relationship_satisfaction"
											  data-field-id="{{ $customer->majorcustomerrid }}">
											  @if ($customer->relationship_satisfaction == 1)
											    {{ trans('business_details2.mc_relation_good') }}
											  @elseif ($customer->relationship_satisfaction == 2)
											    {{ trans('business_details2.mc_relation_satisfactory') }}
											  @elseif ($customer->relationship_satisfaction == 3)
											    {{ trans('business_details2.mc_relation_unsatisfactory') }}
											  @elseif ($customer->relationship_satisfaction == 4)
											    {{ trans('business_details2.mc_relation_bad') }}
											  @else
											    {{ trans('business_details2.mc_relation_not_set') }}
											  @endif
											</span>
											<span
											  class="investigator-control"
											  group_name="Major Customer/s"
											  label="{{ trans('mc_relationship_satisfaction') }}"
											  group="major_customer"
											  value="{{ $customer->relationship_satisfaction }}"
											  item="typename_{{ $customer->majorcustomerrid }}">
											</span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_payment_behavior') }}</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_settlement' data-field-id = '{{ $customer->majorcustomerrid }}'>
												@if ($customer->customer_settlement == 1)
													{{ trans('business_details2.mc_ahead') }}
												@elseif ($customer->customer_settlement == 2)
													{{ trans('business_details2.mc_ondue') }}
												@elseif ($customer->customer_settlement == 3)
													{{ trans('business_details2.mc_portion') }}
												@elseif ($customer->customer_settlement == 4)
													{{ trans('business_details2.mc_beyond') }}
												@else
													{{ trans('business_details2.mc_delinquent') }}
												@endif
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Customer payment behavior in last 3 payments' group="major_customer" value="{{ $customer->customer_settlement }}" item="typename_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6">
										<b>{{ trans('business_details2.mc_percent_share') }}</b>
										</div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_share_sales' data-field-id = '{{ $customer->majorcustomerrid }}'>
											  {{ number_format($customer->customer_share_sales, 2) }}%
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = '% Share of Your Sales to this Company vs. Your Total Sales' group="major_customer" value="{{ $customer->customer_share_sales }}" item="customer_share_sales_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.mc_order_frequency') }}</b></div>
										<div class="col-md-6 col-sm-6">
											<span class = 'editable-field' data-name = 'major_cust.customer_order_frequency' data-field-id = '{{ $customer->majorcustomerrid }}'>
											 	{{ trans('business_details2.'.$customer->customer_order_frequency) }}
											</span>
											<span class="investigator-control" group_name="Major Customer/s" label = 'Order Frequency' group="major_customer" value="{{ $customer->customer_order_frequency }}" item="customer_order_frequency_{{ $customer->majorcustomerrid }}"></span>
										</div>
								    </div>

								    <div class="details-row">
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
										<div class="col-md-12">
											<a href = "{{ URL::To('/sme/majorcustomer/'.$customer->majorcustomerrid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-cust' data-cntr = '#major-cust-list-cntr'>{{ trans('messages.delete') }}</a>
										</div>
										@endif
								    </div>
							    @endforeach
							</div>
							<div class="details-row no-major-cust-cntr" style="text-align: left; width: 100%;">
								<div class = 'reload-no-major-cust'>
									@if (isset($majorcustomeraccount[0]->is_agree))
										{{ Form::hidden('is_agree', $majorcustomeraccount[0]->is_agree, array('id' => 'is_agree')) }}
										@if (1 == $majorcustomeraccount[0]->is_agree)
											@if(Auth::user()->role == 2 || Auth::user()->role == 3)
											<div class="completeness_check">true</div>
										@endif
										<span class="progress-required-counter" min-value="8">8</span>
										<label>
										<input class = 'no-major-customer' type="checkbox" value="1" checked = "checked" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
										</label>
									@else
										@if($bank_standalone == 0)
											@if (0 == $progress['majorcustomer'])
												@if(Auth::user()->role == 2 || Auth::user()->role == 3)
													<div class="completeness_check">false</div>
												@endif
													<span class="progress-required-counter" min-value="8">0</span>
												@else
													@if(Auth::user()->role == 2 || Auth::user()->role == 3)
														<div class="completeness_check">true</div>
													@endif
													<span class="progress-required-counter" min-value="8">8</span>
												@endif
											@else
											<div class="completeness_check">true</div>
											<span class="progress-required-counter" min-value="8">8</span>
											@endif
											<label>
											<input class = 'no-major-customer' type="checkbox" value="1" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
											</label>
										@endif
									@else
										{{ Form::hidden('is_agree', 0, array('id' => 'is_agree')) }}
										@if($bank_standalone == 0)
											@if (0 == $progress['majorcustomer'])
												@if(Auth::user()->role == 2 || Auth::user()->role == 3)
													<div class="completeness_check">false</div>
												@endif
													<span class="progress-required-counter" min-value="8">0</span>
												@else
													@if(Auth::user()->role == 2 || Auth::user()->role == 3)
														<div class="completeness_check">true</div>
													@endif
													<span class="progress-required-counter" min-value="8">8</span>
												@endif
											@else
												<div class="completeness_check">true</div>
											<span class="progress-required-counter" min-value="8">8</span>
										@endif
										<label>
										<input class = 'no-major-customer' type="checkbox" value="1" {{ ($entity->status == 0 && Auth::user()->role == 1) ? '' : 'disabled="disabled"' }} /> {{ trans('business_details2.mc_no_major_customer') }}
										</label>
									@endif
									<span class="investigator-control" group_name="Major Customer/s" label="No Individual Customer" value="No Individual Customer" group="major_customer" item="is_agree"></span>
								</div>
							</div>
							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right; width: 100%;">
								@if ($entity->loginid == Auth::user()->loginid)
									<a id = 'show-major-cust-expand' href="{{ URL::to('/') }}/sme/majorcustomer/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_customer')}}</a>
								@endif
							</span>
				 @endif

				 <div id = 'expand-major-cust-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif

	@if(!Session::has('tql') || (Session::has('tql') && in_array('major-supp-list-cntr', $tql->data)))
		<div id = 'major-supp-list-cntr' class="spacewrapper">
			 <div class="read-only reload-major-supp">
				<h4>{{trans('business_details2.major_supplier')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.ms_hint')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						<div class="majorsupplier">
						@foreach($majorsuppliers as $supplier)
						<input type="hidden" value="{{ $supplier->supplier_share_sales }}" name="totalsuppliersalevalue" class="totalsuppliersalevalue">
						<input type="hidden" value="{{ $supplier->supplier_experience }}" name="sexperience" class="texperience">

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{trans('messages.name')}}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_name' data-field-id = '{{ $supplier->majorsupplierid }}'>
									@if ((1 != Auth::user()->role) AND (1 == $entity->anonymous_report))
										Classified Information
									@else
										{{ $supplier->supplier_name }}
									@endif
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Name' group="major_supplier" value="{{ $supplier->supplier_name }}" item="supplier_name_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>Address</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_address' data-field-id = '{{ $supplier->majorsupplierid }}'>
									{{ $supplier->supplier_address }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Address' group="major_supplier" value="{{ $supplier->supplier_address }}" item="supplier_address_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_person')}}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_contact_person' data-field-id = '{{ $supplier->majorsupplierid }}'>
								{{ $supplier->supplier_contact_person }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Person' group="major_supplier" value="{{ $supplier->supplier_contact_person }}" item="supplier_contact_person_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_phone')}}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_phone' data-field-id = '{{ $supplier->majorsupplierid }}'>
								{{ $supplier->supplier_phone }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Phone Number' group="major_supplier" value="{{ $supplier->supplier_phone }}" item="supplier_phone_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{trans('messages.contact_email')}}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_email' data-field-id = '{{ $supplier->majorsupplierid }}'>
								{{ $supplier->supplier_email }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Contact Email' group="major_supplier" value="{{ $supplier->supplier_email }}" item="supplier_email_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
						<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_year_started') }}</b></div>
						<div class="col-md-6 col-sm-6">
							<span class = 'editable-field' data-name = 'major_supp.supplier_started_years' data-field-id = '{{ $supplier->majorsupplierid }}'>
							{{ $supplier->supplier_started_years }}
							</span>
							<span class="investigator-control" group_name="Major Supplier/s" label = 'Year Started Doing Business With' group="major_supplier" value="{{ $supplier->supplier_started_years }}" item="supplier_started_years_{{ $supplier->majorsupplierid }}"></span>
						</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_year_doing_business') }}</b></div>
							<div class="col-md-6 col-sm-6"> <span id = '{{ $supplier->majorsupplierid }}-auto-calc-years'> {{ $supplier->supplier_years_doing_business }} </span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Years Doing Business With' group="major_supplier" value="{{ $supplier->supplier_years_doing_business }}" item="supplier_years_doing_business_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6">
							<b>{{ trans('business_details2.ms_relationship_satisfaction') }}</b>
							</div>
							<div class="col-md-6 col-sm-6">
								<span class="editable-field"
								  data-name="major_supp.relationship_satisfaction"
								  data-field-id="{{ $supplier->majorsupplierid }}">
									@if ($supplier->relationship_satisfaction == 1)
										{{ trans('business_details2.ms_relation_good') }}
									@elseif ($supplier->relationship_satisfaction == 2)
										{{ trans('business_details2.ms_relation_satisfactory') }}
									@elseif ($supplier->relationship_satisfaction == 3)
										{{ trans('business_details2.ms_relation_unsatisfactory') }}
									@elseif ($supplier->relationship_satisfaction == 4)
										{{ trans('business_details2.ms_relation_bad') }}
									@else
										{{ trans('business_details2.ms_relation_not_set') }}
									@endif
								</span>
								<span
								  class="investigator-control"
								  group_name="Major Supplier/s"
								  label="{{ trans('ms_relationship_satisfaction') }}"
								  group="major_supplier"
								  value="{{ $supplier->relationship_satisfaction }}"
								  item="typename_{{ $supplier->majorsupplierid }}">
								</span>
							</div>
						</div>

						<div class="details-row">
						<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_payment') }}</b></div>
						<div class="col-md-6 col-sm-6">
							<span class = 'editable-field' data-name = 'major_supp.supplier_settlement' data-field-id = '{{ $supplier->majorsupplierid }}'>
								@if ($supplier->supplier_settlement == 1)
									{{ trans('business_details2.ms_ahead') }}
								@elseif ($supplier->supplier_settlement == 2)
									{{ trans('business_details2.ms_ondue') }}
								@elseif ($supplier->supplier_settlement == 3)
									{{ trans('business_details2.ms_portion') }}
								@elseif ($supplier->supplier_settlement == 4)
									{{ trans('business_details2.ms_beyond') }}
								@else
									{{ trans('business_details2.ms_delinquent') }}
								@endif
							</span>

							<span class="investigator-control" group_name="Major Supplier/s" label = 'Your payment to supplier in last 3 payments' group="major_supplier" value="{{ $supplier->supplier_settlement }}" item="typename_{{ $supplier->majorsupplierid }}"></span>
						</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_percentage_share') }}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_share_sales' data-field-id = '{{ $supplier->majorsupplierid }}'>
								{{ number_format($supplier->supplier_share_sales, 2) }}%
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = '% Share of Total Purchases' group="major_supplier" value="{{ $supplier->supplier_share_sales }}" item="supplier_share_sales_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>{{ trans('business_details2.ms_frequency') }}</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.supplier_order_frequency' data-field-id = '{{ $supplier->majorsupplierid }}'>
								{{ trans('business_details2.'.$supplier->supplier_order_frequency) }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Payment Frequency' group="major_supplier" value="{{ $supplier->supplier_order_frequency }}" item="supplier_order_frequency_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-6 col-sm-6"><b>Average Monthly Volume</b></div>
							<div class="col-md-6 col-sm-6">
								<span class = 'editable-field' data-name = 'major_supp.average_monthly_volume' data-field-id = '{{ $supplier->majorsupplierid }}'>
								  {{ $supplier->average_monthly_volume }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Average Monthly Volume' group="major_supplier" value="{{ $supplier->average_monthly_volume }}" item="average_monthly_volume_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							<div class="col-md-12"><b>Items Purchased (List of three distinct items)</b></div>
							<div class="col-md-12">
								<span class = 'editable-field' data-name = 'major_supp.item_1' data-field-id = '{{ $supplier->majorsupplierid }}'>
									{{ $supplier->item_1 }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 1' group="major_supplier" value="{{ $supplier->item_1 }}" item="item_1_{{ $supplier->majorsupplierid }}"></span>
							</div>
							<div class="col-md-12">
								<span class = 'editable-field' data-name = 'major_supp.item_2' data-field-id = '{{ $supplier->majorsupplierid }}'>
									{{ $supplier->item_2 }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 2' group="major_supplier" value="{{ $supplier->item_2 }}" item="item_2_{{ $supplier->majorsupplierid }}"></span>
							</div>
							<div class="col-md-12">
								<span class = 'editable-field' data-name = 'major_supp.item_3' data-field-id = '{{ $supplier->majorsupplierid }}'>
									{{ $supplier->item_3 }}
								</span>
								<span class="investigator-control" group_name="Major Supplier/s" label = 'Items Purchased 3' group="major_supplier" value="{{ $supplier->item_3 }}" item="item_3_{{ $supplier->majorsupplierid }}"></span>
							</div>
						</div>

						<div class="details-row">
							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								<div class="col-md-12">
									<a href = "{{ URL::To('/sme/majorsupplier/'.$supplier->majorsupplierid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-supp' data-cntr = '#major-supp-list-cntr'>{{ trans('messages.delete') }}</a>
								</div>
							@endif
						</div>
				@endforeach
				</div>
				<div class="details-row no-major-supp-cntr" style="text-align: left; width: 100%;">
					<div class = 'reload-no-major-supp'>
						@if($bank_standalone == 0)
							@if (0 == $progress['majorsupplier'])
								@if(Auth::user()->role == 2 || Auth::user()->role == 3)
									<div class="completeness_check">false</div>
								@endif
								<span class="progress-required-counter" min-value="8">0</span>
							@else
							@if(Auth::user()->role == 2 || Auth::user()->role == 3)
								<div class="completeness_check">true</div>
							@endif
								<span class="progress-required-counter" min-value="8">8</span>
							@endif
							@else
								<div class="completeness_check">true</div>
								<span class="progress-required-counter" min-value="8">8</span>
						@endif
					</div>
				</div>
				@if ($entity->status == 0 or $entity->status == 3)
					<span class="details-row" style="text-align: right;">
				@if ($entity->loginid == Auth::user()->loginid)
					<a id = 'show-major-supp-expand' href="{{ URL::to('/') }}/sme/majorsupplier/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_supplier')}}</a>
				@endif
					</span>
				@endif

				<div id = 'expand-major-supp-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->major_market)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('major-loc-list-cntr', $tql->data)))
	<div id = 'major-loc-list-cntr' class="spacewrapper">
		<div class="read-only reload-major-loc">
			<h4>{{trans('business_details2.major_market_location')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.major_market_location_hint')}}">{{trans('messages.more_info')}}</a></h4>
			<div class="read-only-text">
				@if(Auth::user()->role == 2 || Auth::user()->role == 3)
					<div class="completeness_check">{{ $progress['majormarketlocation']==1 ? "true" : "" }}</div>
				@endif

				@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->major_market)
					<span class="progress-required-counter" min-value="1"> {{ @count($majorlocations) > 0 ? 1 : 0 }} </span>
				@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->major_market)
					<span class="progress-field-counter" min-value="1"> {{ @count($majorlocations) > 0 ? 1 : 0 }} </span>
				@else

				@endif

				<div class="majorlocation">
					@foreach ($majorlocations as $major_loc)
						<div class="details-row">
							<div class="col-md-12">
								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									<a href = "{{ URL::To('/sme/majorlocation/'.$major_loc->marketlocationid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-major-loc' data-cntr = '#major-loc-list-cntr'>{{ trans('messages.delete') }}</a>
								@endif
								<span class = 'editable-field' data-name = 'major_loc.marketlocation' data-field-id = '{{ $major_loc->marketlocationid }}'>
									
									@php
										$city = '';
										if(!empty($major_loc->city))
											$city = $major_loc->city;
									@endphp

								{{ $city }}

								</span>
								<span class="investigator-control" group_name="Major Market Location"  label="Major Market Location" value="{{ $city }}" group="major_market_location" item="location_{{ $major_loc->marketlocationid }}"></span>

							</div>
						</div>
					@endforeach
				</div>
			
				@if ($entity->status == 0 or $entity->status == 3)
					<span class="details-row" style="text-align: right;">
						 @if ($entity->loginid == Auth::user()->loginid)
						 	<a id = 'show-major-loc-expand' href="{{ URL::to('/') }}/sme/majorlocation/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.major_market_location')}}</a>
						 @endif
					</span>
				@endif

				<div id = 'expand-major-loc-form' class = 'spacewrapper'></div>
			</div>
		</div>
	</div>
	@endif
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->competitors)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('competitor-list-cntr', $tql->data)))
		<div id = 'competitor-list-cntr' class="spacewrapper">
			<div class="read-only reload-competitor">
				<h4>{{trans('business_details2.competitors')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.competitors_hint')}}">{{trans('messages.more_info')}}</a></h4>
				<div class="read-only-text">
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
						<div class="completeness_check">{{ $progress['competitor']==1 ? "true" : "" }}</div>
					@endif

					@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->competitors)
						<span class="progress-required-counter" min-value="3"> {{ @count($competitors) > 0 ? 3 : 0 }} </span>
					@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->competitors)
						<span class="progress-field-counter" min-value="3"> {{ @count($competitors) > 0 ? 3 : 0 }} </span>
					@else

				@endif

				<div class="competitor">
					<div class="details-row hidden-xs hidden-sm">
						<div class="col-md-3 col-sm-3"><b>Name</b></div>
						<div class="col-md-3 col-sm-3"><b>Address</b></div>
						<div class="col-md-3 col-sm-3"><b> Contact Phone Number</b></div>
						<div class="col-md-3 col-sm-3"><b> Contact Email</b></div>
					</div>

					@foreach ($competitors as $competitor)
						<div class="details-row hidden-xs hidden-sm">
							<div class="col-md-3 col-sm-3">
								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									<a href = "{{ URL::To('/sme/competitor/'.$competitor->competitorid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-competitor' data-cntr = '#competitor-list-cntr'>{{ trans('messages.delete') }}</a>
								@endif
								<span class = 'editable-field' data-name = 'competitor.competitor_name' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_name }}
								</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<span class = 'editable-field' data-name = 'competitor.competitor_address' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_address }}
								</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<span class = 'editable-field' data-name = 'competitor.competitor_phone' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_phone }}
								</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<span class = 'editable-field' data-name = 'competitor.competitor_email' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_email }}
								</span>
								<span class="investigator-control" group_name="Competitors"  label="Name" value="{{ $competitor->competitor_name }}" group="competitor" item="competitor_{{ $competitor->competitorid }}"></span>
							</div>
						</div>

						<div class="details-row visible-sm-block visible-xs-block">
							<div class="col-md-3 col-sm-3">
								<b class="visible-sm-block visible-xs-block">{{trans('messages.name')}}<br /></b>
								<span class = 'editable-field' data-name = 'competitor.competitor_name' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_name }}
								</span>
							</div>

							<div class="col-md-3 col-sm-3">
								<b class="visible-sm-block visible-xs-block">Address<br /></b>
								<span class = 'editable-field' data-name = 'competitor.competitor_address' data-field-id = '{{ $competitor->competitorid }}'>
								{{ $competitor->competitor_address }}
								</span>
							</div>

							<div class="col-md-3 col-sm-3">
								<b class="visible-sm-block visible-xs-block">{{trans('messages.contact_phone')}}<br /></b>
								<span class = 'editable-field' data-name = 'competitor.competitor_phone' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_phone }}
								</span>
							</div>

							<div class="col-md-3 col-sm-3">
								<b class="visible-sm-block visible-xs-block">{{trans('messages.contact_email')}}<br /></b>
								<span class = 'editable-field' data-name = 'competitor.competitor_email' data-field-id = '{{ $competitor->competitorid }}'>
									{{ $competitor->competitor_email }}
								</span>
								<span class="investigator-control" group_name="Competitors"  label="Name" value="{{ $competitor->competitor_name }}" group="competitor" item="competitor_{{ $competitor->competitorid }}"></span>
							</div>

							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								<div class="col-xs-12 visible-xs-block visible-sm-block">
									<a href = "{{ URL::To('/sme/competitor/'.$competitor->competitorid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-competitor' data-cntr = '#competitor-list-cntr'>{{ trans('messages.delete') }}</a>
								</div>
							@endif

						</div>
					@endforeach
				</div>
				@if ($entity->status == 0 or $entity->status == 3)
					<span class="details-row" style="text-align: right;">
						@if ($entity->loginid == Auth::user()->loginid)
							<a id = 'show-competitor-expand' href="{{ URL::to('/') }}/sme/competitor/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}}  {{trans('business_details2.competitors')}}</a>
						@endif
					</span>
				@endif

				<div id = 'expand-competitor-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif
@endif

@if (Auth::user()->role != 2)
	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->business_drivers)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('bizdriver-list-cntr', $tql->data)))
			<div id = 'bizdriver-list-cntr' class="spacewrapper">
				<div class="read-only reload-bizdriver">
					<h4>{{trans('business_details2.bdriver')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.bdriver_hint')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ $progress['businessdriver']==1 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->business_drivers)
							<span class="progress-required-counter" min-value="2"> {{ @count($bizdrivers) > 0 ? 2 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->business_drivers)
							<span class="progress-field-counter" min-value="2"> {{ @count($bizdrivers) > 0 ? 2 : 0 }} </span>
						@else

						@endif

						<div class="businessdriver">
							<div class="details-row hidden-xs hidden-sm">
								<div class="col-md-4 col-sm-4">
								  <b>{{ trans('business_details2.bdriver_name') }}</b>
								</div>

								<div class="col-md-8 col-sm-8">
								  <b>{{ trans('business_details2.bdriver_share') }}</b>
								</div>
							</div>

							@foreach ($bizdrivers as $driver)
								<div class="details-row hidden-xs hidden-sm">
									<div class="col-md-4 col-sm-4">
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											<a href = "{{ URL::To('/sme/businessdriver/'.$driver->businessdriverid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-bizdriver' data-cntr = '#bizdriver-list-cntr'>{{ trans('messages.delete') }}</a>
										@endif
										<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
											{{ $driver->businessdriver_name }}
										</span>
									</div>

									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
										{{ number_format($driver->businessdriver_total_sales, 2) }}%
										</span>
									</div>
								</div>

								<div class="details-row visible-sm-block visible-xs-block">
								<div class="col-md-4">
									<b>{{ trans('business_details2.bdriver_name') }}</b><br/>
									<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
									{{ $driver->businessdriver_name }}
									</span>
								</div>

								<div class="col-md-8">
									<b>{{ trans('business_details2.bdriver_share') }}</b><br />
									<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
									{{ $driver->businessdriver_total_sales }}%
									</span>
								</div>

								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									<div class="col-xs-12">'
										<a href = "{{ URL::To('/sme/businessdriver/'.$driver->businessdriverid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-bizdriver' data-cntr = '#bizdriver-list-cntr'>{{ trans('messages.delete') }}</a>
									</div>
								@endif

								</div>
							@endforeach
					</div>
					@if ($entity->status == 0 or $entity->status == 3)
						<span class="details-row" style="text-align: right;">
			 				@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-bizdriver-expand' href="{{ URL::to('/') }}/sme/businessdriver/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.bdriver')}}</a>
							@endif
						</span>
					@endif

					<div id = 'expand-bizdriver-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif
@endif

	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->customer_segments)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('bizsegment-list-cntr', $tql->data)))
			<div id = 'bizsegment-list-cntr' class="spacewrapper">
				<div class="read-only reload-bizsegment">
					<h4>{{trans('business_details2.customer_segment')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.cs_hint')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ $progress['businesssegment']==1 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->customer_segments)
							<span class="progress-required-counter" min-value="1"> {{ @count($bizsegments) > 0 ? 1 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->customer_segments)
							<span class="progress-field-counter" min-value="1"> {{ @count($bizsegments) > 0 ? 1 : 0 }} </span>
						@else

						@endif

						<div class="businesssegment">
							@foreach ($bizsegments as $bizsegment)
								<div class="details-row">
									<div class="col-md-12">
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											<a href = "{{ URL::To('/sme/businesssegment/'.$bizsegment->businesssegmentid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-bizsegment' data-cntr = '#bizsegment-list-cntr'>{{ trans('messages.delete') }}</a>
										@endif
										<span class = 'editable-field' data-name = 'bizsegment.businesssegment_name' data-field-id = '{{ $bizsegment->businesssegmentid }}'>
											{{ $bizsegment->businesssegment_name }}
										</span>
									</div>
								</div>
							@endforeach
						</div>

						@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-bizsegment-expand' href="{{ URL::to('/') }}/sme/businesssegment/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.customer_segment')}}</a>
							@endif
							</span>
						@endif

			   			<div id = 'expand-bizsegment-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif
@endif

@if ($entity->status == 6 and Auth::user()->role == 3 and $bank_standalone == 0)
<div class="spacewrapper">
	<div class="read-only">
		<h4 class="read-only-red">Rating for Business Drivers and Customer Segments</h4>
		<div class="read-only-text">
			<div class="col-md-12">{{ Form::select('bdmaindependency',
			  array('' => 'Choose here',
			  '36' => 'With full knowledge of business drivers and segments',
			  '12' => 'With little or no knowledge of business drivers and segments'
			  ), Input::old('bdmaindependency'), array('id' => 'bdmaindependency', 'class' => 'form-control rscorenumber')) }}
			</div>
		</div>
	</div>
</div>
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->past_projects)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('past-proj-list-cntr', $tql->data)))
		<div id = 'past-proj-list-cntr' class="spacewrapper">
			<div class="read-only reload-past-proj">
				@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->past_projects)
					<span class="progress-required-counter" min-value="1"> {{ @count($pastprojects) > 0 ? 1 : 0 }} </span>
				@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->past_projects)
					<span class="progress-field-counter" min-value="1"> {{ @count($pastprojects) > 0 ? 1 : 0 }} </span>
				@else

				@endif

			<h4>{{trans('business_details2.past_proj_completed')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.ppc_hint')}}">{{trans('messages.more_info')}}</a></h4>
			<div class="read-only-text">
				<div class="pastprojectscompleted">
					<div class="details-row hidden-sm hidden-xs">
						<div class="col-md-3 col-sm-4">
							<b>{{ trans('business_details2.ppc_purpose') }}</b>
						</div>

						<div class="col-md-2 col-sm-2">
							<b>{{ trans('business_details2.ppc_cost_short') }}</b>
						</div>

						<div class="col-md-3 col-sm-2">
							<b>{{ trans('business_details2.ppc_source_short') }}</b>
						</div>

						<div class="col-md-2 col-sm-1">
							<b>{{ trans('business_details2.ppc_year_short') }}</b>
						</div>

						<div class="col-md-2 col-sm-3">
							<b>{{ trans('business_details2.ppc_result_short') }}</b>
						</div>
					</div>

					@foreach ($pastprojects as $past_proj)
						<div class="details-row hidden-sm hidden-xs">
							<div class="col-md-3 col-sm-4">
								@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									<a href = "{{ URL::To('/sme/pastprojectscompleted/'.$past_proj->projectcompletedid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-past-proj' data-cntr = '#past-proj-list-cntr'>{{ trans('messages.delete') }}</a>
								@endif
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ $past_proj->projectcompleted_name }}
								</span>
							</div>

							<div class="col-md-2 col-sm-2">
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ number_format($past_proj->projectcompleted_cost, 2) }}
								</span>
							</div>
							<div class="col-md-3 col-sm-2">
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
								</span>
							</div>
							<div class="col-md-2 col-sm-1">
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ $past_proj->projectcompleted_year_began }}
								</span>
							</div>
							<div class="col-md-2 col-sm-3">
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
								</span>
								<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="{{ $past_proj->projectcompleted_name }}" group="past_projects" item="past_project_{{ $past_proj->projectcompletedid }}"></span>
							</div>
						</div>

						<div class="details-row visible-sm-block visible-xs-block">
							<div class="col-md-5">
								<b>{{ trans('business_details2.ppc_purpose') }}</b><br/>
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
									{{ $past_proj->projectcompleted_name }}
								</span>
							</div>

							<div class="col-md-1">
								<b>{{ trans('business_details2.ppc_cost') }}</b><br/>
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
								  {{ number_format($past_proj->projectcompleted_cost, 2) }}
								</span>
							</div>

							<div class="col-md-2">
								<b>{{ trans('business_details2.ppc_source') }}</b><br/>
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
								  {{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
								</span>
							</div>

							<div class="col-md-1">
								<b>{{ trans('business_details2.ppc_year') }}</b><br/>
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
								  {{ $past_proj->projectcompleted_year_began }}
								</span>
							</div>

							<div class="col-md-3">
								<b>{{ trans('business_details2.ppc_result') }}</b><br/>
								<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
								  {{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
								</span>
								<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="{{ $past_proj->projectcompleted_name }}" group="past_projects" item="past_project_{{ $past_proj->projectcompletedid }}"></span>
							</div>

							@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
								<div class="col-xs-12">
									<a href = "{{ URL::To('/sme/pastprojectscompleted/'.$past_proj->projectcompletedid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-past-proj' data-cntr = '#past-proj-list-cntr'>{{ trans('messages.delete') }}</a>
								</div>
							@endif

						</div>
					@endforeach
				</div>
				@if ($entity->status == 0 or $entity->status == 3)
					<div class="details-row" style="text-align: right;">
					@if ($entity->loginid == Auth::user()->loginid)
						<a id = 'show-past-proj-expand' href="{{ URL::to('/') }}/sme/pastprojectscompleted/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.past_proj_completed')}}</a>
					@endif
					</div>
				@endif

				<div id = 'expand-past-proj-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif
@endif

	@if (Auth::user()->role != 2)
		@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->future_growth)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('future-growth-list-cntr', $tql->data)))
				<div id = 'future-growth-list-cntr' class="spacewrapper">
					<div class="read-only reload-future-growth">
						<h4>{{trans('business_details2.future_growth_initiatives')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.fgi_hint')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							@if(Auth::user()->role == 2 || Auth::user()->role == 3)
								<div class="completeness_check">{{ $progress['futuregrowthinitiative']==1 ? "true" : "" }}</div>
							@endif

							@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->future_growth)
								<span class="progress-required-counter" min-value="4"> {{ @count($futuregrowths) > 0 ? 4 : 0 }} </span>
							@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->future_growth)
								<span class="progress-field-counter" min-value="4"> {{ @count($futuregrowths) > 0 ? 4 : 0 }} </span>
							@else

							@endif

							<div class="futuregrowthinitiative">
								<div class="details-row hidden-sm hidden-xs">
									<div class="col-md-3 col-sm-3"><b>{{ trans('business_details2.fgi_purpose') }}</b></div>
									<div class="col-md-2 col-sm-2"><b>{{ trans('business_details2.fgi_cost') }}</b></div>
									<div class="col-md-2 col-sm-2"><b>{{ trans('business_details2.fgi_date') }}</b></div>
									<div class="col-md-2 col-sm-2"><b>{{ trans('business_details2.fgi_source') }}</b></div>
									<div class="col-md-3 col-sm-3"><b>{{ trans('business_details2.fgi_proj_goal') }}</b></div>
								</div>

								@foreach ($futuregrowths as $future_init)
									<div class="details-row hidden-sm hidden-xs">
										<div class="col-md-3 col-sm-3">
											@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
												<a href = "{{ URL::To('/sme/futuregrowthinitiative/'.$future_init->futuregrowthid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-future-growth' data-cntr = '#future-growth-list-cntr'>{{ trans('messages.delete') }}</a>
											@endif
											<span class = 'editable-field' data-name = 'fgi.futuregrowth_name' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ $future_init->futuregrowth_name }}
											</span>
										</div>

										<div class="col-md-2 col-sm-2">
											<span class = 'editable-field' data-name = 'fgi.futuregrowth_estimated_cost' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
											</span>
										</div>
										<div class="col-md-2 col-sm-2">
											<span class = 'editable-field date-input' data-name = 'fgi.futuregrowth_implementation_date' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ date("F d, Y", strtotime($future_init->futuregrowth_implementation_date)) }}
											</span>
										</div>
										<div class="col-md-2 col-sm-2">
											<span class = 'editable-field' data-name = 'fgi.futuregrowth_source_capital' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ trans('business_details2.'.$future_init->futuregrowth_source_capital) }}
											</span>
										</div>
										<div class="col-md-3 col-sm-3">
											<span class = 'editable-field' data-name = 'fgi.planned_proj_goal' data-field-id = '{{ $future_init->futuregrowthid }}'>
												{{ trans('business_details2.'.$future_init->planned_proj_goal) }}
											</span>
											by
											<span class = 'editable-field' data-name = 'fgi.planned_goal_increase' data-field-id = '{{ $future_init->futuregrowthid }}'>
												{{ $future_init->planned_goal_increase }}%
											</span>
											beginning
											<span class = 'editable-field' data-name = 'fgi.proj_benefit_date' data-field-id = '{{ $future_init->futuregrowthid }}'>
												{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
											</span>
											<style id = 'dp-style'></style>
										</div>
									</div>

									<div class="details-row visible-sm-block visible-xs-block">
										<div class="col-md-5">
											<b>{{ trans('business_details2.fgi_purpose') }}</b><br/>
											{{ $future_init->futuregrowth_name }}
										</div>

										<div class="col-md-2">
											<b>{{ trans('business_details2.fgi_cost') }}</b><br/>
											{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
										</div>

										<div class="col-md-2">
											<b>{{ trans('business_details2.fgi_date') }}</b><br/>
											{{ date("F d, Y", strtotime($future_init->futuregrowth_implementation_date)) }}
										</div>

										<div class="col-md-3">
											<b>{{ trans('business_details2.fgi_source') }}</b><br/>
											{{ trans('business_details2.'.$future_init->futuregrowth_source_capital) }}
										</div>

										<div class="col-md-3 col-sm-3">
											{{ trans('business_details2.'.$future_init->planned_proj_goal) }} by
											{{ $future_init->planned_goal_increase }}% beginning
											{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
										</div>

										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											<div class="col-xs-12">
												<a href = "{{ URL::To('/sme/futuregrowthinitiative/'.$future_init->futuregrowthid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-future-growth' data-cntr = '#future-growth-list-cntr'>{{ trans('messages.delete') }}</a>
											</div>
										@endif

									</div>
								@endforeach
							</div>
							@if ($entity->status == 0 or $entity->status == 3)
								<div class="details-row" style="text-align: right;">
									@if ($entity->loginid == Auth::user()->loginid)
										<a id = 'show-future-growth-expand' href="{{ URL::to('/') }}/sme/futuregrowthinitiative/{{ $entity->entityid }}" class="navbar-link">{{trans('messages.add')}} {{trans('business_details2.future_growth_initiatives')}}</a>
									@endif
								</div>
							@endif

							<div id = 'expand-future-growth-form' class = 'spacewrapper'></div>
						</div>
					</div>
				</div>
			@endif
		@endif
	@endif

	@if ($entity->status == 6 and Auth::user()->role == 3 and $bank_standalone == 0)
		<div class="spacewrapper">
			<div class="read-only">
				<h4 class="read-only-red">Rating for Past Projects Completed and Future Growth Initiatives</h4>
				<div class="read-only-text">
					<div class="col-md-12">{{ Form::select('ppfuture',
					 array('' => 'Choose here',
					 '36' => 'Evidence of coherent growth strategy',
					 '12' => 'Lack of growth strategy'
					 ), Input::old('ppfuture'), array('id' => 'ppfuture', 'class' => 'form-control rscorenumber')) }}
					</div>
				</div>
			</div>
		</div>
	@endif

	<p style="font-size: 11px;margin-top:15px;margin-left:10px;" class = 'print-none'>{{trans('messages.disclaimer')}}</p>
	@if(!Session::has('tql'))
		<input type="button" id="nav_registration2_prev" value="{{trans('messages.previous')}}" />

		<input type="button" id="nav_registration2_next" value="{{trans('messages.next')}}" />

	@endif

	</div>
@endif

@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'assocFiles'))
	<div id="registration3" class="tab-pane fade">

		<div id="dropbox-upload-cntr" class="spacewrapper print-none">
			<div class="read-only">
				<h4>{{ trans('messages.files_list') }}</h4>
				<div class="read-only-text">

					<div id='dropbox-upload-list-cntr' class="details-row">
						<div class='reload-dropbox-upload'>
							<div class="col-md-12 text-center alert-success copied hidden">
								<span class="alert-success">{{ trans('messages.link_copied') }}</span>
							</div>
							@if ($folderUrl == '')
								<div class="col-md-12 text-center alert-warning">
								  <span class="alert-warning">{{ trans('messages.no_assoc_files') }}</span>
								</div>
							@else
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th scope="col" class="col-md-1 text-center">#</th>
											<th scope="col" class="col-md-7">{{ trans('messages.file_name') }}</th>
											<th scope="col" class="col-md-4 text-center">{{ trans('messages.action') }}</th>
										</tr>
									</thead>
									<tbody>
								  
								@foreach($entityFiles as $key => $filename)
									<tr>
										<td scope="row" class="text-center">{{ $key+1 }}</td>

										<td><a target="_blank">{{ $filename->file_name }}</a></td>

										<td class="text-center">
											<a title="{{ trans('messages.download')}}" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ url('sme/download?directory=associated_files/'.$entity->entityid.'&filename='.$filename->file_name) }}"><span class="glyphicon glyphicon-download-alt"></span></a>
										@if ($entity->status == 0 or $entity->status == 3)
											<a href="{{ URL::to('/') }}/sme/delete_assocfile/{{ $filename->id }}"  title="{{ trans('messages.delete')}}" class="navbar-link delete-file btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-dropbox-upload' data-cntr = '#dropbox-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
										@endif
										</td>
									</tr>
								@endforeach
								  </tbody>
								</table>
							@endif

							<div id = 'expand-dropbox-upload-form' class = 'spacewrapper'></div>

							@if ($entity->status == 0 or $entity->status == 3)
								<span class="details-row" style="text-align: right;">
									@if ($entity->loginid == Auth::user()->loginid)
										<a id = 'show-dropbox-upload-expand' href="{{ URL::to('/') }}/sme/dropbox_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.upload_file') }}</a>
									@endif
								</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="modal fade centered-modal" id="shareLinkModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">{{ trans('messages.share_link') }}</h4>
				</div>
				<div class="modal-body">
					<div>
						<a href="" target="_blank" id="shareLink" value=""></a>
					</div>
					<div class="text-center">
						<button type="button" class="btn btn-success" data-dismiss="modal" id="copyButton">{{ trans('messages.copy_link') }}</button>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('messages.close') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div id="ubill-upload-cntr" class="spacewrapper print-none hidden">
		<div class="read-only">
			<h4>Utility Bills</h4>
			<div class="read-only-text">
				<div id='ubill-upload-list-cntr' class="details-row">
			        <div class='reload-ubill-upload'>
						@if (count($ubillFiles) == 0)
							<div class="col-md-12 text-center alert-warning">
								<span class="alert-warning">No Utility Bill</span>
							</div>
						@else
							<table class="table table-hover table-bordered">
							    <thead>
							        <tr>
										<th scope="col" class="col-md-1 text-center">#</th>
										<th scope="col" class="col-md-7 ">File Name</th>
										<th scope="col" class="col-md-4 text-center">Action</th>
							        </tr>
							    </thead>
							    <tbody>
							    
							  @foreach($ubillFiles as $key => $filename)
								<tr>
									<th scope="row" class="text-center">{{ $key+1 }}</th>

									<td><a href="{{ URL::to('/') }}/sme/download_ubill/{{ $filename->entity_id }}/{{ $filename->utility_bill_file }}" target="_blank">{{ $filename->utility_bill_file }}</a></td>

									<td class="text-center">
										<a title="Download" data-toggle="tooltip" data-placement="top" class="btn btn-success btn-sm" href="{{ URL::to('/') }}/sme/download_ubill/{{ $filename->entity_id }}/{{ $filename->utility_bill_file }}"><span class="glyphicon glyphicon-download-alt"></span></a>
										@if ($entity->status == 0 or $entity->status == 3)
											<a href="{{ URL::to('/') }}/sme/delete_ubill/{{ $filename->id }}"  title="Delete" class="navbar-link delete-ubill btn btn-danger btn-sm" data-toggle="tooltip" data-reload = '.reload-ubill-upload' data-cntr = '#ubill-upload-list-cntr'><span class="glyphicon glyphicon-trash"></span></a>
										@endif
									</td>
								</tr>
							  @endforeach
							    </tbody>
							</table>
						@endif

						<div id = 'expand-ubill-upload-form' class = 'spacewrapper'></div>

							@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								@if (count($ubillFiles) == 0)
									<a id = 'show-ubill-upload-expand' href="{{ URL::to('/') }}/sme/ubill_upload_files/{{ $entity->entityid }}" class="navbar-link">{{ 'Upload Utility Bill' }}</a>
								@endif
								@endif
							</span>
							@endif
						</div>
						</div>
					</div>
				</div>
			</div>

		<input type="button" class = 'reload-ecf' id="nav_assocfiles_prev" value="{{trans('messages.previous')}}" />
		<input type="button" class = 'reload-ecf' id="nav_assocfiles_next" value="{{trans('messages.next')}}" />

	</div>
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->ecf)
	@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'payfactor'))
		<div id="payfactor" class="tab-pane fade">
			@if ($entity->status == 0 or $entity->status == 3)
				<div class="spacewrapper reload-ecf">
					<span class="details-row" style="border: 0px;">
						@if ($entity->loginid == Auth::user()->loginid)
							<a href="{{ URL::to('/') }}/sme/planfacility/{{ $entity->entityid }}" class="navbar-link pop-ecf-form add-link-text">{{trans('messages.add')}} {{trans('ecf.ecf')}}</a>
						@endif
					</span>
				</div>
			@endif
			<div class="spacewrapper reload-ecf">
				<div class="read-only">
					@if(count($planfacility)>0)
						<h4>{{trans('ecf.ecf')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('ecf.ecf_hint')}}">{{trans('messages.more_info')}}</a></h4>
					@endif
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
						<div class="completeness_check">{{ @count($planfacility) > 0 ? "true" : "" }}</div>
					@endif

					@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->ecf)
						<span class="progress-required-counter" min-value="1"> {{ @count($planfacility) > 0 ? 1 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->ecf)
						<span class="progress-field-counter" min-value="14"> {{ @count($planfacility) > 0 ? 14 : 0 }} </span>
					@else

					@endif

					@foreach ($planfacility as $planfacilityinfo)
						<div class="read-only-text">
							 <p><b>{{trans('ecf.credit_facilities')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('ecf.ecf_hint')}}">{{trans('messages.more_info')}}</a></p>
							 <span class="details-row">
								<div class="col-md-4 col-sm-4">{{trans('ecf.lending_institution')}}</div>
								<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.lending_institution' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ $planfacilityinfo->lending_institution }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.lending_institution',[],'en')}}" value="{{ $planfacilityinfo->lending_institution }}" group="plan_facility" item="lending_institution_{{ $planfacilityinfo->planfacilityid }}"></span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4">{{trans('ecf.amount_of_line')}}</div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'ecf.plan_credit_availment' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
										{{ number_format($planfacilityinfo->plan_credit_availment, 2) }}
									</span>
									<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.amount_of_line',[],'en')}}" value="{{ $planfacilityinfo->plan_credit_availment }}" group="plan_facility" item="plan_credit_availment_{{ $planfacilityinfo->planfacilityid }}"></span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4">{{trans('ecf.purpose_of_cf')}}</div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'ecf.purpose_credit_facility' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
										@if ($planfacilityinfo->purpose_credit_facility == 1)
											{{trans('ecf.working_capital')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 2)
											{{trans('ecf.expansion')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 3)
											{{trans('ecf.takeout')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 5)
											{{trans('ecf.supplier_credit')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 6)
											{{trans('ecf.equipment_purchase')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 7)
											{{trans('ecf.guarantee')}}
										@else
											{{trans('ecf.other_specify')}}
										@endif
									</span>
									<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.purpose_of_cf',[],'en')}}" value="{{ ($planfacilityinfo->purpose_credit_facility == 1) ? trans('ecf.working_capital',[],'en') : (($planfacilityinfo->purpose_credit_facility == 2) ? trans('ecf.expansion',[],'en') : (($planfacilityinfo->purpose_credit_facility == 3) ? trans('ecf.takeout',[],'en') : trans('ecf.other_specify',[],'en'))) }}" group="plan_facility" item="purpose_credit_facility_{{ $planfacilityinfo->planfacilityid }}"></span>
								</div>
							</span>

							@if (4 == $planfacilityinfo->purpose_credit_facility)
								<span id = 'ecf-other-purpose-{{ $planfacilityinfo->planfacilityid }}' class="details-row">
							@else
								<span id = 'ecf-other-purpose-{{ $planfacilityinfo->planfacilityid }}' class="details-row" style = 'display: none;'>
							@endif
							<div class="col-md-4 col-sm-4">{{trans('ecf.purpose_of_cf_others')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.purpose_credit_facility_others' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								{{ $planfacilityinfo->purpose_credit_facility_others }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.purpose_of_cf_others',[],'en')}}" value="{{ $planfacilityinfo->purpose_credit_facility_others }}" group="plan_facility" item="purpose_credit_facility_others_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						@if (5 == $planfacilityinfo->purpose_credit_facility)
							<span id = 'ecf-credit-terms-{{ $planfacilityinfo->planfacilityid }}' class="details-row">
						@else
							<span id = 'ecf-credit-terms-{{ $planfacilityinfo->planfacilityid }}' class="details-row" style = 'display: none;'>
						@endif
							<div class="col-md-4 col-sm-4">{{trans('ecf.credit_terms')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.credit_terms' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								  {{ $planfacilityinfo->credit_terms }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.credit_terms',[],'en')}}" value="{{ $planfacilityinfo->credit_terms }}" group="plan_facility" item="credit_terms{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.outstanding_balance')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.outstanding_balance' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								@if ('' != $planfacilityinfo->outstanding_balance)
									{{ number_format($planfacilityinfo->outstanding_balance, 2) }}
								@else

								@endif
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.outstanding_balance',[],'en')}}" value="{{ $planfacilityinfo->outstanding_balance }}" group="plan_facility" item="outstanding_balance{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{ trans('ecf.experience') }}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.experience' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								  @if ($planfacilityinfo->experience == 1)
								  {{ trans('messages.poor') }}
								  @elseif ($planfacilityinfo->experience == 2)
								  {{ trans('messages.fair') }}
								  @elseif ($planfacilityinfo->experience == 3)
								  {{ trans('messages.prompt') }}
								  @else

								  @endif
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.experience',[],'en')}}" value="{{ $planfacilityinfo->experience }}" group="plan_facility" item="experience{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{ trans('ecf.other_remarks') }}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field empty-remarks' data-name = 'ecf.other_remarks' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								@if (STR_EMPTY != $planfacilityinfo->other_remarks)
									{{ nl2br($planfacilityinfo->other_remarks) }}
								@else

								@endif
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.other_remarks',[],'en')}}" value="{{ $planfacilityinfo->other_remarks }}" group="plan_facility" item="other_remarks{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						<p><b>{{trans('ecf.existing_collateral')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('ecf.existing_collateral_hint')}}">{{trans('messages.more_info')}}</a></p>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.type_of_deposit')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.cash_deposit_details' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ $planfacilityinfo->cash_deposit_details }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.type_of_deposit',[],'en')}}" value="{{ $planfacilityinfo->cash_deposit_details }}" group="plan_facility" item="cash_deposit_details_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.deposit_amount')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.cash_deposit_amount' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ number_format($planfacilityinfo->cash_deposit_amount, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.deposit_amount',[],'en')}}" value="{{ $planfacilityinfo->cash_deposit_amount }}" group="plan_facility" item="cash_deposit_amount_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.marketable_securities')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.securities_details' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ $planfacilityinfo->securities_details }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.marketable_securities',[],'en')}}" value="{{ $planfacilityinfo->securities_details }}" group="plan_facility" item="securities_details_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.marketable_securities_value')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.securities_estimated_value' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ number_format($planfacilityinfo->securities_estimated_value, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.marketable_securities_value',[],'en')}}" value="{{ $planfacilityinfo->securities_estimated_value }}" group="plan_facility" item="securities_estimated_value_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>

						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.property')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.property_details' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ $planfacilityinfo->property_details }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.property',[],'en')}}" value="{{ $planfacilityinfo->property_details }}" group="plan_facility" item="property_details_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.property_value')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.property_estimated_value' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ number_format($planfacilityinfo->property_estimated_value, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.property_value',[],'en')}}" value="{{ $planfacilityinfo->property_estimated_value }}" group="plan_facility" item="property_estimated_value_{{ $planfacilityinfo->planfacilityid }}"></span>
						</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.chattel')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.chattel_details' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
									{{ $planfacilityinfo->chattel_details }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.chattel',[],'en')}}" value="{{ $planfacilityinfo->chattel_details }}" group="plan_facility" item="chattel_details_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.chattel_value')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.chattel_estimated_value' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								{{ number_format($planfacilityinfo->chattel_estimated_value, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.chattel_value',[],'en')}}" value="{{ $planfacilityinfo->chattel_estimated_value }}" group="plan_facility" item="chattel_estimated_value_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4">{{trans('ecf.others')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.others_details' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								{{ $planfacilityinfo->others_details }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.others',[],'en')}}" value="{{ $planfacilityinfo->others_details }}" group="plan_facility" item="others_details_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						<span class="details-row">
						<div class="col-md-4 col-sm-4">{{trans('ecf.others_value')}}</div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'ecf.others_estimated_value' data-field-id = '{{ $planfacilityinfo->planfacilityid }}'>
								{{ number_format($planfacilityinfo->others_estimated_value, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('ecf.ecf',[],'en')}}" label="{{trans('ecf.others_value',[],'en')}}" value="{{ $planfacilityinfo->others_estimated_value }}" group="plan_facility" item="others_estimated_value_{{ $planfacilityinfo->planfacilityid }}"></span>
							</div>
						</span>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right; border: 0px;">
						@if ($entity->loginid == Auth::user()->loginid )
							<a href="{{ URL::to('/') }}/sme/planfacility/{{ $planfacilityinfo->planfacilityid }}/delete" class="navbar-link delete-info" data-reload = '.reload-ecf' data-cntr = '#payfactor'>{{trans('messages.delete')}}</a>
						@endif
					</span>
				@endif
			</div>
		@endforeach
	</div>
</div>

	@if (Auth::user()->role == 2)
		<div class="spacewrapper">
			<div class="read-only">
				<h4>Adverse Records</h4>
				<div class="read-only-text">
					<span class="adverserecord"></span>
					<span class="details-row" style="text-align: right;">
						<a href="{{ URL::to('/') }}/sme/adverserecord/{{$entity->entityid}}" class="navbar-link">Add Adverse Record</a>
					</span>
				</div>
				</div>
			</div>
			@endif

			<p style="font-size: 11px;margin-top:15px;margin-left:10px;" class = 'print-none reload-ecf'>{{trans('messages.disclaimer')}}</p>
			<input type="button" class = 'reload-ecf' id="nav_payfactor_prev" value="{{trans('messages.previous')}}" />
			<input type="button" class = 'reload-ecf' id="nav_payfactor_next" value="{{trans('messages.next')}}" />

		</div>
	@endif
@endif

@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'bcs'))
	<div id="bcs" class="tab-pane fade">
		@if($bank_standalone == 0)
			@if(!Session::has('tql') || (Session::has('tql') && in_array('succession-list-cntr', $tql->data)))
				<div id = 'succession-list-cntr' class="spacewrapper">
					<div class="read-only reload-succession">
						<h4>{{trans('condition_sustainability.succession')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('condition_sustainability.succession_explain')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ ($sustainability != null && $sustainability->succession_plan != null) ? "true" : "" }}</div>
						@endif
						<span class="progress-required-counter" min-value="2">{{ ($sustainability != null && $sustainability->succession_plan != null) ? 2 : 0}}</span>
						@if ($sustainability != null && $sustainability->succession_plan != null)
						{{ Form::hidden('successplan', $sustainability->succession_plan, array('id' => 'successplan')) }}

						<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.plan') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'bcs.succession_plan' data-field-id = '{{ $sustainability->sustainabilityid }}'>
								@if ($sustainability->succession_plan == 1)
									{{ trans('condition_sustainability.mngmt_relative') }}
								@elseif ($sustainability->succession_plan == 2)
									{{ trans('condition_sustainability.trusted_employee') }}
								@else
									{{ trans('condition_sustainability.none_less_five') }}
								@endif
								</span>
								<span class="investigator-control" group_name="{{trans('condition_sustainability.succession',[],'en')}}" label="{{trans('condition_sustainability.plan',[],'en')}}" value="{{ ($sustainability->succession_plan == 1) ? trans('condition_sustainability.mngmt_relative',[],'en') : (($sustainability->succession_plan == 2) ? trans('condition_sustainability.trusted_employee',[],'en') : trans('condition_sustainability.none_less_five',[],'en')) }}" group="succession" item="succession_plan"></span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.time_frame') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'bcs.succession_timeframe' data-field-id = '{{ $sustainability->sustainabilityid }}'>
									@if ($sustainability->succession_timeframe == 1)
									{{ trans('condition_sustainability.succession_within_five') }}
									@elseif ($sustainability->succession_timeframe == 2)
									{{ trans('condition_sustainability.succession_over_five') }}
									@else
									{{ trans('condition_sustainability.succession_plan_none') }}
									@endif
								</span>
								<span class="investigator-control" group_name="{{trans('condition_sustainability.succession',[],'en')}}" label="{{trans('condition_sustainability.succession_plan_none',[],'en')}}" value="{{($sustainability->succession_timeframe == 1) ? trans('condition_sustainability.succession_within_five',[],'en') : trans('condition_sustainability.succession_over_five',[],'en')}}" group="succession" item="succession_timeframe"></span>
							</div>
						</span>
						@else
							<span class="details-row" style="text-align: right; border: 0px;">
							@if (($entity->status == 0 or $entity->status == 3) && $entity->loginid == Auth::user()->loginid)
								<a id = 'show-succession-expand' href="{{ URL::to('/') }}/sme/busconandsus/succession/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{ trans('condition_sustainability.succession') }}</a>
							@endif
							</span>
						@endif

						<div id = 'expand-succession-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif

@if (Auth::user()->role != 2 AND Auth::user()->role != 4)
	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->landscape)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('competitionlandscape-list-cntr', $tql->data)))
			<div id = 'competitionlandscape-list-cntr' class="spacewrapper">
				<div class="read-only reload-competitionlandscape">
					<h4>{{trans('condition_sustainability.competitive_landscape')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('condition_sustainability.identify_competition')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ ($sustainability != null && $sustainability->competition_landscape != null) ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->landscape)
							<span class="progress-required-counter" min-value="2"> {{ ($sustainability != null && $sustainability->competition_landscape != null) ? 2 : 0 }} </span>
							@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->landscape)
							<span class="progress-field-counter" min-value="2"> {{ ($sustainability != null && $sustainability->competition_landscape != null) ? 2 : 0 }} </span>
							@else

							@endif

							@if ($sustainability != null && $sustainability->competition_landscape != null)
							<span class="details-row">
							 <div class="col-md-4 col-sm-4"><b>Landscape</b></div>
							 <div class="col-md-8 col-sm-8">
							  <span class = 'editable-field' data-name = 'bcs.competition_landscape' data-field-id = '{{ $sustainability->sustainabilityid }}'>
							    @if ($sustainability->competition_landscape == 1)
							    {{ trans('condition_sustainability.market_pioneer') }}
							    @elseif ($sustainability->competition_landscape == 2)
							    {{ trans('condition_sustainability.limited_market_entry') }}
							    @else
							    {{ trans('condition_sustainability.high_market_penetration') }}
							    @endif
							  </span>
							</div>
							</span>
						@else
							<span class="details-row" style="text-align: right; border: 0px;">
							@if (($entity->status == 0 or $entity->status == 3) && $entity->loginid == Auth::user()->loginid)
								<a id = 'show-landscape-expand' href="{{ URL::to('/') }}/sme/busconandsus/competitionlandscape/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }}  {{ trans('condition_sustainability.landscape') }}</a>
							@endif
							</span>
						@endif

						<div id = 'expand-landscape-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->economic_factors)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('economicfactors-list-cntr', $tql->data)))
		<div id = 'economicfactors-list-cntr' class="spacewrapper">
			<div class="read-only reload-economicfactors">
				<h4>{{ trans('condition_sustainability.economic_factor') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('condition_sustainability.rate_eco_factor')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ ($sustainability != null && $sustainability->ev_susceptibility_economic_recession != null) ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->economic_factors)
							<span class="progress-required-counter" min-value="4"> {{ ($sustainability != null && $sustainability->ev_susceptibility_economic_recession != null) ? 4 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->economic_factors)
							<span class="progress-field-counter" min-value="4"> {{ ($sustainability != null && $sustainability->ev_susceptibility_economic_recession != null) ? 4 : 0 }} </span>
						@else

						@endif

						@if ($sustainability != null && $sustainability->ev_susceptibility_economic_recession != null)
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.recession_susceptibility') }}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'bcs.ev_susceptibility_economic_recession' data-field-id = '{{ $sustainability->sustainabilityid }}'>
										@if ($sustainability->ev_susceptibility_economic_recession == 1)
											{{ trans('messages.low') }}
										@elseif ($sustainability->ev_susceptibility_economic_recession == 2)
											{{ trans('messages.medium') }}
										@else
											{{ trans('messages.high') }}
										@endif
									</span>
								</div>
							</span>
							<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.forex_sensitivity') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'bcs.ev_foreign_exchange_interest_sensitivity' data-field-id = '{{ $sustainability->sustainabilityid }}'>
									@if ($sustainability->ev_foreign_exchange_interest_sensitivity == 1)
										{{ trans('messages.low') }}
									@elseif ($sustainability->ev_foreign_exchange_interest_sensitivity == 2)
										{{ trans('messages.medium') }}
									@else
										{{ trans('messages.high') }}
									@endif
								</span>
							</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.price_volatility') }}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'bcs.ev_commodity_price_volatility' data-field-id = '{{ $sustainability->sustainabilityid }}'>
										@if ($sustainability->ev_commodity_price_volatility == 1)
											{{ trans('messages.low') }}
										@elseif ($sustainability->ev_commodity_price_volatility == 2)
											{{ trans('messages.medium') }}
										@else
											{{ trans('messages.high') }}
										@endif
									</span>
								</div>
							</span>
							<span class="details-row">
								<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.government_regulation') }}</b></div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'bcs.ev_governement_regulation' data-field-id = '{{ $sustainability->sustainabilityid }}'>
										@if ($sustainability->ev_governement_regulation == 1)
											{{ trans('messages.low') }}
										@elseif ($sustainability->ev_governement_regulation == 2)
											{{ trans('messages.medium') }}
										@else
											{{ trans('messages.high') }}
										@endif
									</span>
								</div>
							</span>
							@else
							<span class="details-row" style="text-align: right; border: 0px;">
								@if (($entity->status == 0 or $entity->status == 3) && $entity->loginid == Auth::user()->loginid)
									<a id = 'show-eco-factors-expand' href="{{ URL::to('/') }}/sme/busconandsus/economicfactors/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{ trans('condition_sustainability.economic_factor') }}</a>
								@endif
							</span>
						@endif

						<div id = 'expand-eco-factor-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->tax_payments)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('taxpayments-list-cntr', $tql->data)))
		<div id = 'taxpayments-list-cntr' class="spacewrapper">
			<div class = "read-only reload-taxpayments">
				<h4>{{ trans('condition_sustainability.tax_payments') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ trans('condition_sustainability.taxes_ontime') }}<br/>
				 {{ trans('condition_sustainability.taxes_delayed') }}<br/>
				 {{ trans('condition_sustainability.taxes_deliquent') }}">{{trans('messages.more_info')}}</a></h4>
				<div class="read-only-text">
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
					<div class="completeness_check">{{ ($sustainability != null && $sustainability->tax_payments != null) ? "true" : "" }}</div>
					@endif

					@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->tax_payments)
					<span class="progress-required-counter" min-value="1"> {{ ($sustainability != null && $sustainability->tax_payments != null) ? 1 : 0 }} </span>
					@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->tax_payments)
					<span class="progress-field-counter" min-value="1"> {{ ($sustainability != null && $sustainability->tax_payments != null) ? 1 : 0 }} </span>
					@else

					@endif

					@if ($sustainability != null && $sustainability->tax_payments != null)
						<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.tax_payments') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'bcs.tax_payments' data-field-id = '{{ $sustainability->sustainabilityid }}'>
									@if ($sustainability->tax_payments == 1)
										{{ trans('condition_sustainability.taxes_current') }}
									@elseif ($sustainability->tax_payments == 2)
										{{ trans('condition_sustainability.taxes_arrears') }}
									@else
										{{ trans('condition_sustainability.taxes_bad') }}
									@endif
								</span>
								<span class="investigator-control" group_name="{{trans('condition_sustainability.tax_payments',[],'en')}}" label="{{trans('condition_sustainability.tax_payments',[],'en')}}" value="{{($sustainability->tax_payments == 1) ? trans('condition_sustainability.taxes_current',[],'en') : (($sustainability->tax_payments == 2) ? trans('condition_sustainability.taxes_arrears',[],'en') : trans('condition_sustainability.taxes_bad',[],'en'))}}" group="tax_payments" item="tax_payments"></span>

							</div>
						</span>
					@else
					<span class="details-row" style="text-align: right; border: 0px;">
					@if (($entity->status == 0 or $entity->status == 3) && $entity->loginid == Auth::user()->loginid)
						<a id = 'show-tax-payments-expand' href="{{ URL::to('/') }}/sme/busconandsus/taxpayments/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{ trans('condition_sustainability.tax_payments') }}</a>
					@endif
						</span>
					@endif

					<div id = 'expand-bcs-tax-payments-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif
@endif

@if($bank_standalone == 0)
	@if (Auth::user()->role != 2 AND Auth::user()->role != 4)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('ra-list-cntr', $tql->data)))
			<div id = 'ra-list-cntr' class="spacewrapper">
				<div class="read-only reload-ra">
					<h4>{{ trans('condition_sustainability.risk_assessment') }} <a href="#" class="popup-hint popup-hint-header" data-hint="

					{{trans('condition_sustainability.risk_assessment')}}
					<br /><br />
					- {{trans('condition_sustainability.major_risks')}}<br />
					- {{trans('condition_sustainability.buying_power')}}<br />
					- {{trans('condition_sustainability.supplying_power')}}<br />
					- {{trans('condition_sustainability.competetive_rivalry')}}<br />
					- {{trans('condition_sustainability.substitution_threat')}}<br />

					<hr/>">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ $progress['riskassessment']==1 ? "true" : "" }}</div>
						@endif
						<span class="progress-required-counter" min-value="2">{{ ($progress['riskassessment'] * 2) }}</span>
						<div class="riskassessment">
						<input type="hidden" class="ra-class" value="{{ @count($riskassessment) }}">
						<?php $risk_num = 1; ?>
						@foreach ($riskassessment as $risk)
							<div class="details-row">

								<div class="col-md-6 col-sm-6">
									@if (($entity->loginid == Auth::user()->loginid) AND ($entity->status == 0 or $entity->status == 3))
										<a href = "{{ URL:: To('/sme/riskassessment/'.$risk->riskassessmentid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-ra' data-cntr = '#ra-list-cntr'> {{ trans('messages.delete') }}</a>
									@endif
									<b>{{ trans('condition_sustainability.threat') }}</b> #{{ $risk_num }} &nbsp;
									<span class = 'editable-field' data-name = 'risk.risk_assessment_name' data-field-id = '{{ $risk->riskassessmentid }}'>
										{{ trans('condition_sustainability.'.$risk->risk_assessment_name) }}
									</span>
									</div>

									<div class="col-md-6 col-sm-6">
									<b> {{ trans('condition_sustainability.counter_measures') }} </b> #{{ $risk_num }} &nbsp;
									<span class = 'editable-field' data-name = 'risk.risk_assessment_solution' data-field-id = '{{ $risk->riskassessmentid }}'>
										{{ trans('condition_sustainability.'.$risk->risk_assessment_solution) }}
									</span>
								</div>

							</div>
							<?php $risk_num++; ?>
						@endforeach
						</div>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								@if (3 > @count($riskassessment))
									<a id = 'show-risk-mng-expand' href="{{ URL::to('/') }}/sme/riskassessment/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{ trans('condition_sustainability.risk_assessment') }}</a>
								@endif
							@endif
							</span>
						@endif

						<div id = 'expand-risk-mng-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif
@endif

@if (Auth::user()->role != 2)
	@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->growth_potential)
		@if(!Session::has('tql') || (Session::has('tql') && in_array('rev-growth-list-cntr', $tql->data)))
			<div id = 'rev-growth-list-cntr' class="spacewrapper">
				<div class="read-only reload-rev-growth">
					<h4>{{ trans('condition_sustainability.revenue_growth') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('condition_sustainability.expected_revenue_3years')}}">{{trans('messages.more_info')}}</a></h4>
					<div class="read-only-text">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ @count($revenuepotential)>0 ? "true" : "" }}</div>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->growth_potential)
							<span class="progress-required-counter" min-value="2"> {{ @count($revenuepotential) > 0 ? 2 : 0 }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->growth_potential)
							<span class="progress-field-counter" min-value="2"> {{ @count($revenuepotential) > 0 ? 2 : 0 }} </span>
						@else

						@endif

						@foreach ($revenuepotential as $revenuepotentialinfo)
						<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.under_current_market') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'rev_growth.current_market' data-field-id = '{{ $revenuepotentialinfo->growthpotentialid }}'>
									@if ($revenuepotentialinfo->current_market == 1)
										0%
									@elseif ($revenuepotentialinfo->current_market == 2)
										{{ trans('condition_sustainability.less_3_percent') }}
									@elseif ($revenuepotentialinfo->current_market == 3)
										{{ trans('condition_sustainability.between_3to5_percent') }}
									@else
										{{ trans('condition_sustainability.greater_5_percent') }}
									@endif
								</span>
							</div>
						</span>
						<span class="details-row">
							<div class="col-md-4 col-sm-4"><b>{{ trans('condition_sustainability.expected_future_market') }}</b></div>
							<div class="col-md-8 col-sm-8">
								<span class = 'editable-field' data-name = 'rev_growth.new_market' data-field-id = '{{ $revenuepotentialinfo->growthpotentialid }}'>
									@if ($revenuepotentialinfo->new_market == 1)
										0%
									@elseif ($revenuepotentialinfo->new_market == 2)
										{{ trans('condition_sustainability.less_3_percent') }}
									@elseif ($revenuepotentialinfo->new_market == 3)
										{{ trans('condition_sustainability.between_3to5_percent') }}
									@else
										{{ trans('condition_sustainability.greater_5_percent') }}
									@endif
								</span>
							</div>
						</span>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a href="{{ URL::to('/') }}/sme/revenuepotential/{{ $revenuepotentialinfo->growthpotentialid }}/delete" class="navbar-link delete-info" data-reload = '.reload-rev-growth' data-cntr = '#rev-growth-list-cntr'>{{ trans('messages.delete') }}</a>
							@endif
							</span>
						@endif
					@endforeach
					@if ($entity->status == 0 or $entity->status == 3)
						@if(@count($revenuepotential) == 0)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-rev-growth-expand' href="{{ URL::to('/') }}/sme/revenuepotential/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} Revenue Growth Potential</a>
							@endif
							</span>
						@endif
					@endif
					<div id = 'expand-rev-growth-form' class = 'spacewrapper'></div>
				</div>
			</div>
		</div>
	@endif
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->investment_expansion)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('phpm-list-cntr', $tql->data)))
		<div id = 'phpm-list-cntr' class="spacewrapper">
			<div class="read-only reload-phpm">
				<h4>{{ trans('condition_sustainability.capital_expansion') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('condition_sustainability.future_growth_relation')}}">{{trans('messages.more_info')}}</a></h4>
				<div class="read-only-text">
					@if(Auth::user()->role == 2 || Auth::user()->role == 3)
						<div class="completeness_check">{{ @count($capacityexpansion)>0 ? "true" : "" }}</div>
					@endif

					@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->investment_expansion)
						<span class="progress-required-counter" min-value="2"> {{ @count($capacityexpansion) > 0 ? 2 : 0 }} </span>
					@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->investment_expansion)
						<span class="progress-field-counter" min-value="2"> {{ @count($capacityexpansion) > 0 ? 2 : 0 }} </span>
					@else

					@endif

					@if(@count($capacityexpansion) != 0)
						<span class="details-row hidden-sm hidden-xs">
						<div class="col-md-6 col-sm-6"><b>{{ trans('condition_sustainability.within_12_mons') }}</b></div>
						<div class="col-md-6 col-sm-6"><b>{{ trans('condition_sustainability.within_24_mons') }}</b></div>
					</span>
					@endif
					@foreach ($capacityexpansion as $capacityexpansioninfo)
						<span class="details-row">
							<div class="col-md-6 col-sm-6"><b class="visible-sm-block visible-xs-block">{{ trans('condition_sustainability.within_12_mons') }}<br/></b>PHP &nbsp;
								<span class = 'editable-field' data-name = 'cap_expand.current_year' data-field-id = '{{ $capacityexpansioninfo->capacityexpansionid }}'>
									{{ number_format($capacityexpansioninfo->current_year, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('condition_sustainability.capital_expansion',[],'en')}}" label="{{trans('condition_sustainability.within_12_mons',[],'en')}}" value="PHP {{ $capacityexpansioninfo->current_year }}" group="capacity_expansion" item="current_year"></span>
							</div>
							<div class="col-md-6 col-sm-6"><b class="visible-sm-block visible-xs-block">{{ trans('condition_sustainability.within_24_mons') }}<br/></b>PHP &nbsp;
								<span class = 'editable-field' data-name = 'cap_expand.new_year' data-field-id = '{{ $capacityexpansioninfo->capacityexpansionid }}'>
									{{ number_format($capacityexpansioninfo->new_year, 2) }}
								</span>
								<span class="investigator-control" group_name="{{trans('condition_sustainability.capital_expansion',[],'en')}}" label="{{trans('condition_sustainability.within_24_mons',[],'en')}}" value="PHP {{ $capacityexpansioninfo->new_year }}" group="capacity_expansion" item="new_year"></span>
							</div>
						</span>
						@if ($entity->status == 0 or $entity->status == 3)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a href="{{ URL::to('/') }}/sme/phpm/{{ $capacityexpansioninfo->capacityexpansionid }}/delete" class="navbar-link delete-info" data-reload = '.reload-phpm' data-cntr = '#phpm-list-cntr'>{{ trans('messages.delete') }}</a>
							@endif
							</span>
						@endif
					@endforeach
					@if ($entity->status == 0 or $entity->status == 3)
						@if(@count($capacityexpansion) == 0)
							<span class="details-row" style="text-align: right;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a id = 'show-phpm-expand' href="{{ URL::to('/') }}/sme/phpm/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{ trans('condition_sustainability.capital_expansion') }}</a>
							@endif
							</span>
						@endif
					@endif
					<div id = 'expand-phpm-form' class = 'spacewrapper'></div>
					</div>
				</div>
			</div>
		@endif
	@endif
@endif

	<p style="font-size: 11px;margin-top:15px;margin-left:10px;" class = 'print-none'>{{trans('messages.disclaimer')}}</p>
	@if(!Session::has('tql'))
		<input type="button" id="nav_bcs_prev" value="{{trans('messages.previous')}}" />
		<input type="button" id="nav_bcs_next" value="{{trans('messages.next')}}" />
	@endif

	</div>
@endif

@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->rcl)
	@if(!Session::has('tql') || (Session::has('tql') && $tql->tab == 'pfs'))
		<div id="pfs" class="tab-pane fade">
			@if(!Session::has('tql') || (Session::has('tql') && in_array('pfs', $tql->data)))
				@if ($entity->status == 0 or $entity->status == 3)
					<div class="spacewrapper reload-rcl">
						<span class="details-row" style="border: 0px;">
							@if ($entity->loginid == Auth::user()->loginid)
								<a href="{{ URL::to('/') }}/sme/planfacilityrequested/{{ $entity->entityid }}" class="navbar-link pop-rcl-form add-link-text">{{trans('messages.add')}} {{trans('rcl.requested_cf')}}</a>
							@endif
						</span>
					</div>
				@endif
				<div class="spacewrapper reload-rcl">
					<div class="read-only">
						@if(Auth::user()->role == 2 || Auth::user()->role == 3)
							<div class="completeness_check">{{ @count($planfacilityrequested)>0 ? "true" : "" }}</div>
						@endif

						@if(@count($planfacilityrequested)>0)
							<h4>{{trans('rcl.rcl')}} <a href="#" class="popup-hint popup-hint-header" style="color: white" data-hint="{{trans('rcl.rcl_hint')}}">{{trans('messages.more_info')}}</a></h4>
						@endif

						@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->rcl)
							<span class="progress-required-counter" min-value="1"> {{ @count($planfacilityrequested) }} </span>
						@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->rcl)
							<span class="progress-field-counter" min-value="13"> {{ @count($planfacilityrequested) > 0 ? 13 : 0 }} </span>
						@else

						@endif

						@foreach ($planfacilityrequested as $planfacilityinfo)
							<div class="read-only-text">
								<p><b>{{trans('rcl.requested_cf')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.rcl_hint')}}">{{trans('messages.more_info')}}</a></p>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.amount_of_line')}}</div>
									<div class="col-md-8 col-sm-8">
										<span id="ds_loan_amount_source" class = 'editable-field' data-name = 'rcl.plan_credit_availment' data-field-id = '{{ $planfacilityinfo->id }}'>
											{{ number_format($planfacilityinfo->plan_credit_availment, 2) }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.amount_of_line')}}" value="{{ $planfacilityinfo->plan_credit_availment }}" group="plan_facility_requested" item="plan_credit_availment_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>

								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.purpose_of_cf')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.purpose_credit_facility' data-field-id = '{{ $planfacilityinfo->id }}'>
											@if ($planfacilityinfo->purpose_credit_facility == 1)
												{{trans('rcl.working_capital')}}
											@elseif ($planfacilityinfo->purpose_credit_facility == 2)
												{{trans('rcl.expansion')}}
											@elseif ($planfacilityinfo->purpose_credit_facility == 3)
												{{trans('rcl.takeout')}}
											@elseif ($planfacilityinfo->purpose_credit_facility == 5)
												{{trans('rcl.supplier_credit')}}
											@elseif ($planfacilityinfo->purpose_credit_facility == 6)
												{{trans('rcl.equipment_purchase')}}
											@elseif ($planfacilityinfo->purpose_credit_facility == 7)
												{{trans('rcl.guarantee')}}
											@else
												{{trans('rcl.other_specify')}}
											@endif
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.purpose_of_cf')}}" value="{{($planfacilityinfo->purpose_credit_facility == 1) ? trans('rcl.working_capital') : (($planfacilityinfo->purpose_credit_facility == 2) ? trans('rcl.expansion') : (($planfacilityinfo->purpose_credit_facility == 3) ? trans('rcl.takeout') : trans('rcl.other_specify')))}}" group="plan_facility_requested" item="purpose_credit_facility_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>

								@if (4 == $planfacilityinfo->purpose_credit_facility)
									<span id = 'rcl-other-purpose-{{ $planfacilityinfo->id }}' class="details-row">
								@else
									<span id = 'rcl-other-purpose-{{ $planfacilityinfo->id }}' class="details-row" style = 'display: none;'>
								@endif
									<div class="col-md-4 col-sm-4">{{trans('rcl.purpose_of_cf_others')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.purpose_credit_facility_others' data-field-id = '{{ $planfacilityinfo->id }}'>
											{{ $planfacilityinfo->purpose_credit_facility_others }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.purpose_of_cf_others')}}" value="{{ $planfacilityinfo->purpose_credit_facility_others }}" group="plan_facility_requested" item="purpose_credit_facility_others_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>

								@if (5 == $planfacilityinfo->purpose_credit_facility)
									<span id = 'rcl-credit-terms-{{ $planfacilityinfo->id }}' class="details-row">
								@else
									<span id = 'rcl-credit-terms-{{ $planfacilityinfo->id }}' class="details-row" style = 'display: none;'>
								@endif
									<div class="col-md-4 col-sm-4">{{trans('rcl.credit_terms')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.credit_terms' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->credit_terms }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.credit_terms',[],'en')}}" value="{{ $planfacilityinfo->credit_terms }}" group="plan_facility" item="credit_terms{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>

								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{ trans('rcl.other_remarks') }}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field empty-remarks' data-name = 'rcl.other_remarks' data-field-id = '{{ $planfacilityinfo->id }}'>
										  @if (STR_EMPTY != $planfacilityinfo->other_remarks)
										  {{ nl2br($planfacilityinfo->other_remarks) }}
										  @else

										  @endif
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.other_remarks',[],'en')}}" value="{{ $planfacilityinfo->other_remarks }}" group="plan_facility" item="other_remarks{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>

								<p><b>{{trans('rcl.offered_collateral')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.offered_collateral_hint')}}">{{trans('messages.more_info')}}</a></p>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.type_of_deposit')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.cash_deposit_details' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->cash_deposit_details }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.type_of_deposit')}}" value="{{ $planfacilityinfo->cash_deposit_details }}" group="plan_facility_requested" item="cash_deposit_details_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								<span class="details-row">
								<div class="col-md-4 col-sm-4">{{trans('rcl.deposit_amount')}}</div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.cash_deposit_amount' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ number_format($planfacilityinfo->cash_deposit_amount, 2) }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.deposit_amount')}}" value="{{ $planfacilityinfo->cash_deposit_amount }}" group="plan_facility_requested" item="cash_deposit_amount_{{ $planfacilityinfo->id }}"></span>
								</div>
								</span>
								<span class="details-row">
								<div class="col-md-4 col-sm-4">{{trans('rcl.marketable_securities')}}</div>
								<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.securities_details' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->securities_details }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.marketable_securities')}}" value="{{ $planfacilityinfo->securities_details }}" group="plan_facility_requested" item="securities_details_{{ $planfacilityinfo->id }}"></span>
								</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.marketable_securities_value')}}</div>
									<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.securities_estimated_value' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ number_format($planfacilityinfo->securities_estimated_value, 2) }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.marketable_securities_value')}}" value="{{ $planfacilityinfo->securities_estimated_value }}" group="plan_facility_requested" item="securities_estimated_value_{{ $planfacilityinfo->id }}"></span>
								</div>
								</span>

								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.property')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.property_details' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->property_details }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.property')}}" value="{{ $planfacilityinfo->property_details }}" group="plan_facility_requested" item="property_details_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.property_value')}}</div>
									<div class="col-md-8 col-sm-8">
										<span class = 'editable-field' data-name = 'rcl.property_estimated_value' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ number_format($planfacilityinfo->property_estimated_value, 2) }}
										</span>
										<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.property_value')}}" value="{{ $planfacilityinfo->property_estimated_value }}" group="plan_facility_requested" item="property_estimated_value_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.chattel')}}</div>
									<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.chattel_details' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->chattel_details }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.chattel')}}" value="{{ $planfacilityinfo->chattel_details }}" group="plan_facility_requested" item="chattel_details_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.chattel_value')}}</div>
									<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.chattel_estimated_value' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ number_format($planfacilityinfo->chattel_estimated_value, 2) }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.chattel_value')}}" value="{{ $planfacilityinfo->chattel_estimated_value }}" group="plan_facility_requested" item="chattel_estimated_value_{{ $planfacilityinfo->id }}"></span>
								</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.others')}}</div>
									<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.others_details' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ $planfacilityinfo->others_details }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.others')}}" value="{{ $planfacilityinfo->others_details }}" group="plan_facility_requested" item="others_details_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								<span class="details-row">
									<div class="col-md-4 col-sm-4">{{trans('rcl.others_value')}}</div>
									<div class="col-md-8 col-sm-8">
									<span class = 'editable-field' data-name = 'rcl.others_estimated_value' data-field-id = '{{ $planfacilityinfo->id }}'>
										{{ number_format($planfacilityinfo->others_estimated_value, 2) }}
									</span>
									<span class="investigator-control" group_name="{{trans('rcl.rcl',[],'en')}}" label="{{trans('rcl.others_value')}}" value="{{ $planfacilityinfo->others_estimated_value }}" group="plan_facility_requested" item="others_estimated_value_{{ $planfacilityinfo->id }}"></span>
									</div>
								</span>
								@if ($entity->status == 0 or $entity->status == 3)
									<span class="details-row" style="text-align: right; border: 0px;">
									@if ($entity->loginid == Auth::user()->loginid)
										<a href="{{ URL::to('/') }}/sme/planfacilityrequested/{{ $planfacilityinfo->id }}/delete" class="navbar-link delete-info" data-reload = '.reload-rcl' data-cntr = '#pfs'>{{trans('messages.delete')}}</a>
									@endif
									</span>
								@endif
							</div>
						@endforeach
					</div>
					</div>
					@endif
					@if(!Session::has('tql') || (Session::has('tql') && in_array('debt-service-calc-cntr', $tql->data)))
						@if (Auth::user()->role == 1 || Auth::user()->role == 4 || Auth::user()->role == 0)
							<div id="debt-service-calc-cntr" class="spacewrapper reload-rcl">	
								<div class="read-only">
									<h4>{{trans('rcl.debt_service_calculator')}}</h4>
									<div class="read-only-text">
										<div id="debtservicecalculator"></div>
										<input type="hidden" id="debtcalculatorerror" value="{{trans('rcl.payment_upload')}}">
										<div id="debt_service_calculator_container"></div>
									</div>
								</div>
							</div>
						@endif
					@endif


<p style="font-size: 11px;margin-top:15px;margin-left:10px;" class = 'print-none reload-rcl'>{{trans('messages.disclaimer')}}</p>
@if(!Session::has('tql'))
	<input type="button" class = 'reload-rcl' id="nav_pfs_prev" value="{{trans('messages.previous')}}" />
	@if (((4 == Auth::user()->role) && (7 > $entity->status))
	|| ((1 == Auth::user()->role) && (6 == $entity->status))
	|| (($entity->loginid == Auth::user()->loginid) && (5 == $entity->status)))
		@else
			<input type="button" class = 'reload-rcl' id="nav_pfs_next" value="{{trans('messages.next')}}" />
		@endif
	@endif
	</div>
	@endif
@endif

@if($bank_standalone == 0)
	@if(!Session::has('tql') || (Session::has('tql') && in_array('ownerdetails', $tql->data)))
		<div id="ownerdetails" class="tab-pane fade">
			<div class="spacewrapper reload-owners-details">
				@if ($entity->status == 0 or $entity->status == 3)
					<span class="details-row" style="border: 0px;">
						@if ($entity->loginid == Auth::user()->loginid)
							@if (BIZ_ENTITY_SOLE == $entity->entity_type)
								<a href="{{ URL::to('/') }}/sme/busowner/{{ $entity->entityid }}" class="navbar-link add-link-text">{{trans('messages.add')}} {{trans('owner_details.owner_and_key_management')}}</a>
							@else
								<a href="{{ URL::to('/') }}/sme/keymanager/{{ $entity->entityid }}" class="navbar-link add-link-text">{{trans('messages.add')}} {{trans('owner_details.key_manager')}}</a>
							@endif
						@endif
					</span>
				@endif
			</div>
			<div class="bs-data reload-owners-details">
			<p>{{trans('owner_details.owner_details_hint')}}</p>
			<div class="panel-group" id="accordion">
				@if(Auth::user()->role == 2 || Auth::user()->role == 3)
					<div class="completeness_check">true</div>
				@endif
				@if (BIZ_ENTITY_SOLE == $entity->entity_type)
					<span class="progress-required-counter" min-value="1">
				@if (0 >= @count($owner))
					0
				@else
					1
				@endif
				</span>

				@foreach ($owner as $ownerinfo)
					<input type="hidden" value="{{ $ownerinfo->number_of_year_engaged }}" name="cbusowner" class="cbusowner">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" class = 'print-click' href="#{{ $ownerinfo->ownerid }}">{{ $ownerinfo->firstname }} {{ $ownerinfo->lastname }}  {{ $ownerinfo->position }}</a>
								@if ($entity->status==0 AND $entity->loginid == Auth::user()->loginid)
								<a href="{{ URL::to('/') }}/sme/busowner/{{ $ownerinfo->ownerid }}/delete"
								style="float: right; font-size: 12px; line-height: 16px;"
								class="navbar-link delete-info delete-owner" data-reload = '.reload-owners-details' data-cntr = '#ownerdetails'>{{trans('messages.delete')}}</a>
								@endif
								<span class="investigator-control text-sm-xs-small" group_name="Owner Details" label="Owner" value="{{ $ownerinfo->firstname }} {{ $ownerinfo->lastname }}  {{ $ownerinfo->position }}" group="owner" item="owner_{{$ownerinfo->ownerid}}"></span>
							</h4>
						</div>
						<div id="{{ $ownerinfo->ownerid }}" class="panel-collapse collapse">
							{{ Form::hidden('ownerid', $ownerinfo->ownerid, array('class' => 'ownerid')) }}
							<div class="panel-body">
								<div class="spacewrapper">
									<div class="read-only">
										<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo1_{{ $ownerinfo->ownerid }}">{{trans('messages.owner_details')}}</button>
										<div id="demo1_{{ $ownerinfo->ownerid }}" class="collapse">
											<div class="read-only-text owner-info">
												<span class="details-row">
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.firstname')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.middlename')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.lastname')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.birthdate')}}</b></div>
												</span>
												<span class="details-row">
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.firstname' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->firstname }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.middlename' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->middlename }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.lastname' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->lastname }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field date-input' data-name = 'biz_owner.birthdate' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ date("F d, Y", strtotime($ownerinfo->birthdate)) }}
														</span>
													</div>
												</span>
												<span class="details-row">
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.nationality')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.civilstatus')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.profession')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.email')}}</b></div>
												</span>
												<span class="details-row">
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.nationality' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->nationality }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.civilstatus' data-field-id = '{{ $ownerinfo->ownerid }}'>
															@if ($ownerinfo->civilstatus == 1)
															Married
															@elseif ($ownerinfo->civilstatus == 2)
															Single
															@else
															Separated
															@endif
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.profession' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->profession }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.email' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->email }}
														</span>
													</div>
												</span>
												<span class="details-row">
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address1')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address2')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.province')}}</b></div>
													<div class="col-md-3 col-sm-3"><b></b></div>
												</span>
												<span class="details-row">
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.address1' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->address1 }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.address2' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->address2 }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.province' data-field-id = '{{ $ownerinfo->ownerid }}'>
															{{ $ownerinfo->province }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'biz_owner-city-{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->city_name }}
														</span>
													</div>
												</span>
										        <span class="details-row">
													<div class="col-md-3 col-sm-3"><b>Zip</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.no_years_present')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.phone')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>TIN</b></div>
										        </span>
										        <span class="details-row">
													<div class="col-md-3 col-sm-3">
														<span class = 'biz_owner-zipcode-{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->zipcode }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.no_yrs_present_address' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->no_yrs_present_address }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.phone' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->phone }}
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.tin_num' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->tin_num }}
														</span>
													</div>
										        </span>
										        <span class="details-row">
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.percent_ownership')}}</b></div>
													<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.years_exp')}}</b></div>
													<div class="col-md-6 col-sm-6"><b>{{trans('owner_details.position')}}</b></div>
										        </span>
										        <span class="details-row">
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.percent_of_ownership' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ number_format($ownerinfo->percent_of_ownership, 2) }}%
														</span>
													</div>
													<div class="col-md-3 col-sm-3">
														<span class = 'editable-field' data-name = 'biz_owner.number_of_year_engaged' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->number_of_year_engaged }}
														</span>
													</div>
													<div class="col-md-6 col-sm-6">
														<span class = 'editable-field' data-name = 'biz_owner.position' data-field-id = '{{ $ownerinfo->ownerid }}'>
														  {{ $ownerinfo->position }}
														</span>
													</div>
										        </span>
										    </div>
											<div class="clearfix"></div>
												<p style="margin: 10px 0;"><strong>{{trans('owner_details.personal_loans')}}</strong></p>
												<div class="read-only-text owner-loans-{{ $ownerinfo->ownerid }}">
													@if(is_array($ownerinfo->loans) AND @count($ownerinfo->loans) > 0)
														<span class="details-row reload-loans-{{ $ownerinfo->ownerid }}">
														<div class="col-sm-4">{{trans('owner_details.pl_purpose')}}</div>
														<div class="col-sm-2">{{trans('owner_details.pl_outstanding')}}</div>
														<div class="col-sm-2">{{trans('owner_details.pl_monthly')}}</div>
														<div class="col-sm-2">{{trans('owner_details.pl_creditor')}}</div>
														<div class="col-sm-2">{{trans('owner_details.pl_due')}}</div>
														</span>
														@foreach($ownerinfo->loans as $ownerloans)
															<span class="details-row reload-loans-{{ $ownerinfo->ownerid }}">
															<div class="col-sm-4">
															  @if (($entity->loginid == Auth::user()->loginid) && ((0 == $entity->status) || (3 == $entity->status)))
															  <a href="{{URL::to('/')}}/sme/delete/owner_loans/{{$ownerloans->id}}" class="navbar-link delete-info delete-loans" data-reload = '.reload-loans-{{ $ownerinfo->ownerid }}' data-cntr = '.owner-loans-{{ $ownerinfo->ownerid }}'>{{trans('messages.delete')}}</a>
															  @endif
															  @if (('Auto' == $ownerloans->purpose) || ('Housing' == $ownerloans->purpose))
															  {{ $ownerloans->purpose }}
															  @else
															  <span class = 'editable-field' data-name = 'owner_loans.purpose' data-field-id = '{{ $ownerloans->id }}'>
															    {{ $ownerloans->purpose }}
															  </span>
															  @endif
															</div>
															<div class="col-sm-2">
															  <span class = 'editable-field' data-name = 'owner_loans.balance' data-field-id = '{{ $ownerloans->id }}'>
															    {{ number_format($ownerloans->balance, 2) }}
															  </span>
															</div>
															<div class="col-sm-2">
															  <span class = 'editable-field' data-name = 'owner_loans.monthly_amortization' data-field-id = '{{ $ownerloans->id }}'>
															    {{ number_format($ownerloans->monthly_amortization, 2) }}
															  </span>
															</div>
															<div class="col-sm-2">
															  <span class = 'editable-field' data-name = 'owner_loans.creditor' data-field-id = '{{ $ownerloans->id }}'>
															    {{$ownerloans->creditor}}
															  </span>
															</div>
															<div class="col-sm-2">
															  <span class = 'editable-field date-input' data-name = 'owner_loans.due_date' data-field-id = '{{ $ownerloans->id }}'>
															    {{ date("F d, Y", strtotime($ownerloans->due_date)) }}
															  </span>
															</div>
															</span>
														@endforeach
													@endif
													@if ($entity->status == 0 or $entity->status == 3)
														<span class="details-row reload-loans-{{ $ownerinfo->ownerid }}" style="text-align: right;">
														@if ($entity->loginid == Auth::user()->loginid)
															<a href="{{URL::to('/')}}/sme/owner_loans/{{ $entity->entityid }}/{{ $ownerinfo->ownerid }}" class="navbar-link pop-personal-loan-form">{{trans('messages.add')}} Personal Loans</a>
														@endif
														</span>
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="spacewrapper">
										<div class="read-only">
											<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo7_{{ $ownerinfo->ownerid }}">{{trans('owner_details.key_manager')}}</button>
											<div id="demo7_{{ $ownerinfo->ownerid }}" class="collapse">
												<div class="read-only-text reload-key-manager-{{ $ownerinfo->ownerid }}">
													  @if(@count($ownerinfo->keymanage) > 0)
														  <div class="keymanagers">
														    @foreach($ownerinfo->keymanage as $manager)
														    	<input type="hidden" value="{{ $manager->number_of_year_engaged }}" name="ckeymanager" class="ckeymanager">

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.firstname')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.middlename')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.lastname')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.birthdate')}}</b></div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.firstname' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->firstname }}
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.middlename' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->middlename }}
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.lastname' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->lastname }}
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field date-input' data-name = 'key_managers.birthdate' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ date("F d, Y", strtotime($manager->birthdate)) }}
									        </span>
									      </div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.nationality')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.civilstatus')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.profession')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.email')}}</b></div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.nationality' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->nationality }}
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.civilstatus' data-field-id = '{{ $manager->keymanagerid }}'>
									          @if ($manager->civilstatus == 1)
									          Married
									          @elseif ($manager->civilstatus == 2)
									          Single
									          @else
									          Separated
									          @endif
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.profession' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->profession }}
									        </span>
									      </div>
									      <div class="col-md-3 col-sm-3">
									        <span class = 'editable-field' data-name = 'key_managers.email' data-field-id = '{{ $manager->keymanagerid }}'>
									          {{ $manager->email }}
									        </span>
									      </div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address1')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address2')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.province')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b><!-- {{trans('owner_details.city')}} --></b></div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3">
									       <span class = 'editable-field' data-name = 'key_managers.address1' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->address1 }}
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									     <span class = 'editable-field' data-name = 'key_managers.address2' data-field-id = '{{ $manager->keymanagerid }}'>
									      {{ $manager->address2 }}
									    </span>
									  </div>
									  <div class="col-md-3 col-sm-3">
									   <span class = 'editable-field' data-name = 'key_managers.province' data-field-id = '{{ $manager->keymanagerid }}'>
									    {{ $manager->province }}
									  </span>
									</div>
									<div class="col-md-3 col-sm-3">
									 <span class="keymanager-city-{{ $manager->keymanagerid }}">
									  {{ $manager->city_name }}
									</span>
									</div>
									</div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3"><b>Zip Code</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.no_years_present')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.phone')}}</b></div>
									      <div class="col-md-3 col-sm-3"><b>TIN</b> (e.g. 111-111-111 or 111-111-111-111A) </div>
									    </div>

									    <div class="details-row">
									      <div class="col-md-3 col-sm-3">
									       <span class="keymanager-zipcode-{{ $manager->keymanagerid }}">
									        {{ $manager->zipcode }}
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.no_yrs_present_address' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->no_yrs_present_address }}
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.phone' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->phone }}
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.tin_num' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->tin_num }}
									      </span>
									    </div>
									  </div>

									  <div class="details-row">
									    <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.percent_ownership')}}</b></div>
									    <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.years_exp')}}</b></div>
									    <div class="col-md-6 col-sm-6"><b>{{trans('owner_details.position')}}</b></div>
									  </div>

									  <div class="details-row">
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.percent_of_ownership' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->percent_of_ownership }}%
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.number_of_year_engaged' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->number_of_year_engaged }}
									      </span>
									    </div>
									    <div class="col-md-3 col-sm-3">
									      <span class = 'editable-field' data-name = 'key_managers.position' data-field-id = '{{ $manager->keymanagerid }}'>
									        {{ $manager->position }}
									      </span>
									    </div>

									    @if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
									    <div class="col-md-3 col-sm-3">
									      <a class="navbar-link delete-info delete-keymanager" href="{{ URL::To('sme/delete/keymanager/'.$manager->keymanagerid) }}" data-reload = '.reload-key-manager-{{ $ownerinfo->ownerid }}' data-cntr = '#demo7_{{ $ownerinfo->ownerid }}'>{{ trans('messages.delete')}}</a>
									    </div>
									    @endif
									  </div>

									  <span class="details-row">&nbsp;</span>
									  @endforeach
									</div>
									@endif

									@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
										<div class="details-row" style="text-align: right;">
											<a href="{{ URL::To('/sme/keymanager/'.$entity->entityid.'/'.$ownerinfo->ownerid) }}" class="navbar-link pop-biz-manager-form" data-refresh-target = 'demo7_{{ $ownerinfo->ownerid }}'>{{ trans('messages.add').' '.trans('owner_details.key_manager') }}</a>
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="spacewrapper">
					  <strong>Optional</strong>
					</div>
					<div class="spacewrapper">
						<div class="read-only">
							<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo2_{{ $ownerinfo->ownerid }}">{{trans('owner_details.other_business')}}</button>
							<div id="demo2_{{ $ownerinfo->ownerid }}" class="collapse">
								<div class="read-only-text reload-owner-biz-{{ $ownerinfo->ownerid }}">
									<div class="ownerbusinessmanage">
										@if (@count($ownerinfo->own_bizs) > 0)
											<div class="details-row">
												<div class="col-md-4 col-sm-4"><b>{{ trans('messages.name') }}</b></div>
												<div class="col-md-4 col-sm-4"><b>{{ trans('owner_details.business_type') }}</b></div>
												<div class="col-md-4 col-sm-4"><b>{{ trans('owner_details.business_location') }}</b></div>
											</div>
											@foreach($ownerinfo->own_bizs as $own_biz)
												<div class="details-row">
													<div class="col-md-4 col-sm-4">
														@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
															<a href = "{{ URL::To('/sme/delete/managebus/'.$own_biz->managebusid) }}" class="navbar-link delete-info delete-business" data-reload = '.reload-owner-biz-{{ $ownerinfo->ownerid }}' data-cntr = '#demo2_{{ $ownerinfo->ownerid }}'>{{ trans('messages.delete') }}</a>
														@endif
														<span class = 'editable-field' data-name = 'other_biz.bus_name' data-field-id = '{{ $own_biz->managebusid }}'>
														  {{ $own_biz->bus_name }}
														</span>
													</div>

													<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'other_biz.bus_type' data-field-id = '{{ $own_biz->managebusid }}'>
														  {{ $own_biz->bus_type }}
														</span>
														</div>

														<div class="col-md-4 col-sm-4">
														<span class = 'editable-field' data-name = 'other_biz.bus_location' data-field-id = '{{ $own_biz->managebusid }}'>
														  
														</span>
													</div>
												</div>
											@endforeach
										@endif
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											<div class="details-row" style="text-align: right;">
												<a href="{{ URL::To('sme/managebus/'.$entity->entityid.'/'.$ownerinfo->ownerid.'/1') }}" class="navbar-link pop-biz-mng-form">{{ trans('messages.add').' '.trans('owner_details.other_business') }}</a>
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="spacewrapper">
						<div class="read-only">
							<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo3_{{ $ownerinfo->ownerid }}">{{ trans('owner_details.educational_background') }}</button>
							<div id="demo3_{{ $ownerinfo->ownerid }}" class="collapse">
								<div class="read-only-text reload-owner-educ-{{ $ownerinfo->ownerid }}">
									<div class="ownereducation">
										@if(@count($ownerinfo->owner_educs) > 0)
											@foreach($ownerinfo->owner_educs as $owner_educ)
												<div class="details-row">
													<div class="col-md-6 col-sm-6">
													<b>
														@if($owner_educ->educ_type == 1)
															{{ trans('owner_details.highschool') }}
														@elseif ($owner_educ->educ_type == 2)
															{{ trans('owner_details.college') }}
														@elseif ($owner_educ->educ_type == 3)
															{{ trans('owner_details.postgrad') }}
														@else
															{{ trans('owner_details.others') }}
														@endif
													</b>
													</div>
													<div class="col-md-6 col-sm-6">
													<b>
														{{ trans('owner_details.level') }}
													</b>
													</div>
													<div class="col-md-6 col-sm-6">
														@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
															<a href = "{{ URL::To('sme/delete/education/'.$owner_educ->educationid) }}" class="navbar-link delete-info delete-education" data-reload = '.reload-owner-educ-{{ $ownerinfo->ownerid }}' data-cntr = '#demo3_{{ $ownerinfo->ownerid }}'>{{ trans('messages.delete') }}</a>
														@endif
														<span class = 'editable-field' data-name = 'owner_educ.school_name' data-field-id = '{{ $owner_educ->educationid }}'>
														  {{ $owner_educ->school_name }}
														</span>
													</div>
													<div class="col-md-6 col-sm-6">
														<span class = 'editable-field' data-name = 'owner_educ.educ_degree' data-field-id = '{{ $owner_educ->educationid }}'>
														  {{ $owner_educ->educ_degree }}
														</span>
													</div>
												</div>
											@endforeach
										@endif
										@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
											<div class="details-row" style="text-align: right;">
												<a href="{{ URL::To('/sme/education/'.$entity->entityid.'/'.$ownerinfo->ownerid.'/1') }}" class="navbar-link pop-educ-form">{{ trans('messages.add').' '.trans('owner_details.educational_background') }}</a>
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="spacewrapper">
						<div class="read-only">
							<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo4_{{ $ownerinfo->ownerid }}">{{ trans('owner_details.spouse_details') }}</button>
							@if($ownerinfo->spouse!=null)
								<div id="demo4_{{ $ownerinfo->ownerid }}" class="collapse">
									<div class="read-only-text">
										<div class="spouseofownerdiv">
										@if (@count($ownerinfo->spouse) > 0)
										<div class="details-row">
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.firstname') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.middlename') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.lastname') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.birthdate') }}</b></div>
										</div>

										<div class="details-row">
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.firstname' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->firstname }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.middlename' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->middlename }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.lastname' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->lastname }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field date-input' data-name = 'owner_spouse.birthdate' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												@if (isset($ownerinfo->spouse->birthdate))
												@if ('0000-00-00' != $ownerinfo->spouse->birthdate)
												{{ date("F d, Y", strtotime($ownerinfo->spouse->birthdate)) }}
												@endif
												@endif
												</span>
											</div>
										</div>

										<div class="details-row">
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.nationality') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.profession') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.phone') }}</b></div>
											<div class="col-md-3 col-sm-3"><b>TIN</b> (e.g. 111-111-111 or 111-111-111-111A) </div>
										</div>

										<div class="details-row">
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.nationality' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->nationality }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.profession' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->profession }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.phone' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->phone }}
												</span>
											</div>
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.tin_num' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
												{{ $ownerinfo->spouse->tin_num }}
												</span>
											</div>
										</div>

										<div class="details-row">
											<div class="col-md-3 col-sm-3"><b>{{ trans('owner_details.email') }}</b></div>
										</div>

										<div class="details-row">
											<div class="col-md-3 col-sm-3">
												<span class = 'editable-field' data-name = 'owner_spouse.email' data-field-id = '{{ $ownerinfo->spouse->spouseid }}'>
											    	{{ $ownerinfo->spouse->email }}
											  	</span>
											</div>
										</div>

										<div style="text-align: right; border: 0px;" class="details-row">

										</div>
									@endif
								</div>

								<div class="spacewrapper">
									<div class="read-only">

										<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo5_{{ $ownerinfo->spouse->spouseid }}">{{ trans('owner_details.spouse') }}: {{trans('owner_details.other_business')}}</button>
											<div id="demo5_{{ $ownerinfo->spouse->spouseid }}" class="collapse">
												<div class="read-only-text reload-spouse-biz-{{ $ownerinfo->spouse->spouseid }}">
													<div class="spousebusinessmanage">
														@if (@count($ownerinfo->sp_bizs) > 0)
															<div class="details-row">
																<div class="col-md-4 col-sm-4"><b>{{ trans('messages.name') }}</b></div>
																<div class="col-md-4 col-sm-4"><b>{{ trans('owner_details.business_type') }}</b></div>
																<div class="col-md-4 col-sm-4"><b>{{ trans('owner_details.business_location') }}</b></div>
															</div>
															@foreach($ownerinfo->sp_bizs as $sp_biz)
															<div class="details-row">
															<div class="col-md-4 col-sm-4">
																@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
																	<a href = "{{ URL::To('/sme/delete/managebus/'.$sp_biz->managebusid) }} " class="navbar-link delete-info delete-sbusiness" data-reload = '.reload-spouse-biz-{{ $ownerinfo->spouse->spouseid }}' data-cntr = '#demo5_{{ $ownerinfo->spouse->spouseid }}'>{{ trans('messages.delete') }}</a>
																@endif
																<span class = 'editable-field' data-name = 'other_biz.bus_name' data-field-id = '{{ $sp_biz->managebusid }}'>
																{{ $sp_biz->bus_name }}
																</span>
															</div>

															<div class="col-md-4 col-sm-4">
																<span class = 'editable-field' data-name = 'other_biz.bus_type' data-field-id = '{{ $sp_biz->managebusid }}'>
															    	{{ $sp_biz->bus_type }}
															  	</span>
															</div>
															<div class="col-md-4 col-sm-4">
																<span class = 'editable-field' data-name = 'other_biz.bus_location' data-field-id = '{{ $sp_biz->managebusid }}'>

																</span>
															</div>
															</div>
															@endforeach
														@endif
													@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0) && ($ownerinfo->spouse!=null))
														<div class="details-row" style="text-align: right;">
															<a href="{{ URL::To('sme/managebus/'.$entity->entityid.'/'.$ownerinfo->spouse->spouseid.'/2') }}" class="navbar-link pop-biz-mng-form">{{ trans('messages.add').' '.trans('owner_details.other_business') }} (Spouse)</a>
														</div>
														@endif
													</div>
											</div>
										</div>
									</div>
								</div>

								<div class="spacewrapper">
									<div class="read-only">
										<button type="button" class="btn btn-info print-click" data-toggle="collapse" data-target="#demo6_{{ $ownerinfo->spouse->spouseid }}">{{ trans('owner_details.spouse_educ_bg') }}</button>
											<div id="demo6_{{ $ownerinfo->spouse->spouseid }}" class="collapse">
												<div class="read-only-text reload-spouse-educ-{{ $ownerinfo->spouse->spouseid }}">
													<div class="spouseeducation">
													@if(@count($ownerinfo->sp_educs) > 0)
														@foreach($ownerinfo->sp_educs as $sp_educ)
															<div class="details-row">
																<div class="col-md-6 col-sm-6">
																	<b>
																	@if($sp_educ->educ_type == 1)
																		{{ trans('owner_details.highschool') }}
																	@elseif ($sp_educ->educ_type == 2)
																		{{ trans('owner_details.college') }}
																	@elseif ($sp_educ->educ_type == 3)
																		{{ trans('owner_details.postgrad') }}
																	@else
																		{{ trans('owner_details.others') }}
																	@endif
																	</b>
																</div>
																<div class="col-md-6 col-sm-6">
																	<b>
																		{{ trans('owner_details.level') }}
																	</b>
																</div>
																<div class="col-md-6 col-sm-6">
																	@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
																		<a href = "{{ URL::To('sme/delete/education/'.$sp_educ->educationid) }}" class="navbar-link delete-info delete-seducation" data-reload = '.reload-spouse-educ-{{ $ownerinfo->spouse->spouseid }}' data-cntr = '#demo6_{{ $ownerinfo->spouse->spouseid }}'>{{ trans('messages.delete') }}</a>
																	@endif
																	<span class = 'editable-field' data-name = 'owner_educ.school_name' data-field-id = '{{ $sp_educ->educationid }}'>
																	{{ $sp_educ->school_name }}
																	</span>
																</div>
																<div class="col-md-6 col-sm-6">
																	<span class = 'editable-field' data-name = 'owner_educ.educ_degree' data-field-id = '{{ $sp_educ->educationid }}'>
																    {{ $sp_educ->educ_degree }}
																  </span>
																</div>
															</div>
														@endforeach
													@endif

													@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
														<div class="details-row" style="text-align: right;">
														<a href="{{ URL::To('/sme/education/'.$entity->entityid.'/'.$ownerinfo->spouse->spouseid.'/2') }}" class="navbar-link pop-educ-form">{{ trans('messages.add').' '.trans('owner_details.spouse_educ_bg') }}</a>
														</div>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach
@else
	<span class="progress-required-counter" min-value="1">
		@if (is_countable($keymanagers) && 0 >= count($keymanagers))
			0
		@else
			1
		@endif
	</span>
	@foreach ($keymanagers as $manager)
		<input type="hidden" value="{{ $manager->number_of_year_engaged }}" name="ckeymanager" class="ckeymanager">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" class = 'print-click' href="#{{ $manager->keymanagerid }}">{{ $manager->firstname }} {{ $manager->lastname }}  {{ $manager->position }}</a>
					@if ($entity->status==0 AND $entity->loginid == Auth::user()->loginid)
						<a href="{{ URL::to('/') }}/sme/delete/keymanager/{{ $manager->keymanagerid }}"
						style="float: right; font-size: 12px; color: #FFFFFF; line-height: 16px;"
						class="navbar-link delete-info" data-reload = '.reload-owners-details' data-cntr = '#ownerdetails'>{{trans('messages.delete')}}</a>
					@endif
					<span class="investigator-control text-sm-xs-small" group_name="Owner Details" label="Owner" value="{{ $manager->firstname }} {{ $manager->lastname }}  {{ $manager->position }}" group="owner" item="owner_{{$manager->keymanagerid}}"></span>
				</h4>
			</div>
			<div id="{{ $manager->keymanagerid }}" class="panel-collapse collapse">
			<div class="spacewrapper">
			<div class="read-only-text reload-key-manager-{{ $manager->keymanagerid }}">
			<div class="details-row">
				 <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.firstname')}}</b></div>
				 <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.middlename')}}</b></div>
				 <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.lastname')}}</b></div>
				 <div class="col-md-3 col-sm-3"><b>{{trans('owner_details.birthdate')}}</b></div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.firstname' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->firstname }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.middlename' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->middlename }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.lastname' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->lastname }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field date-input' data-name = 'key_managers.birthdate' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ date("F d, Y", strtotime($manager->birthdate)) }}
					</span>
				</div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.nationality')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.civilstatus')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.profession')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.email')}}</b></div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.nationality' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->nationality }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.civilstatus' data-field-id = '{{ $manager->keymanagerid }}'>
					@if ($manager->civilstatus == 1)
					Married
					@elseif ($manager->civilstatus == 2)
					Single
					@else
					Separated
					@endif
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.profession' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->profession }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.email' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->email }}
					</span>
				</div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address1')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.address2')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.province')}}</b></div>
				<div class="col-md-3 col-sm-3"><b></b></div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.address1' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->address1 }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.address2' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->address2 }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.province' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->province }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class="keymanager-city-{{ $manager->keymanagerid }}">
					{{ $manager->city_name }}
					</span>
				</div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3"><b>Zip Code</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.no_years_present')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.phone')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>TIN</b> (e.g. 111-111-111 or 111-111-111-111A) </div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3">
					<span class="keymanager-zipcode-{{ $manager->keymanagerid }}">
					{{ $manager->zipcode }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.no_yrs_present_address' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->no_yrs_present_address }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.phone' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->phone }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.tin_num' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->tin_num }}
					</span>
				</div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.percent_ownership')}}</b></div>
				<div class="col-md-3 col-sm-3"><b>{{trans('owner_details.years_exp')}}</b></div>
				<div class="col-md-6 col-sm-6"><b>{{trans('owner_details.position')}}</b></div>
			</div>

			<div class="details-row">
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.percent_of_ownership' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->percent_of_ownership }}%
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.number_of_year_engaged' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->number_of_year_engaged }}
					</span>
				</div>
				<div class="col-md-3 col-sm-3">
					<span class = 'editable-field' data-name = 'key_managers.position' data-field-id = '{{ $manager->keymanagerid }}'>
					{{ $manager->position }}
					</span>
				</div>

				@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
					<div class="col-md-3 col-sm-3">
						<a class="navbar-link" href="{{ URL::To('sme/delete/keymanager/'.$manager->keymanagerid) }}" class="navbar-link delete-info" data-reload = '.reload-owners-details' data-cntr = '#ownerdetails'>{{ trans('messages.delete')}}</a>
					</div>
				@endif
			</div>

			<span class="details-row">&nbsp;</span>
		</div>
	</div>
</div>
</div>
@endforeach
@endif
</div>
</div>
@endif

	@if(!Session::has('tql') || (Session::has('tql') && in_array('management-list-cntr', $tql->data)))
		@if (BIZ_ENTITY_CORP == $entity->entity_type)
			<div id = 'management-list-cntr' class="spacewrapper">
		@else
		<div id = 'management-list-cntr' class="spacewrapper" style = 'display: none;'>
		@endif
		<div class="read-only reload-management">
			<h4>{{trans('business_details1.management')}}</h4>

			@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->shareholders)
				<div class="read-only-text">
			@if(Auth::user()->role == 2 || Auth::user()->role == 3)
				<div class="completeness_check">{{ @count($shareholders)>0 ? "true" : "" }}</div>
			@endif

			@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->shareholders)
				<span class="progress-required-counter" min-value="4"> {{ @count($shareholders) > 0 ? 4 : 0 }} </span>
			@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->shareholders)
				<span class="progress-field-counter" min-value="4"> {{ @count($shareholders) > 0 ? 4 : 0 }} </span>
			@else

			@endif

			@if(@count($shareholders)>0)
				<h5><b>{{trans('business_details1.shareholder')}}</b></h5>
			@endif
			@foreach ($shareholders as $shinfo)
				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{ trans('messages.name') }}</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.name' data-field-id = '{{ $shinfo->id }}'>
				    {{ $shinfo->name }}
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="Name" value="{{ $shinfo->name }}" group="shareholders" item="shareholders_name_{{ $shinfo->id }}"></span>
				</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>Address</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.address' data-field-id = '{{ $shinfo->id }}'>
				    {{ $shinfo->address }}
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="Address" value="{{ $shinfo->address }}" group="shareholders" item="shareholders_address_{{ $shinfo->id }}"></span>
				</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.amt_shareholdongs')}}</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.amount' data-field-id = '{{ $shinfo->id }}'>
				    {{ number_format($shinfo->amount, 2) }}
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="Amount of Shareholdings (Php)" value="{{ $shinfo->amount }}" group="shareholders" item="shareholders_amount_{{ $shinfo->id }}"></span>
				</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.share_to_total_stockholders_ety')}}</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.percentage_share' data-field-id = '{{ $shinfo->id }}'>
				    {{ number_format($shinfo->percentage_share, 2) }}%
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="% Share to Total Stockholders' Equity" value="{{ $shinfo->percentage_share }}" group="shareholders" item="shareholders_share_{{ $shinfo->id }}"></span>
				</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.tin_no')}}</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.id_no' data-field-id = '{{ $shinfo->id }}'>
				    @if ($shinfo->id_no != '') {{ $shinfo->id_no }} @else &nbsp; @endif
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="TIN No." value="{{ $shinfo->id_no }}" group="shareholders" item="shareholders_id_no_{{ $shinfo->id }}"></span>
				</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.nationality')}}</strong></div>
				<div class="col-md-8 col-sm-8">
				  <span class = 'editable-field' data-name = 'shareholders.nationality' data-field-id = '{{ $shinfo->id }}'>
				    {{ $shinfo->nationality }}
				  </span>
				  <span class="investigator-control" group_name="Shareholders" label="Nationality" value="{{ $shinfo->nationality }}" group="shareholders" item="shareholders_nationality_{{ $shinfo->id }}"></span>
				</div>

				@if ($entity->status == 0 && $entity->loginid == Auth::user()->loginid)
				<div class="col-md-12">
				  <a href="{{ URL::to('/') }}/sme/shareholders/{{ $shinfo->id }}/delete" class="navbar-link delete-info" data-reload = '.reload-management' data-cntr = '#management-list-cntr'>{{ trans('messages.delete') }}</a>
				</div>
				@endif
				</span>

			@endforeach
			@if ($entity->status == 0 && $entity->loginid == Auth::user()->loginid)
				<span class="details-row" style="text-align: right; width: 100%;">
				<a id = 'show-shareholders-expand' href="{{ URL::to('/') }}/sme/shareholders/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{trans('business_details1.shareholder')}}</a>
				</span>
			@endif

			<div id = 'expand-shareholders-form' class = 'spacewrapper'></div>
			    </div>
			@endif

			@if (QUESTION_CNFG_HIDDEN != $questionnaire_cnfg->directors)
				<div class="read-only-text">
			@if(@count($directors)>0)
				<h5><b>{{trans('business_details1.director')}}</b></h5>
			@endif

			@if (QUESTION_CNFG_REQUIRED == $questionnaire_cnfg->directors)
				<span class="progress-required-counter" min-value="4"> {{ @count($directors) > 0 ? 4 : 0 }} </span>
			@elseif (QUESTION_CNFG_OPTIONAL == $questionnaire_cnfg->directors)
				<span class="progress-field-counter" min-value="4"> {{ @count($directors) > 0 ? 4 : 0 }} </span>
			@else

			@endif

			@foreach ($directors as $dinfo)
				<span class="details-row">
					<div class="col-md-4 col-sm-4"><strong>{{ trans('messages.name') }}</strong></div>
					<div class="col-md-8 col-sm-8">
					<span class = 'editable-field' data-name = 'directors.name' data-field-id = '{{ $dinfo->id }}'>
						{{ $dinfo->name }}
					</span>
					<span class="investigator-control" group_name="Directors" label="Name" value="{{ $dinfo->name }}" group="directors" item="directors_name_{{ $dinfo->id }}"></span>
					</div>
				</span>

				<span class="details-row">
					<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.tin_no')}}</strong></div>
					<div class="col-md-8 col-sm-8">
						<span class = 'editable-field' data-name = 'directors.id_no' data-field-id = '{{ $dinfo->id }}'>
							@if ($dinfo->id_no != '') {{ $dinfo->id_no }} @else &nbsp; @endif
						</span>
						<span class="investigator-control" group_name="Directors" label="TIN No." value="{{ $dinfo->id_no }}" group="directors" item="directors_id_no_{{ $dinfo->id }}"></span>
					</div>
				</span>

				<span class="details-row">
					<div class="col-md-4 col-sm-4"><strong>Address</strong></div>
					<div class="col-md-8 col-sm-8">
						<span class = 'editable-field' data-name = 'directors.address' data-field-id = '{{ $dinfo->id }}'>
						  {{ $dinfo->address }}
						</span>
						<span class="investigator-control" group_name="Directors" label="Address" value="{{ $dinfo->address }}" group="directors" item="directors_address_{{ $dinfo->id }}"></span>
					</div>
				</span>

				<span class="details-row">
					<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.nationality')}}</strong></div>
					<div class="col-md-8 col-sm-8">
						<span class = 'editable-field' data-name = 'directors.nationality' data-field-id = '{{ $dinfo->id }}'>
						  {{ $dinfo->nationality }}
						</span>
						<span class="investigator-control" group_name="Directors" label="Nationality" value="{{ $dinfo->nationality }}" group="directors" item="directors_nationality_{{ $dinfo->id }}"></span>
					</div>
				</span>

				<span class="details-row">
				<div class="col-md-4 col-sm-4"><strong>{{trans('business_details1.share_to_total_stockholders_ety')}}</strong></div>
				<div class="col-md-8 col-sm-8">
					<span class = 'editable-field' data-name = 'directors.percentage_share' data-field-id = '{{ $dinfo->id }}'>
						{{ $dinfo->percentage_share }}%
					</span>
					<span class="investigator-control" group_name="Directors" label="% Share to Total Stockholders' Equity" value="{{ $dinfo->percentage_share }}" group="directors" item="directors_share_{{ $dinfo->id }}"></span>
				</div>

				@if ($entity->status == 0 && $entity->loginid == Auth::user()->loginid)
					<div class="col-md-12">
					<a href="{{ URL::to('/') }}/sme/directors/{{ $dinfo->id }}/delete" class="navbar-link delete-info" data-reload = '.reload-management' data-cntr = '#management-list-cntr'>{{ trans('messages.delete') }}</a>
					</div>
				@endif
				</span>
			@endforeach
			@if ($entity->status == 0 && $entity->loginid == Auth::user()->loginid)
				<span class="details-row" style="text-align: right; width: 100%;">
				<a id = 'show-directors-expand' href="{{ URL::to('/') }}/sme/directors/{{ $entity->entityid }}" class="navbar-link">{{ trans('messages.add') }} {{trans('business_details1.director')}}</a>
				</span>
			@endif

			<div id = 'expand-directors-form' class = 'spacewrapper'></div>
		</div>
	@endif
</div>
</div>
@endif


@if(!Session::has('tql'))
	<input type="button" id="nav_ownerdetails_prev" class = 'reload-owners-details' value="{{trans('messages.previous')}}" />
	<input type="button" id="nav_ownerdetails_next" class = 'reload-owners-details' value="{{trans('messages.next')}}" />
@endif
	</div>
@endif

@if ($entity->status == 0 && !Session::has('tql'))
	<div id="sci" class="tab-pane fade">
		<div class="spacewrapper">
			<span class="details-row" style="text-align: center; border: 0px;">
			 	
				@if ($entity->status == 0)
					{{ Form::open(['route' => ['postFinancialAnalysis', $entity->entityid], 'method' => 'post','id'=>'submit_financial_analysis_form']) }}
					<button id="credit_risk_rating_submit" type="button" disabled="disabled">
					<!-- <button id="credit_financial_analysis" type="button"> -->	
					{{trans('messages.submit_for_financial_analysis')}}</button>
					{{ Form::close() }}

				@endif

			</span>
		</div>

		<div class="spacewrapper">
			@if($bank_standalone == 0)
				<input type="button" data-toggle="tab" id="nav_sci_prev" value="{{trans('messages.previous')}}" />
			@else
				<input type="button" id="nav_submit_prev" value="{{trans('messages.previous')}}" />
			@endif
		</div>

		<div id = 'incomplete-summary' class="spacewrapper"> </div>
	</div>

@elseif ($entity->status == 3 and Auth::user()->role == 1)
	<div id="sci" class="tab-pane fade">
		<div class="spacewrapper">
			<span class="details-row" style="text-align: left; border: 0px; width: 100%;">
				@if ($entity->status == 3)
				{{ Form::open(['route' => ['postResumeInvestigation', $entity->entityid], 'method' => 'post']) }}
				@foreach ($hold as $holdinfo)
					<div class="col-md-12"><b>Notes</b></div>
					<div class="col-md-12">{{ $holdinfo->notes }}</div>
				@endforeach
				@if (@count($holddocument) >= 1)
					<div class="col-md-12"><b>Attachment</b></div>
					@foreach ($holddocument as $holddocumentinfo)
						<div class="col-md-12"><a href="{{ URL::to('/') }}/download/holddocuments/{{ $holddocumentinfo->document_rename }}" target="_blank">{{ $holddocumentinfo->document_rename }}</a></div>
					@endforeach
				@endif
				<div class="col-md-12"><button type="submit">Resume Investigation</button></div>
				{{ Form::close() }}
				@endif
			</span>
		</div>

		<input type="button" data-toggle="tab" href="#ownerdetails" id="nav_sci_prev" value="{{trans('messages.previous')}}" />

	</div>
@endif

@if ($entity->status == 6 and Auth::user()->role == 3)
<div id="ssc" class="tab-pane fade">
	<div class="col-md-4">
		<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
	</div>
<div class="col-md-12"><hr/></div>

@if ($entity->status == 6)
	{{ Form::open(['route' => ['postFinishScoring', $entity->entityid], 'method' => 'post', 'files' => true, 'id'=>'analyst_form']) }}
	{{ Form::hidden('entityid', $entity->entityid, array('id' => 'entityidscoring')) }}
	{{ Form::hidden('loginemail', $login->email, array('id' => 'loginemail')) }}
@foreach ($riskassessmentforcount as $rscoreinfo)
	@if ($rscoreinfo->risk_assessment_name == 'supplier_bargainer_pow' and $rscoreinfo->risk_assessment_solution == 'cost_leadership_sel')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'buyer_bargainer_pow' and $rscoreinfo->risk_assessment_solution == 'cost_leadership_sel')
	<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'rivalry_intensity' and $rscoreinfo->risk_assessment_solution == 'cost_leadership_sel')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_substitute' and $rscoreinfo->risk_assessment_solution == 'cost_leadership_sel')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_new_entrants' and $rscoreinfo->risk_assessment_solution == 'cost_leadership_sel')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'rivalry_intensity' and $rscoreinfo->risk_assessment_solution == 'product_innovation')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_substitute' and $rscoreinfo->risk_assessment_solution == 'product_innovation')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_new_entrants' and $rscoreinfo->risk_assessment_solution == 'product_innovation')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'rivalry_intensity' and $rscoreinfo->risk_assessment_solution == 'focus')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_substitute' and $rscoreinfo->risk_assessment_solution == 'focus')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
	@if ($rscoreinfo->risk_assessment_name == 'threat_of_new_entrants' and $rscoreinfo->risk_assessment_solution == 'focus')
		<input type="hidden" class="ra-class-unique" value="1">
	@endif
@endforeach

<div class="spacewrapper">
	<div class="col-md-12">
		<div class="col-md-12"><p id = 'analysis-error-msg' class = 'alert alert-danger' style = 'display: none;'></p></div>
		<div class="col-md-12"><b>Business Considerations and Conditions (BCC)</b></div>
		<div class="col-md-6">Risk Management</div>
		<div class="col-md-6">
			<input type="text" value="" id="bconsideration_mirror" class="form-control" readonly="readonly" />
			<input type="hidden" value="" name="bconsideration_for_save" id="bconsideration_for_save" class="form-control" />
		</div>
		<div class="col-md-6">Customer Dependency</div>
		<div class="col-md-6">{{ Form::select('cdependency',
		array('' => 'Choose here',
		'10' => 'High dependence on any one customer',
		'20' => 'Medium dependence on any one customer',
		'30' => 'Low dependence on any one customer'
		), Input::old('cdependency'), array('id' => 'cdependency', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="cdependency_for_save" id="cdependency_for_save" class="form-control" />
		</div>
		<div class="col-md-6">Supplier Dependency</div>
		<div class="col-md-6">{{ Form::select('sdependency',
		array('' => 'Choose here',
		'10' => 'High dependence on any one supplier',
		'20' => 'Medium dependence on any one supplier',
		'30' => 'Low dependence on any one supplier'
		), Input::old('sdependency'), array('id' => 'sdependency', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="sdependency_for_save" id="sdependency_for_save" class="form-control" />
		</div>
		<div class="col-md-6">Business Outlook Index Score</div>
		<div class="col-md-6">{{ Form::select('boindex',
		array('' => 'Choose here',
		'90' => 'Upward',
		'60' => 'Flat',
		'30' => 'Downward'
		), Input::old('boindex'), array('id' => 'boindex', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="boindex_for_save" id="boindex_for_save" class="form-control" />
		</div>
		<div class="col-md-12"><b>Management Quality (MQ)</b></div>
		<div class="col-md-6">Business Owner Experience</div>
		<div class="col-md-6">{{ Form::select('ownerexp',
		array('' => 'Choose here',
		'21' => '>= 5 years experience',
		'14' => '3 to 4 years experience',
		'7' => '<=2 years experience'
		), Input::old('ownerexp'), array('id' => 'ownerexp', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="ownerexp_for_save" id="ownerexp_for_save" class="form-control" />
		</div>
		<div class="col-md-6">Management Team Experience</div>
		<div class="col-md-6">{{ Form::select('mtdependency',
		array('' => 'Choose here',
		'21' => '>= 5 years experience',
		'14' => '3 to 4 years experience',
		'7' => '<=2 years experience'
		), Input::old('mtdependency'), array('id' => 'mtdependency', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="mtdependency_for_save" id="mtdependency_for_save" class="form-control" />
		</div>

		@if ($bank_standalone == 0)
			<div class="col-md-6">Business Drivers and Main Business Segments</div>
			<div class="col-md-6">
				<input type="text" value="" id="bdmaindependency_mirror" class="form-control" readonly="readonly" />
				<input type="hidden" value="" name="bdmaindependency_for_save" id="bdmaindependency_for_save" class="form-control" />
			</div>
		@endif

		<div class="col-md-6">Succession Plan</div>
		<div class="col-md-6">{{ Form::select('successionplan',
		array('' => 'Choose here',
		'36' => 'Spouse or Children aged 25-55 yrs & active in mgmt of the business for at least 5 years',
		'24' => 'Trusted employee, Distant Relative, Non-family member & active in mgmt of the business for at least 5 years',
		'12' => 'None of the Above, Less than 5 years / or No clear Succession Plan'
		), Input::old('successionplan'), array('id' => 'successionplan', 'class' => 'form-control rscorenumber', 'disabled' => 'disabled')) }}
			<input type="hidden" value="" name="successionplan_for_save" id="successionplan_for_save" class="form-control" />
		</div>

		@if ($bank_standalone == 0)
			<div class="col-md-6">Past Projects and Future Initiatives</div>
			<div class="col-md-6">
				<input type="text" value="" id="ppfuture_mirror" class="form-control" readonly="readonly" />
				<input type="hidden" value="" name="ppfuture_for_save" id="ppfuture_for_save" class="form-control" />
			</div>
		@endif

<div class="col-md-12"><b>Financial Analysis (FA)</b></div>
<div class="col-md-6">Rating of Financial Position</div>
<div class="col-md-6">
	{{ Form::select('ratingfiscore', array('' => 'Choose your score') + $readyrationList, Input::old('farawscore',$financialCondition['position_score']), array('id' => 'ratingfiscore', 'class' => 'form-control rscorenumber')) }}
	<input type="hidden" value="{{$financialCondition['position_score']}}" name="ratingfiscore_for_save" id="ratingfiscore_for_save" class="form-control" />
</div>
<div class="col-md-6">Rating of Financial Performance</div>
<div class="col-md-6">
	{{ Form::select('ratingfinancialperformancescore', array('' => 'Choose your score') + $readyrationList, Input::old('farawscore', $financialCondition['performance_score']), array('id' => 'ratingfinancialperformancescore', 'class' => 'form-control rscorenumber')) }}
	<input type="hidden" value="{{$financialCondition['performance_score']}}" name="ratingfinancialperformancescore_for_save" id="ratingfinancialperformancescore_for_save" class="form-control" />
</div>

<div class="col-md-6">Financial Analysis Converted Score</div>
<div class="col-md-6">
	{{ Form::text('faccscore', '100', array('id' => 'faccscore', 'class' => 'form-control', 'readonly' => 'readonly')) }}
	<input type="hidden" value="" name="farawscore_for_save" id="farawscore_for_save" class="form-control" />
	<input type="hidden" value="" name="faccscore_for_save" id="faccscore_for_save" class="form-control" />
</div>

@if ($bank_standalone == 0)
	<div class="col-md-6"><b>SCORE</b></div>
@endif

<div class="col-md-6">
	<span id="riskscoretotal"></span>
	<input type="hidden" value="" name="riskscoretotal_for_save" id="riskscoretotal_for_save" class="form-control" />
	</div>
	<div style="float: left; width: 100%;">
	<div class="col-md-6">&nbsp;</div>
	<div class="col-md-3">&nbsp;</div>
	<div class="col-md-3">Latest Year</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Year</div>
	<div class="col-md-3">{{ Form::text('year1', ($fa_report!=null) ? (int)$fa_report->year - 1: '', array('id' => 'year1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-3">{{ Form::text('year2', ($fa_report!=null) ? $fa_report->year: '', array('id' => 'year2', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Gross Revenue Growth</div>
	<div class="col-md-3">{{ Form::text('gross_revenue_growth1', ($fa_report!=null) ? $fa_report->income_statements[1]->Revenue : '0.00', array('id' => 'gross_revenue_growth1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-3">{{ Form::text('gross_revenue_growth2', ($fa_report!=null) ? $fa_report->income_statements[0]->Revenue : '0.00', array('id' => 'gross_revenue_growth2', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Net Income Growth</div>
	<div class="col-md-3">{{ Form::text('net_income_growth1', ($fa_report!=null) ? $fa_report->income_statements[1]->ComprehensiveIncome : '0.00', array('id' => 'net_income_growth1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-3">{{ Form::text('net_income_growth2', ($fa_report!=null) ? $fa_report->income_statements[0]->ComprehensiveIncome : '0.00', array('id' => 'net_income_growth2', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Gross Profit Margin</div>
	@if (isset($fa_report->income_statements[1]->Revenue))
		@if (0 != $fa_report->income_statements[1]->Revenue)
			<div class="col-md-3">{{ Form::text('gross_profit_margin1', ($fa_report!=null && $fa_report->income_statements[1]->Revenue != 0) ? number_format($fa_report->income_statements[1]->GrossProfit / $fa_report->income_statements[1]->Revenue,2,'.','') : '0.00', array('id' => 'gross_profit_margin1', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
		@else
			<div class="col-md-3">{{ Form::text('gross_profit_margin1', ($fa_report!=null) ? number_format($fa_report->income_statements[1]->GrossProfit,2,'.','') : '0.00', array('id' => 'gross_profit_margin1', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
		@endif
	@endif
	<div class="col-md-3">{{ Form::text('gross_profit_margin2', ($fa_report!=null && $fa_report->income_statements[0]->Revenue != 0) ? number_format($fa_report->income_statements[0]->GrossProfit / $fa_report->income_statements[0]->Revenue,2,'.','') : '0.00', array('id' => 'gross_profit_margin2', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Net Profit Margin</div>
	@if (isset($fa_report->income_statements[1]->Revenue))
		@if (0 != $fa_report->income_statements[1]->Revenue)
			<div class="col-md-3">{{ Form::text('net_profit_margin1', ($fa_report!=null && $fa_report->income_statements[1]->Revenue != 0) ? number_format($fa_report->income_statements[1]->ProfitLoss / $fa_report->income_statements[1]->Revenue,2,'.','') : '0.00', array('id' => 'net_profit_margin1', 'class' => 'form-control rscorenumber')) }}</div>
		@else
		<div class="col-md-3">{{ Form::text('net_profit_margin1', ($fa_report!=null) ? number_format($fa_report->income_statements[1]->ProfitLoss,2,'.','') : '0.00', array('id' => 'net_profit_margin1', 'class' => 'form-control rscorenumber')) }}</div>
		@endif
	@endif
	<div class="col-md-3">{{ Form::text('net_profit_margin2', ($fa_report!=null && $fa_report->income_statements[0]->Revenue != 0) ? number_format($fa_report->income_statements[0]->ProfitLoss / $fa_report->income_statements[0]->Revenue,2,'.','') : '0.00', array('id' => 'net_profit_margin2', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Net Cash Margin</div>
	<div class="col-md-3">{{ Form::text('net_cash_margin1', '0.00', array('id' => 'net_cash_margin1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-3">{{ Form::text('net_cash_margin2', '0.00', array('id' => 'net_cash_margin2', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Current Ratio</div>
	<div class="col-md-3">{{ Form::text('current_ratio1', ($fa_report!=null && $fa_report->balance_sheets[1]->CurrentLiabilities!=0) ? number_format($fa_report->balance_sheets[1]->CurrentAssets / $fa_report->balance_sheets[1]->CurrentLiabilities,2,'.','') : '0.00', array('id' => 'current_ratio1', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
	<div class="col-md-3">{{ Form::text('current_ratio2', ($fa_report!=null && $fa_report->balance_sheets[0]->CurrentLiabilities!=0) ? number_format($fa_report->balance_sheets[0]->CurrentAssets / $fa_report->balance_sheets[0]->CurrentLiabilities,2,'.','') : '0.00', array('id' => 'current_ratio2', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Debt-to-Equity Ratio</div>
	<div class="col-md-3">{{ Form::text('debt_equity_ratio1', ($fa_report!=null && $fa_report->balance_sheets[1]->Equity > 0) ? number_format($fa_report->balance_sheets[1]->Liabilities / $fa_report->balance_sheets[1]->Equity,2,'.','') : '0.00', array('id' => 'debt_equity_ratio1', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
	<div class="col-md-3">{{ Form::text('debt_equity_ratio2', ($fa_report!=null && $fa_report->balance_sheets[0]->Equity > 0) ? number_format($fa_report->balance_sheets[0]->Liabilities / $fa_report->balance_sheets[0]->Equity,2,'.','') : '0.00', array('id' => 'debt_equity_ratio2', 'class' => 'form-control rscorenumber analyst_check')) }}</div>
</div>
<!-- -->
<div class="col-md-12"><hr/></div>
<!-- -->
<div style="float: left; width: 100%;">
	<div class="col-md-6">Year</div>
	<div class="col-md-2">{{ Form::text('cc_year1', ($fa_report!=null) ? (int)$fa_report->year - 2: '', array('id' => 'cc_year1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('cc_year2', ($fa_report!=null) ? (int)$fa_report->year - 1: '', array('id' => 'cc_year2', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('cc_year3', ($fa_report!=null) ? (int)$fa_report->year - 0: '', array('id' => 'cc_year3', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
<?php
$ReceivablesTurnover1 = 0;
$ReceivablesTurnover2 = 0;
$ReceivablesTurnover3 = 0;
if($fa_report!=null){
if($fa_report->income_statements[2]->Revenue != 0 && isset($fa_report->balance_sheets[3]))
$ReceivablesTurnover1 = (($fa_report->balance_sheets[2]->TradeAndOtherCurrentReceivables +
 $fa_report->balance_sheets[3]->TradeAndOtherCurrentReceivables) / 2) /
($fa_report->income_statements[2]->Revenue / 365);
if($fa_report->income_statements[1]->Revenue != 0 && isset($fa_report->balance_sheets[2]))
$ReceivablesTurnover2 = (($fa_report->balance_sheets[1]->TradeAndOtherCurrentReceivables +
 $fa_report->balance_sheets[2]->TradeAndOtherCurrentReceivables) / 2) /
($fa_report->income_statements[1]->Revenue / 365);
if($fa_report->income_statements[0]->Revenue != 0 && isset($fa_report->balance_sheets[1]))
$ReceivablesTurnover3 = (($fa_report->balance_sheets[0]->TradeAndOtherCurrentReceivables +
 $fa_report->balance_sheets[1]->TradeAndOtherCurrentReceivables) / 2) /
($fa_report->income_statements[0]->Revenue / 365);
}
?>
<div class="col-md-6">Receivables turnover</div>
<div class="col-md-2">{{ Form::text('receivables_turnover1', round($ReceivablesTurnover1,1), array('id' => 'receivables_turnover1', 'class' => 'form-control rscorenumber')) }}</div>
<div class="col-md-2">{{ Form::text('receivables_turnover2', round($ReceivablesTurnover2,1), array('id' => 'receivab2es_turnover2', 'class' => 'form-control rscorenumber')) }}</div>
<div class="col-md-2">{{ Form::text('receivables_turnover3', round($ReceivablesTurnover3,1), array('id' => 'receivab2es_turnover3', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<?php
	if($fa_report!=null){
	$y=1;
	$InventoryTurnover = [];
	for($x=2; $x>=0; $x--){
	if(isset($fa_report->income_statements[$x]) && $fa_report->income_statements[$x]->CostOfSales != 0){

	  $inventories = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Inventories : 0;

	  $InventoryTurnover[$y] = ((
	   $fa_report->balance_sheets[$x]->Inventories +
	    $inventories
	   ) / 2) /
	  ((
	   $fa_report->income_statements[$x]->CostOfSales
	   ) / 365);
	}
	else {
	 $InventoryTurnover[$y] = 0;
	}
	$y++;
	}
	} else {
	$InventoryTurnover = [0,0,0,0];
	}
	?>

	<div class="col-md-6">Inventory turnover</div>
	<div class="col-md-2">{{ Form::text('inventory_turnover1', round($InventoryTurnover[1],1), array('id' => 'inventory_turnover1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('inventory_turnover2', round($InventoryTurnover[2],1), array('id' => 'inventory_turnover2', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('inventory_turnover3', round($InventoryTurnover[3],1), array('id' => 'inventory_turnover3', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<?php
	if($fa_report!=null){
	$y=1;
	$PayableTurnover = [];
	for($x=2; $x>=0; $x--){
	$inventories = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Inventories : 0;
	if(isset($fa_report->income_statements[$x]) &&
	 isset($fa_report->balance_sheets[$x]) &&
	 ($fa_report->income_statements[$x]->CostOfSales +
	   $fa_report->balance_sheets[$x]->Inventories - $inventories) != 0) {

	  if (isset($fa_report->balance_sheets[$x + 1])) {
	    $tcp = $fa_report->balance_sheets[$x + 1]->TradeAndOtherCurrentPayables;
	    $cpe = $fa_report->balance_sheets[$x + 1]->CurrentProvisionsForEmployeeBenefits;
	  } else {
	    $tcp = 0;
	    $cpe = 0;
	  }


	  $PayableTurnover[$y] = ((
	   $fa_report->balance_sheets[$x]->TradeAndOtherCurrentPayables +
	   $tcp + $cpe +
	   $fa_report->balance_sheets[$x]->CurrentProvisionsForEmployeeBenefits) / 2) /
	((
	 $fa_report->income_statements[$x]->CostOfSales +
	 $fa_report->balance_sheets[$x]->Inventories -
	 $inventories
	 ) / 365);
	}
	else {
	$PayableTurnover[$y] = 0;
	}
	$y++;
	}
	} else {
	$PayableTurnover = [0,0,0,0];
	}
	?>
	<div class="col-md-6">Accounts payable turnover</div>
	<div class="col-md-2">{{ Form::text('accounts_payable_turnover1', round($PayableTurnover[1],1), array('id' => 'accounts_payable_turnover1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('accounts_payable_turnover2', round($PayableTurnover[2],1), array('id' => 'accounts_payable_turnover2', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('accounts_payable_turnover3', round($PayableTurnover[3],1), array('id' => 'accounts_payable_turnover3', 'class' => 'form-control rscorenumber')) }}</div>
</div>
<div style="float: left; width: 100%;">
	<div class="col-md-6">Cash conversion cycle</div>
	<div class="col-md-2">{{ Form::text('cash_conversion_cycle1', round($ReceivablesTurnover1+$InventoryTurnover[1]-$PayableTurnover[1],1), array('id' => 'cash_conversion_cycle1', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('cash_conversion_cycle2', round($ReceivablesTurnover2+$InventoryTurnover[2]-$PayableTurnover[2],1), array('id' => 'cash_conversion_cycle2', 'class' => 'form-control rscorenumber')) }}</div>
	<div class="col-md-2">{{ Form::text('cash_conversion_cycle3', round($ReceivablesTurnover3+$InventoryTurnover[3]-$PayableTurnover[3],1), array('id' => 'cash_conversion_cycle3', 'class' => 'form-control rscorenumber')) }}</div>
</div>

<div>
<div class="col-md-12">
	{{ Form::label('average_daily_balance_save', 'Average Daily Balance') }}
	<div class="input-group">
		<span class="input-group-addon">Php</span>
		{{ Form::text('average_daily_balance_save', Input::old('average_daily_balance'), array('id' => 'average_daily_balance_save', 'class' => 'form-control fnumeric')) }}
	</div>
	<div class="col-md-6">
		<a href="#" id="auto_ocr">Auto-compute Average Daily Balance</a>
		<span id="auto_ocr_status" hidden> 
			<img  src="{{ URL::asset('images/flowing_gradient.gif')}}" width="20px" height="20px">
		</span>
		<span id="auto_ocr_success" hidden>
			Success!
		</span>
	</div>
</div>
<div class="col-md-6">
	{{ Form::label('operating_cashflow_margin_save', 'Operating Cashflow Margin') }}
<div class="input-group">
	{{ Form::text('operating_cashflow_margin_save', Input::old('operating_cashflow_margin'), array('id' => 'operating_cashflow_margin_save', 'class' => 'form-control fnumeric')) }}
	<span class="input-group-addon">%</span>
</div>
</div>
<div class="col-md-6">
	{{ Form::label('operating_cashflow_ratio_save', 'Operating Cashflow Ratio') }}
	{{ Form::text('operating_cashflow_ratio_save', Input::old('operating_cashflow_ratio'), array('id' => 'operating_cashflow_ratio_save', 'class' => 'form-control fnumeric')) }}
</div>
</div>
<div class="clearfix"></div>


<div class="col-md-6">
	<a href="#" id="auto_cashflow">Auto-compute Operating Cashflow Margin and Ratio</a>
	<span id="auto_cashflow_status"></span>
</div>

<div class="col-md-12"><hr/></div>

<div class="col-md-6"><b>FINANCIAL ANALYSIS DOCUMENT</b></div>
<div class="col-md-6">
	<div class="col-md-12">
		<input type="file" name="file" id="fadocuments"/>
	</div>
</div>

<div class="col-md-12">
	<label>Admin Review <input type="checkbox" name="admin-review" checked data-toggle="toggle" /> </label>
</div>

{{ Form::token() }}
<div class="col-md-12">
	<input id="riskscoringbutton" type="button" class="btn btn-large btn-primary" value="Save" disabled="disabled">
</div>
</div>
</div>
{{ Form::close() }}
@endif
<input type="button" data-toggle="tab" href="#ownerdetails" id="sscore_prev" value="{{trans('messages.previous')}}" />
</div>
@endif


@if ($entity->status == 7)
	@if (Auth::user()->role != 3 && Auth::user()->role != 2)
		@if ($bank_standalone == 0)
			<div id="ssc" class="tab-pane fade">
				<div class="row col-md-12">
					<a id="dev-ref-link" class = 'hidden-xs hidden-sm'>{{trans('messages.api_request_code')}}</a>
					<a id="privatelink">{{trans('messages.private_link')}}</a>
					<a id="printthis" href = '{{ URL::To("show/cbpo_pdf/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}}</a>
				</div>
				<div class="row col-md-4">
				<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
				</div>
				<div class="col-md-7" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
				<div class="col-md-12"><hr/></div>

				<div class="spacewrapper break-page">
					@if ($entity->status == 7)
						@if(isset($readyratiodata[0]))
							@if (0 != $readyratiodata[0]->gross_revenue_growth1)
								{{ Form::hidden('gross_revenue_growth2', $companyIndustry['gross_revenue_growth'], array('id' => 'gross_revenue_growth22')) }}
							@else
								{{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
							@endif

							@if (0 != $readyratiodata[0]->net_income_growth1)
								{{ Form::hidden('net_income_growth2', $companyIndustry['net_income_growth'], array('id' => 'net_income_growth22')) }}
							@else
								{{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
							@endif
								{{ Form::hidden('gross_profit_margin2', $companyIndustry['gross_profit_margin'], array('id' => 'gross_profit_margin22')) }}
								{{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
								{{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
								{{ Form::hidden('current_ratio2', $companyIndustry['current_ratio'], array('id' => 'current_ratio22')) }}
								{{ Form::hidden('debt_equity_ratio2', $companyIndustry['debt_to_equity'], array('id' => 'debt_equity_ratio22')) }}
							@endif
							<div class="row chart-remove-padding">
								<div class="col-lg-12">
									@if($show_bank_rating)
										<input type="hidden" id="bank_rating_score" value="{{$grandscore}}" />
										<div class="rating_panel_switcher">
										<span tab="original" class="active">CreditBPO Rating</span>
										<span tab="bank">Bank Rating</span>
										</div>
									@endif
									<div class="chart-container original_rating_panel">
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div id="title3_3" style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.credit_bpo_rating')}}</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div id="description3_3" style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
													<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="float: left; width: 100%;">
										<div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
											<div id="graphs3_3text0000" class="gcircle-container">
												<span id="graphs3_3text000" style="font-size: 35px;"></span>
											</div>
										</div>
										<div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
											<div style="float: left; width: 100%;">
												<p><b>Result: <span id="graphs3_3text0"></span><br/>
												<span id="graphs3_3text00" style = 'font-size: 14px;'></span></b></p>
											</div>
											<p style="display: none;"><b>Preliminary Probability of Default: <span class="prelimdefault">0%</span></b></p>
										</div>
									</div>
								</div>
								@if($show_bank_rating)
									<div class="chart-container bank_rating_panel">
										<div style="padding: 5px 0px; float: left; width: 100%;">
											<div style="float: left; width: 100%;">
												<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>Bank Rating</b></div>
												<div style="float: right; padding-right: 10px;">
												</div>
											</div>
											<div style="float: left; width: 100%; display: none;">
												<div style="padding: 10px;">
													<span class="description-bar"></span>
												</div>
											</div>
										</div>
										<div style="float: left; width: 100%;">
											<div style="float: left; text-align: center; padding-left: 10%; padding-bottom: 20px; width: 40%;">
												<div id="graphs3_3text00001" class="gcircle-container">
													<span id="graphs3_3text0001" style="font-size: 35px;"></span>
												</div>
											</div>
										<div style="float: right; width: 59%; font-size: 12px; padding-right:10px;">
											<div style="float: left; width: 100%;">
												<p><b>Result: <span id="graphs3_3text01"></span><br/>
												<span id="graphs3_3text001" style = 'font-size: 14px;'></span></b></p>
											</div>
										</div>
									</div>
								</div>
							@endif

							<div class="clearfix"></div>

						</div>
					</div>
					<div class="row chart-remove-padding break-page">
						<div class="col-lg-6">
						<!-- chart box wrapper -->
							<div class="chart-container">
								<div style="margin:0 auto; width: 97%;">
									<div style="float: left; width: 100%;">
										<div id="title1" style="float: left; width: 100%;">
											<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.business_considerations')}}</b></div>
											<div style="float: right; padding-right: 10px;">
											<b>
											  <span id="show1" style="display: block; cursor: pointer;">Expand +</span>
											</b>
											</div>
										</div>
									</div>
									<div id="description1" style="display: none;">
										<div style="padding: 10px;">
										 @foreach ($finalscore as $finalscoreinfo)
											{{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
											{{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
											{{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
											{{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
											@if ($finalscoreinfo->score_rm == 90)
												<p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
												<br/>{{trans('risk_rating.rm_high')}}
												</p>
												{{ Form::hidden('score_rm_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_rm_value')) }}
											@elseif ($finalscoreinfo->score_rm == 30)
												<p style="font-size:11px;"><b>{{trans('risk_rating.risk_management')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_risk_management_rating')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
												<br/>{{trans('risk_rating.rm_low')}}
												</p>
												{{ Form::hidden('score_rm_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_rm_value')) }}
											@endif

											{{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
											@if ($finalscoreinfo->score_cd == 30)
												<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
												<br/>{{trans('risk_rating.cd_high')}}
												</p>
												{{ Form::hidden('score_cd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_cd_value')) }}
											@elseif ($finalscoreinfo->score_cd == 20)
												<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
												<br/>{{trans('risk_rating.cd_moderate')}}
												</p>
												{{ Form::hidden('score_cd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_cd_value')) }}
											@else
												<p style="font-size:11px;"><b>{{trans('risk_rating.customer_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_customer_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
												<br/>{{trans('risk_rating.cd_low')}}
												</p>
												{{ Form::hidden('score_cd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_cd_value')) }}
											@endif

											{{ Form::hidden('score_sd', $finalscoreinfo->score_sd, array('id' => 'score_sd')) }}
											@if ($finalscoreinfo->score_sd == 30)
												<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
												<br/>{{trans('risk_rating.sd_high')}}
												</p>
												{{ Form::hidden('score_sd_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sd_value')) }}
											@elseif ($finalscoreinfo->score_sd == 20)
												<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
												<br/>{{trans('risk_rating.sd_moderate')}}
												</p>
												{{ Form::hidden('score_sd_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sd_value')) }}
											@else
												<p style="font-size:11px;"><b>{{trans('risk_rating.supplier_dependency')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_supplier_dependency')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
												<br/>{{trans('risk_rating.sd_low')}}
												</p>
												{{ Form::hidden('score_sd_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sd_value')) }}
											@endif

											{{ Form::hidden('score_bois', $finalscoreinfo->score_bois, array('id' => 'score_bois')) }}
											@if ($finalscoreinfo->score_bois == 90)
												<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
												<br/>{{trans('risk_rating.bo_high')}}
												</p>
												{{ Form::hidden('score_bois_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bois_value')) }}
											@elseif ($finalscoreinfo->score_bois == 60)
												<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
												<br/>{{trans('risk_rating.bo_moderate')}}
												</p>
												{{ Form::hidden('score_bois_value', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_bois_value')) }}
											@else
												<p style="font-size:11px;"><b>{{trans('risk_rating.business_outlook')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_outlook_index_score')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
												<br/>{{trans('risk_rating.bo_low')}}
												</p>
												{{ Form::hidden('score_bois_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bois_value')) }}
											@endif

											{{ Form::hidden('score_agroup_total', 0, array('id' => 'score_agroup_total')) }}
											{{ Form::hidden('score_agroup_sum', 240, array('id' => 'score_agroup_sum')) }}

										 @endforeach
										</div>
									</div>
									<div style="float: left; width: 98%;">
										<div id="container-pie1" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0 0 10px;"><img id = 'test-chart-export'></div>
									</div>
								</div>
							</div><!-- #End chart box wrapper -->
						</div>
						<div class="col-lg-6">
						<!-- chart box wrapper -->
						<div class="chart-container">
							<div style="margin:0 auto; width: 97%;">
								<div style="float: left; width: 100%;">
									<div id="title1" style="float: left; width: 100%;">
										<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.management_quality')}}</b></div>
										<div style="float: right; padding-right: 10px;">
										<b>
										<span id="show2" style="display: block;cursor: pointer;">Expand +</span>
										</b>
									</div>
								</div>
							</div>
							<div id="description2" style="display: none;">
							  <div style="padding: 10px;">
							   @foreach ($finalscore as $finalscoreinfo)
								   {{ Form::hidden('score_sf', $finalscoreinfo->score_sf, array('id' => 'score_sf')) }}
								   {{ Form::hidden('score_bdms', $finalscoreinfo->score_bdms, array('id' => 'score_bdms')) }}
								   {{ Form::hidden('score_boe', $finalscoreinfo->score_boe, array('id' => 'score_boe')) }}
								   {{ Form::hidden('score_mte', $finalscoreinfo->score_mte, array('id' => 'score_mte')) }}
								   @if ($finalscoreinfo->score_boe == 21)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
									   {{ Form::hidden('score_owner_exp', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_owner_exp')) }}
								   @elseif ($finalscoreinfo->score_boe == 14)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
									   {{ Form::hidden('score_owner_exp', '<span style="color: orange;">'.trans('risk_rating.moderate').'</span>', array('id' => 'score_owner_exp')) }}
								   @elseif ($finalscoreinfo->score_boe == 7)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
									   {{ Form::hidden('score_owner_exp', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_owner_exp')) }}
								   @endif
								   @if ($finalscoreinfo->score_mte == 21)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
									   {{ Form::hidden('score_mng_exp', '<span style="color: green;"> '.trans('risk_rating.high').'</span>', array('id' => 'score_mng_exp')) }}
								   @elseif ($finalscoreinfo->score_mte == 14)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
									   {{ Form::hidden('score_mng_exp', '<span style="color: orange;"> '.trans('risk_rating.moderate').'</span>', array('id' => 'score_mng_exp')) }}
								   @elseif ($finalscoreinfo->score_mte == 7)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.management_team')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
									   {{ Form::hidden('score_mng_exp', '<span style="color: red;"> '.trans('risk_rating.low').'</span>', array('id' => 'score_mng_exp')) }}
								   @endif
								   @if ($finalscoreinfo->score_bdms == 36)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
									     <br/>{{trans('risk_rating.bd_high')}}
									   </p>
									   {{ Form::hidden('score_bdms_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_bdms_value')) }}
								   @elseif ($finalscoreinfo->score_bdms == 12)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.business_driver')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_business_drivers')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
									     <br/>{{trans('risk_rating.bd_low')}}
									   </p>
									   {{ Form::hidden('score_bdms_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_bdms_value')) }}
								   @endif

								   {{ Form::hidden('score_sp', $finalscoreinfo->score_sp, array('id' => 'score_sp')) }}
								   @if ($finalscoreinfo->score_sp == 36)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
									     <br/>{{trans('risk_rating.sp_high')}}
									   </p>
									   {{ Form::hidden('score_sp_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_sp_value')) }}
								   @elseif ($finalscoreinfo->score_sp == 24)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
									     <br/>{{trans('risk_rating.sp_moderate')}}
									   </p>
									   {{ Form::hidden('score_sp_value', '<span style="color: orange;"><br/>'.trans('risk_rating.moderate').'</span>', array('id' => 'score_sp_value')) }}
								   @else
									   <p style="font-size:11px;"><b>{{trans('risk_rating.succession_plan')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_succession_plan')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
									     <br/>{{trans('risk_rating.sp_low')}}
									   </p>
									   {{ Form::hidden('score_sp_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_sp_value')) }}
								   @endif

								   {{ Form::hidden('score_ppfi', $finalscoreinfo->score_ppfi, array('id' => 'score_ppfi')) }}
								   @if ($finalscoreinfo->score_ppfi == 36)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
									     <br/>{{trans('risk_rating.pp_high')}}
									   </p>
									   {{ Form::hidden('score_ppfi_value', '<span style="color: green;">'.trans('risk_rating.high').'</span>', array('id' => 'score_ppfi_value')) }}
								   @elseif ($finalscoreinfo->score_ppfi == 12)
									   <p style="font-size:11px;"><b>{{trans('risk_rating.past_project')}} <img class="sumpopup" data-text="{{trans('risk_rating.pop_past_project')}}" src="{{ URL::asset('images/questionmark.png') }}" /> <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
									     <br/>{{trans('risk_rating.pp_low')}}
									   </p>
									   {{ Form::hidden('score_ppfi_value', '<span style="color: red;">'.trans('risk_rating.low').'</span>', array('id' => 'score_ppfi_value')) }}
								   @endif

								   {{ Form::hidden('score_bgroup_total', 0, array('id' => 'score_bgroup_total')) }}
								   {{ Form::hidden('score_bgroup_sum', 150, array('id' => 'score_bgroup_sum')) }}

							   @endforeach
							 </div>
						</div>
						<div style="float: left; width: 98%;">
							<div id="container-pie2" style="min-width: 310px; height: 350px; max-width: 100%; margin: 0 0px 0 10px;"></div>
						</div>
					</div>
				</div><!-- #End chart box wrapper -->
			</div>
		</div>
		<div class="row chart-remove-padding">
			<div class="col-lg-6 col-sm-6">
				<div class="chart-container">
				<div style="padding: 5px 0px; width: 100%; height: 35px">
					<div id="title4" style="float: left; width: 100%;">
						<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
						<div style="float: right; padding-right: 10px;">
						</div>
					</div>
					<div id="description3" style="float: left; width: 100%; display: none;">
						<div style="padding: 10px;">
						<span class="description-bar"></span>
						</div>
					</div>
				</div>
				<div style="width: 100%; height: 390px; overflow: hidden;">

				<div style="font-size: 11px;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
							<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
						</tr>
						<tr>
							<td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
							<td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
						</tr>
					</table>
				</div>
				<div style="text-align: center; font-size: 14px;">
						<div style = 'margin-top:30px;display:block;min-height: 80px; width: 100%; text-align: center;'>
                        	<img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style="width:250px;margin-bottom: 50px" />
                    	</div>
                    	<div style="clear:both"></div>
					<!-- <div id="container-semi" style="min-width: 150px; height: 250px; max-width: 100%; margin: 0 auto"></div> -->
						<div 'fc-description' style="font-size: 14px; min-width: 150px; ">
							@if ($finalscoreinfo->score_facs >= 162)
								{{trans('risk_rating.finalscore_one')}}
							@elseif ($finalscoreinfo->score_facs >= 145)
								{{trans('risk_rating.finalscore_one')}}
							@elseif ($finalscoreinfo->score_facs >= 127)
								{{trans('risk_rating.finalscore_two')}}
							@elseif ($finalscoreinfo->score_facs >= 110)
								{{trans('risk_rating.finalscore_three')}}
							@elseif ($finalscoreinfo->score_facs >= 92)
								{{trans('risk_rating.finalscore_four')}}
							@elseif ($finalscoreinfo->score_facs >= 75)
								{{trans('risk_rating.finalscore_five')}}
							@elseif ($finalscoreinfo->score_facs >= 57)
								{{trans('risk_rating.finalscore_six')}}
							@elseif ($finalscoreinfo->score_facs >= 40)
								{{trans('risk_rating.finalscore_seven')}}
							@elseif ($finalscoreinfo->score_facs >= 22)
								{{trans('risk_rating.finalscore_seven')}}
							@elseif ($finalscoreinfo->score_facs >= 4)
								{{trans('risk_rating.finalscore_seven')}}
							@else
								{{trans('risk_rating.finalscore_seven')}}
							@endif

							@if (USER_ROLE_BANK == Auth::user()->role && $m_score >= -2.22)
								<br/><br/><b>{{trans('risk_rating.fs_earnings_manipulator')}}</b>
							@endif
						</div>
				</div>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-sm-6">
			<div class="chart-container">

			<div style="width: 100%; height: 429px; font-size: 14px;">
				<table class="rating-summary-padding-table" cellpadding="0" cellspacing="0" border="0" width="100%">

					<tr>
						<td>&nbsp;</td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cc_year1 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cc_year2 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cc_year3 : "" }}</span></td>
					</tr>
					<tr>
						<td>{{trans('risk_rating.fc_receivable')}}</td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->receivables_turnover1 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->receivables_turnover2 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->receivables_turnover3 : "" }}</span></td>
					</tr>
					<tr>
						<td>+ {{trans('risk_rating.fc_inventory')}}</td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->inventory_turnover1 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->inventory_turnover2 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->inventory_turnover3 : "" }}</span></td>
					</tr>
					<tr>
						<td>- {{trans('risk_rating.fc_payable')}}</td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->accounts_payable_turnover1 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->accounts_payable_turnover2 : "" }}</span></td>
						<td style="text-align: center"><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->accounts_payable_turnover3 : "" }}</span></td>
					</tr>
					<tr>
						<td><b>= {{trans('risk_rating.fc_ccc')}}</b></td>
						<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year1 != 0) ? $readyratiodata[0]->cash_conversion_cycle1 : "" }}</span></b></td>
						<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year2 != 0) ? $readyratiodata[0]->cash_conversion_cycle2 : "" }}</span></b></td>
						<td style="text-align: center"><b><span style="white-space: nowrap;">{{ (isset($readyratiodata[0]) && $readyratiodata[0]->cc_year3 != 0) ? $readyratiodata[0]->cash_conversion_cycle3 : "" }}</span></b></td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td>{{trans('risk_rating.adb_1')}}</td>
						<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($finalscore[0]->average_daily_balance, 2, '.', ',') }}</span></td>
					</tr>
					<tr>
						<td>{{trans('risk_rating.adb_2')}}</td>
						<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_margin }} %</span></td>
					</tr>
					<tr>
						<td>{{trans('risk_rating.adb_3')}}</td>
						<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore[0]->operating_cashflow_ratio }}</span></td>
					</tr>
					<tr>
						<td>{{trans('risk_rating.adb_4')}}</td>'
						<?php
						$loan_amount = ( $finalscore[0]->average_daily_balance * 0.3 * 360 ) / ( 181 * 0.04900416667 );
						?>
						<td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($loan_amount, 2, '.', ',') }}</span></td>
					</tr>
				</table>
			</div>

		</div>
	</div>
	@if(!empty($keySummary))

		<div class="col-lg-12 col-sm-12">
			<div class="chart-container">

				<div style="width: 100%; font-size: 11px;">
			
					<div class = 'col-md-6'>
					
					<span class="divider1"></span>
					<div id="positive-points">
						<b style = 'font-size: 16px;'> The Good </b>
						<ul>
					        {{-- Noncurrent assets --}}
					        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
					        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
								<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Debt-to-equity Ratio --}}
					        @if($keySummary[0]->DebtRatio < 0.30)
								<li>{!! trans('financial_analysis/conclusion.the_debt_to_equity', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
								<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
								<li>{!! trans('financial_analysis/conclusion.the_percentage_of_liabilities', ['debtratioPercentage' => number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Working Capital and Inventories --}}
					        @if($keySummary[0]->NWC > 0)
					            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
									<li>{{trans('financial_analysis/conclusion.long_term_resources')}}</li>
					            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))
									<li>{{trans('financial_analysis/conclusion.working_capital')}}</li>
					            @endif
					        @endif

					        {{-- Current Ratio --}}
					        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CurrentRatio >= 2.1)
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio_criteria', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Quick Ratio --}}
					        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
								<li>{!! trans('financial_analysis/conclusion.a_good_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->QuickRatio >= 1.1)
								<li>{!! trans('financial_analysis/conclusion.an_outstanding_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Cash Ratio --}}
					        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->CashRatio > 0.25)
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Return of Equity --}}
					        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
								<li>{!! trans('financial_analysis/conclusion.return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year ]) !!}</li>
					        @elseif($keySummary[0]->ROE > 0.2)
								<li>{!! trans('financial_analysis/conclusion.high_return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',',')]) !!}	</li>
					        @endif

					        {{-- ROA --}}
					        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
								<li>{!! trans('financial_analysis/conclusion.good_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',',')]) !!}</li>
					        @elseif($keySummary[0]->ROA > 0.1)
								<li>{!! trans('financial_analysis/conclusion.excellent_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					        @endif

					        {{-- Net Worth --}}
					        @if($fa_report)
					            @if($balance_sheets[0]->IssuedCapital > 0)
					                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
					                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
											<li>{!! trans('financial_analysis/conclusion.net_worth_net_assets', ['netAsset' => number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
					                    @else
											<li>{{trans('financial_analysis/conclusion.the_net_worth_is_higher')}}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- Equity --}}
					        @if($fa_report)
					            @if($balance_sheets)
					                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
					                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
											<li>{!! trans('financial_analysis/conclusion.equity_value_grew', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' =>  $balance_sheets[0]->Year]) !!}</li>
					                    @else
											<li>{!! trans('financial_analysis/conclusion.the_equity_growth_for_the', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' => $balance_sheets[0]->Year ]) !!}</li>
					                    @endif
					                @endif
					            @endif
					        @endif

					        {{-- EBIT --}}
					        @if($keySummary[0]->EBIT > 0)
					            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
					                @if($keySummary[0]->EBIT > 0)
										<li>{!! trans('financial_analysis/conclusion.during_the_entire_period', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @else
										<li>{!! trans('financial_analysis/conclusion.earnings_before_interest', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
					                @endif
					            @endif
					        @endif

					        {{-- Comprehensive Income --}}
					        @if($fa_report)
					            @if($income_statements[0]->ComprehensiveIncome > 0)
									<li>{!! trans('financial_analysis/conclusion.the_income_from_financial', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}</li>
					            @endif
					        @endif

					    </ul>
					</div>
					</div>
			
					<div class = 'col-md-6'>
					
					<span class="divider1"></span>
					<div id="negative-points">
						<b style = 'font-size: 16px;'> The Bad </b>
						<ul>
					        {{-- Debt-to-equity Ratio --}}
					        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
								<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage' => number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->DebtRatio > 1)
								<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_critical', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Working Capital and Inventories --}}
					        @if($keySummary[0]->NWC > 0)
					            @if($keySummary[0]->InventoryNWC > 1.0)
									<li>{{trans('financial_analysis/conclusion.available_working_capital') }}</li>
					            @endif
					        @else
								<li>{{trans('financial_analysis/conclusion.not_enough_long_term')}}</li>
					        @endif

					        {{-- Current Ratio --}}
					        @if($keySummary[0]->CurrentRatio < 1)
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio_standard', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
								<li>{!! trans('financial_analysis/conclusion.the_current_ratio_normal', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Quick Ratio --}}
					        @if($keySummary[0]->QuickRatio < 0.5)
								<li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
								<li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
					        @endif

					        {{-- Cash Ratio --}}
							@if($keySummary[0]->CashRatio < 0.05)
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
							@elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
								<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
							@endif

							{{-- Return of Equity --}}
							@if($keySummary[0]->ROE < 0)
								<li>{!! trans('financial_analysis/conclusion.return_on_equity_roe_critical', ['roe' => number_format($keySummary[0]->ROE,2,'.',',') ]) !!}</li>
							@elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
								<li>{!! trans('financial_analysis/conclusion.return_on_equity_roe', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}</li>
							@endif

					        {{-- ROA --}}
							@if($keySummary[0]->ROA < 0)
								<li>{!! trans('financial_analysis/conclusion.critical_return_on_assets', ['year' => $keySummary[0]->Year ]) !!}</li>
							@elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
								<li>{!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets', ['roa' => number_format($keySummary[0]->ROA*100,1,'.', ','), 'year'=>$keySummary[0]->Year]) !!}</li>
							@endif

					        {{-- Net Worth --}}
					        @if($fa_report)
								@if($balance_sheets[0]->IssuedCapital > 0)
									@if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
										<li>{!! trans('financial_analysis/conclusion.net_worth_is_less_than', ['issuedCapital' => number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
									@endif
								@endif
							@endif

					        {{-- EBIT --}}
					        @if($keySummary[0]->EBIT < 0)
								<li>{!! trans('financial_analysis/conclusion.during_the_entire_period_ebit', ['ebit' => $keySummary[0]->EBIT ]) !!}</li>
							@endif

							{{-- comprehensive income --}}
							@if($fa_report)
								@if($income_statements[0]->ComprehensiveIncome < 0)
									<li>{!! trans('financial_analysis/conclusion.the_income_from_financial_operational', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',') ]) !!}</li>
								@endif
							@endif

					    </ul>
					</div>
					</div>
					
				</div>

			</div>
		</div>
	@endif
</div>

<div class="clearfix"></div>

<div class="row chart-remove-padding">
	<div class="col-lg-6 col-sm-6">
		<div class="chart-container" style = 'min-height: 375px;'>
			<div style="padding: 5px 0px; float: left; width: 100%;">
				<div id="title4" style="float: left; width: 100%;">
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
					<div style="float: right; padding-right: 10px; text-align: right;">
						<b>
						 <span id="show4" style="display: block;cursor: pointer;">Expand +</span>
						</b>
		                <div class="grdp_display"><small><strong>Region:</strong> {{ $entity->regDesc }}</small></div>
						<div class="grdp_value"><small><strong>GRDP:</strong> {{$industryGrdp->grdp * 100}} <strong>Year:</strong> {{$industryComparisonYear}}</small></div>
					</div>
				</div>
			</div>
			<div id="description4" style="display: none;">
				<div style="padding: 10px;">
					<span class="description-bar"></span>
				</div>
			</div>
			<div style="float: left; width: 98%;">
				<div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
			</div>
			<div style="float: left; width: 98%;">
				<div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
			</div>

		</div>
		<div class="clearfix"></div>
		</div>
		<div class="col-lg-6 col-sm-6">
			<div class="chart-container" style = 'min-height: 375px;'>
			<div style="float: left; width: 98%;">
				<div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
			</div>
			<div style="float: left; width: 98%;">
				<div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
			</div>
			<div style="float: left; width: 98%;">
				<div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
			</div>
			<div class="legend">
				<div class="legend-item">
					<span class="legend-info"></span>
					<span>Company</span>
				</div>
				<div class="legend-item">
					<span class="legend-info industry"></span>
					<span>Industry</span>
				</div>
			</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
		<div class="clearfix"></div>
		<p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
		<br/>
		<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
		<p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
		<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

		@endif

		<input type="button" id="finananalysis_next" value="{{trans('messages.next')}}" />

	</div>
</div>
@else
	<div id="ssc" class="tab-pane fade">
	@foreach ($finalscore as $finalscoreinfo)
		{{ Form::hidden('score_cd', $finalscoreinfo->score_cd, array('id' => 'score_cd')) }}
		{{ Form::hidden('score_rfp', $finalscoreinfo->score_rfp, array('id' => 'score_rfp')) }}
		{{ Form::hidden('score_rfpm', $finalscoreinfo->score_rfpm, array('id' => 'score_rfpm')) }}
		{{ Form::hidden('score_facs', $finalscoreinfo->score_facs, array('id' => 'score_facs')) }}
		{{ Form::hidden('score_rm', $finalscoreinfo->score_rm, array('id' => 'score_rm')) }}
	@endforeach
	<div class="row col-md-12">
		<a id="dev-ref-link" class = 'hidden-xs hidden-sm'>{{trans('messages.api_request_code')}}</a>
		<a id="privatelink">{{trans('messages.private_link')}}</a>
		@if (4 == Auth::user()->role)
		<a id="printdscr" href = '{{ URL::To("show/dscr_print/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}} DSCR</a>
		@endif

		<a id="printthis" href = '{{ URL::To("show/cbpo_pdf/".$entity->entityid) }}' target = '_blank'>{{trans('messages.print')}} Rating Report</a>

	</div>
	<div class="row col-md-4">
		<img src="{{ URL::asset('images/creditbpo-logo.jpg') }}" style="margin-bottom:5px;" />
	</div>
	<div class="col-md-7" style="margin-top:10px;margin-left:10px;font-size:18px;">{{trans('risk_rating.rating_report_for')}} {{ $entity->companyname }}</div>
	<div class="col-md-12"><hr/></div>

	<div class="spacewrapper break-page">
		@if ($entity->status == 7)
			@if(isset($readyratiodata[0]))
				@if (0 != $readyratiodata[0]->gross_revenue_growth1)
					{{ Form::hidden('gross_revenue_growth2', $companyIndustry['gross_revenue_growth'], array('id' => 'gross_revenue_growth22')) }}
				@else
					{{ Form::hidden('gross_revenue_growth2', 0, array('id' => 'gross_revenue_growth22')) }}
				@endif

				@if (0 != $readyratiodata[0]->net_income_growth1)
					{{ Form::hidden('net_income_growth2', $companyIndustry['net_income_growth'], array('id' => 'net_income_growth22')) }}
				@else
					{{ Form::hidden('net_income_growth2', 0, array('id' => 'net_income_growth22')) }}
				@endif
				{{ Form::hidden('gross_profit_margin2', $companyIndustry['gross_profit_margin'], array('id' => 'gross_profit_margin22')) }}
				{{ Form::hidden('net_profit_margin2', $readyratiodata[0]->net_profit_margin2, array('id' => 'net_profit_margin22')) }}
				{{ Form::hidden('net_cash_margin2', $readyratiodata[0]->net_cash_margin2, array('id' => 'net_cash_margin22')) }}
				{{ Form::hidden('current_ratio2', $companyIndustry['current_ratio'], array('id' => 'current_ratio22')) }}
				{{ Form::hidden('debt_equity_ratio2', $companyIndustry['debt_to_equity'], array('id' => 'debt_equity_ratio22')) }}
			@endif
			<div class="row chart-remove-padding">
				<div class="col-lg-12 col-sm-12">
					<div class="chart-container">
					<!-- <div class="chart-container" id="financial_condition_chart"> -->
					<!-- <div style="padding: 5px 0px; width: 100%; height: 35px">
						<div id="title4" style="float: left; width: 100%;">
							<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
							<div style="float: right; padding-right: 10px;">
							</div>
						</div>
						<div id="description3" style="float: left; width: 100%; display: none;">
							<div style="padding: 10px;">
							<span class="description-bar"></span>
							</div>
						</div>
						</div> -->
					<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.financial_condition')}}</b></div>
					<div style="width: 100%; overflow: hidden;">

					<div style="font-size: 11px;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_position_rating')}}</b></td>
							<td width="50%" style="text-align: center; font-size: 16px;"><b>{{trans('risk_rating.financial_performance_rating')}}</b></td>
						</tr>
						<tr>
							<td style="text-align: center; font-size: 15px;"><span id="fposition_value"></span></td>
							<td style="text-align: center; font-size: 15px;"><span id="fperformance_value"></span></td>
						</tr>
					</table>
					</div>
					<div style="text-align: center; font-size: 14px;">
						<div style = 'margin-top:30px;display:block;min-height: 80px; width: 100%; text-align: center;'>
                        	<img src = '{{ URL::asset("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style="width:250px;margin-bottom: 50px" />
                    	</div>
                    	<div style="clear:both"></div>
						<!-- <div id="container-semi" style="min-width: 150px; height: 300px; max-width: 100%; margin: 0 auto"></div> -->
						<div 'fc-description' style="min-width: 150px; max-width: 100%; margin: 0 auto; padding-left: 75px; padding-right: 75px; padding-bottom: 40px;">
							@if ($finalscoreinfo->score_facs >= 162)
								{{trans('risk_rating.finalscore_one')}}
							@elseif ($finalscoreinfo->score_facs >= 145)
								{{trans('risk_rating.finalscore_one')}}
							@elseif ($finalscoreinfo->score_facs >= 127)
								{{trans('risk_rating.finalscore_two')}}
							@elseif ($finalscoreinfo->score_facs >= 110)
								{{trans('risk_rating.finalscore_three')}}
							@elseif ($finalscoreinfo->score_facs >= 92)
								{{trans('risk_rating.finalscore_four')}}
							@elseif ($finalscoreinfo->score_facs >= 75)
								{{trans('risk_rating.finalscore_five')}}
							@elseif ($finalscoreinfo->score_facs >= 57)
								{{trans('risk_rating.finalscore_six')}}
							@elseif ($finalscoreinfo->score_facs >= 40)
								{{trans('risk_rating.finalscore_seven')}}
							@elseif ($finalscoreinfo->score_facs >= 22)
								{{trans('risk_rating.finalscore_seven')}}
							@elseif ($finalscoreinfo->score_facs >= 4)
								{{trans('risk_rating.finalscore_seven')}}
							@else
								{{trans('risk_rating.finalscore_seven')}}
							@endif

							@if (USER_ROLE_BANK == Auth::user()->role && $m_score >= -2.22)
								<br/><br/><b>{{trans('risk_rating.fs_earnings_manipulator')}} </b>
							@endif
						</div>

					</div>

					@if(!empty($keySummary))
					<div style = 'font-size: 11px;' class = 'col-md-12'>
							
						
								<div class = 'col-md-6 positive_points_parent'>
							
							
							<span class="divider1"></span>
							<div id="positive-points">
								<b style = 'font-size: 16px;'> The Good </b>
								<ul class="positive_points">
							        {{-- Noncurrent assets --}}
							        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
										<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
							        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
										<li>{!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good', ['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Debt-to-equity Ratio --}}
							        @if($keySummary[0]->DebtRatio < 0.30)
										<li>{!! trans('financial_analysis/conclusion.the_debt_to_equity', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
										<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has', ['debtratio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
										<li>{!! trans('financial_analysis/conclusion.the_percentage_of_liabilities', ['debtratioPercentage' => number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Working Capital and Inventories --}}
							        @if($keySummary[0]->NWC > 0)
							            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
							                <li>{{trans('financial_analysis/conclusion.long_term_resources')}}</li>
							            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))
											<li>{{trans('financial_analysis/conclusion.working_capital')}}</li>
							            @endif
							        @endif

							        {{-- Current Ratio --}}
							        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->CurrentRatio >= 2.1)
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio_criteria', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Quick Ratio --}}
							        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
										<li>{!! trans('financial_analysis/conclusion.a_good_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->QuickRatio >= 1.1)
										<li>{!! trans('financial_analysis/conclusion.an_outstanding_relationship', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Cash Ratio --}}
							        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->CashRatio > 0.25)
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Return of Equity --}}
							        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
										<li>{!! trans('financial_analysis/conclusion.return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',','), 'year' => $keySummary[0]->Year ]) !!}</li>
							        @elseif($keySummary[0]->ROE > 0.2)
										<li>{!! trans('financial_analysis/conclusion.high_return_on_equity', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.',',')]) !!}</li>
							        @endif

							        {{-- ROA --}}
							        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
										<li>{!! trans('financial_analysis/conclusion.good_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',',')]) !!}</li>
							        @elseif($keySummary[0]->ROA > 0.1)
										<li>{!! trans('financial_analysis/conclusion.excellent_return_on_assets', ['roaPercentage' => number_format($keySummary[0]->ROA*100,2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
							        @endif

							        {{-- Net Worth --}}
							        @if($fa_report)
							            @if($balance_sheets[0]->IssuedCapital > 0)
							                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
							                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
													<li>{!! trans('financial_analysis/conclusion.net_worth_net_assets', ['netAsset' => number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
							                    @else
							                        <li>{{trans('financial_analysis/conclusion.the_net_worth_is_higher')}}</li>
							                    @endif
							                @endif
							            @endif
							        @endif

							        {{-- Equity --}}
							        @if($fa_report)
							            @if($balance_sheets)
							                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
							                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
													<li>{!! trans('financial_analysis/conclusion.equity_value_grew', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' =>  $balance_sheets[0]->Year]) !!}</li>
							                    @else
													<li>{!! trans('financial_analysis/conclusion.the_equity_growth_for_the', ['startYear' => $balance_sheets[count($balance_sheets)-1]->Year, 'endYear' => $balance_sheets[0]->Year ]) !!}</li>
							                    @endif
							                @endif
							            @endif
							        @endif

							        {{-- EBIT --}}
							        @if($keySummary[0]->EBIT > 0)
							            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
							                @if($keySummary[0]->EBIT > 0)
												<li>{!! trans('financial_analysis/conclusion.during_the_entire_period', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
							                @else
												<li>{!! trans('financial_analysis/conclusion.earnings_before_interest', ['ebit' => number_format($keySummary[0]->EBIT,2,'.',',') ]) !!}</li>
							                @endif
							            @endif
							        @endif

							        {{-- Comprehensive Income --}}
							        @if($fa_report)
							            @if($income_statements[0]->ComprehensiveIncome > 0)
											<li>{!! trans('financial_analysis/conclusion.the_income_from_financial', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}</li>
							            @endif
							        @endif

							    </ul>
							</div>
							</div>
						
						
								<div class = 'col-md-6 negative_points_parent'>
							
						
						    <span class="divider1"></span>
						    <div id="negative-points">
								<b style = 'font-size: 16px;'> The Bad </b>
								<ul class="negative_points">
							        {{-- Debt-to-equity Ratio --}}
							        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
										<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage' => number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->DebtRatio > 1)
										<li>{!! trans('financial_analysis/conclusion.the_debt_ratio_critical', ['debtRatio' => number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Working Capital and Inventories --}}
							        @if($keySummary[0]->NWC > 0)
							            @if($keySummary[0]->InventoryNWC > 1.0)
											<li>{{trans('financial_analysis/conclusion.available_working_capital') }}</li>
							            @endif
							        @else
							            <li>{{trans('financial_analysis/conclusion.not_enough_long_term')}}</li>
							        @endif

							        {{-- Current Ratio --}}
							        @if($keySummary[0]->CurrentRatio < 1)
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio_standard', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
										<li>{!! trans('financial_analysis/conclusion.the_current_ratio_normal', ['currentRatio' => number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Quick Ratio --}}
							        @if($keySummary[0]->QuickRatio < 0.5)
										<li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
										<li>{!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable', ['quickRatio' => number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}</li>
							        @endif

							        {{-- Cash Ratio --}}
							        @if($keySummary[0]->CashRatio < 0.05)
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
							        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
										<li>{!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal', ['cashRatio' => number_format($keySummary[0]->CashRatio, 2, '.', ',') ]) !!}</li>
							        @endif

							        {{-- Return of Equity --}}
							        @if($keySummary[0]->ROE < 0)
										<li>{!! trans('financial_analysis/conclusion.return_on_equity_roe_critical', ['roe' => number_format($keySummary[0]->ROE,2,'.',',') ]) !!}</li>
							        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
										<li>{!! trans('financial_analysis/conclusion.return_on_equity_roe', ['roePercentage' => number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}</li>
							        @endif

							        {{-- ROA --}}
							        @if($keySummary[0]->ROA < 0)
										<li>{!! trans('financial_analysis/conclusion.critical_return_on_assets', ['year' => $keySummary[0]->Year ]) !!}</li>
							        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
										<li>{!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets', ['roa' => number_format($keySummary[0]->ROA*100,1,'.', ','), 'year'=>$keySummary[0]->Year]) !!}</li>
							        @endif

							        {{-- Net Worth --}}
							        @if($fa_report)
							            @if($balance_sheets[0]->IssuedCapital > 0)
							                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
												<li>{!! trans('financial_analysis/conclusion.net_worth_is_less_than', ['issuedCapital' => number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','), 'year' => $keySummary[0]->Year]) !!}</li>
							                @endif
							            @endif
							        @endif

							        {{-- EBIT --}}
							        @if($keySummary[0]->EBIT < 0)
										<li>{!! trans('financial_analysis/conclusion.during_the_entire_period_ebit', ['ebit' => $keySummary[0]->EBIT ]) !!}</li>
							        @endif

							        {{-- comprehensive income --}}
							        @if($fa_report)
							            @if($income_statements[0]->ComprehensiveIncome < 0)
											<li>{!! trans('financial_analysis/conclusion.the_income_from_financial_operational', ['comprehensiveIncome' => number_format($income_statements[0]->ComprehensiveIncome,2,'.',',') ]) !!}</li>
							            @endif
							        @endif

							    </ul>
						    </div>
						  </div>
						  
						</div>
						@endif
					</div>

				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<!-- <div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container">
					
					<p><strong>Statement of Cash Flows</strong></p>

					<table class="table">
                        <tr>
                            <td>Net Operating Income</td>
                            <td>
                            	@if(!empty($cash_flows[0]->NetOperatingIncome))
                            		{{ number_format($cash_flows[0]->NetOperatingIncome,2,".",",") }}
                            	@endif
                            </td>
                        </tr>
                        <tr>
                            <td>Net Cashflow from Operations</td>
                            <td>
                            	@if(!empty($cash_flows[0]->NetCashFlowFromOperations))
                            		{{ number_format($cash_flows[0]->NetCashFlowFromOperations,2,".",",") }}
                            	@endif
                            </td>
                        </tr>
                        <tr>
                            <td>Net Profit Margin</td>
                            <td>{{ $cf_net_profit_margin_percentage }}%</td>
                        </tr>
                        <tr>
                            <td>Asset Turnover</td>
                            <td>{{ $cf_asset_turnover_percentage }}%</td>
                        </tr>
                        <tr>
                            <td>Equity Multiplier</td>
                            <td>{{ $cf_equity_multiplier_percentage }}%</td>
                        </tr>
                        <tr>
                            <td>DuPont Analysis</td>
                            <td>{{ $cf_dupont_analysis_percentage }}%</td>
                        </tr>

                    </table>
				</div>
			</div>
		</div> -->

		<div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container">
					<p><strong>Cash Conversion Cycle</strong></p>
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col" class="col-md-2 text-center" rowspan="2" ></th>
								<th scope="col" class="col-md-2 text-center" colspan="{{count($turnoverRatios)}}" style="font-size: 15px;">Value, days</th>
								<th scope="col" class="col-md-2 text-center" rowspan ="2" style="font-size: 15px;">{!! trans('financial_analysis/turnover_ratios.change',['colCount' => count($turnoverRatios)+1]) !!}</th>
							</tr>
							@foreach($turnoverRatios as $turnover)
								<th style="text-align:center;" style="font-size: 15px;">{{$turnover->year}}</th>
							@endforeach
						<thead>
						<tbody>
							<tr>
								<td scope="col" class="col-md-2 text-center">1</td>
								@for($x=2; $x<=(count($turnoverRatios) + 1); $x++)
									<td scope="col" class="col-md-2 text-center" style="vertical-align: middle;">{{$x}}</td>
								@endfor
								<td scope="col" class="col-md-2 text-center">{{$x}}</td>
							</tr>
							<tr>
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">Days Sales Outstanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">{{$turnover->ReceivablesTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">Days Payable Oustanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">{{$turnover->PayableTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">Days Inventory Outstanding</td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">{{$turnover->InventoryTurnover}}</td>
								@endforeach
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
									@endif
								</td>
							</tr>
							<tr>
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;"><strong>Cash Conversion Cycle</strong></td>
								@foreach($turnoverRatios as $turnover)
									<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">{{$turnover->CCC}}</td>
								@endforeach
								<td scope="col" class="text-center" style="vertical-align: middle; font-size: 15px;">
									@if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) == 0)
										-
									@elseif(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) > 0)
										<span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
									@else
										<span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
					<!-- <p style="font-size: 11px;" class = 'print-none'>
						@php
						$totalDays = 0;
						$totalPayableDays = 0;
						$cnt = 0;
						foreach($turnoverRatios as $turnover){
							$totalDays += $turnover->AssetTurnover;
							$totalPayableDays += $turnover->PayableTurnover;
							$cnt += 1;
						}
						$arr = [
							'receivablesTurnover' => $turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover,
							'payableTurnover' => $turnoverRatios[count($turnoverRatios)-1]->PayableTurnover,
							'financialReportTitle' => $entity->companyname,
							'totalDays'            => round(($totalDays/$cnt),0),
							'payableDays'          => round(($totalPayableDays/$cnt),0)
						];
					@endphp
					{!! trans('financial_analysis/turnover_ratios.during_the_last_year',$arr) !!} -->
					</p>
				</div>
			</div>
		</div>

		<div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container">
					
					<p><strong>Return on Equity Drivers</strong></p>

					<table class="table">
						<thead class="thead-dark">
						<tr>
							<th scope="col" class="col-md-2 text-center">    </th>
							@foreach($ROEDrivers as $drivers)
								<th scope="col" class="text-center" style="font-size: 15px;">{{$drivers['year']}}</th>
							@endforeach
						</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Net Income</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['net_income'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Profit Margin</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['profit_margin'],2,".",",")}}</strong></td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Ave. Assets</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Asset Turnover</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['turnover_asset'],2,".",",")}}</strong></td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Revenue</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['revenue'],2,".",",")}}</td>
								@endforeach
							</tr>	
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;">Ave. Assets</td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;">{{number_format($drivers['assets_average'],2,".",",")}}</td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Financial Leverage</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format($drivers['financial_leverage'],2,".",",")}}</strong></td>
								@endforeach
							</tr>
							<tr>
								<td scope="col" class="text-left" style="font-size: 15px;"><strong>Return on Equity</strong></td>
								@foreach($ROEDrivers as $drivers)
									<td scope="col" class="text-center" style="font-size: 15px;"><strong>{{number_format(($drivers['roe']*100),2,".",",")}}%</strong></td>
								@endforeach
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		@if($forecast_data != null)
		<div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container" id="growth_forecasting_chart">
					<p>
						<strong>Growth Forecast</strong>
						<a style = "color: #056fc4;" href="#" class="popup-hint popup-hint-header" data-hint="{!!trans('risk_rating.more_info')!!}">
							{{trans('messages.more_info')}}
						</a>
					</p>
					<script>var forecast_data = {!! json_encode($forecast_data) !!};</script>
					<div id="forecast_graph"></div>
					<div>
						<p style = 'font-size: 14px;'>{!! $forecast_data['description'] !!}</p>
					</div>
				</div>
			</div>
		</div>
	@endif

	@if ($export_dscr == 1)
		<div class="clearfix"></div>
		<div class="row chart-remove-padding">
			<div class="col-lg-12 col-sm-12">
				<div class="chart-container" id="export_dscr_rating">
					<strong>User Exported Parameters</strong>
					<div id="debt_service_calculator_rs_container"></div>
				</div>
			</div>
		</div>
	@endif

	<div class="clearfix"></div>
	<div class="row chart-remove-padding" id="industry_composition_chart">
		<div class="col-lg-6 col-sm-6">
		<div class="chart-container" style = 'min-height: 375px;'>
		<div style="padding: 5px 0px; float: left; width: 100%;">
			<div id="title4" style="float: left; width: 100%;">
				<div style="float: left; padding-left: 10px; padding-bottom: 10px;"><b>{{trans('risk_rating.industry_comparison')}}: {{$entity->main_title}}</b></div>
				<div style="float: right; padding-right: 10px; text-align: right;">
					<small><b><span id="show4" style="display: block;cursor: pointer;">Expand +</span></b></small>
					<div class="grdp_display"><small><strong>Region:</strong> {{ $entity->regDesc }}</small></div>
					<div class="grdp_value"><small><strong>GRDP:</strong> {{$industryGrdp->grdp * 100}} <strong>Year:</strong> {{$industryComparisonYear}}</small></div>
				</div>
			</div>
		</div>
		<div id="description4" style="display: none;">
			<div style="padding: 10px;">
				<span class="description-bar"></span>
			</div>
		</div>
		<div style="float: left; width: 98%;">
			<div id="container-bar" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
		</div>
		<div style="float: left; width: 98%;">
			<div id="container-bar2" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
		</div>

    </div>
	<div class="clearfix"></div>
</div>
<div class="col-lg-6 col-sm-6">
	<div class="chart-container" style = 'min-height: 375px;'>
		<div style="float: left; width: 98%;">
			<div id="container-bar3" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
		</div>
		<div style="float: left; width: 98%;">
			<div id="container-bar4" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
		</div>
		<div style="float: left; width: 98%;">
			<div id="container-bar5" style="min-width: 310px; height: 100px; max-width: 100%; margin: 0 auto"></div>
		</div>
		<div class="legend">
			<div class="legend-item">
				<span class="legend-info"></span>
				<span>You</span>
			</div>
			<div class="legend-item">
				<span class="legend-info industry"></span>
				<span>Industry</span>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
</div>
<div class="clearfix"></div>
<p><i>{{trans('risk_rating.credit_bpo_rating_hint')}}</i></p>
<br/>
<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
<p style="font-size: 12px;" class = 'print-none'><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
<p style="font-size: 11px;" class = 'print-none'>{{trans('risk_rating.copyright')}}</p>

@endif

<input type="button" id="finananalysis_next" value="{{trans('messages.next')}}" />

</div>
</div>
  @endif
  @if($entity != null && $entity->is_premium !==2 )
  <div id="fianalysis" class="tab-pane fade">
  	
    <div class="spacewrapper">
      
     <div class="col-md-4">
      <img src="{{ URL::asset('images/creditbpo-logo-big.png') }}" width="100%" style="margin-bottom:5px;" />
    </div>

    <div class="col-md-8" style="margin-top:60px;padding-left:60px;font-size:18px;">{{ trans('messages.financial_analysis_for') }} {{ $entity->companyname }}</div>
    <div class="col-md-12"><hr/></div> 
    @php
    $fname_count = 0;
    $fname_exp  = [];
    if(!empty($documents9->document_orig)){
      $fname_exp = explode('.', $documents9->document_orig);
      $fname_count = @count($fname_exp);
    }
    
    $useIndex = 0;
    if($fname_count > 2){
      $useIndex = $fname_count - 1;
    }else{
      $useIndex = 1;
    }

    $lang = app()->getLocale();
    ($lang == 'fil') ? $suffix = 'tl':$suffix = 'en';
    
    @endphp

    @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'test')

      @if(!empty($fname_exp[$useIndex]))
        @if ('pdf' == $fname_exp[$useIndex])
        <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
          <div class="col-md-12">
            <p>
              It appears you don't have a PDF plugin for this browser.
              No biggie... you can
              <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}">click here to download the PDF file.</a>
            </p>
          </div>
        </object>
        @else
        <div class="col-md-12">
          <p>
            It appears you don't have a PDF plugin for this browser.
            No biggie... you can
            <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}">click here to download the PDF file.</a>
          </p>
        </div>
        @endif

      @else
        
        <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
          <div class="col-md-12">
            <p>
              It appears you don't have a PDF plugin for this browser.
              No biggie... you can
              <a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" target="_blank">click here to download the PDF file.</a>
            </p>
          </div>
        </object>

      @endif

    @else

      <object data="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" type="application/pdf" width="100%" height="1000px">
          <div class="col-md-12">
				<p>
					It appears you don't have a PDF plugin for this browser.
					No biggie... you can
					<a href="{{ URL::To('/') }}/financial-report/download/financial-analysis/{{ $fa_report->entity_id.'/'.$suffix }}" target="_blank">click here to download the PDF file.</a>
				</p>
          </div>
        </object>

    @endif    

    <div class="col-md-12">
		<p style="font-size: 11px;">{{trans('risk_rating.rating_summary_disclaimer1')}} </p>
		<p style="font-size: 12px;"><b>{{trans('risk_rating.rating_summary_disclaimer2')}}</b></p>
		<p style="font-size: 11px;">{{trans('risk_rating.copyright')}}</p>

		<input type="button" id="sscore_prev" value="{{trans('messages.previous')}}" />

		@if($bank_standalone == 0)
			<input type="button" id="sscore_next" value="{{trans('messages.next')}}" />
		@else
			<input type="button" id="strategicprofile_next" value="{{trans('messages.next')}}" />
		@endif
    </div>

  </div>
  @endif
</div>
@endif
@endif
@if($bank_standalone == 0)
	@if (Auth::user()->role != 2)
		@if(!Session::has('tql'))
			<div id="strategicprofile" class="tab-pane fade">
				<div class="spacewrapper">
					<div class="row">
						@if($forecast_data != null)
							<script>var forecast_data = {!! json_encode($forecast_data) !!};</script>
							<div class="col-md-12">
								<div class="read-only">
									<h4>
										Growth Forecast
										<a href="#" class="popup-hint popup-hint-header" data-hint="The Growth Forecast graph is meant to present CreditBPO’s own
										forecast for expected industry growth alongside expected company
										growth 1, 2 and 3 years forward. Calculation emphasis was placed
										on keeping the base numbers used for both trendlines raw, and
										pushing multivariable series analysis into the trend line itself.
										<br/><br/>
										The purpose of the Industry Comparison Graph is to compare the Industry
										and the Company using metrics that are calculated from current (as opposed to forecast)
										figures. This is important because the metrics used must be exactly the
										same in terms of both source and derivation. In perspective, it is a
										static measure of how the company of the data-specific period compares
										to the greater industry of the same period.
										<br/><br/>
										Given the aims for both graphs explained above, the figures used differ
										by their very nature. For example, Revenue Growth is used in both graphs but the
										Industry figure in Growth Forecast is very different from the one in
										Industry Comparison. This is because the Industry Comparison figure is
										averaged out over thousands of companies for which we have individual
										figures. The overall Growth Forecast is based on the entire Industry
										indicator which includes companies for which there is no sufficient granular data.">
											{{trans('messages.more_info')}}
										</a>
									</h4>
									<div class="read-only-text">
										<div class="row">
											<div class="col-sm-8">
												<div id="forecast_graph"></div>
											</div>
											<div class="col-sm-4">
												<p style = 'font-size: 14px;'> Growth Forecast {{$forecast_data['description']}} </p>
											</div>
										</div>
										</div>
									</div>
								</div>
							@endif
							<div class="col-md-4">
								<div class="read-only">
									<h4>{{trans('business_details2.bdriver')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.bdriver_hint')}}">{{trans('messages.more_info')}}</a></h4>
									<div class="read-only-text">
										<div class="businessdriver">
										<div class="details-row hidden-xs hidden-sm">
											<div class="col-md-4 col-sm-4">
												<b>{{ trans('business_details2.bdriver_name') }}</b>
											</div>
											<div class="col-md-8 col-sm-8">
												<b>{{ trans('business_details2.bdriver_share') }}</b>
											</div>
										</div>

										@foreach ($bizdrivers as $driver)
											<div class="details-row hidden-xs hidden-sm">
												<div class="col-md-4 col-sm-4">
													<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
												 	{{ $driver->businessdriver_name }}
													</span>
												</div>

												<div class="col-md-8 col-sm-8">
													<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
													{{ $driver->businessdriver_total_sales }}%
												</span>
												</div>
											</div>

										<div class="details-row visible-sm-block visible-xs-block">
											<div class="col-md-4">
												<b>{{ trans('business_details2.bdriver_name') }}</b><br/>
												<span class = 'editable-field' data-name = 'driver.businessdriver_name' data-field-id = '{{ $driver->businessdriverid }}'>
												{{ $driver->businessdriver_name }}
												</span>
											</div>

											<div class="col-md-8">
												<b>{{ trans('business_details2.bdriver_share') }}</b><br />
												<span class = 'editable-field' data-name = 'driver.businessdriver_total_sales' data-field-id = '{{ $driver->businessdriverid }}'>
												{{ $driver->businessdriver_total_sales }}%
												</span>
											</div>

										</div>
										@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="read-only">
									<h4>{{trans('business_details2.customer_segment')}}  <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.cs_hint')}}">{{trans('messages.more_info')}}</a></h4>
								<div class="read-only-text">
									<div class="businesssegment">
										@foreach ($bizsegments as $bizsegment)
										<div class="details-row">
											<div class="col-md-12">
												@if (($entity->loginid == Auth::user()->loginid) && ($entity->status == 0))
													<a href = "{{ URL::To('/sme/businesssegment/'.$bizsegment->businesssegmentid.'/delete') }}" class="navbar-link delete-info" data-reload = '.reload-bizsegment' data-cntr = '#bizsegment-list-cntr'>{{ trans('messages.delete') }}</a>
												@endif
											<span class = 'editable-field' data-name = 'bizsegment.businesssegment_name' data-field-id = '{{ $bizsegment->businesssegmentid }}'>
												{{ $bizsegment->businesssegment_name }}
											</span>
											</div>
										</div>
										@endforeach
									</div>
								</div>
								</div>
							</div>
							<div class="col-md-4">
							<div class="read-only">
								<h4>{{ trans('condition_sustainability.revenue_growth') }} <a href="#" class="popup-hint popup-hint-header" data-hint="{{ Lang::get('condition_sustainability.expected_revenue_3years', array(), 'en') }}<hr/>{{ Lang::get('condition_sustainability.expected_revenue_3years', array(), 'fil') }}">{{trans('messages.more_info')}}</a></h4>
								<div class="read-only-text">
									@foreach ($revenuepotential as $revenuepotentialinfo)
									<span class="details-row">
										<div class="col-md-8">{{ trans('condition_sustainability.under_current_market') }}</div>
										<div class="col-md-4">
											@if ($revenuepotentialinfo->current_market == 1)
											0%
											@elseif ($revenuepotentialinfo->current_market == 2)
											{{ trans('condition_sustainability.less_3_percent') }}
											@elseif ($revenuepotentialinfo->current_market == 3)
											{{ trans('condition_sustainability.between_3to5_percent') }}
											@else
											{{ trans('condition_sustainability.greater_5_percent') }}
											@endif
										</div>
									</span>
									<span class="details-row">
										<div class="col-md-8">Expected Future Market Conditions</div>
										<div class="col-md-4">
											@if ($revenuepotentialinfo->new_market == 1)
											0%
											@elseif ($revenuepotentialinfo->new_market == 2)
											{{ trans('condition_sustainability.less_3_percent') }}
											@elseif ($revenuepotentialinfo->new_market == 3)
											{{ trans('condition_sustainability.between_3to5_percent') }}
											@else
											{{ trans('condition_sustainability.greater_5_percent') }}
											@endif
										</div>
									</span>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-7">

							<div class="read-only">
								<h4>{{trans('business_details2.past_proj_completed')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.ppc_hint')}}">{{trans('messages.more_info')}}</a></h4>
								<div class="read-only-text">
									<div class="pastprojectscompleted">
										<div class="details-row hidden-sm hidden-xs">
											<div class="col-md-3 col-sm-4">
												<b>{{ trans('business_details2.ppc_purpose') }}</b>
											</div>

											<div class="col-md-2 col-sm-2">
												<b>{{ trans('business_details2.ppc_cost_short') }}</b>
											</div>

											<div class="col-md-3 col-sm-2">
												<b>{{ trans('business_details2.ppc_source_short') }}</b>
											</div>

											<div class="col-md-2 col-sm-1">
												<b>{{ trans('business_details2.ppc_year_short') }}</b>
											</div>

											<div class="col-md-2 col-sm-3">
												<b>{{ trans('business_details2.ppc_result_short') }}</b>
											</div>
										</div>

										@foreach ($pastprojects as $past_proj)
											<div class="details-row hidden-sm hidden-xs">
												<div class="col-md-3 col-sm-4">
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ $past_proj->projectcompleted_name }}
													</span>
												</div>

												<div class="col-md-2 col-sm-2">
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ number_format($past_proj->projectcompleted_cost, 2) }}
													</span>
												</div>

												<div class="col-md-3 col-sm-2">
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
													</span>
												</div>

												<div class="col-md-2 col-sm-1">
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ $past_proj->projectcompleted_year_began }}
													</span>
												</div>

												<div class="col-md-2 col-sm-3">
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
													</span>
												</div>
											</div>

											<div class="details-row visible-sm-block visible-xs-block">
												<div class="col-md-5">
													<b>{{ trans('business_details2.ppc_purpose') }}</b><br/>
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_name' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ $past_proj->projectcompleted_name }}
													</span>
												</div>

												<div class="col-md-1">
													<b>{{ trans('business_details2.ppc_cost') }}</b><br/>
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_cost' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ number_format($past_proj->projectcompleted_cost, 2) }}
													</span>
												</div>

												<div class="col-md-2">
													<b>{{ trans('business_details2.ppc_source') }}</b><br/>
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_source_funding' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ trans('business_details2.'.$past_proj->projectcompleted_source_funding) }}
													</span>
												</div>

												<div class="col-md-1">
													<b>{{ trans('business_details2.ppc_year') }}</b><br/>
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_year_began' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ $past_proj->projectcompleted_year_began }}
													</span>
												</div>

												<div class="col-md-3">
													<b>{{ trans('business_details2.ppc_result') }}</b><br/>
													<span class = 'editable-field' data-name = 'ppc.projectcompleted_result' data-field-id = '{{ $past_proj->projectcompletedid }}'>
														{{ trans('business_details2.'.$past_proj->projectcompleted_result) }}
													</span>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>

							<div class="read-only">
								<h4>{{trans('business_details2.future_growth_initiatives')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('business_details2.fgi_hint')}}">{{trans('messages.more_info')}}</a></h4>
								<div class="read-only-text">
									<div class="futuregrowthinitiative">
									<div class="details-row hidden-sm hidden-xs">
										<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_purpose') }}</b></div>
										<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_cost') }}</b></div>
										<div class="col-md-4 col-sm-4"><b>{{ trans('business_details2.fgi_proj_goal') }}</b></div>
									</div>

									@foreach ($futuregrowths as $future_init)
									<div class="details-row hidden-sm hidden-xs">
										<div class="col-md-4 col-sm-4">
										<span class = 'editable-field' data-name = 'fgi.futuregrowth_name' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ $future_init->futuregrowth_name }}
										</span>
									</div>

									<div class="col-md-4 col-sm-4">
										<span class = 'editable-field' data-name = 'fgi.futuregrowth_estimated_cost' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
										</span>
									</div>

									<div class="col-md-4 col-sm-4">
										<span class = 'editable-field' data-name = 'fgi.planned_proj_goal' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ trans('business_details2.'.$future_init->planned_proj_goal) }}
										</span>
										by
										<span class = 'editable-field' data-name = 'fgi.planned_goal_increase' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{ $future_init->planned_goal_increase }}%
										</span>
										beginning
										<span class = 'editable-field' data-name = 'fgi.proj_benefit_date' data-field-id = '{{ $future_init->futuregrowthid }}'>
											{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
										</span>
										<style id = 'dp-style'></style>
									</div>
								</div>

								<div class="details-row visible-sm-block visible-xs-block">
									<div class="col-md-5">
										<b>{{ trans('business_details2.fgi_purpose') }}</b><br/>
										{{ $future_init->futuregrowth_name }}
									</div>

									<div class="col-md-2">
										<b>{{ trans('business_details2.fgi_cost') }}</b><br/>
										{{ number_format($future_init->futuregrowth_estimated_cost, 2) }}
									</div>

									<div class="col-md-2">
										<b>{{ trans('business_details2.fgi_date') }}</b><br/>
										{{ date("F d, Y", strtotime($future_init->futuregrowth_implementation_date)) }}
									</div>

									<div class="col-md-3">
										<b>{{ trans('business_details2.fgi_source') }}</b><br/>
										{{ trans('business_details2.'.$future_init->futuregrowth_source_capital) }}
									</div>

									<div class="col-md-3 col-sm-3">
										{{ trans('business_details2.'.$future_init->planned_proj_goal) }} by
										{{ $future_init->planned_goal_increase }}% beginning
										{{  date("F Y", strtotime($future_init->proj_benefit_date))  }}
									</div>

								</div>
							@endforeach
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-5">
					<div class="read-only">
						<h4>{{trans('rcl.rcl')}} <a href="#" class="popup-hint popup-hint-header" data-hint="{{trans('rcl.rcl_hint')}}">{{trans('messages.more_info')}}</a></h4>
						<div class="read-only-text">
							@foreach ($planfacilityrequested as $planfacilityinfo)
								<p><b>{{trans('rcl.requested_cf')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.rcl_hint')}}">{{trans('messages.more_info')}}</a></p>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.amount_of_line')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->plan_credit_availment }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.purpose_of_cf')}}</div>
									<div class="col-md-8">
										@if ($planfacilityinfo->purpose_credit_facility == 1)
											{{trans('rcl.working_capital')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 2)
											{{trans('rcl.expansion')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 3)
											{{trans('rcl.takeout')}}
										@elseif ($planfacilityinfo->purpose_credit_facility == 5)
											{{trans('rcl.supplier_credit')}}
										@else
											{{trans('rcl.other_specify')}}
										@endif
									</div>
								</span>
								@if (4 == $planfacilityinfo->purpose_credit_facility)
									<span class="details-row">
										<div class="col-md-4">{{trans('rcl.purpose_of_cf_others')}}</div>
										<div class="col-md-8">{{ $planfacilityinfo->purpose_credit_facility_others }}</div>
									</span>
								@endif
								@if (5 == $planfacilityinfo->purpose_credit_facility)
									<span class="details-row">
										<div class="col-md-4">{{trans('rcl.credit_terms')}}</div>
										<div class="col-md-8">{{ $planfacilityinfo->credit_terms }}</div>
									</span>
								@endif
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.other_remarks')}}</div>
									<div class="col-md-8">{{ nl2br($planfacilityinfo->other_remarks) }}</div>
								</span>
								<p><b>{{trans('rcl.offered_collateral')}}</b> <a href="#" class="popup-hint popup-hint-content" data-hint="{{trans('rcl.offered_collateral_hint')}}">{{trans('messages.more_info')}}</a></p>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.type_of_deposit')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->cash_deposit_details }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.deposit_amount')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->cash_deposit_amount }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.marketable_securities')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->securities_details }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.marketable_securities_value')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->securities_estimated_value }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.property')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->property_details }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.property_value')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->property_estimated_value }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.chattel')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->chattel_details }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.chattel_value')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->chattel_estimated_value }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.others')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->others_details }}</div>
								</span>
								<span class="details-row">
									<div class="col-md-4">{{trans('rcl.others_value')}}</div>
									<div class="col-md-8">{{ $planfacilityinfo->others_estimated_value }}</div>
								</span>
							@endforeach
							</div>
						</div>
					</div>
				</div>

				<input type="button" id="strategicprofile_prev" value="{{trans('messages.previous')}}" />
				<input type="button" id="strategicprofile_next" value="{{trans('messages.next')}}" />

			</div>
		</div>
	@endif
@endif
@endif

@if ((Auth::user()->role==2) || ((USER_ROLE_BANK == Auth::user()->role) && ($bank_ci_view == 1)) )
	<div id="investigationsummary" class="tab-pane fade">
		<div class="spacewrapper">
			<h3>Summary of Incorrect Fields</h3>
			<div class="investigator-summary"></div>
			<div class="read-only-text">
				{{Form::open(['route' => ['postSaveInvestigation'], 'method' => 'post', 'id' => 'iv_form'])}}
					<div class="form-group">
					<label>Notes:</label>
					<input type="hidden" name="entity_id" value="{{$entity->entityid}}"/>
					@if (0 == $bank_ci_view)
						<textarea name="notes" class="form-control" rows="9">{{($investigator_notes!=null) ? $investigator_notes->notes : ''}}</textarea>
					@else
						<p> {{($investigator_notes!=null) ? $investigator_notes->notes : 'None'}}</p>
					@endif
					</div>
					<h5>Required Documents <small>Select the required documents needed from Company on the list below</small></h5>
					<div class="form-group investigator-docs">
					<label><input type="checkbox" name="required_docs[]" value="1" {{ ($investigator_notes!=null && in_array(1, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> SEC: Certificate of Registration</label>
					<label><input type="checkbox" name="required_docs[]" value="2" {{ ($investigator_notes!=null && in_array(2, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> SEC: Articles</label>
					<label><input type="checkbox" name="required_docs[]" value="3" {{ ($investigator_notes!=null && in_array(3, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> SEC: By-laws</label>
					<label><input type="checkbox" name="required_docs[]" value="4" {{ ($investigator_notes!=null && in_array(4, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> SEC: General Information Sheet</label>
					<label><input type="checkbox" name="required_docs[]" value="11" {{ ($investigator_notes!=null && in_array(11, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> DTI: Registration of Business Name</label>
					<label><input type="checkbox" name="required_docs[]" value="12" {{ ($investigator_notes!=null && in_array(12, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Authority to borrow</label>
					<label><input type="checkbox" name="required_docs[]" value="13" {{ ($investigator_notes!=null && in_array(13, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Authorized Signatory </label>
					<label><input type="checkbox" name="required_docs[]" value="14" {{ ($investigator_notes!=null && in_array(14, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Bank Statements</label>
					<label><input type="checkbox" name="required_docs[]" value="15" {{ ($investigator_notes!=null && in_array(15, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Utility Bills</label>
					<label><input type="checkbox" name="required_docs[]" value="16" {{ ($investigator_notes!=null && in_array(16, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> BIR: Income Tax Return </label>
					<label><input type="checkbox" name="required_docs[]" value="5" {{ ($investigator_notes!=null && in_array(5, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> BIR: Certificate of Registration documents</label>
					<label><input type="checkbox" name="required_docs[]" value="6" {{ ($investigator_notes!=null && in_array(6, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Updated Mayor's permit</label>
					<label><input type="checkbox" name="required_docs[]" value="7" {{ ($investigator_notes!=null && in_array(7, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Certifications, Licenses</label>
					<label><input type="checkbox" name="required_docs[]" value="8" {{ ($investigator_notes!=null && in_array(8, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Transfer Certificates of Title (for real property)</label>
					<label><input type="checkbox" name="required_docs[]" value="9" {{ ($investigator_notes!=null && in_array(9, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Chattel ownership documents</label>
					<label><input type="checkbox" name="required_docs[]" value="10" {{ ($investigator_notes!=null && in_array(10, $investigator_notes->required_docs)) ? "checked='checked'" : "" }} /> Others <input type="text" name="investigator_others_docs" value="{{($investigator_notes!=null) ? $investigator_notes->others_docs : ''}}" /></label>
					</div>
					@if (0 == $bank_ci_view)
						<input type="hidden" id="iv_action" name="iv_action" value="submit_investigation" />
						<button type="button" id="iv_send_to_email" value="true" class="btn btn-primary">Send Email to Company</button>
						<button type="button" id="iv_submit_investigation" value="true" class="btn btn-primary">Submit Investigation</button>
					@endif
				{{Form::close()}}
			</div>
		</div>
	</div>
@endif
@if((Auth::user()->role==1 || Auth::user()->role==2 || 1 == $bank_ci_view) && @count($requested_files)>0)
	<div id="requested_files" class="tab-pane fade">
		<div class="spacewrapper">

			<h3>Requested Documents</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<ul>
						@if(in_array(1, $requested_files))<li>SEC: Certificate of Registration</li>@endif
						@if(in_array(2, $requested_files))<li>SEC: Articles</li>@endif
						@if(in_array(3, $requested_files))<li>SEC: By-laws</li>@endif
						@if(in_array(4, $requested_files))<li>SEC: General Information Sheet</li>@endif
						@if(in_array(11, $requested_files))<li>DTI: Registration of Business Name</li>@endif
						@if(in_array(12, $requested_files))<li>Authority to borrow</li>@endif
						@if(in_array(13, $requested_files))<li>Authorized Signatory</li>@endif
						@if(in_array(14, $requested_files))<li>Bank Statements</li>@endif
						@if(in_array(15, $requested_files))<li>Utility Bills</li>@endif
						@if(in_array(16, $requested_files))<li>BIR: Income Tax Return</li>@endif
						@if(in_array(5, $requested_files))<li>BIR: Certificate of Registration documents</li>@endif
						@if(in_array(6, $requested_files))<li>Updated Mayor's permit</li>@endif
						@if(in_array(7, $requested_files))<li>Certifications, Licenses</li>@endif
						@if(in_array(8, $requested_files))<li>Transfer Certificates of Title (for real property)</li>@endif
						@if(in_array(9, $requested_files))<li>Chattel ownership documents</li>@endif
						@if(in_array(10, $requested_files))<li>{{($investigator_notes!=null) ? $investigator_notes->others_docs : ''}}</li>@endif
					</ul>
					@if(Auth::user()->role==1)
						<a href="{{ URL::to('/') }}/sme/upload_requested_documents">Click here to upload documents</a>
					@endif
				</div>
			</div>


			@if (@count($documents4) != 0)
				<h3>Uploaded Documents</h3>
				<div class="panel panel-default">
					<div class="panel-body">
						@foreach ($documents4 as $documents4)
							<div class="col-md-12">
				  				<a href="{{ URL::to('/') }}/download/cidocuments/{{ $documents4->document_rename }}" target="_blank">{{ $documents4->document_orig }}</a>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>
	</div>
@endif

@if(Session::has('tql'))
	<p align="center">
		@if(Session::has('tql_owner'))
			<a href="{{URL::to('/')}}/save-external-link" class="btn btn-primary">Back</a>
		@else
			<a href="{{URL::to('/')}}/save-external-link" class="btn btn-primary">Save and Exit</a>
		@endif
	</p>
@endif
</div>
</div>
</div>
</div>

<div id="modal_investigator_item" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<input type="hidden" class="group" />
	<input type="hidden" class="item" />
	<input type="hidden" class="group_name" />
	<input type="hidden" class="label" />
	<input type="hidden" class="value" />
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Notes:</h4>
			</div>
			<div class="modal-body">
				<div id = 'ci_err_msg' style = 'display:none' class = 'alert alert-danger'> </div>
					<form id="modal_investigator_item_form" method="post" enctype="multipart/form-data">
						<textarea class="notes form-control" rows="3"></textarea>
						<br>
						<input type="file" name="support_docs[]" />
						<input type="file" name="support_docs[]" />
					  <input type="file" name="support_docs[]" />
					</form>
				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close-ci-modal" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary investigator-modal-save-btn">Save</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id = 'modal-next-tab-msg' role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">{{trans('messages.remember')}}</h4>
			</div>
			<div class="modal-body" style="text-align: justify; text-justify: inter-word;" >
				<p id = 'tab-msg-warning' class = 'errormessage'> </p>
				<p>
				{{ trans('messages.tab_reminder1') }}
				<br/><br/>
				{{ trans('messages.tab_reminder2') }}
				{{ Form::checkbox('confirm-tab-msg', 'confirm_tab_msg', false) }}
				{{ trans('messages.tab_reminder3') }}
				<br/><br/>
				{{ trans('messages.tab_reminder4') }}
				<br/><br/>
				{{ trans('messages.tab_reminder5') }}
				</p>

				<hr/>
				<p class = 'not-filled-list'> </p>
				</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-primary" id = 'modal-next-tab-review'>{{trans('messages.review_it')}}</button>
				<button type="button" class="btn btn-default" id = 'modal-next-tab-continue'>{{trans('messages.continue')}}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id = 'rate-tab-query-modal' role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">{{trans('messages.rate_this_tab')}}</h4>
			</div>
			<div class="modal-body">
				<p>{{trans('messages.body_tab')}}?</p>
				</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-default" id = 'rate-tab-query-rate'>{{trans('messages.rate_tab')}}</button>
				<button type="button" class="btn btn-primary" id = 'rate-tab-query-no-thanks'>{{trans('messages.no_thanks')}}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="rating-tab-modal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">{{trans('messages.feedback')}}</h4>
			</div>
			<div class="modal-body" id = 'rating-tab-body'>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id = 'submit-feedback-tab'>{{trans('messages.submit_rating')}}</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="private-link-modal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">{{trans('messages.private_link')}}</h4>
			</div>
			<div class="modal-body" id = 'rating-tab-body'>
				<p style="word-wrap: break-word;"><strong>URL:</strong> {{$private_link}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="dev-ref-codes" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">{{trans('messages.api_request_code')}}</h4>
			</div>
			<div class="modal-body" id = 'rating-tab-body'>
				<ol>
					<li>
						<p style="word-wrap: break-word;">
							<b>{{trans('messages.m_request1')}}</b><br/>
							<b>URL: </b>{{ URL::to('/cbpo_pub_rest/get_rating_result/') }}<br/>
							<b>Key Code: </b>{{ base64_encode(base64_encode($entity->entityid.'$encode')) }}
						</p>
					</li>

					<li>
						<p style="word-wrap: break-word;">
							<b>{{trans('messages.m_request2')}}</b><br/>
							<b> URL: </b>{{ URL::To('/cbpo_pub_rest/get_company_rating') }}
						</p>
					</li>

					<li>
						<p style="word-wrap: break-word;">
							<b>{{trans('messages.m_request3')}}</b> <br/>
							<b>URL: </b>{{ URL::to('/cbpo_pub_rest/get_account_rating/') }}<br/>
							<b>Key Code: </b>{{ base64_encode(base64_encode($entity->loginid.'$account')) }}
						</p>
					</li>
				</ol>
			</div>  
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="sumpopupmodal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><span class="icon-more-info glyphicon glyphicon-exclamation-sign" aria-hidden="true" style="color: #2a99c6;"></span> {{trans('messages.more_info')}}</h4>
			</div>
			<div class="modal-body">
				<div style="padding: 10px;">
					<p class="sumpopupmodalcontent"></p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="iv_confirm_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				 	<h4 class="modal-title"><span class="icon-more-info glyphicon glyphicon-exclamation-sign" aria-hidden="true" style="color: #2a99c6;"></span> Confirm sending email...</h4>
				</div>
				<div class="modal-body">
					<div style="padding: 10px;">
					<p>You did not select any Required Documents.  Are you sure you want to send email?</p>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="iv_confirm">Send</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="invalid_utility_bill_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><span class="icon-more-info glyphicon glyphicon-exclamation-sign" aria-hidden="true" style="color: #2a99c6;"></span> Unrecognized Utility Bill</h4>
					</div>
				<div class="modal-body">
					<div style="padding: 10px;">
					<p>The system is unable to recognize the the following utility bill/s:</p>
					<p class="bills"></p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="utility_bill_modal_ok" class="btn btn-default" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<div id="invalid_bank_statement_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><span class="icon-more-info glyphicon glyphicon-exclamation-sign" aria-hidden="true" style="color: #2a99c6;"></span> Unrecognized Bank Statement</h4>
				</div>
				<div class="modal-body">
					<div style="padding: 10px;">
						<p>Your uploaded Bank Statement does not fit the common format. This may be due to image quality or processor error. Would you like to upload another image or keep the current one?</p>
						<p class="bills"></p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="bank_statement_modal_ok" class="btn btn-default" data-dismiss="modal">Keep current image</button>
					<button type="button" id="bank_statement_modal_reupload" class="btn btn-default">Upload new image</button>
				</div>
				</div>
			</div>
		</div>

	@include('user-2fa-form')
	@include('modal-add-info')
	@include('modal-upload-file')
	@include('modal-refresh-warning')
	@if(Auth::user()->role == 1 && $entity->status == 0)
	@include('modal-external-link')
	@include('modal-external-link-notification')
@endif
@include('modal-confirm-sec-override')

<div class="modal" id="fs-input-template-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class = "glyphicon glyphicon-exclamation-sign"> </i> Financial Statement Input Template </h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                <p>Hello! Thank you for being a part of our offerings so far! 
It seems that you did not upload an FS Input Template. We can do the transcription process but it usually takes 24-48 hours.<br/>
Is this alright with you?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary btn-submit-col" href="javascript:void(0)" id="submit_financial_analysis_btn">Yes, this is fine</a>
                <a class="btn btn-primary btn-submit-col2" href="{{ url('summary/'.$entity->entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname).'?showfs=1#fs-template-cntr') }}">No, I'd like to upload my FS Input Template</a>

                @php
					if(!empty($_GET['showfs'])){
						echo '<input type="hidden" id="showfs" value="1">';
					}else{
						echo '<input type="hidden" id="showfs" value="0">';
					}
				@endphp
            </div>
        </div>
    </div>
</div>

<div class="modal" id="dscr-modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id = "dscr-body">
                
            </div>
            <!-- <div class="modal-footer">
                
            </div> -->
        </div>
    </div>
</div>

<input type="hidden" name="matching_optin" id="matching_optin" value="{{ $entity->matching_optin }}" />
<input type="hidden" name="status" id="status" value="{{ $entity->status }}" />
<div class="modal" id="report-matching-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class = "glyphicon glyphicon-exclamation-sign"> </i> Notice </h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                <p>Would you like to opt in to Report Matching Service?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="{{ url('report_matching/optin/Yes/'.$entity->entityid) }}">Yes</a>
                <a class="btn btn-primary" href="{{ url('report_matching/optin/No/'.$entity->entityid) }}">No</a>
            </div>
        </div>
    </div>
</div>
@if(session()->has('success'))
<input type="hidden" name="success" id="success" value="{{ session()->get('success') }}">
<div class="modal" id="matching-success-modal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class = "glyphicon glyphicon-exclamation-sign"> </i> Success Notice </h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                
			    <div class="alert alert-success">
			        {{ session()->get('success') }}
			    </div>
				
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
@endif



@stop

@section('javascript')
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/busidentification.js') }}"></script>
<script src="{{ URL::asset('js/creditfacilities.js') }}"></script>
<script src="{{ URL::asset('js/smesummary.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">

	if($('.showfs').val() == 1){
		$('#side-menu li').removeClass('active');
		$('#side-menu li a').removeClass('active');
		$('#nav_registration1').addClass('active');
		$('#nav_registration1').parent().addClass('active');
		$('#registration1').show();
		$('#registration2').hide();
		$('#registration3').hide();
		$('#pfs').hide();
		$('#sci').hide();
	}

	// if($('#matching_optin').val() == 'Pending' && ($('#status').val() == 5 || $('#status').val() == 7) && $('#role').val() != 4){
		
	// 	$('#report-matching-modal').modal('show');
	// }

	// if($('#success').val()){
	// 	$('#matching-success-modal').modal('show');
	// }

	// $("#matching").change(function() {

	// 	var matching = 'No';
	// 	if(this.checked){
	// 		var matching = 'Yes';
	// 	}
	     
	//     window.location = "{{ URL::to('report_matching/optin/') }}/" + matching + '/' + $('#entityid').val();

	// });

	$('#credit_risk_rating_submit').click(function(){
		$.ajax({
			url: "{{ URL::to('summary/check_fsinput/'.$entity->entityid) }}",
			dataType: "text",
			success: function(response){
				
				if(response == 'false'){
					$('#fs-input-template-modal').modal('show');
				}else{
					$('#submit_financial_analysis_form').submit();
				}
			}
		});
	});

	$('#submit_financial_analysis_btn').click(function(){
		$('#submit_financial_analysis_form').submit();
	});
</script>

<script>

	if ($('.positive_points li').length == 0){
		$('.positive_points_parent').hide();
		$('.negative_points_parent').removeClass('col-md-6');
		$('.negative_points_parent').removeClass('col-md-6');
	}

	if ($('.negative_points li').length == 0){
		$('.negative_points_parent').hide();
		$('.positive_points_parent').removeClass('col-md-6');
		$('.positive_points_parent').removeClass('col-md-6');
	}

    var curSub = null, curRow = null;
		$(document).ready(function() {
			$('.showMessage').show();
			setTimeout(function() {
				$('.showMessage').fadeOut();
			}, 3000);
		});

		$(function () {
		$('[data-toggle="tooltip"]').tooltip()
		});
	</script>
	<script>
		$(document).on("click", ".delete-file", function () {
     var fileName = $(this).data('id');
     $(".modal-body #fileName").text( fileName );
     $(".modal-body #filenameHidden").val( fileName );
		});
	</script>
	<script>
		$(document).on("click", ".share-link", function () {
     var link = $(this).data('id');
     $(".modal-body #shareLink").text(link);
     $(".modal-body #shareLink").attr("href", link);
		});
	</script>
	<script>
		$(document).ready(function(){
      $('#copyButton').click(function(){

        var text = $("#shareLink").get(0)
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
        //add to clipboard.
        document.execCommand('copy');
        
        $('.copied').removeClass('hidden');
        $('.copied').show();
        setTimeout(function() {
          $('.copied').fadeOut();
        }, 3000);
      })

      $('.toShareModal').click(function(){
        $(".navbar").addClass('zindex');
      });

      $("#shareLinkModal").on("hidden.bs.modal", function () {
      $('.navbar').removeClass('zindex');
      });

		});
</script>

<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/index.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>

@if($entity->status == 7)
<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/dscrexport.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>
@endif

@if(Session::has('redirect_tab'))
<script type="text/javascript">
  $(document).ready(function(){
   ClickTab("{{Session::get('redirect_tab')}}");
 });
</script>
@endif
@if(Auth::user()->role == 1)
<script src="{{ URL::asset('js/smesummary_progressbar.js') }}"></script>
@endif
@if(Auth::user()->role == 2 || Auth::user()->role == 3)
<script src="{{ URL::asset('js/smesummary_tab_indicator.js') }}"></script>
@endif
@if ((Auth::user()->role == 1) && ($entity->status == 0))
<script type="text/javascript" src="{{ URL::asset('js/cmn-lib/cmn-upd-info.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz1.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-biz2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-ecf.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-owner.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-bcs.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/upd-rcl.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/summary-ccc.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/toggle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/external-link.js') }}"></script>
@endif
<script src="{{ URL::asset('js/smesummary_investigator.js') }}"></script>
<script src="{{ URL::asset('js/chart-min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.printarea.js') }}"></script>
<script src="{{ URL::asset('js/graphs-financial-analysis.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison2.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison3.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison4.js') }}"></script>
<script src="{{ URL::asset('js/graphs-industry-comparison5.js') }}"></script>
<script src="{{ URL::asset('js/graphs-business-condition.js') }}"></script>
<script src="{{ URL::asset('js/graphs-maganement-quality.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>
<script src="{{ URL::asset('js/graphs-strategic-forecast.js') }}"></script>
<script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
<script src="{{ URL::asset('js/next-tab-msg.js') }}"></script>
<script src="{{ URL::asset('js/jquery.form.min.js') }}"></script>
<!-- <script src="{{ URL::asset('js/jquery.js') }}"></script> -->
<script src="{{ URL::asset('js/busidentification.js') }}"></script>
<script src="{{ URL::asset('js/creditfacilities.js') }}"></script>
<script src="{{ URL::asset('js/smesummary.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
<!-- <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->

<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/index.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>

@if($entity->status == 7)
<script type="text/javascript">
    $(document).ready(function() {
      require(['/static/js/modules/smesummary/dscrexport.js'], function(smesummary) {
          smesummary.initialize({{ $entity->entityid }}, {{ $is_financing }});
      });
    });
</script>
@endif

@if(Auth::user()->role == 4 || Auth::user()->role == 0)
<script>
    $(document).ready(function(){
        /**Check empty deliverables */
        if( ({{$deliverables->financial_rating}} == 0) && ({{$deliverables->growth_forecast}} == 0) && ({{$deliverables->industry_forecasting}} == 0)  ){
            // $('#financial_condition_chart').show();
            $('#growth_forecasting_chart').show();
            $('#industry_composition_chart').show();
        }else{
            // if( {{$deliverables->financial_rating}} == 1 ){
            //     $('#financial_condition_chart').show();
            // }
            if( {{$deliverables->growth_forecast}} == 1 ){
                $('#growth_forecasting_chart').show();
            }
            if( {{$deliverables->industry_forecasting}} == 1 ){
                $('#industry_composition_chart').show();
            }
        }

    });
</script>
@endif

@if(Session::has('redirect_tab'))
<script type="text/javascript">
  $(document).ready(function(){
   ClickTab("{{Session::get('redirect_tab')}}");
 });
</script>
@endif
@if(Auth::user()->role == 1)
<script src="{{ URL::asset('js/smesummary_progressbar.js') }}"></script>
@endif
@if(Auth::user()->role == 2 || Auth::user()->role == 3)
<script src="{{ URL::asset('js/smesummary_tab_indicator.js') }}"></script>
@endif
@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag==0)
<script src="{{ URL::asset('js/tutorial.js') }}"></script>
<script type="text/javascript">
 $(window).load(function(){
  Tutorial.init('step3');
});
</script>
@endif
@if (1 == $bank_ci_view)
<script type="text/javascript">
  $(document).ready(function(){
    $('.investigator-docs input[type="checkbox"').each(function(){
     $(this).prop('disabled', true);
   });

    $('textarea[name="notes"]').prop('disabled', true);
    $('input[name="investigator_others_docs"]').prop('disabled', true);
  });
</script>
@endif

<input type="hidden" id="export_dscr" name="export_dscr" value="{{ $export_dscr }}" />
<script type="text/javascript">

	var checkExist = setInterval(function() {
	if ($('#export-dscr-btn').length) {
		
			setTimeout(function() {

				if({{ $export_dscr }} == 1){

					$('#export-dscr-btn').prop('disabled', false);
					$('#export-dscr-btn').text("Unexport to Rating Summary");
					$('#export-dscr-btn').attr('class', 'btn btn-warning');

				
				}else{
					$('#export-dscr-btn').prop('disabled', false);
					$('#export-dscr-btn').text("Export to Rating Summary");
				}

		}, 3000);
	}
	}, 1000); 

</script>
@stop
@section('style')
@if(Auth::user()->role==1 AND Auth::user()->tutorial_flag!=2)
<link rel="stylesheet" href="{{ URL::asset('css/tutorial.css') }}" media="screen"/>
@endif
@if(Session::has('tql'))
<link rel="stylesheet" href="{{ URL::asset('css/external-link.css') }}" media="screen"/>
@endif
<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="print"/>
<link rel="stylesheet" href="{{ URL::asset('css/debt-service-calculator.css') }}" media="print"/>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"/>

<style>
  .centered-modal.in {
    display: flex !important;
}
.centered-modal .modal-dialog {
    margin: auto;
}
.modal-body{
  word-wrap: break-word;
}
.zindex {
  z-index:500 !important;
}
</style>
@endif
@stop
