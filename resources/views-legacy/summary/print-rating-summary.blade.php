<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>

<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

<style>
    @page { margin: 100 25px; }
    body { margin: 14pt; font-size: 13px; color: #000000; font-family: Arial, Helvetica, sans-serif; }
    h1 { font-size: 20pt; text-align: center; }
    h2 { font-size: 17pt; text-align: center; }
    h3 { font-size: 14pt; text-align: center; }
    table { width: 100%; border-collapse: collapse; border-top: 1px solid #000; border-left: 1px solid #000;}
    table td { border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 2px 5px; }
    table th { border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 2px 5px; }
    .center { text-align: center; }
    .green { color: GREEN; }
    .red { color: RED; }
    .black { color: #000; }


    #graphs0value00000 {
        font-size: 20px !important;
    }
    
    .header { position: fixed; top: -80px; left: 20px; right: 15px; bottom: 10px }

    .annexA>td,th {
        width: 20%;
    }

    .avoidPageBreak {
        page-break-inside: avoid;
    }
    
    .mainLocation {
        margin-bottom: 25px;
    }

    .mainLocation table {
        width: 100%;
    }

    .mainLocation tr td:first-child {
        width: 75%;
        white-space: normal;
    }

    .negativeFindings {
        padding-top: 5px;
    }
    
    .negativeFindings table {
        white-space: normal!important;
    }
    
    .negativeFindings td, th{
        word-wrap: break-word;
    }

    .cmapSearch tr td:nth-child(1) {
        width: 30%;
    }

    .cmapSearch tr td:nth-child(2) {
        width: 30%;
    }

    .keyPerson td:nth-child(1) {
        text-align: center;
    }

    .keyPerson tr td:nth-child(2) {
        width: 80%;
        white-space: normal;
    }

    .grid-container {
        display: grid;
        grid-template-columns: auto auto;
        background-color: #2196F3;
        padding: 10px;
}

    .grid-item-label {
        text-align: center;
    }

    .grid-item {
        background-color: rgba(255, 255, 255, 0.8);
        border: 1px solid rgba(0, 0, 0, 0.8);
        padding: 20px;
        font-size: 30px;
        text-align: center;
     }

    /* .grid-item {
        text-align: left;
    } */

    .circle-container {
        border: 10px solid red;
        border-radius: 62px;
        color: red;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .gcircle-container {
        border: 10px solid green;
        border-radius: 62px;
        color: green;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .ocircle-container {
        border: 10px solid orange;
        border-radius: 62px;
        color: orange;
        height: 80px;
        line-height: 0;
        text-align: center;
        vertical-align: middle;
        width: 130px;
    }

    .hundred-percent {
        width: 100%;
        display: block;
        margin: 20px
    }

    .seventy-percent {
        width: 60%;
        display: inline-block;
        margin: 20px;
    }

    .fifty-percent {
        width: 45%;
        display: inline-block;
        margin: 10px;
    }

    .thirty-percent {
        width: 20%;
        display: inline-block;
        margin: 20px;
    }

    .break-page {
        page-break-after: always;
    }

    .last-break-page {
        page-break-inside: avoid !important;
        float: none !important;
    }

    .pdf-hdr-logo {
        width: auto;
        height: 60px;
    }

    table td.tb_hdr {
        background-color: #055688;
        font-weight: bold;
        color: #fff;
        padding: 5px;
    }

    .ind-legend {
        text-align: center;
        margin-bottom: 10px;
    }

    .ind-legend .legend-item {
        display: inline-block;
        width: 70px;
        margin: 0 5px;
    }

    .ind-legend .legend-item .legend-info {
        height: 10px;
        width: 10px;
        background-color: #BDCF8E;
        display: inline-block;
    }

    .ind-legend .legend-item .industry {
        background-color: #F9967C;
    }

    .title3_3 {
        font-size: 20px;
    }
    
    .title3_4 {
        font-size:15px;
    }

    .hideChart {
        display : none;
    }
</style>
</head><body>
   
    <div class="break-page">
        <div>
            <img src="{{ URL::asset('images/creditbpo-logo-big.png') }}" width="100%" style="margin-bottom:5px;" />
        </div>
        <div style="margin-top:60px;font-size:18px; text-align: center;"> Rating Summary and Strategic Profile for {{ $entity->companyname }}</div>
        <div><hr/></div>
        <div style="text-align: center; font-size: 16px;">
            Report Generated: {{ $date_created }}
        </div>
    </div>
    
    <div class="header">
        <div>
                <img class = 'pdf-hdr-logo' src="{{ URL::asset('images/creditbpo-logo-2.jpg') }}" style="margin-bottom:5px;" />
        </div>
        <div> <hr/> </div>
    </div>

	<div id  = 'rating-summary-main' class = 'container'>
        <div class = 'break-page'>
            <table style="border: 0px; margin-left: -3px;">
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>SEC Company Reg. No.: </b></td>
                    <td style="text-align: left; border: 0px;">{{ $entity->tin_num }}</td>
                </tr>
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>SEC Reg. Date: </b></td>
                    <td style="text-align: left; border: 0px;">@php  echo !empty($sec_reg_date) ? $sec_reg_date:''; @endphp</td>
                </tr>
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>Company TIN: </b></td>
                    <td style="text-align: left; border: 0px;">{{  $entity->company_tin }}</td>
                </tr>
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>Business Address: </b></td>
                    <td style="text-align: left; border: 0px;">{{  $entity->address1 }}; @endphp</td>
                </tr>
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>Telephone Number:</b></td>
                    <td style="text-align: left; border: 0px;">{{ $entity->phone }}</td>
                </tr>
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>Company Email:</b></td>
                    <td style="text-align: left; border: 0px;">{{  $entity->email }}</td>
                </tr>
                @if ($entity->website)
                <tr>
                    <td style="text-align: left; border: 0px;  width: 25%;"><b>Company Website:</b> </td>
                    <td style="text-align: left; border: 0px;">{{  $entity->website }}</td>
                </tr>
                @endif
            </table><br>
          
            
            <span><b>Company Profile: </b></span><br>
            @if($entity->description)
           <div style="margin-left: 31px; text-align: justify;"><span>{{ $entity->description }}</span><br><br></div>
           @else
           <div style="margin-left: 31px; text-align: justify;"><span>{{ "Please describe the company's business." }}</span><br><br></div>
           @endif

            <span><b>Philippine Standard Industrial Classification:</b></span><br>
                <table style="border: 0px; width: 80%; margin-left: 26px;">
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;">Industry Section: </td>
                        <td style="text-align: left; border: 0px;">{{ $industry->main_title }} </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px; width: 25%;"Industry >Industry Division: </td>
                        <td style="text-align: left; border: 0px;">{{ $ind_sub->sub_title  }} </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: 0px;  width: 25%;">Industry Group: </td>
                        <td style="text-align: left; border: 0px;">@php echo !empty($ind_row->class_description) ? $ind_row->class_description:''; @endphp </td>
                    </tr>
                </table><br>

            
                <table style="border: 0px; margin-left: -3px;">
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>Asset Size:</b></td>
                            <td style="text-align: left; border: 0px;">PHP {{ number_format((float)$entity->total_assets, 2, '.', ',') }} (@php echo !empty($assetClassification) ? $assetClassification:''; @endphp)</td>
                        </tr>
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>Years of Managerial Exp.:</b></td>
                            <td style="text-align: left; border: 0px;">{{  $entity->number_year_management_team }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>Employee Size:</b></td>
                            <td style="text-align: left; border: 0px;">{{ $entity->employee_size }}</td>
                        </tr>
                        {{-- @if(!$entity->negative_findings and !$cmapSearch)
                        <tr>
                            <td style="text-align: left; border: 0px;  width: 25%;"><b>Negative Findings:</b></td>
                            <td style="text-align: left; border: 0px;">No Negative Finding Found.</td>
                        </tr>
                        @endif --}}
                </table>
            
                <div class="avoidPageBreak">

                    @if(!empty($majorLocations))
                        @if (count($majorLocations) != 0) 
                        <span><b>Major Market Location: </b></span><br>
                                <ul style="list-style-type:none; margin-top: 2px; margin-left: -9px;">
                                @foreach($majorLocations as $majorLocation)
                                 <li>{{ $majorLocation->citymunDesc }}</li>
                                @endforeach
                                </ul>
                        @endif
                    @endif
                </div>
                @if(!empty($majorLocations))
                    @if(count($customerSegment) != 0)
                    <span><b>Customer Segment:</b></span><br>
                        <ul style="list-style-type:none; margin-top: 2px; margin-left: -9px;">
                        @foreach($customerSegment as $custSeg)
                         <li>{{ $custSeg->businesssegment_name }}</li>
                        @endforeach
                        </ul>
                    @endif
                @endif
        </div>
        @if (!empty($cmapSearch) || $entity->negative_findings)
        <div class="break-page">
            @if(!empty($cmapSearch))
                <div class="negativeFindings">
                    @foreach ($cmapSearch as $tables)
                <div class="avoidPageBreak {{ $tables[0] == "court_cases" ? "cmapSearch" : "" }}">
                        <b>
                            @switch($tables[0])
                                @case("court_cases")
                                Court Cases:
                                @break
                                @case("accounts_referred_to_lawyers")
                                Accounts Referred To Lawyers:
                                @break
                                @case("returned_cheques")
                                Returned Cheques:
                                @break
                                @case("telecom_data")
                                Telecom Data:
                                @break
                                @default
                            @endswitch
                        </b>
                            <table>
                                <thead>
                                    <tr>
                                        @foreach($tables[1] as $key => $th)
                                            <th>{{ $th }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                    @foreach($tables[2] as $cases)
                                    <tr>
                                        @foreach($cases as $key2 => $td)
                                            <td> {{ $td == "&nbsp;" ? '' : $td }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                            </table>
                        </div>
                        <br>
                    @endforeach
                </div>
            @endif
            @if($entity->negative_findings)
                <div class="avoidPageBreak negativeFindings keyPerson">
                    <b>Key Persons:</b>
                    {!! $entity->negative_findings !!}<br><br></div>
            @endif
        </div>
        @endif
        @if ($entity->is_premium == 1)
        @if (($capitalDetails != null) || (count($certifications) != 0) || ( count($insurances) != 0) || (count($competitors) != 0) || (count($relatedCompanies) != 0))
        <div class="break-page">
            @if ($capitalDetails != null) 
            <span><b>Capital Details:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>Authorized Capital (PHP)</th>
                    <th>Issued Capital (PHP)</th>
                    <th>Paid-up Capital (PHP)</th>
                    <th>Ordinary Shares</th>
                    <th>Par Value</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        
                            <td> {{ number_format((float)$capitalDetails->capital_authorized, 2, '.', ',') }} </td>
                            @if ($capitalDetails->capital_issued != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_issued, 2, '.', ',') }}  </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_paid_up != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_paid_up, 2, '.', ',') }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_ordinary_shares != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_ordinary_shares, 2, '.', ',') }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($capitalDetails->capital_par_value != 0)
                            <td> {{ number_format((float)$capitalDetails->capital_par_value, 2, '.', ',') }} </td>
                            
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                </tbody>
                
              </table><br>
            @endif

            @if (count($certifications) != 0) 
            <span><b>Certification / Accreditation / Tax Exemptions:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>Certification Details</th>
                    <th>Registration No.</th>
                    <th>Registration Date</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($certifications as $certification)
                    <tr>
                            <td> {{ $certification->certification_details }} </td>
                            <td> {{ $certification->certification_reg_no }}  </td>
                            <td> {{ $certification->certification_reg_date }} </td>
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($insurances) != 0) 
            <span><b>Insurance Coverage:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>Insurance Type</th>
                    <th>Insured Amount</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($insurances as $insurance)
                    <tr>
                            <td> {{ $insurance->insurance_type }} </td>
                            <td>PHP {{ number_format((float)$insurance->insured_amount, 2, '.', ',') }}  </td>
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($competitors) != 0)
            <span><b>Competitors:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone No.</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($competitors as $competitor)
                    <tr>
                        
                            <td> {{ $competitor->competitor_name }} </td>
                            <td> {{ $competitor->competitor_address }}  </td>
                            <td> {{ $competitor->competitor_phone }} </td>
                            @if ($competitor->competitor_email)
                            <td> {{ $competitor->competitor_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
                
              </table><br>
            @endif

            @if (count($relatedCompanies) != 0)
            <span><b>Affiliates / Subsidiaries:</b> </span>
            <table class="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone No.</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($relatedCompanies as $relatedCompany)
                    <tr>
                        <td> {{ $relatedCompany->related_company_name }} </td>
                        @if($entity->is_premium == 1)
                            <td>{{ $relatedCompany->related_company_address1 }}</td>
                        @else
                            <td> {{ $relatedCompany->related_company_address1 }}, 
                                    {{ $relatedCompany->related_company_cityid }},
                                    {{ $relatedCompany->related_company_province }},
                                    {{ $relatedCompany->related_company_zipcode }}  </td>
                        @endif
                        @if ($relatedCompany->related_company_phone)
                        <td> {{ $relatedCompany->related_company_phone }} </td>
                        @else
                        <td> - </td>
                        @endif
                        @if ($relatedCompany->related_company_email = ' ')
                        <td> - </td>
                        @else
                        <td> {{ $relatedCompany->related_company_email }} </td>
                        @endif
                        
                    </tr>
                    @endforeach
                </tbody>
                
              </table>
            @endif
        </div>
        @endif
        @endif
        
        @if(!empty($is_deliverables) || !empty($deliverables->financial_rating))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->financial_rating))
        <div class = 'hideChart last-break-page'>
        @endif
			@if($bank_standalone==0)
            <div style = 'margin-bottom: 30px;'>
                <div>
                    <div class="title3_3">
                        <div><b>{{trans('risk_rating.credit_bpo_rating')}}</b></div>
                    </div>
                </div>

                <div class = 'fifty-percent'>
                    <div class="{{ $cbpo_rate['rating_class'] }}">
                        <div style="font-size: 30px; position: relative; float:left; top: 30px;">
                            {{ $cbpo_rate['rating_txt'] }}
                        </div>
                    </div>
                </div>

                <div class = 'fifty-percent'>
                    <p>
                        <b>Result: </b>
                        <span style="font-size: 16px; color: {{ $cbpo_rate['rating_color'] }}">
                            {{ $cbpo_rate['rating_label'] }}
                        </span>
                        <br/>
                        <span>
                            {{ $cbpo_rate['rating_desc'] }}
                        </span>
                    </p>
                </div>
            </div>

            <div> <hr/> </div>
			@endif

            <div>
                <div>
                    <div class="title3_3">
                        <div>
                            <b>Financial Condition Rating</b>&nbsp;({{ $readyratiodata->cc_year1 }} - {{ $readyratiodata->cc_year3 }})
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                        <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["fpos_graph"]) }}' style = 'height: auto; width: 630px; vertical-align: middle;'/>
                        <div class = 'center' style = 'position: relative; top: -50px;'>
                            {{ trans('risk_rating.financial_position_rating') }}: <b> {{ $financial['fin_position'] }} </b>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            {{ trans('risk_rating.financial_performance_rating') }}:<b> {{ $financial['fin_performance'] }} </b>
                        </div>
                        <div class = 'center' style = 'position: relative;'>
                            <span> <b>Significance of Rating</b></span><br/>
                            @if ($finalscore->score_facs >= 162)
                                Capacity to meet financial commitments on financial obligations are very strong.
                            @elseif ($finalscore->score_facs >= 145)
                                Capacity to meet financial commitments on financial obligations are very strong.
                            @elseif ($finalscore->score_facs >= 127)
                                Capacity to meet financial obligations is still strong but somewhat more susceptible to the adverse effects of changes in business and economic conditions.
                            @elseif ($finalscore->score_facs >= 110)
                                Exhibits adequate protection parameters. However, adverse economic conditions or changing circumstances are more likely to lead to a weakened capacity to meet debt payments or loan covenants.
                            @elseif ($finalscore->score_facs >= 92)
                                Currently has the capacity to meet financial commitment on loans. However, adverse business, financial, or economic conditions will likely impair the capacity or willingness to meet debt obligations.
                            @elseif ($finalscore->score_facs >= 75)
                                Faces major ongoing uncertainties or exposure to adverse business, financial, or economic conditions which could lead to the obligor's inadequate capacity to meet debt obligations.​
                            @elseif ($finalscore->score_facs >= 57)
                                Currently vulnerable to nonpayment, and is dependent upon favorable business, financial, and economic conditions in order to meet financial obligations. Not likely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscore->score_facs >= 40)
                                High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscore->score_facs >= 22)
                                High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @elseif ($finalscore->score_facs >= 4)
                                High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @else
                                High tendency to nonpayment and may not succeed in meeting financial obligations even with good business conditions. Unlikely to have the capacity to meet its financial obligations in the event of adverse business, financial, or economic conditions.
                            @endif
                        </div>
                    </div>
                </div>

                @if ($bank_standalone == 1)
                    @if ((STR_EMPTY != $finalscore->positive_points) || (STR_EMPTY != $finalscore->negative_points))
                        <?php
                            $pos_exp    = explode('|', $finalscore->positive_points);
                            $neg_exp    = explode('|', $finalscore->negative_points);
                        ?>
                        @if (STR_EMPTY != $finalscore->positive_points)
                            <div class = 'hundred-percent'>
                                <b> The Good </b>
                                <ul>
                                @foreach ($pos_exp as $positive)
                                    <li> {{ $positive }} </li>
                                @endforeach
                                </ul>
                            </div>
                        @endif


                        @if (STR_EMPTY != $finalscore->negative_points)
                            <div class = 'hundred-percent'>
                                <b> The Bad </b>
                                <ul>
                                @foreach ($neg_exp as $negative)
                                    <li> {{ $negative }} </li>
                                @endforeach
                                </ul>
                            </div>
                        @endif
                    @endif
                @endif

                @if($bank_standalone == 0)
                <div class = 'hundred-percent'>
                    <table style="border:1px solid #000;">
                        <tr>
                            <td>&nbsp;</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->cc_year1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->cc_year2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->cc_year3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>{{trans('risk_rating.fc_receivable')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->receivables_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->receivables_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->receivables_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>+ {{trans('risk_rating.fc_inventory')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->inventory_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->inventory_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->inventory_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td>- {{trans('risk_rating.fc_payable')}}</td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->accounts_payable_turnover1 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->accounts_payable_turnover2 : "" }}</span></td>
                            <td style="text-align: center"><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->accounts_payable_turnover3 : "" }}</span></td>
                        </tr>
                        <tr>
                            <td><b>= {{trans('risk_rating.fc_ccc')}}</b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year1 != 0) ? $readyratiodata->cash_conversion_cycle1 : "" }}</span></b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year2 != 0) ? $readyratiodata->cash_conversion_cycle2 : "" }}</span></b></td>
                            <td style="text-align: center"><b><span style="white-space: nowrap;">{{ ($readyratiodata->cc_year3 != 0) ? $readyratiodata->cash_conversion_cycle3 : "" }}</span></b></td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Average Daily Balance</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">Php {{ number_format($finalscore->average_daily_balance, 2, '.', ',') }}</span></td>
                        </tr>
                        <tr>
                            <td>Operating Cashflow Margin</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore->operating_cashflow_margin }} %</span></td>
                        </tr>
                        <tr>
                            <td>Operating Cashflow Ratio</td>
                            <td colspan="3" style="text-align: center"><span style="white-space: nowrap;">{{ $finalscore->operating_cashflow_ratio }}</span></td>
                        </tr>
                    </table>
                </div>
                @endif
            </div>

        </div>

        @if ((($bank_standalone == 0) && (STR_EMPTY != $finalscore->positive_points)) || ($bank_standalone == 1))

        @if(!empty($is_deliverables) || !empty($deliverables->growth_forecast))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->growth_forecast))
        <div class = 'hideChart blast-break-page'>
        @endif
            @if ($bank_standalone == 0)
                @if ((STR_EMPTY != $finalscore->positive_points) || (STR_EMPTY != $finalscore->negative_points))
                    <?php
                        $pos_exp    = explode('|', $finalscore->positive_points);
                        $neg_exp    = explode('|', $finalscore->negative_points);
                    ?>


                    @if (STR_EMPTY != $finalscore->positive_points)
                        <div class = 'hundred-percent'>
                            <b> The Good </b>
                            <ul>
                            @foreach ($pos_exp as $positive)
                                <li> {{ $positive }} </li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (STR_EMPTY != $finalscore->negative_points)
                        <div class = 'hundred-percent'>
                            <b> The Bad </b>
                            <ul>
                            @foreach ($neg_exp as $negative)
                                <li> {{ $negative }} </li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                @endif
            @endif

            @if ($bank_standalone == 0)
            </div>
            <div class = 'break-page'>
            @endif

            <div>
				<div class = 'hundred-percent'>
                    <div>
                        <div class="title3_3">
                            <div style = 'margin-left: -15px;'>
                                <b>Growth Forecast</b>&nbsp;(
                                @php
                                    echo !empty($startingYear) ? $startingYear . ' - ' : $readyratiodata->startingYear . ' - ';
                                    echo !empty($endingYear) ? $endingYear : $readyratiodata->endingYear;
                                @endphp
                                )
                            </div>
                        </div>
                    </div>
                </div>

                <div class = 'hundred-percent'>
                    <div>
                        <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["forecast"]) }}' style = 'height: auto; width: 630px; vertical-align: middle;'/>
                    </div>
                </div>

				<div class = 'hundred-percent'>
                    <p style="font-size:13px;">
						{!! $growth_desc !!}
					</p>
                </div>

                <div class = 'hundred-percent'>
                    <b> What am I looking at? </b>
                    <p style="font-size:13px;">
						The Growth Forecast graph is meant to present CreditBPO’s own
                        forecast for expected industry growth alongside expected company
                        growth 1, 2 and 3 years forward. Calculation emphasis was placed
                        on keeping the base numbers used for both trendlines raw, and
                        pushing multivariable series analysis into the trend line itself.
                        <br/><br/>
                        The purpose of the Industry Comparison Graph is to compare the Industry
                        and the Company using metrics that are calculated from current (as opposed to forecast)
                        figures. This is important because the metrics used must be exactly the
                        same in terms of both source and derivation. In perspective, it is a
                        static measure of how the company of the data-specific period compares
                        to the greater industry of the same period.
                        <br/><br/>
                        Given the aims for both graphs explained above, the figures used differ
                        by their very nature. For example, Revenue Growth is used in both graphs but the
                        Industry figure in Growth Forecast is very different from the one in
                        Industry Comparison. This is because the Industry Comparison figure is
                        averaged out over thousands of companies for which we have individual
                        figures. The overall Growth Forecast is based on the entire Industry
                        indicator which includes companies for which there is no sufficient granular data.
                        <br/><br/>
					</p>
                </div>
            </div>

        </div>
        @endif

        @if($entity->export_DSCR == 1)
        <div>
            <div class = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div style = 'margin-left: -15px;'><b>User Exported Parameters</b></div>
                    </div>
                </div>
            </div>
            @if($is_financing)
            <table style="border:1px solid #000;">
                <tr>
                    <td><b>DSCR</b></td>
                    <td><b>{{ $dscr_details->dscr }}x</b></td>
                </tr>
                @if($dscr_details->source == "NetOperatingIncome")
                <tr>    
                    <td><b>Source (NOI (Net Operating Income))</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b></td>
                </tr>
                @elseif($dscr_details->source == "NetCashByOperatingActivities")
                <tr>    
                    <td><b>Source (Net Cash Flow)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b></td>
                </tr>
                @elseif($dscr_details->source == "ADB")
                <tr>
                    <td><b>Source (Average Daily Balance)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }}</b>
                </tr>
                @else
                <tr>
                    <td><b>Source (Custom)</b></td> 
                    <td></b>{{ number_format($dscr_details->net_operating_income, 2) }} </b></td>
                </tr>              
                @endif
                <tr>
                    <td><b>Total Loan Duration (months) </b></td>
                    <td><b>{{ $dscr_details->loan_duration }} </b> </td>
                </tr>
                <tr>
                    <td><b>Loan Amount</b></td>
                    <td><b>{{ number_format($dscr_details->loan_amount, 2) }} </b> </td>
                </tr>
            </table>
            @else
            <table style="border:1px solid #000;">
                <tr>
                    <td><b>Coverage Ratio - Funds Source to Fulfill Contract Award</b></td>
                    <td> {{ $dscr_details->dscr }}x</b> </td>
                </tr>
                @if($dscr_details->source == "NetOperatingIncome")
                <tr>
                    <td><b>Funds Source to Cover Contract Fulfillment: (NOI (Net Operating Income))</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @elseif($dscr_details->source == "NetCashByOperatingActivities")
                <tr>
                    <td><b>Funds Source to Cover Contract Fulfillment: (Net Cash Flow)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b></td><br/>
                </tr>
                @elseif($dscr_details->source == "ADB")
                <tr>
                    <td><b>Funds Source to Cover Contract Fulfillment: (Average Daily Balance)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @else
                <tr>
                    <td><b>Funds Source to Cover Contract Fulfillment: (Custom)</b></td>
                    <td><b>{{ number_format($dscr_details->net_operating_income, 2) }} </b> <td/>
                </tr>
                @endif
                <tr>
                    <td><b>Required Delivery/Completion (months)</b></td>
                    <td><b> {{ $dscr_details->loan_duration }} </b> </td>
                </tr>
                <tr>
                    <td><b>Suggested Contract Award Based on Funds Source</b></td>
                    <td><b> {{ number_format($dscr_details->loan_amount, 2) }} </b> </td>
                </tr>
            </table>
            @endif
        </div>
        @endif

        @if($bank_standalone==0)
        <div class = 'break-page'>
            <div class = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div><b>{{ trans('risk_rating.business_considerations') }}</b></div>
                    </div>
                </div>

                @if ($finalscore->score_rm == 90)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.risk_management')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.rm_high')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.risk_management')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.rm_low')}}
                    </p>
                @endif

                @if ($finalscore->score_cd == 30)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.cd_high')}}
                    </p>
                @elseif ($finalscore->score_cd == 20)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.cd_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.customer_dependency')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.cd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_sd == 30)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.sd_high')}}
                    </p>
                @elseif ($finalscore->score_sd == 20)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.sd_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.supplier_dependency')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.sd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_bois == 90)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.bo_high')}}
                    </p>
                @elseif ($finalscore->score_bois == 60)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.bo_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_outlook')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.bo_low')}}
                    </p>
                @endif
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["bcc_graph"]) }}' style = 'max-width: 100%;'/>
            </div>
        </div>

        <div class = 'break-fpage'>
            <div id = 'hundred-percent'>
                <div>
                    <div class="title3_3">
                        <div><b>{{ trans('risk_rating.management_quality') }}</b></div>
                    </div>
                </div>

                @if ($finalscore->score_boe == 21)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                @elseif ($finalscore->score_boe == 14)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                @elseif ($finalscore->score_boe == 7)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_owner')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                @endif

                @if ($finalscore->score_mte == 21)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b></p>
                @elseif ($finalscore->score_mte == 14)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b></p>
                @elseif ($finalscore->score_mte == 7)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.management_team')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b></p>
                @endif

                @if ($finalscore->score_bdms == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_driver')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.bd_high')}}
                    </p>
                @elseif ($finalscore->score_bdms == 12)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.business_driver')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.bd_low')}}
                    </p>
                @endif

                @if ($finalscore->score_sp == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.sp_high')}}
                    </p>
                @elseif ($finalscore->score_sp == 24)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: orange;">{{trans('risk_rating.moderate')}}</span></b>
                    <br/>{{trans('risk_rating.sp_moderate')}}
                    </p>
                @else
                    <p style="font-size:13px;"><b>{{trans('risk_rating.succession_plan')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.sp_low')}}
                    </p>
                @endif

                @if ($finalscore->score_ppfi == 36)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.past_project')}} <span style="color: green;">{{trans('risk_rating.high')}}</span></b>
                    <br/>{{trans('risk_rating.pp_high')}}
                    </p>
                @elseif ($finalscore->score_ppfi == 12)
                    <p style="font-size:13px;"><b>{{trans('risk_rating.past_project')}} <span style="color: red;">{{trans('risk_rating.low')}}</span></b>
                    <br/>{{trans('risk_rating.pp_low')}}
                    </p>
                @endif
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["mq_graph"]) }}' style = 'max-width: 100%;'/>
            </div>
        </div>
        @endif

        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
        <div class = 'last-break-page'>    
        @elseif(empty($deliverables->industry_forecasting))
        <div class = 'hideChart last-break-page'>
        @endif
            <div>
                <div class="title3_4">
                    <div>
                        <b> {{ trans('risk_rating.industry_comparison') }} </b> <br/>
                        > {{ $industry->main_title }} <br/>
                        >> {{ $ind_sub->sub_title }} <br/>
                        >>> @php echo !empty($ind_row->class_description) ? $ind_row->class_description:''; @endphp
                    </div>
					<!-- <div> Region: {{ $region }} </div> -->
                </div>
            </div>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.gross_growth_vs_industry') }}
                    <span style = 'color: #BDCF8E;'> {{ number_format($ind_cmp['grg'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #F9967C;"> {{ number_format($boost['grg'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['grg'] > $boost['grg'])
                    {{ trans('risk_rating.growth_high') }}
                @elseif ($ind_cmp['grg'] == $boost['grg'])
                    {{ trans('risk_rating.growth_moderate') }}
                @elseif ($boost['grg'] > $ind_cmp['grg'])
                    {{ trans('risk_rating.growth_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.net_growth_vs_industry') }}
                    <span style = 'color: #BDCF8E;'> {{ number_format($ind_cmp['nig'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #F9967C;"> {{ number_format($boost['nig'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['nig'] > $boost['nig'])
                    {{ trans('risk_rating.net_high') }}
                @elseif ($ind_cmp['nig'] == $boost['nig'])
                    {{ trans('risk_rating.net_moderate') }}
                @elseif ($boost['nig'] > $ind_cmp['nig'])
                    {{ trans('risk_rating.net_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.profit_vs_industry') }}
                    <span style = 'color: #BDCF8E;'> {{ number_format($ind_cmp['gpm'], 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #F9967C;"> {{ number_format($boost['gpm'], 2) }} </span>
                </b>
                <br/>
                @if ($ind_cmp['gpm'] > $boost['gpm'])
                    {{ trans('risk_rating.profit_high') }}
                @elseif ($ind_cmp['gpm'] == $boost['gpm'])
                    {{ trans('risk_rating.profit_moderate') }}
                @elseif ($boost['gpm'] > $ind_cmp['gpm'])
                    {{ trans('risk_rating.profit_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.ratio_vs_industry') }}
                    <span style = 'color: #BDCF8E;'> {{ number_format($readyratiodata->current_ratio2, 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #F9967C;"> {{ number_format($boost['cr'], 2) }} </span>
                </b>
                <br/>
                @if ($readyratiodata->current_ratio2 > $boost['cr'])
                    {{ trans('risk_rating.ratio_high') }}
                @elseif ($readyratiodata->current_ratio2 == $boost['cr'])
                    {{ trans('risk_rating.ratio_moderate') }}
                @elseif ($boost['cr'] > $readyratiodata->current_ratio2)
                    {{ trans('risk_rating.ratio_low') }}
                @endif
            </p>

            <p style="font-size:13px;">
                <b> {{ trans('risk_rating.debt_vs_industry') }}
                    <span style = 'color: #BDCF8E;'> {{ number_format($readyratiodata->debt_equity_ratio2, 2) }} </span>
                    <span style="color: orange;">  vs </span>
                    <span style="color: #F9967C;"> {{ number_format($boost['der'], 2) }} </span>
                </b>
                <br/>
                @if ($readyratiodata->debt_equity_ratio2 > $boost['der'])
                    {{ trans('risk_rating.debt_high') }}
                @elseif ($readyratiodata->debt_equity_ratio2 == $boost['der'])
                    {{ trans('risk_rating.debt_moderate') }}
                @elseif ($boost['der'] > $readyratiodata->debt_equity_ratio2)
                    {{ trans('risk_rating.debt_low') }}
                @endif
            </p>

            <div class="ind-legend hundred-percent">
                <div class="legend-item">
                    <span class="legend-info"></span>
                    <span>Company</span>
                </div>
                <div class="legend-item">
                    <span class="legend-info industry"></span>
                    <span>Industry</span>
                </div>
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["reg1_graph"]) }}' style = 'width: 100%; max-height: 90px;'/>
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["reg2_graph"]) }}' style = 'width: 100%; max-height: 90px;'/>
            </div>

            <div class = 'hundred-percent'>
                <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["reg3_graph"]) }}' style = 'width: 100%; max-height: 90px;'/>
            </div>

        </div>

        @if ($bank_standalone==0)
            <div class = 'break-page'>
        @else
            <div class = 'last-break-page'>
        @endif

        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
            <div class = 'hundred-percent'>
        @elseif(empty($deliverables->industry_forecasting))
            <div class = 'hideChart hundred-percent'>
        @endif
        <div>
            <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["reg4_graph"]) }}' style = 'width: 100%; max-height: 90px;'/>
        </div>

        <div class = 'hundred-percent'>
            <img src = '{{ URL::To("images/rating-report-graphs/".$graphs["reg5_graph"]) }}' style = 'width: 100%; max-height: 90px;'/>
        </div>

        </div>
		@if($bank_standalone==0)
        <div class = 'break-page'>
            <div>
                <div class="title3_3">
                    <div><b>Strategic Profile</b></div>
                </div>
            </div>

            <div class = 'hundred-percent' style = 'margin: 10px;'>
                @if ($businessdriver)
                <div style = 'width: 35%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'>{{ trans('business_details2.bdriver') }} </td>
                            </tr>

                            <tr>
                                <td> <b>Business Driver Name</b> </td>
                                <td> <b>% Share of Total Sales</b> </td>
                            </tr>

                            @foreach ($businessdriver as $driver)
                            <tr>
                                <td> {{ $driver->businessdriver_name }} </td>
                                <td> {{ $driver->businessdriver_total_sales }}% </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif

                @if ($bizsegment)
                <div style = 'width: 30%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr'>{{ trans('business_details2.customer_segment') }} </td>
                            </tr>
                            @foreach ($bizsegment as $segment)
                            <tr>
                                <td> {{ $segment->businesssegment_name }} </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif

                @if ($revenuepotential)
                <div style = 'width: 30%; display: inline-block;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'> Revenue Growth Potential within next 3 years </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('condition_sustainability.under_current_market') }}</b> </td>
                                <td> <b>{{ trans('condition_sustainability.expected_future_market') }}</b> </td>
                            </tr>

                            @foreach ($revenuepotential as $potential)
                            <tr>
                                <td>
                                    @if ($potential->current_market == 1)
                                        0%
                                    @elseif ($potential->current_market == 2)
                                        {{ trans('condition_sustainability.less_3_percent') }}
                                    @elseif ($potential->current_market == 3)
                                        {{ trans('condition_sustainability.between_3to5_percent') }}
                                    @else
                                        {{ trans('condition_sustainability.greater_5_percent') }}
                                    @endif
                                </td>

                                <td>
                                    @if ($potential->new_market == 1)
                                        0%
                                    @elseif ($potential->new_market == 2)
                                        {{ trans('condition_sustainability.less_3_percent') }}
                                    @elseif ($potential->new_market == 3)
                                        {{ trans('condition_sustainability.between_3to5_percent') }}
                                    @else
                                        {{ trans('condition_sustainability.greater_5_percent') }}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                @endif
            </div>

            @if ($past_projs)
            <div class = 'hundred-percent' style = 'margin: 10px;'>
                <div class="read-only">
                    <table style = 'width: 96%;'>
                        <tr>
                            <td class = 'tb_hdr' colspan = '5'> {{ trans('business_details2.past_proj_completed') }} </td>
                        </tr>

                        <tr>
                            <td> <b>{{ trans('business_details2.ppc_purpose') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_cost_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_source_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_year_short') }}</b> </td>
                            <td> <b>{{ trans('business_details2.ppc_result_short') }}</b> </td>
                        </tr>

                        @foreach ($past_projs as $proj)
                        <tr>
                            <td> {{ $proj->projectcompleted_name }} </td>
                            <td> {{ number_format($proj->projectcompleted_cost, 2) }} </td>
                            <td> {{ trans('business_details2.'.$proj->projectcompleted_source_funding) }} </td>
                            <td> {{ $proj->projectcompleted_year_began }} </td>
                            <td> {{ trans('business_details2.'.$proj->projectcompleted_result) }} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif

            @if ($future_growths)
            <div class = 'hundred-percent' style = 'margin: 10px;'>
                <div class="read-only">
                    <table style = 'width: 96%;'>
                        <tr>
                            <td class = 'tb_hdr' colspan = '5'> {{ trans('business_details2.future_growth_initiatives') }} </td>
                        </tr>

                        <tr>
                            <td> <b>{{ trans('business_details2.fgi_purpose') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_cost') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_date') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_source') }}</b> </td>
                            <td> <b>{{ trans('business_details2.fgi_proj_goal') }}</b> </td>
                        </tr>

                        @foreach ($future_growths as $future)
                        <tr>
                            <td> {{ $future->futuregrowth_name }} </td>
                            <td> {{ number_format($future->futuregrowth_estimated_cost, 2) }} </td>
                            <td> {{ $future->futuregrowth_implementation_date }} </td>
                            <td> {{ trans('business_details2.'.$future->futuregrowth_source_capital) }} </td>
                            <td> {{ trans('business_details2.'.$future->planned_proj_goal) }} by {{ $future->planned_goal_increase }}% beginning {{ substr($future->proj_benefit_date, 5, 2) }} {{ substr($future->proj_benefit_date, 0, 4) }} </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif

            @if ($planfacilities)
            @foreach ($planfacilities as $planfacility)
            <div class = 'hundred-percent' style = 'margin: 10px; margin-right: 12px;'>
                <div class = 'fifty-percent' style = 'margin: 0px;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '3'> {{ trans('rcl.rcl') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.amount_of_line') }}</b> </td>
                                @if ($planfacility->purpose_credit_facility == 5)
                                <td> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                <td> <b>{{ trans('rcl.supplier_credit') }}</b> </td>
                                @elseif ($planfacility->purpose_credit_facility == 4)
                                <td> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                <td> <b>{{ trans('rcl.purpose_of_cf_others') }}</b> </td>
                                @else
                                <td colspan = '2'> <b>{{ trans('rcl.purpose_of_cf') }}</b> </td>
                                @endif
                            </tr>

                            <tr>
                                <td> {{ number_format($planfacility->plan_credit_availment, 2) }} </td>
                                @if (($planfacility->purpose_credit_facility == 4) || ($planfacility->purpose_credit_facility == 5))
                                <td>
                                @else
                                <td colspan = '2'>
                                @endif
                                    @if ($planfacility->purpose_credit_facility == 1)
                                            {{trans('rcl.working_capital')}}
                                    @elseif ($planfacility->purpose_credit_facility == 2)
                                            {{trans('rcl.expansion')}}
                                    @elseif ($planfacility->purpose_credit_facility == 3)
                                            {{trans('rcl.takeout')}}
                                    @elseif ($planfacility->purpose_credit_facility == 5)
                                            {{trans('rcl.supplier_credit')}}
                                    @else
                                            {{trans('rcl.other_specify')}}
                                    @endif
                                </td>

                                @if (($planfacility->purpose_credit_facility == 4) || ($planfacility->purpose_credit_facility == 5))
                                <td>
                                    @if ($planfacility->purpose_credit_facility == 5)
                                        {{ $planfacility->credit_terms }}
                                    @elseif ($planfacility->purpose_credit_facility == 4)
                                        {{ $planfacility->purpose_credit_facility_others }}
                                    @endif
                                </td>
                                @endif
                            </tr>

                            @if ('' != $planfacility->other_remarks)
                            <tr>
                                <td class = 'tb_hdr' colspan = '3'> {{ trans('rcl.other_remarks') }} </td>
                            </tr>

                            <tr>
                                <td colspan = '3'> <b>{{ $planfacility->other_remarks }}</b> </td>
                            </tr>
                            @endif
                        </table>
                    </div>

                </div>

                <div class = 'fifty-percent' style = 'margin: 0px; width: 51%;'>
                    <div class="read-only">
                        <table>
                            <tr>
                                <td class = 'tb_hdr' colspan = '2'> {{ trans('rcl.offered_collateral') }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.type_of_deposit') }}</b> </td>
                                <td> {{ $planfacility->cash_deposit_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.deposit_amount') }}</b> </td>
                                <td> {{ number_format($planfacility->cash_deposit_amount, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.marketable_securities') }}</b> </td>
                                <td> {{ $planfacility->securities_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.marketable_securities_value') }}</b> </td>
                                <td> {{ number_format($planfacility->securities_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.property') }}</b> </td>
                                <td> {{ $planfacility->property_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.property_value') }}</b> </td>
                                <td> {{ number_format($planfacility->property_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.chattel') }}</b> </td>
                                <td> {{ $planfacility->chattel_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.chattel_value') }}</b> </td>
                                <td> {{ number_format($planfacility->chattel_estimated_value, 2) }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.others') }}</b> </td>
                                <td> {{ $planfacility->others_details }} </td>
                            </tr>

                            <tr>
                                <td> <b>{{ trans('rcl.others_value') }}</b> </td>
                                <td> {{ number_format($planfacility->others_estimated_value, 2) }} </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            @endforeach
            @endif

        </div>
        @endif

        
        @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
        <div><hr/></div>
        @elseif(empty($deliverables->industry_forecasting))
        <div class="hideChart"><hr/></div>
        @endif
        
        {{-- @if ($bank_standalone==0) --}}
        @if($entity->is_premium == 1)
            @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && (count($majorCustomers) == 0) && (count($majorSuppliers) == 0) && !$location)
            <div class="last-break-page">
            @else
            <div class="break-page">
            @endif
        @else
            <div class = 'last-break-page'>
        @endif
        {{-- @else --}}
            {{-- <div class='last-break-page'> --}}
        {{-- @endif --}}
            @if(!empty($is_deliverables) || !empty($deliverables->industry_forecasting))
            <div class = 'hundred-percent'>
            @elseif(empty($deliverables->industry_forecasting))
            <div class = 'hideChart hundred-percent'>
            @endif
                <div id = 'creditbpo-disc'>
                    <p>
                        THIS INFORMATION IS PROVIDED BY CREDITBPO TECH, INC. SUBJECT TO THE CREDITBPO PLATFORM TERMS OF USE AND IS NOT TO BE DISCLOSED. READING FURTHER IMPLIES FULL COMPREHENSION AND ACCEPTANCE OF CREDITBPO PLATFORM TERMS OF USE.
                        <br />
                        This generated report is forwarded to the User in strict confidence for use by the User as one considering factor in connection with credit and other business decisions. For reasons outlined in Terms of Use, CreditBPO does not accept responsibility for the accuracy, completeness or timeliness of the report. CreditBPO Tech, Inc. disclaims all liability for any loss or damage arising out of or in any way related to the contents of this report. This material is confidential and proprietary to CreditBPO Tech, Inc. and/or third parties and may not be reproduced, published or disclosed to others without the express authorization of CreditBPO Tech, Inc. or the General Counsel of CreditBPO Tech, Inc.
                        <br/>
                        COPYRIGHT {{ date('Y') }} CREDITBPO TECH, INC. THIS REPORT MAY NOT BE REPRODUCED IN WHOLE OR IN PART IN ANY FORM OR MANNER WHATSOEVER.
                    </p>
                </div>
            </div>
            </div>
        </div>

    @if ($entity->is_premium == 1)
    <div>
    @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0))
    @else
    @if(!$location && ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0)))
    <div class="last-break-page annexA"></div>
    @else
    <div class="break-page annexA">
    @endif
        @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0))
        @elseif ((count($keyManagers) != 0) || (count($shareHolders) != 0) || (count($boardOfDirectors) != 0))
        <h3>Annex A: Associated Persons</h3>
        @endif
        @if (count($keyManagers) != 0)
        <div class="avoidPageBreak">
            <p><b>Key Managers</b></p>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Nationality</th>
                    <th scope="col">Position</th>
                    <th scope="col">% of Ownership</th>
                    
                    
                </tr>
                </thead>
                <tbody>
                    @foreach ($keyManagers as $keyManager)
                    <tr>
                        <td> {{ $keyManager->firstname }} {{ $keyManager->middlename }} {{ $keyManager->lastname }} </td>
                        @if ($keyManager->address1)
                        <td> {{ $keyManager->address1 }}  </td>
                        @else
                        <td> - </td>
                        @endif
                        @if ($keyManager->nationality)
                        <td style="text-align: center;"> {{ $keyManager->nationality }}  </td>
                        @else
                        <td> - </td>
                        @endif
                        <td style="text-align: center;"> {{ $keyManager->position }}  </td>
                        @if ($keyManager->percent_of_ownership)
                        <td style="text-align: center;"> {{ $keyManager->percent_of_ownership }}  </td>
                        @else
                        <td> - </td>
                        @endif
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
                
        </div><br>
        @endif
        @if (count($shareHolders) != 0)
        <div class="avoidPageBreak">
                <p><b>Shareholders</b></p>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Nationality</th>
                        <th scope="col">TIN</th>
                        <th scope="col">% Ownership</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($shareHolders as $shareHolder)
                        <tr>
                            <td> {{ $shareHolder->name }} </td>
                            <td> {{ $shareHolder->address ? $shareHolder->address : ' - ' }}  </td>
                            <td style="text-align: center;"> {{ $shareHolder->nationality }}  </td>
                            @if ($shareHolder->id_no)
                            <td style="text-align: center;"> {{ $shareHolder->id_no }}  </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($shareHolder->percentage_share)
                            <td style="text-align: center;"> {{ $shareHolder->percentage_share }}  </td>
                            @else
                            <td> - </td>
                            @endif
                        </tr>
                            
                        @endforeach
                    </tbody>
                </table>
                    
            </div><br>
            @endif
            @if (count($boardOfDirectors) != 0)
            <div class="avoidPageBreak">
                <p><b>Board of Directors</b></p>
                <table class="table">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Nationality</th>
                        <th>TIN</th>
                        <th>% Ownership</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($boardOfDirectors as $boardOfDirector)
                        <tr>
                            
                                <td> {{ $boardOfDirector->name }} </td>
                                <td> {{ $boardOfDirector->address ? $boardOfDirector->address : ' - ' }}  </td>
                                <td style="text-align: center;"> {{ $boardOfDirector->nationality }} </td>
                                @if ($boardOfDirector->id_no)
                                <td style="text-align: center;"> {{ $boardOfDirector->id_no }} </td>
                                @else
                                <td> - </td>
                                @endif
                                @if ($boardOfDirector->percentage_share != 0)
                                <td style="text-align: center;"> {{ (int)$boardOfDirector->percentage_share }} </td>
                                @else
                                <td> - </td>
                                @endif
                            
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                
            </div><br>
            @endif
    </div>
    @endif

    @if ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0))
    @else
    @if($location)
    <div class="break-page">
    @else
    <div class="last-break-page">
    @endif
        @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && ((count($majorCustomers) != 0) || (count($majorSuppliers)) != 0))
            <h3>Annex A: Major Customers and Suppliers</h3>
        @elseif ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0))
        @else
            <h3>Annex B: Major Customers and Suppliers</h3> 
        @endif
        
        @if (count($majorCustomers) != 0) 
        <div>
            <p><b>Major Customers</b></p>
            <table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone No.</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($majorCustomers as $majorCustomer)
                    <tr>
                        
                            <td> {{ $majorCustomer->customer_name }} </td>
                            <td> {{ $majorCustomer->customer_address }}  </td>
                            @if ($majorCustomer->customer_phone)
                            <td> {{ $majorCustomer->customer_phone }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($majorCustomer->customer_email)
                            <td> {{ $majorCustomer->customer_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
              </table>
            
        </div><br>
        @endif
        @if (count($majorSuppliers) != 0)  
        <div>
            <p><b>Major Suppliers</b></p>
            <table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone No.</th>
                    <th>Email Address</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($majorSuppliers as $majorSupplier)
                    <tr>
                        
                            <td> {{ $majorSupplier->supplier_name }} </td>
                            <td> {{ $majorSupplier->supplier_address }}  </td>
                            @if ($majorSupplier->supplier_phone)
                            <td> {{ $majorSupplier->supplier_phone }} </td>
                            @else
                            <td> - </td>
                            @endif
                            @if ($majorSupplier->supplier_email)
                            <td> {{ $majorSupplier->supplier_email }} </td>
                            @else
                            <td> - </td>
                            @endif
                        
                    </tr>
                    @endforeach
                </tbody>
              </table>
            
        </div>
        @endif
    </div>
    @endif


    @if ($location)
        @if ((count($keyManagers) == 0) && (count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && ((count($majorCustomers) != 0) || (count($majorSuppliers) != 0)))
        <h3>Annex B: Site Inspection</h3>
        @elseif (((count($keyManagers) != 0) || (count($shareHolders) != 0) || (count($boardOfDirectors) != 0)) && ((count($majorCustomers) == 0) && (count($majorSuppliers) == 0)))
        <h3>Annex B: Site Inspection</h3>
        @elseif((count($keyManagers) == 0) && ( count($shareHolders) == 0) && (count($boardOfDirectors) == 0) && (count($majorCustomers) == 0) && (count($majorSuppliers) == 0))
        <h3>Annex A: Site Inspection</h3>
        @else
        <h3>Annex C: Site Inspection</h3>
        @endif
        <div class="mainLocation">
            <p><b>Main Location</b></p>
            <table class="table">
                <thead>
                  <tr>
                    <th>Address</th>
                    <th>Size of Premises</th>
                    <th>No. of Floors</th>
                    <th>Premises Owned/Leased</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        
                            <td> {{ $entity->address1 }}, {{ $entityCity }}, {{ $entity->province }}, {{ $zipcode }} </td>
                            <td style="text-align: center;"> {{ $location->location_size }} sq.m</td>
                            <td style="text-align: center;"> {{ $location->floor_count }} </td>
                            <td style="text-align: center;"> {{ trans('business_details1.' . $location->location_type) }} </td>
                        
                    </tr>
                </tbody>
              </table>
            
        </div><br>

        <div class="hundred-percent" style="margin: auto;">
            <div class="break-page">
            @if (!$location->map)
                <img src="{{ public_path() }}/documents/location/No-image-found.jpg" alt="No image found" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @else
                <img src="{{ public_path() }}/documents/location/{{ $location->map }}" alt="map" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @endif
            
                <div class = 'center' style = 'position: relative; top: -10px;'>
                    @if ($location->other)
                    <p style="text-align:center; padding-top: 5px;">{{ $location->other }}</p>
                    @else
                    <br /><br />
                    @endif
                </div>
            </div>

            @if (!$location->map)
                <img src="{{ public_path() }}/documents/location/No-image-found.jpg" alt="No image found" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @else
                <img src="{{ public_path() }}/documents/location/{{ $location->location_photo }}" alt="map" style="width: 100%; height:440px; border: solid #052462; vertical-align: middle;">
            @endif

                <div class = 'center' style = 'position: relative; top: -10px;'>
                    <p style="text-align:center;">The main office of {{ $entity->companyname }}.</p>
                </div>
        </div>
    @endif
    </div>
    @endif

    <script type="text/javascript">
		Lang = {!!LanguageHelper::getAll()!!};
		function trans(sData){
			sCommand = sData.split('.');
			return Lang[sCommand[0]][sCommand[1]];
		}
		function convert(sGroup, sKey){
			if(sKey!=null && sKey!=''){
				if(Lang[sGroup][sKey]){
					return Lang[sGroup][sKey];
				}
			}
			return sKey;
		}
	</script>

</body></html>
