@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')

@show

@section('content')
    <div id="loginwrapper" class="container">
        <h3 style="margin-top:0px;">{{ $pay_sts }}</h3>

        <p>
            {{ $error }}
            <br/>
        </p>

        <p>
            Error Detected. We are sorry for the inconvenience. Please <a style = 'color: #0000EE;' href="{{ URL::to('/') }}">Click Here</a> to return.
        </p>

    </div>
@stop
