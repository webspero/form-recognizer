@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')

@show

@section('content')
        <div id="loginwrapper" class="container">
	{{ Form::open(array('url' => 'login', 'id' => 'login')) }}

        <h3 style="margin-top:0px;">Error</h3>


        You don't have access to view this page. Please <a style = 'color: #0000EE;' href="{{ URL::to('/') }}/logout">Click Here</a> to log out.



	{{ Form::close() }}
        </div>
@stop
