@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')

@show

@section('content')
        <div id="loginwrapper" class="container">
	{{ Form::open(array('url' => 'login', 'id' => 'login')) }}

       <h3 style="margin-top:0px;">{{$title}}</h3>

			{{$message}}
        </div>

	{{ Form::close() }}
        </div>
@stop
