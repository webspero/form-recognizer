@extends('layouts.master', array('hide' => true)))

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<h1>CreditBPO Signup Confirmation Page</h1>
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text">
                    @if (Session::has('fail'))
                        <p>Error Code: SR001 - An error occured during Registration. Please contact notify.user@creditbpo.com.<p>
                    @elseif (Session::has('success'))
                        <p>
                            {{ Session::get('success') }} <br/><br/>
                            Click <a href = '{{ URL::To("/") }}'> Here </a> to go back to the login page
                        </p>
                    @else
                        <div>
                            <p>Thank you for registering.</p>
                            <p>Please check your inbox. </p>
                            <p>You should receive an email from CreditBPO. Click on the link provided to activate your account.</p>
                            <p>This will redirect you to the sign in page.</p>
                            <p>Check your spam folder if you do not see it in your inbox.</p>
                            <br>
                            <p>Click <a href = '{{ URL::To("/") }}'> Here </a> to go back to the login page</p>
                        </div>
                    @endif
			    </div>
			</div>
		</div>
	</div>
@stop
@section('javascript')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}
,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-59609039-1', 'auto');
ga('send', 'pageview');
</script>
<script language="JavaScript"><!--
javascript:window.history.forward(1);
//--></script>
@stop
