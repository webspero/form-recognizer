@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<!-- live2support.com tracking codes starts --><div id="l2s_trk" style="z-index:99;">website live chat</div><script type="text/javascript"> var l2slhight=400; var l2slwdth=350; var l2slay_mnst="#l2snlayer {}";var l2slv=3; var l2slay_hbgc="#0097c2"; var l2slay_bcolor="#0097c2"; var l2sdialogofftxt="Live Chat Offline"; var l2sdialogontxt="Live Chat Online"; var l2sminimize=true;    var l2senblyr=true; var l2slay_pos="R"; var l2s_pht=escape(location.protocol); if(l2s_pht.indexOf("http")==-1) l2s_pht='http:'; (function () { document.getElementById('l2s_trk').style.visibility='hidden';
	var l2scd = document.createElement('script'); l2scd.type = 'text/javascript'; l2scd.async = true; l2scd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's01.live2support.com/js/lsjs1.php?stid=26893 &jqry=Y&l2stxt='; var l2sscr = document.getElementsByTagName('script')[0]; l2sscr.parentNode.insertBefore(l2scd, l2sscr); })(); </script><!-- live2support.com tracking codes closed -->

	<div class="container">
		<h1>API TESTER</h1>
		{{ Form::open( array('route' => 'postSupervisorSignup', 'role'=>'form', 'files' => true)) }}
		<div class="spacewrapper">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
            </div>
            @endif
			<div class="read-only">
				<h4>API TEST</h4>
				<div class="read-only-text">
                    <div class="form-group">
                        {{Form::label('services', 'Services')}}
                        <select name = 'services' class = 'form-control'>
                            <option value = 'addAffiliates' data-method = 'post' data-valid = '1'> Adding Affiliates - Valid </option>
                            <option value = 'addAffiliates' data-method = 'post' data-valid = '0'> Adding Affiliates - Invalid </option>

                            <option value = 'addProducts' data-method = 'post' data-valid = '1'> Adding Products - Valid </option>
                            <option value = 'addProducts' data-method = 'post' data-valid = '0'> Adding Products - Invalid </option>

                            <option value = 'addCapitalDetails' data-method = 'post' data-valid = '1'> Add / Edit Capital Details - Valid </option>
                            <option value = 'addCapitalDetails' data-method = 'post' data-valid = '0'> Add / Edit Capital Details - Invalid </option>

                            <option value = 'addMainLocation' data-method = 'post' data-valid = '1'> Adding Main Location - Valid </option>
                            <option value = 'addMainLocation' data-method = 'post' data-valid = '0'> Adding Main Location - Invalid </option>

                            <option value = 'addBranches' data-method = 'post' data-valid = '1'> Adding Branches - Valid </option>
                            <option value = 'addBranches' data-method = 'post' data-valid = '0'> Adding Branches - Invalid </option>

                            <option value = 'addShareholders' data-method = 'post' data-valid = '1'> Adding Shareholders - Valid </option>
                            <option value = 'addShareholders' data-method = 'post' data-valid = '0'> Adding Shareholders - Invalid </option>

                            <option value = 'addDirectors' data-method = 'post' data-valid = '1'> Adding Directors - Valid </option>
                            <option value = 'addDirectors' data-method = 'post' data-valid = '0'> Adding Directors - Invalid </option>

                            <option value = 'uploadCertification' data-method = 'post' data-valid = '1'> Adding Certifications - Valid </option>
                            <option value = 'uploadCertification' data-method = 'post' data-valid = '0'> Adding Certifications - Invalid </option>

                            <option value = 'addImportExport' data-method = 'post' data-valid = '1'> Adding Import / Export - Valid </option>
                            <option value = 'addImportExport' data-method = 'post' data-valid = '0'> Adding Import / Export - Invalid </option>

                            <option value = 'addInsurance' data-method = 'post' data-valid = '1'> Adding Insurance - Valid </option>
                            <option value = 'addInsurance' data-method = 'post' data-valid = '0'> Adding Insurance - Invalid </option>

                            <option value = 'addConversionCycle' data-method = 'post' data-valid = '1'> Add / Edit Cash Conversion Cycle - Valid </option>
                            <option value = 'addConversionCycle' data-method = 'post' data-valid = '0'> Add / Edit Cash Conversion Cycle - Invalid </option>

                            <option value = 'uploadFsTemplate' data-method = 'post' data-valid = '1'> Adding FS Template - Valid </option>
                            <option value = 'uploadFsTemplate' data-method = 'post' data-valid = '0'> Adding FS Template - Invalid </option>

                            <option value = 'uploadDocument' data-method = 'post' data-valid = '1'> Adding Financial Document - Valid </option>
                            <option value = 'uploadDocument' data-method = 'post' data-valid = '0'> Adding Financial Document - Invalid </option>

                            <option value = 'uploadBankStatement' data-method = 'post' data-valid = '1'> Adding Bank Statement - Valid </option>
                            <option value = 'uploadBankStatement' data-method = 'post' data-valid = '0'> Adding Bank Statement - Invalid </option>

                            <option value = 'uploadUtilityBills' data-method = 'post' data-valid = '1'> Adding Utility Bills - Valid </option>
                            <option value = 'uploadUtilityBills' data-method = 'post' data-valid = '0'> Adding Utility Bills - Invalid </option>

                            <option value = 'addMajorCustomer' data-method = 'post' data-valid = '1'> Adding Major Customer - Valid </option>
                            <option value = 'addMajorCustomer' data-method = 'post' data-valid = '0'> Adding Major Customer - Invalid </option>

                            <option value = 'addMajorSupplier' data-method = 'post' data-valid = '1'> Adding Major Supplier - Valid </option>
                            <option value = 'addMajorSupplier' data-method = 'post' data-valid = '0'> Adding Major Supplier - Invalid </option>

                            <option value = 'addMajorLocation' data-method = 'post' data-valid = '1'> Adding Major Location - Valid </option>
                            <option value = 'addMajorLocation' data-method = 'post' data-valid = '0'> Adding Major Location - Invalid </option>

                            <option value = 'addCompetitors' data-method = 'post' data-valid = '1'> Adding Competitors - Valid </option>
                            <option value = 'addCompetitors' data-method = 'post' data-valid = '0'> Adding Competitors - Invalid </option>

                            <option value = 'addBusinessDrivers' data-method = 'post' data-valid = '1'> Adding Business Drivers - Valid </option>
                            <option value = 'addBusinessDrivers' data-method = 'post' data-valid = '0'> Adding Business Drivers - Invalid </option>

                            <option value = 'addBusinessSegments' data-method = 'post' data-valid = '1'> Adding Customer Segments - Valid </option>
                            <option value = 'addBusinessSegments' data-method = 'post' data-valid = '0'> Adding Customer Segments - Invalid </option>

                            <option value = 'addPastProject' data-method = 'post' data-valid = '1'> Adding Past Project - Valid </option>
                            <option value = 'addPastProject' data-method = 'post' data-valid = '0'> Adding Past Project - Invalid </option>

                            <option value = 'addFutureGrowth' data-method = 'post' data-valid = '1'> Adding Future Initiative - Valid </option>
                            <option value = 'addFutureGrowth' data-method = 'post' data-valid = '0'> Adding Future Initiative - Invalid </option>

                            <option value = 'addCompanySuccession' data-method = 'post' data-valid = '1'> Add / Edit Company Succession - Valid </option>
                            <option value = 'addCompanySuccession' data-method = 'post' data-valid = '0'> Add / Edit Company Succession - Invalid </option>

                            <option value = 'addCompetitiveLandscape' data-method = 'post' data-valid = '1'> Add / Edit Competitive Landscape - Valid </option>
                            <option value = 'addCompetitiveLandscape' data-method = 'post' data-valid = '0'> Add / Edit Competitive Landscape - Invalid </option>

                            <option value = 'addEconomicFactor' data-method = 'post' data-valid = '1'> Add / Edit Economic Factor - Valid </option>
                            <option value = 'addEconomicFactor' data-method = 'post' data-valid = '0'> Add / Edit Economic Factor - Invalid </option>

                            <option value = 'addTaxPayments' data-method = 'post' data-valid = '1'> Add / Edit Tax Payments - Valid </option>
                            <option value = 'addTaxPayments' data-method = 'post' data-valid = '0'> Add / Edit Tax Payments - Invalid </option>

                            <option value = 'addTaxPayments' data-method = 'post' data-valid = '1'> Add / Edit Tax Payments - Valid </option>
                            <option value = 'addTaxPayments' data-method = 'post' data-valid = '0'> Add / Edit Tax Payments - Invalid </option>

                            <option value = 'addRiskAssement' data-method = 'post' data-valid = '1'> Adding Risk Assessment - Valid </option>
                            <option value = 'addRiskAssement' data-method = 'post' data-valid = '0'> Adding Risk Assessment - Invalid </option>

                            <option value = 'addRevenueGrowth' data-method = 'post' data-valid = '1'> Adding Revenue Growth Potential - Valid </option>
                            <option value = 'addRevenueGrowth' data-method = 'post' data-valid = '0'> Adding Revenue Growth Potential - Invalid </option>

                            <option value = 'addCapitalExpansion' data-method = 'post' data-valid = '1'> Adding Capital for Expansion - Valid </option>
                            <option value = 'addCapitalExpansion' data-method = 'post' data-valid = '0'> Adding Capital for Expansion - Invalid </option>

                            <option value = 'addRequiredCredit' data-method = 'post' data-valid = '1'> Adding Required Credit Line - Valid </option>
                            <option value = 'addRequiredCredit' data-method = 'post' data-valid = '0'> Adding Required Credit Line - Invalid </option>

                            <option value = 'addOwnerDetails' data-method = 'post' data-valid = '1'> Adding Owner Details - Valid </option>
                            <option value = 'addOwnerDetails' data-method = 'post' data-valid = '0'> Adding Owner Details - Invalid </option>

                            <option value = 'editSpouseDetails' data-method = 'post' data-valid = '1'> Add / Edit Spouse Details - Valid </option>
                            <option value = 'editSpouseDetails' data-method = 'post' data-valid = '0'> Add / Edit Spouse Details - Invalid </option>

                            <option value = 'addPersonalLoans' data-method = 'post' data-valid = '1'> Adding Personal Loans - Valid </option>
                            <option value = 'addPersonalLoans' data-method = 'post' data-valid = '0'> Adding Personal Loans - Invalid </option>

                            <option value = 'addKeyManager' data-method = 'post' data-valid = '1'> Adding Key Manager - Valid </option>
                            <option value = 'addKeyManager' data-method = 'post' data-valid = '0'> Adding Key Manager - Invalid </option>

                            <option value = 'addOwnerOtherBusiness' data-method = 'post' data-valid = '1'> Adding Other Business - Valid </option>
                            <option value = 'addOwnerOtherBusiness' data-method = 'post' data-valid = '0'> Adding Other Business - Invalid </option>

                            <option value = 'addSpouseOtherBusiness' data-method = 'post' data-valid = '1'> Adding Spouse Other Business - Valid </option>
                            <option value = 'addSpouseOtherBusiness' data-method = 'post' data-valid = '0'> Adding Spouse Other Business - Invalid </option>

                            <option value = 'addOwnerEducation' data-method = 'post' data-valid = '1'> Adding Education - Valid </option>
                            <option value = 'addOwnerEducation' data-method = 'post' data-valid = '0'> Adding Education - Invalid </option>

                            <option value = 'addSpouseEducation' data-method = 'post' data-valid = '1'> Adding Spouse Education - Valid </option>
                            <option value = 'addSpouseEducation' data-method = 'post' data-valid = '0'> Adding Spouse Education - Invalid </option>

                            <option value = 'addExistingCredit' data-method = 'post' data-valid = '1'> Adding Existing Credit - Valid </option>
                            <option value = 'addExistingCredit' data-method = 'post' data-valid = '0'> Adding Existing Credit - Invalid </option>

                            <option value = 'getBusinessDetails' data-method = 'get' data-valid = '1'> Get Businesss Details </option>
                            <option value = 'getAffiliates' data-method = 'get' data-valid = '1'> Get Affiliates </option>
                            <option value = 'getProducts' data-method = 'get' data-valid = '1'> Get Products / Services </option>
                            <option value = 'getCapital' data-method = 'get' data-valid = '1'> Get Capital Details </option>
                            <option value = 'getMainLocations' data-method = 'get' data-valid = '1'> Get Main Locations </option>
                            <option value = 'getBranches' data-method = 'get' data-valid = '1'> Get Branches </option>
                            <option value = 'getShareholders' data-method = 'get' data-valid = '1'> Get Shareholders </option>
                            <option value = 'getDirectors' data-method = 'get' data-valid = '1'> Get Directors </option>
                            <option value = 'getCertifications' data-method = 'get' data-valid = '1'> Get Certifications </option>
                            <option value = 'getImportExport' data-method = 'get' data-valid = '1'> Get Import / Export </option>
                            <option value = 'getInsurance' data-method = 'get' data-valid = '1'> Get Insurance </option>
                            <option value = 'getCashConversion' data-method = 'get' data-valid = '1'> Get Cash Conversion Cycle </option>
                            <option value = 'getFsDocuments' data-method = 'get' data-valid = '1'> Get Financial Documents </option>
                            <option value = 'getBankStatements' data-method = 'get' data-valid = '1'> Get Bank Statements </option>
                            <option value = 'getUtilityBills' data-method = 'get' data-valid = '1'> Get Utility Bills </option>
                            <option value = 'getMajorCustomer' data-method = 'get' data-valid = '1'> Get Major Customer </option>
                            <option value = 'getMajorSupplier' data-method = 'get' data-valid = '1'> Get Major Supplier </option>
                            <option value = 'getMajorLocations' data-method = 'get' data-valid = '1'> Get Major Locations </option>
                            <option value = 'getCompetitors' data-method = 'get' data-valid = '1'> Get Competitors </option>
                            <option value = 'getBizDriver' data-method = 'get' data-valid = '1'> Get Business Driver </option>
                            <option value = 'getBizSegment' data-method = 'get' data-valid = '1'> Get Customer Segment </option>
                            <option value = 'getPastProj' data-method = 'get' data-valid = '1'> Get Past Projects </option>
                            <option value = 'getFutureInit' data-method = 'get' data-valid = '1'> Get Future Initiatives </option>
                            <option value = 'getOwnerDetails' data-method = 'get' data-valid = '1'> Get Owner Details </option>
                            <option value = 'getPersonalLoans' data-method = 'get' data-valid = '1'> Get Owner Loans </option>
                            <option value = 'getOtherBusiness' data-method = 'get' data-valid = '1'> Get Other Business </option>
                            <option value = 'getEducation' data-method = 'get' data-valid = '1'> Get Education </option>
                            <option value = 'getSpouse' data-method = 'get' data-valid = '1'> Get Spouse </option>
                            <option value = 'getKeyManager' data-method = 'get' data-valid = '1'> Get Key Manager </option>
                            <option value = 'getEcf' data-method = 'get' data-valid = '1'> Get Existing Credit Facilities </option>
                            <option value = 'getRcl' data-method = 'get' data-valid = '1'> Get Required Credit Line </option>
                            <option value = 'getBusinessCondtion' data-method = 'get' data-valid = '1'> Get Business Condition </option>
                            <option value = 'getRiskAssessment' data-method = 'get' data-valid = '1'> Get Risk Assessment </option>
                            <option value = 'getGrowthPotential' data-method = 'get' data-valid = '1'> Get Revenue Growth Potential </option>
                            <option value = 'getInvestmentExpansion' data-method = 'get' data-valid = '1'> Get Capital Investment for Expansion </option>
                            <option value = 'getReports' data-method = 'get' data-valid = '1'> Get Account Reports </option>

							<option value = 'editBusinessDetails' data-method = 'post' data-valid = '1'> Edit Business Details - Valid </option>
                            <option value = 'editBusinessDetails' data-method = 'post' data-valid = '0'> Edit Business Details - Invalid </option>

							<option value = 'editAffiliates' data-method = 'post' data-valid = '1'> Editing Affiliates - Valid </option>
                            <option value = 'editAffiliates' data-method = 'post' data-valid = '0'> Editing Affiliates - Invalid </option>

                            <option value = 'editProducts' data-method = 'post' data-valid = '1'> Editing Products - Valid </option>
                            <option value = 'editProducts' data-method = 'post' data-valid = '0'> Editing Products - Invalid </option>

                            <option value = 'editMainLocation' data-method = 'post' data-valid = '1'> Editing Main Location - Valid </option>
                            <option value = 'editMainLocation' data-method = 'post' data-valid = '0'> Editing Main Location - Invalid </option>

                            <option value = 'editBranches' data-method = 'post' data-valid = '1'> Editing Branches - Valid </option>
                            <option value = 'editBranches' data-method = 'post' data-valid = '0'> Editing Branches - Invalid </option>

                            <option value = 'editShareholders' data-method = 'post' data-valid = '1'> Editing Shareholders - Valid </option>
                            <option value = 'editShareholders' data-method = 'post' data-valid = '0'> Editing Shareholders - Invalid </option>

                            <option value = 'editDirectors' data-method = 'post' data-valid = '1'> Editing Directors - Valid </option>
                            <option value = 'editDirectors' data-method = 'post' data-valid = '0'> Editing Directors - Invalid </option>

                            <option value = 'editImportExport' data-method = 'post' data-valid = '1'> Editing Import / Export - Valid </option>
                            <option value = 'editImportExport' data-method = 'post' data-valid = '0'> Editing Import / Export - Invalid </option>

                            <option value = 'editInsurance' data-method = 'post' data-valid = '1'> Editing Insurance - Valid </option>
                            <option value = 'editInsurance' data-method = 'post' data-valid = '0'> Editing Insurance - Invalid </option>

                            <option value = 'editMajorCustomer' data-method = 'post' data-valid = '1'> Editing Major Customer - Valid </option>
                            <option value = 'editMajorCustomer' data-method = 'post' data-valid = '0'> Editing Major Customer - Invalid </option>

                            <option value = 'editMajorSupplier' data-method = 'post' data-valid = '1'> Editing Major Supplier - Valid </option>
                            <option value = 'editMajorSupplier' data-method = 'post' data-valid = '0'> Editing Major Supplier - Invalid </option>

                            <option value = 'editMajorLocation' data-method = 'post' data-valid = '1'> Editing Major Location - Valid </option>
                            <option value = 'editMajorLocation' data-method = 'post' data-valid = '0'> Editing Major Location - Invalid </option>

                            <option value = 'editCompetitors' data-method = 'post' data-valid = '1'> Editing Competitors - Valid </option>
                            <option value = 'editCompetitors' data-method = 'post' data-valid = '0'> Editing Competitors - Invalid </option>

                            <option value = 'editBusinessDrivers' data-method = 'post' data-valid = '1'> Editing Business Drivers - Valid </option>
                            <option value = 'editBusinessDrivers' data-method = 'post' data-valid = '0'> Editing Business Drivers - Invalid </option>

                            <option value = 'editPastProject' data-method = 'post' data-valid = '1'> Editing Past Project - Valid </option>
                            <option value = 'editPastProject' data-method = 'post' data-valid = '0'> Editing Past Project - Invalid </option>

                            <option value = 'editFutureGrowth' data-method = 'post' data-valid = '1'> Editing Future Initiative - Valid </option>
                            <option value = 'editFutureGrowth' data-method = 'post' data-valid = '0'> Editing Future Initiative - Invalid </option>

                            <option value = 'editRiskAssement' data-method = 'post' data-valid = '1'> Editing Risk Assessment - Valid </option>
                            <option value = 'editRiskAssement' data-method = 'post' data-valid = '0'> Editing Risk Assessment - Invalid </option>

                            <option value = 'editRevenueGrowth' data-method = 'post' data-valid = '1'> Editing Revenue Growth Potential - Valid </option>
                            <option value = 'editRevenueGrowth' data-method = 'post' data-valid = '0'> Editing Revenue Growth Potential - Invalid </option>

                            <option value = 'editCapitalExpansion' data-method = 'post' data-valid = '1'> Editing Capital for Expansion - Valid </option>
                            <option value = 'editCapitalExpansion' data-method = 'post' data-valid = '0'> Editing Capital for Expansion - Invalid </option>

                            <option value = 'editRequiredCredit' data-method = 'post' data-valid = '1'> Editing Required Credit Line - Valid </option>
                            <option value = 'editRequiredCredit' data-method = 'post' data-valid = '0'> Editing Required Credit Line - Invalid </option>

                            <option value = 'editOwnerDetails' data-method = 'post' data-valid = '1'> Editing Owner Details - Valid </option>
                            <option value = 'editOwnerDetails' data-method = 'post' data-valid = '0'> Editing Owner Details - Invalid </option>

                            <option value = 'editPersonalLoans' data-method = 'post' data-valid = '1'> Editing Personal Loans - Valid </option>
                            <option value = 'editPersonalLoans' data-method = 'post' data-valid = '0'> Editing Personal Loans - Invalid </option>

                            <option value = 'editKeyManager' data-method = 'post' data-valid = '1'> Editing Key Manager - Valid </option>
                            <option value = 'editKeyManager' data-method = 'post' data-valid = '0'> Editing Key Manager - Invalid </option>

                            <option value = 'editOwnerOtherBusiness' data-method = 'post' data-valid = '1'> Editing Other Business - Valid </option>
                            <option value = 'editOwnerOtherBusiness' data-method = 'post' data-valid = '0'> Editing Other Business - Invalid </option>

                            <option value = 'editSpouseOtherBusiness' data-method = 'post' data-valid = '1'> Editing Spouse Other Business - Valid </option>
                            <option value = 'editSpouseOtherBusiness' data-method = 'post' data-valid = '0'> Editing Spouse Other Business - Invalid </option>

                            <option value = 'editOwnerEducation' data-method = 'post' data-valid = '1'> Editing Education - Valid </option>
                            <option value = 'editOwnerEducation' data-method = 'post' data-valid = '0'> Editing Education - Invalid </option>

                            <option value = 'editSpouseOwnerEducation' data-method = 'post' data-valid = '1'> Editing Spouse Education - Valid </option>
                            <option value = 'editSpouseOwnerEducation' data-method = 'post' data-valid = '0'> Editing Spouse Education - Invalid </option>

                            <option value = 'editExistingCredit' data-method = 'post' data-valid = '1'> Editing Existing Credit - Valid </option>
                            <option value = 'editExistingCredit' data-method = 'post' data-valid = '0'> Editing Existing Credit - Invalid </option>

                            <option value = 'delAffiliates' data-method = 'get' data-valid = '1'> Delete Affiliates </option>
                            <option value = 'delProducts' data-method = 'get' data-valid = '1'> Delete Products / Services </option>
                            <option value = 'delCapital' data-method = 'get' data-valid = '1'> Delete Capital Details </option>
                            <option value = 'delBranches' data-method = 'get' data-valid = '1'> Delete Branches </option>
                            <option value = 'delShareholders' data-method = 'get' data-valid = '1'> Delete Shareholders </option>
                            <option value = 'delDirectors' data-method = 'get' data-valid = '1'> Delete Directors </option>
                            <option value = 'delCertifications' data-method = 'get' data-valid = '1'> Delete Certifications </option>
                            <option value = 'delImportExport' data-method = 'get' data-valid = '1'> Delete Import / Export </option>
                            <option value = 'delInsurance' data-method = 'get' data-valid = '1'> Delete Insurance </option>
                            <option value = 'delCashCycle' data-method = 'get' data-valid = '1'> Delete Cash Conversion Cycle </option>
                            <option value = 'delFsDocument' data-method = 'get' data-valid = '1'> Delete FS Document </option>
                            <option value = 'delBankStatement' data-method = 'get' data-valid = '1'> Delete Bank Statement </option>
                            <option value = 'delUtilityBills' data-method = 'get' data-valid = '1'> Delete Utility Bills </option>
                            <option value = 'delMajorCustomers' data-method = 'get' data-valid = '1'> Delete Major Customers </option>
                            <option value = 'delMajorSuppliers' data-method = 'get' data-valid = '1'> Delete Major Suppliers </option>
                            <option value = 'delMajorLoc' data-method = 'get' data-valid = '1'> Delete Major Locations </option>
                            <option value = 'delCompetitor' data-method = 'get' data-valid = '1'> Delete Competitors </option>
                            <option value = 'delBizDriver' data-method = 'get' data-valid = '1'> Delete Business Driver </option>
                            <option value = 'delBizSegments' data-method = 'get' data-valid = '1'> Delete Customer Segments </option>
                            <option value = 'delPastProj' data-method = 'get' data-valid = '1'> Delete Past Projects </option>
                            <option value = 'delFutureInit' data-method = 'get' data-valid = '1'> Delete Future Initiative </option>
                            <option value = 'delOwnerDetails' data-method = 'get' data-valid = '1'> Delete Owner Details </option>
                            <option value = 'delPersonalLoans' data-method = 'get' data-valid = '1'> Delete Personal Loans </option>
                            <option value = 'delKeyManager' data-method = 'get' data-valid = '1'> Delete Key Managers </option>
                            <option value = 'delOtherBusiness' data-method = 'get' data-valid = '1'> Delete Other Business </option>
                            <option value = 'delEducation' data-method = 'get' data-valid = '1'> Delete Education </option>
                            <option value = 'delSpouse' data-method = 'get' data-valid = '1'> Delete Spouse </option>
                            <option value = 'delEcf' data-method = 'get' data-valid = '1'> Delete Exisiting Credit Facilities </option>
                            <option value = 'delRcl' data-method = 'get' data-valid = '1'> Delete Required Credit Line </option>
                            <option value = 'delBusinessCondtion' data-method = 'get' data-valid = '1'> Delete Business Condition </option>
                            <option value = 'delRiskMng' data-method = 'get' data-valid = '1'> Delete Risk Assessment </option>
                            <option value = 'delGrowthPotential' data-method = 'get' data-valid = '1'> Delete Revenue Growth Potential </option>
                            <option value = 'delExpansionInvestment' data-method = 'get' data-valid = '1'> Delete Capital Investment for Expansion </option>

                            <option value = 'getQuestionnaireConfig' data-method = 'post' data-valid = '1'> Get Questionnaire Configuration </option>
                            <option value = 'getReportCompletion' data-method = 'post' data-valid = '1'> Get Report Completion </option>
                            <option value = 'getProvinces' data-method = 'post' data-valid = '1'> Get Provinces </option>
                            <option value = 'getCities' data-method = 'post' data-valid = '1'> Get Cities </option>
                            <option value = 'getIndustries' data-method = 'post' data-valid = '1'> Get Industries </option>
                            <option value = 'getIndustriesSub' data-method = 'post' data-valid = '1'> Get Industries Sub </option>
                            <option value = 'getIndustriesRow' data-method = 'post' data-valid = '1'> Get Industries Row </option>
                            <option value = 'getBankList' data-method = 'post' data-valid = '1'> Get Banks </option>
                            <option value = 'getMajorCities' data-method = 'post' data-valid = '1'> Get Major Cities </option>
                            <option value = 'getCompletedReports' data-method = 'post' data-valid = '1'> Get Completed Reports </option>

                        </select>

                        <br/>
                    </div>

                    <p>SENT HEADER</p>
                    <div id = 'hdr-sent' class="form-group" style = 'border: 1px solid #000000; margin: 20px; padding: 20px;'> </div>

                    <p>SENT DATA</p>
                    <div id = 'post-fld' class="form-group" style = 'border: 1px solid #000000; margin: 20px; padding: 20px;'> </div>

                    <p>RECEIVED HEADER</p>
                    <div id = 'resp-hdr' class="form-group" style = 'border: 1px solid #000000; margin: 20px; padding: 20px;'> </div>
				</div>

			</div>
		</div>

        <div class="row">
			<div class="col-sm-6">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" id="test-run-btn" class="btn btn-large btn-primary" value="TEST">
				</div>
			</div>

		</div>
		{{ Form::close() }}
	</div>
@stop

@section('javascript')
	<script>
        $(document).ready(function() {
            $('#test-run-btn').click(function() {
                var sel_service = $('select[name="services"]').find('option:selected');
                var valid_val   = sel_service.attr('data-valid');

                $('#hdr-sent').html('');
                $('#post-fld').html('');
                $('#resp-hdr').html('');

                if ('post' == sel_service.data('method')) {
                    $.get(BaseURL+'/api-test-post',
                    {
                        'service':  sel_service.val(),
                        'valid':    valid_val
                    },
                    function(result) {
                        var send_msg = '';
                        var post_msg = '';

                        $('#hdr-sent').html('Content Type: '+result.sent_hdr['content_type']+'<br/>');
                        $('#hdr-sent').append('Download Content Length: '+result.sent_hdr['download_content_length']+'<br/>');
                        $('#hdr-sent').append('Header Size: '+result.sent_hdr['header_size']+'<br/>');
                        $('#hdr-sent').append('HTTP Code: '+result.sent_hdr['http_code']+'<br/>');
                        $('#hdr-sent').append('Request Header: '+result.sent_hdr['request_header']+'<br/>');
                        $('#hdr-sent').append('URL: '+result.sent_hdr['url']+'<br/>');

                        for (idx = 0; idx < result.post_fld.length; idx++) {
                            post_msg += result.post_fld[idx] +'<br/>';
                        }

                        $('#post-fld').html(post_msg);
                        $('#resp-hdr').html('<p style = "word-wrap: break-word;">'+result.response+'</p>');
                    }, 'json');
                }
                else {
                    $.get(BaseURL+'/api-test-get',
                    {
                        'service':  sel_service.val()
                    },
                    function(result) {
                        var resp_msg = '';

                        $('#hdr-sent').html('');
                        $('#post-fld').html(result.full_url);
                        $('#resp-hdr').html('<p style = "word-wrap: break-word;">'+result.response+'</p>');

                    }, 'json');
                }

                return false;
            });
        });
    </script>
@stop
