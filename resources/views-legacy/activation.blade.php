@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div id="loginwrapper" class="container">
	{{ Form::model($activationcode, ['route' => ['postActivationCode', $activationcode], 'method' => 'post', 'id' => 'login']) }}
	<input type="hidden" name="activationcode" value="{{ $activationcode }}" />
	<div class="form-group">
		<input type="button" value="New Users Click Here to Sign Up" onclick="javascript: location.href='{{ URL::to('/') }}/signup'" class="btn btn-large btn-primary" style="font-weight: normal;font-size: 26px;width: 100%;" />
	</div>

	<h3>Member Login</h3>

	<div class="form-group{{ ($errors->has('email')) ? ' has-error' : '' }}">
	    {{ Form::label('email', 'Email Address') }}
	    {{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}

		@if($errors->has('email'))
			<span class="errormessage">{{ $errors->first('email') }}</span>
		@endif
	</div>

	<div class="form-group{{ ($errors->has('username')) ? ' has-error' : '' }}">
	    {{ Form::label('password', 'Password') }}
	    {{ Form::password('password', array('class' => 'form-control')) }}

		@if($errors->has('password'))
			<span class="errormessage">{{ $errors->first('password') }}</span>
		@endif
	</div>

	<div class="form-group">
		{{ Form::submit('Log In',array('class'=>'btn btn-large btn-primary')) }}
	</div>

	{{ Form::close() }}
@stop
