@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('style')
 	<style>
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			font-size: 14px !important;
		}
		ul#select2-bank_id-results{
			font-size: 14px !important;
		}
		.centered-modal.in {
			display: flex !important;
		}
		.centered-modal .modal-dialog {
			margin: auto;
		}
		#ui-id-1 {
			height: auto;
			max-height: 300px;
			overflow-x: hidden;
		}
    </style>
@stop

@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Create Free Trial Account</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postCreateFreeTrialAccount')) }}
					<div class="form-group">
						{{Form::label('name', 'Name')}}
						{{ Form::text('name', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						 {{ Form::label('bank_name', "Organization Name") }}
						{{ Form::text('bank_name', '', array('class'=>'form-control typeahead', 'required' => 'required')) }}
					</div>

					{{-- <div class="form-group">
						{{Form::label('bank_id', 'Organization')}}
						{{Form::select('bank_id', $banks, '', array('class'=>'form-control'))}}
					</div> --}}

					<div class="form-group">
                        {{Form::label('bank_type', 'Organization type')}}
                        {{Form::select('bank_type', array(
									'Bank'=>'Bank',
									'Non-Bank Financing'=>'Non-Bank Financing',
									'Corporate'=>'Corporate',
									'Government'=>'Government'
							  ), 'bank_type', array('class' => 'form-control'))}}
					</div>

					<div class="form-group">
						{{ Form::hidden('bank_type_hidden', 'Bank', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('phone', "Phone No.") }}
						{{ Form::text('phone', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('email', "Email") }}
						{{ Form::email('email', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('basic_username', "Basic User Name") }}
						{{ Form::text('basic_username', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('basic_email', "Basic User Email") }}
						{{ Form::email('basic_email', '', array('class'=>'form-control', 'required' => 'required')) }}
						<p><em><small>This will be used to login to basic user account. Password will be sent via email</small></em></p>
					</div>

					<div class="form-group">
						{{ Form::label('supervisor_username', "Supervisor User Name") }}
						{{ Form::text('supervisor_username', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('supervisor_email', "Supervisor User Email") }}
						{{ Form::email('supervisor_email', '', array('class'=>'form-control', 'required' => 'required')) }}
						<p><em><small>This will be used to login to supervisor account. Password will be sent via email</small></em></p>
					</div>

					<div class="form-group">
						{{ Form::submit('Create', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap3-typeahead.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.typeahead').autocomplete({
		 showHintOnFocus: true,
		 source: BaseURL+'/search_companies', 
		 minLength: 0,
		 items: 9999,
	});

	$(".typeahead").on("click", function () {
    ev = $.Event("keydown")
    ev.keyCode = ev.which = 40
    $(this).trigger(ev)
    return true
});

$('#bank_name').change(function(){
		var bank_name = $(this).val();

		$.ajax({
			method: 'GET',
			url: BaseURL+'/organization-type/'+ bank_name,
			success: function(response){
				if (response == 0) {
					$("#bank_type").removeAttr("disabled");
					$("#bank_type").val('Bank');
					$("input[name='bank_type_hidden']").val('Bank');
				} else {
					bank_type = response['bank_type'];
					var optionExists = ($("#bank_type option[value='" + bank_type + "']").length > 0);

					if (bank_type != '') {
						if (optionExists) {
							$("#bank_type").val(bank_type);
							$("#bank_type").attr("disabled", true)
							$("input[name='bank_type_hidden']").val(bank_type);
						} else {
							$('#bank_type').append("<option value='"+bank_type+"'>"+bank_type+"</option>");
							$("#bank_type").val(bank_type);
							$("#bank_type").attr("disabled", true);
							$("input[name='bank_type_hidden']").val(bank_type);
						}
					}
					else {
						$("#bank_type").removeAttr("disabled");
						$("#bank_type").val('Bank');
						$("input[name='bank_type_hidden']").val('Bank');
					}
				}
			}
		})
	});

	$("#bank_type").change(function(){
        var selected = $(this).children("option:selected").val();
        $("input[name='bank_type_hidden']").val(selected);
    });
	 
});

</script>
@stop