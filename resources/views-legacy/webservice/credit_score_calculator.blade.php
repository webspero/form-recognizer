@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
	<style>
        .ui-dialog-titlebar-close .ui-button-text { height: 14px !important; }
        body { font-size:14px;line-height:22px;color:#777777}
        .more-info-link, .modal-title, .text-succes, .icon {color:#13b36b}
        .img-responsive{padding-bottom:10px}
        .margin_left{margin-left:15px;background-color:#13b36b; border-color:#13b36b}
        .margin_left:hover, .btn:hover{background-color:#d2322d; border-color:#13b36b}
        .btn {background-color:#13b36b; border-color:#13b36b}
        .spacing_left{margin-left:15px}
        #dial-needle {
            position: absolute;
            float: left;
            left: 34%;
            top: 58%;
            z-index: 1000;
        }

        #dial-needle-mb {
            z-index: 1000;
        }

        .dial-img {
            -ms-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        .score-summary,
        .score-difference,
        .score-rating-label {
            text-align: center;
        }
    </style>
    
@stop
@section('nav')

@show
@section('content')
    <div class="container">
        <div class="row"> </div>
        <div class="row">
        
        <div style = 'display: none;' id = 'fb-actions-cntr'>
            <div id = 'fb-like-btn' style = 'display: none;'>
                <div>Your Personal Credit Rating will be displayed after you click on the "LIKE" button below. </div>
                <div class="fb-like" data-href="https://www.facebook.com/CreditBPO/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
        </div>

        <div style = 'display: none;' id = 'loading-cntr'>
            <span> Loading results... </span>
        </div>

        <div id = 'credit-calculator-cntr'>
            <h3 class="text-success"> Personal Credit Score Calculator </h3>
            <br>
            <p>As advocates of responsible credit, we here at CreditBPO have deployed a personal credit score calculator for free. We hope you use it well. </p>
            <p><span > <b> Note: This is not the same as your business credit rating. </b> </span> </p>
        </div>
        <div class="row">
            <form id="cred-calc-form" class="form-row" >
                <div class="col-lg-6 col-md-6 col-sm-6"> 
                    <div class="form-group"> 
                        <label > Your total Revolving Credit balance? *</label>
                        <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="This is how much money you have left unpaid across your available revolving credit. Only credit card balances and similar are included – no car, education, or house loans. For example, if you have PHP 3,000 of a PHP 100,000 limit unpaid in one card and PHP 6,000 of a PHP 100,000 limit in another card then you have a total balance of PHP 9,000."> More info... </a>
                        <input class="form-control" name="total-balance" type="text" required>
                    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                
                    <div class="form-group"> 
                        <label> Your total Revolving Credit limit? * </label>
                        <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="This is the total limit of all your available revolving credit sources. For example, if you have 3 credit cards with a limit of PHP 100,000 each, then you have a total credit limit of PHP 300,000."> More info... </a>
                        <input class="form-control" name="total-limit" type="text" required>
                    </div> 
                            
                    <div class="form-group"> 
                        <label> How many Credit accounts do you have? </label>
                        <select name="num-accnt" class="form-control form-text">
                            <option value="1"> 1</option>
                            <option value="2"> 2 - 3</option>
                            <option value="3"> 4 - 5</option>
                            <option value="4"> 5 - 6</option><option value="5"> 6 above</option> 
                        </select>
                    </div>   
                    
                    <div class="form-item"> 
                        <span class="goleft"> Types of Consumer Finance readily available to you </span> <br> 
                        <input type="checkbox" name="credit-type[]" value="installment"> Installment <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="A loan that is repaid by the borrower in regular installments wherein, in a set period, a little bit is paid off. Examples include car, education, house, and other large items though certain expensive appliances such as computers and stereo systems may be included."> More info... </a> <br> 
                        <input type="checkbox" name="credit-type[]" value="revolving"> Revolving <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="A line of credit where the customer pays a fee to gain access to funds when needed. These funds are also paid off by the customer at a later date. The most common example of this is the credit card."> More info... </a> <br> 
                        <input type="checkbox" name="credit-type[]" value="consumer_finance"> Consumer Finance <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="A type of subprime loan given to borrowers by certain companies. The terms are less advantageous to the client than usual as individuals with subprime credit histories are the usual customers here."> More info... </a> <br> 
                        <input type="checkbox" name="credit-type[]" value="mortgage"> Mortgage <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="An instrument of debt with a predetermined payment schedule. Collateral in the form of a specified real estate property is used to secure the loan. Mortgages are used to make major real estate purchases while avoiding paying full value up front. Alternative terms include “claims on property” or “liens against property&quot;"> More info... </a> <br>
                    </div> 

                    <br>
                    <div class="form-item"> 
                        <span class="goleft"> Average age (in years) of your existing consumer finance accounts? </span> 
                        <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="Average age of all currently accessible consumer financing. This includes a wide variety of loans, including credit cards, mortgage loans, and auto loans, and can also be used to refer to loans taken out at either the prime rate or the subprime rate."> More info... </a> 
                        <input type="text" name="ave-age" class="form-text form-control"></div><div class="form-item"> 
                    </div>

                    <br>
                    <div class="form-item"> 
                        <span class="goleft"> Age (in years) of your oldest existing consumer finance accounts? </span>
                        <a href="#" class="pull-right more-info-link" data-toggle="modal" data-target="#fields-info" data-hint="Age of oldest consumer financing currently available. This includes a wide variety of loans, including credit cards, mortgage loans, and auto loans, and can also be used to refer to loans taken out at either the prime rate or the subprime rate."> More info... </a> 
                        <input type="text" name="oldest-accnt" class="form-text form-control">
                    </div>
                </div>

                
                <div class="col-lg-6 col-md-6 col-sm-6"> 
                    <img class="img-responsive" src="https://creditbpo.com/sites/default/files/Credit-Score.jpg" alt="Credit rating">
                    <div class="form-group">
                        <label for="number_applied"> How many times have you applied for consumer credit within the past year? </label>
                        <select name="number_applied" class="form-control form-text">
                            <option value="10"> None</option>
                            <option value="9"> 1 or 2 times</option>
                            <option value="7"> 3 or 5 times</option>
                            <option value="5"> 6 or 8 times</option>
                            <option value="3"> 9 times or more</option> 
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="bad-event"> Have you had any of the following in the past: Bankruptcy, liens, judgments, settlements, charge offs, repossessions, foreclosures, and late payments? </label>
                        <select name="bad-event" class="form-control form-text">
                            <option value="35"> No</option>
                            <option value="20"> Yes</option> 
                        </select>
                    </div> 
                </div>
                <div class="clearfix"> </div>
                <div class="form-actions"> 
                    <input type="submit" id="submit-credit-btn" class="btn-primary btn margin_left" value="Calculate Personal Credit Rating">
                </div>
            </form>
            <div class = 'row'>
                    <div class = 'col-md-12'>
                        <p class="spacing_left"> Content on CreditBPO.rcom is not sponsored by any bank or credit card issuer. The free personal scoring calculator here is based on non-validated information supplied by an interested party (i.e., you). It should not be used as the only source of a personal credit score. <br/><b>Note: This is not the same as your business credit rating.</b> </p>
                    </div>
            </div>

            <div class="modal fade" id="fb-login-main" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"> Log In to Facebook </h4>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <div>You must log in to facebook to view your Personal Credit Rating.</div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class = 'btn' style = 'background-color: #4e69a2; border-color: #435a8b #3c5488 #334c83; color: #fff' id = 'fb-login-btn'> <i class = 'icon icon-facebook'> </i> Login on Facebook </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="fields-info" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"> <i class = 'glyphicon glyphicon-info-sign'> </i> Information </h4>
                        </div>

                        <div class="modal-body">
                            <div class="container-fluid">
                                <div id = 'info-content'></div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class = 'btn btn-primary' data-dismiss="modal"> Close </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
<script src="https://code.highcharts.com/4.1.0/highcharts.js"></script>
<script src="https://code.highcharts.com/4.1.0/modules/exporting.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/webservice/credit_score_calculator.js') }}"></script>
@stop