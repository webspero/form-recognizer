    <div class="modal" id="refresh-page-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ">
                        <span class = 'icon-more-info glyphicon glyphicon-exclamation-sign' style = 'color: #2a99c6;'> </span> &nbsp;
                        <b>Warning: The page did not load completely</b>
                    </h4>
                </div>    
                <div class="modal-body">
					<p style="word-wrap: break-word;">
                        You maybe experiencing a slow or intermittent connection as of the moment.
                        </br>
                        <br/>
                        If this problem persists, contact your Internet Service Provider or move to a location with better connection.
                        <br/>
                        <br/>
                        This page will refresh in <span id = 'countdown-refresh-num' style = 'font-weight: bold;'> 5... </span>
                    </p>
                </div>
            </div>
        </div>
    </div>