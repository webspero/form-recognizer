@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<a href="{{URL::to('/fr/upload_vfa')}}" class="btn btn-primary">Upload Vfa</a>
				<a href="{{URL::to('/fr/upload_excel')}}" class="btn btn-primary">Upload Excel Template</a>
				<a href="{{URL::to('/fr/upload_fs_input')}}" class="btn btn-primary">Upload FS Input Template</a>
				<a href="{{URL::to('/fr/clear')}}" class="btn btn-primary">Clear All</a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h3>Reports</h3>
				<ul>
					@foreach($financial_reports as $fr)
					@if(count($fr->keyRatio) > 0)
						<li>
							<a href="{{URL::to('/fr/key-ratio-view')}}/{{$fr->id}}">{{$fr->title}}</a>
						</li>
					@else
						<li>
							<a href="{{URL::to('/fr/view')}}/{{$fr->id}}">{{$fr->title}}</a>
						</li>
					@endif
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
