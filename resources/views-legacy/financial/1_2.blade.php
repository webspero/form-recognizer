<h3>1.2. Net Assets (Net Worth)</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="3">Indicator</td>
            <td class="center" colspan="{{ @count($balance_sheets) + 2 }}">Value</td>
            <td class="center" colspan="2">Change</td>
        </tr>
        <tr dontbreak="true">
            <td class="center" colspan="2">in thousand PHP</td>
            <td class="center" colspan="{{ @count($balance_sheets) }}">% of the balance total</td>
            <td class="center" rowspan="2"><i>thousand PHP</i> (col.3-col.2)</td>
            <td class="center" rowspan="2">% ((col.3-col.2) : col.2)</td>
        </tr>
        <tr dontbreak="true">
            <td class="center">at the beginning of the period analyzed (12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td class="center">at the end of the period analyzed (12/31/{{ (int)$financial_report->year }})</td>
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>1. Net tangible assets</td>
            <?php
                // IntangibleAssets
                $IntangibleAssets = $balance_sheets[@count($balance_sheets) - 1]->Goodwill
                    + $balance_sheets[@count($balance_sheets) - 1]->IntangibleAssetsOtherThanGoodwill;
                // NetTangibleAssets
                $NetTangibleAssets = $balance_sheets[@count($balance_sheets) - 1]->Equity - $IntangibleAssets;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets, 0, '.', ',')}}
            </td>
            <?php
                // IntangibleAssets
                $IntangibleAssets2 = $balance_sheets[0]->Goodwill
                    + $balance_sheets[0]->IntangibleAssetsOtherThanGoodwill;
                // NetTangibleAssets
                $NetTangibleAssets2_r = $balance_sheets[0]->Equity - $IntangibleAssets2;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets2_r, 0, '.', ',')}}
            </td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    // IntangibleAssets
                    $IntangibleAssets3 = $balance_sheets[$x]->Goodwill
                        + $balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill;
                    // NetTangibleAssets
                    $NetTangibleAssets3 = $balance_sheets[$x]->Equity - $IntangibleAssets3;
                    $Balance = ($NetTangibleAssets3 / $balance_sheets[$x]->EquityAndLiabilities) * 100;
                ?>
                <td class="center {{ $Balance > 0 ? 'green' : 'red' }}">
                    {{number_format($Balance, 1, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change_r = $NetTangibleAssets2_r - $NetTangibleAssets;
            ?>
            <td class="center {{ $Change_r > 0 ? 'green' : 'red' }}">
                {{ $Change_r > 0 ? '+' : '' }}{{ number_format($Change_r, 0, '.', ',') }}
            </td>
            <?php
                if($NetTangibleAssets != 0)
                    $ChangePercentage_r = (($NetTangibleAssets2_r / $NetTangibleAssets) - 1) * 100;
                else 
                    $ChangePercentage_r = 0;
            ?>
            <td class="center {{ $Change_r > 0 ? 'green' : 'red' }}">
                @if($ChangePercentage_r < 200)
                    {{ $ChangePercentage_r > 0 ? '+' : '' }}{{ number_format($ChangePercentage_r, 1, '.', ',') }}
                @else
                    {{ $ChangePercentage_r > 0 ? '+' : '' }}{{ number_format(($ChangePercentage_r / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Net assets (Net worth)</td>
            <?php
                $NetTangibleAssets = $balance_sheets[@count($balance_sheets) - 1]->Equity;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets, 0, '.', ',')}}
            </td>
            <?php
                $NetTangibleAssets2_r2 = $balance_sheets[0]->Equity;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets2_r2, 0, '.', ',')}}
            </td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $NetTangibleAssets3 = $balance_sheets[$x]->Equity;
                    $Balance = ($NetTangibleAssets3 / $balance_sheets[$x]->EquityAndLiabilities) * 100;
                ?>
                <td class="center {{ $Balance > 0 ? 'green' : 'red' }}">
                    {{number_format($Balance, 1, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change = $NetTangibleAssets2_r2 - $NetTangibleAssets;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                {{ $Change > 0 ? '+' : '' }}{{ number_format($Change, 0, '.', ',') }}
            </td>
            <?php
                if($NetTangibleAssets !=0)
                    $ChangePercentage = (($NetTangibleAssets2_r2 / $NetTangibleAssets) - 1) * 100;
                else
                    $ChangePercentage = 0;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                @if($ChangePercentage < 200)
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format($ChangePercentage, 1, '.', ',') }}
                @else
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format(($ChangePercentage / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>3. Issued (share) capital</td>
            <?php
                $NetTangibleAssets = $balance_sheets[@count($balance_sheets) - 1]->IssuedCapital;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets, 0, '.', ',')}}
            </td>
            <?php
                $NetTangibleAssets2_r3 = $balance_sheets[0]->IssuedCapital;
            ?>
            <td class="center">
                {{number_format($NetTangibleAssets2_r3, 0, '.', ',')}}
            </td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $NetTangibleAssets3 = $balance_sheets[$x]->IssuedCapital;
                    $Balance = ($NetTangibleAssets3 / $balance_sheets[$x]->EquityAndLiabilities) * 100;
                ?>
                <td class="center {{ $Balance > 0 ? 'green' : 'red' }}">
                    {{number_format($Balance, 1, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change = $NetTangibleAssets2_r3 - $NetTangibleAssets;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                {{ $Change > 0 ? '+' : '' }}{{ number_format($Change, 0, '.', ',') }}
            </td>
            <?php
                if($NetTangibleAssets !=0)
                    $ChangePercentage = (($NetTangibleAssets2_r3 / $NetTangibleAssets) - 1) * 100;
                else
                    $ChangePercentage =0;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                @if($ChangePercentage < 200)
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format($ChangePercentage, 1, '.', ',') }}
                @else
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format(($ChangePercentage / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>4. Difference between net assets and Issued (share) capital (line 2 - line 3)</td>
            <?php
                $NetTangibleAssets = $balance_sheets[@count($balance_sheets) - 1]->Equity -
                    $balance_sheets[@count($balance_sheets) - 1]->IssuedCapital;
            ?>
            <td class="center {{ $NetTangibleAssets > 0 ? 'green' : 'red' }}">
                {{number_format($NetTangibleAssets, 0, '.', ',')}}
            </td>
            <?php
                $NetTangibleAssets2 = $balance_sheets[0]->Equity - 
                    $balance_sheets[0]->IssuedCapital;
            ?>
            <td class="center {{ $NetTangibleAssets2 > 0 ? 'green' : 'red' }}">
                {{number_format($NetTangibleAssets2, 0, '.', ',')}}
            </td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $NetTangibleAssets3 = $balance_sheets[$x]->Equity - $balance_sheets[$x]->IssuedCapital;
                    $Balance = ($NetTangibleAssets3 / $balance_sheets[$x]->EquityAndLiabilities) * 100;
                ?>
                <td class="center {{ $Balance > 0 ? 'green' : 'red' }}">
                    {{number_format($Balance, 1, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change = $NetTangibleAssets2 - $NetTangibleAssets;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                {{ $Change > 0 ? '+' : '' }}{{ number_format($Change, 0, '.', ',') }}
            </td>
            <?php
                $ChangePercentage = (($NetTangibleAssets2 / $NetTangibleAssets) - 1) * 100;
            ?>
            <td class="center {{ $Change > 0 ? 'green' : 'red' }}">
                @if($ChangePercentage < 200)
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format($ChangePercentage, 1, '.', ',') }}
                @else
                    {{ $ChangePercentage > 0 ? '+' : '' }}{{ number_format(($ChangePercentage / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">On the last day of the period analyzed, the net tangible assets equaled PHP <span style="color: GREEN">{{number_format($NetTangibleAssets2_r, 0, '.', ',')}} </span>thousand.
For the {{@count($balance_sheets) - 1}} years, the net tangible assets were seen to grow quickly by PHP <span style="color: GREEN">{{number_format($Change_r, 0, '.', ',')}}</span> thousand, or by
<span style="color: GREEN">
@if($ChangePercentage_r < 200)
	{{ number_format($ChangePercentage_r, 1, '.', ',') }}%
@else
	{{ number_format(($ChangePercentage_r / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
@endif
</span>. In this case, {{ $financial_report->title }} has no goodwill or other intangible assets. This
is why amounts of net worth and net tangible assets are equal on the last day of the period
analyzed.
</p>

<p style="text-indent: 50px;">On 12/31/{{ (int)$financial_report->year }}, the net worth of {{ $financial_report->title }} was much higher 
(by <span style="color: GREEN">
@if (0 >= $NetTangibleAssets2_r3) {
{{ number_format($NetTangibleAssets2_r2, 1) }}
@else
{{ number_format($NetTangibleAssets2_r2 / $NetTangibleAssets2_r3,1,'.',',') }}
@endif
</span>
times) than the share capital. Such a ratio positively describes the company's financial position. Net
worth is used as a measure of the company's book value (as opposed to a shareholder's value, the
value based on expected earnings and other methods used to estimate the company's value). In
financial analysis, amount of net worth (own equity) is one of the key indicators of property status of
the company.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>