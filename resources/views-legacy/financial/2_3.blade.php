<h3>2.3. Analysis of the Business Activity (Turnover Ratios)</h3>

<p style="text-indent: 50px;">To assess {{ $financial_report->title }}'s business activity, the table below provides the
main rates of turnover: receivables, inventory, current and total assets turnovers; accounts payable
and capital turnovers of the company. Turnover ratios have strong industry specifics and depend on 
activity. This is why an absolute value of the ratios does not permit making a qualitative
assessment. When assets turnover ratios are analyzed, an increase in ratios (i.e. velocity of
circulation) and a reduction in circulation days are deemed to be positive dynamics. There is no
well-defined interaction for accounts payable and capital turnover. In any case, an accurate
conclusion can only be made after the reasons that caused these changes are considered.</p>

<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Turnover ratio</td>
            <td class="center" colspan="{{ @count($income_statements) }}">Value, days</td>
            <td class="center" rowspan="2">Ratio {{((int)$financial_report->year - @count($income_statements) + 1)}}</td>
            <td class="center" rowspan="2">Ratio {{((int)$financial_report->year)}}</td>
            <td class="center" rowspan="2">Change, days (col.4 - col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($income_statements); $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>
                Receivables turnover (days sales outstanding)<br/>
                <small>(average trade and other current receivables divided by average daily revenue*)</small>
            </td>
			<?php $CCRT = array(); $CCAP = array(); $CCIT = array(); ?>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php
                    $ReceivablesTurnover = (($balance_sheets[$x]->TradeAndOtherCurrentReceivables + 
                                        $balance_sheets[$x + 1]->TradeAndOtherCurrentReceivables) / 2) /
                                        ($income_statements[$x]->Revenue / 365);
					$CCRT[] = $ReceivablesTurnover;
                    if($x == @count($income_statements) - 1) $ReceivablesTurnover1 = $ReceivablesTurnover;
                    if($x == 0) $ReceivablesTurnover2 = $ReceivablesTurnover;
                ?>
                <td class="center">
                    @if($ReceivablesTurnover != 0)
                        {{ round($ReceivablesTurnover) }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent1 = number_format($ReceivablesTurnover, 0, '.', ','); ?>
            @endfor
            <td class="center">
                @if($ReceivablesTurnover1 != 0)
                    {{ round(365 / $ReceivablesTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($ReceivablesTurnover2 != 0)
                    {{ round(365 / $ReceivablesTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($ReceivablesTurnover2 - $ReceivablesTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>
                Accounts payable turnover (days payable outstanding)<br/>
                <small>(average current payables divided by average daily purchases)</small>
            </td>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php 
                    if(($income_statements[$x]->CostOfSales +
                        $balance_sheets[$x]->Inventories -
                        $balance_sheets[$x + 1]->Inventories) != 0){
                            $PayableTurnover = ((
                                        $balance_sheets[$x]->TradeAndOtherCurrentPayables + 
                                        $balance_sheets[$x + 1]->TradeAndOtherCurrentPayables +
                                        $balance_sheets[$x]->CurrentProvisionsForEmployeeBenefits + 
                                        $balance_sheets[$x + 1]->CurrentProvisionsForEmployeeBenefits
                                        ) / 2) /
                                        ((
                                        $income_statements[$x]->CostOfSales +
                                        $balance_sheets[$x]->Inventories -
                                        $balance_sheets[$x + 1]->Inventories
                                        ) / 365);
                        }
                    else {
                        $PayableTurnover = 0;
                    }
                    $CCAP[] = $PayableTurnover;
                    if($x == @count($income_statements) - 1) $PayableTurnover1 = $PayableTurnover;
                    if($x == 0) $PayableTurnover2 = $PayableTurnover;
                ?>
                <td class="center">
                    @if($PayableTurnover != 0)
                        {{ round($PayableTurnover) }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent2 = number_format($PayableTurnover, 0, '.', ','); ?>
            @endfor
            <td class="center">
                @if($PayableTurnover1 != 0)
                    {{ round(365 / $PayableTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($PayableTurnover2 != 0)
                    {{ round(365 / $PayableTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($PayableTurnover2 - $PayableTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>
                Inventory turnover (days inventory outstanding)<br/>
                <small>(average inventory divided by average daily cost of sales)</small>
            </td>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php 
                    if(($income_statements[$x]->CostOfSales) != 0){
                            $InventoryTurnover = ((
                                        $balance_sheets[$x]->Inventories + 
                                        $balance_sheets[$x + 1]->Inventories
                                        ) / 2) /
                                        ((
                                        $income_statements[$x]->CostOfSales
                                        ) / 365);
                        }
                    else {
                        $InventoryTurnover = 0;
                    }
                    $CCIT[] = $InventoryTurnover;  
                    if($x == @count($income_statements) - 1) $InventoryTurnover1 = $InventoryTurnover;
                    if($x == 0) $InventoryTurnover2 = $InventoryTurnover;
                ?>
                <td class="center">
                    @if($InventoryTurnover != 0)
                        {{ round($InventoryTurnover) }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <td class="center">
                @if($InventoryTurnover1 != 0)
                    {{ round(365 / $InventoryTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($InventoryTurnover2 != 0)
                    {{ round(365 / $InventoryTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($InventoryTurnover2 - $InventoryTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>
                Asset turnover<br/>
                <small>(average total assets divided by average daily revenue)</small>
            </td>
            <?php $AssetTurnoverAve = 0; ?>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php 
                    if(($income_statements[$x]->Revenue) != 0){
                            $AssetTurnover = ((
                                        $balance_sheets[$x]->Assets + 
                                        $balance_sheets[$x + 1]->Assets
                                        ) / 2) /
                                        ((
                                        $income_statements[$x]->Revenue
                                        ) / 365);
                        }
                    else {
                        $AssetTurnover = 0;
                    }
                            
                    if($x == @count($income_statements) - 1) $AssetTurnover1 = $AssetTurnover;
                    if($x == 0) $AssetTurnover2 = $AssetTurnover;
                ?>
                <td class="center">
                    @if($AssetTurnover != 0)
                        {{ round($AssetTurnover) }}
                    @else
                        -
                    @endif
                </td>
                <?php $AssetTurnoverAve += $AssetTurnover; ?>
            @endfor
            <?php $textcontent3 = ceil($AssetTurnoverAve / @count($income_statements)); ?>
            <td class="center">
                @if($AssetTurnover1 != 0)
                    {{ round(365 / $AssetTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($AssetTurnover2 != 0)
                    {{ round(365 / $AssetTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($AssetTurnover2 - $AssetTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>
                Current asset turnover<br/>
                <small>(average current assets divided by average daily revenue)</small>
            </td>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php 
                    if(($income_statements[$x]->Revenue) != 0){
                            $CAssetTurnover = ((
                                        $balance_sheets[$x]->CurrentAssets + 
                                        $balance_sheets[$x + 1]->CurrentAssets
                                        ) / 2) /
                                        ((
                                        $income_statements[$x]->Revenue
                                        ) / 365);
                        }
                    else {
                        $CAssetTurnover = 0;
                    }
                            
                    if($x == @count($income_statements) - 1) $CAssetTurnover1 = $CAssetTurnover;
                    if($x == 0) $CAssetTurnover2 = $CAssetTurnover;
                ?>
                <td class="center">
                    @if($CAssetTurnover != 0)
                        {{ round($CAssetTurnover) }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <td class="center">
                @if($CAssetTurnover1 != 0)
                    {{ round(365 / $CAssetTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($CAssetTurnover2 != 0)
                    {{ round(365 / $CAssetTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($CAssetTurnover2 - $CAssetTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>
                Capital turnover<br/>
                <small>(average equity divided by average daily revenue)</small>
            </td>
            @for($x=@count($income_statements) - 1; $x >= 0 ; $x--)
                <?php 
                    if(($income_statements[$x]->Revenue) != 0){
                            $CapitalTurnover = ((
                                        $balance_sheets[$x]->Equity + 
                                        $balance_sheets[$x + 1]->Equity
                                        ) / 2) /
                                        ((
                                        $income_statements[$x]->Revenue
                                        ) / 365);
                        }
                    else {
                        $CapitalTurnover = 0;
                    }
                            
                    if($x == @count($income_statements) - 1) $CapitalTurnover1 = $CapitalTurnover;
                    if($x == 0) $CapitalTurnover2 = $CapitalTurnover;
                ?>
                <td class="center">
                    @if($CapitalTurnover != 0)
                        {{ round($CapitalTurnover) }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <td class="center">
                @if($CapitalTurnover1 != 0)
                    {{ round(365 / $CapitalTurnover1, 1) }}
                @else
                    -
                @endif
            </td>
            <td class="center">
                @if($CapitalTurnover2 != 0)
                    {{ round(365 / $CapitalTurnover2, 1) }}
                @else
                    -
                @endif
            </td>
            <?php 
                $Change = ($CapitalTurnover2 - $CapitalTurnover1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
		
		<tr>
            <td>
                <i>Reference:</i><br/>
				Cash Conversion Cycle
                <small>(days sales outstanding + days inventory outstanding - days payable outstanding)</small>
            </td>
            @for($x=0; $x < @count($CCRT); $x++)
                <?php 
					if($CCRT[$x]!=0 && $CCIT[$x]!=0 && $CCAP[$x]!=0){
						$CCC = round($CCRT[$x] + $CCIT[$x] - $CCAP[$x]);
					} else {
						$CCC = '-';
					}
                ?>
                <td class="center">
					{{$CCC}}
                </td>
            @endfor
            <td class="center">
				<?php 
					if($CCRT[0]!=0 && $CCIT[0]!=0 && $CCAP[0]!=0){
						$CCC_ratio1 = round(365 / ($CCRT[0] + $CCIT[0] - $CCAP[0]), 1);
					} else {
						$CCC_ratio1 = 0;
					}
                ?>
				@if($CCC_ratio1 != 0)
                    {{$CCC_ratio1}}
                @else
                    X
                @endif
            </td>
            <td class="center">
                <?php 
					$i = @count($CCRT) - 1;
					if($CCRT[$i]!=0 && $CCIT[$i]!=0 && $CCAP[$i]!=0){
						$CCC_ratio2 = round(365 / ($CCRT[$i] + $CCIT[$i] - $CCAP[$i]), 1);
					} else {
						$CCC_ratio2 = 0;
					}
                ?>	
                @if($CCC_ratio2 != 0)
                    {{$CCC_ratio2}}
                @else
                    X
                @endif
            </td>
            <?php 
                $Change = ($CCC_ratio2 - $CCC_ratio1);
                if($Change > 0) $Change = ceil($Change); else $Change = floor($Change);
                $class = ($Change <= 0) ? ($Change < 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{ $Change }}
                @else
                    -
                @endif
            </td>
        </tr>
	</tbody>
</table>
<p>* Calculation in days. Ratio value is equal to 365 divided by days outstanding.</p>
<br /><br/>
<p style="text-indent: 50px;">For the period from 01/01/{{ (int)$financial_report->year }} to 12/31/{{ (int)$financial_report->year }}, the average collection period (Days Sales
Outstanding) was {{$textcontent1}} days and the average days payable outstanding was {{$textcontent2}} days as shown in the
table. The rate of asset turnover means that {{ $financial_report->title }} gains revenue equal
to the sum of all the available assets every {{$textcontent3}} days (on average during the period analyzed
(01.01.{{substr(((int)$financial_report->year - @count($income_statements) + 1), 2)}}-
31.12.{{substr(((int)$financial_report->year),2)}})).
</p>
