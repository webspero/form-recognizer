<h3>3.1. Key Ratios Summary</h3>
<p style="text-indent: 50px;">The main financial state indicator values and {{ $financial_report->title }}'s activity results
are classified by qualitative assessment according to the results of the analysis for the {{@count($income_statements)}} years and
are given below.</p>

<p style="text-indent: 50px;">Financial characteristics with <i>outstanding</i> values:</p>
<ul style="margin-left: 20px;">
	<?php
		$NCAtoNW = $balance_sheets[0]->NoncurrentAssets / $balance_sheets[0]->Equity;
	?>
	<li>on 31 December, {{ $financial_report->year }}, the non-current assets to net worth ratio shows an absolutely normal value of <span style="color: GREEN">{{number_format($NCAtoNW, 2, '.', ',')}}</span>;</li>


	<?php
		if($balance_sheets[0]->CurrentLiabilities != 0)
			$CurrentRatio = $balance_sheets[0]->CurrentAssets /
				$balance_sheets[0]->CurrentLiabilities;
		else
			$CurrentRatio = 0;
	?>
	<li>the current ratio (<span style="color: GREEN">{{number_format($CurrentRatio, 2, '.', ',')}}</span>) completely corresponds to the standard criteria for this rate;</li>
	
	<?php
		if($balance_sheets[0]->CurrentLiabilities != 0)
			$QuickRatio = ($balance_sheets[0]->CashAndCashEquivalents + 
							$balance_sheets[0]->OtherCurrentFinancialAssets + 
							$balance_sheets[0]->TradeAndOtherCurrentReceivables) / 
							$balance_sheets[0]->CurrentLiabilities;
		else
			$QuickRatio = 0;
	?>
	<li>an outstanding relationship between liquid assets (current assets minus inventories) 
	and current liabilities (quick ratio is <span style="color: GREEN">{{number_format($QuickRatio, 2, '.', ',')}})</span>;</li>
	
	<li>the outrunning growth of equity relative to the total increments of assets;</li>
	
	<?php
		if($balance_sheets[0]->IssuedCapital != 0){
			$nettoshare = number_format($balance_sheets[0]->Equity / $balance_sheets[0]->IssuedCapital,1,'.',',');
		} else {
			$nettoshare = 0;
		}
	?>
	<li>net worth (net assets) of the company is much higher (by <span style="color: GREEN">{{$nettoshare}}</span> times) than the share capital on the last day of the period analyzed;</li>
	
	<li>long-term resources of the financing of the company's activity are enough to form a normal amount of working capital which would cover the available inventories;</li>
	
	<?php
		$EBIT = $income_statements[0]->ProfitLossBeforeTax + 
					$income_statements[0]->FinanceCosts;
		$Change = ($income_statements[0]->ProfitLossBeforeTax + 
							$income_statements[0]->FinanceCosts) -
						($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
							$income_statements[@count($income_statements) - 1]->FinanceCosts);
	?>
	<li>For the last year, earnings before interest and taxes (EBIT) showed PHP <span style="color: GREEN">{{number_format($EBIT, 0, '.', ',')}}</span> thousand,
at the same time, a positive dynamics compared with the data for the penultimate year
(PHP <span style="color: GREEN">{{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}</span> thousand) was observed.</li>


</ul>

<p style="text-indent: 50px;">Financial indicators with <i>positive</i> values are indicated below:</p>
<ul style="margin-left: 20px;">

	<?php
		// (DebtRatio)
		$DebtRatio = $balance_sheets[0]->Liabilities /
			$balance_sheets[0]->Assets;
		$class = ($DebtRatio <= 0.6) ? 'green' : 'red';
	?>
	<li>the percentage of liabilities in the total balance of {{ $financial_report->title }} is {{number_format($DebtRatio * 100, 1, '.', ',')}}% which is normal for stable activity;</li>
	
	<?php
		$ROE = ($income_statements[0]->ProfitLoss / 
								(($balance_sheets[0]->Equity + 
							$balance_sheets[1]->Equity) / 2)) * 100;
	?>
	<li>return on equity (ROE) was <span style="color: GREEN">{{number_format($ROE, 1, '.', ',')}}%</span> per annum during the year {{ $financial_report->year }} ;</li>

	<?php
		$ROA = ($income_statements[0]->ProfitLoss / 
								(($balance_sheets[0]->Assets + 
							$balance_sheets[1]->Assets) / 2)) * 100;
	?>
	<li>normal return on assets, which was <span style="color: GREEN">{{number_format($ROA, 1, '.', ',')}}%</span> for the period from 01/01/{{ $financial_report->year }} to 12/31/{{ $financial_report->year }};</li>
	
	<?php $ComprehensiveIncome = $income_statements[0]->ComprehensiveIncome; ?>
	<li>the income from financial and operational activities (comprehensive income) was PHP <span style="color: GREEN">{{number_format($ComprehensiveIncome, 0, '.', ',')}}</span> thousand for the year {{ $financial_report->year }}.</li>
</ul>


<?php
if($balance_sheets[0]->CurrentLiabilities != 0)
	$CashRatio = $balance_sheets[0]->CashAndCashEquivalents / 
					$balance_sheets[0]->CurrentLiabilities;
else
	$CashRatio = 0;
	$class = ($CashRatio >= 0.2) ? 'green' : 'red';
?>
<p style="text-indent: 50px;">The following indicator negatively describes {{ $financial_report->title }}'s financial state -
the cash ratio is equal to <span class="{{$class}}">{{ number_format($CashRatio, 2, '.', ',') }}</span> at the end of the period reviewed (a low cash at hand required for
current payments).</p>