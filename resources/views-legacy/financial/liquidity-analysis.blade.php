<!-- Liquidity Analysis  -->
    <div class="text-center">
        <h3><strong>1.4. Liquidity Analysis</strong></h3>
    </div>
    <p>One of the most widespread indicators of a company's solvency are liquidity related ratios. There are three liquidity related ratios: current ratio, quick ratio and cash ratio. Current ratio is one of the most widespread and shows to what degree the current assets of the company are meeting the current liabilities. The solvency of the company in the near future is described with the quick ratio which reflects if there are enough fund's for normal execution of current transactions with creditors. All three ratios for {{ $financial_report->title ?? "N/A" }} are calculated in the following table.</p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <td rowspan="2" class="center">Liquidity ratio</td>
            <td colspan="{{ count($balance_sheets) }}" class="center">Value</td>
            <td rowspan="2" class="center">Change (col.5 - col.2) </td>
            <td rowspan="2" class="center">Description of the ratio and it's recommended value</td>
        </tr>
        <tr>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">12/31/{{ $balance_sheets[$x]->Year}}</td>
            @endfor
        </tr>
        <tr>
            @for($indexCount = 1; $indexCount <= (count($balance_sheets)+3); $indexCount++)
                <td class="center"> {{ $indexCount }}</td>
            @endfor
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>1. Current ratio (working capital ratio)</td>
                <?php
                    $i=0;
                    $currentRatioPositiveValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                    $currentRatio = $currentAssets / $currentLiabilities;
                    $currentRatio = round($currentRatio, 2, PHP_ROUND_HALF_DOWN);

                    // First and Last value to find change value.
                    if ($i == 0) {
                        $firstCurrentRatio = $currentRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCurrentRatio = $currentRatio;
                        $currentRatioChangeValue = $lastCurrentRatio - $firstCurrentRatio;
                        $currentRatioChangeColor = 'green';
                        if($currentRatioChangeValue < 0)
                        {
                            $currentRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $currentRatioColor = 'red';
                    if($currentRatio >= 2)
                    {
                        array_push($currentRatioPositiveValues, $currentRatio);
                        $currentRatioColor = 'green';
                    }
                ?>
                <td class="center {{$currentRatioColor}}"> {{ $currentRatio }}</td>
                @endfor
                <td class="center {{ $currentRatioChangeColor }}">{{ $currentRatioChangeValue }} </td>
                <td>The current ratio is calculated by dividing current assets by current liabilities. It indicates a company's ability to meet short-term debt obligations. Normal value: 2 or more.</td>
            </tr>
            <tr>
                <td>2. Quick ratio (acid-test ratio)</td>
                <?php
                    $i=0;
                    $quickRatioPositiveValues=[];
                    $quickRatioValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Quick Ratio,  ∞ < crit. < 0.5 ≤ unsat. < 1 ≤ good < 1.1 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $otherCurrentFinancialAssets = $balance_sheets[$x]->OtherCurrentFinancialAssets;
                    $tradeAndOtherCurrentReceivables = $balance_sheets[$x]->TradeAndOtherCurrentReceivables;
                    $quickRatio = ($cashAndCashEquivalents + $otherCurrentFinancialAssets + $tradeAndOtherCurrentReceivables) / $currentLiabilities;
                    $quickRatio = round($quickRatio, 2, PHP_ROUND_HALF_DOWN);

                    if ($i == 0) {
                        $firstQuickRatio = $quickRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastQuickRatio = $quickRatio;
                        $quickRatioChangeValue = $lastQuickRatio - $firstQuickRatio;
                        $quickRatioChangeColor = 'green';
                        if($quickRatioChangeValue < 0)
                        {
                            $quickRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $quickRatioColor = 'red';
                    if($quickRatio >= 1)
                    {
                        $quickRatioColor = 'green';
                        array_push($quickRatioPositiveValues, $quickRatio);
                    }
                    array_push($quickRatioValues, $quickRatio);
                ?>
                <td class="center {{ $quickRatioColor }}"> {{ $quickRatio }}</td>
                @endfor
                <td class="center {{ $quickRatioChangeColor }}">{{ $quickRatioChangeValue }} </td>
                <td>The quick ratio is calculated by dividing liquid assets (cash and cash equivalents, trade and other current receivables, other current financial assets) by current liabilities. It is a measure of a company's ability to meet its short-term obligations using its most liquid assets (near cash or quick assets). Acceptable value: 1 or more.</td>
            </tr>
            <tr>
                <td>3. Cash ratio (acid-test ratio)</td>
                <?php
                    $i=0;
                    $cashRatioPositiveValues = [];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Cash Ratio, ∞ < crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $cashRatio = $cashAndCashEquivalents / $currentLiabilities;
                    $cashRatio = round($cashRatio, 2, PHP_ROUND_HALF_DOWN);

                    if ($i == 0) {
                        $firstCashRatio = $cashRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCashRatio = $cashRatio;
                        $cashRatioChangeValue = $lastCashRatio - $firstCashRatio;
                        $cashRatioChangeColor = 'green';
                        if($cashRatioChangeValue < 0)
                        {
                            $cashRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $cashRatioColor = 'red';
                    if($cashRatio >= 0.2)
                    {
                        $cashRatioColor = 'green';
                        array_push($cashRatioPositiveValues, $cashRatio);
                    }
                ?>
                <td class="center {{ $cashRatioColor }}"> {{ $cashRatio }}</td>
                @endfor
                <td class="center {{ $cashRatioChangeColor }}">{{ $cashRatioChangeValue }} </td>
                <td>Cash ratio is calculated by dividing absolute liquid assets (cash and cash equivalents) by current liabilities. Normal value: 0.2 or more.</td>
            </tr>
        </tbody>
    </table>
    <?php
        $currentRatioText = '';
        if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue<0) )
        {
            $currentRatioText = 'During the entire period reviewed, the current ratio went down significantly (by <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>), additionally, a similar tendency during the period is proved by the linear trend. On 12/31/'.(int)$financial_report->year.', the ratio has an atypical value. The current ratio kept an unacceptable value during the whole of the reviewed period.';
        }
        else if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue>0) )
        {
            $currentRatioText = 'During the period analysed, it was verified that there was an appreciable increase in the current ratio of <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>. At the end of the period, the value of the ratio is unacceptable and lies in the area of critical values. The current ratio did not keep its acceptable value during the whole of the period.';
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue>0) )
        {
            $currentRatioText = 'The value of the ratio can be characterized as first-rate on the last day of the period analyzed. During the whole of the period, the current ratio corresponded to the set norm.';
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue<0) )
        {
            $currentRatioText = 'On the last day of the period analysed, the value of the ratio can be characterized as first-rate. The current ratio kept an acceptable value during the whole of the analysed period.';
        }
        else if( ($firstCurrentRatio >= 2) && ($lastCurrentRatio < 2) && ($currentRatioChangeValue > 0)  )
        {
            $currentRatioText = 'During the period reviewed, the growth in the current ratio was <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>. Despite the fact that at the beginning of the analyzed period and the value of the current ratio corresponded to the norm, further it became unacceptable.';
        }
        else if( ($firstCurrentRatio < 2) && ($lastCurrentRatio >= 2) && ($currentRatioChangeValue > 0)  )
        {
            $currentRatioText = 'The growth in the current ratio was <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span> for the entire period reviewed. On the last day of the period analysed (12/31/'.(int)$financial_report->year.'), the ratio demonstrates an excellent value. Despite the fact that at the beginning of the considered period, the value of the current ratio did not correspond to the norm, later it became acceptable.';
        }
        else
        {
            if($lastCurrentRatio >= 2)
            {
                $currentRatioText = 'During the reviewed period, multidirectional changes in the ratio (both increase and reduction) were observed,  later it became acceptable.';
            }
            else
            {
                $currentRatioText = 'During the reviewed period, multidirectional changes in the ratio (both increase and reduction) were observed,  later it became unacceptable.';
            }
        }
    ?>
    <p>On 12/31/{{ (int)$financial_report->year }}, the current ratio was equal to <span class="{{ $currentRatioColor }}">{{ $lastCurrentRatio }}</span>. {!! $currentRatioText !!}</p>

    <?php
        // Quick Ratio Text
        $quickRatioText = '';
        if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue<0) )
        {
            $quickRatioText = 'During the entire period reviewed, it was seen that there was a marked lowering in the quick ratio, which showed <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>, moreover, a similar tendency during the period is confirmed by the linear trend. During the reviewed period, both growth and fall in the ratio were verified; the maximum value was '.max($quickRatioValues).', the minimum one was '.min($quickRatioValues).'. The quick ratio has an unsatisfactory value at the end of the period analysed. It means that '.$financial_report->title.' does not have enough liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.';
        }
        else if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue>0) )
        {
            $quickRatioText = 'however at the beginning of the considered period, the quick ratio was lower '.$firstQuickRatio.' (i.e. the growth was equal to <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>). At the beginning of the evaluated period, the ratio spiked, but later the trend altered into a reversal. On 12/31/'.(int)$financial_report->year.', the value of the quick ratio can be described as unsatisfactory. This means, '.$financial_report->title.' has either too many current liabilities or not enough liquid assets to satisfy the mentioned liabilities.';
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue>0) )
        {
            $quickRatioText = 'During the reviewed period, the ratio changed multidirectionally; the maximum value was '.max($quickRatioValues).', the minimum one was '.min($quickRatioValues).'. The value of the quick ratio can be described as absolutely normal on 12/31/'.(int)$financial_report->year.'. It means that '.$financial_report->title.' has enough liquid assets (cash and other assets which can be rapidly sold)to meet all their current liabilities.';
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue<0) )
        {
            $quickRatioText = 'During the whole period reviewed, the quick ratio sharply reduced (<span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>). Despite the forecasted growth of the rate in this period, its&apos; reduction followed further. The value of the quick ratio can be described as very good on 12/31/'.(int)$financial_report->year.'. It means that '.$financial_report->title.' has enough liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.';
        }
        else if( ($firstQuickRatio >= 1) && ($lastQuickRatio < 1) && ($quickRatioChangeValue > 0)  )
        {
            $quickRatioText = 'For the whole period reviewed, it was verified that there was a quick growth in the quick ratio of <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>. During the whole of the analysed period, a growth of the ratio changed into reduction. The quick ratio has an unsatisfactory value at the end of the period analysed. It means that '.$financial_report->title.' does not have enough liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.';
        }
        else if( ($firstQuickRatio < 1) && ($lastQuickRatio >= 1) && ($quickRatioChangeValue > 0)  )
        {
            $quickRatioText = 'For the whole period reviewed, it was verified that there was a quick growth in the quick ratio of <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>. During the whole of the analysed period, a reduction of the ratio changed into growth. The value of the quick ratio can be described as absolutely normal on the last day of the period analysed. It means that '.$financial_report->title.' has enough liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.';
        }
        else
        {
            if($lastQuickRatio >= 1)
            {
                $quickRatioText = 'On 12/31/'.(int)$financial_report->year.', the value of the quick ratio can be described as normal. This means that, '.$financial_report->title.' has enough liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.';
            }
            else
            {
                $quickRatioText = 'On 12/31/'.(int)$financial_report->year.', the value of the quick ratio can be described as unsatisfactory. This means that, '.$financial_report->title.' does not have enough liquid assets to satisfy the mentioned liabilities.';
            }
        }
    ?>
    <p>On 12/31/{{(int)$financial_report->year}}., the quick ratio was equal to <span class="{{ $quickRatioColor }}">{{ $lastQuickRatio }}</span>.{!! $quickRatioText !!}</p>

    <?php
        // cash Ratio Text
        $cashRatioText = '';
        if( (count($cashRatioPositiveValues)<=0) && ($cashRatioChangeValue<0) )
        {
            $cashRatioText = 'The value of the third ratio, the cash ratio has an unsatisfactory value (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) on the last day of the period analysed that describes the lack of the most liquid assets in the company (cash and cash equivalents) to meet all current liabilities.';
        }
        else if( (count($cashRatioPositiveValues)<=0) && ($cashRatioChangeValue>0) )
        {
            $cashRatioText = 'The value of the third ratio, the cash ratio has an unsatisfactory value (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) on the last day of the period analysed that describes the lack of the most liquid assets in the company (cash and cash equivalents) to meet all current liabilities.';
        }
        else if( (count($cashRatioPositiveValues)===count($balance_sheets)) && ($cashRatioChangeValue>0) )
        {
            $cashRatioText = 'The value of the third ratio, the cash ratio, lies in the normal range on 12/31/'.$financial_report->year.'. '.$financial_report->title.' is observed to have enough cash and cash equivalents to meet current liabilities.';
        }
        else if( (count($cashRatioPositiveValues)===count($balance_sheets)) && ($cashRatioChangeValue<0) )
        {
            $cashRatioText = 'The value of the third ratio, the cash ratio, lies in the acceptable range on 12/31/'.$financial_report->year.'. '.$financial_report->title.' is observed to have enough cash and cash equivalents to meet current liabilities.';
        }
        else if( ($firstCashRatio >= 0.2) && ($lastCashRatio < 0.2) && ($cashRatioChangeValue > 0)  )
        {
            $cashRatioText = 'The cash ratio amounted to <span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>. on the last day of the period analyzed (12/31/'.$financial_report->year.'). During the entire period reviewed, the increase in the cash ratio was <span class="'.$cashRatioChangeColor.'">'.$cashRatioChangeValue.'</span>., despite that the linear trend indicates that on average the cash ratio was decreasing during the period.';
        }
        else if( ($firstCashRatio < 0.2) && ($lastCashRatio >= 0.2) && ($cashRatioChangeValue > 0)  )
        {
            $cashRatioText = 'The value of the third ratio, the cash ratio, lies in the normal range at the end of the period reviewed. '.$financial_report->title.' is observed to have enough cash and cash equivalents to meet current liabilities.';
        }
        else
        {
            if($lastCashRatio >= 0.2)
            {
                $cashRatioText = 'The value of the third ratio, the cash ratio, lies in the normal range at the end of the period reviewed. '.$financial_report->title.' is observed to have enough cash and cash equivalents to meet current liabilities.';
            }
            else
            {
                $cashRatioText = 'The value of the third ratio, the cash ratio has an unsatisfactory value (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) on the last day of the period analysed that describes the lack of the most liquid assets in the company (cash and cash equivalents) to meet all current liabilities.';
            }
        }
    ?>

    <p>{!! $cashRatioText !!}</p>
    <!-- Liquidity Graph -->
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_4_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>
<!-- End: Liquidity Analysis  -->