<h3>3.2 Rating of the Financial Position and Financial Performance of {{ $financial_report->title }}</h3>
<?php
    //Calculate linear regression
	function CalculateSf($data = array(), $slope=false){
		$n  = @count($data);
		$a = 0;
		foreach($data as $k=>$v){
			$a += ($k + 1) * $v;
		}
		$a = $a * $n;
		$ba = 0; $bb = 0;
		foreach($data as $k=>$v){
			$ba += ($k + 1);
			$bb += $v;
		}
		$b = $ba * $bb;
		$c = 0;
		for($x = 1; $x <= $n; $x++){
			$c += $x * $x;
		}
		$c = $c * $n;
		$d = 0;
		for($x = 1; $x <= $n; $x++){
			$d += $x;
		}
		$d = $d * $d;
		$m = ($a - $b) / ($c - $d);
		if($slope){
			return $m;
		}
		$e = 0;
		foreach($data as $v){
			$e += $v;
		}
		$f = 0;
		for($x = 1; $x <= $n; $x++){
			$f += $x;
		}
		$f = $f * $m;
		$y_intercept = ($e -$f) / $n;
		
		return ($m * ($n + 1)) + $y_intercept;
	}
	
	function CalculateSp($data = array()){
		$t = 0;
		for($x = 0; $x < @count($data)-1; $x++){
			$t += $data[$x];
		}
		if(@count($data) > 1){
			return $t / (@count($data)-1);
		} else {
			return 0;
		}
	}
	function CalculateAverageScore($data = array()){
		return ($data[0] * 0.25) + ($data[1] * 0.6) + ($data[2] * 0.15);
	}
	
	function CalculateSlope($data=array(), $present=false){
		if($present){
			return $data[@count($data)-1] - $data[@count($data)-2];
		} else {
			if(@count($data)>2){
				return ($data[@count($data)-2] - $data[0]) / (@count($data) - 2);
			} else {
				return $data[@count($data)-1] - $data[@count($data)-2];
			}
		}
	}
	
	function GetRatingValue($value){
		if($value >= 1.6) return ['position'=>1, 'rating'=>'AAA', 'text'=>'excellent'];
		elseif($value >= 1.2 && $value < 1.6) return ['position'=>2, 'rating'=>'AA', 'text'=>'very good'];
		elseif($value >= 0.8 && $value < 1.2) return ['position'=>3, 'rating'=>'A', 'text'=>'good'];
		elseif($value >= 0.4 && $value < 0.8) return ['position'=>4, 'rating'=>'BBB', 'text'=>'positive'];
		elseif($value >= 0 && $value < 0.4) return ['position'=>5, 'rating'=>'BB', 'text'=>'normal'];
		elseif($value >= -0.4 && $value < 0) return ['position'=>6, 'rating'=>'B', 'text'=>'satisfactory'];
		elseif($value >= -0.8 && $value < -0.4) return ['position'=>7, 'rating'=>'CCC', 'text'=>'unsatisfactory'];
		elseif($value >= -1.2 && $value < -0.8) return ['position'=>8, 'rating'=>'CC', 'text'=>'adverse'];
		elseif($value >= -1.6 && $value < -1.2) return ['position'=>9, 'rating'=>'C', 'text'=>'bad'];
		else return ['position'=>10, 'rating'=>'D', 'text'=>'critical'];
	}

	//Financial Position
	
	//Debt Ratio
	$DebtRatio = array();
	for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
		$DebtRatio[] = $balance_sheets[$x]->Liabilities / $balance_sheets[$x]->Assets;
	}
	$DebtRatioS = [
		CalculateSp($DebtRatio), 
		$DebtRatio[@count($balance_sheets)-1],
		CalculateSf($DebtRatio)
	];
	//∞ < good < 0.3 ≤ excel. ≤ 0.5 < good ≤ 0.6 < unsat. ≤ 1 < crit. < ∞
	foreach($DebtRatioS as $k=>$v){
		if($v < 0.3) $DebtRatioS[$k] = 1;
		elseif($v >= 0.3 && $v <= 0.5) $DebtRatioS[$k] = 2;
		elseif($v > 0.5 && $v <= 0.6) $DebtRatioS[$k] = 1;
		elseif($v > 0.6 && $v <= 1) $DebtRatioS[$k] = -1;
		else $DebtRatioS[$k] = -2;
	}
	$DebtRatioAv = CalculateAverageScore($DebtRatioS);
	
	//Non-current Assets to Net Worth
	$NCAtoNW = array();
	for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
		$NCAtoNW[] = $balance_sheets[$x]->NoncurrentAssets / $balance_sheets[$x]->Equity;
	}
	$NCAtoNWS = [
		CalculateSp($NCAtoNW), 
		$NCAtoNW[@count($balance_sheets)-1],
		CalculateSf($NCAtoNW)
	];
	//∞ < crit. < 0 ≤ excel. ≤ 1 < good ≤ 1.25 < unsat. ≤ 2 < crit. < ∞
	foreach($NCAtoNWS as $k=>$v){
		if($v < 0) $NCAtoNWS[$k] = -2;
		elseif($v >= 0 && $v <= 1) $NCAtoNWS[$k] = 2;
		elseif($v > 1 && $v <= 1.25) $NCAtoNWS[$k] = 1;
		elseif($v > 1.25 && $v <= 2) $NCAtoNWS[$k] = -1;
		else $NCAtoNWS[$k] = -2;
	}
	$NCAtoNWAv = CalculateAverageScore($NCAtoNWS); 
	
	//Current Ratio
	$CurrentRatio = array();
	for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
		if($balance_sheets[$x]->CurrentLiabilities != 0)
			$CurrentRatio[] = $balance_sheets[$x]->CurrentAssets / $balance_sheets[$x]->CurrentLiabilities;
		else
			$CurrentRatio[] = 0;
	}
	$CurrentRatioS = [
		CalculateSp($CurrentRatio), 
		$CurrentRatio[@count($balance_sheets)-1],
		CalculateSf($CurrentRatio)
	];
	//∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
	foreach($CurrentRatioS as $k=>$v){
		if($v < 1) $CurrentRatioS[$k] = -2;
		elseif($v >= 1 && $v < 2) $CurrentRatioS[$k] = -1;
		elseif($v >= 2 && $v < 2.1) $CurrentRatioS[$k] = 1;
		else $CurrentRatioS[$k] = 2;
	}
	$CurrentRatioAv = CalculateAverageScore($CurrentRatioS);
	
	//QuickRatio
	$QuickRatio = array();
	for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
		if($balance_sheets[$x]->CurrentLiabilities != 0)
			$QuickRatio[] = ($balance_sheets[$x]->CashAndCashEquivalents + 
							$balance_sheets[$x]->OtherCurrentFinancialAssets + 
							$balance_sheets[$x]->TradeAndOtherCurrentReceivables) / 
							$balance_sheets[$x]->CurrentLiabilities;
		else
			$QuickRatio[] = 0;
	}
	$QuickRatioS = [
		CalculateSp($QuickRatio), 
		$QuickRatio[@count($balance_sheets)-1],
		CalculateSf($QuickRatio)
	];
	//∞ < crit. < 0.5 ≤ unsat. < 1 ≤ good < 1.1 ≤ excel. < ∞
	foreach($QuickRatioS as $k=>$v){
		if($v < 0.5) $QuickRatioS[$k] = -2;
		elseif($v >= 0.5 && $v < 1) $QuickRatioS[$k] = -1;
		elseif($v >= 1 && $v < 1.1) $QuickRatioS[$k] = 1;
		else $QuickRatioS[$k] = 2;
	}
	$QuickRatioAv = CalculateAverageScore($QuickRatioS);
	
	//Cash Ratio
	$CashRatio = array();
	for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
		if($balance_sheets[$x]->CurrentLiabilities != 0)
			$CashRatio[] = $balance_sheets[$x]->CashAndCashEquivalents / 
							$balance_sheets[$x]->CurrentLiabilities;
		else
			$CashRatio[] = 0;
	}
	$CashRatioS = [
		CalculateSp($CashRatio), 
		$CashRatio[@count($balance_sheets)-1],
		CalculateSf($CashRatio)
	];
	//∞ < crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∞
	foreach($CashRatioS as $k=>$v){
		if($v < 0.05) $CashRatioS[$k] = -2;
		elseif($v >= 0.05 && $v < 0.2) $CashRatioS[$k] = -1;
		elseif($v >= 0.2 && $v < 0.25) $CashRatioS[$k] = 1;
		else $CashRatioS[$k] = 2;
	}
	$CashRatioAv = CalculateAverageScore($CashRatioS);
	
	
	//Financial Condition
	
	//Return on equity (ROE)
	$ROE = array();
	for($x=@count($income_statements)-1; $x >= 0 ; $x--){
		$ROE[] = $income_statements[$x]->ProfitLoss / 
								(($balance_sheets[$x]->Equity + 
							$balance_sheets[$x + 1]->Equity) / 2);
	}
	$ROES = [
		CalculateSp($ROE), 
		$ROE[@count($income_statements)-1],
		CalculateSf($ROE)
	];
	//∞ < crit. < 0 ≤ unsat. < 0.12 ≤ good < 0.2 ≤ excel. < ∞
	foreach($ROES as $k=>$v){
		if($v < 0) $ROES[$k] = -2;
		elseif($v >= 0 && $v < 0.112) $ROES[$k] = -1;
		elseif($v >= 0.112 && $v < 0.128) $ROES[$k] = 0;
		elseif($v >= 0.128 && $v < 0.2) $ROES[$k] = 1;
		else $ROES[$k] = 2;
	}
	$ROEAv = CalculateAverageScore($ROES);
	
	//Return on assets (ROA)
	$ROA = array();
	for($x=@count($income_statements)-1; $x >= 0 ; $x--){
		$ROA[] = $income_statements[$x]->ProfitLoss / 
								(($balance_sheets[$x]->Assets + 
							$balance_sheets[$x + 1]->Assets) / 2);
	}
	$ROAS = [
		CalculateSp($ROA), 
		$ROA[@count($income_statements)-1],
		CalculateSf($ROA)
	];
	//∞ < crit. < 0 ≤ unsat. < 0.06 ≤ good < 0.1 ≤ excel. < ∞
	foreach($ROAS as $k=>$v){
		if($v < 0) $ROAS[$k] = -2;
		elseif($v >= 0 && $v < 0.056) $ROAS[$k] = -1;
		elseif($v >= 0.056 && $v < 0.064) $ROAS[$k] = 0;
		elseif($v >= 0.064 && $v < 0.1) $ROAS[$k] = 1;
		else $ROAS[$k] = 2;
	}
	$ROAAv = CalculateAverageScore($ROAS);
	
	//Sales Growth
	$SalesGrowth = array();
	$SGtotal = 0;
	for($x=@count($income_statements)-1; $x >= 0 ; $x--){
		$SalesGrowth[] = $income_statements[$x]->Revenue;
		$SGtotal += $income_statements[$x]->Revenue;
	}
	$SGaverage = $SGtotal / @count($income_statements);
	$SalesGrowthS = [
		CalculateSlope($SalesGrowth, false) / $SGaverage, 
		CalculateSlope($SalesGrowth, true) / $SGaverage,
		CalculateSf($SalesGrowth, true) / $SGaverage
	];
	/*
	<-0.3 – score "-2";
	-0.3 – 0.04 – score "-1";
	-0.04 – 0.04 – score "0";
	0.04 – 0.3 – score "+1";
	> 0.3 – score "+2"
	*/
	foreach($SalesGrowthS as $k=>$v){
		if($v < -0.3) $SalesGrowthS[$k] = -2;
		elseif($v >= -0.3 && $v < -0.04) $SalesGrowthS[$k] = -1;
		elseif($v >= -0.04 && $v < 0.04) $SalesGrowthS[$k] = 0;
		elseif($v >= 0.04 && $v < 0.3) $SalesGrowthS[$k] = 1;
		else $SalesGrowthS[$k] = 2;
	}
	$SalesGrowthAv = CalculateAverageScore($SalesGrowthS);
	
	// Final Scores
	
	//$DebtRatioAv $NCAtoNWAv $CurrentRatioAv $QuickRatioAv $CashRatioAv
	
	$FinancialPosition = ($DebtRatioAv * 0.3) +
						($NCAtoNWAv * 0.15) +
						($CurrentRatioAv * 0.2) +
						($QuickRatioAv * 0.2) +
						($CashRatioAv * 0.15);
						
	//$ROEAv $ROAAv $SalesGrowthAv
	
	$FinancialPerformance = ($ROEAv * 0.5) +
						($ROAAv * 0.3) +
						($SalesGrowthAv * 0.2);
						
							
	$FinalRating = ($FinancialPosition * 0.6) + ($FinancialPerformance * 0.4);

	
	$FinancialPositionRV = GetRatingValue($FinancialPosition);
	$FinancialPerformanceRV = GetRatingValue($FinancialPerformance);
	$FinalRV = GetRatingValue($FinalRating);
		
	$xpos = $FinancialPositionRV['position'];
	$ypos = $FinancialPerformanceRV['position'];	
?>

<table cellpadding="0" cellspacing="0" border="0" style="border: 0 none;">
	<tr>
		<td width="66%" style="border: 0 none;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="center" rowspan="2"><b>Financial performance</b> for the period analyzed 
						(01.01.{{substr(((int)$financial_report->year - @count($income_statements) + 1), 2)}}-
						31.12.{{substr(((int)$financial_report->year),2)}})
					</td>
					<td class="center" colspan="10"><b>Financial position</b> on 12/31/{{((int)$financial_report->year)}}</td>
				</tr>
				<tr dontbreak="true">
					<td width="7%" style="background-color: #6eff94; text-align: center;">AAA</td>
					<td width="7%" style="background-color: #a8ffbd; text-align: center;">AA</td>
					<td width="7%" style="background-color: #bfffce; text-align: center;">A</td>
					<td width="7%" style="background-color: #d4ffdc; text-align: center;">BBB</td>
					<td width="7%" style="background-color: #e1fce9; text-align: center;">BB</td>
					<td width="7%" style="background-color: #f0f0f0; text-align: center;">B</td>
					<td width="7%" style="background-color: #fad4e1; text-align: center;">CCC</td>
					<td width="7%" style="background-color: #ffbbba; text-align: center;">CC</td>
					<td width="7%" style="background-color: #ff8585; text-align: center;">C</td>
					<td width="7%" style="background-color: #fc5151; text-align: center;">D</td>
				</tr>
				<tr>
					<td style="background-color: #6eff94">Excellent (AAA)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 1) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #a8ffbd">Very good (AA)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 2) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #bfffce">Good (A)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 3) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #d4ffdc">Positive (BBB)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 4) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #e1fce9">Normal (BB)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 5) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #f0f0f0">Satisfactory (B)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 6) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #fad4e1">Unsatisfactory (CCC)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 7) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #ffbbba">Adverse (CC)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 8) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #ff8585">Bad (C)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 9) ? "<b>V</b>" : "" }}</td>
				</tr>
				<tr>
					<td style="background-color: #fc5151">Critical (D)</td>
					<td style="text-align: center;">{{ ($xpos == 1 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 2 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 3 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 4 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 5 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 6 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 7 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 8 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 9 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
					<td style="text-align: center;">{{ ($xpos == 10 && $ypos == 10) ? "<b>V</b>" : "" }}</td>
				</tr>
			</table>
		</td>
		<td style="padding-left: 10px; border: 0;">
			<div style="border: 1px solid #000; width: 100%; padding: 10px;">
				<p>Final rating of the
				financial condition of
				{{ $financial_report->title }} (period
				analyzed: from
				01/01/{{((int)$financial_report->year - @count($income_statements) + 1)}} to
				12/31/{{((int)$financial_report->year)}} analysis step
				- year):</p>
				<br />
				<p align="center" style="font-size: 18px; margin-bottom: 0; font-weight: bold;">{{$FinalRV['rating']}}</p>
				<p align="center">({{$FinalRV['text']}})</p>
			</div>
		</td>
	</tr>
</table>
</div>
<p style="text-indent: 50px;">According to the results of the conducted analysis, the financial position for {{ $financial_report->title }} was assessed at a score scale in 
<span class="{{ ($FinancialPosition <= 0) ? (($FinancialPosition < 0) ? 'red' : '') : 'green' }}">{{ ($FinancialPosition > 0) ? "+" : "" }}{{ number_format($FinancialPosition, 2, '.', ',') }}</span>
, which corresponds to the {{$FinancialPositionRV['rating']}} rating ({{$FinancialPositionRV['text']}} position). 
The financial results of the company's activities were scored at 
<span class="{{ ($FinancialPerformance <= 0) ? (($FinancialPerformance < 0) ? 'red' : '') : 'green' }}">{{ ($FinancialPerformance > 0) ? "+" : "" }}{{ number_format($FinancialPerformance, 2, '.', ',') }}</span> during the
period analyzed, which corresponds to the {{$FinancialPerformanceRV['rating']}} rating ({{$FinancialPerformanceRV['text']}} results). One should tell that final scores
are calculated considering both rates at the end of the period analyzed and rates dynamics,
including their forecasted values for the next year. The final score of the financial condition, which
includes analysis of the company's financial position and financial performance, makes 
<span class="{{ ($FinalRating <= 0) ? (($FinalRating < 0) ? 'red' : '') : 'green' }}">{{ ($FinalRating > 0) ? "+" : "" }}{{ number_format($FinalRating, 2, '.', ',') }}</span>,
which equals the rating scale to a {{$FinalRV['text']}} ({{$FinalRV['rating']}}) condition.

An "{{$FinalRV['rating']}}" rating shows a good financial condition of a company and its' capability to meet their
likely current liabilities. Companies with this rating refer to the category of the borrowers who can
obtain credits with high probability (good creditworthiness).</a>

<h2>4. Appendix</h2>
<h3>4.1 Calculation of the Final Rating of the Financial Condition</h3>

<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Ratio</td>
            <td class="center" rowspan="2">Weighting Factor</td>
            <td class="center" colspan="3">Score</td>
            <td class="center" rowspan="2">Average Score (col.3 x 0.25 + col.4 x 0.6 + col.5 x0.15)</td>
            <td class="center" rowspan="2">Weighted Average Score (col.2 x col.6)</td>
        </tr>
        <tr dontbreak="true">
            <td class="center">past</td>
            <td class="center">present</td>
            <td class="center">future</td>
        </tr>
        <tr>
            <td colspan="7"><b>I. Rating of the company's financial position</b></td>
        </tr>
        <tr>
            <td>Debt Ratio</td>
            <td class="center">0.3</td>
            <td class="center {{ ($DebtRatioS[0] <= 0) ? (($DebtRatioS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($DebtRatioS[0] > 0) ? "+" : "" }}{{ $DebtRatioS[0] }}
            </td>
            <td class="center {{ ($DebtRatioS[1] <= 0) ? (($DebtRatioS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($DebtRatioS[1] > 0) ? "+" : "" }}{{ $DebtRatioS[1] }}
            </td>
            <td class="center {{ ($DebtRatioS[2] <= 0) ? (($DebtRatioS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($DebtRatioS[2] > 0) ? "+" : "" }}{{ $DebtRatioS[2] }}
            </td>
            <td class="center {{ ($DebtRatioAv <= 0) ? (($DebtRatioAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($DebtRatioAv > 0) ? "+" : "" }}{{ $DebtRatioAv }}
            </td>
            <td class="center {{ (($DebtRatioAv * 0.3) <= 0) ? ((($DebtRatioAv * 0.3) < 0) ? 'red' : '') : 'green' }}">
                {{ (($DebtRatioAv * 0.3) > 0) ? "+" : "" }}{{ ($DebtRatioAv * 0.3) }}
            </td>
        </tr>
        
        <tr>
            <td>Non-current assets to net worth</td>
            <td class="center">0.15</td>
            <td class="center {{ ($NCAtoNWS[0] <= 0) ? (($NCAtoNWS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($NCAtoNWS[0] > 0) ? "+" : "" }}{{ $NCAtoNWS[0] }}
            </td>
            <td class="center {{ ($NCAtoNWS[1] <= 0) ? (($NCAtoNWS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($NCAtoNWS[1] > 0) ? "+" : "" }}{{ $NCAtoNWS[1] }}
            </td>
            <td class="center {{ ($NCAtoNWS[2] <= 0) ? (($NCAtoNWS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($NCAtoNWS[2] > 0) ? "+" : "" }}{{ $NCAtoNWS[2] }}
            </td>
            <td class="center {{ ($NCAtoNWAv <= 0) ? (($NCAtoNWAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($NCAtoNWAv > 0) ? "+" : "" }}{{ $NCAtoNWAv }}
            </td>
            <td class="center {{ (($NCAtoNWAv * 0.15) <= 0) ? ((($NCAtoNWAv * 0.15) < 0) ? 'red' : '') : 'green' }}">
                {{ (($NCAtoNWAv * 0.15) > 0) ? "+" : "" }}{{ ($NCAtoNWAv * 0.15) }}
            </td>
        </tr>
        
        <tr>
            <td>Current Ratio</td>
            <td class="center">0.2</td>
            <td class="center {{ ($CurrentRatioS[0] <= 0) ? (($CurrentRatioS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CurrentRatioS[0] > 0) ? "+" : "" }}{{ $CurrentRatioS[0] }}
            </td>
            <td class="center {{ ($CurrentRatioS[1] <= 0) ? (($CurrentRatioS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CurrentRatioS[1] > 0) ? "+" : "" }}{{ $CurrentRatioS[1] }}
            </td>
            <td class="center {{ ($CurrentRatioS[2] <= 0) ? (($CurrentRatioS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CurrentRatioS[2] > 0) ? "+" : "" }}{{ $CurrentRatioS[2] }}
            </td>
            <td class="center {{ ($CurrentRatioAv <= 0) ? (($CurrentRatioAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($CurrentRatioAv > 0) ? "+" : "" }}{{ $CurrentRatioAv }}
            </td>
            <td class="center {{ (($CurrentRatioAv * 0.2) <= 0) ? ((($CurrentRatioAv * 0.2) < 0) ? 'red' : '') : 'green' }}">
                {{ (($CurrentRatioAv * 0.2) > 0) ? "+" : "" }}{{ ($CurrentRatioAv * 0.2) }}
            </td>
        </tr>
        
        <tr>
            <td>Quick Ratio</td>
            <td class="center">0.2</td>
            <td class="center {{ ($QuickRatioS[0] <= 0) ? (($QuickRatioS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($QuickRatioS[0] > 0) ? "+" : "" }}{{ $QuickRatioS[0] }}
            </td>
            <td class="center {{ ($QuickRatioS[1] <= 0) ? (($QuickRatioS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($QuickRatioS[1] > 0) ? "+" : "" }}{{ $QuickRatioS[1] }}
            </td>
            <td class="center {{ ($QuickRatioS[2] <= 0) ? (($QuickRatioS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($QuickRatioS[2] > 0) ? "+" : "" }}{{ $QuickRatioS[2] }}
            </td>
            <td class="center {{ ($QuickRatioAv <= 0) ? (($QuickRatioAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($QuickRatioAv > 0) ? "+" : "" }}{{ $QuickRatioAv }}
            </td>
            <td class="center {{ (($QuickRatioAv * 0.2) <= 0) ? ((($QuickRatioAv * 0.2) < 0) ? 'red' : '') : 'green' }}">
                {{ (($QuickRatioAv * 0.2) > 0) ? "+" : "" }}{{ ($QuickRatioAv * 0.2) }}
            </td>
        </tr>
        
        <tr>
            <td>Cash Ratio</td>
            <td class="center">0.15</td>
            <td class="center {{ ($CashRatioS[0] <= 0) ? (($CashRatioS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CashRatioS[0] > 0) ? "+" : "" }}{{ $CashRatioS[0] }}
            </td>
            <td class="center {{ ($CashRatioS[1] <= 0) ? (($CashRatioS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CashRatioS[1] > 0) ? "+" : "" }}{{ $CashRatioS[1] }}
            </td>
            <td class="center {{ ($CashRatioS[2] <= 0) ? (($CashRatioS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($CashRatioS[2] > 0) ? "+" : "" }}{{ $CashRatioS[2] }}
            </td>
            <td class="center {{ ($CashRatioAv <= 0) ? (($CashRatioAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($CashRatioAv > 0) ? "+" : "" }}{{ $CashRatioAv }}
            </td>
            <td class="center {{ (($CashRatioAv * 0.15) <= 0) ? ((($CashRatioAv * 0.15) < 0) ? 'red' : '') : 'green' }}">
                {{ (($CashRatioAv * 0.15) > 0) ? "+" : "" }}{{ ($CashRatioAv * 0.15) }}
            </td>
        </tr>
        
        <tr>
            <td class="center">Total</td>
            <td class="center">1</td>
            <td class="center" colspan="4"><b>Final score</b> (in total col.7 : col. 2):</td>
            <td class="center {{ ($FinancialPosition <= 0) ? (($FinancialPosition < 0) ? 'red' : '') : 'green' }}">
                <b>{{ ($FinancialPosition > 0) ? "+" : "" }}{{ $FinancialPosition }}</b>
            </td>
        </tr>
        
        <tr>
            <td colspan="7"><b>II. Rating of the company's financial performance</b></td>
        </tr>
        
        <tr>
            <td>Return on equity (ROE)</td>
            <td class="center">0.5</td>
            <td class="center {{ ($ROES[0] <= 0) ? (($ROES[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROES[0] > 0) ? "+" : "" }}{{ $ROES[0] }}
            </td>
            <td class="center {{ ($ROES[1] <= 0) ? (($ROES[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROES[1] > 0) ? "+" : "" }}{{ $ROES[1] }}
            </td>
            <td class="center {{ ($ROES[2] <= 0) ? (($ROES[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROES[2] > 0) ? "+" : "" }}{{ $ROES[2] }}
            </td>
            <td class="center {{ ($ROEAv <= 0) ? (($ROEAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROEAv > 0) ? "+" : "" }}{{ $ROEAv }}
            </td>
            <td class="center {{ (($ROEAv * 0.5) <= 0) ? ((($ROEAv * 0.5) < 0) ? 'red' : '') : 'green' }}">
                {{ (($ROEAv * 0.5) > 0) ? "+" : "" }}{{ ($ROEAv * 0.5) }}
            </td>
        </tr>
        
        <tr>
            <td>Return on assets (ROA)</td>
            <td class="center">0.3</td>
            <td class="center {{ ($ROAS[0] <= 0) ? (($ROAS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROAS[0] > 0) ? "+" : "" }}{{ $ROAS[0] }}
            </td>
            <td class="center {{ ($ROAS[1] <= 0) ? (($ROAS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROAS[1] > 0) ? "+" : "" }}{{ $ROAS[1] }}
            </td>
            <td class="center {{ ($ROAS[2] <= 0) ? (($ROAS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROAS[2] > 0) ? "+" : "" }}{{ $ROAS[2] }}
            </td>
            <td class="center {{ ($ROAAv <= 0) ? (($ROAAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($ROAAv > 0) ? "+" : "" }}{{ $ROAAv }}
            </td>
            <td class="center {{ (($ROAAv * 0.3) <= 0) ? ((($ROAAv * 0.3) < 0) ? 'red' : '') : 'green' }}">
                {{ (($ROAAv * 0.3) > 0) ? "+" : "" }}{{ ($ROAAv * 0.3) }}
            </td>
        </tr>
        
        <tr>
            <td>Sales Growth</td>
            <td class="center">0.2</td>
            <td class="center {{ ($SalesGrowthS[0] <= 0) ? (($SalesGrowthS[0] < 0) ? 'red' : '') : 'green' }}">
                {{ ($SalesGrowthS[0] > 0) ? "+" : "" }}{{ $SalesGrowthS[0] }}
            </td>
            <td class="center {{ ($SalesGrowthS[1] <= 0) ? (($SalesGrowthS[1] < 0) ? 'red' : '') : 'green' }}">
                {{ ($SalesGrowthS[1] > 0) ? "+" : "" }}{{ $SalesGrowthS[1] }}
            </td>
            <td class="center {{ ($SalesGrowthS[2] <= 0) ? (($SalesGrowthS[2] < 0) ? 'red' : '') : 'green' }}">
                {{ ($SalesGrowthS[2] > 0) ? "+" : "" }}{{ $SalesGrowthS[2] }}
            </td>
            <td class="center {{ ($SalesGrowthAv <= 0) ? (($SalesGrowthAv < 0) ? 'red' : '') : 'green' }}">
                {{ ($SalesGrowthAv > 0) ? "+" : "" }}{{ $SalesGrowthAv }}
            </td>
            <td class="center {{ (($SalesGrowthAv * 0.2) <= 0) ? ((($SalesGrowthAv * 0.2) < 0) ? 'red' : '') : 'green' }}">
                {{ (($SalesGrowthAv * 0.2) > 0) ? "+" : "" }}{{ ($SalesGrowthAv * 0.2) }}
            </td>
        </tr>
        
        
        <tr>
            <td class="center">Total</td>
            <td class="center">1</td>
            <td class="center" colspan="4"><b>Final score</b> (in total col.7 : col. 2):</td>
            <td class="center {{ ($FinancialPerformance <= 0) ? (($FinancialPerformance < 0) ? 'red' : '') : 'green' }}">
                <b>{{ ($FinancialPerformance > 0) ? "+" : "" }}{{ $FinancialPerformance }}</b>
            </td>
        </tr>
    </tbody>
</table>

<p style="text-indent: 50px;">Final rating score for {{ $financial_report->title }}'s financial condition: 
(<b><span class="{{ ($FinancialPosition <= 0) ? (($FinancialPosition < 0) ? 'red' : '') : 'green' }}">{{ ($FinancialPosition > 0) ? "+" : "" }}{{ $FinancialPosition }}</span></b> x 0.6) +
(<b><span class="{{ ($FinancialPerformance <= 0) ? (($FinancialPerformance < 0) ? 'red' : '') : 'green' }}">{{ ($FinancialPerformance > 0) ? "+" : "" }}{{ $FinancialPerformance }}</span></b> x 0.4) 
= <b><span class="{{ ($FinalRating <= 0) ? (($FinalRating < 0) ? 'red' : '') : 'green' }}">{{ ($FinalRating > 0) ? "+" : "" }}{{ number_format($FinalRating, 2, '.', ',') }}</span></b> ({{$FinalRV['rating']}} - {{$FinalRV['text']}})</p>

<p><b>Reference:</b> Financial condition scale</p>

<table cellpadding="0" cellspacing="0" style="margin: 0 auto; width: 60%;">
	<tr>
		<td class="center" colspan="2"><b>Total score</b></td>
		<td class="center" rowspan="2"><b>Sign</b></td>
		<td class="center" rowspan="2"><b>The qualitative assessment of a financial condition </b></td>
	</tr>
	<tr dontbreak="true">
		<td class="center"><b>from</b></td>
		<td class="center"><b>to</b><br><small>(inclusive)</small></td>
	</td>
	<tr>
		<td class="center">2</td>
		<td class="center">1.6</td>
		<td class="center">AAA</td>
		<td>Excellent</td>
	</tr>
	<tr>
		<td class="center">1.6</td>
		<td class="center">1.2</td>
		<td class="center">AA</td>
		<td>Very Good</td>
	</tr>
	<tr>
		<td class="center">1.2</td>
		<td class="center">0.8</td>
		<td class="center">A</td>
		<td>Good</td>
	</tr>
	<tr>
		<td class="center">0.8</td>
		<td class="center">0.4</td>
		<td class="center">BBB</td>
		<td>Positive</td>
	</tr>
	<tr>
		<td class="center">0.4</td>
		<td class="center">0</td>
		<td class="center">BB</td>
		<td>Normal</td>
	</tr>
	<tr>
		<td class="center">0</td>
		<td class="center">-0.4</td>
		<td class="center">B</td>
		<td>Satisfactory</td>
	</tr>
	<tr>
		<td class="center">-0.4</td>
		<td class="center">-0.8</td>
		<td class="center">CCC</td>
		<td>Unsatisfactory</td>
	</tr>
	<tr>
		<td class="center">-0.8</td>
		<td class="center">-1.2</td>
		<td class="center">CC</td>
		<td>Adverse</td>
	</tr>
	<tr>
		<td class="center">-1.2</td>
		<td class="center">-1.6</td>
		<td class="center">C</td>
		<td>Bad</td>
	</tr>
	<tr>
		<td class="center">-1.6</td>
		<td class="center">-2</td>
		<td class="center">D</td>
		<td>Critical</td>
	</tr>
</table>