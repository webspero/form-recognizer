<h3>2.2 Mga Ratios na Kakayahang Makita</h3>
<table cellpadding="5" cellspacing="0">
    <thead></thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Mga Ratios na Kakayahang Makita</td>
            <td class="center" colspan="{{ @count($balance_sheets)}}">Halaga sa %</td>
            <td class="center" rowspan="2">Magbago (col.4 - col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>1. Pangkalahatang Palugit.</td>
            @php
                $max_count = @count($balance_sheets);
            @endphp
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $GrossMargin = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $GrossMargin = ($income_statements[$x]->GrossProfit / $income_statements[$x]->Revenue) * 100;
                    }
                @endphp
                <td class="center">
                @php
                        if($GrossMargin != 0){
                        	$grossMarginDec = number_format($GrossMargin, 2, '.', ',');
                        	$grossMarginDecArr = explode('.',$grossMarginDec);

                        	if($grossMarginDecArr == 0 || $grossMarginDecArr == '00'){
                        		echo $grossMarginDec;
	                       	}else{
	                       		echo number_format($grossMarginDecArr[0], 0, '.', ',');
                        	}

                            if($GrossMargin < 0)
                                $grossSign = 0;
                        }else{
                            echo '-';
                    }
                        
                    @endphp
                </td>
                @php if($x==0) $textcontent1 = number_format($GrossMargin, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = 0;
                if($income_statements[0]->Revenue != 0 && isset($income_statements[$max_count]) && $income_statements[$max_count]->Revenue){
                    $Change = (($income_statements[0]->GrossProfit / 
                            $income_statements[0]->Revenue) -
                            ($income_statements[$max_count]->GrossProfit / 
                            $income_statements[$max_count]->Revenue)) * 100;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>2. Balik sa kita (palugit sa operasyon).</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $ROS = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $ROS = (($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->Revenue) * 100;
                    }
                @endphp
                <td class="center">
                    @if($ROS != 0)
                        {{ number_format($ROS, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent2 = number_format($ROS, 1, '.', ','); @endphp
            @endfor
            @php 
                if($income_statements[0]->FinanceCosts != 0 && isset($income_statements[$max_count]) && $income_statements[$max_count]->FinanceCosts != 0){
                    $Change = ((($income_statements[0]->ProfitLossBeforeTax + 
                            $income_statements[0]->FinanceCosts) /
                            $income_statements[0]->Revenue) -
                            (($income_statements[$max_count]->ProfitLossBeforeTax + 
                            $income_statements[$max_count]->FinanceCosts) /
                            $income_statements[$max_count]->Revenue)) * 100;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>3. Margin ng kita. </td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $ProfitMargin = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $ProfitMargin = ($income_statements[$x]->ProfitLoss / $income_statements[$x]->Revenue) * 100;
                    }
                @endphp
                <td class="center">
                    @if($ProfitMargin != 0)
                        {{ number_format($ProfitMargin, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ProfitMargin, 1, '.', ','); @endphp
            @endfor
            @php
                $Change = 0;
                if($income_statements[0]->Revenue != 0 && isset($income_statements[$max_count]) && $income_statements[$max_count]->Revenue != 0){
                    $Change = (($income_statements[0]->ProfitLoss /
                            $income_statements[0]->Revenue) -
                            ($income_statements[$max_count]->ProfitLoss /
                            $income_statements[$max_count]->Revenue)) * 100;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>Sanggunian:<br />Ratio ng saklaw ng interes (ICR). Katanggap-tanggap na halaga: hindi kukulangin sa 1.5.</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && $income_statements[$x]->FinanceCosts != 0){
                        $ICR = ($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->FinanceCosts;
                    } else {
                        $ICR = 0;
                    }
                    
                    $class = ($ICR >= 1.5) ? 'green' : 'red'
                @endphp
                <td class="center {{ $class }}">
                    @if($ICR != 0)
                        {{ number_format($ICR, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent4 = number_format($ICR, 1, '.', ','); @endphp
            @endfor
            @php 
                if($income_statements[0]->FinanceCosts != 0 && isset($income_statements[$max_count]) && $income_statements[$max_count]->FinanceCosts != 0){
                    $Change = (($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) /
                                $income_statements[0]->FinanceCosts) -
                                (($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts) /
                                $income_statements[$max_count]->FinanceCosts);
                } else {
                    $Change = 0;
                }
                
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
    </tbody>
</table>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_1_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>

<br/><br/>

<table cellpadding="5" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Mga Ratios na Kakayahang Makita</td>
            <td class="center" colspan="{{ @count($balance_sheets)-1 }}">Halaga sa %</td>
            <td class="center" rowspan="2">Magbago (col.4 - col.2)</td>
            <td class="center" rowspan="2">Paglalarawan ng ratio at ang halaga ng sanggunian</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets)-1; $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td width="12%">Return on equity (ROE)</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROE = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x + 1]->Equity) / 2)) * 100;
                        if($x == $max_count) $ROE1 = round($ROE, 1);
                        if($x == 0) $ROE2 = round($ROE, 1);
                        $class = ($ROE >= 12) ? 'green' : 'red';
                    }else{
                        $ROE = 0;
                        $ROE1 = 0;
                        $ROE2 = 0;
                    }
                @endphp
                <td class="center {{ $class }}">
                    @if($ROE != 0)
                        {{ number_format($ROE, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ROE, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROE2 - $ROE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                Kinakalkula ang ROE sa pamamagitan ng pagkuha ng halagang isang taon ng
                 kita (net profit) at paghati sa kanila ng
                 average na equity ng shareholder para sa panahong iyon, at ay
                 ipinahayag bilang isang porsyento. Ito ay isa sa pinaka
                 mahahalagang ratios sa pananalapi at sukatan ng kakayahang kumita.
                 Katanggap-tanggap na halaga: 12% o higit pa.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Balik ng pag-aari (ROA)</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROA = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Assets + 
                                    $balance_sheets[$x + 1]->Assets) / 2)) * 100;
                        if($x == $max_count) $ROA1 = round($ROA, 1);
                        if($x == 0) $ROA2 = round($ROA, 1);
                        $class = ($ROA >= 6) ? 'green' : 'red';
                    }else{
                        $ROA = 0;
                        $ROA1 = 0;
                        $ROA2 = 0;
                    }
                @endphp
                <td class="center {{ $class }}">
                    @if($ROA != 0)
                        {{ number_format($ROA, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent1 = number_format($ROA, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROA2 - $ROA1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 1, '.', ',');
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                Ang ROA ay kinakalkula sa pamamagitan ng paghahati ng netong kita sa kabuuan
                 mga assets, at ipinakita bilang isang porsyento. Katanggap-tanggap
                 halaga: 6% o higit pa.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Nagtatrabaho sa pagbabalik ng kapital (ROCE)</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php

                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROCE = (
                                (
                                $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts
                                ) / 
                                (
                                    (
                                    $balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x]->NoncurrentLiabilities + 
                                    $balance_sheets[$x + 1]->Equity + 
                                    $balance_sheets[$x + 1]->NoncurrentLiabilities
                                    ) / 
                                2)
                            ) * 100;
                        if($x == $max_count) $ROCE1 = round($ROCE, 1);
                        if($x == 0) $ROCE2 = round($ROCE, 1);
                    }else{
                        $ROCE = 0;
                        $ROCE1 = 0;
                        $ROCE2 = 0;
                    }
                @endphp
                <td class="center">
                    @if($ROCE != 0)
                        {{ number_format($ROCE  , 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = $ROCE2 - $ROCE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                Ang ROCE ay kinakalkula sa pamamagitan ng paghahati ng EBIT ng kabisera
                 nagtatrabaho (equity plus non-kasalukuyang pananagutan). Ito
                 ay nagpapahiwatig ng kahusayan at kakayahang kumita ng a
                 pamumuhunan ng kapital ng kumpanya.
            </td>
        </tr>
    </tbody>
</table>

<br /><br/>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_2_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>