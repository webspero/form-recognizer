<h3> 1. 3 Pagsusuri sa Pagpapanatili ng Pananalapi</h3>
<h3>1.3.1 Susing Tumbasan ng Pagpapatuloy ng Pananalapi ng Kumpanya</h3>
    <table class="table">
        <tr>
            <td rowspan="2" style="text-align:center;">Ratio</td>
            <td colspan="{{count($financial_table)}}" style="text-align:center;">Value</td>
            <td rowspan="2" style="text-align:center;">Change (Col{{count($financial_table)}}-Col.2)</td>
            <td rowspan="2" style="text-align:center;">Description of the ratio and it's recommended value</td>
        </tr>
        <tr>
            @foreach($financial_table as $val)
                <td style="text-align:center;">12/31/{{$val->year}}</td>
            @endforeach
        </tr>
        <tr>
            <td>Ratio sa utang sa equity (financial leverage)</td>
            <?php
                $i=0;
                $debtToEquityFinancialPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->FinancialLeverage <= 1.5)
                    <?php
                        array_push($debtToEquityFinancialPositiveValues, $val->FinancialLeverage);
                    ?>
                    <td style="text-align:center; color:green;">{{$val->FinancialLeverage}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FinancialLeverage}}</td>
                @endif

                <?php
                    if ($i == 0) {
                        $firstDebtToEquityFinancial = $val->FinancialLeverage;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityFinancial = $val->FinancialLeverage;
                        $lastDebtToEquityFinancialColor = 'red';
                        if($lastDebtToEquityFinancial <= 1.5)
                        {
                            $lastDebtToEquityFinancialColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'red'; ?>
            @endif
            
            <td>Ang isang tumbas ng utang-sa-karampatan ay kinakalkula sa pamamagitan ng pagkuha ng kabuuang mga pananagutan at paghahati nito sa equity ng mga shareholder. Ito ang susi sa pananalapi at ginagamit bilang
                 isang pamantayan para sa paghusga sa kalagayan sa pananalapi ng isang kumpanya. Karaniwang halaga: 1.5 o mas mababa (pinakamabuting kalagayan na 0.43-1).</td>
        </tr>
        <tr>
            <?php
                $i=0;
                $debtRatioPositiveValues=[];
            ?>
            <td>Utang na ratio (utang sa ratio ng mga assets)</td>
            @foreach($financial_table as $val)
                @if($val->DebtRatio <= 0.6)
                    <td style="text-align:center; color:green;">{{$val->DebtRatio}}</td>
                    <?php array_push($debtRatioPositiveValues, $val->DebtRatio);?>
                @else
                    <td style="text-align:center; color:red;">{{$val->DebtRatio}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstDebtToEquityAsset = $val->DebtRatio;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityAsset = $val->DebtRatio;
                        $lastDebtToEquityAssetColor = 'red';
                        if($lastDebtToEquityAsset <= 0.6)
                        {
                            $lastDebtToEquityAssetColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['DebtRatio_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'red'; ?>
            @endif
            <td>Ang isang karampatan ng utang ay kinakalkula sa pamamagitan ng paghahati ng kabuuang mga pananagutan (hal. Pangmatagalan at panandaliang pananagutan) ng kabuuang mga yaman. Ipinapakita nito kung magkano ang kumpanyang
                 umaasa sa utang upang tustusan ang mga assets (katulad ng karampatan ng utang-sa-karampatan). Karaniwang halaga: 0.6 o mas mababa (optimum na 0.3-0.5).</td>
        </tr>
        <tr>
            <td>Pangmatagalang utang sa Equity</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->LTDtoE}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['LTDtoE_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['LTDtoE_change']}}</td>
            @endif
            <td>Ang karampatan na ito ay kinakalkula sa pamamagitan ng paghahati ng mga pangmatagalang (hindi kasalukuyang) pananagutan sa pamamagitan ng karampatan.</td>
        </tr>
        <tr>
            <td>Mga hindi kasalukuyang assets sa halagang Neto</td>
            <?php
                $i=0;
                $NCAtoNWPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->NCAtoNW <= 1.25)
                    <td style="text-align:center; color:green;">{{$val->NCAtoNW}}</td>
                    <?php array_push($NCAtoNWPositiveValues, $val->NCAtoNW); ?>
                @else
                    <td style="text-align:center; color:red;">{{$val->NCAtoNW}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstNCAtoNW = $val->NCAtoNW;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastNCAtoNW = $val->NCAtoNW;
                        $lastNCAtoNWColor = 'red';
                        if($lastNCAtoNW <= 1.25)
                        {
                            $lastNCAtoNWColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['NCAtoNW_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'red'; ?>
            @endif
            <td>Ang ratio na ito ay kinakalkula sa pamamagitan ng paghahati ng mga pangmatagalang (hindi kasalukuyang) pananagutan sa pamamagitan ng netong halaga (equity) at sinusukat ang lawak ng pamumuhunan ng isang kumpanya sa
                 mababang-pagkatubig na mga di-kasalukuyang assets. Mahalaga ang ratio na ito para sa pagtatasa ng paghahambing sapagkat hindi gaanong nakasalalay sa industriya (istraktura ng kumpanya
                 mga assets) kaysa sa ratio ng utang at ratio ng debt-to-equity. Karaniwang halaga: hindi hihigit sa 1.25.</td>
        </tr>
        <tr>
            <td>Capitalization ratio</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->Capitalization}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['Capitalization_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['Capitalization_change']}}</td>
            @endif
            <td>Kinakalkula sa pamamagitan ng paghahati ng mga hindi kasalukuyang pananagutan sa pamamagitan ng kabuuan ng equity at hindi kasalukuyang pananagutan.</td>
        </tr>
        <tr>
            <td>Ang mga naayos na assets sa halagang Net</td>
            @foreach($financial_table as $val)
                @if($val->DebtRatio <= 0.75)
                    <td style="text-align:center; color:green;">{{$val->FAtoNW}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FAtoNW}}</td>
                @endif
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FAtoNW_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FAtoNW_change']}}</td>
            @endif
            <td>Ipinapahiwatig ng ratio na ito kung hanggang saan ang pera ng mga nagmamay-ari ay na-freeze sa anyo ng mga nakapirming assets, tulad ng pag-aari, halaman, at kagamitan,
                 pag-aari ng pamumuhunan at di-kasalukuyang mga biological assets. Karaniwang halaga: 0.75 o mas mababa.</td>
        </tr>
        <tr>
            <td>Current Liability Ratio</td>
            @foreach($financial_table as $val) 
                <td style="text-align:center;">{{$val->CLiabilityRatio}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['CLiabilityRatio_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['CLiabilityRatio_change']}}</td>
            @endif
            <td>Ang kasalukuyang ratio ng pananagutan ay kinakalkula sa pamamagitan ng paghahati ng mga hindi kasalukuyang pananagutan sa pamamagitan ng kabuuang (hal. Kasalukuyan at hindi kasalukuyang) mga pananagutan.</td>
        </tr>

    </table>
    <div>
        <p>Una, ang pansin ay dapat na iguhit sa ratio ng utang-sa-katarungan at ratio ng utang bilang mga ratios na naglalarawan sa istraktura ng kapital. Ang parehong mga ratio ay may katulad na kahulugan at ipahiwatig kung walang sapat na kapital (equity) para sa matatag na trabaho para sa kumpanya. Ang ratio ng utang-sa-katarungan ay kinakalkula bilang isang ugnayan ng hiniram na kapital (pananagutan) sa equity, habang ang ratio ng utang ay kinakalkula bilang isang relasyon ng mga pananagutan sa pangkalahatang kapital (ibig sabihin ang kabuuan ng equity at liability).<p>

        <?php
            // Debt to equality and asset text, no more than 1.5
            $debtRatioToEquityText = '';
            if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                // All negative including change value
                $debtRatioToEquityText = 'Sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.', ang utang-sa-katarungan ay katumbas ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Para sa buong panahon na nasuri, napansin na mayroong isang napahahalagang pagbawas sa ratio ng utang sa <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> (<span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = 'Sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.', ang utang-sa-katarungan ay nagkakahalaga ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Sa panahon ng pagsusuri, ang ratio ng utang ay tumaas nang kaunti (to <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = 'Sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.', ang utang-sa-katarungan ay nagkakahalaga ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span> Ang ratio ng utang ay umabot sa <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.'. Para sa panahong sinuri (31.12.14–31.12.17), napatunayan na mayroong isang mabilis na pagtaas sa ratio ng utang ng <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                $debtRatioToEquityText = 'Sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity amounted to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Ang ratio ng utang ay umabot sa <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> sa pagtatapos ng panahon na pinag-aralan; ito a <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span> mas mababa kaysa sa antas ng ratio ng utang sa unang araw ng panahong pinag-aralan.';
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial > 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioToEquityText = 'Sa pagtatapos ng panahon na sinuri, ang utang-sa-katarungan ay katumbas ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Ang ratio ng utang ay katumbas ng <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> sa huling araw ng panahong pinag-aralan (12/31/'.$financial_table[count($financial_table)-1]->year.'). Sa panahong nasuri, napatunayan na mayroong isang makabuluhang pagtaas sa ratio ng utang ng <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', ang utang-sa-katarungan ay <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Ang ratio ng utang ay katumbas ng <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> on 12/31/'.$financial_table[count($financial_table)-1]->year.', sa parehong oras ang ratio ng utang ay mas mababa at katumbas - sa simula ng panahon (12/31/'.$financial_table[0]->year.') (i.e. the growth was <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $debtRatioToEquityText = 'On 12/31/'.$financial_table[count($financial_table)-1]->year.', ang utang-sa-katarungan ay katumbas ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Ang ratio ng utang ay nabawasan at ipinakita <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span> para sa buong panahon na pinag-aralan.';
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtToEquityFinancialPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change minus, but in middle minus
                $debtRatioToEquityText = 'Sa pagtatapos ng panahon ng pagsusuri, ang utang-sa-katarungan ay katumbas ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. On 31 December, '.$financial_table[count($financial_table)-1]->year.', ang ratio ng utang ay <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. Sa panahon ng huling '.count($balance_sheets).' taon, nalaman na mayroong isang pambihirang paglaki sa ratio ng utang ng <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else
            {
                if($lastDebtToEquityFinancial <= 1.5 && $financial_change['DebtRatio_change'] >= 0 )
                {
                    $debtRatioToEquityText = 'Sa pagtatapos ng panahon ng pagsusuri, ang utang-sa-katarungan ay katumbas ng <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. Sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.', ang ratio ng utang ay <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. Sa panahon ng huling '.count($balance_sheets).' taon, nalaman na mayroong isang pambihirang paglaki sa ratio ng utang ng <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
                }
                else
                {
                    $debtRatioToEquityText = 'Sa pagtatapos ng panahon ng pagsusuri, ang utang-sa-katarungan ay katumbas ng '.$lastDebtToEquityFinancial.'. Sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.', ang ratio ng utang ay <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. Sa panahon ng huling '.count($balance_sheets).' taon, ito ay natagpuan na may isang nabawasan sa utang ratio ng <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
                }
            }
        ?>
        
        <p>{!! $debtRatioToEquityText !!}</p>

        <?php
            // Debt to equality and asset text
            $debtRatioText = '';
            if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                // All negative including change value
                $debtRatioText = 'Ang halaga ng ratio ng utang ay negatibong naglalarawan sa posisyon ng pananalapi ng '.$financial_report->title.' on 12/31/'.$financial_table[count($financial_table)-1]->year.', ang porsyento ng mga pananagutan ay masyadong mataas at katumbas '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kapital na kumpanya. Ang maximum na katanggap-tanggap na porsyento ay 60%. Masyadong mas mataas na pagtitiwala mula sa mga nagpapautang ay maaaring magpababa ng katatagan sa pananalapi ng kumpanya, lalo na sa kaso ng kawalang-tatag ng ekonomiya at krisis sa merkado ng hiniram na kapital. Inirerekumenda na panatilihin ang halaga ng ratio ng utang sa antas na 0.6 o mas mababa (pinakamabuting kalagayan 0.3-0.5).';
            }
            else if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All negative, change plus
                $debtRatioText = 'Ang halaga ng ratio ng utang para sa '.$financial_report->title.' nagpapahiwatig ng labis na porsyento ng mga pananagutan sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.', na katumbas '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kabuuang kapital. Pinaniniwalaan na ang mga pananagutan ay hindi dapat higit sa 60%. Kung hindi, ang pagtitiwala mula sa mga nagpapautang ay tumataas nang labis at ang katatagan ng pananalapi ng kumpanya ay naghihirap. Kapag normal ang istraktura ng kapital, ang ratio ng utang ay hindi hihigit sa 0.6 (pinakamabuting kalagayan 0.3-0.5).';
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All positive, change plus
                $debtRatioText = 'Ayon sa ratio ng utang, ang porsyento ng hiniram na kapital (pananagutan) ay mas mababa kaysa sa tinatanggap na halaga at ginagawang 2.9% ng pangkalahatang kapital sa 12/31 /'.$financial_table[count($financial_table)-1]->year.'. Sa isang banda positibong inilalarawan nito ang sitwasyong pampinansyal ng '.$financial_report->title.'. Sa kabilang banda sinasabi nito tungkol sa mga hindi nakuha na pagkakataon na gumamit ng hiniram na kapital para sa pagpapalawak ng aktibidad at pagpapabilis ng mga rate ng pag-unlad. Maaaring dagdagan ng kumpanya ang porsyento ng mga kredito at utang nang walang pinsala sa sitwasyong pampinansyal nito kung ang isang plano sa mahusay na paggamit ng karagdagang kapital ay magagamit. Ang ratio ng utang ay nag-iingat ng katanggap-tanggap na halaga sa buong panahon.';
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                // All positive, change minus
                $debtRatioText = 'Inilalarawan ng ratio ng utang ang posisyon sa pananalapi ng '.$financial_report->title.' sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.' bilang napakahusay, ang porsyento ng mga pananagutan ay '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. Ang maximum na katanggap-tanggap na porsyento ng mga pananagutan ay itinuturing na 60%. Kapag ang mga mapagkukunan upang pondohan ang kumpanya ay pinlano, ang porsyento na ito ay dapat gamitin bilang isang mas mataas na limitasyon ng katanggap-tanggap na porsyento ng hiniram na kapital. Ang ratio ng utang ay may katanggap-tanggap na halaga sa kabuuan ng pinag-aralan na panahon.';
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset > 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioText = 'Ang halaga ng ratio ng utang ay negatibong naglalarawan sa posisyon ng pananalapi ng '.$financial_report->title.' sa pagtatapos ng panahon ng pagsusuri, ang porsyento ng mga pananagutan ay masyadong mataas at katumbas '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kabuuang kabisera ng kumpanya. Ang maximum na katanggap-tanggap na porsyento ay 60%. Masyadong mas mataas na pagtitiwala mula sa mga nagpapautang ay maaaring magpababa ng katatagan sa pananalapi ng kumpanya, lalo na sa kaso ng kawalang-tatag ng ekonomiya at krisis sa merkado ng hiniram na kapital. Inirerekumenda na panatilihin ang halaga ng ratio ng utang sa antas na hindi hihigit sa 0.6 (pinakamabuting kalagayan 0.3-0.5). Sa kabila ng katotohanang sa simula ng pinag-aralan na panahon at ang halaga ng ratio ng utang ay tumutugma sa pamantayan, kalaunan ay hindi ito katanggap-tanggap.';
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $debtRatioText = 'Ang halaga ng ratio ng utang ay nauugnay sa '.$financial_report->title.' magandang istruktura ng kapital sa Disyembre 31, '.$financial_table[count($financial_table)-1]->year.' mula sa pananaw ng katatagan sa pananalapi. Ang mga porsyento ng pananagutan ay '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kabuuang kabisera ng kumpanya. Bagaman ang pangwakas na halaga ng ratio ng utang ay pinananatili sa pamantayan nito, ang rate ay pangunahing hindi katanggap-tanggap sa buong panahon ng pinag-aralan.';
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $debtRatioText = 'Ang halaga ng ratio ng utang ay nauugnay sa '.$financial_report->title.' mahusay na istraktura ng kapital sa huling araw ng panahong pinag-aralan (12/31/'.$financial_table[count($financial_table)-1]->year.') mula sa pananaw ng katatagan sa pananalapi. Ang mga porsyento ng pananagutan ay '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kabuuang kabisera ng kumpanya. Sa kabila ng katotohanang sa simula ng isinasaalang-alang na panahon, ang halaga ng ratio ng utang ay hindi tumutugma sa pamantayan, kalaunan ay naging katanggap-tanggap ito.';
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtRatioPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change plus, and in middle minus
                $debtRatioText = 'Inilalarawan ng ratio ng utang ang posisyon sa pananalapi ng '.$financial_report->title.' sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.' bilang napakahusay, ang porsyento ng mga pananagutan ay '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. Ang maximum na katanggap-tanggap na porsyento ng mga pananagutan ay itinuturing na 60%. Kapag ang mga mapagkukunan upang pondohan ang kumpanya ay pinlano, ang porsyento na ito ay dapat gamitin bilang isang mas mataas na limitasyon ng katanggap-tanggap na porsyento ng hiniram na kapital. Sa kabuuan ng pinag-aralan na panahon, ang ratio ng utang ay nag-iingat ng katanggap-tanggap na halaga.';
            }
            else
            {
                if($lastDebtToEquityAsset <= 0.6)
                {
                    $debtRatioText = 'Inilalarawan ng ratio ng utang ang posisyon sa pananalapi ng '.$financial_report->title.' sa 31 Disyembre, '.$financial_table[count($financial_table)-1]->year.' bilang napakahusay, ang porsyento ng mga pananagutan ay '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. Ang maximum na katanggap-tanggap na porsyento ng mga pananagutan ay itinuturing na 60%. Kapag ang mga mapagkukunan upang pondohan ang kumpanya ay pinlano, ang porsyento na ito ay dapat gamitin bilang isang mas mataas na limitasyon ng katanggap-tanggap na porsyento ng hiniram na kapital. Sa pagtatapos ng pinag-aralan na panahon, ang ratio ng utang ay naging katanggap-tanggap.';
                }
                else
                {
                    $debtRatioText = 'Ang halaga ng ratio ng utang ay negatibong naglalarawan sa posisyon ng pananalapi ng '.$financial_report->title.' sa pagtatapos ng panahon ng pagsusuri, ang porsyento ng mga pananagutan ay masyadong mataas at katumbas '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% ng kabuuang kabisera ng kumpanya. Ang maximum na katanggap-tanggap na porsyento ay 60%. Masyadong mas mataas na pagtitiwala mula sa mga nagpapautang ay maaaring magpababa ng katatagan sa pananalapi ng kumpanya, lalo na sa kaso ng kawalang-tatag ng ekonomiya at krisis sa merkado ng hiniram na kapital. Inirerekumenda na panatilihin ang halaga ng ratio ng utang sa antas na hindi hihigit sa 0.6 (pinakamabuting kalagayan 0.3-0.5).';
                }
            }
        ?>

        <p>{!! $debtRatioText !!}</p>
        <p>Ang istraktura ng kapital ng kumpanya ay ipinapakita sa tsart sa ibaba:</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>

        <?php
            // Non-current assets to Net worth text
            $NCAtoNWText = '';
            if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] < 0))
            {
                // All negative including change value
                $NCAtoNWText = 'Ayon sa karaniwang mga patakaran, ang mga hindi kasalukuyang pamumuhunan ay dapat gawin, una, sa tulong ng pinaka-matatag na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ipinapakita ng mga hindi kasalukuyang assets sa ratio na nagkakahalaga ng Net kung sinusunod ang panuntunang ito. Sa 12/31/'.$financial_table[count($financial_table)-1]->year.', ang ratio ay katumbas <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>, habang ang ratio ay katumbas <span class="'.$lastNCAtoNWColor.'">'.$firstNCAtoNW.'</span> (i.e. a pagbaba ng <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) ay natagpuan.';
            }
            else if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                $NCAtoNWText = 'Ayon sa karaniwang mga patakaran, ang mga hindi kasalukuyang pamumuhunan ay dapat gawin, una, sa tulong ng pinaka-matatag na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ipinapakita ng mga hindi kasalukuyang assets sa ratio na nagkakahalaga ng Net kung sinusunod ang panuntunang ito. Ang ratio ay katumbas ng <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> on 12/31/'.$financial_table[count($financial_table)-1]->year.'. During the period reviewed (31.12.'.$financial_table[0]->year.'–31.12.'.$financial_table[count($financial_table)-1]->year.'), the ratio rapidly increased (<span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>).';
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                // All positive including change
                $NCAtoNWText = 'Ayon sa karaniwang mga patakaran, ang mga hindi kasalukuyang pamumuhunan ay dapat gawin, una, sa tulong ng pinaka-matatag na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ipinapakita ng mga hindi kasalukuyang assets sa ratio na nagkakahalaga ng Net kung sinusunod ang panuntunang ito. Matindi ang paglaki ng ratio (ng <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) and katumbas <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> para sa buong panahon na nasuri. Sa pagtatapos ng panahon na sinuri, ang halaga ng ratio ay maaaring inilarawan bilang mahusay.';
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] < 0) )
            {
                // All positive, except for change
                $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang assets) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng panuntunang ito ay ang mga di-kasalukuyang assets sa net ratio na nagkakahalaga. Ang ratio ay umabot sa <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> sa huling araw ng panahong pinag-aralan. Para sa panahong sinuri, mabilis na bumagsak ang ratio (ng <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>). Sa huling araw ng panahon na sinuri, ang ratio ay nagpapakita ng isang napakahusay na halaga.';
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW > 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang pag-aari) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng patakarang ito ay ang mga hindi kasalukuyang assets sa net ratio na nagkakahalaga. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', ang ratio ay <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. Para sa buong panahon na nasuri, nalaman na mayroong matinding pagtaas sa ratio ng <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>, bukod dito, ang katulad na pagkahilig ay napatunayan ng isang linear na takbo sa panahon. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', nagpapakita ang ratio ng isang hindi kasiya-siyang halaga.';
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang pag-aari) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng patakarang ito ay ang mga hindi kasalukuyang assets sa net ratio na nagkakahalaga. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', ang ratio ay katumbas ng <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>;. Sa pagtatapos ng panahon na sinuri, ang halaga ng ratio ay maaaring inilarawan bilang katanggap-tanggap.';
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang pag-aari) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng patakarang ito ay ang mga hindi kasalukuyang assets sa net ratio na nagkakahalaga. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', ang ratio ay katumbas ng <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>; it is <span class="red">'.$firstNCAtoNW.'</span> mas mababa kaysa sa Disyembre 31, '.$financial_table[0]->year.'. Sa pagtatapos ng panahon na sinuri, ang halaga ng ratio ay maaaring inilarawan bilang katanggap-tanggap.';
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0) && (count($NCAtoNWPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change minus, but in middle minus
                $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang pag-aari) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng patakarang ito ay ang mga hindi kasalukuyang assets sa net ratio na nagkakahalaga. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', ang ratio ay katumbas ng <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. Sa pagtatapos ng panahon na sinuri, ang halaga ng ratio ay maaaring inilarawan bilang katanggap-tanggap.';
            }
            else
            {
                if($lastNCAtoNW <= 1.25 && $financial_change['NCAtoNW_change'] >= 0 )
                {
                    $NCAtoNWText = 'Ayon sa karaniwang mga patakaran, ang mga hindi kasalukuyang pamumuhunan ay dapat gawin, una, sa tulong ng pinaka-matatag na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ipinapakita ng mga hindi kasalukuyang assets sa ratio na nagkakahalaga ng Net kung sinusunod ang panuntunang ito. Matindi ang paglaki ng ratio (ng <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) at katumbas <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> para sa buong panahon na nasuri. Sa pagtatapos ng panahon na sinuri, ang halaga ng ratio ay maaaring inilarawan bilang mahusay.';
                }
                else
                {
                    $NCAtoNWText = 'Ayon sa mga kilalang prinsipyo para sa matatag na pag-unlad ng kumpanya, ang mga pamumuhunan na may pinakamaliit na likidong assets (mga hindi kasalukuyang pag-aari) ay dapat munang gawin sa tulong mula sa pinakatagal na mapagkukunan ng financing, ibig sabihin sa tulong ng sariling kapital (equity). Ang isang tagapagpahiwatig ng patakarang ito ay ang mga hindi kasalukuyang assets sa net ratio na nagkakahalaga. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', ang ratio ay <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. Sa 31.12.'.$financial_table[count($financial_table)-1]->year.', nagpapakita ang ratio ng isang hindi kasiya-siyang halaga.';
                }
            }
        ?>

        <p>{!! $NCAtoNWText !!}</p>
        <?php
            $nonCurrentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->nonCurrentLiabilitiesPercentage;
            $currentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->currentLiabilitiesPercentage;

            $liabilityText = '';
            if( ($nonCurrentLiabilitiesPercentage>40) && ($currentLiabilitiesPercentage>40) ){
                $liabilityText = 'Ang kasalukuyang ratio ng pananagutan ay ipinapakita na ang kasalukuyan at hindi kasalukuyang pananagutan ng kumpanya ay halos pantay ('.$currentLiabilitiesPercentage.'% at '.$nonCurrentLiabilitiesPercentage.'%, ayon sa pagkakabanggit).';
            }
            else if($nonCurrentLiabilitiesPercentage > $currentLiabilitiesPercentage)
            {
                $liabilityText = 'Ipinapakita ng kasalukuyang ratio ng pananagutan na ang dami ng mga hindi kasalukuyang pananagutan ng '.$financial_report->title.' makabuluhang lumampas sa halaga ng kasalukuyang mga pananagutan ('.$nonCurrentLiabilitiesPercentage.'% at '.$currentLiabilitiesPercentage.'% ng kabuuang mga pananagutan, ayon sa pagkakabanggit).';
            }
            else
            {
                // Current liabilities (short-term liabilities)
                $liabilityText = 'Ipinapakita ng kasalukuyang ratio ng pananagutan na ang bahagi ng panandaliang at pangmatagalang pananagutan ng kabuuang mga pananagutan ng '.$financial_report->title.' katumbas '.$currentLiabilitiesPercentage.'% at '.$nonCurrentLiabilitiesPercentage.'% ayon sa pagkakabanggit sa pagtatapos ng panahon na nasuri.';
            }
        ?>
        <p>{!! $liabilityText !!}</p>
        <p>Ipinapakita ng sumusunod na tsart ang dynamics ng pangunahing ratios ng katatagan sa pananalapi ng {{$financial_report->title}}.</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>
    </div>