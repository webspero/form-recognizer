    <h3>1.2. Mga Net Asset (Net Worth)</h3>
    
    <table>
        <thead>
        <tr class="center">
            <td rowspan="3">Tagapagpahiwatig</td>
            <td colspan="{{ count($balance_sheets)+2 }}">Halaga</td>
            <td colspan="2">Magbago</td>
        </tr>
        <tr class="center">
            <td colspan="2">sa PHP</td>
            <td colspan="{{ count($balance_sheets) }}">% ng kabuuan ng balanse</td>
            <td rowspan="2">PHP (col.3-col.2)</td>
            <td rowspan="2">% ((col.3-col.2):col.2)</td>
        </tr>
        <tr class="center">
            <td>sa simula ng panahong pinag-aralan(12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td>sa pagtatapos ng panahong pinag-aralan(12/31/{{ (int)$financial_report->year }})</td>
            @for($x=count($balance_sheets); $x > 0 ; $x--)
                <td>12/31/{{ ($financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr class="center">
            @for($indexCount = 1; $indexCount <= (count($balance_sheets)+5); $indexCount++)
                <td> {{ $indexCount }}</td>
            @endfor
        </tr>
        </thead>
        <tbody>
            @php 
                // beginning of the period
                $b_intangible_assets = $balance_sheets[count($balance_sheets)-1]->Goodwill+$balance_sheets[count($balance_sheets)-1]->IntangibleAssetsOtherThanGoodwill;
                $b_net_tangible_assets = $balance_sheets[count($balance_sheets)-1]->Equity-$b_intangible_assets;
                $b_net_assets = $balance_sheets[count($balance_sheets)-1]->Equity;
                $b_issued_capital = $balance_sheets[count($balance_sheets)-1]->IssuedCapital;
                $b_difference = $b_net_assets-$b_issued_capital;

                // end of the period
                $e_intangible_assets = $balance_sheets[0]->Goodwill+$balance_sheets[0]->IntangibleAssetsOtherThanGoodwill;
                $e_net_tangible_assets = $balance_sheets[0]->Equity-$e_intangible_assets;
                $e_net_assets = $balance_sheets[0]->Equity;
                $e_issued_capital = $balance_sheets[0]->IssuedCapital;
                $e_difference = $e_net_assets-$e_issued_capital;

                // change column
                $net_tangible_assets_change = $e_net_tangible_assets - $b_net_tangible_assets;
                if($b_net_tangible_assets > 0) {
                    $net_tangible_assets_change_prec = ($net_tangible_assets_change/$b_net_tangible_assets)*100;
                    $net_tangible_assets_change_times = $e_net_tangible_assets/$b_net_tangible_assets;
                } else {
                    $net_tangible_assets_change_prec = 0;
                    $net_tangible_assets_change_times = 0;
                }
                $net_assets_change = $e_net_assets - $b_net_assets;
                if($b_net_assets > 0) {
                    $net_assets_change_prec = ($net_assets_change/$b_net_assets)*100;
                    $net_assets_change_times = $e_net_tangible_assets/$b_net_assets;
                } else {
                    $net_assets_change_prec = 0;
                    $net_assets_change_times = 0;
                }
                $issued_capital_change = $e_issued_capital - $b_issued_capital;
                if($b_issued_capital > 0) {
                    $issued_capital_change_prec = ($issued_capital_change/$b_issued_capital)*100;
                    $issued_capital_change_times = $e_issued_capital/$b_issued_capital;
                } else {
                    $issued_capital_change_prec = 0;
                    $issued_capital_change_times = 0;
                }
                $difference_change = $net_assets_change - $issued_capital_change;
                if($b_difference > 0) {
                    $difference_change_prec = ($difference_change/$b_difference)*100;
                    $difference_change_times = $e_difference/$b_difference;
                } else {
                    $difference_change_prec = 0;
                    $difference_change_times = 0;
                }

                if($e_issued_capital > 0) {
                    $net_worth_times = $e_net_assets/$e_issued_capital;
                } else {
                    $net_worth_times = 0;
                }
                if($e_issued_capital > 0) {
                    $net_worth_percentage = (($e_net_assets/$e_issued_capital)-1)*100;
                } else {
                    $net_worth_percentage = 0;
                }
                $latest_net_tangible_assets_change = $e_net_assets-$e_net_tangible_assets;
                @endphp
            
            <tr>
                <td>1. Net Tangible assets</td>
                <td class="center"> {{ ($b_net_tangible_assets != 0) ? number_format($b_net_tangible_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_tangible_assets != 0) ? number_format($e_net_tangible_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_intangible_assets = $balance_sheets[$x]->Goodwill+$balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill;
                        $i_net_tangible_assets = $balance_sheets[$x]->Equity-$i_intangible_assets;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_tangible_assets_prec = ($i_net_tangible_assets/$i_assets)*100;
                        } else {
                            $i_net_tangible_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_tangible_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_tangible_assets_prec != 0) ? round($i_net_tangible_assets_prec,1) : '-' }}</td>
                @endfor
                    <td class="center nowrap {{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_tangible_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @elseif($net_tangible_assets_change > 0)
                        {{ '+'.number_format($net_tangible_assets_change) }}
                    @else
                        {{ $net_tangible_assets_change > 0 ? '+' : '' }}{{ number_format($net_tangible_assets_change) }}
                    @endif
                </td>
                @if($net_tangible_assets_change_prec > 200)
                    <td class="center green">
                        {{ $net_tangible_assets_change_times > 0 ? '+' : '' }}{{ round($net_tangible_assets_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">
                        @if($net_tangible_assets_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @else
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '' }}{{ round($net_tangible_assets_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>2. Net assets(Net Worth)</td>
                <td class="center"> {{ ($b_net_assets != 0) ? number_format($b_net_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_assets != 0) ? number_format($e_net_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_net_assets = $balance_sheets[$x]->Equity;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_assets_prec = ($i_net_assets/$i_assets)*100;
                        } else {
                            $i_net_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_assets_prec != 0) ? round($i_net_assets_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $net_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $net_assets_change > 0 ? '+' : '' }}{{ number_format($net_assets_change) }}
                    @endif
                </td>
                @if($net_assets_change_prec > 200)
                    <td class="center green">
                        {{ $net_assets_change_times > 0 ? '+' : '' }}{{ round($net_assets_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $net_assets_change_prec > 0 ? 'green' : 'red' }}">
                        @if($net_assets_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @else
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '' }}{{ round($net_assets_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>3. Issued (share) capital</td>
                <td class="center"> {{ ($b_issued_capital != 0) ? number_format($b_issued_capital) : '-' }}</td>
                <td class="center"> {{ ($e_issued_capital != 0) ? number_format($e_issued_capital) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_issued_capital = $balance_sheets[$x]->IssuedCapital;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_issued_capital_prec = ($i_issued_capital/$i_assets)*100;
                        } else {
                            $i_issued_capital_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_issued_capital_prec > 0 ? 'green' : 'red' }}"> {{ ($i_issued_capital_prec != 0) ? round($i_issued_capital_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $issued_capital_change > 0 ? 'green' : 'red' }}">
                    @if($issued_capital_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $issued_capital_change > 0 ? '+' : '' }}{{ number_format($issued_capital_change) }}
                    @endif
                </td>
                @if($issued_capital_change_prec > 200)
                    <td class="center green">
                        {{ $issued_capital_change_times > 0 ? '+' : '' }}{{ round($issued_capital_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $issued_capital_change_prec > 0 ? 'green' : 'red' }}">
                        @if($issued_capital_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @else
                            {{ $issued_capital_change_prec > 0 ? '+' : '' }}{{ round($issued_capital_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>4. Difference between net assets and Issued (share) capital (line 2 - line 3)</td>
                <td class="center {{ $b_difference > 0 ? 'green' : 'red' }}"> {{ ($b_difference != 0) ? number_format($b_difference) : '-' }}</td>
                <td class="center {{ $e_difference > 0 ? 'green' : 'red' }}"> {{ ($e_difference != 0) ? number_format($e_difference) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_retained_earnings = $balance_sheets[$x]->RetainedEarnings;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_difference_prec = ($i_retained_earnings/$i_assets)*100;
                        } else {
                            $i_difference_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_difference_prec > 0 ? 'green' : 'red' }}"> {{ ($i_difference_prec != 0) ? round($i_difference_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $difference_change > 0 ? 'green' : 'red' }}">
                    @if($difference_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $difference_change > 0 ? '+' : '' }}{{ number_format($difference_change) }}
                    @endif
                </td>
                @if($difference_change_prec > 200)
                    <td class="center green">
                        {{ $difference_change_times > 0 ? '+' : '' }}{{ round($difference_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $difference_change_prec > 0 ? 'green' : 'red' }}">
                        @if($difference_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @else
                            {{ $difference_change_prec > 0 ? '+' : '' }}{{ round($difference_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
        </tbody>
    </table>
    <div class="justify">
        <p>
            @if($net_tangible_assets_change > 0)
                @if($net_tangible_assets_change_prec > 40)
                    Ang net na nasasalat na assets ay katumbas ng PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_net_tangible_assets) }}</span> sa pagtatapos ng panahong sinuri.
                     Ang net na nasasalat na assets ay mahigpit na tumaas (PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($net_tangible_assets_change) }}</span>, or by <span class="{{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">{{ round($net_tangible_assets_change_prec, 1) }}</span>%) sa loob ng taon.
                @elseif($net_tangible_assets_change_prec > 15)
                    Sa huling araw ng panahon na pinag-aralan, ang net na nasasalat na mga assets ay nagkakahalaga ng PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green': 'red' }}"> {{ number_format($e_net_tangible_assets) }} </span>.
                    Para sa panahong pinag-aralan, napag-alaman na mayroong isang kasiya-siyang paglaki sa net na nahahawakan na mga assets mula sa <span class="{{ $b_net_tangible_assets > 0 ? 'green': 'red'}}"> {{ number_format($b_net_tangible_assets) }} </span> sa PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green': 'red'}}">{{ number_format($e_net_tangible_assets) }}</span>, na nagpakita ng PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green': 'red' }}">{{ $net_tangible_assets_change > 0 ? '+': '' }}{{ number_format($net_tangible_assets_change) }}</span>, o <span class="{{ $net_tangible_assets_change_prec > 0 ? 'green': 'red' }}">{{ round($net_tangible_assets_change_prec, 1) }}</span>%.
                @else
                    Ang net na nasasalat na assets ay katumbas ng PHP <span class = "{{$e_net_tangible_assets> 0 ? 'green': 'red'}}"> {{number_format ($e_net_tangible_assets) }}</span> sa pagtatapos ng panahon sinuri.
                    Sa buong panahon ng pagsusuri, nakita na may makabuluhang paglago sa mga netong mahihinang neto, na kung saan ay PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green': 'red'}}">{{ number_format($net_tangible_assets_change) }}</span>, o <span class="{{ $net_tangible_assets_change_prec > 0 ? 'green': 'red'}}">{{round( $net_tangible_assets_change_prec, 1) }}</span>%.
                @endif
            @else
                Napansin na mayroong halatang pagbaba ng net na mahihinang assets mula sa PHP <span class="{{ $b_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($b_net_tangible_assets) }}</span> to PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_net_tangible_assets) }}</span> (PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($net_tangible_assets_change) }}</span>) sa panahong sinuri.
                 Sa huling araw ng panahon na pinag-aralan, ang hindi madaling unawain na mga assets ay katumbas ng PHP <span class="{{ $latest_net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($latest_net_tangible_assets_change) }}</span>. Ipinapakita ng halagang ito ang pagkakaiba sa pagitan ng halaga ng net na nasasalat na mga assets at lahat ng net na nagkakahalaga.
            @endif
            @if($e_intangible_assets == 0)
                Sa kasong ito, {{ $financial_report->title }} walang mabuting kalooban o iba pang hindi madaling unawain na mga pag-aari.
                 Ito ang dahilan kung bakit ang halaga ng net na nagkakahalaga at net na nasasalat na mga assets ay pantay sa pagtatapos ng panahong sinuri.
            @endif
        </p>
        <p>
            @if($e_difference > 0)
                @if($net_worth_times > 4)
                    Ang neto nagkakahalaga (net assets) of {{ $financial_report->title }} ay mas mataas (ng <span class="{{ $net_worth_times > 0 ? 'green' : 'red' }}">{{ round($net_worth_times, 1) }}</span> times) kaysa sa pagbabahagi ng kapital sa huling araw ng panahong pinag-aralan (12/31/{{ (int)$financial_report->year}}).
                    Ang nasabing isang ratio ay positibong naglalarawan sa estado ng pananalapi ng kumpanya. 
                @else
                    Ang netong halaga ng {{ $financial_report->title }} lumagpas sa pagbabahagi ng kapital ng <span class="{{ $net_worth_percentage > 0 ? 'green' : 'red' }}">{{ round($net_worth_percentage, 1) }}</span>% on 12/31/{{ (int)$financial_report->year}}.
                    Ang ganoong sitwasyon ay normal, ang netong nagkakahalaga (net assets) ay hindi dapat mas mababa kaysa sa pagbabahagi ng kapital.
                @endif
                Ginagamit ang netong halaga bilang isang sukatan ng halaga ng libro ng kumpanya (taliwas sa halaga ng shareholder, ang halaga batay sa inaasahang kita at iba pang mga pamamaraan na ginamit upang tantyahin ang halaga ng kumpanya).
                 Sa pagtatasa sa pananalapi, ang halaga ng net worth (sariling equity) ay isa sa mga pangunahing tagapagpahiwatig ng katayuan ng pag-aari ng kumpanya.
            @else
                on 12/31/{{ (int)$financial_report->year}}, ang net na nagkakahalaga (net assets) ng {{ $financial_report->title }} ay mas mababa kaysa sa pagbabahagi ng kapital (ang pagkakaiba ay gumagawa ng PHP <span class="{{ $e_difference > 0 ? 'green' : 'red' }}">{{ number_format(abs($e_difference)) }} </span>). 
                Ito ay isang negatibong rate na nagsasaad na natuklasan ng isang kumpanya ang isang pagkawala.
                 Ito ay itinuturing na normal kapag ang net nagkakahalaga (net assets) ng kumpanya ay mas mataas kaysa sa pagbabahagi ng kapital na nagpapakita ng kakayahang kumita ng negosyo.
                 Ginagamit ang netong halaga bilang isang sukatan ng halaga ng libro ng kumpanya (taliwas sa halaga ng shareholder, ang halaga batay sa inaasahang kita at iba pang mga pamamaraan na ginamit upang tantyahin ang halaga ng kumpanya).
                 Sa pagtatasa sa pananalapi, ang halaga ng net worth (sariling equity) ay isa sa mga pangunahing tagapagpahiwatig ng katayuan ng pag-aari ng kumpanya.
            @endif
        </p>
    </div>
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>
    @if($b_issued_capital == $e_issued_capital)
        <p>Ang naisyu (magbahagi) na kapital ay hindi nagbago sa kabuuan ng pinag-aralan na panahon.</p>
    @elseif($issued_capital_change_prec > 90)
        <p>Ang pagtaas sa inisyu (ibahagi) na kapital ay biglang sa pinag-aralan na panahon.</p>
    @elseif($issued_capital_change_prec > 50)
        <p>Sa nasuri na panahon, naganap ang malaking pagtaas sa inisyu (ibahagi) na kabisera.</p>
    @endif