<h3> 2. 3 Pagsusuri ng Aktibidad sa Negosyo (Mga Ratio sa Pagbabago)</h3>
<p>
    Dagdag sa talahanayan, ang mga kinakalkula na rate ng paglilipat ng mga assets at pananagutan ay naglalarawan kung gaano kabilis ang prepaid
     ang mga assets at pananagutan sa mga tagapagtustos, kontratista at kawani ay naisagawa. Ang mga ratio ng turnover ay may malakas na industriya
     detalye at nakasalalay sa aktibidad. Ito ang dahilan kung bakit ang isang ganap na halaga ng mga ratios ay hindi pinapayagan ang paggawa ng isang
     pagtatasa ng husay. Kapag pinag-aralan ang mga ratio ng pag-turnover ng mga assets, isang pagtaas sa mga ratios (ibig sabihin, tulin ng bilis ng
     sirkulasyon) at isang pagbawas sa mga araw ng sirkulasyon ay itinuturing na positibong dinamika. Walang naitukoy nang maayos
     pakikipag-ugnayan para sa mga account na mababayaran at paglilipat ng kapital. Sa anumang kaso, magagawa lamang ang isang tumpak na konklusyon
     pagkatapos ng mga kadahilanang sanhi ng mga pagbabagong ito ay isinasaalang-alang.
</p>
<table class="table">
    <tr>
        <td rowspan="2">Ratio ng turnover</td>
        <td colspan="{{count($turnoverRatios)}}">Value, Days</td>
        <td rowspan="2">Ratio</td>
        <td rowspan ="2">Ratio</td>
        <td rowspan ="2">Change, days Col - Col</td>
    </tr>
    <tr>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->year}}</td>
        @endforeach
    </tr>
    <tr>
        <td>
            Natanggap na paglilipat ng tungkulin (natitirang benta ng araw) <br>
            <small>(average na kalakalan at iba pang mga kasalukuyang natanggap na hinati sa average na pang-araw-araw na kita *)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->ReceivablesTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->ReceivablesTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->ReceivablesTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->ReceivablesTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Mga binabayaran na paglilipat ng mga account (araw na mababayaran na natitira) <br>
            <small>(average na kasalukuyang bayad na hinati sa average na pang-araw-araw na mga pagbili)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->PayableTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->PayableTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->PayableTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->PayableTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->PayableTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->PayableTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Inventory turnover (days inventory outstanding) <br>
            <small>(average inventory divided by average daily cost of sales)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->InventoryTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->InventoryTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->InventoryTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->InventoryTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Pag-turnover ng asset <br>
            <small>(average na kabuuang mga assets na hinati sa average na pang-araw-araw na kita)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->AssetTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->AssetTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->AssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->AssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->AssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->AssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Current asset turnover <br>
            <small>(average current assets divided by average daily revenue)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->CAssetTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->CAssetTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->CAssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->CAssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Capital turnover <br>
            <small>(average na equity na hinati ng average na pang-araw-araw na kita)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->CapitalTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->CapitalTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->CapitalTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->CapitalTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <i>Sanggunian:</i> <br>
            Siklo ng conversion ng cash <br>
            <small>(araw na natitirang benta + araw na natitirang imbentaryo - araw na mababayaran na hindi pa nababayaran) </small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->CCC}}</td>
        @endforeach
        <td style="text-align:center;">
            x
        </td>
        <td style="text-align:center;">
            x
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @endif
        </td>
    </tr>
</table>

<p>
    @php
        $totalDays = 0;
        $totalPayableDays = 0;
        $cnt = 0;
        foreach($turnoverRatios as $turnover){
            $totalDays += $turnover->AssetTurnover;
            $totalPayableDays += $turnover->PayableTurnover;
            $cnt += 1;
        }

        $arr = [
            'receivablesTurnover' => $turnoverRatios[0]->ReceivablesTurnover,
            'payableTurnover' => $turnoverRatios[0]->PayableTurnover,
            'financialReportTitle' => $financial_report->title,
            'totalDays'            => round(($totalDays/$cnt),0),
            'payableDays'          => round(($totalPayableDays/$cnt),0)
        ];

    @endphp

    Sa nakaraang taon, ang average na tagal ng pagkolekta (Natitirang Benta ng Araw) ay {{$turnoverRatios[0]->ReceivablesTurnover}} araw at ang
    average na araw na babayaran na natitira ay {{$arr['payableDays']}} araw tulad ng ipinakita sa talahanayan. Ang rate ng pag-turnover ng asset ay nangangahulugan na
    {{$financial_report->title}} nakakakuha ng kita na katumbas ng kabuuan ng lahat ng magagamit na mga assets tuwing {{$arr['totalDays']}} araw (sa average sa panahon ng sinuri).
</p>