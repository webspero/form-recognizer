<h3 class="pdf_title">Wakas</h3>
<h3>Buod ng Mahahalagang Tumbasan</h3>

<div>
    <p>
        Ang pangunahing tagapagpahiwatig ng estadong pampinansyal at {{$financial_report->title}} resulta ng gawain ay nauuri sa  kwalitatibong pagsusuri ayon sa resulta ng pagsusuri sa panahong sinuri(mula ika-31 ng Disyembre, {{$balance_sheets[count($balance_sheets)-1]->Year}} hanggang ika-31 ng Disyembre, {{$balance_sheets[0]->Year}}) at ibinigay sa baba.
    </p> 
    {{-- @if(($keySummary->NCAtoNW >= 0) && ($keySummary->NCAtoNW <= 1.25) && ($keySummary->DebtRatio < 0.30) && ($keySummary->DebtRatio <= 0.60)) --}}
    <p>May mga sumusunod na siguradong magaling na pampinansyal na tagapagpahiwatig:</p>
    <ul>
        {{-- Noncurrent assets --}}
        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
            <li>
                Ang halaga ng hindi pangkasalukuyang pag-aari tungo sa tumbasan ng netong halaga katumbas ay <span style="color:green;">{{number_format($keySummary[0]->NCAtoNW, 2, '.', ',')}}</span> ay walang dudang napakahusay.
            </li>
        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
            <li>
                Ang halaga ng hindi pangkasalukuyang pag-aari tungo sa tumbasan ng netong halaga katumbas ay <span style="color:green;">{{number_format($keySummary[0]->NCAtoNW, 2, '.', ',')}}</span> ay mabuti.
            </li>
        @endif

        {{-- Debt-to-equity Ratio --}}
        @if($keySummary[0]->DebtRatio < 0.30)
            <li>
                Ang tumbasan ng debt-to-equity at tumbasan ng pagkakautang ay nagpapakita ng magandang halaga, ngunit sabihin masyadong maingat ang kumpansya upang gamitin ang hiniram na kapital na kung saan lamang ay <span style="color:green;">{{number_format($keySummary[0]->DebtRatio, 2, '.', ',')}}%</span> sa kabuuang balanse ng kumpanya
            </li>
        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
            <li>
                Ang tumbasan ng pagkakautang ay may napakahusay na halaga <span style="color:green;">{{number_format($keySummary[0]->DebtRatio, 2, '.', ',')}}</span> sanhi ng mababang porsyento ng pagkakautang, ({{number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')}}% kabuuang kapital ng kumpanya)
            </li>
        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
            <li>
                Ang porsyento ng pagkakautang sa kabuuang balanse ng kumpanya ay <span style="color:green;">{{number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')}}</span>%kung saan ay normal para sa matatag na gawain.
            </li>
        @endif

        {{-- Working Capital and Inventories --}}
        @if($keySummary[0]->NWC > 0)
            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
                <li>
                    Ang porsyento ng pagkakautang sa kabuuang balanse ng kumpanya ay <span style="color:green;">{{number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')}}</span>%kung saan ay normal para sa matatag na gawain.

                </li>
            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))

                <li>
                    Kapital na paggawa ay may normal na halaga sa paghahambing sa imbentaryong pagmamay-ari ng kumpanya.
                </li>
            @endif
        @endif

        {{-- Current Ratio --}}
        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
            <li>
                Ang tumbasang pangkasalukuyan <span style="color:green;">{{number_format($keySummary[0]->CurrentRatio, 2, '.', ',')}}</span> ay mas mataas sa halagang pamantayan (2).
            </li>
        @elseif($keySummary[0]->CurrentRatio >= 2.1)
            <li>
                Ang tumbasang pangkasalukuyan <span style="color:green;">{{number_format($keySummary[0]->CurrentRatio, 2, '.', ',')}}</span> ganap na tumutugon sa pamantayan para sa ganitong tantyahan.
                {!! trans('financial_analysis/conclusion.the_current_ratio_criteria',['currentRatio'=>number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}
               
            </li>
        @endif

        {{-- Quick Ratio --}}
        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
            <li>
                Ang isang magandang ugnayan sa pagitan ng pag-aaring liquid (pangkasalukuyang pag-aari liban sa imbentaryo) at pangkasalukuyang pagkakautang <span style="color: green;">{{number_format($keySummary[0]->QuickRatio, 2, '.', ',')}}</span>.
            </li>
        @elseif($keySummary[0]->QuickRatio >= 1.1)
            <li>
                Isang natatanging ugnayan sa pagitan ng pag-aaring liquid (pangkasalukuyang pag-aari liban sa imbentaryo) at pangkasalukuyang pagkakautang (mabilis na tumbasan ay <span style="color: green;">{{number_format($keySummary[0]->QuickRatio, 2, '.', ',')}})
            </li>
        @endif

        {{-- Cash Ratio --}}
        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
            <li>
                Ang tumbasan ng salapi ay<span style="color: green;">{{number_format($keySummary[0]->CashRatio, 2, '.', ',')}}</span> sa wakas ng panahon (mataas na nakahandang salapi ay kailangan para sa pangkasalukuyang bayarin).
            </li>
        @elseif($keySummary[0]->CashRatio > 0.25)
            <li>
                Ang tumbasan ng salapi ay <span style="color: green;">{{number_format($keySummary[0]->CashRatio, 2, '.', ',')}}</span> sa wakas ng panahon (mataas na nakahandang salapi ay kailangan para sa pangkasalukuyang bayarin).
            </li>
        @endif

        {{-- Return of Equity --}}
        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
            <li>
                Balik sa karampatang halaga (ROE) ay <span style="color:green;">{{number_format($keySummary[0]->ROE*100,2,'.',',')}}</span>% kada taon para sa {{$keySummary[0]->Year}} kung saan ay  nagmula sa pagkakaroon ng mababang porsyento ng sariling kapital (karampatang halaga).
            </li>
        @elseif($keySummary[0]->ROE > 0.2)
            <li>
                Mataas na balik sa equity (<span style="color:green;">{{number_format($keySummary[0]->ROE*100,2,'.',',')}}</span>% kada taon), kung saan ay nagmula sa pagkakaroon ng mababang porsyento ng sariling kapital (karampatang halaga).
            </li>
        @endif

        {{-- ROA --}}
        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
            <li>
                Magandang balik sa mga pag-aari, kung saan ay <span style="color:green;">{{number_format($keySummary[0]->ROA*100,2,'.',',')}}</span>% sa taong {{$keySummary[0]->Year}}.
            </li>
        @elseif($keySummary[0]->ROA > 0.1)
            <li>
                Mahusay balik sa pag-aari, kung saan ay <span style="color:green;">{{number_format($keySummary[0]->ROA*100,2,'.',',')}}</span>% sa taong {{$keySummary[0]->Year}}.
            </li>
        @endif

        {{-- Net Worth --}}
        @if($financial_report)
            @if($balance_sheets[0]->IssuedCapital)
                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
                	@if($balance_sheets[0]->IssuedCapital > 0)
	                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
	                        <li>
                                Netong halaga (netong pag-aari) ng kumpanya ay higit na mataas (by <span style="color:green;">{{number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',',')}}/span> times) kumpara sa bahagi ng kapital sa 12/31/{{$keySummary[0]->Year}}.
	                        </li>
	                    @else
                            Ang netong halaga ay mas mataas kumpara sa bahagi ng kapital ng kumpanya.
	                    @endif
                    @endif
                @endif
            @endif
        @endif

        {{-- Equity --}}
        @if($financial_report)
            @if($balance_sheets)
                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
                        <li>
                            Halaga ng Karampatang halaga ay lumago sa panahong sinuri (mula ika-31 ng Disyembre, {{$balance_sheets[count($balance_sheets)-1]->Year}} hanggang ika-31 ng Disyembre, {{$balance_sheets[0]->Year}}) sa kabila ng pagbaba ng kabuuang pag-aari ng kumpanya.
                        </li>
                    @else
                        <li>
                            Ang paglago ng karampatang halaga para sa panahong sinuri(mula ika-31 ng Disyembre, {{$balance_sheets[count($balance_sheets)-1]->Year}} hanggang ika-31 ng Disyembre, {{$balance_sheets[0]->Year}}) ay hindi sumobra sa kabuuang paglago ng halaga ng pag-aari.
                        </li>
                    @endif
                @endif
            @endif
        @endif

        {{-- EBIT --}}
        @if($keySummary[0]->EBIT > 0)
            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
                @if($keySummary[0]->EBIT > 0)
                    <li>
                        Sa kabuuan ng panahong sinuri, kita bago interes at buwis (EBIT) ay nagpapakita ng PHP <span style="color:green;">{{number_format($keySummary[0]->EBIT,2,'.',',')}}</span>, sa parehas na panahon, isang positibong dinamika kumpara sa nakalipas na halaga.
                    </li>
                @else
                    <li>
                        Kita bago interes at buwis (EBIT) kung saan PHP <span style="color:green;">{{number_format($keySummary[0]->EBIT,2,'.',',')}}</span> sa nakalipas na taon, ngunit ito ay may negatibong pagkumpara sa nakalipas na halaga.
                    </li>
                @endif
            @endif
        @endif

        {{-- Comprehensive Income --}}
        @if($financial_report)
            @if($income_statements[0]->ComprehensiveIncome > 0)
                <li>
                    Ang kita mula sa pinansyal at operasyonal na mga gawain (komprehinsibong kita) ay PHP <span style="color:green;">{{number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')}}</span> sa  taon.
                </li>
            @endif
        @endif

    </ul>
    {{-- @endif --}}

    @php $thebad = 0; @endphp
	@if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
       @php $thebad++; @endphp
    @elseif($keySummary[0]->DebtRatio > 1)
        @php $thebad++; @endphp
    @endif

    {{-- Working Capital and Inventories --}}
    @if($keySummary[0]->NWC > 0)
        @if($keySummary[0]->InventoryNWC > 1.0)
            @php $thebad++; @endphp
        @endif
    @else
        @php $thebad++; @endphp
    @endif

    {{-- Current Ratio --}}
    @if($keySummary[0]->CurrentRatio < 1)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
        @php $thebad++; 0 @endphp
    @endif

    {{-- Quick Ratio --}}
    @if($keySummary[0]->QuickRatio < 0.5)
        @php $thebad++; 0 @endphp
    @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
        @php $thebad++; @endphp
    @endif

    {{-- Cash Ratio --}}
    @if($keySummary[0]->CashRatio < 0.05)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
        @php $thebad++; @endphp
    @endif

    {{-- Return of Equity --}}
    @if($keySummary[0]->ROE < 0)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
        @php $thebad++; @endphp
    @endif

    {{-- ROA --}}
    @if($keySummary[0]->ROA < 0)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
        @php $thebad++; @endphp
    @endif

    {{-- Net Worth --}}
    @if($financial_report)
        @if($balance_sheets[0]->IssuedCapital > 0)
            @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                @php $thebad++; @endphp
            @endif
        @endif
    @endif

    {{-- EBIT --}}
    @if($keySummary[0]->EBIT < 0)
        @php $thebad++; @endphp
    @endif

    {{-- comprehensive income --}}
    @if($financial_report)
        @if($income_statements[0]->ComprehensiveIncome < 0)
            @php $thebad++; @endphp
        @endif
    @endif

    {{-- @if(($keySummary->DebtRatio > 0.6) && ($keySummary->DebtRatio > 1)) --}}
    @if($thebad > 0)
    <p>{{ trans('financial_analysis/conclusion.the_analysis_revealed') }}</p>
    @endif
    <ul>
        {{-- Debt-to-equity Ratio --}}
        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
            <li>
            Ang tumbasan ng pagkakautang ay may hindi kanais-nais na halaga <span style="color:red;">:debtRatio</span> dahil sa mataas na porsyento ng pagkakautang, <span style="color:red;">:debtRatioPercentage</span>% ng kabuuang kapital ng kumpanya).
                {!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory',['debtRatio'=>number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage'=>number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->DebtRatio > 1)
            <li>
            Ang tumbasang pagkakautang ay may kritikal na halaga <span style="color:red;">:debtRatio</span>.
                {!! trans('financial_analysis/conclusion.the_debt_ratio_critical',['debtRatio'=>number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}

            </li>
        @endif

        {{-- Working Capital and Inventories --}}
        @if($keySummary[0]->NWC > 0)
            @if($keySummary[0]->InventoryNWC > 1.0)
                <li> Magagamit na kapital na nagpapalakad ay hindi sumasalo ng imbentaryo ng kumpansya sa kabuuan.</li>
            @endif
        @else
            <li>Hindi sapat na pangmatagalang pinagkukunan ng pampinansyal na gawain ng kumpanya (walang kapital na nagpapalakad)</li>
        @endif

        {{-- Current Ratio --}}
        @if($keySummary[0]->CurrentRatio < 1)
            <li>
                Ang tumbasan sa pangkasalukuyan <span style="color:red;"> {{number_format($keySummary[0]->CurrentRatio, 2, '.', ',')}}</span> ay higit na mababa sa pamantayang halaga (2).
            </li>
        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
            <li>
                Ang tumbasan sa pangkasalukuyan <span style="color:red;">{{number_format($keySummary[0]->CurrentRatio, 2, '.', ',')}}</span> ay hindi tumutugon sa normal na pamantayan sa ganitong halaga (2).
            </li>
        @endif

        {{-- Quick Ratio --}}
        @if($keySummary[0]->QuickRatio < 0.5)
            <li>
                Pag-aaring liquid (pag-aaring pangkasalukuyan liban ang mga imbentaryo) ay tiyak na hindi sapat upang matugunan ang pangkasalukuyang pagkakautang (tumbasang mabilis ay pantay sa <span style="color: red;">{{number_format($keySummary[0]->QuickRatio, 2, '.', ',')}}</span>, habang ang katanggap tanggap na halaga ay 1).

            </li>
        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
            <li>
                Pag-aaring liquid (pag-aaring pangkasalukuyan ibawas ang mga imbentaryo) ay tiyak na hindi sapat upang matugunan ang pangkasalukuyang pagkakautang (tumbasang mabilis ay pantay sa <span style="color: red;">{{number_format($keySummary[0]->QuickRatio, 2, '.', ',')}}</span>, habang ang katanggap tanggap na halaga ay 1).
            </li>
        @endif

        {{-- Cash Ratio --}}
        @if($keySummary[0]->CashRatio < 0.05)
            <li>
                Ang tumbasan ng salapi ay<span style="color: red;">{{number_format($keySummary[0]->CashRatio, 2, '.', ',')}}</span> sa wakas ng panahong sinuri (ang kakulangan sa nakahandang salaping kailangan para sa mga pangkasalukuyang bayarin).
            </li>
        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
            <li>
                Ang tumbasan ng salapi ay pantay sa <span style="color: red;">{{number_format($keySummary[0]->CashRatio, 2, '.', ',')}}</span> sa wakas ng panahong sinuri (ang mababang nakahandang salaping kailangan para sa mga pangkasalukuyang bayarin).
            </li>
        @endif

        {{-- Return of Equity --}}
        @if($keySummary[0]->ROE < 0)
            <li>
                Balik sa karampatang halaga (ROE) ay may kritikal na halaga <span style="color:red;">{{number_format($keySummary[0]->ROE,2,'.',',')}} </span>.
            </li>
        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
            <li>
            Balik sa karampatang halaga (ROE) ay <span style="color:red;">:roePercentage</span>% lamang kada taon sa nakalipas na taon
                {!! trans('financial_analysis/conclusion.return_on_equity_roe',['roePercentage'=>number_format($keySummary[0]->ROE*100,2,'.', ',')]) !!}
            </li>
        @endif

        {{-- ROA --}}
        @if($keySummary[0]->ROA < 0)
            <li>
                Kritikal na pagbalik sa pag-aari, sa {{$keySummary[0]->Year}}.
            </li>
        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
            <li>
                Hindi kanais-nais napagbalik sa pag-aari, kung saan  <span style="color:red;">{{number_format($keySummary[0]->ROA*100,1,'.', ',')}}</span>% sa panahong {{$keySummary[0]->Year}}.
            </li>
        @endif

        {{-- Net Worth --}}
        @if($financial_report)
            @if($balance_sheets[0]->IssuedCapital)
                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                    <li>
                        Netong halaga ay mas mababa sa bahagi ng kapital (ang pagkakaiba ay  PHP <span style="color:red;">{{number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',',')}}</span> sa huling araw ng pagsusuri (12/31/{{$keySummary[0]->Year}})).
                    </li>
                @endif
            @endif
        @endif

        {{-- EBIT --}}
        @if($keySummary[0]->EBIT < 0)
            <li>
                Sa kabuuan ng panahong sinuri, kita bago interes at buwis (EBIT) ay nagpakita ng PHP <span style="color:red;">{{$keySummary[0]->EBIT}}</span>
            </li>
        @endif

        {{-- comprehensive income --}}
        @if($financial_report)
            @if($income_statements[0]->ComprehensiveIncome < 0)
                <li>
                    Ang kita mula sa pinansyal at operasyonal na gawain (komprehensibong kita) ay PHP <span style="color:red;">{{number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')}}</span> sa loob ng taon.
                </li>
            @endif
        @endif


    </ul>
    {{-- @endif --}}
</div>
