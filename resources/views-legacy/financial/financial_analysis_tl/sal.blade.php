<h3>1.1 Istraktura ng Mga Asset at Pananagutan</h3>
<table cellpadding="5" cellspacing="0" class="table_width">
    <tbody>
        <tr>
            <td class="center" rowspan="3">Tagapagpahiwatig</td>
            <td class="center" colspan="{{ @count($balance_sheets) + 2 }}">Halaga</td>
            <td class="center" colspan="2">Pagbabago para sa panahon na pinag-aralan</td>
        </tr>
        <tr dontbreak="true">
            <td class="center" colspan="{{ @count($balance_sheets) }}">sa libong PHP</td>
            <td class="center" colspan="2">% ng kabuuan ng balanse</td>
            <td class="center" rowspan="2"><i>libong PHP</i> (col.{{ @count($balance_sheets) + 1}}-col.2)</td>
            <td class="center" rowspan="2">&plusmn; % ((col.{{ @count($balance_sheets) + 1}}-col.2) : col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">sa simula ng panahong pinag-aralan (12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td class="center">sa pagtatapos ng panahong pinag-aralan (12/31/{{ (int)$financial_report->year }})</td>
        </tr>
    
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Assets</b></td>
        </tr>

        <tr>
            <td>1. Mga hindi kasalukuyang assets</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets /
                   $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = round(($balance_sheets[0]->NoncurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->NoncurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend =$balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->NoncurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Mga kasalukuyang assets, total</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = round(($balance_sheets[0]->CurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->CurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Mga imbentaryo</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->Inventories > 0) ? number_format(round($balance_sheets[$x]->Inventories), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 

                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Inventories /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Inventories / $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php
                $vardata = $balance_sheets[0]->Inventories - $balance_sheets[@count($balance_sheets) - 1]->Inventories;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories > 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Inventories / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Kalakal at iba pang mga kasalukuyang natanggap</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->TradeAndOtherCurrentReceivables > 0) ? number_format(round($balance_sheets[$x]->TradeAndOtherCurrentReceivables), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                    $dividend = $balance_sheets[0]->Assets;
                        if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[0]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }

            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->TradeAndOtherCurrentReceivables - $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables > 0){
                    $vardata = (($balance_sheets[0]->TradeAndOtherCurrentReceivables / $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables) - 1) * 100;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Mga katumbas na cash at cash</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->CashAndCashEquivalents > 0) ? number_format(round($balance_sheets[$x]->CashAndCashEquivalents), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CashAndCashEquivalents /
            $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CashAndCashEquivalents - $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;

                if($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents > 0){

                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CashAndCashEquivalents / $dividend) - 1) * 100;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Karampatan at Pananagutan</b></td>
        </tr>
        <tr>
            <td>1. Karampatan</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->Equity), 0, '.', ',')}}</td>
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Equity /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Equity /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->Equity - $balance_sheets[@count($balance_sheets) - 1]->Equity;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Equity;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->Equity / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Mga hindi kasalukuyang pananagutan</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->NoncurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->NoncurrentLiabilities / $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities) - 1) * 100;
                    }
                }
                
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>3. Mga kasalukuyang pananagutan</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->CurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CurrentLiabilities / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>Mga Asset; Equity at Pananagutan</b></td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center"><b>{{number_format(round($balance_sheets[$x]->Assets), 0, '.', ',')}}</b></td>
            @endfor
            <td class="center">
                <b>100</b>
            </td>
            <td class="center">
                <b>100</b>
            </td>
            @php 
                $vardata = $balance_sheets[0]->Assets - $balance_sheets[@count($balance_sheets) - 1]->Assets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>{{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}</b>          
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Assets / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
                </b>
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
@php
    $b_assets = $balance_sheets[@count($balance_sheets) - 1]->Assets;
    $e_assets = $balance_sheets[0]->Assets;
    $assets_change = $e_assets - $b_assets;
    if($e_assets != '0.00' && $e_assets != 0) {
        $e_non_current_perc = ($balance_sheets[0]->NoncurrentAssets / $e_assets)*100;
        $e_current_perc = ($balance_sheets[0]->CurrentAssets / $e_assets)*100;
    } else {
        $e_non_current_perc = 0;
        $e_current_perc = 0;
    }
    $b_equity = $balance_sheets[@count($balance_sheets) - 1]->Equity;
    $e_equity = $balance_sheets[0]->Equity;
    $equity_change = $e_equity - $b_equity;
    if($b_equity != '0.00' && $b_equity != 0){
        $equity_change_perc = (($e_equity / $b_equity) - 1) * 100;
        $equity_change_times = $e_equity/$b_equity;
    }else{
        $equity_change_perc = 0;
        $equity_change_times = 0;
    }
    if($b_assets != '0.00' && $b_assets != 0) {
        $assets_change_times = $balance_sheets[0]->Assets/$b_assets;
        $assets_change_perc = ($assets_change/$b_assets)*100;
    }else{
        $assets_change_times = 0;
        $assets_change_perc = 0;
    }
    $b_trade = $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
    $e_trade = $balance_sheets[0]->TradeAndOtherCurrentReceivables;
    $trade_change = $e_trade - $b_trade;
    if($b_trade != '0.00' && $b_trade != 0) {
        $trade_change_perc = (($e_trade/$b_trade)-1)*100;
        $trade_change_times = $e_trade/$b_trade;
    }else{
        $trade_change_times = 0;
        $trade_change_perc = 0;
    }
    $b_inventories = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
    $e_inventories = $balance_sheets[0]->Inventories;
    $inventories_change = $e_inventories - $b_inventories;
    if($b_inventories != '0.00' && $b_inventories != 0) {
        $inventories_change_times = $e_inventories/$b_inventories;
    } else {
        $inventories_change_times = 0;
    }
@endphp

<p class="justify">
    @if($e_non_current_perc >= 24 && $e_non_current_perc <=26)
        Ayon sa talahanayan sa itaas, ang mga hindi kasalukuyang assets ng {{ $financial_report->title }} makes about a quarter (<span class="{{ $e_non_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_non_current_perc, 1) }}</span>%) sa pagtatapos ng panahon ng pagsusuri, habang ang kasalukuyang mga assets ay gumagawa ng tatlong kapat(<span class="{{ $e_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_current_perc, 1) }}</span>%), respectively.
    @elseif($e_non_current_perc >= 31 && $e_non_current_perc <=34)
        Ayon sa datos na ibinigay sa talahanayan, ang bahagi ng mga hindi kasalukuyang assets ay gumagawa ng halos isang-katlo (<span class="{{ $e_non_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_non_current_perc, 1) }}</span>%) sa pagtatapos ng panahon ng pagsusuri, habang ang kasalukuyang mga assets ay tumatagal ng dalawang katlo, ayon sa pagkakabanggit.
         Ang nasabing ugnayan ay pangkaraniwan para sa mga uri ng aktibidad kung saan ang malaking pamumuhunan ng mga nakapirming assets at pangmatagalang pamumuhunan ay hindi kinakailangan ngunit kung saan ang mga stock ng mga hilaw na materyales, supply at kalakal ay masagana.
    @elseif($e_non_current_perc >= 47 && $e_non_current_perc <=54)
        Noong Disyembre 31 {{ $financial_report->year }}, ang mga assets ng {{ $financial_report->title }} ay nagsama ng pantay na halaga ng kasalukuyan at hindi kasalukuyang mga assets {{ round($e_current_perc, 1) }}% at {{ round($e_non_current_perc, 1) }}%, ayon sa pagkakabanggit)
    @else
        {{ $financial_report->title }}'s ang mga assets ay inilarawan sa sumusunod na ugnayan sa huling araw ng panahon na sinuri: ang mga hindi kasalukuyang assets ay {{ round($e_non_current_perc, 1) }}%, habang ang kasalukuyang mga assets ay {{ round($e_current_perc, 1) }}%.
    @endif

    @if($assets_change > 0)
        Ang mga assets ay na-verify sa {{ $assets_change_perc > 20 ? 'taasan nang husto' : 'spike moderately' }} from PHP <span class="{{ $b_assets > 0 ? 'green' : 'red' }}">{{ number_format($b_assets) }}</span> to PHP <span class="{{ $e_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_assets) }}</span> (i.e. by <span class="{{ $assets_change_perc > 0 ? 'green' : 'red' }}">{{ $assets_change_perc <= 200 ? round($assets_change_perc, 1) : round($assets_change_times, 1)}}</span> {{ $assets_change_perc <= 200 ? "%" : "times" }}) para sa buong panahon na nasuri.
    @else
        Bumawas ang mga assets mula sa PHP {{ number_format($b_assets) }} to PHP {{ number_format($e_assets) }} (by PHP <span class="{{ $assets_change > 0 ? 'green' : 'red' }}">{{ number_format($assets_change) }}</span> , or by <span class="{{ $assets_change_perc > 0 ? 'green' : 'red' }}">{{ round($assets_change_perc, 1) }}</span>%) sa buong panahon ng pagsusuri.
    @endif

    @if($assets_change > 0 && $equity_change_perc > 0)
        @if($equity_change_perc < 5)
            Sa kabila ng paglago ng mga assets ng kumpanya, ang equity ng {{ $financial_report->title }} halos hindi nagbago (PHP <span class="{{ $equity_change > 0 ? 'green' : 'red' }}">{{ $equity_change > 0 ? '+' : ''}}{{ number_format($equity_change) }}</span>) and showed PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ number_format($e_equity) }}</span> 31 December, {{ (int)$financial_report->year }}.
        @else
            Ang mga assets ng kumpanya ay sabay na lumago nang may equity (<span class="{{ $equity_change_perc > 0 ? 'green' : 'red' }}">{{ $equity_change_perc > 0 ? '+' : ''}}{{ $equity_change_perc <= 200 ? round($equity_change_perc, 1) : round($equity_change_times, 1) }}</span> {{ $equity_change_perc <= 200 ? "%" : "times" }} para sa buong panahon na nasuri).
            Ang paglago ng halaga ng equity ay isang kadahilanan na positibong naglalarawan ng mga dinamika ng {{ $financial_report->title }} estado sa pananalapi.
        @endif
    @elseif($assets_change > 0 && $equity_change_perc < 0)
        Sa kabila ng paglago ng mga assets ng kumpanya, ang equity ng {{ $financial_report->title }} halos hindi nagbago (PHP <span class="{{ $equity_change > 0 ? 'green' : 'red' }}">{{ $equity_change > 0 ? '+' : ''}}{{ number_format($equity_change) }}</span>) and showed PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ number_format($e_equity) }}</span> 31 December, {{ (int)$financial_report->year }}.
    @elseif($assets_change < 0 && $equity_change_perc > 0)
        Lumaki ang karampatan ng <span class="{{ $equity_change_perc > 0 ? 'green' : 'red' }}">{{ $equity_change_perc > 0 ? '+' : ''}}{{ $equity_change_perc <= 200 ? round($equity_change_perc, 1) : round($equity_change_times, 1) }}</span> {{ $equity_change_perc <= 200 ? "%" : "times" }} at ipinakita PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ $e_equity > 0 ? '+' : ''}}{{ number_format($e_equity) }}</span> sa pagtatapos ng panahong pinag-aralan.
        Ang paglago ng equity ng kumpanya ay dapat isaalang-alang bilang isang positibong kadahilanan.
    @elseif($assets_change < 0 && $equity_change_perc < 0)
        Ang equity ng {{ $financial_report->title }} halos hindi nagbago (PHP <span class="{{ $equity_change > 0 ? 'green' : 'red' }}">{{ $equity_change > 0 ? '+' : ''}}{{ number_format($equity_change) }}</span>) at ipinakita ang PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ number_format($e_equity) }}</span> 31 Disyembre, {{ (int)$financial_report->year }}.
    @endif
</p>

    @php
        $negative_assets_list_count = 0;
        $positive_assets_list_count = 0;
        $assets_list = [
            'PropertyPlantAndEquipment' => 'Property, plant and equipment',
            'InvestmentProperty' => 'Investment property',
            'Goodwill' => 'Goodwill',
            'IntangibleAssetsOtherThanGoodwill' => 'Intangible assets other than goodwill',
            'InvestmentAccountedForUsingEquityMethod' => 'Investment accounted for using equity method',
            'InvestmentsInSubsidiariesJointVenturesAndAssociates' => 'Investments in subsidiaries, joint ventures and associates',
            'NoncurrentBiologicalAssets' => 'Non-current biological assets',
            'NoncurrentReceivables' => 'Trade and other non-current receivables',
            'NoncurrentInventories' => 'Non-current inventories',
            'DeferredTaxAssets' => 'Deferred tax assets',
            'CurrentTaxAssetsNoncurrent' => 'Current tax assets, non-current',
            'OtherNoncurrentFinancialAssets' => 'Other non-current financial assets',
            'OtherNoncurrentNonfinancialAssets' => 'Other non-current non-financial assets',
            'NoncurrentNoncashAssetsPledgedAsCollateral' => 'Non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'Inventories' => 'Current inventories',
            'TradeAndOtherCurrentReceivables' => 'Kalakal at ibang matatanggap sa kasalukuyan',
            'CurrentTaxAssetsCurrent' => 'Current tax assets, current',
            'CurrentBiologicalAssets' => 'Current biological assets',
            'OtherCurrentFinancialAssets' => 'Other current financial assets',
            'OtherCurrentNonfinancialAssets' => 'Other current non-financial assets',
            'CashAndCashEquivalents' => 'Pera at mga katumbas ng pera',
            'CurrentNoncashAssetsPledgedAsCollateral' => 'Current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'NoncurrentAssetsOrDisposalGroups' => 'Non-current assets or disposal groups classified as held for sale or as held for distribution to owners',
        ];
        $positives_assets_total = 0;
        $negatives_assets_total = 0;
        $positives_assets_list = array();
        $negatives_assets_list = array();
        $positives_assets_list_show = array();
        $negatives_assets_list_show = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_assets_list_count++;
                $positives_assets_total += $amount;
            } elseif($amount < 0) {
                $negatives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 1
                );
                $negative_assets_list_count++;
                $negatives_assets_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_assets_list, 'amount'), SORT_DESC, $positives_assets_list);
        array_multisort(array_column($negatives_assets_list, 'amount'), SORT_ASC, $negatives_assets_list);

        if($assets_change > 0) {
            foreach($positives_assets_list as $k=>$a) {
                $percentage = ($a['amount'] / $positives_assets_total) * 100;
                if($percentage > 5) {
                    $positives_assets_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        } else {
            foreach($negatives_assets_list as $k=>$a) {
                $percentage = (abs($a['amount']) / $negatives_assets_total) * 100;
                if($percentage > 5) {
                    $negatives_assets_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        }
    @endphp
    @if($assets_change > 0)
        @if(count($positives_assets_list_show) == 1)
            <p class="justify">
                Isang pagtaas sa kabuuang mga assets ng {{$financial_report->title}} ay naganap dahil sa paglaki ng item na "{{ $positives_assets_list_show[0]['item'] }}" ng PHP <span class="green"> {{ number_format(abs($positives_assets_list_show[0]['amount'])) }} </span>, iyon ay {{ round($positives_assets_list_show[0]['percentage'], 1) }}% ng lahat ng positibong binago mga assets.
            </p>
        @else
            <p class="justify">
                Ang dagdag sa kabuuang mga assets ng {{$financial_report->title}} naganap dahil sa paglaki ng mga sumusunod na uri ng asset (halaga ng pagbabago at porsyento ng pagbabagong ito na may kaugnayan sa kabuuang paglago ng mga assets ay ipinapakita sa ibaba):
            </p>
            <ul>
                @foreach($positives_assets_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="green">{{ number_format(abs($v['amount'])) }}</span> (<span class="green">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @else
        @if(count($negatives_assets_list_show) == 1)
            <p class="justify">
                Isang pagbawas sa kabuuang mga assets ng {{ $financial_report->title }} ay naganap dahil sa pagbaba ng item na "{{ $negatives_assets_list_show[0]['item'] }}" ng PHP <span class="red"> {{ number_format(abs($negatives_assets_list_show[0]['amount'])) }} </span>, iyon ay {{round ($negatives_assets_list_show[0]['percentage'], 1) }}% ng lahat ng binagong mga assets .
            </p>
        @else
            <p class="justify">
                Ang pagbawas sa kabuuang mga assets ng {{$financial_report->title}} ay naganap dahil sa pagbaba ng mga sumusunod na uri ng asset (halaga ng pagbabago at porsyento ng pagbabagong ito na may kaugnayan sa pagbawas ng kabuuang asset na ipinakita sa ibaba):
            </p>
            <ul>
                @foreach($negatives_assets_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="red">{{ number_format(abs($v['amount'])) }}</span> (<span class="red">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @endif
    
    @php
        $assets_list = [
            'IssuedCapital' => 'Inisyu (share) na kapital',
            'SharePremium' => 'Share premium',
            'TreasuryShares' => 'Pagbabahagi ng Pananalapi',
            'OtherEquityInterest' => 'Iba pang interes ng equity',
            'OtherReserves' => 'Iba pang mga reserba',
            'RetainedEarnings' => 'Nananatiling Kita',
            'NoncontrollingInterests' => 'Mga hindi nakokontrol na interes',
            'NoncurrentProvisionsForEmployeeBenefits' => 'Mga hindi kasaluuyang probisyon para sa mga benepisyo ng empleyado',
            'OtherLongtermProvisions' => 'Iba pang mga di-kasalukuyang probisyon',
            'NoncurrentPayables' => 'Kalakal at ibang matatanggap sa kasalukuyan',
            'DeferredTaxLiabilities' => 'Mga ipinagpaliban na pananagutan sa buwis',
            'CurrentTaxLiabilitiesNoncurrent' => 'Mga hindi kasalukuyang pananagutan sa buwis',
            'OtherNoncurrentFinancialLiabilities' => 'Iba pang pangmatagalang pananagutan sa pananalapi',
            'OtherNoncurrentNonfinancialLiabilities' => 'Other non-current non-financial liabilities',
            'CurrentProvisionsForEmployeeBenefits' => 'Mga kasalukuyang probisyon para sa mga benepisyo ng empleyado',
            'OtherShorttermProvisions' => 'Iba pang kasalukuyang mga probisyon',
            'TradeAndOtherCurrentPayables' => 'Kalakal at ibang matatanggap sa kasalukuyan',
            'CurrentTaxLiabilitiesCurrent' => 'Mga kasalukuyang pananagutan sa buwis, kasalukuyan',
            'OtherCurrentFinancialLiabilities' => 'Iba pang kasalukuyang pananagutan sa pananalapi',
            'OtherCurrentNonfinancialLiabilities' => 'Iba pang mga kasalukuyang pananagutang hindi pang-pinansyal',
            'LiabilitiesIncludedInDisposalGroups' => 'Ang mga pananagutan na kasama sa mga pangkat ng pagtatapon na inuri bilang ipinagbibili'
        ];
        $positive_finance_list_count = 0;
        $negative_finance_list_count = 0;
        $positives_finance_total = 0;
        $negatives_finance_total = 0;
        $positives_finance_list = array();
        $negatives_finance_list = array();
        $positives_finance_list_show = array();
        $negatives_finance_list_show = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_finance_list_count++;
                $positives_finance_total += $amount;
            } elseif($amount < 0) {
                $negatives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 2
                );
                $negative_finance_list_count++;
                $negatives_finance_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_finance_list, 'amount'), SORT_DESC, $positives_finance_list);
        array_multisort(array_column($negatives_finance_list, 'amount'), SORT_ASC, $negatives_finance_list);

        if($assets_change > 0) {
            foreach($positives_finance_list as $k=>$a) {
                $percentage = ($a['amount'] / $positives_finance_total) * 100;
                if($percentage > 5) {
                    $positives_finance_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        } else {
            foreach($negatives_finance_list as $k=>$a) {
                $percentage = (abs($a['amount']) / $negatives_finance_total) * 100;
                if($percentage > 5) {
                    $negatives_finance_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        }
    @endphp

    @if($assets_change > 0)
        @if(count($positives_finance_list_show) == 1)
            <p class="justify">    
                Ang isang paglago sa mga mapagkukunan ng pananalapi ay nakikita sa linyang "{{ $positives_finance_list_show[0]['item'] }}" ng PHP <span class="green"> {{ number_format(abs($positives_finance_list_show[0]['amount'])) }} </span>, iyon ay {{ round ($positives_finance_list_show[0]['percentage'], 1) }}% ng lahat ng positibong binago na mga mapagkukunan.
            </p>
        @else
            <p class="justify">
                Ang pinakamahalagang paglaki ng mga mapagkukunan ng pananalapi ("Equity and Liability") ay nakikita sa mga sumusunod na rate (ang porsyento mula sa kabuuang pagbabago ng equity at liability ay ipinapakita sa mga braket):
            </p>
            <ul>
                @foreach($positives_finance_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="green">{{ number_format(abs($v['amount'])) }}</span> (<span class="green">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @else
        @if(count($negatives_finance_list_show) == 1)
            <p class="justify">
                Ang isang pagbawas sa mga mapagkukunan ng pananalapi ay nakikita sa linya na "{{ $negatives_finance_list_show[0]['item'] }}" ng PHP <span class="red"> {{ number_format(abs($negatives_finance_list_show[0]['amount'])) }} </span>, iyon ay {{ round($negatives_finance_list_show[0]['percentage'], 1)}}% ng lahat ng binago na mapagkukunan.
            </p>
        @else
            <p class="justify">
                Ang pinakamahalagang pagbawas ng mga mapagkukunan ng pananalapi ("Equity and Liability") ay nakikita sa mga sumusunod na rate (ang porsyento mula sa kabuuang pagbabago ng equity at liability ay ipinapakita sa mga bracket):
            </p>
            <ul>
                @foreach($negatives_finance_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="red">{{ number_format(abs($v['amount'])) }}</span> (<span class="red">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @endif

<p class="justify">
    @if($assets_change > 0)
        @if($negative_assets_list_count > 0)
            Negatibong pagbabago sa {{ ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1) ? 'itmes' : 'item'}}
            @php
                $str = '';
                foreach($negatives_assets_list as $k=>$n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($negatives_finance_list) > 0) {
                    echo ' in assets and';
                    $str = '';
                    foreach($negatives_finance_list as $n) {
                        $str .= ' "'.$n['item'].'",';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' in finance';
                }
                echo ', which';
                echo ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1)? ' were' : ' was';
                $str = '';
                foreach($negatives_assets_list as $n) {
                    $str .= ' PHP <span class="red">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($negatives_finance_list) > 0) {
                    echo " and ";
                    $str = '';
                    foreach($negatives_finance_list as $n) {
                        $str .= ' PHP <span class="red">'.number_format($n['amount']).'</span>,';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' respectively,';
                }
            @endphp
             ay hindi pinapayagan ang kabuuang mga assets ng kumpanya na tumaas sa isang mas mataas na degree.
        @endif
    @else
        @if($positive_assets_list_count > 0)
            Ang positibong nagbago {{ ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'itmes' : 'item'}} sa sheet ng balanse sa buong panahon ng pagsusuri {{ ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'mga' : 'ay'}}
            @php
                $str = '';
                foreach($positives_assets_list as $n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($positives_finance_list) > 0) {
                    echo ' in assets and';
                    $str = '';
                    foreach($positives_finance_list as $n) {
                        $str .= ' "'.$n['item'].'",';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' in finance';
                }
                echo ', kung saan';
                echo ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1)? ' were' : ' ay';
                $str = '';
                foreach($positives_assets_list as $n) {
                    $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($positives_finance_list) > 0) {
                    echo " and ";
                    $str = '';
                    foreach($positives_finance_list as $n) {
                        $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' respectively.';
                }
            @endphp
        @endif
    @endif
</p>

<p class="justify">Sa tsart sa ibaba, makikita mo ang isang ugnayan ng mga pangunahing pangkat ng mga assets ng kumpanya.</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
</div>

@if($e_inventories > 0)
    <p class="justify">
        @if($inventories_change > 0)
            @if($inventories_change_times > 5)
                Sa pagtatapos ng panahon ng pagsusuri, ang mga imbentaryo na katumbas ng PHP ay katumbas ng PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
                Ang mga imbentaryo ay lumago ng <span class="{{ $inventories_change_times > 0 ? 'green' : 'red' }}">{{ round($inventories_change_times, 1) }}</span> beses sa buong panahon na pinag-aralan.
            @else
                Sa panahong pinag-aralan, ang mga imbentaryo ay umabot sa PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
                Para sa panahon na pinag-aralan, ang mga imbentaryo {{ $inventories_change > 50000 ? 'matulis na pagtaas' : 'katamtamang tumaas' }} sa PHP <span class="{{ $inventories_change > 0 ? 'green' : 'red' }}">{{ number_format($inventories_change) }}</span>.
            @endif
        @else
            Sa huling araw ng panahon na pinag-aralan, ang mga imbentaryo ay umabot sa PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
            Para sa panahong nasuri, ang mga imbentaryo ay bumaba ng PHP <span class="red">{{ number_format($inventories_change) }}</span>.
        @endif
    </p>
@endif

<p class="justify">
    @if($trade_change > 0)
        Ang kasalukuyang mga natanggap ay umakyat ng PHP <span class="{{$trade_change> 0 ? 'green': 'red' }}"> {{ number_format ($trade_change) }}</span>, o ng <span class="green">{{ $trade_change_perc <= 200 ? round($trade_change_perc, 1) : round($trade_change_times, 1) }}</span> {{$trade_change_perc <= 200 ? "%" : "times"}} sa buong panahon na sinuri.
    @else
        Para sa buong panahon na pinag-aralan, nakita na mayroong isang minarkahang pagbawas sa kasalukuyang mga matatanggap ng PHP <span class="red">{{ number_format($trade_change) }}</span>.
    @endif
</p>
