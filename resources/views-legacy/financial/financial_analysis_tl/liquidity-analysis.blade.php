<!-- Liquidity Analysis  -->
    <div class="text-center">
        <h3><strong>1.4. Pagsusuri sa Liquidity</strong></h3>
    </div>
    <p>Ang isa sa pinakalat na tagapagpahiwatig ng solvency ng isang kumpanya ay ang mga ratios na nauugnay sa pagkatubig. Mayroong tatlong mga ratio na nauugnay sa pagkatubig: kasalukuyang ratio, mabilis na ratio at ratio ng cash. Ang kasalukuyang ratio ay isa sa pinakalaganap at ipinapakita sa kung anong degree ang kasalukuyang mga assets ng kumpanya na nakakatugon sa mga kasalukuyang pananagutan. Ang solvency ng kumpanya sa malapit na hinaharap ay inilarawan sa mabilis na ratio na sumasalamin kung mayroong sapat na pondo para sa normal na pagpapatupad ng mga kasalukuyang transaksyon sa mga nagpapautang. Lahat ng tatlong mga ratio para sa {{ $financial_report->title ?? "N/A" }} ay kinakalkula sa sumusunod na talahanayan.</p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <td rowspan="2" class="center">Ratio ng pagkatubig</td>
            <td colspan="{{ count($balance_sheets) }}" class="center">Halaga</td>
            <td rowspan="2" class="center">Magbago (col.5 - col.2) </td>
            <td rowspan="2" class="center">Paglalarawan ng ratio at inirerekumenda na halaga</td>
        </tr>
        <tr>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">12/31/{{ $balance_sheets[$x]->Year}}</td>
            @endfor
        </tr>
        <tr>
            @for($indexCount = 1; $indexCount <= (count($balance_sheets)+3); $indexCount++)
                <td class="center"> {{ $indexCount }}</td>
            @endfor
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>1. Kasalukuyang ratio (working capital ratio)</td>
                <?php
                    $i=0;
                    $currentRatioPositiveValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                    $currentRatio = $currentAssets / $currentLiabilities;
                    $currentRatio = round($currentRatio, 2, PHP_ROUND_HALF_DOWN);

                    // First and Last value to find change value.
                    if ($i == 0) {
                        $firstCurrentRatio = $currentRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCurrentRatio = $currentRatio;
                        $currentRatioChangeValue = $lastCurrentRatio - $firstCurrentRatio;
                        $currentRatioChangeColor = 'green';
                        if($currentRatioChangeValue < 0)
                        {
                            $currentRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $currentRatioColor = 'red';
                    if($currentRatio >= 2)
                    {
                        array_push($currentRatioPositiveValues, $currentRatio);
                        $currentRatioColor = 'green';
                    }
                ?>
                <td class="center {{$currentRatioColor}}"> {{ $currentRatio }}</td>
                @endfor
                <td class="center {{ $currentRatioChangeColor }}">{{ $currentRatioChangeValue }} </td>
                <td>Ang kasalukuyang ratio ay kinakalkula sa pamamagitan ng paghahati ng kasalukuyang mga assets sa pamamagitan ng kasalukuyang mga pananagutan. Ipinapahiwatig nito ang kakayahan ng isang kumpanya na matugunan ang mga panandaliang obligasyon sa utang. Karaniwang halaga: 2 o higit pa.</td>
            </tr>
            <tr>
                <td>2. Mabilis na ratio (acid-test ratio)</td>
                <?php
                    $i=0;
                    $quickRatioPositiveValues=[];
                    $quickRatioValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Quick Ratio,  ∞ < crit. < 0.5 ≤ unsat. < 1 ≤ good < 1.1 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $otherCurrentFinancialAssets = $balance_sheets[$x]->OtherCurrentFinancialAssets;
                    $tradeAndOtherCurrentReceivables = $balance_sheets[$x]->TradeAndOtherCurrentReceivables;
                    $quickRatio = ($cashAndCashEquivalents + $otherCurrentFinancialAssets + $tradeAndOtherCurrentReceivables) / $currentLiabilities;
                    $quickRatio = round($quickRatio, 2, PHP_ROUND_HALF_DOWN);

                    if ($i == 0) {
                        $firstQuickRatio = $quickRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastQuickRatio = $quickRatio;
                        $quickRatioChangeValue = $lastQuickRatio - $firstQuickRatio;
                        $quickRatioChangeColor = 'green';
                        if($quickRatioChangeValue < 0)
                        {
                            $quickRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $quickRatioColor = 'red';
                    if($quickRatio >= 1)
                    {
                        $quickRatioColor = 'green';
                        array_push($quickRatioPositiveValues, $quickRatio);
                    }
                    array_push($quickRatioValues, $quickRatio);
                ?>
                <td class="center {{ $quickRatioColor }}"> {{ $quickRatio }}</td>
                @endfor
                <td class="center {{ $quickRatioChangeColor }}">{{ $quickRatioChangeValue }} </td>
                <td>Ang mabilis na ratio ay kinakalkula sa pamamagitan ng paghahati ng mga likidong assets (cash at cash na katumbas, kalakal at iba pang mga kasalukuyang natanggap, iba pang kasalukuyang mga financial assets) ng kasalukuyang mga pananagutan. Ito ay isang sukat ng kakayahan ng isang kumpanya na matugunan ang mga panandaliang obligasyon nito gamit ang pinaka-likidong mga assets (malapit sa cash o mabilis na mga assets). Katanggap-tanggap na halaga: 1 o higit pa.</td>
            </tr>
            <tr>
                <td>3. Cash ratio (acid-test ratio)</td>
                <?php
                    $i=0;
                    $cashRatioPositiveValues = [];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Cash Ratio, ∞ < crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $cashRatio = $cashAndCashEquivalents / $currentLiabilities;
                    $cashRatio = round($cashRatio, 2, PHP_ROUND_HALF_DOWN);

                    if ($i == 0) {
                        $firstCashRatio = $cashRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCashRatio = $cashRatio;
                        $cashRatioChangeValue = $lastCashRatio - $firstCashRatio;
                        $cashRatioChangeColor = 'green';
                        if($cashRatioChangeValue < 0)
                        {
                            $cashRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $cashRatioColor = 'red';
                    if($cashRatio >= 0.2)
                    {
                        $cashRatioColor = 'green';
                        array_push($cashRatioPositiveValues, $cashRatio);
                    }
                ?>
                <td class="center {{ $cashRatioColor }}"> {{ $cashRatio }}</td>
                @endfor
                <td class="center {{ $cashRatioChangeColor }}">{{ $cashRatioChangeValue }} </td>
                <td>Ang ratio ng cash ay kinakalkula sa pamamagitan ng paghahati ng ganap na likidong mga assets (cash at cash na katumbas) ng kasalukuyang mga pananagutan. Karaniwang halaga: 0.2 o higit pa.</td>
            </tr>
        </tbody>
    </table>
    <?php
        $currentRatioText = '';
        if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue<0) )
        {
            $currentRatioText = 'Sa buong panahon ng pagsusuri, ang kasalukuyang ratio ay bumaba nang malaki (ng <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>), bilang karagdagan, ang isang katulad na pagkahilig sa panahon ay pinatunayan ng linear na kalakaran. On 12/31/'.(int)$financial_report->year.', ang ratio ay may isang hindi tipikal na halaga. Ang kasalukuyang ratio ay nag-iingat ng isang hindi katanggap-tanggap na halaga sa kabuuan ng nasuri na panahon.';
        }
        else if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue>0) )
        {
            $currentRatioText = 'Sa panahon ng pag-aralan ng panahon, napatunayan na mayroong isang kasiya-siyang pagtaas sa kasalukuyang ratio ng <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>. Sa pagtatapos ng panahon, ang halaga ng ratio ay hindi katanggap-tanggap at nakasalalay sa lugar ng mga kritikal na halaga. Ang kasalukuyang ratio ay hindi nag-iingat ng katanggap-tanggap na halaga nito sa buong panahon.';
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue>0) )
        {
            $currentRatioText = 'Ang halaga ng ratio ay maaaring mailalarawan bilang unang rate sa huling araw ng panahong sinuri. Sa buong panahon, ang kasalukuyang ratio ay tumutugma sa itinakdang pamantayan.';
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue<0) )
        {
            $currentRatioText = 'Sa huling araw ng panahon na sinuri, ang halaga ng ratio ay maaaring mailalarawan bilang unang-rate. Ang kasalukuyang ratio ay nag-iingat ng katanggap-tanggap na halaga sa kabuuan ng pinag-aralan na panahon.';
        }
        else if( ($firstCurrentRatio >= 2) && ($lastCurrentRatio < 2) && ($currentRatioChangeValue > 0)  )
        {
            $currentRatioText = 'Sa panahon ng pagsusuri, ang paglaki ng kasalukuyang ratio ay <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span>. Sa kabila ng katotohanang sa simula ng pinag-aralan na panahon at ang halaga ng kasalukuyang ratio ay tumutugma sa pamantayan, karagdagang naging hindi katanggap-tanggap.';
        }
        else if( ($firstCurrentRatio < 2) && ($lastCurrentRatio >= 2) && ($currentRatioChangeValue > 0)  )
        {
            $currentRatioText = 'Ang paglaki sa kasalukuyang ratio ay <span class="'.$currentRatioChangeColor.'">'.$currentRatioChangeValue.'</span> para sa buong panahon na nasuri. Sa huling araw ng panahong pinag-aralan (12/31/'.(int)$financial_report->year.'), ang ratio ay nagpapakita ng isang mahusay na halaga. Sa kabila ng katotohanang sa simula ng isinasaalang-alang na panahon, ang halaga ng kasalukuyang ratio ay hindi tumutugma sa pamantayan, kalaunan ay naging katanggap-tanggap.';
        }
        else
        {
            if($lastCurrentRatio >= 2)
            {
                $currentRatioText = 'Sa nasuri na panahon, ang mga multidirectional na pagbabago sa ratio (parehong pagtaas at pagbawas) ay sinusunod, kalaunan ay naging katanggap-tanggap ito.';
            }
            else
            {
                $currentRatioText = 'Sa nasuri na panahon, ang mga multidirectional na pagbabago sa ratio (parehong pagtaas at pagbawas) ay sinusunod, kalaunan ay hindi ito katanggap-tanggap.';
            }
        }
    ?>
    <p>On 12/31/{{ (int)$financial_report->year }}, the current ratio was equal to <span class="{{ $currentRatioColor }}">{{ $lastCurrentRatio }}</span>. {!! $currentRatioText !!}</p>

    <?php
        // Quick Ratio Text
        $quickRatioText = '';
        if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue<0) )
        {
            $quickRatioText = 'Sa buong panahon ng pagsusuri, nakita na mayroong isang minarkahang pagbaba sa mabilis na ratio, na ipinakita <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>, bukod dito, ang isang katulad na pagkahilig sa panahon ay nakumpirma ng linear trend. Sa nasuri na panahon, ang parehong paglago at pagbagsak ng ratio ay na-verify; ang maximum na halaga ay '.max($quickRatioValues).', the minimum one was '.min($quickRatioValues).'. Ang mabilis na ratio ay may isang hindi kasiya-siyang halaga sa pagtatapos ng panahon na sinuri. Ibig sabihin nito ay '.$financial_report->title.' ay walang sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
        }
        else if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue>0) )
        {
            $quickRatioText = 'subalit sa simula ng isinasaalang-alang na panahon, ang mabilis na ratio ay mas mababa '.$firstQuickRatio.' (i.e. ang paglaki ay katumbas ng <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>). Sa simula ng nasuri na panahon, lumaki ang ratio, ngunit kalaunan ay binago ang takbo sa isang pagbaligtad. Sa 12/31/'.(int)$financial_report->year.', ang halaga ng mabilis na ratio ay maaaring inilarawan bilang hindi kasiya-siya. Ibig sabihin nito, '.$financial_report->title.' ay mayroong masyadong maraming kasalukuyang mga pananagutan o walang sapat na mga likidong assets upang masiyahan ang mga nabanggit na pananagutan.';
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue>0) )
        {
            $quickRatioText = 'Sa panahon ng nasuri na panahon, nagbago ang ratio ng multidirectionally; ang maximum na halaga ay '.max($quickRatioValues).', ang pinakamaliit ay '.min($quickRatioValues).'. Ang halaga ng mabilis na ratio ay maaaring inilarawan bilang ganap na normal sa 12/31/'.(int)$financial_report->year.'. It means that '.$financial_report->title.' ay may sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue<0) )
        {
            $quickRatioText = 'Sa buong panahon ng pagsusuri, ang mabilis na ratio ay mabilis na nabawasan (<span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>). Sa kabila ng tinatayang paglaki ng rate sa panahong ito, ang pagbawas nito ay sinundan pa. Ang halaga ng mabilis na ratio ay maaaring inilarawan bilang napakahusay sa 12/31/'.(int)$financial_report->year.'. Ibig sabihin nito ay '.$financial_report->title.' ay may sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
        }
        else if( ($firstQuickRatio >= 1) && ($lastQuickRatio < 1) && ($quickRatioChangeValue > 0)  )
        {
            $quickRatioText = 'Para sa buong panahon na nasuri, napatunayan na mayroong isang mabilis na paglago sa mabilis na ratio ng <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>. Sa kabuuan ng pinag-aralan na panahon, isang paglago ng ratio ang nabago sa pagbawas. Ang mabilis na ratio ay may isang hindi kasiya-siyang halaga sa pagtatapos ng panahon na sinuri. Ibig sabihin nito ay '.$financial_report->title.' ay walang sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
        }
        else if( ($firstQuickRatio < 1) && ($lastQuickRatio >= 1) && ($quickRatioChangeValue > 0)  )
        {
            $quickRatioText = 'Para sa buong panahon na nasuri, napatunayan na mayroong isang mabilis na paglago sa mabilis na ratio ng <span class="'.$quickRatioChangeColor.'">'.$quickRatioChangeValue.'</span>. Sa kabuuan ng pinag-aralan na panahon, isang pagbawas ng ratio ang nabago sa paglago. Ang halaga ng mabilis na ratio ay maaaring inilarawan bilang ganap na normal sa huling araw ng panahon na sinuri. Ibig sabihin nito ay '.$financial_report->title.' ay may sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
        }
        else
        {
            if($lastQuickRatio >= 1)
            {
                $quickRatioText = 'On 12/31/'.(int)$financial_report->year.', ang halaga ng mabilis na ratio ay maaaring inilarawan bilang normal. Nangangahulugan ito na, '.$financial_report->title.' ay may sapat na likidong assets (cash at iba pang mga assets na maaaring mabilis na maibenta) upang matugunan ang lahat ng kanilang kasalukuyang pananagutan.';
            }
            else
            {
                $quickRatioText = 'On 12/31/'.(int)$financial_report->year.', ang halaga ng mabilis na ratio ay maaaring inilarawan bilang hindi kasiya-siya. Nangangahulugan ito na, '.$financial_report->title.' ay walang sapat na likidong assets upang masiyahan ang mga nabanggit na pananagutan.';
            }
        }
    ?>
    <p>On 12/31/{{(int)$financial_report->year}}., the quick ratio was equal to <span class="{{ $quickRatioColor }}">{{ $lastQuickRatio }}</span>.{!! $quickRatioText !!}</p>

    <?php
        // cash Ratio Text
        $cashRatioText = '';
        if( (count($cashRatioPositiveValues)<=0) && ($cashRatioChangeValue<0) )
        {
            $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio ay may isang hindi kasiya-siyang halaga (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) sa huling araw ng panahon na pinag-aralan na naglalarawan sa kakulangan ng pinaka likidong mga assets sa kumpanya (cash at cash na katumbas) upang matugunan ang lahat ng kasalukuyang pananagutan.';
        }
        else if( (count($cashRatioPositiveValues)<=0) && ($cashRatioChangeValue>0) )
        {
            $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio ay may isang hindi kasiya-siyang halaga (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) sa huling araw ng panahon na pinag-aralan na naglalarawan sa kakulangan ng pinaka likidong mga assets sa kumpanya (cash at cash na katumbas) upang matugunan ang lahat ng kasalukuyang pananagutan.';
        }
        else if( (count($cashRatioPositiveValues)===count($balance_sheets)) && ($cashRatioChangeValue>0) )
        {
            $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio, nakasalalay sa normal na saklaw na 12/31/'.$financial_report->year.'. '.$financial_report->title.' ay sinusunod na magkaroon ng sapat na cash at katumbas na salapi upang matugunan ang mga kasalukuyang pananagutan.';
        }
        else if( (count($cashRatioPositiveValues)===count($balance_sheets)) && ($cashRatioChangeValue<0) )
        {
            $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio, nakasalalay sa katanggap-tanggap na saklaw sa 12/31/'.$financial_report->year.'. '.$financial_report->title.' ay sinusunod na magkaroon ng sapat na cash at katumbas na salapi upang matugunan ang mga kasalukuyang pananagutan.';
        }
        else if( ($firstCashRatio >= 0.2) && ($lastCashRatio < 0.2) && ($cashRatioChangeValue > 0)  )
        {
            $cashRatioText = 'Ang halaga ng cash ay nagkakahalaga ng <span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>. sa huling araw ng panahong pinag-aralan (12/31/'.$financial_report->year.'). Sa panahon ng buong panahong sinuri, ang pagtaas ng ratio ng cash ay <span class="'.$cashRatioChangeColor.'">'.$cashRatioChangeValue.'</span>., sa kabila nito ang linear trend ay nagpapahiwatig na sa average na ang cash ratio ay bumababa sa panahon.';
        }
        else if( ($firstCashRatio < 0.2) && ($lastCashRatio >= 0.2) && ($cashRatioChangeValue > 0)  )
        {
            $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio, nakasalalay sa normal na saklaw sa pagtatapos ng panahong sinuri. '.$financial_report->title.' ay sinusunod na magkaroon ng sapat na cash at katumbas na salapi upang matugunan ang mga kasalukuyang pananagutan.';
        }
        else
        {
            if($lastCashRatio >= 0.2)
            {
                $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio, nakasalalay sa normal na saklaw sa pagtatapos ng panahong sinuri. '.$financial_report->title.' ay sinusunod na magkaroon ng sapat na cash at katumbas na salapi upang matugunan ang mga kasalukuyang pananagutan.';
            }
            else
            {
                $cashRatioText = 'Ang halaga ng pangatlong ratio, ang cash ratio ay may isang hindi kasiya-siyang halaga (<span class="'.$cashRatioColor.'">'.$lastCashRatio.'</span>) sa huling araw ng panahon na pinag-aralan na naglalarawan sa kakulangan ng pinaka likidong mga assets sa kumpanya (cash at cash na katumbas) upang matugunan ang lahat ng kasalukuyang pananagutan.';
            }
        }
    ?>

    <p>{!! $cashRatioText !!}</p>
    <!-- Liquidity Graph -->
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_4_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>
<!-- End: Liquidity Analysis  -->