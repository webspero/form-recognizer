<div class="toc">
	<ul>
		<li>1. {{ $financial_report->title }} Pagsusuri sa Posisyon sa Pinansyal
			<ul>
				<li>1.1. Istraktura ng Mga Asset at Pananagutan</li>
				<li>1.2. Mga Net Asset (Net Worth)</li>
				<li>1.3. Pagsusuri sa Pagpapanatili ng Pananalapi
					<ul>
						<li>1.3.1. Key ratios ng pagpapanatili ng pananalapi ng kumpanya</li>
						<li>1.3.2. Paggawa ng pagsusuri sa kapital</li>
					</ul>
				<li>1.4. Pagsusuri sa Liquidity</li>
			</ul>
		</li>
		<li>2. Pagganap sa Pinansyal
			<ul>
				<li>2.1. Pangkalahatang-ideya ng Mga Resulta sa Pinansyal</li>
				<li>2.2. Mga Ratios na Kakayahang Makita</li>
				<li>2.3. Pagsusuri ng Aktibidad sa Negosyo (Mga Ratio sa Pagbabago)</li>
			</ul>
		</li>
		<li>3. Konklusyon
			<ul>
				<li>3.1. Buod ng Mga Ratios ng Key</li>
				<li>3.2. Rating ng Posisyong Pinansyal at Pagganap ng Pananalapi ng {{ $financial_report->title }}</li>
			</ul>
		</li>
	</ul>

</div>