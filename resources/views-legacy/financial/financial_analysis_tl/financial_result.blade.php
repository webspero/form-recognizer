<h3>2.1 Pangkalahatang-ideya ng Mga Resulta sa Pinansyal</h3>

<table cellpadding="5" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Tagapagpahiwatig</td>
            <td class="center" colspan="{{ @count($balance_sheets)-1 }}">Halaga, libong PHP</td>
            <td class="center" colspan="2">Pagbabago</td>
            <td class="center" rowspan="2">Average na taunang halaga, libong PHP</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets)-1; $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">libong PHP (col.4 - col.2)</td>
            <td class="center">&plusmn; % (4-2) : 2</td>
        </tr>
        <tr>
            <td width="30%">1. Kita</td>
            @php
                $max_count = @count($balance_sheets)-2;
                for($x=$max_count; $x >= 0 ; $x--){
                    
                        $Revenue = $income_statements[$x]->Revenue;
                        $class = ($Revenue >= 2) ? 'green' : 'red';
                
            @endphp
                    <td class="center {{ $class }}">
                        @if($Revenue != 0)
                            {{ number_format($Revenue, 0, '.', ',') }}
                        @else
                            -
                        @endif
                    </td>
                
            @php
                    if($x==0) $textcontent1 = number_format($Revenue, 0, '.', ','); 

                }
                $Change = $income_statements[0]->Revenue -
                            $income_statements[$max_count]->Revenue;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 0, '.', ',');
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php
                if($income_statements[$max_count]->Revenue != 0){
                    $PercentageChange = 0;
                    $dividend = $income_statements[$max_count]->Revenue;
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = (($income_statements[0]->Revenue -
                            $income_statements[$max_count]->Revenue) / 
                            $dividend) * 100;
                    }
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->Revenue;
                }

                $Average = 0;
                $dividend = $max_count+1;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>2. Gastos ng benta</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $CostOfSales = $income_statements[$x]->CostOfSales;
                    $class = ($CostOfSales >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($CostOfSales != 0)
                        {{ number_format($CostOfSales, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = $income_statements[0]->CostOfSales -
                            $income_statements[$max_count]->CostOfSales;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if($income_statements[$max_count]->CostOfSales != 0) {
                    $PercentageChange = 0;
                    $dividend = $income_statements[$max_count]->CostOfSales;
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = (($income_statements[0]->CostOfSales -
                            $income_statements[$max_count]->CostOfSales) / 
                            $dividend) * 100;
                    }
                    
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->CostOfSales;
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }

                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>3.<i>Kabuuang kita</i> (1-2)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $GrossProfit = $income_statements[$x]->GrossProfit;
                    $class = ($GrossProfit >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($GrossProfit != 0)
                        {{ number_format($GrossProfit, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($GrossProfit, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->GrossProfit) -
                            ($income_statements[$max_count]->GrossProfit);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent4 = number_format($Change, 0, '.', ',');
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if($income_statements[$max_count]->GrossProfit != 0) {
                    $PercentageChange = 0;                    
                    $dividend = ($income_statements[$max_count]->GrossProfit);

                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->GrossProfit) -
                            ($income_statements[$max_count]->GrossProfit)) / 
                            $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
                $textcontent5 = number_format($PercentageChange, 1, '.', ',');
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->GrossProfit);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>4. Iba pang kita at gastos, maliban sa gastos sa Pananalapi</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $OIEeFC = $income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($OIEeFC >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($OIEeFC != 0)
                        {{ number_format($OIEeFC, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts);
                    
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center" width="10%">
                @if($PercentageChange != 0)
                    up
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts);
                }
                
                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>5. <i>EBIT</i> (3+4)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $EBIT = $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($EBIT >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($EBIT != 0)
                        {{ number_format($EBIT, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent6 = number_format($EBIT, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts)) / 
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>6. Mga gastos sa pananalapi</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $FinanceCosts = $income_statements[$x]->FinanceCosts;
                    $class = ($FinanceCosts >= 2) ? 'green' : 'red';
                @endphp
                <td class="center red">
                    @if($FinanceCosts != 0)
                        {{ number_format($FinanceCosts, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->FinanceCosts) != 0) {
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->FinanceCosts);
                    
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->FinanceCosts)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->FinanceCosts);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center red" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>7. Gastos sa buwis sa kita (mula sa patuloy na pagpapatakbo)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $IncomeTaxExpenseContinuingOperations = $income_statements[$x]->IncomeTaxExpenseContinuingOperations;
                    $class = ($IncomeTaxExpenseContinuingOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center red">
                    @if($IncomeTaxExpenseContinuingOperations != 0)
                        {{ number_format($IncomeTaxExpenseContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                            ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations) != 0) {

                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                                ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations)) / 
                                $dividend) * 100;
                    }
                    

                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->IncomeTaxExpenseContinuingOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center red" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>8. Kita (pagkawala) mula sa patuloy na pagpapatakbo (5-6-7)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLossFromContinuingOperations = $income_statements[$x]->ProfitLossFromContinuingOperations;
                    $class = ($ProfitLossFromContinuingOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ProfitLossFromContinuingOperations != 0)
                        {{ number_format($ProfitLossFromContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossFromContinuingOperations) -
                            ($income_statements[$max_count]->ProfitLossFromContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossFromContinuingOperations) != 0) {

                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossFromContinuingOperations);

                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ProfitLossFromContinuingOperations) -
                                ($income_statements[$max_count]->ProfitLossFromContinuingOperations)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromContinuingOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>9. Kita (pagkawala) mula sa hindi na ipinagpatuloy na pagpapatakbo</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLossFromDiscontinuedOperations = $income_statements[$x]->ProfitLossFromDiscontinuedOperations;
                    $class = ($ProfitLossFromDiscontinuedOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ProfitLossFromDiscontinuedOperations != 0)
                        {{ number_format($ProfitLossFromDiscontinuedOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php
                $Change = ($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                            ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations);

                    if($dividend != 0 && $dividend != '0.00'){    
                        $PercentageChange = ((($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                                ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromDiscontinuedOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($Average != 0 && $Average != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>10. Kita (pagkawala) (8 + 9)</b></td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLoss = $income_statements[$x]->ProfitLoss;
                    $class = ($ProfitLoss >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    <b>
                    @if($ProfitLoss != 0)
                        {{ number_format($ProfitLoss, 0, '.', ',') }}
                    @else
                        -
                    @endif
                    </b>
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLoss) -
                            ($income_statements[$max_count]->ProfitLoss);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                <b>
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
                </b>
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLoss) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLoss);    
                    
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ProfitLoss) -
                                ($income_statements[$max_count]->ProfitLoss)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                <b>
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
                </b>
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLoss);
                }
                $Average = $Sum / ($max_count+1);;
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                <b>
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
                </b>
            </td>
        </tr>
        
        <tr>
            <td>11. Iba pang komprehensibong kita</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $OtherComprehensiveIncome = $income_statements[$x]->OtherComprehensiveIncome;
                    $class = ($OtherComprehensiveIncome >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($OtherComprehensiveIncome != 0)
                        {{ number_format($OtherComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->OtherComprehensiveIncome) -
                            ($income_statements[$max_count]->OtherComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->OtherComprehensiveIncome) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->OtherComprehensiveIncome);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->OtherComprehensiveIncome) -
                                ($income_statements[$max_count]->OtherComprehensiveIncome)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->OtherComprehensiveIncome);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>12. Komprehensibong kita (10 + 11) </td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ComprehensiveIncome = $income_statements[$x]->ComprehensiveIncome;
                    $class = ($ComprehensiveIncome >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ComprehensiveIncome != 0)
                        {{ number_format($ComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent7 = number_format($ComprehensiveIncome, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->ComprehensiveIncome) -
                            ($income_statements[$max_count]->ComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ComprehensiveIncome) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ComprehensiveIncome);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ComprehensiveIncome) -
                                ($income_statements[$max_count]->ComprehensiveIncome)) / $dividend) * 100;
                    }

                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ComprehensiveIncome);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>

<div class="center">
    <p>Katumbas ang kita <span style="color:{{($income_statements[0]->Revenue >= 0) ? "green" : "red"}}" > {{number_format($income_statements[0]->Revenue, 0, '.', ',')}} </span> para sa huling taon,
         habang ang kita ay makabuluhang mas mababa at katumbas ng PHP <span style="color:{{($income_statements[1]->Revenue >= 0) ? "green" : "red"}}">{{number_format($income_statements[1]->Revenue, 0, '.', ',')}}</span>
        para sa taon {{number_format($income_statements[1]->Year, 0, '.', ',')}} (i.e. lumaki ito ng PHP 
        <span style="color:{{(($income_statements[0]->Revenue - $income_statements[1]->Revenue) >= 0) ? "green" : "red"}}" > {{number_format(($income_statements[0]->Revenue - $income_statements[1]->Revenue), 0, '.', ',')}} </span>). 
        Ang pagbabago sa kita ay ipinakita sa tsart. Ang kabuuang kita ay katumbas ng PHP <span style="color:{{($income_statements[0]->GrossProfit >= 0) ? "green" : "red"}}" > {{number_format($income_statements[0]->GrossProfit, 0, '.', ',')}} </span> 
        sa huling taon. Para sa huling taon sa paghahambing sa parehong panahon ng nakaraang taon ng pananalapi, napansin na mayroong isang natitirang pagtaas sa kita ng kita (PHP
        <span style="color:{{(($income_statements[0]->GrossProfit - $income_statements[1]->GrossProfit) >= 0) ? "green" : "red"}}" > {{number_format(($income_statements[0]->GrossProfit - $income_statements[1]->GrossProfit), 0, '.', ',')}}  </span>).
    </p>
    <p>
        Para sa huling taon, ang kumpanya ay nag-sulat ng isang kabuuang kita at mga kita bago ang interes at buwis (EBIT), na kung saan ay 
        <span style="color:{{($income_statements[0]->ProfitLossFromOperatingActivities >= 0) ? "green" : "red"}}" > {{number_format($income_statements[0]->ProfitLossFromOperatingActivities, 0, '.', ',')}} </span>. The comprehensive income of {{$financial_report->title}} 
        dating PHP <span style="color:{{($income_statements[0]->ComprehensiveIncome >= 0) ? "green" : "red"}}" > {{number_format($income_statements[0]->ComprehensiveIncome, 0, '.', ',')}} </span> 
        sa kabuuan para sa panahon mula 01/01/{{$income_statements[0]->Year}} hanggang 12/31/{{$income_statements[0]->Year}}.
    </p>
</div>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_1_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>