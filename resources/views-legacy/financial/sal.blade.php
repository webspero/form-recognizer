<h3>1.1 Structure of the Assets and Liabilities</h3>
<table cellpadding="5" cellspacing="0" class="table_width">
    <tbody>
        <tr>
            <td class="center" rowspan="3">Indicator</td>
            <td class="center" colspan="{{ @count($balance_sheets) + 2 }}">Value</td>
            <td class="center" colspan="2">Change for the period analyzed</td>
        </tr>
        <tr dontbreak="true">
            <td class="center" colspan="{{ @count($balance_sheets) }}">in thousand PHP</td>
            <td class="center" colspan="2">% of the balance total</td>
            <td class="center" rowspan="2"><i>thousand PHP</i> (col.{{ @count($balance_sheets) + 1}}-col.2)</td>
            <td class="center" rowspan="2">&plusmn; % ((col.{{ @count($balance_sheets) + 1}}-col.2) : col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">at the beginning of the period analyzed (12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td class="center">at the end of the period analyzed (12/31/{{ (int)$financial_report->year }})</td>
        </tr>
    
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Assets</b></td>
        </tr>

        <tr>
            <td>1. Non-current assets</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets /
                   $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = round(($balance_sheets[0]->NoncurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->NoncurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend =$balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->NoncurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Current assets, total</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = round(($balance_sheets[0]->CurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->CurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Inventories</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->Inventories > 0) ? number_format(round($balance_sheets[$x]->Inventories), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 

                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Inventories /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Inventories / $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php
                $vardata = $balance_sheets[0]->Inventories - $balance_sheets[@count($balance_sheets) - 1]->Inventories;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories > 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Inventories / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Trade and other current receivables</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->TradeAndOtherCurrentReceivables > 0) ? number_format(round($balance_sheets[$x]->TradeAndOtherCurrentReceivables), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                    $dividend = $balance_sheets[0]->Assets;
                        if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[0]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }

            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->TradeAndOtherCurrentReceivables - $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables > 0){
                    $vardata = (($balance_sheets[0]->TradeAndOtherCurrentReceivables / $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables) - 1) * 100;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Cash and cash equivalents</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->CashAndCashEquivalents > 0) ? number_format(round($balance_sheets[$x]->CashAndCashEquivalents), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CashAndCashEquivalents /
            $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CashAndCashEquivalents - $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;

                if($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents > 0){

                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CashAndCashEquivalents / $dividend) - 1) * 100;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Equity and Liabilities</b></td>
        </tr>
        <tr>
            <td>1. Equity</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->Equity), 0, '.', ',')}}</td>
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Equity /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Equity /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->Equity - $balance_sheets[@count($balance_sheets) - 1]->Equity;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Equity;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->Equity / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Non-current liabilities</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->NoncurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->NoncurrentLiabilities / $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities) - 1) * 100;
                    }
                }
                
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>3. Current liabilities</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->CurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CurrentLiabilities / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>Assets; Equity and Liabilities</b></td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center"><b>{{number_format(round($balance_sheets[$x]->Assets), 0, '.', ',')}}</b></td>
            @endfor
            <td class="center">
                <b>100</b>
            </td>
            <td class="center">
                <b>100</b>
            </td>
            @php 
                $vardata = $balance_sheets[0]->Assets - $balance_sheets[@count($balance_sheets) - 1]->Assets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>{{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}</b>          
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Assets / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
                </b>
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
@php
    $b_assets = $balance_sheets[@count($balance_sheets) - 1]->Assets;
    $e_assets = $balance_sheets[0]->Assets;
    $assets_change = $e_assets - $b_assets;
    if($e_assets != '0.00' && $e_assets != 0) {
        $e_non_current_perc = ($balance_sheets[0]->NoncurrentAssets / $e_assets)*100;
        $e_current_perc = ($balance_sheets[0]->CurrentAssets / $e_assets)*100;
    } else {
        $e_non_current_perc = 0;
        $e_current_perc = 0;
    }
    $b_equity = $balance_sheets[@count($balance_sheets) - 1]->Equity;
    $e_equity = $balance_sheets[0]->Equity;
    $equity_change = $e_equity - $b_equity;
    if($b_equity != '0.00' && $b_equity != 0){
        $equity_change_perc = (($e_equity / $b_equity) - 1) * 100;
    }else{
        $equity_change_perc = 0;
    }
    if($b_assets != '0.00' && $b_assets != 0) {
        $assets_change_times = $balance_sheets[0]->Assets/$b_assets;
        $assets_change_perc = ($assets_change/$b_assets)*100;
    }else{
        $assets_change_times = 0;
        $assets_change_perc = 0;
    }
    $b_trade = $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
    $e_trade = $balance_sheets[0]->TradeAndOtherCurrentReceivables;
    $trade_change = $e_trade - $b_trade;
    if($b_trade != '0.00' && $b_trade != 0) {
        $trade_change_times = $e_trade/$b_trade;
    }else{
        $trade_change_times = 0;
    }
    $b_inventories = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
    $e_inventories = $balance_sheets[0]->Inventories;
    $inventories_change = $e_inventories - $b_inventories;
    if($b_inventories != '0.00' && $b_inventories != 0) {
        $inventories_change_times = $e_inventories/$b_inventories;
    } else {
        $inventories_change_times = 0;
    }
@endphp

<p class="justify">
    @if($e_non_current_perc >= 24 && $e_non_current_perc <=26)
        According to the table above, the non-current assets of {{ $financial_report->title }} makes about a quarter (<span class="{{ $e_non_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_non_current_perc, 1) }}</span>%) at the end of the period reviewed, while current assets make three quarters(<span class="{{ $e_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_current_perc, 1) }}</span>%), respectively.
    @elseif($e_non_current_perc >= 31 && $e_non_current_perc <=34)
        According to the data given in the table, the share of non-current assets makes about one third (<span class="{{ $e_non_current_perc > 0 ? 'green' : 'red' }}">{{ round($e_non_current_perc, 1) }}</span>%) at the end of the period reviewed, while current assets take two thirds, respectively.
        Such correlation is typical for the activity types where considerable investing of fixed assets and long-term investments are not required but where stocks of raw materials, supplies and goods are plentiful.
    @else
        {{ $financial_report->title }}'s assets are described with the following correlation on the last day of the period analyzed: non-current assets are {{ round($e_non_current_perc, 1) }}%, while current assets are {{ round($e_current_perc, 1) }}%.
    @endif

    @if($assets_change > 0)
        The assets were verified to {{ $assets_change_perc > 20 ? 'increase sharply' : 'spike moderately' }} from PHP <span class="{{ $b_assets > 0 ? 'green' : 'red' }}">{{ number_format($b_assets) }}</span> to PHP <span class="{{ $e_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_assets) }}</span> (i.e. by <span class="{{ $assets_change_perc > 0 ? 'green' : 'red' }}">{{ $assets_change_perc < 200 ? round($assets_change_perc, 1).'%' : round($assets_change_times, 1).' times'}} </span>) for the entire period reviewed.
    @else
        The assets lessened from PHP {{ number_format($b_assets) }} to PHP {{ number_format($e_assets) }} (by PHP <span class="{{ $assets_change > 0 ? 'green' : 'red' }}">{{ number_format($assets_change) }}</span> , or by <span class="{{ $assets_change_perc > 0 ? 'green' : 'red' }}">{{ round($assets_change_perc, 1) }}</span>%) during the whole period reviewed.
    @endif

    @if($assets_change > 0 && $equity_change_perc > 0)
        The company's assets grew simultaneously with equity (<span class="{{ $equity_change_perc > 0 ? 'green' : 'red' }}">{{ $equity_change_perc > 0 ? '+' : ''}}{{ round($equity_change_perc, 1) }}</span>% for the entire period reviewed). 
        Growth of the equity value is a factor which positively describes the dynamics of {{ $financial_report->title }}'s financial state.
    @elseif($assets_change > 0 && $equity_change_perc < 0)
        Despite a growth of the company's assets, the equity of {{ $financial_report->title }} almost did not change (PHP <span class="{{ $equity_change > 0 ? 'green' : 'red' }}">{{ $equity_change > 0 ? '+' : ''}}{{ number_format($equity_change) }}</span>) and showed PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ number_format($e_equity) }}</span> 31 December, {{ (int)$financial_report->year }}.
    @elseif($assets_change < 0 && $equity_change_perc > 0)
        Equity grew by <span class="{{ $equity_change_perc > 0 ? 'green' : 'red' }}">{{ $equity_change_perc > 0 ? '+' : ''}}{{ round($equity_change_perc, 1) }}</span>% and showed PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ $e_equity > 0 ? '+' : ''}}{{ number_format($e_equity) }}</span> at the end of the period analyzed.
        Growth of the company's equity should be considered as a positive factor.
    @elseif($assets_change < 0 && $equity_change_perc < 0)
        The equity of {{ $financial_report->title }} almost did not change (PHP <span class="{{ $equity_change > 0 ? 'green' : 'red' }}">{{ $equity_change > 0 ? '+' : ''}}{{ number_format($equity_change) }}</span>) and showed PHP <span class="{{ $e_equity > 0 ? 'green' : 'red' }}">{{ number_format($e_equity) }}</span> 31 December, {{ (int)$financial_report->year }}.
    @endif
</p>

<p class="justify">
    The {{ $assets_change > 0 ? 'increase' : 'reduction' }} in total assets of {{ $financial_report->title }} occurred due to the growth of the following asset types (amount of change and percentage of this change relative to the total assets growth are shown below):
</p>

<ul>
    @php
        $negative_assets_list_count = 0;
        $positive_assets_list_count = 0;
        $assets_list = [
            'PropertyPlantAndEquipment' => 'Property, plant and equipment',
            'InvestmentProperty' => 'Investment property',
            'Goodwill' => 'Goodwill',
            'IntangibleAssetsOtherThanGoodwill' => 'Intangible assets other than goodwill',
            'InvestmentAccountedForUsingEquityMethod' => 'Investment accounted for using equity method',
            'InvestmentsInSubsidiariesJointVenturesAndAssociates' => 'Investments in subsidiaries, joint ventures and associates',
            'NoncurrentBiologicalAssets' => 'Non-current biological assets',
            'NoncurrentReceivables' => 'Trade and other non-current receivables',
            'NoncurrentInventories' => 'Non-current inventories',
            'DeferredTaxAssets' => 'Deferred tax assets',
            'CurrentTaxAssetsNoncurrent' => 'Current tax assets, non-current',
            'OtherNoncurrentFinancialAssets' => 'Other non-current financial assets',
            'OtherNoncurrentNonfinancialAssets' => 'Other non-current non-financial assets',
            'NoncurrentNoncashAssetsPledgedAsCollateral' => 'Non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'Inventories' => 'Current inventories',
            'TradeAndOtherCurrentReceivables' => 'Trade and other current receivables',
            'CurrentTaxAssetsCurrent' => 'Current tax assets, current',
            'CurrentBiologicalAssets' => 'Current biological assets',
            'OtherCurrentFinancialAssets' => 'Other current financial assets',
            'OtherCurrentNonfinancialAssets' => 'Other current non-financial assets',
            'CashAndCashEquivalents' => 'Cash and cash equivalents',
            'CurrentNoncashAssetsPledgedAsCollateral' => 'Current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'NoncurrentAssetsOrDisposalGroups' => 'Non-current assets or disposal groups classified as held for sale or as held for distribution to owners',
        ];
        $positives_assets_total = 0;
        $negatives_assets_total = 0;
        $positives_assets_list = array();
        $negatives_assets_list = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_assets_list_count++;
                $positives_assets_total += $amount;
            } elseif($amount < 0) {
                $negatives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 1
                );
                $negative_assets_list_count++;
                $negatives_assets_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_assets_list, 'amount'), SORT_DESC, $positives_assets_list);
        array_multisort(array_column($negatives_assets_list, 'amount'), SORT_ASC, $negatives_assets_list);
    @endphp
    @if($assets_change > 0)
        @foreach($positives_assets_list as $k=>$a)
            @php
                $percentage = ($a['amount'] / $positives_assets_total) * 100;
            @endphp
            @if($percentage > 5)
                <li>{{ $a['item'] }} – PHP <span class="green">{{ number_format($a['amount']) }}</span> (<span class="green">{{ round($percentage, 1)  }}</span>%)</li>
            @endif
        @endforeach
    @else
        @foreach($negatives_assets_list as $k=>$a)
            @php
                $percentage = (abs($a['amount']) / $negatives_assets_total) * 100;
            @endphp
            @if($percentage > 5)
                <li>{{ $a['item'] }} – PHP <span class="red">{{ number_format(abs($a['amount'])) }}</span> (<span class="red">{{ round($percentage, 1)  }}</span>%)</li>
            @endif
        @endforeach
    @endif
</ul>
<p class="justify">
    The most significant {{ $assets_change > 0 ? 'growth' : 'reduction' }} of sources of finance ("Equity and Liabilities") is seen on the following rates (the percentage from total equity and liabilities change is shown in brackets):
</p>
<ul>
    @php
        $assets_list = [
            'IssuedCapital' => 'Issued (share) capital',
            'SharePremium' => 'Share premium',
            'TreasuryShares' => 'Treasury shares',
            'OtherEquityInterest' => 'Other equity interest',
            'OtherReserves' => 'Other reserves',
            'RetainedEarnings' => 'Retained earnings',
            'NoncontrollingInterests' => 'Non-controlling interests',
            'NoncurrentProvisionsForEmployeeBenefits' => 'Non-current provisions for employee benefits',
            'OtherLongtermProvisions' => 'Other non-current provisions',
            'NoncurrentPayables' => 'Trade and other non-current payables',
            'DeferredTaxLiabilities' => 'Deferred tax liabilities',
            'CurrentTaxLiabilitiesNoncurrent' => 'Current tax liabilities, non-current',
            'OtherNoncurrentFinancialLiabilities' => 'Other long-term financial liabilities',
            'OtherNoncurrentNonfinancialLiabilities' => 'Other non-current non-financial liabilities',
            'CurrentProvisionsForEmployeeBenefits' => 'Current provisions for employee benefits',
            'OtherShorttermProvisions' => 'Other current provisions',
            'TradeAndOtherCurrentPayables' => 'Trade and other current payables',
            'CurrentTaxLiabilitiesCurrent' => 'Current tax liabilities, current',
            'OtherCurrentFinancialLiabilities' => 'Other current financial liabilities',
            'OtherCurrentNonfinancialLiabilities' => 'Other current non-financial liabilities',
            'LiabilitiesIncludedInDisposalGroups' => 'Liabilities included in disposal groups classified as held for sale'
        ];
        $positive_finance_list_count = 0;
        $negative_finance_list_count = 0;
        $positives_finance_total = 0;
        $negatives_finance_total = 0;
        $positives_finance_list = array();
        $negatives_finance_list = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_finance_list_count++;
                $positives_finance_total += $amount;
            } elseif($amount < 0) {
                $negatives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 2
                );
                $negative_finance_list_count++;
                $negatives_finance_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_finance_list, 'amount'), SORT_DESC, $positives_finance_list);
        array_multisort(array_column($negatives_finance_list, 'amount'), SORT_ASC, $negatives_finance_list);
    @endphp
    @if($assets_change > 0)
        @foreach($positives_finance_list as $k=>$a)
            @php
                $percentage = ($a['amount'] / $positives_finance_total) * 100;
            @endphp
            @if($percentage > 5)
                <li>{{ $a['item'] }} – PHP <span class="green">{{ number_format($a['amount']) }}</span> (<span class="green">{{ round($percentage, 1)  }}</span>%)</li>
            @endif
        @endforeach
    @else
        @foreach($negatives_finance_list as $k=>$a)
            @php
                $percentage = (abs($a['amount']) / $negatives_finance_total) * 100;
            @endphp
            @if($percentage > 5)
                <li>{{ $a['item'] }} – PHP <span class="red">{{ number_format(abs($a['amount'])) }}</span> (<span class="red">{{ round($percentage, 1)  }}</span>%)</li>
            @endif
        @endforeach
    @endif
</ul>

<p class="justify">
    @if($assets_change > 0)
        Negative change in the {{ ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1) ? 'itmes' : 'item'}}
        @php
            $str = '';
            foreach($negatives_assets_list as $k=>$n) {
                $str .= ' "'.$n['item'].'",';
                break;
            }
            $str = rtrim($str, ",");
            echo $str;
            if(count($negatives_finance_list) > 0) {
                echo ' in assets and';
                $str = '';
                foreach($negatives_finance_list as $n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str. ' in finance';
            }
            echo ', which';
            echo ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1)? ' were' : ' was';
            $str = '';
            foreach($negatives_assets_list as $n) {
                $str .= ' PHP <span class="red">'.number_format($n['amount']).'</span>,';
                break;
            }
            $str = rtrim($str, ",");
            echo $str;
            if(count($negatives_finance_list) > 0) {
                echo " and ";
                $str = '';
                foreach($negatives_finance_list as $n) {
                    $str .= ' PHP <span class="red">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str. ' respectively,';
            }
        @endphp
         did not allow the total assets of the company to increase to a greater degree.
    @else
        The positively changed {{ ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'itmes' : 'item'}} in the balance sheet during the whole period reviewed {{ ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'are' : 'is'}}
        @php
            $str = '';
            foreach($positives_assets_list as $n) {
                $str .= ' "'.$n['item'].'",';
                break;
            }
            $str = rtrim($str, ",");
            echo $str;
            if(count($positives_finance_list) > 0) {
                echo ' in assets and';
                $str = '';
                foreach($positives_finance_list as $n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str. ' in finance';
            }
            echo ', which';
            echo ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1)? ' were' : ' was';
            $str = '';
            foreach($positives_assets_list as $n) {
                $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                break;
            }
            $str = rtrim($str, ",");
            echo $str;
            if(count($positives_finance_list) > 0) {
                echo " and ";
                $str = '';
                foreach($positives_finance_list as $n) {
                    $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str. ' respectively.';
            }
        @endphp
    @endif
</p>

<p class="justify">In the chart below, you will see a correlation of the basic groups of the company's assets.</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
</div>

@if($e_inventories > 0)
    <p class="justify">
        @if($inventories_change > 0)
            @if($inventories_change_times > 5)
                At the end of the period reviewed, the inventories equaled PHP were equal to PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
                The inventories grew by <span class="{{ $inventories_change_times > 0 ? 'green' : 'red' }}">{{ round($inventories_change_times, 1) }}</span> times during the entire period analyzed.
            @else
                During the period analyzed, the inventories amounted to PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
                For the period analyzed, the inventories {{ $inventories_change > 50000 ? 'sharply spiked' : 'appreciably climbed' }} to PHP <span class="red">{{ number_format($inventories_change) }}</span>.
            @endif
        @else
            On the last day of the period analyzed, the inventories amounted to PHP <span class="{{ $e_inventories > 0 ? 'green' : 'red' }}">{{ number_format($e_inventories) }}</span>.
            For the period analyzed, the inventories went down by PHP <span class="red">{{ number_format($inventories_change) }}</span>.
        @endif
    </p>
@endif

<p class="justify">
    @if($trade_change > 0)
        The current receivables went up by <span class="green">{{ round($trade_change_times, 1) }}</span> times during the entire period analyzed.
    @else
        For the entire period analyzed, it was seen that there was a marked reduction in the current receivables of PHP <span class="red">{{ number_format($trade_change) }}</span>.
    @endif
</p>
