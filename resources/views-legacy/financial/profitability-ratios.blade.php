<h3>2.2 Profitability Ratios</h3>
<table cellpadding="5" cellspacing="0">
    <thead></thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Profitability Ratios</td>
            <td class="center" colspan="{{ @count($balance_sheets)-1 }}">Value in %</td>
            <td class="center" rowspan="2">Change (col.4 - col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets)-1; $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>1. Gross margin.</td>
            @php
                $max_count = @count($balance_sheets)-2;
            @endphp
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $GrossMargin = ($income_statements[$x]->GrossProfit / 
                                    $income_statements[$x]->Revenue) * 100;
                @endphp
                <td class="center">
                    @if($GrossMargin != 0)
                        {{ number_format($GrossMargin, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent1 = number_format($GrossMargin, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = (($income_statements[0]->GrossProfit / 
                            $income_statements[0]->Revenue) -
                            ($income_statements[$max_count]->GrossProfit / 
                            $income_statements[$max_count]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>2. Return on sales (operating margin).</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ROS = (($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->Revenue) * 100;
                @endphp
                <td class="center">
                    @if($ROS != 0)
                        {{ number_format($ROS, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent2 = number_format($ROS, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = ((($income_statements[0]->ProfitLossBeforeTax + 
                            $income_statements[0]->FinanceCosts) /
                            $income_statements[0]->Revenue) -
                            (($income_statements[$max_count]->ProfitLossBeforeTax + 
                            $income_statements[$max_count]->FinanceCosts) /
                            $income_statements[$max_count]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>3. Profit margin. </td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitMargin = ($income_statements[$x]->ProfitLoss /
                            $income_statements[$x]->Revenue) * 100;
                @endphp
                <td class="center">
                    @if($ProfitMargin != 0)
                        {{ number_format($ProfitMargin, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ProfitMargin, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = (($income_statements[0]->ProfitLoss /
                            $income_statements[0]->Revenue) -
                            ($income_statements[$max_count]->ProfitLoss /
                            $income_statements[$max_count]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>Reference:<br />Interest coverage ratio (ICR). Acceptable value: no less than 1.5.</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    if($income_statements[$x]->FinanceCosts != 0){
                        $ICR = ($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->FinanceCosts;
                    } else {
                        $ICR = 0;
                    }
                    
                    $class = ($ICR >= 1.5) ? 'green' : 'red'
                @endphp
                <td class="center {{ $class }}">
                    @if($ICR != 0)
                        {{ number_format($ICR, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent4 = number_format($ICR, 1, '.', ','); @endphp
            @endfor
            @php 
                if($income_statements[0]->FinanceCosts != 0 && $income_statements[$max_count]->FinanceCosts != 0){
                    $Change = (($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) /
                                $income_statements[0]->FinanceCosts) -
                                (($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts) /
                                $income_statements[$max_count]->FinanceCosts);
                } else {
                    $Change = 0;
                }
                
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
    </tbody>
</table>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_1_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>

<br/><br/>

<table cellpadding="5" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Profitability Ratios</td>
            <td class="center" colspan="{{ @count($balance_sheets)-1 }}">Value in %</td>
            <td class="center" rowspan="2">Change (col.4 - col.2)</td>
            <td class="center" rowspan="2">Description of the ratio and its reference value</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets)-1; $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td width="12%">Return on equity (ROE)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROE = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x + 1]->Equity) / 2)) * 100;
                        if($x == $max_count) $ROE1 = round($ROE, 1);
                        if($x == 0) $ROE2 = round($ROE, 1);
                        $class = ($ROE >= 12) ? 'green' : 'red';
                    }else{
                        $ROE = 0;
                        $ROE1 = 0;
                        $ROE2 = 0;
                    }
                @endphp
                <td class="center {{ $class }}">
                    @if($ROE != 0)
                        {{ number_format($ROE, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ROE, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROE2 - $ROE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROE is calculated by taking a year's worth of
                earnings (net profit) and dividing them by the
                average shareholder equity for that period, and is
                expressed as a percentage. It is one of the most 
                important financial ratios and profitability metrics.
                Acceptable value: 12% or more.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Return on assets (ROA)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROA = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Assets + 
                                    $balance_sheets[$x + 1]->Assets) / 2)) * 100;
                        if($x == $max_count) $ROA1 = round($ROA, 1);
                        if($x == 0) $ROA2 = round($ROA, 1);
                        $class = ($ROA >= 6) ? 'green' : 'red';
                    }else{
                        $ROA = 0;
                        $ROA1 = 0;
                        $ROA2 = 0;
                    }
                @endphp
                <td class="center {{ $class }}">
                    @if($ROA != 0)
                        {{ number_format($ROA, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent1 = number_format($ROA, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROA2 - $ROA1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 1, '.', ',');
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROA is calculated by dividing net income by total
                assets, and displayed as a percentage. Acceptable
                value: 6% or more.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Return on capital employed (ROCE)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php

                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROCE = (
                                (
                                $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts
                                ) / 
                                (
                                    (
                                    $balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x]->NoncurrentLiabilities + 
                                    $balance_sheets[$x + 1]->Equity + 
                                    $balance_sheets[$x + 1]->NoncurrentLiabilities
                                    ) / 
                                2)
                            ) * 100;
                        if($x == $max_count) $ROCE1 = round($ROCE, 1);
                        if($x == 0) $ROCE2 = round($ROCE, 1);
                    }else{
                        $ROCE = 0;
                        $ROCE1 = 0;
                        $ROCE2 = 0;
                    }
                @endphp
                <td class="center">
                    @if($ROCE != 0)
                        {{ number_format($ROCE  , 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = $ROCE2 - $ROCE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROCE is calculated by dividing EBIT by capital
                employed (equity plus non-current liabilities). It
                indicates the efficiency and profitability of a
                company's capital investments.
            </td>
        </tr>
    </tbody>
</table>

<br /><br/>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_2_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>