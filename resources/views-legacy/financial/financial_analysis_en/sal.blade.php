<h3 class="pdf_title">{{ trans('financial_analysis/sal.1_1_structure_of_the') }}</h3>

<table style="width: 100%;page-break-inside: avoid !important;margin-bottom: 0;float: left;"  class="smallfont">

	<tr>
        <td class="center labels" rowspan="3" style="width: 75px">{{ trans('financial_analysis/sal.indicator') }}</td>
        <td class="center labels" colspan="{{ @count($balance_sheets) + 2 }}">{{ trans('financial_analysis/sal.value') }}</td>
        <td class="center labels" colspan="2">{{ trans('financial_analysis/sal.change_for_the_period') }}</td>
    </tr>
    <tr dontbreak="true">
        @if($financial_report->money_factor == 1000000)
            <td class="center labels" colspan="{{ @count($balance_sheets) }}">{!! trans('financial_analysis/sal.in_php_millions')!!}</td>
        @else
            <td class="center labels" colspan="{{ @count($balance_sheets) }}">{!! trans('financial_analysis/sal.in_php_thousand')!!}</td>
        @endif
        <td class="center labels" colspan="2">{{ trans('financial_analysis/sal.of_the_balance_total') }}</td>
        <td class="center labels" rowspan="2" style="width: 83px">{!! $financial_report->money_factor == 1000000 ? '<i>million</i>' : '<i>thousand</i>' !!} {!! trans('financial_analysis/sal.php_col',['col' => @count($balance_sheets) + 1]) !!}</td>
        <td class="center labels" rowspan="2" style="width: 40px">&plusmn; {{ trans('financial_analysis/sal.percent_col',['col' => @count($balance_sheets) + 1]) }}</td>
    </tr>
    <tr dontbreak="true">
        @for($x=@count($balance_sheets); $x > 0 ; $x--)
            <td class="center"  style="width: 83px">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
        @endfor
        <td class="center labels" style="width: 70px">{{ trans('financial_analysis/sal.at_the_beginning',['year' => ((int)$financial_report->year - @count($balance_sheets) + 1)]) }}</td>
        <td class="center labels"  style="width: 70px">{{ trans('financial_analysis/sal.at_the_end_of',['year' => (int)$financial_report->year]) }}</td>
    </tr>
	
</table>
<div style="clear: both"></div>
<table style="width:100%;float: left;margin-bottom: 15px" class="smallfont">
    <tbody>

        <tr>
            @php
            	echo '<td class="center" style="width: 75px">1</td>';
            	$col_count = @count($balance_sheets) + 1;
                for($i=2;$i<=$col_count;$i++){
                    echo '<td class="center" style="width: 83px">'.$i.'</td>';
                }
                echo '<td class="center" style="width: 70px">'.$i.'</td>';
                $i++;
                echo '<td class="center" style="width: 70px">'.$i.'</td>';
                $i++;
                echo '<td class="center" style="width: 83px">'.$i.'</td>';
                $i++;
                echo '<td class="center" style="width: 40px">'.$i.'</td>';
            @endphp

        </tr>
    
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Assets</b></td>
        </tr>

        <tr>
            <td>{!! wordwrap(trans('financial_analysis/sal.1_non_current_assets'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets /
                   $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = round(($balance_sheets[0]->NoncurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->NoncurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend =$balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->NoncurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/sal.2_current_assets_total'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentAssets), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = round(($balance_sheets[0]->CurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->CurrentAssets / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;{!! wordwrap(trans('financial_analysis/sal.inventories'),12,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->Inventories > 0) ? number_format(round($balance_sheets[$x]->Inventories), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 

                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Inventories /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Inventories / $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php
                $vardata = $balance_sheets[0]->Inventories - $balance_sheets[@count($balance_sheets) - 1]->Inventories;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories > 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Inventories / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }

                $inventory = $vardata;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;{!! wordwrap(trans('financial_analysis/sal.trade_and_other'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->TradeAndOtherCurrentReceivables > 0) ? number_format(round($balance_sheets[$x]->TradeAndOtherCurrentReceivables), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                    $dividend = $balance_sheets[0]->Assets;
                        if($dividend != '0.00' && $dividend != 0){
                        $vardata = round(($balance_sheets[0]->TradeAndOtherCurrentReceivables /
                $dividend) * 100, 1);
                    }else{
                        $vardata = 0;
                    }

            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->TradeAndOtherCurrentReceivables - $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables > 0){
                    $vardata = (($balance_sheets[0]->TradeAndOtherCurrentReceivables / $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables) - 1) * 100;
                }

                $tradeChange = number_format($vardata, 1, '.', ',');


            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">

                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                    @php
                    	$tradeChange = number_format(($vardata / 100) + 1, 1, '.', ',');
                    @endphp
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;{!! wordwrap(trans('financial_analysis/sal.cash_and_cash'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->CashAndCashEquivalents > 0) ? number_format(round($balance_sheets[$x]->CashAndCashEquivalents), 0, '.', ',') : '-'}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->Assets;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CashAndCashEquivalents /
            $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            @php 
                $vardata = $balance_sheets[0]->CashAndCashEquivalents - $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
            @endphp
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }} 
                @else
                    -
                @endif
            </td>
            @php
                $vardata = 0;

                if($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents > 0){

                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CashAndCashEquivalents / $dividend) - 1) * 100;
                    }
                }

                $cashEquivalent = $vardata;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Equity and Liabilities</b></td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/sal.1_equity'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->Equity), 0, '.', ',')}}</td>

                @php 
                    if($x == 0)
                        session(['capital' => $balance_sheets[$x]->Equity])
                @endphp
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Equity /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->Equity /$dividend
                ) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 

                $vardata = $balance_sheets[0]->Equity - $balance_sheets[@count($balance_sheets) - 1]->Equity;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Equity;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = (($balance_sheets[0]->Equity / $dividend) - 1) * 100;
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/sal.2_non_current_liabilities'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">
                    @php
                        if(round($balance_sheets[$x]->NoncurrentLiabilities) != 0){
                            echo number_format(round($balance_sheets[$x]->NoncurrentLiabilities), 0, '.', ',');
                        }else{
                            echo '-';
                        }
                    @endphp
                </td>
            @endfor
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @php
                    if($vardata != 0){
                        echo number_format($vardata, 1, '.', ',');
                    }else{
                        echo '-';
                    }
                @endphp
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->NoncurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @php
                    if($vardata != 0){
                        echo number_format($vardata, 1, '.', ',');
                    }else{
                        echo '-';
                    }
                @endphp
            </td>
            @php 
                $vardata = $balance_sheets[0]->NoncurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}
                @php
                    if($vardata != 0){
                        echo number_format($vardata, 0, '.', ',');
                    }else{
                        echo '-';
                    }
                @endphp
         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->NoncurrentLiabilities / $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities) - 1) * 100;
                    }
                }
                
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}

                    @php
                        if($vardata != 0){
                            echo number_format($vardata, 1, '.', ',');
                        }else{
                            echo '-';
                        }
                    @endphp

                @else
                    {{ $vardata > 0 ? '+' : '' }}

                    @php
                        if($vardata != 0){
                            echo number_format(($vardata / 100) + 1, 1, '.', ',');
                        }else{
                            echo '-';
                        }
                    @endphp
                     <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/sal.3_current_liabilities'),16,"<br/>") !!}</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            @php 
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php 
                $dividend = $balance_sheets[0]->EquityAndLiabilities;
                if($dividend != '0.00' && $dividend != 0){
                    $vardata = round(($balance_sheets[0]->CurrentLiabilities /
                $dividend) * 100, 1);
                }else{
                    $vardata = 0;
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            @php
                $vardata = $balance_sheets[0]->CurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}         
            </td>
            @php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0){
                    $dividend = $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->CurrentLiabilities / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
                }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>{!! wordwrap(trans('financial_analysis/sal.assets_equity'),16,"<br/>") !!}</b></td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center"><b>{{number_format(round($balance_sheets[$x]->Assets), 0, '.', ',')}}</b></td>
            @endfor
            <td class="center">
                <b>100</b>
            </td>
            <td class="center">
                <b>100</b>
            </td>
            @php 
                $vardata = $balance_sheets[0]->Assets - $balance_sheets[@count($balance_sheets) - 1]->Assets;
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>{{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}</b>          
            </td>
            @php
                $dividend = $balance_sheets[@count($balance_sheets) - 1]->Assets;
                    if($dividend != '0.00' && $dividend != 0){
                        $vardata = (($balance_sheets[0]->Assets / $dividend) - 1) * 100;
                    }else{
                        $vardata = 0;
                    }
            @endphp
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
                </b>
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
@php
    $b_assets = $balance_sheets[count($balance_sheets)-1]->Assets;
    $e_assets = $balance_sheets[0]->Assets;
    $assets_change = $e_assets - $b_assets;
    if($e_assets != '0.00' && $e_assets != 0) {
        $e_non_current_perc = ($balance_sheets[0]->NoncurrentAssets / $e_assets)*100;
        $e_current_perc = ($balance_sheets[0]->CurrentAssets / $e_assets)*100;
    } else {
        $e_non_current_perc = 0;
        $e_current_perc = 0;
    }
    $b_equity = $balance_sheets[@count($balance_sheets) - 1]->Equity;
    $e_equity = $balance_sheets[0]->Equity;
    $equity_change = $e_equity - $b_equity;
    if($b_equity != '0.00' && $b_equity != 0){
        $equity_change_perc = (($e_equity / $b_equity) - 1) * 100;
        $equity_change_times = $e_equity/$b_equity;
    }else{
        $equity_change_perc = 0;
        $equity_change_times = 0;
    }
    if($b_assets != '0.00' && $b_assets != 0) {
        $assets_change_times = $balance_sheets[0]->Assets/$b_assets;
        $assets_change_perc = ($assets_change/$b_assets)*100;
    }else{
        $assets_change_times = 0;
        $assets_change_perc = 0;
    }
    $b_trade = $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
    $e_trade = $balance_sheets[0]->TradeAndOtherCurrentReceivables;
    $trade_change = $e_trade - $b_trade;
    if($b_trade != '0.00' && $b_trade != 0) {
        $trade_change_perc = number_format($tradeChange,1,".",",");
        $trade_change_times = $e_trade/$b_trade;
    }else{
        $trade_change_perc = 0;
        $trade_change_times = 0;
    }
    $b_inventories = $balance_sheets[@count($balance_sheets) - 1]->Inventories;
    $e_inventories = $balance_sheets[0]->Inventories;
    $inventories_change = $e_inventories - $b_inventories;
    if($b_inventories != '0.00' && $b_inventories != 0) {
        $inventories_change_perc = (($e_inventories/$b_inventories)-1)*100;
        $inventories_change_times = $e_inventories/$b_inventories;
    } else {
        $inventories_change_perc = 0;
        $inventories_change_times = 0;
    }
    
@endphp

<p class="justify">

    @php

        $eNonCurrentPercClass = $e_non_current_perc > 0 ? 'green' : 'red';
        $eCurrentPercClass = $e_current_perc > 0 ? 'green' : 'red';

        $arr = [
            'eNonCurrentPercClass' => $eNonCurrentPercClass,
            'eNonCurrentPerc' => round($e_non_current_perc, 1),
            'eCurrentPercClass' => $eCurrentPercClass,
            'eCurrentPerc' => round($e_current_perc, 1),
            'year' => $financial_report->year,
            'financialReportTitle' => $financial_report->title
        ];

        if($e_non_current_perc >= 24 && $e_non_current_perc <=26):

            $percText = trans('financial_analysis/sal.percText1',$arr);

        elseif($e_non_current_perc >= 31 && $e_non_current_perc <=34):
            $percText = trans('financial_analysis/sal.percText2',$arr);
        elseif($e_non_current_perc >= 47 && $e_non_current_perc <=54):
            $percText = trans('financial_analysis/sal.percText3',$arr);
        else:
            $percText = trans('financial_analysis/sal.percText4',$arr);
        endif;

        echo '<p>'.$percText.'</p>';

        $assetsChangePercDescribe = $assets_change_perc > 20 ? 'underwent a sharp increase' : 'spike moderately';
        $bAssetsClass = $b_assets > 0 ? 'green' : 'red';
        $eAssetsClass = $e_assets > 0 ? 'green' : 'red';
        $assetsChangeClass = $assets_change > 0 ? 'green' : 'red';
        $assetsChangePercClass = $assets_change_perc > 0 ? 'green' : 'red';
        $assetsChangePerc = $assets_change_perc <= 200 ? round($assets_change_perc, 1) : round($assets_change_times, 1);
        $assetsChangePercSign = $assets_change_perc <= 200 ? "%" : " times";

        $arr = [
            'assetsChangePercDescribe' => $assetsChangePercDescribe,
            'bAssetsClass' => $bAssetsClass,
            'bAssets' => number_format($b_assets),
            'eAssets' => number_format($e_assets),
            'assetsChangeClass' => $assetsChangeClass,
            'assetsChange' => number_format($assets_change),
            'assetsChangePerc' => $assetsChangePerc,
            'assetsChangePercSign' => $assetsChangePercSign,
            'assetsChangePerc2' => round($assets_change_perc, 1)

        ];

        if($assets_change > 0){
            $assetsChangeText = trans('financial_analysis/sal.assetsChangeText1',$arr);
        }
        else{
            $assetsChangeText = trans('financial_analysis/sal.assetsChangeText2',$arr);
        }

        echo '<p>'.$assetsChangeText.'</p>';

        $equityChangeClass = $equity_change > 0 ? 'green' : 'red';
        $equityChangeSign = $equity_change > 0 ? '+' : '';
        $eEquityClass = $e_equity > 0 ? 'green' : 'red';
        $equityChangePercClass = $equity_change_perc > 0 ? 'green' : 'red';
        $equityChangePercSign = $equity_change_perc > 0 ? '+' : '';
        $equityChangePerc = $equity_change_perc <= 200 ? round($equity_change_perc, 1) : round($equity_change_times, 1);
        $equityChangePercTimes = $equity_change_perc <= 200 ? "%" : " times";
        $eEquitySign = $e_equity > 0 ? '+' : '';

        $arr = [
            'financialReportTitle' => $financial_report->title,
            'equityChangeClass' => $equityChangeClass,
            'equityChangeSign' => $equityChangeSign,
            'equityChange' => number_format($equity_change),
            'equityChangePercClass' => $equityChangePercClass,
            'equityChangePercSign' => $equityChangePercSign,
            'equityChangePerc' => $equityChangePerc,
            'equityChangePercTimes' => $equityChangePercTimes,
            'eEquityClass' => $eEquityClass,
            'eEquity' => number_format($e_equity),
            'year' => (int)$financial_report->year,
            'eEquitySign' => $eEquitySign
        ];

        $equityChangeText = '';

        if($assets_change > 0 && $equity_change_perc > 0):
            if($equity_change_perc < 5):
                $equityChangeText = trans('financial_analysis/sal.equityChangeText1',$arr);
            else:
                $equityChangeText = trans('financial_analysis/sal.equityChangeText2',$arr);
            endif;

        elseif($assets_change > 0 && $equity_change_perc < 0):
            $equityChangeText = trans('financial_analysis/sal.equityChangeText3',$arr);
        elseif($assets_change < 0 && $equity_change_perc > 0):
            $equityChangeText = trans('financial_analysis/sal.equityChangeText4',$arr);
        elseif($assets_change < 0 && $equity_change_perc < 0):
            $equityChangeText = trans('financial_analysis/sal.equityChangeText5',$arr);
        endif;

        echo '<p>'.$equityChangeText.'</p>';

    @endphp
</p>

    @php
        $negative_assets_list_count = 0;
        $positive_assets_list_count = 0;
        $assets_list = [
            'PropertyPlantAndEquipment' => 'Property, plant and equipment',
            'InvestmentProperty' => 'Investment property',
            'Goodwill' => 'Goodwill',
            'IntangibleAssetsOtherThanGoodwill' => 'Intangible assets other than goodwill',
            'InvestmentAccountedForUsingEquityMethod' => 'Investment accounted for using equity method',
            'InvestmentsInSubsidiariesJointVenturesAndAssociates' => 'Investments in subsidiaries, joint ventures and associates',
            'NoncurrentBiologicalAssets' => 'Non-current biological assets',
            'NoncurrentReceivables' => 'Trade and other non-current receivables',
            'NoncurrentInventories' => 'Non-current inventories',
            'DeferredTaxAssets' => 'Deferred tax assets',
            'CurrentTaxAssetsNoncurrent' => 'Current tax assets, non-current',
            'OtherNoncurrentFinancialAssets' => 'Other non-current financial assets',
            'OtherNoncurrentNonfinancialAssets' => 'Other non-current non-financial assets',
            'NoncurrentNoncashAssetsPledgedAsCollateral' => 'Non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'Inventories' => 'Current inventories',
            'TradeAndOtherCurrentReceivables' => 'Trade and other current receivables',
            'CurrentTaxAssetsCurrent' => 'Current tax assets, current',
            'CurrentBiologicalAssets' => 'Current biological assets',
            'OtherCurrentFinancialAssets' => 'Other current financial assets',
            'OtherCurrentNonfinancialAssets' => 'Other current non-financial assets',
            'CashAndCashEquivalents' => 'Cash and cash equivalents',
            'CurrentNoncashAssetsPledgedAsCollateral' => 'Current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
            'NoncurrentAssetsOrDisposalGroups' => 'Non-current assets or disposal groups classified as held for sale or as held for distribution to owners',
        ];
        $positives_assets_total = 0;
        $negatives_assets_total = 0;
        $positives_assets_list = array();
        $negatives_assets_list = array();
        $positives_assets_list_show = array();
        $negatives_assets_list_show = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_assets_list_count++;
                $positives_assets_total += $amount;
            } elseif($amount < 0) {
                $negatives_assets_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 1
                );
                $negative_assets_list_count++;
                $negatives_assets_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_assets_list, 'amount'), SORT_DESC, $positives_assets_list);
        array_multisort(array_column($negatives_assets_list, 'amount'), SORT_ASC, $negatives_assets_list);

        if($assets_change > 0) {
            foreach($positives_assets_list as $k=>$a) {
                $percentage = ($a['amount'] / $positives_assets_total) * 100;
                if($a['item'] == 'Trade and other current receivables'){
                	$percentage = abs($tradeChange);
                }
                if($a['item'] == 'Cash and cash equivalents'){
                	$percentage = abs($cashEquivalent);
                }
                if($a['item'] == 'Current inventories'){
                	$percentage = abs($inventory);
                }
                
                if($percentage > 5) {
                    $positives_assets_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );


                }
            }
        } else {
            foreach($negatives_assets_list as $k=>$a) {
                $percentage = (abs($a['amount']) / $negatives_assets_total) * 100;
                if($a['item'] == 'Trade and other current receivables'){
                	$percentage = abs($tradeChange);
                }
                if($a['item'] == 'Cash and cash equivalents'){
                	$percentage = abs($cashEquivalent);
                }
                if($a['item'] == 'Current inventories'){
                	$percentage = abs($inventory);
                }


                if($percentage > 5) {
                    $negatives_assets_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        }
    @endphp
    @if($assets_change > 0)
        @if(count($positives_assets_list_show) == 1)
            <p class="justify">
                @php
                    $arr = [
                        'financialReportTitle' => $financial_report->title,
                        'item' => $positives_assets_list_show[0]['item'],
                        'amount' => number_format(abs($positives_assets_list_show[0]['amount'])),
                        'percentage' => round($positives_assets_list_show[0]['percentage'], 1)

                    ];
                @endphp

                {!! trans('financial_analysis/sal.the_increase_in_total',$arr) !!}
            </p>
        @else
            <p class="justify">
                
                {{ trans('financial_analysis/sal.the_increase_in_total2',['financialReportTitle' => $financial_report->title]) }}
            </p>
            <ul>
                @foreach($positives_assets_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="green">{{ number_format(abs($v['amount'])) }}</span> (<span class="green">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @else
        @if(count($negatives_assets_list_show) == 1)
            @php

                $arr = [
                    'financialReportTitle' => $financial_report->title,
                    'item' => $negatives_assets_list_show[0]['item'],
                    'amount' => number_format(abs($negatives_assets_list_show[0]['amount'])),
                    'percentage' => round($negatives_assets_list_show[0]['percentage'], 1)
                ];
            @endphp

            <p class="justify">
                
                {!! trans('financial_analysis/sal.the_reduction_in_total',$arr) !!}
            </p>
        @else
            <p class="justify">
                
                {{ trans('financial_analysis/sal.the_reduction_in_total2',$arr) }}
            </p>
            <ul>
                @foreach($negatives_assets_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="red">{{ number_format(abs($v['amount'])) }}</span> (<span class="red">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @endif

    @php
        $assets_list = [
            'IssuedCapital' => 'Issued (share) capital',
            'SharePremium' => 'Share premium',
            'TreasuryShares' => 'Treasury shares',
            'OtherEquityInterest' => 'Other equity interest',
            'OtherReserves' => 'Other reserves',
            'RetainedEarnings' => 'Retained earnings',
            'NoncontrollingInterests' => 'Non-controlling interests',
            'NoncurrentProvisionsForEmployeeBenefits' => 'Non-current provisions for employee benefits',
            'OtherLongtermProvisions' => 'Other non-current provisions',
            'NoncurrentPayables' => 'Trade and other non-current payables',
            'DeferredTaxLiabilities' => 'Deferred tax liabilities',
            'CurrentTaxLiabilitiesNoncurrent' => 'Current tax liabilities, non-current',
            'OtherNoncurrentFinancialLiabilities' => 'Other long-term financial liabilities',
            'OtherNoncurrentNonfinancialLiabilities' => 'Other non-current non-financial liabilities',
            'CurrentProvisionsForEmployeeBenefits' => 'Current provisions for employee benefits',
            'OtherShorttermProvisions' => 'Other current provisions',
            'TradeAndOtherCurrentPayables' => 'Trade and other current payables',
            'CurrentTaxLiabilitiesCurrent' => 'Current tax liabilities, current',
            'OtherCurrentFinancialLiabilities' => 'Other current financial liabilities',
            'OtherCurrentNonfinancialLiabilities' => 'Other current non-financial liabilities',
            'LiabilitiesIncludedInDisposalGroups' => 'Liabilities included in disposal groups classified as held for sale'
        ];
        $positive_finance_list_count = 0;
        $negative_finance_list_count = 0;
        $positives_finance_total = 0;
        $negatives_finance_total = 0;
        $positives_finance_list = array();
        $negatives_finance_list = array();
        $positives_finance_list_show = array();
        $negatives_finance_list_show = array();
        foreach($assets_list as $k=>$al) {
            $amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
            if($amount > 0){
                $positives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al
                );
                $positive_finance_list_count++;
                $positives_finance_total += $amount;
            } elseif($amount < 0) {
                $negatives_finance_list[$k] = array(
                    'amount' => $amount,
                    'item' => $al,
                    'type' => 2
                );
                $negative_finance_list_count++;
                $negatives_finance_total += abs($amount);
            }
        }
        array_multisort(array_column($positives_finance_list, 'amount'), SORT_DESC, $positives_finance_list);
        array_multisort(array_column($negatives_finance_list, 'amount'), SORT_ASC, $negatives_finance_list);

        if($assets_change > 0) {
            foreach($positives_finance_list as $k=>$a) {
                $percentage = ($a['amount'] / $positives_finance_total) * 100;

                if($percentage > 5) {
                    $positives_finance_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        } else {
            foreach($negatives_finance_list as $k=>$a) {
                $percentage = (abs($a['amount']) / $negatives_finance_total) * 100;
                 
                if($percentage > 5) {
                    $negatives_finance_list_show[] = array(
                        'item'      => $a['item'],
                        'amount'    => $a['amount'],
                        'percentage'=> $percentage
                    );
                }
            }
        }


    @endphp

    @if($assets_change > 0)

        @php

            $arr = [
                'item' => $positives_finance_list_show[0]['item'],
                'amount' => number_format(abs($positives_finance_list_show[0]['amount'])),
                'percentage' => round($positives_finance_list_show[0]['percentage'], 1)
            ];

        @endphp
        @if(count($positives_finance_list_show) == 1)
            <p class="justify">    
               {!! trans('financial_analysis/sal.a_growth_in_sources',$arr) !!}
            </p>
        @else
            <p class="justify">
                
                {{ trans('financial_analysis/sal.the_most_significant_growth') }}
            </p>
            <ul>
                @foreach($positives_finance_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="green">{{ number_format(abs($v['amount'])) }}</span> (<span class="green">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @else
        @if(count($negatives_finance_list_show) == 1)

            @php

                $arr = [
                    'item' => $negatives_finance_list_show[0]['item'],
                    'amount' => number_format(abs($negatives_finance_list_show[0]['amount'])),
                    'percentage' => round($negatives_finance_list_show[0]['percentage'], 1)
                ];

            @endphp
            <p class="justify">
                {!! trans('financial_analysis/sal.a_reduction_in_sources',$arr) !!}
            </p>
        @else
            <p class="justify">
                {{ trans('financial_analysis/sal.the_most_significant_reduction',$arr) }}
            </p>
            <ul>
                @foreach($negatives_finance_list_show as $k=>$v)
                    <li>{{ $v['item'] }} – PHP <span class="red">{{ number_format(abs($v['amount'])) }}</span> (<span class="red">{{ round($v['percentage'], 1)  }}</span>%)</li>
                @endforeach
            </ul>
        @endif
    @endif

<p class="justify">
    @if($assets_change > 0)
        @if($negative_assets_list_count > 0)
            
            @php

            $negativeText = 'Negative change in the';
            $item = ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1) ? 'items' : 'item';

            echo trans('financial_analysis/sal.negative_change',['item' => $item]);
            
                $str = '';
                foreach($negatives_assets_list as $k=>$n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($negatives_finance_list) > 0) {
                    echo ' '.trans('financial_analysis/sal.in_assets_and');
                    $str = '';
                    foreach($negatives_finance_list as $n) {
                        $str .= ' "'.$n['item'].'",';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' in finance';
                }
                
                echo ', '.trans('financial_analysis/sal.which');
                echo ($negative_assets_list_count >= 1 && $negative_finance_list_count >= 1)? ' '.trans('financial_analysis/sal.were') : ' '.trans('financial_analysis/sal.was');
                $str = '';
                foreach($negatives_assets_list as $n) {
                    $str .= ' loss PHP <span class="red">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($negatives_finance_list) > 0) {
                    echo ' '.trans('financial_analysis/sal.and').' ';
                    $str = '';
                    foreach($negatives_finance_list as $n) {
                        $str .= ' PHP <span class="red">'.number_format($n['amount']).'</span>,';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' respectively,';
                }
            @endphp
             {{ trans('financial_analysis/sal.did_not_allow') }}
        @endif
    @else
        @if($positive_assets_list_count > 0)

            @php

                $item = ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'items' : 'item';

                $is = ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1) ? 'are' : 'is';

                $arr = [
                    'item' => $item,
                    'is' => $is
                ];

                echo trans('financial_analysis/sal.the_positively_changed',$arr);
            
                $str = '';
                foreach($positives_assets_list as $n) {
                    $str .= ' "'.$n['item'].'",';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($positives_finance_list) > 0) {
                    echo ' in assets and';
                    $str = '';
                    foreach($positives_finance_list as $n) {
                        $str .= ' "'.$n['item'].'",';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' in finance';
                }
                echo ', '.trans('financial_analysis/sal.which');
                echo ($positive_assets_list_count >= 1 && $positive_finance_list_count >= 1)? ' '.trans('financial_analysis/sal.were') : ' '.trans('financial_analysis/sal.was');
                $str = '';
                foreach($positives_assets_list as $n) {
                    $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                    break;
                }
                $str = rtrim($str, ",");
                echo $str;
                if(count($positives_finance_list) > 0) {
                    echo ' '.trans('financial_analysis/sal.and').' ';
                    $str = '';
                    foreach($positives_finance_list as $n) {
                        $str .= ' PHP <span class="green">'.number_format($n['amount']).'</span>,';
                        break;
                    }
                    $str = rtrim($str, ",");
                    echo $str. ' respectively.';
                }
            @endphp
        @endif
    @endif
</p>

<p class="justify">{{ trans('financial_analysis/sal.in_the_chart_below') }}</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
</div>

@if($e_inventories > 0)
    <p class="justify">

        @php

            $eInventoriesClass = $e_inventories > 0 ? 'green' : 'red';
            $inventoriesChangeClass =  $inventories_change > 0 ? 'green' : 'red';
            $inventoriesChangeTimesClass = $inventories_change_times > 0 ? 'green' : 'red';
            $inventoriesChangePercDescribe = $inventories_change_perc > 30 ? 'sharply spiked' : 'appreciably climbed';
            $inventoriesChangePercClass = $inventories_change_perc > 0 ? 'green' : 'red';
            $inventoriesChangePercSign = $inventories_change_perc <= 200 ? "%" : " times";
            $inventoriesChangePercL200 = $inventories_change_perc <= 200 ? round(abs($inventories_change_perc), 1) : round(abs($inventories_change_times), 1);

            $arr = [
                'eInventoriesClass' => $eInventoriesClass,
                'eInventories' => number_format($e_inventories),
                'inventoriesChangeClass' => $inventoriesChangeClass,
                'inventoriesChange' => number_format($inventories_change),
                'inventoriesChangeTimes' => round($inventories_change_times, 1),
                'inventoriesChangePercDescribe' => $inventoriesChangePercDescribe,
                'inventoriesChangePercClass' => $inventoriesChangePercClass,
                'inventoriesChangePercSign' => $inventoriesChangePercSign,
                'inventoriesChangePercL200' => $inventoriesChangePercL200
            ];

        @endphp

        @if($inventories_change > 0)
            @if($inventories_change_times > 5)
                
                {!! trans('financial_analysis/sal.inventoriesText1',$arr) !!}

            @else
                
                {!! trans('financial_analysis/sal.inventoriesText2',$arr) !!}

                @if($inventories_change_perc != 0)

                {!! trans('financial_analysis/sal.inventoriesText3',$arr) !!}

                @endif 

                for the entire period reviewed.
            @endif
        @else
            

            {!! trans('financial_analysis/sal.inventoriesText4',$arr) !!}

            @if($inventories_change_perc != 0)
           

            {!! trans('financial_analysis/sal.inventoriesText5',$arr) !!}

            @endif.
        @endif
    </p>
@endif

<p class="justify"> 

    @php
        $tradeChangeClass = $trade_change > 0 ? 'green' : 'red';
        $tradeChangePerc = $trade_change_perc <= 200 ? round($trade_change_perc, 1) : round($trade_change_times, 1);
        $tradeChangePercSign = $trade_change_perc <= 200 ? "%" : " times";

        $arr = [
            'tradeChangeClass' => $tradeChangeClass,
            'tradeChange' => number_format($trade_change),
            'tradeChangePerc' => $tradeChangePerc,
            'tradeChangePercSign' => $tradeChangePercSign

        ];

    @endphp
</p>
