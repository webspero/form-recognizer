@extends('layouts.report')

@section('content')

	@if($bank->isLabel == 1)
		<div>
			<img class = 'pdf-hdr-logo' src="{{ URL::to('/') }}/images/brand_images/{{$bank->brand_img}}" style="width: auto; height: 60px; margin-bottom:5px;" /><br>
			<p><img src="{{ URL::to('/') }}/images/powered-by-cbpo-logo.png" alt=".CreditBPO" style="width: auto; height: 30px; margin-bottom:5px;"/></p>
		</div>
	@else
		<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt=".CreditBPO" />
	@endif

	<h1>

		{{ trans('financial_analysis/financial_analysis.financial_condition_analysis',['financial_report_title' => $financial_report->title, 'startYear' => ((int)$financial_report->year - @count($balance_sheets) + 2),'endYear' => (int)$financial_report->year]) }}
	</h1>

	<!-- include Table of Contents -->
	@include('financial.financial_analysis_en.toc')


	<h2 class="pdf_title">
		
		{{ trans('financial_analysis/financial_analysis.financial_position_analysis',['financial_report_title' => $financial_report->title]) }}

	</h2>

	<!-- Paragraph Before Structure of Assets and Liabilities -->
	<p class="justify">
	
		{{ trans('financial_analysis/financial_analysis.the_following_report_analyzes',['financial_report_title' => $financial_report->title, 'startYear' => ((int)$financial_report->year - @count($balance_sheets) + 2),'endYear' => (int)$financial_report->year]) }}

	</p>

	<!-- include Structure of Assets and Liabilities -->
	@include('financial.financial_analysis_en.sal')

	<!-- include Net Assets -->
	@include('financial.financial_analysis_en.net-assets')

	<!-- include Financial SUstainability Analysis -->
	@include('financial.financial_analysis_en.financial_sustainability')

	<!-- include Liquidity Analysis -->
	@include('financial.financial_analysis_en.liquidity-analysis')

	<h2>
	{{ trans('financial_analysis/financial_analysis.2_financial_performance') }}
	</h2>

	<!-- include Financial Results -->
	@include('financial.financial_analysis_en.financial_result')

	<!-- include Profitability Ratios -->
	@include('financial.financial_analysis_en.profitability-ratios')

	<!-- include Turnover Ratios -->
	@include('financial.financial_analysis_en.turnover-ratios')

	<!-- include Conclusion section -->
	@include('financial.financial_analysis_en.conclusion')

<!--To set page number: Page X of Y  -->
<script type="text/php">
if ( isset($pdf) ) { 
    $pdf->page_script('
        if ($PAGE_COUNT > 1) {
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $size = 8;
            $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
            /*$y = 800;
            $x = 520;*/
            $width = $fontMetrics->get_text_width($pageText, $font, $size) / 2;
            $x = (($pdf->get_width() - $width) / 2) - 12;
        	$y = $pdf->get_height() - 35;
            $pdf->text($x, $y, $pageText, $font, $size);
        } 
    ');
}
</script>

@stop