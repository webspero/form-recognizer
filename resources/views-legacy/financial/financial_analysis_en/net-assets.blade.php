    <h3 class="pdf_title">{{ trans('financial_analysis/net_assets.1_2_net_assets') }}</h3>
    <table style="page-break-inside: avoid !important; width: 100%">
    	
        <tr class="center">
            <td class = "labels" rowspan="3" style="width: 70px">{{ trans('financial_analysis/net_assets.indicator') }}</td>
            <td class = "labels"colspan="{{ count($balance_sheets)+2 }}">{{ trans('financial_analysis/net_assets.value') }}</td>
            <td class = "labels"colspan="2">{{ trans('financial_analysis/net_assets.change') }}</td>
        </tr>
        <tr class="center">
            <td class = "labels"colspan="2">{!! trans('financial_analysis/net_assets.in_php') !!}</td>
            <td class = "labels"colspan="{{ count($balance_sheets) }}">{{ trans('financial_analysis/net_assets.of_the_balance_total') }}</td>
            <td class = "labels"rowspan="2" style="width: 90px">{{ trans('financial_analysis/net_assets.php_col') }}</td>
            <td class = "labels"rowspan="2" style="width: 40px">{{ trans('financial_analysis/net_assets.col') }}</td>
        </tr>
        <tr class="center">
            <td class = "labels" style="width: 90px">{!! trans('financial_analysis/net_assets.at_the_beginning',['year' => ((int)$financial_report->year - @count($balance_sheets) + 1)]) !!}</td>
            <td class = "labels" style="width: 90px">{!! trans('financial_analysis/net_assets.at_the_end',['year' => ((int)$financial_report->year)]) !!}</td>
            @for($x=count($balance_sheets); $x > 0 ; $x--)
                <td class = "labels" style="width: 70px">12/31/{{ ($financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        
        
    </table>
    
    <table style="width: 100%" class="smallfont">

    	<tr class="center">
    		<td style="width: 70px">1</td>
    		<td style="width: 90px">2</td>
    		<td style="width: 90px">3</td>
            @for($indexCount = 4; $indexCount <= (count($balance_sheets)+3); $indexCount++)
                <td style="width: 70px"> {{ $indexCount }}</td>
            @endfor
            <td style="width: 90px">{{ $indexCount }}</td>
            @php 
            	$indexCount++;
            @endphp
            <td style="width: 40px">{{ $indexCount }}</td>
        </tr>
        
        <tbody>
            @php 
                // beginning of the period
                $b_intangible_assets = $balance_sheets[count($balance_sheets)-1]->Goodwill+$balance_sheets[count($balance_sheets)-1]->IntangibleAssetsOtherThanGoodwill;
                $b_net_tangible_assets = $balance_sheets[count($balance_sheets)-1]->Equity-$b_intangible_assets;
                $b_net_assets = $balance_sheets[count($balance_sheets)-1]->Equity;
                $b_issued_capital = $balance_sheets[count($balance_sheets)-1]->IssuedCapital;
                $b_difference = $b_net_assets-$b_issued_capital;

                // end of the period
                $e_intangible_assets = $balance_sheets[0]->Goodwill+$balance_sheets[0]->IntangibleAssetsOtherThanGoodwill;
                $e_net_tangible_assets = $balance_sheets[0]->Equity-$e_intangible_assets;
                $e_net_assets = $balance_sheets[0]->Equity;
                $e_issued_capital = $balance_sheets[0]->IssuedCapital;
                $e_difference = $e_net_assets-$e_issued_capital;

                // change column
                $net_tangible_assets_change = $e_net_tangible_assets - $b_net_tangible_assets;
                if($b_net_tangible_assets > 0) {
                    $net_tangible_assets_change_prec = ($net_tangible_assets_change/$b_net_tangible_assets)*100;
                    $net_tangible_assets_change_times = $e_net_tangible_assets/$b_net_tangible_assets;
                } else {
                    $net_tangible_assets_change_prec = 0;
                    $net_tangible_assets_change_times = 0;
                }
                $net_assets_change = $e_net_assets - $b_net_assets;
                if($b_net_assets > 0) {
                    $net_assets_change_prec = ($net_assets_change/$b_net_assets)*100;
                    $net_assets_change_times = $e_net_tangible_assets/$b_net_assets;
                } else {
                    $net_assets_change_prec = 0;
                    $net_assets_change_times = 0;
                }
                $issued_capital_change = $e_issued_capital - $b_issued_capital;
                if($b_issued_capital > 0) {
                    $issued_capital_change_prec = ($issued_capital_change/$b_issued_capital)*100;
                    $issued_capital_change_times = $e_issued_capital/$b_issued_capital;
                } else {
                    $issued_capital_change_prec = 0;
                    $issued_capital_change_times = 0;
                }
                $difference_change = $net_assets_change - $issued_capital_change;
                if($b_difference > 0) {
                    $difference_change_prec = ($difference_change/$b_difference)*100;
                    $difference_change_times = $e_difference/$b_difference;
                } else {
                    $difference_change_prec = 0;
                    $difference_change_times = 0;
                }

                if($e_issued_capital > 0) {
                    $net_worth_times = $e_net_assets/$e_issued_capital;
                } else {
                    $net_worth_times = 0;
                }
                if($e_issued_capital > 0) {
                    $net_worth_percentage = (($e_net_assets/$e_issued_capital)-1)*100;
                } else {
                    $net_worth_percentage = 0;
                }
                $latest_net_tangible_assets_change = $e_net_assets-$e_net_tangible_assets;
                @endphp
            
            <tr>
                <td>{!! wordwrap(trans('financial_analysis/net_assets.1_net_tangible_assets'),18,"<br/>") !!}</td>
                <td class="center"> {{ ($b_net_tangible_assets != 0) ? number_format($b_net_tangible_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_tangible_assets != 0) ? number_format($e_net_tangible_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_intangible_assets = $balance_sheets[$x]->Goodwill+$balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill;
                        $i_net_tangible_assets = $balance_sheets[$x]->Equity-$i_intangible_assets;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_tangible_assets_prec = ($i_net_tangible_assets/$i_assets)*100;
                        } else {
                            $i_net_tangible_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_tangible_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_tangible_assets_prec != 0) ? round($i_net_tangible_assets_prec,1) : '-' }}</td>
                @endfor
                    <td class="center nowrap {{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_tangible_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @elseif($net_tangible_assets_change > 0)
                        {{ '+'.number_format($net_tangible_assets_change) }}
                    @else
                        {{ $net_tangible_assets_change > 0 ? '+' : '' }}{{ number_format($net_tangible_assets_change) }}
                    @endif
                </td>
                @if($net_tangible_assets_change_prec > 200)
                    <td class="center green">
                        {{ $net_tangible_assets_change_times > 0 ? '+' : '' }}{{ round($net_tangible_assets_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">
                        @if($net_tangible_assets_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @elseif($net_tangible_assets_change_prec < 0.10)
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '-' }}{{ '<0.1' }}
                        @else
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '' }}{{ round($net_tangible_assets_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>{!! wordwrap(trans('financial_analysis/net_assets.2_net_assets'),18,"<br/>") !!}</td>
                <td class="center"> {{ ($b_net_assets != 0) ? number_format($b_net_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_assets != 0) ? number_format($e_net_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_net_assets = $balance_sheets[$x]->Equity;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_assets_prec = ($i_net_assets/$i_assets)*100;
                        } else {
                            $i_net_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_assets_prec != 0) ? round($i_net_assets_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $net_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $net_assets_change > 0 ? '+' : '' }}{{ number_format($net_assets_change) }}
                    @endif
                </td>
                @if($net_assets_change_prec > 200)
                    <td class="center green">
                        {{ $net_assets_change_times > 0 ? '+' : '' }}{{ round($net_assets_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $net_assets_change_prec > 0 ? 'green' : 'red' }}">
                        @if($net_assets_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @elseif($net_tangible_assets_change_prec < 0.10)
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '-' }}{{ '<0.1' }}
                        @else
                            {{ $net_tangible_assets_change_prec > 0 ? '+' : '' }}{{ round($net_assets_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>{!! wordwrap(trans('financial_analysis/net_assets.3_issued_share'),18,"<br/>") !!}</td>
                <td class="center"> {{ ($b_issued_capital != 0) ? number_format($b_issued_capital) : '-' }}</td>
                <td class="center"> {{ ($e_issued_capital != 0) ? number_format($e_issued_capital) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_issued_capital = $balance_sheets[$x]->IssuedCapital;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_issued_capital_prec = ($i_issued_capital/$i_assets)*100;
                        } else {
                            $i_issued_capital_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_issued_capital_prec > 0 ? 'green' : 'red' }}"> {{ ($i_issued_capital_prec != 0) ? round($i_issued_capital_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $issued_capital_change > 0 ? 'green' : 'red' }}">
                    @if($issued_capital_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $issued_capital_change > 0 ? '+' : '' }}{{ number_format($issued_capital_change) }}
                    @endif
                </td>
                @if($issued_capital_change_prec > 200)
                    <td class="center green">
                        {{ $issued_capital_change_times > 0 ? '+' : '' }}{{ round($issued_capital_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $issued_capital_change_prec > 0 ? 'green' : 'red' }}">
                        @if($issued_capital_change_prec == 0)
                            <span class="black">{{ '-' }}</span>
                        @elseif($issued_capital_change_prec < 0.10)
                            {{ $issued_capital_change_prec > 0 ? '+' : '-' }}{{ '<0.1' }}
                        @else
                            {{ $issued_capital_change_prec > 0 ? '+' : '' }}{{ round($issued_capital_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                <td>{!! wordwrap(trans('financial_analysis/net_assets.4_difference_between'),18,"<br/>") !!}</td>
                <td class="center {{ $b_difference > 0 ? 'green' : 'red' }}"> {{ ($b_difference != 0) ? number_format($b_difference) : '-' }}</td>
                <td class="center {{ $e_difference > 0 ? 'green' : 'red' }}"> {{ ($e_difference != 0) ? number_format($e_difference) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_assets = $balance_sheets[$x]->Assets;
                        $i_net_assets = $balance_sheets[$x]->Equity;
                        $i_issued_capital = $balance_sheets[$x]->IssuedCapital;
                        if($i_assets > 0) {
                            $int_net_assets_prec = ($i_net_assets/$i_assets)*100;
                            $int_issued_capital_prec = ($i_issued_capital/$i_assets)*100;
                            $i_difference_prec = $int_net_assets_prec - $int_issued_capital_prec; 
                        } else {
                            $i_difference_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_difference_prec > 0 ? 'green' : 'red' }}"> {{ ($i_difference_prec != 0) ? round($i_difference_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $difference_change > 0 ? 'green' : 'red' }}">
                    @if($difference_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $difference_change > 0 ? '+' : '' }}{{ number_format($difference_change) }}
                    @endif
                </td>
                @if($difference_change_prec > 200)
                    <td class="center green">
                        {{ $difference_change_times > 0 ? '+' : '' }}{{ round($difference_change_times, 1) }}<span class="black"> times</span>
                    </td>
                @else
                    <td class="center {{ $difference_change_prec > 0 ? 'green' : 'red' }}">
                        @if($difference_change_prec == 0)
                            @if($e_difference > 0 && 0 > $b_difference)
                                <span class="black">Up</span>
                            @else
                                <span class="black">{{ '-' }}</span>
                            @endif
                        @elseif($difference_change_prec < 0.10)
                            {{ $difference_change_prec > 0 ? '+' : '-' }}{{ '<0.1' }}
                        @else
                            {{ $difference_change_prec > 0 ? '+' : '' }}{{ round($difference_change_prec, 1) }}
                        @endif
                    </td>
                @endif
            </tr>
        </tbody>
    </table>
    <div class="justify">
        <p>
            @php

                $eNetClass = $e_net_tangible_assets > 0 ? 'green' : 'red';
                $bNetClass = $b_net_tangible_assets > 0 ? 'green' : 'red';
                $netTangibleAssetsChangeClass = $net_tangible_assets_change > 0 ? 'green' : 'red';
                $netTangibleAssetsChangeSign = $net_tangible_assets_change > 0 ? '+' : '';
                $netTangibleAssetsChange = number_format($net_tangible_assets_change);
                $netTangibleAssetsChangePrecClass = $net_tangible_assets_change_prec > 0 ? 'green' : 'red';
                $netTangibleAssetsChangePrec = $net_tangible_assets_change_prec <= 200 ? round($net_tangible_assets_change_prec, 1) : round($net_tangible_assets_change_times, 1);
                $netTangibleAssetsChangePrecSign = $net_tangible_assets_change_prec <= 200 ? "%" : " times";
                $eNetTangibleAssets = number_format($e_net_tangible_assets);
                $bNetTangibleAssets = number_format($b_net_tangible_assets);
                $latestNetTangibleAssetsChange = number_format($latest_net_tangible_assets_change);
                $latestNetTangibleAssetsChangeClass = $latest_net_tangible_assets_change > 0 ? 'green' : 'red';

                $arr = [
                    'eNetClass' => $eNetClass,
                    'bNetClass' => $bNetClass,
                    'netTangibleAssetsChange' => $netTangibleAssetsChange,
                    'eNetTangibleAssets' => $eNetTangibleAssets,
                    'bNetTangibleAssets' => $bNetTangibleAssets,
                    'netTangibleAssetsChangeClass' => $netTangibleAssetsChangeClass,
                    'netTangibleAssetsChangeSign' => $netTangibleAssetsChangeSign,
                    'netTangibleAssetsChange' => $netTangibleAssetsChange,
                    'netTangibleAssetsChangePrecClass' => $netTangibleAssetsChangePrecClass,
                    'netTangibleAssetsChangePrec' => $netTangibleAssetsChangePrec,
                    'netTangibleAssetsChangePrecSign' => $netTangibleAssetsChangePrecSign,
                    'latestNetTangibleAssetsChange' => $latestNetTangibleAssetsChange
                ];
                
                if($net_tangible_assets_change > 0):
                    if($net_tangible_assets_change_prec > 40):
                       
                        $netText = trans('financial_analysis/net_assets.netText1',$arr);

                    elseif($net_tangible_assets_change_prec > 15):
                        $netText = trans('financial_analysis/net_assets.netText2',$arr);

                    else:
                        
                        $netText = trans('financial_analysis/net_assets.netText3',$arr);

                        if($net_tangible_assets_change_prec != 0):

                            $netText .= trans('financial_analysis/net_assets.netText4',$arr);
                        
                        endif;
                    endif;
                else:
                    $netText = trans('financial_analysis/net_assets.netText5',$arr);

                    if($latest_net_tangible_assets_change != 0):
                        $netText .= trans('financial_analysis/net_assets.netText6',$arr);
                    endif;
                endif;

                echo $netText;
            @endphp

            @if($e_intangible_assets == 0)

                {{ trans('financial_analysis/net_assets.intangibleAssetsText',['financialReportTitle' => $financial_report->title]) }}
            @endif
        </p>
        <p>
            @php

                $netWorthTimesClass = $net_worth_times > 0 ? 'green' : 'red';
                $netWorthPercentageClass = $net_worth_percentage > 0 ? 'green' : 'red';
                $eDifferenceClass = $e_difference > 0 ? 'green' : 'red';

                $arr = [
                    'financialReportTitle' => $financial_report->title,
                    'netWorthTimesClass' => $netWorthTimesClass,
                    'year' => (int)$financial_report->year,
                    'netWorthTimes' => round($net_worth_times, 1),
                    'netWorthPercentage' => round($net_worth_percentage, 1),
                    'netWorthPercentageClass' => $netWorthPercentageClass,
                     'eDifferenceClass' => $eDifferenceClass,
                     'eDifference' => number_format(abs($e_difference))

                ];

                $netWorthText = '';
                if($e_difference > 0):
                    if($net_worth_percentage > 200):

                        $netWorthText =  trans('financial_analysis/net_assets.netWorthText1',$arr);
                    else:
                        
                        $netWorthText =  trans('financial_analysis/net_assets.netWorthText2',$arr);

                        if($net_worth_percentage != 0):
                         
                         $netWorthText .=  trans('financial_analysis/net_assets.netWorthText3',$arr);
                        endif;

                        $netWorthText .=  trans('financial_analysis/net_assets.netWorthText4',$arr);
                    endif;
                    
                    $netWorthText .=  trans('financial_analysis/net_assets.netWorthText5',$arr);
                else:
                    
                    $netWorthText .=  trans('financial_analysis/net_assets.netWorthText6',$arr);
                endif;

                echo $netWorthText;
            @endphp
        </p>
    </div>
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>

    @php
    	$issuedCapitalText = '';
        if($b_issued_capital == $e_issued_capital):
            $issuedCapitalText = trans('financial_analysis/net_assets.issuedCapitalText1');
        elseif($issued_capital_change_prec > 90):
            $issuedCapitalText = trans('financial_analysis/net_assets.issuedCapitalText2');
        elseif($issued_capital_change_prec > 45):
            $issuedCapitalText = trans('financial_analysis/net_assets.issuedCapitalText3');
        endif;

        echo '<p>'.$issuedCapitalText.'</p>';
       
    @endphp

