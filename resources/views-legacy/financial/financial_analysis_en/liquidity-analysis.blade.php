    <div class="text-center">
        <h3 class="pdf_title"><strong>{{ trans('financial_analysis/liquidity_analysis.1_4_liquidity_analysis') }}</strong></h3>
    </div>
    <p>
        @php
            $financialReportTitle = $financial_report->title ?? "N/A";
        @endphp

        {!! trans('financial_analysis/liquidity_analysis.one_of_the_most_widespread',['financial_report_title' => $financialReportTitle]) !!}</p>

    <table style="page-break-inside: avoid !important;" class="smallfont">
    	<tr>
            <td rowspan="2" class="center" style="width: 90px !important">{{ trans('financial_analysis/liquidity_analysis.liquidity_ratio') }}</td>
            <td colspan="{{ count($balance_sheets) }}" style="width: 160px !important" class="center">{{ trans('financial_analysis/liquidity_analysis.value') }}</td>
            <td rowspan="2" class="center" style="width: 70px !important">{!! trans('financial_analysis/liquidity_analysis.col_count',['col_count' => count($balance_sheets) + 1]) !!}</td>
            <td rowspan="2" class="center" style="width: 140px !important">{{ trans('financial_analysis/liquidity_analysis.description_of_the_ratio') }}</td>
        </tr>
        <tr>
        	@php
                $descW = 39;
        		$w = intval(160/(@count($balance_sheets) - 1));
        	@endphp
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center" style="width: {{ $w }}px !important">12/31/{{ $balance_sheets[$x]->Year}}</td>
            @endfor
        </tr>
        
    </table>

    <table class="smallfont">
    	<tbody>
        <tr>
        	<td class="center" style="width: 90px !important">1</td>
            @for($indexCount = 2; $indexCount <= (count($balance_sheets)+1); $indexCount++)
                <td class="center" style="width: {{ $w }}px !important"> {{ $indexCount }}</td>
            @endfor
            <td class="center" style="width: 70px !important"> {{ $indexCount }}</td>
            @php $indexCount++; @endphp
            <td class="center" style="width: 140px !important"> {{ $indexCount }}</td>
        </tr>
        
            <tr>
                <td>{!! wordwrap("1. Current ratio (working capital ratio)",23,"<br/>") !!}</td>
                <?php
                    $i=0;
                    $currentRatioPositiveValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                    $currentRatio = 0;

                    if($currentLiabilities > 0){
                    	$currentRatio = $currentAssets / $currentLiabilities;
                    	$currentRatio = round($currentRatio, 2, PHP_ROUND_HALF_DOWN);
                	}

                    // First and Last value to find change value.
                    if ($i == 0) {
                        $firstCurrentRatio = $currentRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCurrentRatio = $currentRatio;
                        $currentRatioChangeValue = $lastCurrentRatio - $firstCurrentRatio;
                        $currentRatioChangeColor = 'green';
                        if($currentRatioChangeValue < 0)
                        {
                            $currentRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $currentRatioColor = 'red';
                    if($currentRatio >= 2)
                    {
                        array_push($currentRatioPositiveValues, $currentRatio);
                        $currentRatioColor = 'green';
                    }
                ?>
                <td class="center {{$currentRatioColor}}"> {{ number_format($currentRatio, 2) }}</td>
                @endfor
                @if($currentRatioChangeValue < 0)
                    <td class="center {{ $currentRatioChangeColor }}">{{ number_format($currentRatioChangeValue, 2) }} </td>
                @else
                    <td class="center {{ $currentRatioChangeColor }}">+{{ number_format($currentRatioChangeValue, 2) }} </td>
                @endif

                @php
	                $arr = [
			            'lastCurrentRatio' => $lastCurrentRatio,
			            'year' => (int)$financial_report->year
			        ];
			    @endphp
                <td>{!! wordwrap(trans('financial_analysis/liquidity_analysis.the_current_ratio',$arr),$descW,"<br/>") !!}</td>
            </tr>
            <tr>
                <td>{!! wordwrap('2. Quick ratio',23,"<br/>") !!}</td>
                <?php
                    $i=0;
                    $quickRatioPositiveValues=[];
                    $quickRatioValues=[];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Quick Ratio,  ∞ < crit. < 0.5 ≤ unsat. < 1 ≤ good < 1.1 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $otherCurrentFinancialAssets = $balance_sheets[$x]->OtherCurrentFinancialAssets;
                    $tradeAndOtherCurrentReceivables = $balance_sheets[$x]->TradeAndOtherCurrentReceivables;

                    $quickRatio = 0;
                    if($currentLiabilities > 0){
                    	$quickRatio = ($cashAndCashEquivalents + $otherCurrentFinancialAssets + $tradeAndOtherCurrentReceivables) / $currentLiabilities;
                    	$quickRatio = round($quickRatio, 2, PHP_ROUND_HALF_DOWN);
                	}

                    if ($i == 0) {
                        $firstQuickRatio = $quickRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastQuickRatio = $quickRatio;
                        $quickRatioChangeValue = $lastQuickRatio - $firstQuickRatio;
                        $quickRatioChangeColor = 'green';
                        if($quickRatioChangeValue < 0)
                        {
                            $quickRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $quickRatioColor = 'red';
                    if($quickRatio >= 1)
                    {
                        $quickRatioColor = 'green';
                        array_push($quickRatioPositiveValues, $quickRatio);
                    }
                    array_push($quickRatioValues, $quickRatio);
                ?>
                <td class="center {{ $quickRatioColor }}"> {{ number_format($quickRatio, 2) }}</td>
                @endfor
                @if($quickRatioChangeValue < 0)
                    <td class="center {{ $quickRatioChangeColor }}">{{ number_format($quickRatioChangeValue, 2) }} </td>
                @else
                    <td class="center {{ $quickRatioChangeColor }}">+{{ number_format($quickRatioChangeValue, 2) }} </td>
                @endif
                <td>{!! wordwrap(trans('financial_analysis/liquidity_analysis.the_quick_ratio'),$descW,"<br/>") !!}</td>
            </tr>
            <tr>
                <td>{!! wordwrap('3. Cash ratio',23,"<br/>") !!}</td>
                <?php
                    $i=0;
                    $cashRatioPositiveValues = [];
                ?>
                @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <?php
                    $currentAssets = $balance_sheets[$x]->CurrentAssets;
                    $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

                    // Cash Ratio, ∞ < crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∞
                    $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;
                    $cashRatio = 0;
                    if($currentLiabilities > 0){
                    	$cashRatio = $cashAndCashEquivalents / $currentLiabilities;
                    	$cashRatio = round($cashRatio, 2, PHP_ROUND_HALF_DOWN);
                	}

                    if ($i == 0) {
                        $firstCashRatio = $cashRatio;
                    }
                    else if ($i == count($balance_sheets) - 1) {
                        $lastCashRatio = $cashRatio;
                        $cashRatioChangeValue = $lastCashRatio - $firstCashRatio;
                        $cashRatioChangeColor = 'green';
                        if($cashRatioChangeValue < 0)
                        {
                            $cashRatioChangeColor = 'red';
                        }
                    }
                    $i++;
                    $cashRatioColor = 'red';
                    if($cashRatio >= 0.2)
                    {
                        $cashRatioColor = 'green';
                        array_push($cashRatioPositiveValues, $cashRatio);
                    }
                ?>
                <td class="center {{ $cashRatioColor }}">
                    @if($cashRatio == 0)
                        {{ '<0.01' }}
                    @else 
                        {{ number_format($cashRatio, 2) }}
                    @endif
                </td>
                @endfor
                @if($cashRatioChangeValue < 0)
                    <td class="center {{ $cashRatioChangeColor }}">{{ number_format($cashRatioChangeValue, 2) }} </td>
                @else
                    <td class="center {{ $cashRatioChangeColor }}">+{{ number_format($cashRatioChangeValue, 2) }} </td>
                @endif
                <td>{!! wordwrap('Cash ratio is calculated by dividing absolute liquid assets (cash and cash equivalents) by current liabilities. Normal value: 0.2 or more.',$descW,"<br/>") !!}</td>
            </tr>
        </tbody>
    </table>
    @php
        $currentRatioText = '';
        $arr = [
            'currentRatioChangeColor' =>$currentRatioChangeColor,
            'currentRatioChangeValue' => number_format(str_replace("-", " ", $currentRatioChangeValue), 2),
            'year' => (int)$financial_report->year
        ];
        if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue<0) )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText1',$arr);
        }
        else if( (count($currentRatioPositiveValues)<=0) && ($currentRatioChangeValue>0) )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText2',$arr);
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue>0) )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText3',$arr);
        }
        else if( (count($currentRatioPositiveValues)===count($balance_sheets)) && ($currentRatioChangeValue<0) )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText4',$arr);
        }
        else if( ($firstCurrentRatio >= 2) && ($lastCurrentRatio < 2) && ($currentRatioChangeValue > 0)  )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText5',$arr);
        }
        else if( ($firstCurrentRatio < 2) && ($lastCurrentRatio >= 2) && ($currentRatioChangeValue > 0)  )
        {
            trans('financial_analysis/liquidity_analysis.currentRatioText6',$arr);
        }
        else
        {
            if($lastCurrentRatio >= 2 && $currentRatioChangeValue > 0)
            {
                trans('financial_analysis/liquidity_analysis.currentRatioText7',$arr);
            }
            else if($lastCurrentRatio >= 2 && $currentRatioChangeValue < 0)
            {
               trans('financial_analysis/liquidity_analysis.currentRatioText8',$arr);
            }
            else
            {
               trans('financial_analysis/liquidity_analysis.currentRatioText9',$arr);
            }
        }

        $arr = [
            'year' => (int)$financial_report->year,
            'currentRatioColor' => $currentRatioColor,
            'lastCurrentRatio' => $lastCurrentRatio
        ];

    @endphp
  

    <?php
        $maxColor = 'red';
        $minColor = 'red';
        if(max($quickRatioValues) >= 1)
        {
            $maxColor='green';
        }
        if(min($quickRatioValues) >= 1)
        {
            $minColor='green';
        }
        // Quick Ratio Text
        $quickRatioText = '';

        $arr = [
            'quickRatioChangeColor' => $quickRatioChangeColor,
            'quickRatioChangeValue' => number_format(str_replace("-", " ", $quickRatioChangeValue), 2),
            'maxColor' => $maxColor,
            'maxQuickRatioValues' => number_format(max($quickRatioValues), 2),
            'minQuickRatioValues' => number_format(min($quickRatioValues), 2),
            'financialReportTitle' => $financial_report->title,
            'firstQuickRatio' => number_format($firstQuickRatio, 2),
            'year' => (int)$financial_report->year

        ];
        if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue<0) )
        {
            trans('financial_analysis/liquidity_analysis.quickRatioText1',$arr);
        }
        else if( (count($quickRatioPositiveValues)<=0) && ($quickRatioChangeValue>0) )
        {
            trans('financial_analysis/liquidity_analysis.quickRatioText2',$arr);
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue>0) )
        {
            trans('financial_analysis/liquidity_analysis.quickRatioText3',$arr);
        }
        else if( (count($quickRatioPositiveValues)===count($balance_sheets)) && ($quickRatioChangeValue<0) )
        {
            trans('financial_analysis/liquidity_analysis.quickRatioText4',$arr);
        }
        else if( ($firstQuickRatio >= 1) && ($lastQuickRatio < 1) && ($quickRatioChangeValue > 0)  )
        {
           trans('financial_analysis/liquidity_analysis.quickRatioText5',$arr);
        }
        else if( ($firstQuickRatio < 1) && ($lastQuickRatio >= 1) && ($quickRatioChangeValue > 0)  )
        {
           trans('financial_analysis/liquidity_analysis.quickRatioText6',$arr);
        }
        else
        {
            if($lastQuickRatio >= 1 && $quickRatioChangeValue > 0)
            {
                trans('financial_analysis/liquidity_analysis.quickRatioText7',$arr);
            }
            else if($lastQuickRatio < 1 && $quickRatioChangeValue < 0)
            {
                trans('financial_analysis/liquidity_analysis.quickRatioText8',$arr);
            }
            else
            {
                trans('financial_analysis/liquidity_analysis.quickRatioText9',$arr);
            }
        }
    ?>

    <?php
        // cash Ratio Text
        $cashRatioText = '';
        $arr = [
            'cashRatioColor' => $cashRatioColor,
            'lastCashRatio' => $lastCashRatio,
            'financialReportTitle' => $financial_report->title,
            'year' => $financial_report->year,
            'countBalanceSheet' => count($balance_sheets)
        ];

        if($lastCurrentRatio >=2 && $lastQuickRatio >=1 && $lastCashRatio >= 0.2)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText1',$arr);
        }
        else if($lastCurrentRatio < 2 && $lastQuickRatio < 1 && $lastCashRatio < 0.2)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText2',$arr);
        }
        else if($lastCurrentRatio < 2 && $lastQuickRatio < 1 && $lastCashRatio >= 0.2)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText3',$arr);
        }
        else if($lastCurrentRatio >= 1 && $lastQuickRatio < 1 && $lastCashRatio < 0.2 && $cashRatioChangeValue < 0)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText4',$arr);
        }
        else if($lastCurrentRatio >= 1 && $lastQuickRatio < 1 && $lastCashRatio >= 0.2 && $cashRatioChangeValue < 0)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText5',$arr);
        }
        else if($lastQuickRatio >=1 && $lastCashRatio >= 0.2)
        {
            trans('financial_analysis/liquidity_analysis.cashRatioText2',$arr);
        }
        else
        {
            $cashRatioText = '';
        }
        if(!empty($cashRatioText))
        {
            $cashRatioText = '<p>'.$cashRatioText.'</p>';
        }

    ?>

    {!! $cashRatioText !!}
    <!-- Liquidity Graph -->
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_4_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>

    @php
        $financialReportTitle = $financial_report->title ?? "N/A";
    @endphp
    @if($lastCurrentRatio > 2 && $lastQuickRatio >= 1 && $lastCashRatio >= 0.2)
        <p>{!! trans('financial_analysis/liquidity_analysis.in_summary_all_three',['financialReportTitle' => $financialReportTitle]) !!}
    @endif
<!-- End: Liquidity Analysis 