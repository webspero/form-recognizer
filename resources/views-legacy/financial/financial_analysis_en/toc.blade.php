<div class="toc">
	<ul>
		<li>{{ trans('financial_analysis/toc.1_financial_position_analysis',['financial_report_title' => $financial_report->title]) }}
			<ul>
				<li>{{ trans('financial_analysis/toc.1_1_structure_of_the_assets') }}</li>
				<li>{{ trans('financial_analysis/toc.1_2_net_assets') }}</li>
				<li>{{ trans('financial_analysis/toc.1_3_financial_sustainability') }}
					<ul>
						<li>{{ trans('financial_analysis/toc.1_3_1_key_ratios') }}</li>
						<li>{{ trans('financial_analysis/toc.1_3_2_working_capital') }}</li>
					</ul>
				<li>1.4. Liquidity Analysis</li>
			</ul>
		</li>
		<li>{{ trans('financial_analysis/toc.2_financial_performance') }}
			<ul>
				<li>{{ trans('financial_analysis/toc.2_1_overview_of_the_financial') }}</li>
				<li>{{ trans('financial_analysis/toc.2_2_profitability_ratios') }}</li>
				<li>{{ trans('financial_analysis/toc.2_3_analysis') }}</li>
			</ul>
		</li>
		<li>{{ trans('financial_analysis/toc.3_conclusion') }}
			<ul>
				<li>{{ trans('financial_analysis/toc.3_1_key_ratios_summary') }}</li>
			</ul>
		</li>
	</ul>

</div>