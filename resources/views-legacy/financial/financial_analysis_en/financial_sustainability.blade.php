<h3 class="pdf_title"> {{ trans('financial_analysis/financial_sustainability.1_3_financial_sustainability') }}</h3>
<h3>{{ trans('financial_analysis/financial_sustainability.1_3_1_key_ratios') }}</h3>
    
<table style="page-break-inside: avoid !important;" class="smallfont">
	<tr>
        <td rowspan="2" style="text-align:center;width: 90px">{{ trans('financial_analysis/financial_sustainability.ratio') }}</td>
        <td colspan="{{count($financial_table)}}" style="text-align:center;">{{ trans('financial_analysis/financial_sustainability.value') }}</td>
        <td rowspan="2" style="text-align:center;width: 90px">{!! trans('financial_analysis/financial_sustainability.change',['col0' => count($financial_table)+ 1]) !!}</td>
        <td rowspan="2" style="text-align:center;width: 150px">{{ trans('financial_analysis/financial_sustainability.description') }}</td>
    </tr>
    <tr>
    	@php
			$descW = 35;
			$w = intval(250/count($financial_table));
		@endphp
    	@foreach($financial_table as $val)
            <td style="text-align:center;width: {{ $w }}px">12/31/{{$val->year}}</td>
        @endforeach
    </tr>

</table>
	
	
    <table class="smallfont">
        
        <tr>
        	<td style="text-align:center;width: 90px">1</td>
            @for ($i = 2; $i <= count($financial_table) + 1; $i++)
                <td style="text-align:center;width: {{ $w }}px">{{$i}}</td>
            @endfor
            <td style="text-align:center;width: 90px">{{$i}}</td>
            @php $i++; @endphp
            <td style="text-align:center;width: 150px">{{$i}}</td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.debt_to_equity_ratio'),17,"<br/>") !!}</td>
            <?php
                $i=0;
                $debtToEquityFinancialPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->FinancialLeverage <= 1.5)
                    <?php
                        array_push($debtToEquityFinancialPositiveValues, $val->FinancialLeverage);
                    ?>
                    <td style="text-align:center; color:green;">{{$val->FinancialLeverage}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FinancialLeverage}}</td>
                @endif

                <?php
                    if ($i == 0) {
                        $firstDebtToEquityFinancial = $val->FinancialLeverage;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityFinancial = $val->FinancialLeverage;
                        $lastDebtToEquityFinancialColor = 'red';
                        if($lastDebtToEquityFinancial <= 1.5)
                        {
                            $lastDebtToEquityFinancialColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] == 0)
                    <td style="text-align:center;">-</td>
                    <?php $debtToEquityFinancialChangeColor = 'black'; ?>
            @elseif($financial_change['FinancialLeverage_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'red'; ?>
            @endif
            
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.debt_to_equity_ratio_calculated'),$descW,"<br/>") !!}</td>
        </tr>
        <tr>
            <?php
                $i=0;
                $debtRatioPositiveValues=[];
            ?>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.debt_to_assets_ratio'),17,"<br/>") !!}</td>
            @foreach($financial_table as $val)
                @if($val->DebtRatio <= 0.6)
                    <td style="text-align:center; color:green;">{{$val->DebtRatio}}</td>
                    <?php array_push($debtRatioPositiveValues, $val->DebtRatio);?>
                @else
                    <td style="text-align:center; color:red;">{{$val->DebtRatio}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstDebtToEquityAsset = $val->DebtRatio;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityAsset = $val->DebtRatio;
                        $lastDebtToEquityAssetColor = 'red';
                        if($lastDebtToEquityAsset <= 0.6)
                        {
                            $lastDebtToEquityAssetColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['DebtRatio_change'] == 0)
                    <td style="text-align:center;">-</td>
                <?php $debtToEquityAssetChangeColor = 'black'; ?>
            @elseif($financial_change['DebtRatio_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'red'; ?>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.debt_to_equity_ratio_normal'),$descW,"<br/>") !!}</td>

        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.long_term_debt'),17,"<br/>") !!}</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->LTDtoE}}</td>
            @endforeach
            @if($financial_change['LTDtoE_change'] == 0)
                <td style="text-align: center;">-</td>
            @elseif($financial_change['LTDtoE_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['LTDtoE_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['LTDtoE_change']}}</td>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.this_ratio_is_calculated'),$descW,"<br/>") !!}</td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.non_current_assets_net_worth'),17,"<br/>") !!}</td>
            <?php
                $i=0;
                $NCAtoNWPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->NCAtoNW <= 1.25)
                    <td style="text-align:center; color:green;">{{$val->NCAtoNW}}</td>
                    <?php array_push($NCAtoNWPositiveValues, $val->NCAtoNW); ?>
                @else
                    <td style="text-align:center; color:red;">{{$val->NCAtoNW}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstNCAtoNW = $val->NCAtoNW;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastNCAtoNW = $val->NCAtoNW;
                        $lastNCAtoNWColor = 'red';
                        if($lastNCAtoNW <= 1.25)
                        {
                            $lastNCAtoNWColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['NCAtoNW_change'] == 0)
                <td style="text-align:center;">-</td>
                <?php $NCAtoNWChangeColor = 'black'; ?>
            @elseif($financial_change['NCAtoNW_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'red'; ?>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.this_ratio_is_calculated2'),$descW,"<br/>") !!}</td>
        </tr>
        <tr>
            <td>{!! wordwrap('Capitalization ratio',17,"<br/>") !!}</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->Capitalization}}</td>
            @endforeach
            @if($financial_change['Capitalization_change'] == 0)
                <td style="text-align:center;">-</td>
            @elseif($financial_change['Capitalization_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['Capitalization_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['Capitalization_change']}}</td>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.calculated_by_dividing'),$descW,"<br/>") !!}</td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.fixed_assets_to_net_worth'),17,"<br/>") !!}</td>
            @foreach($financial_table as $val)
                @if($val->FAtoNW <= 0.75)
                    <td style="text-align:center; color:green;">{{$val->FAtoNW}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FAtoNW}}</td>
                @endif
            @endforeach
            @if($financial_change['FAtoNW_change'] == 0)
                <td style="text-align: center;">-</td>
            @elseif($financial_change['FAtoNW_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FAtoNW_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FAtoNW_change']}}</td>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.this_ratio_indicates'),$descW,"<br/>") !!}</td>
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.current_liability_ratio'),17,"<br/>") !!}</td>
            @foreach($financial_table as $val) 
                <td style="text-align:center;">{{$val->CLiabilityRatio}}</td>
            @endforeach
            @if($financial_change['CLiabilityRatio_change'] == 0)
                <td style="text-align: center;">-</td>
            @elseif($financial_change['CLiabilityRatio_change'] > 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['CLiabilityRatio_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['CLiabilityRatio_change']}}</td>
            @endif
            <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.current_liability_ratio_calculated'),$descW,"<br/>") !!}</td>
        </tr>

    </table>
    <div>
        <p>{{ trans('financial_analysis/financial_sustainability.first_attention_should') }}<p>

        <?php
            // Debt to equality and asset text, no more than 1.5
            $debtRatioToEquityText = '';

            $arr = [
                    'year1' => $financial_table[0]->year,
                    'year2' => $financial_table[count($financial_table)-1]->year,
                    'lastDebtToEquityFinancialColor' => $lastDebtToEquityFinancialColor,
                    'lastDebtToEquityFinancial' => $lastDebtToEquityFinancial,
                    'lastDebtToEquityAssetColor' => $lastDebtToEquityAssetColor,
                    'lastDebtToEquityAsset' => $lastDebtToEquityAsset,
                    'debtToEquityAssetChangeColor' => $debtToEquityAssetChangeColor,
                    'debtRatioChange' => str_replace("-", " ", $financial_change['DebtRatio_change']),
                    'yearCount' => count($balance_sheets)

                ];

            if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                
                // All negative including change value
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText1',$arr);
            }
            else if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText2',$arr);
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText3',$arr);
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText4',$arr);
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial > 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText5',$arr);
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText6',$arr);
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText7',$arr);
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtToEquityFinancialPositiveValues)!=count($balance_sheets)) )
            {
                $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText8',$arr);
            }
            else
            {
                if($lastDebtToEquityFinancial <= 1.5 && $financial_change['DebtRatio_change'] >= 0 )
                {
                    $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText9',$arr);
                }
                else
                {
                    $debtRatioToEquityText = trans('financial_analysis/financial_sustainability.debtRatioToEquityText10',$arr);
                }
            }
        ?>
        
        <p>{!! $debtRatioToEquityText !!}</p>

        <?php

            $arr = [
                'financial_report_title' => $financial_report->title,
                'year2' => $financial_table[count($financial_table)-1]->year,
                'debtRatioPercentage' => $financial_table[count($financial_table)-1]->debtRatioPercentage
            ];

            // Debt to equality and asset text
            $debtRatioText = '';
            if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                // All negative including change value
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText1',$arr);
            }
            else if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All negative, change plus
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText2',$arr);
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All positive, change plus
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText3',$arr);
                
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                // All positive, change minus
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText4',$arr);
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset > 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText5',$arr);
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
              
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText6',$arr);
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText7',$arr);
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtRatioPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change plus, and in middle minus
                
                $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText8',$arr);
            }
            else
            {
                if($lastDebtToEquityAsset <= 0.6)
                {
                   $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText9',$arr);
                }
                else
                {
                    $debtRatioText = trans('financial_analysis/financial_sustainability.debtRatioText10',$arr);
                }
            }
        ?>

        <p>{!! $debtRatioText !!}</p>
        <p>{{ trans('financial_analysis/financial_sustainability.chart_below') }}</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>

        @php
            // Non-current assets to Net worth text
            $NCAtoNWText = '';
            $arr = [
                'year1' => $financial_table[0]->year,
                'year2' => $financial_table[count($financial_table)-1]->year,
                'lastNCAtoNWColor' => $lastNCAtoNWColor,
                'lastNCAtoNW' => $lastNCAtoNW,
                'firstNCAtoNW' => $firstNCAtoNW,
                'NCAtoNWChangeColor' => $NCAtoNWChangeColor,
                'NCAtoNW_change' => str_replace("-", " ", $financial_change['NCAtoNW_change'])
            ];

            if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] < 0))
            {
                // All negative including change value
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText1',$arr);
            }
            else if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText2',$arr);
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                // All positive including change
                if($lastNCAtoNW > $firstNCAtoNW)
                {
                    $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText3',$arr);
                }
                else
                {
                    $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText4',$arr);
                }
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] < 0) )
            {
                // All positive, except for change
                
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText5',$arr);
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW > 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first plus-end minus: change plus
               
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText6',$arr);
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText7',$arr);
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] < 0)  )
            {
                // first minus-end plus: change minus
                
                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText8',$arr);
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0) && (count($NCAtoNWPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change minus, but in middle minus
                

                $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText9',$arr);
            }
            else
            {
                if($lastNCAtoNW <= 1.25 && $financial_change['NCAtoNW_change'] >= 0 )
                {
                    
                    $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText10',$arr);
                }
                else
                {
                    $NCAtoNWText = trans('financial_analysis/financial_sustainability.NCAtoNWText11',$arr);
                }
            }
        @endphp

        <p>{!! $NCAtoNWText !!}</p>
        @php
            $nonCurrentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->nonCurrentLiabilitiesPercentage;
            $currentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->currentLiabilitiesPercentage;

            $liabilityText = '';
            $arr = [
                'financial_report_title' => $financial_report->title,
                'year2' => $financial_table[count($financial_table)-1]->year,
                'currentLiabilitiesPercentage' => $currentLiabilitiesPercentage,
                'nonCurrentLiabilitiesPercentage' => $nonCurrentLiabilitiesPercentage
            ];

            if($nonCurrentLiabilitiesPercentage<=0)
            {
                $liabilityText = trans('financial_analysis/financial_sustainability.liabilityText1',$arr);
            }
            else if( ($nonCurrentLiabilitiesPercentage>40) && ($currentLiabilitiesPercentage>40) ){
                
                $liabilityText = trans('financial_analysis/financial_sustainability.liabilityText2',$arr);
            }
            else if($nonCurrentLiabilitiesPercentage > $currentLiabilitiesPercentage)
            {
               
                $liabilityText = trans('financial_analysis/financial_sustainability.liabilityText3',$arr);
            }
            else
            {
                // Current liabilities (short-term liabilities)
                
                $liabilityText = trans('financial_analysis/financial_sustainability.liabilityText4',$arr);
            }
        @endphp
        <p>{!! $liabilityText !!}</p>
        <p>{!! trans('financial_analysis/financial_sustainability.the_following_chart',['financial_report_title' => $financial_report->title]) !!}</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>
    </div>
<h3>{{ trans('financial_analysis/financial_sustainability.1_3_2_working_capital_analysis') }}</h3>

@php
	$w = intval(450/(count($financial_table) + 3));
@endphp
<table style="page-break-inside: avoid !important;" class="smallfont">
	<tr>
        <td rowspan="2" style="text-align:center;width:120px">{{ trans('financial_analysis/financial_sustainability.indicator') }}</td>
        <td colspan="{{count($financial_table)}}" style="text-align:center;">{{ trans('financial_analysis/financial_sustainability.value') }}</td>
        <td rowspa="2" colspan="2" style="text-align:center;">{{ trans('financial_analysis/financial_sustainability.change_for_the_period') }}</td>
    </tr>
    <tr>
        @foreach($financial_table as $val)
            <td style="text-align:center;width: {{ $w }}">12/31/{{$val->year}}</td>
        @endforeach
        <td style="text-align:center;width: {{ $w }}">(Col.{{count($financial_table)+1}}-col.2)</td>
        <td style="text-align:center;width: {{ $w }}">((Col.{{count($financial_table)+1}}-col.2) : col.2)</td>
    </tr>	
</table>
<table class="smallfont">
    <tr>
    	<td style="text-align:center;width: 120px">1</td>
        @for ($i = 2; $i <= count($financial_table); $i++)
            <td style="text-align:center;width: {{ $w }}">{{$i}}</td>
        @endfor
        <td style="text-align:center;width: {{ $w }}">{{$i}}</td>
        @php $i++; @endphp
        <td style="text-align:center;width: {{ $w }}">{{$i}}</td>
        @php $i++; @endphp
      	<td style="text-align:center;width: {{ $w }}">{{$i}}</td>

    </tr>
    <tr>
        <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.1_working_capital'),23,"<br/>") !!}</td>
        @foreach($financial_table as $val)
            @if($val->NWC == 0)
                <td style="text-align:center;">-</td>
            @elseif($val->NWC > 0)
                <td style="text-align:center; color:green;">+{{number_format($val->NWC, 0, '.', ',')}}</td>
            @else
                <td style="text-align:center; color:red;">{{number_format($val->NWC, 0, '.', ',')}}</td>
            @endif
        @endforeach
        <td style="text-align:center;">
            @if(($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC) == 0)
                <span style="text-align:center;">-</span>
            @elseif(($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC) > 0)
                <span style="color:green;">+{{number_format($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC, 0, '.', ',')}}</span>
            @else
                <span style="color:red;">{{number_format($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC, 0, '.', ',')}}</span>
            @endif
        </td>
        <td style="text-align:center;">
            @php
                if($financial_table[count($financial_table)-1]->NWC != 0 && $financial_table[0]->NWC != 0){
                    $NWC_change_perc = (($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC)  / $financial_table[0]->NWC)*100;
                    $NWC_times = $financial_table[count($financial_table)-1]->NWC / $financial_table[0]->NWC;
                }else{
                    $NWC_change_perc = 0;
                    $NWC_times = 0;
                }
            @endphp
            @if($NWC_change_perc > 200)
                <span style="color:green;">+{{number_format($NWC_times,1,'.',',') }}times</span>
            @else
                @if($NWC_change_perc == 0)
                    -
                @else   
                    @if($NWC_change_perc > 0)
                        <span style="color:green;">{{number_format($NWC_change_perc,1,'.',',') }}</span>
                    @else
                        <span style="color:red;">{{number_format($NWC_change_perc,1,'.',',') }}</span>
                    @endif
                @endif
            @endif
        </td>
    </tr>
    <tr>
        <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.2_inventories_php'),23,"<br/>") !!}</td>
        @foreach($financial_table as $val)
            @if($val->Inventories == 0)
                <td style="text-align:center;">-</td>
            @elseif($val->Inventories > 0 )
                <td style="text-align:center; color:green;">+{{number_format($val->Inventories, 0, '.', ',')}} </td>
            @else
                <td style="text-align:center; color:red;">{{number_format($val->Inventories, 0, '.', ',')}}</td>
            @endif
        @endforeach
        <td style="text-align:center;">
            @if(($financial_table[count($financial_table)-1]->Inventories - $financial_table[0]->Inventories) == 0)
                <span>-</span>
            @elseif(($financial_table[count($financial_table)-1]->Inventories - $financial_table[0]->Inventories) > 0)
                <span style="color:green;">+{{number_format($financial_table[count($financial_table)-1]->Inventories - $financial_table[0]->Inventories, 0, '.', ',')}}</span>
            @else
                <span style="color:red;">{{number_format($financial_table[count($financial_table)-1]->Inventories - $financial_table[0]->Inventories, 0, '.', ',')}}</span>
            @endif
        </td>
        <td style="text-align:center;">
            <?php
                if($financial_table[count($financial_table)-1]->Inventories != 0 && $financial_table[0]->Inventories != 0){
                    $Inventories_change_perc = (($financial_table[count($financial_table)-1]->Inventories - $financial_table[count($financial_table)-1]->Inventories) / $financial_table[0]->Inventories)*100;
                    $Inventories_times = $financial_table[count($financial_table)-1]->Inventories / $financial_table[0]->Inventories;
                }else{
                    $Inventories_change_perc = 0;
                    $Inventories_times = 0;
                }
            ?>
            @if($Inventories_change_perc > 200)
                <span style="color:green;">+{{number_format($Inventories_times,1,'.',',') }} times</span>
            @else
                @if($Inventories_change_perc == 0)
                    -
                @else   
                    @if($Inventories_change_perc > 0)
                        <span style="color:green;">{{number_format($Inventories_change_perc,1,'.',',') }}</span>
                    @else
                        <span style="color:red;">{{number_format($Inventories_change_perc,1,'.',',') }}</span>
                    @endif
                @endif
            @endif
        </td>
    </tr>
    <tr>
        <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.3_working_capital_sufficiency'),23,"<br/>") !!}</td>
        @foreach($financial_table as $val)
            @if($val->WCD == 0)
                <td style="text-align:center;">-</td>
            @elseif($val->WCD > 0)
                <td style="text-align:center; color:green;">+{{number_format($val->WCD, 0, '.', ',')}}</td>
            @else
                <td style="text-align:center; color:red;">{{number_format($val->WCD, 0, '.', ',')}}</td>
            @endif
        @endforeach
        <td style="text-align:center;">
            @if(($financial_table[count($financial_table)-1]->WCD - $financial_table[0]->WCD) == 0)
                <span style="text-align:center;">-</span>
            @elseif(($financial_table[count($financial_table)-1]->WCD - $financial_table[0]->WCD) > 0)
                <span style="color:green;">+{{number_format($financial_table[count($financial_table)-1]->WCD - $financial_table[0]->WCD, 0, '.', ',')}}</span>
            @else
                <span style="color:red;">{{number_format($financial_table[count($financial_table)-1]->WCD - $financial_table[0]->WCD, 0, '.', ',')}}</span>
            @endif
        </td>
        <td style="text-align:center;">
            @php
                if($financial_table[count($financial_table)-1]->WCD  != 0 && $financial_table[0]->WCD != 0){
                    $WCD_change_perc = (($financial_table[count($financial_table)-1]->WCD - $financial_table[0]->WCD) / $financial_table[0]->WCD)*100;
                    $WCD_times = $financial_table[count($financial_table)-1]->WCD / $financial_table[0]->WCD;
                }else{
                    $WCD_change_perc = 0;
                    $WCD_times = 0;
                }
            @endphp
            @if($WCD_change_perc > 200)
                <span style="color:green;">+{{number_format($WCD_times,1,'.',',') }} times</span>
            @else
                @if($WCD_change_perc == 0)
                    -
                @else   
                    @if($WCD_change_perc > 0)
                        <span style="color:green;">{{number_format($WCD_change_perc,1,'.',',') }}</span>
                    @else
                        <span style="color:red;">{{number_format($WCD_change_perc,1,'.',',') }}</span>
                    @endif
                @endif
            @endif
        </td>
    </tr>
    <tr>
        <td>{!! wordwrap(trans('financial_analysis/financial_sustainability.4_inventory_to_working'),23,"<br/>") !!}</td>
        @foreach($financial_table as $val)
            @if($val->InventoryNWC == 0)
                <td style="text-align:center;">-</td>
            @elseif($val->InventoryNWC > 0 && $val->InventoryNWC < 1)
                <td style="text-align:center; color:green;">{{number_format($val->InventoryNWC, 2, '.', ',')}}</td>
            @else
                <td style="text-align:center; color:red;">{{number_format($val->InventoryNWC, 2, '.', ',')}}</td>
            @endif
        @endforeach
        <td style="text-align:center;">
            @if(($financial_table[count($financial_table)-1]->InventoryNWC - $financial_table[0]->InventoryNWC) == 0)
                <span style="text-align:center;">-</span>
            @elseif(($financial_table[count($financial_table)-1]->InventoryNWC - $financial_table[0]->InventoryNWC) > 0)
                <span style="color:green;">+{{number_format($financial_table[count($financial_table)-1]->InventoryNWC - $financial_table[0]->InventoryNWC, 2, '.', ',')}}</span>
            @else
                <span style="color:red;">{{number_format($financial_table[count($financial_table)-1]->InventoryNWC - $financial_table[0]->InventoryNWC, 2, '.', ',')}}</span>
            @endif
        </td>
        <td style="text-align:center;">x</td>
    </tr>
</table>

<div style="margin-top: 20px;">
        @php
            $arr = [
                'nwc' => number_format($financial_table[count($financial_table)-1]->NWC, 0, '.', ','),
                'financial_table_count' => count($financial_table)
            ];

            $nwc_percentage = 0;

            if($financial_table[0]->NWC != 0){
                number_format(abs(((($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC) / $financial_table[0]->NWC)*100)), '1', '.', ',');
            }

            $arr2 = [
                'nwc' => number_format($financial_table[count($financial_table)-1]->NWC, 0, '.', ','),
                'financial_table_count' => count($financial_table),
                'year1' => $financial_table[0]->year,
                'year2' => $financial_table[count($financial_table)-1]->year,
                'nwc_change' => number_format(abs($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC), '0', '.', ','),
                'nwc_percentage' => $nwc_percentage,
                'wcd' => number_format($financial_table[count($financial_table)-1]->WCD, 0, ',', ',')
            ];

            $arr3 = [
                'financial_report_title' => $financial_report->title,
                'year2' => $financial_table[count($financial_table)-1]->year,
                'nwc' => number_format(abs($financial_table[count($financial_table)-1]->NWC), 0, '.', ','),

            ];       
        @endphp

        @if(($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC > 0) &&  ($financial_table[count($financial_table)-1]->NWC > 0))
            <p>
            
            {!! trans('financial_analysis/financial_sustainability.the_working_capital_equaled',$arr) !!}

            @if($NWC_change_perc > 200)
                <span style="color:green;">{{number_format($NWC_times,0,'.',',') }}</span>
            @else
                @if($NWC_change_perc == 0)
                    -
                @else   
                    @if($NWC_change_perc > 0)
                        <span style="color:green;">{{number_format($NWC_change_perc,2,'.',',') }}</span>
                    @else
                        <span style="color:red;">{{number_format($NWC_change_perc,2,'.',',') }}</span>
                    @endif
                @endif
            @endif
            {{ trans('financial_analysis/financial_sustainability.times_moreover_the') }}
             
            <span @if($financial_table[count($financial_table)-1]->WCD > 0)
                    style="color:green;"
                  @else
                    style="color:red;"
                  @endif> {{number_format($financial_table[count($financial_table)-1]->WCD, 0, ',', ',')}}</span>. 

                <?php
                    if($financial_table[count($financial_table)-1]->InventoryNWC == 0){
                        $inventoryCapitalRatioClass = "black";
                    }elseif(($financial_table[count($financial_table)-1]->InventoryNWC > 0) && ($financial_table[count($financial_table)-1]->InventoryNWC < 1)){
                        $inventoryCapitalRatioClass = "green";
                    }else{
                        $inventoryCapitalRatioClass = "red";
                    }
                ?>
                {!! trans('financial_analysis/financial_sustainability.at_the_end_of_the_period',['inventoryCapitalRatio' => $financial_table[count($financial_table)-1]->InventoryNWC, 'inventoryCapitalRatioClass' => $inventoryCapitalRatioClass]) !!}

            </p>
        @elseif(($financial_table[count($financial_table)-1]->NWC - $financial_table[0]->NWC < 0) &&($financial_table[count($financial_table)-1]->NWC > 0))
            <p>
                {!! trans('financial_analysis/financial_sustainability.the_working_capital_amounted',$arr2) !!} <span @if($financial_table[count($financial_table)-1]->WCD > 0)
                    style="color:green;"
                  @else
                    style="color:red;"
                  @endif> {{number_format($financial_table[count($financial_table)-1]->WCD, 0, ',', ',')}}</span> . {!! trans('financial_analysis/financial_sustainability.the_inventory_to_working',$arr2) !!}
            </p>
        @else

            {!! trans('financial_analysis/financial_sustainability.has_no_working_capital',$arr3) !!}
            
        @endif  

        <div class = 'center' style="margin-top: 20px">
            <img src = '{{ URL::To("images/graph_1_3_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>
</div>