<h3 class="pdf_title">{{ trans('financial_analysis/profitability_ratios.2_2_profitability_ratios') }}</h3>


<table style="page-break-inside: avoid !important;">
	<tr>
        <td class="center" rowspan="2" style="width: 120px;">{{ trans('financial_analysis/profitability_ratios.profitability_ratios') }}</td>
        <td class="center" colspan="{{ @count($balance_sheets)}}">{{ trans('financial_analysis/profitability_ratios.value_in') }}</td>
        <td class="center" rowspan="2" style="width: 70px;">{!! trans('financial_analysis/profitability_ratios.change_col',['countBalanceSheets' => @count($balance_sheets)]) !!}</td>
    </tr>
    <tr dontbreak="true">
    	@php
    		$w = intval(130/(@count($balance_sheets)));
    	@endphp
        @for($x=@count($balance_sheets); $x > 0 ; $x--)
            
            <td class="center" style="width: {{ $w }}px;">{{ ((int)$financial_report->year - $x + 1) }}</td>
            
            @php $year = ((int)$financial_report->year - $x + 1) @endphp

        @endfor
    </tr>
</table>

<table>
      
        <tr>

            @php

            	echo '<td class="center" style="width: 120px;">1</td>';
                $td_count = @count($balance_sheets)+1;

                for($i=2;$i<=$td_count;$i++){
                    echo '<td class="center" style="width: '.$w.'px;">'.$i.'</td>';
                }
                echo '<td class="center" style="width: 70px;">'.$i.'</td>';
            @endphp
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.1_gross_margin'),35,"<br/>") !!}</td>
            @php
                $max_count = @count($balance_sheets);
                $grossSign = 1;
                
            @endphp
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $GrossMargin = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $GrossMargin = ($income_statements[$x]->GrossProfit / $income_statements[$x]->Revenue) * 100;
                    }
                    
                    $finalGrossMargin = 0;
                    if($x == 0){
                        $finalGrossMargin = $GrossMargin;
                    }

                    if($x == $max_count){
                        $startGrossMargin = $GrossMargin;
                    }
                @endphp
                <td class="center">
                    @php
                        if($GrossMargin != 0){

                        	$grossMarginDec = number_format($GrossMargin, 2, '.', ',');
                        	$grossMarginDecArr = explode('.',$grossMarginDec);

                        	if($grossMarginDecArr == 0 || $grossMarginDecArr == '00'){
                        		echo $grossMarginDec;
	                       	}else{
	                       		echo number_format($grossMarginDecArr[0], 0, '.', ',');
                        	}

                            if($GrossMargin < 0)
                                $grossSign = 0;
                        }else{
                            echo '-';
                        }
                        
                    @endphp
                </td>
                @php if($x==0) $textcontent1 = number_format($GrossMargin, 1, '.', ','); @endphp
            @endfor
            @php 
                $gross1 = 0;
                $gross2 = 0;
                if($income_statements[0]->Revenue != 0){
                    $gross1 = ($income_statements[0]->GrossProfit / $income_statements[0]->Revenue)*100;
                }

                if(isset($income_statements[$max_count]) && $income_statements[$max_count]->Revenue != 0){
                    $gross2 = ($income_statements[$max_count]->GrossProfit / $income_statements[$max_count]->Revenue)*100;
                }
                $gross1 = number_format($gross1, 1, '.', ',');
                $gross2 = number_format($gross2, 1, '.', ',');

                $Change = $gross1 - $gross2;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $change1 = $Change;
            @endphp
            <td class="center {{ $class }}" width="12%">
                
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.2_return_on_sales'),35,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $ROS = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $ROS = (($income_statements[$x]->ProfitLossBeforeTax + $income_statements[$x]->FinanceCosts) / $income_statements[$x]->Revenue) * 100;
                    }
                @endphp
                <td class="center">
                    @php
                        if($ROS != 0)
                            echo number_format($ROS, 1, '.', ',');
                        else
                            echo '-';

                        $finalROS = 0;
                        if($x == 0){
                            $finalROS = $ROS;
                        }
                    @endphp
                </td>
                @php if($x==0) $textcontent2 = number_format($ROS, 1, '.', ','); @endphp
            @endfor
            @php 
                $return1 = 0;
                $return2 = 0;
                if($income_statements[0]->Revenue != 0){
                    $return1 = (($income_statements[0]->ProfitLossBeforeTax + $income_statements[0]->FinanceCosts) / $income_statements[0]->Revenue)*100;
                }
                if(isset($income_statements[$max_count]) && $income_statements[$max_count]->Revenue != 0){
                    $return2 = (($income_statements[$max_count]->ProfitLossBeforeTax + $income_statements[$max_count]->FinanceCosts) / $income_statements[$max_count]->Revenue)*100;
                }

                $return1 = number_format($return1, 1, '.', ',');
                $return2 = number_format($return2, 1, '.', ',');
                
                $Change = (float)$return1 - (float)$return2;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';

                $change2 = $Change;
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.3_profit_margin'),35,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $ProfitMargin = 0;
                    $finalProfitMargin = 0;
                    if(isset($income_statements[$x]) && $income_statements[$x]->Revenue != 0){
                        $ProfitMargin = ($income_statements[$x]->ProfitLoss / $income_statements[$x]->Revenue) * 100;
                    }
                @endphp
                <td class="center">
                    @if($ProfitMargin != 0)
                        {{ number_format($ProfitMargin, 1, '.', ',') }}
                        
                        @php 

                            if($x == 0){
                                $finalProfitMargin = $ProfitMargin;
                            }

                        @endphp
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ProfitMargin, 1, '.', ','); @endphp
            @endfor
            @php 
                $profit1 = 0;
                $profit2 = 0;
                if($income_statements[0]->Revenue != 0){
                    $profit1 = ($income_statements[0]->ProfitLoss / $income_statements[0]->Revenue)*100;
                }
                if(isset($income_statements[$max_count]) && $income_statements[$max_count]->Revenue != 0){
                    $profit2 = ($income_statements[$max_count]->ProfitLoss / $income_statements[$max_count]->Revenue)*100;
                }

                $profit1 = number_format($profit1, 1, '.', ',');
                $profit2 = number_format($profit2, 1, '.', ',');

                $Change = (float)$profit1 - (float)$profit2;

                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';

                $change3 = $Change;
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.reference_interest_coverage'),35,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && $income_statements[$x]->FinanceCosts != 0){
                        $ICR = ($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->FinanceCosts;
                    } else {
                        $ICR = 0;
                    }
                    
                    $class = ($ICR >= 1.5) ? 'green' : 'red'
                @endphp
                <td class="center {{ $class }}">
                    @if($ICR != 0)
                        {{ number_format($ICR, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent4 = number_format($ICR, 1, '.', ','); @endphp
            @endfor
            @php 
                if(isset($income_statements[$max_count]) && $income_statements[0]->FinanceCosts != 0 && $income_statements[$max_count]->FinanceCosts != 0){
                    $Change = (($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) /
                                $income_statements[0]->FinanceCosts) -
                                (($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts) /
                                $income_statements[$max_count]->FinanceCosts);
                } else {
                    $Change = 0;
                }
                
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';

                $finalChange = $Change;
            @endphp
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
    
</table>

@php
    
    $startGrossMarginClass = 'red';
    if($startGrossMargin > 0){
        $startGrossMarginClass = 'green';
    }

    $finalGrossMarginClass = 'red';
    if($finalGrossMargin > 0){
        $finalGrossMarginClass = 'green';
    }

    $grossPercentageClass = 'red';
    $finalROSClass = 'red';
    if($finalROS > 0){
        $grossPercentageClass = 'green';
        $finalROSClass = 'green';
    }

    $finalROSClass = 'red';
    if($finalROS > 0){
        $finalROSClass = 'green';
    }

    $change1Class = 'red';
    if($change1 > 0){
        $change1Class = 'green';
    }

    $change1raw = $change1;
    $change1 = number_format($change1, 1, '.', ',');
    $finalROS = number_format($finalROS, 1, '.', ',');
    $finalGrossMargin = number_format($finalGrossMargin, 1, '.', ',');
    $startGrossMargin = number_format($startGrossMargin, 1, '.', ',');
    $finalROSPercentage = number_format((float)$finalROS/100,2, '.', ',');
    $finalProfitMargin = number_format($finalProfitMargin, 1, '.', ',');
    $year = $balance_sheets[count($balance_sheets)-1]->Year;
    $endYr = $financial_report->year;
    
    $arr = [
        'financialReportTitle' => $financial_report->title,
        'finalGrossMarginClass' => $finalGrossMarginClass,
        'finalGrossMargin' => $finalGrossMargin,
        'startGrossMarginClass' => $startGrossMarginClass,
        'change1Class' => $change1Class,
        'change1' => $change1,
        'year' => $endYr
    ];

    $arr2 = [
        'finalROSPercentage' => $finalROSPercentage,
        'finalROS' => $finalROS,
        'finalProfitMargin' => $finalProfitMargin,
        'year' => $endYr
    ];

    $profitTextPA = '';
    $profitTextPB = '';

    if($change1raw <= -20){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA1',['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB1',['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    if($change1raw < 0 && $change1raw > -20){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA2', ['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB2', ['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    if($change1raw > 0 && $change1raw <= 5 && $ROS > 0){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA3', ['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB3', ['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    if($change1raw > 5 && $ROS > 0){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA4', ['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB4', ['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    if($change1raw > 0 && $ROS < 0){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA5', ['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB5', ['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    if($change1raw == '-' || $change1raw == 0){
        $profitTextPA = trans('financial_analysis/profitability_ratios.profitTextPA6', ['financialReportTitle'=>$financial_report->title,'startGrossMargin'=>$startGrossMargin,'finalGrossMargin' =>$finalGrossMargin, 'change1' => $change1, 'year' => $endYr]);
        $profitTextPB = trans('financial_analysis/profitability_ratios.profitTextPB6', ['finalROSPercentage'=>$finalROSPercentage,'finalROS'=>$finalROS, 'finalProfitMargin' => $finalProfitMargin, 'year' => $endYr]);
    }

    echo '<p>'.$profitTextPA.'</p>';
    echo '<p>'.$profitTextPB.'</p>';
    
@endphp


<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_1_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>

<br/><br/>

<table style="page-break-inside: avoid !important;">
	<tr>
        <td class="center" rowspan="2" style="width: 70px">{{ trans('financial_analysis/profitability_ratios.profitability_ratios') }}</td>
        <td class="center" colspan="{{ @count($balance_sheets)-1 }}">{{ trans('financial_analysis/profitability_ratios.value_in') }}</td>
        <td class="center" rowspan="2" style="width: 70px">{{ trans('financial_analysis/profitability_ratios.change_col', ['countBalanceSheets' => count($balance_sheets) ]) }}</td>
        <td class="center" rowspan="2" style="width: 180px">{{ trans('financial_analysis/profitability_ratios.description_of_the_ratio') }}</td>
    </tr>
    <tr dontbreak="true">

        @php
        	$w = intval(140/(@count($balance_sheets)-1));
            for($x = @count($balance_sheets)-1; $x > 0 ; $x--){
                echo '<td class="center" style="width: '.$w.'px">'.((int)$financial_report->year - $x + 1).'</td>';

                if($x == count($balance_sheets)-1)
                    $initialYr = ((int)$financial_report->year - $x + 1);

                if($x == 1)
                    $endYr = ((int)$financial_report->year - $x + 1);
            }
            
        @endphp
    </tr>
</table>

<table>
   
        
        <tr>
            @php
                $td_count = @count($balance_sheets);
                echo '<td class="center" style="width: 70px">1</td>';
                for($i=2;$i<=$td_count;$i++){
                    echo '<td class="center" style="width: '.$w.'px">'.$i.'</td>';
                }

                echo '<td class="center" style="width: 70px">'.$i.'</td>';
                $i++;
                echo '<td class="center" style="width: 180px">'.$i.'</td>';
            @endphp
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.return_on_equity'),16,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x]) && $balance_sheets[$x + 1]->Equity != 0){
                        $ROE = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x + 1]->Equity) / 2)) * 100;
                        if($x == $max_count) $ROE1 = round($ROE, 1);
                        if($x == 0) $ROE2 = round($ROE, 1);
                        $class = ($ROE >= 12) ? 'green' : 'red';
                    }else{
                        $ROE = 0;
                        $ROE1 = 0;
                        $ROE2 = 0;
                    }


                @endphp
                <td class="center {{ $class }}">
                    @if($ROE != 0)
                        {{ number_format($ROE, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($ROE, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROE2 - $ROE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';

                $ROEChangeRaw = $Change;
                $ROEChange = number_format($Change, 1, '.', ',');
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td>
                {!! wordwrap(trans('financial_analysis/profitability_ratios.roe_is_calculated'),43,"<br/>") !!}
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.return_on_assets'),16,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x])){
                        $ROA = ($income_statements[$x]->ProfitLoss / 
                                        (($balance_sheets[$x]->Assets + 
                                    $balance_sheets[$x + 1]->Assets) / 2)) * 100;
                        if($x == $max_count) $ROA1 = round($ROA, 1);
                        if($x == 0) $ROA2 = round($ROA, 1);
                        $class = ($ROA >= 6) ? 'green' : 'red';
                    }else{
                        $ROA = 0;
                        $ROA1 = 0;
                        $ROA2 = 0;
                    }
                @endphp
                <td class="center {{ $class }}">
                    @if($ROA != 0)
                        {{ number_format($ROA, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent1 = number_format($ROA, 1, '.', ','); @endphp
            @endfor
            @php 
                $Change = $ROA2 - $ROA1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 1, '.', ',');

                $ROAChange = number_format($Change, 1, '.', ',');
                $ROAChangeRaw = $Change;
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td>
                {!! wordwrap(trans('financial_analysis/profitability_ratios.roa_is_calculated'),43,"<br/>") !!}
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/profitability_ratios.return_on_capital'),16,"<br/>") !!}</td>
            @for($x=$max_count; $x > 0 ; $x--)
                @php
                    $ret_cap_divisor = 0;
                    if( isset($balance_sheets[$x]) && isset($balance_sheets[$x + 1]) ){
                        $ret_cap_divisor = $balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x]->NoncurrentLiabilities + 
                                    $balance_sheets[$x + 1]->Equity + 
                                    $balance_sheets[$x + 1]->NoncurrentLiabilities;
                    }

                    if(isset($income_statements[$x]) && isset($balance_sheets[$x + 1]) && isset($balance_sheets[$x]) && $ret_cap_divisor != 0){
                        $ROCE = (
                                (
                                $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts
                                ) / 
                                (
                                    (
                                    $balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x]->NoncurrentLiabilities + 
                                    $balance_sheets[$x + 1]->Equity + 
                                    $balance_sheets[$x + 1]->NoncurrentLiabilities
                                    ) / 
                                2)
                            ) * 100;
                        if($x == $max_count) $ROCE1 = round($ROCE, 1);
                        if($x == 0) $ROCE2 = round($ROCE, 1);
                    }else{
                        $ROCE = 0;
                        $ROCE1 = 0;
                        $ROCE2 = 0;
                    }
                @endphp
                <td class="center">
                    @if($ROCE != 0)
                        {{ number_format($ROCE  , 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = $ROCE2 - $ROCE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td>
                {!! wordwrap(trans('financial_analysis/profitability_ratios.roce_is_calculated'),43,"<br/>") !!}
            </td>
        </tr>
    
</table>


@php

$ROAChangeClass = 'red';
if($ROAChange > 0){
    $ROAChangeClass = 'green';
}

$ROEChangeClass = 'red';
if($ROEChange > 0){
    $ROEChangeClass = 'green';
}

$ROAClass = 'red';
if($ROA >= 12){
    $ROAClass = 'green';
}

$ROEClass = 'red';
if($ROE >= 6){
    $ROEClass = 'green';
}

$finalROA = number_format($ROA, 1, '.', ',');
$finalROE = number_format($ROE, 1, '.', ',');
$ROAText = '';
$ROEtext = '';
$ROEtextB = '';

$ROAChange = str_replace('-','',$ROAChange);

$arr = [
    'endYr' => $endYr,
    'ROAClass' => $ROAClass,
    'finalROA' => $finalROA,
    'ROAChange' => $ROAChange,
    'initialYr' => $initialYr,
    'ROAChangeClass' => $ROAChangeClass,
    'financialReportTitle' => $financial_report->title,
    'finalROE' => $finalROE,
    'capital' => session('capital'),
    'yearCount' => @count($balance_sheets)-1,
    'ROEClass' => $ROEClass,
    'finalROE' => $finalROE,
    'initialYr' => $initialYr
];

session(['finalROE' => $finalROE]);

if($ROAChangeRaw > 5 && $ROAChangeRaw <= 10){
    $ROAText = trans('financial_analysis/profitability_ratios.ROAText1',$arr);
}

if($ROAChangeRaw > 0 && $ROAChangeRaw <= 5 && $ROA > 0){
    $ROAText = trans('financial_analysis/profitability_ratios.ROAText2',$arr);
}

if($ROAChangeRaw < 0 && $ROA < 0){
    $ROAText = trans('financial_analysis/profitability_ratios.ROAText3',$arr);
}

if($ROAChangeRaw < 0 && intval($ROA) == 0 ){
   $ROAText = trans('financial_analysis/profitability_ratios.ROAText4',$arr);
}

if($ROAChangeRaw <= 0 && $ROA > 0 && intval($ROA) > 0 && $ROA <=5 ){
    $ROAText = trans('financial_analysis/profitability_ratios.ROAText5',$arr);
}

if($ROAChangeRaw < 0 && $ROA > 5){
   $ROAText = trans('financial_analysis/profitability_ratios.ROAText6',$arr);
}

if($ROE > 0 && $ROEChangeRaw >= 0 && $ROAChangeRaw < 0){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText1',$arr);

    $ROETextB = trans('financial_analysis/profitability_ratios.OETextB1',$arr);
}

if($ROE > 0 && $ROAChangeRaw >= 0){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText2',$arr);

    $ROETextB = trans('financial_analysis/profitability_ratios.ROETextB2',$arr);
}

if($ROE > 1 && $ROAChangeRaw < 0 && $ROEChangeRaw < 0 && $ROAChangeRaw > -5){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText3',$arr);

    $ROETextB = trans('financial_analysis/profitability_ratios.ROETextB3',$arr);
}

if($ROE < 1 && $ROAChangeRaw < -5){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText4',$arr);

    $ROETextB = trans('financial_analysis/profitability_ratios.ROETextB4',$arr);
}

if($ROE < 1 && $ROAChangeRaw > -5 && $ROAChangeRaw < 0){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText5',$arr);
    
    $ROETextB = trans('Rinancial_analysis/profitability_ratios.ROETextB5',$arr);
}

if($ROE >= 1 && $ROAChangeRaw < -5){
    $ROEText = trans('financial_analysis/profitability_ratios.ROEText6',$arr);

    $ROETextB = trans('financial_analysis/profitability_ratios.ROETextB6',$arr);

}

echo '<p>'.$ROAText.'</p>';
echo '<p>'.$ROEtext.'</p>';
echo '<p>'.$ROEtextB.'</p>';

@endphp

<br /><br/>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_2_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>