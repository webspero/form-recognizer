<h3 class="pdf_title">
    {{ trans('financial_analysis/financial_result.21_overview_of_the_financial') }}
</h3>

<table style="page-break-inside: avoid !important;"  class="smallestfont">
	<tr>
	    <td class="center" rowspan="2" style="width:110px">{{ trans('financial_analysis/financial_result.indicator') }}</td>
        <td class="center" colspan="{{ @count($income_statements) }}">{!! trans('financial_analysis/financial_result.value_php') !!}</td>
	    <td class="center" colspan="2">{{ trans('financial_analysis/financial_result.change') }}</td>
	    <td class="center" rowspan="2" style="width: 75px">{{ trans('financial_analysis/financial_result.average_annual_value') }}</td>
	</tr>
	<tr dontbreak="true">
		@php
			$w = intval(200/@count($income_statements));

		@endphp
	    @for($x=@count($income_statements); $x > 0 ; $x--)
	        <td class="center" style="width: {{ $w }}px">{{ ((int)$financial_report->year - $x + 1) }}</td>
	    @endfor
	    <td class="center"  style="width: 85px"> PHP (col.{{ @count($income_statements)+1 }} - col.2)</td>
	    <td class="center" style="width: 70px">&plusmn; % ({{ @count($income_statements)+1 }}-2) : 2</td>
	</tr>
</table>

<table class="smallestfont">
       
        <tr>
            @php
                

                echo '<td class="center" style="width:110px">1</td>';
                $td_count = @count($income_statements) + 1;

                for($i=2;$i<=$td_count;$i++){
                    echo '<td class="center" style="width: '.$w.'px">'.$i.'</td>';
                    
                }

                echo '<td class="center" style="width: 85px">'.$i.'</td>';
                $i++;

                echo '<td class="center" style="width: 70px">'.$i.'</td>';
                $i++;
                echo '<td class="center" style="width: 75px">'.$i.'</td>';
            @endphp
        </tr>
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.1_revenue'),28,"<br/>") !!}</td>
            @php
                $max_count = @count($income_statements) -1;
                for($x=$max_count; $x >= 0 ; $x--){
                    
                        $Revenue = $income_statements[$x]->Revenue;
                        $class = ($Revenue >= 2) ? 'green' : 'red';
                
            @endphp
                    <td class="center {{ $class }}">
                        @if($Revenue != 0)
                            {{ number_format($Revenue, 0, '.', ',') }}
                        @else
                            -
                        @endif
                    </td>
                
            @php
                    if($x==0) $textcontent1 = number_format($Revenue, 0, '.', ','); 

                }
                $Change = $income_statements[0]->Revenue -
                            $income_statements[$max_count]->Revenue;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 0, '.', ',');
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php
                if($income_statements[$max_count]->Revenue != 0){
                    $PercentageChange = 0;
                    $dividend = $income_statements[$max_count]->Revenue;
                    if($dividend != 0 && $dividend != '0.00' && $income_statements[$max_count]->Revenue != 0){
                        $PercentageChange = (($income_statements[0]->Revenue -
                            $income_statements[$max_count]->Revenue) / 
                            $dividend) * 100;
                    }
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->Revenue;
                }

                $Average = 0;
                $dividend = $max_count+1;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.2_cost_of_sales'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $CostOfSales = $income_statements[$x]->CostOfSales;
                    $class = ($CostOfSales >= 2) ? 'green' : 'red';
                @endphp
                <td class="center red">
                    @if($CostOfSales != 0)
                        {{ number_format($CostOfSales, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = $income_statements[0]->CostOfSales -
                            $income_statements[$max_count]->CostOfSales;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if($income_statements[$max_count]->CostOfSales != 0) {
                    $PercentageChange = 0;
                    $dividend = $income_statements[$max_count]->CostOfSales;
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = (($income_statements[0]->CostOfSales -
                            $income_statements[$max_count]->CostOfSales) / 
                            $dividend) * 100;
                    }
                    
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->CostOfSales;
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }

                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center red">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.3_gross_profit'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $GrossProfit = $income_statements[$x]->GrossProfit;
                    $class = ($GrossProfit >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($GrossProfit != 0)
                        {{ number_format($GrossProfit, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent3 = number_format($GrossProfit, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->GrossProfit) -
                            ($income_statements[$max_count]->GrossProfit);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent4 = number_format($Change, 0, '.', ',');
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if($income_statements[$max_count]->GrossProfit != 0) {
                    $PercentageChange = 0;                    
                    $dividend = ($income_statements[$max_count]->GrossProfit);

                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->GrossProfit) -
                            ($income_statements[$max_count]->GrossProfit)) / 
                            $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
                $textcontent5 = number_format($PercentageChange, 1, '.', ',');
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->GrossProfit);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.4_other_income_and_expenses'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $OIEeFC = $income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($OIEeFC >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($OIEeFC != 0)
                        {{ number_format($OIEeFC, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts);
                    
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
            	
            	@php
	            	$dividend = ($income_statements[$max_count]->ProfitLossBeforeTax -
                                $income_statements[$max_count]->GrossProfit + 
                                $income_statements[$max_count]->FinanceCosts);

	                if($Change != 0){
	                    $percentageChange = number_format(($Change/$dividend)*100,2,".",",");
	                    echo $percentageChange;
	                }
	                else{
	                	echo '-';
	            	}
	                
                @endphp
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts);
                }
                
                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>5. <i>EBIT</i> (3+4)</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $EBIT = $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($EBIT >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($EBIT != 0)
                        {{ number_format($EBIT, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent6 = number_format($EBIT, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts)) / 
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif

                <!-- @if($Change != 0)
                    @if($Change > 0)
                        up
                    @else
                        down
                    @endif
                @else
                    -
                @endif -->
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>&nbsp;&nbsp;5.1 <i>EBITDA</i></td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $EBITDA = $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts + $cashflow[$x]->DepreciationAndAmortisationExpense;
                    $class = ($EBIT >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($EBITDA != 0)
                        {{ number_format($EBITDA, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent6 = number_format($EBIT, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts + $cashflow[0]->DepreciationAndAmortisationExpense) -
                            ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts + $cashflow[$max_count]->DepreciationAndAmortisationExpense);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts + $cashflow[0]->DepreciationAndAmortisationExpense) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts + $cashflow[0]->DepreciationAndAmortisationExpense) -
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts + $cashflow[$max_count]->DepreciationAndAmortisationExpense)) / 
                                ($income_statements[$max_count]->ProfitLossBeforeTax + 
                                $income_statements[$max_count]->FinanceCosts + $cashflow[$max_count]->DepreciationAndAmortisationExpense)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">

                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif

                <!-- @if($Change != 0)
                    @if($Change > 0)
                        up
                    @else
                        down
                    @endif
                @else
                    -
                @endif -->
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts + $cashflow[$x]->DepreciationAndAmortisationExpense);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>

        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.6_finance_costs'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $FinanceCosts = $income_statements[$x]->FinanceCosts;
                    $class = ($FinanceCosts >= 2) ? 'green' : 'red';
                @endphp
                <td class="center red">
                    @if($FinanceCosts != 0)
                        {{ number_format($FinanceCosts, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->FinanceCosts) -
                            ($income_statements[$max_count]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->FinanceCosts) != 0) {
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->FinanceCosts);
                    
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->FinanceCosts) -
                                ($income_statements[$max_count]->FinanceCosts)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->FinanceCosts);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center red">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.7_income_tax_expense'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $IncomeTaxExpenseContinuingOperations = $income_statements[$x]->IncomeTaxExpenseContinuingOperations;
                    $class = ($IncomeTaxExpenseContinuingOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center red">
                    @if($IncomeTaxExpenseContinuingOperations != 0)
                        {{ number_format($IncomeTaxExpenseContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                            ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations) != 0) {

                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                                ($income_statements[$max_count]->IncomeTaxExpenseContinuingOperations)) / 
                                $dividend) * 100;
                    }
                    

                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->IncomeTaxExpenseContinuingOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center red">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.8_profit_loss'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLossFromContinuingOperations = $income_statements[$x]->ProfitLossFromContinuingOperations;
                    $class = ($ProfitLossFromContinuingOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ProfitLossFromContinuingOperations != 0)
                        {{ number_format($ProfitLossFromContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLossFromContinuingOperations) -
                            ($income_statements[$max_count]->ProfitLossFromContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossFromContinuingOperations) != 0) {

                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossFromContinuingOperations);

                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ProfitLossFromContinuingOperations) -
                                ($income_statements[$max_count]->ProfitLossFromContinuingOperations)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif

                <!-- @if($Change != 0)
                    @if($Change > 0)
                        up
                    @else
                        down
                    @endif
                @else
                    -
                @endif -->
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromContinuingOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($dividend != 0 && $dividend != '0.00'){
                    $Average = $Sum / $dividend;
                }
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.9_profit_loss'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLossFromDiscontinuedOperations = $income_statements[$x]->ProfitLossFromDiscontinuedOperations;
                    $class = ($ProfitLossFromDiscontinuedOperations >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ProfitLossFromDiscontinuedOperations != 0)
                        {{ number_format($ProfitLossFromDiscontinuedOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php
                $Change = ($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                            ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations);

                    if($dividend != 0 && $dividend != '0.00'){    
                        $PercentageChange = ((($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                                ($income_statements[$max_count]->ProfitLossFromDiscontinuedOperations)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromDiscontinuedOperations);
                }

                $Average = 0;
                $dividend = $max_count+1;;

                if($Average != 0 && $Average != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>{!! wordwrap(trans('financial_analysis/financial_result.10_profit_loss'),28,"<br/>") !!}</b></td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ProfitLoss = $income_statements[$x]->ProfitLoss;
                    $class = ($ProfitLoss >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    <b>
                    @if($ProfitLoss != 0)
                        {{ number_format($ProfitLoss, 0, '.', ',') }}
                    @else
                        -
                    @endif
                    </b>
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->ProfitLoss) -
                            ($income_statements[$max_count]->ProfitLoss);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                <b>
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
                </b>
            </td>
            @php 
                if(($income_statements[$max_count]->ProfitLoss) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ProfitLoss);    
                    
                    if($dividend != 0 && $dividend != '0.00' && $income_statements[$max_count]->ProfitLoss!= 0){
                        $PercentageChange = ((($income_statements[0]->ProfitLoss) -
                                ($income_statements[$max_count]->ProfitLoss)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                <b>
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif

                <!-- @if($Change != 0)
                    @if($Change > 0)
                        up
                    @else
                        down
                    @endif
                @else
                    -
                @endif -->
                </b>
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLoss);
                }
                $Average = $Sum / ($max_count+1);;
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                <b>
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
                </b>
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.11_other_comprehensive_income'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $OtherComprehensiveIncome = $income_statements[$x]->OtherComprehensiveIncome;
                    $class = ($OtherComprehensiveIncome >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($OtherComprehensiveIncome != 0)
                        {{ number_format($OtherComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            @php 
                $Change = ($income_statements[0]->OtherComprehensiveIncome) -
                            ($income_statements[$max_count]->OtherComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->OtherComprehensiveIncome) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->OtherComprehensiveIncome);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->OtherComprehensiveIncome) -
                                ($income_statements[$max_count]->OtherComprehensiveIncome)) / 
                                $dividend) * 100;
                    }
                
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->OtherComprehensiveIncome);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>{!! wordwrap(trans('financial_analysis/financial_result.12_comprehensive_income'),28,"<br/>") !!}</td>
            @for($x=$max_count; $x >= 0 ; $x--)
                @php
                    $ComprehensiveIncome = $income_statements[$x]->ComprehensiveIncome;
                    $class = ($ComprehensiveIncome >= 2) ? 'green' : 'red';
                @endphp
                <td class="center {{ $class }}">
                    @if($ComprehensiveIncome != 0)
                        {{ number_format($ComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                @php if($x==0) $textcontent7 = number_format($ComprehensiveIncome, 0, '.', ','); @endphp
            @endfor
            @php 
                $Change = ($income_statements[0]->ComprehensiveIncome) -
                            ($income_statements[$max_count]->ComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            @php 
                if(($income_statements[$max_count]->ComprehensiveIncome) != 0) {
                    
                    $PercentageChange = 0;
                    $dividend = ($income_statements[$max_count]->ComprehensiveIncome);
                    if($dividend != 0 && $dividend != '0.00'){
                        $PercentageChange = ((($income_statements[0]->ComprehensiveIncome) -
                                ($income_statements[$max_count]->ComprehensiveIncome)) / $dividend) * 100;
                    }

                } else {
                    $PercentageChange = 0;
                }
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 2, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 2, '.', ',')}}
                    @endif
                @else
                    -
                @endif

                <!-- @if($Change != 0)
                    @if($Change > 0)
                        up
                    @else
                        down
                    @endif
                @else
                    -
                @endif -->
                
            </td>
            @php
                $Sum = 0;
                for($x=$max_count; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ComprehensiveIncome);
                }

                $Average = 0;
                $dividend = $max_count+1;;
                if($dividend != 0 && $dividend != '0.00')
                    $Average = $Sum / $dividend;
                
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            @endphp
            <td class="center {{ $class }}">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
    
</table>
<br /><br/>

<div>

    @php
        $revenue1Class = ($income_statements[0]->Revenue >= 0) ? "green" : "red";
        $revenue2Class = ($income_statements[1]->Revenue >= 0) ? "green" : "red";
        $revenue1DiffClass = (($income_statements[0]->Revenue - $income_statements[1]->Revenue) >= 0) ? "green" : "red";
        $grossProfit = ($income_statements[0]->GrossProfit >= 0) ? "green" : "red";
        $grossProfit2 = (($income_statements[0]->GrossProfit - $income_statements[1]->GrossProfit) >= 0) ? "green" : "red";

        $arr = [
            'revenue1Class' => $revenue1Class,
            'revenue2Class' => $revenue2Class,
            'revenue1DiffClass' => $revenue1DiffClass,
            'grossProfit' => $grossProfit,
            'grossProfit2' => $grossProfit2,
            'revenue' => number_format($income_statements[0]->Revenue, 0, '.', ','),
            'revenue2' => number_format($income_statements[1]->Revenue, 0, '.', ','),
            'year1' => $income_statements[1]->Year,
            'incomeStatementDif' => number_format(($income_statements[0]->Revenue - $income_statements[1]->Revenue), 0, '.', ','),
            'grossProfitVal' => number_format($income_statements[0]->GrossProfit, 0, '.', ','),
            'grossProfitVal2' => number_format(($income_statements[0]->GrossProfit - $income_statements[1]->GrossProfit), 0, '.', ',')
        ];
    @endphp

    <p>

        {!! trans('financial_analysis/financial_result.the_revenue_equaled',$arr) !!}
    </p>
    <p>
        @php

            $profitLossClass = ($EBIT >= 0) ? "green" : "red";

            $comprehensiveClass = ($income_statements[0]->ComprehensiveIncome >= 0) ? "green" : "red";

            $profitLoss = number_format($income_statements[0]->ProfitLossFromOperatingActivities, 0, '.', ',');

            $financial_report_title = $financial_report->title;

            $comprehensiveIncome = number_format($income_statements[0]->ComprehensiveIncome, 0, '.', ',');

            $year = $income_statements[0]->Year;

            $arr = [
                'profitLossClass' => $profitLossClass,
                'comprehensiveClass' => $comprehensiveClass,
                'ebit' => number_format($EBIT,2,".",","),
                'profitLoss' => $profitLoss,
                'grossProfit' => $grossProfit,
                'financial_report_title' => $financial_report_title,
                'comprehensiveIncome' => $comprehensiveIncome,
                'grossProfitVal' => number_format($income_statements[0]->GrossProfit, 0, '.', ','),
                'year' => $year
            ];

        @endphp

        {!! trans('financial_analysis/financial_result.for_the_last_year',$arr) !!}
    </p>
</div>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_1_".$income_statements[0]->financial_report_id.".png") }}' class="graph_width">
</div>