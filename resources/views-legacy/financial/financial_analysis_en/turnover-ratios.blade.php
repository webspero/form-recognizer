<h3 class="pdf_title"> {{ trans('financial_analysis/turnover_ratios.2_3_analysis_of_the_business') }}</h3>
<p>
    {{ trans('financial_analysis/turnover_ratios.further_in_the_table') }}
</p>

<table style="page-break-inside: avoid !important;">
	<tr>
        <td style="text-align:center;width: 150px" rowspan="2" >{{ trans('financial_analysis/turnover_ratios.turnover_ratio') }}</td>
        <td style="text-align:center;" colspan="{{count($turnoverRatios)}}">{{ trans('financial_analysis/turnover_ratios.value_days') }}</td>
        <td style="text-align:center;width: 70px" rowspan="2">{!! trans('financial_analysis/turnover_ratios.ratio_year1',['year1' => $turnoverRatios[0]->year]) !!}</td>
        <td style="text-align:center;width: 70px" rowspan ="2">{!! trans('financial_analysis/turnover_ratios.ratio_year2',['year2' => $turnoverRatios[count($turnoverRatios)-1]->year]) !!}</td>
        <td style="text-align:center;width: 80px" rowspan ="2">{!! trans('financial_analysis/turnover_ratios.change',['colCount' => count($turnoverRatios)+1]) !!}</td>
    </tr>
    <tr>
    	@php
    		$w = intval(170/(count($turnoverRatios)));
    	@endphp
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;width: {{ $w }}px">{{$turnover->year}}</td>
        @endforeach
    </tr>
</table>

<table style="width: 100%">
    
    <tr>
    	<td style="text-align:center;width: 150px">1</td>

    	@php
    	$i = 1;
    	foreach($turnoverRatios as $turnover){
    		$i++;
    		echo '<td style="text-align:center;width: '.$w.'px">'.$i.'</td>';
    	}
    	@endphp
        @php $i++; @endphp
        <td style="text-align:center;width: 70px">{{ $i }}</td>
        @php $i++; @endphp
        <td style="text-align:center;width: 70px">{{ $i }}</td>
        @php $i++; @endphp
        <td style="text-align:center;width: 80px">{{ $i }}</td>
    </tr>

    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.receivables_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->ReceivablesTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->ReceivablesTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->ReceivablesTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->ReceivablesTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.accounts_payable_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->PayableTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->PayableTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->PayableTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->PayableTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->PayableTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->PayableTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.inventory_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->InventoryTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->InventoryTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->InventoryTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->InventoryTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.asset_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->AssetTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->AssetTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->AssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->AssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->AssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->AssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.current_asset_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->CAssetTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->CAssetTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->CAssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->CAssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.capital_turnover'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->CapitalTurnover}}</td>
        @endforeach
        <td style="text-align:center;">
            @if($turnoverRatios[0]->CapitalTurnover != 0)
                @if(round(($turnoverRatios[0]->num_days/$turnoverRatios[0]->CapitalTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[0]->num_days/$turnoverRatios[0]->CapitalTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover != 0)
                @if(round(($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover),2) >= 0.1)
                    {{number_format($turnoverRatios[count($turnoverRatios)-1]->num_days/$turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover, 1, '.', ',')}}
                @else
                    {{'<0.1'}}
                @endif
            @else
                -
            @endif
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            {!! wordwrap(trans('financial_analysis/turnover_ratios.reference'),28,"<br/>") !!}
        </td>
        @foreach($turnoverRatios as $turnover)
            <td style="text-align:center;">{{$turnover->CCC}}</td>
        @endforeach
        <td style="text-align:center;">
            x
        </td>
        <td style="text-align:center;">
            x
        </td>
        <td style="text-align:center;">
            @if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) == 0)
                -
            @elseif(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) > 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @endif
        </td>
        
    </tr>
</table>

<p>
    @php
        $totalDays = 0;
        $totalPayableDays = 0;
        $cnt = 0;
        foreach($turnoverRatios as $turnover){
            $totalDays += $turnover->AssetTurnover;
            $totalPayableDays += $turnover->PayableTurnover;
            $cnt += 1;
        }

        $arr = [
            'receivablesTurnover' => $turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover,
            'payableTurnover' => $turnoverRatios[count($turnoverRatios)-1]->PayableTurnover,
            'financialReportTitle' => $financial_report->title,
            'totalDays'            => round(($totalDays/$cnt),0),
            'payableDays'          => round(($totalPayableDays/$cnt),0)
        ];

    @endphp

    {!! trans('financial_analysis/turnover_ratios.during_the_last_year',$arr) !!}
</p>