<h3 class="pdf_title">{{ trans('financial_analysis/conclusion.conclusion') }}</h3>
<h3>{{ trans('financial_analysis/conclusion.31_key_ratios_summary') }}</h3>

<div>
    <p>
        {{ trans('financial_analysis/conclusion.the_main_financial_state',['financial_report_title' => $financial_report->title, 'startYear' => $balance_sheets[count($balance_sheets)-1]->Year,'endYear' => $balance_sheets[0]->Year]) }}
    </p> 
    {{-- @if(($keySummary->NCAtoNW >= 0) && ($keySummary->NCAtoNW <= 1.25) && ($keySummary->DebtRatio < 0.30) && ($keySummary->DebtRatio <= 0.60)) --}}
    <p>{{ trans('financial_analysis/conclusion.there_are_the_following') }}</p>
    <ul>
        {{-- Noncurrent assets --}}
        @if(($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1)) 
            <li>
                {!! trans('financial_analysis/conclusion.the_value_of_the_non_current',['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}
            </li>
        @elseif(($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25))
            <li>
                {!! trans('financial_analysis/conclusion.the_value_of_the_non_current_good',['ncatonw' => number_format($keySummary[0]->NCAtoNW, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Debt-to-equity Ratio --}}
        @if($keySummary[0]->DebtRatio < 0.30)
            <li>
                {!! trans('financial_analysis/conclusion.the_debt_to_equity',['debtratio' =>number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50)
            <li>

                {!! trans('financial_analysis/conclusion.the_debt_ratio_has',['debtratio' =>number_format($keySummary[0]->DebtRatio, 2, '.', ','),'debtratioPercentage'=>number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60)
            <li>
                {!! trans('financial_analysis/conclusion.the_percentage_of_liabilities',['debtratioPercentage'=>number_format(($keySummary[0]->DebtRatio * 100), 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Working Capital and Inventories --}}
        @if($keySummary[0]->NWC > 0)
            @if(($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9))
                <li>

                {!! trans('financial_analysis/conclusion.long_term_resources') !!}

                </li>
            @elseif(($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))

                <li>

                    {!! trans('financial_analysis/conclusion.working_capital') !!}

                </li>
            @endif
        @endif

        {{-- Current Ratio --}}
        @if(($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1))
            <li>
                {!! trans('financial_analysis/conclusion.the_current_ratio',['currentRatio'=>number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->CurrentRatio >= 2.1)
            <li>

                {!! trans('financial_analysis/conclusion.the_current_ratio_criteria',['currentRatio'=>number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}
               
            </li>
        @endif

        {{-- Quick Ratio --}}
        @if(($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1))
            <li>
                {!! trans('financial_analysis/conclusion.a_good_relationship',['quickRatio'=>number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->QuickRatio >= 1.1)
            <li>
                {!! trans('financial_analysis/conclusion.an_outstanding_relationship',['quickRatio'=>number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Cash Ratio --}}
        @if(($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25))
            <li>
                {!! trans('financial_analysis/conclusion.the_cash_ratio_is',['cashRatio'=>number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->CashRatio > 0.25)
            <li>
                {!! trans('financial_analysis/conclusion.the_cash_ratio_is_high_cash',['cashRatio'=>number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Return of Equity --}}
        @if(($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2))
            <li>
                {!! trans('financial_analysis/conclusion.return_on_equity',['roePercentage'=>number_format($keySummary[0]->ROE*100,2,'.',','),'year'=>$keySummary[0]->Year]) !!}
            </li>
        @elseif($keySummary[0]->ROE > 0.2)
            <li>
                {!! trans('financial_analysis/conclusion.high_return_on_equity',['roePercentage'=>number_format($keySummary[0]->ROE*100,2,'.',',')]) !!}
            </li>
        @endif

        {{-- ROA --}}
        @if(($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1))
            <li>
                {!! trans('financial_analysis/conclusion.good_return_on_assets',['roaPercentage'=>number_format($keySummary[0]->ROA*100,2,'.',','),'year'=>$keySummary[0]->Year]) !!}
            </li>
        @elseif($keySummary[0]->ROA > 0.1)
            <li>
                {!! trans('financial_analysis/conclusion.excellent_return_on_assets',['roaPercentage'=>number_format($keySummary[0]->ROA*100,2,'.',','),'year'=>$keySummary[0]->Year]) !!}
            </li>
        @endif

        {{-- Net Worth --}}
        @if($financial_report)
            @if($balance_sheets[0]->IssuedCapital)
                @if($keySummary[0]->NetAsset > $balance_sheets[0]->IssuedCapital)
                	@if($balance_sheets[0]->IssuedCapital > 0)
	                    @if(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital) > 10)
	                        <li>

	                            {!! trans('financial_analysis/conclusion.net_worth_net_assets',['netAsset'=>number_format(($keySummary[0]->NetAsset/$balance_sheets[0]->IssuedCapital),2,'.',','),'year'=>$keySummary[0]->Year]) !!}
	                        </li>
	                    @else
	                        <li>{!! trans('financial_analysis/conclusion.the_net_worth_is_higher') !!}</li>
	                    @endif
                    @endif
                @endif
            @endif
        @endif

        {{-- Equity --}}
        @if($financial_report)
            @if($balance_sheets)
                @if($balance_sheets[0]->Equity > $balance_sheets[count($balance_sheets)-1]->Equity)
                    @if($balance_sheets[0]->Assets  < $balance_sheets[count($balance_sheets)-1]->Assets)
                        <li>
                            {!! trans('financial_analysis/conclusion.equity_value_grew',['startYear'=>$balance_sheets[count($balance_sheets)-1]->Year,'endYear'=>$balance_sheets[0]->Year]) !!}
                        </li>
                    @else
                        <li>

                            {!! trans('financial_analysis/conclusion.the_equity_growth_for_the',['startYear'=>$balance_sheets[count($balance_sheets)-1]->Year,'endYear'=>$balance_sheets[0]->Year]) !!}
                        </li>
                    @endif
                @endif
            @endif
        @endif

        {{-- EBIT --}}
        @if($keySummary[0]->EBIT > 0)
            @if($keySummary[0]->EBIT > $keySummary[1]->EBIT)
                @if($keySummary[0]->EBIT > 0)
                    <li>
                        {!! trans('financial_analysis/conclusion.during_the_entire_period',['ebit'=>number_format($keySummary[0]->EBIT,2,'.',',')]) !!}

                    </li>
                @else
                    <li>
                        {!! trans('financial_analysis/conclusion.earnings_before_interest',['ebit'=>number_format($keySummary[0]->EBIT,2,'.',',')]) !!}
                    </li>
                @endif
            @endif
        @endif

        {{-- Comprehensive Income --}}
        @if($financial_report)
            @if($income_statements[0]->ComprehensiveIncome > 0)
                <li>
                   
                    {!! trans('financial_analysis/conclusion.the_income_from_financial',['comprehensiveIncome'=>number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}

                </li>
            @endif
        @endif

    </ul>
    {{-- @endif --}}

    @php $thebad = 0; @endphp
	@if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
       @php $thebad++; @endphp
    @elseif($keySummary[0]->DebtRatio > 1)
        @php $thebad++; @endphp
    @endif

    {{-- Working Capital and Inventories --}}
    @if($keySummary[0]->NWC > 0)
        @if($keySummary[0]->InventoryNWC > 1.0)
            @php $thebad++; @endphp
        @endif
    @else
        @php $thebad++; @endphp
    @endif

    {{-- Current Ratio --}}
    @if($keySummary[0]->CurrentRatio < 1)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
        @php $thebad++; 0 @endphp
    @endif

    {{-- Quick Ratio --}}
    @if($keySummary[0]->QuickRatio < 0.5)
        @php $thebad++; 0 @endphp
    @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
        @php $thebad++; @endphp
    @endif

    {{-- Cash Ratio --}}
    @if($keySummary[0]->CashRatio < 0.05)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
        @php $thebad++; @endphp
    @endif

    {{-- Return of Equity --}}
    @if($keySummary[0]->ROE < 0)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
        @php $thebad++; @endphp
    @endif

    {{-- ROA --}}
    @if($keySummary[0]->ROA < 0)
        @php $thebad++; 0 @endphp
    @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
        @php $thebad++; @endphp
    @endif

    {{-- Net Worth --}}
    @if($financial_report)
        @if($balance_sheets[0]->IssuedCapital > 0)
            @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                @php $thebad++; @endphp
            @endif
        @endif
    @endif

    {{-- EBIT --}}
    @if($keySummary[0]->EBIT < 0)
        @php $thebad++; @endphp
    @endif

    {{-- comprehensive income --}}
    @if($financial_report)
        @if($income_statements[0]->ComprehensiveIncome < 0)
            @php $thebad++; @endphp
        @endif
    @endif

    {{-- @if(($keySummary->DebtRatio > 0.6) && ($keySummary->DebtRatio > 1)) --}}
    @if($thebad > 0)
    <p>{{ trans('financial_analysis/conclusion.the_analysis_revealed') }}</p>
    @endif
    <ul>
        {{-- Debt-to-equity Ratio --}}
        @if(($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1))
            <li>

                {!! trans('financial_analysis/conclusion.the_debt_ratio_has_an_unsatisfactory',['debtRatio'=>number_format($keySummary[0]->DebtRatio, 2, '.', ','), 'debtRatioPercentage'=>number_format(($keySummary[0]->DebtRatio*100), 2, '.', ',')]) !!}
            </li>
        @elseif($keySummary[0]->DebtRatio > 1)
            <li>

                {!! trans('financial_analysis/conclusion.the_debt_ratio_critical',['debtRatio'=>number_format($keySummary[0]->DebtRatio, 2, '.', ',')]) !!}

            </li>
        @endif

        {{-- Working Capital and Inventories --}}
        @if($keySummary[0]->NWC > 0)
            @if($keySummary[0]->InventoryNWC > 1.0)
                <li>{!! trans('financial_analysis/conclusion.available_working_capital') !!}
                </li>
            @endif
        @else
            <li>{!! trans('financial_analysis/conclusion.not_enough_long_term') !!}</li>
        @endif

        {{-- Current Ratio --}}
        @if($keySummary[0]->CurrentRatio < 1)
            <li>
                {!! trans('financial_analysis/conclusion.the_current_ratio_standard',['currentRatio'=>number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}

            </li>
        @elseif(($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2))
            <li>
                {!! trans('financial_analysis/conclusion.the_current_ratio_normal',['currentRatio'=>number_format($keySummary[0]->CurrentRatio, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Quick Ratio --}}
        @if($keySummary[0]->QuickRatio < 0.5)
            <li>

                {!! trans('financial_analysis/conclusion.liquid_assets_current_assets',['quickRatio'=>number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}

            </li>
        @elseif($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1)
            <li>
                {!! trans('financial_analysis/conclusion.liquid_assets_current_assets_acceptable',['quickRatio'=>number_format($keySummary[0]->QuickRatio, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Cash Ratio --}}
        @if($keySummary[0]->CashRatio < 0.05)
            <li>
                {!! trans('financial_analysis/conclusion.the_cash_ratio_is_deficit',['cashRatio'=>number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}
            </li>
        @elseif(($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20))
            <li>
              
                {!! trans('financial_analysis/conclusion.the_cash_ratio_is_equal',['cashRatio'=>number_format($keySummary[0]->CashRatio, 2, '.', ',')]) !!}
            </li>
        @endif

        {{-- Return of Equity --}}
        @if($keySummary[0]->ROE < 0)
            <li>
              
                {!! trans('financial_analysis/conclusion.return_on_equity_roe_critical',['roe'=> session('finalROE')]) !!}

            </li>
        @elseif(($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12))
            <li>

                {!! trans('financial_analysis/conclusion.return_on_equity_roe',['roePercentage'=>session('finalROE')]) !!}
            </li>
        @endif

        {{-- ROA --}}
        @if($keySummary[0]->ROA < 0)
            <li>
                {!! trans('financial_analysis/conclusion.critical_return_on_assets',['year'=>$keySummary[0]->Year]) !!}
            </li>
        @elseif(($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06))
            <li>

                {!! trans('financial_analysis/conclusion.unsatisfactory_return_on_assets',['roa'=>number_format($keySummary[0]->ROA*100,1,'.', ','),'year'=>$keySummary[0]->Year]) !!}

            </li>
        @endif

        {{-- Net Worth --}}
        @if($financial_report)
            @if($balance_sheets[0]->IssuedCapital)
                @if($keySummary[0]->NetAsset < $balance_sheets[0]->IssuedCapital)
                    <li>
                       
                        {!! trans('financial_analysis/conclusion.net_worth_is_less_than',['issuedCapital'=>number_format(($balance_sheets[0]->IssuedCapital - $keySummary[0]->NetAsset),2,'.',','),'year'=>$keySummary[0]->Year]) !!}
                    </li>
                @endif
            @endif
        @endif

        {{-- EBIT --}}
        @if($keySummary[0]->EBIT < 0)
            <li>
                {!! trans('financial_analysis/conclusion.during_the_entire_period_ebit',['ebit'=> number_format($keySummary[0]->EBIT,2,".",",")]) !!}
            </li>
        @endif

        {{-- comprehensive income --}}
        @if($financial_report)
            @if($income_statements[0]->ComprehensiveIncome < 0)
                <li>
                    {!! trans('financial_analysis/conclusion.the_income_from_financial_operational',['comprehensiveIncome'=>number_format($income_statements[0]->ComprehensiveIncome,2,'.',',')]) !!}
                </li>
            @endif
        @endif


    </ul>
    {{-- @endif --}}
</div>
