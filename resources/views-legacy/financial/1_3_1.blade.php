<h3>1.3.1. Key ratios of the company's financial sustainability</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Ratio</td>
            <td class="center" colspan="{{ @count($balance_sheets) }}">Value</td>
            <td class="center" rowspan="2">Change (col.5-col.2)</td>
            <td class="center" rowspan="2">Description of the ratio and its recommended value</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>Debt-to-equity ratio (financial leverage)</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // (FinancialLeverage)
                    $FinancialLeverage = $balance_sheets[$x]->Liabilities /
                        $balance_sheets[$x]->Equity;
                    $class = ($FinancialLeverage >= 0 && $FinancialLeverage <= 1.5) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{number_format($FinancialLeverage, 2, '.', ',')}}
                </td>
                <?php
                    if($x == 0) $textcontent1 = number_format($FinancialLeverage, 2, '.', ',');
                ?>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->Liabilities / $balance_sheets[0]->Equity) - 
                    ($balance_sheets[@count($balance_sheets) - 1]->Liabilities /
                    $balance_sheets[@count($balance_sheets) - 1]->Equity);
                $class = ($Change >= 0) ? 'green' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                A debt-to-equity ratio
                is calculated by taking
                the total liabilities and
                dividing it by
                shareholders' equity. It
                is the key financial
                ratio and used as a
                standard for judging a
                company's financial
                standing.
                Acceptable value: 1.5
                or less (optimum 0.43-
                1).
            </td>
        </tr>
        <tr>
            <td>Debt ratio (debt to assets ratio)</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // (DebtRatio)
                    $DebtRatio = $balance_sheets[$x]->Liabilities /
                        $balance_sheets[$x]->Assets;
                    $class = ($DebtRatio <= 0.6) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{number_format($DebtRatio, 2, '.', ',')}}
                </td>
                <?php
                    if($x == 0) {
                        $textcontent2 = number_format($DebtRatio, 2, '.', ',');
                        $textcontent4 = number_format($DebtRatio * 100, 1, '.', ',');
                    }
                ?>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->Liabilities / $balance_sheets[0]->Assets) - 
                    ($balance_sheets[@count($balance_sheets) - 1]->Liabilities /
                    $balance_sheets[@count($balance_sheets) - 1]->Assets);
                $class = ($Change >= 0) ? 'green' : 'red';
                $textcontent3 = number_format($Change, 2, '.', ',');
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                A debt ratio is
                calculated by dividing
                total liabilities (i.e.
                long-term and shortterm
                liabilities) by total
                assets. It shows how
                much the company
                relies on debt to
                finance assets (similar
                to debt-to-equity ratio).
                Normal value: no more
                than 0.6 (optimum 0.3-
                0.5).
            </td>
        </tr>
        <tr>
            <td>Long-term debt to Equity</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // (LTDtoE)
                    $LTDtoE = $balance_sheets[$x]->NoncurrentLiabilities /
                        $balance_sheets[$x]->Equity;
                ?>
                <td class="center">
                    {{number_format($LTDtoE, 2, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->NoncurrentLiabilities / $balance_sheets[0]->Equity) - 
                    ($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                    $balance_sheets[@count($balance_sheets) - 1]->Equity);
                $class = ($Change >= 0) ? 'green' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                This ratio is calculated
                by dividing long-term
                (non-current) liabilities
                by equity.
            </td>
        </tr>
        <tr>
            <td>Non-current assets to Net worth</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // NCAtoNW
                    $NCAtoNW = $balance_sheets[$x]->NoncurrentAssets /
                        $balance_sheets[$x]->Equity;
                    $class = ($NCAtoNW >= 0 && $NCAtoNW <= 1.25) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{number_format($NCAtoNW, 2, '.', ',')}}
                </td>
                <?php
                    if($x == 0) $textcontent5 = number_format($NCAtoNW, 2, '.', ',');
                ?>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->NoncurrentAssets / $balance_sheets[0]->Equity) - 
                    ($balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets /
                    $balance_sheets[@count($balance_sheets) - 1]->Equity);
                $class = ($Change >= 0) ? 'green' : 'red';
                $textcontent6 = number_format($Change, 2, '.', ',');
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                This ratio is calculated
                by dividing long-term
                (non-current) liabilities
                by net worth (equity)
                and measures the
                extent of a company's
                investment in lowliquidity
                non-current
                assets. This ratio is
                important for
                comparison analysis
                because it's less
                dependent on industry
                (structure of
                company's assets)
                than debt ratio and
                debt-to-equity ratio.
                Normal value: no more 
                than 1.25.
            </td>
        </tr>
        <tr>
            <td>Capitalization ratio</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // Capitalization
                    $Capitalization = $balance_sheets[$x]->NoncurrentLiabilities /
                        ($balance_sheets[$x]->NoncurrentLiabilities + $balance_sheets[$x]->Equity);
                ?>
                <td class="center">
                    {{number_format($Capitalization, 2, '.', ',')}}
                </td>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->NoncurrentLiabilities /
                        ($balance_sheets[0]->NoncurrentLiabilities + $balance_sheets[0]->Equity)) -
                        ($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                        ($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities + $balance_sheets[@count($balance_sheets) - 1]->Equity));
                $class = ($Change >= 0) ? 'green' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                Calculated by dividing
                non-current liabilities
                by the sum of equity
                and non-current
                liabilities.
            </td>
        </tr>
        <tr>
            <td>Fixed assets to Net worth</td>
            @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // FixedAssets
                    $FixedAssets = $balance_sheets[$x]->PropertyPlantAndEquipment +
                                $balance_sheets[$x]->InvestmentProperty + 
                                $balance_sheets[$x]->NoncurrentBiologicalAssets;
                    // FAtoNW
                    $FAtoNW = $FixedAssets / $balance_sheets[$x]->Equity;
                    $class = ($FAtoNW >= 0 && $FAtoNW <= 0.75) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{number_format($FAtoNW, 2, '.', ',')}}
                </td>
            @endfor
            <?php
                $FixedAssets1 = $balance_sheets[0]->PropertyPlantAndEquipment +
                                $balance_sheets[0]->InvestmentProperty + 
                                $balance_sheets[0]->NoncurrentBiologicalAssets;
                $FixedAssets2 = $balance_sheets[@count($balance_sheets) - 1]->PropertyPlantAndEquipment +
                                $balance_sheets[@count($balance_sheets) - 1]->InvestmentProperty + 
                                $balance_sheets[@count($balance_sheets) - 1]->NoncurrentBiologicalAssets;
                                
                $Change = ($FixedAssets1 / $balance_sheets[0]->Equity) -
                            ($FixedAssets2 / $balance_sheets[@count($balance_sheets) - 1]->Equity);
                $class = ($Change >= 0) ? 'green' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                This ratio indicates the
                extent to which the
                owners' cash is frozen
                in the form of fixed
                assets, such as
                property, plant, and
                equipment, investment
                property and noncurrent
                biological
                assets.
                Acceptable value: no
                more than 0.75.
            </td>
        </tr>
        <tr>
            <td>Current liability ratio</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    // CLiabilityRatio
                    if($balance_sheets[$x]->Liabilities!=0)
                        $CLiabilityRatio = $balance_sheets[$x]->CurrentLiabilities /
                            $balance_sheets[$x]->Liabilities;
                    else
                        $CLiabilityRatio = 0;
                ?>
                <td class="center">
                    {{number_format($CLiabilityRatio, 2, '.', ',')}}
                </td>
                <?php
                    if($x == 0) $textcontent7 = $CLiabilityRatio;
                ?>
            @endfor
            <?php
                if($balance_sheets[0]->Liabilities!=0 && $balance_sheets[@count($balance_sheets) - 1]->Liabilities!=0)
                    $Change = ($balance_sheets[0]->CurrentLiabilities /
                            $balance_sheets[0]->Liabilities) - 
                            $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities /
                            $balance_sheets[@count($balance_sheets) - 1]->Liabilities;
                else
                    $Change = 0;
                $class = ($Change >= 0) ? 'green' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td width="30%">
                Current liability ratio is
                calculated by dividing
                non-current liabilities
                by total (i.e. current
                and non-current)
                liabilities.
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">The debt-to-equity ratio and debt ratio are the main coefficients describing financial stability.
The ratios are similar in their meaning and indicate a relationship between two main sources of
capital: equity and borrowed capital. The difference between the ratios is that the first one is
calculated as a relationship of the borrowed capital (liabilities) to the equity, while the second ratio is
calculated as a relationship of the liabilities to the overall capital (i.e. the sum of equity and
liabilities).</p>

<p style="text-indent: 50px;">The debt-to-equity amounted to <span style="color: GREEN">{{$textcontent1}}</span> on 12/31/{{ (int)$financial_report->year }}.
On 12/31/{{ (int)$financial_report->year }}, the debt ratio was equal
to <span style="color: GREEN">{{$textcontent2}}</span>. An alteration in the debt ratio was <span style="color: RED">{{$textcontent3}}</span> during the {{@count($balance_sheets)-1}} years.</p>

<p style="text-indent: 50px;">The debt ratio describes {{ $financial_report->title }}'s financial condition as a good one
on 12/31/{{ (int)$financial_report->year }}, the percentage of liabilities is {{$textcontent4}}%, while a maximum acceptable percentage is
deemed to be 60%. At the beginning of the analyzed period, the debt ratio did not correspond to the
norm, but later the situation changed.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_3_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>

<p style="text-indent: 50px;">According to common rules, non-current investments should be made, in the first place, with
the help of the most stable source of financing, i.e. with the help of own capital (equity). The noncurrent
assets to Net worth ratio shows if this rule is followed. On the last day of the period analyzed
(12/31/{{ (int)$financial_report->year }}), the ratio was <span style="color: GREEN">{{$textcontent5}}</span>. For the last {{@count($balance_sheets)-1}} years, the ratio was verified to go up 
quickly by <span style="color: GREEN">{{$textcontent6}}</span>.
At the end of the period reviewed, the ratio shows a very good value.</p>

<p style="text-indent: 50px;">The current liability ratio, which equaled {{number_format($textcontent7,2,'.',',')}} at the end of the period, shows that the total
liabilities are divided in almost equal parts with long and short-term debts ({{number_format(100-($textcontent7*100),0,'.',',')}}% and {{number_format(($textcontent7*100),0,'.',',')}}%,
respectively).
</p>

<p style="text-indent: 50px;">The following chart demonstrates the dynamics of the main ratios of financial stability of
{{ $financial_report->title }}.</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_3_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>
