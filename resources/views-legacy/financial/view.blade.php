@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p><strong>Entity Name: </strong><span>{{$financial_report->title}}</span></p>
				<p><strong>Industry: </strong><span>{{$industry}}</span></p>
				<p><strong>Currency: </strong><span>{{$financial_report->money_factor}} {{$financial_report->currency}}</span></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h3>Statement of Financial Position (Balance Sheet)</h3>
				<div class="table-responsive">
					<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
						<tbody>
							<tr>
								<td>&nbsp;</td>
								@for($x=0; $x < @count($balance_sheets); $x++)
									<td>{{ ((int)$financial_report->year - $x) }}</td>
								@endfor
							</tr>
							<tr>
								<td><b>NON-CURRENT ASSETS</b></td>
								@foreach($balance_sheets as $bs)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>Property, plant and equipment</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->PropertyPlantAndEquipment}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Investment property</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->InvestmentProperty}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Goodwill</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->Goodwill}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Intangible assets other than goodwill</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->IntangibleAssetsOtherThanGoodwill}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Investment accounted for using equity method</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->InvestmentAccountedForUsingEquityMethod}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Investments in subsidiaries, joint ventures and associates</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->InvestmentsInSubsidiariesJointVenturesAndAssociates}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-current biological assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentBiologicalAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Trade and other non-current receivables</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentReceivables}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-current inventories</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentInventories}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Deferred tax assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->DeferredTaxAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current tax assets, non-current</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentTaxAssetsNoncurrent}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other non-current financial assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherNoncurrentFinancialAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other non-current non-financial assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherNoncurrentNonfinancialAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentNoncashAssetsPledgedAsCollateral}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Non-current assets:</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentAssets}}</td>
								@endforeach
							</tr>

							<tr>
								<td><b>CURRENT ASSETS</b></td>
								@foreach($balance_sheets as $bs)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>Current inventories</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->Inventories}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Trade and other current receivables</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->TradeAndOtherCurrentReceivables}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current tax assets, current</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentTaxAssetsCurrent}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current biological assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentBiologicalAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other current financial assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherCurrentFinancialAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other current non-financial assets</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherCurrentNonfinancialAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Cash and cash equivalents</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CashAndCashEquivalents}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentNoncashAssetsPledgedAsCollateral}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-current assets or disposal groups classified as held for sale or as held for distribution to owners</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentAssetsOrDisposalGroups}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Current assets</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>ASSETS</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->Assets}}</td>
								@endforeach
							</tr>

							<tr><td colspan="{{@count($balance_sheets) + 1}}"></td></tr>

							<tr>
								<td><b>EQUITY</b></td>
								@foreach($balance_sheets as $bs)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>Issued (share) capital</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->IssuedCapital}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Share premium</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->SharePremium}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Treasury shares</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->TreasuryShares}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other equity interest</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherEquityInterest}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other reserves</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherReserves}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Retained earnings</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->RetainedEarnings}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-controlling interests</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncontrollingInterests}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Equity</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->Equity}}</td>
								@endforeach
							</tr>

							<tr>
								<td><b>NON-CURRENT LIABILITIES</b></td>
								@foreach($balance_sheets as $bs)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>Non-current provisions for employee benefits</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentProvisionsForEmployeeBenefits}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other non-current provisions</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherLongtermProvisions}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Trade and other non-current payables</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentPayables}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Deferred tax liabilities</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->DeferredTaxLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current tax liabilities, non-current</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentTaxLiabilitiesNoncurrent}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other long-term financial liabilities</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherNoncurrentFinancialLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other non-current non-financial liabilities</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherNoncurrentNonfinancialLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Non-current liabilities</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->NoncurrentLiabilities}}</td>
								@endforeach
							</tr>

							<tr>
								<td><b>CURRENT LIABILITIES</b></td>
								@foreach($balance_sheets as $bs)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>Current provisions for employee benefits</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentProvisionsForEmployeeBenefits}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other current provisions</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherShorttermProvisions}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Trade and other current payables</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->TradeAndOtherCurrentPayables}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Current tax liabilities, current</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentTaxLiabilitiesCurrent}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other current financial liabilities</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherCurrentFinancialLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other current non-financial liabilities</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->OtherCurrentNonfinancialLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Liabilities included in disposal groups classified as held for sale</td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->LiabilitiesIncludedInDisposalGroups}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Current liabilities</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->CurrentLiabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Liabilities</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->Liabilities}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>EQUITY AND LIABILITIES</b></td>
								@foreach($balance_sheets as $bs)
									<td>{{$bs->EquityAndLiabilities}}</td>
								@endforeach
							</tr>
						</tbody>
					</table>
				</div>
				<h3>Statement of Comprehensive Income (P&L Statement)</h3>
				<div class="table-responsive">
					<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
						<tbody>
							<tr>
								<td>&nbsp;</td>
								@for($x=0; $x < @count($income_statements); $x++)
									<td>{{ ((int)$financial_report->year - $x) }}</td>
								@endfor
							</tr>
							<tr>
								<td>Revenue</td>
								@foreach($income_statements as $is)
									<td>{{$is->Revenue}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Cost of sales</td>
								@foreach($income_statements as $is)
									<td>{{$is->CostOfSales}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Gross profit</b></td>
								@foreach($income_statements as $is)
									<td>{{$is->GrossProfit}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other income</td>
								@foreach($income_statements as $is)
									<td>{{$is->OtherIncome}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Distribution costs</td>
								@foreach($income_statements as $is)
									<td>{{$is->DistributionCosts}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Administrative expense</td>
								@foreach($income_statements as $is)
									<td>{{$is->AdministrativeExpense}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other expense</td>
								@foreach($income_statements as $is)
									<td>{{$is->OtherExpenseByFunction}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other gains (losses)</td>
								@foreach($income_statements as $is)
									<td>{{$is->OtherGainsLosses}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Profit (loss) from operating activities</b></td>
								@foreach($income_statements as $is)
									<td>{{$is->ProfitLossFromOperatingActivities}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Difference between carrying amount of dividends payable and carrying amount of non-cash assets distributed</td>
								@foreach($income_statements as $is)
									<td>{{$is->DifferenceBetweenCarryingAmountOfDividendsPayable}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Gains (losses) on net monetary position</td>
								@foreach($income_statements as $is)
									<td>{{$is->GainsLossesOnNetMonetaryPosition}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Gain (loss) arising from derecognition of financial assets measured at amortised cost</td>
								@foreach($income_statements as $is)
									<td>{{$is->GainLossArisingFromDerecognitionOfFinancialAssets}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Finance income</td>
								@foreach($income_statements as $is)
									<td>{{$is->FinanceIncome}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Finance costs</td>
								@foreach($income_statements as $is)
									<td>{{$is->FinanceCosts}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Share of profit (loss) of associates and joint ventures accounted for using equity method</td>
								@foreach($income_statements as $is)
									<td>{{$is->ShareOfProfitLossOfAssociates}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Gains (losses) arising from difference between previous carrying amount and fair value of financial assets reclassified as measured at fair value</td>
								@foreach($income_statements as $is)
									<td>{{$is->GainsLossesArisingFromDifference}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Profit (loss) before tax</b></td>
								@foreach($income_statements as $is)
									<td>{{$is->ProfitLossBeforeTax}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Income tax expense (from continuing operations)</td>
								@foreach($income_statements as $is)
									<td>{{$is->IncomeTaxExpenseContinuingOperations}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Profit (loss) from continuing operations</td>
								@foreach($income_statements as $is)
									<td>{{$is->ProfitLossFromContinuingOperations}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Profit (loss) from discontinued operations</td>
								@foreach($income_statements as $is)
									<td>{{$is->ProfitLossFromDiscontinuedOperations}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>Profit (loss)</b></td>
								@foreach($income_statements as $is)
									<td>{{$is->ProfitLoss}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Other comprehensive income</td>
								@foreach($income_statements as $is)
									<td>{{$is->OtherComprehensiveIncome}}</td>
								@endforeach
							</tr>
							<tr>
								<td><b>COMPREHENSIVE INCOME</b></td>
								@foreach($income_statements as $is)
									<td>{{$is->ComprehensiveIncome}}</td>
								@endforeach
							</tr>

						</tbody>
					</table>
				</div>
				<h3>Cashflow Statement</h3>
				<div class="table-responsive">
					<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
						<tbody>
							<tr>
								<td>&nbsp;</td>
								@for($x=0; $x < @count($cash_flow); $x++)
									<td>{{ ((int)$financial_report->year - $x) }}</td>
								@endfor
							</tr>
							<tr>
								<td>Amortization</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->Amortization}}</td>
								@endforeach
							</tr>
							<tr>
								<td>Depreciation</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->Depreciation}}</td>
								@endforeach
							</tr>
							<tr>
								<td>InterestExpense</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->InterestExpense}}</td>
								@endforeach
							</tr>
							<tr>
								<td>NonCashExpenses</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->NonCashExpenses}}</td>
								@endforeach
							</tr>
							<tr>
								<td>NetOperatingIncome</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->NetOperatingIncome}}</td>
								@endforeach
							</tr>
							<tr>
								<td>&nbsp;</td>
								@foreach($cash_flow as $cf)
									<td>&nbsp;</td>
								@endforeach
							</tr>
							<tr>
								<td>PrincipalPayments</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->PrincipalPayments}}</td>
								@endforeach
							</tr>
							<tr>
								<td>InterestPayments</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->InterestPayments}}</td>
								@endforeach
							</tr>
							<tr>
								<td>LeasePayments</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->LeasePayments}}</td>
								@endforeach
							</tr>
							<tr>
								<td>DebtServiceCapacity</td>
								@foreach($cash_flow as $cf)
									<td>{{$cf->DebtServiceCapacity}}</td>
								@endforeach
							</tr>

							<tr>
								<td>&nbsp;</td>
								@foreach($cash_flow as $cf)
									<td>&nbsp;</td>
								@endforeach
							</tr>

							<tr>
								<td><b>DebtServiceCapacityRatio</b></td>
								@foreach($cash_flow as $cf)
									<td><b>{{$cf->DebtServiceCapacityRatio}}</b></td>
								@endforeach
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
