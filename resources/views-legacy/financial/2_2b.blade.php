
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Profitability Ratios</td>
            <td class="center" colspan="{{ @count($income_statements) }}">Value in %</td>
            <td class="center" rowspan="2">Change (col.4 - col.2)</td>
            <td class="center" rowspan="2">Description of the ratio and its reference value</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($income_statements); $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td width="12%">Return on equity (ROE)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ROE = ($income_statements[$x]->ProfitLoss / 
                                    (($balance_sheets[$x]->Equity + 
                                $balance_sheets[$x + 1]->Equity) / 2)) * 100;
                    if($x == @count($income_statements)-1) $ROE1 = round($ROE, 1);
                    if($x == 0) $ROE2 = round($ROE, 1);
                    $class = ($ROE >= 12) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($ROE != 0)
                        {{ number_format($ROE, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent3 = number_format($ROE, 1, '.', ','); ?>
            @endfor
            <?php 
                $Change = $ROE2 - $ROE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROE is calculated by taking a year's worth of
                earnings (net profit) and dividing them by the
                average shareholder equity for that period, and is
                expressed as a percentage. It is one of the most 
                important financial ratios and profitability metrics.
                Acceptable value: 12% or more.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Return on assets (ROA)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ROA = ($income_statements[$x]->ProfitLoss / 
                                    (($balance_sheets[$x]->Assets + 
                                $balance_sheets[$x + 1]->Assets) / 2)) * 100;
                    if($x == @count($income_statements)-1) $ROA1 = round($ROA, 1);
                    if($x == 0) $ROA2 = round($ROA, 1);
                    $class = ($ROA >= 6) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($ROA != 0)
                        {{ number_format($ROA, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent1 = number_format($ROA, 1, '.', ','); ?>
            @endfor
            <?php 
                $Change = $ROA2 - $ROA1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 1, '.', ',');
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROA is calculated by dividing net income by total
                assets, and displayed as a percentage. Acceptable
                value: 6% or more.
            </td>
        </tr>
        
        <tr>
            <td width="12%">Return on capital employed (ROCE)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ROCE = (
                                (
                                $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts
                                ) / 
                                (
                                    (
                                    $balance_sheets[$x]->Equity + 
                                    $balance_sheets[$x]->NoncurrentLiabilities + 
                                    $balance_sheets[$x + 1]->Equity + 
                                    $balance_sheets[$x + 1]->NoncurrentLiabilities
                                    ) / 
                                2)
                            ) * 100;
                    if($x == @count($income_statements)-1) $ROCE1 = round($ROCE, 1);
                    if($x == 0) $ROCE2 = round($ROCE, 1);
                ?>
                <td class="center">
                    @if($ROCE != 0)
                        {{ number_format($ROCE	, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = $ROCE2 - $ROCE1;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <td width="50%">
                ROCE is calculated by dividing EBIT by capital
                employed (equity plus non-current liabilities). It
                indicates the efficiency and profitability of a
                company's capital investments.
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">The return on assets was equal to <span style="color: GREEN">{{$textcontent1}}</span>% during the year 2014. During the entire period
analyzed, it was found that there was an obvious growth in the return on assets (<span style="color: GREEN">{{$textcontent2}}</span>%). At the
beginning of the analyzed period, the return on assets was not normal, but later it was.
</p>

<p style="text-indent: 50px;">The most important ratio of business profitability is the return on equity (ROE), which reflects
the profitability of investments by the owners. During the last year, a return on equity was <span style="color: GREEN">{{$textcontent3}}</span>% per
annum. It is a normal return on investment. Nonetheless, the rate should be assessed not only in
absolute terms, but also by taking into account other factors, including macroeconomic rates (for
example, the country's inflation rate).
</p>

<p style="text-indent: 50px;">The following chart demonstrates the dynamics of the main rates of return on total assets and
equity of {{ $financial_report->title }} for the whole period reviewed.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_2_".$income_statements[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>