    <h3>1.2. Net Assets (Net Worth)</h3>
    
    <table>
        <thead>
        <tr class="center">
            <td rowspan="3">Indicator</td>
            <td colspan="{{ count($balance_sheets)+2 }}">Value</td>
            <td colspan="2">Change</td>
        </tr>
        <tr class="center">
            <td colspan="2">in PHP</td>
            <td colspan="{{ count($balance_sheets) }}">% of the balance total</td>
            <td rowspan="2">PHP (col.3-col.2)</td>
            <td rowspan="2">% ((col.3-col.2):col.2)</td>
        </tr>
        <tr class="center">
            <td>at the beginning of the period analyzed(12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td>at the end of the period analyzed(12/31/{{ (int)$financial_report->year }})</td>
            @for($x=count($balance_sheets); $x > 0 ; $x--)
                <td>12/31/{{ ($financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr class="center">
            @for($indexCount = 1; $indexCount <= (count($balance_sheets)+5); $indexCount++)
                <td> {{ $indexCount }}</td>
            @endfor
        </tr>
        </thead>
        <tbody>
            @php 
                // beginning of the period
                $b_intangible_assets = $balance_sheets[count($balance_sheets)-1]->Goodwill+$balance_sheets[count($balance_sheets)-1]->IntangibleAssetsOtherThanGoodwill;
                $b_net_tangible_assets = $balance_sheets[count($balance_sheets)-1]->Equity-$b_intangible_assets;
                $b_net_assets = $balance_sheets[count($balance_sheets)-1]->Equity;
                $b_issued_capital = $balance_sheets[count($balance_sheets)-1]->IssuedCapital;
                $b_difference = $b_net_assets-$b_issued_capital;

                // end of the period
                $e_intangible_assets = $balance_sheets[0]->Goodwill+$balance_sheets[0]->IntangibleAssetsOtherThanGoodwill;
                $e_net_tangible_assets = $balance_sheets[0]->Equity-$e_intangible_assets;
                $e_net_assets = $balance_sheets[0]->Equity;
                $e_issued_capital = $balance_sheets[0]->IssuedCapital;
                $e_difference = $e_net_assets-$e_issued_capital;

                // change column
                $net_tangible_assets_change = $e_net_tangible_assets - $b_net_tangible_assets;
                if($b_net_tangible_assets > 0) {
                    $net_tangible_assets_change_prec = ($net_tangible_assets_change/$b_net_tangible_assets)*100;
                } else {
                    $net_tangible_assets_change_prec = 0;
                }
                $net_assets_change = $e_net_assets - $b_net_assets;
                if($b_net_assets > 0) {
                    $net_assets_change_prec = ($net_assets_change/$b_net_assets)*100;
                } else {
                    $net_assets_change_prec = 0;
                }
                $issued_capital_change = $e_issued_capital - $b_issued_capital;
                if($b_issued_capital > 0) {
                    $issued_capital_change_prec = ($issued_capital_change/$b_issued_capital)*100;
                } else {
                    $issued_capital_change_prec = 0;
                }
                $difference_change = $net_assets_change - $issued_capital_change;
                if($b_difference > 0) {
                    $difference_change_prec = ($difference_change/$b_difference)*100;
                } else {
                    $difference_change_prec = 0;
                }

                if($e_issued_capital > 0) {
                    $net_worth_times = $e_net_assets/$e_issued_capital;
                } else {
                    $net_worth_times = 0;
                }
                if($e_issued_capital > 0) {
                    $net_worth_percentage = (($e_net_assets/$e_issued_capital)-1)*100;
                } else {
                    $net_worth_percentage = 0;
                }
                $latest_net_tangible_assets_change = $e_net_assets-$e_net_tangible_assets;
                @endphp
            
            <tr>
                <td>1. Net Tangible assets</td>
                <td class="center"> {{ ($b_net_tangible_assets != 0) ? number_format($b_net_tangible_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_tangible_assets != 0) ? number_format($e_net_tangible_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_intangible_assets = $balance_sheets[$x]->Goodwill+$balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill;
                        $i_net_tangible_assets = $balance_sheets[$x]->Equity-$i_intangible_assets;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_tangible_assets_prec = ($i_net_tangible_assets/$i_assets)*100;
                        } else {
                            $i_net_tangible_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_tangible_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_tangible_assets_prec != 0) ? round($i_net_tangible_assets_prec,1) : '-' }}</td>
                @endfor
                    <td class="center nowrap {{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_tangible_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @elseif($net_tangible_assets_change > 0)
                        {{ '+'.number_format($net_tangible_assets_change) }}
                    @else
                        {{ $net_tangible_assets_change > 0 ? '+' : '' }}{{ number_format($net_tangible_assets_change) }}
                    @endif
                </td>
                <td class="center {{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">
                    @if($net_tangible_assets_change_prec == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ round($net_tangible_assets_change_prec, 1) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>2. Net assets(Net Worth)</td>
                <td class="center"> {{ ($b_net_assets != 0) ? number_format($b_net_assets) : '-' }}</td>
                <td class="center"> {{ ($e_net_assets != 0) ? number_format($e_net_assets) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_net_assets = $balance_sheets[$x]->Equity;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_net_assets_prec = ($i_net_assets/$i_assets)*100;
                        } else {
                            $i_net_assets_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_net_assets_prec > 0 ? 'green' : 'red' }}"> {{ ($i_net_assets_prec != 0) ? round($i_net_assets_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $net_assets_change > 0 ? 'green' : 'red' }}">
                    @if($net_assets_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $net_assets_change > 0 ? '+' : '' }}{{ number_format($net_assets_change) }}
                    @endif
                </td>
                <td class="center {{ $net_assets_change_prec > 0 ? 'green' : 'red' }}">
                    @if($net_assets_change_prec == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ round($net_assets_change_prec, 1) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>3. Issued (share) capital</td>
                <td class="center"> {{ ($b_issued_capital != 0) ? number_format($b_issued_capital) : '-' }}</td>
                <td class="center"> {{ ($e_issued_capital != 0) ? number_format($e_issued_capital) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_issued_capital = $balance_sheets[$x]->IssuedCapital;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_issued_capital_prec = ($i_issued_capital/$i_assets)*100;
                        } else {
                            $i_issued_capital_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_issued_capital_prec > 0 ? 'green' : 'red' }}"> {{ ($i_issued_capital_prec != 0) ? round($i_issued_capital_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $issued_capital_change > 0 ? 'green' : 'red' }}">
                    @if($issued_capital_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $issued_capital_change > 0 ? '+' : '' }}{{ number_format($issued_capital_change) }}
                    @endif
                </td>
                <td class="center {{ $issued_capital_change_prec > 0 ? 'green' : 'red' }}">
                    @if($issued_capital_change_prec == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ round($issued_capital_change_prec, 1) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>4. Difference between net assets and Issued (share) capital (line 2 - line 3)</td>
                <td class="center {{ $b_difference > 0 ? 'green' : 'red' }}"> {{ ($b_difference != 0) ? number_format($b_difference) : '-' }}</td>
                <td class="center {{ $e_difference > 0 ? 'green' : 'red' }}"> {{ ($e_difference != 0) ? number_format($e_difference) : '-' }}</td>
                @for($x=count($balance_sheets)-1; $x >= 0 ; $x--)
                    @php
                        $i_retained_earnings = $balance_sheets[$x]->RetainedEarnings;
                        $i_assets = $balance_sheets[$x]->Assets;
                        if($i_assets > 0) {
                            $i_difference_prec = ($i_retained_earnings/$i_assets)*100;
                        } else {
                            $i_difference_prec = 0;
                        }
                    @endphp
                    <td class="center {{ $i_difference_prec > 0 ? 'green' : 'red' }}"> {{ ($i_difference_prec != 0) ? round($i_difference_prec,1) : '-' }}</td>
                @endfor
                <td class="center nowrap {{ $difference_change > 0 ? 'green' : 'red' }}">
                    @if($difference_change == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ $difference_change > 0 ? '+' : '' }}{{ number_format($difference_change) }}
                    @endif
                </td>
                <td class="center {{ $difference_change_prec > 0 ? 'green' : 'red' }}">
                    @if($difference_change_prec == 0)
                        <span class="black">{{ '-' }}</span>
                    @else
                        {{ round($difference_change_prec, 1) }}
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <div class="justify">
        <p>
            @if($net_tangible_assets_change > 0)
                @if($net_tangible_assets_change_prec > 40)
                    The net tangible assets were equal to PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_net_tangible_assets) }}</span> at the end of the period reviewed.
                    The net tangible assets sharply rose (PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($net_tangible_assets_change) }}</span>, or by <span class="{{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">{{ round($net_tangible_assets_change_prec, 1) }}</span>%) during the year.
                @elseif($net_tangible_assets_change_prec > 15)
                    For the period analyzed, it was found that there was an appreciable growth in the net tangible assets to PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_net_tangible_assets) }}</span>, which showed PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ $net_tangible_assets_change > 0 ? '+' : '' }}{{ number_format($net_tangible_assets_change) }}</span>.
                @else
                    The net tangible assets climbed somewhatmodestly (by PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($net_tangible_assets_change) }}</span> , or by <span class="{{ $net_tangible_assets_change_prec > 0 ? 'green' : 'red' }}">{{ round($net_tangible_assets_change_prec, 1) }}</span>%) for the entire period analyzed, at the same time there is no linear trend on average, i.e. there is no noticeable increase or falling.
                @endif
            @else
                It was observed that there was an obvious lowering in the net tangible assets from PHP <span class="{{ $b_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($b_net_tangible_assets) }}</span> to PHP <span class="{{ $e_net_tangible_assets > 0 ? 'green' : 'red' }}">{{ number_format($e_net_tangible_assets) }}</span> (PHP <span class="{{ $net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($net_tangible_assets_change) }}</span>) during the period analyzed.
                On the last day of the period analyzed, the intangible assets were equal to PHP <span class="{{ $latest_net_tangible_assets_change > 0 ? 'green' : 'red' }}">{{ number_format($latest_net_tangible_assets_change) }}</span>. This value shows the difference between the value of net tangible assets and all net worth.
            @endif
            @if($e_intangible_assets == 0)
                In this case, {{ $financial_report->title }} has no goodwill or other intangible assets.
                This is why amounts of net worth and net tangible assets are equal at the end of the period reviewed.
            @endif
        </p>
        <p>
            @if($e_difference > 0)
                @if($net_worth_times > 2)
                    The net worth (net assets) of {{ $financial_report->title }} was much higher (by <span class="{{ $net_worth_times > 0 ? 'green' : 'red' }}">{{ round($net_worth_times, 1) }}</span> times) than the share capital on the last day of the period analyzed (12/31/{{ (int)$financial_report->year}}).
                    Such a ratio positively describes the company's financial state. 
                @else
                    The net worth of {{ $financial_report->title }} exceeds the share capital by <span class="{{ $net_worth_percentage > 0 ? 'green' : 'red' }}">{{ round($net_worth_percentage, 1) }}</span>% on 12/31/{{ (int)$financial_report->year}}.
                    Such a situation is normal, the net worth (net assets) should not be less than the share capital.
                @endif
                Net worth is used as a measure of the company's book value (as opposed to a shareholder's value, the value based on expected earnings and other methods used to estimate the company's value). 
                In financial analysis, the amount of net worth (own equity) is one of the key indicators of the property status of the company.
            @else
                on 12/31/{{ (int)$financial_report->year}}, the net worth (net assets) of {{ $financial_report->title }} is less than the share capital (difference makes PHP <span class="{{ $e_difference > 0 ? 'green' : 'red' }}">{{ number_format(abs($e_difference)) }} </span>). 
                It is a negative rate indicating that the company has uncovered a loss. 
                It is deemed to be normal when the net worth (net assets) of the company is higher than the share capital that shows the profitability of the business.
                Net worth is used as a measure of the company's book value (as opposed to a shareholder's value, the value based on expected earnings and other methods used to estimate the company's value).
                In financial analysis, the amount of net worth (own equity) is one of the key indicators of the property status of the company.
            @endif
        </p>
    </div>
    <div class = 'center'>
        <img src = '{{ URL::To("images/graph_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' class="graph_width_75">
    </div>
    @if($b_issued_capital == $e_issued_capital)
        <p>The issued (share) capital did not change during the whole of the analyzed period.</p>
    @elseif($issued_capital_change_prec > 90)
        <p>Increase in the issued (share) capital was abrupt in the analyzed period.</p>
    @elseif($issued_capital_change_prec > 50)
        <p>During the reviewed period, considerable increase in the issued (share) capital took place.</p>
    @endif