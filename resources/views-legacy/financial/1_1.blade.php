<p style="text-indent: 50px;">The analysis given in this report on {{ $financial_report->title }}'s financial state and
activity efficiency is made for the period from 01/01/{{ ((int)$financial_report->year - count($balance_sheets) + 1) }} 
to 12/31/{{ (int)$financial_report->year }} based on the financial
statements data prepared according to International Financial Reporting Standards (IFRS).</p>

<h3>1.1. Structure of the Assets and Liabilities</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="3">Indicator</td>
            <td class="center" colspan="{{ @count($balance_sheets) + 2 }}">Value</td>
            <td class="center" colspan="2">Change for the period analyzed</td>
        </tr>
        <tr dontbreak="true">
            <td class="center" colspan="{{ @count($balance_sheets) }}">in thousand PHP</td>
            <td class="center" colspan="2">% of the balance total</td>
            <td class="center" rowspan="2"><i>thousand PHP</i> (col.{{ @count($balance_sheets) + 1}}-col.2)</td>
            <td class="center" rowspan="2">&plusmn; % ((col.{{ @count($balance_sheets) + 1}}-col.2) : col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">at the beginning of the period analyzed (12/31/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})</td>
            <td class="center">at the end of the period analyzed (12/31/{{ (int)$financial_report->year }})</td>
        </tr>
    

    
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Assets</b></td>
        </tr>

        <tr>
            <td>1. Non-current assets</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentAssets), 0, '.', ',')}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets /
                $balance_sheets[@count($balance_sheets) - 1]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->NoncurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->NoncurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}			
            </td>
            <?php
                $vardata = (($balance_sheets[0]->NoncurrentAssets / $balance_sheets[@count($balance_sheets) - 1]->NoncurrentAssets) - 1) * 100;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Current assets, total</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentAssets), 0, '.', ',')}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets /
                $balance_sheets[@count($balance_sheets) - 1]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->CurrentAssets /
                $balance_sheets[0]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->CurrentAssets - $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}			
            </td>
            <?php
                $vardata = (($balance_sheets[0]->CurrentAssets / $balance_sheets[@count($balance_sheets) - 1]->CurrentAssets) - 1) * 100;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Inventories</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->Inventories > 0) ? number_format(round($balance_sheets[$x]->Inventories), 0, '.', ',') : '-'}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Inventories /
                $balance_sheets[@count($balance_sheets) - 1]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->Inventories /
                $balance_sheets[0]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->Inventories - $balance_sheets[@count($balance_sheets) - 1]->Inventories;
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}	
                @else
                    -
                @endif
            </td>
            <?php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories > 0){
                    $vardata = (($balance_sheets[0]->Inventories / $balance_sheets[@count($balance_sheets) - 1]->Inventories) - 1) * 100;
                }
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Trade and other current receivables</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->TradeAndOtherCurrentReceivables > 0) ? number_format(round($balance_sheets[$x]->TradeAndOtherCurrentReceivables), 0, '.', ',') : '-'}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables /
                $balance_sheets[@count($balance_sheets) - 1]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->TradeAndOtherCurrentReceivables /
                $balance_sheets[0]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->TradeAndOtherCurrentReceivables - $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables;
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}	
                @else
                    -
                @endif
            </td>
            <?php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables > 0){
                    $vardata = (($balance_sheets[0]->TradeAndOtherCurrentReceivables / $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables) - 1) * 100;
                }
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;Cash and cash equivalents</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{ ($balance_sheets[$x]->CashAndCashEquivalents > 0) ? number_format(round($balance_sheets[$x]->CashAndCashEquivalents), 0, '.', ',') : '-'}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents /
                $balance_sheets[@count($balance_sheets) - 1]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->CashAndCashEquivalents /
                $balance_sheets[0]->Assets) * 100, 1);
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                {{ ($vardata > 0) ? number_format($vardata, 1, '.', ',') : '-' }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->CashAndCashEquivalents - $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents;
            ?>
            <td class="center {{ $vardata != 0 ? $vardata > 0 ? 'green' : 'red' : '' }}">
                @if($vardata != 0)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}	
                @else
                    -
                @endif
            </td>
            <?php
                $vardata = 0;
                if($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents > 0){
                    $vardata = (($balance_sheets[0]->CashAndCashEquivalents / $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents) - 1) * 100;
                }
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata==0)
                    -
                @elseif($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td colspan="{{@count($balance_sheets) + 5}}"><b>Equity and Liabilities</b></td>
        </tr>
        <tr>
            <td>1. Equity</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->Equity), 0, '.', ',')}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->Equity /
                $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->Equity /
                $balance_sheets[0]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->Equity - $balance_sheets[@count($balance_sheets) - 1]->Equity;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}			
            </td>
            <?php
                $vardata = (($balance_sheets[0]->Equity / $balance_sheets[@count($balance_sheets) - 1]->Equity) - 1) * 100;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Non-current liabilities</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->NoncurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities /
                $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->NoncurrentLiabilities /
                $balance_sheets[0]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->NoncurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}			
            </td>
            <?php
                if($balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities != 0)
                    $vardata = (($balance_sheets[0]->NoncurrentLiabilities / $balance_sheets[@count($balance_sheets) - 1]->NoncurrentLiabilities) - 1) * 100;
                else
                    $vardata = 0;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>3. Current liabilities</td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center">{{number_format(round($balance_sheets[$x]->CurrentLiabilities), 0, '.', ',')}}</td>
            @endfor
            <?php 
                $vardata = round(($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities /
                $balance_sheets[@count($balance_sheets) - 1]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = round(($balance_sheets[0]->CurrentLiabilities /
                $balance_sheets[0]->EquityAndLiabilities) * 100, 1);
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ number_format($vardata, 1, '.', ',') }}
            </td>
            <?php 
                $vardata = $balance_sheets[0]->CurrentLiabilities - $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}			
            </td>
            <?php
                if($balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0)
                    $vardata = (($balance_sheets[0]->CurrentLiabilities / $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities) - 1) * 100;
                else
                    $vardata = 0;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>Assets; Equity and Liabilities</b></td>
            @for($x=@count($balance_sheets) - 1; $x >= 0 ; $x--)
                <td class="center"><b>{{number_format(round($balance_sheets[$x]->Assets), 0, '.', ',')}}</b></td>
            @endfor
            <td class="center">
                <b>100</b>
            </td>
            <td class="center">
                <b>100</b>
            </td>
            <?php 
                $vardata = $balance_sheets[0]->Assets - $balance_sheets[@count($balance_sheets) - 1]->Assets;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>{{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 0, '.', ',') }}</b>			
            </td>
            <?php
                $vardata = (($balance_sheets[0]->Assets / $balance_sheets[@count($balance_sheets) - 1]->Assets) - 1) * 100;
            ?>
            <td class="center {{ $vardata > 0 ? 'green' : 'red' }}">
                <b>
                @if($vardata < 200)
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format($vardata, 1, '.', ',') }}
                @else
                    {{ $vardata > 0 ? '+' : '' }}{{ number_format(($vardata / 100) + 1, 1, '.', ',') }} <span class="black">times</span>
                @endif
                </b>
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<?php
	$last_index = @count($balance_sheets) - 1;
	$non_current_p = number_format(($balance_sheets[0]->NoncurrentAssets / 
			($balance_sheets[0]->NoncurrentAssets + $balance_sheets[0]->CurrentAssets)) * 100, 1, '.', '');
	$asset_increase = number_format(($balance_sheets[0]->Assets + $balance_sheets[0]->Assets),0,'.',',');
	$asset_percentage = number_format((($balance_sheets[0]->Assets / $balance_sheets[@count($balance_sheets) - 1]->Assets) - 1) * 100, 1, '.', ',');
	$equity_percentage = number_format((($balance_sheets[0]->Equity / $balance_sheets[@count($balance_sheets) - 1]->Equity) - 1) * 100, 1, '.', ',');
?>
<p style="text-indent: 50px;">According to the data given in the table, the share of non-current assets makes about one
third ({{ $non_current_p }}) on the last day of the period analyzed, while current 
assets take two thirds, respectively. Such correlation is typical for the activity types where considerable investing of fixed
assets and long-term investments are not required but where stocks of raw materials, supplies and
goods are plentiful. It was observed that there was an increase in the assets of PHP <span style="color: GREEN">{{ $asset_increase }}</span>
thousand, or of <span style="color: GREEN">{{$asset_percentage}}</span>% (to PHP {{ number_format($balance_sheets[0]->Assets, 0, '.', ',') }} thousand) 
for the period reviewed (from 31 December, {{((int)$financial_report->year - @count($income_statements) + 1)}} to
31 December, {{((int)$financial_report->year)}}). Assets were observed to grow simultaneously with equity. With that, the
equity has grown with outrunning rates (<span style="color: GREEN">+{{ $equity_percentage }}</span>% for the period reviewed 
(31/12/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }} – 12/31/{{ (int)$financial_report->year }}). An
outrunning increase in equity relative to a total change in assets should be considered as a positive
factor.
</p>

<p style="text-indent: 50px;">The increase in total assets of {{ $financial_report->title }} occurred due to the growth of
the following asset types (amount of change and percentage of this change relative to the total
assets growth are shown below):</p>

<ul>
	<?php 
	$negatives_list = array();
	$assets_list = [
		'PropertyPlantAndEquipment' => 'Property, plant and equipment',
		'InvestmentProperty' => 'Investment property',
		'Goodwill' => 'Goodwill',
		'IntangibleAssetsOtherThanGoodwill' => 'Intangible assets other than goodwill',
		'InvestmentAccountedForUsingEquityMethod' => 'Investment accounted for using equity method',
		'InvestmentsInSubsidiariesJointVenturesAndAssociates' => 'Investments in subsidiaries, joint ventures and associates',
		'NoncurrentBiologicalAssets' => 'Non-current biological assets',
		'NoncurrentReceivables' => 'Trade and other non-current receivables',
		'NoncurrentInventories' => 'Non-current inventories',
		'DeferredTaxAssets' => 'Deferred tax assets',
		'CurrentTaxAssetsNoncurrent' => 'Current tax assets, non-current',
		'OtherNoncurrentFinancialAssets' => 'Other non-current financial assets',
		'OtherNoncurrentNonfinancialAssets' => 'Other non-current non-financial assets',
		'NoncurrentNoncashAssetsPledgedAsCollateral' => 'Non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
	
		'Inventories' => 'Current inventories',
		'TradeAndOtherCurrentReceivables' => 'Trade and other current receivables',
		'CurrentTaxAssetsCurrent' => 'Current tax assets, current',
		'CurrentBiologicalAssets' => 'Current biological assets',
		'OtherCurrentFinancialAssets' => 'Other current financial assets',
		'OtherCurrentNonfinancialAssets' => 'Other current non-financial assets',
		'CashAndCashEquivalents' => 'Cash and cash equivalents',
		'CurrentNoncashAssetsPledgedAsCollateral' => 'Current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral',
		'NoncurrentAssetsOrDisposalGroups' => 'Non-current assets or disposal groups classified as held for sale or as held for distribution to owners',
	];
	
	$al_total = 0;
	$al_list = array();
	foreach($assets_list as $k=>$al) {
		$amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
		if($amount > 0){
			$al_list[$k] = array(
				'amount' => $amount,
				'item' => $al
			);
			$al_total += $amount;
		} elseif($amount < 0) {
			$negatives_list[] = array(
				'amount' => $amount,
				'item' => $al,
				'type' => 1
			);
		}
	}
	
	foreach($al_list as $k=>$a){
		$al_percentage = number_format(($a['amount'] / $al_total) * 100, 1, '.', '');
		echo '<li>'.$a['item'].' – PHP <span style="color: GREEN">'.number_format($a['amount'],0,'.',',').'</span> thousand (<span style="color: GREEN">'.$al_percentage.'</span>%)</li>';
	}
	?>
</ul>

<p style="text-indent: 50px;">At the same time, the most significant growth was seen on the following positions in the
section "Equity and Liabilities" of the company's balance sheet (the percentage from total equity
and liabilities change is shown in brackets):
</p>

<ul>
	<?php 
	$assets_list = [
		'IssuedCapital' => 'Issued (share) capital',
		'SharePremium' => 'Share premium',
		'TreasuryShares' => 'Treasury shares',
		'OtherEquityInterest' => 'Other equity interest',
		'OtherReserves' => 'Other reserves',
		'RetainedEarnings' => 'Retained earnings',
		'NoncontrollingInterests' => 'Non-controlling interests',
		
		'NoncurrentProvisionsForEmployeeBenefits' => 'Non-current provisions for employee benefits',
		'OtherLongtermProvisions' => 'Other non-current provisions',
		'NoncurrentPayables' => 'Trade and other non-current payables',
		'DeferredTaxLiabilities' => 'Deferred tax liabilities',
		'CurrentTaxLiabilitiesNoncurrent' => 'Current tax liabilities, non-current',
		'OtherNoncurrentFinancialLiabilities' => 'Other long-term financial liabilities',
		'OtherNoncurrentNonfinancialLiabilities' => 'Other non-current non-financial liabilities',
	
		'CurrentProvisionsForEmployeeBenefits' => 'Current provisions for employee benefits',
		'OtherShorttermProvisions' => 'Other current provisions',
		'TradeAndOtherCurrentPayables' => 'Trade and other current payables',
		'CurrentTaxLiabilitiesCurrent' => 'Current tax liabilities, current',
		'OtherCurrentFinancialLiabilities' => 'Other current financial liabilities',
		'OtherCurrentNonfinancialLiabilities' => 'Other current non-financial liabilities',
		'LiabilitiesIncludedInDisposalGroups' => 'Liabilities included in disposal groups classified as held for sale'
	];
	
	$al_total = 0;
	$al_list = array();
	foreach($assets_list as $k=>$al) {
		$amount = ($balance_sheets[0]->$k - $balance_sheets[@count($balance_sheets) - 1]->$k);
		if($amount > 0){
			$al_list[$k] = array(
				'amount' => $amount,
				'item' => $al
			);
			$al_total += $amount;
		} elseif($amount < 0) {
			$negatives_list[] = array(
				'amount' => $amount,
				'item' => $al,
				'type' => 2
			);
		}
	}
	
	foreach($al_list as $k=>$a){
		$al_percentage = number_format(($a['amount'] / $al_total) * 100, 1, '.', '');
		echo '<li>'.$a['item'].' – PHP <span style="color: GREEN">'.number_format($a['amount'],0,'.',',').'</span> thousand (<span style="color: GREEN">'.$al_percentage.'</span>%)</li>';
	}
	?>
</ul>

<p style="text-indent: 50px;">Total assets of the company did not grow to a greater degree due to a negative change in
items such as 
<?php
$str = '';
foreach($negatives_list as $n){
	if($n['type'] == 1){
		$str .= ' "'.$n['item'].'",';
	}
}
$str = rtrim($str, ",");
echo $str . ' in assets and ';
$str = '';
foreach($negatives_list as $n){
	if($n['type'] == 2){
		$str .= ' "'.$n['item'].'",';
	}
}
$str = rtrim($str, ",");
echo $str;
?>

in the company's sources of finance, which were 
<?php
$str = '';
foreach($negatives_list as $n){
	if($n['type'] == 1){
		$str .= ' PHP <span style="color: RED">'.number_format($n['amount'],0,'.',',').'</span> thousand,';
	}
}
$str = rtrim($str, ",");
echo $str . " and ";
$str = '';
foreach($negatives_list as $n){
	if($n['type'] == 2){
		$str .= ' PHP <span style="color: RED">'.number_format($n['amount'],0,'.',',').'</span> thousand,';
	}
}
$str = rtrim($str, ",");
echo $str;
?>

respectively during the period reviewed.</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>