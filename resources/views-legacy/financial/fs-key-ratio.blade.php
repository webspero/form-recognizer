@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
    <div id="page-wrapper" style="margin-left:0px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p><strong>Entity Name: </strong><span>{{$financial_report->title}}</span></p>
                    <p><strong>Currency: </strong><span>{{$financial_report->money_factor}} {{$financial_report->currency}}</span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3>Key Ratios</h3>
                    <div class="table-responsive">
                        <table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
                            <tbody>
                                <tr>
                                    <th>Year</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->Year }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Intangible Asset</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->IntangibleAsset }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Net Tangible Asset</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->NetTangibleAsset }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Net assets (Net worth)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->NetAsset }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Debt-to-equity ratio (financial leverage)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->FinancialLeverage }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Debt ratio (debt to assets)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->DebtRatio }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Long-term debt to Equity</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->LTDtoE }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Non-current assets to Net worth</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->NCAtoNW }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Fixed assets</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->FixedAsset }}</td>
                                    @endforeach
                                <tr>
                                    <th>Fixed assets to Net worth</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->FAtoNW }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Current liability ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CLiabilityRatio }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Capitalization ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->Capitalization }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Net working capital</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->NWC }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Working capital sufficiency</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->WCD }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Inventory to Working capital ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->InventoryNWC }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Current ratio (acid-test ratio)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CurrentRatio }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Cash ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CashRatio }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Quick ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->QuickRatio }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Other income and expenses, except finance costs</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->OIEeFC }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>EBIT</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->EBIT }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>EBITDA</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->EBITDA }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Gross Margin</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->GrossMargin }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Return on sales (operating margin)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROS }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Profit margin (net profit margin)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ProfitMargin }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Interest coverage ratio</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ICR }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Return on equity (ROE)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROE }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>ROE (using Comprehensive income)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROE_CI }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Return on assets (ROA)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROA }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>ROA (using Comprehensive income)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROA_CI }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Return on capital employed (ROCE)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ROCE }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Receivables turnover (days sales outstanding)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->ReceivablesTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Accounts payable turnover (days payable outstanding)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->PayableTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Inventory turnover (days inventory outstanding)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->InventoryTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Asset turnover (days)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->AssetTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Current asset turnover (days)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CAssetTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Capital turnover (days)</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CapitalTurnover }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Cash conversion cycle</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->CCC }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>T1. Working Capital / Total Assets</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->T1 }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>T2. Retained Earnings / Total Assets</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->T2 }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>T3. Earnings Before Interest and Taxes / Total Assets</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->T3 }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>T4. Equity / Total Liabilities</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->T4 }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>T5. Sales / Total Assets</th>
                                    @foreach($key_ratios as $kr)
                                        <td>{{ $kr->T5 }}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop