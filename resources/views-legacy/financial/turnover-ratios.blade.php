<h3> 2. 3 Analysis of the Business Activity (Turnover Ratios)</h3>
<p>
    Further in the table, the calculated rates of turnover of assets and liabilities describe how fast prepaid
    assets and liabilities to suppliers, contractors and staff are effected. Turnover ratios have strong industry
    specifics and depend on activity. This is why an absolute value of the ratios does not permit making a
    qualitative assessment. When assets turnover ratios are analyzed, an increase in ratios (i.e. velocity of
    circulation) and a reduction in circulation days are deemed to be positive dynamics. There is no well-defined
    interaction for accounts payable and capital turnover. In any case, an accurate conclusion can only be made
    after the reasons that caused these changes are considered.
</p>
<table class="table">
    <tr>
        <td rowspan="2">Turnover Ratio</td>
        <td colspan="{{count($turnoverRatios)}}">Value, Days</td>
        <td rowspan="2">Ratio</td>
        <td rowspan ="2">Ratio</td>
        <td rowspan ="2">Change, days Col - Col</td>
    </tr>
    <tr>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->year}}</td>
        @endforeach
    </tr>
    <tr>
        <td>
            Receivables turnover (days sales outstanding) <br>
            <small>(average trade and other current receivables divided by average daily revenue*)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->ReceivablesTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->ReceivablesTurnover != 0)
                {{number_format($turnover->num_days/$turnover->ReceivablesTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->ReceivablesTurnover - $turnoverRatios[0]->ReceivablesTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Accounts payable turnover (days payable outstanding) <br>
            <small>(average current payables divided by average daily purchases)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->PayableTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->PayableTurnover != 0)
                {{number_format($turnover->num_days/$turnover->PayableTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->PayableTurnover - $turnoverRatios[0]->PayableTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Inventory turnover (days inventory outstanding) <br>
            <small>(average inventory divided by average daily cost of sales)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->InventoryTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->InventoryTurnover != 0)
                {{number_format($turnover->num_days/$turnover->InventoryTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->InventoryTurnover - $turnoverRatios[0]->InventoryTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Asset turnover <br>
            <small>(average total assets divided by average daily revenue)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->AssetTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->AssetTurnover != 0)
                {{number_format($turnover->num_days/$turnover->AssetTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->AssetTurnover - $turnoverRatios[0]->AssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Current asset turnover <br>
            <small>(average current assets divided by average daily revenue)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->CAssetTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->CAssetTurnover != 0)
                {{number_format($turnover->num_days/$turnover->CAssetTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CAssetTurnover - $turnoverRatios[0]->CAssetTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Capital turnover <br>
            <small>(average equity divided by average daily revenue)</small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->CapitalTurnover}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->CapitalTurnover != 0)
                {{number_format($turnover->num_days/$turnover->CapitalTurnover, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CapitalTurnover - $turnoverRatios[0]->CapitalTurnover) }} </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <i>Reference:</i> <br>
            Cash conversion cycle <br>
            <small>(days sales outstanding + days inventory outstanding - days payable outstanding) </small>
        </td>
        @foreach($turnoverRatios as $turnover)
            <td>{{$turnover->CCC}}</td>
        @endforeach
        @foreach($turnoverRatios as $turnover)
        <td>
            @if($turnover->CCC != 0)
                {{number_format($turnover->num_days/$turnover->CCC, 1, '.', ',')}}
            @else
                -
            @endif
        </td>
        @endforeach
        <td>
            @if(($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) >= 0)
                <span style="color:green">+{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @else
                <span style="color:red">{{ round($turnoverRatios[count($turnoverRatios)-1]->CCC - $turnoverRatios[0]->CCC) }} </span>
            @endif
        </td>
        
    </tr>
</table>

<p>
    During the last year, the average collection period (Days Sales Outstanding) was {{$turnoverRatios[0]->ReceivablesTurnover}} days and the
    average days payable outstanding was {{$turnoverRatios[0]->PayableTurnover}} days as shown in the table. The rate of asset turnover means that
    {{$financial_report->title}} gains revenue equal to the sum of all the available assets every 1298 days (on average during the period analyzed).
</p>