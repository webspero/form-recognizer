<h3>1.4. Liquidity Analysis</h3>

<p style="text-indent: 50px;">One of the most widespread indicators of a company's solvency are liquidity related ratios.
The current ratio shows the capacity of a company to meet current liabilities with all available
current assets. Quick ratio describes solvency in the near future. Cash ratio shows if there is
enough means for uninterrupted execution of current transactions. All three ratios for {{ $financial_report->title }} are calculated in the following table.</p>

<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Liquidity Ratio</td>
            <td class="center" colspan="{{ @count($balance_sheets) }}">Value</td>
            <td class="center" rowspan="2">Change (col.5 - col.2)</td>
            <td class="center" rowspan="2">Description of the ratio and its recommended value</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>1. Current ratio (working capital ratio)</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                if($balance_sheets[$x]->CurrentLiabilities != 0)
                    $CurrentRatio = $balance_sheets[$x]->CurrentAssets /
                        $balance_sheets[$x]->CurrentLiabilities;
                else
                    $CurrentRatio = 0;
                    $class = ($CurrentRatio >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{ number_format($CurrentRatio, 2, '.', ',') }}
                </td>
                <?php
                    if($x==0) $textcontent1 = number_format($CurrentRatio, 2, '.', ',');
                ?>
            @endfor
            <?php 
                if($balance_sheets[0]->CurrentLiabilities != 0 && $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0)
                    $Change = ($balance_sheets[0]->CurrentAssets /
                            $balance_sheets[0]->CurrentLiabilities) - 
                            ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets /
                            $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities);
                else
                    $Change = 0;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = ($Change > 0) ? '+' : '';
                $textcontent2 .= number_format($Change, 1, '.', ',');
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
            </td>
            <td width="30%">
                The current ratio is
                calculated by dividing
                current assets by current
                liabilities. It indicates a
                company's ability to meet
                short-term debt obligations.
                Normal value: no less than
                2.
            </td>
        </tr>
        <tr>
            <td>2. Quick ratio (acid-test ratio)</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    if($balance_sheets[$x]->CurrentLiabilities != 0)
                        $QuickRatio = ($balance_sheets[$x]->CashAndCashEquivalents + 
                                        $balance_sheets[$x]->OtherCurrentFinancialAssets + 
                                        $balance_sheets[$x]->TradeAndOtherCurrentReceivables) / 
                                        $balance_sheets[$x]->CurrentLiabilities;
                    else
                        $QuickRatio = 0;
                    $class = ($QuickRatio >= 1) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{ number_format($QuickRatio, 2, '.', ',') }}
                </td>
                <?php
                    if($x==0) $textcontent3 = number_format($QuickRatio, 2, '.', ',');
                ?>
            @endfor
            <?php 
                if($balance_sheets[0]->CurrentLiabilities != 0 && $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0)
                    $Change = (($balance_sheets[0]->CashAndCashEquivalents + 
                            $balance_sheets[0]->OtherCurrentFinancialAssets + 
                            $balance_sheets[0]->TradeAndOtherCurrentReceivables) / 
                            $balance_sheets[0]->CurrentLiabilities) - 
                            (($balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents + 
                            $balance_sheets[@count($balance_sheets) - 1]->OtherCurrentFinancialAssets + 
                            $balance_sheets[@count($balance_sheets) - 1]->TradeAndOtherCurrentReceivables) / 
                            $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities);
                else
                    $Change = 0;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent4 = ($Change > 0) ? '+' : '';
                $textcontent4 .= number_format($Change, 1, '.', ',');
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
            </td>
            <td>
                The quick ratio is
                calculated by dividing liquid
                assets (cash and cash
                equivalents, trade and
                other current receivables,
                other current financial 
                assets) by current
                liabilities. It is a measure of
                a company's ability to meet
                its short-term obligations
                using its most liquid assets
                (near cash or quick
                assets).
                Normal value: no less than
                1.
            </td>
        </tr>
        <tr>
            <td>3. Cash ratio</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                if($balance_sheets[$x]->CurrentLiabilities != 0)
                    $CashRatio = $balance_sheets[$x]->CashAndCashEquivalents / 
                                    $balance_sheets[$x]->CurrentLiabilities;
                else
                    $CashRatio = 0;
                    $class = ($CashRatio >= 0.2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{ number_format($CashRatio, 2, '.', ',') }}
                </td>
            @endfor
            <?php 
                if($balance_sheets[0]->CurrentLiabilities != 0 && $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities != 0)
                $Change = ($balance_sheets[0]->CashAndCashEquivalents / 
                        $balance_sheets[0]->CurrentLiabilities) -
                        $balance_sheets[@count($balance_sheets) - 1]->CashAndCashEquivalents / 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities;
                else
                    $Change = 0;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 2, '.', ',')}}
            </td>
            <td>
                Cash ratio is calculated by
                dividing absolute liquid
                assets (cash and cash
                equivalents) by current
                liabilities.
                Acceptable value: no less
                than 0.2.
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">At the end of the period, the current ratio equaled <span style="color: GREEN">{{$textcontent1}}</span>. 
A change in the current ratio ade <span style="color: GREEN">{{$textcontent2}}</span>
during the 3 years, moreover, the increase tendency is also confirmed by a straight-line trend. On
the last day of the period analyzed, the value of the ratio can be described as very good. At the
beginning of the evaluated period, the current ratio was not normal, but later it was.
</p>

<p style="text-indent: 50px;">On the last day of the period analyzed, the quick ratio equaled <span style="color: GREEN">{{$textcontent3}}</span>. The growth in the quick
ratio was <span style="color: GREEN">{{$textcontent4}}</span> during the period analyzed, in addition, the growth tendency is also confirmed by a
straight-line trend. During the whole of the analyzed period, a constant growth in the ratio was
observed. The value of the quick ratio can be described as without doubt excellent on the last day
of the period analyzed (12/31/{{ (int)$financial_report->year }}). It means that {{ $financial_report->title }} has enough
liquid assets (cash and other assets which can be rapidly sold) to meet all their current liabilities.
</p>

<p style="text-indent: 50px;">The value of the third ratio, the cash ratio, does not lie in the accepted range as opposed to
the two previous ratios on 12/31/{{ (int)$financial_report->year }}. {{ $financial_report->title }} is observed to have a deficit
of cash and cash equivalents to meet current liabilities.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_4_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>