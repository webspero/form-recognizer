<h3>1.3.2. Working capital analysis</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Indicator</td>
            <td class="center" colspan="{{ @count($balance_sheets) }}">Value</td>
            <td class="center" colspan="2">Change for the period analyzed</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($balance_sheets); $x > 0 ; $x--)
                <td class="center">12/31/{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">(col.5-col.2)</td>
            <td class="center">% ((col.5-col.2) : col.2)</td>
        </tr>
        <tr>
            <td>1. Working capital (net	working capital), thousand PHP</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    $NWC = $balance_sheets[$x]->CurrentAssets -
                        $balance_sheets[$x]->CurrentLiabilities;
                    $class = ($NWC >= 0) ? ($NWC > 0) ? 'green' : '' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{ $NWC > 0 ? '+' : '' }}
                    {{ number_format($NWC, 0, '.', ',') }}
                </td>
                <?php
                    if($x == 0) $textcontent1 = number_format($NWC, 0, '.', ',');
                ?>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->CurrentAssets - 
                        $balance_sheets[0]->CurrentLiabilities) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 0, '.', ',');
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
            </td>
            <?php
                $PercentageChange = ((($balance_sheets[0]->CurrentAssets - 
                        $balance_sheets[0]->CurrentLiabilities) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities)) / 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities)) * 100;
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange >= 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}}
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
        </tr>
        <tr>
            <td>2. Inventories, thousand PHP</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    $Inventories = $balance_sheets[$x]->Inventories;
                    $class = ($Inventories >= 0) ? ($Inventories > 0) ? 'green' : '' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($Inventories != 0)
                        {{ $Inventories > 0 ? '+' : '' }}
                        {{ number_format($Inventories, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php
                $Change = $balance_sheets[0]->Inventories -  
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories != 0) {
                    $PercentageChange = (($balance_sheets[0]->Inventories -  
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories) /
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange >= 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}}
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
        </tr>
        <tr>
            <td>3. Working capital sufficiency (1-2), thousand PHP</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    $WCD = ($balance_sheets[$x]->CurrentAssets -
                        $balance_sheets[$x]->CurrentLiabilities) - 
                        $balance_sheets[$x]->Inventories;
                    $class = ($WCD >= 0) ? ($WCD > 0) ? 'green' : '' : 'red';
                ?>
                <td class="center {{ $class }}">
                    {{ $WCD > 0 ? '+' : '' }}
                    {{ number_format($WCD, 0, '.', ',') }}
                </td>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->CurrentAssets - 
                        $balance_sheets[0]->CurrentLiabilities - 
                        $balance_sheets[0]->Inventories) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities - 
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
            </td>
            <?php
                $PercentageChange = ((($balance_sheets[0]->CurrentAssets - 
                        $balance_sheets[0]->CurrentLiabilities - 
                        $balance_sheets[0]->Inventories) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities - 
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories)) / 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets - 
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities - 
                        $balance_sheets[@count($balance_sheets) - 1]->Inventories)) * 100;
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange >= 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}}
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
        </tr>
        <tr>
            <td>4. Inventory to working capital ratio (2:1) Acceptable value: no more than 1.</td>
            @for($x=@count($balance_sheets)-1; $x >= 0 ; $x--)
                <?php
                    $InventoryNWC = $balance_sheets[$x]->Inventories / 
                        ($balance_sheets[$x]->CurrentAssets -
                        $balance_sheets[$x]->CurrentLiabilities);
                    $class = ($InventoryNWC >= 0) ? ($InventoryNWC > 0) ? 'green' : '' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($InventoryNWC != 0)
                        {{ $InventoryNWC > 0 ? '+' : '' }}
                        {{ number_format($InventoryNWC, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php
                $Change = ($balance_sheets[0]->Inventories / 
                        ($balance_sheets[0]->CurrentAssets -
                        $balance_sheets[0]->CurrentLiabilities)) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->Inventories / 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets -
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities));
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php
                if($balance_sheets[@count($balance_sheets) - 1]->Inventories != 0){
                    $PercentageChange = ((($balance_sheets[0]->Inventories / 
                        ($balance_sheets[0]->CurrentAssets -
                        $balance_sheets[0]->CurrentLiabilities)) - 
                        ($balance_sheets[@count($balance_sheets) - 1]->Inventories / 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets -
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities))) / 
                        ($balance_sheets[@count($balance_sheets) - 1]->Inventories / 
                        ($balance_sheets[@count($balance_sheets) - 1]->CurrentAssets -
                        $balance_sheets[@count($balance_sheets) - 1]->CurrentLiabilities))) * 100;
                } else {
                    $PercentageChange = 0;
                }
                
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}">
                @if($PercentageChange != 0)
                    @if($PercentageChange >= 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}}
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    x
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">The working capital was equal to PHP <span style="color: GREEN">{{$textcontent1}}</span> thousand on 31 December, {{ (int)$financial_report->year }}. During the
entire period analyzed, the working capital significantly went up (by PHP <span style="color: GREEN">{{$textcontent2}}</span> thousand).
According to calculations, working capital fully covers the inventories of the company and is
deemed to be a positive factor. The inventory to working capital ratio was equal to zero on
12/31/{{ (int)$financial_report->year }}. Such a correlation is deemed to be normal, although it can be achieved through 
warehouse inventories that are too low, but not through enough of long-term resources of financing
in some cases.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_1_3_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>