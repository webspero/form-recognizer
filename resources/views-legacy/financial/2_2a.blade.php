<h3>2.2. Profitability Ratios</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Profitability Ratios</td>
            <td class="center" colspan="{{ @count($income_statements) }}">Value in %</td>
            <td class="center" rowspan="2">Change (col.4 - col.2)</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($income_statements); $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
        </tr>
        <tr>
            <td>1. Gross margin.</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $GrossMargin = ($income_statements[$x]->GrossProfit / 
                                    $income_statements[$x]->Revenue) * 100;
                ?>
                <td class="center">
                    @if($GrossMargin != 0)
                        {{ number_format($GrossMargin, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent1 = number_format($GrossMargin, 1, '.', ','); ?>
            @endfor
            <?php 
                $Change = (($income_statements[0]->GrossProfit / 
                            $income_statements[0]->Revenue) -
                            ($income_statements[@count($income_statements) - 1]->GrossProfit / 
                            $income_statements[@count($income_statements) - 1]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>2. Return on sales (operating margin).</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ROS = (($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->Revenue) * 100;
                ?>
                <td class="center">
                    @if($ROS != 0)
                        {{ number_format($ROS, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent2 = number_format($ROS, 1, '.', ','); ?>
            @endfor
            <?php 
                $Change = ((($income_statements[0]->ProfitLossBeforeTax + 
                            $income_statements[0]->FinanceCosts) /
                            $income_statements[0]->Revenue) -
                            (($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                            $income_statements[@count($income_statements) - 1]->FinanceCosts) /
                            $income_statements[@count($income_statements) - 1]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>3. Profit margin. </td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ProfitMargin = ($income_statements[$x]->ProfitLoss /
                            $income_statements[$x]->Revenue) * 100;
                ?>
                <td class="center">
                    @if($ProfitMargin != 0)
                        {{ number_format($ProfitMargin, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent3 = number_format($ProfitMargin, 1, '.', ','); ?>
            @endfor
            <?php 
                $Change = (($income_statements[0]->ProfitLoss /
                            $income_statements[0]->Revenue) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLoss /
                            $income_statements[@count($income_statements) - 1]->Revenue)) * 100;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>Reference:<br />Interest coverage ratio (ICR). Acceptable value: no less than 1.5.</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    if($income_statements[$x]->FinanceCosts != 0){
                        $ICR = ($income_statements[$x]->ProfitLossBeforeTax + 
                            $income_statements[$x]->FinanceCosts) /
                            $income_statements[$x]->FinanceCosts;
                    } else {
                        $ICR = 0;
                    }
                    
                    $class = ($ICR >= 1.5) ? 'green' : 'red'
                ?>
                <td class="center {{ $class }}">
                    @if($ICR != 0)
                        {{ number_format($ICR, 1, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent4 = number_format($ICR, 1, '.', ','); ?>
            @endfor
            <?php 
                if($income_statements[0]->FinanceCosts != 0 && $income_statements[@count($income_statements) - 1]->FinanceCosts != 0){
                    $Change = (($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) /
                                $income_statements[0]->FinanceCosts) -
                                (($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts) /
                                $income_statements[@count($income_statements) - 1]->FinanceCosts);
                } else {
                    $Change = 0;
                }
                
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 1, '.', ',')}}
                @else
                    -
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">The profitability ratios given in the table have positive values as a result of the profitability of
{{ $financial_report->title }}'s activities during the last year. The gross margin was equal to
<span style="color: GREEN">{{$textcontent1}}</span>% for the last year. For the last year in comparison with the same period of the prior financial
year, the gross margin did not change.</p>

<p style="text-indent: 50px;">The profitability calculated by earnings before interest and taxes (Return on sales) is more
important from a comparative analyzes point of view. The return on sales was {{$textcontent2}}% per
annum for the last year, while the profit margin was {{$textcontent3}}% per annum.</p>

<p style="text-indent: 50px;">To assess the liabilities that the company should repay for the use of borrowed capital, an
interest coverage ratio was calculated. The acceptable value is deemed to be not less than 1.5. In
this case, the interest coverage ratio was <span style="color: GREEN">{{$textcontent4}}</span> for the last year, which is evidence of {{ $financial_report->title }}'s capability to pay interest on borrowed assets. It should also be mentioned
that not all interest payments are necessarily included on the income statement and used to
calculate the indicated ratio. Interest related to investments in qualified assets is not included in the
financial results (they are taken into account in the asset value).
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_2_1_".$income_statements[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>