<h3> 1. 3 Financial Sustainability Analysis</h3>
<h3>1.3.1 Key Ratios of the Company's Financial Sustainability</h3>
    <table class="table">
        <tr>
            <td rowspan="2" style="text-align:center;">Ratio</td>
            <td colspan="{{count($financial_table)}}" style="text-align:center;">Value</td>
            <td rowspan="2" style="text-align:center;">Change (Col{{count($financial_table)}}-Col.2)</td>
            <td rowspan="2" style="text-align:center;">Description of the ratio and it's recommended value</td>
        </tr>
        <tr>
            @foreach($financial_table as $val)
                <td style="text-align:center;">12/31/{{$val->year}}</td>
            @endforeach
        </tr>
        <tr>
            <td>Debt-to-equity ratio (financial leverage)</td>
            <?php
                $i=0;
                $debtToEquityFinancialPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->FinancialLeverage <= 1.5)
                    <?php
                        array_push($debtToEquityFinancialPositiveValues, $val->FinancialLeverage);
                    ?>
                    <td style="text-align:center; color:green;">{{$val->FinancialLeverage}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FinancialLeverage}}</td>
                @endif

                <?php
                    if ($i == 0) {
                        $firstDebtToEquityFinancial = $val->FinancialLeverage;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityFinancial = $val->FinancialLeverage;
                        $lastDebtToEquityFinancialColor = 'red';
                        if($lastDebtToEquityFinancial <= 1.5)
                        {
                            $lastDebtToEquityFinancialColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FinancialLeverage_change']}}</td>
                <?php $debtToEquityFinancialChangeColor = 'red'; ?>
            @endif
            
            <td>A debt-to-equity ratio is calculated by taking the total liabilities and dividing it by shareholders' equity. It is the key financial ratio and used as
                a standard for judging a company's financial standing. Normal value: 1.5 or less (optimum 0.43-1).</td>
        </tr>
        <tr>
            <?php
                $i=0;
                $debtRatioPositiveValues=[];
            ?>
            <td>Debt ratio (debt to assets ratio)</td>
            @foreach($financial_table as $val)
                @if($val->DebtRatio <= 0.6)
                    <td style="text-align:center; color:green;">{{$val->DebtRatio}}</td>
                    <?php array_push($debtRatioPositiveValues, $val->DebtRatio);?>
                @else
                    <td style="text-align:center; color:red;">{{$val->DebtRatio}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstDebtToEquityAsset = $val->DebtRatio;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastDebtToEquityAsset = $val->DebtRatio;
                        $lastDebtToEquityAssetColor = 'red';
                        if($lastDebtToEquityAsset <= 0.6)
                        {
                            $lastDebtToEquityAssetColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['DebtRatio_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['DebtRatio_change']}}</td>
                <?php $debtToEquityAssetChangeColor = 'red'; ?>
            @endif
            <td>A debt ratio is calculated by dividing total liabilities (i.e. long-term and short-term liabilities) by total assets. It shows how much the company
                relies on debt to finance assets (similar to debt-to-equity ratio). Normal value: 0.6 or less (optimum 0.3-0.5).</td>
        </tr>
        <tr>
            <td>Long-term debt to Equity</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->LTDtoE}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['LTDtoE_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['LTDtoE_change']}}</td>
            @endif
            <td>This ratio is calculated by dividing long-term (non-current) liabilities by equity.</td>
        </tr>
        <tr>
            <td>Non-current assets to Net worth</td>
            <?php
                $i=0;
                $NCAtoNWPositiveValues=[];
            ?>
            @foreach($financial_table as $val)
                @if($val->NCAtoNW <= 1.25)
                    <td style="text-align:center; color:green;">{{$val->NCAtoNW}}</td>
                    <?php array_push($NCAtoNWPositiveValues, $val->NCAtoNW); ?>
                @else
                    <td style="text-align:center; color:red;">{{$val->NCAtoNW}}</td>
                @endif
                <?php
                    if ($i == 0) {
                        $firstNCAtoNW = $val->NCAtoNW;
                    }
                    else if ($i == count($financial_table) - 1) {
                        $lastNCAtoNW = $val->NCAtoNW;
                        $lastNCAtoNWColor = 'red';
                        if($lastNCAtoNW <= 1.25)
                        {
                            $lastNCAtoNWColor = 'green';
                        }
                    }
                    $i++;
                ?>
            @endforeach
            @if($financial_change['NCAtoNW_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'green'; ?>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['NCAtoNW_change']}}</td>
                <?php $NCAtoNWChangeColor = 'red'; ?>
            @endif
            <td>This ratio is calculated by dividing long-term (non-current) liabilities by net worth (equity) and measures the extent of a company's investment in
                low-liquidity non-current assets. This ratio is important for comparison analysis because it's less dependent on industry (structure of company's
                assets) than debt ratio and debt-to-equity ratio. Normal value: no more than 1.25.</td>
        </tr>
        <tr>
            <td>Capitalization ratio</td>
            @foreach($financial_table as $val)
                <td style="text-align:center;">{{$val->Capitalization}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['Capitalization_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['Capitalization_change']}}</td>
            @endif
            <td>Calculated by dividing non-current liabilities by the sum of equity and non-current liabilities.</td>
        </tr>
        <tr>
            <td>Fixed assets to Net worth</td>
            @foreach($financial_table as $val)
                @if($val->DebtRatio <= 0.75)
                    <td style="text-align:center; color:green;">{{$val->FAtoNW}}</td>
                @else
                    <td style="text-align:center; color:red;">{{$val->FAtoNW}}</td>
                @endif
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['FAtoNW_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['FAtoNW_change']}}</td>
            @endif
            <td>This ratio indicates the extent to which the owners' cash is frozen in the form of fixed assets, such as property, plant, and equipment,
                investment property and non-current biological assets. Normal value: 0.75 or less.</td>
        </tr>
        <tr>
            <td>Current Liability Ratio</td>
            @foreach($financial_table as $val) 
                <td style="text-align:center;">{{$val->CLiabilityRatio}}</td>
            @endforeach
            @if($financial_change['FinancialLeverage_change'] >= 0)
                <td style="text-align:center; color:green;" >+{{$financial_change['CLiabilityRatio_change']}}</td>
            @else
                <td style="text-align:center; color:red;" >{{$financial_change['CLiabilityRatio_change']}}</td>
            @endif
            <td>Current liability ratio is calculated by dividing non-current liabilities by total (i.e. current and non-current) liabilities.</td>
        </tr>

    </table>
    <div>
        <p>First, attention should be drawn to the debt-to-equity ratio and debt ratio as the ratios describing the capital structure. Both ratios have similar meaning and indicate if there is not enough capital (equity) for stable work for the company. Debt-to-equity ratio is calculated as a relationship of the borrowed capital (liabilities) to the equity, while debt ratio is calculated as a relationship of the liabilities to the overall capital (i.e. the sum of equity and liabilities).<p>

        <?php
            // Debt to equality and asset text, no more than 1.5
            $debtRatioToEquityText = '';
            if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                // All negative including change value
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. For the whole period reviewed, it was observed that there was a an appreciable decrease in the debt ratio to <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> (<span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( (count($debtToEquityFinancialPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity amounted to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. During the period reviewed, the debt ratio increased a little (to <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span> The debt ratio amounted to <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> on 31 December, '.$financial_table[count($financial_table)-1]->year.'. For the period reviewed (31.12.14–31.12.17), it was verified that there was a rapid increase in the debt ratio of <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else if( (count($debtToEquityFinancialPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity amounted to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. The debt ratio amounted to <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> at the end of the period analysed; it is <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span> lower than the level of the debt ratio on the first day of the period analysed.';
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial > 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioToEquityText = 'At the end of the period analyzed, the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. The debt ratio was equal to <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> on the last day of the period analyzed (12/31/'.$financial_table[count($financial_table)-1]->year.'). During the period reviewed, it was verified that there was a significant increase in the debt ratio of <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $debtRatioToEquityText = 'On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt-to-equity was <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. The debt ratio was equal to <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span> on 12/31/'.$financial_table[count($financial_table)-1]->year.', at the same time the debt ratio was lower and equaled – at the beginning of the period (12/31/'.$financial_table[0]->year.') (i.e. the growth was <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>).';
            }
            else if( ($firstDebtToEquityFinancial > 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $debtRatioToEquityText = 'On 12/31/'.$financial_table[count($financial_table)-1]->year.', the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. The debt ratio decreased and showed <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span> for the entire period analyzed.';
            }
            else if( ($firstDebtToEquityFinancial <= 1.5) && ($lastDebtToEquityFinancial <= 1.5) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtToEquityFinancialPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change minus, but in middle minus
                $debtRatioToEquityText = 'At the end of the period reviewed, the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt ratio was <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. During the last '.count($balance_sheets).' years, it was found that there was a notable growth in the debt ratio of <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
            }
            else
            {
                if($lastDebtToEquityFinancial <= 1.5 && $financial_change['DebtRatio_change'] >= 0 )
                {
                    $debtRatioToEquityText = 'At the end of the period reviewed, the debt-to-equity was equal to <span class="'.$lastDebtToEquityFinancialColor.'">'.$lastDebtToEquityFinancial.'</span>. On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt ratio was <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. During the last '.count($balance_sheets).' years, it was found that there was a notable growth in the debt ratio of <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
                }
                else
                {
                    $debtRatioToEquityText = 'At the end of the period reviewed, the debt-to-equity was equal to '.$lastDebtToEquityFinancial.'. On 31 December, '.$financial_table[count($financial_table)-1]->year.', the debt ratio was <span class="'.$lastDebtToEquityAssetColor.'">'.$lastDebtToEquityAsset.'</span>. During the last '.count($balance_sheets).' years, it was found that there was a decreased in the debt ratio of <span class="'.$debtToEquityAssetChangeColor.'">'.$financial_change['DebtRatio_change'].'</span>.';
                }
            }
        ?>
        
        <p>{!! $debtRatioToEquityText !!}</p>

        <?php
            // Debt to equality and asset text
            $debtRatioText = '';
            if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] < 0))
            {
                // All negative including change value
                $debtRatioText = 'The value of the debt ratio negatively describes the financial position of '.$financial_report->title.' on 12/31/'.$financial_table[count($financial_table)-1]->year.', the percentage of liabilities is too high and equaled '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of the company&apos; total capital. The maximum acceptable percentage is 60%. Too higher dependence from creditors can lower the financial stability of the company, especially in the case of economic instability and crisis on the market of borrowed capital. It is recommended to keep the value of the debt ratio at a level of 0.6 or less (optimum 0.3-0.5).';
            }
            else if( (count($debtRatioPositiveValues)<=0) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All negative, change plus
                $debtRatioText = 'The value of the debt ratio for '.$financial_report->title.' indicates an excessive percentage of liabilities on 31 December, '.$financial_table[count($financial_table)-1]->year.', which equaled '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of total capital. It is believed that liabilities should not be more than 60%. If not, dependence from creditors increases too much and the financial stability of the company suffers. When the structure of capital is normal, the debt ratio is no more than 0.6 (optimum 0.3-0.5).';
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] >= 0) )
            {
                // All positive, change plus
                $debtRatioText = 'According to the debt ratio, the percentage of the borrowed capital (liabilities) is significantly lower than the admissible value and makes 2.9% of overall capital on 12/31/'.$financial_table[count($financial_table)-1]->year.'. On the one hand it positively describes the financial situation of '.$financial_report->title.'. On the other hand it says about missed opportunities to use borrowed capital for the extension of activity and acceleration of development rates. The company can increase the percentage of credits and debts without damage to its&apos; financial situation if a plan on efficient use of additional capital is available. The debt ratio kept an acceptable value during the whole of the period.';
            }
            else if( (count($debtRatioPositiveValues)===count($balance_sheets)) && ($financial_change['DebtRatio_change'] < 0) )
            {
                // All positive, change minus
                $debtRatioText = 'The debt ratio describes the financial position of '.$financial_report->title.' on 31 December, '.$financial_table[count($financial_table)-1]->year.' as a very good one, the percentage of liabilities is '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. The maximum acceptable percentage of liabilities is deemed to be 60%. When sources to finance the company are planned, this percentage should be used as an upper limit of acceptable percentage of the borrowed capital. The debt ratio had an acceptable value during the whole of the analysed period.';
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset > 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $debtRatioText = 'The value of the debt ratio negatively describes the financial position of '.$financial_report->title.' at the end of the period reviewed, the percentage of liabilities is too high and equaled '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of the company&apos;s total capital. The maximum acceptable percentage is 60%. Too higher dependence from creditors can lower the financial stability of the company, especially in the case of economic instability and crisis on the market of borrowed capital. It is recommended to keep the value of the debt ratio at a level of no more than 0.6 (optimum 0.3-0.5). Despite the fact that at the beginning of the analyzed period and the value of the debt ratio corresponded to the norm, later it became unacceptable.';
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $debtRatioText = 'The value of the debt ratio relates to '.$financial_report->title.'&apos;s good capital structure on 31 December, '.$financial_table[count($financial_table)-1]->year.' from the point of view of financial stability. The percentages of liability is '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of the company&apos;s total capital. Though the final value of the debt ratio kept to its&apos; norm, the rate was primarily unacceptable during the whole of the analyzed period.';
            }
            else if( ($firstDebtToEquityAsset > 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $debtRatioText = 'The value of the debt ratio relates to '.$financial_report->title.' good capital structure on the last day of the period analyzed (12/31/'.$financial_table[count($financial_table)-1]->year.') from the point of view of financial stability. The percentages of liability is '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of the company&apos;s total capital. Despite the fact that at the beginning of the considered period, the value of the debt ratio did not correspond to the norm, later it became acceptable.';
            }
            else if( ($firstDebtToEquityAsset <= 0.6) && ($lastDebtToEquityAsset <= 0.6) && ($financial_change['DebtRatio_change'] >= 0) && (count($debtRatioPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change plus, and in middle minus
                $debtRatioText = 'The debt ratio describes the financial position of '.$financial_report->title.' on 31 December, '.$financial_table[count($financial_table)-1]->year.' as a very good one, the percentage of liabilities is '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. The maximum acceptable percentage of liabilities is deemed to be 60%. When sources to finance the company are planned, this percentage should be used as an upper limit of acceptable percentage of the borrowed capital. During the whole of the analyzed period, the debt ratio kept a acceptable value.';
            }
            else
            {
                if($lastDebtToEquityAsset <= 0.6)
                {
                    $debtRatioText = 'The debt ratio describes the financial position of '.$financial_report->title.' on 31 December, '.$financial_table[count($financial_table)-1]->year.' as a very good one, the percentage of liabilities is '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'%. The maximum acceptable percentage of liabilities is deemed to be 60%. When sources to finance the company are planned, this percentage should be used as an upper limit of acceptable percentage of the borrowed capital. At the end of the analyzed period, the debt ratio became acceptable.';
                }
                else
                {
                    $debtRatioText = 'The value of the debt ratio negatively describes the financial position of '.$financial_report->title.' at the end of the period reviewed, the percentage of liabilities is too high and equaled '.$financial_table[count($financial_table)-1]->debtRatioPercentage.'% of the company&apos;s total capital. The maximum acceptable percentage is 60%. Too higher dependence from creditors can lower the financial stability of the company, especially in the case of economic instability and crisis on the market of borrowed capital. It is recommended to keep the value of the debt ratio at a level of no more than 0.6 (optimum 0.3-0.5).';
                }
            }
        ?>

        <p>{!! $debtRatioText !!}</p>
        <p>The structure of the company's capital is shown in the chart below:</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_1_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>

        <?php
            // Non-current assets to Net worth text
            $NCAtoNWText = '';
            if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] < 0))
            {
                // All negative including change value
                $NCAtoNWText = 'According to common rules, non-current investments should be made, in the first place, with the help of the most stable source of financing, i.e. with the help of own capital (equity). The non-current assets to Net worth ratio shows if this rule is followed. On 12/31/'.$financial_table[count($financial_table)-1]->year.', the ratio equaled <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>, while the ratio equaled <span class="'.$lastNCAtoNWColor.'">'.$firstNCAtoNW.'</span> (i.e. a descrease of <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) was found.';
            }
            else if( (count($NCAtoNWPositiveValues)<=0) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                $NCAtoNWText = 'According to common rules, non-current investments should be made, in the first place, with the help of the most stable source of financing, i.e. with the help of own capital (equity). The non-current assets to Net worth ratio shows if this rule is followed. The ratio was equal to <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> on 12/31/'.$financial_table[count($financial_table)-1]->year.'. During the period reviewed (31.12.'.$financial_table[0]->year.'–31.12.'.$financial_table[count($financial_table)-1]->year.'), the ratio rapidly increased (<span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>).';
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] >= 0) )
            {
                // All positive including change
                $NCAtoNWText = 'According to common rules, non-current investments should be made, in the first place, with the help of the most stable source of financing, i.e. with the help of own capital (equity). The non-current assets to Net worth ratio shows if this rule is followed. The ratio strongly grew (by <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) and equaled <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> for the entire period reviewed. At the end of the period analyzed, the value of the ratio can be described as excellent.';
            }
            else if( (count($NCAtoNWPositiveValues)===count($balance_sheets)) && ($financial_change['NCAtoNW_change'] < 0) )
            {
                // All positive, except for change
                $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (non-current assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the non-current assets to net worth ratio. The ratio amounted to <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> on the last day of the period analyzed. For the period reviewed, the ratio fell rapidly (by <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>). On the last day of the period analyzed, the ratio shows a very good value.';
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW > 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first plus-end minus: change plus
                $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (noncurrent assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the noncurrent assets to net worth ratio. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio was <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. For the whole period reviewed, it was found that there was an extreme increase in the ratio of <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>, moreover, the similar tendency is proven by a linear trend during the period. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio shows an unsatisfactory value.';
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0)  )
            {
                // first minus-end plus: change plus
                $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (noncurrent assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the noncurrent assets to net worth ratio. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio was equal to <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>;. At the end of the period analysed, the value of the ratio can be described as acceptable.';
            }
            else if( ($firstNCAtoNW > 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] < 0)  )
            {
                // first minus-end plus: change minus
                $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (noncurrent assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the noncurrent assets to net worth ratio. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio was equal to <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>; it is <span class="red">'.$firstNCAtoNW.'</span> lower than on 31 December, '.$financial_table[0]->year.'. At the end of the period analysed, the value of the ratio can be described as acceptable.';
            }
            else if( ($firstNCAtoNW <= 1.25) && ($lastNCAtoNW <= 1.25) && ($financial_change['NCAtoNW_change'] >= 0) && (count($NCAtoNWPositiveValues)!=count($balance_sheets)) )
            {
                // first plus-end plus: change minus, but in middle minus
                $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (noncurrent assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the noncurrent assets to net worth ratio. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio was equal to <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. At the end of the period analysed, the value of the ratio can be described as acceptable.';
            }
            else
            {
                if($lastNCAtoNW <= 1.25 && $financial_change['NCAtoNW_change'] >= 0 )
                {
                    $NCAtoNWText = 'According to common rules, non-current investments should be made, in the first place, with the help of the most stable source of financing, i.e. with the help of own capital (equity). The non-current assets to Net worth ratio shows if this rule is followed. The ratio strongly grew (by <span class="'.$NCAtoNWChangeColor.'">'.$financial_change['NCAtoNW_change'].'</span>) and equaled <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span> for the entire period reviewed. At the end of the period analyzed, the value of the ratio can be described as excellent.';
                }
                else
                {
                    $NCAtoNWText = 'According to well known principles for stable company development, investments with the least liquid assets (noncurrent assets) should first be made with help from the most long-term sources of financing, i.e. with the help of own capital (equity). An indicator of this rule is the noncurrent assets to net worth ratio. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio was <span class="'.$lastNCAtoNWColor.'">'.$lastNCAtoNW.'</span>. On 31.12.'.$financial_table[count($financial_table)-1]->year.', the ratio shows an unsatisfactory value.';
                }
            }
        ?>

        <p>{!! $NCAtoNWText !!}</p>
        <?php
            $nonCurrentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->nonCurrentLiabilitiesPercentage;
            $currentLiabilitiesPercentage = $financial_table[count($financial_table)-1]->currentLiabilitiesPercentage;

            $liabilityText = '';
            if( ($nonCurrentLiabilitiesPercentage>40) && ($currentLiabilitiesPercentage>40) ){
                $liabilityText = 'The current liability ratio shows that current and noncurrent liabilities of the company are almost equal ('.$currentLiabilitiesPercentage.'% and '.$nonCurrentLiabilitiesPercentage.'%, respectively).';
            }
            else if($nonCurrentLiabilitiesPercentage > $currentLiabilitiesPercentage)
            {
                $liabilityText = 'The current liability ratio shows that the amount of non-current liabilities of '.$financial_report->title.' significantly exceeds the amount of current liabilities ('.$nonCurrentLiabilitiesPercentage.'% and '.$currentLiabilitiesPercentage.'% of total liabilities, respectively).';
            }
            else
            {
                // Current liabilities (short-term liabilities)
                $liabilityText = 'The current liability ratio shows that the share of short-term and long-term liabilities of total liabilities of '.$financial_report->title.' equaled '.$currentLiabilitiesPercentage.'% and '.$nonCurrentLiabilitiesPercentage.'% respectively at the end of the period analyzed.';
            }
        ?>
        <p>{!! $liabilityText !!}</p>
        <p>The following chart demonstrates the dynamics of the main ratios of financial stability of {{$financial_report->title}}.</p>
        <div class = 'center'>
            <img src = '{{ URL::To("images/graph_1_3_1_2_".$balance_sheets[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
        </div>
    </div>