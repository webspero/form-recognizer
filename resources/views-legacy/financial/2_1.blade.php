<h3>2.1. Overview of the Financial Results</h3>

<p style="text-indent: 50px;">The table below gives information about the main financial results of {{ $financial_report->title }}'s activities during the period analyzed (31.12.{{substr(((int)$financial_report->year - @count($income_statements) + 1), 2)}}-
31.12.{{substr(((int)$financial_report->year),2)}}).</p>

<table cellpadding="0" cellspacing="0">
    <thead>
    
    </thead>
    
    <tbody>
        <tr>
            <td class="center" rowspan="2">Indicator</td>
            <td class="center" colspan="{{ @count($income_statements) }}">Value, thousand PHP</td>
            <td class="center" colspan="2">Change</td>
            <td class="center" rowspan="2">Average annual value, thousand PHP</td>
        </tr>
        <tr dontbreak="true">
            @for($x=@count($income_statements); $x > 0 ; $x--)
                <td class="center">{{ ((int)$financial_report->year - $x + 1) }}</td>
            @endfor
            <td class="center">thousand PHP (col.4 - col.2)</td>
            <td class="center">&plusmn; % (4-2) : 2</td>
        </tr>
        <tr>
            <td width="30%">1. Revenue</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $Revenue = $income_statements[$x]->Revenue;
                    $class = ($Revenue >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($Revenue != 0)
                        {{ number_format($Revenue, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent1 = number_format($Revenue, 0, '.', ','); ?>
            @endfor
            <?php 
                $Change = $income_statements[0]->Revenue -
                            $income_statements[@count($income_statements) - 1]->Revenue;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent2 = number_format($Change, 0, '.', ',');
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if($income_statements[@count($income_statements) - 1]->Revenue != 0){
                    $PercentageChange = (($income_statements[0]->Revenue -
                            $income_statements[@count($income_statements) - 1]->Revenue) / 
                            $income_statements[@count($income_statements) - 1]->Revenue) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->Revenue;
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>2. Cost of sales</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $CostOfSales = $income_statements[$x]->CostOfSales;
                    $class = ($CostOfSales >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($CostOfSales != 0)
                        {{ number_format($CostOfSales, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = $income_statements[0]->CostOfSales -
                            $income_statements[@count($income_statements) - 1]->CostOfSales;
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if($income_statements[@count($income_statements) - 1]->CostOfSales != 0) {
                    $PercentageChange = (($income_statements[0]->CostOfSales -
                            $income_statements[@count($income_statements) - 1]->CostOfSales) / 
                            $income_statements[@count($income_statements) - 1]->CostOfSales) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += $income_statements[$x]->CostOfSales;
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>3.<i>Gross profit</i> (1-2)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $GrossProfit = $income_statements[$x]->GrossProfit;
                    $class = ($GrossProfit >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($GrossProfit != 0)
                        {{ number_format($GrossProfit, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent3 = number_format($GrossProfit, 0, '.', ','); ?>
            @endfor
            <?php 
                $Change = ($income_statements[0]->GrossProfit) -
                            ($income_statements[@count($income_statements) - 1]->GrossProfit);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
                $textcontent4 = number_format($Change, 0, '.', ',');
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if($income_statements[@count($income_statements) - 1]->GrossProfit != 0) {
                    $PercentageChange = ((($income_statements[0]->GrossProfit) -
                            ($income_statements[@count($income_statements) - 1]->GrossProfit)) / 
                            ($income_statements[@count($income_statements) - 1]->GrossProfit)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
                $textcontent5 = number_format($PercentageChange, 1, '.', ',');
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->GrossProfit);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>4. Other income and expenses, except Finance costs</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $OIEeFC = $income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($OIEeFC >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($OIEeFC != 0)
                        {{ number_format($OIEeFC, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax -
                                $income_statements[@count($income_statements) - 1]->GrossProfit + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax -
                                $income_statements[@count($income_statements) - 1]->GrossProfit + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax -
                                $income_statements[0]->GrossProfit + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax -
                                $income_statements[@count($income_statements) - 1]->GrossProfit + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts)) / 
                                ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax -
                                $income_statements[@count($income_statements) - 1]->GrossProfit + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center" width="10%">
                @if($PercentageChange != 0)
                    &darr;
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax -
                                $income_statements[$x]->GrossProfit + 
                                $income_statements[$x]->FinanceCosts);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>5. <i>EBIT</i> (3+4)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $EBIT = $income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts;
                    $class = ($EBIT >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($EBIT != 0)
                        {{ number_format($EBIT, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent6 = number_format($EBIT, 0, '.', ','); ?>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossBeforeTax + 
                                $income_statements[0]->FinanceCosts) -
                                ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts)) / 
                                ($income_statements[@count($income_statements) - 1]->ProfitLossBeforeTax + 
                                $income_statements[@count($income_statements) - 1]->FinanceCosts)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossBeforeTax + 
                                $income_statements[$x]->FinanceCosts);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        
        <tr>
            <td>6. Finance costs</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $FinanceCosts = $income_statements[$x]->FinanceCosts;
                    $class = ($FinanceCosts >= 2) ? 'green' : 'red';
                ?>
                <td class="center red">
                    @if($FinanceCosts != 0)
                        {{ number_format($FinanceCosts, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->FinanceCosts) -
                            ($income_statements[@count($income_statements) - 1]->FinanceCosts);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->FinanceCosts) != 0) {
                    $PercentageChange = ((($income_statements[0]->FinanceCosts) -
                                ($income_statements[@count($income_statements) - 1]->FinanceCosts)) / 
                                ($income_statements[@count($income_statements) - 1]->FinanceCosts)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->FinanceCosts);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center red" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>7. Income tax expense (from continuing operations)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $IncomeTaxExpenseContinuingOperations = $income_statements[$x]->IncomeTaxExpenseContinuingOperations;
                    $class = ($IncomeTaxExpenseContinuingOperations >= 2) ? 'green' : 'red';
                ?>
                <td class="center red">
                    @if($IncomeTaxExpenseContinuingOperations != 0)
                        {{ number_format($IncomeTaxExpenseContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                            ($income_statements[@count($income_statements) - 1]->IncomeTaxExpenseContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->IncomeTaxExpenseContinuingOperations) != 0) {
                    $PercentageChange = ((($income_statements[0]->IncomeTaxExpenseContinuingOperations) -
                                ($income_statements[@count($income_statements) - 1]->IncomeTaxExpenseContinuingOperations)) / 
                                ($income_statements[@count($income_statements) - 1]->IncomeTaxExpenseContinuingOperations)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->IncomeTaxExpenseContinuingOperations);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center red" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>8. Profit (loss) from continuing operations (5-6-7)</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ProfitLossFromContinuingOperations = $income_statements[$x]->ProfitLossFromContinuingOperations;
                    $class = ($ProfitLossFromContinuingOperations >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($ProfitLossFromContinuingOperations != 0)
                        {{ number_format($ProfitLossFromContinuingOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ProfitLossFromContinuingOperations) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLossFromContinuingOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ProfitLossFromContinuingOperations) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossFromContinuingOperations) -
                                ($income_statements[@count($income_statements) - 1]->ProfitLossFromContinuingOperations)) / 
                                ($income_statements[@count($income_statements) - 1]->ProfitLossFromContinuingOperations)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromContinuingOperations);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>9. Profit (loss) from discontinued operations</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ProfitLossFromDiscontinuedOperations = $income_statements[$x]->ProfitLossFromDiscontinuedOperations;
                    $class = ($ProfitLossFromDiscontinuedOperations >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($ProfitLossFromDiscontinuedOperations != 0)
                        {{ number_format($ProfitLossFromDiscontinuedOperations, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLossFromDiscontinuedOperations);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ProfitLossFromDiscontinuedOperations) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLossFromDiscontinuedOperations) -
                                ($income_statements[@count($income_statements) - 1]->ProfitLossFromDiscontinuedOperations)) / 
                                ($income_statements[@count($income_statements) - 1]->ProfitLossFromDiscontinuedOperations)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLossFromDiscontinuedOperations);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td><b>10. Profit (loss) (8+9)</b></td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ProfitLoss = $income_statements[$x]->ProfitLoss;
                    $class = ($ProfitLoss >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    <b>
                    @if($ProfitLoss != 0)
                        {{ number_format($ProfitLoss, 0, '.', ',') }}
                    @else
                        -
                    @endif
                    </b>
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ProfitLoss) -
                            ($income_statements[@count($income_statements) - 1]->ProfitLoss);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                <b>
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
                </b>
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ProfitLoss) != 0) {
                    $PercentageChange = ((($income_statements[0]->ProfitLoss) -
                                ($income_statements[@count($income_statements) - 1]->ProfitLoss)) / 
                                ($income_statements[@count($income_statements) - 1]->ProfitLoss)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                <b>
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
                </b>
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ProfitLoss);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                <b>
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
                </b>
            </td>
        </tr>
        
        <tr>
            <td>11. Other comprehensive income</td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $OtherComprehensiveIncome = $income_statements[$x]->OtherComprehensiveIncome;
                    $class = ($OtherComprehensiveIncome >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($OtherComprehensiveIncome != 0)
                        {{ number_format($OtherComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
            @endfor
            <?php 
                $Change = ($income_statements[0]->OtherComprehensiveIncome) -
                            ($income_statements[@count($income_statements) - 1]->OtherComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->OtherComprehensiveIncome) != 0) {
                    $PercentageChange = ((($income_statements[0]->OtherComprehensiveIncome) -
                                ($income_statements[@count($income_statements) - 1]->OtherComprehensiveIncome)) / 
                                ($income_statements[@count($income_statements) - 1]->OtherComprehensiveIncome)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->OtherComprehensiveIncome);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
        
        <tr>
            <td>12. Comprehensive income (10+11) </td>
            @for($x=@count($income_statements)-1; $x >= 0 ; $x--)
                <?php
                    $ComprehensiveIncome = $income_statements[$x]->ComprehensiveIncome;
                    $class = ($ComprehensiveIncome >= 2) ? 'green' : 'red';
                ?>
                <td class="center {{ $class }}">
                    @if($ComprehensiveIncome != 0)
                        {{ number_format($ComprehensiveIncome, 0, '.', ',') }}
                    @else
                        -
                    @endif
                </td>
                <?php if($x==0) $textcontent7 = number_format($ComprehensiveIncome, 0, '.', ','); ?>
            @endfor
            <?php 
                $Change = ($income_statements[0]->ComprehensiveIncome) -
                            ($income_statements[@count($income_statements) - 1]->ComprehensiveIncome);
                $class = ($Change >= 0) ? ($Change > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="12%">
                @if($Change != 0)
                    {{ $Change > 0 ? '+' : '' }}{{number_format($Change, 0, '.', ',')}}
                @else
                    -
                @endif
            </td>
            <?php 
                if(($income_statements[@count($income_statements) - 1]->ComprehensiveIncome) != 0) {
                    $PercentageChange = ((($income_statements[0]->ComprehensiveIncome) -
                                ($income_statements[@count($income_statements) - 1]->ComprehensiveIncome)) / 
                                ($income_statements[@count($income_statements) - 1]->ComprehensiveIncome)) * 100;
                } else {
                    $PercentageChange = 0;
                }
                $class = ($PercentageChange >= 0) ? ($PercentageChange > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="10%">
                @if($PercentageChange != 0)
                    @if($PercentageChange > 200)
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format(($PercentageChange / 100) + 1, 1, '.', ',')}} <span class="black">times</span>
                    @else
                        {{ $PercentageChange > 0 ? '+' : '' }}{{number_format($PercentageChange, 1, '.', ',')}}
                    @endif
                @else
                    -
                @endif
            </td>
            <?php
                $Sum = 0;
                for($x=@count($income_statements)-1; $x >= 0 ; $x--){
                    $Sum += ($income_statements[$x]->ComprehensiveIncome);
                }
                $Average = $Sum / @count($income_statements);
                $class = ($Average >= 0) ? ($Average > 0) ? 'green' : '' : 'red';
            ?>
            <td class="center {{ $class }}" width="15%">
                @if($Average != 0)
                    {{ number_format($Average, 0, '.', ',') }}
                @else
                    -
                @endif
            </td>
        </tr>
    </tbody>
</table>
<br /><br/>
<p style="text-indent: 50px;">During the period 01.01–12/31/{{ ((int)$financial_report->year) }}, the revenue was PHP <span style="color: GREEN">{{$textcontent1}}</span> thousand. For the year
2014 in comparison with the same period of the prior financial year, it was verified that there was a
notable increase in the revenue of PHP <span style="color: GREEN">{{$textcontent2}}</span> thousand. The change in revenue is demonstrated
on the chart. The gross profit equaled PHP <span style="color: GREEN">{{$textcontent3}}</span> thousand during the last year. For the period
reviewed (31.12.{{substr(((int)$financial_report->year - @count($income_statements) + 1), 2)}}-
31.12.{{substr(((int)$financial_report->year),2)}}), the gross profit went up by PHP 
<span style="color: GREEN">{{$textcontent4}}</span> thousand, or by <span style="color: GREEN">{{$textcontent5}}</span>%.
</p>

<p style="text-indent: 50px;">For the period from 01/01/{{ ((int)$financial_report->year) }} to 12/31/{{ ((int)$financial_report->year) }}, the company posted a gross profit and
earnings before interest and taxes (EBIT), which was PHP {{$textcontent6}} thousand. The final
comprehensive income for {{ $financial_report->title }} was PHP {{$textcontent7}} thousand for the period
01.01–12/31/2014.
</p>

<div class = 'center'>
    <img src = '{{ URL::To("images/graph_2_1_".$income_statements[0]->financial_report_id.".png") }}' style = 'width: 530px;'>
</div>