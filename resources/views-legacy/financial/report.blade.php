<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="stylesheet" href="{{ URL::asset('css/financial_report.css') }}"  media="screen">
</head>
<body>
	<script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                if ($PAGE_COUNT > 1) {
                    $font = Font_Metrics::get_font("Arial, Helvetica, sans-serif", "normal");
                    $size = 8;
                    $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
                    $y = $pdf->get_height() - 20;
                    $x = ($pdf->get_width() - Font_Metrics::get_text_width($pageText, $font, $size)) / 2;
                    $pdf->text($x, $y, $pageText, $font, $size);
                }

                if ($PAGE_NUM > 1) {
                    $font = Font_Metrics::get_font("Arial, Helvetica, sans-serif", "normal");
                    $size = 7;

                    $str = "Financial Condition Analysis";
                    $x = $pdf->get_width() - Font_Metrics::get_text_width($str, $font, $size) - 20;
                    $pdf->text($x, 16, $str, $font, $size);

                    $str = "<?= addslashes($financial_report->title) ?> (cont\'d)";
                    $x = $pdf->get_width() - Font_Metrics::get_text_width($str, $font, $size) - 20;

                    $pdf->text($x, 24, $str, $font, $size);

                    $str = "Period: 01/01/<?= ((int)$financial_report->year - @count($balance_sheets) + 1) ?> to 12/31/<?= (int)$financial_report->year ?>";
                    $x = $pdf->get_width() - Font_Metrics::get_text_width($str, $font, $size) - 20;
                    $pdf->text($x, 32, $str, $font, $size);
                }
            ');
        }
    </script>

	<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" />
	<h1>
		{{ $financial_report->title }}'s Financial Condition Analysis
		for the period of (01/01/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})
		to (12/31/{{ (int)$financial_report->year }})
	</h1>

	@include('financial.toc')

	<h2>1. {{ $financial_report->title }}'s Financial Position Analysis</h2>

	@include('financial.1_1', array('balance_sheets'=>$balance_sheets))
	@include('financial.1_2', array('balance_sheets'=>$balance_sheets))
	@include('financial.1_3_1', array('balance_sheets'=>$balance_sheets))
	@include('financial.1_3_2', array('balance_sheets'=>$balance_sheets))
	@include('financial.1_4', array('balance_sheets'=>$balance_sheets))

	<h2>2. Financial Performance</h2>

	@include('financial.2_1', array('income_statements'=>$income_statements))
	@include('financial.2_2a', array('income_statements'=>$income_statements))
	@include('financial.2_2b', array('balance_sheets'=>$balance_sheets, 'income_statements'=>$income_statements))
	@include('financial.2_3', array('balance_sheets'=>$balance_sheets, 'income_statements'=>$income_statements))

	<h2>3. Conclusion</h2>

	@include('financial.3_1', array('balance_sheets'=>$balance_sheets, 'income_statements'=>$income_statements))
	@include('financial.3_2', array('balance_sheets'=>$balance_sheets, 'income_statements'=>$income_statements))
</body>
</html>