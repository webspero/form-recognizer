@extends('layouts.report')

@section('content')

<div class="page_height">
	<img src="{{ URL::to('/') }}/images/creditbpo-logo.jpg" alt="CreditBPO" />

	<h1>
		{{ $financial_report->title }}'s Financial Condition Analysis
		for the period of (01/01/{{ ((int)$financial_report->year - @count($balance_sheets) + 1) }})
		to (12/31/{{ (int)$financial_report->year }})
	</h1>

	<!-- include Table of Contents -->
	@include('financial.toc')

</div>

<div class="clear"></div>

<div class="page_height">

	<h2>1. {{ $financial_report->title }}'s Financial Position Analysis</h2>

	<!-- include Structure of Assets and Liabilities -->
	@include('financial.sal')

</div>

<div class="clear"></div>

<div class="page_height">
	<!-- include Net Assets -->
	@include('financial.net-assets')

</div>

<div class="clear"></div>

<div class="page_height">
	<!-- include Liquidity Analysis -->
	@include('financial.liquidity-analysis')

</div>

<div class="clear"></div>

<div class="page_height">
	<!-- include Financial SUstainability Analysis -->
	@include('financial.financial_sustainability')

</div>

<div class="clear"></div>

<div class="page_height">
	<h2>2. Financial Performance</h2>

	<!-- include Financial Results -->
	@include('financial.financial_result')
</div>

<div class="clear"></div>

<div class="page_height">

	<!-- include Profitability Ratios -->
	@include('financial.profitability-ratios')

</div>

<div class="page_height">

	<!-- include Turnover Ratios -->
	@include('financial.turnover-ratios')

</div>

@stop

