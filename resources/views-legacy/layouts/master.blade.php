<!doctype html>
<html lang="en">
<head>
    @section('header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="norton-safeweb-site-verification" content="k2241jroedp-06x-cirbgriw026othqbvfe6dn3iskm8s53we1nhp7-ignp0n52bn4vr6s3iwmr-bcieph8lmuz4gb88i67w1wnm8jyo--37dmfcr4y6u18hdnvid68k" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/jasny-bootstrap.min.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/navmenu-push.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/dataTables.bootstrap.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/validationEngine.jquery.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/hint.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/start/jquery-ui-1.10.4.custom.min.css') }}"  media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-slider.css') }}"  media="screen" />
    <!--    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}"></link>-->

    <!--new-->
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="screen" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--new end-->

        @show

        <!--new-->
        @yield('style')
        <!--new end-->

        <script type="text/javascript"> var BaseURL = '{{ URL::to("/") }}'; </script>
        <script src="{{ URL::asset('js/1.11.2.jquery.min.js') }}"></script>
        <script type = 'text/javascript'>
        /**------------------------------------------------------------------------
        |   Checks the loading status of the page
        |--------------------------------------------------------------------------
        |   @param [in]     NONE
        |   @param [out]    NONE
        |   @return         NONE
        |------------------------------------------------------------------------*/
        var checkPageLoadSts = function()
        {
            /** Variable Declaration    */
            var loading_sts = 0;
            var start_time  = new Date();

            /** After 60 seconds have passed                        */
            setTimeout(function() {

                if (0 == loading_sts) {

                    if (typeof modal !== 'undefined' && $.isFunction(modal)) {
                        $('#refresh-page-modal').modal({
                            keyboard: false,
                            backdrop: 'static',
                        }).modal('show');

                        refreshPageCountdown(5);
                    }
                    else {
                        alert ('You maybe experiencing a slow or intermittent connection as of the moment. If this problem persists, contact your Internet Service Provider or move to a location with better connection. This page will refresh in 5 seconds');
                        setTimeout(function() {
                            location.reload();
                        }, 5000);
                    }
                }
            }, 60000);

            /** Detect if all resources of the page has been loaded */
            $(window).load(function() {

                $('div.panel-body').css('display', 'block');
                loading_sts++;

                /** Time elapsed of loading all resources   */
                var end_time        = new Date();
                var millisecs       = end_time - start_time;
                var total_secs      = parseInt(millisecs / 1000);
                var mins            = parseInt(total_secs / 60);
                var secs            = total_secs % 60;

                /* console.log('Time elapsed of resources: '+ mins + ' minutes and '+ secs + ' seconds'); */
            });
        }

        /**------------------------------------------------------------------------
        |   Counts down to 0 and refresh the page
        |--------------------------------------------------------------------------
        |   @param [in]     NONE
        |   @param [out]    NONE
        |   @return         NONE
        |------------------------------------------------------------------------*/
        var refreshPageCountdown = function(countdown)
        {
            /** Variable Declaration    */

            /** Countsdown and refreshes page when timer reaches 0  */
            setTimeout(function() {
                countdown--;
                $('#countdown-refresh-num').text(countdown+'...');

                if (0 < countdown) {
                    refreshPageCountdown(countdown);
                }
                else {
                    location.reload();
                }
            }, 1000);
        }

        checkPageLoadSts();

    </script>

    @if(env('APP_ENV') == 'Production')
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '813561166218738');
            fbq('track', 'PageView');
        </script>

        <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=813561166218738&ev=PageView&noscript=1"/>
        </noscript>
    @else
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '508104823617882');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=508104823617882&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    @endif

</head>
<body>
    @if(Auth::check())
    @if ((Session::get('enable_support') == 0) && (Auth::user()->role == 1))

    @else
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'> (function(){ var widget_id = 'EPm8g31tw4'; var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <script type = 'text/javascript'>
        function jivo_onIntroduction()
        {
            var client_data = jivo_api.getContactInfo();
            var ajax_url    = BaseURL+'/ajax/save_jivo_leads';

            $.post(ajax_url, client_data, function() {
                /** No Processing   */
            });
        }
    </script>

    @if (0)

    @if (isset(Auth::user()->email))
    <script>
        var jivo_mail = '{{ Auth::user()->email }}';
    </script>
    @else
    <script>
        var jivo_mail = '';
    </script>
    @endif
    <script>

        $(document).ready(function(){
            var invite_docs = 0;
            var fs_template_cntr    = $('#fs-template-cntr');
            var rep_status          = $('#status');
            var user_role           = $('#userid');

            if ((fs_template_cntr.length) && (rep_status.length) && (user_role)) {
                if ((0 == rep_status.val()) && (1 == user_role.val())) {
                    $(window).scroll(function() {
                        var window_scroll   = $(this).scrollTop() + $(this).innerHeight();
                        var fs_div_top      = fs_template_cntr.offset().top;

                        if (window_scroll > fs_div_top) {

                            if (0 == invite_docs) {
                                invite_docs = 1;
                                jivo_api.showProactiveInvitation("Paalala lang po, pakihanda ang mga kinakailangang dokumento para sa pag upload");
                            }
                        }
                    });
                }
            }
        });

        function jivo_onLoadCallback()
        {
            jivo_api.open();

            setTimeout(function() {
                var jivo_iframe     = $("#jivo_container");
                var jivo_chat_area  = $('#jivo_container').contents().find('#jivo_iframe_wrap').find('.td-textarea textarea');
                var jivo_chat_btn   = $('#jivo_container').contents().find('#jivo_iframe_wrap').find('.jivoBtn');

                if (0 < jivo_chat_area.length) {

                    if ('' != jivo_mail) {
                        jivo_chat_area.val('Hi, this is from '+ jivo_mail +', can you help me? Pwede mo ba akong tulungan?');
                    }
                    else {
                        jivo_chat_area.val('Hi, can you help me? Pwede mo ba akong tulungan?');
                    }

                    jivo_chat_area.css('height', '60px');
                    jivo_chat_area.css('overflow-y', 'hidden');
                    jivo_chat_area.click();
                    jivo_chat_area.focus();

                    jivo_chat_btn.prop('disabled', false);
                //jivo_chat_btn.click();
                //jivo_chat_btn.prop('disabled', true);

            }
        }, '7000');
        }
    </script>
    @endif
    <!-- {/literal} END JIVOSITE CODE -->
    @endif
    @endif
    <!--new-->
    <div id="wrapper">
        <div class="print-only company-logo" style="display: none;">
            <img src="<?php echo URL::asset('images/creditbpo-logo.png'); ?>"" alt="" width="170" style="margin-top:-5px" />
        </div>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header" style="height:60px;">
                @if(isset($hide) && $hide == true)

                @else
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                @endif
                <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ URL::asset('images/creditbpo-logo.png') }}" alt="" width="170" style="margin-top:-5px"></a>
            </div>

            @section('nav')
            @include('dashboard.top-right')
            @show

        </nav>

        @yield('content')
        <div class="clearfix"></div>
        @include('footer')
    </div>

    <div id="hint_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <input type="hidden" class="group" />
      <input type="hidden" class="item" />
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><span class="icon-more-info glyphicon glyphicon-exclamation-sign" aria-hidden="true" style="color: #2a99c6;"></span> More info...</h4>
        </div>
        <div class="modal-body">
            <p></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!--new end-->

<!--    <script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>-->
<script src="{{ URL::asset('js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/dataTables.bootstrap.js') }}"></script>
<script src="{{ URL::asset('js/ui/minified/jquery-ui-1.10.4.custom.min.js') }}"></script>
<script src="{{ URL::asset('js/languages/jquery.validationEngine-en.js') }}"></script>
<script src="{{ URL::asset('js/jquery.validationEngine.js') }}"></script>
<script src="{{ URL::asset('js/entity.js') }}"></script>
<script src="{{ URL::asset('js/select2.min.js') }}"></script>
<script src="{{ URL::asset('js/form-autocomplete.js') }}"></script>
<script src="{{ URL::asset('js/popup_hint.js') }}"></script>
<script src="{{ URL::asset('js/cookie-mng.js') }}"></script>
<script src="{{ URL::asset('js/createNewReport.js') }}"></script>
<script type="text/javascript">
    Lang = {!!LanguageHelper::getAll()!!};
    function trans(sData){
        sCommand = sData.split('.');
        return Lang[sCommand[0]][sCommand[1]];
    }
    function convert(sGroup, sKey){
        if(sKey!=null && sKey!=''){
            if(Lang[sGroup][sKey]){
                return Lang[sGroup][sKey];
            }
        }
        return sKey;
    }
    // set csrf token in every ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // refresh token to avoid 419 Expired Issue.
    setInterval(() => {
        $.get('/refresh_csrf').done(function(data){
            $("[name='_token']").val(data);
            $("meta[name='csrf-token']").attr("content",data);
        });
    }, 180000);
</script>

@yield('javascript')

<script type="text/javascript" src="{{ URL::asset('static/js/modules/require.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/config.js') }}"></script>

@if (!Auth::check() || (Auth::check() && Auth::user()->role != 0) || (Auth::check() && Auth::user()->role == 0 && Session::has('supervisor')))
    <!--START ZOHO SalesIQ-->
    <script type="text/javascript">
        require(['/static/js/modules/zoho/index.js'], function(zohoInstance) {
          zohoInstance.initialize();
        });
    </script>
    <!--END ZOHO SalesIQ-->
@endif

@yield('requirejs')

<style type="text/css">
	.grecaptcha-badge{
		left:0 !important;
	}
</style>

</body>
</html>
