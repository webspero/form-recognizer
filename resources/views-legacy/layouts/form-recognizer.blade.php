<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}"  media="screen" />

    <!--new-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/from-recognizer.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css/resource-management.css') }}" media="screen" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--new end-->
</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header" style="height:60px;">
          <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ URL::asset('images/creditbpo-logo.png') }}" alt="" width="170" style="margin-top:-5px"></a>
      </div>
      @include('dashboard.top-right')
  </nav>
  <!-- Navigation -->
  <div id="loader"></div>
  <div class="container-fluid">
    <div class="text-right">
      <button type="button" onclick="window.location='{{ route($route) }}'" style="margin :4px; background: #348998; border: none; color: #fff; text-transform: uppercase;font-size: 16px; letter-spacing: 1px;" name="button">
        {{$name}}
      </button>
    </div>
    @yield('content')
  </div>
</body>
</html>
<script src="https://code.jquery.com/jquery.js"></script>
<script>
  //adds event listener to the file uploader button
  document.querySelector("#upload-button").addEventListener("click", () => {
      //clicks on the file input
      document.querySelector("#hidden-upload").click();
  });

  //adds event listener on the hidden file input to listen for any changes
  document.querySelector("#hidden-upload").addEventListener("change", (event) => {
    //gets the file name
    document.querySelector("#upload-name").value = event.target.files[0].name;
  });
</script>
@stack('scripts')
