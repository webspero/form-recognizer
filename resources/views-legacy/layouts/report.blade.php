<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="{{ URL::asset('css/financial_report.css') }}"  media="screen">

</head>
<body>
    
    @yield('content')
    
</body>
</html>
