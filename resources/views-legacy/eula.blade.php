@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h2>End User License Agreement - Terms of Use</h2>
				<p>This is an agreement between the end user and CreditBPO Tech, Inc. -Read carefully before using this web application. By using this web application, you agree with these terms of use.</p>
				<ol>
					<li>This application (hereinafter referred to as "the Application") is an online RatingTool web application provided by CreditBPO Tech, Inc.(hereinafter referred to as "CREDITBPO").</li>
					<li>You cannot use the Application without accepting the terms and conditions of this End User License Agreement (EULA).Once you open the Application, it is considered that you accepted all of the terms and conditions of the EULA and you will be licensed to use the Application for storing data regarding your own business, solely in connection with your computer devices as set out below.</li>
					<li>Scope of permitted use.This application is for your individual use, solely for internal useas a diagnostic tool for your businessby you.  CREDITBPOgrants you a limited,nontransferable, nonsublicensable, revocable license to the Application only in the manner presented by CREDITBPO. You shall not disguise the origin of information transmitted through the Application. You will not place false or misleading information onthe Application.  You will not input or upload to the Applcation any information which contains viruses, Trojan Horses, worms, time bombs or other computer programming routines that are intended to damage, interfere with, intercept or expropriate any system, the Application or contents or that infringes the rights of another. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, sublicense, transfer, assign, rent, sell or otherwise convey any information, software, products or services obtained from the Application without the prior written consent from CREDITBPO.You may not disassemble, decompile or otherwise reverse engineer all or any portion of the Application. You agree not to export or re-export the Application, directly or indirectly, to any country. CREDITBPO does not grant to you any express or implied rights to CREDITBPO's or any third party's intellectual property.</li>
					<li>Upon your acceptance of the EULA, it is considered that you agreed tochanging the terms and conditions of EULA at any time by CREDITBPOwithout your separate acceptance. When such changes are made to EULA, you shall be bound to the updated version.</li>
					<li>All the rights in the Application are owned by CREDITBPOand/or any third parties from whom CREDITBPO received the rights to provide the Application.</li>
					<li>Under all conditions, CREDITBPO can stop distributing the Application at our sole discretion.</li>
					<li>No warranty; limitations of liability.
						<ol style="list-style-type: lower-alpha;">
							<li>CREDITBPO provides the application "as is," "with all faults" and "as available." CREDITBPO does not guarantee the accuracy or timeliness of information available from the application.Your use of the application is at your sole risk.CREDITBPO gives no express warranties, guarantees or conditions.CREDITBPO excludes any implied warranties including those of merchantability, fitness for a particular purpose, workmanlike effort, title and non-infringement.</li>
							<li>CREDITBPO makes no representations about the suitability, reliability,availability, timeliness, or lack of viruses or other harmful components related to the application.</li>
							<li>You understand and agree thatCREDITBPO shall not be liable to you for any direct, indirect, punitive, incidental, special consequencial damages or anydamages whatsoever including, without limitation damages for loss of use, data, or profits, arising out of or in any way connected with the use or performance of the application, with delay or inability to use the application, or the provision or failure to provide the application, whether based on contract, tort, negligence, strict liability or otherwise, even if CREDITBPO has been advised of the possibility of damages.</li>
							<li>Each provision of this clause excluding or limiting liability shall be construed separately, applying and surviving even if for any reason one or other of these provisions is held inapplicable or unenforceable in any circumstances and shall remain in force notwithstanding the expiration or termination of this agreement.</li>
						</ol>
					</li>
					<li>At all times your information provided in conjunction with the use of the Application will be treated in accordance with Privacy Policy of CREDITBPO.</li>
					<li>Philippine law governs the interpretation of this Agreement, regardless of conflict of laws principles.You and we irrevocably consent to the exclusive jurisdiction and venue of the courts in Philippinesfor all disputes arising out of or relating to this Agreement. Your use of the Application may also be subject to other international laws.</li>
					<li>The end user's rights under the license will terminate automatically without notice if user fails to comply with any term(s) of this license. Upon termination of the license, user should cease all use of the Application, and destroy all copies, if any.</li>
				</ol>
				<br/><br/>
				<p align="center"><a class="btn btn-primary" onclick="window.close(); return false;">Close this window</a></p>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
