@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Add: Recipient </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
                {{Form::open(array('method' => 'post', 'route' => 'saveInnodataRecipient'))}}
                    <div class="form-group">
						<div class="row">
							<div class="col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
								{{Form::label('name', 'Name')}}
								{{Form::text('name', $errors->has('name') ? Input::old('name') : "", array('class' => 'form-control'))}}
							</div>
						</div>
					</div>
					<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
						{{Form::label('email', 'Email Address')}}
						{{Form::text('email', $errors->has('email') ? Input::old('email') : "", array('class' => 'form-control', 'required' => 'required'))}}
					</div>

					<div class="form-group {{$errors->has('type') ? 'has-error' : ''}}">
						{{ Form::label('type', 'Recipient Type') }}
						{{ Form::select('type', [1 => 'Innodata', 2 => 'CreditBPO'], null, array('class' => 'form-control', 'placeholder' => 'Select Type', 'required' => 'required')) }}
					</div>
                    <div class="form-group text-center">
                        {{Form::submit('Save', array('class' => 'btn btn-primary'))}}
                        <a href="{{ route('getInnodataRecipients')}}" class="btn btn-default">Back</a>
                    </div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
