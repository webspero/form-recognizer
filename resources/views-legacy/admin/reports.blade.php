@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
    <div class="row col-md-12">
        <p class="text-right">
        <p class="col-md-9"></p>
            <p class="text-right col-md-3">
                <a class="btn btn-primary btn-block" data-toggle="modal" data-target="#weeklyModal">Weekly Report</a>
            </p>
        </p>
        <div>
            <p class="col-md-9">
                
            </p>
            <p class="text-right col-md-3">
                <a type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exportModal">Export Data</a>
            </p>
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12">
		{{Form::open(array('route'=>'getReports', 'method'=>'get', 'class'=>'form-inline'))}}
			<div class="form-group">
				{{Form::label('search', 'Search')}}
				{{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'id'=>"input_search", 'placeholder'=>'search...'))}}
			</div>
			<div class="form-group">
				{{Form::select('type', array('reportid'=>'Report ID', 'companyname'=>'Compay Name', 'basic_email'=>'Basic User Email', 'organization' => 'Organization', 'date_started'=> 'Date Started', 
                    'date_finished'=> 'Date Finished', 'discount_code' => 'Discount Code'), Input::has('type') ? Input::get('type') : 'email', array('class'=>'form-control', 'id' => 'type'))}}
			</div>
			<div class="form-group">
				{{Form::submit('Go', array('class'=>'btn btn-primary'))}}
			</div>
		{{Form::close()}}
		</div>
	</div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Basic User Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Organization</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Started</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Finished</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report Type</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Price</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Rating</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Payment Status</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Payment Method</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Discount Code</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Client Key</th>
				</tr>
			</thead>
			<tbody>
                @if(count($reports) == 0)
                    <tr>
                        <td colspan =12 style="text-align:center;"><i><strong>No records found.</strong></i></td>
                    </tr>
                @endif
			    @foreach($reports as $report)
                    <tr>
                        <td>{{$report->entityid}}</td>
                        <td>{{$report->companyname}}</td>
                        <td>{{$report->email}}</td>
                        <td>{{$report->bank_name}}</td>
                        <td>{{$report->created_at}}</td>
                        <td>{{$report->completed_date}}</td>
                        <td>
                            @if($report->is_premium == 1)
                                Premium
                            @elseif($report->is_premium)
                                Simplified
                            @else
                                Standalone
                            @endif
                        </td>
                        <td>{{$report->price}}</td>
                        <td>{!! $report->rating !!}</td>
                        <td>{{$report->is_paid}}</td>
                        <td>{{$report->payment_method}}</td>
                        <td>{{$report->discount_code}}</td>
                        <td>{{$report->serial_key}}</td>
                    </tr>
                @endforeach
			</tbody>
		</table>
		{{$paginator->appends(array('search'=>Input::get('search'),'type'=>Input::get('type'),'role'=>Input::get('role')))->links()}}
	</div>
</div>

<!-- Modal for Export Data -->
<div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-target=".bd-example-modal-sm">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Export Data</h5>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group col-md-6">
                <label for="from" class="col-form-label">From:</label>
                <input type="date" class="form-control col-md-6" id="from_date">
            </div>
            <div class="form-group col-md-6">
                <label for="to" class="col-form-label">To:</label>
                <input class="form-control" id="to_date" type="date" disabled> 
            </div>
            </form>
            <div class="form-group text-center">
                <br><br>
                <button type="button" class="btn btn-primary btn-lg" id="export" disabled>Export</button>
            </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal for weeekly report-->
<div class="modal fade" id="weeklyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Weekly Report</h5>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="">Add Contact Email Address</label>
                <div class="input-group">
                    <input type="text" class="form-control" required name="email" id="agency_email" >
                    <span class="input-group-btn">
                        <button id="btnAddEmail" class="btn btn-success">+</button>
                    </span>
                </div>
                <label for="email" class="errormessage" id="error_email" hidden></label>
            </div>
            @if($reports_receiver)
            <table class="table table-bordered" id="agency_table">
                <thead>
                    <th> Email </th>
                    <th> Action </th>
                </thead>
                <tbody>
                    @if(count($reports_receiver) == 0)
                        <tr>
                            <td colspan="2" style="text-align:center;">
                                No records found
                            </td>
                        </tr>
                    @else
                        @foreach($reports_receiver as $email)
                        <tr> 
                            <td> {{$email->email}}</td>
                            <td> <a href="{{ URL::to('/admin/reports/receiver/'.$email->id) }}" id="delete_agency" class="btn btn-link" > Delete</a>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @endif
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script>
    $('#type').change(function(){
        if( ($('#type').val() == 'date_finished') || ($('#type').val() == 'date_started') ){
            $("#input_search").prop("type", "date");
        }else{
            $("#input_search").prop("type", "text");
            $("#input_search").val('');
        }
    });

    if( ($('#type').val() == 'date_finished') || ($('#type').val() == 'date_started') ){
        $("#input_search").prop("type", "date");
    }else{
        $("#input_search").prop("type", "text");
    }

    /** Date Picker */
    $('#from_date').change(function(){
        $("#to_date" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: $('#from_date').val()
        });
        $("#to_date").prop( "disabled", false );
    });

    $('#to_date').change(function(){
        $("#export").prop( "disabled", false );
    });

    $('#export').click(function(){
        $.ajax({
            url: BaseURL + '/admin/reports/export',
            type: 'POST',
            data: {
                from:  $('#from_date').val(),
                to :  $('#to_date').val()
            },
            success: function(data){
                $('#exportModal').modal('hide');
                window.open(BaseURL + "/admin/export/download");
            }
        });
    });

    $('#delete_agency').click(function(){
        console.log('get value of the button is ' + $('#delete_agency').val());
    });

    // Click add button emails
    $('#btnAddEmail').click(function(){
        agency_email = $('#agency_email').val();

        if(agency_email== ''){
            $('#error_email').html('Email field is required');
            $('#error_email').show();
        }else{
            $.ajax({
                url: BaseURL + '/admin/reports/receiver',
                type: 'POST',
                data: {
                    agency_email:  agency_email
                },
                success: function(data){
                    if(data.error){
                        $('#error_email').html(data.error);
                        $('#error_email').show();
                    }else{
                        $('#error_email').hide();
                        $('#agency_email').val('');
                        /** reload table display */
                        var trHTML = '';
                        var trHTML =
                            "<thead><th scope='col' class='col-md-7 text-center'>Email</th><th scope='col' class='col-md-4 text-center'>Action</th><thead><tbody>";
                        if (data.length == 0) {
                            trHTML +=
                                '<tr><td colspan="2" style="text-align:center; color:gray;"><i>No contact agency</td></i></tr>';
                        } else {
                            $.each(data, function(i, item) {
                                id = data.id;
                                trHTML += '<tr><td>' + item.email +
                                    '</td><td><a id="delete_agency" value="' + item.id + '"' +
                                    "href=" + BaseURL + "/admin/reports/receiver/" + item.id + '"'
                                    + '">Delete</a></td>';
                                trHTML += '</tr>';
                            });
                        }
                        $('#agency_table').html(trHTML);
                    }
                }
            });
        }
    });
 


</script>
@stop
