@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')

<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div  class="col-lg-8 col-md-8 col-sm-8">
            <table id="entitydata" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center; background-color: #055688;color: #fff; font-weight: bold; font-size:20px" colspan="2">Blocked Addresses
                                <button type="button" href = "#add_new_ipadd" name="subscription_status" data-toggle="modal" class="btn btn-success add-new pull-right"><i class="fa fa-plus"></i> Add New</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-weight: bold; text-align:center; font-size: 11px;">Email</td>
                            <td style="font-weight: bold; text-align:center; font-size: 11px;">Action</td>
                        </tr>
                        
                        @if(count($address) == 0)
                            <tr>
                                <td colspan="2" style="text-align:center;"> <i>No blocked address</i></td>
                            </tr>
                        @else
                            @foreach($address as $key => $add)
                                <tr>
                                    <td style=" text-align:center;">{{$add->email_address}}</td>
                                    <td style=" text-align:center;">
                                        <button href="#add_new_ipadd" data-toggle="modal" type="button" id="btn_edit" class="btn btn-default navbar-btn btn-xs" title="Edit" data-id = "{{ $add->id}}" data-email = "{{$add->email_address}}"><span class="glyphicon glyphicon-edit"></span></button>
                                        <button href="#delete_record_address" data-toggle="modal" type="button" id="btn_delete" class="btn btn-default navbar-btn btn-xs" title="Delete" data-id = "{{ $add->id}}" data-email = "{{$add->email_address}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    
                    </tbody>
                </table>
                {{$address->links()}}
        </div>
    </div>
</div>

<div id="add_new_ipadd" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" style="font-weight: bold;">Add Block Address</h4>
                </div>
            <div class="modal-body">
                <div id = 'ci_err_msg' style = 'display:none' class = 'alert alert-danger'></div>
                <form  id="add_ipadd" method = "post" action = "edit">
                    <input type="hidden" name="user_id" id="user_id" value=""/>
                </form>
                <table class="table table-bordered">
                    <tr>  
                        <td class="text-left row">Email Address:</td>
                        <td class="row"><input class="form-control" name="email_address" id="email_address"/></span>
                    </tr>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_submit" name="btn_submit"  class="btn btn-primary">Save</button>
                <button type="button" id="btn_update" name="btn_update"  class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

<div id="delete_record_address" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <br>
      </div>
      <div class="modal-body">
        <input type="hidden" name="user_id_delete" id="user_id_delete" value=""/>
        <p>Are you sure you want to delete this address?</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="btn_delete_record" name="btn_delete_record" class="btn btn-warning">Delete</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(function(){
        $("button[name='btn_submit']").on("click",function(){
            $.ajax({
                url: '/admin/security',
                type: 'post',
                dataType: 'json',
                data: {
                    email:$('#email_address').val(), 
                },
                success: function(data) {
                    if(data == 1){
                        $("#add_new_ipadd").modal('hide');
                        location.reload();
                    }else{
                        $('#ci_err_msg').html(data[0]);
                        $('#ci_err_msg').css('display', 'block');
                    }
                }
            });
        })

        $("button[name='btn_update']").on("click",function(){
            $.ajax({
                url: '/admin/security/update',
                type: 'post',
                dataType: 'json',
                data: {
                    email:$('#email_address').val(),
                    id:$("#user_id").val()
                },
                success: function(data) {
                    $("#add_new_ipadd").modal('hide');
                    location.reload();
                }
            });
        })

        $("button[name='btn_delete_record']").on("click",function(){
            $.ajax({
                url: '/admin/security/delete',
                type: 'post',
                dataType: 'json',
                data: {
                    id:$("#user_id_delete").val()
                },
                success: function(data) {
                    $("#delete_record_address").modal('hide');
                    location.reload();
                }
            });
        })

         $("#delete_record_address").on("shown.bs.modal",function(e){
            var link     = e.relatedTarget,
            modal    = $(this),
            data = $(link).data();

            $("#user_id_delete").val(data.id);
        })

        $("#add_new_ipadd").on("shown.bs.modal",function(e){
            var link     = e.relatedTarget,
            modal    = $(this),
            data = $(link).data();

            if(data.email && data.id){
                $("#email_address").val(data.email);
                $("#user_id").val(data.id);

                $("#btn_submit").hide();
                $("#btn_update").show();
            }else{
                $('#ci_err_msg').css('display', 'none');
                $('#email_address').val('');

                $("#btn_submit").show();
                $("#btn_update").hide();
            }
        })
    });
</script>

@stop