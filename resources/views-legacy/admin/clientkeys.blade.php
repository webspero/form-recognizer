@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
		<div class="col-sm-6"><a href="{{URL::to('/admin/create_keys')}}" class="btn btn-primary">Create Additional Keys</a></div>
		<div class="col-sm-2 text-right">Registered Keys: {{ $used }}</div>
		<div class="col-sm-2 text-right">Unregistered Keys: {{ $unused }}</div>
		<div class="col-sm-2 text-right">Total Keys: {{@count($entity)}}</div>

	</div>
	<div class="clearfix"></div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email Address</th>					
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Created</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Name on Certificate</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Is Claimed?</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">URL</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($entity as $info)
				<tr>
					<td>{{ $info->email }}</td>					
					<td>{{ $info->created_at }}</td>
					<td>{!! $info->status !!}</td>
					<td>{{ $info->name }}</td>
					<td>{{ $info->used }}</td>
					<td>
						@if($info->is_used == 1)
							&nbsp;
						@else
							<a href="{{URL::to('/')}}/certificate/{{$info->code}}">{{URL::to('/')}}/certificate/{{$info->code}}</a>
						@endif
					</td>
					<td>
						@if($info->is_used == 0)
							<a href="{{URL::to('/')}}/admin/delete_key/{{$info->id}}" onclick="return confirm('Are you sure you want to delete {{ $info->serial_key }}?');">delete</a>
						@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
@stop
