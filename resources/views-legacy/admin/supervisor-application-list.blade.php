@extends('layouts.master')

@section('header')
	@parent
    <title>CreditBPO</title>
    <style>
        .modal-dialog{
            top: 200px;
        }
    </style>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">    
    <div class="row">
        <div class="col-md-4">
		    <h3> Supervisor Accounts </h3>
        </div>
        <div class="col-md-8">
        <div id="bank-sme-listing-filter">
            {{Form::open(array('route'=>'getSupervisorApplications', 'method'=>'get', 'class'=>'form-inline text-right'))}}
                <div class="form-group">
                    {{Form::select('type', array(
                        'email'=>'Email',
                        'organization'=>'Organization'
                    ), Input::has('type') ? Input::get('type') : 'email', array('class'=>'form-control', 'id'=>'type_select'))}}
                </div>
                <div class="form-group search selector">
                    {{Form::label('search', 'Search')}}
                    {{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'placeholder'=>'search...'))}}
                </div>
                <div class="form-group">
                    {{Form::submit('Go', array('class'=>'btn btn-primary'))}}
                </div>
                <a href="{{ URL::To('supervisor-signup') }}" class="btn btn-primary show-feedback-summary">Supervisor Sign Up Form</a>
            {{Form::close()}}
        </div>
    </div>
	<hr/>
    
    <br/>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered table-hover" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Organization Type</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Organization</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Subscription</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Product</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date registered</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">ID Picture</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Questionnaire Type</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
            @if (@count($supervisors) > 0)
                @foreach ($supervisors as $supervisor)
                    @php
                        if($supervisor->status === 3) continue;
                    @endphp

                    @if ($supervisor->status !== 0)
                    <tr >
                    @else
                    <tr>
                    @endif
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>
                            @if($supervisor->name)
                                {{$supervisor->name}}
                            @else
                                {{ $supervisor->name_opt }}
                            @endif
                        </td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>{{ $supervisor->email }}</td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>{{ $supervisor->organization_type }}</td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>{{ $supervisor->organization }}</td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>
                            <a href = "#edit_subscription" name="subscription_status" data-toggle="modal" data-id = "{{ isset($supervisor->loginid) ? $supervisor->loginid : 0}}" data-supervisor="{{ $supervisor->name }}" data-status ="{{$supervisor->subscription_status}}" > {{$supervisor->subscription_status}} </a>
                        </td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>
                            @if (1 == $supervisor->product)
                                Plan A
                            @elseif (2 == $supervisor->product)
                                Plan B
                            @elseif (3 == $supervisor->product)
                                Plan C
                            <!-- default is Plan A -->
                            @else
                                Plan A
                            @endif
                        </td>
                        <td>
                            @if($supervisor->application_date == null)
                                {{$supervisor->date_registered}}
                            @else
                                {{$supervisor->application_date}}
                            @endif
                        </td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>
                            @if ($supervisor->picture != STR_EMPTY || $supervisor->picture != NULL)
                                <a href = '{{ URL::To("download/images/".$supervisor->picture) }}' /> Download </a>
                            @else
                                No Picture
                            @endif
                        </td>
                        <td role="button" data-href='{{URL::To("/bank/dashboard?bank=".$supervisor->loginid)}}'>
                            @if ($supervisor->questionnaire_type == QUESTION_TYPE_CORP)
                                Corporate
                            @elseif ($supervisor->questionnaire_type == QUESTION_TYPE_GOVT)
                                Government
                            @else
                                Bank
                            @endif
                        </td>
                        <td>
                            @if ($supervisor->status === 0)
                                <!-- <a href = '{{ URL::To("supervisor/approve_application/".$supervisor->application_id) }}'>
                                    Approve Application
                                </a> -->

                                <button type="button" class="btn btn-primary btn-sm mb-1" onclick="confirmApplication('approve',{{ $supervisor->application_id }})" style="margin-bottom: 5px;">
                                    Approve
                                </button><br/>
                                <button type="button" class="btn btn-danger btn-sm" onclick="confirmApplication('reject',{{ $supervisor->application_id }})">
                                    Reject
                                </button>

                            @endif
                              
                            @if ($supervisor->status === 2)

                                <span class="mb-1 text-danger">Rejected</span><br/>
                                <button type="button" class="btn btn-danger btn-sm" onclick="confirmApplication('delete',{{ $supervisor->application_id }})">
                                    Delete
                                </button>
                            @endif

                            @if ($supervisor->status == 1 || ($supervisor->status !== 2 && $supervisor->status !== 0))

                                <span class="mb-1">Active</span><br/>
                                <button type="button" class="btn btn-danger btn-sm" onclick="confirmApplication('delete',{{ $supervisor->application_id }})">
                                    Delete
                                </button>
                            @endif

                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan=9 style="text-align: center; font-style: italic">No data found.</td></tr>
            @endif
			</tbody>
		</table>
        {{$paginator->appends(array('search'=>Input::get('search')))->links()}}
	</div>
<div id="edit_subscription" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Subscription</h4>
                </div>
            <div class="modal-body">
                <div id = 'ci_err_msg' style = 'display:none' class = 'alert alert-danger'> </div>
                <form  id="edit_supervisor" method = "post" action = "edit">
                    <input type="hidden" name="supervisor_id" id="supervisor_id" value=""/>
                </form>
                <table class="table table-bordered">
                    <tr>  
                        <td class="text-left">Name:</td>
                        <td ><span id="supervisor_name"> </span>
                    </tr>
                    <tr> 
                        <td class="text-left" >Subscription Status:</td>
                        <td ><span id="subscription_status"> </span>
                    </tr>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-ci-modal" data-dismiss="modal">Close</button>
                <button type="button" name="btn_submit" id="btn_unsubscribe" class="btn btn-danger" style="display:none">Unsubscribe</button>
                <button type="button" name="btn_submit" id="btn_subscribe" class="btn btn-success" style="display:none">Subscribe</button>
            </div>
        </div>
    </div>
</div>

<div id="approve_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Approve Application</h4>
                </div>
            <div class="modal-body">
                <span class="text-primary">Are you sure you want to <strong>APPROVE</strong> this application?</span>
                <div class="clearfix"></div>                
                
            </div>
            <div class="modal-footer">
                <a href="#" name="btn_submit" id="btn_approve" class="btn btn-primary">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="reject_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Reject Application</h4>
                </div>
            <div class="modal-body">
                <span class="text-danger">Are you sure you want to <strong>REJECT</strong> this application?</span>
                <div class="clearfix"></div>
                                
            </div>
            <div class="modal-footer">
                <a href="#" name="btn_submit" id="btn_reject" class="btn btn-primary">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="delete_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete Application</h4>
                </div>
            <div class="modal-body">
                <span class="text-danger">Are you sure you want to <strong>DELETE</strong> this application?</span>
                <div class="clearfix"></div>
                
            </div>
            <div class="modal-footer">
                <a href="#" name="btn_submit" id="btn_delete" class="btn btn-primary">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
$(function(){
    var item = "" ; 
    $(".table").on("click", "td[role=\"button\"]", function (e) {
        var elemTxt = $(e.target).text().trim();
        if($(e.target).is("a") && (elemTxt == "Inactive" || elemTxt == "Active" || elemTxt == "Unpaid" )) {
        } else {
            window.location = $(this).data("href");
        }
     });
     $("#edit_subscription").on("shown.bs.modal",function(e){
        var link     = e.relatedTarget,
        modal    = $(this),
        data = $(link).data();
        if($(link).text().trim() == "Active"){
            $("#btn_unsubscribe").show();
            $("#btn_subscribe").hide();
        }else{
            $("#btn_unsubscribe").hide();
            $("#btn_subscribe").show();
        }

        $("#supervisor_name").text(data.supervisor);
        $("#subscription_status").text(data.status);
        $("#supervisor_id").val(data.id);
    });
    $("#edit_subscription").on("hidden.bs.modal", function(e){
        $("#btn_unsubscribe").hide();
        $("#btn_subscribe").hide();
    })

    $("button[name='btn_submit']").on("click",function(){
        let sub = false;
        if(this.id == "btn_subscribe"){
            sub = true;
        }
        $.ajax({
            url: '/supervisor/edit',
            type: 'post',
            dataType: 'json',
            data: {
                id:$('#supervisor_id').val(), 
                status: $("#subscription_status").text().trim(),
                subscription: sub
            },
            success: function(data) {
                location.reload();
            }
        });
    })
});

function confirmApplication(status,id){

    let url = "{{ url('/')}}";
    $href = url + '/supervisor/approve_application/'+status+'/'+id;

    if(status == 'approve'){
        $('#approve_confirmation').modal('show');
        $('#btn_approve').attr("href",$href);
    }

    if(status == 'reject'){
        $('#reject_confirmation').modal('show');
        $('#btn_reject').attr("href",$href);
    }

    if(status == 'delete'){
        $('#delete_confirmation').modal('show');
        $('#btn_delete').attr("href",$href);
    }

}
</script>


@stop
