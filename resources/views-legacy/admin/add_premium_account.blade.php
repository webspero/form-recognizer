@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Add: Third Party </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::open(array('method' => 'post', 'route' => 'postAddPremiumUser'))}}
					<div class="form-group">
						<div class="row">
                            <div class="col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                {{Form::label('firstname', 'Name')}}
                                {{Form::text('name', $errors->has('name') ? Input::old('name') : "", array('class' => 'form-control'))}}
                            </div>
						</div>
					</div>
					<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
						{{Form::label('email', 'Email Address')}}
						{{Form::text('email', $errors->has('email') ? Input::old('email') : "", array('class' => 'form-control'))}}
					</div>
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin/third_party" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
	$('select#role').change(function(){
		if($(this).val() == "4"){
			$('.bank-selection').show();
			$('.other-selection').hide();
		} else if($(this).val() == "5" || $(this).val() == "6" || $(this).val() == "7") {
			$('.other-selection').show();
			$('.bank-selection').hide();
		} else {
			$('.bank-selection').hide();
			$('.other-selection').hide();
		}
	});
	$('select#role').change();
    $('#bank-select').select2();
</script>
@stop
