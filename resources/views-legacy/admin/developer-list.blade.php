@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
    <p class="text-right">
        <a href="{{ URL::To('/register/developers') }}" class="btn btn-primary show-feedback-summary">Register new Developer</a>
    </p>
    <div>
		<h3> Developer API Users </h3>
	</div>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Client ID</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Secret Key</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($developers as $developer)
				<tr>
                    <td>{{ $developer->client_id }}</td>
                    <td>{{ $developer->firstname.' '.$developer->lastname }}</td>
                    <td>{{ $developer->email }}</td>
                    <td>{{ $developer->secret_key }}</td>
                    <td>
                        @if (ACTIVE == $developer->status)
                            Active
                        @else
                            Inactive
                        @endif
                    </td>
					<td>
                        @if (STS_NG == $developer->status)
                            <a href = '{{ URL::To("admin/change_dev_sts/1/".$developer->client_id) }}'>
                                Set to Active
                            </a>
                        @else
                            <a href = '{{ URL::To("admin/change_dev_sts/0/".$developer->client_id) }}'>
                                Set to Inactive
                            </a>
                        @endif

                    </td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>


@stop
