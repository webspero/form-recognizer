@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<p class="text-right">
		<a href="{{URL::to('/')}}/admin/analyst_account/add" class="btn btn-primary">Add Account</a>
	</p>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">No.</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Full Name</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
			@foreach($users as $key => $info)
				<tr>
					<td>{{$key +1}}</td>
					<td>{{$info->name}}</td>
					<td>{{$info->email}}</td>
					<td><a href="{{ URL::to('/') }}/admin/analyst/delete/{{ $info->id }}">Delete</a></td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
@stop
