@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<p class="text-right">
		<a href="{{URL::to('/')}}/admin/add_discount" class="btn btn-primary">Add Discount Code</a>
	</p>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Discount Code</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Associated Business/Website</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Discount</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Validity</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($discounts as $info)
				<tr>
					<td>{{ $info->discount_code }}</td>
					<td>{{ $info->email }}</td>
					<td>{{ $info->business }}</td>
					<td>
						@if($info->type == 1)
							{{ $info->amount }} %
						@elseif ($info->type == 0)
							Php {{ $info->amount }}
						@else
							INVALID
						@endif
					</td>
					<td>{{ $info->valid_from }} to {{ $info->valid_to }}</td>
					<td>
						@if ($info->status == 0)
							<span style="color: RED">OFF</span>
						@elseif ($info->status == 1)
							<span style="color: GREEN">ON</span>
						@else
							INVALID STATUS
						@endif
					</td>
                    <td>
						@if ($info->status == 0)
							<a href="{{URL::to('/')}}/admin/discount_switch/on/{{$info->id}}">Turn On</a>
						@elseif ($info->status == 1)
							<a href="{{URL::to('/')}}/admin/discount_switch/off/{{$info->id}}">Turn Off</a>
						@endif
						|
						<a href="{{URL::to('/')}}/admin/edit_discount/{{$info->id}}">Edit</a>
						|
						<a href="{{URL::to('/')}}/admin/delete_discount/{{$info->id}}">Delete</a>
                    </td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
@stop
