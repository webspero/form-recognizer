@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<p class="text-right">
		<button class="btn btn-primary btn_refresh">Refresh List</button>
		<a href="{{URL::to('/')}}/admin/leads_export" class="btn btn-primary">Export as CSV</a>
		<input type="text" id="email" placeholder="Email Address" class="form-control" style="width: 200px; display: inline-block;" />
		<button class="btn btn-primary btn_send">Send List to Email</button>
		<input type="text" id="cron_email" placeholder="New Leads Email Address" value="{{$cron_email}}" class="form-control" style="width: 200px; display: inline-block;" />
		<button class="btn btn-primary btn_set">Set as New Leads Recepient</button>
	</p>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">First Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Last Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Activity</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Contact Number</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Job Title</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Submitted Date</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($leads as $info)
				<tr>
					<td>{{ $info->id }}</td>
					<td>{{ $info->email }}</td>
					<td>{{ $info->first_name }}</td>
					<td>{{ $info->last_name }}</td>
					<td>{{ $info->company_name }}</td>
                    @if (2 == $info->activity_id)
					<td>Request for Sample</td>
                    @else
                    <td>Feedback</td>
                    @endif
					<td>{{ $info->contact_number }}</td>
					<td>{{ $info->job_title }}</td>
					<td>{{ $info->submitted_date }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
	$('.btn_refresh').click(function(e){
		e.preventDefault();
		$.ajax({
			url: '{{URL::to("/cron/get_leads")}}',
			type: 'get',
			beforeSend: function(){
				$('.btn_refresh').text('Refreshing List...').prop('disabled', true);
			},
			success: function(sReturn){
				alert(sReturn);
				window.location.reload(true);
			}
		});
	});

	$('.btn_send').click(function(e){
		e.preventDefault();
		$.ajax({
			url: '{{URL::to("/cron/send_new_leads")}}',
			type: 'get',
			data: {
				email: $('#email').val()
			},
			beforeSend: function(){
				$('.btn_send').text('Sending List...').prop('disabled', true);
			},
			success: function(sReturn){
				alert(sReturn);
				$('.btn_send').text('Send List to Email').removeAttr('disabled');
			}
		});
	});

	$('.btn_set').click(function(e){
		e.preventDefault();
		$.ajax({
			url: '{{URL::to("/admin/set_new_leads_email")}}',
			type: 'post',
			data: {
				email: $('#cron_email').val()
			},
			beforeSend: function(){
				$('.btn_set').text('Configuring...').prop('disabled', true);
			},
			success: function(sReturn){
				alert(sReturn);
				$('.btn_set').text('Set as New Leads Recepient').removeAttr('disabled');
			}
		});
	});
});
</script>
@stop
