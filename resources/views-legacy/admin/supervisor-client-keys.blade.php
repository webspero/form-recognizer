@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<style>
    .td_size {    
            width:100; 
            height:50px;
            max-width:100;
            min-width:200; 
            max-height:50px; 
            min-height:50px;
            overflow:hidden; /*(Optional)This might be useful for some overflow contents*/   
        },
</style>
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
		<div class="col-sm-6"></div>
		<div class="col-sm-2 text-right">Registered Keys: {{ $overall_used }}</div>
		<div class="col-sm-2 text-right">Unregistered Keys: {{ $overall_unused }}</div>
		<div class="col-sm-2 text-right">Total Keys: {{ $overall_keys }}</div>

	</div>
	<div class="clearfix"></div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" >
			<thead>
				<tr>					
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Organization</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Supervisor Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Registered Keys</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Unregistered Keys</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Total Keys</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($organization as $org)
				<tr>
                    <td class="td_size">{{$org->bank_name}}</td>
                    <td class="td_size" style="width:100px">
                        @foreach($org->supervisor as $key => $emails)
                            <p>
                            <small>{{$emails->email}}</small>
                            </p>
                        @endforeach
                    </td>
                    <td class="td_size">{{$org->used}}</td>
                    <td class="td_size">{{$org->unused}}</td>
                    <td class="td_size">{{$org->total_use}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
@stop
