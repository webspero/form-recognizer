@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
    <div class="row">
		<div class="col-md-8">
            {{Form::open(array('route'=>'getInnodataRecipients', 'method'=>'get', 'class'=>'form-inline'))}}
                <div class="form-group">
                    {{Form::label('search', 'Search')}}
                    {{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'placeholder'=>'search...'))}}
                </div>
                <div class="form-group">
                    {{Form::select('type', array('email'=>'Email', 'name'=>'Name'), Input::has('type') ? Input::get('type') : 'email', array('class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    {{Form::submit('Go', array('class'=>'btn btn-primary'))}}
                </div>
            {{Form::close()}}
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ route('addInnodataRecipient') }}" class="btn btn-primary">Add New Recipient</a>
        </div>
	</div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            <h3 class="text-center">Innodata Recipients</h3>
            <div class="table-responsive">
                <table id="innodata" class="table table-striped table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">No.</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Full Name</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($innodata as $key => $recipient)
                            <tr>
                                <td>{{ ($innodata->currentPage()-1) * $innodata->perPage() + $key + 1 }}</td>
                                <td>{{ $recipient->name }}</td>
                                <td>{{ $recipient->email }}</td>
                                <td><a href="{{ route('deleteInnodataRecipient', $recipient->id) }}">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $innodata->appends(array('search'=>Input::get('search'),'type'=>Input::get('type'), 'innodata_page'=>Input::get('innodata_page'), 'creditbpo_page'=>Input::get('creditbpo_page')))->links() }}
        </div>
        <div class="col-md-6">
            <h3 class="text-center">CreditBPO Recipients</h3>
            <div class="table-responsive">
                <table id="creditbpo" class="table table-striped table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">No.</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Full Name</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($creditbpo as $key => $credit_recipient)
                            <tr>
                                <td>{{ ($creditbpo->currentPage()-1) * $creditbpo->perPage() + $key + 1 }}</td>
                                <td>{{ $credit_recipient->name }}</td>
                                <td>{{ $credit_recipient->email }}</td>
                                <td><a href="{{ route('deleteInnodataRecipient', $credit_recipient->id) }}">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $creditbpo->appends(array('search'=>Input::get('search'),'type'=>Input::get('type'), 'innodata_page'=>Input::get('innodata_page'),'creditbpo_page'=>Input::get('creditbpo_page')))->links() }}
        </div>
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
@stop