@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                            <h2>OCR Options</h2>
                            <hr/>
                            <p>Here you will find all available OCR options!</p>


                            {{ Form::open( array('route' => array('postOCROptions'), 'role'=>'form', 'class'=>'ocr-options-form', 'id' => 'ocr-options-form', 'method'=>'post', 'files'=>true)) }}
                            <div class = 'col-sm-12'>
                                <label>
                                @if ($enableOCRProcessing == 1)
                                    {{Form::checkbox('enable_ocr_processing','1',true)}}
                                @else
                                    {{Form::checkbox('enable_ocr_processing','1')}}
                                @endif
                                Enable OCR Processing
                                </label>
                             </div>
                            
                            <div class = 'col-sm-12' style="margin:20px 0px;">
                                <label>
                                Test Tesseract-OCR output with JPG/PNG/GIF (Optional)
                                <input type="file" name="imagefile" />
                                </label>
                            </div>
                            <div class = 'col-sm-12'>
                                <button type = 'submit' class="btn btn-primary btn_filter_trials">Save</button>
                            </div>
                            {{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop