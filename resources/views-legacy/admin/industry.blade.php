@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div>
		{{Form::open(array('route'=>'postResetIndustry', 'method'=>'post'))}}
		<div class="form-group form-inline">
			{{Form::label('bank_id', 'Bank Selection')}}
			{{Form::select('bank_id', $banks, $bank_id, array('class'=>'form-control'))}}
			{{Form::submit('Restore Industry Weights to Standard', array('class'=>'btn btn-primary'))}}
			<button type = 'button' class = 'btn btn-primary' data-toggle="modal" data-target = '#upload-outlook-modal' id = 'pop-upload-bsp-btn'> Upload BSP Business Expectation </button>
		</div>
		{{Form::close()}}
	</div>
	<div class="clearfix"></div>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					@if($is_main)
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry ID</th>
					@endif
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Title</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Business Considerations</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Management Quality</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Financial Analysis</th>
					@if($is_main)
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Business Outlook</th>
					@endif
				</tr>
			</thead>
			<tbody>
			@foreach ($industry as $info)
				<tr>
					@if($is_main)
					<td>{{ $info->industry_main_id }}
					@endif
					<td>
						@if($is_main)
						<a href="{{ URL::to('/') }}/admin/industry/edit/{{ $info->industry_main_id }}">
						{{ $info->main_title }}
						</a>
						@else
						{{ $info->main_title}}
						@endif
					</td>
					<td>{{ $info->business_group }}</td>
					<td>{{ $info->management_group }}</td>
					<td>{{ $info->financial_group }}</td>
					@if($is_main)
					<td>{{ $info->industry_trend }}</td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>

<div id = 'bsp-htm-upload' style = 'display: none;'> </div>

@include('admin.upload-bsp-expectation')
@include('admin.detect-bsp-expectation')

@stop
@section('javascript')
<script src="{{ URL::asset('js/biz-outlook-calculate.js') }}"></script>
<script src="{{ URL::asset('js/biz-outlook-detect.js') }}"></script>
@stop
