@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('style')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop

@section('style', '<style>
    .alert-danger{
        color: #414141;
        background-color: #ffdada;
        font-size: 16px;
    }
    .alert-warning{
        color: #414141;
        background-color: #ffe1b2;
        font-size: 16px;
        text-align: center;
    }
    </style>'
)

@section('content')
	<div class="container">
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
		<h1>Update Organization Details</h1>
		{{ Form::open(array('route' => array('putOrganizations', $organization->id), 'method' => 'put', 'files' => true)) }}
		<div class="spacewrapper">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
            </div>
            @endif
			<div class="read-only">
				<h4>Update Registration Details</h4>
				<div class="read-only-text">
                    @if($isUsed)
                        <div class="alert alert-warning">
                         If you continue, supervisors that uses this organization will be affected.
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('organization_name', 'Organization Name')}}*
                                {{Form::text('organization_name', $organization->bank_name, array('class' => 'form-control', 'readonly' => 'true')) }}
                                {{ Form::hidden('recaptcha','', array("id"=>"recaptcha"))}}
                            </div>
                            <div class="form-group">
                                {{Form::label('organization_type', 'Organization type')}}*
                                {{Form::select('organization_type', array(
                                    ''=>'--Choose one--',
                                    'Bank'=>'Bank',
                                    'Non-Bank Financing'=>'Non-Bank Financing',
                                    'Corporate'=>'Corporate',
                                    'Government'=>'Government',
                                    'Contractor'=>'Contractor'
                                ), $organization->bank_type, array('class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('deliverables_name', 'Select deliverables you want to display. (At least one)')}}
                                <div class="checkboxes" style="margin-left:50px;">
                                    <input type="checkbox" id="financial_rating" name="financial_rating"> Financial Rating <br>
                                    <input type="checkbox" id="growth_forecast" name="growth_forecast"> Growth Forecast <br>
                                    <input type="checkbox" id="industry_forecasting" name="industry_forecasting"> Industry Comparison
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        {{Form::label('questionnaire_type', 'Questionnaire type')}}*
                        {{Form::select('questionnaire_type', array(
                            ''=>'--Choose one--',
                            '0'=>'Bank',
                            '1'=>'Corporate',
                            '2'=>'Government'
                        ), $organization->questionnaire_type, array('class' => 'form-control'))}}
                    </div> -->

                    <!-- <div class="form-group">
                        <input type = 'checkbox' class='custom_price_switch' name = 'custom_price_switch' value = '1' {{ ($organization->is_custom == 1) ? 'checked' : '' }} />
                        Turn On Custom Report Price<br/>
                    </div> -->

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <strong>REPORT PRICING OPTION</strong>
                                <input class="btn btn-large btn-warning" value="Restore Default Prices" id="restore_default_report_price">
                            </div>

                            <!-- SIMPLIFIED REPORT PRICE -->
                            <div class="col-md-8">
                                Simplified Report Price
                            </div>
                            <div class="col-md-4">
                                {{Form::number('simplified_price', $organization->simplified_price, array('class' => 'form-control', 'required', 'step'=>'any'))}}
                            </div>

                            <!-- STANDALONE REPORT PRICE -->
                            <div class="col-md-8">
                                Standalone Report Price
                            </div>
                            <div class="col-md-4">
                                {{Form::number('standalone_price', $organization->standalone_price, array('class' => 'form-control', 'required', 'step'=>'any'))}}
                            </div>


                            <!-- PREMIUM ZONE 1 REPORT PRICE - Metro Manila -->
                            <div class="col-md-8">
                                <p title="Metro Manila (Manila, Quezon City, Caloocan, Las Piñas, Makati, Malabon, Mandaluyong, Marikina, Muntinlupa, Navotas, Parañaque, Pasay, Pasig, San Juan, Taguig, Valenzuela, and Pateros)">
                                    Premium Zone 1 Report Price
                                </p>
                            </div>
                            <div class="col-md-4">
                                {{Form::number('premium_zone_one_price', $organization->premium_zone1_price, array('class' => 'form-control', 'required', 'step'=>'any'))}}
                            </div>

                            <!-- PREMIUM ZONE 2 REPORT PRICE - Cavite, Laguna, Bulacan and Rizal -->
                            <div class="col-md-8">
                                <p title="Cavite, Bulacan, Laguna, and Rizal">
                                    Premium Zone 2 Report Price
                                </p>
                            </div>
                            <div class="col-md-4">
                                {{Form::number('premium_zone_two_price', $organization->premium_zone2_price, array('class' => 'form-control', 'required', 'step'=>'any'))}}
                            </div>

                            <!-- PREMIUM ZONE 3 REPORT PRICE - Rest of the Philippines -->
                            <div class="col-md-8">
                                <p title="Other regions">
                                    Premium Zone 3 Report Price
                                </p>
                            </div>
                            <div class="col-md-4">
                                {{Form::number('premium_zone_three_price', $organization->premium_zone3_price, array('class' => 'form-control', 'required', 'step'=>'any'))}}
                            </div>
                            <div class="col-md-12" style="color:red;"><strong><small><i>Note: Prices are in Philippine Pesos</i></small></strong></div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('deliverables_name', 'Allowed report types. (Atleast one)')}}
                                <div class="checkboxes" style="margin-left:50px;">
                                    <input type="checkbox" id="simplified_type" name="simplified_type"> Simplified Report <br>
                                    <input type="checkbox" id="standalone_type" name="standalone_type"> Standalone Report<br>
                                    <input type="checkbox" id="premium_type" name="premium_type"> Premium Report
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="col-md-12">
                        <br/>
                        <div class="form-group">
                        <br>
                        <input type="checkbox" name="custom_price_switch" id="custom_price_switch" data-toggle="toggle" data-on="On" data-off="Off" data-onstyle="success" data-offstyle="primary" {{ ($organization->is_custom == 1) ? 'checked' : '' }} />
                        Custom Report Price
                        <div class="custom_price col-sm-12" >
                            <div class="form-group">
                                <p style="color:red"><b>Turning on Custom Report Price will make this Organization's subscription free.</b></p>
                                <div class="col-sm-8">
                                    <div class="col-sm-6">
                                        {{ Form::label('customPrice', "Custom Price") }}
                                    </div>
                                    <div class="col-sm-6">
                                        {{ Form::text('customPrice', $organization->custom_price, array('class'=>'form-control')) }}
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <input type="checkbox" name="white_label_switch" id="white_label_switch" data-toggle="toggle" data-on="On" data-off="Off" data-onstyle="success" data-offstyle="primary" {{ ($organization->isLabel == 1) ? 'checked' : '' }}/>
                        White Label
                        <div class="white_label col-sm-12" >
                            <div class="form-group">
                                <p style="color:red"><b>Turning on White Label will replace all logo image on the PDF</b></p>
                                <div class="col-sm-8">
                                    <div class="col-sm-7">
                                        {{ Form::label('white_label_org_name', "White Label Organization Name") }}
                                    </div>
                                    <div class="col-sm-5">
                                        {{ Form::text('white_label_org_name_field', $organization->white_label_name, array('class'=>'form-control')) }}
                                        <br>
                                    </div> 
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-7">
                                        {{ Form::label('brand_image_label', "White Label Brand Image") }}
                                    </div>
                                    <div class="col-sm-5">
                                        @if($organization->white_label_name && $organization->isLabel == 1)
                                            <a href="{{ URL::to('/') }}/images/brand_images/{{$organization->brand_img}}">View Uploaded Brand Image</a>
                                        @endif
                                        {{Form::file('brand_image', ['style' => 'border: 1px solid #ccc', 'class' => 'form-control']) }}
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>


				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-sm-6">
				<div class="spacewrapper">
					@if($errors->has('captcha'))
						{{ $errors->first('captcha', '<span class="errormessage">:message</span>') }}
					@endif
				</div>

				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" id="submit_btn" class="btn btn-large btn-primary" value="Register">
				</div>
			</div>

		</div>
		{{ Form::close() }}
	</div>
@stop

@section('javascript')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            grecaptcha.ready(function() {
                grecaptcha.execute('6LdX1qUUAAAAAPGSS5YkcXlfxFUCTD-SNfDGL_3M', {action: 'home'}).then(function(token) {
                    if(token) $("#recaptcha").val(token);
                });
            });

            if({{ $organization->is_custom }} != 1){
                $('.custom_price').hide();
            }

            if({{ $organization->isLabel }} == 0){
                $('.white_label').hide();
            }

            // $('input.custom_price_switch').click(function(){
            //     if($(this).is(':checked')){
            //         $('.custom_price').show();
            //     }else{
            //         $('.custom_price').hide();
            //     }
            // });

            $('#custom_price_switch').change(function(){
                if($(this).prop('checked')){
                    $('.custom_price').show();
                }else{
                    $('.custom_price').hide();
                }
            });

            $('#white_label_switch').change(function(){
                if($(this).prop('checked')){
                    $('.white_label').show();
                }else{
                    $('.white_label').hide();
                }
            });

            /**Check empty deliverables */
            if( ({{$deliverables->financial_rating}} == 0) && ({{$deliverables->growth_forecast}} == 0) && ({{$deliverables->industry_forecasting}} == 0)  ){
                $('#financial_rating').prop("checked", true);
                $('#growth_forecast').prop("checked", true);
                $('#industry_forecasting').prop("checked", true);
            }else{
                if( {{$deliverables->financial_rating}} == 1 ){
                    $('#financial_rating').prop("checked", true);
                }
                if( {{$deliverables->growth_forecast}} == 1 ){
                    $('#growth_forecast').prop("checked", true);
                }
                if( {{$deliverables->industry_forecasting}} == 1 ){
                    $('#industry_forecasting').prop("checked", true);
                }
            }

            /**Check empty deliverables */
            if( ({{$deliverables->financial_rating}} == 0) && ({{$deliverables->growth_forecast}} == 0) && ({{$deliverables->industry_forecasting}} == 0)  ){
                $('#financial_rating').prop("checked", true);
                $('#growth_forecast').prop("checked", true);
                $('#industry_forecasting').prop("checked", true);
            }else{
                if( {{$deliverables->financial_rating}} == 1 ){
                    $('#financial_rating').prop("checked", true);
                }
                if( {{$deliverables->growth_forecast}} == 1 ){
                    $('#growth_forecast').prop("checked", true);
                }
                if( {{$deliverables->industry_forecasting}} == 1 ){
                    $('#industry_forecasting').prop("checked", true);
                }
            }

            // Set selected report types
            if( {{$reportType->simplified_type}} == 1 ){
                $('#simplified_type').prop("checked", true);
                console.log("here");
            }
            if( {{$reportType->standalone_type}} == 1 ){
                $('#standalone_type').prop("checked", true);
            }
            if( {{$reportType->premium_type}} == 1 ){
                $('#premium_type').prop("checked", true);
            }

            /** Restore Default Report Price */
            $("#restore_default_report_price").click(function(e){
                $('input[name="simplified_price"]').val(1767.5); // Simplified Report
                $('input[name="standalone_price"]').val(3535); // Standalone Report
                $('input[name="premium_zone_one_price"]').val(4343); // Premium Zone 1 Report
                $('input[name="premium_zone_two_price"]').val(5353); // Premium Zone 2 Report
                $('input[name="premium_zone_three_price"]').val(6363); // Premium Zone 3 Report
            });
        });
    </script>
@stop