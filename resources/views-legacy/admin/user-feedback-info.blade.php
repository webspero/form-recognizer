    <style>
        .feedback-question {
            color: #46b789;
            font-weight: bold;
        }

        .feedback-answer {
            color: #2a99c6;
        }
    </style>
    
    <div class="modal fade" id="feedback-info-modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> User Feedback from {{ $user->firstname }} {{ $user->lastname }} of {{ $user->companyname }}</h4>
                </div>
                
                <div class="modal-body">
                    <div class="container-fluid">
                        @if (0 < $feedback_count)
                            @foreach ($tab_feedbacks as $feedback)
                            <div class = 'row'>
                                <div class = 'col-md-12'> <h4> {{ $feedback['tab_label'] }} </h4> </div>
                                @if (1 == $feedback['ignored'])
                                <div class = 'col-md-6 feedback-answer'> Ignored </div>
                                @else
                                <div class = 'col-md-6 feedback-question'> 1. Which question did you have the most difficulty with? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ trans($feedback['lang_file'].'.'.$feedback['user_difficulty']) }} </div>
                                <div class = 'col-md-6 feedback-question'> 2. What would make it easier for you to fill out these questions? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $feedback['user_recommendation'] }} </div>
                                <div class = 'col-md-6 feedback-question'> 3. Rate this tab overall </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $feedback['user_rating'] }} </div>
                                @endif
                            </div>
                            @endforeach
                            @if (0 < $final_count)
                            <div class = 'row'>
                                <div class = 'col-md-12'> <h4> Final Feedback </h4> </div>
                                <div class = 'col-md-6 feedback-question'> 1. How easy was it to engage our service? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $final_feedback['ease_of_experience'] }}</div>
                                <div class = 'col-md-6 feedback-question'> 2. How happy are you with the report? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $final_feedback['report_satisfaction'] }} </div>
                                <div class = 'col-md-6 feedback-question'> 3. How well did we understand what you want? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $final_feedback['our_understanding'] }} </div>
                                <div class = 'col-md-6 feedback-question'> 4. How likely is it that you would recommend us to a peer or colleague? </div>
                                <div class = 'col-md-6 feedback-answer'> {{ $final_feedback['will_recommend'] }} </div>
                            </div>
                            @else
                            <div class = 'row'>
                                <div class = 'col-md-12 feedback-answer'> Not yet submitted </div>
                            </div>
                            @endif
                        @else
                            <div class = 'row'>
                                <div class = 'col-md-12'> No feedbacks yet </div>
                            </div>
                        @endif
                    </div>
                </div>
                
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>