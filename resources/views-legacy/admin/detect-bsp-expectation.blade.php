    <div class="modal fade" id="bsp-detected-main" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> The System has detected the release of the latest BSP Business Expectation Report !</h4>
                </div>
                
                <div class="modal-body" id = 'bsp-detected-body'>
                    <div class="container-fluid">
						<p class="auto-status"> Searching for latest BSP Outlook data...</p>
                    </div>
                </div>
                
                <div class="modal-footer" id = 'bsp-detected-footer'>
                    
                </div>
                
            </div>
        </div>
    </div>