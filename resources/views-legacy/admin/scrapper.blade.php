@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>BSP Directory Scraper</h2>
				<hr/>
				<p>This process may take a while. Click the button to start the process</p>
				<button id="run_scrapper"class="btn btn-primary">Start</button><span id="scrap_status"></span>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<p id="scrap_result"></p>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/XMLHttpRequest.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#run_scrapper').on('click', function(){
		$('#scrap_status').text('Processing...');
		var xhr = new XMLHttpRequest();
		xhr.open('GET', BaseURL + '/admin/run_scrapper', true);
		xhr.send(null);
		var timer;
		timer = window.setInterval(function() {
			if (xhr.readyState == XMLHttpRequest.DONE) {
				window.clearTimeout(timer);
				$('#scrap_status').text('');
				if(xhr.responseText == 1){
					$('#scrap_result').html("BSP Scraping Done Succesfully! You can download the scraped data on this <a href={{ URL::to('/') }}/admin/download/bsp>link</a>.");
				}
			}
		}, 1000);

	});
});
</script>
@stop
