@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Add: User </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::open(array('method' => 'post', 'route' => 'postAddUser'))}}
					<div class="form-group">
						<div class="row">
						<div class="col-sm-6 {{$errors->has('firstname') ? 'has-error' : ''}}">
							{{Form::label('firstname', 'First Name')}}
							{{Form::text('firstname', $errors->has('firstname') ? Input::old('firstname') : "", array('class' => 'form-control'))}}
						</div>
						<div class="col-sm-6 {{$errors->has('lastname') ? 'has-error' : ''}}">
							{{Form::label('lastname', 'Last Name')}}
							{{Form::text('lastname', $errors->has('lastname') ? Input::old('lastname') : "", array('class' => 'form-control'))}}
						</div>
						</div>
					</div>
					<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
						{{Form::label('email', 'Email Address')}}
						{{Form::text('email', $errors->has('email') ? Input::old('email') : "", array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('job_title') ? 'has-error' : ''}}">
						{{Form::label('job_title', 'Job Title')}}
						{{Form::text('job_title', $errors->has('job_title') ? Input::old('job_title') : "", array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('role') ? 'has-error' : ''}}">
						{{Form::label('role', 'User Role')}}
						{{Form::select('role', array(
							'1'=>'Basic User',
							'4'=>'Supervisor',
						), '1', array('class' => 'form-control'))}}
						<small>Basic User accounts should be created through registration.</small>
					</div>
					<div style="display: none;" class="form-group bank-selection">
						{{Form::label('bank_id', 'Bank')}}
						{{Form::select('bank_id', $banks, '1', array('id' => 'bank-select', 'class' => 'form-control', 'style' => 'width: 100%;'))}}
					</div>
					<div style="display: none;" class="form-group bank-selection">
						<label>
						{{Form::checkbox('can_view_free_sme', '1')}}
						Supervisor can view SMEs without requesting bank?
						</label>
					</div>
					<div style="display: none;" class="form-group other-selection">
						{{Form::label('other', 'Entity Name (eg. "Department of Trade and Industry")')}}
						{{Form::text('other', $errors->has('other') ? Input::old('other') : "", array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						{{Form::label('password', 'Password')}}
						{{Form::password('password', array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						{{Form::label('confirm_password', 'Confirm Password')}}
						{{Form::password('confirm_password', array('class' => 'form-control'))}}
					</div>
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
	$('select#role').change(function(){
		if($(this).val() == "4"){
			$('.bank-selection').show();
			$('.other-selection').hide();
		} else if($(this).val() == "5" || $(this).val() == "6" || $(this).val() == "7") {
			$('.other-selection').show();
			$('.bank-selection').hide();
		} else {
			$('.bank-selection').hide();
			$('.other-selection').hide();
		}
	});
	$('select#role').change();
    $('#bank-select').select2();
</script>
@stop
