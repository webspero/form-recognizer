@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
@include('modal-trial-login')
<div  id="page-wrapper" style="margin-left:0px;">
	@if (isset(Auth::user()->loginid))
    <div class="row">
		<div class="col-sm-6">
            <a href="{{URL::to('/admin/create_trial_account')}}" class="btn btn-primary">New Trial Account</a>

            <input type="text" id="set_trial_receiver" placeholder="Emails separated by comma" value="{{ $receiver }}" class="form-control" style="width: 300px; display: inline-block;" />
            <button class="btn btn-primary btn_set_receiver">Set as New Recipient</button>
        </div>

	</div>

	<div class="clearfix"></div>
	<hr />
    @endif

    <div class="row">
		<div class="col-sm-12">
            <span>Filters:
                <small> <a class = 'toggle-filter' href = '#' data-status = 'filter-hidden'> Show Filter </a></small>
            </span>
        </div>

        <div id = 'trial-filter-cntr' style = 'display:none;'>
            {{ Form::open( array('route' => array('filterTrialAccounts', 'tbllogin.email', 'asc'), 'role'=>'form', 'class'=>'filter-trial-form', 'id' => 'filter-trial-form')) }}
            <div class = 'col-sm-12'>
                <input type="text" id="filter-bank-name" placeholder="Institute" name = 'filter-bank-name' value="" class="form-control trial-account-filter" />
                <input type="text" id="filter-email" placeholder="Email" name = 'filter-email' value="" class="form-control trial-account-filter" />
            </div>
            <div class = 'col-sm-12'>
                <input type="text" id="filter-company-name" placeholder="Company Name" name = 'filter-company-name' value="" class="form-control trial-account-filter" />
                <input type="text" id="filter-registration-date" placeholder="Registration Date" name = 'filter-registration-date' value="" class="form-control trial-account-filter calendar-active" />
            </div>

            <div class = 'col-sm-12'>
                <select id = 'filter-industry' name = 'filter-industry' class = 'form-control trial-account-filter' style = 'width: 600px;'>
                    <option value = ''> Choose Industry </option>
                    @foreach ($industry_list as $industry)
                    <option value = '{{ $industry->industry_main_id }}'> {{ $industry->main_title }} </option>
                    @endforeach
                </select>
            </div>

            <div class = 'col-sm-12'>
                <div class="clearfix"></div>
            </div>

            <div class = 'col-sm-6'>
                <select id = 'filter-fa-cmp' name = 'filter-fa-cmp' class = 'form-control trial-account-filter'>
                    <option value = '<='> Before </option>
                    <option value = '>='> After </option>
                </select>
                <input type="text" id="filter-first-activity" placeholder="First Activity" name = 'filter-first-activity' value="" class="form-control trial-account-filter calendar-active" />

                <select id = 'filter-la-cmp' name = 'filter-la-cmp' class = 'form-control trial-account-filter'>
                    <option value = '<='> Before </option>
                    <option value = '>='> After </option>
                </select>
                <input type="text" id="filter-last-activity" placeholder="Last Activity" name = 'filter-last-activity' value="" class="form-control trial-account-filter calendar-active" />

                <select id = 'filter-ll-cmp' name = 'filter-ll-cmp' class = 'form-control trial-account-filter'>
                    <option value = '<='> Before </option>
                    <option value = '>='> After </option>
                </select>
                <input type="text" id="filter-last-login" placeholder="Last Login" name = 'filter-last-login' value="" class="form-control trial-account-filter calendar-active" />

                <select id = 'filter-comp-cmp' name = 'filter-comp-cmp' class = 'form-control trial-account-filter'>
                    <option value = '<='> Less than </option>
                    <option value = '>='> Greater than </option>
                    <option value = '='> Equals </option>
                </select>
                <input type="text" id="filter-completion" placeholder="Completion" name = 'filter-completion' value="" class="form-control trial-account-filter" />
            </div>

            <div class = 'col-sm-12'>
                <div class="clearfix"></div>
            </div>

            <div class = 'col-sm-12'>
                <button type = 'submit' class="btn btn-primary btn_filter_trials">Filter</button>
            </div>
            {{ Form::close() }}
        </div>
	</div>

    <div class="clearfix"></div>
	<hr />

	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
                    <th class = 'trial-list-col' data-field = 'tblbank.bank_name' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Institute</th>
					<th class = 'trial-list-col' data-field = 'tbllogin.email' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Email Address</th>
					<th class = 'trial-list-col' data-field = 'tblentity.companyname' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th class = 'trial-list-col' data-field = 'tblindustry_main.main_title' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
					<th class = 'trial-list-col' data-field = 'tbllogin.created_at' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Registration Date</th>
					<th class = 'trial-list-col' data-field = 'activity_logs.first_activity' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">First Activity</th>
					<th class = 'trial-list-col' data-field = 'activity_logs.last_activity' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Last Activity</th>
					<th class = 'trial-list-col' data-field = 'tbllogin.updated_at' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Last Login</th>
					<th class = 'trial-list-col' data-field = 'tblentity.completion' data-order = 'asc' style="cursor: pointer; background-color: #055688;color: #fff; font-weight: bold;">Completion</th>
                    @if (isset(Auth::user()->loginid))
					<th id = 'email-col' data-field = 'tbllogin.email' data-order = 'asc' style="background-color: #055688;color: #fff; font-weight: bold;">Send Login Info</th>
                    @endif
				</tr>
			</thead>
			<tbody id = 'filter-trial-tblist'>
			@foreach ($accounts as $account)
				<tr>
                    <td>
                        @if (!$account->bank_name)
                            Independent Report
                        @else
                            {{ $account->bank_name }}
                        @endif
                    </td>
					<td>{{ $account->email }}</td>
					<td>
                        @if ((STS_OK == $account->is_paid) && (isset(Auth::user()->loginid)))
                            <a href = '{{ URL::To("summary/smesummary/".$account->entityid) }}'> {{ $account->companyname }} </a>
                        @else
                            {{ $account->companyname }}
                        @endif
                    </td>
					<td>{{ $account->main_title }}</td>
					<td>{{ ($account->created_at && $account->created_at != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($account->created_at.' +8 hours')) : '-' }} </td>
					<td>{{ ($account->first_activity && $account->first_activity != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($account->first_activity.' +8 hours')) : '-' }}</td>
					<td>{{ ($account->last_activity && $account->last_activity != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($account->last_activity.' +8 hours')) : '-' }}</td>
					<td>{{ ($account->updated_at && $account->updated_at != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($account->updated_at.' +8 hours')) : '-' }}</td>
					<td>{{ $account->completion }}%</td>
                    @if (isset(Auth::user()->loginid))
					<td>
                        <a class = 'pop-trial-send-form' href = '{{ URL::To("/admin/send_trial_login/".$account->entityid) }}' data-email = '{{ $account->email }}'> Send </a>
                    </td>
                    @endif
				</tr>
			@endforeach
			</tbody>
		</table>
        {{ $accounts->links() }}
	</div>
</div>
@stop

@section('style')
<style>
    .ui-datepicker {
        z-index: 1030 !important;
    }
</style>
@stop

@section('javascript')
<script src="{{ URL::asset('js/send-trial-login.js') }}"></script>
<script>
    $(document).ready(function(){
        var set_btn = $('.btn_set_receiver');
        var set_val = $('#set_trial_receiver');

        set_btn.click(function() {
            set_btn.prop('disabled', true);
            set_btn.val('Configuring...');

            $.post(BaseURL+'/admin/set_trial_receiver', { 'emails': set_val.val() }, function() {

                set_btn.prop('disabled', false);
                set_btn.val('Set as new Recipient');
            });
        });


        $('#filter-trial-form').submit(function() {
            var field   = 'tbllogin.email';
            var order   = 'asc';

            filterTrialList(field, order);

            return false;
        });

        $('.trial-list-col').click(function() {
            var field   = $(this).data('field');
            var order   = $(this).data('order');

            filterTrialList(field, order);

            $('.icon-order-list').remove();

            if ('asc' == $(this).data('order')) {
                $(this).data('order', 'desc');
                $(this).append("<i class = 'icon-order-list glyphicon glyphicon-arrow-up'></i>");
            }
            else {
                $(this).data('order', 'asc');
                $(this).append("<i class = 'icon-order-list glyphicon glyphicon-arrow-down'></i>");
            }

            return false;
        });

        function filterTrialList(field, order) {

            $.post(BaseURL+'/admin/filter_trial_accounts/'+field+'/'+order, $('#filter-trial-form').serialize(), function(accounts) {
                var htm_list    = '';

                for (idx = 0; idx < accounts.length; idx++) {
                    htm_list += '<tr>';
                    htm_list += '<td>'+ accounts[idx].bank_name +'</td>';
                    htm_list += '<td>'+ accounts[idx].email +'</td>';
                    htm_list += '<td>'+ accounts[idx].companyname +'</td>';
                    htm_list += '<td>'+ accounts[idx].main_title +'</td>';
                    htm_list += '<td>'+ accounts[idx].created_at +'</td>';
                    htm_list += '<td>'+ accounts[idx].first_activity +'</td>';
                    htm_list += '<td>'+ accounts[idx].last_activity +'</td>';
                    htm_list += '<td>'+ accounts[idx].updated_at +'</td>';
                    htm_list += '<td>'+ accounts[idx].completion +'%</td>';

                    if ($('#email-col').length) {
                        htm_list += "<td> <a class = 'pop-trial-send-form' href = '"+BaseURL+"admin/send_trial_login/"+accounts[idx].entityid+"' data-email = '"+accounts[idx].email+"'> Send </a> </td>";
                    }

                    htm_list += '</tr>';
                }

                $('#filter-trial-tblist').html(htm_list);
            }, 'json');
        }

        /** Adds the JQuery UI Datepicker to the Input field    */
        $(".calendar-active").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-50:+10',
            onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
        });

        $('.toggle-filter').click(function() {

            if ('filter-hidden' == $(this).data('status')) {
                $('#trial-filter-cntr').show();
                $(this).text('Hide Filter');
                $(this).data('status', 'filter-shown');
            }
            else {
                $('#trial-filter-cntr').hide();
                $(this).text('Show Filter');
                $(this).data('status', 'filter-hidden');
            }

            return false;
        });

    });
</script>
@stop
