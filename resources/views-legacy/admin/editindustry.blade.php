@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit: Industy </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::model($industry, array('method' => 'post', 'route' => array('postEditIndustry', $industry->industry_main_id)))}}
					<div class="form-group">
						{{Form::label('main_title', 'Title')}}
						{{Form::text('main_title', $industry->main_title, array('class' => 'form-control', 'disabled' => 'disabled'))}}
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-4 {{$errors->has('business_group') ? 'has-error' : ''}}">
								{{Form::label('business_group', 'Business Considerations')}}
								{{Form::text('business_group', $errors->has('business_group') ? Input::old('business_group') : $industry->business_group, array('class' => 'form-control'))}}
							</div>
							<div class="col-sm-4 {{$errors->has('management_group') ? 'has-error' : ''}}">
								{{Form::label('management_group', 'Management Quality')}}
								{{Form::text('management_group', $errors->has('management_group') ? Input::old('management_group') : $industry->management_group, array('class' => 'form-control'))}}
							</div>
							<div class="col-sm-4 {{$errors->has('financial_group') ? 'has-error' : ''}}">
								{{Form::label('financial_group', 'Financial Analysis')}}
								{{Form::text('financial_group', $errors->has('financial_group') ? Input::old('financial_group') : $industry->financial_group, array('class' => 'form-control'))}}
							</div>
						</div>
					</div>

					<div class="form-group">
                        <div class = 'row'>
                            <div class = 'col-md-12'> <h4 style = 'display: inline;'> Business Outlook: </h4> <span> {{ $industry->industry_trend }} </span>  </div>
                        </div>
                        <div class = 'row'>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q1', 'Q1 (Latest Semester)') }}
                                {{ Form::text('biz-outlook-q1', $errors->has('biz-outlook-q1') ? Input::old('biz-outlook-q1') : $industry->outlook_idx_q1, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q2', 'Q2') }}
                                {{ Form::text('biz-outlook-q2', $errors->has('biz-outlook-q2') ? Input::old('biz-outlook-q2') : $industry->outlook_idx_q2, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q3', 'Q3') }}
                                {{ Form::text('biz-outlook-q3', $errors->has('biz-outlook-q3') ? Input::old('biz-outlook-q3') : $industry->outlook_idx_q3, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q4', 'Q4') }}
                                {{ Form::text('biz-outlook-q4', $errors->has('biz-outlook-q4') ? Input::old('biz-outlook-q4') : $industry->outlook_idx_q4, array('class' => 'form-control') )}}
                            </div>
                        </div>
                        <div class = 'row'>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q5', 'Q5') }}
                                {{ Form::text('biz-outlook-q5', $errors->has('biz-outlook-q5') ? Input::old('biz-outlook-q5') : $industry->outlook_idx_q5, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q6', 'Q6') }}
                                {{ Form::text('biz-outlook-q6', $errors->has('biz-outlook-q6') ? Input::old('biz-outlook-q6') : $industry->outlook_idx_q6, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q7', 'Q7') }}
                                {{ Form::text('biz-outlook-q7', $errors->has('biz-outlook-q7') ? Input::old('biz-outlook-q7') : $industry->outlook_idx_q7, array('class' => 'form-control') )}}
                            </div>
                            <div class = 'col-md-3'>
                                {{ Form::label('biz-outlook-q8', 'Q8') }}
                                {{ Form::text('biz-outlook-q8', $errors->has('biz-outlook-q8') ? Input::old('biz-outlook-q8') : $industry->outlook_idx_q8, array('class' => 'form-control') )}}
                            </div>
                        </div>
					</div>
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin/industry" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>

@stop
@section('javascript')
@stop
