@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<p class="text-right">
		<a href="{{URL::to('/')}}/admin/add" class="btn btn-primary">Add New User</a>
	</p>
	<div class="row">
		<div class="col-xs-12">
		{{Form::open(array('route'=>'getAdminDashboard', 'method'=>'get', 'class'=>'form-inline'))}}
			<div class="form-group">
				{{Form::label('search', 'Search')}}
				{{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'placeholder'=>'search...'))}}
			</div>
			<div class="form-group">
				{{Form::select('type', array('loginid'=>'ID', 'email'=>'Email', 'name'=>'Name'), Input::has('type') ? Input::get('type') : 'email', array('class'=>'form-control'))}}
			</div>
			<div class="form-group">
				{{Form::submit('Go', array('class'=>'btn btn-primary'))}}
			</div>
		{{Form::close()}}
		</div>
	</div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Full Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Role</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Created Date / Time</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Account Lock</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($users as $info)
				<tr>
					<td>{{ $info->loginid }}</td>
					@if (!empty($info->name))
					<td><a href="{{ URL::to('/') }}/admin/edit/{{ $info->loginid }}" class = 'show-feedback-link' data-user-id = '{{ $info->loginid }}'>{{ $info->name }}</a></td>
					@else
					<td><a href="{{ URL::to('/') }}/admin/edit/{{ $info->loginid }}" class = 'show-feedback-link' data-user-id = '{{ $info->loginid }}'>{{ $info->firstname }} {{ $info->lastname }}</a></td>
					@endif
					<td>{{ $info->email }}</td>
					<td>
						@if($info->role == 1)
							Basic User
						
						@elseif($info->role == 4)
							Supervisor
						@else
							unknown
						@endif
					</td>
					<td>
						@if($info->role == 1)
							@if ($info->status == 0)
								<span style="color: RED">New</span>
							@elseif ($info->status == 1)
								<span style="color: ORANGE">Activated</span>
							@elseif ($info->status == 2)
								<span style="color: GREEN">Paid</span>
							@else
								INVALID STATUS
							@endif
						@endif
					</td>
                    <td>
                        {{ gmdate("F d, Y / H:i", strtotime($info->created_at)) }}
                    </td>
                    <td>
                        @if (1 <= $info->lock)
                            <a href = "{{ URL::To('rest/unlock-account').'/'.$info->loginid }}" class = 'unlock-account'"> Unlock </a>
                        @else
                            No
                        @endif
                    </td>
				</tr>
			@endforeach
			</tbody>
		</table>
		{{$paginator->appends(array('search'=>Input::get('search'),'type'=>Input::get('type'),'role'=>Input::get('role')))->links()}}
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
@stop
