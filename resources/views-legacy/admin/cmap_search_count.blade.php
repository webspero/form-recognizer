@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
		<div class="col-xs-12">
        <h3><b>CMAP Search Count</b></h3>
		{{Form::open(array('route'=>'getCmapSearchCount', 'method'=>'get', 'class'=>'form-inline'))}}
			<div class="form-group">
				{{Form::label('year', 'Year:')}}
				{{Form::select('year', $years, Input::has('year') ? Input::get('year') : 'year', array('class'=>'form-control'))}}
			</div>
			<div class="form-group">
				{{Form::label('month', 'Month:')}}
				{{Form::select('month', $months, $month, array('class'=>'form-control'))}}
			</div>
			<div class="form-group">
				{{Form::submit('Go', array('class'=>'btn btn-primary'))}}
			</div>
		{{Form::close()}}
        </div>
	</div>
	<hr />
	<div class="table-responsive">
        <p><b>Search Count: </b>{{ count($queries) }}</p>
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Query Date and Time</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($queries as $query)
                    <tr>
                        <td>{{ $query->id }}</td>
                        <td>{{ $query->company_name }}</td>
                        <td>{{ $query->created_at->format('h:i:s A - F j, Y') }}</td>
                    </tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
@stop
