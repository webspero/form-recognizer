@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit: User </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::model($user, array('method' => 'post', 'route' => array('postEditUser', $user->loginid)))}}
					<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
						{{Form::label('name', 'Full Name')}}
						{{Form::text('name', $errors->has('name') ? Input::old('name') : $user->name, array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
						{{Form::label('email', 'Email Address')}}
						{{Form::text('email', $errors->has('email') ? Input::old('email') : $user->email, array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('job_title') ? 'has-error' : ''}}">
						{{Form::label('job_title', 'Job Title')}}
						{{Form::text('job_title', $errors->has('job_title') ? Input::old('job_title') : $user->job_title, array('class' => 'form-control'))}}
					</div>
					@if($user->role == 4)
					<div class="form-group">
						{{Form::label('bank_id', 'Entity')}}
						{{Form::select('bank_id', $banks, $user->bank_id, array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						<label>
						{{Form::checkbox('can_view_free_sme', $user->can_view_free_sme)}}
						User can view reports without requesting bank?
						</label>
					</div>
					@endif
					<div class="form-group">
						{{Form::label('password', 'Password')}}
						{{Form::password('password', array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						{{Form::label('confirm_password', 'Confirm Password')}}
						{{Form::password('confirm_password', array('class' => 'form-control'))}}
					</div>
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
