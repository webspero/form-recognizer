@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>Excel to ReadyRatios (vfa) file</h2>
				<hr/>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('url' => 'admin/converter', 'files' => true)) }}
					<div class="form-group">
						{{ Form::label('file', 'Select the Excel template that you want to convert to vfa') }}
						{{ Form::file('file') }}
					</div>
					{{ Form::submit('Convert', array('class'=>'btn btn-primary')) }}
				{{ Form::close() }}
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<hr/>
				<h2>Public Converter Key</h2>
				{{Form::open(array('url' => 'admin/converter/update', 'method'=>'post'))}}
					<div class="form-group">
						<label>Current Key:</label>
						{{$converter_key}}
					</div>
					<div class="form-group form-inline">
						<label>New Key:</label>
						{{Form::text('new_key', '', array('class'=>'form-control'))}}
						{{Form::button('Generate New Key', array('class'=>'generate_btn btn btn-primary'))}}
					</div>
					<div class="form-group">
						{{Form::submit('Update Current Key', array('class'=>'btn btn-primary'))}}
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){

	$('.generate_btn').on('click', function(){
		var text = "";
		var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < 10; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		$('input[name=new_key]').val(text);
	});
});
</script>
@stop
