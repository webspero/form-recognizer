    <div class="modal fade" id="upload-outlook-modal" role="dialog" aria-labelledby="upload-outlook-modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"> Auto-Calculate Business Outlooks </h4>
                </div>
                
                <div class="modal-body" id = 'rating-tab-body'>
                    {{ Form::open(array('id' => 'calc-biz-outlook-form', 'files' => true)) }}
                    <div class="container-fluid">
                        
                        <div class = 'row'>
                            <div class = 'col-md-12'>
                                <div id = 'upload-bsp-msg' class = 'errormessage'> </div>
                            </div>
                        </div>
                        
                        <div class = 'row'>
                            <div class = 'form-group'>
                                <div class = 'col-md-12'> {{ Form::label('bsp-expectation-file', 'Upload the HTML file with Business Outlook Index on Own Operations') }} * </div>
                                <div class = 'col-md-12'> {{ Form::file('bsp-expectation-file', array('id'=>'sec_cert','class'=>'form-control','multiple'=>true)) }} </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="modal-footer" id = 'upload-bsp-footer'>
                    <button id = 'btn-upload-bsp' type="submit" class="btn btn-primary">Calculate Business Outlook</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>