@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<p class="text-right">
		<a href="#" class="btn btn-primary show-feedback-summary">Feedback Summary</a>
	</p>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Full Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Feedbacks</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{ $user->loginid }}</td>
					@if (!empty($user->name))
					<td><a href="#" class = 'show-feedback-link' data-user-id = '{{ $user->loginid }}'>{{ $user->name }}</a></td>
					@else
					<td><a href="#" class = 'show-feedback-link' data-user-id = '{{ $user->loginid }}'>{{ $user->firstname }} {{ $user->lastname }}</a></td>
					@endif
					<td>{{ $user->email }}</td>
					<td>{{ $user->companyname }} </td>
					<td> 1 </td>
				</tr>
			@endforeach
			</tbody>
		</table>
		{{$users->links()}}
	</div>
</div>

<div id = 'feedback-modal-cntr'>

</div>

<div id = 'feedback-summary-cntr'>

</div>

@stop
@section('javascript')
<script>
/** Open the Rating form modal */

$('.show-feedback-link').click(function() {

    var link = $(this);

     $('#feedback-modal-cntr').load(BaseURL+'/admin/user-feedbacks/'+link.attr('data-user-id'), function() {
        $('#feedback-info-modal').modal({
            keyboard: false,
        })
            .on('show.bs.modal', function(evt) {

            })
            .modal('show');
    });


    return false;
});

$('.show-feedback-summary').click(function() {

     $('#feedback-summary-cntr').load(BaseURL+'/admin/feedback-summary', function() {
        $('#feedback-summary-modal').modal({
            keyboard: false,
        })
            .on('show.bs.modal', function(evt) {

            })
            .modal('show');
    });

    return false;
});
</script>
@stop
