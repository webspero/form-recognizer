    <style>
        .feedback-question {
            color: #46b789;
            font-weight: bold;
        }

        .feedback-answer {
            color: #2a99c6;
        }
        
        .feedback-hdr {
            background-color: #055688;color: #fff; font-weight: bold;
            border: 1px solid #FFFFFF;
        }
        
        .feedback-data {
            border: 1px solid #EFEFEF;
        }
    </style>
    
    <div class="modal fade" id="feedback-summary-modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> User Feedback Summary </h4>
                </div>
                
                <div class="modal-body">
                    <div class="container-fluid">
                        @if (0 < $feedback_count['answered'])
                            @for ($idx = 1; 6 >= $idx; $idx++)
                            <div class = 'row'>
                                <div class = 'col-md-12'> <h4> {{ trans($lang_file[$idx].'.'.$tab_labels[$idx]) }} </h4> </div>
                                <div class = 'col-md-6 feedback-hdr'> Questions </div>
                                <div class = 'col-md-6 feedback-hdr'> Votes </div>
                                @foreach ($tab_questions[$idx] as $tab_feedback)
                                <div class = 'col-md-6 feedback-data'> {{ trans($lang_file[$idx].'.'.$tab_feedback) }} </div>
                                @if (isset($difficulty_question[$tab_feedback]))
                                <div class = 'col-md-6 feedback-data'> {{ $difficulty_question[$tab_feedback] }} </div>
                                @else
                                <div class = 'col-md-6 feedback-data'> 0 </div>
                                @endif
                                @endforeach
                                <div class = 'col-md-6 feedback-data'> Average Tab Rating </div>
                                @if (isset($rating_score[$idx]))
                                <div class = 'col-md-6 feedback-data'> {{ $rating_score[$idx] }} </div>
                                @else
                                <div class = 'col-md-6 feedback-data'> No ratings yet </div>
                                @endif
                            </div>
                            @endfor
                        @else
                            <div class = 'row'>
                                <div class = 'col-md-12'> No tab feedbacks yet </div>
                            </div>
                        @endif
                        
                        @if (0 < $feedback_count['final'])
                            <div class = 'row'>
                                <div class = 'col-md-12'> <h4> Final Feedbacks </h4> </div>
                                <div class = 'col-md-6 feedback-hdr'> Questions </div>
                                <div class = 'col-md-6 feedback-hdr'> Average Ratings </div>
                                <div class = 'col-md-6 feedback-data'> How easy was it to engage our service? </div>
                                <div class = 'col-md-6 feedback-data'> {{ $final_feedbacks['ease_of_experience'] }}</div>
                                <div class = 'col-md-6 feedback-data'> How happy are you with the report? </div>
                                <div class = 'col-md-6 feedback-data'> {{ $final_feedbacks['report_satisfaction'] }} </div>
                                <div class = 'col-md-6 feedback-data'> How well did we understand what you want? </div>
                                <div class = 'col-md-6 feedback-data'> {{ $final_feedbacks['our_understanding'] }} </div>
                                <div class = 'col-md-6 feedback-data'> How likely is it that you would recommend us to a peer or colleague? </div>
                                <div class = 'col-md-6 feedback-data'> {{ $final_feedbacks['will_recommend'] }} </div>
                            </div>
                        @else
                            <div class = 'row'>
                                <div class = 'col-md-12'> No final feedbacks yet </div>
                            </div>
                        @endif
                    </div>
                </div>
                
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>