@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Add: Discount </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::open(array('method' => 'post', 'route' => array('postEditDiscount', $discount->id)))}}

					<div class="form-group {{$errors->has('discount_code') ? 'has-error' : ''}}">
						{{Form::label('organization', 'Organization')}}
						<select id="organization" class="form-control" name="organization">
							<option value=""></option>
							@foreach($organizations as $organization)
								@if($organization->id == $discount->bank)
									<option value="{{ $organization->id }}" selected="selected">{{$organization->bank_name}}</option>
								@else
									<option value="{{ $organization->id }}">{{$organization->bank_name}}</option>
								@endif
							@endforeach
						</select>
					</div>

					<div class="form-group {{$errors->has('discount_code') ? 'has-error' : ''}}">
						{{Form::label('discount_code', 'Discount Code')}}
						{{Form::text('discount_code', Input::old('discount_code', $discount->discount_code), array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
						{{Form::label('email', 'Email Address')}}
						{{Form::text('email',   Input::old('email', $discount->email), array('class' => 'form-control'))}}
					</div>
					<div class="form-group {{$errors->has('business') ? 'has-error' : ''}}">
						{{Form::label('business', 'Associated Business/Website')}}
						{{Form::text('business',   Input::old('business', $discount->business), array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								{{Form::label('type', 'Discount Type')}}
								{{Form::select('type', array('0'=>'Fixed', '1'=>'Percentage'),   Input::old('type', $discount->type), array('class' => 'form-control'))}}
							</div>
							<div class="col-sm-6  {{$errors->has('amount') ? 'has-error' : ''}}">
								{{Form::label('amount', 'Amount / Percentage')}}
								{{Form::text('amount',   Input::old('amount', $discount->amount), array('class' => 'form-control'))}}
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6  {{$errors->has('valid_from') ? 'has-error' : ''}}">
								{{Form::label('valid_from', 'Valid From')}}
								{{Form::text('valid_from',   Input::old('valid_from', date('Y-m-d', strtotime($discount->valid_from))), array('class' => 'form-control datepicker'))}}
							</div>
							<div class="col-sm-6  {{$errors->has('valid_to') ? 'has-error' : ''}}">
								{{Form::label('valid_to', 'Valid To')}}
								{{Form::text('valid_to',   Input::old('valid_to', date('Y-m-d', strtotime($discount->valid_to))), array('class' => 'form-control datepicker'))}}
							<div>
						</div>
					</div>
					<div class="clearfix"></div>
					<br/>
					<div class="text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin/discounts" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
	$('.datepicker').datepicker({
		dateFormat: 'yy-mm-dd'
	});

	$("#organization").select2();
</script>
@stop
