@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('style')
 	<style>
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			font-size: 14px !important;
		}
		ul#select2-bank_id-results{
			font-size: 14px !important;
		}
		.centered-modal.in {
			display: flex !important;
		}
		.centered-modal .modal-dialog {
			margin: auto;
		}
    </style>
@stop

@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Create Trial Account</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postCreateTrialAccounts')) }}
					<!-- <div class="form-group">
						{{ Form::label('current_bank', "Organization Name") }}
						{{ Form::text('current_bank', '', array('class'=>'form-control typeahead', 'autocomplete'=>'off', 'required' => 'required')) }}
					</div> -->
					<div class="form-group">
                        {{Form::label('bank_type', 'Organization type')}}
                        {{Form::select('bank_type', array(
                            'Bank'=>'Bank',
                            'Non-Bank Financing'=>'Non-Bank Financing',
                            'Corporate'=>'Corporate',
                            'Government'=>'Government'
                        ), 'Bank', array('class' => 'form-control'))}}
                    </div>
                    <div class="form-group">
                        {{Form::label('bank_id', 'Organization')}}
                        {{Form::select('bank_id', array(
                        ), null , array('class' => 'form-control'))}}
                    </div>
					<div class="form-group">
						{{ Form::label('email', "User Email") }}
						{{ Form::text('email', '', array('class'=>'form-control', 'required' => 'required')) }}
					</div>
                    <div class="form-group">
						{{ Form::label('password', "Password") }}
						{{ Form::password('password', array('class'=>'form-control', 'required' => 'required')) }}
					</div>
                    <div class="form-group">
						<input type = 'checkbox' class='create_supervisor_account' name = 'create_supervisor_account' value = '1' />
						Create a corresponding Supervisor account<br/>
					</div>
					<div class="supervisor-fields" style="display:none">
						<div class="form-group">
							{{ Form::label('supervisorEmail', "Supervisor Email") }}
							{{ Form::text('supervisorEmail', '', array('class'=>'form-control')) }}
							<button type="button" class="btn btn-success" style="font-size:10px" id="generate_email"> Generate Email</button>
						</div>
						<div class="form-group">
							{{ Form::label('supervisorPassword', "Supervisor Password") }}
							{{ Form::password('supervisorPassword', array('class'=>'form-control')) }}
							<button type="button" class="btn btn-success" id="random_password" style="font-size:10px"> Generate Random Password</button>
						</div>

						<small style="font-size: 12px;">
							You can generate Email and Password for Supervisor Trial account. Click 'Generate Email' and 'Generate Random Password'.<br/>
						</small>
						<br>
					</div>

					<div class="form-group">
						{{ Form::submit('Create', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

<!-- modal for generated password -->

<div class="modal centered-modal" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
		<h6 style="color:red; text-align: center;">
			<b><i>This generated password can be viewed once. Please make sure to copy and remember the generated password.</i></b>
		</h6>
      </div>
      <div class="modal-body">
	  	<div class="text-center">
		     <p id="show_password"></p>
            <button type="button" class="btn btn-success" data-dismiss="modal" id="copyButton">Copy Link to Clipboard</button>
        </div>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- alert modal -->
<div class="modal centered-modal" tabindex="-1" role="dialog" id="alertModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  	<div class="text-center">
			<small>You must first fill up the <b>User Email</b> in order to use <b>Generate Email</b> for Supervisor Account</small> <br>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Okay</button>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/bootstrap3-typeahead.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#supervisorEmail').val("");
	$('#email').val("");
	$('select#bank_type').change(function(){
        var type = $(this).val();
        $.ajax({
            method: 'GET',
            url: BaseURL+'/admin/organization-type/'+ type,
            success: function(response){
                var len = response.length;

                $("#bank_id").empty();
                for( var i = 0; i<len; i++){
                    var id = response[i]['id'];
                    var name = response[i]['bank_name'];
                    $("#bank_id").append("<option value='"+id+"'>"+name+"</option>");
                }
                $('#bank_id').change();
            }
        })
    });
    $('select#bank_type').change();
	$('#bank_id').select2({placeholder: "Please select an organization"});

    $('.typeahead').autocomplete({
        source: BaseURL+'/search_companies',
        minLength: 3,
        select: function(evt, ui) {
        }
	});
	
	$('input.create_supervisor_account').click(function(){
		if($(this).is(':checked')){
			$('.supervisor-fields').show();
			document.getElementById("supervisorEmail").required =  true;
			document.getElementById("supervisorPassword").required =  true;
		}else{
			$('.supervisor-fields').hide();
			document.getElementById("supervisorEmail").required =  false;
			document.getElementById("supervisorPassword").required =  false;
		}
	});

	$('#random_password').on("click", function(){
		$('#myModal').modal({
			backdrop: 'static'
		});

		$.ajax({
			url: '/generateRandomPassword',
			type: 'get',
			dataType: 'text',
			success: function(resp){
				$('#show_password').html(resp);
			}
		});
	});

	$('#copyButton').on("click", function(){
		$("#supervisorPassword").val($("#show_password").html());

		var text = $("#show_password").get(0)
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
        
		document.execCommand('copy');		
	});

	$('#generate_email').on("click", function(){
		if($('#email').val() != ""){
			let email = $('#email').val();
			let trim_email = email.split('@')[0];
			let trial = "web+trial_" + trim_email + "@creditbpo.com";
			$('#supervisorEmail').val(trial);
		}else{
			$('#supervisorEmail').val("");
			$('#alertModal').modal({
				backdrop: 'static'
			});
		}
	});
	
});

</script>
@stop
