@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
    <style>
        .table .thead-dark th {
            color: #fff;
            background-color: #343a40;
            border-color: #454d55
        }
        .table .thead-dark tr th {
            color: #fff;
        }
        .table .thead-dark th {
            color: inherit;
            border-color: #dee2e6
        }
        .status {
            width:30px;
            height:30px;
        }

        .status-cursor, .edit {
            cursor: pointer;
        }
        .center {
            text-align: center;
        }
        .float-right {
            float: right;
        }

        .hidden {
            display:none;
        }
        
        .modal-body {
            min-height: 100px;
        }

        .error {
            color: #e81e1e;
            font-size: 14px;
        }
        .alert-danger {
            font-size: 14px;
        }
    </style>

@stop
@section('content')
    <div class='container' id="table_div">
        <div  class="col-lg-8 col-md-8 col-sm-8">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th class="center"> </th>
                        <th> Name </th>
                        <th> Receiver</th>
                        <th> Status </th>
                        <th class="center"> Action</th>
                    </tr>
                <thead>
                <tbod>
                    @foreach($data as $d)
                    <tr>
                        <td class="center"> <input type="checkbox" name="notifications" id="notification_{{ $d->id}}" data-id="{{$d->id}}"></td>
                        <td> {{ $d->name}} </td>
                        <td> {{ $d->receiver}}</td>
                        <td> 
                            @if($d->is_running == 1) 
                                <span> Running </span>
                            @else
                                <span> Stop </span>
                            @endif
                        </td>
                        <td class="center"> 
                            @if($d->is_running == 0) 
                                <img name="img_status" alt = "play" class="status status-cursor" src = '{{ URL::To("images/icons/play_notif.png") }}' data-id="{{$d->id}}" data-value="run" > 
                                <img name="img_status" alt = "stop" class="status" src = '{{ URL::To("images/icons/pause_notif.png") }} ' data-id="{{$d->id}}" data-value="stop"> 
                                <a href="{{ URL::To('edit_custom/'.$d->id) }}" target="_blank"><img name="img_status" alt = "edit" class="status edit" src = '{{ URL::To("images/icons/edit_notif.png") }}' data-id="{{$d->id}}" data-value="edit"></a>
                            @else
                                <img name="img_status" alt = "play" class="status" src = '{{ URL::To("images/icons/play_notif.png") }}' data-id="{{$d->id}}" data-value="run" > 
                                <img name="img_status" alt = "stop" class="status status-cursor" src = '{{ URL::To("images/icons/pause_notif.png") }}' data-id="{{$d->id}}" data-value="stop"> 
                                <a href="{{ URL::To('edit_custom/'.$d->id) }}" target="_blank"><img name="img_status" alt = "edit" class="status edit" src = '{{ URL::To("images/icons/edit_notif.png") }}' data-id="{{$d->id}}" data-value="edit"></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="float-right " >
                <!-- <button class="btn btn-md btn-primary"> <i class="glyphicon glyphicon-plus text-default"> </i> Add Notifications</button>  -->
                <button class="btn btn-md btn-success" id="show_receiver" data-toggle="modal" data-target="#setReceiver"> <i class="glyphicon glyphicon-edit text-default"> </i> Set Receiver</button> 
            </div>
        </div> 
        <div class="col-lg-4 col-md-4 col-sm-4"> 
            @if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
            @endif
            {{ Form::open(array('route'=>'addNotificationEmails', 'method'=>'post', 'id' => "addForm")) }}
                <div class="form-group">
                    {{ Form::label('email', 'Email Address') }}
                    <div class="input-group">
                        {{ Form::email('email', Input::old('email'), array('class'=>'form-control', 'required' => 'required')) }}
                        <span class="input-group-btn">
                            {{ Form::submit('+', array('class'=>'btn btn-success', 'id' => "btnAddEmail")) }}
                        </span>
                    </div>
                    <label for="email" class="error" hidden></label>
                </div>
            {{ Form::close() }}
            @if($emails->count() > 0)
            <table class="table table-bordered">
                <thead>
                    <th> Email </th>
                    <th> Action </th>
                </thead>
                <tbody>
                    @foreach($emails as $email)
                    <tr> 
                        <td> {{$email->email}}</td>
                        <td> <a href="/delete_email_by_id/{{ $email->id }}" data-id="{{$email->id}}" > Delete</a>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
    <div class="modal fade" id="setReceiver" tabindex="-1" role="dialog" aria-labelledby="setReceiverLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-uppercase font-weight-bold" id="setReceiverLabel"><b> Set Receiver</b> </h4>
                </div>
                <div class="modal-body">
                    <div name="update_div"> 
                        {{Form::model('', array('method' => 'post', 'id' => 'form_receiver', 'route' => array('saveCustomNotifications')))}}
                            <div class="form-group">
                                <label for="receiverEmail" > Email Address: </label>
                                <input name= "receiverEmail" id="receiverEmail" type="email" class="form-control" required/> 
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveReceiver" class="btn btn-success">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    
@stop
@section('javascript')
    <script type="text/javascript" src="{{ URL::asset('js/custom_notification.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
@stop
