@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Update Forecasting Data</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				<p>Upload Quarterly Gross Value Added per Industry Group at Current Prices</p>
				<p>Reference: LINK <a href="http://openstat.psa.gov.ph">http://openstat.psa.gov.ph</a></p>
				<p>Template can be downloaded <a href="{{ url('admin/download_pse_forecasting_template') }}">here</a>.</p>
				{{ Form::open(array('route'=>'postPSAForecasting', 'files'=>true)) }}
					<div class="form-group">
						{{ Form::file('template', array('id'=>'template','class'=>'form-control','multiple'=>true, 'required')) }}
					</div>
					<div class="form-group">
						{{ Form::submit('Update', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
		<hr>
		<table class="table table-bordered table-striped">
			<tr>
				<th>Industry</th>
				<th>Q1</th>
				<th>Q2</th>
				<th>Q3</th>
				<th>Q4</th>
				<th>Q5</th>
				<th>Q6</th>
				<th>Q7</th>
				<th>Q8</th>
				<th>Qtr Trend</th>
			</tr>
			@foreach($data as $d)
			<tr>
				<td>{{$d->main_title}}</td>
				<td>{{$d->q1}}</td>
				<td>{{$d->q2}}</td>
				<td>{{$d->q3}}</td>
				<td>{{$d->q4}}</td>
				<td>{{$d->q5}}</td>
				<td>{{$d->q6}}</td>
				<td>{{$d->q7}}</td>
				<td>{{$d->q8}}</td>
				<td>{{$d->slope * 100}}%</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@stop
@section('javascript')
@stop
