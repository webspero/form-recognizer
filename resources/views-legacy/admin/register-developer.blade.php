@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<!-- live2support.com tracking codes starts --><div id="l2s_trk" style="z-index:99;">website live chat</div><script type="text/javascript"> var l2slhight=400; var l2slwdth=350; var l2slay_mnst="#l2snlayer {}";var l2slv=3; var l2slay_hbgc="#0097c2"; var l2slay_bcolor="#0097c2"; var l2sdialogofftxt="Live Chat Offline"; var l2sdialogontxt="Live Chat Online"; var l2sminimize=true;    var l2senblyr=true; var l2slay_pos="R"; var l2s_pht=escape(location.protocol); if(l2s_pht.indexOf("http")==-1) l2s_pht='http:'; (function () { document.getElementById('l2s_trk').style.visibility='hidden';
	var l2scd = document.createElement('script'); l2scd.type = 'text/javascript'; l2scd.async = true; l2scd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's01.live2support.com/js/lsjs1.php?stid=26893 &jqry=Y&l2stxt='; var l2sscr = document.getElementsByTagName('script')[0]; l2sscr.parentNode.insertBefore(l2scd, l2sscr); })(); </script><!-- live2support.com tracking codes closed -->

	<div class="container">
		<h1>API Developer Sign Up Page</h1>
		{{ Form::open( array('route' => 'postDevRegForm', 'role'=>'form')) }}
		<div class="spacewrapper">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
            </div>
            @endif
			<div class="read-only">
				<h4>Registration Details</h4>
				<div class="read-only-text">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 {{$errors->has('fname') ? 'has-error' : ''}}">
                                {{Form::label('fname', 'First Name')}}
                                {{Form::text('fname', $errors->has('fname') ? Input::old('fname') : "", array('class' => 'form-control'))}}
                            </div>
                            <div class="col-sm-6 {{$errors->has('lname') ? 'has-error' : ''}}">
                                {{Form::label('lname', 'Last Name')}}
                                {{Form::text('lname', $errors->has('lname') ? Input::old('lastname') : "", array('class' => 'form-control'))}}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 {{$errors->has('email') ? 'has-error' : ''}}">
                                {{Form::label('email', 'Email Address')}}
                                {{Form::text('email', $errors->has('email') ? Input::old('email') : "", array('class' => 'form-control'))}}
                            </div>

                            <div class="col-sm-6 {{$errors->has('email') ? 'has-error' : ''}}">
                                {{Form::label('pword', 'Password')}}
                                {{Form::password('pword', array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>

                </div>
			</div>
		</div>

        <div class="row">
			<div class="col-sm-6">
				<div class="spacewrapper">
					{{ Form::token() }}
					<input type="submit" id="submit_btn" class="btn btn-large btn-primary" value="Register">
				</div>
			</div>

		</div>
		{{ Form::close() }}
	</div>
@stop
