@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
        <div class="col-md-4">
		    <h3> Organizations </h3>
        </div>
        <div class="col-md-8">
	        <div id="bank-sme-listing-filter">
	            {{Form::open(array('route'=>'getOrganizations', 'method'=>'get', 'class'=>'form-inline text-right'))}}
	                
	                <div class="form-group search selector">
	                    {{Form::label('search', 'Search')}}
	                    {{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'placeholder'=>'search...'))}}
	                </div>
	                <div class="form-group">
	                    {{Form::submit('Go', array('class'=>'btn btn-primary'))}}
	                </div>

	                <a href="{{ URL::To('/admin/create_organization') }}" class="btn btn-primary show-feedback-summary">Register New Organization</a>
	            {{Form::close()}}
	        </div>
    	</div>
	</div>
	<hr/>
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif

	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Name</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Organization Type</th>
					<!-- <th style="background-color: #055688;color: #fff; font-weight: bold;">Questionnaire Type</th> -->
					<!-- <th style="background-color: #055688;color: #fff; font-weight: bold;">Report Type</th> -->
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Supervisor Count</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report Price</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($organizations as $organization)
				<tr>
                    <td>{{ $organization->bank_name }}</td>
                    <td>{{ $organization->bank_type }}</td>
                    <!-- <td>
						@if($organization->questionnaire_type === 0)
							Bank
						@elseif($organization->questionnaire_type === 1)
							Corporate
						@elseif($organization->questionnaire_type === 2)
							Government
						@else
							Not Set
						@endif
					</td> -->
					<!-- <td>
						@if($organization->is_standalone == 1)
							Standalone Report
						@else
							Full Rating Report
						@endif
					</td> -->
					<td>{{ $organization->supervisor_count }}</td>
					<td>
						@if($organization->is_custom == 1)
							{{ $organization->custom_price }}
						@else
							Default
						@endif
					</td>
					<td>
                        <a href = '{{ URL::To("admin/update_organization/".$organization->id) }}'>
                        Edit
                        </a>
                    </td>
				</tr>
			@endforeach
			</tbody>
		</table>

		{{ $organizations->links() }}
	</div>
</div>


@stop
