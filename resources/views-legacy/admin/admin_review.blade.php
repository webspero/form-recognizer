@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">

	<div class="row">
        <div class="col-md-4">
		    <h3> Admin Review </h3>
        </div>
        <div class="col-md-8">
	        <div id="bank-sme-listing-filter">
	            {{Form::open(array('route'=>'getAdminReviews', 'method'=>'get', 'class'=>'form-inline text-right'))}}
	                <div class="form-group">
	                    {{Form::select('type', array(
	                        'email'=>'Email',
	                        'organization'=>'Organization'
	                    ), Input::has('type') ? Input::get('type') : 'email', array('class'=>'form-control', 'id'=>'type_select'))}}
	                </div>
	                <div class="form-group search selector">
	                    {{Form::label('search', 'Search')}}
	                    {{Form::text('search', Input::has('search') ? Input::get('search') : '', array('class'=>'form-control', 'placeholder'=>'search...'))}}
	                </div>
	                <div class="form-group">
	                    {{Form::submit('Go', array('class'=>'btn btn-primary'))}}
	                </div>
	            {{Form::close()}}
	        </div>
    	</div>
	</div>
	<hr/>

		<div class="row">
			<div class="table-responsive">
				<table id="entitydata" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">ID</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Company</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Established</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">City</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Score</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Completed</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Analyst Responsible</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Report Type</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold;">Processing Time</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($entity as $info)
						<tr class="odd" role="row">
							<td>{{ $info->entityid }}</td>
							<td><a href="{{ URL::to('/summary/smesummary/'.$info->entityid) }}">{{ $info->companyname }}</a></td>
							<td>{{ $info->email }}</td>
							<td>{{ $info->date_established }}</td>
							<td>{{ $info->row_title }}</td>
							<td>{{ $info->citymunDesc }}</td>
							<td><span style="color: {{ $info->rating_color }}; font-weight: bold;">{{ $info->score_sf }}</span></td>
							<td>{{ ($info->end_date!=null) ? date('m/d/Y', strtotime($info->end_date)) : ""}}</td>
							<td>{{ $info->firstname }} {{ $info->lastname }}</td>
							<td>
								@switch($info->is_premium)
									@case(0)
										{{ 'Standalone' }}
										@break
									@case(1)
										{{ 'Premium' }}
										@break
									@case(2)
										{{ 'Simplified' }}
										@break
								@endswitch
							</td>
							<td>{{ $info->process_time }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>

				{{ $entity->links() }}
			</div>
		</div>
	</div>

@stop
