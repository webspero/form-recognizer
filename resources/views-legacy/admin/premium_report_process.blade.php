@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<style type="text/css">

    td.specifictd {
        width:140px;  /* adjust to desired wrapping */
        display:table;
        white-space: pre-wrap; /* css-3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        word-wrap: break-word; /* Internet Explorer 5.5+ */
    	word-break: break-all;
    }
    tr.odd {
    	border: 1px solid #dddddd;
    }

	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}

	.table-responsive {
	    max-height:600px;
	}

	.td_size {    
		width:200px; 
		height:50px;
		max-width:200px;
		min-width:200px; 
		max-height:50px; 
		min-height:50px;
		overflow:hidden; /*(Optional)This might be useful for some overflow contents*/   
	}

</style>
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div  class="col-lg-9 col-md-8 col-sm-8">
			<div class="table-responsive">
				<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
					<thead>
						<tr>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">ID</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Company</th>
							<th class="specifictd" style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Agency Contact Email</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">FS Input Template</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Authorization Letter</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Report Type</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Status</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Date Started</th>
							<th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">Date Completed</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($entity as $info)
						<tr class="odd" role="row">
							<td class="">{{ $info->entityid }}</td>
							<td class="td_size"><a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">{{ $info->companyname }}</a></td>
							<td class="td_size">
								@if(!empty($info->premium_report_sent_to))
									@foreach($info->premium_report_sent_to as $mail)
										<p><small>{{$mail}}</small></p>
									@endforeach
								@endif
							</td>
							<td class="td_size">
								@foreach ($info->fs_template as $key => $documents2)
					                  <span>
									  	<p><small>
					                    @if($documents2->document_type == 1)
					                    	{{trans('business_details1.unaudited')}}
					                    @elseif($documents2->document_type == 2)
					                    	{{trans('business_details1.interim')}}
					                    @elseif($documents2->document_type == 4)
					                    	{{trans('business_details1.recasted')}}
					                    @else
					                    	{{trans('business_details1.audited')}}
					                    @endif
					                    &nbsp;-&nbsp;
					                  </span>
					                  <a href="{{ URL::to('/') }}/download/documents/{{ $documents2->document_rename }}" target="_blank">{{ $documents2->document_orig }}</a>
									  </small></p>
					            @endforeach
							</td>
							<td class="td_size">
								@foreach ($info->letter_authorization_template as $authorization_documents)
					                  <a href="{{ URL::to('/') }}/letterofauthorization/{{ $info->entityid}}" title="{{$authorization_documents->document_orig}}" target="_blank">{{ $authorization_documents->document_orig }}</a>
					            @endforeach
							</td>
							<td class="td_size">
								@switch($info->is_premium)
									@case(0)
										{{ 'Standalone' }}
										@break
									@case(1)
										{{ 'Premium' }}
										@break
									@case(2)
										{{ 'Simplified' }}
										@break
								@endswitch
							</td>
							<td class="td_size">{!! $info->status_description !!}</td>
							<td class="td_size">{{date('M d, Y', strtotime($info->created_at)) }}</td>
							<td class="td_size">
								@if($info->status == 7)
									{{date('M d, Y', strtotime($info->submitted_at)) }}
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				{{$entity->links()}}
			</div>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-4"> 
            @if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
            @endif
            <?php
            	$isChecked = '';
            	if(optional($admin_setting)->is_innodata_process_premium_reports && $admin_setting->is_innodata_process_premium_reports === 1)
            	{
            		$isChecked = 'checked';
            	}
            ?>
            	<div class="form-group">
					@if($isChecked)
						<div id="premium_desc"> Disable the Premium Report Process </div>
					@else
						<div id="premium_desc"> Enable the Premium Report Process </div>
					@endif
            		<div class="input-group">
						<label class="switch">
						  <input type="checkbox" name="innodata_switch" id="innodata_switch" {{ $isChecked }}>
						  <span class="slider round"></span>
						</label>
					</div>
				</div>
            {{ Form::open(array('route'=>'addThirdPartyEmails', 'method'=>'post', 'id' => "addForm")) }}
                <div class="form-group">
                    {{ Form::label('email', 'Add Agency Contact Email Address') }}
                    <div class="input-group">
                        {{ Form::email('email', Input::old('email'), array('class'=>'form-control', 'required' => 'required')) }}
                        <span class="input-group-btn">
                            {{ Form::submit('+', array('class'=>'btn btn-success', 'id' => "btnAddEmail")) }}
                        </span>
                    </div>
                    <label for="email" class="error" hidden></label>
                </div>
            {{ Form::close() }}
            @if($third_party_emails)
            <table class="table table-bordered">
                <thead>
                    <th> Email </th>
                    <th> Action </th>
                </thead>
                <tbody>
                    @foreach($third_party_emails as $email)
                    <tr> 
                        <td> {{$email->email}}</td>
                        <td> <a href="{{ URL::to('/delete_third_party_email_by_id/'.$email->id) }}" data-id="{{$email->id}}" > Delete</a>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
	$(document).ready(function(){
		$('#innodata_switch').change(function(e){
			var isChecked = $('#innodata_switch:checkbox:checked').length > 0;
			var isProcessPremiumReport = 2;
			if(isChecked)
			{
				isProcessPremiumReport = 1;
			}
			$.ajax({
				url: "{{ URL::to('/change_innodata_setting/') }}?isProcessPremiumReport="+isProcessPremiumReport,
				type: 'get',
				success: function(sReturn){
					//  1-ON 2-OFF
					if(isProcessPremiumReport == 1){
						$('#premium_desc').html('Disable the Premium Report Process');
					}else{
						$('#premium_desc').html('Enable the Premium Report Process');
					}
				}
			});
		});
	});
</script>
@stop
