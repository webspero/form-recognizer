@extends('layouts.master')

@section('header')
@parent
<title>CreditBPO</title>
@stop
@section('content')
<style type="text/css">
td.specifictd {
    width: 140px;
    /* adjust to desired wrapping */
    display: table;
    white-space: pre-wrap;
    /* css-3 */
    white-space: -moz-pre-wrap;
    /* Mozilla, since 1999 */
    white-space: -pre-wrap;
    /* Opera 4-6 */
    white-space: -o-pre-wrap;
    /* Opera 7 */
    word-wrap: break-word;
    /* Internet Explorer 5.5+ */
    word-break: break-all;
}

tr.odd {
    border: 1px solid #dddddd;
}

.switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
}

.switch input {
    opacity: 0;
    width: 0;
    height: 0;
}

.slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
}

.slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}

input:checked+.slider {
    background-color: #2196F3;
}

input:focus+.slider {
    box-shadow: 0 0 1px #2196F3;
}

input:checked+.slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
    border-radius: 34px;
}

.slider.round:before {
    border-radius: 50%;
}

.table-responsive {
    max-height: 600px;
}

.td_size {
    width: 250px;
    max-width: 250px;
    min-width: 250px;
    overflow: hidden;
    /*(Optional)This might be useful for some overflow contents*/
}

#{
 max-width: 100% !important;
}

#tr_notification {
    max-width: 15% !important;
}

#tr_email {
    max-width: 85% !important;
    ;
}
</style>
<div id="page-wrapper" style="margin-left:0px;">
    <div class="container">

        @if(!Auth::guest())
        @if(Auth::user()->role == 0)
        <p>External Users can access this section by accessinng the public link below. Use Transcribe Key to login on
            the page.</p>
        <small>Public Link: <a
                href="{{route('getExternalCreditBpoFTP')}}">{{route('getExternalCreditBpoFTP')}}</a></small>
        <br /><br />
        <div class="col-sm-6">
            <h2>Public Transcribe Key</h2>
            {{Form::open(array('url' => 'admin/transcribe/update', 'method'=>'post'))}}
            <div class="form-group">
                <label>Current Key:</label>
                {{$transcribe_key}}
            </div>
            <div class="form-group form-inline">
                <label>New Key:</label>
                {{Form::text('new_key', '', array('class'=>'form-control'))}}
                {{Form::button('Generate New Key', array('class'=>'generate_btn btn btn-primary'))}}
            </div>
            <div class="form-group">
                {{Form::submit('Update Current Key', array('class'=>'btn btn-primary'))}}
            </div>
            {{Form::close()}}
        </div>
        <div class="col-sm-6">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                <p>{{$e}}</p>
                @endforeach
            </div>
            @endif
            {{ Form::open(array('route'=>'addExternalTranscriberEmail', 'method'=>'post', 'id' => "addForm")) }}
            <div class="form-group">
                {{ Form::label('email', 'Add External Users') }}
                <div class="input-group">
                    {{ Form::email('email', Input::old('email'), array('class'=>'form-control', 'required' => 'required')) }}
                    <span class="input-group-btn">
                        {{ Form::submit('+', array('class'=>'btn btn-success', 'id' => "btnAddEmail")) }}
                    </span>
                </div>
                <label for="email" class="error" hidden></label>
            </div>
            <div>
                <table class="table table-bordered table-responsive" id="table_transcription"
                    style="display: block; height: 100px; overflow-y: scroll;">
                    <thead>
      
                        <tr class="row">
                            <th class="td_size tr_email col-sm-8" style="min-width: 70% !important"> Email </th>
                            <th class="td_size tr_notification col-sm-2" style="min-width: 15%  !important"> Notification
                            </th>
                            <th class="td_size tr_notification col-sm-2" style="min-width: 15%  !important"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($receiver) > 0)
                            @foreach($receiver as $r)
                            <tr class="row">
                                <td class="td_size tr_email col-sm-8" style="min-width: 70%  !important"> {{$r->email}}</td>
                                <td class="td_size tr_notification col-sm-2" style="min-width: 15% !important"> <input
                                        type="checkbox" value="{{$r->email}}" name="notification" class="notification"
                                        <?php if($r->status == 1){ echo 'checked';} ?>></td>
                                <td class="td_size tr_notification col-sm-2" style="min-width: 15%  !important"> <a
                                        href="{{ URL::to('/delete_transcriber_email/'.$r->id) }}"
                                        data-id="{{$r->id}}">Delete</a> </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan=2 style="text-align:center;"><i>No receiver found</i></td>
                            </tr>
                            @endif
                        </tbody>
                </table>
            </div>
            {{ Form::close() }}
        </div>
        @endif
        @endif
        <br />

        <div class="col-lg-12 col-md-8 col-sm-8">

            @if(($transcribe_key == $userTranscribeKey && Auth::guest()) || !Auth::guest())
            <small><i><span style="color:red">Note:</span> Please follow the required format when uploading transcribe
                    file. File name should be <strong>ReportID - Company Name.xlsx </i></strong>
                <br /><br />

                <table id="entitydata" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">
                                Report ID</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">
                                Company Name</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">
                                Organization</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">
                                Action</th>
                            <th style="background-color: #055688;color: #fff; font-weight: bold; text-align:center;">
                                Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reports as $report)
                        <tr>
                            <td>{{$report->entityid}}</td>
                            <td>{{$report->companyname}}</td>
                            <td>{{$report->bank_name}}</td>
                            <td>
                                <button type="button" class="btn btn-info btn-xs" href="#upload_transcribe_file"
                                    name="upload_transcribe_file" data-toggle="modal" data-id="{{$report->entityid}}"><a
                                        style="color:black;"><i class="glyphicon glyphicon-upload"></i>Upload Transcribe
                                        FS</a></button>
                                @auth
                                <button type="button" class="btn btn-danger btn-xs" href="#view_transcribe_file"
                                    name="upload_transcribe_file" data-toggle="modal" data-id="{{$report->entityid}}"><a
                                        style="color:black;"><i class="glyphicon glyphicon-eye-open"></i>Transcribed
                                        FS</a></button>
                                @endauth
                                <button type="button" class="btn btn-warning btn-xs" href="#financial_raw_file"
                                    name="financial_raw_file" data-toggle="modal" data-id="{{$report->entityid}}"><a
                                        style="color:black;"><i class="glyphicon glyphicon-eye-open"></i>Financial Raw
                                        File</a></button>
                            </td>
                            <td style="text-align:center;">
                                <!-- {{ $report->report_status }} -->
                                @if($report->report_status == "Upload Error")
                                <label><span type="button" style="color:red;" class="glyphicon glyphicon-question-sign"
                                        data-toggle="modal" data-target="#exampleModal"
                                        data-id="{{$report->entityid}}">{{$report->report_status}}</span></label>
                                @elseif($report->report_status == "Transcription Uploaded Successfully")
                                <span style="color:blue;">{{$report->report_status}}</span>
                                @else
                                <span style="color:green;">{{$report->report_status}}</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @else
                <div class="col-sm-12">
                    <div class="center-block transcribe-login">

                        @if(session('error'))
                        <div class="alert alert-danger">Invalid transcribe key</div>
                        @php
                        session(['error' => '']);
                        @endphp
                        @endif
                        {{Form::open(array('url' => 'transcribe/login', 'method'=>'post'))}}

                        <div class="form-group form-inline">
                            <label>Transcribe Key:</label>
                            {{Form::text('transcribe_key', '', array('class'=>'form-control'))}}


                            {{Form::submit('Login', array('class'=>'btn btn-primary'))}}
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
                @endif
        </div>
    </div>
</div>

<!-- ERROR MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Upload Error</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="error_result"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="financial_raw_file" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;">Financial Raw File</h4>
            </div>
            <div class="modal-body">
                <table class='table table-bordered' id='records_table'>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="upload_transcribe_file" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <label>Upload Transcribed Financial Statement Input Template</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="report_id" id="report_id" value="" />
                <p>This template is for Financial Statement uploads to be done below. We do this to better organize
                    information for our users. Please do not choose to "Enable Editing" when opening the file in excel
                    as only certain fields can be edited for ease of use. Additionally, the original Financial Statement
                    is required to substantiate numbers (Excel accepted)</p>
                <!-- <label><small><a  href="{{ URL::to('/') }}/getFsTemplateFile/id">Click here to download Financial Statement Template</a></small></label> -->
                <div class="row">
                    <label class="col-md-3" style="margin-top: 8px;">Currency *</label>
                    <div class="col-md-8">
                        {{ Form::select('currency[]',
                            $currency,
                            'PHP',
                            array('class' => 'form-control', 'id' => 'currency'))
                        }}
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3" style="margin-top: 8px;">Unit *</label>
                    <div class="col-md-8">
                        {{ Form::select('unit[]',
							array(
								1 => 'Default',
								1000 => 'Thousands',
								1000000 => 'Millions',
							),
							Input::old('unit'),
							array('class' => 'form-control', 'id' => 'unit'))
						}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        {{ Form::select('document_type[]',
							array(
							'1' => trans('business_details1.unaudited'),
							'4' => trans('business_details1.recasted'),
							'2' => trans('business_details1.interim'),
							'3' => trans('business_details1.audited')
                        ), Input::old('document_type'), array('class' => 'form-control', 'id' => 'document_type')) }}
                    </div>
                    <div class="col-md-8">
                        <input type="file" class="form-control-file" id="transcribe_file" name="transcribe_file">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_upload" name="btn_upload">Upload</button>
            </div>
        </div>
    </div>
</div>
<div id="view_transcribe_file" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;">Transcribe Files</h4>
            </div>
            <div class="modal-body">
                <table class='table table-bordered' id='transcribe_table'>
                </table>
            </div>
        </div>
    </div>
</div>

@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script type="text/javascript" scr="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
@stop

<script>
$('.notification').on('change', function() {
    var email = this.value;
    if (this.checked) {
        var status = 1;
    } else {
        var status = 0;
    }
    $.ajax({
        url: '/admin/transcription/notification/status',
        type: 'post',
        data: {
            'status': status,
            'email': email,
        },
        success: function(response) {
            // $('#view_transcribe_file').modal('hide');
        }
    });
});
$(function() {
    $('#entitydata').DataTable({
        "order": [
            [0, "desc"]
        ]
    });

    $('.generate_btn').on('click', function() {
        var text = "";
        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        $('input[name=new_key]').val(text);
    });

    $("#financial_raw_file").on("shown.bs.modal", function(e) {
        var link = e.relatedTarget,
            modal = $(this),
            data = $(link).data();
        var file_url = window.location.origin + '/download_fspdfFile/';
        $.ajax({
            url: '/fsRawFile',
            type: 'get',
            dataType: 'json',
            data: {
                id: data.id,
            },
            success: function(response) {
                var trHTML = '';
                var trHTML =
                    "<thead><th scope='col' class='col-md-1 text-center'>#</th><th scope='col' class='col-md-7 text-center'>File Name</th><th scope='col' class='col-md-4 text-center'>Action</th><thead><tbody>";
                $.each(response, function(i, item) {
                    report_url = file_url + item.id;
                    trHTML += '<tr class="text-center"><td>' + (i + 1) +
                        '</td><td class="text-center">' + item.file_name + '</td>' +
                        '<td class="text-center"><a type="button" class="btn btn-success glyphicon glyphicon-download-alt btn-xs" href=' +
                        report_url + '></a></td></tr>';
                });
                $('#records_table').html(trHTML);
            }
        });
    });

    $("#upload_transcribe_file").on("shown.bs.modal", function(e) {
        var link = e.relatedTarget,
            modal = $(this),
            data = $(link).data();
        $("#transcribe_file").val("");
        $("#report_id").val(data.id);
    });

    $("button[name='btn_upload']").on("click", function() {
        var file_data = $('#transcribe_file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('currency', $("#currency").val());
        form_data.append('unit', $("#unit").val());
        form_data.append('document_type', $("#document_type").val());

        url_route = "/upload/creditbpofs/" + $("#report_id").val();

        $.ajax({
            url: url_route,
            type: 'post',
            dataType: 'json',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {
                $("#upload_transcribe_file").modal('hide');
                if (data == 1) {
                    alert("Transcribed FS Template uploaded succesfully.");
                    location.reload();
                } else {
                    alert(
                        "Please upload Financial Statement Input Template in excel format."
                        );
                }
            }
        });
    });

    $("#view_transcribe_file").on("shown.bs.modal", function(e) {
        var link = e.relatedTarget,
            modal = $(this),
            data = $(link).data();
        var file_url = window.location.origin + '/download/transcibe_fs/';

        $.ajax({
            url: '/viewTranscribeFile',
            type: 'get',
            dataType: 'json',
            data: {
                id: data.id,
            },
            success: function(response) {
                var trHTML = '';
                var trHTML =
                    "<thead><th scope='col' class='col-md-1 text-center'>#</th><th scope='col' class='col-md-7 text-center'>File Name</th><th scope='col' class='col-md-4 text-center'>Action</th><thead><tbody>";
                if (response.length == 0) {
                    trHTML +=
                        '<tr><td colspan="3" style="text-align:center; color:gray;"><i>No files uploaded</td></i></tr>';
                } else {
                    $.each(response, function(i, item) {
                        report_url = file_url + item.id;
                        trHTML += '<tr class="text-center"><td>' + (i + 1) +
                            '</td><td class="text-center">' + item.file_name +
                            '</td>' +
                            '<td class="text-center"><a type="button" class="btn btn-success glyphicon glyphicon-download-alt btn-xs" href=' +
                            report_url + '></a>';
                        if (item.error == 1) { // there's an error on uploaded files
                            delete_url = window.location.origin +
                                '/delete/transcribe_fs/' + item.id;
                            trHTML +=
                                ' <button class="btn btn-danger glyphicon glyphicon-trash btn-xs" id="delete_transcribe_file" value=' +
                                item.id + '></button></td>';
                        } else { // no error found on uploaded file
                            trHTML += '</td>';
                        }
                        trHTML += '</tr>';
                    });
                }
                $('#transcribe_table').html(trHTML);
            }
        });
    });

    $(document).on('click', '#delete_transcribe_file', function() {
        button_id = $(this).attr("value");
        if (confirm("Are you sure you want to delete this?")) {
            $("#delete-button").attr("href", "query.php?ACTION=delete&ID='1'");
            $.ajax({
                url: '/delete/transcribe_fs/' + button_id,
                type: 'get',
                dataType: 'json',
                data: {
                    id: button_id,
                },
                success: function(response) {
                    $('#view_transcribe_file').modal('hide');
                }
            });
        } else {
            $('#view_transcribe_file').modal('hide');
        }
    });

    $("#exampleModal").on("shown.bs.modal", function(e) {
        var link = e.relatedTarget,
            modal = $(this),
            data = $(link).data();
        $.ajax({
            url: '/getErrorFile/' + data.id,
            type: 'get',
            dataType: 'text',
            data: {
                id: data.id,
            },
            success: function(response) {
                $('.error_result').html(response);
            }
        });
    });

});
</script>

@stop