@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit: Custom Notification </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::model($notifications, array('method' => 'post', 'route' => array('saveCustomNotifications')))}}
					<div class="form-group">
						<div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 {{$errors->has('firstname') ? 'has-error' : ''}}">
                                {{Form::label('name', 'Notification')}}
                                {{Form::text('name', $errors->has('name') ? Input::old('name') : $notifications->name, array('class' => 'form-control', 'readonly'))}}
                            </div>
                            <input type="hidden" name="id" value="{{$notifications->id}}" />
						</div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 {{$errors->has('lastname') ? 'has-error' : ''}}">
                                {{Form::label('receiver', 'Email Address')}}
                                {{Form::text('receiver', $errors->has('receiver') ? Input::old('receiver') : $notifications->receiver, array('class' => 'form-control'))}}
                            </div>
                        </div>
					</div>
					
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/admin/notifications" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
