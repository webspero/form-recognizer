@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Create Client Keys</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postCreateClientKeys')) }}
					<div class="form-group">
						{{ Form::label('email', "Buyer's Email Address") }}
						{{ Form::text('email', '', array('class'=>'form-control')) }}
					</div>
					<div class="form-group">
						{{ Form::label('quantity', 'Quantity') }}
						{{ Form::text('quantity', '', array('id' => 'quantity', 'class'=>'form-control', 'style'=>'width:100px;')) }}
					</div>
					<!-- <div class="form-group">
						<label>
						{{ Form::checkbox('is_free', 1) }} Check this box to create keys for free
						</label>
					</div> -->
					<div class="form-group">
						{{ Form::submit('Send to buyer', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script>
    $(document).ready(function() {
        $("#quantity").on("keypress",function(evt){
            var keycode = evt.charCode || evt.keyCode;
            if (keycode  == 46) {
                return false;
            }
        });
    });
</script>
@stop
