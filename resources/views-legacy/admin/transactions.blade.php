@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row">
		<div class="col-xs-12">
		{{Form::open(array('route'=>'getAdminTransactions', 'method'=>'get', 'class'=>'form-inline'))}}
			<div class="form-group">
				{{Form::label('vector', 'Vector')}}
				{{Form::select('vector', array(''=>'- all -', 'dragonpay'=>'DragonPay', 'paypal'=>'PayPal'), Input::has('vector') ? Input::get('vector') : '', array('class'=>'form-control'))}}
			</div>
			<div class="form-group">
				{{Form::label('status', 'Status')}}
				{{Form::select('status', array(''=>'- all -', 'Paid'=>'Paid', 'Pending'=>'Pending'), Input::has('status') ? Input::get('status') : '', array('class'=>'form-control'))}}
			</div>
			<div class="form-group">
				{{Form::label('currency', 'Currency')}}
				{{Form::text('currency', Input::has('currency') ? Input::get('currency') : 'PHP', array('class'=>'form-control', 'placeholder'=>'Currency'))}}
			</div>
			<div class="form-group">
				{{Form::label('amount', 'Amount')}}
				{{Form::text('amount', Input::has('amount') ? Input::get('amount') : '', array('class'=>'form-control', 'placeholder'=>'0.00'))}}
			</div>
			<div class="form-group">
				{{Form::submit('Filter', array('class'=>'btn btn-primary'))}}
			</div>
		{{Form::close()}}
		</div>
	</div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Login ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Reference</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Vector</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Description</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Status</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Amount</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Currency</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Created</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($transactions as $t)
				<tr>
					<td>{{$t->loginid}}</td>
					<td>{{$t->reference}}</td>
					<td>{{$t->vector}}</td>
					<td>{{$t->email}}</td>
					<td>{{$t->description}}</td>
					<td>{{$t->status}}</td>
					<td>{{$t->amount}}</td>
					<td>{{$t->currency}}</td>
					<td>{{$t->created_at}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		{{$paginator->appends(array('vector'=>Input::get('vector'),'status'=>Input::get('status'),'currency'=>Input::get('currency'),'amount'=>Input::get('amount')))->links()}}
	</div>
</div>
@stop
@section('javascript')
@stop
