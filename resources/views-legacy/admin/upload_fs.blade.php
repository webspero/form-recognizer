@extends('layouts.master', array('hide' => true))

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">	
		<div class="spacewrapper">
			<h1>Upload Financial Statement Input Template</h1>
			<div class="read-only">
				<div class="read-only-text">
					@if($errors->any())
						<div class="alert alert-danger" role="alert">
							@foreach($errors->all() as $e)
								<p>{{$e}}</p>
							@endforeach
						</div>
					@endif

					@if(session()->has('message'))
						<div class="alert alert-success">
							{{ session()->get('message') }}
						</div>
					@endif

					{{ Form::open( array('route' => array('postCreditBpoFTP'), 'role'=>'form', 'files' => true)) }}
					<div>
					{{ Form::file('fstemplate', ['style' => 'width: 310px; border: 1px solid #ccc', 'class' => 'form-control fs-file-uploader', 'id' => 'fstemplate'], array('multiple'=>true)) }}
						<br>
						<input type="submit" class="btn btn-large btn-primary" value="Upload File">
                        <input hidden value="{{$entity->entityid}}" name="entityid">
					</div>
					{{ Form::close() }}
			</div>
		</div>
	</div>
@stop
@section('style')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>.toggle{ width:100% !important; }</style>
@stop
