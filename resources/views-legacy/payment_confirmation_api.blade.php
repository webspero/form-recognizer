@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
	<div class="container">
		<div class="spacewrapper">
			<div class="read-only">
				<div class="read-only-text text-center">
			        <h3>Your payment transaction is successful. Thank you for purchasing the report!</h3>
				</div>
			</div>
		</div>
	</div>
@stop
