@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit CI User</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::model($entity, ['route' => ['putCIUsers', $entity->loginid], 'method' => 'put']) }}
					<div class="form-group">
						{{ Form::label('email', 'Email Address') }}
						{{ Form::text('email', $entity->email, array('class'=>'form-control', 'disabled')) }}
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('password', 'Password (leave blank for no change)') }}
							{{ Form::password('password', array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('password_confirmation', 'Confirm Password') }}
							{{ Form::password('password_confirmation', array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('firstname', 'First Name') }}
							{{ Form::text('firstname', $entity->firstname, array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('lastname', 'Last Name') }}
							{{ Form::text('lastname', $entity->lastname, array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('job_title', 'Job Title') }}
						{{ Form::text('job_title', $entity->job_title, array('class'=>'form-control')) }}
					</div>
					<div class="form-group">
						{{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
