@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
	<style>
		label.error{
			text-transform: capitalize;
			color: red;
			font-size: 0.8em;
			font-weight: normal;
		}
	</style>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Purchase Client Keys</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postPurchaseBankKeys', 'id'=> 'purchase_form')) }}
					
					<div class="form-group">
						{{ Form::label('keyType', 'Type') }}
						<select name="keyType" id="keyType" class="form-control" style ="width:200px">
							@if($reportType->simplified_type == 1)
								<option value="Simplified">Simplified</option>
							@endif

							@if($reportType->standalone_type == 1)
								<option value="Standalone">Standalone</option>
							@endif

							@if($reportType->premium_type == 1)
								<option value="Premium Zone 1">Premium Zone 1</option>
								<option value="Premium Zone 2">Premium Zone 2</option>
								<option value="Premium Zone 3">Premium Zone 3</option>
							@endif
						</select>
						<!-- <strong><small><label> <span id="location_zone"></span></label></small></strong> -->
					</div>
					<input hidden id="organizationid" value="{{$organization->id}}">

					<div class="form-group">
						<labe>Quantity (PHP <span id="price-span">3,535.00</span> each Key)</label>
						<!-- @if(isset($organization))
							{{ Form::label('quantity', 'Quantity (Php ' . number_format((float)$organization->custom_price * 1.03, 2, '.', '') . ' each Key)') }}
						@else
							{{ Form::label('quantity', 'Quantity (Php 3,500.00 each Key)') }}
						@endif -->
						{{ Form::text('quantity', '', array('id' => 'quantity', 'class'=>'form-control', 'style'=>'width:100px;')) }}
					</div>
                    <div class="form-group">
						{{ Form::label('discount_code', 'Discount Code') }}
						{{ Form::text('discount_code', '', array('class'=>'form-control', 'style'=>'width:100px;')) }}
					</div>
					<div class="form-group">
						{{ Form::label('payment_option', 'Payment Option') }}
					</div>
					<div class="form-group">
						<label>
						{{ Form::radio('payment_option', 'paypal', false, array('id'=>'paypal')) }} via PayPal (for CC and Paypal transactions)
						</label>
					</div>
					<div class="form-group">
						<label>
						{{ Form::radio('payment_option', 'dragonpay', false, array('id'=>'dragonpay')) }} via Dragonpay (for Online Banking and Over-the-Counter transactions)
						</label>
					</div>
					<label id="payment_option-error" class="error" for="payment_option" style="display: none"></label>
					<div class="form-group">
						<label>
							<input id="agree" name="agree" style="display: inline-block;" type="checkbox" value="true" class="checkbox"/>
							I have read and agree to the
							<a href="{{ URL::to('/termsofuse') }}" class="btn btn-primary" target="_blank">Terms of Use</a>
							and
							<a href="{{ URL::to('/privacypolicy') }}" class="btn btn-primary" target="_blank">Privacy Policy</a>
							<img src="{{ URL::asset('images/thumb_12601.png') }}" />
						</label>
					</div>
					<label id="agree-error" class="error" for="agree" style="display: none"></label>
					<div class="form-group">
						{{ Form::submit('Purchase', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
	<script>

		var URL = "{{ url('/') }}";

		getClientKeyPrice();

		$('#keyType').change(function(){
			getClientKeyPrice();

			/** Display location of zones */
			if($("#keyType").val() == "Premium Zone 1"){
				$('#location_zone').text("Metro Manila (Manila, Quezon City, Caloocan, Las Piñas, Makati, Malabon, Mandaluyong, Marikina, Muntinlupa, Navotas, Parañaque, Pasay, Pasig, San Juan, Taguig, Valenzuela, and Pateros)");
				$('#location_zone').show();
			}else if($("#keyType").val() == "Premium Zone 2"){
				$('#location_zone').text("Cavite, Bulacan, Laguna, and Rizal");
				$('#location_zone').show();
			}else if($("#keyType").val() == "Premium Zone 3"){
				$('#location_zone').text("Other Regions");
				$('#location_zone').show();
			}else{
				$('#location_zone').hide();
			}
		});

		function getClientKeyPrice() {
			$.ajax({
				url: URL + '/bank/get_client_key_price',
				dataType: 'text',
				method: 'POST',
				data: {
					keyType: $('#keyType').val(),
					bankid: $('#organizationid').val()
				},
				success: function(response){
					$('#price-span').text(response);
				}
			});
		}

		$("#purchase_form").validate({
			rules: {
				"quantity": {
					required: true,
					number: true,
					min: 1,
				},
				"payment_option": "required",
				"agree": "required",
			},
			messages: {
				"quantity": {
					"required": "Please enter quantity",
					"number": "Please enter valid number",
					"min": "quantity must be at least 1",
				},
				"payment_option": "Please select payment option",
				"agree": "Please read and agree to the Terms of Use and Privacy Policy"

			}
		});
	</script>
<script>
    $(document).ready(function() {
        $("#quantity").on("keypress",function(evt){
            var keycode = evt.charCode || evt.keyCode;
            if (keycode  == 46) {
                return false;
            }
        });
    });
</script>
@stop
