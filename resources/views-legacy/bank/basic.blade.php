@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">

	<div class="text-center" style="padding: 10px; background-color: #ff9c51;">
		<p style="margin: 0;">

            To avail of the full functionality of this site, subscribe to us by clicking <a href="{{ URL::to('/bank/subscribe') }}">here</a>

        </p>
	</div>

	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report Date</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($entity as $info)
				<tr>
					<td><a href="{{ URL::to('/') }}/bank/sme-reports-list/{{ $info->companyname }}">{{ $info->companyname }}</a></td>
					<td>{{ date('F j, Y', strtotime($info->created_at)) }}</td>
					<td>
					@if($info->status == 7 && $info->cbpo_pdf != '')
						<a href="#" class="sendmebtn" data-entity="{{ $info->entityid }}">
							Send Me Report
						</a>
					@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal" id="modal-sendme" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body"><p></p></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
@include('user-2fa-form')

@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.sendmebtn').click(function(e){
		e.preventDefault();
		$.ajax({
			url: BaseURL + '/bank/send-me-report/' + $(this).data('entity'),
			beforeSend: function(){
				$('#modal-sendme .modal-body p').text('Sending Client Report...');
				$('#modal-sendme').modal();
			},
			success: function(sReturn){
				$('#modal-sendme').modal('hide');
				if(sReturn=="success"){
					$('#modal-sendme .modal-body p').text('Report successfully sent.');
				} else {
					$('#modal-sendme .modal-body p').text('An error occurred. Please try again or contact creditbpo.com');
				}
				$('#modal-sendme').modal();
			}
		});
	});
});
</script>
@stop
