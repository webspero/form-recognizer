@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">

	<div class="row filters">
		<div class="col-sm-3" style="border-right: 1px solid #e8e8e8;">
			{{Form::open(array('route'=>'postStatistics', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
			<div class="form-group">
				{{Form::label('sme', 'Company Name', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					<select id="select-sme" class="form-control" name="sme[]" multiple="multiple">
						@foreach($sme_list as $key=>$value)
							@if(in_array($key, $sme_selected))
								<option value="{{$key}}" selected="selected">{{$value}}</option>
							@else
								<option value="{{$key}}">{{$value}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry1', 'Industry Tier 1', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry1', array(''=>'All') + $main_industry_list, $main_industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry2', 'Industry Tier 2', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry2', array(''=>'All') + $sub_industry_list, $sub_industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry3', 'Industry Tier 3', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry3', array(''=>'All') + $industry_list, $industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<!-- <div class="form-group">
				{{Form::label('region', 'Region', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('region', array(''=>'All') + $region_list, '', array('class'=>'form-control'))}}
				</div>
			</div> -->
			<div class="form-group" style="display:none">
				{{Form::label('bcc', 'Business Considerations and Conditions (BCC)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('bcc_from', array(
							'' => ' - ',
							226 => 'AAA',
							211 => 'AA',
							196 => 'A',
							181 => 'BBB',
							166 => 'BB',
							151 => 'B',
							136 => 'CCC',
							121 => 'CC',
							101 => 'C',
							81 => 'D',
							0 => 'E',
						), $bcc_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('bcc_to', array(
							'' => ' - ',
							240 => 'AAA',
							225 => 'AA',
							210 => 'A',
							195 => 'BBB',
							180 => 'BB',
							165 => 'B',
							150 => 'CCC',
							135 => 'CC',
							120 => 'C',
							100 => 'D',
							80 => 'E',
						), $bcc_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('mq', 'Management Quality (MQ)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('mq_from', array(
							'' => ' - ',
							141 => 'AAA',
							131 => 'AA',
							121 => 'A',
							111 => 'BBB',
							101 => 'BB',
							91 => 'B',
							81 => 'CCC',
							71 => 'CC',
							61 => 'C',
							51 => 'D',
							0 => 'E',
						), $mq_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('mq_to', array(
							'' => ' - ',
							150 => 'AAA',
							140 => 'AA',
							130 => 'A',
							120 => 'BBB',
							110 => 'BB',
							100 => 'B',
							90 => 'CCC',
							80 => 'CC',
							70 => 'C',
							60 => 'D',
							50 => 'E',
						), $mq_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('fc', 'Financial Condition (FC)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('fc_from', array(
							'' => ' - ',
							'AAA' => 'AAA',
							'AA' => 'AA',
							'A' => 'A',
							'BBB' => 'BBB',
							'BB' => 'BB',
							'B' => 'B',
							'CCC' => 'CCC',
							'CC' => 'CC',
							'C' => 'C',
							'D' => 'D'
						), $fc_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('fc_to', array(
							'' => ' - ',
							'AAA' => 'AAA',
							'AA' => 'AA',
							'A' => 'A',
							'BBB' => 'BBB',
							'BB' => 'BB',
							'B' => 'B',
							'CCC' => 'CCC',
							'CC' => 'CC',
							'C' => 'C',
							'D' => 'D'
						), $fc_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('rating', 'CreditBPO Rating', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('rating_from', array(
							'' => ' - ',
							177 => 'AA',
							150 => 'A',
							123 => 'BBB',
							96 => 'BB',
							68 => 'B',
							0 => 'CCC',
						), $rating_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('rating_to', array(
							'' => ' - ',
							250 => 'AA',
							176 => 'A',
							149 => 'BBB',
							122 => 'BB',
							95 => 'B',
							67 => 'CCC',
						), $rating_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('brating', 'Supervisor Rating', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('brating_from', array(
							'' => ' - ',
							177 => 'AA',
							150 => 'A',
							123 => 'BBB',
							96 => 'BB',
							68 => 'B',
							0 => 'CCC',
						), $brating_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('brating_to', array(
							'' => ' - ',
							250 => 'AA',
							176 => 'A',
							149 => 'BBB',
							122 => 'BB',
							95 => 'B',
							67 => 'CCC',
						), $brating_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('pdefault', 'Preliminary Probability of Default', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{ Form::text('pdefault_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
						<label>to</label>
						{{ Form::text('pdefault_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
					</div>
				</div>
			</div>

			<div class="form-group filter4">
				{{Form::label('selector', 'Data Shown', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('selector', array(
						'1'=>'Gross Revenue Growth',
						'2'=>'Net Income Growth',
						'3'=>'Gross Profit Margin',
						'4'=>'Current Ratio',
						'5'=>'Debt-to-Equity Ratio',
					), (isset($selector)) ? $selector : 1, array('class'=>'form-control'))}}
				</div>
			</div>

			{{-- major customer info filter --}}
			<div class="form-group">
				<label for="type" class="col-md-8">
					Major Customer Info Filters 
				</label>
				<a href="#" id="majorCustFilter" class="majorCustFilter btn btn-primary col-md-1">+</a>
				<input type="hidden" name="with_majCustFilter" id="with_majCustFilter" value="0">
			</div>

			<div class="col-md-12 majorCustomerFilters hidden">
				{{-- years doing business filter --}}
				<div class="form-group">
						{{Form::label('type', 'Years doing business with', array('class' => 'col-md-6'))}}
						<div class="col-sm-6 form-inline">
							<div class="form-group">
								{{ Form::text('yearsDoingBusiness_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
								<label>to</label>
								{{ Form::text('yearsDoingBusiness_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
							</div>
						</div>
				</div>
				
				{{-- % share of sales to this company vs. total sales filter --}}
				<div class="form-group">
						{{Form::label('type', '% Share Sales To this Company vs. Your Total Sales', array('class' => 'col-md-6'))}}
						<div class="col-sm-6 form-inline">
							<div class="form-group">
								{{ Form::text('percentShareSales_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
								<label>to</label>
								{{ Form::text('percentShareSales_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
							</div>
						</div>
				</div>

				{{-- order frequency filter --}}
				<div class="form-group">
					{{Form::label('type', 'Order Frequency', array('class'=>'col-sm-4'))}}
					<div class="col-sm-8">
								<select id="select-order-frequency" class="form-control" name="orderFrequency[]" multiple="multiple">
											<option value="mcof_regularly">Regularly</option>
											<option value="mcof_often">Often</option>
											<option value="mcof_rarely">Rarely</option>
								</select>
					</div>
				</div>

				{{-- payment behavior filter --}}
				<div class="form-group">
					{{Form::label('type', 'Payment Behavior', array('class'=>'col-sm-4'))}}
					<div class="col-sm-8">
								<select id="select-payment-behavior" class="form-control" name="paymentBehavior[]" multiple="multiple">
											<option value="1">Ahead of Due Date</option>
											<option value="2">On Due Date</option>
											<option value="3">Portion Settled on Due Date</option>
											<option value="4">Settled Past Due Date</option>
											<option value="5">Delinquent</option>
								</select>
					</div>
				</div>

				{{-- relationship satisfaction filter --}}
				<div class="form-group">
						{{Form::label('type', 'Relationship Satisfaction', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
									<select id="select-relationship-satisfaction" class="form-control" name="relationshipSatisfaction[]" multiple="multiple">
												<option value="1">Good</option>
												<option value="2">Satisfactory</option>
												<option value="3">Unsatisfactory</option>
												<option value="4">Bad</option>
									</select>
						</div>
					</div>

			</div>

			{{-- major supplier filters --}}
			<div class="form-group">
				<label for="type" class="col-md-8">
					Major Supplier Info Filters
				</label>
				<a href="#" id="majorSuppFilter" class="majorSuppFilter btn btn-primary col-md-1">+</a>
				<input type="hidden" name="with_majSuppFilter" id="with_majSuppFilter" value="0">
			</div>

			<div class="col-md-12 majorSupplierFilters hidden">
				{{-- supplier years doing business filter --}}
				<div class="form-group">
						{{Form::label('type', 'Years doing business with', array('class' => 'col-md-6'))}}
						<div class="col-sm-6 form-inline">
							<div class="form-group">
								{{ Form::text('supp_yearsDoingBusiness_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
								<label>to</label>
								{{ Form::text('supp_yearsDoingBusiness_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
							</div>
						</div>
				</div>

				{{-- % share of total purchases --}}
				<div class="form-group">
						{{Form::label('type', '% Share of Total Purchases', array('class' => 'col-md-6'))}}
						<div class="col-sm-6 form-inline">
							<div class="form-group">
								{{ Form::text('supp_percentShareSales_from', 0, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
								<label>to</label>
								{{ Form::text('supp_percentShareSales_to', 100, array('class'=>'form-control numeric100', 'style'=>'max-width: 60px; text-align: right;' )) }}
							</div>
						</div>
				</div>

				{{-- your payment to supplier in last 3 payments filter --}}
				<div class="form-group">
					{{Form::label('type', 'Payment to Supplier in Last 3 Payments', array('class'=>'col-sm-4'))}}
					<div class="col-sm-8">
								<select id="select-payment-behavior-supplier" class="form-control" name="paymentBehaviorSupplier[]" multiple="multiple">
											<option value="1">Ahead of Due Date</option>
											<option value="2">On Due Date</option>
											<option value="3">Portion Settled on Due Date</option>
											<option value="4">Settled Past Due Date</option>
											<option value="5">Delinquent</option>
								</select>
					</div>
				</div>

				{{-- Purchase frequency filter --}}
				<div class="form-group">
					{{Form::label('type', 'Purchase Frequency', array('class'=>'col-sm-4'))}}
					<div class="col-sm-8">
								<select id="select-purchase-frequency" class="form-control" name="purchaseFrequency[]" multiple="multiple">
											<option value="ms_cod">COD</option>
											<option value="ms_monthly">Monthly</option>
											<option value="ms_bimonthly">Bi-monthly</option>
											<option value="ms_weekly">Weekly</option>
											<option value="ms_daily">Daily</option>
								</select>
					</div>
				</div>











			</div>

			<div class="form-group">
				{{Form::label('type', 'Graph Type', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					@if ($is_standalone)
					{{Form::select('type', array('1'=>'Industry', 
															'4'=>'Industry Comparison: Report vs Industry',
															'5'=>'Report Overview'), $type, array('class'=>'form-control'))}}
					@else
					{{Form::select('type', array('1'=>'Industry',
															'2'=>'Composite Final Ratings',
															'3'=>'Composite Rating Breakdown',
															'4'=>'Industry Comparison: Report vs Industry',
															'5'=>'Report Overview'), $type, array('class'=>'form-control'))}}
					@endif
				</div>
			</div>

			<div class="form-group">
				<a href="#" class="create_graph btn btn-primary">Create New Graph</a>
				<a href="#" class="update_graph btn btn-primary">Update Current Graph</a>
			</div>
			<div class="form-group">
				<a href="#" class="delete_graph btn btn-primary">Delete Selected Graph</a>
				<a href="#" class="delete_all_graph btn btn-primary">Clear All Graphs</a>
				<a href="#" onclick="window.print(); return false;" class="btn btn-primary">Print</a>
			</div>

			{{ Form::close() }}
		</div>
		
		<div class="col-sm-9">
			<div id="chartcontainer" style="width:100%; min-height: 700px;"><div></div></div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/highcharts-more.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/exporting.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/no-data-to-display.js') }}"></script>
<script src="{{ URL::asset('/Highcharts-4.1.4/js/modules/solid-gauge.src.js') }}"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="{{ URL::to('/')}}/js/jquery.form.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart1.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart2.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart3.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_chart4.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_statistics_reportOverview.js"></script>
@stop
@section('style')
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
<link rel="stylesheet" href="{{ URL::asset('css/statistics-print.css') }}" media="print"></link>
<style>
/*#chartcontainer div.highcharts-container {
    min-height: 500px !important;
}*/
</style>
@stop
