@extends('layouts.master')

@section('header')
    @parent
    <title>CreditBPO</title>
@stop
@section('content')

<div id="page-wrapper" style="margin-left:0px;">
    <div class="row no-print">
        <div class="col-sm-5 col-md-5">
            <p style="margin-top: 5px; margin-bottom:  0px;">
                <span style="font-weight: bold; font-size: 16px;">{{ $bank_name }}</span> - {{ Auth::user()->email }}
            </p>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr class="no-print" />
    <div class="row row-expired">
        <div class="col-md-4 col-sm-4 col-xs-12 col-center">
            <img src="{{ URL::to('/') }}/images/reject.png" alt="reject" width="100px">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <p class="error-text">Your subscription has expired</p>
            <p>You may only access reports that were completed in the last two weeks. <br/>
            Please renew your subscription to access all the features of our platform.</p>
            <div><a href="{{URL::to('/bank/subscribe')}}" class="btn btn-primary">Renew Subscription</a></div>
        </div>
    </div>
    @if (@count($entity) > 0)
        <table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
            <tr>
                <th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
                <th style="background-color: #055688;color: #fff; font-weight: bold;">Date Completed</th>
                <th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
            </tr>
            @foreach ($entity as $info)
                <tr>
                    <td>{{ $info->companyname }}</td>
                    <td>{{ $info->completed_date }}</td>
                    <td style="color: #666;">
                        <!-- Only report for the latest 2 weeks can have email report feature -->
                        @if (Carbon\Carbon::today()->diffInDays(Carbon\Carbon::parse($info->completed_date)) < 14)
                        <span class="email-sender"
                            data-id="{{ $info->entityid }}"
                            style="cursor: pointer; color: #055688">
                            Email Report
                        </span>
                        <span class="email-sender-status" style="display: none;"></span>
                        @else
                        <span style="color: #888">
                            Email Report
                        </span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    @endif
</div>
@include('modal-subscription-expired');
<script type="text/javascript">

	$(function(){
		$('#modal-subscription-expired').modal('show');
	});

    $('.email-sender').on('click', function() {
        var self = $(this);

        $.ajax({
            url: BaseURL + '/bank/send-me-report/' + self.data('id'),
            beforeSend: function() {
                self.hide();
                self.next('.email-sender-status').show();
                self.next('.email-sender-status').html('Sending Email. Please wait.')
            },
            success: function(response) {
                var status = response === 'success' ? 'Email sent' : 'Failed to send email';
                self.next('.email-sender-status').html(status);
            }
        });
    });
</script>
@stop
