@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h1>Notification Settings</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif

				<div class="form-group-row">

					<div class="col-md-6">
						{{ Form::open(array('route'=>'postNewNotificationEmailSub', 'method'=>'post')) }}
							<div class="form-group">
								
									{{ Form::label('email', 'Email Address') }}
									<div class="input-group">
										{{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'required' => 'required')) }}
										<span class="input-group-btn">
											{{ Form::submit('+', array('class'=>'btn btn-success')) }}
										</span>
									</div>
								
							</div>
						{{ Form::close() }}

						{{ Form::open(array('route'=>'postNotificationSettings', 'method'=>'post', 'id'=>'update')) }}
							<div class="form-group">
								<strong>Notifications to be sent: </strong>
							</div>
							<input type="checkbox" name="notif_daily" id="notif_daily" {{ $bankNotifSettings->notif_daily == 1 ? 'checked="checked"' : '' }} />
							<label>Daily notification: &nbsp;</label>
							<div class="form-group notif_daily_options hidden">
								<div class="alert alert-danger col-md-12 hidden" id="error_daily_notif" role="alert">
									<small>Daily notification checked, choose at least one from the options below.</small>
								</div>
								<div class="col-md-12">
									<input type="checkbox" name="notif_reports_started" id="notif_reports_started" class="" {{ $bankNotifSettings->notif_reports_started == 1 ? 'checked="checked"' : '' }} />
									<label>Reports Started Today </label>
								</div>
								<div class="col-md-12">
									<input type="checkbox" name="notif_reports_completed" id="notif_reports_completed" {{ $bankNotifSettings->notif_reports_completed == 1 ? 'checked="checked"' : '' }} />
									<label>Reports Completed Today </label>
								</div>
								<div class="col-md-12">
									<label>Every: </label>
								</div>
								<div class="col-md-5 col-md-offset-1">
									<label>Time:</label>
									<input type="time" name="time_trigger" value="{{ $bankNotifSettings->original_time ? $bankNotifSettings->original_time : "08:00" }}" class="form-control"/>
								</div>
								<div class="col-md-7 col-md-offset-1">
									<label>Timezone:</label>
									{{Form::select('timezone', $timezoneList, $bankNotifSettings->timezone ? $bankNotifSettings->timezone : 'Asia/Manila', array('class'=>'form-control'))}}
								</div>
							</div>
							<div class="form-group">
								<label><input type="checkbox" name="newreport" {{ $bankNotifSettings->notif_new_report == 1 ? 'checked="checked"' : '' }} /> Every new Report Created</label><br/>
								<label><input type="checkbox" name="keyuse" {{ $bankNotifSettings->notif_client_key_use == 1 ? 'checked="checked"' : '' }} /> Everytime Client Key/s is/are used</label><br/>
								<label><input type="checkbox" name="keysavail" {{ $bankNotifSettings->notif_keys_available == 1 ? 'checked="checked"' : '' }} /> Client Keys Available (Every Monday)</label><br/>
								<label><input type="checkbox" name="keysalmostout" {{ $bankNotifSettings->notif_keys_almost_out == 1 ? 'checked="checked"' : '' }} /> If available Client Keys are equal to or below: {{ Form::text('numberkeytrigger', $bankNotifSettings->keys_number_trigger == null ? '5' : $bankNotifSettings->keys_number_trigger, array('width'=>'50px', 'size'=>'2')) }} keys</label><br/>
								<label><input type="checkbox" name="nomorekeys" {{ $bankNotifSettings->notif_no_more_keys == 1 ? 'checked="checked"' : '' }} /> If all Client Keys are used</label><br />
							</div>
							<div class="clearfix"></div>
							<div class="form-group text-center">
								{{ Form::submit('Save', array('class'=>'btn btn-primary submit')) }}
							</div>
						{{ Form::close() }}
					</div>
				
					<div class="col-md-6">
						<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
							<thead>
								<tr>
									<th><label>Email</label></th>
									<th><label>Action</label></th>
								</tr>
							</thead>
							<tbody>
							@foreach ($bankEmailNotifList as $emailList) 
								<tr class="odd" role="row">
									<td>{{ $emailList->email }}</td>
									<td>
										<a href="{{ URL::to('/') }}/bank/notifications/deleteemail/{{ $emailList->email_notif_id }}">
											Delete
										</a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>

				

			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script>
	$(document).ready(function() {
		if ($('#notif_daily').is(':checked')) {
			$('.notif_daily_options').removeClass("hidden");
		}

		$("#notif_daily").change(function() {
			if (this.checked) {
				$('.notif_daily_options').removeClass("hidden");
			} else {
				$('.notif_daily_options').addClass("hidden");
				$("#notif_reports_started").prop("checked", false);
				$("#notif_reports_completed").prop("checked", false);
			}
		});

		$("#update").submit(function() {
			if ($('#notif_daily').is(':checked')) {
				if ($('#notif_reports_started').not(':checked').length &&
				$('#notif_reports_completed').not(':checked').length) {
					$('#error_daily_notif').removeClass('hidden');
					$('#notif_reports_started').addClass('heartBeat');
					$('#notif_reports_completed').addClass('heartBeat');
					setTimeout(function() { $("#error_daily_notif").addClass('hidden'); }, 5000);
					setTimeout(function() { $("#notif_reports_started").removeClass('heartBeat'); }, 5000);
					setTimeout(function() { $("#notif_reports_completed").removeClass('heartBeat'); }, 5000);
					return false;
				}
			}
		})
	})
</script>
@stop
