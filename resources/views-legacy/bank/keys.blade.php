@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
    @if (3 == Auth::user()->product_plan)
    <div class="text-center" style="padding: 10px; background-color: #ff9c51;">
		<p style="margin: 0;">

            To avail of the full functionality of this site, subscribe to us by clicking <a href="{{ URL::to('/bank/subscribe') }}">here</a>

        </p>
	</div>
    @endif
	<div class="row">

		<div class="col-sm-6">

			<a href="{{URL::to('/bank/purchase_keys')}}" class="btn btn-primary">Purchase Additional Keys</a>
			<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_distributed_keys" class="btn btn-primary">Distribute Key</a>

		</div>

		<div class="col-sm-6 text-right">
			Registered Keys: {{ $used }} <br/>
			Unregistered Keys: {{ $unused }} <br/>
			Total Keys: {{@count($entity)}}
		</div>

	</div>
		
	<div class="row">
		<div class="col-sm-12" style="border-right: 1px solid #e8e8e8;">
			{{Form::open(array('route'=>'getBankKeys', 'method'=>'get', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
			<div class="row">
				<div class="col-sm-8">
					
					<div class="form-group form_row_col">
						{{Form::label('filter', 'Filter', array('class'=>'col-sm-2'))}}
						<div class="col-sm-5">
							<select id="filter" class="form-control col-sm-4" name="filter">
								<option value="Registered" @if($filter == 'Registered' || $filter == '') selected="selected" @endif>Registered</option>
								<option value="Unregistered" @if($filter == 'Unregistered') selected="selected" @endif>Unregistered</option>
								<option value="" @if($filter == '') selected="selected" @endif>All</option>
							</select>
						</div>
						<button class="col-sm-2 update_table_list btn btn-primary">Update Table List</button>
					</div>
					
				</div>

			</div>
			{{ Form::close() }}
		</div>
	</div>

	<div class="clearfix"></div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Client Key</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Report Type</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Basic User Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Date Started</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($entity as $info)
				<tr>
					<td>{{ $info->serial_key }}</td>
					<td><a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">{{ $info->entityid }}</a></td>
					<td>{{ $info->type }}</td>
					<td>{{ $info->companyname }}</td>
					
					<td>{{ $info->email }}</td>
					<td>{{ ($info->created_at!=null) ? date('m/d/Y', strtotime($info->created_at)) : ""}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-sm-12"><a href="{{URL::to('/bank/export_keys')}}" class="btn btn-primary">Export Unused Keys</a></div>
	</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_distributed_keys">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Distribute Key</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>				   
				@elseif(!empty($another))
					<div class="alert alert-success">Keys has been sent! Please add another! <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button></div> 
				@endif

				{{ Form::open(array('route'=>'postDistributeKeys', 'method'=>'post','id' => 'key_form')) }}

					<div class="group_fields">
						<div class="field_rows" id="original_field_row">
							<div class="form-group">
								{{ Form::label('email', 'Email Address') }}
								{{ Form::text('email','', ['class'=>'form-control','required']) }}
							</div>

							<div class="form-group">
								{{ Form::label('no_of_keys', 'Number of Keys') }}
								{{ Form::number('no_of_keys','', ['class'=>'form-control','required']) }}
							</div>
							
							{{ Form::hidden('another',0, ['id'=>'another']) }}
						</div>

					</div>

					<div class="clearfix"></div>
					<div class="form-group text-right">
						{{ Form::submit('Send', array('class'=>'btn btn-primary')) }} 
						{{ Form::button('Send and Add Another', array('id' => 'add_another','class'=>'btn btn-primary')) }} 
						{{ Form::submit('Close', array('class'=>'btn btn-warning','data-dismiss' => 'modal')) }}
					</div>

				{{ Form::close() }}

				<div class="clearfix"></div>
			</div>
		  
		</div>
	</div>
</div>
@include('modal-subscription-expired');

<input type="hidden" name="unsubscribe" id="unsubscribe" value="{{ empty($subscription) ? 0:1 }}" />

@stop
@section('javascript')
<script type="text/javascript">

	// if($('#unsubscribe').val() == 0)
	// 	$('#modal-subscription-expired').modal('show');

	$('#add_another').click(function(){
		$('#another').val(1);
		$('#key_form').submit();
	});

	if('{{ $another }}' == '1'){
		$('#modal_distributed_keys').modal('show');
	}
</script>
@stop
