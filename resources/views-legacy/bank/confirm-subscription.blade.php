@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('nav')

@show

@section('content')
    <div id="loginwrapper" class="container">
	{{ Form::open(array('url' => 'bank_subscribe/recurring_profile', 'id' => 'recurring_payment')) }}

        <h3 style="margin-top:0px;">Confirm Payment Details</h3>

        <h4> Supervisor Account Subscription </h4>

        <p>
            <b>Total Amount:</b> {{ number_format($total_price, 2) }}
        </p>

        {{ Form::hidden('paypal_token', $paypal_token, array('id' => 'paypal_token')) }}

        {{ Form::submit('CONFIRM',array('class'=>'btn btn-large btn-primary')) }}

	{{ Form::close() }}
    </div>
@stop
