@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Questionnaire Configuration</h1>
                <p>
                    The Questionnaire has been pre-populated based on your preferences.
                    You can set more detailed requirements below with the ability
                    to set each question as Mandatory, Optional or Hidden.
                    <a href="#" style = 'color:#337ab7;' class="popup-hint" data-hint="{{ Lang::get('messages.mandatory_questions', array(), 'en') }} <br/><br/> {{ Lang::get('messages.optional_questions', array(), 'en') }} <br/><br/> {{ Lang::get('messages.hidden_questions', array(), 'en') }}">
                        <small>
                            <i class = 'glyphicon glyphicon-question-sign'> </i>
                            More info...
                        </small>
                    </a>
                    <br/>
                    <br/>

                    Some sections cannot be configured due to the nature of our model. Other questions are fair game.
                </p>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
            </div>
        </div>

        <div>
            <div class="panel-group" id="questionnaire-config-tabs" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="biz1-tab-config">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#biz1-item-config" aria-expanded="true" aria-controls="biz1-item-config">
                                Business Details I
                            </a>
                        </h4>
                    </div>

                    <div id="biz1-item-config" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="biz1-tab-config">
                        <div class="panel-body">
                            @foreach ($biz1_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                       </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $biz1_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="biz2-tab-config">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#biz2-item-config" aria-expanded="false" aria-controls="biz2-item-config">
                                Business Details II
                            </a>
                        </h4>
                    </div>

                    <div id="biz2-item-config" class="panel-collapse collapse" role="tabpanel" aria-labelledby="biz2-tab-config">
                        <div class="panel-body">
                            @foreach ($biz2_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                        </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $biz2_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="owner-tab-config">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#owner-item-config" aria-expanded="false" aria-controls="owner-item-config">
                                Owner Details / Key Managers
                            </a>
                        </h4>
                    </div>

                    <div id="owner-item-config" class="panel-collapse collapse" role="tabpanel" aria-labelledby="owner-tab-config">
                        <div class="panel-body">
                            @foreach ($owner_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                        </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $owner_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="ecf-tab-config">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#ecf-item-config" aria-expanded="false" aria-controls="ecf-item-config">
                                Existing Credit Facilities
                            </a>
                        </h4>
                    </div>

                    <div id="ecf-item-config" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ecf-tab-config">
                        <div class="panel-body">
                            @foreach ($ecf_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                        </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $ecf_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="cns-tab-config">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#cns-item-config" aria-expanded="false" aria-controls="cns-item-config">
                                Condition and Sustainability
                            </a>
                        </h4>
                    </div>

                    <div id="cns-item-config" class="panel-collapse collapse" role="tabpanel" aria-labelledby="cns-tab-config">
                        <div class="panel-body">
                            @foreach ($cns_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                        </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $cns_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="rcl-tab-config">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#questionnaire-config-tabs" href="#rcl-item-config" aria-expanded="false" aria-controls="rcl-item-config">
                                Required Credit Line
                            </a>
                        </h4>
                    </div>

                    <div id="rcl-item-config" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rcl-tab-config">
                        <div class="panel-body">
                            @foreach ($rcl_sections as $key => $val)
                            <div class = 'row'>
                                <div class = 'col-md-2'>
                                    @if (in_array($val, $algo_related))
                                        <span class = 'pull-right'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            Optional
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            Required
                                        @else
                                            Hidden
                                        @endif
                                        </span>
                                    @else
                                    <select class = 'change-config pull-right' name = '{{ $val }}'>
                                        @if (QUESTION_CNFG_OPTIONAL == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @elseif (QUESTION_CNFG_REQUIRED == $config_data->$val)
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                        @else
                                            <option value = '{{ QUESTION_CNFG_HIDDEN }}'> Hidden </option>
                                            <option value = '{{ QUESTION_CNFG_REQUIRED }}'> Required </option>
                                            <option value = '{{ QUESTION_CNFG_OPTIONAL }}'> Optional </option>
                                        @endif
                                    </select>
                                    @endif
                                </div>
                                <div class = 'col-md-10'>
                                    {{ $rcl_lbl[$key] }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="standalone-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p>You have set Financial Analysis Report Standalone mode "ON". Industry weight settings will be fixed. Changes in
					<a href="{{URL::to('/bank/formula')}}">Rating Formula</a>,
					<a href="{{URL::to('/bank/document_options')}}">Document Options</a> and
					Questionnaire Config will not have any effect.</p>
				<p>To turn off Financial Analysis Report Standalone mode, uncheck the option in <a href="{{URL::to('/bank/industry')}}">Industry Weights</a>.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
	var select_input = $('.change-config');

    select_input.change(function() {
       var config_lbl           = $(this).attr('name');
       var config_val           = $(this).val();
       var post_val             = {};
       post_val[config_lbl] = config_val;

        $.post(BaseURL+'/bank/set_questionnaire_config', post_val, function(response){
            console.log(response);
        });
    });
});
</script>
@if($bank_standalone==1)
<script>$(document).ready(function(){$('#standalone-modal').modal()});</script>
@endif
@stop
