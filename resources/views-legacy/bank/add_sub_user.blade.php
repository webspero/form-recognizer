@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Add New User Account</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postSubUsers', 'method'=>'post')) }}
					<div class="form-group">
						{{ Form::label('email', 'Email Address') }}
						{{ Form::text('email', Input::old('email'), array('class'=>'form-control')) }}
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('password', 'Password') }}
							{{ Form::password('password', array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('password_confirmation', 'Confirm Password') }}
							{{ Form::password('password_confirmation', array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('firstname', 'First Name') }}
							{{ Form::text('firstname', Input::old('firstname'), array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('lastname', 'Last Name') }}
							{{ Form::text('lastname', Input::old('lastname'), array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group">
						<strong>Permissions</strong>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							{{-- <label><input type="checkbox" name="permissions[]" value="2" /> Industry Weights</label><br/> --}}
							<label><input type="checkbox" name="permissions[]" value="3" /> Client Keys</label><br/>
                            {{-- <label><input type="checkbox" name="permissions[]" value="6" /> Rating Formula</label><br/> --}}
                            {{-- <label><input type="checkbox" name="permissions[]" value="8" /> Document Options</label><br/> --}}
							<label><input type="checkbox" name="permissions[]" value="10" /> Notifications</label><br />
							<label><input type="checkbox" name="permissions[]" value="4" /> Portfolio Statistics</label><br/>
						</div>
						<div class="col-sm-6">
							<label><input type="checkbox" name="permissions[]" value="5" /> Credit Investigator</label><br />
                            <label><input type="checkbox" name="permissions[]" value="7" /> Heat Map</label><br />
							<label><input type="checkbox" name="permissions[]" value="11" /> Company Matching</label><br />
                            {{-- <label><input type="checkbox" name="permissions[]" value="9" /> Questionnaire Configuration</label><br /> --}}
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group text-center">
						{{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
