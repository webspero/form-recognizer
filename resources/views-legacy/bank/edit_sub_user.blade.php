@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit User Account</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::model($entity, ['route' => ['putSubUsers', $entity->loginid], 'method' => 'put']) }}
					<div class="form-group">
						{{ Form::label('email', 'Email Address') }}
						{{ Form::text('email', $entity->email, array('class'=>'form-control', 'disabled')) }}
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('password', 'Password (leave blank for no change)') }}
							{{ Form::password('password', array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('password_confirmation', 'Confirm Password') }}
							{{ Form::password('password_confirmation', array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							{{ Form::label('firstname', 'First Name') }}
							{{ Form::text('firstname', $entity->firstname, array('class'=>'form-control')) }}
						</div>
						<div class="col-sm-6">
							{{ Form::label('lastname', 'Last Name') }}
							{{ Form::text('lastname', $entity->lastname, array('class'=>'form-control')) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							{{-- <label><input type="checkbox" name="permissions[]" value="2" {{ (in_array('2',$entity->permissions)) ? 'checked="checked"' : '' }} /> Industry Weights</label><br/> --}}
							<label><input type="checkbox" name="permissions[]" value="3" {{ (in_array('3',$entity->permissions)) ? 'checked="checked"' : '' }} /> Client Keys</label><br/>
							<label><input type="checkbox" name="permissions[]" value="10" {{ (in_array('10',$entity->permissions)) ? 'checked="checked"' : '' }} /> Notifications</label><br />
							<label><input type="checkbox" name="permissions[]" value="4" {{ (in_array('4',$entity->permissions)) ? 'checked="checked"' : '' }} /> Portfolio Statistics</label><br/>
							{{-- <label><input type="checkbox" name="permissions[]" value="6" {{ (in_array('6',$entity->permissions)) ? 'checked="checked"' : '' }} /> Rating Formula</label><br/>
							<label><input type="checkbox" name="permissions[]" value="8" {{ (in_array('8',$entity->permissions)) ? 'checked="checked"' : '' }} /> Document Options</label><br/> --}}
						</div>
						<div class="col-sm-6">
							<label><input type="checkbox" name="permissions[]" value="5" {{ (in_array('5',$entity->permissions)) ? 'checked="checked"' : '' }} /> Credit Investigator</label><br />
							<label><input type="checkbox" name="permissions[]" value="7" {{ (in_array('7',$entity->permissions)) ? 'checked="checked"' : '' }} /> Heat Map</label><br />
							<label><input type="checkbox" name="permissions[]" value="11" {{ (in_array('11',$entity->permissions)) ? 'checked="checked"' : '' }} /> Company Matching</label><br />
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group text-center">
						{{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
