@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<h2>Rating Formula Customization</h2>

		@if(@count($history) > 0)
		<div class="form-inline">
			<div class="form-group">
				<label>History</label>
				<select id="history_select" class="form-control">
					@foreach($history as $h)
						<option value="{{$h->id}}">{{$h->name}} ({{$h->created_at}})</option>
					@endforeach
				</select>
				<input type="button" class="btn btn-primary btn-set-history" value="Load This Setting"></button>
				<span class="formula_save_status2"></span>
			</div>
		</div>
		<br />
		@endif
		<div id="formula_panel">
			<span>Rating = Summation of:</span> <a class="add_new_section_btn" href="#"><span class="glyphicon glyphicon-plus green" aria-hidden="true"></span> Add New Section</a>
			<ul>
				<li>
					<span>Business Considerations and Conditions (BCC) = Summation of:</span> &times; <span class="iw_box">Industry Weight - Business Condition Factor</span>
					<ul class="bcc">
						<li {{ (strpos($formula->bcc, 'rm') === false) ? 'style="display: none;"' : '' }} class="rm"><span>Risk Management</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->bcc, 'cd') === false) ? 'style="display: none;"' : '' }} class="cd"><span>Customer Dependency</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->bcc, 'sd') === false) ? 'style="display: none;"' : '' }} class="sd"><span>Supplier Dependency</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->bcc, 'boi') === false) ? 'style="display: none;"' : '' }} class="boi"><span>Business Outlook Index Score</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
					</ul>
				</li>
				<li>
					<span>Management Quality (MQ) = Summation of:</span> &times; <span class="iw_box">Industry Weight - Management Quality Factor</span>
					<ul class="mq">
						<li {{ (strpos($formula->mq, 'boe') === false) ? 'style="display: none;"' : '' }} class="boe"><span>Business Owner Experience</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->mq, 'mte') === false) ? 'style="display: none;"' : '' }} class="mte"><span>Management Team Experience</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->mq, 'bd') === false) ? 'style="display: none;"' : '' }} class="bd"><span>Business Drivers and Main Business Segments</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->mq, 'sp') === false) ? 'style="display: none;"' : '' }} class="sp"><span>Succession Plan</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->mq, 'ppfi') === false) ? 'style="display: none;"' : '' }} class="ppfi"><span>Past Projects and Future Initiatives</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
					</ul>
				</li>
				<li>
					<span>Financial Analysis (FA) = Summation of:</span> &times; <span class="iw_box">Industry Weight - Financial Analysis Factor</span>
					<ul class="fa">
						<li {{ (strpos($formula->fa, 'fp') === false) ? 'style="display: none;"' : '' }} class="fp"><span>Rating of Financial Position</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
						<li {{ (strpos($formula->fa, 'fr') === false) ? 'style="display: none;"' : '' }} class="fr"><span>Rating of Financial Performance</span><span class="glyphicon glyphicon-remove red" aria-hidden="true"></span></li>
					</ul>
				</li>
			</ul>
		</div>
		<label>
			<input type="checkbox" id="boost" value="1" {{ ($formula->boost==1) ? 'checked="checked"' : '' }} /> Include Verification Modifiers?
		</label>
		<br />
		<button class="btn btn-primary btn-formula-update" disabled="disabled">Save Settings</button>
		<a href="{{URL::to('/bank/formula_reset')}}" class="btn btn-primary">Reset</a>
	</div>
</div>
<div id="formula_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Add New Section</h4>
			</div>
			<div class="modal-body">
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<span class="formula_select_selected" item="">Select item to add</span>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li class="dropdown-header">Business Considerations and Conditions (BCC)</li>
						<li><a href="#" item="bcc_rm">Risk Management</a></li>
						<li><a href="#" item="bcc_cd">Customer Dependency</a></li>
						<li><a href="#" item="bcc_sd">Supplier Dependency</a></li>
						<li><a href="#" item="bcc_boi">Business Outlook Index Score</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Management Quality (MQ)</li>
						<li><a href="#" item="mq_boe">Business Owner Experience</a></li>
						<li><a href="#" item="mq_mte">Management Team Experience</a></li>
						<li><a href="#" item="mq_bd">Business Drivers and Main Business Segments</a></li>
						<li><a href="#" item="mq_sp">Succession Plan</a></li>
						<li><a href="#" item="mq_ppfi">Past Projects and Future Initiatives</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Financial Analysis (FA)</li>
						<li><a href="#" item="fa_fp">Rating of Financial Position</a></li>
						<li><a href="#" item="fa_fr">Rating of Financial Performance</a></li>
					</ul>
				</div>
				<span class="error-message"></span>
				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary formula-add-btn">Add</button>
			</div>
		</div>
	</div>
</div>
<div id="formula_update_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Save Setting</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" id="formula_name" class="form-control" />
				</div>
			</div>
			<div class="modal-footer">
				<span class="formula_save_status"></span>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-formula-save">Save</button>
			</div>
		</div>
	</div>
</div>
<div id="standalone-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p>You have set Financial Analysis Report Standalone mode "ON". Industry weight settings will be fixed. Changes in
					Rating Formula,
					<a href="{{URL::to('/bank/document_options')}}">Document Options</a> and
					<a href="{{URL::to('/bank/questionnaire_config')}}">Questionnaire Config</a> will not have any effect.</p>
				<p>To turn off Financial Analysis Report Standalone mode, uncheck the option in <a href="{{URL::to('/bank/industry')}}">Industry Weights</a>.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script src="{{ URL::asset('/js/formula.js') }}"></script>
@if($bank_standalone==1)
<script>$(document).ready(function(){$('#standalone-modal').modal()});</script>
@endif
@stop
