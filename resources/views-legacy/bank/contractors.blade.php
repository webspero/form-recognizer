@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="text-right">
		<a href="{{URL::to('/')}}/bank/add_contractors" class="btn btn-primary">Add New User Account</a>
	</div>
	<div class="clearfix"></div>
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Last Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">First Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Responsibilities</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Actions</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($users as $u)
				<tr class="odd" role="row">
					<td>{{ $u->email }}</td>
					<td>{{ $u->lastname }}</td>
					<td>{{ $u->firstname }}</td>
					<td>{{ $u->responsibilities_formatted }}</td>
					<td>
						<a href="{{URL::to('/')}}/bank/edit_contractor/{{$u->loginid}}">edit</a> |
						<a href="{{URL::to('/')}}/bank/delete_contractor/{{$u->loginid}}">delete</a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('javascript')
@stop
