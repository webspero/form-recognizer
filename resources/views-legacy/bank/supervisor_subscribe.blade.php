@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">    
    <div class="row">
        <div class="col-md-4">
		    <h3> Supervisor Subscription </h3>
        </div>        
    </div>
	<hr/>
    
    <br/>
	<div class="table-responsive">
        <input type="hidden" id="user_id" value="{{$user->loginid}}" />
		<table id="entitydata" class="table table-striped table-bordered table-hover" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Name</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Started</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Expiration</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Subscription</th>
                    <th style="background-color: #055688;color: #fff; font-weight: bold;">Action</th>
				</tr>
			</thead>
			<tbody>
                    @foreach($subscription as $sub)
                    <tr>
                        <td> {{$user->name}}</td>
                        <td> {{date("M d, Y", strtotime($sub->created_at))}}</td>
                        <td> {{date("M d, Y", strtotime($sub->expiry_date))}}</td>
                        <td> {{$sub->subscription_status}}</td>
                        @if($sub->subscription_status == "Active")
                        <td> <a href='#' id = "sub_btn" > {{$sub->subscription_btn}} </a></td>
                        @else
                        <td> <a href='{{ URL::To("/bank/subscribe")}}' > {{$sub->subscription_btn}} </a></td>
                        @endif

                    </tr>
                    @endforeach
			</tbody>
		</table>
	</div>
</div>

@include('modal-subscription-expired');

<input type="hidden" name="unsubscribe" id="unsubscribe" value="{{ empty($currentSubscription) ? 0:1 }}" />

<script>
$(function(){
	if($('#unsubscribe').val() == 0)
		$('#modal-subscription-expired').modal('show');
	
    $("#sub_btn").on("click", function(){
        var confirm_modal = $("<div> Do you want to unsubscribe? </div>").dialog({
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            title: 'Subscription',
            modal: true,
            open: function(event, ui) { 
                /** Remove the close button */
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                'Continue': function() {
                    $(this).dialog('close');
                    
                    confirmEndSubscription();
                },
                'Cancel': function() {
                    $(this).dialog('destroy');
                }
            }
        });
    })
});
function confirmEndSubscription(){
    $.ajax({
        method: 'POST',
        url:'/bank/subscription',
        data: {id: $("#user_id").val()},
        success: function(response){
            location.reload();
        }
    });
}
</script>
@stop
