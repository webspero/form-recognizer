@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO - Reports List</title>
@stop
@section('style')
<style>
    @media print{
   .noprint{
       display:none;
   }
}
</style>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<h3>Report Listing</h3>
	<div class="row no-print">
		<div class="col-sm-12" style="border-right: 1px solid #e8e8e8;">
			{{Form::open(array('route'=>'getBankReportList', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group form_row_col">
						{{Form::label('email', 'Email', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{ Form::text('email',$email, array('class'=>'form-control'))}}
						</div>
					</div>

					<div class="form-group form_row_col">
						{{Form::label('sme', 'Organization', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							<select id="select-sme" class="form-control" name="sme[]" multiple="multiple">
								@foreach($entity as $sme)
									@if(in_array($sme->entityid, $smeSelected))
										<option value="{{ $sme->entityid }}" selected="selected">{{$sme->companyname}}</option>
									@else
										<option value="{{ $sme->entityid }}">{{$sme->companyname}}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>
					
					<div class="form-group form_row_col">
						{{Form::label('bcc', 'Rating', array('class'=>'col-sm-6'))}}
						<div class="col-sm-6 form-inline">
							<div class="form-group">
								{{Form::select('bcc_from', array(
									'' => ' - ',
									'AAA' => 'AAA',
									'AA' => 'AA',
									'A' => 'A',
									'BBB' => 'BBB',
									'BB' => 'BB',
									'B' => 'B',
									'CCC' => 'CCC',
									'CC' => 'CC',
									'C' => 'C',
									'D' => 'D',
									'E' => 'E',
								), $bccFrom, array('class'=>'form-control'))}}
								<label>to</label>
								{{Form::select('bcc_to', array(
									'' => ' - ',
									'AAA' => 'AAA',
									'AA' => 'AA',
									'A' => 'A',
									'BBB' => 'BBB',
									'BB' => 'BB',
									'B' => 'B',
									'CCC' => 'CCC',
									'CC' => 'CC',
									'C' => 'C',
									'D' => 'D',
									'E' => 'E',
								), $bccTo, array('class'=>'form-control'))}}
							</div>
						</div>
					</div>

					</div>

					<div class="col-sm-4">

					<!-- <div class="form-group form_row_col">
						{{Form::label('region', 'Region', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{Form::select('region',[''=>'All'] + $provinces, $region, array('class'=>'form-control'))}}
						</div>
					</div> -->

				

					<div class="form-group form_row_col">
						{{Form::label('industry_section', 'Industry Section', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{Form::select('industry_section',[''=>'All'] + $industries, $industry_section, array('class'=>'form-control','id' => 'industry_section'))}}

							<input type="hidden" name="s_industry_section" id="s_industry_section" value="{{ $industry_section }}">
						</div>
					</div>

					<div class="form-group form_row_col">
						{{Form::label('industry_division', 'Industry Division', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{Form::select('industry_division',[''=>'All'], $industry_division, array('class'=>'form-control','id' => 'industry_division'))}}

							<input type="hidden" name="s_industry_division" id="s_industry_division" value="{{ $industry_division }}">
						</div>
					</div>

					</div>

					<div class="col-sm-4">

					<div class="form-group form_row_col">
						{{Form::label('industry_group', 'Industry Group', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{Form::select('industry_group',[''=>'All'], $industry_group, array('class'=>'form-control','id' => 'industry_group'))}}

							<input type="hidden" name="s_industry_group" value="{{ $industry_group }}">
						</div>
					</div>

					<div class="form-group form_row_col">
						{{Form::label('earnings_manipulator', 'Earnings Manipulator', array('class'=>'col-sm-4'))}}
						<div class="col-sm-8">
							{{Form::select('earnings_manipulator',[
								''=>'All',
								'Likely'=>'Likely',
								'Unlikely'=>'Unlikely'
							],$earningsManipulator, array('class'=>'form-control'))}}
						</div>
					</div>
				</div>

				<div class="form_row col-xs-12" style="text-align: right">
					<button class="update_table_list btn btn-primary">Update Table List</button>
				</div>
			</div>
			{{ Form::close() }}
		</div>

	<div class="{{(Auth::user()->role==1) ? 'hidden-xs' : 'col-xs-12'}}">
        <table id="entitydata" class="table table-striped table-bordered" style="table-layout: fixed; width:100%; overflow: hidden;">
			<thead>
				<tr>
                    <th class="text-center noprint" style="background-color: #055688;color: #fff; font-weight: bold;">
                        <input type="checkbox" name="select_all" value="1" id="select-all">
                    </th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Company Name</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Basic User Email</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Revenue Growth</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Years in Business</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Province</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Latest CreditBPO Rating</th>
					<th style="display:none; background-color: #055688;color: #fff; font-weight: bold;">Supervisor Rating</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Rating Date</th>
                    {{-- commented out for future use --}}
					{{-- <th style="background-color: #055688;color: #fff; font-weight: bold;">Utility Bill</th> --}}
          @if(Auth::user()->role != 1)
            <th style="display:none; background-color: #055688;color: #fff; font-weight: bold;">Beneish M-Score</th>
            <th style="background-color: #055688;color: #fff; font-weight: bold;">Earnings Manipulator</th>
          @endif
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Preliminary Probability of Default</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Latest Report Status</th>
                    @if(Auth::user()->role == 4 || Auth::user()->role == 2)
					<th style="background-color: #055688;color: #fff; font-weight: bold;">CI Verification Status</th>
                    @endif
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Approval Status</th>

				</tr>
			</thead>
			<tbody>
            @foreach ($entity as $info)
				<tr class="odd" role="row">
                    <td class="text-center noprint"></td>
					<td style="overflow: hidden; word-break: break-word;">
                        <a href="{{ URL::to('/') }}/summary/smesummary/{{ $info->entityid }}">

                            @if (1 == $info->anonymous_report)
                                Company - {{ $info->entityid }}
                            @else
                                {{ $info->entityid }} - {{ $info->companyname }}
                            @endif
                        </a>
                    </td>
					<td style="overflow: hidden; word-break: break-word;">{{ $info->basic_email }}</td>
					<td>
                        <!-- @if (!empty($info->forecast_data["gf_icon"]))
                        <img src = '{{ $info->forecast_data["gf_icon"] }}' style = 'height: 50px; width: 50px;'/>
                        @endif -->
                        @if(isset($info->revenue_growth))
                            @if($info->revenue_growth == 0)
                                <span style="color:black;"> {{$info->revenue_growth}} </span>
                            @else
                                <span style="color:green;"> {{$info->revenue_growth}} </span>
                            @endif
                        @endif
                    </td>
					<td>{{ $info->years_in_business }}</td>
					<td>{{ $info->province }}</td>
					<td><span style="color: {{ $info->cbpo_score_color }}; font-weight: bold;">{{$info->val == "In Progress" ? '' : $info->score_sf}}</span></td>
					<td style="display:none"><span style="color: {{ $info->bank_score_color }}; font-weight: bold;">{{$info->val == "In Progress" ? '' : $info->score_bank}}</span></td>
					<td>
                        @if($info->val== "In Progress")
                        @else
                            {{ ($info->completed_date!=null) ? date('m/d/Y', strtotime($info->completed_date)) : ""}}
                        @endif
                    </td>
                    {{-- commented out for future use --}}
                    {{-- <td>
                        @if ($info->utility_bill == 0)
                            No Data
                        @elseif ($info->utility_bill == 1)
                            Current
                        @elseif ($info->utility_bill == 2)
                            Overdue
                        @endif
                    </td> --}}
                    @if(Auth::user()->role != 1)
                        <td style="display:none">{{$info->val == "In Progress" ? '' :  number_format($info->m_score, 2)}}</td>
                        <td>
                            {{ $info->earnings_manipulator }}
                        </td>
                    @endif
					<td>{{ $info->main_title }}</td>
					<td><span style="color: red; font-weight: bold;">{{$info->val == "In Progress" ? '' :  $info->pdefault}}</span></td>
					<td>{!! $info->status !!}</td>
                    @if(Auth::user()->role == 4 || Auth::user()->role == 2)
					<td>{!! $info->invst_sts !!}</td>
                    @endif
					<td>
                        @if (STR_EMPTY != $info->score_sf)
                        <a class = 'upd-approval-link' href = '#' data-name = '{{ $info->companyname }}' data-report-id = '{{ $info->entityid }}'>
                            <i class = "glyphicon glyphicon-edit"> </i>
                            <span id = 'upd-approval-lbl-{{ $info->entityid }}'>
                                @if (LOAN_APPROVED_STS == $info->approval_status)
                                    Loan Approved
                                @elseif (PROJ_APPROVED_STS == $info->approval_status)
                                    Project Denied
                                @elseif (LOAN_DENIED_STS == $info->approval_status)
                                    Loan Denied
                                @elseif (PROJ_DENIED_STS == $info->approval_status)
                                    Project Denied
                                @else
                                    In Process
                                @endif
                            </span>
                        </a>
                        @else
                            Not yet rated
                        @endif
                    </td>
				</tr>
			@endforeach
			</tbody>
        </table>

        <button class="btn btn-primary noprint" data-toggle="modal" data-target="#receiver_modal">
            Send Email
        </button>
	</div>
</div>

<div class="modal" id="bank-dev-codes" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Developer API Request Codes</h4>
            </div>
            <div class="modal-body" id = 'rating-tab-body'>
                <ol>
                    <li>
                        <p style="word-wrap: break-word;">
                            <b>Request all Credit Ratings in this account</b> <br/>
                            <b>URL: </b>{{ URL::to('/cbpo_pub_rest/get_account_rating/') }}<br/>
                            @if(Session::has('supervisor'))
                            <b>Key Code: </b>{{ base64_encode(base64_encode(Session::get('supervisor.loginid').'$account')) }}
                            @else
                            <b>Key Code: </b>{{ base64_encode(base64_encode(Auth::user()->loginid.'$account')) }}
                            @endif
                        </p>
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Select Receiver of Notification -->
<div class="modal fade noprint" id="receiver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Send Report Notification</h5>
      </div>
      <div class="modal-body">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="basic_user" name="basic_user">
            <label class="form-check-label">
                Basic User
            </label>
            <br>
            <input class="form-check-input" type="checkbox" id="supervisor" name="supervisor">
            <label class="form-check-label">
                Supervisor
            </label>
            <br>
            <input class="form-check-input" type="checkbox" id="others" name="others">
            <label class="form-check-label">
                Others
            </label>
            <br>
            <div hidden id="emailTagName">
                <p> <small> Add Email: </small></p>
                <input id="emailInput" class="input-tags form-control" type="text" data-role="tagsinput">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="close_modal" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="selected_receiver">Send</button>
        <div id="spinner" hidden>
            <span> <img src="{{URL::asset('images/email.gif')}}" width="65px" height="35px"></span>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- spinner -->
<!-- 
<div class="modal" role="dialog" id="spinnerModal" hidden style="top: 50% !important; position:absolute; left: 30% !important; transform: traslate(-50%,-50%) !important; margin: 0px !important;">
    <div class="modal-dialog spinnerClass" role="document" >
        <div class="modal-content">
        <img src="{{URL::asset('images/email.gif')}}" width="80px" height="35px">
        </div>
    </div>
</div> -->



@include('user-2fa-form')
@include('modal-upd-approval')
@include('modal-approval-process')

@stop
@section('javascript')
<script src="{{ URL::asset('js/prev-page-scroll.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/user-2fa-auth.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script type="text/javascript" scr="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        var industryValue = $('#industryValue').val();

        if(industryValue){
            document.getElementById('type_select').getElementsByTagName('option')[3].selected = 'selected';
            $("#industry_field").val(industryValue);
        }
    });
</script>

<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">

	$('#select-sme').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#industry_section').change(function(){
		let industrySection = $(this).val();
        if(industrySection == ''){
            industrySection = false;
        }

		$.ajax({
			url: "{{ url('signup/getIndustrySub') }}/" + industrySection,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';

				$.each(data, function(key,value){
					options += '<option value="'+value.sub_code+'">'+value.sub_title+'</option>';
				});

				$('#industry_division').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	});

	$('#industry_division').change(function(){
		let industryDivision = $(this).val();

        if(industryDivision == ''){
            industryDivision = false;
        }
		$.ajax({
			url: "{{ url('signup/getIndustryRow') }}/" + industryDivision,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';

				$.each(data, function(key,value){
					options += '<option value="'+value.group_code+'">'+value.group_description+'</option>';
				});

				$('#industry_group').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	});

	getIndustryDivision();

	function getIndustryDivision(){
		let industrySection = $('#s_industry_section').val();
        if(industrySection == ''){
            industrySection = false;
        }

		$.ajax({
			url: "{{ url('signup/getIndustrySub') }}/" + industrySection,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';
				let s_industry_division = $('#s_industry_division').val();

				$.each(data, function(key,value){
					options += '<option value="'+value.sub_code+'"';

					if(s_industry_division != '')
						options += ' selected ';

					options += '>'+value.sub_title+'</option>';
				});

				$('#industry_division').find('option')
				    .remove()
				    .end().append(options);
			}
		});

	}

	getIndustryGroup();

	function getIndustryGroup(){
		let industryDivision = $('#s_industry_division').val();
        if(industryDivision == ''){
            industryDivision = false;
        }
		$.ajax({
			url: "{{ url('signup/getIndustryRow') }}/" + industryDivision,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				
				let options = '<option value="">All</option>';
				let s_industry_group = $('#s_industry_group').val();

				$.each(data, function(key,value){
					options += '<option value="'+value.group_code+'"';
					if(s_industry_group != ''){
						options += ' selected ';	
					}
					options += '>'+value.group_description+'</option>';
				});

				$('#industry_group').find('option')
				    .remove()
				    .end().append(options);
			}
		});
	}

</script>
<script>
$(document).ready(function(){
    $('#entitydata').DataTable({
        columnDefs: [{
            orderable: false,
            targets:   0,
            render: function(data, type, row, meta){
                str = "";
                str = row[1];
                str1 = str.split('- ')[1];
                company = str1.replace("</a>", '');
                var companyname = company.replace(/^\s+|\s+$/g, '');
                
                ent = str.split('>')[1];
                str_ent = ent.replace(/^\s+|\s+$/g, '');
                entity = str_ent.substr(0, str_ent.indexOf(' ')); 
                var entityId = entity.replace(/^\s+|\s+$/g, '')
 
                if(row[13].indexOf("Completed") != -1){
                    return '<input type="checkbox" class="entity_record" id="'+ entityId +'" name="'+ companyname +'" email="' + row[2] + '" value="' + $('<div/>').text(data).html() + '">';
                }else{
                    return '<input type="checkbox" class="entity_record" disabled  id="'+ entityId +'" name="'+ companyname +'" email="' + row[2] + '" value="' + $('<div/>').text(data).html() + '">';
                }
            }
        }]
        /*,
        order: [[ 7, 'desc' ]]*/
    });

    var table = $('#entitydata').DataTable(); 

    /** Handle select all checkbox */
    $('#select-all').on('click', function(){
        /** Get all rows with search applied */ 
        let rows = table.rows({ 'search': 'applied' }).nodes();
        /** Check/uncheck checkboxes for all rows in the table */
        $('input[type="checkbox"]:enabled', rows).prop('checked', this.checked);
    });

    /** Handle click on checkbox to set state of "Select all" control */
    $('#entitydata tbody').on('change', 'input[type="checkbox"]', function(){
        if(!this.checked){
            var el = $('#select-all').get(0); 
            /** If "Select all" control is checked and has 'indeterminate' property */
            if(el && el.checked && ('indeterminate' in el)){
                /** Set visual state of "Select all" control as 'indeterminate' */
                el.indeterminate = true;
            }
        }
    });

    $('#others').on("click", function(){
        if($('#others').is(':checked')){
            $('#emailTagName').show();
        }else{
            $('#emailTagName').hide();
        }
    });

    $('#receiver_modal').on('hidden.bs.modal', function(){
        $('#emailInput').val('');
        $(this).find('input[type="checkbox"]').prop('checked', false);
        $('#emailTagName').hide();
    });

    $('#selected_receiver').on("click", function(){
        var record = [];

        let checkedvalues = table.$('input:checked').each(function () {
            entity = new Object();
            entity.id = $(this).attr('id');
            entity.email = $(this).attr('email');
            entity.company = $(this).attr('name');
            record.push(entity);
        });

        if($('#basic_user').prop("checked") == true){
            var basic_user = 1;
        }
        if($('#supervisor').prop("checked") == true){
            var supervisor = 1;
        }
        if($('#others').prop("checked") == true){
            var others = 1;
        }

        var emailOthers =  $('#emailInput').tagsinput('items');

        if($('#others').is(':checked')){
            $('#emailTagName').show();
        }else{
            $('#emailTagName').hide();
            emailOthers = '';
        }

        if(record.length == 0){
            alert("Please select a record before sending an email.");
            $('#receiver_modal').modal('toggle');
        }else if( $('#basic_user').prop("checked") == false && $('#supervisor').prop("checked") == false && $('#others').prop("checked") == false){
            alert("Please select a receiver of the report.");
        }
        else{
            $.ajax({
                url: BaseURL + '/bank/email/reportlist',
                type: 'post',
                dataType: 'json',
                data: {
                    entity: record,
                    basicUser: basic_user,
                    supervisor: supervisor,
                    others: others,
                    otherEmail: emailOthers
                },
                beforeSend: function(){
                    $('#selected_receiver').hide();
                    $('#close_modal').hide();
                    $('#spinner').show();
                },
                success: function(resp){
                    $('#selected_receiver').show();
                    $('#close_modal').show();
                    $('#spinner').hide();
                    $('#receiver_modal').modal('toggle');
                    table.$('.entity_record').removeAttr( 'checked' );
                }
            });
        }  
    });
});
</script>
<script>
    $(document).ready(function(){
        $('#bank-dev-link').click(function(e){
            $('#bank-dev-codes').modal();
            return false;
        });

        var t;

        if (document.getElementById('printWindow')) {
            document.getElementById('printWindow')
                .addEventListener('click', function() {
                    if ($zoho && $zoho.salesiq && $zoho.salesiq.floatwindow) {
                        $zoho.salesiq.floatwindow.visible('hide');
                    }

                    if ($zoho && $zoho.salesiq && $zoho.salesiq.floatbutton) {
                        $zoho.salesiq.floatbutton.visible('hide');
                    }

                    clearTimeout(t);
                    t = setTimeout(function() {
                        window.print();
                        if ($zoho && $zoho.salesiq && $zoho.salesiq.floatbutton) {
                            $zoho.salesiq.floatbutton.visible('show');
                        }
                    }, 1000);
                });
        }

    });
</script>

<script>
    $(document).ready(function(){
        $('.upd-approval-link').click(function(e){
            var link_data   = $(this);

            $('#modal-upd-approval').modal()
                .on('show.bs.modal', function(evt) {
                    $('#upd-approval-company').text(link_data.data('name'));
                    $('#upd-approval-id').val(link_data.data('report-id'));
                })
                .modal('show');

            return false;
        });

        $('.upd-approval-submit').click(function(){
            $('#upd-approval-form').submit();
        });

        $('#upd-approval-form').submit(function() {

            $.post($(this).attr('action'), $(this).serialize(), function(srv_resp) {
                var srv_message = '';
                var error_cntr  = $('.approval-error-msg');

                $('.upd-approval-submit').prop('disabled', true).val('Saving');

                if (1 == srv_resp.sts) {
                    $('#modal-upd-approval').modal('hide');
                    error_cntr.hide();

                    $('#upd-approval-lbl-'+srv_resp.report_id).text(srv_resp.sts_lbl);
                }
                else {
                    /**	Formats the warnings received from the Server	*/
                    for (idx = 0; idx < srv_resp.messages.length; idx++) {
                        srv_message += srv_resp.messages[idx] +'<br/>';
                    }

                    /**	Display the warning								*/
                    error_cntr.html(srv_message);
                    error_cntr.show();
                }

                $('.upd-approval-submit').prop('disabled', false).val('Save');

            }, 'json');

            return false;
        });

        $.get(BaseURL+'/bank/check-in-process', function(srv_resp) {

           if (0 < srv_resp.length) {
               $('#modal-approval-process').modal()
                    .on('show.bs.modal', function(evt) {
                        var companies   = [];

                        /**	Formats the warnings received from the Server	*/
                        for (idx = 0; idx < srv_resp.length; idx++) {
                            companies += srv_resp[idx] +'<br/>';
                        }

                        $('#companies-list').html(companies);
                    })
                    .modal('show');
           }
        }, 'json');
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){
        $('#entitydata_filter input,label').addClass('noprint');
		$('.datepicker').datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
		});
		$('#type_select').on('change', function(){
			if($(this).val() == 'location'){
				$('.selector').hide();
				$('.location').show();
			}
			else if($(this).val() == 'date_established'){
				$('.selector').hide();
				$('.date-established').show();
			}
			else if($(this).val() == 'industry'){
				$('.selector').hide();
				$('.industry').show();
			}
			else if($(this).val() == 'status'){
				$('.selector').hide();
				$('.form-status').show();
			}
			else if($(this).val() == 'invst_sts'){
				$('.selector').hide();
				$('.ci-status').show();
			}
			else
			{
				$('.selector').hide();
				$('.search').show();
			}
		});
		$('#type_select').change();
		$('#province').on('change', function(){
			var sProvince = $(this).val();
			var uiCity = $('#city');
			$.ajax({
				url: BaseURL + '/api/cities',
				type: 'get',
				dataType: 'json',
				data: {
					province: sProvince
				},
				beforeSend: function() {
					uiCity.html('<option>&nbsp;&nbsp;&nbsp;Loading cities...</option>');
				},
				success: function(oData)	{
					uiCity.html('<option value="">All</option>');
                    for(x in oData){
                        uiCity.append('<option value="'+oData[x].id+'">'+oData[x].citymunDesc+'</option>');
                    }
					@if(Input::has('city') && Input::get('city')!="")
						uiCity.val("{{Input::get('city')}}");
					@endif
				}
			});
		});
		if($('#province').is(':visible')){
			$('#province').change();
		}
	});
</script>
<style>
    .control-set {
        text-align: right;
    }

    #entitydata {
        margin-bottom: 10px;
    }

</style>
<link rel="stylesheet" href="{{ URL::asset('css/supervisor-overview-print.css') }}" media="print"></link>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
@stop