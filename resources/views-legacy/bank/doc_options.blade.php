@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h1>Document Options</h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{ Form::open(array('route'=>'postDocumentOptions', 'method'=>'post', 'id'=>'post_doc_form', 'class'=>'form-inline')) }}
					{{Form::button('Save Settings', array('class'=>'btn btn-primary btn-save-settings'))}}
					@if(@count($history)>0)
						<div class="form-group">
							<select id="history_select" class="form-control">
								@foreach($history as $h)
									<option value="{{$h->id}}">{{$h->name}} ({{$h->created_at}})</option>
								@endforeach
							</select>
						</div>
						{{Form::button('Load Selected Setting', array('class'=>'btn btn-primary btn-load-settings'))}}
					@endif
					<input type="hidden" name="name" id="setting_name_h" />
					<br/><br/>
					<div>
						<p><strong>Required Documents</strong></p>
						<p>Basic Users under you will be required to upload documents that are checked below</p>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<strong>Corporation</strong><br/>
							<label><input type="checkbox" name="corp_sec_reg" value="1" {{ ($options->corp_sec_reg!=0) ? 'checked="checked"' : '' }} />
								SEC Registration Certificate, Articles of Incorporation and By-Laws</label><br/>
							<label><input type="checkbox" name="corp_sec_gen" value="1" {{ ($options->corp_sec_gen!=0) ? 'checked="checked"' : '' }} />
								SEC Generation Information Sheet (Most Recent)</label><br/>
							<label><input type="checkbox" name="corp_business_license" value="1" {{ ($options->corp_business_license!=0) ? 'checked="checked"' : '' }} />
								Business License / Permit to Operate</label><br/>
							<label><input type="checkbox" name="corp_tax_reg" value="1" {{ ($options->corp_tax_reg!=0) ? 'checked="checked"' : '' }} />
								Tax Registration: BIR 2303</label><br/>
							<label><input type="checkbox" name="corp_itr" value="1" {{ ($options->corp_itr!=0) ? 'checked="checked"' : '' }} />
								Income Tax Return (Most Recent)</label><br/>
							<br/>

							<strong>Sole Proprietorship</strong><br/>
							<label><input type="checkbox" name="sole_business_license" value="1" {{ ($options->sole_business_license!=0) ? 'checked="checked"' : '' }} />
								Business License / Permit to Operate</label><br/>
							<label><input type="checkbox" name="sole_business_owner" value="1" {{ ($options->sole_business_owner!=0) ? 'checked="checked"' : '' }} />
								Business owner in DTI</label><br/>
							<label><input type="checkbox" name="sole_tax_reg" value="1" {{ ($options->sole_tax_reg!=0) ? 'checked="checked"' : '' }} />
								Tax Registration: BIR 2303</label><br/>
							<label><input type="checkbox" name="sole_itr" value="1" {{ ($options->sole_itr!=0) ? 'checked="checked"' : '' }} />
								Income Tax Return (Most Recent)</label><br/>
							<br/>

							<strong>Certifications, Accreditations, and Tax Exemptions</strong><br/>
							<label><input type="checkbox" name="boi" value="1" {{ ($options->boi!=0) ? 'checked="checked"' : '' }} />
								Board of Investments (BOI) / Philippine Export Processing Zone Authority (PEZA)</label><br/>
                            <br/>

							<strong>Financial Statements</strong><br/>
							<label><input type="checkbox" name="fs_template" value="1" disabled checked }} />
								FS Template (FS Template is Mandatory)</label><br/>
                            <br/>

							<strong>Cash Proxy</strong><br/>
							<label><input type="checkbox" name="bank_statement" value="1" {{ ($options->bank_statement!=0) ? 'checked="checked"' : '' }} />
								Bank Statements</label><br/>
							<label><input type="checkbox" name="utility_bill" value="1" {{ ($options->utility_bill!=0) ? 'checked="checked"' : '' }} />
								Utility Bills</label><br/>
							<br/>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>

        <div> <hr/> </div>

        <div class = 'row'>
            <div class="spacewrapper">
                <div class="read-only">
                    <h1> Custom Documents </h1>

                    <div class = 'col-md-12'>
                        {{ Form::open(array('url' => 'bank/add/custom-doc', 'id' => 'custom-doc-form', 'class' => 'form-horizontal form-label-left')) }}
                            <div class="form-group">
                                <div class = 'col-md-12'> <label>Document Label:</label> </div>
                                <div class = 'col-md-7'>
                                    {{ Form::text('doc_lbl', '', array('class' => 'form-control')) }}
                                    {{ Form::hidden('doc_id', '0') }}
                                </div>

                                <div class = 'col-md-5'>
                                    <input class = 'btn btn-primary' type = 'submit' value = 'SAVE DOCUMENT'>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>

                    <div id = 'custom-doc-list'>
                        @foreach ($custom_docs as $doc)
                        <div class = 'row doc-record'>
                            <div class = 'col-md-12'>
                                <div class = 'col-md-12'>
                                    <input type="checkbox" class = 'req-custom-doc' data-doc-id = '{{ $doc->document_id }}' {{ ($doc->bank_required != 0) ? 'checked="checked"' : '' }} />
                                    &nbsp;&nbsp;
                                    <a href = '{{ URL::To("bank/get/custom-doc/".$doc->document_id) }}' class = 'edit-doc'> <i class = 'glyphicon glyphicon-pencil'> </i> </a>
                                    &nbsp;&nbsp;
                                    <a href = '{{ URL::To("bank/delete/custom-doc/".$doc->document_id) }}' class = 'delete-doc'> <i class = 'glyphicon glyphicon-remove'> </i> </a>
                                    &nbsp;&nbsp; - &nbsp;&nbsp; {{ $doc->document_label }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<div id="doc_options_save_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Save Setting</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" id="setting_name" class="form-control" />
				</div>
			</div>
			<div class="modal-footer">
				<span class="save_status"></span>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-setting-save">Save</button>
			</div>
		</div>
	</div>
</div>
<div id="standalone-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p>You have set Financial Analysis Report Standalone mode "ON". Industry weight settings will be fixed. Changes in
					<a href="{{URL::to('/bank/formula')}}">Rating Formula</a>,
					Document Options and
					<a href="{{URL::to('/bank/questionnaire_config')}}">Questionnaire Config</a> will not have any effect.</p>
				<p>To turn off Financial Analysis Report Standalone mode, uncheck the option in <a href="{{URL::to('/bank/industry')}}">Industry Weights</a>.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
	$('.btn-save-settings').click(function(e){
		e.preventDefault();
		$('#doc_options_save_modal').modal();
	});

	$('.btn-setting-save').click(function(e){
		e.preventDefault();
		if($.trim($('#setting_name').val())==""){
			$('.save_status').text('Please input setting name.');
		} else {
			$('#setting_name_h').val($('#setting_name').val());
			$('#post_doc_form').submit();
		}
	});

	$('.btn-load-settings').click(function(e){
		e.preventDefault();
		$.ajax({
			url: BaseURL + '/bank/document_options_load',
			type: 'post',
			data: {
				id: $('#history_select').val()
			},
			beforeSend: function(){
				$('.btn-load-settings').prop('disabled', true);
			},
			success: function(sReturn){
				$('.btn-load-settings').removeAttr('disabled');
				if(sReturn == "success"){
					window.location.reload(true);
				}
			}
		});
	});
});
</script>
@if($bank_standalone==1)
<script>$(document).ready(function(){$('#standalone-modal').modal()});</script>
@endif

<script>
    $(document).ready(function() {
        var doc_form = $('#custom-doc-form');

        doc_form.submit(function() {

            $.post($(this).attr('action'), $(this).serialize(), function(srv_resp) {

                if (1 == srv_resp) {
                    $('#custom-doc-list').load(BaseURL+'/bank/document_options .doc-record');
                }

            });

            $('input[name="doc_id"]').val('0');
            $('input[name="doc_lbl"]').val('');

            return false;
        });

        $('#custom-doc-list').on('click', '.delete-doc', function() {

            $.get($(this).attr('href'), function() {
                $('#custom-doc-list').load(BaseURL+'/bank/document_options .doc-record');
            });

            return false;
        });

        $('#custom-doc-list').on('click', '.edit-doc', function() {

            $.get($(this).attr('href'), function(srv_resp) {
                $('input[name="doc_id"]').val(srv_resp.document_id);
                $('input[name="doc_lbl"]').val(srv_resp.document_label);
            }, 'json');

            return false;
        });

        $('#custom-doc-list').on('click', '.req-custom-doc', function() {

            console.log($(this).prop('checked'));

            $.post(BaseURL+'/bank/required/custom-doc', { required: $(this).prop('checked'), document_id: $(this).data('doc-id') }, function() {

            });

        });
    });
</script>

@stop
