@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div class="row filters">
		<div class="col-sm-12">
			<div class="col-sm-8 col-sm-offset-2">
				<h3>COMPUTATION OF NET FINANCIAL CONTRACTING CAPACITY</h3>
				<br>
				<p>The Net Financial Contracting Capacity (NFCC) is computed as follows:</p>
				<p><b>NFCC = K</b> (current asset – current liabilities) minus value of all outstanding works under ongoing contracts including awarded contracts yet to be started</p>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td width="30%" style="text-align: right; padding-right: 20px;">Where <b>K =</b></td>
						<td>10 for a contract duration of one (1) year or less</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>15 for more than one (1) year up to two (2) years</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>20 for more than two (2) years</td>
					</tr>
				</table>
				<br><br>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<table class="table table-bordered">
							<tr>
								<td style="background-color: #DBDBDB;">Company</td>
								<td style="padding: 0; background-color: #DBDBDB;">
									<select style="background-color: #DBDBDB;" id="nfcc_company" class="form-control">
										<option value="">Choose here</option>
										@foreach($entity as $e)
											<option value="{{$e->entityid}}">{{$e->companyname}}</option>
										@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td>Current Assets</td>
								<td id="nfcc_current_assets" style="text-align: right;"></td>
							</tr>
							<tr>
								<td>- Current Liabilities</td>
								<td id="nfcc_current_liabilities" style="text-align: right;"></td>
							</tr>
							<tr>
								<td style="background-color: #DBDBDB;">* K</td>
								<td style="padding: 0; background-color: #DBDBDB;">
									<select style="background-color: #DBDBDB;" id="nfcc_k" class="form-control update_nfcc_dd">
										<option value="10">10 - contract duration of one (1) year or less</option>
										<option value="15">15 - more than one (1) year up to two (2) years</option>
										<option value="20">20 - more than two (2) years</option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="background-color: #DBDBDB;">- Value of Outstanding Contracts</td>
								<td style="padding: 0; background-color: #DBDBDB;">
									<input id="nfcc_voc" type="text" class="form-control update_nfcc" style="text-align: right; background-color: #DBDBDB;" />
								</td>
							</tr>
							<tr>
								<td><b>Net Financial Contracting Capacity (NFCC)</b></td>
								<td id="nfcc" style="text-align: right;">
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="background-color: #DBDBDB;">Approved Budget for the Contract (ABC)</td>
								<td style="padding: 0; background-color: #DBDBDB;">
									<input id="nfcc_budget" type="text" class="form-control update_nfcc" style="text-align: right; background-color: #DBDBDB;" />
								</td>
							</tr>
							<tr>
								<td><b>NFCC/ABC</b></td>
								<td id="nfcc_abc" style="text-align: right;"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/nfcc.js"></script>
@stop
@section('style')
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
@stop
