@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px;">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Edit: Industry </h1>
				@if($errors->any())
				<div class="alert alert-danger" role="alert">
					@foreach($errors->all() as $e)
						<p>{{$e}}</p>
					@endforeach
				</div>
				@endif
				{{Form::model($industry, array('method' => 'post', 'route' => array('postEditBankIndustry', $industry->id)))}}
					<div class="form-group">
						{{Form::label('main_title', 'Title')}}
						{{Form::text('main_title', $industry->main_title, array('class' => 'form-control', 'disabled' => 'disabled'))}}
					</div>
					<div class="form-group">
						<div class="row">
                            <div class = 'col-sm-4'>
                                {{Form::label('business_group', 'Business Considerations (% value)')}}
                            </div>

                            <div class = 'col-sm-4'>
                                {{Form::label('management_group', 'Management Quality (% value)')}}
                            </div>

                            <div class = 'col-sm-4'>
                                {{Form::label('financial_group', 'Financial Condition (% value)')}}
                            </div>
                        </div>

                        <div class="row">
							<div class="col-sm-4 {{$errors->has('business_group') ? 'has-error' : ''}}">
								{{Form::text('business_group', $errors->has('business_group') ? Input::old('business_group') : $industry->business_group, array('class' => 'form-control'))}}
							</div>

							<div class="col-sm-4 {{$errors->has('management_group') ? 'has-error' : ''}}">
								{{Form::text('management_group', $errors->has('management_group') ? Input::old('management_group') : $industry->management_group, array('class' => 'form-control'))}}
							</div>

                            <div class="col-sm-4 {{$errors->has('financial_group') ? 'has-error' : ''}}">
								{{Form::text('financial_group', $errors->has('financial_group') ? Input::old('financial_group') : $industry->financial_group, array('class' => 'form-control'))}}
							</div>
						</div>
					</div>
					<div class="form-group text-center">
						{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
						<a href="{{URL::to('/')}}/bank/industry" class="btn btn-default">Back</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
@stop
