@extends('layouts.master')

@section('header')
    <link rel="stylesheet" href="{{ URL::asset('css-new/material/material-dashboard.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css-new/metronic/vendors/vendors.bundle.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css-new/metronic/base/style.bundle.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css-new/supervisor_dashboard.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ URL::asset('css-new/select/multiselect.css') }}" media="screen" />  
    <style>
        #industry_composition rect{
            height: 330px;
        }
        #industry_composition{
            height: 330px;
        }

    </style>
@parent
    <title>CreditBPO</title>
@stop
@section('content')
<div id="page-wrapper" style="margin-left:0px">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12">
                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                    @endforeach
                </div>
                @endif

                <div class="col col-md-4 col-sm-4 col-xs-12">
                    <div class="card card-chart">
                        <div class="card-header card-header-submitted-bg">
                            <div class="ct-chart" id="reportSubmittedChart"></div>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title"><span id="reports_sum">  </span> </h4>
                        </div>
                    </div>
                </div>

                <div class="col col-md-4 col-sm-4 col-xs-12">
                    <div class="card card-chart">
                        <div class="card-header card-header-average-bg">
                            <div class="ct-chart" id="avgWeeklyChart"></div>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title"> <span id="reports_avg_time">  </span> </h4>
                            <p class="card-category">  Average Time To Finish Report </p>
                        </div>
                    </div>
                </div>

                <div class="col col-md-4 col-sm-4 col-xs-12">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon" >
                            <i class="material-icons" >Client Keys</i>
                        </div>
                        <h3 class="card-title"> {{$client_keys["is_used"]}} of <span  id="count-of-client-keys">{{$client_keys["total_keys"]}}</span></h3>
                        <p class="card-category">Remaining</p>
                        </div>
                        <div class="card-body">
                            <div class="m-widget25__progress">
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--md">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: {{$client_keys['key_average']}}%;"
                                        aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-lg-12 col-sm-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header card-header-industry">
                        <div class="col-md-4 col-sm-6 col-xs-6">
                            <h3 class="card-title">Industry Composition</h3>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-6 industry_body">
                            <div class="col-md-3 coAl-sm-2 col-xs-2 tier_label">
                                <h4> Industry Tier: </h4>
                            </div> 
                            <div class="col-md-3 col-sm-12 col-xs-12 tier_multiselect">
                                {{ Form::select('tierFilter',
                                    array(
                                    '1' => 'I',
                                    '2' => 'II',
                                    '3' => 'III'
                                    ), Input::old('tierFilter'), array('id' => 'tierFilter', 'class' => 'tier_filter',  'multiple')) 
                                }}
                            </div>
                            <!-- <div class="col-md-3 col-sm-2 tier_label">
                                <h4> Region: </h4>
                            </div> 
                            <div class="col-md-3 col-sm-12 col-xs-12 region_multiselect">
                                <select id="regionFilter" name="regionFilter" class="region_filter" multiple>
                                    @for($i = 0; $i < @count($regions); $i++)
                                        <option value="{{$regions[$i]["regCode"]}}"> {{$regions[$i]["regDesc"]}}</option>
                                    @endfor
                                </select> 
                            </div> -->
                        </div>
                    </div>
                    <div id="industry_composition" class="card-body">
                        <div class="m-widget10">
                            <div class="row  align-items-center">
                                <div class="col-md-12 col-sm-8">
                                    <div id="m_chart_industry_composition">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-overview">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <h4 class="card-title">Report Overview</h4>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <a class="white" id="href-report-list" href="{{ URL::to('/') }}/bank/reportlist" target="_blank"> see details </a>
                        </div>
                    </div>
                    <div id="report_overview" class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-report">
                                <th >ID</th>
                                <th>Company</th>
                                {{-- <th>Utility Bill</th> --}}
                                <th>Report Type</th>
                                <th>Date Submitted</th>
                                <th>Rating</th>
                            </thead>
                            <tbody>
                                <!-- Show Table Result Using Loop -->
                                @foreach ($completed_reports as $report)
                                <tr>
                                    <td>{{$report->entityid}}</td>
                                    <td><a href="{{ URL::to('/') }}/summary/smesummary/{{ $report->entityid }}" target="_blank">{{$report->companyname}}</a></td>
                                    {{-- <td>{{ $report->ubill }}</td> --}}
                                    <td>{{$report->report_type}}</td>
                                    <td>{{date("m/d/Y", strtotime($report->completed_date))}}</td>
                                    <td class="{{$report->bank_score_color}}">{{$report->score_bank}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/material/plugins/material-dashboard.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/material/plugins/chartist.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/metronic/app/dashboard.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/metronic/vendors/vendors.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/metronic/base/scripts.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/metronic/jquery.mousewheel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/supervisor_dashboard.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/amchart/core.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/amchart/charts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/amchart/animated.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('static/js/modules/sdashboard/select/multiselect.min.js') }}"></script>
@stop