@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop

@section('content')
<div class="container">
	<div class="spacewrapper">
		<div class="read-only">
		<h4>Payment Selection</h4>
			<div class="read-only-text">
				<h2>Bank user subscription (1 month): Php {{$price}} = Php {{$totalAmount}}*</h2>
				<h3>Choose your preferred payment option:</h3>
				<hr />
				<div class="row">
					<div class="col-sm-6 text-center">
						<p>For Paypal and Credit Card transactions use:</p>
						<a href="{{URL::to('/')}}/bank_subscribe/checkout_paypal">
							<img src="{{URL::to('/')}}/images/paypal_option.jpg" style="margin-top: 35px;"/>
						</a>
					</div>
					<div class="col-sm-6 text-center">
						<p>For Online Banking and Over-the-counter transactions use:</p>
						<a href="{{URL::to('/')}}/bank_subscribe/checkout_dragonpay">
							<img src="{{URL::to('/')}}/images/dragonpay_option.jpg" />
						</a>
					</div>
				</div>
				<hr/>
				<h6><em>*All prices are Tax-inclusive</em></h6>


			</div>
		</div>
	</div>
</div>

@include('modal-subscription-expired');
@stop

@section('javascript')
<script type="text/javascript">
	$(function(){
		$('#modal-subscription-expired').modal('show');
	});	
</script>
@stop
