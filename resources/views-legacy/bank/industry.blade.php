@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	<div>
	{{Form::open(array('route'=>'postResetWeights', 'method'=>'post', 'class'=>'form-inline'))}}
	@if($bank_standalone==0)
		{{Form::submit('Restore Default', array('class'=>'btn btn-primary'))}}
		{{Form::button('Save Settings', array('class'=>'btn btn-primary btn-save-settings'))}}
		@if(@count($history)>0)
			<div class="form-group">
				<select id="history_select" class="form-control">
					@foreach($history as $h)
						<option value="{{$h->id}}">{{$h->name}} ({{$h->created_at}})</option>
					@endforeach
				</select>
			</div>
			{{Form::button('Load Selected Setting', array('class'=>'btn btn-primary btn-load-settings'))}}
		@endif
	@endif
	&nbsp;
	{{Form::close()}}
	<div class="clearfix"></div>
	</div>
	<div style="float: right">
		<label style="font-weight: normal;">
			<img class="preloader" style="display: none;" src="{{ URL::to('/images/preloader.gif') }}" />
			<input type="checkbox" id="fa_standalone" {{$bank_standalone==1 ? "checked='checked'" : ''}} />
			Financial Analysis Report Standalone
		</label>
	</div>
	<div class="clearfix"></div>
	<hr />
	<div class="table-responsive">
		<table id="entitydata" class="table table-striped table-bordered" cellspacing="0">
			<thead>
				<tr>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Industry ID</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Title</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Business Considerations</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Management Quality</th>
					<th style="background-color: #055688;color: #fff; font-weight: bold;">Financial Analysis</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($industry as $info)
				<tr>
					<td>{{ $info->id }}</td>
					@if($bank_standalone==1)
						<td>{{ $info->main_title }}</td>
					@else
						<td><a href="{{ URL::to('/') }}/bank/industry/edit/{{ $info->id }}">{{ $info->main_title }}</a></td>
					@endif
					<td>{{ $info->business_group }}%</td>
					<td>{{ $info->management_group }}%</td>
					<td>{{ $info->financial_group }}%</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
<div id="industry_save_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Save Setting</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" id="setting_name" class="form-control" />
				</div>
			</div>
			<div class="modal-footer">
				<span class="save_status"></span>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-setting-save">Save</button>
			</div>
		</div>
	</div>
</div>
<div id="standalone-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Information</h4>
			</div>
			<div class="modal-body">
				<p>You have set Financial Analysis Report Standalone mode "ON". Industry weight settings will be fixed. Changes in
					<a href="{{URL::to('/bank/formula')}}">Rating Formula</a>,
					<a href="{{URL::to('/bank/document_options')}}">Document Options</a> and
					<a href="{{URL::to('/bank/questionnaire_config')}}">Questionnaire Config</a> will not have any effect.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop
@section('javascript')
<script src="{{ URL::asset('/js/bank_industry_weights.js') }}"></script>
@if($bank_standalone==1)
<script>$(document).ready(function(){$('#standalone-modal').modal()});</script>
@endif
@stop
