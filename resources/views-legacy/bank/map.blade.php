@extends('layouts.master')

@section('header')
	@parent
	<title>CreditBPO</title>
@stop
@section('content')
<div  id="page-wrapper" style="margin-left:0px;">
	{{Form::open(array('route'=>'postViewMap', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'formfilter'))}}
	<div class="row filters" style="margin-bottom: 20px;">
		<div class="col-sm-6">
			<div class="form-group">
				{{Form::label('sme', 'Company Name', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					<select id="select-sme" class="form-control" name="sme[]" multiple="multiple">
						@foreach($sme_list as $key=>$value)
							@if(in_array($key, $sme_selected))
								<option value="{{$key}}" selected="selected">{{$value}}</option>
							@else
								<option value="{{$key}}">{{$value}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry1', 'Industry Tier 1', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry1', array(''=>'All') + $main_industry_list, $main_industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry2', 'Industry Tier 2', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry2', array(''=>'All') + $sub_industry_list, $sub_industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<div class="form-group">
				{{Form::label('industry3', 'Industry Tier 3', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('industry3', array(''=>'All') + $industry_list, $industry_selected, array('class'=>'form-control'))}}
				</div>
			</div>
			<!-- <div class="form-group">
				{{Form::label('region', 'Region', array('class'=>'col-sm-4'))}}
				<div class="col-sm-8">
					{{Form::select('region', array(''=>'All') + $region_list, $region_selected, array('class'=>'form-control'))}}
				</div>
			</div> -->
		</div>
		<div class="col-sm-6">
			<div class="form-group" style="display:none">
				{{Form::label('bcc', 'Business Considerations and Conditions (BCC)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('bcc_from', array(
							'' => ' - ',
							226 => 'AAA',
							211 => 'AA',
							196 => 'A',
							181 => 'BBB',
							166 => 'BB',
							151 => 'B',
							136 => 'CCC',
							121 => 'CC',
							101 => 'C',
							81 => 'D',
							0 => 'E',
						), $bcc_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('bcc_to', array(
							'' => ' - ',
							240 => 'AAA',
							225 => 'AA',
							210 => 'A',
							195 => 'BBB',
							180 => 'BB',
							165 => 'B',
							150 => 'CCC',
							135 => 'CC',
							120 => 'C',
							100 => 'D',
							80 => 'E',
						), $bcc_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('mq', 'Management Quality (MQ)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('mq_from', array(
							'' => ' - ',
							141 => 'AAA',
							131 => 'AA',
							121 => 'A',
							111 => 'BBB',
							101 => 'BB',
							91 => 'B',
							81 => 'CCC',
							71 => 'CC',
							61 => 'C',
							51 => 'D',
							0 => 'E',
						), $mq_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('mq_to', array(
							'' => ' - ',
							150 => 'AAA',
							140 => 'AA',
							130 => 'A',
							120 => 'BBB',
							110 => 'BB',
							100 => 'B',
							90 => 'CCC',
							80 => 'CC',
							70 => 'C',
							60 => 'D',
							50 => 'E',
						), $mq_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('fc', 'Financial Condition (FC)', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('fc_from', array(
							'' => ' - ',
							200 => 'AAA',
							180 => 'AA',
							160 => 'A',
							140 => 'BBB',
							120 => 'BB',
							100 => 'B',
							80 => 'CCC',
							60 => 'CC',
							40 => 'C',
							20 => 'D',
							0 => 'E',
						), $fc_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('fc_to', array(
							'' => ' - ',
							300 => 'AAA',
							199 => 'AA',
							179 => 'A',
							159 => 'BBB',
							139 => 'BB',
							119 => 'B',
							99 => 'CCC',
							79 => 'CC',
							59 => 'C',
							39 => 'D',
							19 => 'E',
						), $fc_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('rating', 'CreditBPO Rating', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('rating_from', array(
							'' => ' - ',
							177 => 'AA',
							150 => 'A',
							123 => 'BBB',
							96 => 'BB',
							68 => 'B',
							0 => 'CCC',
						), $rating_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('rating_to', array(
							'' => ' - ',
							250 => 'AA',
							176 => 'A',
							149 => 'BBB',
							122 => 'BB',
							95 => 'B',
							67 => 'CCC',
						), $rating_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none">
				{{Form::label('brating', 'Bank Rating', array('class'=>'col-sm-6'))}}
				<div class="col-sm-6 form-inline">
					<div class="form-group">
						{{Form::select('brating_from', array(
							'' => ' - ',
							177 => 'AA',
							150 => 'A',
							123 => 'BBB',
							96 => 'BB',
							68 => 'B',
							0 => 'CCC',
						), $brating_from, array('class'=>'form-control'))}}
						<label>to</label>
						{{Form::select('brating_to', array(
							'' => ' - ',
							250 => 'AA',
							176 => 'A',
							149 => 'BBB',
							122 => 'BB',
							95 => 'B',
							67 => 'CCC',
						), $brating_to, array('class'=>'form-control'))}}
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-6">
					<input type="button" value="Toggle Heatmap" id="heatmap_btn" class="btn btn-primary" />
					<input type="submit" value="Generate Map" class="btn btn-primary" />
				</div>
			</div>
		</div>
	</div>
	{{ Form::close() }}
	<div class="clearfix"></div>
	<div id="map" style="width: 100%; height: 400px;"></div>
</div>
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::to('/')}}/js/jquery.form.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="{{ URL::to('/')}}/js/bank_maps.js"></script>
@if($data)
<script type="text/javascript">
var mapData = {!! $data !!};
function getPoints() {
	var arLngLat = [];
	for(x in mapData){
		arLngLat.push(new google.maps.LatLng(parseFloat(mapData[x].latitude), parseFloat(mapData[x].longitude)))
	}
	return arLngLat;
}
function initMap() {

    if(mapData.length > 0 ) {
		var centerLatLng = {lat: parseFloat(mapData[0].latitude), lng: parseFloat(mapData[0].longitude)};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 6,
			center: centerLatLng
		});

		var heatmap = new google.maps.visualization.HeatmapLayer({
			data: getPoints(),
			radius: 20
		});

		var marker = [], infowindow = [];

		for(x in mapData){

			marker[x] = new google.maps.Marker({
				position: {lat: parseFloat(mapData[x].latitude), lng: parseFloat(mapData[x].longitude)},
				map: map,
				title: mapData[x].companyname,
				animation: google.maps.Animation.DROP,
			});

			infowindow[x] = new google.maps.InfoWindow({
				content: '<h3>' + mapData[x].companyname + '</h3>' + '<p>' + mapData[x].full_address + '</p>'
			});

			marker[x].addListener('click', function(y) {
				return function() {
				infowindow[y].open(map, marker[y]);
				}
			}(x));
		}
	}
	$('#heatmap_btn').click(function(e){
			e.preventDefault();
			if(heatmap.getMap()){
				map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
				heatmap.setMap(null);
				for(x in mapData){
					marker[x].setVisible(true);
				}
			}
			else {
				map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
				heatmap.setMap(map);
				for(x in mapData){
					marker[x].setVisible(false);
				}
			}
		});
  

}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{GOOGLE_MAP_API_KEY}}&signed_in=true&callback=initMap&v=3.exp&libraries=visualization"></script>
@endif
@stop
@section('style')
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-multiselect.css') }}" media="screen"></link>
@stop
