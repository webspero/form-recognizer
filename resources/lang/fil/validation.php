<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "Ang :attribute ay kailangang tanggapin",
	"active_url"           => "Ang :attribute ay hindi balidong URL",
	"after"                => "Ang :attribute ay kailangang maging isang petsa pagkatapos ng :date",
	"alpha"                => "Ang :attribute ay kailangang naglalaman ng mga letra lamang",
	"alpha_dash"           => "Ang :attribute ay kailangang naglalaman ng mga letra, numero at gatlang lamang",
	"alpha_num"            => "Ang :attribute ay kailangang naglalaman ng mga letra at numero lamang",
	"array"                => "Ang :attribute ay kailangang maging isang array",
	"before"               => "Ang :attribute ay kailangang maging isang petsa bago ang :date",
	"between"              => array(
		"numeric" => "Ang :attribute ay kailangang nasa pagitan ng :min at :max",
		"file"    => "Ang :attribute ay kailangang nasa pagitan ng :min at :max na mga kilobyte",
		"string"  => "Ang :attribute ay kailangang nasa pagitan ng :min at :max na mga titik",
		"array"   => "Ang :attribute ay kailangang nasa pagitan ng :min at :max na mga bagay",
	),
	"boolean"              => "Ang :attribute field ay kailangang maging tama o mali",
	"confirmed"            => "Ang kumpirmasyon ng :attribute ay hindi tumutugma",
	"date"                 => "Ang :attribute ay hindi balidong petsa",
	"date_format"          => "Ang :attribute ay hindi tumutugma sa format ng :format",
	"different"            => "Ang :attribute at ang :other ay kailangang magkaiba",
	"digits"               => "Ang :attribute ay kailangang may :digits na numero",
	"digits_between"       => "Ang :attribute ay kailangang nasa pagitan ng :min at :max na numero",
	"email"                => "Ang :attribute ay kailangang maging isang balidong email address",
	"exists"               => "Ang napiling :attribute ay hindi balido",
	"image"                => "Ang :attribute ay kailangang maging isang imahe",
	"in"                   => "Ang napiling :attribute ay hindi balido",
	"integer"              => "Ang :attribute ay kailangang maging isang buong numero",
	"ip"                   => "Ang :attribute ay kailangang maging isang balidong IP address",
	"max"                  => array(
		"numeric" => "Ang :attribute ay hindi maaaring humigit sa :max",
		"file"    => "Ang :attribute ay hindi maaaring humigit sa :max na mga kilobyte",
		"string"  => "Ang :attribute ay hindi maaaring humigit sa :max na mga titik",
		"array"   => "Ang :attribute ay hindi maaaring humigit sa :max na mga bagay",
	),
	"mimes"                => "Ang :attribute ay kailangang maging isang file na kauri ng: :values",
	"min"                  => array(
		"numeric" => "Ang :attribute ay kailangang di bumaba sa :min.",
		"file"    => "Ang :attribute ay kailangang di bumaba sa :min na mga kilobyte",
		"string"  => "Ang :attribute ay kailangang di bumaba sa :min na mga titik",
		"array"   => "Ang :attribute ay kailangang di bumaba sa :min na mga bagay",
	),
	"not_in"               => "Ang napiling :attribute ay hindi balido",
	"numeric"              => "Ang :attribute ay kailangang maging isang numero - huwag gumamit ng anumang simbulo ($, %, #, atbp)",
	"regex"                => "Ang format ng :attribute ay hindi balido",
	"required"             => "Ang :attribute field ay kailangan",
	"required_if"          => "Ang :attribute field ay kailangan kung ang :other ay :value",
	"required_with"        => "Ang :attribute field ay kailangan kung may :values",
	"required_with_all"    => "Ang :attribute field ay kailangan kung may :values",
	"required_without"     => "Ang :attribute field ay kailangan kung walang :values",
	"required_without_all" => "Ang :attribute field ay kailangan kung wala ang alinman sa mga :values",
	"same"                 => "Ang :attribute at ang :other ay kailangang tumugma",
	"size"                 => array(
		"numeric" => "Ang :attribute ay kailangang maging isang :size",
		"file"    => "Ang :attribute ay kailangang maging :size na mga kilobyte",
		"string"  => "Ang :attribute ay kailangang magkaroon ng :size na mga titik",
		"array"   => "Ang :attribute ay kailangang maglaman ng :size na mga bagay",
	),
	"unique"               => "Ang :attribute ay nakuha na",
	"url"                  => "Ang format ng :attribute ay hindi balido",
	"timezone"             => "Ang :attribute ay kailangang maging isang balidong sona",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
