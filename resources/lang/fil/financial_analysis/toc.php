<?php

return array(
    '1_financial_position_analysis' => '1 :financial_report_title\'s Pagsusuri sa Posisyon ng Pananalapi',
    '1_1_structure_of_the_assets'   => '1.1. Balangkas ng Pag-aari at Pagkakautang',
    '1_2_net_assets'     			=> '1.2. Linis na Pag-aari (Netong Halaga)',
    '1_3_financial_sustainability'  => '1.3. Pagsusuri ng Pagpapanatiling Pinansyal',
    '1_3_1_key_ratios'     			=> '1.3.1. Pinakamahahalagang mga Tumbasan sa Pagpapanatiling Pinansyal ng Kompanya',
    '1_3_2_working_capital'  		=> '1.3.2. Pagsusuri ng Nagpapatakbong Kapital',
    '2_financial_performance' 		=> '2. Pananalaping Pagsasakatuparan',
    '2_1_overview_of_the_financial'	=> '2.1. Kabuuang Ideya ng Bungang Pananalapi',
    '2_2_profitability_ratios'     	=> '2.2. Tumbasan ng Kakayahang Kumita',
    '2_3_analysis'     				=> '2.3. Pagsusuri sa mga Aktibidad sa Negosyo (Tumbasan ng Pagbalik ng Puhunan)',
    '3_conclusion'     				=> '3. Wakas',
    '3_1_key_ratios_summary' 		=> '3.1. Kabuuran ng mga Pinakamahahalagang Tumbasan'
   
);
