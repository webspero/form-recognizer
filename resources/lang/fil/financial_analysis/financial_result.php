<?php
 
return array(
    
    '21_overview_of_the_financial'      => '2.1 Pangkalahatang Ideya sa Resultang Pampinansyal',
    'indicator'                         => 'Tagapagpahiwatig',
    'value_php'                         => 'Halaga, PHP',
    'change'                            => 'Baguhin',
    'average_annual_value'              => 'Taunang karaniwang halaga, PHP',
    '1_revenue'                         => '1. Kita',
    '2_cost_of_sales'                   => '2. Gastos sa pagbenta',
    '3_gross_profit'                    => '3.<i>Gross profit</i> (1-2)',
    '4_other_income_and_expenses'       => '4. Ibang kita at gastos, bukod sa gastos pampinansyal',
    '6_finance_costs'                   => '6. Gastos pampinansyal',
    '7_income_tax_expense'              => '7. Gastos sa buwis (mula sa pagtuloy ng operasyon)',
    '8_profit_loss'                     => '8. Kita (lugi) mula sa pagtuloy ng operasyon (5-6-7)',
    '9_profit_loss'                     => '9. Kita (lugi) mula sa tinigil ng operasyon',
    '10_profit_loss'                    => '10. Kita (lugi) (8+9)',
    '11_other_comprehensive_income'     => '11. Ibang komprehensibong kita',
    '12_comprehensive_income'           => '12. Komprehensibong kita (10+11)',
    'the_revenue_equaled'               => 'Ang kita ay tumumbas <span style="color::revenue1Class" > :revenue </span> sa nakalipas na taon,  habang ang kita ay higit na bumaba at tumumbas sa PHP <span style="color::revenue2Class;">:revenue2</span> para sa taon :year1 (hal. Ito ay lumago ng PHP <span style="color::revenue1DiffClass" > :incomeStatementDif </span>). Ang pag-iba ng kita ay ipinakita sa tsart. Ang kabuuang kita ay katumbas sa PHP <span style="color::grossProfit" > :grossProfitVal </span>. Sa nakalipas na taon. Sa pagkumpara sa nakalipas na taon sa parehas na panahon bago ang pinansyal na taon, mapapansin na mayroon makabuluhang pag angat sa kabuuang kita (PHP <span style="color::grossProfit2" > :grossProfitVal2  </span>).',
    'for_the_last_year'                 => 'Para sa nakalipas na taon, ang kumpanya ay nagpaskil ng kabuuang kita at kita bago interes at buwis (EBIT), kung saan <span style="color::profitLossClass" > :profitLoss </span>. Ang komprehensibong kita ng :financial_report_title ay PHP <span style="color::comprehensiveClass" > :comprehensiveIncome </span> sa kabuuang panahon mula 01/01/:year hanggang 12/31/:year.'

);
