<?php
 
return array(
    'financial_condition_analysis'     => ':financial_report_title\'s Pagsusuri sa Kalagayang Pampinansyal para sa panahong (01/01/:startYear) hanggang (12/31/:endYear)',
    'financial_position_analysis'      => '1. :financial_report_title\'s Pagsusuri sa Posisyong Pampinansyal',
    'the_following_report_analyzes'    => 'Ang mga sumusunod na ulat ay sumusuri ng :financial_report_title kalagayang pinansyal base sa mga datos pampinansyal na hinayag ayon sa International Financial Reporting Standards (IFRS) mula 01/01/:startYear hanggang 12/31/:endYear.',
    '2_financial_performance'          => '2. Pinansyal na Pagganap'    
);
