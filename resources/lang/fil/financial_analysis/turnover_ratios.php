<?php

return array(
    '2_3_analysis_of_the_business'  => '2.3 Pagsusuri sa mga Aktibidad sa Negosyo (Turnover Ratios)',
    'further_in_the_table'          => 'Makikita rin sa talahanayan ang mga nakalkulang tumbasan ng pagbalik ng puhunan ng ari-arian at kakulangan at paano nito inilalarawan kung gaano kabilis naisasakatuparan ang bayad ng ari-arian at kakulangang panghinaharap sa mga tumutustos, kontratista, at manggagawa. Ito ang rason kung bakit ang ganap na halaga ng mga tumbasan ay hindi maaaring gawan ng pagtatasang nabibilang. Kapag sinuri ang tumbasan ng pagbalik ng puhunan ng ari-arian, ang pagtaas ng tumbasan (hal. tulin ng pag-ikot) at pagbawas sa araw ng pag-ikot ay itinuturing na mga magandang katangian. Walang malinaw na interaksyon para sa pagkakautang sa kalakalan at pagbalik puhunan ng kapital. Kung tutuusin, ang isang tamang konklusyon ay maaari lamang makamit matapos maisaalangalang ang mga rason na nagsanhi ng mga pagbabagong ito.',
    'turnover_ratio'                => 'Turnover Ratio',
    'value_days'                    => 'Halaga, Mga Araw',
    'ratio_year1'                   => 'Tumbas :year1',
    'ratio_year2'                   => 'Tumbas :year2',
    'change'                        => 'Palitan, mga araw',
    'receivables_turnover'          => 'Pagbalik ng mga matatanggap (bilang ng araw para sa natitirang mga benta) <br> <small>(pamantayan ng kalakal at ibang pangkasalukuyang mga matatanggap bahaginin ng pamantayan ng kitang arawan*)</small>',
    'accounts_payable_turnover'     => 'Pagbalik ng Pagkakautang sa Kalakalan (bilang ng araw para sa natitirang bayarin) <br><small>(pamantayan ng pagkakautang sa kasalukuyan  bahaginin ng pamantayan ng bilihing pang-arawan)</small>',
    'inventory_turnover'            => 'Pagbalik ng imbentaryo (bilang ng araw sa mga natitirang imbentaryo) <br><small>(pamantayan ng imbentaryo bahaginin ng pamantayan ng pamantayang pang-arawang gastos ng mga benta)</small>',
    'asset_turnover'                => 'Pagbalik ng ari-arian <br><small>(pamantayang kabuuang ari-arian bahaginin ng  pamantayang arawang kita)</small>',
    'current_asset_turnover'        => 'Pagbalik ng ari-ariang pangkasalukuyan <br><small>(pamantayang ari-ariang pangkasalukyan bahaginin ng pamantayang arawang kita)</small>',
    'capital_turnover'              => 'Pagbalik ng kapital <br><small>(pamantayang katwiran bahaginin ng pamantayang arawang kita)</small>',
    'reference'                     => '<i>Reprerensiya:</i> <br>Hakbangin sa pagpalit ng pera <br><small>(bilang ng araw para sa natitirang mga benta + bilang ng araw para sa natitirang imbentaryo – bilang ng araw para sa mga natitiang bayarin) </small>',
    'during_the_last_year'          => 'Noong nakalipas na taon, ang pamantayang pahanon ng pangongolekta (Bilang ng mga Araw para sa Natitirang mga Benta) ay :receivablesTurnover mga araw at ang pamantayang araw para sa mga natitirang bayarin ay :receivablesTurnover mga araw ay ipinakita sa talahanayan. Ang bilis ng pagbalik ng ari-arian any nangangahulugang :financialReportTitle mga nadagdag sa kita ay kapantay ng kabuuan ng lahat ng mga magagamit na ari-arian kada 1298 na araw (sa pamantayan tuwing panahon ng pagsusuri).'
    
);
