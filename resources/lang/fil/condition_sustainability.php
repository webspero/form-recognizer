<?php

return array(
    'condition_sustainability'  => 'Kondisyon at Pagpapanatili',
    
    /*--------------------------------------------------------------------
    /*	Sucession
    /*------------------------------------------------------------------*/
	'succession' 				=> 'Pamamahala-sunod (Company Head Succession)',
	'succession_explain'    	=> 'Balak mong mapalitan sa negosyong ito ni:',
	'plan'    	                => 'Plano',
	'choose_plan'    	        => 'Piliin ang Plano',
	'time_frame'    	        => 'Itinatakdang Panahon',
    'succession_time'           => 'Inaasahan mong mapalitan bilang tagapamahala sa itinakdang panahong ito',
    'succession_plan'           => 'Plano sa Pamamahala-sunod (Management Succession Plan)',
    'mngmt_relative'            => 'Asawa o mga anak na nasa edad 25-55 taong gulang at aktibo sa pamamahala ng negosyo nang di bababa sa 5 na taon.',
    'trusted_employee'          => 'Pinagkakatiwalaang empleyado, Malayong Kamag-anak, Hindi kapamilya at aktibo sa pamamahala ng negosyo nang di bababa sa 5 na taon.',
    'none_less_five'            => 'Wala sa pinagpipilian, Mas mababa sa 5 na taon',
    'succession_plan_none'      => 'Walang malinaw na Plano sa Pagpapalit ng Kasalukuyang Tagapangasiwa',
    'succesion_time_frame'      => 'Itinakdang Panahon (ng Pamamahala-sunod o Management Succession)',
    'choose_succession_time'    => 'Piliin ang iyong Itinakdang Panahon',
    'succession_within_five'    => 'Sa Loob ng 5 na Taon',
    'succession_over_five'      => 'Higit sa 5 na Taon',
    
    /*--------------------------------------------------------------------
    /*	Competitive Landscape in Industry
    /*------------------------------------------------------------------*/
    'competitive_landscape'     => 'Mga Katangian ng Kompetensya sa iyong Industriya',
    'landscape_competitive'     => 'Uri ng Kompetisyon (Kompetensya sa Industriya)',
    'landscape'                 => 'Uri ng Kompetisyon',
    'identify_competition'      => "Sa loob ng iyong industriya, kilalahin ang uri ng kompetisyon. Kapag higit sa 60% ng merkado ay gumagamit na ng  produkto o serbisyo na nagmumula sa iba't-ibang nagbebenta, ito ay tinatawag na 'High Market Penetration Rate'",
    'choose_landscape'          => 'Piliin ang Uri ng Kompetisyon sa iyong Industriya',
    'market_pioneer'            => 'Isa Sa Mga Nagsimula ng Merkado / Nangunguna',
    'limited_market_entry'      => 'Limitado Ang Pagpasok sa Merkado',
    'high_market_penetration'   => "Mataas Ang Market Penetration Rate (Higit sa 60% ng merkado ang gumagamit ng produkto o serbisyo na binebenta ng iba't ibang kumpanya)",
    
    /*--------------------------------------------------------------------
    /*	Important Economic Factors
    /*------------------------------------------------------------------*/
    'economic_factor'           => 'Mga Importanteng Kadahilanang Ekonomiko Nakakaapekto ng inyong negosyo',
    'rate_eco_factor'           => 'I-rate mo ang epekto ng mga sumusunod na dahilang ekonomiko na matinding nakakaapekto ng itong negosyo',
    'recession_susceptibility'  => 'Madaling tablan ng pagbaba ng ekonomiya',
    'forex_sensitivity'         => 'Sensitibo sa dayuhang pagpapalit (foreign exchange rate) at interes (interest rate)',
    'price_volatility'          => 'Paggalaw ng presyo ng mga kailanganin o commodities (hal. Krudo, asukal)',
    'government_regulation'     => 'Regulasyon ng Gobyerno (hal. Buwis, taripa)',
    
    /*--------------------------------------------------------------------
    /*	Tax Payments
    /*------------------------------------------------------------------*/
    'tax_payments'              => 'Mga Pagbabayad ng Buwis',
    'taxes_ontime'              => 'Kasalukuyan: Ang mga buwis ay naisaayos sa takdang petsa',
    'taxes_delayed'             => 'Naisaayos, May Mga Nahuhuli ang Bayad: Ang mga buwis ay naisaayos ng lampas sa takdang petsa. Nabayaran ang mga multa.',
    'taxes_deliquent'           => 'Delingkwente (Di Nagbabayad): Hindi pa naisaayos ang mga buwis. Lumampas na ang takdang petsa.',
    'taxes_current'             => 'Walang Nahuhuling Bayad sa Buwis',
    'taxes_arrears'             => "Naisaayos Bagama't May Mga Atraso",
    'taxes_bad'                 => "Delingkwente (Di Nagbabayad)",
    
    /*--------------------------------------------------------------------
    /*	Risk Assessment
    /*------------------------------------------------------------------*/
    'risk_assessment'           => 'Pagtatansya ng Panganib (Risk Assessment)',
    'major_risks'               => 'Ang mga pinakamalaking panganib na hinaharap ng iyong kumpanya at ang iyong mga plano para matugunan ito. Isaayos mula sa pinakamapanganib at pababa.',
    'buying_power'              => 'Kapangyarihan ng Mamimili (Buyer Power): Ang kapangyarihan ng iyong mga kostumer na pababain ang iyong mga presyo.',
    'supplying_power'           => 'Kapangyarihan ng Tagatustos (Supplier Power): Ang kapangyarihan ng mga supplier na pataasin ang presyo ng iyong mga raw materials na ginagamit sa paggawa ng iyong produkto o serbisyo',
    'competetive_rivalry'       => 'Kompetisyon: Ang pagkatindi ng kompetisyon sa industriya.',
    'substitution_threat'       => 'Ang Banta ng Pagpalit (Thread to Substitution): Ang posibilidad na maaaring gamitin ang ibang mga produkto at serbisyo kapalit ng iyong sariling produkto o serbisyo.',
    'new_entry_threat'          => 'Ang Banta ng Bagong Pasok (Thread of New Entry): Gaano kadaling pumasok ang bagong kompetisyon sa merkado kung makita nila na maganda ang kita sa industriyang iyon (at pagkatapos ay pabababain ang iyong presyo)',
    'threat_1'                  => 'Banta #1',
    'counter_measure_1'         => 'Ang Uri ng Pagtugon o Maaring Gawin sa Pagharap sa Banta #1',
    'threat_2'                  => 'Banta #2',
    'counter_measure_2'         => 'Ang Uri ng Pagtugon o Maaring Gawin sa Pagharap sa Banta #2',
    'threat_3'                  => 'Banta #3',
    'counter_measure_3'         => 'Ang Uri ng Pagtugon o Maaring Gawin sa Pagharap sa Banta #3',
    'threat'                    => 'Banta',
    'threat_of_new_entrants'    => 'Banta ng mga bagong negosyo (Threat of New Entrants)',
    'threat_of_substitute'      => 'Banta ng mga panghaliling produkto o serbiyo (Threat of Substitute Products or Services)',
    'buyer_bargainer_pow'       => 'Kapangyarihan sa pangangalakal ng mga mamimili (Bargaining Power of Buyers)',
    'supplier_bargainer_pow'    => 'Kapangyarihan sa pangangalakal ng mga tagatustos (Bargaining Power of Suppliers)',
    'rivalry_intensity'         => 'Tindi ng Kumpetisyon',
    'counter_measures'          => 'Mga Maaaring Gawin sa Pagharap sa Banta',
    'cost_leadership'           => 'Pamumuno sa Pagpepresyo: Palakihin ang kita sa pamamagitan ng pagpapababa ng mga gastusin habang sinisingil ang karaniwang presyo sa industriya, o palakihin ang inyong bahagi sa merkado (o market share) sa pamamagitan ng pagbababa ng presyo ng inyong produkto o serbisyo habang pinapanatili ang mga kita.',
    'product_differentiation'   => 'Pagdadag ng Iba Pang Mga Katangian ng Produkto o di kaya ay Bagong Ideya para sa Produkto (Product Differentiation / Innovation): Gawing sadyang naiiba ang mga produkto ng iyong kumpanya kaysa kompetisyon, pagpapabuti ng iyong produkto at kahalagahan sa publiko. Ang istratehiyang ito ay parehong nangangailangan ng masuring pananaliksik at pagbubuo at epektibong mga tagapagbenta at pagmamarkado.',
    'focus_market'              => 'Pagtutok: Piliin ang mga naaangkop na merkado upang pagbentahan ng iyong mga kalakal. Ang istratehiyang ito ay nangangailangan ng malalim na pag-unawa ng merkado, kaalaman tungkol sa mga nagbebenta rito, mga mamimili at mga kompetisyon. Ang paggamit ng istratehiyang ito ay madalas nangangailangan din ng pamumuno sa pagpepresyo (cost leadership) o pag-iiba ng katangian ng produkto/ serbisyo (differentiation position).',
    'cost_leadership_sel'       => 'Pamumuno sa Pagpepresyo (Cost Leadership)',
    'product_innovation'        => 'Pag-iiba ng Produkto/Bagong Ideya',
    'focus'                     => 'Pagtutok o Focus',
    
    /*--------------------------------------------------------------------
    /*	Revenue Growth Potential within next 3 years 
    /*------------------------------------------------------------------*/
    'revenue_growth'            => 'Potensyal na Paglaki ng Kita sa loob ng 3 na taon',
    'expected_revenue_3years'   => 'Sa susunod na 3 taon, ang iyong inaasahang paglaki ng benta sa iyong mga kasalukuyan at bagong merkado (kung mayroon), ay:',
    'under_current_market'      => 'Sa Kasalukuyang Mga Kondisyon ng Merkado',
    'expected_future_market'    => 'Inaasahang Kondisyon ng Merkado sa Hinaharap',
    'less_3_percent'            => 'Mas mababa sa 3%',
    'between_3to5_percent'      => 'Sa pagitan ng 3% at 5%',
    'greater_5_percent'         => 'Higit sa 5%',
    
    /*--------------------------------------------------------------------
    /*	Capital Investment for Expansion
    /*------------------------------------------------------------------*/
    'capital_expansion'         => 'Pamumuhunan ng Kapital para sa Pagpapalaki ng Negosyo',
    'future_growth_relation'    => 'Nauukol sa iyong mga inisyatibo para sa pagpapalaki na inilagay sa Mga Detalye ng Negosyo,  sa susunod na 12 hanggang 24 na mga buwan, ikaw bilang may-ari ay mamumuhunan ng ganitong mga halaga (o kapital) sa iyong negosyo',
    'within_12_mons'            => 'Sa loob ng susunod na 12 na buwan',
    'within_24_mons'            => 'Sa loob ng susunod na 2 na taon',
);