<?php

return array(
	'ecf' => 'Mga Kasalukuyang Pasilidad ng Kredito (Credit Line)',
	'ecf_hint' => 'Ang pasilidad ng kredito na mayrooon ka ngayon ay:',
	
	'credit_facilities'         => 'Mga Pasilidad ng Kredito (Credit Line)',
	'credit_facilities_title'   => '(Mga) Pasilidad ng Kredito (Credit Line)',
	'lending_institution'       => 'Nagpapahiram na Institusyon',
	'amount_of_line'            => 'Halaga ng Linya',
	'purpose_of_cf'             => 'Layunin ng Pasilidad ng Kredito',
	'purpose_of_cf_others'      => 'Layunin ng Iba Pang Pasilidad ng Kredito (kung wala sa mapagpiliang layunin)',
    'credit_terms'              => 'Credit Terms (e.g. 30 days)',
	'outstanding_balance'       => 'Outstanding Balance',
	'experience'                => 'Experience',
	'other_remarks'             => 'Other Remarks',
	
	'working_capital'           => 'Gagamiting Puhunan',
	'expansion'                 => 'Pagpapalaki',
	'takeout'                   => 'Pagbayad sa mga umiiral na obligasyon',
    'supplier_credit'           => "Suppliers' Credit",
	'equipment_purchase'        => 'Equipment Purchase',
	'guarantee'                 => 'Guarantee',
	'other_specify'             => 'Iba pa (isa-isahin)',
    
    'banks'                     => 'Banks',
	'suppliers'                 => 'Suppliers',
	'guarantors'                => 'Guarantors',
	'other_lenders'             => 'Other Lenders',
	
	'existing_collateral' => 'Kasalukuyang Panggarantiya (Collateral)',
	'existing_collateral_hint' => 'Garantiya na iyong inalok upang makuha ang pasilidad ng kredito. Gamitin ang kasalukuyang halaga ng mga ari-arian ng puhunan, tinasang halaga para sa iba pa.',
	'type_of_deposit' => 'Uri ng Deposit Account (CA, SA)',
	'deposit_amount' => 'Halaga ng Deposito',
	'marketable_securities' => 'Mga Mabebentang Papel (stocks, bonds)',
	'marketable_securities_value' => 'Tinatayang Halaga ng Mga Mabebentang Papel',
	'property' => 'Ari-arian (lupain)',
	'property_value' => 'Tinatayang Halaga ng Ari-arian',
	'chattel' => 'Kagamitan (Chattel)',
	'chattel_hint' => 'Nagagalaw na personal na ari-arian, hal. mga kinakabit, kagamitan, makinarya',
	'chattel_value' => 'Tinatayang Halaga ng Kagamitan',
	'others' => 'Iba Pa',
	'others_value' => 'Tinatayang Halaga ng Iba Pa',
);