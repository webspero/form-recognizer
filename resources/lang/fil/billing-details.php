<?php

return array(
    'billing_details'           => 'Mga Detalye ng Pagsingil',
    'province'                  => 'Probinsya',
    'city'                      => 'Lungsod',
    'street_address'            => 'Kalye',
    'zipcode'                   => 'Zipcode',
    'phone'                     => 'Numero ng Telepono',
    'save_details'              => 'I-save ang mga detalye',
);
