<?php

return array(
    'condition_sustainability'  => 'Condition and Sustainability',
    
    /*--------------------------------------------------------------------
    /*	Sucession
    /*------------------------------------------------------------------*/
	'succession' 				=> 'Company Head Succession',
	'succession_explain'    	=> 'You intend to be succeeded in this business by:',
	'plan'    	                => 'Plan',
    'choose_plan'               => 'Choose your Plan',
	'time_frame'    	        => 'Time Frame',
    'succession_time'           => 'You expect to be succeeded in this time frame',
    'succession_plan'           => 'Succession Plan',
    'mngmt_relative'            => 'Spouse or Children aged 25-55 yrs & active in mgmt of the business for at least 5 years.',
    'trusted_employee'          => 'Trusted employee, Distant Relative, Non-family member & active in mgmt of the business for at least 5 years.',
    'none_less_five'            => 'None of the Above, Less than 5 years',
    'succession_plan_none'      => 'No clear Succession Plan',
    'succesion_time_frame'      => 'Time Frame',
    'choose_succession_time'    => 'Choose your Time Frame',
    'succession_within_five'    => 'Within 5 Years',
    'succession_over_five'      => 'Over 5 Years',
    
    /*--------------------------------------------------------------------
    /*	Competitive Landscape in Industry
    /*------------------------------------------------------------------*/
    'competitive_landscape'     => 'Competitive Landscape in your Industry',
    'landscape_competitive'     => 'Landscape (Competitive)',
    'landscape'                 => 'Landscape',
    'identify_competition'      => "Within your industry, identify the nature of the competition. High Market Penetration rate suggests over 60% of the market have adopted the product or service from various sellers.",
    'choose_landscape'          => 'Choose your Landscape',
    'market_pioneer'            => 'Market Pioneer / Dominant',
    'limited_market_entry'      => 'Limited Market Entry',
    'high_market_penetration'   => "High Market Penetration Rate",
    
    /*--------------------------------------------------------------------
    /*	Important Economic Factors
    /*------------------------------------------------------------------*/
    'economic_factor'           => 'Important Economic Factors',
    'rate_eco_factor'           => 'With this business in mind, you rate the impact of these important economic factors as follows:',
    'recession_susceptibility'  => 'Susceptibility to economic recession',
    'forex_sensitivity'         => 'Foreign exchange & interest sensitivity',
    'price_volatility'          => 'Commodity price volatility (e.g. Oil)',
    'government_regulation'     => 'Governement regulation (e.g. Tax, tariff)',
    
    /*--------------------------------------------------------------------
    /*	Tax Payments
    /*------------------------------------------------------------------*/
    'tax_payments'              => 'Tax Payments',
    'taxes_ontime'              => 'Current: Taxes filed on time. ',
    'taxes_delayed'             => 'Filed With Arrears: Taxes filed beyond due date. Penalties paid.',
    'taxes_deliquent'           => 'Delinquent: Taxes not yet filed. Due date already passed.',
    'taxes_current'             => 'Current',
    'taxes_arrears'             => "Filed With Arrears",
    'taxes_bad'                 => "Delinquent",
    
    /*--------------------------------------------------------------------
    /*	Risk Assessment
    /*------------------------------------------------------------------*/
    'risk_assessment'           => 'Risk Assessment',
    'major_risks'               => 'The major risks your company faces and your plan to address them. Ranked from most to least threatening.',
    'buying_power'              => 'Buyer Power: The power of your customers to drive down your prices.',
    'supplying_power'           => 'Supplier Power: The power of suppliers to drive up the prices of your inputs.',
    'competetive_rivalry'       => 'Competitive Rivalry: The strength of competition in the industry.',
    'substitution_threat'       => 'The Threat of Substitution: The extent to which different products and services can be used in place of your own.',
    'new_entry_threat'          => 'The Threat of New Entry: The ease with which new competitors can enter the market if they see that you are making good profits (and then drive your prices down)',
    'threat_1'                  => 'Threat #1',
    'counter_measure_1'         => 'Counter-Measures #1',
    'threat_2'                  => 'Threat #2',
    'counter_measure_2'         => 'Counter-Measures #2',
    'threat_3'                  => 'Threat #3',
    'counter_measure_3'         => 'Counter-Measures #3',
    'threat'                    => 'Threat',
    'threat_of_new_entrants'    => 'Threat of new entrants',
    'threat_of_substitute'      => 'Threat of substitute products or services',
    'buyer_bargainer_pow'       => 'Bargaining power of buyers',
    'supplier_bargainer_pow'    => 'Bargaining power of suppliers',
    'rivalry_intensity'         => 'Intensity of competitive rivalry',
    'counter_measures'          => 'Counter-Measures',
    'cost_leadership'           => 'Cost Leadership: Increase profits by reducing costs while charging industry-standard prices, or increase market share by reducing the sales price while retaining profits.',
    'product_differentiation'   => "Product Differentiation/Innovation: Make your company's products significantly different from the competition, improving product competitiveness and value to the public. This strategy requires both good research and development and effective sales and marketing teams.",
    'focus_market'              => 'Focus: Select niche markets in which to sell your goods. This strategy requires intense understanding of the marketplace, its sellers, buyers and competitors. The use of this strategy frequently requires the companies to also implement a cost leadership or differentiation position.',
    'cost_leadership_sel'       => 'Cost Leadership',
    'product_innovation'        => 'Product Differentiation/Innovation',
    'focus'                     => 'Focus',
    
    /*--------------------------------------------------------------------
    /*	Revenue Growth Potential within next 3 years 
    /*------------------------------------------------------------------*/
    'revenue_growth'            => 'Revenue Growth Potential within next 3 years',
    'expected_revenue_3years'   => 'Over the next 3 years, your expected revenue growth in your current and new markets (if applicable), are:',
    'under_current_market'      => 'Under Current Market Conditions',
    'expected_future_market'    => 'Expected Future Market Conditions',
    'less_3_percent'            => 'Less than 3%',
    'between_3to5_percent'      => 'Between 3% to 5%',
    'greater_5_percent'         => 'Greater than 5%',
    
    /*--------------------------------------------------------------------
    /*	Capital Investment for Expansion
    /*------------------------------------------------------------------*/
    'capital_expansion'         => 'Capital Investment for Expansion',
    'future_growth_relation'    => 'Relating to your future growth initiatives in Business Details, over the next 12 to 24 months, you as owner will invest these amounts into your business ',
    'within_12_mons'            => 'Within next 12 months',
    'within_24_mons'            => 'Within next 2 years',
);