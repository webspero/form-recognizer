
<?php

return [
    'balance_sheet_required' => 'Uploaded balance sheet file has no Balance Sheet Entry.',
    'balance_sheet_required_year_entry'  => 'Uploaded balance sheet file should be by year entry.',
    'balance_sheet_atleast' => 'We need 3 years of Balance Sheet data at minimum. Please regenerate QuickBook files and try uploading again.',
    'income_sheet_required_year_entry'  => 'Uploaded income sheet file should be by year entry.',
    'income_statement_atleast' => 'We need 3 years of Income Statement data at minimum. Please regenerate QuickBook files and try uploading again.',
    'income_sheet_required' => 'Uploaded income statement file has no Profit and Loss Entry.',
    'cashflow_sheet_required_year_entry'  => 'Uploaded cashflow sheet file should be by year entry.',
    'cashflow_statement_atleast' => 'We need 3 years of Statement of Cashflows data at minimum. Please regenerate QuickBook files and try uploading again.',
    'cashflow_sheet_required' => 'Uploaded cashflow file has no Statement of Cashflows.',
];