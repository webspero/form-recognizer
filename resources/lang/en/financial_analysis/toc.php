<?php

return array(
    '1_financial_position_analysis' => '1. :financial_report_title\'s Financial Position Analysis',
    '1_1_structure_of_the_assets'   => '1.1. Structure of the Assets and Liabilities',
    '1_2_net_assets'     			=> '1.2. Net Assets (Net Worth)',
    '1_3_financial_sustainability'  => '1.3. Financial Sustainability Analysis',
    '1_3_1_key_ratios'     			=> '1.3.1. Key ratios of the company\'s financial sustainability',
    '1_3_2_working_capital'  		=> '1.3.2. Working capital analysis',
    '2_financial_performance' 		=> '2. Financial Performance',
    '2_1_overview_of_the_financial'	=> '2.1. Overview of the Financial Results',
    '2_2_profitability_ratios'     	=> '2.2. Profitability Ratios',
    '2_3_analysis'     				=> '2.3. Analysis of the Business Activity (Turnover Ratios)',
    '3_conclusion'     				=> '3. Conclusion',
    '3_1_key_ratios_summary' 		=> '3.1. Key Ratios Summary'
   
);
