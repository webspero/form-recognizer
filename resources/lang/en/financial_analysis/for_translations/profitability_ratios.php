<?php

return array(
    '2_2_profitability_ratios'     => '2.2 Profitability Ratios',
    'profitability_ratios'         => 'Profitability Ratios',
    'value_in'                     => 'Value in %',
    'change_col'                   => 'Change (col.:countBalanceSheets - col.2)',
    '1_gross_margin'               => '1. Gross margin.',
    '2_return_on_sales'            => '2. Return on sales (operating margin).',
    '3_profit_margin'              => '3. Profit margin.',
    'reference_interest_coverage'  => '>Reference:<br />Interest coverage ratio (ICR). Acceptable value: no less than 1.5.',
    'profitTextPA1'             => 'The profitability ratios given in the table have positive values as a result of the profitability of :financialReportTitle\'s activities for the last year. During the year :year, the gross margin was <span class=":finalGrossMarginClass">:finalGrossMargin%</span>, but the gross margin amounted to <span class=":finalGrossMarginClass">:finalGrossMargin%</span> ( the rate decreased by <span class=":change1Class">:change1%</span>).',
    'profitTextPA2'             => 'All three profitability ratios given in the table have positive values for the last year, as the company gained gross profit and comprehensive income from operational and financial activity for this period. The gross margin equaled <span class=":finalGrossMarginClass">:finalGrossMargin%</span> during the last year. For the last year in comparison with the same period of the priorfinancial year, the gross margin significantly decreased (by <span class=":change1Class">:change1%</span>).',
    'profitTextPA3'             => 'The profitability ratios given in the table have positive values as a result of the profitability of :financialReportTitle\'s activities during the last year. For the last year in comparisonwith the same period of the prior financial year, it was observed that there was a moderate growth in the gross margin from <span class=":startGrossMarginClass">'.$startGrossMargin.'%</span> to <span class=":finalGrossMarginClass">:finalGrossMargin%</span> (<span class=":change1Class">+:change1%</span>)',
    'profitTextPA4'             => 'The profitability ratios given in the table have positive values as a result of the profitability of '.$financial_report->title.'\'s activities during the last year. The gross margin amounted to <span class=":startGrossMarginClass">:finalGrossMargin%</span> during the year :year. An alteration in the gross margin of <span class=":change1Class">+:change1%</span> for the year :year in comparison withthe same period of the prior financial year.',
    'profitTextPA5'             => 'The profitability ratios given in the table have positive values as a result of the profitability of :financialReportTitle\'s activities during the year 2017. During the period 01.01–12/31/:year,the gross margin was <span class=":startGrossMarginClass">:finalGrossMargin%</span>. The gross margin went up significantly (by <span class=":change1Class">:change1%</span>) during the whole periodreviewed.',
    'profitTextPA6'             => 'The profitability ratios given in the table have positive values as a result of the profitability of :financialReportTitle\'s activities for the last year. The gross margin went down somewhat to :finalGrossMargin% during the entire period reviewed.',
    'profitTextPB1'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzes point of view. The return on sales was :finalROSPercentage or :finalROS% per annum for the period 01.01–12/31/:year, while the profit margin was :finalProfitMargin% per annum.',
    'profitTextPB2'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzes point of view. For the period 01.01–12/31/:year, the return on sales was :finalROSPercentage or :finalROS% , and profitability calculated by net profit was :finalProfitMargin% per annum.',
    'profitTextPB3'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzespoint of view. For the period 01.01–12/31/:year, the return on sales was :finalROSPercentage or :finalROS% , and profitability calculated by net profit was :finalProfitMargin% per annum',
    'profitTextPB4'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzes point of view. For the period 01.01–12/31/:year, the return on sales was :finalROSPercentage or :finalROS% , and profitability calculated by net profit was :finalProfitMargin% per annum.',
    'profitTextPB5'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzes point of view. For the period 01.01–12/31/:year, the return on sales was :finalROSPercentage or :$finalROS% , and profitability calculated by net profit was :finalProfitMargin% per annum.',
    'profitTextPB6'             => 'The profitability calculated by earnings before interest and taxes (Return on sales) is more important from a comparative analyzes point of view. For the period 01.01–12/31/:year, the return on sales was :finalROSPercentage or :finalROS% , and profitability calculated by net profit was :finalProfitMargin% per annum.',
    'description_of_the_ratio'  => 'Description of the ratio and its reference value',
    'return_on_equity'             => 'Return on equity (ROE)',
    'roe_is_calculated'             => 'ROE is calculated by taking a year\'s worth of
                earnings (net profit) and dividing them by the
                average shareholder equity for that period, and is
                expressed as a percentage. It is one of the most 
                important financial ratios and profitability metrics.
                Acceptable value: 12% or more.',
    'return_on_assets'             => 'Return on assets (ROA)',
    'roa_is_calculated'             => 'ROA is calculated by dividing net income by total
                assets, and displayed as a percentage. Acceptable
                value: 6% or more.',
    'return_on_capital'             => 'Return on capital employed (ROCE)',
    'roce_is_calculated'             => 'ROCE is calculated by dividing EBIT by capital
                employed (equity plus non-current liabilities). It
                indicates the efficiency and profitability of a
                company\'s capital investments.',
    'ROAText1'             => 'For the year :endYr, the return on assets amounted to <span class=":ROAClass">:finalROA%</span>. That is :ROAChange% higher than for the year :initialYr.',
    'ROAText2'             => 'During the last year, the return on assets was equal to <span class=":ROAClass">:finalROA%</span>%. During the whole period reviewed, thereturn on assets was observed to spring up rapidly by <span class=":ROAChangeClass">:ROAChange%</span>%. Despite the fact that at the beginning of theconsidered period, the value of the return on assets did not correspond to the norm, later it became acceptable.',
    'ROAText3'             => 'During the last year, the return on assets equaled <span class=":ROAClass">:finalROA%</span>. For the entire period analyzed, it was found that there was an obvious decrease in the return on assets of <span class=":ROAClass">:ROAChange%</span>. The return on assets kept a normal value during the whole of the period.',
    'ROAText4'             => 'The return on assets was nearly equal to zero for the period from 01/01/:endYr to 12/31/:endYr. The return on assets fell by 3.6% for the period reviewed.',
    'ROAText5'             => 'During the year :endYr, the return on assets was equal to <span class=":ROAClass">:finalROA%</span>. For the last year in comparison with the same period of the prior financial year, it was found that there was a marked reduction in the return on assets of <span class=":ROAChangeClass">:ROAChange%</span>.',
    'ROAText6'             => 'During the period 01.01–12/31/:endYr, the return on assets was equal to <span class=":ROAClass">:finalROA%</span>, which is <span class=":ROAChangeClass">:ROAChange%</span> lower than for the year :initialYr. The return on assets kept an acceptable value during the whole of the period.',
    'ROEtext1'             => 'A key indicator of business profitability is the return of equity (ROE), i.e. return from money invested bythe owners. The return on equity for :financialReportTitle for the year :endYr was :finalROE% per annum.',
    'ROEtext2'             => 'A key indicator of business profitability is the return of equity (ROE), i.e. return from money invested bythe owners. During the year :endYr, a return on equity was :finalROE% per annum. It is a normal return on investment.Nonetheless, the rate should be assessed not only in absolute terms, but also by taking into account otherfactors, including macroeconomic rates (for example, the country\'s inflation rate).',
    'ROEtext3'             => 'A key indicator of business profitability is the return of equity (ROE), i.e. return from money invested by the owners. The profitability of the owners\' investments in :financialReportTitle\'s assets was <span class=":ROEClass">:finalROE%</span> per annum during the period 01.01–12/31/:endYr. And it should be observed that such a high return on equity is a result of a low percentage of own capital (equity) of the total company\'s capital (:capital%).',
    'ROEtext4'             => 'One of the most important ratio of business profitability is the return on equity (ROE), which reflects the profitability of investments by the owners. The return on equity for :financialReportTitle for the period 01.01–12/31/:endYr was <span class=":ROEClass">:finalROE%</span> per annum.',
    'ROEtext5'             => 'A key indicator of business profitability is the return of equity (ROE), i.e. return from money invested by the owners. The return on equity for :financialReportTitle, Inc. for the year :endYr was <span class=":ROEClass">:finalROE%</span> per annum.',
    'ROEtext6'             => 'One of the most important ratio of business profitability is the return on equity (ROE), which reflects the profitability of investments by the owners. The profitability of the owners\' investments in Iserve :financialReportTitle\'s assets was <span class=":ROEClass">:finalROE%</span> per annum for the last year.',
    'ROEtextB1'             => 'The change in the main rates of return on assets and equity of :financialReportTitle isdemonstrated in the following chart during the entire period analyzed.',
    'ROEtextB2'             => 'The chart below shows changes of the main rates of return on assets and equity of the company duringthe entire period analyzed.',
    'ROEtextB3'             => 'The chart below shows changes of the main rates of return on assets and equity of the company during the :yearCount years.',
    'ROEtextB4'             => 'The chart below shows changes of the main rates of return on assets and equity of the company during the period analyzed (from 31 December, :initialYr to 31 December, :endYr).',
    'ROEtextB5'             => 'The change in the main rates of return on assets and equity of :financialReportTitle is demonstrated in the following chart for the period reviewed.',
    'ROEtextB6'             => 'The following chart demonstrates the dynamics of the main rates of return on total assets and equity of :financialReportTitle during the period analyzed (from 12/31/:initialYr to 12/31/:endYr).'
);
