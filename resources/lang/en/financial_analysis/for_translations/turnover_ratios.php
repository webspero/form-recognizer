<?php

return array(
    '2_3_analysis_of_the_business'            => '2. 3 Analysis of the Business Activity (Turnover Ratios)',
    'further_in_the_table'            => 'Further in the table, the calculated rates of turnover of assets and liabilities describe how fast prepaid
    assets and liabilities to suppliers, contractors and staff are effected. Turnover ratios have strong industry
    specifics and depend on activity. This is why an absolute value of the ratios does not permit making a
    qualitative assessment. When assets turnover ratios are analyzed, an increase in ratios (i.e. velocity of
    circulation) and a reduction in circulation days are deemed to be positive dynamics. There is no well-defined
    interaction for accounts payable and capital turnover. In any case, an accurate conclusion can only be made
    after the reasons that caused these changes are considered.',
    'turnover_ratio'            => 'Turnover Ratio',
    'value_days'            => 'Value, Days',
    'ratio_year1'            => 'Ratio :year1',
    'ratio_year2'            => 'Ratio :year2',
    'change'            => 'Change, days Col.:colCount - Col.2',
    'receivables_turnover'            => 'Receivables turnover (days sales outstanding) <br>
            <small>(average trade and other current receivables divided by average daily revenue*)</small>',
    'accounts_payable_turnover'            => 'Accounts payable turnover (days payable outstanding) <br>
            <small>(average current payables divided by average daily purchases)</small>',
    'inventory_turnover'            => 'Inventory turnover (days inventory outstanding) <br>
            <small>(average inventory divided by average daily cost of sales)</small>',
    'asset_turnover'            => 'Asset turnover <br>
            <small>(average total assets divided by average daily revenue)</small>',
    'current_asset_turnover'            => 'Current asset turnover <br>
            <small>(average current assets divided by average daily revenue)</small>',
    'capital_turnover'            => 'Capital turnover <br>
            <small>(average equity divided by average daily revenue)</small>',
    'reference'            => '<i>Reference:</i> <br>
            Cash conversion cycle <br>
            <small>(days sales outstanding + days inventory outstanding - days payable outstanding) </small>',
    'during_the_last_year'            => 'During the last year, the average collection period (Days Sales Outstanding) was :receivablesTurnover days and the
    average days payable outstanding was {{$turnoverRatios[0]->PayableTurnover}} days as shown in the table. The rate of asset turnover means that
    :financialReportTitle gains revenue equal to the sum of all the available assets every 1298 days (on average during the period analyzed).'
    
);
