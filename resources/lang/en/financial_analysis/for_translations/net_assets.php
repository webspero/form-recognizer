<?php

return array(
    '1_2_net_assets'            => '1.2. Net Assets (Net Worth)',
    'indicator'                 => 'Indicator',
    'value'                     => 'Value',
    'change'                    => 'Change',
    'in_php'                    => 'in PHP',
    'of_the_balance_total'      => '% of the balance total',
    'php_col'                   => 'PHP (col.3-col.2)',
    'col'                       => '% ((col.3-col.2):col.2)',
    'at_the_beginning'          => 'at the beginning of the period analyzed(12/31/:year) }})',
    'at_the_end'                => 'at the end of the period analyzed(12/31/:year)',
    '1_net_tangible_assets'     => '1. Net Tangible assets',
    '2_net_assets'              => '2. Net assets(Net Worth)',
    '3_issued_share'            => '3. Issued (share) capital',
    '4_difference_between '     => '4. Difference between net assets and Issued (share) capital (line 2 - line 3)',
    'netText1'     => 'The net tangible assets were equal to PHP <span class=":eNetClass">:eNetTangibleAssets</span> at the end of the period reviewed. The net tangible assets sharply rose (PHP <span class=":netTangibleAssetsChangeClass">:netTangibleAssetsChangeSign:netTangibleAssetsChange</span>, or by <span class=":netTangibleAssetsChangePrecClass">:netTangibleAssetsChangePrec</span>:netTangibleAssetsChangePrecSign) during the period analyzed.',
    'netText2'     => 'On the last day of the period analyzed, the net tangible assets amounted to PHP  <span class=":eNetClass">:eNetTangibleAssets</span>.
                        For the period analyzed, there was an appreciable growth in the net tangible assets by PHP <span class=":netTangibleAssetsChangeClass">:netTangibleAssetsChangeSign:netTangibleAssetsChange</span>, or <span class=":netTangibleAssetsChangePrecClass">:netTangibleAssetsChangePrec</span>:netTangibleAssetsChangePrecSign to PHP <span class=":eNetClass">:eNetTangibleAssets</span> by the last day of the period.',
    'netText3'     => 'The net tangible assets were equal to PHP <span class=":eNetClass">:eNetTangibleAssets</span> at the end of the period reviewed.
                        During the entire period reviewed, it was seen that there was significant growth in net tangible assets, which was PHP <span class=":netTangibleAssetsChangeClass">:netTangibleAssetsChangeSign:netTangibleAssetsChange</span>',
    'netText4'     => ', or <span class=":netTangibleAssetsChangePrecClass">:netTangibleAssetsChangePrec/span>:netTangibleAssetsChangePrecSign',
    'netText5'     => 'It was observed that there was an obvious lowering in the net tangible assets from PHP <span class=":bNetClass">:bNetTangibleAssets</span> to PHP <span class=":eNetClass">:eNetTangibleAssets</span> (PHP <span class=":netTangibleAssetsChangeClass">:netTangibleAssetsChangeSign:netTangibleAssetsChange</span>) during the period analyzed.',
    'netText6'     => 'On the last day of the period analyzed, the intangible assets were equal to PHP <span class=":latestNetTangibleAssetsChangeClass">:latestNetTangibleAssetsChange</span>. This value shows the difference between the value of net tangible assets and all net worth.',
    'intangibleAssetsText'     => 'In this case, :financialReportTitle has no goodwill or other intangible assets.
                This is why amounts of net worth and net tangible assets are equal at the end of the period reviewed.',
    'netWorthText1'     => 'The net worth (net assets) of :financialReportTitlewas much higher (by <span class=":netWorthTimesClass">:netWorthTimes</span> times) than the share capital on the last day of the period analyzed (12/31/:year).
                        Such a ratio positively describes the company\'s financial position.',
    'netWorthText2'     => 'The net worth of :financialReportTitleexceeds the share capital',
    'netWorthText3'     => 'by <span class=":netWorthPercentageClass">:netWorthPercentage</span>%',
    'netWorthText4'     => 'on 12/31/:year.
                        Such a situation is normal, the net worth (net assets) should not be less than the share capital.',
    'netWorthText5'     => 'Net worth is used as a measure of the company\'s book value (as opposed to a shareholder\'s value, the value based on expected earnings and other methods used to estimate the company\'s value). 
                    In this case, net worth remains a key value in the estimation of a company\'s financial condition.',
    'netWorthText6'     => 'on 12/31/:year, the net worth (net assets) of :financialReportTitle is less than the share capital (difference makes PHP <span class=":eDifferenceClass">:eDifference </span>). 
                    It is a negative rate indicating that the company has uncovered a loss. 
                    It is deemed to be normal when the net worth (net assets) of the company is higher than the share capital that shows the profitability of the business.
                    Net worth is used as a measure of the company\'s book value (as opposed to a shareholder\'s value, the value based on expected earnings and other methods used to estimate the company\'s value).
                    In this case, net worth remains a key value in the estimation of a company\'s financial condition.',
    'issuedCapitalText1'     => 'The issued (share) capital did not change during the whole of the analyzed period.',
    'issuedCapitalText2'     => 'Increase in the issued (share) capital was abrupt in the analyzed period.',
    'issuedCapitalText3'     => 'Growth in the issued (share) capital was observed during the whole of the analyzed period.'
);
