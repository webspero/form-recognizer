<?php

return array(
    '21_overview_of_the_financial'  => '2.1 Overview of the Financial Results',
    'indicator'     				=> 'Indicator',
    'value_php'     				=> 'Value, PHP',
    'change'     					=> 'Change',
    'average_annual_value'     		=> 'Average annual value, PHP',
    '1_revenue'     				=> '1. Revenue',
    '2_cost_of_sales'     			=> '2. Cost of sales',
    '3_gross_profit'     			=> '3.<i>Gross profit</i> (1-2)',
    '4_other_income_and_expenses'   => '4. Other income and expenses, except Finance costs',
    '6_finance_costs'     			=> '6. Finance costs',
    '7_income_tax_expense'     		=> '7. Income tax expense (from continuing operations)',
    '8_profit_loss'     			=> '8. Profit (loss) from continuing operations (5-6-7)',
    '9_profit_loss'     			=> '9. Profit (loss) from discontinued operations',
    '10_profit_loss'     			=> '10. Profit (loss) (8+9)',
    '11_other_comprehensive_income' => '11. Other comprehensive income',
    '12_comprehensive_income'     	=> '12. Comprehensive income (10+11)',
    'the_revenue_equaled'     		=> 'The revenue equaled <span style="color::revenue1Class" > :revenue </span> for the last year,  while the revenue was significantly lower and equaled PHP <span style="color::revenue2Class;">:revenue2</span> for the year :year1 (i.e. it grew by PHP <span style="color::revenue1DiffClass" > :incomeStatementDif </span>). The change in revenue is demonstrated on the chart. The gross profit was equal to PHP <span style="color::grossProfit" > :grossProfitVal </span> 
        during the last year. For the last year in comparison with the same period of the prior financial year, it was observed that there was an outstanding increase in the gross profit (PHP <span style="color::grossProfit2" > :grossProfitVal2  </span>).',
    'for_the_last_year'     		=> 'For the last year, the company posted a gross profit and earnings before interest and taxes (EBIT), which was 
        <span style="color::profitLossClass" > :profitLoss </span>. The comprehensive income of :financial_report_title was PHP <span style="color::comprehensiveClass" > :comprehensiveIncome </span> in total for the period from 01/01/:year to 12/31/:year.'
      
);
