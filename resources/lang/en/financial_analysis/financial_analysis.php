<?php

return array(
    'financial_condition_analysis'      => ':financial_report_title\'s Financial Condition Analysis for the period of (01/01/:startYear) to (12/31/:endYear)',
    'financial_position_analysis'       => '1. :financial_report_title\'s Financial Position Analysis',
    'the_following_report_analyzes'     => 'The following report analyzes :financial_report_title\'s financial condition based on the financial statements data prepared according to International Financial Reporting Standards (IFRS) for the period from 01/01/:startYear to 12/31/:endYear.',
    '2_financial_performance'           => '2. Financial Performance'    
);
