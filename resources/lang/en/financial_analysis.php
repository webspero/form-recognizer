<?php

return array(
    'financial_condition_analysis'          => 'Financial Condition Analysis',
    'for_the_period_of'						=> 'for the period of',
    'financial_position_analysis'			=> 'Financial Position Analysis',
    'to'									=> 'to',
    'overview_of_the_financial_results'		=> 'Overview of the Financial Results',
    'indicator'								=> 'Indicator',
    'value_thousand_php'					=> 'Value, thousand PHP',
    'change'								=> 'Change',
    'average_annual_value'					=> 'Average annual value, thousand PHP',
    'thousand_php'							=> 'thousand PHP',
    'revenue'								=> 'Revenue',
    'cost_of_sales'							=> 'Cost of sales',
    'gross_profit'							=> 'Gross profit',
    'other_income_and_expenses'				=> 'Other income and expenses, except Finance costs',
    'finance_costs'							=> 'Finance costs',
    'income_tax_expense'					=> 'Income tax expense (from continuing operations)',
    'profit_loss_from_continuing'			=> 'Profit (loss) from continuing operations (5-6-7)',
    'profit_loss_from_discontinued'			=> 'Profit (loss) from discontinued operations',
    'profit_loss_8+9'						=> 'Profit (loss) (8+9)',
    'other_comprehensive_income'			=> 'Other comprehensive income',
    'comprehensive_income_10+11'			=> 'Comprehensive income (10+11)',
    'the_revenue_equaled'					=> 'The revenue equaled',
    'for_the_last_year'						=> 'for the last year, while the revenue was significantly lower and equaled PHP',
    
    'it_grew_by_php'						=> 'it grew by PHP',
    'the_change_in_revenue'					=> 'The change in revenue is demonstrated on the chart. The gross profit was equal to PHP',
    'during_the_last_year'					=> 'during the last year. For the last year in comparison with the same period of the prior financial year, it was observed that there was an outstanding increase in the gross profit',
    'for_the_last_year_the_company'			=> 'For the last year, the company posted a gross profit and earnings before interest and taxes (EBIT), which was',
    'was_php'								=> 'was PHP',
    'in_total_for'							=> 'in total for the period from',
    'financial_sustainability_analysis'		=> 'Financial Sustainability Analysis',
    'key_ratios_of_the_company'				=> 'Key Ratios of the Company\'s Financial Sustainability',
    'ratio'									=> 'Ratio',
    'value'									=> 'Value',
    'change'								=> 'Change',
    'description_of_the_ratio'				=> 'Description of the ratio and it\'s recommended value',
    'debt_to_equity_ratio'					=> 'Debt-to-equity ratio (financial leverage)',
    'a_debt_to_equity_ratio'				=> 'A debt-to-equity ratio is calculated by taking the total liabilities and dividing it by shareholders\' equity. It is the key financial ratio and used as a standard for judging a company\'s financial standing. Normal value: 1.5 or less (optimum 0.43-1).',
    'debt_ratio_debt'						=> 'Debt ratio (debt to assets ratio)',
    'a_debt_ratio'							=> 'A debt ratio is calculated by dividing total liabilities (i.e. long-term and short-term liabilities) by total assets. It shows how much the company relies on debt to finance assets (similar to debt-to-equity ratio). Normal value: 0.6 or less (optimum 0.3-0.5).',
    'long_term_debt'						=> 'Long-term debt to Equity',
    'this_ratio_is_calculated'				=> 'This ratio is calculated by dividing long-term (non-current) liabilities by equity.',
    'non_current_assets'					=> 'Non-current assets to Net worth',
    'this_ratio_is_calculated_by'			=> 'This ratio is calculated by dividing long-term (non-current) liabilities by net worth (equity) and measures the extent of a company\'s investment in low-liquidity non-current assets. This ratio is important for comparison analysis because it\'s less dependent on industry (structure of company\'s assets) than debt ratio and debt-to-equity ratio. Normal value: no more than 1.25.',
    'capitalization_ratio'					=> 'Capitalization ratio',
    'calculated_by_dividing'				=> 'Calculated by dividing non-current liabilities by the sum of equity and non-current liabilities.',
    'fixed_assets_to'						=> 'Fixed assets to Net worth',
    'this_ratio_indicates'					=> 'This ratio indicates the extent to which the owners\' cash is frozen in the form of fixed assets, such as property, plant, and equipment,
                investment property and non-current biological assets. Normal value: 0.75 or less.',
    'current_liability_ratio'				=> 'Current Liability Ratio',
    'current_liability_ratio_is_calculated'	=> 'Current liability ratio is calculated by dividing non-current liabilities by total (i.e. current and non-current) liabilities.',
    'first_attention_should'				=> 'First, attention should be drawn to the debt-to-equity ratio and debt ratio as the ratios describing the capital structure. Both ratios have similar meaning and indicate if there is not enough capital (equity) for stable work for the company. Debt-to-equity ratio is calculated as a relationship of the borrowed capital (liabilities) to the equity, while debt ratio is calculated as a relationship of the liabilities to the overall capital (i.e. the sum of equity and liabilities).',
    'on_31_december'						=> 'On 31 December',
    'the_debt_to_equity'					=> 'the debt-to-equity was equal to',
    'for_the_whole_period_reviewed'			=> 'For the whole period reviewed, it was observed that there was a an appreciable decrease in the debt ratio to',
    'the_debt_to_equity_amounted'			=> 'the debt-to-equity amounted to',
    'during_the_period_reviewed'			=> 'During the period reviewed, the debt ratio increased a little (to',
    'the_debt_ratio_amounted_to'			=> 'The debt ratio amounted to',
    'for_the_period_reviewed'				=> 'For the period reviewed (31.12.14–31.12.17), it was verified that there was a rapid increase in the debt ratio of',
    'at_the_end_of_the_period_analysed'		=> 'at the end of the period analysed; it is',
    'lower_than_the_level'					=> 'lower than the level of the debt ratio on the first day of the period analysed',
    'at_the_end_of_the_period_analysed2'	=> 'At the end of the period analyzed, the debt-to-equity was equal to',
    'the_debt_ratio_was_equal_to'			=> 'The debt ratio was equal to',
    'on_the_last_day'						=> 'on the last day of the period analyzed',
    'during_the_period_reviewed2'			=> 'During the period reviewed, it was verified that there was a significant increase in the debt ratio of',
    'the_debt_to_equity_was'				=> 'the debt-to-equity was',
    'at_the_same_time_the_debt'				=> 'at the same time the debt ratio was lower and equaled – at the beginning of the period',
    'on'									=> 'On',
    'the_debt_ratio_decreased'				=> 'The debt ratio decreased and showed',
    'for_the_entire_period_analyzed'		=> 'for the entire period analyzed',
    'at_the_end_of_the_period'				=> 'At the end of the period reviewed, the debt-to-equity was equal to',
    'the_debt_ratio_was'					=> 'the debt ratio was',
    'during_the_last'						=> 'During the last',
    'years_it_was_found'					=> 'years, it was found that there was a notable growth in the debt ratio of',
    'years_it_was_found_decreased'			=> 'years, it was found that there was a decreased in the debt ratio of',
    'the_value_of_the_debt'					=> 'The value of the debt ratio negatively describes the financial position of',
    'the_percentage_of_liabilities'			=> 'the percentage of liabilities is too high and equaled',
    'of_the_company_total'					=> 'of the company&apos; total capital. The maximum acceptable percentage is 60%. Too higher dependence from creditors can lower the financial stability of the company, especially in the case of economic instability and crisis on the market of borrowed capital. It is recommended to keep the value of the debt ratio at a level of 0.6 or less (optimum 0.3-0.5)',
    'the_value_of_the_debt_ratio_for'		=> 'The value of the debt ratio for',
    'indicates_an_excessive_percentage'		=> 'indicates an excessive percentage of liabilities on 31 December',
    'which_equaled'							=> 'which equaled',
    'of_total_capital'						=> 'of total capital. It is believed that liabilities should not be more than 60%. If not, dependence from creditors increases too much and the financial stability of the company suffers. When the structure of capital is normal, the debt ratio is no more than 0.6 (optimum 0.3-0.5)',
    'according_to_the_debt_ratio'			=> 'According to the debt ratio, the percentage of the borrowed capital (liabilities) is significantly lower than the admissible value and makes 2.9% of overall capital on 12/31/',
    'on_the_one_hand_it_positively'			=> 'On the one hand it positively describes the financial situation of',
    'on_the_other_hand'						=> 'On the other hand it says about missed opportunities to use borrowed capital for the extension of activity and acceleration of development rates. The company can increase the percentage of credits and debts without damage to its&apos; financial situation if a plan on efficient use of additional capital is available. The debt ratio kept an acceptable value during the whole of the period.',
    'the_debt_ratio_describes'				=> 'The debt ratio describes the financial position of',
    'as_a_very_good_one'					=> 'as a very good one, the percentage of liabilities is',
    'the_maximum_acceptable'				=> 'The maximum acceptable percentage of liabilities is deemed to be 60%. When sources to finance the company are planned, this percentage should be used as an upper limit of acceptable percentage of the borrowed capital. The debt ratio had an acceptable value during the whole of the analysed period.',
    'at_the_end_of_the_period'				=> 'at the end of the period reviewed, the percentage of liabilities is too high and equaled',
    'of_the_company_total'			=> 'of the company\'s total capital. The maximum acceptable percentage is 60%. Too higher dependence from creditors can lower the financial stability of the company, especially in the case of economic instability and crisis on the market of borrowed capital. It is recommended to keep the value of the debt ratio at a level of no more than 0.6 (optimum 0.3-0.5). Despite the fact that at the beginning of the analyzed period and the value of the debt ratio corresponded to the norm, later it became unacceptable.',
    'The_value_of_the_debt2'		=> 'The value of the debt ratio relates to',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',
    ''								=> '',

);
