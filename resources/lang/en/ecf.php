<?php

return array(
	'ecf' => 'Existing Credit Facilities',
	'ecf_hint' => 'The credit facilities you curently have are:',
	
	'credit_facilities'         => 'Credit Facilities',
	'credit_facilities_title'   => 'Credit Facility/ies from External Sources',
	'lending_institution'       => 'Lending Entity',
	'amount_of_line'            => 'Amount of Line',
	'purpose_of_cf'             => 'Purpose of Credit Facility',
	'purpose_of_cf_others'      => 'Purpose of Credit Facility Others',
	'credit_terms'              => 'Credit Terms (e.g. 30 days)',
	'outstanding_balance'       => 'Outstanding Balance',
	'experience'                => 'Experience',
	'other_remarks'             => 'Other Remarks',
	
	'working_capital'       => 'Working Capital',
	'expansion'             => 'Expansion',
	'takeout'               => 'Take-out of existing obligations',
	'supplier_credit'       => "Suppliers' Credit",
	'equipment_purchase'    => 'Equipment Purchase',
	'guarantee'             => 'Guarantee',
	'other_specify'         => 'Others (Pls Specify)',
	
    'banks'                 => 'Banks',
	'suppliers'             => 'Suppliers',
	'guarantors'            => 'Guarantors',
	'other_lenders'         => 'Other Lenders',
    
	'existing_collateral' => 'Existing Collateral',
	'existing_collateral_hint' => 'Collateral you offered to avail of the credit facilities such as land, buildings, machinery, etc. For estimated values, use current market value or appraised value, whichever is lower.',
	'type_of_deposit' => 'Type of Deposit Account (CA, SA)',
	'deposit_amount' => 'Deposit Amount',
	'marketable_securities' => 'Marketable Securities',
	'marketable_securities_value' => 'Marketable Securities Estimated Value',
	'property' => 'Property',
	'property_value' => 'Property Estimated Value',
	'chattel' => 'Chattel',
	'chattel_hint' => 'Personal property that is movable, e.g. fixtures or equipment',
	'chattel_value' => 'Chattel Estimated Value',
	'others' => 'Others',
	'others_value' => 'Others Estimated Value',
);