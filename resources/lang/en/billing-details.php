<?php

return array(
    'billing_details'           => 'Billing Details',
    'province'                  => 'Province',
    'city'                      => 'City',
    'street_address'            => 'Street Address',
    'zipcode'                   => 'Zipcode',
    'phone'                     => 'Phone Number',
    'save_details'              => 'Save Details',
);
