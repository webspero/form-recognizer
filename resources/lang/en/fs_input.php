
<?php

return [
    'financial_analysis_minumum' => 'Financial Analysis cannot be done in one period of data. 2 or more period of data is required.',
    'template_outdated' => 'FS Input Template is outdated.',
    'company_name_empty' => 'Company Name should not be empty.',
    'invalid_year' => 'Invalid year. Ending year for Balance Sheet, Income Statement and Cashflow should all be the same.',
    'entity_required' => 'Entity ID is required.',
    'unit_required' => 'Unit is required.',
    'currency_required' => 'Currency is required.',
    'industry_required' => 'Industry is required.',
    'industry_id_required' => 'Industry ID is required.',
    'non_current_assets_tally' => 'Non-current Assets in column :column did not tally.',
    'current_assets_tally' => 'Current Assets in column :column did not tally.',
    'assets_tally' => 'Assets in column :column did not tally.',
    'equity_tally' => 'Equity in column :column did not tally.',
    'non_current_liabilities_tally' => 'Non-current Liabilities in column :column did not tally.',
    'current_liabilities_tally' => 'Current Liabilities in column :column did not tally.',
    'liabilities_tally' => 'Liabilities in column :column did not tally.',
    'equity_liabilities_tally' => 'Equity and Liabilities in column :column did not tally.',
    'assets_liabilities_tally' => 'Assets and Liabilities in column :column did not tally.',
    'gross_profit_tally' => 'Gross Profit in column :column did not tally.',
    'profit_loss_operating_activities_tally' => 'Profit (loss) from operating activities in column :column did not tally.',
    'profit_loss_before_tax_tally' => 'Profit (loss) before tax in column :column did not tally.',
    'comprehensive_income_tally' => 'Comprehensive Income in column :column did not tally.',
    'net_cash_required' => 'Please input the Net Cash from Operating Activities into the Statement of Cashflow section of the FS Input Template',
    'current_assets_data_required' => 'Data must not be empty and must be numeric for Current Assets in cell :column',
    'current_fields'    => 'Data must be numeric in cell :column',
    'special_character_found'       => 'Data must not contain special character or non-numeric value. Please check cell :column',
    'special_character_found_company_name' => 'Data must not contain special character. Please check cell :column',
    'total_assets_data_required' => 'Data must not be empty and must be numeric for Total Assets in cell :column',
    'current_liabilities_data_required' => 'Data must not be empty and must be numeric for Current Liabilities in cell :column',
    'equity_data_required' => 'Data must not be empty and must be numeric for Equity in cell :column',
    'liabilities_data_required' => 'Data must not be empty and must be numeric for Liabilities in cell :column',
    'equity_and_liabilities_data_required' => 'Data must not be empty and must be numeric for Equity and Liabilities in cell :column',
    'gross_profit_data_required' => 'Data must not be empty and must be numeric for Gross Profit in cell :column',
    'profit_loss_oa_data_required' => 'Data must not be empty and must be numeric for Profit (loss) from operating activities in cell :column',
    'comprehensive_income_data_required' => 'Data must not be empty and must be numeric for Comprehensive Income in cell :column',
    'net_cash_oa_data_required' => 'Data must not be empty and must be numeric for Net Cash provided by Operating Activities in cell :column',
    'data_zero' => 'Assets not equivalent to Liabilities + Equity on Balance Sheet. Cells C76, D76 and E76 must be blank.',
    'additional_lines_required' => 'Additional Lines Sheet not found. Please download the correct Financial Statement Template',
    'data_entry_required' => 'Data Entry Sheet not found. Please download the correct Financial Statement Template',
    'input_numeric' => "Data must be numeric value for cell :column"
];