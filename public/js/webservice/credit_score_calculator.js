$(function ($) {
    $(document).ready(function(){

        var user_id     = 0;
        var rest_url    = '/webservice/fbdata?callback=?';

        var MAX_CREDIT_PTS  = 850;

        var EXCEL_RATING    = '752 - 850';
        var GOOD_RATING     = '692 - 752';
        var FAIR_RATING     = '634 - 692';
        var SPRIME_RATING   = '516 - 634';
        var POOR_RATING     = '0 - 516';

        var EXCEL_STRING    = 'Excellent';
        var GOOD_STRING     = 'Good';
        var FAIR_STRING     = 'Fair';
        var SPRIME_STRING   = 'Subprime';
        var POOR_STRING     = 'Poor';

        var EXCEL_COLOR    = 'darkgreen';
        var GOOD_COLOR     = 'green';
        var FAIR_COLOR     = 'darkorange';
        var SPRIME_COLOR   = 'orange';
        var POOR_COLOR     = 'darkred';

        var EXCEL_EXPLAIN   = 'Having a score in this range means a long and distinguished credit history that shows historically responsible payment habits and the ability to handle multiple types of credit responsibly. A score in this tier will allow access to the best interest rates and loan repayment terms. If you want to make major purchases, such as an investment property, this credit score range is the most ideal.';
        var GOOD_EXPLAIN    = 'Getting a score in this range is good news. Decent lending terms are easily accessible, though not as preferred as those offered to borrowers with truly excellent credit scores. You’ll also have no trouble getting an insurance policy for just about any need, but you should expect your premiums to be somewhat higher.';
        var FAIR_EXPLAIN    = 'Here is the absolute minimum credit score tier you can carry and still get fair mortgage terms. Minor items that require financing are still available in this range, but at conditions significantly lesser than those of a good credit score. There are fewer lenders willing to do business at this tier, especially if the low credit score is a result of slow payments. A higher risk of default to a lender means a likelihood of down payments or requiring physical collateral before a loan offer will be extended.';
        var SPRIME_EXPLAIN  = 'This tier is far from favorable. Loans might still be accessible depending on circumstances, but conditions will always be unfavorable to you. Higher interest and financing charges are the norm here. Certain employers in higher tier industries may even turn you away. This is also the minimum tier at which auto financing is still feasible.';
        var POOR_EXPLAIN    = 'Being on this tier means that there are active collections and delinquency on more than one account with at least one judgment, reposession or bankruptcy on file. Credit cards are either maxed out or shut off. Scores in this tier are so bad that any legitimate financing is out of the question. You definitely require credit counselling at this tier.';

        var EXCEL_ANGLE   = 180;
        var GOOD_ANGLE    = 135;
        var FAIR_ANGLE    = 90;
        var SPRIME_ANGLE  = 40;
        var POOR_ANGLE    = 5;

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1619950818276865',
                status     : true,
                xfbml      : true,
                cookie     : true,
                version    : 'v2.11'
            });
            FB.Event.subscribe('edge.create', function(href, widget) {
                showCalculator();
                $('#fb-like-btn').hide();
                $('#fb-actions-cntr').hide();
                $.getJSON(rest_url, { action: 'addFbLike', fbid: user_id, method:'getQuote',format:'jsonp',lang:'en'  });
            });
        };

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $('#fields-info').on('show.bs.modal', function(evt) {
            var link = $(evt.relatedTarget);
            var info = link.attr('data-hint');

            var info_cntr   = $('#info-content');

            info_cntr.html(info);
        });

        function generateGaugeChart(graph_data)
        {
            var sel_circle      = [false, false, false, false, false];
            var sel_width       = [20, 20, 20, 20, 20];
            var rating_min_max  = [POOR_RATING, SPRIME_RATING, FAIR_RATING, GOOD_RATING, EXCEL_RATING];
            var dial_angle      = [POOR_ANGLE, SPRIME_ANGLE, FAIR_ANGLE, GOOD_ANGLE, EXCEL_ANGLE];

            for (i = 0; 5 > i; i++) {
                if (rating_min_max[i] == graph_data[0]) {
                    sel_circle[i]   = true;
                    break;
                }
            }

            Highcharts.setOptions({
                colors: ['darkred', 'orange', 'darkorange', 'green', 'darkgreen']
            });

            $('#semi-circ-graph').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: '<div id = "dial-needle-mb" class = "visible-sm visible-xs"> <img class = "dial-img" style = "height: 95px;" src = "https://creditbpo.com/sites/default/files/417bc393.gerd-gauge-needle.png" alt="business consultant Philippines" /> </div>',
                    align: 'center',
                    verticalAlign: 'middle',
                    useHTML: true,
                    y: 40
                },
                tooltip: {
                    pointFormat: ''
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%']
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Your Credit Score',
                    innerSize: '50%',
                    events: {
                        afterAnimate: function() {
                            var needle_htm  = '';

                            needle_htm  = '<div id = "dial-needle" class = "hidden-sm hidden-xs"> <img class = "dial-img" src = "https://creditbpo.com/sites/default/files/417bc393.gerd-gauge-needle.png" alt="business consultant Philippines"/> </div>';

                            $('.highcharts-container').append(needle_htm);

                            $('.dial-img').animate({  textIndent: graph_data[4] }, {
                                step: function(now,fx) {
                                    $('.dial-img').css('transform','rotate('+now+'deg)');
                                    $('.highcharts-container').css('height', '280px')
                                },
                                duration:'slow'
                            },'linear');
                        }
                    },
                    data: [
                        {
                            name : 'Poor',
                            y: sel_width[0],
                            sliced: sel_circle[0],
                            selected: sel_circle[0]
                        }, {
                            name : 'Subprime',
                            y: sel_width[1],
                            sliced: sel_circle[1],
                            selected: sel_circle[1]
                        }, {
                            name : 'Fair',
                            y: sel_width[2],
                            sliced: sel_circle[2],
                            selected: sel_circle[2]
                        }, {
                            name : 'Good',
                            y: sel_width[3],
                            sliced: sel_circle[3],
                            selected: sel_circle[3]
                        }, {
                            name : 'Excellent',
                            y: sel_width[4],
                            sliced: sel_circle[4],
                            selected: sel_circle[4]
                        }

                    ]
                }]
            });
        }

        function showCalculator() {
            $('#credit-calculator-cntr').show();
            submitPersonalCreditInfo();
        }

        function fb_statusChangeCallback(response)
        {
            var loader      = $('#loading-cntr');
            var submit_btn  = $('#submit-credit-btn');
            //var rest_url    = 'https://RuffyColladoPC:1337/CreditBPO_Mk1.00/public/webservice/fbdata?callback=?';
            

            switch(response.status) {
                case 'connected':
                    $('#credit-calculator-cntr').hide();
                    $('#fb-login-main').modal('hide');
                    loader.show();

                    user_id = response.authResponse.userID;

                    $('body').scrollTop(200);

                    $.getJSON(rest_url, { action: 'getFbLike', fbid: user_id, method:'getQuote',format:'jsonp',lang:'en'  }, function(response) {

                        if (0 < response.valid) {
                            loader.hide();
                            showCalculator();
                        }
                        else {
                            $("#fb-like-btn iframe").on("load", function () {
                                loader.hide();
                                $('#fb-like-btn').hide();
                                $('#fb-actions-cntr').hide();
                                showCalculator();
                                $.getJSON(rest_url, { action: 'addFbLike', fbid: user_id, method:'getQuote',format:'jsonp',lang:'en' });
                            });

                            loader.hide();
                            $('#credit-calculator-cntr').hide();
                            $('#fb-like-btn').show();
                            $('#fb-actions-cntr').show();                           
                        }
                    });

                    break;

                case 'not_authorized':
                default:
                    $('#fb-login-main').modal({
                        keyboard: false,
                    })
                        .on('show.bs.modal', function(evt) {

                        })
                        .on('hide.bs.modal', function(evt) {
                            $(submit_btn).prop('disabled', false);
                        })
                        .modal('show');
            }

        }

        function loginOnFacebook()
        {
            $('#fb-login-btn').click(function() {
                FB.login(function(response) {
                    fb_statusChangeCallback(response);
                }, {scope: 'public_profile'});
            });
        }

        function shareOnFacebook(rating) {
            $('#fb-share-btn').click(function(e){

                FB.ui(
                    {
                    method: 'feed',
                    name: 'My Credit Rating is: '+rating,
                    link: 'https://creditbpo.com/credit-score-calculator',
                    picture: 'https://creditbpo.com/sites/default/files/'+rating+'-score.png',
                    caption: 'CreditBPO Personal Credit Calculator',
                    description: "I just had my personal credit score calculated at CreditBPO.com. Try it out. If you're applying for a business loan with DBP, submit your CreditBPO Rating Report at https://creditbpo.com/business-credit-rating",
                    message: ""
                });

                return false;
            });
        }

        function calculateCreditScore()
        {
            var calc_cred_form  = $('#cred-calc-form');

            $(calc_cred_form).submit(function() {

                validateCreditInput();

                return false;
            });
        }

        function validateCreditInput()
        {
            var cred_bal        = $('input[name="total-balance"]').val();
            var cred_limit      = $('input[name="total-limit"]').val();
            var ave_accnt_age   = $('input[name="ave-age"]').val();
            var old_accnt_age   = $('input[name="oldest-accnt"]').val();
            var cred_typ        = $('input[name="credit-type[]"]:checked');

            var error_cntr      = $('.error-msg');
            var calc_cntr       = $('#credit-calculator-cntr');
            var submit_btn      = $('#submit-credit-btn');

            var valid           = false;
            var error_msg       = '';

            $(submit_btn).prop('disabled', true);

            if ((0 == cred_bal.length) ||
            (0 == cred_limit.length) ||
            (0 == ave_accnt_age.length) ||
            (0 == old_accnt_age.length) ||
            (0 == cred_typ.length)) {
                error_msg = 'All fields are required';
            }
            else if (false == $.isNumeric(cred_bal)) {
                error_msg = 'Credit balance must be numeric';
            }
            else if (false == $.isNumeric(cred_limit)) {
                error_msg = 'Credit limit must be numeric';
            }
            else if (false == $.isNumeric(cred_bal)) {
                error_msg = 'Credit balance must be numeric';
            }
            else if (false == $.isNumeric(ave_accnt_age)) {
                error_msg = 'Average age of accounts must be numeric';
            }
            else if (false == $.isNumeric(old_accnt_age)) {
                error_msg = 'Oldest account must be numeric';
            }
            else {
                valid = true;
            }

            if (valid) {
                loginOnFacebook();

                FB.getLoginStatus(function(response) {
                    fb_statusChangeCallback(response);
                });
            }
            else {
                error_cntr.html(error_msg);
                $(submit_btn).prop('disabled', false);
                $('body').scrollTop(200);
            }
        }

        function submitPersonalCreditInfo()
        {
            var calc_cntr       = $('#credit-calculator-cntr');
            var cred_html_ui    = '';
            var row_style       = '';

            var result          = [];
            var rating_min_max  = [EXCEL_RATING, GOOD_RATING, FAIR_RATING, SPRIME_RATING, POOR_RATING];
            var rating_text     = [EXCEL_STRING, GOOD_STRING, FAIR_STRING, SPRIME_STRING, POOR_STRING];
            var rating_color    = [EXCEL_COLOR, GOOD_COLOR, FAIR_COLOR, SPRIME_COLOR, POOR_COLOR];
            var total_perc      = 0;
            var total_pts       = 0;
            var total_diff      = 0;

            var payment_hist    = 0;
            var debt_burden     = 0;
            var len_cred_hist   = 0;
            var num_credit_avl  = 0;
            var hard_cred       = 0;

            payment_hist    = calculatePaymentHistory();
            debt_burden     = calculateDebtBurden();
            len_cred_hist   = calculateCreditHistory();
            num_credit_avl  = calculateCreditsUsed();
            hard_cred       = calculateHardCreditSearch();

            total_perc  = payment_hist + debt_burden + len_cred_hist + num_credit_avl + hard_cred;
            total_pts   = MAX_CREDIT_PTS * (total_perc / 100);
            total_diff  = MAX_CREDIT_PTS - total_pts;

            result      = getRatingSummary(total_pts);

            cred_html_ui += '<div class = "credit-score-result">';

            cred_html_ui += '<div class = "advertise-prod row col-md-12">';
            cred_html_ui += '<p> Content on CreditBPO.com is not sponsored by any bank or credit card issuer. The free personal scoring calculator here is based on non-validated information supplied by an interested party (i.e., you). It should not be used as the only source of a personal credit score. <br/><b>Note: This is not the same as your business credit rating.</b> </p>';
            cred_html_ui += '</div>';

            cred_html_ui += '<div class = "row score-summary" style = "position: relative; top: 30px; clear: both;">';
            cred_html_ui += '<div class = "col-md-6" style = "margin: 0 auto; float: none; color: #000000;">';
            cred_html_ui += '<p><b> Your Personal Credit Rating:</b></p>';
            cred_html_ui += '<p style = "font-size: 43px; color: '+ result[3] +';"> '+ result[0] +' </p>';
            cred_html_ui += '</div>';
            cred_html_ui += '</div>';

            cred_html_ui += '<div class = "row"> <div id = "semi-circ-graph" class = "col-md-6 col-md-offset-5" style = "padding: 20px; margin: 0 auto; float: none;"></div> </div>';

            cred_html_ui += '<div class = "row score-rating-label">';
            cred_html_ui += '<div class = "col-md-6" style = "margin: 0 auto; float: none;">';
            cred_html_ui += '<p>This is a <b style = "color: '+ result[3] + ';">'+result[1]+'</b> score </p>';
            cred_html_ui += '<p> <button class = "btn" style = "background-color: #4e69a2; border-color: #435a8b #3c5488 #334c83; color: #fff" id = "fb-share-btn"> <i class = "icon icon-facebook"> </i> Share </button> your results with peers.</p>';
            cred_html_ui += '</div>';
            cred_html_ui += '</div>';

            cred_html_ui += '<div class = "row">';
            cred_html_ui += '<div class = "col-md-6" style = "border: 1px solid #13b36b; margin: 0 auto; float: none; padding: 20px; background-color: #EFEFEF; min-height: 175px;">';
            cred_html_ui += '<p><b>'+result[2]+'</b></p>';
            cred_html_ui += '</div>';
            cred_html_ui += '</div>';

            cred_html_ui += '<div class = "advertise-prod col-md-12" style = "padding-top: 20px;">';
            cred_html_ui += '<p> If you are a business owner, the CreditBPO Rating Report is your indispensable management tool-- a comprehensive analysis and scoring of qualitative and quantitative information that will compare you with your industry and help you grow your business. Order it today, know your creditworthiness, and be guided in making the right strategic decisions for your business </p>';
            cred_html_ui += '</div>';

            cred_html_ui += '</div>';

            calc_cntr.html(cred_html_ui);

            shareOnFacebook(result[1]);

            generateGaugeChart(result);
        }

        function calculatePaymentHistory()
        {
            var payment_history = 0;
            var liability       = Math.round($('select[name="bad-event"]').val());

            payment_history     = liability;

            return payment_history;
        }

        function calculateDebtBurden()
        {
            var debt_burden     = 0;
            var bal_limit_pts   = 0;
            var cred_bal_limit  = 0;
            var num_accnt       = Math.round($('select[name="num-accnt"]').val());
            var cred_bal        = Math.round($('input[name="total-balance"]').val(), 2);
            var cred_limit      = Math.round($('input[name="total-limit"]').val(), 2);

            cred_bal_limit  = cred_bal / cred_limit;

            if (0.51 <= cred_bal_limit) {
                bal_limit_pts   = 5;
            }
            else if (0.41 <= cred_bal_limit) {
                bal_limit_pts   = 10;
            }
            else if (0.31 <= cred_bal_limit) {
                bal_limit_pts   = 15;
            }
            else if (0.11 <= cred_bal_limit) {
                bal_limit_pts   = 20;
            }
            else {
                bal_limit_pts   = 25;
            }

            debt_burden         = bal_limit_pts + num_accnt;

            return debt_burden;
        }

        function calculateCreditHistory()
        {
            var len_cred_hist       = 0;
            var ave_age_pts         = 0;
            var oldest_accnt_pts    = 0;
            var ave_accnt_age       = Math.round($('input[name="ave-age"]').val(), 2);
            var old_accnt_age       = Math.round($('input[name="oldest-accnt"]').val(), 2);

            if (9 <= ave_accnt_age) {
                ave_age_pts = 10;
            }
            else if (6 <= ave_accnt_age) {
                ave_age_pts = 9;
            }
            else if (4 <= ave_accnt_age) {
                ave_age_pts = 7;
            }
            else if (2 <= ave_accnt_age) {
                ave_age_pts = 5;
            }
            else {
                ave_age_pts = 0;
            }

            if (9 <= old_accnt_age) {
                oldest_accnt_pts = 5;
            }
            else if (6 <= old_accnt_age) {
                oldest_accnt_pts = 4;
            }
            else if (4 <= old_accnt_age) {
                oldest_accnt_pts = 3;
            }
            else if (2 <= old_accnt_age) {
                oldest_accnt_pts = 2;
            }
            else if (1 <= old_accnt_age) {
                oldest_accnt_pts = 1;
            }
            else {
                oldest_accnt_pts = 0;
            }

            len_cred_hist       = ave_age_pts + oldest_accnt_pts;

            return len_cred_hist;
        }

        function calculateCreditsUsed()
        {
            var num_credit_avl  = 0;
            var sel_typ_pts     = 0;
            var sel_cred_typ    = [];
            var cred_typ        = $('input[name="credit-type[]"]:checked');

            cred_typ.each(function() {
                sel_cred_typ.push($(this).val());
            });

            if (4 <= sel_cred_typ.length) {
                sel_typ_pts = 10;
            }
            else if (3 <= sel_cred_typ.length) {
                sel_typ_pts = 9;
            }
            else if (2 <= sel_cred_typ.length) {
                sel_typ_pts = 8;
            }
            else {
                sel_typ_pts = 7;
            }

            num_credit_avl      = sel_typ_pts;

            return num_credit_avl;
        }

        function calculateHardCreditSearch()
        {
            var hard_cred       = 0;
            var num_appli       = Math.round($('select[name="application-qnty"]').val());

            hard_cred           = num_appli;

            return hard_cred;
        }

        function getRatingSummary(total_perc)
        {
            var result = [];

            if (752 <= total_perc) {
                result[0] = EXCEL_RATING;
                result[1] = EXCEL_STRING;
                result[2] = EXCEL_EXPLAIN;
                result[3] = EXCEL_COLOR;
                result[4] = EXCEL_ANGLE;
                result[5] = 850 - total_perc;
                result[6] = 'from a <b> Perfect score </b>';
            }
            else if (692 <= total_perc) {
                result[0] = GOOD_RATING;
                result[1] = GOOD_STRING;
                result[2] = GOOD_EXPLAIN;
                result[3] = GOOD_COLOR;
                result[4] = GOOD_ANGLE;
                result[5] = 752 - total_perc;
                result[6] = 'from an <b> Excellent score </b>';
            }
            else if (634 <= total_perc) {
                result[0] = FAIR_RATING;
                result[1] = FAIR_STRING;
                result[2] = FAIR_EXPLAIN;
                result[3] = FAIR_COLOR;
                result[4] = FAIR_ANGLE;
                result[5] = 692 - total_perc;
                result[6] = 'from a <b> Good score </b>';
            }
            else if (516 <= total_perc) {
                result[0] = SPRIME_RATING;
                result[1] = SPRIME_STRING;
                result[2] = SPRIME_EXPLAIN;
                result[3] = SPRIME_COLOR;
                result[4] = SPRIME_ANGLE;
                result[5] = 634 - total_perc;
                result[6] = 'from a <b> Fair score </b>';
            }
            else {
                result[0] = POOR_RATING;
                result[1] = POOR_STRING;
                result[2] = POOR_EXPLAIN;
                result[3] = POOR_COLOR;
                result[4] = POOR_ANGLE;
                result[5] = 516 - total_perc;
                result[6] = 'from a <b> Subprime score </b>';
            }

            return result;
        }

        calculateCreditScore();
    });
});