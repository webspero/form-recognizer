//======================================================================
//  Script S35: graphs-financial-analysis.js
//      Graphs for Financial Condition
//======================================================================
$(function () {

  var creditrating = $('#score_sf').val();
  var checker = $("#financialChecker").val();
  if (typeof (creditrating) != "undefined" && !checker) {
    var creditarr = creditrating.split(' - ');
    var gscore = parseFloat(creditarr[0]);

    if (gscore >= 177) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_1') + '<span>');
      $('#graphs3_3text000').html("AA");
      $('#graphs3_3text0000').attr('class', 'gcircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_high'));
      $('.prelimdefault').text('2% - Very Low Risk');
    } else if (gscore >= 150 && gscore <= 176) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_2') + '<span>');
      $('#graphs3_3text000').html("A");
      $('#graphs3_3text0000').attr('class', 'gcircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_high'));
      $('.prelimdefault').text('3% - Low Risk');
    } else if (gscore >= 123 && gscore <= 149) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_3') + '<span>');
      $('#graphs3_3text000').html("BBB");
      $('#graphs3_3text0000').attr('class', 'ocircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_moderate'));
      $('.prelimdefault').text('4% - Moderate Risk');
    } else if (gscore >= 96 && gscore <= 122) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_4') + '<span>');
      $('#graphs3_3text000').html("BB");
      $('#graphs3_3text0000').attr('class', 'ocircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_moderate'));
      $('.prelimdefault').text('6% - Moderate Risk');
    } else if (gscore >= 69 && gscore <= 95) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_5') + '<span>');
      $('#graphs3_3text000').html("B");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('8% - High Risk');
    } else if (gscore < 69) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_6') + '<span>');
      $('#graphs3_3text000').html("CCC");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));

      if (gscore >= 42 && gscore <= 68) {
        $('.prelimdefault').text('15% - High Risk');
      } else if (gscore >= 32 && gscore <= 41) {
        $('.prelimdefault').text('30% - Very High Risk');
      } else if (gscore >= 22 && gscore <= 31) {
        $('.prelimdefault').text('50% - Very High Risk');
      } else if (gscore >= 12 && gscore <= 21) {
        $('.prelimdefault').text('80% - Very High Risk');
      } else if (gscore < 12) {
        $('.prelimdefault').text('100% - Very High Risk');
      }
    }
  }


  var bank_standalone = $("#standalone_bank").val();
  if (bank_standalone == 0) {
    getDetails($("#fpr").val(), "#fposition_value", false);
    getDetails($("#fpm").val(), '#fperformance_value', false);
    getDetails($("#financialTotal").val(), '#graphs0value00000', true);
  } else {
    if ($('#bank_rating_score').length > 0) {
      var gscore1 = parseInt($('#bank_rating_score').val());
      if (gscore1 >= 177) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_1') + '<span>');
        $('#graphs3_3text0001').html("AA");
        $('#graphs3_3text00001').attr('class', 'gcircle-container');
        $('#graphs3_3text001').html(trans('risk_rating.cbr_high'));
      } else if (gscore1 >= 150 && gscore1 <= 176) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_2') + '<span>');
        $('#graphs3_3text0001').html("A");
        $('#graphs3_3text00001').attr('class', 'gcircle-container');
        $('#graphs3_3text001').html(trans('risk_rating.cbr_high'));
      } else if (gscore1 >= 123 && gscore1 <= 149) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_3') + '<span>');
        $('#graphs3_3text0001').html("BBB");
        $('#graphs3_3text00001').attr('class', 'ocircle-container');
        $('#graphs3_3text001').html(trans('risk_rating.cbr_moderate'));
      } else if (gscore1 >= 96 && gscore1 <= 122) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_4') + '<span>');
        $('#graphs3_3text0001').html("BB");
        $('#graphs3_3text00001').attr('class', 'ocircle-container');
        $('#graphs3_3text001').html(trans('risk_rating.cbr_moderate'));
      } else if (gscore1 >= 68 && gscore1 <= 95) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_5') + '<span>');
        $('#graphs3_3text0001').html("B");
        $('#graphs3_3text00001').attr('class', 'circle-container');
        $('#graphs3_3text001').html(trans('risk_rating.crb_low'));
      } else if (gscore1 < 68) {
        $('#graphs3_3text01').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_6') + '<span>');
        $('#graphs3_3text0001').html("CCC");
        $('#graphs3_3text00001').attr('class', 'circle-container');
        $('#graphs3_3text001').html(trans('risk_rating.crb_low'));
      }

      $('.rating_panel_switcher span').on('click', function (e) {
        if ($(this).attr('tab') == "original") {
          $('.bank_rating_panel').hide();
          $('.original_rating_panel').show();
        }
        if ($(this).attr('tab') == "bank") {
          $('.bank_rating_panel').show();
          $('.original_rating_panel').hide();
        }
        $('.rating_panel_switcher span').removeClass('active');
        $(this).addClass('active');
      });
    }
  }

  if (checker) {
    getDetails($("#fpr").val(), "#fposition_value", false);
    getDetails($("#fpm").val(), '#fperformance_value', false);
    getDetails($("#financialTotal").val(), '#graphs0value00000', true);
    let info = $("#financialTotal").val();
    //show creditbpo rating
    if (info >= 1.6) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_1') + '<span>');
      $('#graphs3_3text000').html("AAA");
      $('#graphs3_3text0000').attr('class', 'gcircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_high'));
      $('.prelimdefault').text('1% - Very Low Risk');
    } else if (info >= 1.2 && info < 1.6) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_15') + '<span>');
      $('#graphs3_3text000').html("AA");
      $('#graphs3_3text0000').attr('class', 'gcircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_high'));
      $('.prelimdefault').text('2% - Very Low Risk');
    } else if (info >= 0.8 && info < 1.2){
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: green;">' + trans('messages.result_1') + '<span>');
      $('#graphs3_3text000').html("A");
      $('#graphs3_3text0000').attr('class', 'gcircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_high'));
      $('.prelimdefault').text('3% - Low Risk');
    } else if (info >= 0.4 && info < 0.8) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_16') + '<span>');
      $('#graphs3_3text000').html("BBB");
      $('#graphs3_3text0000').attr('class', 'ocircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_moderate'));
      $('.prelimdefault').text('4% - Moderate Risk');
    } else if (info >= 0 && info < 0.4){
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: orange;">' + trans('messages.result_17') + '<span>');
      $('#graphs3_3text000').html("BB");
      $('#graphs3_3text0000').attr('class', 'ocircle-container');
      $('#graphs3_3text00').html(trans('risk_rating.cbr_moderate'));
      $('.prelimdefault').text('6% - Moderate Risk');
    } else if (info >= -0.4 && info < 0) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_4') + '<span>');
      $('#graphs3_3text000').html("B");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('8% - High Risk');
    } else if (info >= -0.8 && info < -0.4) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_11') + '<span>');
      $('#graphs3_3text000').html("CCC");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('15% - High Risk');
    } else if (info >= -1.2 && info < -0.8) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_12') + '<span>');
      $('#graphs3_3text000').html("CC");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('30% - High Risk');
    } else if (info >= -1.6 && info < -1.2) {
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_13') + '<span>');
      $('#graphs3_3text000').html("C");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('50% - High Risk');
    } else if (info < -1.6){
      $('#graphs3_3text0').html('<span style="font-size: 18px; color: red;">' + trans('messages.result_14') + '<span>');
      $('#graphs3_3text000').html("D");
      $('#graphs3_3text0000').attr('class', 'circle-container');
      $('#graphs3_3text00').html(trans('risk_rating.crb_low'));
      $('.prelimdefault').text('80% - High Risk');
    }
  } else {
    var score_rfp = parseFloat($("#score_rfp").val());
    if (score_rfp >= 162) {
      $('#fposition_value').html("AAA");
    } else if (score_rfp >= 145) {
      $('#fposition_value').html("AA");
    } else if (score_rfp >= 127) {
      $('#fposition_value').html("A");
    } else if (score_rfp >= 110) {
      $('#fposition_value').html("BBB");
    } else if (score_rfp >= 92) {
      $('#fposition_value').html("BB");
    } else if (score_rfp >= 75) {
      $('#fposition_value').html("B");
    } else if (score_rfp >= 57) {
      $('#fposition_value').html("CCC");
    } else if (score_rfp >= 40) {
      $('#fposition_value').html("CC");
    } else if (score_rfp >= 22) {
      $('#fposition_value').html("C");
    } else if (score_rfp >= 4) {
      $('#fposition_value').html("D");
    } else if (score_rfp < 4) {
      $('#fposition_value').html("E");
    }

    var score_rfpm = parseFloat($("#score_rfpm").val());
    if (score_rfpm >= 162) {
      $('#fperformance_value').html("AAA");
    } else if (score_rfpm >= 145) {
      $('#fperformance_value').html("AA");
    } else if (score_rfpm >= 127) {
      $('#fperformance_value').html("A");
    } else if (score_rfpm >= 110) {
      $('#fperformance_value').html("BBB");
    } else if (score_rfpm >= 92) {
      $('#fperformance_value').html("BB");
    } else if (score_rfpm >= 75) {
      $('#fperformance_value').html("B");
    } else if (score_rfpm >= 57) {
      $('#fperformance_value').html("CCC");
    } else if (score_rfpm >= 40) {
      $('#fperformance_value').html("CC");
    } else if (score_rfpm >= 22) {
      $('#fperformance_value').html("C");
    } else if (score_rfpm >= 4) {
      $('#fperformance_value').html("D");
    } else if (score_rfpm < 4) {
      $('#fperformance_value').html("E");
    }
  }


  //-----------------------------------------------------
  //  Binding S35.1: #bank_rating_score
  //      Displays the Financial Condition Chart
  //-----------------------------------------------------
  $('#container-semi').highcharts({

    chart: {
      type: 'solidgauge',
      backgroundColor: 'transparent',
      events: {
        redraw: function () {
          $('.redraw-fin-analysis').remove();
          var score_facs = parseFloat($("#score_facs").val());
          let checker = $("#financialChecker").val();
          if (!checker) {
            if (score_facs >= 162) {
              $('#graphs0value00000').text("AAA").attr('style', 'text-align:center; font-size: 35px;  margin-left: -10px;');
            } else if (score_facs >= 145) {
              $('#graphs0value00000').text("AA").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
            } else if (score_facs >= 127) {
              $('#graphs0value00000').text("A").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
            } else if (score_facs >= 110) {
              $('#graphs0value00000').text("BBB").attr('style', 'text-align:center; font-size: 35px;  margin-left: -10px;');
            } else if (score_facs >= 92) {
              $('#graphs0value00000').text("BB").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
            } else if (score_facs >= 75) {
              $('#graphs0value00000').text("B").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
            } else if (score_facs >= 57) {
              $('#graphs0value00000').text("CCC").attr('style', 'text-align:center; font-size: 35px; margin-left: -10px;');
            } else if (score_facs >= 40) {
              $('#graphs0value00000').text("CC").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
            } else if (score_facs >= 22) {
              $('#graphs0value00000').text("C").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
            } else if (score_facs >= 4) {
              $('#graphs0value00000').text("D").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
            } else if (score_facs < 4) {
              $('#graphs0value00000').text("E").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
            }
          } else {
            let info = $("#financialTotal").val();
            if (info >= 1.6) $('#graphs0value00000').text("AAA").attr('style', 'text-align:center; font-size: 35px;  margin-left: 5px !important;');
            else if (info >= 1.2 && info < 1.6) $('#graphs0value00000').text("AA").attr('style', 'text-align:center; font-size: 35px; margin-left: 20px;');
            else if (info >= 0.8 && info < 1.2) $('#graphs0value00000').text("A").attr('style', 'text-align:center; font-size: 35px; margin-left: 30px;');
            else if (info >= 0.4 && info < 0.8) $('#graphs0value00000').text("BBB").attr('style', 'text-align:center; font-size: 35px;  margin-left: 20px;');
            else if (info >= 0 && info < 0.4) $('#graphs0value00000').text("BB").attr('style', 'text-align:center; font-size: 35px; margin-left: 30px;');
            else if (info >= -0.4 && info < 0) $('#graphs0value00000').text("B").attr('style', 'text-align:center; font-size: 35px; margin-left: 50px;');
            else if (info >= -0.8 && info < -0.4) $('#graphs0value00000').text("CCC").attr('style', 'text-align:center; font-size: 35px;  margin-left: 33px;');
            else if (info >= -1.2 && info < -0.8) $('#graphs0value00000').text("CC").attr('style', 'text-align:center; font-size: 35px; margin-left: 40px;');
            else if (info >= -1.6 && info < -1.2) $('#graphs0value00000').text("C").attr('style', 'text-align:center; font-size: 35px; margin-left: 60px;');
            else if (info < -1.6) $('#graphs0value00000').text("D").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');;
          }

        }
      }
    },

    title: null,
    exporting: {
      enabled: false
    },
    pane: {
      center: ['50%', '50%'],
      size: '100%',
      startAngle: -90,
      endAngle: 90,
      background: {
        backgroundColor: '#fff',
        innerRadius: '75%',
        outerRadius: '100%',
        shape: 'arc',
        borderColor: 'transparent'
      }
    },

    tooltip: {
      enabled: false
    },

    // the value axis
    yAxis: {
      min: -2, //0
      max: 2, //100
      stops: [
        [0.1, '#fcd5c3'], // red
        [0.5, '#f1c40f'], // yellow
        [0.9, '#2ecc71'] // green
      ],
      minorTickInterval: null,
      tickPixelInterval: 400,
      tickWidth: 0,
      gridLineWidth: 0,
      gridLineColor: 'transparent',
      labels: {
        enabled: false
      },
      title: {
        enabled: false
      }
    },

    credits: {
      enabled: false
    },

    plotOptions: {
      solidgauge: {
        innerRadius: '75%',
        dataLabels: {
          y: -45,
          borderWidth: 0,
          useHTML: true
        }
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
    },
    series: [{
      data: [parseFloat($("#financialTotal").val())],
      dataLabels: {
        format: '<p id="graphs0value00000" style="text-align:center; font-size: 35px;">{y}</p>'
      }
    }]
  });

  if (checker != 1) {
    var score_facs = parseFloat($("#score_facs").val());
    if (score_facs >= 162) {
      $('#graphs0value00000').text("AAA").attr('style', 'text-align:center; font-size: 35px;  margin-left: -10px;');
    } else if (score_facs >= 145) {
      $('#graphs0value00000').text("AA").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
    } else if (score_facs >= 127) {
      $('#graphs0value00000').text("A").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
    } else if (score_facs >= 110) {
      $('#graphs0value00000').text("BBB").attr('style', 'text-align:center; font-size: 35px;  margin-left: -10px;');
    } else if (score_facs >= 92) {
      $('#graphs0value00000').text("BB").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
    } else if (score_facs >= 75) {
      $('#graphs0value00000').text("B").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
    } else if (score_facs >= 57) {
      $('#graphs0value00000').text("CCC").attr('style', 'text-align:center; font-size: 35px; margin-left: -10px;');
    } else if (score_facs >= 40) {
      $('#graphs0value00000').text("CC").attr('style', 'text-align:center; font-size: 35px; margin-left: 0px;');
    } else if (score_facs >= 22) {
      $('#graphs0value00000').text("C").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
    } else if (score_facs >= 4) {
      $('#graphs0value00000').text("D").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
    } else if (score_facs < 4) {
      $('#graphs0value00000').text("E").attr('style', 'text-align:center; font-size: 35px; margin-left: 14px;');
    }
  }

  function getDetails(info, field, total) {
    var result = "";
    if (info >= 1.6) result = "AAA";
    else if (info >= 1.2 && info < 1.6) result = "AA";
    else if (info >= 0.8 && info < 1.2) result = "A";
    else if (info >= 0.4 && info < 0.8) result = "BBB";
    else if (info >= 0 && info < 0.4) result = "BB";
    else if (info >= -0.4 && info < 0) result = "B";
    else if (info >= -0.8 && info < -0.4) result = "CCC";
    else if (info >= -1.2 && info < -0.8) result = "CC";
    else if (info >= -1.6 && info < -1.2) result = "C";
    else if (info < -1.6) result = "D";
    if (total) {
      if (result == 'AAA' || result == 'BBB' || result == 'CCC') $('#graphs0value00000').text(result).attr('style', 'text-align:center; font-size: 35px;  margin-left: -10px;');
      else if (result == 'AA' || result == 'BB' || result == 'CC') $('#graphs0value00000').text(result).attr('style', 'text-align:center; font-size: 35px;  margin-left: 0px;');
      else $('#graphs0value00000').text(result).attr('style', 'text-align:center; font-size: 35px;  margin-left: 14px;');
    } else {
      $(field).html(result);
    }
  }

});