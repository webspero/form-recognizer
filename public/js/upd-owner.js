//======================================================================
//  Script S63: upd-owner.js
//      Adding and Editing of Owner Details data
//======================================================================\

$(document).ready(function(){

/**------------------------------------------------------------------------
|	Binds the Add Personal Loan link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popPersonalLoanForm()
{
    /** Variable Definition    */
    var owner_inf_cntr  = $('#ownerdetails');
    var pop_form_btn    = '.pop-personal-loan-form';
    
    //-----------------------------------------------------
    //  Binding S63.1: .pop-personal-loan-form
    //      Launch the Modal Form for Personal Loans
    //-----------------------------------------------------
    owner_inf_cntr.on('click', pop_form_btn, function() {
        var get_url     = $(this).attr('href');
        var hdr_txt     = 'Personal Loan';
        var form_id     = 'personal-loan-form';
    
        initPopForm(hdr_txt, form_id, get_url);
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Key Managers link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popKeyManagerForm()
{
    /** Variable Definition    */
    var owner_inf_cntr  = $('#ownerdetails');
    var pop_form_btn    = '.pop-biz-manager-form';
    
    //-----------------------------------------------------
    //  Binding S63.2: .pop-biz-manager-form
    //      Launch the Modal Form for Key Manager
    //-----------------------------------------------------
    owner_inf_cntr.on('click', pop_form_btn, function() {
        var get_url     = $(this).attr('href');
        var hdr_txt     = 'Key Manager';
        var form_id     = 'biz-manager-form';
    
        initPopForm(hdr_txt, form_id, get_url);
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Other Business Managed link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popOwnerBusinessForm()
{
    /** Variable Definition    */
    var owner_inf_cntr  = $('#ownerdetails');
    var pop_form_btn    = '.pop-biz-mng-form';
    
    //-----------------------------------------------------
    //  Binding S63.3: .pop-biz-mng-form
    //      Launch the Modal Form for Other Business
    //-----------------------------------------------------
    owner_inf_cntr.on('click', pop_form_btn, function() {
        var get_url     = $(this).attr('href');
        var hdr_txt     = 'Other Business';
        var form_id     = 'biz-mng-form';
    
        initPopForm(hdr_txt, form_id, get_url);
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Education link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popEducationForm()
{
    /** Variable Definition    */
    var owner_inf_cntr  = $('#ownerdetails');
    var pop_form_btn    = '.pop-educ-form';
    
    //-----------------------------------------------------
    //  Binding S63.4: .pop-educ-form
    //      Launch the Modal Form for Educational Background
    //-----------------------------------------------------
    owner_inf_cntr.on('click', pop_form_btn, function() {
        var get_url     = $(this).attr('href');
        var hdr_txt     = 'Educational Background';
        var form_id     = 'educ-bg-form';
    
        initPopForm(hdr_txt, form_id, get_url);
        return false;
    });
}

popPersonalLoanForm();
popKeyManagerForm();
popOwnerBusinessForm();
popEducationForm();

});