//======================================================================
//  Script S65: user-2fa-auth.js
//      2FA Authentication Scripts
//======================================================================

$(document).ready(function() {
    
/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function init2faAuth()
{
    /** Binds the events to the elements     */
    // checkSuspiciousAttempt();
    // bindTestFormSubmit();
    // bindUnlockLink();
}

/**------------------------------------------------------------------------
|	Checks Malicious attempts at login
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function checkSuspiciousAttempt()
{
    /** Variable Definition         */
    var ajax_url    = BaseURL+'/rest/check_suspicious_attempts';
    var form_body   = $('.user-2fa-body');
    var form_foot   = $('.user-2fa-footer');
    var form_modal  = $('#user-2fa-form-modal');
    var auth_cause  = $('.user-2fa-message');
    
    $.get(ajax_url, function(response) {
        
        switch (response.valid) {
            
            case 0:
                /** Open the Rating form modal */
                form_modal.modal({
                    keyboard: false,
                    backdrop: 'static',
                })
                    .on('show.bs.modal', function(e) {
                        auth_cause.html(response.messages);
                    })
                    .modal('show');
                break;
                
            
            
            case 2:
                form_body.html(response.messages);
                form_foot.html('');
                
                /** Open the Rating form modal */
                form_modal.modal({
                    keyboard: false,
                    backdrop: 'static',
                }).modal('show');
                
                break;
            
            case 1:
            default:
                /* No Processing    */
                break;
        }
        
    }, 'json' );
}

/**------------------------------------------------------------------------
|	Binds submit event to the Test Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindTestFormSubmit()
{
    /** Variable Definition         */
    var ajax_url    = BaseURL+'/rest/check_suspicious_attempts';
    var srv_message = '';
    var error_msg   = $('.errormessage');
    var form_body   = $('.user-2fa-body');
    var form_foot   = $('.user-2fa-footer');
    var test_form   = $('#user-2fa-test-form');
    var form_modal  = $('#user-2fa-form-modal');
    
    //-----------------------------------------------------
    //  Binding S65.1: #user-2fa-test-form
    //      Validates and saves 2FA Attempt when form is submitted
    //-----------------------------------------------------
    test_form.submit(function() {
        
        $.post(ajax_url, test_form.serialize(), function(response) {
            if (0 == response.valid) {
                for (index = 0; index < response.messages.length; index++) {
                    srv_message += response.messages[index] +'<br/>';
                }
                
                /**	Display the warning								*/
                error_msg.html(srv_message);
            }
            else if (1 == response.valid) {
                form_body.html('Thank you. You have verified your ownership of this account. Click <a href = "#" class = "verified-user">here</a> to continue');
                form_foot.html('');
                $('.verified-user').click(function() {
                    form_modal.modal('hide');
                    return false;
                });
            }
            else if (2 == response.valid) {
                form_body.html(response.messages);
                form_foot.html('');
            }
            
        }, 'json' );
        
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds click event to the Unlock button
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindUnlockLink()
{
    /** Variable Definition         */
    var unlock_link = $('.unlock-account');
    
    //-----------------------------------------------------
    //  Binding S65.2: unlock-account
    //      Removes suspension of an account after clicking
    //-----------------------------------------------------
    unlock_link.click(function() {
       $.get($(this).attr('href'), function() {
            location.reload();
       });
       
       return false;
   });  
}

init2faAuth();

});