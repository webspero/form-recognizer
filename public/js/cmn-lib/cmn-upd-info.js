/**--------------------------------------------------------------------------
| Common scripts for add and edit forms
|----------------------------------------------------------------------------
|	@file	cmn-upd-info.js
|	@brief	Common AJAX scripts for Add and Edit forms
|	@details DESCRIPTION: Common AJAX scripts for Add and Edit forms
|
|		[Pop-up]
|			initPopForm.................Initialization of Pop-up
|			bindUpdBtn..................Binds the Modal Save button to the specified form
|			loadPopForm.................Shows the Modal Pop-up
|
|       [Editable Pop-up]
|           initPopEditableUplod........Initialization of Pop-up Upload Form
|           bindEditableUplodBtn........Binds the Modal Save button to the specified form
|           loadEditableUplod...........Shows the Modal Pop-up
|
|       [Click and Edit fields]
|			bindGetEditableField........Binds the editable fields to the click event
|			bindPostEditableField.......Binds the editable input fields to the blur event
|			cancelEditableField.........Cancels the Editable field currently open
|			showEditableFields..........Displays an edit icon for the editable fields
|
|       [Expander Forms]
|           initExpandForm..............Initialization of Expander Forms
|           showFormExpander............Loads the Form on the specified Container
|           closeFormExpander...........Closes the Form Expander
|           bindSubmitExpander..........Binds the Submit event to the Expander form
|
|       [Commmon Methods]
|			postInfo....................Sends a Post request to the Server via AJAX
|			postUpload..................Uploads the via AJAX Post request
|			bindDateInput...............Binds the Date Picker to the text inputs
|			deleteInfo..................Binds the Delete links to the Click event
|			refocusModal................Refocus on the Previous Modal when the topmost Modal has been closed
|			detectFormData..............Detect if the Browser supports FormData
|			turnOffAjaxUpload...........Removes binding of AJAX Pop-up uploaders
|
|--------------------------------------------------------------------------*/

/**------------------------------------------------------------------------
|	Constants
|------------------------------------------------------------------------*/
var STS_OK      = 1;
var STS_NG      = 0;

var MODAL_CNTR  = $('#modal-add-info');
var UPD_BTN     = '.pop-upd-btn';
var EXP_UPD_BTN = '.submit-expander-form';

var MODAL_UPLOD = $('#modal-file-uploader');
var UPLOD_BTN   = '.pop-uploader-btn';

var flg_form_exist      = STS_NG;       /** Flag to signal that an Editable has been queued for loading */
var editable_lnk_clk    = null;         /** Stores the Queued editable link clicked                     */

var REFRESH_PAGE_ACT    = 0;
var REFRESH_CNTR_ACT    = 1;
var OVERWRITE_INFO_ACT  = 2;
var MODAL_CONFIRM  = 3;

/**------------------------------------------------------------------------
|	Initialization of Pop-up
|--------------------------------------------------------------------------
|	@param [in]		hdr_txt --- Header to appear on Modal Title
|                   form_id --- ID attribute of the Form in string
|                   get_url --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var initPopForm = function(hdr_txt, form_id, get_url)
{
    /** Unbinds previously binded event to the Modal Save Button    */
    MODAL_CNTR.off('click', UPD_BTN);

    /** Display the Pop-up Form */
    loadPopForm(hdr_txt, get_url);

    /** Bind the Save button to the specified form  */
    bindUpdBtn(form_id);
}

/**------------------------------------------------------------------------
|	Binds the Modal Save button to the specified form
|--------------------------------------------------------------------------
|	@param [in]		form_id --- ID Attribute of the form
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindUpdBtn = function(form_id)
{
    /** Variable Definition    */
    var post_data   = '';
    var post_url    = '';

    /** Binds the Save button to click event  */
    MODAL_CNTR.on('click', UPD_BTN, function() {

        var error_cntr  = $('.pop-error-msg');  /** Modal Error Container           */
        var upd_form    = $('#'+form_id);       /** ID attribute of the Form        */

        /** No error on JQuery Validation Engine    */
        if (upd_form.validationEngine('validate')) {
            post_data   = upd_form.serialize();     /** Input data of the Form          */
            post_url    = upd_form.attr('action');  /** Action attribute of the Form    */

            /** Change button to Saving...  */
            $(UPD_BTN).prop('disabled', true).text('Saving...');

            /** Send Post request via AJAX  */
            postInfo(post_url, post_data, error_cntr, 0);
        }

        return false;
    });
}

/**------------------------------------------------------------------------
|	Shows the Modal Pop-up
|--------------------------------------------------------------------------
|	@param [in]		hdr_txt     --- Header to be displayed on the Title
|                   ajax_url    --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var loadPopForm = function(hdr_txt, ajax_url)
{
    /** Variable Definition    */
    var hdr_lbl         = $('#pop-hdr-lbl');
    var modal_body      = $('#add-info-body');
    var error_cntr      = $('.pop-error-msg');

    /** Configure the Modal Pop-up  */
    MODAL_CNTR.modal({
        keyboard: false,
        backdrop: 'static',
    })
        /** Event before Modal Pops-up  */
        .on('show.bs.modal', function(e) {
            /** Loads the Form  */
            hdr_lbl.text(hdr_txt);
            modal_body.html('<img src = "'+BaseURL+'/images/preloader.gif" />');
            modal_body.load(ajax_url, function(){

                var modal_height = $(document).height() + 180;

                if (trans('ecf.ecf') == hdr_txt) {
                    modal_height = $(document).height() + 380;
                }
                else if (trans('rcl.rcl') == hdr_txt) {
                    modal_height = $(document).height() + 260;
                }
                else {
                    /** No Processing   */
                }

                $('.modal-backdrop').css('min-height', modal_height+'px');
            });
        })
        /** Event before Modal closes  */
        .on('hide.bs.modal', function(e) {
            /** Reset the Form and bindings   */
            error_cntr.hide();
            modal_body.empty();
            MODAL_CNTR.unbind();
            $(UPD_BTN).prop('disabled', false).text('Save');
        })
        .modal('show');
}

var listEditable = $('.editable-field');
var index = -1;

/**------------------------------------------------------------------------
|	Binds the editable fields to the click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindGetEditableField = function()
{
    /** Variable Definition    */
    var editable_elem   = '.editable-field';                    /** Class for Editable fields   */
    var get_field_url   = BaseURL+'/rest/get_editable_field';   /** AJAX URL to display Form    */
    var editable_form   = '#editable-field-form';               /** Class for Editable fields   */

    /** Binds the Editable fields to the Click event    */
    $('body').on('click', editable_elem, function() {

        /** When there is an existing editable  */
        if ($('#editable-field-form').length) {

            /** Tells the System that another editable has been clicked */
            if (STS_NG == flg_form_exist) {
                /** Prevents the system from storing more than one editable clicked   */
                flg_form_exist      = STS_OK;

                /** Stores the clicked link */
                editable_lnk_clk    = $(this);
            }
        }
        /** No Editable is opened  */
        else {
            var field_id    = $(this).data('field-id');
            var field_name  = $(this).data('name');
            var ajax_url    = get_field_url+'/'+field_id+'/'+field_name;

            /** Turns off all other bindings to prevent multiple editables opened   */
            $('body').off('click', editable_elem);

            /** Displays Loading image while AJAX request is ongoing    */
            $(this).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

            /** Changes the value to an input form  */
            $(this).load(ajax_url, function() {
                var input_field = $('.edit-input-field');

                /** AJAX request completed signal   */
                flg_form_exist = STS_NG;

                /** Removes bindings to click anywhere submission     */
                $('html').off('click');

                /** Change class to determine which is being edited     */
                $(this).addClass('edit-input-cntr');
                $(this).removeClass('editable-field');

                /** Adds a class for date picker type of inputs         */
                if ($(this).hasClass('date-input')) {
                    input_field.addClass('withdate');
                }

                bindDateInput();

                /** Prevents the input box to overlap to other elements */
                if (input_field.width() > $(this).parent().width()) {
                    input_field.css('width', 'inherit');
                }

                input_field.focus().select();

                /** Rebinds the editables for loading   */
                bindGetEditableField();
                
                index = listEditable.index($(this));

                /** Submits the Form when clicked anywhere outside   */
                $('html').on('click', function(e) {
                    var $target = $(e.target);

                    /** Prevents form submission when calendars, drop downs, or the Form itself is clicked  */
                    if ($target.closest('a, select, #editable-field-form, .ui-datepicker-buttonpane').length == 0) {
                        $(editable_form).submit();
                    }
                });
            });
        }
    });
}

/** Function to tab/shift+tab to next/previous field for editable fields */ 
function tab() {

    var buffer = [];
    var lastKeyTime = Date.now();

    document.addEventListener('keydown', event => {
        if ($(event.target).is('.noTab *')) {
            var key = event.keyCode;
            const currentTime = Date.now();

            if (currentTime - lastKeyTime > 200) {
                buffer = [];
            }

            buffer.push(key);
            lastKeyTime = currentTime;

            if(buffer.length == 1) {
                if(buffer[0] == 9) {
                    event.preventDefault();
                    listEditable[index + 1].click();
                }
            } else if (buffer.length == 2){
                if(buffer[0] == 16 && buffer[1] == 9) {
                    event.preventDefault();
                    listEditable[index - 1].click();
                }
            }
        }
    });
}

/**------------------------------------------------------------------------
|	Binds the editable input fields to the submit event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindPostEditableField = function()
{
    /** Variable Definition    */
    var editable_form   = '#editable-field-form';                   /** Class for Editable fields   */

    /** Binds the editable input fields to the blur event   */
    $('body').on('submit', editable_form, function(evt) {
        var ajax_url    = $(this).attr('action');
        var error_cntr  = $('.editable-error-msg');
        var post_data   = $(editable_form).serialize();

        fields = $(editable_form).serializeArray();
        if(fields[1].name == "companyname"){
            $("#loading").show();
            $("#addCancel").hide();
        }

        /** Sends a Post request via AJAX   */
        postInfo(ajax_url, post_data, error_cntr, 0);

        return false;
    });
}

/**------------------------------------------------------------------------
|	Cancels the Editable field currently open
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var cancelEditableField = function()
{
    var cancel_btn  = '.cancel-editables';
    var cancel_cntr = '.edit-input-cntr';
    var cancel_val  = '#prev_val';

    /** Binds the Cancel button to the click event  */
    $('body').on('click', cancel_btn, function() {
        var prev_val = $(cancel_val).val();

        /** Displays the Previous value to the editable container  */
        $(cancel_cntr).html(prev_val);
        $(cancel_cntr).addClass('editable-field').removeClass('edit-input-cntr');

        /** Removes bindings to click anywhere submission     */
        $('html').off('click');
    });
}

/**------------------------------------------------------------------------
|	Displays an edit icon for the editable fields
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var showEditableFields = function()
{
    /** Variable Definition    */
    var editable_elem   = '.editable-field';    /** Class for Editable fields   */

    /** When mouse is hovered over the editable element */
    $('body').on('mouseenter', editable_elem,
        function() {
            $(this).append('<i class = "glyphicon glyphicon-edit"> </i>');
			$(this).parent().addClass('bg-gray');
        }
    );

    /** When mouse leaves the editable element */
    $('body').on('mouseleave', editable_elem,
        function() {
            $(this).find('i').remove();
			$(this).parent().removeClass('bg-gray');
        }
    );
}

/**------------------------------------------------------------------------
|	Initialization of Pop-up Upload Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var initPopEditableUplod = function()
{
    $('body').on('click', '.pop-file-uploader', function() {
        var hdr_txt     = $(this).data('header');
        var form_elem   = $('#file-uploader-form');
        var ajax_url    = $(this).data('target');

        /** Unbinds previously binded event to the Modal Save Button    */
        MODAL_CNTR.off('click', UPD_BTN);

        /** Display the Pop-up Form */
        loadEditableUplod(hdr_txt, ajax_url);

        /** Bind the Save button to the specified form  */
        bindEditableUplodBtn(form_elem);

        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Modal Save button to the specified form
|--------------------------------------------------------------------------
|	@param [in]		form_elem --- Form element selector string
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindEditableUplodBtn = function(form_elem)
{
    /** Variable Definition    */
    var post_data   = '';
    var post_url    = '';

    /** Binds the Save button   */
    MODAL_UPLOD.on('click', UPLOD_BTN, function() {

        var error_cntr  = $('.pop-error-msg');  /** Modal Error Container           */
        var upd_form    = $(form_elem);         /** Form Element Selector           */

        /** No error on JQuery Validation Engine    */
        if (upd_form.validationEngine('validate')) {
            post_data   = upd_form.serialize();         /** Input data of the Form          */
            post_url    = upd_form.attr('action');      /** Action attribute of the Form    */

            /** Change button to Saving...  */
            $(UPLOD_BTN).prop('disabled', true).text('Saving...');

            /** Send Post request via AJAX  */
            postUpload(post_url, error_cntr);
        }

        return false;
    });
}

/**------------------------------------------------------------------------
|	Shows the Modal Pop-up
|--------------------------------------------------------------------------
|	@param [in]		hdr_txt     --- Header to be displayed on the Title
|                   ajax_url    --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var loadEditableUplod = function(hdr_txt, ajax_url)
{
    /** Variable Definition    */
    var hdr_lbl         = $('#pop-upload-hdr-lbl');
    var form_elem       = $('#file-uploader-form');
    var modal_body      = $('#file-uploader-body');
    var error_cntr      = $('.pop-error-msg');

    /** Configure the Modal Pop-up  */
    MODAL_UPLOD.modal({
        keyboard: false,
        backdrop: 'static',
    })
        /** Event before Modal Pops-up  */
        .on('show.bs.modal', function(e) {
            /** Loads the Form  */
            error_cntr.hide();
            hdr_lbl.text(hdr_txt);
            form_elem.attr('action', ajax_url);

            /* if ('Registration Document' == hdr_txt) {
                var sec_sheet_htm   = '';

                sec_sheet_htm += '<p id = "sec_sheet_hdr"><br/><b>SEC Generation Information Sheet (Most Recent):</b></p>';
                sec_sheet_htm += '<input type = "file" name = "sec_sheet" id = "sec_sheet" class = "form-control">';
                sec_sheet_htm += '<small id = "sec_sheet_info" class="small_text accepted-format-1">Accepted file formats: pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt &nbsp;&nbsp;&nbsp;</small>';

                form_elem.append(sec_sheet_htm);
            } */

			if ('SEC General Information Sheet' == hdr_txt) {
				form_elem.find('.accepted-format-1').html('Accepted file formats: xls (template) &nbsp;&nbsp;&nbsp;');
			}
            else {
				form_elem.find('.accepted-format-1').html('Accepted file formats: pdf,zip,rar,jpeg,png,csv,docx &nbsp;&nbsp;&nbsp;');
			}
        })
        /** Event before Modal closes  */
        .on('hide.bs.modal', function(e) {
            /** Resets the Form and bindings   */
            error_cntr.hide();
            form_elem.attr('action', '');
            form_elem.find('input').val('');

            $("#sec_sheet_hdr").remove();
            $("#sec_sheet").remove();
            $("#sec_sheet_info").remove();

            $(UPLOD_BTN).prop('disabled', false).text('Save');
            MODAL_UPLOD.unbind();
        })
        .modal('show');
}

/**------------------------------------------------------------------------
|	Uploads the via AJAX Post Request
|--------------------------------------------------------------------------
|	@param [in]		post_url    --- URL of the POST Request
|                   error_cntr  --- Error container element
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function postUpload(post_url, error_cntr)
{
    /** Variable Definition         */
    var formData    = new FormData($('#file-uploader-form')[0]);
    var srv_message =   '';

    /** Display current Status      */
    error_cntr.show();
    error_cntr.html('Uploading file....');
	error_cntr.parent().parent().append('<div class="list-group-item progressbar-cntr"><div class="progress progress-striped active"><div class="progress-bar progress-bar-info" style="width: 0%;"></div></div></div>');

    $.ajax({
        url: post_url,
        data: formData,
        type: 'POST',
        dataType: 'json',
        contentType: false,
        processData: false,
		xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', function(evt) {
                    var percent = (evt.loaded / evt.total) * 100;
                    error_cntr.parent().parent().find(".progress-bar").width(percent + "%");
                }, false);
            }
            return xhr;
        },
        /** Successful AJAX Request */
        success: function(svr_resp) {
            error_cntr.parent().parent().find('.progressbar-cntr').remove();
            /** All inputs are valid        */
            if (STS_OK == svr_resp.sts) {
                /** Determines the action according to Server response  */
                switch (svr_resp.action) {

                    /** Overwrites the container    */
                    case OVERWRITE_INFO_ACT:
                        MODAL_UPLOD.modal('hide');
                        $(svr_resp.refresh_cntr).html(svr_resp.refresh_value);
                        $(svr_resp.refresh_cntr).parent().find('span.progress-required-counter').text('1');

                        if ("sec_sheet_refresh" in svr_resp) {
                            $(svr_resp.sec_sheet_cntr).html(svr_resp.sec_sheet_refresh);
                        }
                        break;

					case MODAL_CONFIRM:
                        /*$.ajax({
                            url: BaseURL + '/update_with_new_gis',
                            success: function(){
                                MODAL_UPLOD.modal('hide');
                                location.reload();
                            }
                        });*/
						MODAL_UPLOD.modal('hide');
						$('#modal-confirm-sec-override').modal().on('hide.bs.modal', function(){
							location.reload(true);
						});
						$('#modal-confirm-sec-override .confirm-yes').off('click').on('click', function(e){
							e.preventDefault();
							$.ajax({
								url: BaseURL + '/update_with_new_gis',
								success: function(){
									$('#modal-confirm-sec-override').modal('hide');
								}
							});
						});
						break;

                    /** Reloads the whole page      */
                    default:
                        location.reload();
                        break;
                }

                /** Updates the Progress Bar    */
                update_progress_signs();
            }
            /** There are invalid inputs    */
            else {
                /**	Formats the warnings received from the Server	*/
                for (idx = 0; idx < svr_resp.messages.length; idx++) {
                    srv_message += svr_resp.messages[idx] +'<br/>';
                }

                /**	Display the warning								*/
                error_cntr.show();
                error_cntr.html(srv_message);

                /** Resets the Save button when there are errors    */
                $(UPLOD_BTN).prop('disabled', false).text('Save');
            }
        }
    });
}

/**------------------------------------------------------------------------
|	Sends a Post request to the Server via AJAX
|--------------------------------------------------------------------------
|	@param [in]		ajax_url    --- URL of the Post request
|                   post_data   --- Data to be passed to the Post request
|                   error_cntr  --- Container of the Error in Obj data type
|                   file_form   --- Indicates form with file inputs
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var postInfo = function(ajax_url, post_data, error_cntr, file_form)
{
    /** Variable Definition    */
    var srv_message     =   '';
    var file_allowed    = true;
    var content_allowed = 'application/x-www-form-urlencoded; charset=UTF-8';

    /** When form has a file input  */
    if (STS_OK == file_form) {
        file_allowed    = false;
        content_allowed = false;
    }

	if(!file_allowed){
		error_cntr.parent().append('<div class="list-group-item progressbar-cntr"><div class="progress progress-striped active"><div class="progress-bar progress-bar-info" style="width: 0%;"></div></div></div>');
	}

    error_cntr.hide();

    /** Send Post Request   */
     $.ajax({
        url: ajax_url,
        data: post_data,
        type: 'POST',
        dataType: 'json',
        contentType: content_allowed,
        processData: file_allowed,
		xhr: function() {
			var xhr = $.ajaxSettings.xhr();
			if(!file_allowed){
				if (xhr.upload) {
					xhr.upload.addEventListener('progress', function(evt) {
						var percent = (evt.loaded / evt.total) * 100;
						error_cntr.parent().find(".progress-bar").width(percent + "%");
					}, false);
				}
			}
			return xhr;
        },
        /** Successful AJAX Request */
        success: function(svr_resp) {
			error_cntr.parent().find('.progressbar-cntr').remove();
            /** The Data has been saved in the Database */
            if (STS_OK == svr_resp.sts) {

                /** Checks for the Action to be performed according to Server Response  */
                switch (svr_resp.action) {

                    /** Refresh data from the specfied container */
                    case REFRESH_CNTR_ACT:
                        var refresh_url = svr_resp.refresh_url; /** URL to get the data */

                        var is_premium = svr_resp.is_premium;

                        if(is_premium) {
                            window.location.replace(svr_resp.refresh_url);
                        }

                        /** Elements to be loaded in the Container  */
                        if ('' != svr_resp.refresh_elems) {
                            refresh_url = refresh_url+' '+svr_resp.refresh_elems;
                        }

                        /** Displays the Loading image while Processing AJAX Request    */
                        $(svr_resp.refresh_cntr).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

                        /** For Business Details 1 - Entity Type    */
                        if ('#biz-details-cntr' == svr_resp.refresh_cntr) {
                            /** Shows Sections for Corporations */
                            if ('0' == svr_resp.entity_type) {
                                $('#management-list-cntr').show();
                                $('#cap-details-list-cntr').show();
                                $('#nav_ownerdetails').text('Key Managers');
                            }
                            /** Hides Sections for Sole Proprietorship    */
                            else {
                                $('#management-list-cntr').hide();
                                $('#cap-details-list-cntr').hide();
                                $('#nav_ownerdetails').text(trans('messages.owner_details'));
                            }
                        }
                        
                        if(svr_resp.refresh_elems == '.reload-fs-template') {
                            $('#totalAsset').show();
                            $('#totalAssetGrouping').show();
                        }
                        
                        if ("total_asset_grouping" in svr_resp) {
                            $('[data-name="biz_details.total_asset_grouping"]').html(svr_resp.total_asset_grouping.label);
                        }

                        if ("total_asset" in svr_resp) {
                            $('[data-name="biz_details.total_assets"]').html(svr_resp.total_asset);
                        }

                        $(svr_resp.refresh_cntr).load(refresh_url, function(){
                            /** Updates Progress Bar    */
                            //console.log('update progress');
                            update_progress_signs();
                        });
                        break;

                    /** Changes the data in a specified container   */
                    case OVERWRITE_INFO_ACT:
                        /** Display the updated data to the specified element   */
                        $(svr_resp.refresh_cntr).html(svr_resp.refresh_value);

                        /** Change class to editable field after update         */
                        $(svr_resp.refresh_cntr).removeAttr("style");
                        $(svr_resp.refresh_cntr).addClass('editable-field');
                        $(svr_resp.refresh_cntr).removeClass('edit-input-cntr');
                        
                        /** Fields with Special Conditions, mostly multi part forms */
                        /** Business Details 2 - Major Customer / Supplier: Years doing business calculation    */
                        if ("year_cntr" in svr_resp) {
                            $(svr_resp.year_cntr).html(svr_resp.year_value);
                        }
                        /** Business Details 1: Industry        */
                        else if ("industry" in svr_resp) {
                            $('.industry-sub-title').html(svr_resp.industry.sub_title);
                            $('.industry-row-title').html(svr_resp.industry.row_title);
                        }
                        /** Business Details 1 - Major Location */
                        else if ("province" in svr_resp)  {
                            $(svr_resp.province.city_cntr).html(svr_resp.province.city_name);
                            $(svr_resp.province.zip_cntr).html(svr_resp.province.zipcode);
                        }
                        /** Business Details 1 - Affiliates     */
                        else if ("affiliates_province" in svr_resp)  {
                            $(svr_resp.affiliates_province.city_cntr).html(svr_resp.affiliates_province.city_name);
                            $(svr_resp.affiliates_province.zip_cntr).html(svr_resp.affiliates_province.zipcode);
                        }
						else if ("keymanager_province" in svr_resp)  {
                            $(svr_resp.keymanager_province.city_cntr).html(svr_resp.keymanager_province.city_name);
                            $(svr_resp.keymanager_province.zip_cntr).html(svr_resp.keymanager_province.zipcode);
                        }
						else if ("owner_province" in svr_resp)  {
                            $(svr_resp.owner_province.city_cntr).html(svr_resp.owner_province.city_name);
                            $(svr_resp.owner_province.zip_cntr).html(svr_resp.owner_province.zipcode);
                        }
                        else if ("purpose_others" in svr_resp)  {
                            if (1 == svr_resp.purpose_show) {
                                $(svr_resp.purpose_others).show();
                            }
                            else {
                                $(svr_resp.purpose_others).hide();
                            }

                            if (1 == svr_resp.terms_show) {
                                $(svr_resp.terms_select).show();
                            }
                            else {
                                $(svr_resp.terms_select).hide();
                            }
                        } else if ("total_asset_grouping" in svr_resp) {
                            $('[data-name="biz_details.total_asset_grouping"]').html(svr_resp.total_asset_grouping.label);
                        }

                        /*  When form submission is successful loads the Queued Editable    */
                        if (STS_OK == flg_form_exist) {
                            editable_lnk_clk.click();
                        }

                        /*  Updates the Progress Bar */
                        update_progress_signs();

						generateGISTemplate(true);
                        break;

                    /** Reloads the whole webpage   */
                    case REFRESH_PAGE_ACT:
                    default:
                        location.reload();
                        break;
                }

                error_cntr.hide();
                MODAL_CNTR.modal('hide');
            }
            /** Data was not saved in the Database  */
            else {
                /**	Formats the warnings received from the Server	*/
                for (idx = 0; idx < svr_resp.messages.length; idx++) {
                    srv_message += svr_resp.messages[idx] +'<br/>';
                }

                /** Resets the Queued editable when Form submission is not successful   */
                flg_form_exist      = STS_NG;
                editable_lnk_clk    = null;

                /**	Display the warning								*/
                error_cntr.html(srv_message);
                error_cntr.show();

                /**	Scrolls to top of Modal when error are displayed    */
                MODAL_CNTR.animate({ scrollTop: 0 }, 'slow');

                /**	Resets the Save buttons     */
                $(EXP_UPD_BTN).prop('disabled', false).val('Save');
                $(UPD_BTN).prop('disabled', false).text('Save');

                /** Conditional processing  */
                if ("major_customer" in svr_resp) {
                    if ($('#anonymous-report').prop('checked')) {
                        $('[name="customer_name[0]"]').prop('disabled', true).val('Major Customer');
                    }
                }
                else if ("major_supplier" in svr_resp) {
                    if ($('#anonymous-report').prop('checked')) {
                        $('[name="supplier_name[0]"]').prop('disabled', true).val('Major Supplier');
                    }
                }
            }
        },
        /** There is an Error in the AJAX Request   */
        /* error: function(data) {
            window.location.replace(BaseURL+'/errors');
        } */
    });
}

/**------------------------------------------------------------------------
|	Initialization of Expander Forms
|--------------------------------------------------------------------------
|	@param [in]		expand_cntr     --- HTML element where form is loaded
|                   form_sel        --- Form selector of the Form in string
|                   get_url         --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var initExpandForm = function(expand_cntr, form_sel, get_url)
{
    /** Loads the Form on the specified Container   */
    showFormExpander(expand_cntr, form_sel, get_url);
}

/**------------------------------------------------------------------------
|	Loads the Form on the specified Container
|--------------------------------------------------------------------------
|	@param [in]		expand_cntr     --- HTML element where form is loaded
|                   form_sel        --- Form selector of the Form in string
|                   get_url         --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var showFormExpander = function(expand_cntr, form_sel, get_url)
{
    /** Closes previously loaded Expander Forms */
    $('.expander-shown').empty().removeClass('expander-shown');

    /** Adds a class to tell that the expander is displayed */
    $(expand_cntr).addClass('expander-shown');

    /** Displays the Loading image while AJAX Request is Processing */
    $(expand_cntr).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

    /** Loads the Form on the specified Container   */
    $(expand_cntr).load(get_url, function() {
        bindSubmitExpander(form_sel, get_url);
        closeFormExpander();
        return false;
    });
}

/**------------------------------------------------------------------------
|	Closes the Form Expander
|--------------------------------------------------------------------------
|	@param [in]		expand_cntr     --- HTML element where form is loaded
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var closeFormExpander = function()
{
    /** Loads the Form on the specified Container   */
    $('.close-expanders').click(function() {

        $('.expander-shown').empty().removeClass('expander-shown');

        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Submit event to the Expander form
|--------------------------------------------------------------------------
|	@param [in]		form_sel        --- Form selector of the Form in string
|                   get_url         --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindSubmitExpander = function(form_sel, get_url)
{
    var upd_form    = $(form_sel);      /** Form selector   */

    /** Form is submitted   */
    upd_form.submit(function() {

        /** Validation via JQuery Validation Engine is okay */
        if (upd_form.validationEngine('validate')) {
            var error_cntr  = $('.pop-error-msg');      /** Form Error Container            */
            var post_url    = upd_form.attr('action');  /** Action attribute of the Form    */

            /** Disables Save button while Processing AJAX Request    */
            $(EXP_UPD_BTN).prop('disabled', true).val('Saving...');

            /** Form has file Input */
            if (upd_form.attr('enctype') == 'multipart/form-data') {
                var post_data   = new FormData(upd_form[0]);
                var file_form   = 1;
            }
            /** Form has no file input  */
            else {
                var post_data   = upd_form.serialize();
                var file_form   = 0;
            }

            /** Send Post request via AJAX  */
            postInfo(post_url, post_data, error_cntr, file_form);
        }

        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Date Picker to the text inputs
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var bindDateInput = function()
{
    /** Adds the JQuery UI Datepicker to the Input field    */
    $(".withdate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '1800:+10',
        onChangeMonthYear: function(y, m, i) {
            var d = i.selectedDay;
            $(this).datepicker('setDate', new Date(y, m - 1, d));
        }
    });

    /** Business Details 2: Future Growth Initiative    */
    /** Adds the JQuery UI Datepicker without days.     */
    $('input[name="proj_benefit_date"]').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm',
        beforeShow: function(dateText, inst) {
            $('#dp-style').html('<style> .ui-datepicker-calendar { display: none; } </style>');
        },
        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month));
            $('#dp-style').html('');
        },
        onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
    });
}

/**------------------------------------------------------------------------
|	Binds the Delete links to the Click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var deleteInfo = function()
{
    /** Binds the click event to the delete button  */
    $('#page-wrapper').on('click', '.delete-info', function() {

        var refresh_url     = $(this).attr('href');
        var refresh_cntr    = $(this).data('cntr');
        var refresh_elems   = $(this).data('reload');

        /** Displays a confirmation box before deletion */
        var conf_delete = $("<div> "+trans('messages.delete_modal1')+" </div>").dialog({
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            modal: true,
            open: function(event, ui) {
                /** Remove the close button */
                $(".ui-dialog-titlebar-close").hide();
                $(".ui-dialog-title").append('<span id = "delete_hdr"> <i class = "glyphicon glyphicon-exclamation-sign"> </i> '+trans('messages.delete_modal2')+' </span>');
            },
            buttons: [
				{
					text: trans('messages.cancel'),
					click: function() {
						$('#delete_hdr').remove();
						$(this).dialog('destroy');
					}
				},{
					text: trans('messages.yes'),
					click: function() {
                        let rt = $("#report_type").val();
                        if ('#ownerdetails' == refresh_cntr) {
                            $.get(refresh_url, function() {
                                location.reload();
                            });
                        }
                        else {
                            if(rt == 1) {
                                $.get(refresh_url, function() {
                                    location.reload();
                                });
                            } else {
                                $('#delete_hdr').remove();
                                $(this).dialog('close');

                                if($('#count_fs_template').val() == 1){
                                    $('#total_assets_bizdetails').html(0.00);
                                    $('#total_assets_grouping_bizdetails').html('');
                                    $('#totalAsset').hide();
                                    $('#totalAssetGrouping').hide();
                                }

                                /** Displays loading image while Processing AJAX Request    */
                                $(refresh_cntr).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

                                $(refresh_cntr).load(refresh_url+' '+refresh_elems, function(){
                                    /** Updates the Progess Bar */
                                    update_progress_signs();
                                });
                            }
                        }
					}
				}
			]
        });

        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Delete links to the Click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var deleteFile = function()
{
    /** Binds the click event to the delete button  */
    $('#page-wrapper').on('click', '.delete-file', function() {

        var refresh_url     = $(this).attr('href');
        var refresh_cntr    = $(this).data('cntr');
        var refresh_elems   = $(this).data('reload');

        /** Displays a confirmation box before deletion */
        var conf_delete = $("<div>"+trans('messages.delete_confirmation')+"</div>").dialog({
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            modal: true,
            open: function(event, ui) {
                /** Remove the close button */
                $(".ui-dialog-titlebar-close").hide();
                $(".ui-dialog-title").append('<span id = "delete_hdr"> <i class = "glyphicon glyphicon-exclamation-sign"> </i> '+trans('messages.delete')+'</span>');
            },
            buttons: [
				{
					text: trans('messages.cancel'),
					click: function() {
						$('#delete_hdr').remove();
						$(this).dialog('destroy');
					}
				},{
					text: trans('messages.yes'),
					click: function() {
                        let rt = $("#report_type").val();
                        if ('#ownerdetails' == refresh_cntr) {
                            $.get(refresh_url, function() {
                                location.reload();
                            });
                        }
                        else {
                            if(rt == 1) {
                                $.get(refresh_url, function() {
                                    location.reload();
                                });
                            } else {
                                $('#delete_hdr').remove();
                                $(this).dialog('close');

                                /** Displays loading image while Processing AJAX Request    */
                                $(refresh_cntr).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

                                $(refresh_cntr).load(refresh_url+' '+refresh_elems, function(){
                                    /** Updates the Progess Bar */
                                    update_progress_signs();
                                });
                            }
                        }
					}
				}
			]
        });

        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Delete links to the Click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var deleteUbill = function()
{
    /** Binds the click event to the delete button  */
    $('#page-wrapper').on('click', '.delete-ubill', function() {

        var refresh_url     = $(this).attr('href');
        var refresh_cntr    = $(this).data('cntr');
        var refresh_elems   = $(this).data('reload');

        /** Displays a confirmation box before deletion */
        var conf_delete = $("<div> Are you sure you want to delete this file? </div>").dialog({
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            modal: true,
            open: function(event, ui) {
                /** Remove the close button */
                $(".ui-dialog-titlebar-close").hide();
                $(".ui-dialog-title").append('<span id = "delete_hdr"> <i class = "glyphicon glyphicon-exclamation-sign"> </i> Delete File </span>');
            },
            buttons: [
				{
					text: trans('messages.cancel'),
					click: function() {
						$('#delete_hdr').remove();
						$(this).dialog('destroy');
					}
				},{
					text: trans('messages.yes'),
					click: function() {
                        let rt = $("#report_type").val();
                        if ('#ownerdetails' == refresh_cntr) {
                            $.get(refresh_url, function() {
                                location.reload();
                            });
                        }
                        else {
                            if(rt == 1) {
                                $.get(refresh_url, function() {
                                    location.reload();
                                });
                            } else {
                                $('#delete_hdr').remove();
                                $(this).dialog('close');

                                /** Displays loading image while Processing AJAX Request    */
                                $(refresh_cntr).html('<img src = "'+BaseURL+'/images/preloader.gif" />');

                                $(refresh_cntr).load(refresh_url+' '+refresh_elems, function(){
                                    /** Updates the Progess Bar */
                                    update_progress_signs();
                                });
                            }
                        }
					}
				}
			]
        });

        return false;
    });
}

/**------------------------------------------------------------------------
|	Refocus on the Previous Modal when the topmost Modal has been closed
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var refocusModal = function()
{
    /** Refocus on the Previous Modal when the topmost Modal has been closed    */
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
}

/**------------------------------------------------------------------------
|	Detect if the Browser supports FormData
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var detectFormData = function()
{
    var support = STS_NG;

    if (window.FormData !== undefined) {
        support = STS_OK;
    }

    setCookie('formdata_support', 0, support);
}

/**------------------------------------------------------------------------
|	Removes binding of AJAX Pop-up uploaders
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var turnOffAjaxUpload = function()
{
    var support = getCookie('formdata_support');

    if (STS_NG == support) {
        $('.pop-file-uploader').remove();
        $('.show-certifications-expand').removeClass('show-certifications-expand');
        $('.show-financials-expand').removeClass('show-financials-expand');
        $('#edit-biz-details').show();
    }
}

var generateGISTemplate = function(remove_class){
	/** Checks if system can generate SEC GIS */
	if($.trim($('a[data-header="SEC General Information Sheet"]').parent().find('.progress-required-counter').text()) == "0"){
		if($.trim($('.editable-field[data-name="biz_details.companyname"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.company_tin"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.tin_num"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.sec_reg_date"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.email"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.address1"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.province"]').text())!=""
		)

		if($('#sec_gen_create_modal').hasClass('modal_active')){
			if(remove_class){
				$('#sec_gen_create_modal').removeClass('modal_active');
			}
			$('#sec_gen_create_modal').modal();
			$('#sec_gen_create_yes').click(function(e){
				e.preventDefault();

				$('#sec_gen_create_modal .modal-body p').text('Generating SEC General Information Sheet. Please wait...');
				$('#sec_gen_create_modal .modal-footer #sec_gen_create_yes').hide();
				$('#sec_gen_create_modal .modal-footer #sec_gen_create_close').hide();
				$.ajax({
					url: BaseURL + '/sme/create_sec_gen_sheet',
					data: {
						entity_id: $('#entityids').val(),
						type: $('#entity_type').val()
					},
					type: 'POST',
					dataType: 'json',
					success: function(response) {
						$('#sec_gen_create_modal .modal-body p').html('SEC GIS Generated successfully');
						location.reload(true);
					}
				});
			});
		}
	}
}

var generateGISTemplateManualLink = function(){
	if($.trim($('a[data-header="SEC General Information Sheet"]').parent().find('.progress-required-counter').text()) == "0"){
		if($.trim($('.editable-field[data-name="biz_details.companyname"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.company_tin"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.tin_num"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.sec_reg_date"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.email"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.address1"]').text())!="" &&
			$.trim($('.editable-field[data-name="biz_details.province"]').text())!=""
		) {
			$('#sec-generation-'+$('#entityids').val()).html('<a href="#" class="manual-gis-trigger">Generate GIS Template</a>');
			$('.manual-gis-trigger').click(function(){
				generateGISTemplate(false);
			});
		}
	}
}

/**------------------------------------------------------------------------
|	Shows the Modal Pop-up
|--------------------------------------------------------------------------
|	@param [in]		hdr_txt     --- Header to be displayed on the Title
|                   ajax_url    --- Url of the Form to be loaded via AJAX
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
var setReportAnonymous = function()
{
    var report_id   = $('#entityids').val();
    var report_sts  = $('#status').val();
    var report_ano  = $('#anonymous-report');

    report_ano.change(function() {

        if (1 == $(this).prop('checked')) {
            // Set fields to Classified Information
            $('.anonymous-info').text('Classified Information (Turn off Anonymous report to edit)');
            $('.hdr-company-name').text('Company - '+report_id);

            // Remove editable class
            $('.anonymous-info').removeClass('editable-field');
            $('.anonymous-info').addClass('classified-info');

            // AJAX Call to get data and Save value
            $.get(BaseURL+'/set_anonymous/1/'+report_id, function(srv_resp) {
                /** Updates the Progress Bar    */
                update_progress_signs();
            });
        }
        else {
            // AJAX Call to get data and Save value
            $.get(BaseURL+'/set_anonymous/0/'+report_id);

            $.get(BaseURL+'/get/company_profile/'+report_id, function(srv_resp) {
                $('[data-name="biz_details.companyname"]').text(srv_resp.report.companyname);
                $('.hdr-company-name').text(srv_resp.report.companyname);
                $('[data-name="biz_details.company_tin"]').text(srv_resp.report.company_tin);
                $('[data-name="biz_details.tin_num"]').text(srv_resp.report.tin_num);
                $('[data-name="biz_details.email"]').text(srv_resp.report.email);

                $('[data-name="biz_details.address1"]').text(srv_resp.report.address1);

                $('[data-name="biz_details.phone"]').text(srv_resp.report.phone);
                $('[data-name="biz_details.no_yrs_present_address"]').text(srv_resp.report.no_yrs_present_address);
                $('[data-name="biz_details.date_established"]').text(srv_resp.report.date_established);
                $('[data-name="biz_details.number_year"]').text(srv_resp.report.number_year);
                $('[data-name="biz_details.number_year_management_team"]').text(srv_resp.report.number_year_management_team);

                $('[data-name="major_cust.customer_name"]').each(function(){
                    idx = $.inArray(parseInt($(this).attr('data-field-id')), srv_resp.cust_id);
                    $(this).text(srv_resp.cust_name[idx]);
                });

                $('[data-name="major_supp.supplier_name"]').each(function(){
                    idx = $.inArray(parseInt($(this).attr('data-field-id')), srv_resp.supp_id);
                    $(this).text(srv_resp.supp_name[idx]);
                });

                // Add editable class
                $('.anonymous-info').addClass('editable-field');
                $('.anonymous-info').removeClass('classified-info');

                /** Updates the Progress Bar    */
                update_progress_signs();
            },'json');

        }

        $('.hdr-company-name').show();
    });

    if (1 == $('#report_anonymous').val()) {
        report_ano.prop('checked', true);
        report_ano.click();
    }
}

$(document).ready(function() {

    detectFormData();
    turnOffAjaxUpload();
    bindGetEditableField();
    bindPostEditableField();
    cancelEditableField();
    showEditableFields();
    deleteInfo();
    deleteFile();
    deleteUbill();
    tab();

    initPopEditableUplod();
    refocusModal();
	generateGISTemplateManualLink();

    setReportAnonymous();
});