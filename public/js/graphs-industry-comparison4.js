//======================================================================
//  Script S39: graphs-industry-comparison4.js
//      Chart for Industry comparison of Current Ratio
//======================================================================
$(function () {

    //-----------------------------------------------------
    //  Binding S39.1: #container-bar4
    //      Displays the Industry comparison Chart of Current Ratio
    //-----------------------------------------------------
    $('#container-bar4').highcharts({
        chart: {
            type: 'bar',
			events: {
				redraw: function() {
					$('.redraw-ind-cmp4').remove();
				}
			}
        },
        exporting: {
            buttons: {
                contextButtons: {
                    enabled: false,
                    menuItems: null
                }
            },
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [trans('risk_rating.ic_chart_4')],
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'14px'
                }
            }
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'12px'
                }
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    //format: '{y:.2f}'
                    formatter: function() {
                        if (this.y === 0) {
                            return this.series.name +' : <b>' + trans('risk_rating.ratio_unavailable') + '</b>';
                        }

                        return Highcharts.numberFormat(this.y, 2);
                    }
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Industry',
            color: "#aaaaaa",
            data: [parseFloat($("#current_ratio").val())]
        },{
            name: 'Company',
            color: "#BDCF8E",
            data: [parseFloat($("#current_ratio22").val())]
        }],
        tooltip: {
            formatter: function() {
                console.log(this);
                if (this.y === 0) {
                    value = trans('risk_rating.ratio_unavailable');
                } else {
                    value = Highcharts.numberFormat(this.y, 2);
                }
                return this.key +
                    '<br/>' + '<span style="color:'+
                    this.point.color + '">\u25CF</span> ' + this.series.name +
                    ' : <b>'+value+'</b>';
            }
        },
    });
});
