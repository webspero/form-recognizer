//======================================================================
//  Script S36: graphs-industry-comparison.js
//      Graphs for Industry comparison of Gross Revenue Growth
//======================================================================
$(function () {
    var grg = '';
    if (parseFloat($("#gross_revenue_growth22").val()) > parseFloat($("#gross_revenue_growth").val())) {
        grg += trans('risk_rating.growth_high');
    } else if (parseFloat($("#gross_revenue_growth22").val()) == parseFloat($("#gross_revenue_growth").val())) {
        grg += trans('risk_rating.growth_moderate');
    } else if (parseFloat($("#gross_revenue_growth22").val()) < parseFloat($("#gross_revenue_growth").val())) {
        grg += trans('risk_rating.growth_low');
    }
    
    
    var nig = '';
    if (parseFloat($("#net_income_growth22").val()) > parseFloat($("#net_income_growth").val())) {
        nig += trans('risk_rating.net_high');
    } else if (parseFloat($("#net_income_growth22").val()) == parseFloat($("#net_income_growth").val())) {
        nig += trans('risk_rating.net_moderate');
    } else if (parseFloat($("#net_income_growth22").val()) < parseFloat($("#net_income_growth").val())) {
        nig += trans('risk_rating.net_low');
    }

    var gpm = '';
    if (parseFloat($("#gross_profit_margin22").val()*100) > parseFloat($("#gross_profit_margin").val())) {
        gpm += trans('risk_rating.profit_high');
    } else if (parseFloat($("#gross_profit_margin22").val()*100) == parseFloat($("#gross_profit_margin").val())) {
        gpm += trans('risk_rating.profit_moderate');
    } else if (parseFloat($("#gross_profit_margin22").val()*100) < parseFloat($("#gross_profit_margin").val())) {
        gpm += trans('risk_rating.profit_low');
    }
    
    var cr = '';
    if (parseFloat($("#current_ratio22").val()) > parseFloat($("#current_ratio").val()/100)) {
        cr += trans('risk_rating.ratio_high');
    } else if (parseFloat($("#current_ratio22").val()) == parseFloat($("#current_ratio").val()/100)) {
        cr += trans('risk_rating.ratio_moderate');
    } else if (parseFloat($("#current_ratio22").val()) < parseFloat($("#current_ratio").val()/100)) {
        cr += trans('risk_rating.ratio_low');
    }
    
    var der = '';
    if (parseFloat($("#debt_equity_ratio22").val()) > parseFloat($("#debt_equity_ratio").val()/100)) {
        der += trans('risk_rating.debt_high');
    } else if (parseFloat($("#debt_equity_ratio22").val()) == parseFloat($("#debt_equity_ratio").val()/100)) {
        der += trans('risk_rating.debt_moderate');
    } else if (parseFloat($("#debt_equity_ratio22").val()) < parseFloat($("#debt_equity_ratio").val()/100)) {
        der += trans('risk_rating.debt_low');
    }
    
    $(".description-bar").append('<p style="font-size:11px;"><b>'+trans('risk_rating.gross_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_revenue_growth22").val()).toFixed(2)+' vs '+parseFloat($("#gross_revenue_growth").val()).toFixed(2)+'</span></b><br/>'+grg+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.net_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#net_income_growth22").val()).toFixed(2)+' vs '+parseFloat($("#net_income_growth").val()).toFixed(2)+'</span></b><br/>'+nig+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.profit_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_profit_margin22").val()*100).toFixed(2)+' vs '+parseFloat($("#gross_profit_margin").val()).toFixed(2)+'</span></b><br/>'+gpm+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.ratio_vs_industry')+' <span style="color: orange;">'+parseFloat($("#current_ratio22").val()).toFixed(2)+' vs '+parseFloat($("#current_ratio").val()/100).toFixed(2)+'</span></b><br/>'+cr+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.debt_vs_industry')+' <span style="color: orange;">'+parseFloat($("#debt_equity_ratio22").val()).toFixed(2)+' vs '+parseFloat($("#debt_equity_ratio").val()/100).toFixed(2)+'</span></b><br/>'+der+'</p>');
    
    //-----------------------------------------------------
    //  Binding S36.1: #show4
    //      Displays the Graph description when Clicked
    //-----------------------------------------------------
    $('#show4').click(function() {
        if($('#description4').is(':visible')){
			$('#description4').slideUp();
		} else {
			$('#description4').slideDown();
		}
	});
	
    //-----------------------------------------------------
    //  Binding S36.2: #grdp_select
    //      Changes values on graphs according to chosen year's GRDP
    //-----------------------------------------------------
	$('#grdp_select').on("change", function(){
		var uiThis = $(this);
		if(uiThis.val()!=""){
			$.ajax({
				url: BaseURL + '/grdp/get_boost',
				type: 'get',
				dataType: 'json',
				data: {
					year: uiThis.val(),
					entity_id: $('#entityids').val() 
				},
				success: function(oReturn){
					var fBoost = parseFloat(oReturn.boost);
					var fReverseBoost = 1 + (1 - fBoost);
					grg_b = parseFloat($("#gross_revenue_growth").val()) * fBoost;
					nig_b = parseFloat($("#net_income_growth").val()) * fBoost;
					gpm_b = parseFloat($("#gross_profit_margin").val()) * fBoost;
					cr_b = parseFloat($("#current_ratio").val()) * fBoost;
					der_b = parseFloat($("#debt_equity_ratio").val()) * fReverseBoost;
					
					/* var bdisplay = '';
					switch(fBoost){
						case 0.8: bdisplay = '-20%'; break;	
						case 0.9: bdisplay = '-10%'; break;	
						case 1.1: bdisplay = '+10%'; break;	
						case 1.2: bdisplay = '+20%'; break;	
					}
					$('.grdp_display').text('Region: ' + oReturn.region + ' (' +bdisplay+ ')');
					*/
					
					$('.grdp_display').text('Region: ' + oReturn.region);
					
					var grg = '';
					if (parseFloat($("#gross_revenue_growth22").val()) > grg_b) {
						grg += trans('risk_rating.growth_high');
					} else if (parseFloat($("#gross_revenue_growth22").val()) == grg_b) {
						grg += trans('risk_rating.growth_moderate');
					} else if (parseFloat($("#gross_revenue_growth22").val()) < grg_b) {
						grg += trans('risk_rating.growth_low');
					}
					
					
					var nig = '';
					if (parseFloat($("#net_income_growth22").val()) > nig_b) {
						nig += trans('risk_rating.net_high');
					} else if (parseFloat($("#net_income_growth22").val()) == nig_b) {
						nig += trans('risk_rating.net_moderate');
					} else if (parseFloat($("#net_income_growth22").val()) < nig_b) {
						nig += trans('risk_rating.net_low');
					}

					var gpm = '';
					if (parseFloat($("#gross_profit_margin22").val()*100) > gpm_b) {
						gpm += trans('risk_rating.profit_high');
					} else if (parseFloat($("#gross_profit_margin22").val()*100) == gpm_b) {
						gpm += trans('risk_rating.profit_moderate');
					} else if (parseFloat($("#gross_profit_margin22").val()*100) < gpm_b) {
						gpm += trans('risk_rating.profit_low');
					}
					
					var cr = '';
					if (parseFloat($("#current_ratio22").val()) > (cr_b/100)) {
						cr += trans('risk_rating.ratio_high');
					} else if (parseFloat($("#current_ratio22").val()) == (cr_b/100)) {
						cr += trans('risk_rating.ratio_moderate');
					} else if (parseFloat($("#current_ratio22").val()) < (cr_b/100)) {
						cr += trans('risk_rating.ratio_low');
					}
					
					var der = '';
					if (parseFloat($("#debt_equity_ratio22").val()) > (der_b/100)) {
						der += trans('risk_rating.debt_high');
					} else if (parseFloat($("#debt_equity_ratio22").val()) == (der_b/100)) {
						der += trans('risk_rating.debt_moderate');
					} else if (parseFloat($("#debt_equity_ratio22").val()) < (der_b/100)) {
						der += trans('risk_rating.debt_low');
					}
					$(".description-bar").html('<p style="font-size:11px;"><b>'+trans('risk_rating.gross_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_revenue_growth22").val()).toFixed(2)+' vs '+grg_b.toFixed(2)+'</span></b><br/>'+grg+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.net_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#net_income_growth22").val()).toFixed(2)+' vs '+nig_b.toFixed(2)+'</span></b><br/>'+nig+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.profit_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_profit_margin22").val()*100).toFixed(2)+' vs '+gpm_b.toFixed(2)+'</span></b><br/>'+gpm+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.ratio_vs_industry')+' <span style="color: orange;">'+parseFloat($("#current_ratio22").val()).toFixed(2)+' vs '+(cr_b/100).toFixed(2)+'</span></b><br/>'+cr+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.debt_vs_industry')+' <span style="color: orange;">'+parseFloat($("#debt_equity_ratio22").val()).toFixed(2)+' vs '+(der_b/100).toFixed(2)+'</span></b><br/>'+der+'</p>');
					
					$('#container-bar').highcharts().series[0].setData([grg_b]);
					$('#container-bar2').highcharts().series[0].setData([nig_b]);
					$('#container-bar3').highcharts().series[0].setData([gpm_b]);
					$('#container-bar4').highcharts().series[0].setData([(cr_b/100)]);
					$('#container-bar5').highcharts().series[0].setData([(der_b/100)]);
				}
			});
		} else {
			$('.grdp_display').text('');
			
			var grg = '';
			if (parseFloat($("#gross_revenue_growth22").val()) > parseFloat($("#gross_revenue_growth").val())) {
				grg += trans('risk_rating.growth_high');
			} else if (parseFloat($("#gross_revenue_growth22").val()) == parseFloat($("#gross_revenue_growth").val())) {
				grg += trans('risk_rating.growth_moderate');
			} else if (parseFloat($("#gross_revenue_growth22").val()) < parseFloat($("#gross_revenue_growth").val())) {
				grg += trans('risk_rating.growth_low');
			}
			
			
			var nig = '';
			if (parseFloat($("#net_income_growth22").val()) > parseFloat($("#net_income_growth").val())) {
				nig += trans('risk_rating.net_high');
			} else if (parseFloat($("#net_income_growth22").val()) == parseFloat($("#net_income_growth").val())) {
				nig += trans('risk_rating.net_moderate');
			} else if (parseFloat($("#net_income_growth22").val()) < parseFloat($("#net_income_growth").val())) {
				nig += trans('risk_rating.net_low');
			}

			var gpm = '';
			if (parseFloat($("#gross_profit_margin22").val()*100) > parseFloat($("#gross_profit_margin").val())) {
				gpm += trans('risk_rating.profit_high');
			} else if (parseFloat($("#gross_profit_margin22").val()*100) == parseFloat($("#gross_profit_margin").val())) {
				gpm += trans('risk_rating.profit_moderate');
			} else if (parseFloat($("#gross_profit_margin22").val()*100) < parseFloat($("#gross_profit_margin").val())) {
				gpm += trans('risk_rating.profit_low');
			}
			
			var cr = '';
			if (parseFloat($("#current_ratio22").val()) > parseFloat($("#current_ratio").val()/100)) {
				cr += trans('risk_rating.ratio_high');
			} else if (parseFloat($("#current_ratio22").val()) == parseFloat($("#current_ratio").val()/100)) {
				cr += trans('risk_rating.ratio_moderate');
			} else if (parseFloat($("#current_ratio22").val()) < parseFloat($("#current_ratio").val()/100)) {
				cr += trans('risk_rating.ratio_low');
			}
			
			var der = '';
			if (parseFloat($("#debt_equity_ratio22").val()) > parseFloat($("#debt_equity_ratio").val()/100)) {
				der += trans('risk_rating.debt_high');
			} else if (parseFloat($("#debt_equity_ratio22").val()) == parseFloat($("#debt_equity_ratio").val()/100)) {
				der += trans('risk_rating.debt_moderate');
			} else if (parseFloat($("#debt_equity_ratio22").val()) < parseFloat($("#debt_equity_ratio").val()/100)) {
				der += trans('risk_rating.debt_low');
			}
			$(".description-bar").html('<p style="font-size:11px;"><b>'+trans('risk_rating.gross_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_revenue_growth22").val()).toFixed(2)+' vs '+parseFloat($("#gross_revenue_growth").val()).toFixed(2)+'</span></b><br/>'+grg+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.net_growth_vs_industry')+' <span style="color: orange;">'+parseFloat($("#net_income_growth22").val()).toFixed(2)+' vs '+parseFloat($("#net_income_growth").val()).toFixed(2)+'</span></b><br/>'+nig+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.profit_vs_industry')+' <span style="color: orange;">'+parseFloat($("#gross_profit_margin22").val()*100).toFixed(2)+' vs '+parseFloat($("#gross_profit_margin").val()).toFixed(2)+'</span></b><br/>'+gpm+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.ratio_vs_industry')+' <span style="color: orange;">'+parseFloat($("#current_ratio22").val()).toFixed(2)+' vs '+parseFloat($("#current_ratio").val()/100).toFixed(2)+'</span></b><br/>'+cr+'</p><p style="font-size:11px;"><b>'+trans('risk_rating.debt_vs_industry')+' <span style="color: orange;">'+parseFloat($("#debt_equity_ratio22").val()).toFixed(2)+' vs '+parseFloat($("#debt_equity_ratio").val()/100).toFixed(2)+'</span></b><br/>'+der+'</p>');
			
			$('#container-bar').highcharts().series[0].setData([parseFloat($("#gross_revenue_growth").val())]);
			$('#container-bar2').highcharts().series[0].setData([parseFloat($("#net_income_growth").val())]);
			$('#container-bar3').highcharts().series[0].setData([parseFloat($("#gross_profit_margin").val())]);
			$('#container-bar4').highcharts().series[0].setData([parseFloat($("#current_ratio").val())/100]);
			$('#container-bar5').highcharts().series[0].setData([parseFloat($("#debt_equity_ratio").val())/100]);
		}
	});
	
    //-----------------------------------------------------
    //  Binding S36.3: #container-bar
    //      Displays the Industry comparison Chart of Gross Revenue
    //-----------------------------------------------------
    $('#container-bar').highcharts({
        chart: {
            type: 'bar',
			events: {
				redraw: function() {
					$('.redraw-ind-cmp').remove();
				}
			}
        },
        exporting: {
            buttons: {
                contextButtons: {
                    enabled: false,
                    menuItems: null
                }
            },
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [trans('risk_rating.ic_chart_1')],
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'14px'
                }
            }
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'12px'
                }
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: '{y:.2f}',
                    align: 'left',
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Industry',
            color: "#aaaaaa",
            data: [parseFloat($("#gross_revenue_growth").val())]
        },{
            name: 'Company',
            color: "#BDCF8E",
            data: [parseFloat($("#gross_revenue_growth22").val())]
        }]
    });
});