//======================================================================
//  Script S45: popup_hint.js
//      JS modal for "More Info..." link
//======================================================================
$(document).ready(function(){
	
	$('body').on('click', '.popup-hint', function(e){
		e.preventDefault();
		$('#hint_modal').find('.modal-body p').html($(this).data('hint'));
		$('#hint_modal').modal();
	});
});