//======================================================================
//  Script S42: graphs-strategic-forecast.js
//      Chart for Strategic Forecast
//======================================================================
$(function () {
	console.log(forecast_data['industry']);
    //-----------------------------------------------------
    //  Binding S42.1: #container-pie2
    //      Displays the Strategic Forecast Chart
    //-----------------------------------------------------
    $('#forecast_graph').highcharts({
        chart: {
            type: 'line'
        },
        exporting: {
            buttons: {
                contextButtons: {
                    enabled: false,
                    menuItems: null
                }
            },
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            // categories: [$('#base_year').val(), parseInt($('#base_year').val())+1, parseInt($('#base_year').val())+2, parseInt($('#base_year').val())+3],
            type : 'category',
            categories: [  "Q1 " + $('#base_year').val(),"Q2 " + $('#base_year').val(),"Q3 " + $('#base_year').val(), "Q4 " + $('#base_year').val(),
                           "Q1 " + (parseInt($('#base_year').val())+1), "Q2 " + (parseInt($('#base_year').val())+1), "Q3 " + (parseInt($('#base_year').val())+1), "Q4 " + (parseInt($('#base_year').val())+1), 
                           "Q1 " + (parseInt($('#base_year').val())+2),  "Q2 " + (parseInt($('#base_year').val())+2), "Q3 " + (parseInt($('#base_year').val())+2), "Q4 " + (parseInt($('#base_year').val())+2), 
                           "Q1 " + (parseInt($('#base_year').val())+3)
                        ],
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'12px'
                }
            }
        },
        yAxis: {
            title: {
                text: null
            },
			labels: {
                formatter: function () {
                    return this.value + '%';
                },
                style: {
                    fontSize:'14px'
                }
            },
        },
        tooltip: {
            valueSuffix: '%',
            enabled: true,
            useHTML: true,
            formatter: function () {
                var ind = this.point.index;
                var str ="";
                // if(this.series.name == "Industry") {
                    // if(ind > 0) {
                    //     let index = ind - 1;
                    //     var diff = parseFloat(this.point.y - this.series.data[index].y).toFixed(2);
                    //     str =  '<span style="font-size: 10px">' + this.key + '</span><br/>' + '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + diff + '% </b><br/>' ;
                    // } else {
                    //     str = '<span style="font-size: 10px">' + this.key + '</span><br/>' + '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + parseFloat(this.y).toFixed(2) + '% </b><br/>';
                    // }
                // } else {
                //     str ='<span style="font-size: 10px">' + this.key + '</span><br/>' + '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + parseFloat(this.y).toFixed(2) + '% </b><br/>';
                // }
                if(this.series.name == "Industry") {
                    str = '<span style="font-size: 10px">' + this.key + '</span><br/>' + '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + parseFloat(this.y).toFixed(2) + '% </b><br/>';
                } else {
                    str ='<span style="font-size: 10px">' + this.key + '</span><br/>' + '<span style="color:' + this.series.color + '">\u25CF</span> ' + this.series.name + ': <b>' + parseFloat(this.y).toFixed(2) + '% </b><br/>';
                }
                return str;
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true,
            fontSize: '15px'
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Industry',
			color: "#aaaaaa",
            data: (typeof forecast_data != 'undefined') ? forecast_data['industry'] : [],
            connectNulls:true
        },{
            name: 'Company',
            color: "#B1D46B",
            data: (typeof forecast_data != 'undefined') ? forecast_data['company'] : [],
            connectNulls:true
        }]
    });
});