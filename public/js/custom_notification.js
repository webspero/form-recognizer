$(document).ready(function(){
    $("#notifications").on("change", function(){
        if ($(this).is(':checked')) {
            $("input[type='checkbox']").prop('checked', true);
        } else {
            $("input[type='checkbox']").prop('checked', false);
        }
    });
});

$(document).on("click", "img[name='img_status']", function(e){
    if($(this).hasClass("status-cursor")) {
        status = $(this).attr("data-value");
        status = (status == "stop")? 0: 1;
        id = $(this).attr("data-id");
        $.ajax({
            url: "/update_custom",
            data: {
                id: id,
                status: status
            },
            dataType: "json",
            method:'post',
            success: function(resp){
                href = window.location.href;
                $("#table_div").load(href + ' #table_div');
            },
            error: function(e){
                console.log(e)
            }
        })
    }
})

$(document).on("click", "#btnSaveReceiver", function(){
    $("#form_receiver").validate()
    if($("#receiverEmail").val()) {
        var items = [],
            html = "";
        $.each($("input[name='notifications']:checked"), function(){
            items.push($(this).attr("data-id"));
            html += "<input type = 'hidden' name='ids[]' value=" + $(this).attr("data-id")  + " />";
        });
        if(items.length > 0 ) {
            $('#form_receiver').append(html);
        }
        $("#form_receiver").submit();
    }
});

$(document).on("click", "#btnAddEmail", function(e){
    $("#addForm").validate();
    if($("#email").val()) {
        $("#addForm").submit()
    }
});