//======================================================================
//  Script S65: supervisor_dashboard.js
//      JS contain helper functions
//======================================================================
$(document).ready(function () {
    var regionData = [];
    $("#tierFilter").multiselect();
    $("#regionFilter").multiselect();
    
    $.ajax({
        url: 'reports_submitted',
        type: 'get',
        dataType: 'json',
        data: {},
        success: function (result) {
            if(result.success == false){
                var report_counts = [0, 0, 0, 0, 0, 0, 0];
                var report_time = [0, 0, 0, 0, 0, 0, 0];
            }else{
                var report_counts = [
                    result.data.Mon.length,
                    result.data.Tue.length,
                    result.data.Wed.length,
                    result.data.Thu.length,
                    result.data.Fri.length,
                    result.data.Sat.length,
                    result.data.Sun.length
                ];
    
                var report_time = [
                    result.time_avg.Mon,
                    result.time_avg.Tue,
                    result.time_avg.Wed,
                    result.time_avg.Thu,
                    result.time_avg.Fri,
                    result.time_avg.Sat,
                    result.time_avg.Sun
                ]
            }
            reportSubmittedChart = {
                labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
                series: [report_counts]
            };

            if(typeof result.time_avg != "undefined"){
                var tmp_time = result.time_avg.total;
                // Compute for Days, Hours, Minutes
                var get_days = Math.floor(tmp_time / (24 * 60));
                var get_hours = Math.floor((tmp_time - (get_days * 24 * 60)) / 60);
                var get_minutes = Math.floor(tmp_time - ((get_days * 24 * 60) + (get_hours * 60)));
            }
           
            graph_data('#reportSubmittedChart', reportSubmittedChart, report_counts);
            var reports_sum = report_counts.reduce(add);
            $("#reports_sum").text(reports_sum + " Reports Submitted This Week ");

            if (get_days == 0 && result.success == true) {
                var time_text = get_hours + " Hour " + get_minutes + " Minute";
                var y_axis_label = [0, 3, 6, 9, 12, 15, 18, 24];
            } else if(result.success == true){
                var time_text = get_days + " Day " + get_hours + " Hour " + get_minutes + " Minute";
                var y_axis_label = [];
                for (var i = 0; i < report_time.length; i++) {
                    y_axis_label.push({
                        "meta": (report_time[i] / (24 * 60)).toFixed(2),
                        value: Math.ceil(report_time[i] / (24 * 60))
                    });
                }
                report_time = y_axis_label;
            } else {
                time_text = " 0 Hour 0 Minute"
            }

            $("#reports_avg_time").text(time_text);

            avgDataChart = {
                labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
                series: [report_time]
            };

            optAvgChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: Math.max(report_time),
                chartPadding: {
                    top: 50,
                    right: 0,
                    bottom: 0,
                    left: 0
                }
            }

            new Chartist.Line("#avgWeeklyChart", avgDataChart, optAvgChart);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            // Log Error
        }
    });
    
    var pieGraph = function(filter){
        regionData = [];
        $.ajax({
            url: 'reports_industry',
            type: 'get',
            dataType: 'json',
            data: { industry: filter},
            success: function(result){
                for(item in result.data)
                {
                    var rows = [];
                    for( var i = 0; i <  result.data[item].length; i++)
                    {
                        rows.push(result.data[item][i]);
                    }
                    regionData.push(rows);
                }
                if(rows.length == 0){
                    regionPie(true);
                }else{
                    regionPie(false);
                }
            }            
        });
    };  
    pieGraph();
    function add(sum, a) {
        return sum + a;
    }

    function graph_data(field_id, data, report_counts) {
        completedChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: Math.max(report_counts), // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {
                top: 10,
                right: 0,
                bottom: 0,
                left: 0
            },
        }
        new Chartist.Line(field_id, data, completedChart);
    }

    $("input[type=checkbox]").on({
        change: function() {
            var filter = "";
            var field = $(this).parent().parent().parent().parent().parent().prev().attr("id");
            callback = function(result) {
                $.ajax({
                    url: 'reports_industry',
                    type: 'get',
                    dataType: 'json',
                    data: { filter: result},
                    success: function(result){
                        for(item in result.data)
                        {
                            var rows = [];
                            for( var i = 0; i <  result.data[item].length; i++)
                            {
                                rows.push(result.data[item][i]);
                            }
                            regionData.push(rows);
                        }
                        if(rows.length == 0){
                            regionPie(true);
                        }else{
                            regionPie(false);
                        }
                    }            
                });
            }
            
            if(field == "tierFilter") {
                filter = $("#" + field).val();
                pieGraph(filter);
            } else if(field == "regionFilter") {
                var tmp = [];
                regionData = [];
                var len = $("#regionFilter option[selected=selected]").length, x=0;
                $("#regionFilter option[selected=selected]").each(function(index, ths){
                    x++;
                    tmp.push($(ths).val());
                    if(len == x){
                        callback(tmp);
                    }
                });
            } else {
                if(field == undefined) {
                    $("option").trigger("change");
                    pieGraph(true);
                }
            }
        }
    });

    // Data Used is Similar to Portfolio Statistics
    function regionPie(trigger)
    {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Create chart instance
        var chart = am4core.create("m_chart_industry_composition", am4charts.PieChart);

        // Let's cut a hole in our Pie chart the size of 40% the radius
        chart.width = am4core.percent(100);
        chart.height = am4core.percent(100);
        chart.innerRadius = am4core.percent(40);

        // Add and configure Series
        var pieSeries = [];
        var i;
        var industryId;

        for(var i = 0 ; i < regionData.length; i++){
            pieSeries[i] = chart.series.push(new am4charts.PieSeries());
            pieSeries[i].dataFields.value = "y";
            pieSeries[i].dataFields.category = "name";
            pieSeries[i].slices.template.stroke = am4core.color("#fff");
            pieSeries[i].slices.template.strokeWidth = 1;
            if(trigger == true){
                // Used in case data is not found.
                pieSeries[i].slices.template.strokeOpacity = 0.4;
                chart.data= [{
                    "country": "Dummy",
                    "disabled": true,
                    "value": 1000,
                    "color": am4core.color("#dadada"),
                    "opacity": 0.3,
                    "strokeDasharray": "4,4",
                    "tooltip": ""
                }]

                /* Create series */
                var series = chart.series.push(new am4charts.PieSeries());
                series.dataFields.value = "value";
                series.dataFields.category = "country";

                /* Set tup slice appearance */
                var slice = series.slices.template;
                slice.propertyFields.fill = "color";
                slice.propertyFields.fillOpacity = "opacity";
                slice.propertyFields.stroke = "color";
                slice.propertyFields.strokeDasharray = "strokeDasharray";
                slice.propertyFields.tooltipText = "tooltip";

                series.labels.template.propertyFields.disabled = "disabled";
                series.ticks.template.propertyFields.disabled = "disabled";
            }else{
                pieSeries[i].data = regionData[i];
                pieSeries[i].slices.template.strokeOpacity = 1;
            }
            var hide = true;
            if(i== (regionData.length-1)){
                hide = false;
            }
            // Disabling labels and ticks on inner circle
            pieSeries[i].labels.template.disabled = hide;
            pieSeries[i].ticks.template.disabled = hide;
            pieSeries[i].labels.template.bent = false;
            pieSeries[i].labels.template.radius = 35;
            pieSeries[i].labels.template.padding(0,0,0,0);
            
            var listID = [];
            if(i == 2){
                let pieSlice = [];
                for(let x = 0; x < regionData[i].length; x++){
                    pieSlice[x] = regionData[i][x];
                }
                pieSlice.sort(function(a,b){
                    return b.y - a.y;
                })
                var largestSix = [];
                for(let a = 0; a < 6; a++){
                    largestSix[a] = pieSlice[a];
                    listID.push(largestSix[a].count);
                }
            }

            if(largestSix != undefined){
                var num = largestSix[5].y;
            }
            
            //Enabling 6 largest industry on the outer circle
            pieSeries[i].ticks.template.adapter.add("hidden", hideSmall);
            pieSeries[i].labels.template.adapter.add("hidden", hideSmall);
            pieSeries[i].alignLabels = false;

            function hideSmall(hidden, target) {
                if(listID.indexOf(target.dataItem.dataContext.count) > -1) {
                    return false;
                } else {
                    return true;
                }
            }

            // Disable sliding out of slices when clicking and hovering
            pieSeries[i].slices.template.states.getKey("hover").properties.scale = 1;
            pieSeries[i].slices.template.states.getKey("active").properties.shiftRadius = 0;

            pieSeries[i].slices.template.events.on("hit", function(ev) {
                var series = ev.target.dataItem.component;
                series.slices.each(function(item) {
                    i = ev.target.dataItem.dataContext.i;
                    industryId = ev.target.dataItem.dataContext.id;
                })
                let url = '/bank/reportlist/' + i + '/' + industryId;
                document.location.href = url;
            });
        }
    }

    $(".filter-option-inner-inner").text("All");


    $("#bs-example-navbar-collapse-1").on("shown.bs.collapse", function (){
        var collapse = $("#bs-example-navbar-collapse-1").attr("aria-expanded");
        if(collapse){
            $("#page-wrapper").css("display","none");
        }else{
            $("#page-wrapper").css("display","block");
        }
    });
    $("#bs-example-navbar-collapse-1").on("hidden.bs.collapse", function (){
        $("#page-wrapper").css("display","block");
    });

});