//======================================================================
//  Script S43: prev-page-scroll.js
//      Shows the pop-up boxes when user navigates from tabs
//======================================================================

$(document).ready(function() {

var MSG_COOKIE_EXIST    = 1;
var TAB_RATED_EXIST     = 1;
var EXPIRE_1_DAY        = 1;

var USER_ROLE_SME       = '1';
var USER_ROLE_INV       = '2';
var USER_ROLE_ANLYST    = '3';
var USER_ROLE_BANK      = '4';

var RATING_STS_NONE     = 0;
var RATING_STS_DONE     = 7;

var current_tab_open    = '';
var link_clicked        = '';
var link_current        = '';
var next_tab_prompt     = $( "#modal-next-tab-msg" );

/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initNextTabModal()
{
    /** Binds Pop-up message to the Modal                   */
    bindNextTaskModal();
    
    /** Binds the links navigation links to the Modal       */
    bindNextTabToModal();
    
    /** Binds the submit button to the Modal                */
    bindSubmitSMEDetails();
    
    /** Binds the Feedback submit button to click event     */
    submitTabFeedback();
    
}

/**------------------------------------------------------------------------
|	Binds the links navigation links to the Modal
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindNextTabToModal()
{
    /** Variable Definition     */
    var sts         = false;
    var user_role   = $('#userid');
    var user_sts    = $('#status');
    
    //-----------------------------------------------------
    //  Binding S43.1: .pop-next-tab-msg
    //      Displays the Pop-up reminder when changing tabs
    //-----------------------------------------------------
    $('.pop-next-tab-msg').click(function() {
        
        current_tab_open    = $('.tab-content').find('div.active').attr('id');
        link_clicked        = $(this);
        link_current        = $('#side-menu').find('a[data-prompt-msg="'+current_tab_open+'"]');
        
        /** Check if modal should open     */
        switch (user_role.val()) {
            
            case USER_ROLE_SME:
                if (RATING_STS_NONE == user_sts.val()) {
                    /** Open the Modal Message     */
                    sts = openNextTabModal();
                }
                else {
                    sts = true;
                }
                break;
                
            case USER_ROLE_INV:
            case USER_ROLE_ANLYST:
            case USER_ROLE_BANK:
            default:
                sts = true;
                break;
        }
        
        /** When link will not be stopped by Modal              */
        if (sts) {
            /** Scroll to top when navigating to other pages    */
            page_cookie   = getCookie(PAG_LOD_COOKIE);

            if ($(this).attr('id') != page_cookie) {
                $("html, body").animate({ scrollTop: 0 }, "fast");
            }
            
            /** Set the page loaded cookie                      */
            setCookie(PAG_LOD_COOKIE, 0, $(this).attr('id'));
        }
        
        return sts;
    });
}

/**------------------------------------------------------------------------
|	Opens the Message Dialog Box
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function openNextTabModal()
{
    /** Variable Definition */
    var no_msg_cookie       = getCookie(current_tab_open);
    var nav_value           = link_clicked.attr('data-prompt-msg');
    var sts                 = false;
    var modal_sts           = next_tab_prompt.is(':visible');
    
    /** Current Tab is the same as the destination link     */
    if (current_tab_open == nav_value) {
        sts = true;
    }
    /** Message has already been seen during the day        */
    else if (MSG_COOKIE_EXIST == no_msg_cookie) {
        sts = true;
        // sts = bindRateTabQueryModal();
    }
    else if ('sci' == current_tab_open) {
        sts = true;
    }
    /** Display the Message                                 */
    else {
        
        if (true != modal_sts) {
            next_tab_prompt.modal('show');
        }
        
        /** Sets the cookie to message has been displayed   */
        setCookie(current_tab_open, EXPIRE_1_DAY, MSG_COOKIE_EXIST);
    }
    
    return sts;
}

/**------------------------------------------------------------------------
|	Opens the Message
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindNextTaskModal()
{
    /** Variable Definition */
    var msg_tab_warning = $('#tab-msg-warning');
    
    //-----------------------------------------------------
    //  Binding S43.2: #modal-next-tab-msg
    //      Binds Tab reminder to Bootstrap Modal
    //-----------------------------------------------------
	next_tab_prompt.on('shown.bs.modal', function () {
		$('.modal-backdrop').css('position','fixed');
		page_cookie     = getCookie(PAG_LOD_COOKIE);
        $('.not-filled-list').html('<img src = "'+BaseURL+'/images/preloader.gif" />');
        $('.not-filled-list').load(BaseURL+'/rest/get_unfilled_reminders/'+$('#entityids').val()+'/'+page_cookie, function() {

        });
	}).modal({
		backdrop: 'static',
		keyboard: false,
		show: false
	});
	
    //-----------------------------------------------------
    //  Binding S43.3: #modal-next-tab-review
    //      Closes the reminder after clicking Review button
    //-----------------------------------------------------
	$('#modal-next-tab-review').click(function(e){
		/** Close the message */
		next_tab_prompt.modal('hide');
		
		/** Return message to default configuration */
		msg_tab_warning.empty();
		$('input[name="confirm-tab-msg"]').prop('checked', false);
		
		/** Click to keep the current tab open */
		link_current.click();
	});
	
    //-----------------------------------------------------
    //  Binding S43.4: #modal-next-tab-review
    //      Closes the reminder after clicking Continue button
    //      and checks if tickbox is clicked
    //-----------------------------------------------------
	$('#modal-next-tab-continue').click(function(e){
		if (0 < $('input[name="confirm-tab-msg"]:checked').length) {
			/** Close the message */
			next_tab_prompt.modal('hide');
			
			/** Return message to default configuration */
			msg_tab_warning.empty();
			$('input[name="confirm-tab-msg"]').prop('checked', false);
			
			/** Click to open the next tab */
			link_clicked.click();
		}
		else {
			msg_tab_warning.html('Please check the box to confirm');
		}
	});
}

/**------------------------------------------------------------------------
|	Opens the message for Submission for credit investigation
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindSubmitSMEDetails()
{
    /** Variable Definition */
    var sci_submit_modal    = $('#sci-modal-reminder');
    
    /** Opens the message */
    // Remove prompt after clicking submit
    // $('#wrapper').on('click', '#credit_risk_rating_submit', function(){
        
        // $(this).prop('disabled', true);
        
        // sci_submit_modal.modal({
		// 	backdrop: 'static',
		// 	keyboard: false,
		// });
		
        //-----------------------------------------------------
        //  Binding S43.5: #sci-modal-reminder-continue
        //      Shows a reminder after clicking Submit for
        //      credit risk rating button
        //-----------------------------------------------------
		// $('#sci-modal-reminder-continue').click(function(){
		// 	$('#credit_risk_rating_submit').prop('disabled', false);
		// 	$('#sci-modal-reminder-continue').prop('disabled', true);
		// 	$('#sci-modal-reminder-continue').text('Saving data... please wait');

		// 	$.get(BaseURL+'/feedback/send_rating_email/'+$('#entityids').val(), function() {
		// 		sci_submit_modal.modal('hide');
				
		// 		/** Remove event binding */
		// 		$('#wrapper').off('click', '#credit_risk_rating_submit');
				
		// 		/** Click submit again to proceed */
		// 		$('#credit_risk_rating_submit').click();
				
		// 		$('#credit_risk_rating_submit').prop('disabled', true);
		// 	});
		// });
        
        return false;
    // });;
}

/**------------------------------------------------------------------------
|	Opens the question to ask for Rating
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindRateTabQueryModal()
{
    /** Variable Definition */
    var sts             = false;
    
    var tab_type    = $('#page-wrapper').find('input[data-tab-name="'+current_tab_open+'"]');
    var trans_id    = $('#entityids');
    
    /** Tab has already been rated */
    if (TAB_RATED_EXIST == tab_type.val()) {
       sts = true; 
    }
	if ($('#trial_flag').val() == "1"){
		sts = true;
	}
    
    $('#rate-tab-query-modal').off('click', '#rate-tab-query-rate');
    $('#rate-tab-query-modal').off('click', '#rate-tab-query-no-thanks');
    
    if (false == sts) {
        /** Open the query to rate the tab */
        $('#rate-tab-query-modal').modal({
            backdrop: 'static',
            keyboard: false,
        });
        
        //-----------------------------------------------------
        //  Binding S43.6: #rate-tab-query-modal
        //      Shows the rate tab modal after clicking Rate
        //-----------------------------------------------------
		$('#rate-tab-query-modal').on('click', '#rate-tab-query-rate', function(){
			/** Closes the query */
			$('#rate-tab-query-modal').modal('hide');
			
			/** Open the Rating form modal */
			$('#rating-tab-modal').modal({
				keyboard: false,
				backdrop: 'static',
			})
				.on('show.bs.modal', function(e) {
					$('#rating-tab-body').load(BaseURL+'/feedback/tab_contents/'+tab_type.attr('data-tab-type'), function() {
						$('#rating-trans-id').val($('#entityids').val());
					});
				})
				.modal('show');
		});
		
        //-----------------------------------------------------
        //  Binding S43.7: #rate-tab-query-modal
        //      Closes the dialog box after clicking No thanks
        //-----------------------------------------------------
		$('#rate-tab-query-modal').on('click', '#rate-tab-query-no-thanks', function(){
			/** Closes the query */
			$('#rate-tab-query-modal').modal('hide');
			
			/** Saves ignore of rating to database */
			$.post(BaseURL+'/feedback/ignore_tab_rating', { 'tab_type': tab_type.attr('data-tab-type'), 'trans_id': trans_id.val() }, function () {
				/** Change the status of tab to rated */
				tab_type.val(TAB_RATED_EXIST);
				link_clicked.click();
			},'json');
		});
    }
    
    return sts;
}

/**------------------------------------------------------------------------
|	Submit the Tab's Feedback
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function submitTabFeedback()
{
    //-----------------------------------------------------
    //  Binding S43.8: #submit-feedback-tab
    //      Saves the user's tab feedback on the Database
    //      when submit button is clicked
    //-----------------------------------------------------
    $('#submit-feedback-tab').click(function() {
        var tab_type    = $('input[name="tab-type"]');
        var trans_id    = $('input[name="rating-trans-id"]');
        var hard_query  = $('select[name="difficulty-question"]');
        var recommend   = $('textarea[name="recommendation-question"]');
        var rating      = $('select[name="rate-tab-question"]');
        
        var srv_message = '';
        var error_msg   = $('#feedback-rating-msg');
        
        var tab_rated   = $('#page-wrapper').find('input[data-tab-name="'+current_tab_open+'"]');
        
        $.post(BaseURL+'/feedback/tab_contents', {'tab_type': tab_type.val(), 'trans_id': trans_id.val(), 'hard_query': hard_query.val(), 'recommend': recommend.val(), 'rating': rating.val()  }, function(response) {
            
            if (1 == response.valid) {
                /** Close the Tab Rating Form */
                $('#rating-tab-modal').modal('hide');
                
                /** Change the status of tab to rated */
                tab_rated.val(TAB_RATED_EXIST);
                
                error_msg.html('');
                link_clicked.click();
            }
            /** There is an error */
            else {
                /**	Formats the warnings received from the Server	*/
                for (index = 0; index < response.messages.length; index++) {
                    srv_message += response.messages[index] +'<br/>';
                }
                
                /**	Display the warning								*/
                error_msg.html(srv_message);
				$('#rating-tab-modal').data('bs.modal').handleUpdate();
            }
            
            
        }, 'json');
    });
}

initNextTabModal();

});
