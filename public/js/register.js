var valid_email = true;

$(document).ready(function(){
    jQuery("#formbpo").validationEngine('attach');
    $("input[name=email]").change(function(){
		var uiThis = $(this);

        if (0 < uiThis.val().length) {
            //Check if user is blocked
            $.ajax({
                url: BaseURL + '/api/validate_blocked_email',
                data: { email: uiThis.val() },
                dataType: 'json',
                beforeSend: function(){
                    uiThis.parent().find('span').remove();
                    uiThis.parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating email address...</span>');
                },
                success: function(oReturn){
                    if(oReturn == 1){
                        $.ajax({
                            url: BaseURL + '/api/validate_email',
                            data: { email: uiThis.val() },
                            dataType: 'json',
                            beforeSend: function(){
                                uiThis.parent().find('span').remove();
                                uiThis.parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating email address...</span>');
                            },
                            success: function(oReturn){
                                uiThis.parent().find('span').not('.errormessage').remove();
                                if(oReturn.body.result == 'valid'){
                                    valid_email = true;
                                    uiThis.parent().append('<span style="color: GREEN;" class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span>Valid email address.</span>');
                                } else {
                                    valid_email = false;
                                    uiThis.parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">Email address is not valid.</span>');
                                }
                            }
                        });
                    }else{
                        uiThis.parent().find('span').not('.errormessage').remove();
                        uiThis.parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">Email address has been blocked due to security concerns.</span>');
                    }
                }
            });
        }
        else {
            uiThis.parent().find('span').remove();
            uiThis.parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">Email address is not valid.</span>');
            valid_email = false;
        }
	});

	if($("input[name=email]").val()!=""){
		if($("input[name=email]").parent().find('span.errormessage').length == 0){
			$("input[name=email]").change();
		}
	}

	$("#submit_btn").click(function(e){
		var ret = 0;

        if(valid_email){
			ret = $("#formbpo").validationEngine('validate')

			if (ret) {
				$(this).val('Saving Information...').prop('disabled', true);
				$('#formbpo').submit();
			}
		}
		else {
			$('html, body').animate({
				scrollTop: 0
			}, 500);
		}

        return false;
	});

});