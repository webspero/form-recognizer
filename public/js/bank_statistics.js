//======================================================================
//  Script S3: bank_statistics.js
//      JS file that contains scripts for supervisor statistics page
//======================================================================
$(document).ready(function(){
	
	$('.create_graph').on('click', function(e){
		e.preventDefault();
		if(check_numeric100()){
			var newContainer = $('<div>');
			var newDiv = $('<div>');
			$("#chartcontainer").prepend(newContainer);
			$('#formfilter').ajaxSubmit({
				dataType: 'json',
				beforeSend: function(){
					newContainer.html('Loading chart data please wait...');
				},
				success: function(oReturn){
					
					newContainer.html('<label><input type="radio" name="chart_select" class="chart_select" /> Click here to select this chart</label>');
					newContainer.append(newDiv);
					if(oReturn.type == 1){
						Chart1(newDiv, oReturn.data);
					}
					else if(oReturn.type == 2){
						Chart2(newDiv, oReturn.data);
					}
					else if(oReturn.type == 3){
						Chart3(newDiv, oReturn.data);
					}
					else if(oReturn.type == 4){
						Chart4(newDiv, oReturn.data);
					}
					else if(oReturn.type == 5) {
						ReportOverview(newDiv, oReturn.data);
					}
				}
			});
		}
	});
	
	$('.update_graph').on('click', function(e){
		e.preventDefault();
		if(check_numeric100()){
			if($('.chart_select:checked').length==0){
				alert('No chart is selected.');
			} else {
				var oldContainer = $('.chart_select:checked').parent().parent();
				var newDiv = $('<div>');
				$('#formfilter').ajaxSubmit({
					dataType: 'json',
					beforeSend: function(){
						oldContainer.html('Updating chart data please wait...');
					},
					success: function(oReturn){
						oldContainer.html('<label><input type="radio" name="chart_select" class="chart_select" checked="checked"/> Click here to select this chart</label>');
						oldContainer.append(newDiv);
						if(oReturn.type == 1){
							Chart1(newDiv, oReturn.data);
						}
						else if(oReturn.type == 2){
							Chart2(newDiv, oReturn.data);
						}
						else if(oReturn.type == 3){
							Chart3(newDiv, oReturn.data);
						}
						else if(oReturn.type == 4){
							Chart4(newDiv, oReturn.data);
						}
						else if(oReturn.type == 5) {
							ReportOverview(newDiv, oReturn.data);
						}
					}
				});
			}
		}
	});
	
	$('.delete_graph').on('click', function(e){
		e.preventDefault();
		if($('.chart_select:checked').length==0){
			alert('No chart is selected.');
		} else {
			var oldContainer = $('.chart_select:checked').parent().parent();
			oldContainer.remove();
		}
	});
	
	$('.delete_all_graph').on('click', function(e){
		e.preventDefault();
		$("#chartcontainer > div").remove();
	});
	
	$("#chartcontainer").on('click', '.chart_select', function(){
		if($(this).prop('checked')){
			$("#chartcontainer > div").removeClass('selected');
			$(this).parent().parent().addClass('selected');
		}
	});
	
	$('.filter_toggle').on('click', function(e){
		e.preventDefault();
		$('.filters').slideToggle('fast', function(){
			if($('.filters').is(':visible')){
				$('.filter_toggle').html('hide filters');
			} else {
				$('.filter_toggle').html('show filters');
			}
		});
	});
	
	$('#type').on('change', function(e){
		if($(this).val()=='4'){
			$('.filter4').show();
		} else {
			$('.filter4').hide();
		}
	});
	
	$('#select-sme').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#select-order-frequency').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#select-payment-behavior').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#select-relationship-satisfaction').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});

	$('#select-payment-behavior-supplier').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	})

	$('#select-purchase-frequency').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeigt: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	})
	
	$('#industry1').on('change', function(){
		$.ajax({
            url: BaseURL + "/api/industrysub",
            data: { 'industrymain': $(this).val() },
            beforeSend: function(xhr) {
                $('#industry2').html('<option value = ""> Loading sub-industries... </option>');
				$('#industry3').html('<option value = "">All</option>');
            },
            success: function(response, xhr) {             
                $('#industry2').html('<option value="">All</option>');
                $.each(response, function(key, value) {   
                    $('#industry2').append($("<option></option>").val(key).text(value)); 
                });
            }
        });
	});
	
	$('#industry2').on('change', function(){
		$.ajax({
            url: BaseURL + "/api/industryrow",
            data: { 'industrysub': $(this).val() },
            beforeSend: function(xhr) {
                $('#industry3').html('<option value = ""> Loading industries... </option>');
            },
            success: function(response, xhr) {             
                $('#industry3').html('<option value="">All</option>');
                $.each(response, function(key, value) {   
                    $('#industry3').append($("<option></option>").val(key).text(value)); 
                });
            }
        });
	});
	
	$(".numeric100").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

$('.majorCustFilter').on('click', function(e){
	e.preventDefault();
	if ($('.majorCustomerFilters').hasClass("hidden")) {
		$('.majorCustomerFilters').removeClass("hidden");
		$('#with_majCustFilter').val('1');
		$('#majorCustFilter').text('-');
		$('#majorCustFilter').removeClass("btn-primary");
		$('#majorCustFilter').addClass("btn-danger");
	} else {
		$('.majorCustomerFilters').addClass("hidden");
		$('#with_majCustFilter').val('0');
		$('#majorCustFilter').text('+');
		$('#majorCustFilter').removeClass("btn-danger");
		$('#majorCustFilter').addClass("btn-primary");
	}
});

$('.majorSuppFilter').on('click', function(e) {
	e.preventDefault();
	if ($('.majorSupplierFilters').hasClass("hidden")) {
		$('.majorSupplierFilters').removeClass("hidden");
		$('#with_majSuppFilter').val('1');
		$('#majorSuppFilter').text('-');
		$('#majorSuppFilter').removeClass("btn-primary");
		$('#majorSuppFilter').addClass("btn-danger");
	} else {
		$('.majorSupplierFilters').addClass("hidden");
		$('#with_majSuppFilter').val('0');
		$('#majorSuppFilter').text('+');
		$('#majorSuppFilter').removeClass("btn-danger");
		$('#majorSuppFilter').addClass("btn-primary");
	}
})

function check_numeric100(){
	var cflag = true;
	$('.numeric100').each(function(){
		if(parseInt($(this).val()) < 0 || parseInt($(this).val()) > 100){
			$(this).addClass('numeric100error');
			cflag = false;
		} else {
			$(this).removeClass('numeric100error');
		}
	});
	
	if(parseInt($('input[name=pdefault_from]').val()) > parseInt($('input[name=pdefault_to]').val())){
		$('.numeric100').addClass('numeric100error');
		cflag = false;
	}
	
	return cflag;
}