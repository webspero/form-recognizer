//======================================================================
//  Script S8: biz-outlook-calculate.js
//      Calculates the Business Outlook via AJAX of all industries
//======================================================================

$(document).ready(function(){

var SECTOR_MAX_COUNT    = 10;
var upload_form_footer  = $('#upload-bsp-footer');
var error_msg           = $('#upload-bsp-msg');

/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initBizOutlookCalculate()
{
    bindUploadBSPForm();
}

/**------------------------------------------------------------------------
|	Binds the Upload BSP Business Expectation Form to the submit event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindUploadBSPForm()
{
    //-----------------------------------------------------
    //  Binding S8.1: #calc-biz-outlook-form
    //      Calculates Business Outlooks of all Industries
    //      when the form is submitted
    //-----------------------------------------------------
    $('#calc-biz-outlook-form').submit(function() {
        /** Uploads the BSP Business Expectations HTML via AJAX */
        uploadBspHtml();
        
        return false;
    });
}

/**------------------------------------------------------------------------
|	Uploads the BSP Business Expectations HTML via AJAX
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function uploadBspHtml()
{
    /** Variable Definition         */
    var formData    = new FormData($('#calc-biz-outlook-form')[0]);
    var ajax_url    = BaseURL+'/admin/upload-biz-outlook';
    
    /** Display current Status      */
    upload_form_footer.html('Uploading HTML File...');
    
    $.ajax({
        url: ajax_url,
        data: formData,
        type: 'POST',
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function(response) {
            /** Display Error Messages    */
            if (1 == response) {
               error_msg.html('Upload failed.');
            }
            /** Loads the BSP Business Expectations in a hidden container         */
            else {
               getBspHtml();
            }
        }
    });
}

/**------------------------------------------------------------------------
|	Loads the BSP Business Expectations in a hidden container
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function getBspHtml()
{
    /** Variable Definition         */
    var bsp_htm_cntr    = $('#bsp-htm-upload');
    var ajax_url        = BaseURL+'/admin/bizoutlook/bsphtm';
    var load_cntr       = ' #jpedal';
    
    /** Display current Status      */
    upload_form_footer.html('Extracting Banko Sentral Data...');
    
    bsp_htm_cntr.load(ajax_url+load_cntr, function() {
        /** Extracts the BSP Business Outlook data from the HTML    */
        extractBizOutlook();
    });
}

/**------------------------------------------------------------------------
|	Extracts the BSP Business Outlook data from the HTML
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function extractBizOutlook()
{
    /** Variable Definition         */
    var bsp_sector_exist    = 1;
    var idx                 = 0;
    
    var start_point         = $('div:contains("Industry Sector")').last();
    var bsp_expectation     = start_point.next();
    var sector              = 0;
    
    var business_outlook    = new Array();
    
    /** Initialize Sector array */
    business_outlook[sector] = new Array();
    
    while (bsp_sector_exist) {
        /** Current HTML element is a Business Outlook Data */
        if ($.isNumeric(bsp_expectation.text())) {
            /** Save Business outlook into current sector   */
            business_outlook[sector][idx] = bsp_expectation.text();
            
            /** Increment Index for next Business outlook result   */
            idx++;
        }
        /** HTML element is the End of a Sector's Business Outlook data */
        else {
            /** Stores only the last 8 quarters of Business Outlook    */
            biz_outluk_q8   = business_outlook[sector].length;
            biz_outluk_q1   = biz_outluk_q8 - 8;
            
            business_outlook[sector] = business_outlook[sector].slice(biz_outluk_q1, biz_outluk_q8);
            
            /** Point sector index to the Next Sector    */
            sector++;
            
            /** Reset Business outlook data index   */
            idx = 0;

            /** Max sector has been reached, terminate loop    */
            if (SECTOR_MAX_COUNT == sector) {
                bsp_sector_exist = 0;
            }
            /** Initialize array for next sector    */
            else {
                business_outlook[sector] = new Array();
            }
        }
        
        /** Go to the next HTML element    */
        bsp_expectation = bsp_expectation.next();
    }
    
    /** Calculate Business Outlook for each Industry    */
    calculateBusinessOutlook(business_outlook);
}

/**------------------------------------------------------------------------
|	Calculate Business Outlook for each Industry via AJAX
|--------------------------------------------------------------------------
|	@param [in]		business_outlook    --- BSP Business Expectation data
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function calculateBusinessOutlook(business_outlook)
{
    /** Variable Definition         */
    ajax_url    = BaseURL+'/admin/bizoutlook/calculate';
    
    /** Display current Status      */
    upload_form_footer.html('Calculating Business Outlook...');
    
    $.post(ajax_url, { 'bsp-expectations': business_outlook }, function() {
        location.reload();
    });
}

initBizOutlookCalculate();

});