//======================================================================
//  Script S60: upd-biz1.js
//      Adding and Editing of Business Details 1
//======================================================================

$(document).ready(function(){

/**------------------------------------------------------------------------
|	Binds the Add Affiliates / Subsidiaries link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandRelatedCompForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-affiliates-form';
    var upd_form    = '#affiliates-expand-form';
    var upd_btn     = '#show-affiliates-expand';
    
    //-----------------------------------------------------
    //  Binding S560.1: #show-affiliates-expand
    //      Launch the Expander Form for Affiliates
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Products / Services link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCompOffersForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-offers-form';
    var upd_form    = '#offers-expand-form';
    var upd_btn     = '#show-offers-expand';
    
    //-----------------------------------------------------
    //  Binding S560.2: #show-offers-expand
    //      Launch the Expander Form for Products
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Capital Details link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCapDetailsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-cap-details-form';
    var upd_form    = '#cap-details-expand-form';
    var upd_btn     = '#show-cap-details-expand';
    
    //-----------------------------------------------------
    //  Binding S560.3: #show-cap-details-expand
    //      Launch the Expander Form for Capital Details
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Main Locations link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMainLocForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-main-loc-form';
    var upd_form    = '#main-loc-expand-form';
    var upd_btn     = '#show-main-loc-expand';
    
    //-----------------------------------------------------
    //  Binding S560.4: #show-main-loc-expand
    //      Launch the Expander Form for Main Location
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Branches link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandBranchesForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-branches-form';
    var upd_form    = '#branches-expand-form';
    var upd_btn     = '#show-branches-expand';
    
    //-----------------------------------------------------
    //  Binding S560.5: #show-branches-expand
    //      Launch the Expander Form for Branches
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Directors link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandDirectorsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#ownerdetails');
    var expand_form = '#expand-directors-form';
    var upd_form    = '#directors-expand-form';
    var upd_btn     = '#show-directors-expand';
    
    //-----------------------------------------------------
    //  Binding S560.6: #show-directors-expand
    //      Launch the Expander Form for Directors
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Shareholders link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandShareholdersForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#ownerdetails');
    var expand_form = '#expand-shareholders-form';
    var upd_form    = '#shareholders-expand-form';
    var upd_btn     = '#show-shareholders-expand';
    
    //-----------------------------------------------------
    //  Binding S560.7: #show-shareholders-expand
    //      Launch the Expander Form for Shareholders
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Certifications to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCertificationsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-certifications-form';
    var upd_form    = '#certifications-expand-form';
    var upd_btn     = '#show-certifications-expand';
    
    //-----------------------------------------------------
    //  Binding S560.8: #show-certifications-expand
    //      Launch the Expander Form for Certifications
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Business Type link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandBizTypForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-biztyp-form';
    var upd_form    = '#biztyp-expand-form';
    var upd_btn     = '#show-biztyp-expand';
    
    //-----------------------------------------------------
    //  Binding S560.9: #show-biztyp-expand
    //      Launch the Expander Form for Import / Export
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Insurance link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandInsuranceForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-insurance-form';
    var upd_form    = '#insurance-expand-form';
    var upd_btn     = '#show-insurance-expand';
    
    //-----------------------------------------------------
    //  Binding S560.10: #show-insurance-expand
    //      Launch the Expander Form for Insurance
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Financial Documents link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandFinancialsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-financials-form';
    var upd_form    = '#financials-expand-form';
    var upd_btn     = '#show-financials-expand';
    
    //-----------------------------------------------------
    //  Binding S560.11: #show-financials-expand
    //      Launch the Expander Form for Financial Documents
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Financial Statement PDF link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandFsPdfForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-fspdf-upload-form';
    var upd_form    = '#fspdf-upload-expand-form';
    var upd_btn     = '#show-fspdf-upload-expand';
    
    //-----------------------------------------------------
    //  Binding S560.12: #show-fs-template-expand
    //      Launch the Expander Form for FS Template
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Financial Template link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandFsTemplateForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-fs-template-form';
    var upd_form    = '#fs-template-expand-form';
    var upd_btn     = '#show-fs-template-expand';
    
    //-----------------------------------------------------
    //  Binding S560.12: #show-fs-template-expand
    //      Launch the Expander Form for FS Template
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function expandQuickbooksTemplateForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-quickbooks-form';
    var upd_form    = '#quickbooks-expand-form';
    var upd_btn     = '#show-quickbooks-expand';
    
    //-----------------------------------------------------
    //  Binding S560.12: #show-quickbooks-expand
    //      Launch the Expander Form for FS Template
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function expandAssocFilesForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration3');
    var expand_form = '#expand-dropbox-upload-form';
    var upd_form    = '#dropbox-upload-expand-form';
    var upd_btn     = '#show-dropbox-upload-expand';
    
    //-----------------------------------------------------
    //  Binding S560.12: #show-fs-template-expand
    //      Launch the Expander Form for FS Template
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function expandUbillFilesForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration3');
    var expand_form = '#expand-ubill-upload-form';
    var upd_form    = '#ubill-upload-expand-form';
    var upd_btn     = '#show-ubill-upload-expand';
    
    //-----------------------------------------------------
    //  Binding S560.12: #show-fs-template-expand
    //      Launch the Expander Form for FS Template
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Bank Statement link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandBankStatementsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-bank-statements-form';
    var upd_form    = '#bank-statements-expand-form';
    var upd_btn     = '#show-bank-statements-expand';
    
    //-----------------------------------------------------
    //  Binding S560.13: #show-bank-statements-expand
    //      Launch the Expander Form for Bank Statements
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Utility Bills link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandUtilityBillsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-utility-bills-form';
    var upd_form    = '#utility-bills-expand-form';
    var upd_btn     = '#show-utility-bills-expand';
    
    //-----------------------------------------------------
    //  Binding S560.14: #show-utility-bills-expand
    //      Launch the Expander Form for Utitlity Bills
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Tax Payments link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandTaxPaymentsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-tax-payments-form';
    var upd_form    = '#tax-payments-expand-form';
    var upd_btn     = '#show-tax-payments-expand';
    
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Other Documents to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandOtherDocsForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration1');
    var expand_form = '#expand-other-documents-form';
    var upd_form    = '#other-documents-expand-form';
    var upd_btn     = '#show-other-documents-expand';
    
    //-----------------------------------------------------
    //  Binding S560.15: #show-other-documents-expand
    //      Launch the Expander Form for Other Documents
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}



expandRelatedCompForm();
expandCompOffersForm();
expandCapDetailsForm();
expandMainLocForm();
expandBranchesForm();
expandDirectorsForm();
expandShareholdersForm();
expandCertificationsForm();
expandBizTypForm();
expandInsuranceForm();
expandFsPdfForm();
expandFsTemplateForm();
expandAssocFilesForm();
expandUbillFilesForm();
expandFinancialsForm();
expandBankStatementsForm();
expandUtilityBillsForm();
expandTaxPaymentsForm();
expandOtherDocsForm();
expandQuickbooksTemplateForm();
});