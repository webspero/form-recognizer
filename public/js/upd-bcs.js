//======================================================================
//  Script S59: upd-bcs.js
//      Adding and Editing of Condition and Sustainability data
//======================================================================

$(document).ready(function(){
/**------------------------------------------------------------------------
|	Binds the Succession link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandSuccessionForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-succession-form';
    var bcs_form    = '#bcs-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.1: #show-succession-expand
    //      Launch the Expander Form for Succession
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-succession-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Landscape link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandLandscapeForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-landscape-form';
    var bcs_form    = '#bcs-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.2: #show-landscape-expand
    //      Launch the Expander Form for Landscape
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-landscape-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Economic Factors link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandEconomicFactorForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-eco-factor-form';
    var bcs_form    = '#bcs-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.3: #show-eco-factors-expand
    //      Launch the Expander Form for Economic Factors
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-eco-factors-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Tax Payments link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandTaxPaymentsForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-bcs-tax-payments-form';
    var bcs_form    = '#bcs-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.4: #show-tax-payments-expand
    //      Launch the Expander Form for Tax Payments
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-tax-payments-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Risk Assessment link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandRiskAssesmentForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-risk-mng-form';
    var bcs_form    = '#ra-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.5: #show-risk-mng-expand
    //      Launch the Expander Form for Risk Assessment
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-risk-mng-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Revenue Growth link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandRevGrowthForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-rev-growth-form';
    var bcs_form    = '#rev-growth-expand-form'
    
    //-----------------------------------------------------
    //  Binding S59.6: #show-rev-growth-expand
    //      Launch the Expander Form for Revenue Growth Potential
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-rev-growth-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Capital Expansion to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCapExpForm()
{
    /** Variable Definition    */
    var expand_form = '#expand-phpm-form';
    var bcs_form    = '#rev-phpm-form'
    
    //-----------------------------------------------------
    //  Binding S59.7: #show-phpm-expand
    //      Launch the Expander Form for Capital Investment
    //-----------------------------------------------------
    $('#bcs').on('click', '#show-phpm-expand', function() {
        initExpandForm(expand_form, bcs_form, $(this).attr('href'));
        return false;
    });
}

expandSuccessionForm();
expandLandscapeForm();
expandEconomicFactorForm();
expandTaxPaymentsForm();
expandRiskAssesmentForm();
expandRevGrowthForm();
expandCapExpForm();

});    
    
    
    
    
    