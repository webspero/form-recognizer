//======================================================================
//  Script S22: debt_service_calculator.js
//      JS for Debt service Calculator
//======================================================================
$(document).ready(function(){
	$('#nav_pfs').click(function(e){
		e.preventDefault();
		if($('#debtservicecalculator').length > 0){
			initDSCR();
		}
	});
});

var payPeriod;
var payPeriodAmountLocal = 0;
var payPeriodAmountForeign = 0;
var sliderTimeout;
var postDSCR;
var CCC = 0;
var DebtService = 0;
var NOInoInterestExpense = 0;
var InterestExpense = 0;
var NoiStatic = 0;

var bankInterestRates = {
    'local': {
        'low': 0,
        'mid': 0,
        'high': 0
    },
    'foreign': {
        'low': 0,
        'mid': 0,
        'high': 0
    }
};

function setBankInterestRates(rates) {
    bankInterestRates = {
        'local': {
            'low': rates.local.low,
            'mid': rates.local.mid,
            'high': rates.local.high
        },
        'foreign': {
            'low': rates.foreign.low,
            'mid': rates.foreign.mid,
            'high': rates.foreign.high
        }
    };
}

function getDscrFieldData() {
    return [
        { name: 'WCLocal', value: $('#dscr-data-local-low input').val() },
        { name: 'WCForeign', value: $('#dscr-data-foreign-low input').val() },
        { name: 'TLLocal', value: $('#dscr-data-local-mid input').val() },
        { name: 'TLForeign', value: $('#dscr-data-foreign-mid input').val() },
    ];
}

function setDscrFieldData(dscr) {
    $('#dscr-data-local-low input').val(dscr[0].value);
    $('#dscr-data-foreign-low input').val(dscr[1].value);
    $('#dscr-data-local-mid input').val(dscr[2].value);
    $('#dscr-data-foreign-mid input').val(dscr[3].value);
    $('#dscr-data-local-low span').text(dscr[0].value);
    $('#dscr-data-foreign-low span').text(dscr[1].value);
    $('#dscr-data-local-mid span').text(dscr[2].value);
    $('#dscr-data-foreign-mid span').text(dscr[3].value);
}

function toggleDscrFields(custom) {
    if (custom) {
        $('#custom-interest-rate')
            .text(trans('rcl.restore_interest_rate'))
            .data('value', 1)
            .removeClass('btn-primary')
            .addClass('btn-warning');
        $('#dscr-data-local-low span').hide();
        $('#dscr-data-foreign-low span').hide();
        $('#dscr-data-local-mid span').hide();
        $('#dscr-data-foreign-mid span').hide();
        $('#dscr-data-local-low input').show();
        $('#dscr-data-foreign-low input').show();
        $('#dscr-data-local-mid input').show();
        $('#dscr-data-foreign-mid input').show();
    } else {
        $('#custom-interest-rate')
            .data('value', 0)
            .text(trans('rcl.custom_interest_rate'))
            .removeClass('btn-warning')
            .addClass('btn-primary');
        $('#dscr-data-local-low span').show();
        $('#dscr-data-foreign-low span').show();
        $('#dscr-data-local-mid span').show();
        $('#dscr-data-foreign-mid span').show();
        $('#dscr-data-local-low input').hide();
        $('#dscr-data-foreign-low input').hide();
        $('#dscr-data-local-mid input').hide();
        $('#dscr-data-foreign-mid input').hide();
    }
}

function getDSCRdata(monthDuration) {
    var tlLocal;
    var tlForeign;
    var wcLocal;
    var wcForeign;
    var custom = $('#custom-interest-rate').data('value');

    if (custom) {
        return getDscrFieldData();
    } else {
        if (monthDuration > 60) {
            tlLocal = bankInterestRates.local.high;
            tlForeign = bankInterestRates.foreign.high;
        } else {
            tlLocal = bankInterestRates.local.mid;
            tlForeign = bankInterestRates.foreign.mid;
        }

        wcLocal = bankInterestRates.local.low;
        wcForeign = bankInterestRates.foreign.low;
    }

    return [
        { name: 'WCLocal', value: wcLocal },
        { name: 'WCForeign', value: wcForeign },
        { name: 'TLLocal', value: tlLocal },
        { name: 'TLForeign', value: tlForeign },
    ];
}

function restoreInterestRateDefault() {
    toggleDscrFields(0);
    $('#ds_tenor').val(0);
    $('#ds_slider').bootstrapSlider('setValue', 1.2, true);
    setDscrFieldData(getDSCRdata(0));
}

function customInterestRate() {
    toggleDscrFields(1);
}

function saveDSCR() {
    if (postDSCR) {
        postDSCR.abort();
    }

    var d = getDscrFieldData();

    postDSCR = $.post(
        BaseURL + "/api/save_entity_dscr",
        {
            entity_id: $('#entityids').val(),
            dscr: $('#ds_slider').bootstrapSlider('getValue'),
            net_operating_income: parseFloat($('#ds_noi').val().replace(/,/g , "").replace(/$/g , "")),
            loan_duration: parseInt($('#ds_tenor').val()),
            loan_amount: parseFloat($('#ds_loan').val().replace(/,/g , "").replace(/$/g , "")),
            working_interest_rate_local: d[0].value,
            working_interest_rate_foreign: d[1].value,
            term_interest_rate_local: d[2].value,
            term_interest_rate_foreign: d[3].value,
            period: payPeriod,
            payment_per_period_local: payPeriodAmountLocal,
            payment_per_period_foreign: payPeriodAmountForeign,
            is_custom: parseInt($('#custom-interest-rate').data('value')),
            _token: $('meta[name="csrf-token"]').attr('content')
        },
        function(response) {
            console.log(response);
        }
    );
}

function initDSCR(){
    // DSCRdata = [
    //     { name: 'WCLocal', value: $('#local_rate').val() },
    //     { name: 'WCForeign', value: $('#foreign_rate').val() },
    //     { name: 'TLLocal', value: $('#local_rate').val() },
    //     { name: 'TLForeign', value: $('#foreign_rate').val() },
    // ];

	$.get(BaseURL + "/api/check_fs", { id: $('#entityids').val() },
		function(data) {
			if(data != ""){
                setBankInterestRates(data.bank_interest_rates);
                lock_loan_amount();
				var loanAmount = $.trim($('#ds_loan_amount_source').text());
				if(loanAmount == '') loanAmount = 0;

				DebtService = parseFloat(data.cash_flow[0].DebtServiceCapacity);
				NOInoInterestExpense = parseFloat(data.cash_flow[0].NetOperatingIncome) + parseFloat(data.cash_flow[0].InterestExpense);
                NoiStatic           = data.cash_flow[0].NetOperatingIncome;
				InterestExpense = parseFloat(data.cash_flow[0].InterestExpense);

				var DSCRc = (NOInoInterestExpense - InterestExpense) / DebtService;
				DSCRc = DSCRc.toFixed(2);
				if(isNaN(DSCRc)) DSCRc = 0.00;
				if(!isFinite(DSCRc)) DSCRc = 0.00;

                sHtml = '';
				sHtml += '<div class="row"><div class="col-sm-6"><label id="ds_slider_display">DSCR: '+DSCRc+'x</label><input id="ds_slider" data-slider-id="dscrSlider" type="text" data-slider-min="0" data-slider-max="5" data-slider-step="0.01" data-slider-value="'+DSCRc+'"/></div>';
                sHtml += '<div class="col-sm-6"><label>Total Loan Duration (months)</label><input class="form-control" type="text" id="ds_tenor" value="0" /></div>';
                sHtml += '<div class="col-sm-6"><label>NOI (Net Operating Income)</label><input class="form-control" type="text" readonly="readonly" id="ds_noi" value="'+number_format(parseFloat(data.cash_flow[0].NetOperatingIncome),2,'.',',')+'" /></div>';
				sHtml += '<div class="col-sm-6"><label>'+trans('rcl.loan_amount')+'</label><input class="form-control" readonly="readonly" type="text" id="ds_loan" value="'+loanAmount+'" /></div>';
                sHtml += '<div class="col-sm-6"></div><div class="col-sm-6" style="margin-top: 10px; text-align: right;"><button class="btn btn-primary" data-value="0" id="custom-interest-rate">' + trans('rcl.custom_interest_rate') + '</button></div>';
                sHtml += '</div>';

				sHtml += '<table class="table table-striped table-bordered" style="margin-top: 20px;"><tr><td>&nbsp;</td><td colspan="3" align="center"><b>'+trans('rcl.working_capital_2')+' (1 Year or Less)</b></td></tr>';
				sHtml += '<tr><td>&nbsp;</td><td>'+trans('rcl.interest_rate')+'</td><td>'+trans('rcl.period')+'</td><td>'+trans('rcl.payment_per_period')+'</td></tr>';
				sHtml += '<tr><td><b>'+trans('rcl.local_banks')+'</b></td>';
                sHtml += '<td id="dscr-data-local-low"><span></span><input type="number" class="form-control" style="width: 150px; display: none" /></td>';
                sHtml += '<td id="WCLocalPeriod"></td><td id="WCLocalPayment"></td></tr>';
				sHtml += '<tr><td><b>'+trans('rcl.foreign_banks')+'</b></td>';
                sHtml += '<td id="dscr-data-foreign-low"><span></span><input type="number" class="form-control" style="width: 150px; display: none" /></td>';
                sHtml += '<td id="WCForeignPeriod"></td><td id="WCForeignPayment"></td></tr>';
				sHtml += '<tr><td>&nbsp;</td><td colspan="3" align="center"><b>'+trans('rcl.term_loan')+'  (More than 1 year)</b></td></tr>';
				sHtml += '<tr><td>&nbsp;</td><td>'+trans('rcl.interest_rate')+'</td><td>'+trans('rcl.period')+'</td><td>'+trans('rcl.payment_per_period')+'</td></tr>';
				sHtml += '<tr><td><b>'+trans('rcl.local_banks')+'</b></td>';
                sHtml += '<td id="dscr-data-local-mid"><span></span><input type="number" class="form-control" style="width: 150px; display: none" /></td>';
                sHtml += '<td id="TLLocalPeriod"></td><td id="TLLocalPayment"></td></tr>';
				sHtml += '<tr><td><b>'+trans('rcl.foreign_banks')+'</b></td>';
                sHtml += '<td id="dscr-data-foreign-mid"><span></span><input type="number" class="form-control" style="width: 150px; display: none" /></td>';
                sHtml += '<td id="TLForeignPeriod"></td><td id="TLForeignPayment"></td></tr></table>';
				sHtml += '<small>*'+trans('rcl.dscr_hint')+'</small>';

				sHtml += '<table class="table table-striped table-bordered" style="margin-top: 20px;">';
				sHtml += '<tr><td>&nbsp;</td><td>'+parseInt(data.financial_report.year)+'</td><td>'+(parseInt(data.financial_report.year)-1)+'</td></tr>';
				sHtml += '<tr><td>Trade and other Current Receivables</td><td>'+number_format(parseFloat(data.balance_sheets[0].TradeAndOtherCurrentReceivables), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Trade and other Current Payables</td><td>'+number_format(parseFloat(data.balance_sheets[0].TradeAndOtherCurrentPayables), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.balance_sheets[1].TradeAndOtherCurrentPayables), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Current Provisions for Employee Benefits</td><td>'+number_format(parseFloat(data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Cost of Sales</td><td>'+number_format(parseFloat(data.income_statements[0].CostOfSales), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.income_statements[1].CostOfSales), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Inventories</td><td>'+number_format(parseFloat(data.balance_sheets[0].Inventories), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.balance_sheets[1].Inventories), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Revenue</td><td>'+number_format(parseFloat(data.income_statements[0].Revenue), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.income_statements[1].Revenue), 2, '.', ',')+'</td></tr>';
				sHtml += '<tr><td>Interest Expense</td><td>'+number_format(parseFloat(data.cash_flow[0].InterestExpense), 2, '.', ',')+'</td><td>'+number_format(parseFloat(data.cash_flow[1].InterestExpense), 2, '.', ',')+'</td></tr>';
				sHtml += '</table>';

				var ReceivablesTurnover = ((parseFloat(data.balance_sheets[0].TradeAndOtherCurrentReceivables) + parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables)) / 2) / (parseFloat(data.income_statements[0].Revenue) / 365);
				var PayableTurnover = ((parseFloat(data.balance_sheets[0].TradeAndOtherCurrentPayables) + parseFloat(data.balance_sheets[1].TradeAndOtherCurrentPayables) + parseFloat(data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits) + parseFloat(data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits)) / 2) / ((parseFloat(data.income_statements[0].CostOfSales) + parseFloat(data.balance_sheets[0].Inventories) - parseFloat(data.balance_sheets[1].Inventories)) / 365);
				var InventoryTurnover = ((parseFloat(data.balance_sheets[0].Inventories) + parseFloat(data.balance_sheets[1].Inventories)) / 2) / (parseFloat(data.income_statements[0].CostOfSales) / 365);
				ReceivablesTurnover = isNaN(ReceivablesTurnover) ? 0 : ReceivablesTurnover;
				PayableTurnover = isNaN(PayableTurnover) ? 0 : PayableTurnover;
				InventoryTurnover = isNaN(InventoryTurnover) ? 0 : InventoryTurnover;
				CCC = InventoryTurnover + ReceivablesTurnover - PayableTurnover;
				CCC = (CCC < 0) ? CCC * -1 : CCC;

				$('#debtservicecalculator').html(sHtml);
				$('#ds_slider').bootstrapSlider({
                    step: 0.1,
					formatter: function(value) {
						return value + 'x';
					}
				}).on('change', function(changeEvt) {
                    $("#ds_slider_display").text('DSCR: ' + changeEvt.value.newValue + 'x');
                    CalculateDSCR2();

                    if (sliderTimeout) {
                        clearTimeout(sliderTimeout);
                    }

                    sliderTimeout = setTimeout(saveDSCR, 500);
                });
				$('#ds_tenor').on('change, keyup', function(){
					CalculateDSCR2();
                    saveDSCR();
				});

                $('#custom-interest-rate').on('click', function(event) {
                    if ($(this).data('value')) {
                        restoreInterestRateDefault();
                    } else {
                        toggleDscrFields(1);
                    }
                    saveDSCR();
                });

                $('#dscr-data-local-low input').on('change', function() {
                    CalculateDSCR2();
                    saveDSCR();
                });
                $('#dscr-data-foreign-low input').on('change', function() {
                    CalculateDSCR2();
                    saveDSCR();
                });
                $('#dscr-data-local-mid input').on('change', function() {
                    CalculateDSCR2();
                    saveDSCR();
                });
                $('#dscr-data-foreign-mid input').on('change', function() {
                    CalculateDSCR2();
                    saveDSCR();
                });

                var DSCRdata = getDSCRdata(0);
                setDscrFieldData(DSCRdata);

                if (data.debt_service) {
                    var debt_service = data.debt_service;

                    $('#ds_slider').bootstrapSlider('setValue', parseFloat(debt_service.dscr));
                    $("#ds_slider_display").text('DSCR: ' + parseFloat(debt_service.dscr) + 'x');
                    $('#ds_tenor').val(parseInt(debt_service.loan_duration)),
                    $('#dscr-data-local-low input')
                        .val(parseFloat(debt_service.working_interest_rate_local));
                    $('#dscr-data-foreign-low input')
                        .val(parseFloat(debt_service.working_interest_rate_foreign));
                    $('#dscr-data-local-mid input')
                        .val(parseFloat(debt_service.term_interest_rate_local));
                    $('#dscr-data-foreign-mid input')
                        .val(parseFloat(debt_service.term_interest_rate_foreign));

                    if (debt_service.is_custom) {
                        toggleDscrFields(1);
                    }
                    CalculateDSCR2();
                }
			} else {
				$('#debtservicecalculator').html('You need to upload your Financial Statements using the FS Input Template in order to use this function.');
			}
		}
	);
}

function CalculateDSCR(){
    var monthDuration = $('#ds_tenor').val();
    DSCRdata = getDSCRdata(monthDuration);

	for(x in DSCRdata){

		var IR = DSCRdata[x].value;
		var AIR = IR/100;
		var MIR = AIR/12;

		if(x < 2) {
			var Period = CCC;
			var Annualization = 360;
		} else {
			var Period = 30;
			var Annualization = 365;
		}

		var LA = parseFloat($('#ds_loan').val().replace(/,/g , "").replace(/$/g , ""));
		var IE = LA * AIR * Period / Annualization;
		var NOI = NoiStatic;

		//var DSCR = NOI / DebtService;
        var pmt     = (1 - Math.pow(1 + AIR, -1)) / AIR;
        var pmt2    = LA / pmt;
        var DSCR    = NOI / pmt2;

        //var pmt2 = (1 - (Math.pow(1 + MIR, -24))) / MIR;
        var MonthlyPayment = LA / pmt2;
		//var MonthlyPayment = NOI / (DSCR * 12);
		var PaymentPerPeriod = 0;

        /*  Working Capital */
		if(x < 2) {
			var PaymentPerPeriod = LA * (AIR / (1 - Math.pow(1 + AIR, -1)));

			if(isNaN(CCC) == true){
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html(Math.ceil(CCC) + ' Days');
			}

			if (PaymentPerPeriod == 0) {
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
			}

		}
        /*  Term Loan   */
        else {
			var MonthlyTerm = 24;
			var PaymentPerPeriod = LA * (MIR / (1 - Math.pow(1 + MIR, -Math.ceil(MonthlyTerm))));

            if (isNaN(MonthlyTerm) == true || MonthlyTerm == 0) {
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html(Math.ceil(MonthlyTerm) + ' Months');
			}

			if (number_format(PaymentPerPeriod, 2, '.', ',') == '0.00') {
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
			}

		}

		if (x == 0) {
			var IE2     = LA * 0.049 * CCC / 360;
			var NOI2    = NOInoInterestExpense - (InterestExpense + IE2);
			var DSCR2   = DSCR;
			var dscrv   = parseFloat(DSCR2.toFixed(2));

			if (isNaN(dscrv) || dscrv < 0 || !isFinite(dscrv)) {
				$('#ds_slider').bootstrapSlider('setValue', 0);
				$("#ds_slider_display").text('DSCR: 0x');
			}
            else {
				$('#ds_slider').bootstrapSlider('setValue', dscrv);
				$("#ds_slider_display").text('DSCR: ' + number_format(dscrv, 1)  + 'x');
			}
			//$('#ds_noi').val(number_format(NOI,2,'.',','));
		}
	}
}

function CalculateDSCR2(){
    var monthDuration = $('#ds_tenor').val();
    DSCRdata = getDSCRdata(monthDuration);

	for(x in DSCRdata) {

		var IR = DSCRdata[x].value;
		var AIR = IR/100;
		var MIR = AIR/12;

		if(x < 2) {
			var Period = CCC;
			var Annualization = 365;
		}
        else {
			var Period = 30;
			var Annualization = 360;
		}

		var DSCR    = $('#ds_slider').bootstrapSlider('getValue');
		var tenor   = $('#ds_tenor').val();
		var NOI     = NoiStatic;
		var IE      = NOInoInterestExpense - NOI - InterestExpense;
        var pmt     = NOI / DSCR;

        var LA      = (tenor / 12) * pmt;

		var MonthlyPayment      = (LA + (((LA * AIR) * 30) / 360)) / pmt;
		var PaymentPerPeriod    = 0;

        if (0 == x) {
            $('#ds_loan').val(number_format(LA, 2, '.', ','));
        }

		if(12 >= tenor) {
			var PaymentPerPeriod    = ((LA + (((LA * AIR) * CCC) / 365)) / pmt);
			var PaymentPerPeriod    = (LA + (((LA * AIR) * CCC) / 365)) / tenor;

            if (isNaN(CCC) == true) {
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
                setPeriod(0);
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html(parseInt(CCC) + ' Days');
                setPeriod(parseInt(CCC));
			}

			if(PaymentPerPeriod == 0 || isNaN(PaymentPerPeriod)){
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
                setPaymentPerPeriod(x, 0);
			}
            else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
                setPaymentPerPeriod(
                    x,
                    number_format(PaymentPerPeriod, 2).replace(/,/g , "").replace(/$/g , "")
                );
			}

            $('#' + DSCRdata[2].name + 'Period').html('N/A');
            $('#' + DSCRdata[3].name + 'Period').html('N/A');
            $('#' + DSCRdata[2].name + 'Payment').html('N/A');
            $('#' + DSCRdata[3].name + 'Payment').html('N/A');
		}
        else {
			//var MonthlyPayment = 24;
            var nump1               = -Math.log(1 - (AIR * (LA / pmt)));
            var nump2               = Math.log(1 + AIR);
            var num_payments        = nump1 / nump2;
			var PaymentPerPeriod    = (LA + (((LA * AIR) * 30) / 360)) / tenor;
			//var PaymentPerPeriod = LA / MonthlyPayment;

			if (isNaN(MonthlyPayment) == true || MonthlyPayment == 0) {
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
                setPeriod(0);
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html('1 Month');
                setPeriod(1);
			}

			if (number_format(PaymentPerPeriod, 2, '.', ',') == '0.00' || isNaN(PaymentPerPeriod)) {
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
                setPaymentPerPeriod(x, 0);
			} else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
                setPaymentPerPeriod(
                    x,
                    number_format(PaymentPerPeriod, 2).replace(/,/g , "").replace(/$/g , "")
                );
			}

            $('#' + DSCRdata[0].name + 'Period').html('N/A');
            $('#' + DSCRdata[1].name + 'Period').html('N/A');
            $('#' + DSCRdata[0].name + 'Payment').html('N/A');
            $('#' + DSCRdata[1].name + 'Payment').html('N/A');

		}
	}
}

function setPeriod(value) {
    payPeriod = value;
}

function setPaymentPerPeriod(index, value) {
    if (index === 0 || index === 3) {
        payPeriodAmountLocal = value;
    } else {
        payPeriodAmountLocal = value;
    }
}

function CalculateDSCR3(){
    var monthDuration = $('#ds_tenor').val();
    DSCRdata = getDSCRdata(monthDuration);

	for(x in DSCRdata){

		var IR = DSCRdata[x].value;
		var AIR = IR/100;
		var MIR = AIR/12;

		if(x < 2) {
			var Period = CCC;
			var Annualization = 360;
		} else {
			var Period = 30;
			var Annualization = 365;
		}

        var DSCR    = $('#ds_slider').bootstrapSlider('getValue');
        var tenor   = $('#ds_tenor').val();
		var LA      = parseFloat($('#ds_loan').val().replace(/,/g , "").replace(/$/g , ""));
		var IE      = LA * AIR * Period / Annualization;
		var NOI     = NoiStatic;

        var pmt     = NOI / DSCR;

        var MonthlyPayment      = (LA + (((LA * AIR) * 30) / 360)) / pmt;
		var PaymentPerPeriod    = 0;

        /*  Working Capital */
		if(12 >= tenor) {
            var nump1               = -Math.log(1 - (AIR * (LA / pmt)));
            var nump2               = Math.log(1 + AIR);
            var num_payments        = nump1 / nump2;
            var num_payments_ccc    = 365 / num_payments;

            var PaymentPerPeriod    = ((LA + (((LA * AIR) * CCC) / 365)) / pmt);
			var PaymentPerPeriod    = (LA + (((LA * AIR) * CCC) / 365)) / tenor;

			if(isNaN(CCC) == true){
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html(parseInt(CCC) + ' Days');
			}

			if (PaymentPerPeriod == 0) {
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
			}

            $('#' + DSCRdata[2].name + 'Period').html('N/A');
            $('#' + DSCRdata[3].name + 'Period').html('N/A');
            $('#' + DSCRdata[2].name + 'Payment').html('N/A');
            $('#' + DSCRdata[3].name + 'Payment').html('N/A');
		}
        /*  Term Loan   */
        else {
			var nump1               = -Math.log(1 - (AIR * (LA / pmt)));
            var nump2               = Math.log(1 + AIR);
            var num_payments        = nump1 / nump2;
			var PaymentPerPeriod    = (LA + (((LA * AIR) * 30) / 360)) / tenor;

            if (isNaN(MonthlyPayment) == true || MonthlyPayment == 0) {
				$('#' + DSCRdata[x].name + 'Period').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Period').html('1 Month');
			}

			if (number_format(PaymentPerPeriod, 2, '.', ',') == '0.00') {
				$('#' + DSCRdata[x].name + 'Payment').html('N/A');
			}
            else {
				$('#' + DSCRdata[x].name + 'Payment').html(number_format(PaymentPerPeriod, 2, '.', ','));
			}

            $('#' + DSCRdata[0].name + 'Period').html('N/A');
            $('#' + DSCRdata[1].name + 'Period').html('N/A');
            $('#' + DSCRdata[0].name + 'Payment').html('N/A');
            $('#' + DSCRdata[1].name + 'Payment').html('N/A');
		}

		if (x == 0) {
			var DSCR2   = DSCR;
			var dscrv   = parseFloat(DSCR.toFixed(2));

			if (isNaN(dscrv) || dscrv < 0 || !isFinite(dscrv)) {
				$('#ds_slider').bootstrapSlider('setValue', 0);
				$("#ds_slider_display").text('DSCR: 0x');
			}
            else {
				$('#ds_slider').bootstrapSlider('setValue', dscrv);
				$("#ds_slider_display").text('DSCR: ' + number_format(dscrv, 1)  + 'x');
			}
			//$('#ds_noi').val(number_format(NOI,2,'.',','));
		}
	}
}

function number_format(number, decimals, dec_point, thousands_sep) {

 number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}

function lock_loan_amount()
{
    $('#debt-service-calc-cntr').on('click', '#dscr-lock-loan', function(){

        if (0 == $('#dscr-lock-loan').data('lock')) {
            $('#ds_tenor').prop('readonly', true);
            $('#dscr-lock-loan').text('Unlock amount');
            $('#dscr-lock-loan').data('lock', 1);
        }
        else {
            $('#ds_tenor').prop('readonly', false);
            $('#dscr-lock-loan').text('Lock amount');
            $('#dscr-lock-loan').data('lock', 0);
        }

        return false;
    });
}
