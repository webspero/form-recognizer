//======================================================================
//  Script S1: bank_industry_weights.js
//      JS file that contains scripts for industry weights manipulation
//======================================================================
$(document).ready(function(){
	$('.btn-save-settings').click(function(e){
		e.preventDefault();
		$('#industry_save_modal').modal();
	});
	
	$('.btn-setting-save').click(function(e){
		e.preventDefault();
		if($.trim($('#setting_name').val())==""){
			$('.save_status').text('Please input setting name.');
		} else {
			$.ajax({
				url: BaseURL + '/bank/save_industry_weight_setting',
				type: 'post',
				data: {
					name: $('#setting_name').val()
				},
				beforeSend: function(){
					$('.save_status').text('Saving...');
					$('.btn-setting-save').prop('disabled', true);
				},
				success: function(sReturn){
					$('.btn-setting-save').removeAttr('disabled');
					$('#setting_name').val('');
					if(sReturn == "success"){
						$('.save_status').text('Saved');
						$('#industry_save_modal').modal('hide');
					} else {
						$('.save_status').text('Error saving...');
					}
				}
			});
		}
	});
	
	$('.btn-load-settings').click(function(e){
		e.preventDefault();
		$.ajax({
			url: BaseURL + '/bank/revert_industry_weight_setting',
			type: 'post',
			data: {
				id: $('#history_select').val()
			},
			beforeSend: function(){
				$('.btn-load-settings').prop('disabled', true);
			},
			success: function(sReturn){
				$('.btn-load-settings').removeAttr('disabled');
				if(sReturn == "success"){
					window.location.reload(true);
				}
			}
		});
	});
	
	$('#fa_standalone').on('click', function(){
		var uiThis = $(this);

		$.ajax({
			url: BaseURL + '/bank/standalone_option',
			type: 'post',
			data: {
				option: uiThis.is(':checked')
			},
			beforeSend: function(){
				$('.preloader').show();
				uiThis.hide();
			},
			success: function(sReturn){
				if(sReturn == "success"){
					window.location.reload(true);
				}
			}
		});
	});
});