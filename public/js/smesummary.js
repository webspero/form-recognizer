//======================================================================
//  Script S52: smesummary.js
//      Js helper for SME questionnaire process
//======================================================================
// slow document ready function $(document).ready(function() {

$('.sec-download-links').click(function (e) {
	e.preventDefault();
	$('#modal-sec-download-links').modal();
});

$('#bank_statement_modal_reupload').click(function () {
	$('#invalid_bank_statement_modal').modal('hide');
	$('#nav_registration1').click();
	setTimeout(function () {
		var offset = $('#show-bank-statements-expand').offset();
		$("html, body").animate({
			scrollTop: offset.top
		}, 500, 'swing');
		$('#show-bank-statements-expand').click();
	}, 1500);
});

$('#privatelink').click(function (e) {
	e.preventDefault();
	$('#private-link-modal').modal();
});

$('#dev-ref-link').click(function (e) {
	$('#dev-ref-codes').modal();
	return false;
});

$('#printall').click(function () {
	$('#registration1, #registration2, #payfactor, #bcs, #pfs, #ownerdetails, #ssc, #fianalysis').removeClass('fade');
	$('#registration1, #registration2, #payfactor, #bcs, #pfs, #ownerdetails, #ssc, #fianalysis').printArea();
	return false;
});

$('ul#side-menu li a').click(function () {
	var uiThis = $(this);
	$('ul#side-menu li a').removeClass('active');
	uiThis.addClass('active');
});

$('#nav_registration1_next').click(function () {
	$('#nav_registration2').click();
});

$('#nav_registration2_prev').click(function () {
	$('#nav_registration1').click();
});

$('#nav_ownerdetails_next').click(function () {
	$('#nav_payfactor').click();
});

$('#nav_ownerdetails_prev').click(function () {
	$('#nav_registration2').click();
});

$('#payfactor').on('click', '#nav_payfactor_next', function () {
	$('#nav_bcs').click();
});

$('#nav_bcs_prev').click(function () {
	$('#nav_payfactor').click();
});

$('#nav_bcs_next').click(function () {
	$('#nav_pfs').click();
});

$('#pfs').on('click', '#nav_pfs_prev', function () {
	if ($('#nav_bcs').length) {
		$('#nav_bcs').click();
	} else if ($('#nav_assocFiles')) {
		$('#nav_assocFiles').click();
	}
});

$('#nav_registration2_next').click(function () {
	if ($('#nav_ownerdetails').length) {
		$('#nav_ownerdetails').click();
	} else if ($('#nav_assocFiles').length) {
		$('#nav_assocFiles').click();
	}
});


$('#nav_assocfiles_prev').click(function () {
	if ($('#nav_registration2').length) {
		$('#nav_registration2').click();
	}
});

$('#nav_assocfiles_next').click(function () {
	if ($('#nav_pfs').length) {
		$('#nav_pfs').click();
	}
});

$('#nav_submit_prev').click(function () {
	if ($('#nav_pfs').length) {
		$('#nav_pfs').click();
	}
});



$('#nav_sci_prev').click(function () {
	$('#nav_pfs').click();
});

$('#pfs').on('click', '#nav_pfs_next', function () {
	if ($('#status').val() == 0) {
		$('#nav_sci').click();
	}
	if ($('#status').val() == 6 || $('#status').val() == 7) {
		if (($('#userid').val() == 2) || (1 == $('#bank_ci_view').val())) {
			$('#nav_investigationsummary').click();
		}
		else {
			$('#sscore').click();
		}
	}
});

$('#payfactor').on('click', '#nav_payfactor_prev', function () {
	$('#nav_ownerdetails').click();
});

$('#sscore_prev').click(function () {
	$('#sscore').click();
});

$('#sscore_next').click(function () {
	if ($('#userid').val() == 2) {
		$('#nav_registration1').click();
	} else {
		$('#nav_strategicprofile').click();
	}
});

$('#finananalysis_next').click(function () {
	$('#finananalysis').click();
});

$('#strategicprofile_prev').click(function () {
	$('#finananalysis').click();
});
$('#strategicprofile_next').click(function () {
	$('#nav_registration1').click();
});

$('#nav_sci').click(function () {
	$('#incomplete-summary').load(BaseURL + '/rest/comp-sts-tb/' + $('#entityids').val());

	var inputs = $(".assocInput");

	for(var i = 0; i < inputs.length; i++){
		$(inputs[i]).addClass('required_input');
	}
});

$('#nav_sci').click(function () {
	$('#incomplete-summary-premium').load(BaseURL + '/rest/comp-sts-tb-premium/' + $('#entityids').val());
});

//serviceofferLoad("servicesoffers", "entityids");
//businessdriverLoad("businessdriver", "entityids");
//businesssegmentLoad("businesssegment", "entityids");
//majorcustomerLoad("majorcustomer", "entityids");
//majorsupplierLoad("majorsupplier", "entityids");
//competitorLoad("competitor", "entityids");
//pastprojectscompletedLoad("pastprojectscompleted", "entityids");
//futuregrowthinitiativeLoad("futuregrowthinitiative", "entityids")
//riskassessmentLoad("riskassessment", "entityids");
//majorlocationLoad("majorlocation", "entityids");
adverserecordLoad("adverserecord", "entityids");

$('input[type="radio"]').click(function () {
	if ($(this).val() == 1) {
		$('#onapprovedwrapper').attr('style', 'display: block;');
		$('#onholdwrapper').attr('style', 'display: none;');
	} else {
		$('#onapprovedwrapper').attr('style', 'display: none;');
		$('#onholdwrapper').attr('style', 'display: block;');
	}
});


$('#sscore').click(function () {

	var rascoresum = 0;

	$('.ra-class-unique').each(function () {
		rascoresum += Number($(this).val());
	});

	var totalra = $('.ra-class').val();
	var totalan = rascoresum;

	var computera = (parseInt(totalan) / parseInt(totalra)) * 100;

	if (parseInt(Math.round(computera)) >= 60) {
		$('#bconsideration_mirror').val('Solution addresses the threat');
		$('#bconsideration_for_save').val(90);
	} else {
		$('#bconsideration_mirror').val('Solution does not address threat');
		$('#bconsideration_for_save').val(30);
	}


	// Customer and Supplier Experience
	/*
	var expsum = 0;
	$('.texperience').each(function() {
		expsum += Number($(this).val());
	});

	var totalaverageexpscore = parseInt(expsum) / $('.texperience').val();

	if(totalaverageexpscore >= 27){
		$('#csexperience option[value="40"]').attr("selected", "selected");
	}else if(totalaverageexpscore >= 14 && totalaverageexpscore <= 26){
		$('#csexperience option[value="27"]').attr("selected", "selected");
	}else{
		$('#csexperience option[value="13"]').attr("selected", "selected");
	}
	*/

	// Customer Dependency
	var max = 0;
	var sum = 0;

	$('.totalsalevalue').each(function () {
		sum += Number($(this).val());
		num = parseInt($(this).val());
		if (num > max) {
			max = num;
		}
	});
	/*
	if(max >= 70 || $('#is_agree').val() == 1){
		$('#cdependency option[value="40"]').attr("selected", "selected");
	}else if(max < 70 && max > 30 && $('#is_agree').val() == 0){
		$('#cdependency option[value="27"]').attr("selected", "selected");
	}else if(max <= 30 && $('#is_agree').val() == 0){
		$('#cdependency option[value="13"]').attr("selected", "selected");
	}
	*/

	if (($('#is_agree').val() == 1) && (0 == max)) {
		$('#cdependency option[value="30"]').attr("selected", "selected");
		$('#cdependency_for_save').val(30);
	}
	else {
		if (max >= 70) {
			$('#cdependency option[value="10"]').attr("selected", "selected");
			$('#cdependency_for_save').val(10);
		} else if (max < 70 && max > 30) {
			$('#cdependency option[value="20"]').attr("selected", "selected");
			$('#cdependency_for_save').val(20);
		} else if (max <= 30) {
			$('#cdependency option[value="30"]').attr("selected", "selected");
			$('#cdependency_for_save').val(30);
		} else {
			$('#cdependency option[value="10"]').attr("selected", "selected");
			$('#cdependency_for_save').val(10);
		}
	}

	// Supplier Dependency
	var smax = 0;
	var ssum = 0;

	$('.totalsuppliersalevalue').each(function () {
		ssum += Number($(this).val());
		snum = parseInt($(this).val());
		if (snum > smax) {
			smax = snum;
		}
	});
	/*
		if(smax >= 70){
		  $('#sdependency option[value="13"]').attr("selected", "selected");
		}else if(smax < 70 && smax > 30){
		  $('#sdependency option[value="27"]').attr("selected", "selected");
		}else if(smax <= 30){
		  $('#sdependency option[value="40"]').attr("selected", "selected");
		}
	*/
	if (smax >= 70) {
		$('#sdependency option[value="10"]').attr("selected", "selected");
		$('#sdependency_for_save').val(10);
	}
	else if (smax < 70 && smax > 30) {
		$('#sdependency option[value="20"]').attr("selected", "selected");
		$('#sdependency_for_save').val(20);
	}
	else if (smax <= 30) {
		$('#sdependency option[value="30"]').attr("selected", "selected");
		$('#sdependency_for_save').val(30);
	}
	else {
		$('#sdependency option[value="30"]').attr("selected", "selected");
		$('#sdependency_for_save').val(10);
	}
	// Business Outlook Index Score
	/*
		if($('#bindex').val() == 80){
		  $('#boindex option[value="80"]').attr("selected", "selected");
		}else if($('#bindex').val() == 53){
		  $('#boindex option[value="53"]').attr("selected", "selected");
		}else if($('#bindex').val() == 27){
		  $('#boindex option[value="27"]').attr("selected", "selected");
		}
	*/
	if ($('#bindex').val() == 80) {
		$('#boindex option[value="90"]').attr("selected", "selected");
		$('#boindex_for_save').val(90);
	} else if ($('#bindex').val() == 53) {
		$('#boindex option[value="60"]').attr("selected", "selected");
		$('#boindex_for_save').val(60);
	} else if ($('#bindex').val() == 27) {
		$('#boindex option[value="30"]').attr("selected", "selected");
		$('#boindex_for_save').val(30);
	} else {
		$('#boindex option[value="30"]').attr("selected", "selected");
		$('#boindex_for_save').val(30);
	}

	// Business Owner Experience
	var omax = 0;
	var osum = 0;

	if (0 == $('#entity_type').val()) {
		$('.ckeymanager').each(function () {
			osum += Number($(this).val());
			onum = parseInt($(this).val());
		});
	}
	else {
		$('.cbusowner').each(function () {
			osum += Number($(this).val());
			onum = parseInt($(this).val());
		});
	}

	/*
		if(osum > 5){
		  $('#ownerexp option[value="10"]').attr("selected", "selected");
		}else if(osum < 4 && osum >= 3){
		  $('#ownerexp option[value="5"]').attr("selected", "selected");
		}else if(osum < 3){
		  $('#ownerexp option[value="0"]').attr("selected", "selected");
		}
	*/
	if (osum >= 5) {
		$('#ownerexp option[value="21"]').attr("selected", "selected");
		$('#ownerexp_for_save').val(21);
	} else if (osum <= 4 && osum >= 3) {
		$('#ownerexp option[value="14"]').attr("selected", "selected");
		$('#ownerexp_for_save').val(14);
	} else if (osum < 3) {
		$('#ownerexp option[value="7"]').attr("selected", "selected");
		$('#ownerexp_for_save').val(7);
	} else {
		$('#ownerexp option[value="7"]').attr("selected", "selected");
		$('#ownerexp_for_save').val(7);
	}

	// Management Team Experience
	/*
		if($('#nymteam').val() > 5){
		  $('#mtdependency option[value="10"]').attr("selected", "selected");
		}else if($('#nymteam').val() > 3 && $('#nymteam').val() <= 4){
		  $('#mtdependency option[value="5"]').attr("selected", "selected");
		}else{
		  $('#mtdependency option[value="0"]').attr("selected", "selected");
		}
	*/
	if ($('#nymteam').val() >= 5) {
		$('#mtdependency option[value="21"]').attr("selected", "selected");
		$('#mtdependency_for_save').val(21);
	} else if ($('#nymteam').val() > 3 && $('#nymteam').val() <= 4) {
		$('#mtdependency option[value="14"]').attr("selected", "selected");
		$('#mtdependency_for_save').val(14);
	} else {
		$('#mtdependency option[value="7"]').attr("selected", "selected");
		$('#mtdependency_for_save').val(7);
	}

	// Employee Engagement Score
	/*
		var emmax = 0;
		var emsum = 0;

		$('.empscore').each(function() {
			emsum += Number(jQuery(this).val());
			emnum = parseInt(jQuery(this).val());
		});

		var totalaverageempscore = parseInt(emsum) / jQuery('.emptotalemp').val();

		if(totalaverageempscore > 3){
		  jQuery('#eescore option[value="10"]').attr("selected", "selected");
		}else if(totalaverageempscore == 3){
		  jQuery('#eescore option[value="5"]').attr("selected", "selected");
		}else{
		  jQuery('#eescore option[value="0"]').attr("selected", "selected");
		}
	*/
	// Succession Plan
	/*
		if($('#successplan').val() == 1){
		  $('#successionplan option[value="30"]').attr("selected", "selected");
		}else if($('#successplan').val() == 2){
		  $('#successionplan option[value="20"]').attr("selected", "selected");
		}else{
		  $('#successionplan option[value="10"]').attr("selected", "selected");
		}
	*/
	if ($('#successplan').val() == 1) {
		$('#successionplan option[value="36"]').attr("selected", "selected");
		$('#successionplan_for_save').val(36);
	} else if ($('#successplan').val() == 2) {
		$('#successionplan option[value="24"]').attr("selected", "selected");
		$('#successionplan_for_save').val(24);
	} else {
		$('#successionplan option[value="12"]').attr("selected", "selected");
		$('#successionplan_for_save').val(12);
	}

	// Credit Record
	/*
		if($('#ciscoring1').val() == 1){
		  $('#crdependency option[value="15"]').attr("selected", "selected");
		}else if($('#ciscoring1').val() == 2){
		  $('#crdependency option[value="10"]').attr("selected", "selected");
		}else{
		  $('#crdependency option[value="5"]').attr("selected", "selected");
		}
	*/

	// Credit Facility Established
	/*
		if($('#ciscoring1').val() == 1){
		  $('#crdependency option[value="15"]').attr("selected", "selected");
		}else if($('#ciscoring1').val() == 2){
		  $('#crdependency option[value="10"]').attr("selected", "selected");
		}else{
		  $('#crdependency option[value="5"]').attr("selected", "selected");
		}
	*/

	/*
		if($('#ciscoring2').val() == 1){
		  $('#cfedependency option[value="15"]').attr("selected", "selected");
		}else if($('#ciscoring2').val() == 2){
		  $('#cfedependency option[value="10"]').attr("selected", "selected");
		}else{
		  $('#cfedependency option[value="5"]').attr("selected", "selected");
		}
	*/

	// slow document ready function   });

	$('.rscorenumber').click(function () {

		var cdependency = $('#cdependency_for_save').val();
		var sdependency = $('#sdependency_for_save').val();
		var boindex = $('#boindex_for_save').val();
		var bconsideration = $('#bconsideration_for_save').val();
		// var csexperience = $('#csexperience').val();

		// var totalgroupa = parseInt(cdependency) + parseInt(sdependency) + parseInt(boindex) + parseInt(bconsideration) + parseInt(csexperience);
		var totalgroupa = parseInt(cdependency) + parseInt(sdependency) + parseInt(boindex) + parseInt(bconsideration);
		var gpercentagea = totalgroupa * $('#bgroup').val();

		var ownerexp = $('#ownerexp_for_save').val();
		var mtdependency = $('#mtdependency_for_save').val();
		var bdmaindependency = $('#bdmaindependency_for_save').length > 0 ? $('#bdmaindependency_for_save').val() : 0;
		var successionplan = $('#successionplan').val();
		var ppfuture = $('#ppfuture_for_save').length > 0 ? $('#ppfuture_for_save').val() : 0;
		// var crdependency = $('#crdependency').val();
		// var cfedependency = $('#cfedependency').val();

		// var totalgroupb = parseInt(ownerexp) + parseInt(mtdependency) + parseInt(bdmaindependency) + parseInt(successionplan) + parseInt(ppfuture) + parseInt(crdependency) + parseInt(cfedependency);
		var totalgroupb = parseInt(ownerexp) + parseInt(mtdependency) + parseInt(bdmaindependency) + parseInt(successionplan) + parseInt(ppfuture);
		var gpercentageb = totalgroupb * $('#mgroup').val();

		/** Financial Condition Calculation */
		var fin_position = $('#ratingfiscore').find("option:selected").text();
		var fin_performance = $('#ratingfinancialperformancescore').find("option:selected").text();
		var fin_condition = (parseFloat(fin_position) * 0.6) + (parseFloat(fin_performance) * 0.4);

		var fc_drop_down = $('#ratingfiscore').find('option');
		var rr_raw = [];
		var rr_score = [];

		fc_drop_down.each(function () {
			var fc_raw = $(this).text();
			var fc_score = $(this).val();
			if (fc_score != '') {
				rr_raw.push(fc_raw);
				rr_score.push(fc_score);
			}
		});

		rr_score_exact = rr_raw.indexOf(fin_condition.toString());

		if (-1 != rr_score_exact) {
			var faccscore = rr_score[rr_score_exact];
		}
		else {
			for (var idx = rr_raw.length; 0 <= idx; idx--) {
				var rr_raw_low = 0;
				var rr_raw_high = 0;
				var idx_high = 0;

				if (idx > 0) {
					idx_high = idx - 1;
					rr_raw_low = parseFloat(rr_raw[idx]);
					rr_raw_high = parseFloat(rr_raw[idx_high]);

					if ((fin_condition >= rr_raw_low)
						&& (fin_condition <= rr_raw_high)) {

						var faccscore = rr_score[idx];
						var faccraw = rr_raw[idx];
						break;
					}
				}
				else {
					var faccscore = rr_score[0];
					var faccraw = rr_raw[0];
				}
			}
		}

		$('#faccscore').val(faccscore);

		var totalgroupc = parseInt(faccscore);
		var gpercentagec = totalgroupc * $('#fgroup').val();

		var gscore = Math.ceil(gpercentagea) + Math.ceil(gpercentageb) + Math.ceil(gpercentagec);

		// scoring update: boost if same tier
		var bcc_tier, mq_tier, fa_tier;
		if (totalgroupa >= 191) bcc_tier = 'top';
		if (totalgroupa <= 190 && totalgroupa >= 81) bcc_tier = 'mid';
		if (totalgroupa <= 80) bcc_tier = 'low';

		if (totalgroupb >= 125) mq_tier = 'top';
		if (totalgroupb <= 124 && totalgroupb >= 51) mq_tier = 'mid';
		if (totalgroupb <= 50) mq_tier = 'low';

		if (totalgroupc >= 150) fa_tier = 'top';
		if (totalgroupc <= 149 && totalgroupc >= 96) fa_tier = 'mid';
		if (totalgroupc <= 95) fa_tier = 'low';

		// insert DSCR boost
		var gscore_dscr = 0;
		if (DSCR() !== false) {
			if (DSCR() >= 1.65) {
				gscore_dscr = gscore * 0.15;
			}
			else if (DSCR() >= 1.20 && DSCR() <= 1.64) {
				gscore_dscr = gscore * 0.12;
			}
			else if (DSCR() >= 0.85 && DSCR() <= 1.19) {
				gscore_dscr = gscore * 0.07;
			}
			else if (DSCR() >= 0.75 && DSCR() <= 0.84) {
				gscore_dscr = 0;
			}
			else if (DSCR() < 0.75) {
				gscore_dscr = gscore * -0.07;
			}
		}

		// boost for top top top
		if (bcc_tier == 'top' && mq_tier == 'top' && fa_tier == 'top') {
			gscore = Math.ceil(gscore * 1.05);
		}

		// boost for mid mid mid
		if (bcc_tier == 'mid' && mq_tier == 'mid' && fa_tier == 'mid') {
			gscore = Math.ceil(gscore * 1.05);
		}

		// drop for low low low
		if (bcc_tier == 'low' && mq_tier == 'low' && fa_tier == 'low') {
			gscore = Math.ceil(gscore * 0.95);
		}

		gscore = gscore + gscore_dscr;
		//JM - temporarily hard coded trans(messages.result_n) code because it is not working in js files
		if (gscore >= 177) {
			var svalent = trans('messages.result_1');
		} else if (gscore >= 150 && gscore <= 176) {
			var svalent = trans('messages.result_2');
		} else if (gscore >= 123 && gscore <= 149) {
			var svalent = trans('messages.result_3');
		} else if (gscore >= 96 && gscore <= 122) {
			var svalent = trans('messages.result_4');
		} else if (gscore >= 68 && gscore <= 95) {
			var svalent = trans('messages.result_5');
		} else if (gscore < 68) {
			var svalent = trans('messages.result_6');
		} else if (gscore < 50) {
			var svalent = trans('messages.result_7');
		} else if (gscore < 40) {
			var svalent = trans('messages.result_8');
		} else if (gscore < 30) {
			var svalent = trans('messages.result_9');
		} else {
			var svalent = trans('messages.result_10');
		}

		if (!isNaN(gscore)) {
			$('#riskscoretotal').html(gscore + ' - ' + svalent);
		}

		if (
			isNaN(gscore) &&
			$('#ppfuture_for_save').length > 0 &&
			$('#bdmaindependency_for_save') > 0
		) {
			$('#riskscoringbutton').attr("disabled", true);
		} else {
			if ($('#ratingfiscore').val() != '' && $('#ratingfinancialperformancescore').val() != '') {
				$('#riskscoringbutton').attr("disabled", false);
			} else {
				$('#riskscoringbutton').attr("disabled", true);
			}
		}
        if(!faccraw) faccraw = faccscore;
		$('#farawscore_for_save').val(faccraw);
		$('#faccscore_for_save').val(faccscore);
		$('#riskscoretotal_for_save').val('' + gscore + ' - ' + svalent + '');

	});
	$('.rscorenumber').click();

	// Mirror change trigger
	$('#ppfuture').change(function () {
		$('#ppfuture_mirror').val($('#ppfuture option:selected').text());
		$('#ppfuture_for_save').val($('#ppfuture option:selected').val());
	});
	$('#bdmaindependency').change(function () {
		$('#bdmaindependency_mirror').val($('#bdmaindependency option:selected').text());
		$('#bdmaindependency_for_save').val($('#bdmaindependency option:selected').val());
	});

  /*
  $('#bconsideration').change(function() {
    $('#bconsideration_mirror').val($('#bconsideration option:selected').text());
    $('#bconsideration_for_save').val($('#bconsideration option:selected').val());
  });
  */
	reflow_charts();

	// analyst double check prompt
	$('.analyst_check').on('change', function () {
		var sStandard = $(this).attr('id');
		sStandard = sStandard.substring(0, sStandard.length - 1); //remove number

		var uiThis = $(this);
		var valueThis = $(this).val();
		var valueStandard = $('#' + sStandard).val();

		var deviation = (valueThis - valueStandard) / valueStandard;


		$(this).parent().find('span.error-deviation').remove();
		if (deviation > 2 || deviation < -2) {
			$(this).parent().append('<span class="error-deviation" style="color: RED; font-size: 12px;">Please double check, this value might be wrong.</span>');
		}
	});

	$('#riskscoringbutton').on('click', function (e) {
		var valid = true;
		var gross_rev_growth1 = $('#gross_revenue_growth1');
		var gross_rev_growth2 = $('#gross_revenue_growth2');
		var net_inc_growth1 = $('#net_income_growth1');
		var net_inc_growth2 = $('#net_income_growth2');
		var gross_profit_marg1 = $('#gross_profit_margin1');
		var gross_profit_marg2 = $('#gross_profit_margin2');
		var net_profit_marg1 = $('#net_profit_margin1');
		var net_profit_marg2 = $('#net_profit_margin2');
		var net_cash_marg1 = $('#net_cash_margin1');
		var net_cash_marg2 = $('#net_cash_margin2');
		var current_ratio1 = $('#current_ratio1');
		var current_ratio2 = $('#current_ratio2');
		var debt_equity_ratio1 = $('#debt_equity_ratio1');
		var debt_equity_ratio2 = $('#debt_equity_ratio2');

		var receivables_turnover1 = $('#receivables_turnover1');
		var receivables_turnover2 = $('#receivab2es_turnover2');
		var receivables_turnover3 = $('#receivab2es_turnover3');

		var inventory_turnover1 = $('#inventory_turnover1');
		var inventory_turnover2 = $('#inventory_turnover2');
		var inventory_turnover3 = $('#inventory_turnover3');

		var accounts_payable_turnover1 = $('#accounts_payable_turnover1');
		var accounts_payable_turnover2 = $('#accounts_payable_turnover2');
		var accounts_payable_turnover3 = $('#accounts_payable_turnover3');

		var cash_conversion_cycle1 = $('#cash_conversion_cycle1');
		var cash_conversion_cycle2 = $('#cash_conversion_cycle2');
		var cash_conversion_cycle3 = $('#cash_conversion_cycle3');

		var average_daily_balance_save = $('#average_daily_balance_save');
		var operating_cashflow_margin_save = $('#operating_cashflow_margin_save');
		var operating_cashflow_ratio_save = $('#operating_cashflow_ratio_save');

		var final_report_pdf = $('input[name="file"]');

		$('#analysis-error-msg').html('');

		/**-------------------------------------------------------------------
		/*	Regular Expression Check
		/*------------------------------------------------------------------*/
		valid = fc_validation_checkRegexp(gross_rev_growth1, /^-?[0-9]\d*(\.\d+)?$/i, "Gross Revenue Growth should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(gross_rev_growth2, /^-?[0-9]\d*(\.\d+)?$/i, "Gross Revenue Growth should be numeric");
		valid = fc_validation_checkRegexp(net_inc_growth1, /^-?[0-9]\d*(\.\d+)?$/i, "Net income growth should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(net_inc_growth2, /^-?[0-9]\d*(\.\d+)?$/i, "Net income growth should be numeric");
		valid = fc_validation_checkRegexp(gross_profit_marg1, /^-?[0-9]\d*(\.\d+)?$/i, "Gross Profit Margin should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(gross_profit_marg2, /^-?[0-9]\d*(\.\d+)?$/i, "Gross Profit Margin should be numeric");
		valid = fc_validation_checkRegexp(net_profit_marg1, /^-?[0-9]\d*(\.\d+)?$/i, "Net profit margin should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(net_profit_marg2, /^-?[0-9]\d*(\.\d+)?$/i, "Net profit margin should be numeric");
		valid = fc_validation_checkRegexp(net_cash_marg1, /^-?[0-9]\d*(\.\d+)?$/i, "Net cash margin should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(net_cash_marg2, /^-?[0-9]\d*(\.\d+)?$/i, "Net cash margin should be numeric");
		valid = fc_validation_checkRegexp(current_ratio1, /^-?[0-9]\d*(\.\d+)?$/i, "Current ratio should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(current_ratio2, /^-?[0-9]\d*(\.\d+)?$/i, "Current ratio should be numeric");
		valid = fc_validation_checkRegexp(debt_equity_ratio1, /^-?[0-9]\d*(\.\d+)?$/i, "Debt to equity ratio should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(debt_equity_ratio2, /^-?[0-9]\d*(\.\d+)?$/i, "Debt to equity ratio should be numeric");

		valid = fc_validation_checkRegexp(receivables_turnover1, /^-?[0-9]\d*(\.\d+)?$/i, "Receivables Turnover should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(receivables_turnover2, /^-?[0-9]\d*(\.\d+)?$/i, "Receivables Turnover should be numeric");
		valid = valid && fc_validation_checkRegexp(receivables_turnover3, /^-?[0-9]\d*(\.\d+)?$/i, "Receivables Turnover should be numeric");
		valid = fc_validation_checkRegexp(inventory_turnover1, /^-?[0-9]\d*(\.\d+)?$/i, "Inventory turnover should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(inventory_turnover2, /^-?[0-9]\d*(\.\d+)?$/i, "Inventory turnover should be numeric");
		valid = valid && fc_validation_checkRegexp(inventory_turnover3, /^-?[0-9]\d*(\.\d+)?$/i, "Inventory turnover should be numeric");
		valid = fc_validation_checkRegexp(accounts_payable_turnover1, /^-?[0-9]\d*(\.\d+)?$/i, "Account Payable turnover should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(accounts_payable_turnover2, /^-?[0-9]\d*(\.\d+)?$/i, "Account Payable turnover should be numeric");
		valid = valid && fc_validation_checkRegexp(accounts_payable_turnover3, /^-?[0-9]\d*(\.\d+)?$/i, "Account Payable turnover should be numeric");
		valid = fc_validation_checkRegexp(cash_conversion_cycle1, /^-?[0-9]\d*(\.\d+)?$/i, "Cash Conversion cycle should be numeric") && valid;
		valid = valid && fc_validation_checkRegexp(cash_conversion_cycle2, /^-?[0-9]\d*(\.\d+)?$/i, "Cash Conversion cycle should be numeric");
		valid = valid && fc_validation_checkRegexp(cash_conversion_cycle3, /^-?[0-9]\d*(\.\d+)?$/i, "Cash Conversion cycle should be numeric");

		valid = fc_validation_checkRegexp(average_daily_balance_save, /^-?[0-9]\d*(\.\d+)?$/i, "Average daily balance should be numeric") && valid;
		valid = fc_validation_checkRegexp(operating_cashflow_margin_save, /^-?[0-9]\d*(\.\d+)?$/i, "Operating cashflow margin should be numeric") && valid;
		valid = fc_validation_checkRegexp(operating_cashflow_ratio_save, /^-?[0-9]\d*(\.\d+)?$/i, "Operating cashflow ratio should be numeric") && valid;

		if ('' == final_report_pdf.val()) {
			valid = 0;
			$('#analysis-error-msg').append('Uploading of Financial Report PDF is required');
		}

		if (valid) {
			$('#analyst_form').submit();
		}
		else {
			$('#analysis-error-msg').show();

			$('body').scrollTop(200);

			return false;
		}


	});


	function fc_validation_checkRegexp(sel, regexp, warning) {
		var warn_cont = $('#analysis-error-msg');

		if (0 >= sel.val().length) {
			return true;
		}

		/**-------------------------------------------------------------------
		/*	Regular Expression check failed
		/*------------------------------------------------------------------*/
		if (!(regexp.test(sel.val()))) {
			warn_cont.append(warning + '<br/>');
			return false;
		}
		/**-------------------------------------------------------------------
		/*	Regular Expression check successful
		/*------------------------------------------------------------------*/
		else {
			return true;
		}
	}

	$('.fnumeric').on('keypress', function (evt) {
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode != 46 && charCode > 31
			&& (charCode < 48 || charCode > 57))
			return false;

		return true;
	});

	// Potential Credit Quality Issue
	$('#biz-details-cntr').on('click', '.potential_credit_quality_issues input[type="checkbox"]', function (e) {
		var uiThis = $(this);
		var PCQIselected = '';
		$('.potential_credit_quality_issues input[type="checkbox"]:checked').each(function () {
			PCQIselected += $(this).val() + ',';
		});
		PCQIselected = PCQIselected.slice(0, -1);
		$.ajax({
			url: BaseURL + '/sme/update_pcqi',
			type: 'post',
			data: {
				entity_id: $('#entityids').val(),
				potential_credit_quality_issues: PCQIselected
			},
			beforeSend: function () {
				uiThis.parent().append('<img class="preloader" src = "' + BaseURL + '/images/preloader.gif" />');
			},
			success: function (oData) {
				uiThis.parent().find('.preloader').remove();
			}
		});
	});

	// function checkLatestGrdp() {
	// 	// $('#ssc').on('click', '#printthis', function () {
	// 	// 	return false;
	// 	// });

	// 	//$('#printthis').text('Checking PDF... Please Wait...');

	// 	$.get(BaseURL + '/check/cbpo_pdf/' + $('#entityids').val(), function (svr_resp) {
	// 		if (1 == svr_resp) {
	// 			type_report = $('#type_report').val();
	// 			if (type_report == 2) {
	// 				$('#printthis').text(trans('messages.print') + ' Rating Report');
	// 			} else {
	// 				$('#printthis').text(trans('messages.print') + ' Rating Report + Analysis');
	// 			}
	// 			//$('#ssc').off('click', '#printthis');
	// 		}
	// 	});
	// }

	// if (7 == $('#status').val()) {
	// 	checkLatestGrdp();
	// }

	$('#auto_ocr').click(function (e) {
		$('#auto_ocr_success').hide();
		$('#auto_ocr_status').show();
		e.preventDefault();
		$.ajax({
			url: BaseURL + '/tools/auto_ocr/' + $('#entityids').val(),
			type: 'get',
			success: function (response) {
				var average_daily_balance = $.trim(response);
				if ($.isNumeric(average_daily_balance)) {
					$('#average_daily_balance_save').val(average_daily_balance);
					$('#auto_ocr_status').hide();
					$('#auto_ocr_success').show();
				}
				else {
					$('#auto_ocr_status').html('(' + response + ')');
				}
			}
		});
	});

	/*--------------------------------------------------------------------
	/*	When Auto-compute Cashflow is clicked
	/*------------------------------------------------------------------*/
	$('#auto_cashflow').click(function () {

		/*---------------------------------------------------------------
		/*	Variable Declaration
		/*-------------------------------------------------------------*/
		var cashflow_url = BaseURL + '/ajax/tools/auto_cashflow/' + $('#entityids').val();
		var adb_val = $('#average_daily_balance_save').val();

		/*---------------------------------------------------------------
		/*	Average Daily Balance exists
		/*-------------------------------------------------------------*/
		$.ajax({
			url: cashflow_url,
			type: 'POST',
			dataType: 'json',
			data: { 'adb_val': adb_val },
			/*----------------------------------------------------------
			/*	Before AJAX Call
			/*--------------------------------------------------------*/
			beforeSend: function () {
				$('#auto_cashflow_status').html('(Computing Operating Cashflow...)');
			},
			/*----------------------------------------------------------
			/*	AJAX Processing is successful
			/*--------------------------------------------------------*/
			success: function (response) {
				/*-----------------------------------------------------
				/*	Computation Successful
				/*---------------------------------------------------*/
				if (1 == response.sts) {
					$('#operating_cashflow_margin_save').val(response.cashflow_margin);
					$('#operating_cashflow_ratio_save').val(response.cashflow_ratio);
					$('#auto_cashflow_status').html(response.msg);
				}
				/*-----------------------------------------------------
				/*	Computation Failed
				/*---------------------------------------------------*/
				else {
					/*------------------------------------------------
					/*	Try calculate from FA Inputs of Revenue
					/*----------------------------------------------*/
					if ((0 != $('#gross_revenue_growth2').val())
						|| ('' != $('#gross_revenue_growth2').val())) {
						var revenue = parseFloat($('#gross_revenue_growth2').val());
						var revenue_6m = revenue / 2;
						var ocm_val = 0;

						if (0 != revenue_6m) {
							ocm_val = adb_val / revenue_6m;
						}

						$('#operating_cashflow_margin_save').val(ocm_val);
						$('#auto_cashflow_status').html('Operating Cashflow Ratio cannot be computed with current uploaded FS Template.');
					}
					else {
						$('#auto_cashflow_status').html('Latest Gross Revenue Growth is required');
					}

				}
			}
		});

		return false;
	});
});

function ClickTab(sName) {
	sId = sName.replace('tab', '');
	if (sId != "1") {
		$('ul#side-menu li:nth-child(' + sId + ') a').click();
	}
}

window.onload = function () {
	reflow_charts();
	worker();
	worker2();
};
setInterval(function () { reflow_charts(); }, 1000);

function addHyphen (element) {
	let ele = document.getElementById(element.id);
	 ele = ele.value.split('-').join('');    // Remove dash (-) if mistakenly entered.
	
	finalVal = ele.match(/.{3}(?=.{2,3})|.+/g).join('-');
	document.getElementById(element.id).value = finalVal;
}

function reflow_charts() {
	if (Highcharts) {
		for (x in Highcharts.charts) {
			Highcharts.charts[x].reflow();
		}
	}
}

function worker() {
	var entity_id = $('#entityids').val();
	if (entity_id != undefined && entity_id != '' && entity_id != 0) {
		$.ajax({
			url: BaseURL + '/api/check_invalid_ub',
			data: { id: entity_id },
			type: 'get',
			dataType: 'json',
			success: function (oData) {
				if (oData.length > 0) {
					$('#invalid_utility_bill_modal p.bills').html('');
					var arList = [];
					for (x in oData) {
						$('#invalid_utility_bill_modal p.bills').append(oData[x].document_orig + '<br />');
						arList.push(oData[x].documentid);
					}
					$('#invalid_utility_bill_modal').modal();
					$('#utility_bill_modal_ok').off('click').on('click', function () {
						$.ajax({
							url: BaseURL + '/api/update_invalid_ub',
							data: { id: arList },
							type: 'post',
							success: function () {
								$('#invalid_utility_bill_modal').modal('hide');
							}
						});
					});
				}
			},
			complete: function () {
				setTimeout(worker, 10000);
			}
		});
	}
}

function worker2() {
	var entity_id = $('#entityids').val();
	if (entity_id != undefined && entity_id != '' && entity_id != 0) {
		$.ajax({
			url: BaseURL + '/api/check_invalid_bs',
			data: { id: entity_id },
			type: 'get',
			dataType: 'json',
			success: function (oData) {
				if (oData.length > 0) {
					$('#invalid_bank_statement_modal p.bills').html('');
					var arList = [];
					for (x in oData) {
						$('#invalid_bank_statement_modal p.bills').append(oData[x].document_orig + '<br />');
						arList.push(oData[x].documentid);
					}
					$('#invalid_bank_statement_modal').modal();
					$('#bank_statement_modal_ok').off('click').on('click', function () {
						$.ajax({
							url: BaseURL + '/api/update_invalid_bs',
							data: { id: arList },
							type: 'post',
							success: function () {
								$('#invalid_bank_statement_modal').modal('hide');
							}
						});
					});
				}
			},
			complete: function () {
				setTimeout(worker2, 10000);
			}
		});
	}
}

function DSCR() {
	if ($('#ds_noi').val() == "" || $('#ds_loan').val() == "" || $('#ds_interest').val() == "" || $('#ds_amortization').val() == "") {
		return false;
	}
	var NOI = parseFloat($('#ds_noi').val());
	var LA = parseFloat($('#ds_loan').val());
	var IR = parseFloat($('#ds_interest').val());
	var amort = parseFloat($('#ds_amortization').val());
	var AIR = IR / 100;
	var MIR = AIR / 12;
	var MonthlyTerm = amort * 12;
	var MonthlyPayment = LA * (MIR / (1 - (Math.pow(1 + MIR, -MonthlyTerm))));
	var result = parseFloat(NOI / (MonthlyPayment * 12)).toFixed(2);
	if (result > 5) { result = 5; }

	if (isNaN(result) == true) {
		return false;
	} else {
		return Math.round(10 * result) / 10;
	}
}

// contractor document approval
$("#btn_assoc_submit").unbind().on("click", function(e){
		e.preventDefault();
		var items = [];
		$("input[name='check_document[]']:checked").each(function(){
			items.push(this.value);
		});
		postDocumentVerification("/contractor/submit_document", items, 'submit');
});

$("#btn_assoc_save").unbind().on("click", function(e){
	e.preventDefault();
	var items = [];
	$("input[name='check_document[]']:checked").each(function(){
		items.push(this.value);
	});
	postDocumentVerification("/contractor/save_document",items, 'save');
});

//things should work as it is
$("input[name='check_document[]']").unbind().on("click",function(){
	var val = $(this).val();
	cnt_checkbox = $("input[name='check_document[]']"); // total number of checkbox
	cnt_check =  $("input[name='check_document[]']:checked"); //get number of checked checkbox
	if(cnt_check.length  == (cnt_checkbox.length / 2)) {
		$("#btn_assoc_submit").attr("disabled", false)
	}else {
		$("#btn_assoc_submit").attr("disabled", true)
	}
	if(val.includes("_approve")) {
		val =val.replace("_approve", "_deny");
	} else {
		val =val.replace("_deny", "_approve");
	}

	if(this.checked) {
		$("input[value='" + val + "'] ").attr("disabled", true);
	} else {
		$("input[value='" + val + "'] ").removeAttr("disabled", true);
	}
});

// submit or save document verification
function postDocumentVerification(url, items, action){
	$.ajax({
		url: url,
		type: 'post',
	data: {
			entity_id: $('#entityids').val(),
			doc_status: items
		},
		success: function (resp) {
			// do something after saving
			if(resp.success == true) {
				if(action ==  'save') {
					$("#success_message").attr("hidden", false);
					setTimeout(() => {
						$("#success_message").attr("hidden", true);
					}, 15000);
				} else {
					window.location.href = resp.refresh_url;
					window.location.load();
				}
			}
		}
	});
}

// sometimes magic is what we need. 
$("input[name='check_document']").ready(function(){
	var items = [];
	$("input[name='check_document[]']:checked").each(function(){
		items.push(this.value);
	});
	for(i = 0; i < items.length; i++) {
		var val = items[i];
		if(val.includes("_approve")) {
			val =val.replace("_approve", "_deny");
		} else {
			val =val.replace("_deny", "_approve");
		}

		$("input[value='" + val + "'] ").attr("disabled", true);
	}	
});