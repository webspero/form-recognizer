//======================================================================
//  Script S54: smesummary_progressbar.js
//      Scripts for Questionnaire progress bar
//======================================================================
var progress_total = 0;
var progress_filled = 0;
var progress_additional_total = 0;
var progress_additional_filled = 0;

$(document).ready(function(){
    
	update_progress_signs();
});

var update_progress_signs = function(){
    progress_total              = 0;
    progress_filled             = 0;
    progress_additional_total   = 0;
    progress_additional_filled  = 0;
    
   check_completeness('#registration1', 'ul#side-menu li.pr1');
   check_completeness('#registration2', 'ul#side-menu li.pr2');
   check_completeness('#registration3', 'ul#side-menu li.assocFiles');
	// check_completeness('#payfactor', 'ul#side-menu li.pr3');
	// check_completeness('#bcs', 'ul#side-menu li.pr4');
	// check_completeness('#pfs', 'ul#side-menu li.pr5');
	// check_completeness('#ownerdetails', 'ul#side-menu li.pr6');
    
	populate_progress_bar();
};

var populate_progress_bar = function(){

	var percent_complete = Math.floor((progress_filled / progress_total) * 100);
	$('.progressbar-container .progressbar-count').text(percent_complete + trans('messages.q_complete'));
	$('.progressbar-container .progressbar-wrap .progressbar-full').animate({
		width: percent_complete + '%'
	}, 1500);
    
	var add_percent_complete = Math.floor((progress_additional_filled / progress_additional_total) * 40);
	
    if ((100 <= percent_complete) 
    || (0 < $('#status').val())){
		percent_complete = 100;
		$('.progressbar-container .progressbar-count').text('100' + trans('messages.q_complete'));
		$('#credit_risk_rating_submit').removeAttr('disabled');
        $('#credit_risk_rating_submit').text(trans('messages.submit_for_credit_risk_rating'));
	}
    else {
		$('#credit_risk_rating_submit').attr('disabled', 'disabled');
		$('#credit_risk_rating_submit').text(percent_complete + trans('messages.p_complete'));
	}
	
	//update in db
	$.ajax({
		url: BaseURL + '/update_progress_completion',
		type: 'post',
		data: { 
			entity_id: $('#entityids').val(),
			completion: percent_complete 
		},
		success: function(){}
	});
};

var check_completeness = function(uiContainer, uiTarget){
	var total_required = 0;
	var total_filled = 0;
	var additional_required = 0;
	var additional_filled = 0;
	
	$(uiContainer).find('.progress-required').each(function(){
		total_required++;
		if($.trim($(this).text()) != "" && $.trim($(this).text()) != 0 && $.trim($(this).text()) != "Choose here") {
            total_filled++;
        }
        
	});
	
	$(uiContainer).find('.progress-required-counter').each(function(){
		total_required += parseInt($(this).attr('min-value'));
		total_filled += parseInt($(this).text());
	});
	
	$(uiContainer).find('.progress-field').each(function(){
		additional_required++;
		if($.trim($(this).text()) != "") additional_filled++;
	});
	
	$(uiContainer).find('.progress-field-counter').each(function(){
		additional_required += parseInt($(this).attr('min-value'));
		additional_filled += parseInt($(this).text());
	});
	
	if (total_filled > 0) {
		if ((total_required == total_filled)
        || (0 < $('#status').val())) {
			$(uiTarget).append('<span class="progress-check"></span>');
		}
        else {
			$(uiTarget).append('<span class="progress-dots"></span>');
		}
	}
	progress_total += total_required;
	progress_filled += total_filled;
	progress_additional_total += additional_required;
	progress_additional_filled += additional_filled;
};