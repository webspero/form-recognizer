
//======================================================================
//  Script S11: bsp-bank-extraction.js
//      Extracts the Bank Details from PSE Website
//======================================================================
$(document).ready(function() {

//-----------------------------------------------------
//  Binding S11.1: body
//      Extracts all Banks from PSE Website and save
//      it to the database upon page load
//-----------------------------------------------------
function initBspBankExtraction()
{
	var ajax_url		= BaseURL+'/bsp/bank-extraction';
	var ajax_load_cntr	= ' td.linkover';
	
	var ajax_upd_url	= BaseURL+'/bsp/update-bank-data';
	var bank_list		= [];
	
	var bank_list_cntr	= $('#bank_list');
	
	$('#extraction_container').load(ajax_url+ajax_load_cntr, function(){
		$('#extraction_container').find('td.linkover:eq(0)').remove();
		
		var bank_list_tb 	= $('table.tableborder').find('tr.tableborder');
		var bank_ctr		= 0;
		
		bank_list_tb.each(function(){
			
			var bank_data = [];
		
			$(this).find('td.tableborder').each(function(idx) {
				
				switch (idx) {
					
					case 1:
						bank_data[0] = $(this).text();
						break;
					
					case 2:
						bank_data[1] = $(this).text();
						break;
						
					case 3:
						var contact_position	= $(this).find('em');
						
						bank_data[3] = '';
						
						if (0 < contact_position.length) {
							bank_data[3] = contact_position.text();
							contact_position.remove();
						}
						
						bank_data[2] = $(this).text();
						break;
					
					case 4:
						var no_offices	= $(this).find('em');
						
						bank_data[5] = '';
						
						if (0 < no_offices.length) {
							bank_data[5]	= no_offices.text().match(/\d+/)[0];
							no_offices.remove();
						}
						
						bank_data[4]	= $(this).text();
						break;
					
					case 5:
						var fax_num	= $(this).find('em');
						
						bank_data[7] = '';
						
						if (0 < fax_num.length) {
							fax_str	= fax_num.text().split(':');
							bank_data[7]	= fax_str[1].trim();
							fax_num.remove();
						}
						
						bank_data[6]	= $(this).text();
						break;
						
					case 6:
						var bank_website	= $(this).find('a');
						
						bank_data[9] = '';
						
						if (0 < bank_website.length) {
							bank_data[9]	= bank_website.text();
							bank_website.remove();
							$(this).find('em').remove();
						}
						
						bank_data[8]	= $(this).text();
						break;
				}

			});
			
			setTimeout(function(){
				
				bank_data[10]	= bank_ctr;
				
				$.ajax({
					url: ajax_upd_url,
					data: { 'bank_data': bank_data },
					type: 'post',
					dataType: 'json',
					beforeSend: function() {
						bank_list_cntr.append('<div id = "bank_process_'+bank_ctr+'"> Processing '+bank_data[0]+'...</div>')
					},
					success: function(response) {
					
						console.log('#bank_process_'+response.bank_ctr);
						$('#bank_process_'+response.bank_ctr).text(response.bank_name+' Update Completed');
					}
				});
				
				bank_ctr++;
				
			}, '7000');
			
		});
		
	});
}

initBspBankExtraction();

});