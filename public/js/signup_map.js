var message = "Sorry but we cant locate your exact location. Make sure that your region, major area and city is identified. You can also click close and go back to new report and select your major area and city.";
var majorArea = "";
var province = "";
var province_code = "";
var country_code = "";
var select_city = "";
var select_city_code = "";

function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -33.8688, lng: 151.2195 },
    componentRestrictions: {country: "ph"},
    zoom: 13,
  });
  const card = document.getElementById("pac-card");
  const input = document.getElementById("pac-input");
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
  const autocomplete = new google.maps.places.Autocomplete(input);
  // Bind the map's bounds (viewport) property to the autocomplete object,
  // so that the autocomplete requests use the current map bounds for the
  // bounds option in the request.
  autocomplete.bindTo("bounds", map);
  // Set the data fields to return when the user selects a place.
  autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
  const infowindow = new google.maps.InfoWindow();
  const infowindowContent = document.getElementById("infowindow-content");
  infowindow.setContent(infowindowContent);
  const marker = new google.maps.Marker({
    map,
    anchorPoint: new google.maps.Point(0, -29),
  });
  autocomplete.addListener("place_changed", () => {
    infowindow.close();
    marker.setVisible(false);
    const place = autocomplete.getPlace();

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      windowalert("Not a valid location. Please select location from suggested places. Thank you!");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17); // Why 17? Because it looks good.
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    let address = "";

    if (place.address_components) {
      address = [
        (place.address_components[0] &&
          place.address_components[0].short_name) ||
          "",
        (place.address_components[1] &&
          place.address_components[1].short_name) ||
          "",
        (place.address_components[2] &&
          place.address_components[2].short_name) ||
          "",
      ].join(" ");
      console.log(place.address_components);
      getAddressObject(place.address_components);
    }
    infowindowContent.children["place-icon"].src = place.icon;
    infowindowContent.children["place-name"].textContent = place.name;
    infowindowContent.children["place-address"].textContent = address;
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    const radioButton = document.getElementById(id);
    radioButton.addEventListener("click", () => {
      autocomplete.setTypes(types);
    });
  }
  setupClickListener("changetype-all", []);
  setupClickListener("changetype-address", ["address"]);
  setupClickListener("changetype-establishment", ["establishment"]);
  setupClickListener("changetype-geocode", ["geocode"]);
  document
    .getElementById("use-strict-bounds")
    .addEventListener("click", function () {
      console.log("Checkbox clicked! New state=" + this.checked);
      autocomplete.setOptions({ strictBounds: this.checked });
    });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function getAddressObject(address_components){
  
    var ShouldBeComponent = {
        home: [
            "route",
            "neighborhood",
            "sublocality_level_1",
        ],
        province: [
            "administrative_area_level_2"
        ],
        city: [
            "locality",
            "sublocality",
            "sublocality_level_1",
            "administrative_area_level_2",
            "administrative_area_level_3",
            "administrative_area_level_4",
        ],
        region : [
            "administrative_area_level_1", 
        ],
        country: [
        	"country"
        ]
    };

    var address = {
        home: [],
        province: [],
        city: [],
        region: [],
        country: []
    };

    // console.log('address_components');
    // console.log(address_components);

    address_components.forEach(component => {
        for(var shouldBe in ShouldBeComponent){
            if (ShouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1 ){
                if(shouldBe == "region"){
                    address[shouldBe].push(component.short_name);
                }else{
                    address[shouldBe].push(component.long_name);
                }

                if(shouldBe == "country" && component.short_name != "PH"){
                	$('#country').val('INT');
                }
            }
        }
    });

    // console.log('address');
    // console.log(address);

    $.ajax({
        url: siteUrl + '/checkExistingLocation',
        type: 'get',
        dataType: 'json',
        data: {
            address : address
        },
        success: function(resp){
            if(resp){ 
                $.each(resp, function(key,val){
                    if(key == "provDesc"){
                        province = val;
                    }else if(key == "provCode"){
                        province_code = val;
                    } else if (key == "citymunDesc") {
                        select_city = val;
                    } else if (key == "citymunCode") {
                        select_city_code = val;
                    }
                });
            }else{
                province = null;
            }
        }
    });
}

function saveLocation(){
    if(province_code){
        $("#province").val(province_code).change();
        province_code = null;
        $("#changetype-all").val("");
        $('#modalMap').modal('hide');
    }else{
        if($('#changetype-all').val() == ""){
            alert("Please enter your location.");
        }else{
            alert(message);
        }
    }
}