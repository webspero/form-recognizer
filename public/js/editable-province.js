//======================================================================
//  Script S25: editable-province.js
//      Provinces input field scripts
//======================================================================
$(document).ready(function() {
    $('#editable-field-form').addClass('form-block');
    $('#editable-field-form').removeClass('form-inline');
    
   
    if ('' == $('#prev_val').val()) {
        $('#province').change();
    }
});

function getCities(){
	var sProvince = $('#province').val();
    var uiCity = $('#city');
    $.ajax({
        url: BaseURL + '/api/cities',
        type: 'get',
        dataType: 'json',
        data: {
            province: sProvince
        },
        beforeSend: function() {
            $('.city-preloader').show();
            uiCity.html('<option>&nbsp;&nbsp;&nbsp;Loading cities...</option>');
        },
        success: function(oData)	{
            $('.city-preloader').hide();
            uiCity.html('<option value="">Choose your city</option>');
            for(x in oData){
                uiCity.append('<option value="'+oData[x].id+'">'+oData[x].citymunDesc+'</option>');
            }
        }
    });
}