//======================================================================
//  Script S14: busowner.js
//      JS file for owner details manip
//======================================================================
$(document).ready(function() {

  jQuery("#formbpo").validationEngine('attach');

	$("#birthdate").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});

  $("#sbirthdate").datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    yearRange: '-100:+0',
	onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
  });
  
  $("#km_birthdate").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});
	
	$("#km2_birthdate").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});

  $('#addmore').on('click', function () {
    var id = $(".busownwrapper:last").attr('id');
    var appendDiv = jQuery($(".busownwrapper:last")[0].outerHTML);
      appendDiv.attr('id', ++id).insertAfter(".busownwrapper:last");
  });

  $('.provinceselected option[value="81"]').attr('selected', 'selected'); 
});