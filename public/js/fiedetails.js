//======================================================================
//  Script S29: fiedetails.js
//      JS file used for assigning global vars (unused)
//======================================================================
$(document).ready(function() {

  $('input').focus(function() {   
      var total_income = parseInt($('#total_income').val());

      var living_expenses = parseInt($('#living_expenses').val());
      var education_medical = parseInt($('#education_medical').val());
      var insurance_pension = parseInt($('#insurance_pension').val());
      var rent_amortization = parseInt($('#rent_amortization').val());
      var transportation_utilities = parseInt($('#transportation_utilities').val());
      var credit_cards = parseInt($('#credit_cards').val());
      var other_loans = parseInt($('#other_loans').val());
      var miscellaneous = parseInt($('#miscellaneous').val());

      var totalExpenses = living_expenses + education_medical + insurance_pension + rent_amortization + transportation_utilities + credit_cards + other_loans + miscellaneous;
      $('#total_expenses').val(totalExpenses);
      $('#net_monthly_income').val(total_income - totalExpenses);
  });
  
});