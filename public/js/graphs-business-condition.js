//======================================================================
//  Script S34: graphs-business-condition.js
//      Graphs for Business Condition
//======================================================================
$(function() {

	$('.sumpopup').click(function(e){
		e.preventDefault();
		$('.sumpopupmodalcontent').text($(this).data('text'));
		$('#sumpopupmodal').modal();
	});
	
    $('#show1').click(function() {
        if($('#description1').is(':visible')){
			$('#description1').slideUp();
		} else {
			$('#description1').slideDown();
		}
	});

    $("#score_agroup_total").val(parseFloat($("#score_rm").val()) + parseFloat($("#score_cd").val()) + parseFloat($("#score_sd").val()) + parseFloat($("#score_bois").val()));
    
    //-----------------------------------------------------
    //  Binding S34.1: #container-pie1
    //      Displays the Business Condition Chart
    //-----------------------------------------------------
    $('#container-pie1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
			events: {
				load: function(){
					this.reflow();
				},
				redraw: function() {
					$('.redraw-bus-cond').remove();
				}
			}
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            pie: {
                //borderColor: '#000000',
                borderWidth: 0,
                innerSize: '60%',
                dataLabels: {
                    enabled: false
                }
            }
        },
        legend: {
            enabled: true,
            layout: 'vertical',
            align: 'right',
            width: 155,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function() {
                return '<div style="font-size:14px;font-weight: normal;font-style: normal; text-align: left; width:103px; float:left; padding: 5px 10px 5px 0px; margin-top:-5px;">' + this.name +  '</div>';
            },
            x: 0,
            y: 0
        },
        series: [{
            data: [
                {
                    name: trans('risk_rating.risk_management_chart')+'<br/>'+$("#score_rm_value").val()+"",
                    color: "#FBD5C4",
                    highlight: "#f4ccb9",
                    y: parseFloat($("#score_rm").val())
                },
                {
                    name: trans('risk_rating.customer_dependency_chart')+'<br/>'+$("#score_cd_value").val()+"",
                    color: "#E4D2E3",
                    highlight: "#dabcd7",
                    y: parseFloat($("#score_cd").val())
                },
                {
                    name: trans('risk_rating.supplier_dependency_chart')+'<br/>'+$("#score_sd_value").val()+"",
                    color: "#CFEBEC",
                    highlight: "#d2e8e9",
                    y: parseFloat($("#score_sd").val())
                },
                {
                    name: trans('risk_rating.business_outlook_chart')+'<br/>'+$("#score_bois_value").val()+"",
                    color: "#BDCF8E",
                    highlight: "#b5c883",
                    y: parseFloat($("#score_bois").val())
                },
                {
                    name: "",
                    color: "#fff",
                    highlight: "#fff",
                    y: parseFloat($("#score_agroup_sum").val()) - parseFloat($("#score_agroup_total").val())

                }
            ],
            size: '100%',
            innerSize: '54%',
            showInLegend: true,
            dataLabels: {
                enabled: false
            }
        }]
    });

});