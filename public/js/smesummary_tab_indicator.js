//======================================================================
//  Script S55: smesummary_tab_indicator.js
//      Scripts for Questionnaire sidebar completion icons
//======================================================================

var check_completeness = function(uiContainer, uiTarget){
	var complete = true;
	$(uiContainer).find('.progress-required').each(function(){
		if($.trim($(this).text()) == "") complete = false;
	});

	$(uiContainer).find('.completeness_check').each(function(){
		if($.trim($(this).text()) == "") complete = false;
	});

	if ((complete)
    || (0 < $('#status').val())) {
		$(uiTarget).append('<span class="icon-full"></span>');
	}
    else {
		$(uiTarget).append('<span class="icon-half"></span>');
	}
};

// slow document ready function $(document).ready(function(){
	check_completeness('#registration1', 'ul#side-menu li.pr1');
	check_completeness('#registration2', 'ul#side-menu li.pr2');
	check_completeness('#payfactor', 'ul#side-menu li.pr3');
	check_completeness('#bcs', 'ul#side-menu li.pr4');
	check_completeness('#pfs', 'ul#side-menu li.pr5');
	check_completeness('#ownerdetails', 'ul#side-menu li.pr6');
// slow document ready function });


