//======================================================================
//  Script S19: creditfacilities.js
//      Used to fetch investigator items using AJAX
//======================================================================
var creditlineLoad = function(showto, id, type) {
var typeadd = type;
var cfreasonvar = $('#cfreasons_'+typeadd+'').val();
  if(cfreasonvar == typeadd){
    $.get(BaseURL + "/api/cfreasons", { id: $('#'+id+'').val(), type: type }, 
      function(data) {
        var showResult = $('.'+showto+'');
          showResult.empty();
            showResult.append('<span class="details-row"><div class="col-md-12"><b>Reason(s)</b></span>'); 
            $.each(data, function(key, value) {
              showResult.append('<span class="details-row"><div class="col-md-12">'+value.credit_facilities_reasons+'</div></span>'); 
            });
    }); 
  }else{
    $.get(BaseURL + "/api/creditlines", { id: $('#'+id+'').val(), type: type }, 
      function(data) {
        var showResult = $('.'+showto+'');
          showResult.empty();
            if(type == 1){
              var typename = 'Bank Credit Facilities';
              var typelink = 'creditlines';
            }else if(type == 2){
              var typename = 'Non-Bank Credit lines';
              var typelink = 'creditlines';
            }else if(type == 3){
              var typename = 'Credit Lines with Suppliers';
              var typelink = 'creditlinessupplier';
            }
            showResult.append('<span class="details-row"><div class="col-md-12 bgcolor"><b>'+typename+'</b> <a href="#" class="hint hint--top" data-hint="You are currently using the ff credit lines.">?</a></div></span>'); 
            if(type != 3){
              showResult.append('<span class="details-row"><div class="col-md-2"><b>Name</b></div><div class="col-md-2"><b>Products</b></div><div class="col-md-2"><b>Collateral</b></div><div class="col-md-2"><b>Credit Line (PHP)</b></div><div class="col-md-2"><b>Outstanding Amount (PHP)</b></div><div class="col-md-2"><b>Outstanding as of Date</b></div></span>');   
            }
            $.each(data, function(key, value) {
              
              if($('#status').val() == 0 || $('#status').val() == 3){
                if($('#userid').val() != 2){
                  var showEdit = '<a href = "' + BaseURL + '/sme/'+typelink+'/'+value.creditlineid+'/edit">Edit</a> ';
                }else{
                  var showEdit = '';
                }
              }else{
                var showEdit = '';
              }
              if(type == 3){
                showResult.append('<span class="details-row"><div class="col-md-4"><b>Name</b></div><div class="col-md-8">'+value.bank_name+'</div><div class="col-md-4"><b>Product</b></div><div class="col-md-8">'+value.bank_product+'</div><div class="col-md-4"><b>Collateral</b></div><div class="col-md-8">'+value.bank_collateral+'</div><div class="col-md-4"><b>Credit Line (PHP)</b></div><div class="col-md-8">'+value.bank_initial_availment+'</div><div class="col-md-4"><b>Date Opened</b></div><div class="col-md-8">'+value.bank_outstanding_date+'</div><div class="col-md-4"><b>Outstanding Amount (PHP)</b></div><div class="col-md-8">PHP '+value.bank_outstanding_amount+'</div><div class="col-md-4"><b>Credit Terms</b></div><div class="col-md-8">'+value.bank_credit_term+'</div><div class="col-md-4"><b>Monthly Trade Volume (PHP)</b></div><div class="col-md-8">PHP '+value.bank_volume_trade+'</div><div class="col-md-12">'+showEdit+'</div></span>');             
              }else{
                showResult.append('<span class="details-row"><div class="col-md-2">'+showEdit+''+value.bank_name+'</div><div class="col-md-2">'+value.bank_product+'</div><div class="col-md-2">'+value.bank_collateral+'</div><div class="col-md-2">PHP '+value.bank_initial_availment+'</div><div class="col-md-2">PHP '+value.bank_outstanding_amount+'</div><div class="col-md-2">'+value.bank_outstanding_date+'</div></span>'); 
              }
            });
    }); 
  }
};
var bankproductsLoad = function(showto, id, type) {
  var cfreasonvar = $('#cfreasons_4').val();
  if($('#cfreasons_4').length == 1 && cfreasonvar == 4){
    $.get(BaseURL + "/api/cfreasons", { id: $('#'+id+'').val(), type: type }, 
      function(data) {
        var showResult = $('.'+showto+'');
          showResult.empty();
            showResult.append('<span class="details-row"><div class="col-md-12"><b>Reason(s)</b></span>'); 
            $.each(data, function(key, value) {
              showResult.append('<span class="details-row"><div class="col-md-12">'+value.credit_facilities_reasons+'</div></span>'); 
            });
    }); 
  }else{
    $.get(BaseURL + "/api/bankproducts", { id: $('#'+id+'').val(), type: type }, 
      function(data) {
        var showResult = $('.'+showto+'');
          showResult.empty();
            showResult.append('<span class="details-row"><div class="col-md-2"><b>Name</b></div><div class="col-md-2"><b>Products</b></div><div class="col-md-2"><b>Account Number</b></div><div class="col-md-3"><b>Date Opened</b></div><div class="col-md-3"><b>Average Daily Balance Amount (PHP)</b></div></span>'); 
            $.each(data, function(key, value) {
              
              if($('#status').val() == 0 || $('#status').val() == 3){
                if($('#userid').val() != 2){
                  var showEdit = '<a href = "' + BaseURL + '/sme/bankproducts/'+value.bankproductid+'/edit">Edit</a> ';
                }else{
                  var showEdit = '';
                }
              }else{
                var showEdit = '';
              }

              showResult.append('<span class="details-row"><div class="col-md-2">'+showEdit+''+value.bank_name+'</div><div class="col-md-2">'+value.bank_product+'</div><div class="col-md-2">'+value.bank_account_number+'</div><div class="col-md-3">'+value.bank_initial_availment+'</div><div class="col-md-3">'+value.bank_outstanding_amount+'</div></span>'); 
            });
    });
  }
};
var creditinvestigatorLoad = function(showto, id) {

  $.get(BaseURL + "/api/creditlines_investigation", { id: $('#'+id+'').val()}, 
    function(data) {
      var showResult = $('.'+showto+'');
      if(data.length == 0){
          if($('#status').val() == 2 || $('#status').val() == 4){
            showResult.html('<span style="text-align: right; border: 0px;" class="details-row"><a class="navbar-link" href="../../investigator/ci-creditassessment/'+$('#'+id+'').val()+'">Add</a></span>');
          }
      }else{
        showResult.empty();
          $.each(data, function(key, value) {
            
            if(value.banking_credit_experience == 1){
              var rvalue = 'Has clean legal record with banks and other Fis and up to date with his payments';
            }else if(value.banking_credit_experience == 2){
              var rvalue = 'Has adverse records but have been resolved and creditor was issued clearance';
            }else if(rvalue.banking_credit_experience == 3){
              var rvalue = 'Has adverse records not yet resolved or cleared by creditors';
            }

            if(value.credit_facility_ready == 1){
              var cfrvalue = 'Has Clean Bank/FI credit facility';
            }else if(value.credit_facility_ready == 2){
              var cfrvalue = 'Has Collateralized Bank/FI credit facility';
            }else if(rvalue.credit_facility_ready == 3){
              var cfrvalue = 'Has no etablished credit facility';
            }

            showResult.append('<input type="hidden" value="'+value.banking_credit_experience+'" id="ciscoring1" name="ciscoring1" class="ciscoring"><input type="hidden" value="'+value.credit_facility_ready+'" id="ciscoring2" name="ciscoring2" class="ciscoring"><span class="details-row"><div class="col-md-4"><b>Banking and Credit Experience</b></div><div class="col-md-8">'+rvalue+'</div><div class="col-md-4"><b>Existing Credit Facilities</b></div><div class="col-md-8">'+cfrvalue+'</div></span>'); 
            if($('#status').val() == 2 || $('#status').val() == 4){
              showResult.append('<span style="text-align: right; border: 0px;" class="details-row"><a class="navbar-link" href="../../investigator/ci-creditassessment/'+value.ccinvestigationid+'/edit">Edit</a></span>');
            }
          });
      }
  }); 

};
var managementturnoverLoad = function(showto, id) {

  $.get(BaseURL + "/api/managementturnover", { id: $('#'+id+'').val()}, 
    function(data) {
      var showResult = $('.'+showto+'');
      if(data.length == 0){
          showResult.html('<span style="text-align: right; border: 0px;" class="details-row"><a class="navbar-link" href="../../investigator/managementturnover/'+$('#'+id+'').val()+'">Add</a></span>');
      }else{
        showResult.empty();
          $.each(data, function(key, value) {
            if(value.executive_team == 1){
              var vexecutive_team = 'High';
            }else if(value.executive_team == 2){
              var vexecutive_team = 'Medium';
            }else if(value.executive_team == 3){
              var vexecutive_team = 'Low';
            }

            if(value.middle_management == 1){
              var vmiddle_management = 'High';
            }else if(value.middle_management == 2){
              var vmiddle_management = 'Medium';
            }else if(value.middle_management == 3){
              var vmiddle_management = 'Low';
            }

            if(value.staff == 1){
              var vstaff = 'High';
            }else if(value.staff == 2){
              var vstaff = 'Medium';
            }else if(value.staff == 3){
              var vstaff = 'Low';
            }

            showResult.append('<span class="details-row"><div class="col-md-4"><b>Executive Team</b></div><div class="col-md-8">'+vexecutive_team+'</div><div class="col-md-4"><b>Middle Management</b></div><div class="col-md-8">'+vmiddle_management+'</div><div class="col-md-4"><b>Staff</b></div><div class="col-md-8">'+vstaff+'</div></span>'); 
            if($('#status').val() == 2 || $('#status').val() == 4){
              showResult.append('<span style="text-align: right; border: 0px;" class="details-row"><a class="navbar-link" href="../../investigator/managementturnover/'+value.mturnoverid+'/edit">Edit</a></span>');
            }
          });
      }
  }); 

};
var previousloanLoad = function(showto, id) {

  $.get(BaseURL + "/api/previousloan", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
          showResult.append('<span class="details-row"><div class="col-md-3"><b>Lender</b></div><div class="col-md-3"><b>Products</b></div><div class="col-md-3"><b>Date Applied</b></div><div class="col-md-3"><b>Status</b></div></span>'); 
          $.each(data, function(key, value) {
            
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() != 2){
                var showEdit = '<a href = "' + BaseURL + '/sme/previousloan/'+value.ploanapplicationid+'/edit">Edit</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row"><div class="col-md-3">'+showEdit+''+value.ploan_name+'</div><div class="col-md-3">'+value.ploan_products+'</div><div class="col-md-3">'+value.ploan_date+'</div><div class="col-md-3">'+value.ploan_status+'</div></span>'); 
          });
  }); 

};
var adverserecordLoad = function(showto, id) {

  $.get(BaseURL + "/api/adverserecord", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
         
          showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3"><b>Name</b></div><div class="col-md-3"><b>Product</b></div><div class="col-md-3"><b>Past Due</b></div><div class="col-md-3"><b>Due Amount</b></div></span>'); 

          $.each(data, function(key, value) {
            
            var showEdit = '<a href = "' + BaseURL + '/sme/adverserecord/'+value.adverserecordid+'/edit">Edit</a> | <a href = "' + BaseURL + '/sme/adverserecord/'+value.adverserecordid+'/delete">Delete</a> ';
            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3">'+showEdit+''+value.adverserecord_name+'</div><div class="col-md-3">'+value.adverserecord_products+'</div><div class="col-md-3">'+value.adverserecord_pastdue+'</div><div class="col-md-3">PHP '+value.adverserecord_dueamount+'</div></span>'); 
			//mobile
			showResult.append('<span class="details-row visible-xs-block visible-sm-block"><div class="col-md-3"><b>Name</b><br/>'+value.adverserecord_name+'</div><div class="col-md-3"><b>Product</b><br/>'+value.adverserecord_products+'</div><div class="col-md-3"><b>Past Due</b><br/>'+value.adverserecord_pastdue+'</div><div class="col-md-3"><b>Due Amount</b><br />PHP '+value.adverserecord_dueamount+'</div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
  }); 

};