//======================================================================
//  Script S41: graphs-maganement-quality.js
//      Chart for Management Quality
//======================================================================
$(function() {
    
    //-----------------------------------------------------
    //  Binding S41.1: #show2
    //      Displays the description of the MQ Chart
    //-----------------------------------------------------
    $('#show2').click(function() {
        if($('#description2').is(':visible')){
			$('#description2').slideUp();
		} else {
			$('#description2').slideDown();
		}
	});
	
    $("#score_bgroup_total").val(parseFloat($("#score_boe").val()) + parseFloat($("#score_mte").val()) + parseFloat($("#score_bdms").val()) + parseFloat($("#score_sp").val()) + parseFloat($("#score_ppfi").val()));
    
    //-----------------------------------------------------
    //  Binding S41.2: #container-pie2
    //      Displays the Management Quality Chart
    //-----------------------------------------------------
    $('#container-pie2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
			events: {
				redraw: function() {
					$('.redraw-mng-quality').remove();
				}
			}
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            pie: {
               // borderColor: '#000000',
                borderWidth: 0,
                innerSize: '60%',
                dataLabels: {
                    enabled: false
                }
            }
        },
        legend: {
            enabled: true,
            layout: 'vertical',
            align: 'right',
            width: 155,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function() {
                return '<div style="font-size:14px; font-weight: normal; font-style: normal; text-align: left; float:left; padding: 5px 10px 5px 0px; margin-top:-5px;">' + this.name +  '</div>';
            },
            x: 0,
            y: 0
        },
        series: [{
            data: [
                {
                    name: trans('risk_rating.mq_business_owner')+'<br/>'+$("#score_owner_exp").val()+"",
                    color: "#FBD5C4",
                    highlight: "#f4ccb9",
                    y: parseFloat($("#score_boe").val())
                },
                {
                    name: trans('risk_rating.mq_management_team')+'<br/>'+$("#score_mng_exp").val()+"",
                    color: "#E4D2E3",
                    highlight: "#dabcd7",
                    y: parseFloat($("#score_mte").val())
                },
                {
                    name: trans('risk_rating.mq_business_driver')+$("#score_bdms_value").val()+"",
                    color: "#CFEBEC",
                    highlight: "#d2e8e9",
                    y: parseFloat($("#score_bdms").val())
                },
                {
                    name: trans('risk_rating.mq_succession_plan')+'<br/>'+$("#score_sp_value").val()+"",
                    color: "#BDCF8E",
                    highlight: "#b5c883",
                    y: parseFloat($("#score_sp").val())
                },
                {
                    name: trans('risk_rating.mq_past_project')+'<br/>'+$("#score_ppfi_value").val()+"",
                    color: "#B4BDC3",
                    highlight: "#94a3ac",
                    y: parseFloat($("#score_ppfi").val())
                },
                /*--
                {
                    name: "Credit Facility Established "+$("#score_bois_value").val()+"",
                    color: "#f7c588",
                    highlight: "#f9a43e",
                    y: parseFloat($("#score_bois").val())
                },
                */
                {
                    name: "",
                    color: "#fff",
                    highlight: "#fff",
                    y: parseFloat($("#score_bgroup_sum").val()) - parseFloat($("#score_bgroup_total").val())

                }
            ],
            size: '100%',
            innerSize: '54%',
            showInLegend: true,
            dataLabels: {
                enabled: false
            }
        }]
    },
    // using 

    function(chart1) { // on complete
        var xpos = '50%';
        var ypos = '50%';
        var circleradius = 40;

        // Render the circle
        chart1.renderer.circle(xpos, ypos, circleradius).attr({
            fill: '#fff',
        }).add();

    });

});