//======================================================================
//  Script S37: graphs-industry-comparison2.js
//      Chart for Industry comparison of Net Income growth
//======================================================================
$(function () {
    
    //-----------------------------------------------------
    //  Binding S37.1: #container-bar2
    //      Displays the Industry comparison Chart of Net Income growth
    //-----------------------------------------------------
    $('#container-bar2').highcharts({
        chart: {
            type: 'bar',
			events: {
				redraw: function() {
					$('.redraw-ind-cmp2').remove();
				}
			}
        },
        exporting: {
            buttons: {
                contextButtons: {
                    enabled: false,
                    menuItems: null
                }
            },
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [trans('risk_rating.ic_chart_2')],
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'14px'
                }
            }
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'12px'
                }
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: '{y:.2f}',
                    align: 'left',
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Industry',
            color: "#aaaaaa",
            data: [parseFloat($("#net_income_growth").val())]
        },{
            name: 'Company',
            color: "#BDCF8E",
            data: [parseFloat($("#net_income_growth22").val())]
        }]
    });
});