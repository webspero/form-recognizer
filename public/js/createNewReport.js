$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

$(document).ready(function(){
    $(".standAloneToggle").click(function(){
        $(".standAlone").show();
        $(".premium").hide();
    });
    $(".simplifiedToggle").click(function(){
        $(".standAlone").show();
        $(".premium").hide();
    });
    $(".premiumToggle").click(function(){
        $(".premium").show();
        $(".standAlone").hide();
    });
  });