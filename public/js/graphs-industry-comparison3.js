//======================================================================
//  Script S38: graphs-industry-comparison3.js
//      Chart for Industry comparison of Gross Profit Margin
//======================================================================
$(function () {
	
    //-----------------------------------------------------
    //  Binding S38.1: #container-bar3
    //      Displays the Industry comparison Chart of Gross Profit Margin
    //-----------------------------------------------------
    $('#container-bar3').highcharts({
        chart: {
            type: 'bar',
			events: {
				redraw: function() {
					$('.redraw-ind-cmp3').remove();
				}
			}
        },
        exporting: {
            buttons: {
                contextButtons: {
                    enabled: false,
                    menuItems: null
                }
            },
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [trans('risk_rating.ic_chart_3')],
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'14px'
                }
            }
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize:'12px'
                }
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: '{y:.2f}',
                    align: 'left',
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Industry',
            color: "#aaaaaa",
            data: [parseFloat($("#gross_profit_margin").val())]
        },{
            name: 'Company',
            color: "#BDCF8E",
            data: [parseFloat($("#gross_profit_margin22").val())]
        }]
    });
});