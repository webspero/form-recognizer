//======================================================================
//  Script S7: bank_statistics_chart4.js
//      Used for creating Industry Comparison (SME vs Industry) graph
//======================================================================
function Chart4(container, ChartData){

	var arSeries = [];
	for(x in ChartData['data']){
		arSeries.push({
			name: ChartData['name'][x],
            color: 'rgba(223, 83, 83, .5)',
            data: [ChartData['data'][x]]
		});
	}
	arSeries.push({
            type: 'line',
            name: 'Industry',
            data: [ChartData['high'], ChartData['low']],
            marker: {
                enabled: false
            },
            states: {
                hover: {
                    lineWidth: 0
                }
            },
            enableMouseTracking: false
        });

	container.highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Industry Comparison (Report vs Industry)'
        },
        subtitle: {
            text: ChartData['title']
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Industry ' +  ChartData['title']
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Report ' +  ChartData['title']
            }
        },
		legend: {
			enabled: false
		},
		credits: {
            enabled: false
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    },
					symbol: "circle"
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    pointFormat: 'Industry: {point.x}, Report: {point.y}'
                }
            }
        },
        series: arSeries
    });
};
