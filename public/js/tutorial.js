//======================================================================
//  Script S58: tutorial.js
//      Scripts for Questionnaire tutorial
//======================================================================
var Tutorial = {

	init: function(sPage){

		//create overlay
		if($('body').find('div.tut-overlay').length == 0){
			$('<div>').addClass('tut-overlay').appendTo($('body'));
		}

		//call function
		if(typeof(Tutorial[sPage]) == 'function'){
			Tutorial[sPage].call();
		}
	},

	step0: function(){
		var sContent = '<p style="font-size: 18px;">Do you want to take a quick tour?</p><br />'+
						'<a href="#" class="tut-continue-btn">Yes, Please!</a>'+
						'<a href="#" class="tut-skip-btn">No, Thanks.</a>';
		$('div.tut-overlay').show();
		Tutorial.renderModal('body', sContent, false, { 'width': '320px', 'margin-left': '-160px' });
		$('.tut-skip-btn').on('click', function(e){ e.preventDefault(); Tutorial.updateStatus(2); $('div.tut-overlay').hide(); $('div.tut-modal').remove(); });
		$('.tut-continue-btn').on('click', function(e){ e.preventDefault(); Tutorial.step1(); });
		$('div.tut-modal').fadeIn('slow');
	},

	step1: function(){
		var sContent = "<p style='font-size: 22px; text-align: center;'>Let's Get Started!</p><br />"+
						'<a href="#" class="tut-next-btn">Next &gt;&gt;</a>';
		Tutorial.renderModal('body', sContent, false, { 'width': '320px', 'margin-left': '-160px' });
		$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.updateStatus(2); Tutorial.step2(); });
		$('div.tut-modal').fadeIn('slow');
	},

	step2: function(){
		var sContent = "<p>Click here</p>";
		if($('.sme-icon img.img-responsive').is(':visible')){
			var offset = $('.sme-icon img.img-responsive').offset();
		} else {
			var offset = $('.panel-link.visible-xs-block .panel-body > div:first-child div.col-xs-7').offset();
		}
		$('div.tut-overlay').hide();
		Tutorial.renderModal('body', sContent, 'arrow_box arrow_box_top', { 'opacity': 0, 'width': '110px', 'top': (offset.top + 200) + 'px', 'left': (offset.left + 50) + 'px' });
		$('div.tut-modal').show().animate({
			top: (offset.top + 150) + 'px',
			opacity: 1
		}, 500, 'swing');
	},

	step3: function(){
		var sContent = "<p>Let's start with some business data</p>"+
						'<a href="#" class="tut-next-btn">Next &gt;&gt;</a>';
		$("html, body").animate({ scrollTop: 0 }, 500, function(){
			Tutorial.renderModal('body', sContent, false, { 'width': '320px', 'margin-left': '-160px' });
			$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.step4(); });
			$('div.tut-modal').fadeIn('slow');
		});
	},

	step4: function(){
		var sContent = "<p>We need to fill in the blanks. Click or tap on the space, input your detail and click on the <i class='glyphicon glyphicon-ok'></i> button.</p>";
		var offset = $('#registration1 > div > div .editable-field').first().offset();
		$("html, body").animate({ scrollTop: offset.top - 255 }, 500, function(){
			Tutorial.renderModal('body', sContent, 'arrow_box arrow_box_bottom', { 'opacity': 0, 'width': '260px', 'top': (offset.top - 190) + 'px', 'left': (offset.left - 85) + 'px' });
			$('div.tut-modal').show().animate({
				top: (offset.top - 140) + 'px',
				opacity: 1
			}, 500, 'swing');

			$('#registration1 #biz-details-cntr').on('click.tutorial', 'button#submit-editable', function(e){
				$('div.tut-modal').fadeOut('fast');
				Tutorial.step5();
			});

		});
	},

	step5: function(){
		var sContent = "<p>Your data is saved. Now, continue with the rest of the items.</p>"+
						'<a href="#" class="tut-next-btn">Next &gt;&gt;</a>';
		Tutorial.renderModal('body', sContent, false, { 'width': '320px', 'margin-left': '-160px' });
		if($('#affiliates-list-cntr > div > h4 > a').length > 0){
			$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.step6(); });
		} else {
			$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.step7(); });
		}
		$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.step6(); });
		$('div.tut-modal').fadeIn('slow');
	},

	step6: function(){
		$('#registration1 #biz-details-cntr').off('click.tutorial');

		var sContent = "<p>Some panels have this option for more relevant information. Click these to get more information.</p>"+
						'<a href="#" class="tut-next-btn">Next &gt;&gt;</a>';
		var offset = $('#affiliates-list-cntr > div > h4 > a').offset();
		$("html, body").animate({ scrollTop: offset.top - 250 }, 500, function(){
			Tutorial.renderModal('body', sContent, 'arrow_box arrow_box_right', { 'opacity': 0, 'width': '200px', 'top': (offset.top - 63) + 'px', 'left': (offset.left - 270) + 'px' });
			$('div.tut-modal').show().animate({
				left: (offset.left - 220) + 'px',
				opacity: 1
			}, 500, 'swing');
			$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.step7(); });
		});
	},

	step7: function(){
		var sContent = "<p>Every time you save, your data is stored in the database. So you can quit at any point, log out, and come back when it's most convenient for you.</p>"+
						'<a href="#" class="tut-next-btn">Close</a>';
		$("html, body").animate({ scrollTop: 0 }, 500, function(){
			Tutorial.renderModal('body', sContent, false, { 'width': '320px', 'margin-left': '-160px', 'margin-top': '-100px' });
			$('.tut-next-btn').on('click', function(e){ e.preventDefault(); Tutorial.updateStatus(2); $('div.tut-overlay').hide(); $('div.tut-modal').remove(); $('#sec_gen_modal').modal(); });
			$('div.tut-modal').fadeIn('slow');
		});
	},

	renderModal: function(sAppendTo, sContent, sArrow, oStyle){
		$('div.tut-modal').remove();
		var sClass = 'tut-modal';
		if(sArrow){ sClass += ' '+sArrow; }
		$('<div>').html(sContent).addClass(sClass).css(oStyle).appendTo($(sAppendTo));
	},

	updateStatus: function(iStatus){
		$.ajax({
			url: BaseURL + '/tutorial/update',
			type: 'post',
			data: {
				status: iStatus
			},
			success: function(){}
		});
	}
};