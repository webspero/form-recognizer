//======================================================================
//  Script S32: formula.js
//      Js helper for supervisor Formula customization
//======================================================================
$(document).ready(function(){
	$('.add_new_section_btn').click(function(e){
		e.preventDefault();
		$('#formula_modal .formula_select_selected').text('Select item to add');
		$('#formula_modal .formula_select_selected').attr('item', '');
		$('#formula_modal .error-message').text('');
		$('#formula_modal').modal();
	});
	
	$('#formula_panel .glyphicon-remove').click(function(e){
		e.preventDefault();
		$('.btn-formula-update').removeAttr('disabled');
		$(this).parent().hide();
	});
	
	$('#formula_modal ul.dropdown-menu a').click(function(e){
		e.preventDefault();
		$('#formula_modal .formula_select_selected').text($(this).text());
		$('#formula_modal .formula_select_selected').attr('item', $(this).attr('item'));
	});
	
	$('#formula_modal .formula-add-btn').click(function(e){
		e.preventDefault();
		var uiSelected = $('#formula_modal .formula_select_selected');
		if(uiSelected.attr('item') != ""){
			var arSelect = uiSelected.attr('item').split('_');
			var uiTarget = $('#formula_panel ul.' + arSelect[0] + ' li.' + arSelect[1]);
			if(uiTarget.is(':hidden')){
				$('.btn-formula-update').removeAttr('disabled');
				uiTarget.show();
				$('#formula_modal').modal('hide');
			} else {
				$('#formula_modal .error-message').text('This item is already present.');
			}
		}
	});
	
	$('#boost').click(function(){
		$('.btn-formula-update').removeAttr('disabled');
	});
	
	$('.btn-formula-save').click(function(e){
		e.preventDefault();
		var bcc = '';
		var mq = '';
		var fa = '';
		
		if($.trim($('#formula_name').val())==""){
			
			$('#formula_name').parent().addClass('has-error');
			
		} else {
			
			$('#formula_panel ul.bcc li').each(function(){
				if($(this).is(':visible')){
					bcc += $(this).attr('class') + ',';
				}
			});
			if(bcc.length > 0){ bcc = bcc.substring(0, bcc.length - 1); }
			$('#formula_panel ul.mq li').each(function(){
				if($(this).is(':visible')){
					mq += $(this).attr('class') + ',';
				}
			});
			if(mq.length > 0){ mq = mq.substring(0, mq.length - 1); }
			$('#formula_panel ul.fa li').each(function(){
				if($(this).is(':visible')){
					fa += $(this).attr('class') + ',';
				}
			});
			if(fa.length > 0){ fa = fa.substring(0, fa.length - 1); }
			
			$.ajax({
				url: BaseURL + '/bank/formula',
				type: 'post',
				data: {
					name: $('#formula_name').val(),
					bcc: bcc,
					mq: mq,
					fa: fa,
					boost: ($('#boost').is(':checked')) ? 1 : 0
				},
				beforeSend: function(){
					$('.formula_save_status').text('Updating...');
					$('.btn-formula-save').prop('disabled', true);
				},
				success: function(sReturn){
					if(sReturn == "success"){
						$('.formula_save_status').text('Updated');
						window.location.reload(true);
					} else {
						$('.formula_save_status').text('Error updating formula...');
					}
				}
			});
		}
		
	});
	
	$('.btn-formula-update').click(function(e){
		e.preventDefault();
		$('#formula_update_modal').modal();
	});
	
	$('.btn-set-history').click(function(e){
		e.preventDefault();
		$.ajax({
			url: BaseURL + '/bank/formula_set',
			type: 'post',
			data: {
				id: $('#history_select').val()
			},
			beforeSend: function(){
				$('.formula_save_status2').text('Using previous setup...');
				$('.btn-set-history').prop('disabled', true);
			},
			success: function(sReturn){
				if(sReturn == "success"){
					$('.formula_save_status2').text('Updated');
					window.location.reload(true);
				} else {
					$('.formula_save_status2').text('Error updating formula...');
				}
			}
		});
	});
});