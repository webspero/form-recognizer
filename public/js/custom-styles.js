//======================================================================
//  Script S21: custom-styles.js
//      JS for additional bootstrap scripts
//======================================================================
/* ========= CAROUSEL ARROW SCRIPT =============== */
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
});


/* ========= PROGRESS BAR SCRIPT =============== */
$(document).ready(function() {
if(!Modernizr.meter){
alert('Sorry your brower does not support HTML5 progress bar');
} else {
var progressbar = $('#progressbar'),
max = progressbar.attr('max'),
time = (1000/max)*3,	
value = progressbar.val();

var loading = function() {
value += 1;
addValue = progressbar.val(value);

$('.progress-value').html(value + '% Complete');

if (value == max) {
clearInterval(animate);			           
}
};

var animate = setInterval(function() {
loading();
}, time);
};
});