//======================================================================
//  Script S12: busdetails.js
//      JS file for Business details manipulation
//======================================================================
$(document).ready(function() {

    var BIZ_TYPE_CORP   = 0;
    var BIZ_TYPE_SOLE   = 1;

	jQuery("#formbpo").validationEngine('attach');

	$('#industrymain').change(function(){
        /**--------------------------------------------------------------
        /*	Variable Definition
        /*-------------------------------------------------------------*/
        var ajax_url = BaseURL + "/api/industrysub"

        /** Industry Fields  */
        var industrysub = $('#industrysub');
        var industryrow = $('#industryrow');

        /** Loading Images  */
        var sub_loader  = $('.sub-ind-load');
        var ind_loader   = $('.ind-load');

        $.ajax({
            method: 'get',
            url: ajax_url,
            data: { 'industrymain': $(this).val() },
            beforeSend: function(xhr) {
                /**---------------------------------------------------------
                /*	Disables the Industry Fields that will be redrawn
                /*--------------------------------------------------------*/
                industrysub.prop('disabled', true);
                industryrow.prop('disabled', true);

                /**---------------------------------------------------------
                /*	Shows the Loading Images
                /*--------------------------------------------------------*/
                sub_loader.show();
                ind_loader.show();

                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industrysub.html('<option value = ""> Loading sub-industries... </option>');
                industryrow.html('<option value = ""> Loading industries... </option>');

            },
            success: function(response, xhr) {
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industrysub.html('<option value="">Choose your sub-industry</option>');
                industryrow.html('<option value="">Choose your sub-industry first</option>');

                $.each(response, function(key, value) {
                    industrysub.append($("<option></option>").val(key).text(value));
                });

                /**---------------------------------------------------------
                /*	Enables the Industry Fields that was redrawn
                /*--------------------------------------------------------*/
                industrysub.prop('disabled', false);
                industryrow.prop('disabled', false);

                /**---------------------------------------------------------
                /*	Hides the Loading Images
                /*--------------------------------------------------------*/
                sub_loader.hide();
                ind_loader.hide();
            }
        });
  	});

	$('#industrysub').change(function() {
        /**--------------------------------------------------------------
        /*	Variable Definition
        /*-------------------------------------------------------------*/
        var ajax_url = BaseURL + "/api/industryrow"

        /** Industry Fields  */
        var industrysub = $('#industrysub');
        var industryrow = $('#industryrow');

        /** Loading Images  */
        var sub_loader  = $('.sub-ind-load');
        var ind_loader   = $('.ind-load');

        $.ajax({
            method: 'get',
            url: ajax_url,
            data: { 'industrysub': $(this).val() },
            beforeSend: function(xhr) {
                /**---------------------------------------------------------
                /*	Disables the Industry Fields that will be redrawn
                /*--------------------------------------------------------*/
                industryrow.prop('disabled', true);

                /**---------------------------------------------------------
                /*	Shows the Loading Images
                /*--------------------------------------------------------*/
                ind_loader.show();

                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industryrow.html('<option value = ""> Loading industries... </option>');
            },
            success: function(response, xhr) {
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industryrow.html('<option value="">Choose your industry</option>');

                $.each(response, function(key, value) {
                    industryrow.append($("<option></option>").val(key).text(value));
                });

                /**---------------------------------------------------------
                /*	Enables the Industry Fields that was redrawn
                /*--------------------------------------------------------*/
                industryrow.prop('disabled', false);

                /**---------------------------------------------------------
                /*	Hides the Loading Images
                /*--------------------------------------------------------*/
                ind_loader.hide();
            }
        });
  	});

	$("#dateestablished").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});

	$("#updated_date").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-30:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});

    $('#sec_reg_date').datepicker({
        dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
    });

	$('#dateestablished').change(function() {
		  var today = new Date();
    	var dateestablished = new Date($('#dateestablished').val());
    	var age = today.getFullYear() - dateestablished.getFullYear();
    	var m = today.getMonth() - dateestablished.getMonth();

    	if (m < 0 || (m === 0 && today.getDate() < dateestablished.getDate())) {
        	age--;
    	}
		$('#numberyear').val(age);
	});

	$('#requested_by').change(function(){
		if($(this).val()==0){
			$('.bank-control').hide();
		}
		else {
			$('.bank-control').show();
		}
	});
	$("#current_bank").select2();
	$('#requested_by').change();

	$('#province').on('change', function(){
		var sProvince = $(this).val();
		var uiCity = $('#city');
		$.ajax({
			url: BaseURL + '/api/cities',
			type: 'get',
			dataType: 'json',
			data: {
				province: sProvince
			},
			beforeSend: function() {
				$('.city-preloader').show();
				uiCity.html('<option>&nbsp;&nbsp;&nbsp;Loading cities...</option>');
			},
			success: function(oData)	{
				$('.city-preloader').hide();
				uiCity.html('<option value="">Choose your city</option>');
				for(x in oData){
					uiCity.append('<option value="'+oData[x].id+'" zipcode="'+oData[x].zip_code+'">'+oData[x].city+'</option>');
				}
			}
		});
	});

	$('#city').on('change', function(){
		if($('#city').val()!=''){
			$('#zipcode').val($('#city option:selected').attr('zipcode'));
		}
	});

	$('#formbpo_submit').click(function(e){
		e.preventDefault();
		var sec_cert_file       = $("#sec_cert").val();
		var permit_file         = $("#permit_to_operate").val();
		var borrow_file         = $("#authority_to_borrow").val();
		var signatory_file      = $("#authorized_signatory").val();
		var tax_reg_file        = $("#tax_registration").val();
		var income_tax_file     = $("#income_tax").val();
		var submit_ok           = 1;

        $('.errormessage').text('');

        if (!$("#permit_to_operate").is(':hidden')) {
            if (permit_file == '') {
				$("#permit_to_operate").parent().find('.errormessage').remove();
				$("#permit_to_operate").parent().append('<span class="errormessage">Business Permit is required</span>');
				$("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
			}
            else {
				var ext = permit_file.split('.').pop().toLowerCase();

				if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
					$("#permit_to_operate").parent().find('.errormessage').remove();
					$("#permit_to_operate").parent().append('<span class="errormessage">Business Permit must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
					$("html, body").animate({ scrollTop: 0 }, 300);

                    submit_ok   = submit_ok && 0;
				}
			}
        }

		if (!$('#sec_cert').is(':hidden')) {
			if (sec_cert_file == '') {
				$("#sec_cert").parent().find('.errormessage').remove();
				$("#sec_cert").parent().append('<span class="errormessage">Registration Document is required</span>');
				$("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
			}
            else {
				var ext = sec_cert_file.split('.').pop().toLowerCase();

				if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
					$("#sec_cert").parent().find('.errormessage').remove();
					$("#sec_cert").parent().append('<span class="errormessage">Registration Document must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
					$("html, body").animate({ scrollTop: 0 }, 300);

                    submit_ok   = submit_ok && 0;
                }
			}
		}

        if ('' != borrow_file) {
            var ext = borrow_file.split('.').pop().toLowerCase();

            if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
                $("#authority_to_borrow").parent().find('.errormessage').remove();
                $("#authority_to_borrow").parent().append('<span class="errormessage">Board Resolution must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
                $("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
            }
        }

        if ('' != signatory_file) {
            var ext = signatory_file.split('.').pop().toLowerCase();

            if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
                $("#authorized_signatory").parent().find('.errormessage').remove();
                $("#authorized_signatory").parent().append('<span class="errormessage">Authorized Signatory must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
                $("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
            }
        }

        if ('' != tax_reg_file) {
            var ext = tax_reg_file.split('.').pop().toLowerCase();

            if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
                $("#tax_registration").parent().find('.errormessage').remove();
                $("#tax_registration").parent().append('<span class="errormessage">BIR Registration must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
                $("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
            }
        }

        if ('' != income_tax_file) {
            var ext = income_tax_file.split('.').pop().toLowerCase();

            if ($.inArray(ext, ['txt','pdf','doc','docx','zip','rar','jpeg', 'jpg', 'png','csv','tsv']) == -1) {
                $("#income_tax").parent().find('.errormessage').remove();
                $("#income_tax").parent().append('<span class="errormessage">Income Tax Return must be a ZIP, RAR, JPG, PNG, PDF, Word Document</span>');
                $("html, body").animate({ scrollTop: 0 }, 300);

                submit_ok   = submit_ok && 0;
            }
        }

        if (submit_ok) {
            if ($("#formbpo").validationEngine('validate')) {
                $(this).val('Saving Information...').prop('disabled', true);
                $('#formbpo').submit();
            }
        }
	});

	$('.sec_cert_edit').click(function(e){
		e.preventDefault();
		$('.sec_cert_label').hide();
		$("#sec_cert").show();
		$(this).parent().parent().find(".small_text").show();
	});

    $('.business_permit_edit').click(function(e){
		e.preventDefault();
		$('.permit_to_operate_label').hide();
		$("#permit_to_operate").show();
		$(this).parent().parent().find(".small_text").show();
	});

    $('.authority_to_borrow_edit').click(function(e){
		e.preventDefault();
		$('.authority_to_borrow_label').hide();
		$("#authority_to_borrow").show();
		$(this).parent().parent().find(".small_text").show();
	});

    $('.authorized_signatory_edit').click(function(e){
		e.preventDefault();
		$('.authorized_signatory_label').hide();
		$("#authorized_signatory").show();
		$(this).parent().parent().find(".small_text").show();
	});

    $('.tax_registration_edit').click(function(e){
		e.preventDefault();
		$('.tax_registration_label').hide();
		$("#tax_registration").show();
		$(this).parent().find(".small_text").show();
	});

    $('.income_tax_edit').click(function(e){
		e.preventDefault();
		$('.income_tax_label').hide();
		$("#income_tax").show();
		$(this).parent().find(".small_text").show();
	});

    $('#entity_type').change(function() {
        changeBizEntityLabels();
    });

    function changeBizEntityLabels() {
        var biz_type = parseInt($('#entity_type').val());

        switch (biz_type) {

            case BIZ_TYPE_SOLE:
                $('.sec_reg_date_lbl').text('DTI Registration Date');
                $('.tin_num_lbl').text('DTI Company Reg. No.');
                $('.only-corp-field').hide();
                break;

            case BIZ_TYPE_CORP:
            default:
                $('.sec_reg_date_lbl').text('SEC Registration Date');
                $('.tin_num_lbl').text('SEC Company Reg. No.');
                $('.only-corp-field').show();
                break;
        }
    }

    changeBizEntityLabels();

});
