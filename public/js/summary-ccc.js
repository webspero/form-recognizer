//======================================================================
//  Script S56: smesummary_ccc.js
//      Scripts for Questionnaire Cash Conversion Cycle computation
//======================================================================
$(document).ready(function(){
	
	$('.cycle-input').removeAttr('readonly');
	
	$('.cycle-input').on('change', function(){
		for (i = 0; i < 3; i++) {
			var dio = ($('.ccc-table .inventory_'+i).val() / $('.ccc-table .cost_of_sales_'+i).val()) * 30;
			var dso = ($('.ccc-table .accounts_receivable_'+i).val() / $('.ccc-table .credit_sales_'+i).val()) * 30;
			var dpo = $('.ccc-table .accounts_payable_'+i).val() / ($('.ccc-table .cost_of_sales_'+i).val() / 30);
			var ccc = dio + dso - dpo;
			if(ccc){
				$('.ccc-table .ccc_'+i).text(ccc.toFixed(2));
			} else {
				$('.ccc-table .ccc_'+i).text('');
			}
		}
	});
	
	$('.submit-ccc-form').on('click', function(e){
		e.preventDefault();
		$('#ccc-form').ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function(){
				$('.ccc-message').html('').hide();
				$('.submit-ccc-form').val('Saving...').prop('disabled', true);
			},
			success: function(oResponse){
				
				if (oResponse.sts == 1) {
					$('.submit-ccc-form').val('Saved').css({
						'background-color': 'GREEN',
						'border-color': 'none'
					});
					setTimeout(function(){ $('.submit-ccc-form').val('Save').css({
						'background-color': '#337ab7',
						'border-color': '#204d74'
					}).prop('disabled', false); }, 2000);
				}
                else {
					$('.submit-ccc-form').val('Save').prop('disabled', false);
					for(x in oResponse.messages){
						$('.ccc-message').append('<p>'+oResponse.messages[x]+'</p>');
					}
					$('.ccc-message').show();
				}
			}
		});
	});
});