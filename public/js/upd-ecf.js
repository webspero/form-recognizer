//======================================================================
//  Script S62: upd-ecf.js
//      Adding and Editing of ECF data
//======================================================================

$(document).ready(function(){

/**------------------------------------------------------------------------
|	Binds the Add ECF link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popEcfForm()
{
    /** Variable Definition    */
    var ecf_cntr    = $('#payfactor');
    var hdr_txt     = trans('ecf.ecf');
    var form_id     = 'ecf-form';
    var get_url     = BaseURL+'/sme/planfacility/'+$('#entityids').val();
    var pop_ecf_btn = '.pop-ecf-form';
    
    //-----------------------------------------------------
    //  Binding S62.1: .pop-ecf-form
    //      Launch the Modal Form for Existing Credit Facilities
    //-----------------------------------------------------
    ecf_cntr.on('click', pop_ecf_btn, function(){
        initPopForm(hdr_txt, form_id, get_url);
        return false;
    });
}

popEcfForm();

});