//======================================================================
//  Script S30: forgot-password.js
//      Resets the user's password
//======================================================================

$(document).ready(function() {
    
/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initForgotPassword()
{
    /** Binds the events to the elements     */
    bindLinkToForgotPassword();
    bindForgotFormModal();
    bindLoginButton();
}

/**------------------------------------------------------------------------
|	Binds click event to the Login Button
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindLoginButton()
{
    /** Variable Definition         */
    var chng_pass_form  = $( "#forgot-password-dialog" );
    var login_form      = $('#login');
    
    var login_btn       = $('.login-btn');
    var ajax_url        = BaseURL+'/rest/check_password_request';
    var error_msg       = $('#password-error');
    var email           = $('input[name="email"]');
    
    //-----------------------------------------------------
    //  Binding S30.1: .login-btn
    //      Checks the status of a password request when a
    //      user logs in
    //-----------------------------------------------------
    login_btn.click(function() {
       /** Check the existence of request via AJAX    */
       $.get(ajax_url, { 'email': email.val() }, function(data) {
           /** Display Change Password Form when there is a request    */
           if (1 == data) {
               error_msg.html('As per your request, you are required to change your password.');
               chng_pass_form.dialog('open');
           }
           /** Submit the Login Form when there is no request         */
           else {
               login_form.submit();
           }
       });

       return false;       
    });
}

/**------------------------------------------------------------------------
|	Binds click event to Forgot Password Link
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindLinkToForgotPassword()
{
    /** Variable Definition         */
    form_link    = $('#forgot-pass-link');

    //-----------------------------------------------------
    //  Binding S30.2: #forgot-pass-link
    //      Loads the Forgot Password form when the link is clicked
    //-----------------------------------------------------
    form_link.click(function(){
        
        var confirm_modal = $("<div> This will reset your password. You must complete the process to login. </div>").dialog({
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            title: 'Reset Password',
            modal: true,
            open: function(event, ui) { 
                /** Remove the close button */
                $(".ui-dialog-titlebar-close").hide();
            },
            buttons: {
                'Continue': function() {
                    $(this).dialog('close');
                    
                    confirmForgotEmail();
                },
                'Cancel': function() {
                    $(this).dialog('destroy');
                }
            }
        });
        
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Change Password Form to the Modal Dialog Box
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindForgotFormModal()
{
    /** Variable Definition */
    change_pass_form    = $( "#forgot-password-dialog" );
    
    //-----------------------------------------------------
    //  Binding S30.3: #forgot-password-dialog
    //      Binds the Change password form to the JQuery Modal
    //-----------------------------------------------------
    change_pass_form.dialog({
      autoOpen: false,
      modal: true,
      buttons: {
        'Change Password': verifyChangePassword
      },
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "blind",
        duration: 1000
      }
    });
}

/**------------------------------------------------------------------------
|	Verifies if the Email is registered
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function confirmForgotEmail()
{
    /** Variable Definition */
    var srv_message  = '';
    var ajax_url     = BaseURL+'/rest/get_user_by_email';
    var user_email   = $('input[name="email"]');
    var error_msg    = $('#forgot-pass-error');
    var wait_modal   = '';

    /** Verify Email via AJAX */
    $.ajax({
        method: 'get',
        url: ajax_url,
        dataType: 'json',
        data: { 'email': user_email.val() },
        /** Display a Modal while sending Email */
        beforeSend: function(xhr) {
            wait_modal = $("<div>Verifying your Account... </div>").dialog({
                closeOnEscape: false,
                title: 'Email Verification',
                modal: true,
                open: function(event, ui) { 
                    $(".ui-dialog-titlebar-close").hide();
                },
            });
        },
        /** Verification Finished */
        success: function(response, xhr) {
            
            wait_modal.dialog('destroy').remove();
            
            /** Email is a registered user */
            if (1 == response.valid) {
                /** Show the Change Password Form */
                $( "#forgot-password-dialog" ).dialog('open');
                error_msg.html('');
            }
            /** There is an error */
            else {
                /**	Formats the warnings received from the Server	*/
                for (index = 0; index < response.messages.length; index++) {
                    srv_message += response.messages[index] +'<br/>';
                }
                
                /**	Display the warning								*/
                error_msg.html(srv_message);
            }
        }
    });
}

/**------------------------------------------------------------------------
|	Verifies the inputs and changes the Password
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function verifyChangePassword()
{
    /** Variable Definition */
    var forgot_modal    = $( "#forgot-password-dialog" );
    var success_modal   = '';
    var srv_message     = '';
    
    var ajax_url        = BaseURL+'/rest/change_password';
    
    var request_form    = $('#forgot-password-form');
    var error_msg       = $('#password-error');
    var user_email      = $('input[name="email"]');
    var notif_code      = $('input[name="notif-code"]');
    var new_password    = $('input[name="new-password"]');
    var conf_password   = $('input[name="confirm-password"]');

    /** Change Password via AJAX */
    $.post(
        ajax_url, 
        {
            'email': user_email.val(),
            'notif_code': notif_code.val(),
            'new_pass': new_password.val(),
            'conf_pass': conf_password.val()
        },
        function(data) {
            /** Change Password Successful */
            if (1 == data.valid) {
                forgot_modal.dialog('close');
                
                /** Create a Modal Confirmation      */
                success_modal = $("<div> You may now login using your new Password. </div>").dialog({
                    closeOnEscape: false,
                    title: 'Password Change Succcessful',
                    modal: true,
                    close: function(event, ui) {
                        $(this).dialog('destroy').remove();
                    }
                });
            }
            /** Change Password failed */
            else {
                /**	Formats the warnings received from the Server	*/
                for (index = 0; index < data.messages.length; index++) {
                    srv_message += data.messages[index] +'<br/>';
                }
                
                /**	Display the warning								*/
                error_msg.html(srv_message);
            }
        }, 'json'
    );
}

initForgotPassword();

});