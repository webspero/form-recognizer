//======================================================================
//  Script S48: pse-extraction.js
//      Extracts data from PSE Edge websites
//======================================================================

$(document).ready(function() {

var PSE_PSE_ID          = 0;
var PSE_REPORT_YEAR     = 1;
var PSE_PERIOD_YEAR     = 2;
var PSE_CURRENT_ASSET   = 3;
var PSE_TOTAL_ASSET     = 4;
var PSE_CURRENT_LIAB    = 5;
var PSE_TOTAL_LIAB      = 6;
var PSE_RETAINED_EARN   = 7;
var PSE_EQUITY          = 8;
var PSE_EQUITY_PARENT   = 9;
var PSE_BOOK_VALUE      = 10;

var PSE_GROSS_REVENUE   = 3;
var PSE_GROSS_EXPENSE   = 4;
var PSE_BEFORE_TAX      = 5;
var PSE_AFTER_TAX       = 6;
var PSE_ATTRIB_PARENT   = 7;
var PSE_BASIC_SHARE     = 8;
var PSE_DILUTED_SHARE   = 9;

var PSE_COMPANY_ID      = 0;
var PSE_COMPANY_NAME    = 1;
var PSE_COMPANY_CODE    = 2;


var notes_container     = $('#extracting-data-ongoing');
var initialize_notes    = $('#extract-data-initialize');

var extraction_notes    = $('.extraction-progress-notes');

var annual_report_bar   = $('.extraction-progress');

var close_btn           = $('#close-extract-btn');
var extract_btn         = $('#extract-report-btn');
var loader_img          = $('.extraction-loader');

var progress_completion = 0;
var progress_notes      = '';

var total_company       = $('#total_companies').val();

var initial_notes_cntr          = $('#initial-extraction-ongoing');
var init_extract_notes          = $('.initial-extraction-notes');
var initial_extract_bar         = $('.initial-extraction-progress');
var init_loader_img             = $('.initial-extraction-loader');
var init_progress_completion    = 0;
var init_progress_notes         = '';

/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initPseExtraction()
{
    /** Binds the events to the elements     */
    bindAnnualReportProgressBar();
    bindCloseButton();
    bindExtractReportButton();
    
    bindInitialExtractProgressBar();
    bindInitialExtractButton();
    
    bindViewReportModal();
    bindReportSelectChange();
}

/**------------------------------------------------------------------------
|	Binds the Annual Report Progress Bar
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindAnnualReportProgressBar()
{    
    //-----------------------------------------------------
    //  Binding S48.1: .extraction-progress
    //      Binds HTML element to JQuery Progres Bar
    //-----------------------------------------------------
    annual_report_bar.progressbar({
        value: false,
        change: function() {
            extraction_notes.html('Current Progress: '+Math.floor(progress_completion)+'% '+progress_notes);
        },
        complete: function() {
            close_btn.show();
            loader_img.hide();
            extraction_notes.html('<b>Data Extraction Successful!</b>');
        }
    });
}

/**------------------------------------------------------------------------
|	Binds Close button to the On Click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindCloseButton()
{
    //-----------------------------------------------------
    //  Binding S48.2: #close-extract-btn
    //      Closes the reminder dialog box after clicking button
    //-----------------------------------------------------
    close_btn.click(function() {
        notes_container.hide();
        
        loader_img.show();
        initialize_notes.show();
        extract_btn.show();
        
        extraction_notes.html('Initializing Extraction Interface...');
        annual_report_bar.progressbar("value", false);
    });
}

/**------------------------------------------------------------------------
|	Binds Extract report button to the On Click event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindExtractReportButton()
{
    //-----------------------------------------------------
    //  Binding S48.3: #extract-report-btn
    //      Extracts PSE information upon button click
    //-----------------------------------------------------
    $('#extract-report-btn').click(function() {
        
        initialize_notes.hide();
        close_btn.hide();
        extract_btn.hide();
        notes_container.show();
        
        setTimeout(function() {
            extraction_notes.html('Bypassing PSE Security...');
            setTimeout(extractAnnualReport, 3000);
        }, '2000');
        
    });
}

/**------------------------------------------------------------------------
|	Updates the Progress Bar
|--------------------------------------------------------------------------
|	@param [in]		bar_cntr    --- The Container Element of Progress Bar
|                   completion  --- Completion Percentage
|
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function updateProgressBar(bar_cntr, completion) {
    bar_cntr.progressbar( "value", Math.floor(completion));
}

/**------------------------------------------------------------------------
|	Updates the Progress Bar
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function extractAnnualReport()
{
    /** Variable Definition         */
    var completion          = 0;
    var completed_company   = 0;
    
    
    var pse_data_url        = BaseURL+'/pse/extract_doc/';
    var ajax_load_cntr      = ' #contents';
    
    var post_report_url     = BaseURL+'/pse/post_annual_report';
    
    progress_notes          = '<b> - Fetching all company information...</b>';
    
    updateProgressBar(annual_report_bar, completion);
    
    $('.pse-name-id').each(function() {
        
        var bal_sheet           = new Array();
        var income_statement    = new Array();
    
        var pse_id              = $(this).attr('data-value');
        var fs_period           = '';
        var company_cntr        = '<div id = "'+pse_id+'"></div>';
        
        var extraction_point    = $('#extraction_container').append(company_cntr);
        
        extraction_point.load(pse_data_url+pse_id+ajax_load_cntr, function() {
            
            bal_sheet[PSE_PSE_ID]               = pse_id;
            bal_sheet[PSE_REPORT_YEAR]          = extraction_point.find('.textCont').eq(1).text().substr(37, 4);
            bal_sheet[PSE_PERIOD_YEAR]          = extraction_point.find('.textCont').eq(1).text().substr(0, 41);
            bal_sheet[PSE_CURRENT_ASSET]        = extraction_point.find('th:contains("Current Assets")').first().next().text();
            bal_sheet[PSE_TOTAL_ASSET]          = extraction_point.find('th:contains("Total Assets")').first().next().text();
            bal_sheet[PSE_CURRENT_LIAB]         = extraction_point.find('th:contains("Current Liabilities")').first().next().text();
            bal_sheet[PSE_TOTAL_LIAB]           = extraction_point.find('th:contains("Total Liabilities")').first().next().text();
            bal_sheet[PSE_RETAINED_EARN]        = extraction_point.find('th:contains("Retained Earnings")').first().next().text();
            bal_sheet[PSE_EQUITY]               = extraction_point.find('th:contains("Equity")').first().next().text();
            bal_sheet[PSE_EQUITY_PARENT]        = extraction_point.find('th:contains("Equity - Parent")').first().next().text();
            bal_sheet[PSE_BOOK_VALUE]           = extraction_point.find('th:contains("Book Value")').first().next().text();

            income_statement[PSE_PSE_ID]        = pse_id;
            income_statement[PSE_REPORT_YEAR]   = extraction_point.find('.textCont').eq(1).text().substr(37, 4);
            income_statement[PSE_PERIOD_YEAR]   = extraction_point.find('.textCont').eq(1).text().substr(0, 41);
            income_statement[PSE_GROSS_REVENUE] = extraction_point.find('th:contains("Gross Revenue")').first().next().text();
            income_statement[PSE_GROSS_EXPENSE] = extraction_point.find('th:contains("Gross Expense")').first().next().text();
            income_statement[PSE_BEFORE_TAX]    = extraction_point.find('th:contains("Before Tax")').first().next().text();
            income_statement[PSE_AFTER_TAX]     = extraction_point.find('th:contains("After Tax")').first().next().text();
            income_statement[PSE_ATTRIB_PARENT] = extraction_point.find('th:contains("Attributable to Parent")').first().next().text();
            income_statement[PSE_BASIC_SHARE]   = extraction_point.find('th:contains(" Per Share (Basic)")').first().next().text();
            income_statement[PSE_DILUTED_SHARE] = extraction_point.find('th:contains(" Per Share (Diluted)")').first().next().text();
            
            $.post(post_report_url, { 'bal-sheet': bal_sheet, 'income-statement': income_statement }, function(company) {
                completed_company++;
                
                completion = 100 * (completed_company / total_company);
                
                progress_completion = completion;
                progress_notes      = '<b> - Extracting data of '+company.pse_code+'...</b>';
                
                updateProgressBar(annual_report_bar, completion);
            }, 'json');
        });
    });
}




/**------------------------------------------------------------------------
|	Updates the Progress Bar
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindInitialExtractProgressBar()
{
    //-----------------------------------------------------
    //  Binding S48.4: .initial-extraction-progress
    //      Binds the Extract Element to JQuery Progress Bar
    //-----------------------------------------------------
    initial_extract_bar.progressbar({
        value: false,
        change: function() {
            init_extract_notes.html('Current Progress: '+Math.floor(init_progress_completion)+'% '+init_progress_notes);
        },
        complete: function() {
            init_loader_img.hide();
            init_extract_notes.html('<b>Data Extraction Successful!</b>');
            location.reload();
        }
    });
}

/**------------------------------------------------------------------------
|	Binds the Initial Extraction Button to the Click Event
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindInitialExtractButton()
{
    //-----------------------------------------------------
    //  Binding S48.5: #initial-extraction-btn
    //      Extracts data from PSE when no company has been
    //      extracted yet
    //-----------------------------------------------------
    $('#initial-extraction-btn').click(function() {
        
        initial_notes_cntr.show();

        setTimeout(function() {
            init_extract_notes.html('Bypassing PSE Security...');

            if (0 >= $('#initialize_complete').val()) {
                setTimeout(extractInitialReports, 3000);
            }
            else {
                setTimeout(extractPSECompanies, 3000);
            }
            
        }, '2000');
        
    });
    
    if (0 >= $('#initialize_complete').val()) {
        $('#initial-extraction-btn').click();
    }
    else if ((0 < $('#initialize_complete').val())
    && (0 >= $('#company_list_complete').val())) {
        $('#initial-extraction-btn').click();
    }
    else {
        /*  No Processing   */
    }
    
}

/**------------------------------------------------------------------------
|	Extracts the PSE listed companies
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function extractPSECompanies()
{
    var pse_ids         = new Array();
    var pse_id_counter  = 0;
    var completion      = 0;
    
    var next_comp       = 0;
    var comp_count      = 0;
    
    var post_comp_url   = BaseURL+'/pse/add_companies';
    var comp_id         = '';
    var comp_name       = '';
    var pse_code        = '';
    var link_method     = '';
    var method_param    = '';
    var param_explode   = '';
    var raw_comp_id     = '';
    
    updateProgressBar(initial_extract_bar, completion);
    
    for (page = 1; 6 >= page; page++) {
        $('#company_list').load(BaseURL+'/pse/company_list/'+page, function() {
            var pse_companies   = new Array();
            next_comp           = 0;
            comp_count          = 0;
            
            pse_companies[0]    = new Array();
            
            $('a[href="#company"').each(function(){
                comp_name = $(this).text();

                if (0 >= next_comp) {
                    link_method = $(this).attr('onclick');

                    method_param    = link_method.substr(9);
                    param_explode   = method_param.split(',');
                    raw_comp_id     = param_explode[0];
                    comp_id         = raw_comp_id.replace("'",'');
                    comp_id         = comp_id.replace("'",'');

                    pse_companies[comp_count][PSE_COMPANY_ID]   = comp_id;
                    pse_companies[comp_count][PSE_COMPANY_NAME] = comp_name;
                    
                    next_comp   = 1;
                    
                    pse_ids[pse_id_counter] = comp_id;
                    pse_id_counter++;
                }
                else {
                    pse_code    = comp_name;
                    pse_companies[comp_count][PSE_COMPANY_CODE] = pse_code;
                    comp_count++;
                    
                    pse_companies[comp_count]       = new Array();
                    next_comp                       = 0;
                }
            });
            
            $.post(post_comp_url, { 'pse-companies': pse_companies }, function(sts) {
                
                completion                  = completion + 17;
                
                init_progress_completion    = completion
                init_progress_notes         = '<b> - Extracting PSE Companies...</b>';
                
                updateProgressBar(initial_extract_bar, completion);
            });
        });
    }
}

/**------------------------------------------------------------------------
|	Extracts the 2013 Annual Report of PSE Listed Companies
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function extractInitialReports()
{
    var completed_company   = 0;
    var completion          = 0;

    var pse_data_url        = BaseURL+'/pse/extract_doc/';
    var ajax_load_cntr      = ' #contents';
    
    var post_report_url     = BaseURL+'/pse/post_annual_report';
    
    init_progress_notes     = '<b> - Fetching all company information...</b>';
    
    updateProgressBar(initial_extract_bar, completion);
    
    $('.pse-name-id').each(function() {
        
        var bal_sheet           = new Array();
        var income_statement    = new Array();
    
        var pse_id              = $(this).attr('data-value');
        var cur_year            = '';
        var prev_year           = '';
        var company_cntr        = '<div id = "'+pse_id+'"></div>';
        
        var extraction_point    = $('#extraction_container').append(company_cntr);
        
        extraction_point.load(pse_data_url+pse_id+ajax_load_cntr, function() {
            cur_year                            = extraction_point.find('.textCont').eq(1).text().substr(37, 4);
            prev_year                           = parseInt(cur_year) - 1;
            
            bal_sheet[PSE_PSE_ID]               = pse_id;
            bal_sheet[PSE_REPORT_YEAR]          = prev_year;
            bal_sheet[PSE_PERIOD_YEAR]          = extraction_point.find('.textCont').eq(1).text().substr(0, 37)+prev_year;
            bal_sheet[PSE_CURRENT_ASSET]        = extraction_point.find('th:contains("Current Assets")').first().next().next().text();
            bal_sheet[PSE_TOTAL_ASSET]          = extraction_point.find('th:contains("Total Assets")').first().next().next().text();
            bal_sheet[PSE_CURRENT_LIAB]         = extraction_point.find('th:contains("Current Liabilities")').first().next().next().text();
            bal_sheet[PSE_TOTAL_LIAB]           = extraction_point.find('th:contains("Total Liabilities")').first().next().next().text();
            bal_sheet[PSE_RETAINED_EARN]        = extraction_point.find('th:contains("Retained Earnings")').first().next().next().text();
            bal_sheet[PSE_EQUITY]               = extraction_point.find('th:contains("Equity")').first().next().next().text();
            bal_sheet[PSE_EQUITY_PARENT]        = extraction_point.find('th:contains("Equity - Parent")').first().next().next().text();
            bal_sheet[PSE_BOOK_VALUE]           = extraction_point.find('th:contains("Book Value")').first().next().next().text();
            
            
            income_statement[PSE_PSE_ID]        = pse_id;
            income_statement[PSE_REPORT_YEAR]   = prev_year;
            income_statement[PSE_PERIOD_YEAR]   = extraction_point.find('.textCont').eq(1).text().substr(0, 37)+prev_year;
            income_statement[PSE_GROSS_REVENUE] = extraction_point.find('th:contains("Gross Revenue")').first().next().next().text();
            income_statement[PSE_GROSS_EXPENSE] = extraction_point.find('th:contains("Gross Expense")').first().next().next().text();
            income_statement[PSE_BEFORE_TAX]    = extraction_point.find('th:contains("Before Tax")').first().next().next().text();
            income_statement[PSE_AFTER_TAX]     = extraction_point.find('th:contains("After Tax")').first().next().next().text();
            income_statement[PSE_ATTRIB_PARENT] = extraction_point.find('th:contains("Attributable to Parent")').first().next().next().text();
            income_statement[PSE_BASIC_SHARE]   = extraction_point.find('th:contains(" Per Share (Basic)")').first().next().next().text();
            income_statement[PSE_DILUTED_SHARE] = extraction_point.find('th:contains(" Per Share (Diluted)")').first().next().next().text();
            
            $.post(post_report_url, { 'bal-sheet': bal_sheet, 'income-statement': income_statement }, function(company) {
                completed_company++;
                
                completion = 100 * (completed_company / total_company);
                
                init_progress_completion    = completion;
                init_progress_notes         = '<b> - Extracting data of '+company.pse_code+'...</b>';
                
                updateProgressBar(initial_extract_bar, completion);
            }, 'json');
        });
    });
}

function bindViewReportModal()
{
    //-----------------------------------------------------
    //  Binding S48.5: .annual-report-link
    //      Displays a company's Annual Report after clicking link
    //-----------------------------------------------------
    $('.annual-report-link').click(function() {
        
        var company_name    = $(this).attr('data-company-name');
        
        $.get($(this).attr('href'), function(reports) {
            
            var options         = '<option value = "0"> Choose year </option>';
            var report_count    = reports.annuals.length;
            
            $('#report-pse-id').val(reports.pse_id);
            
            for (idx = 0; idx < report_count; idx++) {
                options += '<option value = "'+reports.annuals[idx]['year']+'">'+reports.annuals[idx]['period']+'</option>';
            }
            
            $('#report-selector').html(options);
            $('.report-company-name').html(company_name);
            
            /** Open the Rating form modal */
            $('#modal-show-report').modal({
                keyboard: true,
            })
                .on('hidden.bs.modal', function(e) {
                    $('#report-view-deck').html('');
                })
                .modal('show');
            
        }, 'json');
        
        return false;
    });
}

function bindReportSelectChange()
{
    //-----------------------------------------------------
    //  Binding S48.6: #report-selector
    //      Loads PSE details when changing year from dropdown
    //-----------------------------------------------------
    $('#report-selector').change(function(){
        
        var year     = $(this).find('option:selected').val();
        var pse_id   = $('#report-pse-id').val();
        var ajax_url = BaseURL+'/pse/view_annual';

        $.post(ajax_url, { 'pse_id': pse_id, 'year': year }, function(htm_result) {
            $('#report-view-deck').html(htm_result);
        });
    });
}

initPseExtraction();

});