//======================================================================
//  Script S23: editable-bank.js
//      Bank Input Fields Script
//======================================================================
$(document).ready(function() {
    $('#editable-field-form').addClass('form-block');
    $('#editable-field-form').removeClass('form-inline');
    
    //-----------------------------------------------------
    //  Binding S23.1: select[name="is_independent"]
    //      Hides or shows Bank List when association
    //      status of an SME changes
    //-----------------------------------------------------
    $('select[name="is_independent"]').change(function(){
        if($(this).val()==0){
            $('.bank-ctrl').hide();
        }
        else {
            $('.bank-ctrl').show();
        }
    });
    
    //-----------------------------------------------------
    //  Binding S23.2: #bank
    //      Binds the Bank List to the Select2 Library
    //-----------------------------------------------------
    $("#bank").select2();
    $('select[name="is_independent"]').change();
});