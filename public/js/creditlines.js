//======================================================================
//  Script S20: creditlines.js
//      JS file that contains tools like percentage checking
//======================================================================
$(document).ready(function() {

	jQuery("#formbpo").validationEngine('attach');

	$(".withdate").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-20:+0',
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
	});

	$(".dbanktype").change(function() {
		if($(this).val() == 3){
			$('#bank_collateral'+$(this).data('tvalue')+'').attr('disabled',true);
		}else{
			$('#bank_collateral'+$(this).data('tvalue')+'').attr('disabled', false);
		}
	});

	$(".formtype").click(function() {
		if($(this).val() == 1){
			$('.reasonsform').removeClass('hideme');
			$('.addform').addClass('hideme');
		}else if($(this).val() == 2){
			$('.reasonsform').addClass('hideme');
			$('.addform').removeClass('hideme');
		}
	});

	$(".percentagecheck").val(0);
	
	$(".percentagecheck").blur(function() {
	    var osum = 0;

	    $('.percentagecheck').each(function() {
	        osum += Number($(this).val());
	    });

	    if(osum <= 100){
	    	$('.btn-primary').removeAttr('disabled');
	    }else{
	    	$('.btn-primary').attr('disabled', true);
	    }
	    
	});


});