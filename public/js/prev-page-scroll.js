//======================================================================
//  Script S46: prev-page-scroll.js
//      Sets the Previous Page parameters for going back
//======================================================================

$(document).ready(function() {
    
/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initPrevPageScroll()
{
    /** Variable Definition */
    url_cookie      = getCookie(PREV_URL_COOKIE);
    scroll_cookie   = getCookie(SCR_POS_COOKIE);
    page_cookie     = getCookie(PAG_LOD_COOKIE);
    wait_time       = 0;

    /** Is the current URL same as the previously saved URL */
    if (escape(window.location.pathname) == url_cookie) {
        $('ul#side-menu').find('#'+page_cookie).click();
        /** Delay Business Details 2 scroll for AJAX load */
        if ('nav_registration2' == page_cookie) {
            wait_time = 3000;
        }
        
        setTimeout(function(){
            window.scrollTo(0, parseInt(scroll_cookie, 10));
        }, wait_time);
    }
    else {
        if($('#status').val() == 7){
            $('#sscore').click();
        }
        else if ((1 == $('#bank_ci_view').val()) && (7 != $('#status').val())) {
            $('#nav_investigationsummary').click();
        }
        else if($('#status').val() == 8 && $("#auth_role").val() == 5){
            $("#nav_assocFiles").click();
        }
        else {
            $('#nav_registration1').click();
            setCookie(PAG_LOD_COOKIE, 0, 'nav_registration1');
        }
    }
    
    /** Binds the links of the pages to the click event     */
    bindLinksToScrollSave();
}

/**------------------------------------------------------------------------
|	Binds the Click event to Links and save the current Window Position
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function bindLinksToScrollSave()
{
    //-----------------------------------------------------
    //  Binding S46.1: document links
    //      Saves current scroll position in a cookie
    //      when clicking any link on the Questionnaire page
    //-----------------------------------------------------
    $(document).on('click', 'a', function() {
        /** Saves the current Window Position     */
        saveScrollPosition();
    });
}

/**------------------------------------------------------------------------
|	Saves the current position in the Window
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function saveScrollPosition()
{
    /** Variable Definition */
    var scroll_pos = $(window).scrollTop() - 30;
    var page_url   = escape(window.location.pathname);
    
    setCookie(SCR_POS_COOKIE, 0, scroll_pos);
    setCookie(PREV_URL_COOKIE, 0, page_url);
}

initPrevPageScroll();

});
