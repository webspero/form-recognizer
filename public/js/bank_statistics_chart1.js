//======================================================================
//  Script S4: bank_statistics_chart1.js
//      Used for creating SME Industry Share graph
//======================================================================
function Chart1(container, ChartData){
	// Create the chart
    container.highcharts({
        chart: {
            type: 'pie',
            marginBottom: 75
        },
        title: {
            text: 'Company Industry Share'
        },
        yAxis: {
            title: {
                text: 'Total percent share'
            }
        },
        exporting: {
            enabled: true,
            sourceWidth: 800
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            useHTML: true,
            headerFormat: '',
            pointFormatter: function() {
                return '<b> '+ this.name +' :</b> '+ this.y +'% <br/> <b>Average: </b>'+ this.average;
            }
        },
		credits: {
            enabled: false
        },        
        series: [
            {
                name: 'Main Industry',
                data: ChartData.series1,
                size: '60%',
                dataLabels: {
                    enabled: false
                }
            },
            {
                name: 'Sub Industry',
                data: ChartData.series2,
                size: '80%',
                innerSize: '60%',
                dataLabels: {
                    enabled: false
                }
            },
            {
                name: 'Industry',
                data: ChartData.series3,
                size: '100%',
                innerSize: '60%',
                dataLabels: {
                    useHTML: true,
                    style: {
                        textShadow: 0
                    },
                    formatter: function () {

                        var label = this.point.name;
                        var labelArr = label.split(' ');
                        var c = 0;
                        var word = '';
                        var wordRaw = '';
                        $.each(labelArr, function(k,v){

                            word += ' '+v;
                            wordRaw += ' '+v;
                            if(wordRaw.length > 20){
                                word += '<br/>';
                                wordRaw = '';
                            }

                            c++;
                        });

                        var wordlabel = word + ': ' + this.y + '%<br/><span style = "color: #055688;">Average: '+ ChartData.series3[this.point.index].average +' </span>';
                        return '<span style="font-size:11px">'+wordlabel+'</span>';
                    }
                }
            }
        ]
    });
};
