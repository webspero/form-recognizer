//======================================================================
//  Script S6: bank_statistics_chart3.js
//      Used for creating Average Composite Rating Breakdown graph
//======================================================================
/**
 * Synchronize zooming through the setExtremes event handler.
 */
function syncExtremes(e) {
	var thisChart = this.chart;

	Highcharts.each(Highcharts.charts, function (chart) {
		if (chart !== thisChart) {
			if (chart.xAxis[0].setExtremes) { // It is null while updating
				chart.xAxis[0].setExtremes(e.min, e.max);
			}
		}
	});
}

function Chart3(container, ChartData){
	$('<div class="chart">')
		.appendTo(container)
		.highcharts({
			chart: {
				type: 'bar',
				height: 120,
				marginLeft: 200, // Keep all charts left aligned
				spacingTop: 0,
				spacingBottom: 0
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			title: {
				text: 'Average Composite Rating Breakdown'
			},
			xAxis: {
				categories: ['Business Considerations and Conditions'],
				events: {
					setExtremes: syncExtremes
				},
				title: {
					text: null
				}
			},
			yAxis: {
				title: {
					text: null
				},
				labels:{
					enabled: false
				},
				max: 250
			},
			exporting: {
				buttons: {
					contextButtons: {
						enabled: false,
						menuItems: null
					}
				},
				enabled: false
			},
			plotOptions: {
				bar: {
					stacking: 'normal',
					dataLabels: {
						enabled: true,
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
						style: {
							textShadow: '0 0 3px black',
							width: '100px'
						},
						format: '{series.name}'
					}
				}
			},
			series: [{
				name: ChartData.category1[0],
				data: [ChartData.series1[0]],
				color: '#7cb5ec'
			},{
				name: ChartData.category1[1],
				data: [ChartData.series1[1]],
				color: '#434348'
			},{
				name: ChartData.category1[2],
				data: [ChartData.series1[2]],
				color: '#90ed7d'
			},{
				name: ChartData.category1[3],
				data: [ChartData.series1[3]],
				color: '#f7a35c'
			}]
		});
		
	$('<div class="chart">')
		.appendTo(container)
		.highcharts({
			chart: {
				type: 'bar',
				height: 80,
				marginLeft: 200, // Keep all charts left aligned
				spacingTop: 0,
				spacingBottom: 0
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ['Management Quality'],
				events: {
					setExtremes: syncExtremes
				},
				title: {
					text: null
				}
			},
			yAxis: {
				title: {
					text: null
				},
				labels:{
					enabled: false
				},
				max: 250
			},
			exporting: {
				buttons: {
					contextButtons: {
						enabled: false,
						menuItems: null
					}
				},
				enabled: false
			},
			plotOptions: {
				bar: {
					stacking: 'normal',
					dataLabels: {
						enabled: true,
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
						style: {
							textShadow: '0 0 3px black',
							width: '50px'
						},
						format: '{series.name}'
					}
				}
			},
			series: [{
				name: ChartData.category2[0],
				data: [ChartData.series2[0]],
				color: '#8085e9'
			},{
				name: ChartData.category2[1],
				data: [ChartData.series2[1]],
				color: '#f15c80'
			},{
				name: ChartData.category2[2],
				data: [ChartData.series2[2]],
				color: '#e4d354'
			},{
				name: ChartData.category2[3],
				data: [ChartData.series2[3]],
				color: '#2b908f'
			},{
				name: ChartData.category2[4],
				data: [ChartData.series2[4]],
				color: '#f45b5b' //91e8e1
			}]
		});
	
	$('<div class="chart">')
		.appendTo(container)
		.highcharts({
			chart: {
				type: 'bar',
				height: 110,
				marginLeft: 200, // Keep all charts left aligned
				spacingTop: 0,
				spacingBottom: 0
			},
			credits: {
				enabled: false
			},
			legend: {
				enabled: false
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ['Financial Condition'],
				events: {
					setExtremes: syncExtremes
				},
				title: {
					text: null
				}
			},
			yAxis: {
				title: {
					text: null
				},
				labels:{
					enabled: true
				},
				max: 250
			},
			exporting: {
				buttons: {
					contextButtons: {
						enabled: false,
						menuItems: null
					}
				},
				enabled: false
			},
			plotOptions: {
				bar: {
					stacking: 'normal',
					dataLabels: {
						enabled: true,
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
						style: {
							textShadow: '0 0 3px black',
							width: '100px'
						},
						format: '{series.name}'
					}
				}
			},
			series: [{
				name: ChartData.category3[0],
				data: [ChartData.series3[0]],
				color: '#91e8e1'
			},{
				name: ChartData.category3[1],
				data: [ChartData.series3[1]],
				color: '#7cb5ec'
			}]
		});
};