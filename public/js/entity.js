//======================================================================
//  Script S27: entity.js
//      JS contains helper functions
//======================================================================
$(document).ready(function() {
	if(typeof $('#ongoingentitydata').dataTable == "function"){
		$('#ongoingentitydata').dataTable();
		$('#approvedentitydata').dataTable();
	}
	$('input[type=file]').each(function(){
		if(!$(this).hasClass('form-control')){
			$(this).addClass('form-control');
		}
	});
});