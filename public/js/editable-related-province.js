//======================================================================
//  Script S26: editable-related-province.js
//      Affiliates Provinces input field scripts
//======================================================================
$(document).ready(function() {
    $('#editable-field-form').addClass('form-block');
    $('#editable-field-form').removeClass('form-inline');
    
    //-----------------------------------------------------
    //  Binding S26.1: select[name="province"]
    //      Loads the related cities when province changes
    //      for Affiliates
    //-----------------------------------------------------
    $('select[name="related_company_province"]').on('change', function(){
        var sProvince = $(this).val();
        var uiCity = $('select[name="related_company_cityid"]');
        $.ajax({
            url: BaseURL + '/api/cities',
            type: 'get',
            dataType: 'json',
            data: {
                province: sProvince
            },
            beforeSend: function() {
                $('.city-preloader').show();
                uiCity.html('<option>&nbsp;&nbsp;&nbsp;Loading cities...</option>');
            },
            success: function(oData)	{
                $('.city-preloader').hide();
                uiCity.html('<option value="">Choose your city</option>');
                for(x in oData){
                    uiCity.append('<option value="'+oData[x].city+'">'+oData[x].city+'</option>');
                }
            }
        });
    });
});