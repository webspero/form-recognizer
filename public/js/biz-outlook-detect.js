//======================================================================
//  Script S9: biz-outlook-detect.js
//      Detection of the Extracted BSP Business Outlook File
//======================================================================

$(document).ready(function() {
    
/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initBizOutlookDetection()
{
    detectBspFile();
    openUploadBspFile();
    addBspFile();
}

/**------------------------------------------------------------------------
|	Determines if a new BSP report is available
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function detectBspFile()
{
    /** Variable Definition */
    var ajax_url            = BaseURL+'/admin/bizoutlook/detect-bsp-pdf';
    var ajax_dl_url         = BaseURL+'/admin/autoupdate_download/';
    var ajax_add_bsp_url    = BaseURL+'/admin/bizoutlook/add-bsp-pdf';
    var bsp_download_modal  = $('#bsp-detected-main');
    
    /** Determines if a new BSP report is available */
    $.get(ajax_url, function(response) {
        
        /** New BSP Business Expectation is available   */
        if (response.valid) {
            /** Open the Download modal */
            bsp_download_modal.modal({
                keyboard: false,
                backdrop: 'static',
            })
                .on('show.bs.modal', function(e) {
                    
                    var bsp_filename = response.filename;
                    
                    $.ajax({
                        url: ajax_dl_url+bsp_filename,
                        beforeSend: function() {
                            $('.auto-status').text('Downloading file... Please wait.'); 
                        },
                        success: function(sts) {
                            if(sts == "success"){
                                
                                $('.auto-status').text('Initializing Data Extraction...');
                                /** Adds the file to the Database   */
                                $.post(ajax_add_bsp_url, { 'filename': bsp_filename }, function(response) {
                                    
                                    $.ajax({
                                        url: BaseURL+'/admin/autoupdate_parse/'+bsp_filename,
                                        beforeSend: function() { 
                                            $('.auto-status').text('Data conversion in progress...');
                                        }, 
                                        success: function(sts) {
                                            window.location = BaseURL+'/admin/industry';
                                        }
                                    });
                                    
                                });

                            }
                            else {
                                $('.auto-status').text('Extraction failed. Please contact Support to initiate manual extraction.');
                            }
                        }
                    });
                })
                .modal('show');
        }
    }, 'json');
}

/**------------------------------------------------------------------------
|	Closes the Detection Modal and Opens the Upload Modal
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function openUploadBspFile()
{
    /** Variable Definition */
    var upload_link = $('#upload-detected-bsp');
    var upload_btn  = $('#pop-upload-bsp-btn');
    var bsp_download_modal  = $('#bsp-detected-main');
    
    //-----------------------------------------------------
    //  Binding S9.1: #upload-detected-bsp
    //      Uploads the BSP Business Outlook File when
    //      button is clicked
    //-----------------------------------------------------
    upload_link.click(function() {
        /** Closes the detection Modal  */
        bsp_download_modal.modal('hide');
        
        /** Opens the Upload Modal      */
        upload_btn.click();
        
        return false;
    });
}

/**------------------------------------------------------------------------
|	Adds the downloaded BSP file to the Database
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function addBspFile()
{
    var filename    = '';
    var ajax_url    = BaseURL+'/admin/bizoutlook/add-bsp-pdf';
    var dl_link     = $('#bsp-dl-link');
    
    //-----------------------------------------------------
    //  Binding S9.2: #bsp-dl-link
    //      Save information of BSP Business Outlook file
    //      to the database when link is clicked
    //-----------------------------------------------------
    dl_link.click(function() {
        /** Acquires the filename   */
        filename    = $(this).attr('data-filename');
        
        /** Adds the file to the Database   */
        $.post(ajax_url, { 'filename': filename }, function(response) {
            /** No processing   */
        });
    });
    
}

initBizOutlookDetection();

});