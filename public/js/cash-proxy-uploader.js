//======================================================================
//  Script S15: cash-proxy-uploader.js
//      Uploads Bank Documents and Utility Bills
//======================================================================
$(document).ready(function(){
    
    //-----------------------------------------------------
    //  Binding S15.1: .datepicker
    //      Converts input boxes into JQuery datepickers
    //-----------------------------------------------------
    $('.datepicker').datepicker({
        dateFormat: 'mm/dd/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '-30:+0',
        onSelect: function(){
            check_container();
        },
		onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
    });
    
    //-----------------------------------------------------
    //  Binding S15.2: #upload-field-cntr
    //      Checks the completion of details when value of
    //      changes
    //-----------------------------------------------------
    $('#upload-field-cntr').on('change', 'input[type="file"]', function(){
        check_container();
    });
    
    //-----------------------------------------------------
    //  Binding S15.3: #upload-field-cntr
    //      Checks the completion of details when value of
    //      input changes
    //-----------------------------------------------------
    $('#upload-field-cntr').on('change', '.datepicker', function(){
        check_container();
    });
    
    //-----------------------------------------------------
    //  Binding S15.4: #dd-upload-field
    //      Creates another set of Upload fields when clicked
    //-----------------------------------------------------
    $('#add-upload-field').click(function() {
        addUploadFields();
        return false;
    });
    
    //-----------------------------------------------------
    //  Binding S15.5: #upload-docs-form
    //      Disables Submit button while processing form
    //-----------------------------------------------------
    $('#upload-docs-form').submit(function() {
        $('#submit').attr('disabled', 'disabled').val('Saving Information');
    });

    function check_container() {
        
        var comp = true;
        
        $('input[type="file"]').each(function(){
            if($(this).val()!=''){
                if($(this).parent().parent().find('.datepicker').val()==''){
                    comp = false;
                }
            }
        });
        
        if (comp) {
            $('#submit').removeAttr('disabled');
        }
        else {
            $('#submit').attr('disabled', 'disabled');
        }
    }

    function addUploadFields()
    {
        var field_cntr      = $('#upload-field-cntr');
        var upload_fields   = $('.upload-fields');
        var field_htm       = '';

        field_htm = upload_fields.clone().removeClass('upload-fields');
        field_htm.find('input[type=file]').val('');
        field_htm.find('.datepicker').val('');
        field_htm.appendTo(field_cntr);
        
        $(".hasDatepicker").removeAttr('id').removeClass("hasDatepicker");
        $('.datepicker').datepicker("destroy");
        $('.datepicker').datepicker({
            dateFormat: 'mm/dd/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: '-30:+0',
            onSelect: function(){
                check_container();
            },
			onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
        });
    }

});