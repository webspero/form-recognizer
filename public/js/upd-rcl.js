//======================================================================
//  Script S64: upd-rcl.js
//      Adding and Editing of RCL data
//======================================================================
$(document).ready(function(){

/**------------------------------------------------------------------------
|	Binds the Add RCL link to the Modal Pop-up form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function popRclForm()
{
    /** Variable Definition    */
    var rcl_cntr    = $('#pfs');
    var hdr_txt     = trans('rcl.rcl');
    var form_id     = 'rcl-form';
    var get_url     = BaseURL+'/sme/planfacilityrequested/'+$('#entityids').val();
    var pop_rcl_btn = '.pop-rcl-form';
    
    //-----------------------------------------------------
    //  Binding S64.1: .pop-rcl-form
    //      Launch the Expander Form for Required Credit Line
    //-----------------------------------------------------
    // rcl_cntr.on('click', pop_rcl_btn, function(){
    //     initPopForm(hdr_txt, form_id, get_url);
    //     return false;
    // });

    rcl_cntr.on('click', pop_rcl_btn, function(){
        $('#dscr-body').load(get_url, function(){

        });
        $('#dscr-modal').modal('show');
        return false;
    });
}

popRclForm();

});
