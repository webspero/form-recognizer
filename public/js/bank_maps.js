//======================================================================
//  Script S2: bank_maps.js
//      JS file that contains scripts for supervisor maps
//======================================================================
$(document).ready(function(){
	
	$('#select-sme').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 200,
		buttonWidth: '100%',
		numberDisplayed: 1,
		enableCaseInsensitiveFiltering: true
	});
	
	$('#industry1').on('change', function(){
		$.ajax({
            url: BaseURL + "/api/industrysub",
            data: { 'industrymain': $(this).val() },
            beforeSend: function(xhr) {
                $('#industry2').html('<option value = ""> Loading sub-industries... </option>');
				$('#industry3').html('<option value = "">All</option>');
            },
            success: function(response, xhr) {             
                $('#industry2').html('<option value="">All</option>');
                $.each(response, function(key, value) {   
                    $('#industry2').append($("<option></option>").val(key).text(value)); 
                });
            }
        });
	});
	
	$('#industry2').on('change', function(){
		$.ajax({
            url: BaseURL + "/api/industryrow",
            data: { 'industrysub': $(this).val() },
            beforeSend: function(xhr) {
                $('#industry3').html('<option value = ""> Loading industries... </option>');
            },
            success: function(response, xhr) {             
                $('#industry3').html('<option value="">All</option>');
                $.each(response, function(key, value) {   
                    $('#industry3').append($("<option></option>").val(key).text(value)); 
                });
            }
        });
	});
});

