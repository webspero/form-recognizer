//======================================================================
//  Script S24: editable-industry.js
//      Industry input fields scripts
//======================================================================
$(document).ready(function() {
    $('#editable-field-form').addClass('form-block');
    $('#editable-field-form').removeClass('form-inline');
    //-----------------------------------------------------
    //  Binding S24.1: #industrymain
    //      Loads the related sub industries when main industry
    //      value changes
    //-----------------------------------------------------
    $('#industrymain').change(function(){
        /**--------------------------------------------------------------
        /*	Variable Definition
        /*-------------------------------------------------------------*/
        var ajax_url = BaseURL + "/api/industrysub"
        
        /** Industry Fields  */
        var industrysub = $('#industrysub');
        var industryrow = $('#industryrow');
        
        /** Loading Images  */
        var sub_loader  = $('.sub-ind-load');
        var ind_loader   = $('.ind-load');
        
        $.ajax({
            method: 'get',
            url: ajax_url,
            data: { 'industrymain': $(this).val() },
            beforeSend: function(xhr) {
                /**---------------------------------------------------------
                /*	Disables the Industry Fields that will be redrawn
                /*--------------------------------------------------------*/
                industrysub.prop('disabled', true);
                industryrow.prop('disabled', true);
                
                /**---------------------------------------------------------
                /*	Shows the Loading Images
                /*--------------------------------------------------------*/
                sub_loader.show();
                ind_loader.show();
                
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industrysub.html('<option value = ""> Loading sub-industries... </option>');
                industryrow.html('<option value = ""> Loading industries... </option>');
                
            },
            success: function(response, xhr) {             
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industrysub.html('<option value="">Choose your sub-industry</option>');
                industryrow.html('<option value="">Choose your sub-industry first</option>');
                
                $.each(response, function(key, value) {   
                    industrysub.append($("<option></option>").val(key).text(value)); 
                });
                
                /**---------------------------------------------------------
                /*	Enables the Industry Fields that was redrawn
                /*--------------------------------------------------------*/
                industrysub.prop('disabled', false);
                industryrow.prop('disabled', false);
                
                if(curSub != null) {
                    industrysub.val(curSub).change();
                    curSub = null;
                }
                /**---------------------------------------------------------
                /*	Hides the Loading Images
                /*--------------------------------------------------------*/
                sub_loader.hide();
                ind_loader.hide();
            }
        });
    });
    
    //-----------------------------------------------------
    //  Binding S25.1: #industrysub
    //      Loads the related industries when Industry sub
    //      values changes
    //-----------------------------------------------------
    $('#industrysub').change(function() {
        /**--------------------------------------------------------------
        /*	Variable Definition
        /*-------------------------------------------------------------*/
        var ajax_url = BaseURL + "/api/industryrow"
        
        /** Industry Fields  */
        var industrysub = $('#industrysub');
        var industryrow = $('#industryrow');
        
        /** Loading Images  */
        var sub_loader  = $('.sub-ind-load');
        var ind_loader   = $('.ind-load');
        
        $.ajax({
            method: 'get',
            url: ajax_url,
            data: { 'industrysub': $(this).val() },
            beforeSend: function(xhr) {
                /**---------------------------------------------------------
                /*	Disables the Industry Fields that will be redrawn
                /*--------------------------------------------------------*/
                industryrow.prop('disabled', true);
                
                /**---------------------------------------------------------
                /*	Shows the Loading Images
                /*--------------------------------------------------------*/
                ind_loader.show();
                
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/
                industryrow.html('<option value = ""> Loading industries... </option>');
            },
            success: function(response, xhr) {
                /**---------------------------------------------------------
                /*	Redraws the Industry Field
                /*--------------------------------------------------------*/                
                industryrow.html('<option value="">Choose your industry</option>');
                
                $.each(response, function(key, value) {   
                    industryrow.append($("<option></option>").val(key).text(value)); 
                });
                
                /**---------------------------------------------------------
                /*	Enables the Industry Fields that was redrawn
                /*--------------------------------------------------------*/
                industryrow.prop('disabled', false);
                
                /**---------------------------------------------------------
                /*	Hides the Loading Images
                /*--------------------------------------------------------*/
                ind_loader.hide();
                if(curRow != null) {
                    industryrow.val(curRow);
                    curRow = null;
                    $(".editable-error-msg").hide();
                }
            }
        });
    });
    
    if ('' == $('#prev_val').val()) {
        $('#industrymain').change();
    }
});