//======================================================================
//  Script S44: nfcc.js
//      JS helper for NFCC computation
//======================================================================
$(document).ready(function(){
	
	$('#nfcc_company').on('change', function(){
		var selected = $(this).val();
		if(selected!=''){
			$.ajax({
				url: BaseURL + '/bank/nfcc_get_fs',
				type: 'get',
				dataType: 'json',
				data: {
					entity_id: selected
				},
				beforeSend: function(){
					$('#nfcc_current_assets').text('loading...');
					$('#nfcc_current_liabilities').text('loading...');
				},
				success: function(oData){
					$('#nfcc_current_assets').text(number_format(oData.CurrentAssets*oData.money_factor,2,'.',','));
					$('#nfcc_current_liabilities').text(number_format(oData.CurrentLiabilities*oData.money_factor,2,'.',','));
					computeNFCC();
				}
			});
		}
	});
	
	$('.update_nfcc').on('keyup', function(){
		computeNFCC();
	});
	
	$('.update_nfcc_dd').on('change', function(){
		computeNFCC();
	});
});

function computeNFCC(){
	var nfcc_current_assets = parseFloat($('#nfcc_current_assets').text().replace(/,/g , ""));
	var nfcc_current_liabilities = parseFloat($('#nfcc_current_liabilities').text().replace(/,/g , ""));
	var k = parseFloat($('#nfcc_k').val());
	var voc = parseFloat($('#nfcc_voc').val().replace(/,/g , ""));
	voc = isNaN(voc) ? 0 : voc;
	var nfcc = (nfcc_current_assets - nfcc_current_liabilities) * k - voc;
	
	$('#nfcc').html('<b>'+number_format(nfcc,2,'.',',')+'</b>');
	
	var abc = parseFloat($('#nfcc_budget').val().replace(/,/g , ""));
	abc = isNaN(abc) ? 0 : abc;
	
	if(abc<=0){
		$('#nfcc_abc').html('<b>-</b>');
	} else {
		$('#nfcc_abc').html('<b>'+number_format(nfcc/abc,2,'.',',')+'</b>');
	}
}

function number_format(number, decimals, dec_point, thousands_sep) {
 
 number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}