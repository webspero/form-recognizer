//======================================================================
//  Script S28: external-link.js
//      JS file for Question pass
//======================================================================
var Panels = {
	'registration1' : [
		{
			'panel' : 'biz-details-cntr',
			'title' : $('#biz-details-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'affiliates-list-cntr',
			'title' : $('#affiliates-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'offers-list-cntr',
			'title' : $('#offers-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'cap-details-list-cntr',
			'title' : $('#cap-details-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'main-loc-list-cntr',
			'title' : $('#main-loc-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'branches-list-cntr',
			'title' : $('#branches-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'certifications-list-cntr',
			'title' : $('#certifications-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'biztyp-list-cntr',
			'title' : $('#biztyp-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'insurance-list-cntr',
			'title' : $('#insurance-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'cash-conversion-cntr',
			'title' : $('#cash-conversion-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'financial-statement-cntr',
			'title' : $('#financial-statement-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'cash-proxy-cntr',
			'title' : $('#cash-proxy-cntr > div > h4').text().replace('More info...', '')
		},
        {
			'panel' : 'other-docs-cntr',
			'title' : $('#other-docs-cntr > div > h4').text().replace('More info...', '')
		}
	],
	'registration2' : [
		{
			'panel' : 'major-cust-list-cntr',
			'title' : $('#major-cust-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'major-supp-list-cntr',
			'title' : $('#major-supp-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'major-loc-list-cntr',
			'title' : $('#major-loc-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'competitor-list-cntr',
			'title' : $('#competitor-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'bizdriver-list-cntr',
			'title' : $('#bizdriver-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'bizsegment-list-cntr',
			'title' : $('#bizsegment-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'past-proj-list-cntr',
			'title' : $('#past-proj-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'future-growth-list-cntr',
			'title' : $('#future-growth-list-cntr > div > h4').text().replace('More info...', '')
		}
	],
	'payfactor' : [
		{
			'panel' : 'payfactor',
			'title' : $('.pr3 > a').text()
		}
	],
	'ownerdetails' : [
		{
			'panel' : 'ownerdetails',
			'title' : $('.pr6 > a').text()
		},
		{
			'panel' : 'management-list-cntr',
			'title' : $('#management-list-cntr > div > h4').text().replace('More info...', '')
		}
	],
	'bcs' : [
		{
			'panel' : 'succession-list-cntr',
			'title' : $('#succession-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'competitionlandscape-list-cntr',
			'title' : $('#competitionlandscape-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'economicfactors-list-cntr',
			'title' : $('#economicfactors-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'taxpayments-list-cntr',
			'title' : $('#taxpayments-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'ra-list-cntr',
			'title' : $('#ra-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'rev-growth-list-cntr',
			'title' : $('#rev-growth-list-cntr > div > h4').text().replace('More info...', '')
		},
		{
			'panel' : 'phpm-list-cntr',
			'title' : $('#phpm-list-cntr > div > h4').text().replace('More info...', '')
		}
	],
	'pfs' : [
		{
			'panel' : 'pfs',
			'title' : $('.pr5 > a').text()
		}
	],
};

var arExternalLinks = [];

function externalLinkWorker() {
	var entity_id = $('#entityids').val();
	if(entity_id != undefined && entity_id != '' && entity_id != 0){
		$.ajax({
		url: BaseURL + '/check_external_link',
		data: { entity_id : entity_id },
		type: 'get',
		dataType: 'json',
		success: function(oData) {
			var sPanels = [];
			for(x in oData){
				if(oData[x].is_active == 1){
					if($.inArray(oData[x].id, arExternalLinks) == -1){
						arExternalLinks.push(oData[x].id);
					}
				} else {
					if($.inArray(oData[x].id, arExternalLinks) > -1){
						obj = JSON.parse(oData[x].panels);
						for(y in Panels[obj.tab]){
							if($.inArray(Panels[obj.tab][y].panel, obj.data) > -1){
								sPanels.push(Panels[obj.tab][y].title);
							}
						}
					}
				}
			}
			if(sPanels.length > 0){
				$('#modal-external-link-notification .list').html('');
				for(x in sPanels){
					$('#modal-external-link-notification .list').append('<li>'+sPanels[x]+'</li>');
				}
				$('#modal-external-link-notification').modal();
			}
		},
		complete: function() {
			setTimeout(externalLinkWorker, 300000);
		}
	  });
	}
}

$(document).ready(function(){
	$('.create-external-link').click(function(e){
		e.preventDefault();
		var sOpenTab    = $('.tab-content > div.active').attr('id');
		var uiModal     = $('#modal-external-link');
		var bank_id     = $('#sme_supervisor').val();
		var sHtml       = '';
        
        if (!$('#sme_supervisor').val()) {
            bank_id = 0;
        }
        
        $.post(BaseURL+'/get/external-link-config/'+bank_id, { 'panel': Panels[sOpenTab] }, function(response) {
            for(x in Panels[sOpenTab]){
                if($('#' + Panels[sOpenTab][x].panel).is(':visible')) {
                    if (0 > $.inArray(Panels[sOpenTab][x].panel, response.hidden)) {
                        sHtml += '<label><input type="checkbox" value="' + Panels[sOpenTab][x].panel + '" /> ';
                        sHtml += Panels[sOpenTab][x].title + '</label><br />';
                    }
                }
            }
            
            if(sHtml==''){
                sHtml = 'You cannot create a link on this tab.';
            }
			
			sHtml += '<br><p>This function allows you to pass-on questions to other people to be answered. Click Question Pass. Check the items you want to pass-on. Then click Generate a link. Copy the link and email that link to the person (non-user) who will answer the parts of the questionnaire you checked. Once this non-user has clicked on the link and filled up the answers to your requested info, ask them to inform you of completion. Once done, refresh your questionnaire page so that it updates with the replies provided by the non-user through the external link. The external link is only good for one use. To send questions from a different tab you must go to the desired tab and click the Question Pass option again.</p>'; 
            
            uiModal.find('.modal-body').html(sHtml);
            $('#modal-external-link .external-link-btn').show();
            uiModal.modal();
            
        },'json');
        
		
	});
	
	$('#modal-external-link .external-link-btn').click(function(e){
		e.preventDefault();
		var sOpenTab = $('.tab-content > div.active').attr('id');
		var arData = [];
		$('#modal-external-link .modal-body input[type=checkbox]:checked').each(function(){
			arData.push($(this).val());
		});
		if(arData.length > 0){
			$.ajax({
				url: BaseURL + '/create-external-link',
				type: 'post',
				data: {
					tab: sOpenTab,
					data: arData
				},
				beforeSend: function(){
					$('#modal-external-link .external-link-btn').hide();
					$('#modal-external-link .modal-body').html('Creating link. Please wait...');
				},
				success: function(sReturn){
					sUrl = BaseURL + '/external-link/' + sReturn;
					$('#modal-external-link .modal-body').html(sUrl);
				}
			});
		}
	});
	
	$('#side-menu > li > a').click(function(){
		if($(this).attr('href')=='#sci'){
			$('.create-external-link').hide();
		} else {
			$('.create-external-link').show();
		}
	});
	
	$('#modal-external-link-notification .meln-yes').click(function(){
		location.reload(true);
	});
	
	externalLinkWorker();
});


