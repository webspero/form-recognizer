//======================================================================
//  Script S13: busidentification.js
//      Used to fetch SME questionnaire items using AJAX
//======================================================================
var serviceofferLoad = function(showto, id) {

  $.get(BaseURL + "/api/servicesoffer", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
          showResult.empty();
          if($('#userid').val() == 2){
            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-6 col-sm-6"><b>'+trans('business_details1.products_services_offered')+'</b></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details1.target_market')+'</b></div></span>'); 
          }else{
            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3 col-sm-3"><b>'+trans('business_details1.products_services_offered')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details1.target_market')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details1.share_to_total_sales')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details1.seasonality')+'</b></div></span>');   
          }
          $.each(data, function(key, value) {
            if(value.servicesoffer_seasonality == 1){
              var typename = trans('business_details1.seasonality_year_round');
            }else if(value.servicesoffer_seasonality == 2){
              var typename = trans('business_details1.seasonality_6_mo');
            }else if(value.servicesoffer_seasonality == 3){
              var typename = trans('business_details1.seasonality_3_mo');
            }else{
              var typename = trans('business_details1.seasonality_less_than_3_mo');
            }

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/servicesoffer/'+value.servicesofferid+'/edit">Edit</a> | <a href = "' + BaseURL + '/sme/servicesoffer/'+value.servicesofferid+'/delete">Delete</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }
            if($('#userid').val() == 2){
              showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-6">'+showEdit+''+value.servicesoffer_name+'</div><div class="col-md-6">'+value.servicesoffer_targetmarket+'<span class="investigator-control" group_name="Products/Services Offered" label="'+trans('business_details1.products_services_offered')+'" value="'+value.servicesoffer_name+'" group="services_offer" item="services_offer_'+value.servicesofferid+'"></span></div></span>'); 
			  //mobile
			  showResult.append('<span class="details-row visible-xs-block visible-sm-block"><div class="col-xs-12"><b>'+trans('business_details1.products_services_offered')+'</b><br />'+value.servicesoffer_name+'</div><div class="col-xs-12"><b>'+trans('business_details1.target_market')+'</b><br />'+value.servicesoffer_targetmarket+'<span class="investigator-control" group_name="Products/Services Offered" label="'+trans('business_details1.products_services_offered')+'" value="'+value.servicesoffer_name+'" group="services_offer" item="services_offer_'+value.servicesofferid+'"></span></div></span>'); 
            }else{
              showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3 col-sm-3">'+showEdit+''+value.servicesoffer_name+'</div><div class="col-md-3 col-sm-3">'+value.servicesoffer_targetmarket+'</div><div class="col-md-3 col-sm-3">'+value.servicesoffer_share_revenue+'%</div><div class="col-md-3 col-sm-3">'+typename+'<span class="investigator-control" group_name="Products/Services Offered" label="'+trans('business_details1.products_services_offered')+'" value="'+value.servicesoffer_name+'" group="services_offer" item="services_offer_'+value.servicesofferid+'"></span></div></span>');
			  //mobile
			  showResult.append('<span class="details-row visible-xs-block visible-sm-block"><div class="col-xs-12 col-sm-3"><b>'+trans('business_details1.products_services_offered')+'</b><br />'+value.servicesoffer_name+'</div><div class="col-xs-12 col-sm-3"><b>'+trans('business_details1.target_market')+'</b><br />'+value.servicesoffer_targetmarket+'</div><div class="col-xs-12 col-sm-3"><b>'+trans('business_details1.share_to_total_sales')+'</b><br />'+value.servicesoffer_share_revenue+'%</div><div class="col-xs-12 col-sm-3"><b>'+trans('business_details1.seasonality')+'</b><br/>'+typename+'<span class="investigator-control" group_name="Products/Services Offered" label="'+trans('business_details1.products_services_offered')+'" value="'+value.servicesoffer_name+'" group="services_offer" item="services_offer_'+value.servicesofferid+'"></span></div><div class="col-xs-12">'+showEdit+'</div></span>');
            }
          });
		  Investigator.initGroup('services_offer');
  }); 

};
var businessdriverLoad = function(showto, id) {

  $.get(BaseURL + "/api/businessdriver", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-4 col-sm-4"><b>'+trans('business_details2.bdriver_name')+'</b></div><div class="col-md-8 col-sm-8"><b>'+trans('business_details2.bdriver_share')+'</b></div></span>'); 
          $.each(data, function(key, value) {
            
            
            if(($('#status').val() == 0 || $('#status').val() == 3) && ($('#userid').val() == 1)){
              var showEdit = '<a href = "' + BaseURL + '/sme/businessdriver/'+value.businessdriverid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/businessdriver/'+value.businessdriverid+'/delete">'+trans('messages.delete')+'</a> ';
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-4 col-sm-4">'+showEdit+''+value.businessdriver_name+'</div><div class="col-md-8 col-sm-8">'+value.businessdriver_total_sales+'%</div></span>'); 
			
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-4"><b>'+trans('business_details2.bdriver_name')+'<br/></b>'+value.businessdriver_name+'</div><div class="col-md-8"><b>'+trans('business_details2.bdriver_share')+'</b><br />'+value.businessdriver_total_sales+'%</div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
        }
  }); 

};
var businesssegmentLoad = function(showto, id) {

  $.get(BaseURL + "/api/businesssegment", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          $.each(data, function(key, value) {
            
            if(($('#status').val() == 0 || $('#status').val() == 3) && ($('#userid').val() == 1)){
              var showEdit = '<a href = "' + BaseURL + '/sme/businesssegment/'+value.businesssegmentid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/businesssegment/'+value.businesssegmentid+'/delete">'+trans('messages.delete')+'</a> ';
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row"><div class="col-md-12">'+showEdit+''+value.businesssegment_name+'</div></span>'); 
          });
        }
  }); 

};
var majorcustomerLoad = function(showto, id) {

  $.get(BaseURL + "/api/majorcustomer", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
          $.each(data, function(key, value) {
            if(value.customer_settlement == 1){
              var typename = trans('business_details2.mc_ahead');
            }else if(value.customer_settlement == 2){
              var typename = trans('business_details2.mc_ondue');
            }else if(value.customer_settlement == 3){
              var typename = trans('business_details2.mc_portion');
            }else if(value.customer_settlement == 4){
              var typename = trans('business_details2.mc_beyond');
            }else{
              var typename = trans('business_details2.mc_delinquent');
            }
            
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/majorcustomer/'+value.majorcustomerrid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/majorcustomer/'+value.majorcustomerrid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }
            
            if($('#userid').val() == 2 || $('#userid').val() == 3){
              showResult.append('<input type="hidden" value="'+value.customer_share_sales+'" name="totalsalevalue" class="totalsalevalue"><input type="hidden" value="'+value.customer_experience+'" name="cexperience" class="texperience"><span class="details-row"><div class="col-md-6"><b>Name</b></div><div class="col-md-6">'+value.customer_name+'<span class="investigator-control" group_name="Major Customer/s" label="Name" value="'+value.customer_name+'" group="major_customer" item="customer_name_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.mc_percent_share')+'</b></div><div class="col-md-6">'+value.customer_share_sales+'%<span class="investigator-control" group_name="Major Customer/s" label="% Share of Your Sales to this Company vs. Your Total Sales" value="'+value.customer_share_sales+'" group="major_customer" item="customer_share_sales_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>Address</b></div><div class="col-md-6">'+value.customer_address+'<span class="investigator-control" group_name="Major Customer/s" label="Address" value="'+value.customer_address+'" group="major_customer" item="customer_address_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>Contact Person</b></div><div class="col-md-6">'+value.customer_contact_person+'<span class="investigator-control" group_name="Major Customer/s" label="Contact Person" value="'+value.customer_contact_person+'" group="major_customer" item="customer_contact_person_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>Contact Phone Number</b></div><div class="col-md-6">'+value.customer_phone+'<span class="investigator-control" group_name="Major Customer/s" label="Contact Phone Number" value="'+value.customer_phone+'" group="major_customer" item="customer_phone_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>Contact Email</b></div><div class="col-md-6">'+value.customer_email+'<span class="investigator-control" group_name="Major Customer/s" label="Contact Email" value="'+value.customer_email+'" group="major_customer" item="customer_email_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>Experience</b></div><div class="col-md-6">'+value.customer_experience+'<span class="investigator-control" group_name="Major Customer/s" label="Experience" value="'+value.customer_experience+'" group="major_customer" item="customer_experience_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.mc_year_started')+'</b></div><div class="col-md-6">'+value.customer_started_years+'<span class="investigator-control" group_name="Major Customer/s" label="Year Started Doing Business With" value="'+value.customer_started_years+'" group="major_customer" item="customer_started_years_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.mc_year_doing_business')+'</b></div><div class="col-md-6">'+value.customer_years_doing_business+'<span class="investigator-control" group_name="Major Customer/s" label="Years Doing Business With" value="'+value.customer_years_doing_business+'" group="major_customer" item="customer_years_doing_business_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.mc_payment_behavior')+'</b></div><div class="col-md-6">'+typename+'<span class="investigator-control" group_name="Major Customer/s" label="Customer payment behavior in last 3 payments" value="'+typename+'" group="major_customer" item="typename_'+value.majorcustomerrid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.mc_order_frequency')+'</b></div><div class="col-md-6">'+convert('business_details2', value.customer_order_frequency)+'<span class="investigator-control" group_name="Major Customer/s" label="Order Frequency" value="'+convert('business_details2', value.customer_order_frequency)+'" group="major_customer" item="customer_order_frequency_'+value.majorcustomerrid+'"></span></div><div class="col-md-12">'+showEdit+'</div></span>'); 
            }else{
              showResult.append('<input type="hidden" value="'+value.customer_share_sales+'" name="totalsalevalue" class="totalsalevalue"><input type="hidden" value="'+value.customer_experience+'" name="cexperience" class="texperience"><span class="details-row"><div class="col-md-6 col-sm-6"><b>Name</b></div><div class="col-md-6 col-sm-6">'+value.customer_name+'<span class="investigator-control" group="major_customer" item="customer_name_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.mc_percent_share')+'</b></div><div class="col-md-6 col-sm-6">'+value.customer_share_sales+'%<span class="investigator-control" group="major_customer" item="customer_share_sales_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>Address</b></div><div class="col-md-6 col-sm-6">'+value.customer_address+'<span class="investigator-control" group="major_customer" item="customer_address_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Person</b></div><div class="col-md-6 col-sm-6">'+value.customer_contact_person+'<span class="investigator-control" group="major_customer" item="customer_contact_person_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Phone Number</b></div><div class="col-md-6 col-sm-6">'+value.customer_phone+'<span class="investigator-control" group="major_customer" item="customer_phone_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Email</b></div><div class="col-md-6 col-sm-6">'+value.customer_email+'<span class="investigator-control" group="major_customer" item="customer_email_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.mc_year_started')+'</b></div><div class="col-md-6 col-sm-6">'+value.customer_started_years+'<span class="investigator-control" group="major_customer" item="customer_started_years_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.mc_year_doing_business')+'</b></div><div class="col-md-6 col-sm-6">'+value.customer_years_doing_business+'<span class="investigator-control" group="major_customer" item="customer_years_doing_business_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.mc_payment_behavior')+'</b></div><div class="col-md-6 col-sm-6">'+typename+'<span class="investigator-control" group="major_customer" item="typename_'+value.majorcustomerrid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.mc_order_frequency')+'</b></div><div class="col-md-6 col-sm-6">'+convert('business_details2', value.customer_order_frequency)+'<span class="investigator-control" group="major_customer" item="customer_order_frequency_'+value.majorcustomerrid+'"></span></div><div class="col-md-12">'+showEdit+'</div></span>'); 
            }
          });
		  Investigator.initGroup('major_customer');
  }); 

};
var majorsupplierLoad = function(showto, id) {

  $.get(BaseURL + "/api/majorsupplier", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
          $.each(data, function(key, value) {
            if(value.supplier_settlement == 1){
              var typename = trans('business_details2.ms_ahead');
            }else if(value.supplier_settlement == 2){
              var typename = trans('business_details2.ms_ondue');
            }else if(value.supplier_settlement == 3){
              var typename = trans('business_details2.ms_portion');
            }else if(value.supplier_settlement == 4){
              var typename = trans('business_details2.ms_beyond');
            }else{
              var typename = trans('business_details2.ms_delinquent');
            }

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/majorsupplier/'+value.majorsupplierid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/majorsupplier/'+value.majorsupplierid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

            if($('#userid').val() == 2 || $('#userid').val() == 3){
              showResult.append('<input type="hidden" value="'+value.supplier_share_sales+'" name="totalsuppliersalevalue" class="totalsuppliersalevalue"><input type="hidden" value="'+value.supplier_experience+'" name="sexperience" class="texperience"><span class="details-row"><div class="col-md-6"><b>Name</b></div><div class="col-md-6">'+value.supplier_name+'<span class="investigator-control" group_name="Major Supplier/s" label="Name" value="'+value.supplier_name+'" group="major_supplier" item="supplier_name_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>Address</b></div><div class="col-md-6">'+value.supplier_address+'<span class="investigator-control" group_name="Major Supplier/s" label="Address" value="'+value.supplier_address+'" group="major_supplier" item="supplier_address_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>Contact Person</b></div><div class="col-md-6">'+value.supplier_contact_person+'<span class="investigator-control" group_name="Major Supplier/s" label="Contact Person" value="'+value.supplier_contact_person+'" group="major_supplier" item="supplier_contact_person_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>Contact Phone Number</b></div><div class="col-md-6">'+value.supplier_phone+'<span class="investigator-control" group_name="Major Supplier/s" label="Contact Phone Number" value="'+value.supplier_phone+'" group="major_supplier" item="supplier_phone_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>Contact Email</b></div><div class="col-md-6">'+value.supplier_email+'<span class="investigator-control"  group_name="Major Supplier/s" label="Contact Email" value="'+value.supplier_email+'" group="major_supplier" item="supplier_email_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>Experience</b></div><div class="col-md-6">'+value.supplier_experience+'<span class="investigator-control" group_name="Major Supplier/s" label="Experience" value="'+value.supplier_experience+'" group="major_supplier" item="supplier_experience_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.ms_year_started')+'</b></div><div class="col-md-6">'+value.supplier_started_years+'<span class="investigator-control" group_name="Major Supplier/s" label="Year Started Doing Business With" value="'+value.supplier_started_years+'" group="major_supplier" item="supplier_started_years_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.ms_year_doing_business')+'</b></div><div class="col-md-6">'+value.supplier_years_doing_business+'<span class="investigator-control" group_name="Major Supplier/s" label="Years Doing Business With" value="'+value.supplier_years_doing_business+'" group="major_supplier" item="supplier_years_doing_business_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.ms_payment')+'</b></div><div class="col-md-6">'+typename+'<span class="investigator-control" group_name="Major Supplier/s" label="Your payment to supplier in last 3 payments" value="'+typename+'" group="major_supplier" item="typename_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.ms_percentage_share')+'</b></div><div class="col-md-6">'+value.supplier_share_sales+'%<span class="investigator-control" group_name="Major Supplier/s" label="% Share of Total Purchases" value="'+value.supplier_share_sales+'" group="major_supplier" item="supplier_share_sales_'+value.majorsupplierid+'"></span></div><div class="col-md-6"><b>'+trans('business_details2.ms_frequency')+'</b></div><div class="col-md-6">'+convert('business_details2', value.supplier_order_frequency)+'<span class="investigator-control" group_name="Major Supplier/s" label="Payment Frequency" value="'+convert('business_details2', value.supplier_order_frequency)+'" group="major_supplier" item="supplier_order_frequency_'+value.majorsupplierid+'"></span></div><div class="col-md-12">'+showEdit+'</div></span>'); 
            }else{
              showResult.append('<input type="hidden" value="'+value.supplier_share_sales+'" name="totalsuppliersalevalue" class="totalsuppliersalevalue"><input type="hidden" value="'+value.supplier_experience+'" name="sexperience" class="texperience"><span class="details-row"><div class="col-md-6 col-sm-6"><b>Name</b></div><div class="col-md-6 col-sm-6">'+value.supplier_name+'<span class="investigator-control" group="major_supplier" item="supplier_name_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>Address</b></div><div class="col-md-6 col-sm-6">'+value.supplier_address+'<span class="investigator-control" group="major_supplier" item="supplier_address_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Person</b></div><div class="col-md-6 col-sm-6">'+value.supplier_contact_person+'<span class="investigator-control" group="major_supplier" item="supplier_contact_person_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Phone Number</b></div><div class="col-md-6 col-sm-6">'+value.supplier_phone+'<span class="investigator-control" group="major_supplier" item="supplier_phone_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>Contact Email</b></div><div class="col-md-6 col-sm-6">'+value.supplier_email+'<span class="investigator-control" group="major_supplier" item="supplier_email_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.ms_year_started')+'</b></div><div class="col-md-6 col-sm-6">'+value.supplier_started_years+'<span class="investigator-control" group="major_supplier" item="supplier_started_years_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.ms_year_doing_business')+'</b></div><div class="col-md-6 col-sm-6">'+value.supplier_years_doing_business+'<span class="investigator-control" group="major_supplier" item="supplier_years_doing_business_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.ms_payment')+'</b></div><div class="col-md-6 col-sm-6">'+typename+'<span class="investigator-control" group="major_supplier" item="typename_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.ms_percentage_share')+'</b></div><div class="col-md-6 col-sm-6">'+value.supplier_share_sales+'%<span class="investigator-control" group="major_supplier" item="supplier_share_sales_'+value.majorsupplierid+'"></span></div><div class="col-md-6 col-sm-6"><b>'+trans('business_details2.ms_frequency')+'</b></div><div class="col-md-6 col-sm-6">'+convert('business_details2', value.supplier_order_frequency)+'<span class="investigator-control" group="major_supplier" item="supplier_order_frequency_'+value.majorsupplierid+'"></span></div><div class="col-md-12">'+showEdit+'</div></span>');             
            }
          });
		  Investigator.initGroup('major_supplier');
  }); 

};
var competitorLoad = function(showto, id) {

  $.get(BaseURL + "/api/competitor", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3 col-sm-3"><b>Name</b></div><div class="col-md-3 col-sm-3"><b>Address</b></div><div class="col-md-3 col-sm-3"><b> Contact Phone Number</b></div><div class="col-md-3 col-sm-3"><b> Contact Email</b></div></span>'); 
          $.each(data, function(key, value) {
            
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/competitor/'+value.competitorid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/competitor/'+value.competitorid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }           
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-3 col-sm-3">'+showEdit+''+value.competitor_name+'</div><div class="col-md-3 col-sm-3">'+value.competitor_address+'</div><div class="col-md-3 col-sm-3">'+value.competitor_phone+'</div><div class="col-md-3 col-sm-3">'+value.competitor_email+'<span class="investigator-control" group_name="Competitors"  label="Name" value="'+value.competitor_name+'" group="competitor" item="competitor_'+value.competitorid+'"></span></div></span>'); 
			
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-3 col-sm-3"><b class="visible-sm-block visible-xs-block">Name<br /></b>'+value.competitor_name+'</div><div class="col-md-3 col-sm-3"><b class="visible-sm-block visible-xs-block">Address<br /></b>'+value.competitor_address+'</div><div class="col-md-3 col-sm-3"><b class="visible-sm-block visible-xs-block">Contact Phone Number<br /></b>'+value.competitor_phone+'</div><div class="col-md-3 col-sm-3"><b class="visible-sm-block visible-xs-block">Contact Email<br /></b>'+value.competitor_email+'<span class="investigator-control" group_name="Competitors"  label="Name" value="'+value.competitor_name+'" group="competitor" item="competitor_'+value.competitorid+'"></span></div><div class="col-xs-12 visible-xs-block visible-sm-block">'+showEdit+'</div></span>'); 
          });
        }
		Investigator.initGroup('competitor');
  }); 

};
var pastprojectscompletedLoad = function(showto, id) {

  $.get(BaseURL + "/api/pastprojectscompleted", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-4 col-sm-4"><b>'+trans('business_details2.ppc_purpose')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.ppc_cost_short')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.ppc_source_short')+'</b></div><div class="col-md-1 col-sm-1"><b>'+trans('business_details2.ppc_year_short')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.ppc_result_short')+'</b></div></span>'); 
          $.each(data, function(key, value) {

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/pastprojectscompleted/'+value.projectcompletedid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/pastprojectscompleted/'+value.projectcompletedid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-4 col-sm-4">'+showEdit+''+value.projectcompleted_name+'</div><div class="col-md-2 col-sm-2">'+value.projectcompleted_cost+'</div><div class="col-md-2 col-sm-2">'+convert('business_details2', value.projectcompleted_source_funding)+'</div><div class="col-md-1 col-sm-1">'+value.projectcompleted_year_began+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2', value.projectcompleted_result)+'<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="'+value.projectcompleted_name+'" group="past_projects" item="past_project_'+value.projectcompletedid+'"></span></div></span>'); 
			
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-5"><b>'+trans('business_details2.ppc_purpose')+'</b><br/>'+value.projectcompleted_name+'</div><div class="col-md-1"><b>'+trans('business_details2.ppc_cost')+'</b><br/>'+value.projectcompleted_cost+'</div><div class="col-md-2"><b>'+trans('business_details2.ppc_source')+'</b><br/>'+convert('business_details2', value.projectcompleted_source_funding)+'</div><div class="col-md-1"><b>'+trans('business_details2.ppc_year')+'</b><br/>'+value.projectcompleted_year_began+'</div><div class="col-md-3"><b>'+trans('business_details2.ppc_result')+'</b><br/>'+convert('business_details2', value.projectcompleted_result)+'<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="'+value.projectcompleted_name+'" group="past_projects" item="past_project_'+value.projectcompletedid+'"></span></div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
		  Investigator.initGroup('past_projects');
        }
  }); 

};
var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var futuregrowthinitiativeLoad = function(showto, id) {

  $.get(BaseURL + "/api/futuregrowthinitiative", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.fgi_purpose')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_cost')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_date')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_source')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.fgi_proj_goal')+'</b></div></span>'); 
          $.each(data, function(key, value) {
            
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/futuregrowthinitiative/'+value.futuregrowthid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/futuregrowthinitiative/'+value.futuregrowthid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-3 col-sm-3">'+showEdit+''+value.futuregrowth_name+'</div><div class="col-md-2 col-sm-2">'+value.futuregrowth_estimated_cost+'</div><div class="col-md-2 col-sm-2">'+value.futuregrowth_implementation_date+'</div><div class="col-md-2 col-sm-2">'+convert('business_details2',value.futuregrowth_source_capital)+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2',value.planned_proj_goal)+' by '+value.planned_goal_increase+'% beginning '+ monthNames[parseInt(value.proj_benefit_date.substr(5, 2)) - 1] +' '+ value.proj_benefit_date.substr(0, 4) +' </div></span>'); 
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-5"><b>'+trans('business_details2.fgi_purpose')+'</b><br/>'+value.futuregrowth_name+'</div><div class="col-md-2"><b>'+trans('business_details2.fgi_cost')+'</b><br/>'+value.futuregrowth_estimated_cost+'</div><div class="col-md-2"><b>'+trans('business_details2.fgi_date')+'</b><br/>'+value.futuregrowth_implementation_date+'</div><div class="col-md-3"><b>'+trans('business_details2.fgi_source')+'</b><br/>'+convert('business_details2',value.futuregrowth_source_capital)+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2',value.planned_proj_goal)+' by '+value.planned_goal_increase+'% beginning '+ monthNames[parseInt(value.proj_benefit_date.substr(5, 2)) - 1] +' '+ value.proj_benefit_date.substr(0, 4) +' </div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
        }
  }); 

};
var riskassessmentLoad = function(showto, id) {

  $.get(BaseURL + "/api/riskassessment", { id: $('#'+id+'').val() }, 
    function(data) {
      var x = 1;
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          $.each(data, function(key, value) {
            var i = x++;
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() != 2){
                var showEdit = '<a href = "' + BaseURL + '/sme/riskassessment/'+value.riskassessmentid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/riskassessment/'+value.riskassessmentid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }           
            }else{
              var showEdit = '';
            }

            showResult.append('<input type="hidden" class="ra-class" value="'+data.length+'"><span class="details-row"><div class="col-md-6 col-sm-6">'+showEdit+' <b>'+trans('condition_sustainability.threat')+'</b> #'+i+' '+convert('condition_sustainability', value.risk_assessment_name)+'</div><div class="col-md-6 col-sm-6"> <b>'+trans('condition_sustainability.counter_measures')+'</b> #'+i+' '+convert('condition_sustainability', value.risk_assessment_solution)+'</div></span>'); 
          });

        }
        
  }); 

};
var majorlocationLoad = function(showto, id) {

  $.get(BaseURL + "/api/majorlocation", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
          $.each(data, function(key, value) {
            
            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/majorlocation/'+value.marketlocationid+'/edit">'+trans('messages.edit')+'</a> | <a href = "' + BaseURL + '/sme/majorlocation/'+value.marketlocationid+'/delete">'+trans('messages.delete')+'</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

            showResult.append('<span class="details-row"><div class="col-md-12">'+showEdit+''+value.city+'<span class="investigator-control" group_name="Major Market Location"  label="Major Market Location" value="'+value.city+'" group="major_market_location" item="location_'+value.marketlocationid+'"></span></div></span>'); 
          });
		  Investigator.initGroup('major_market_location');
  }); 

};
var spouseofownerResult = function(showto, id, entity_id) {

  $.get(BaseURL + "/api/spouseofowner", { id: id, entity_id: entity_id }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        $.each(data, function(key, value) {

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a class="navbar-link" href="' + BaseURL + '/sme/spouse/'+value.spouseid+'/edit">Edit Spouse Personal Details</a>';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

          showResult.append('<input id="spouseid_'+id+'" type="hidden" value="'+value.spouseid+'" name="spouseid" class="spouseid"><span class="details-row"><div class="col-md-3 col-sm-3"><b>First Name</b></div><div class="col-md-3 col-sm-3"><b>Middle Name</b></div><div class="col-md-3 col-sm-3"><b>Last Name</b></div><div class="col-md-3 col-sm-3"><b>Birthdate</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.firstname+'</div><div class="col-md-3 col-sm-3">'+value.middlename+'</div><div class="col-md-3 col-sm-3">'+value.lastname+'</div><div class="col-md-3 col-sm-3">'+value.birthdate+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>Nationality</b></div><div class="col-md-3 col-sm-3"><b>Profession</b></div><div class="col-md-3 col-sm-3"><b>Phone Number</b></div><div class="col-md-3 col-sm-3"><b>TIN</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.nationality+'</div><div class="col-md-3 col-sm-3">'+value.profession+'</div><div class="col-md-3 col-sm-3">'+value.phone+'</div><div class="col-md-3 col-sm-3">'+value.tin_num+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>E-mail Address</b></div><div class="col-md-3 col-sm-3">'+value.email+'</div></span><span style="text-align: right; border: 0px;" class="details-row">'+showEdit+'</span>'); 
        });
  }); 

};
var businessManageResult = function(showto, id, entity_id) {

  $.get(BaseURL + "/api/businessmanage", { id: id, entity_id: entity_id }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
      
        if($('#status').val() == 0 || $('#status').val() == 3){
          if($('#userid').val() == 1){
            showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4"><b>Name</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_type')+'</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_location')+'</b></div></span>'); 
            $.each(data, function(key, value) {
              showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4"><a href = "' + BaseURL + '/sme/managebus/'+value.managebusid+'/edit">'+trans('messages.edit')+'</a> '+value.bus_name+'</div><div class="col-md-4 col-sm-4">'+value.bus_type+'</div><div class="col-md-4 col-sm-4">'+value.city+'</div></span>'); 
            });
            showResult.append('<span class="details-row" style="text-align: right;"><a href="' + BaseURL + '/sme/managebus/'+id+'" class="navbar-link">'+trans('messages.add')+' '+trans('owner_details.other_business')+'</a></span>');
          }else{
            showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4"><b>Name</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_type')+'</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_location')+'</b></div></span>'); 
            $.each(data, function(key, value) {
              showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4">'+value.bus_name+'</div><div class="col-md-4 col-sm-4">'+value.bus_type+'</div><div class="col-md-4 col-sm-4">'+value.city+'</div></span>'); 
            });
          }
        }else{
          showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4"><b>Name</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_type')+'</b></div><div class="col-md-4 col-sm-4"><b>'+trans('owner_details.business_location')+'</b></div></span>'); 
          $.each(data, function(key, value) {
            showResult.append('<span class="details-row"><div class="col-md-4 col-sm-4">'+value.bus_name+'</div><div class="col-md-4 col-sm-4">'+value.bus_type+'</div><div class="col-md-4 col-sm-4">'+value.city+'</div></span>'); 
          });        
        }

  }); 

};
var keyManagersResult = function(showto, id, entity_id) {

  $.get(BaseURL + "/api/keymanage", { id: id, entity_id: entity_id }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        $.each(data, function(key, value) {

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a class="navbar-link" href="' + BaseURL + '/sme/keymanager/'+value.keymanagerid+'/edit">Edit Key Manager</a> | <a class="navbar-link" href="' + BaseURL + '/sme/keymanager/'+value.keymanagerid+'/delete">Delete</a>';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

          showResult.append('<span class="details-row"><div class="col-md-3 col-sm-3"><b>First Name</b></div><div class="col-md-3 col-sm-3"><b>Middle Name</b></div><div class="col-md-3 col-sm-3"><b>Last Name</b></div><div class="col-md-3 col-sm-3"><b>Birthdate</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.firstname+'</div><div class="col-md-3 col-sm-3">'+value.middlename+'</div><div class="col-md-3 col-sm-3">'+value.lastname+'</div><div class="col-md-3 col-sm-3">'+value.birthdate+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>Nationality</b></div><div class="col-md-3 col-sm-3"><b>Civil Status</b></div><div class="col-md-3 col-sm-3"><b>Profession</b></div><div class="col-md-3 col-sm-3"><b>Email Address</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.nationality+'</div><div class="col-md-3 col-sm-3">'+value.civilstatus+'</div><div class="col-md-3 col-sm-3">'+value.profession+'</div><div class="col-md-3 col-sm-3">'+value.email+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>Address Line 1</b></div><div class="col-md-3 col-sm-3"><b>Address Line 2</b></div><div class="col-md-3 col-sm-3"><b>City</b></div><div class="col-md-3 col-sm-3"><b>Province</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.address1+'</div><div class="col-md-3 col-sm-3">'+value.address2+'</div><div class="col-md-3 col-sm-3">'+value.city_name+'</div><div class="col-md-3 col-sm-3">'+value.province_name+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>Zip Code</b></div><div class="col-md-3 col-sm-3"><b>No of Years in Present Address</b></div><div class="col-md-3 col-sm-3"><b>Phone Number</b></div><div class="col-md-3 col-sm-3"><b>TIN</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.zipcode+'</div><div class="col-md-3 col-sm-3">'+value.no_yrs_present_address+'</div><div class="col-md-3 col-sm-3">'+value.phone+'</div><div class="col-md-3 col-sm-3">'+value.tin_num+'</div></span><span class="details-row"><div class="col-md-3 col-sm-3"><b>% Ownership of Business</b></div><div class="col-md-3 col-sm-3"><b>Years of experience in this industry</b></div><div class="col-md-3 col-sm-3"><b>Position</b></div></span><span class="details-row"><div class="col-md-3 col-sm-3">'+value.percent_of_ownership+'</div><div class="col-md-3 col-sm-3">'+value.number_of_year_engaged+'</div><div class="col-md-3 col-sm-3">'+value.position+'</div><div class="col-md-3 col-sm-3">'+showEdit+'</div></span><span class="details-row">&nbsp;</span>'); 
        });
		if($('#status').val() == 0 || $('#status').val() == 3){
          if($('#userid').val() == 1){
            showResult.append('<span class="details-row" style="text-align: right;"><a href="' + BaseURL + '/sme/keymanager/'+id+'" class="navbar-link">Add New Key Manager</a></span>');
          }
        }
  }); 

};
var educationResult = function(showto, id, entity_id) {

  $.get(BaseURL + "/api/education", { id: id, entity_id: entity_id }, 
    function(data) {
      var showEducation = $('.'+showto+'');
        showEducation.empty();
        $.each(data, function(key, value) {
          if(value.educ_type == 1){
            var typename = 'High School';
          }else if(value.educ_type == 2){
            var typename = 'College';
          }else if(value.educ_type == 3){
            var typename = 'Post Graduate';
          }else{
            var typename = 'Others';
          }

            if($('#status').val() == 0 || $('#status').val() == 3){
              if($('#userid').val() == 1){
                var showEdit = '<a href = "' + BaseURL + '/sme/education/'+value.educationid+'/edit">Edit</a> ';
              }else{
                var showEdit = '';
              }
            }else{
              var showEdit = '';
            }

          showEducation.append('<span class="details-row"><div class="col-md-6 col-sm-6"><b>'+typename+': Name</b></div><div class="col-md-6 col-sm-6"><b>'+typename+': Degree / Level / Qualification</b></div><div class="col-md-6 col-sm-6">'+showEdit+''+value.school_name+'</div><div class="col-md-6 col-sm-6">'+value.educ_degree+'</div></span>'); 
        });
        if($('#status').val() == 0 || $('#status').val() == 3){
          if($('#userid').val() == 1){
            showEducation.append('<span class="details-row" style="text-align: right;"><a href="' + BaseURL + '/sme/education/'+id+'" class="navbar-link">Add Education</a></span>');
          }
        }
  }); 

};
function showkeymanagers(showto, id){
  var entity_id = $('#entityids').val();
  keyManagersResult(showto, id, entity_id);
}
function showbusinessmanage(showto, id){
  var entity_id = $('#entityids').val();
  businessManageResult(showto, id, entity_id);
}
function showspousebusinessmanage(showto, id){
  var entity_id = $('#entityids').val();
  var spouseidGet = $('#spouseid_'+id+'').val();
  businessManageResult(showto, spouseidGet, entity_id);
}
function showeducation(showto, id){
  var entity_id = $('#entityids').val();
  educationResult(showto, id, entity_id);
}
function showspouseeducation(showto, id){
  var spouseidGet = $('#spouseid_'+id+'').val();
  var entity_id = $('#entityids').val();
  educationResult(showto, spouseidGet, entity_id);
}
function showspouse(showto, id){
  var entity_id = $('#entityids').val();
  spouseofownerResult(showto, id, entity_id);
}
