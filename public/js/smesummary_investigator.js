//======================================================================
//  Script S53: smesummary_investigator.js
//      Scripts for investigator processes
//======================================================================
var Investigator = {
	
	tags : [
		'business_details',
		'related_company',
		'services_offer',
		'capital_details',
		'certifications',
		'import_export',
		'insurance_coverage',
		'documents',
		'capacity_expansion',
		'main_location',
		'branches',
		'succession',
		'tax_payments',
		'plan_facility',
		'plan_facility_requested',
		'owner',
		'shareholders',
		'directors',
        'major_customer',
        'major_supplier',
        'major_market_location',
        'competitor',
        'past_projects',
	],
	
	initAll : function(){
		for(x in Investigator.tags){
			Investigator.initGroup(Investigator.tags[x]);
		}
		Investigator.bindActions();
	},
	
	initGroup : function(sGroup){
		$.ajax({
			url: BaseURL + '/investigator/get_group',
			dataType: 'json',
			data: {
				entity_id: $('#entityids').val(),
				group: sGroup
			},
			success: function(oReturn){
				$('.investigator-control[group='+sGroup+']').each(function(){
					var sItem = $(this).attr('item');
					var oData = oReturn[sItem];
					Investigator.initItem(sGroup, sItem, oData);
				});
			}
		});
	},
	
	initItem: function(sGroup, sItem, oData){
		var uiThis = $('.investigator-control[group='+sGroup+'][item='+sItem+']');		
		var sHtml = '';
		if($('#userid').val()=='2' && $('#status').val()=='7'){
			if($('#done_with_investigation').length == 0){
				sHtml += '<a href="#" class="investigator-verify-btn">correct</a> | ' +
						'<a href="#" class="investigator-incomplete-btn">incorrect</a>&nbsp;&nbsp;&nbsp;';
			}
		}
		if(oData && oData.status=="verified"){
			sHtml += '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span><span class="sr-only">verified</span>';
		} 
		else if(oData && oData.status=="incomplete") {
			sHtml += '<a href="#" class="investigator-note-btn"><span class="glyphicon glyphicon-alert" aria-hidden="true"></span><span class="sr-only">incomplete!</span></a>' +
						'<span class="investigator-note">' + oData.notes + '</span>';
		}
		uiThis.html(sHtml);
	},
	
	bindActions: function(){
		$('body').on('click', '.investigator-note-btn', function(e){
			e.preventDefault();
			var uiThis = $(this);
			$('span.investigator-note').hide();
			uiThis.next('span.investigator-note').show();
		});
		
		$(document).on('click', function(e) { 
			if(!$(e.target).closest('.investigator-control').length) {
				$('span.investigator-note').hide();
			}        
		});
		
		$('body').on('click', '.investigator-verify-btn', function(e){
			e.preventDefault();
			var uiParent = $(this).parent();
			$.ajax({
				url: BaseURL + '/investigator/post_data',
				type: 'post',
				dataType: 'json',
				data: {
					entity_id: $('#entityids').val(),
					group: uiParent.attr('group'),
					item: uiParent.attr('item'),
					group_name: uiParent.attr('group_name'),
					label: uiParent.attr('label'),
					value: uiParent.attr('value'),
					status: 'verified'
				},
				beforeSend: function(){
					uiParent.html('<img src="'+BaseURL+'/images/preloader.gif" />');
				},
				success: function(oReturn){
					Investigator.initItem(uiParent.attr('group'), uiParent.attr('item'), oReturn);
				}
			});
		});
		
		$('body').on('click', '.investigator-incomplete-btn', function(e){
			e.preventDefault();
			var uiParent = $(this).parent();
			var uiModal = $('#modal_investigator_item');
			uiModal.find('input.group').val(uiParent.attr('group'));
			uiModal.find('input.item').val(uiParent.attr('item'));
			uiModal.find('input.group_name').val(uiParent.attr('group_name'));
			uiModal.find('input.label').val(uiParent.attr('label'));
			uiModal.find('input.value').val(uiParent.attr('value'));
			uiModal.find('textarea.notes').val('');
            uiModal.find('#ci_err_msg').hide();
            uiModal.find('.investigator-modal-save-btn').text('Save');
            uiModal.find('.close-ci-modal').show();
			uiModal.modal({
                keyboard: false,
                backdrop: 'static',
            }).modal('show');
			
		});
		
		$('.investigator-modal-save-btn').on('click', function(e){
			e.preventDefault();
            
            var notes       = $('.notes');
            var prompt      = $('#ci_err_msg');
            var close_btn   = $('.close-ci-modal');
            var sts         = 0;
            var message     = '';
            
            if ('' == notes.val()) {
                message = 'Notes is required';
                prompt.show();
            }
            else {
                prompt.hide();
                sts = 1;
            }
            
            prompt.html(message);
            
            if (1 == sts) {
                var uiModal = $('#modal_investigator_item');
                $('#modal_investigator_item_form').ajaxSubmit({
                    url: BaseURL + '/investigator/post_data',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        entity_id: $('#entityids').val(),
                        group: uiModal.find('input.group').val(),
                        item: uiModal.find('input.item').val(),
                        group_name: uiModal.find('input.group_name').val(),
                        label: uiModal.find('input.label').val(),
                        value: uiModal.find('input.value').val(),
                        status: 'incomplete',
                        notes: uiModal.find('textarea.notes').val()
                    },
                    beforeSend: function(){
                        $('.investigator-modal-save-btn').text('Saving files...');
                        close_btn.hide();
                        $('.investigator-control[group='+uiModal.find('input.group').val()+'][item='+uiModal.find('input.item').val()+']').html('<img src="'+BaseURL+'/images/preloader.gif" />');
                    },
                    success: function(oReturn){
                        Investigator.initItem(uiModal.find('input.group').val(), uiModal.find('input.item').val(), oReturn);
                        uiModal.modal('hide');
                    }
                });
            }
		});
		
		$('#nav_investigationsummary').on('click', function(){
			Investigator.fillSummary();
		});
	},
	
	fillSummary: function(){
		$.ajax({
			url: BaseURL + '/investigator/get_all_failed',
			dataType: 'json',
			type: 'get',
			data: {
				entity_id: $('#entityids').val()
			},
			beforeSend: function(){
				$('.investigator-summary').html('Loading... <img src="'+BaseURL+'/images/preloader.gif" />');
			},
			success: function(oReturn){
				var sHtml = '<table class="table table-striped table-bordered" cellspacing="0">';
				sHtml += '<tr><th width="15%">Group</th><th width="20%">Item/Label</th><th width="20%">Value</th><th>Documentation</th></tr>';
				for(x in oReturn){
					var oData = oReturn[x];
					sHtml += '<tr><td>' + oData.group_name + '</td>';
					sHtml += '<td>' + oData.label + '</td>';
					sHtml += '<td>' + oData.value + '</td>';
					sHtml += '<td><span class="investigator_blurb">' + oData.blurb + '</span> ';
					sHtml += '<a href="#" class="investigator_file_expand"><span class="pull-right glyphicon glyphicon-chevron-down"></span></a><div class="investigator_file_container">';
						sHtml += oData.notes + '<br><br>';
					if(oData.files.length > 0){
						for(x in oData.files){
							sHtml += '<a href="'+BaseURL+'/download/cidocuments/'+oData.files[x].filename+'">' + oData.files[x].old_filename + '</a><br>';
						}
					}
					sHtml += '</div></td></tr>';
				}
				$('.investigator-summary').html(sHtml);
				$('.investigator_file_expand').on('click', function(e){
					e.preventDefault();
					if($(this).prev('span.investigator_blurb').is(':visible')){
						$(this).prev('span.investigator_blurb').hide();
						$(this).next('div.investigator_file_container').show();
						$(this).find('span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
					} else {
						$(this).prev('span.investigator_blurb').show();
						$(this).next('div.investigator_file_container').hide();
						$(this).find('span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
					}
				});
			}
		});
	}
};

$(document).ready(function(){
	Investigator.initAll();
	
	$('#iv_send_to_email').click(function(e){
		e.preventDefault();
		if($('.investigator-docs input[type=checkbox]:checked').length == 0){
			$('#iv_confirm_modal').modal();
		} else {
			$('#iv_action').val('send_to_email');
			$('#iv_form').submit();
		}
	});
	
	$('#iv_confirm').click(function(e){
		e.preventDefault();
		$('#iv_action').val('send_to_email');
		$('#iv_form').submit();
	});
	
	$('#iv_submit_investigation').click(function(e){
		e.preventDefault();
		$('#iv_action').val('submit_investigation');
		$('#iv_form').submit();
	});
});