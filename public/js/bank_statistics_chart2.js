//======================================================================
//  Script S5: bank_statistics_chart2.js
//      Used for creating Average Composite and Final Rating graph
//======================================================================
function Chart2(container, ChartData){
    container.highcharts({
        chart: {
            type: 'bar',
			marginLeft: 200,
        },
        title: {
            text: 'Average Composite and Final Rating'
        },
        xAxis: {
            categories: ChartData.categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Score',
                align: 'high'
            },
			max: 250
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [
			{
				name: 'Score',
				data: ChartData.series
			}
		]
    });
};