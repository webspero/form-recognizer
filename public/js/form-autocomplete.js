//======================================================================
//  Script S31: form-automplete.js
//      Form Autocompletion Override
//======================================================================

$(document).ready(function() {

var AUTOC_DISABLED = 'off';
var AUTOC_ENABLED  = 'on';

/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initFormAutocomplete()
{
   overrideForms(AUTOC_DISABLED);
}

/**------------------------------------------------------------------------
|	Overrides the Form's Autocomplete function
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function overrideForms(sts)
{
    //-----------------------------------------------------
    //  Binding S31.1: form
    //      Turns OFF the autocomplete function of forms upon loading
    //-----------------------------------------------------
    $('form').attr('autocomplete', sts);
}

initFormAutocomplete();

});
