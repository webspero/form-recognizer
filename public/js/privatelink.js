//======================================================================
//  Script S47: privatelink.js
//      Js helper for private link items via AJAX
//======================================================================
var businessdriverLoad = function(showto, id) {

  $.get(BaseURL + "/api/businessdriver_pl", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-4 col-sm-4"><b>'+trans('business_details2.bdriver_name')+'</b></div><div class="col-md-8 col-sm-8"><b>'+trans('business_details2.bdriver_share')+'</b></div></span>'); 
          $.each(data, function(key, value) {

            var showEdit = '';

            showResult.append('<span class="details-row hidden-xs hidden-sm"><div class="col-md-4 col-sm-4">'+showEdit+''+value.businessdriver_name+'</div><div class="col-md-8 col-sm-8">'+value.businessdriver_total_sales+'%</div></span>'); 
			
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-4"><b>'+trans('business_details2.bdriver_name')+'<br/></b>'+value.businessdriver_name+'</div><div class="col-md-8"><b>'+trans('business_details2.bdriver_share')+'</b><br />'+value.businessdriver_total_sales+'%</div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
        }
  }); 

};
var businesssegmentLoad = function(showto, id) {

  $.get(BaseURL + "/api/businesssegment_pl", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          $.each(data, function(key, value) {
            
            var showEdit = '';

            showResult.append('<span class="details-row"><div class="col-md-12">'+showEdit+''+value.businesssegment_name+'</div></span>'); 
          });
        }
  }); 

};
var pastprojectscompletedLoad = function(showto, id) {

  $.get(BaseURL + "/api/pastprojectscompleted_pl", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-4 col-sm-4"><b>'+trans('business_details2.ppc_purpose')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.ppc_cost_short')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.ppc_source_short')+'</b></div><div class="col-md-1 col-sm-1"><b>'+trans('business_details2.ppc_year_short')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.ppc_result_short')+'</b></div></span>'); 
          $.each(data, function(key, value) {

            var showEdit = '';

            showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-4 col-sm-4">'+showEdit+''+value.projectcompleted_name+'</div><div class="col-md-2 col-sm-2">'+value.projectcompleted_cost+'</div><div class="col-md-2 col-sm-2">'+convert('business_details2', value.projectcompleted_source_funding)+'</div><div class="col-md-1 col-sm-1">'+value.projectcompleted_year_began+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2', value.projectcompleted_result)+'<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="'+value.projectcompleted_name+'" group="past_projects" item="past_project_'+value.projectcompletedid+'"></span></div></span>'); 
			
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-5"><b>'+trans('business_details2.ppc_purpose')+'</b><br/>'+value.projectcompleted_name+'</div><div class="col-md-1"><b>'+trans('business_details2.ppc_cost')+'</b><br/>'+value.projectcompleted_cost+'</div><div class="col-md-2"><b>'+trans('business_details2.ppc_source')+'</b><br/>'+convert('business_details2', value.projectcompleted_source_funding)+'</div><div class="col-md-1"><b>'+trans('business_details2.ppc_year')+'</b><br/>'+value.projectcompleted_year_began+'</div><div class="col-md-3"><b>'+trans('business_details2.ppc_result')+'</b><br/>'+convert('business_details2', value.projectcompleted_result)+'<span class="investigator-control" group_name="Past Projects Completed"  label="Purpose of Project" value="'+value.projectcompleted_name+'" group="past_projects" item="past_project_'+value.projectcompletedid+'"></span></div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
		  
        }
  }); 

};
var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var futuregrowthinitiativeLoad = function(showto, id) {

  $.get(BaseURL + "/api/futuregrowthinitiative_pl", { id: $('#'+id+'').val() }, 
    function(data) {
      var showResult = $('.'+showto+'');
        showResult.empty();
        if(data.length != 0){
          showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.fgi_purpose')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_cost')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_date')+'</b></div><div class="col-md-2 col-sm-2"><b>'+trans('business_details2.fgi_source')+'</b></div><div class="col-md-3 col-sm-3"><b>'+trans('business_details2.fgi_proj_goal')+'</b></div></span>'); 
          $.each(data, function(key, value) {
            
            var showEdit = '';

            showResult.append('<span class="details-row hidden-sm hidden-xs"><div class="col-md-3 col-sm-3">'+showEdit+''+value.futuregrowth_name+'</div><div class="col-md-2 col-sm-2">'+value.futuregrowth_estimated_cost+'</div><div class="col-md-2 col-sm-2">'+value.futuregrowth_implementation_date+'</div><div class="col-md-2 col-sm-2">'+convert('business_details2',value.futuregrowth_source_capital)+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2',value.planned_proj_goal)+' by '+value.planned_goal_increase+'% beginning '+ monthNames[parseInt(value.proj_benefit_date.substr(5, 2)) - 1] +' '+ value.proj_benefit_date.substr(0, 4) +' </div></span>'); 
			//mobile
			showResult.append('<span class="details-row visible-sm-block visible-xs-block"><div class="col-md-5"><b>'+trans('business_details2.fgi_purpose')+'</b><br/>'+value.futuregrowth_name+'</div><div class="col-md-2"><b>'+trans('business_details2.fgi_cost')+'</b><br/>'+value.futuregrowth_estimated_cost+'</div><div class="col-md-2"><b>'+trans('business_details2.fgi_date')+'</b><br/>'+value.futuregrowth_implementation_date+'</div><div class="col-md-3"><b>'+trans('business_details2.fgi_source')+'</b><br/>'+convert('business_details2',value.futuregrowth_source_capital)+'</div><div class="col-md-3 col-sm-3">'+convert('business_details2',value.planned_proj_goal)+' by '+value.planned_goal_increase+'% beginning '+ monthNames[parseInt(value.proj_benefit_date.substr(5, 2)) - 1] +' '+ value.proj_benefit_date.substr(0, 4) +' </div><div class="col-xs-12">'+showEdit+'</div></span>'); 
          });
        }
  }); 

};

$(document).ready(function() {
	
	$('ul#side-menu li a').click(function(){
		var uiThis = $(this);
		$('ul#side-menu li a').removeClass('active');
		uiThis.addClass('active');
	});
    
	$('#sscore_prev').click(function() {
        $('#sscore').click();
    });
    
    $('#sscore_next').click(function() {
		$('#nav_strategicprofile').click();
    });
	
	$('#finananalysis_next').click(function() {
        $('#finananalysis').click();
    });
	
	$('#strategicprofile_prev').click(function() {
        $('#finananalysis').click();
    });
   

	businessdriverLoad("businessdriver", "entityids");
	businesssegmentLoad("businesssegment", "entityids");
	pastprojectscompletedLoad("pastprojectscompleted", "entityids");
	//futuregrowthinitiativeLoad("futuregrowthinitiative", "entityids")
	reflow_charts();
});

window.onload = function(){
	reflow_charts();
};
setInterval(function(){ reflow_charts(); }, 1000);

function reflow_charts(){
	if(Highcharts){
		for(x in Highcharts.charts){
			Highcharts.charts[x].reflow();
		}
	}
}