//======================================================================
//  Script S61: upd-biz2.js
//      Adding and Editing of Business Details 2
//======================================================================

$(document).ready(function(){

/**------------------------------------------------------------------------
|	Binds the Major Customer link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMajorCustomerForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-major-cust-form';
    var upd_form    = '#major-cust-expand-form';
    var upd_btn     = '#show-major-cust-expand';
    
    bindCustomerYears(1);
    
    //-----------------------------------------------------
    //  Binding S61.1: #show-major-cust-expand
    //      Launch the Expander Form for Major Customer
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Major Supplier link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMajorSupplierForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-major-supp-form';
    var upd_form    = '#major-supp-expand-form';
    var upd_btn     = '#show-major-supp-expand';
    
    bindCustomerYears(2);
    
    //-----------------------------------------------------
    //  Binding S61.2: #show-major-supp-expand
    //      Launch the Expander Form for Major Supplier
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Machine and Equipments link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMachineEquipmentForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-machine-equip-form';
    var upd_form    = '#machine-equip-expand-form';
    var upd_btn     = '#show-machine-equip-expand';
    
    //-----------------------------------------------------
    //  Binding S61.2: #show-major-supp-expand
    //      Launch the Expander Form for Major Supplier
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Major Location link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMajorLocationForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-major-loc-form';
    var upd_form    = '#major-loc-expand-form';
    var upd_btn     = '#show-major-loc-expand';
    
    //-----------------------------------------------------
    //  Binding S61.3: #show-major-loc-expand
    //      Launch the Expander Form for Major Location
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Competitors link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCompetitorForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-competitor-form';
    var upd_form    = '#competitor-expand-form';
    var upd_btn     = '#show-competitor-expand';
    
    //-----------------------------------------------------
    //  Binding S61.4: #show-competitor-expand
    //      Launch the Expander Form for Competitor
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Business Driver link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandBizDrvrForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-bizdriver-form';
    var upd_form    = '#bizdriver-expand-form';
    var upd_btn     = '#show-bizdriver-expand';
    
    //-----------------------------------------------------
    //  Binding S61.5: #show-bizdriver-expand
    //      Launch the Expander Form for Business Driver
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Customer Segment link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCustSegmentForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-bizsegment-form';
    var upd_form    = '#bizsegment-expand-form';
    var upd_btn     = '#show-bizsegment-expand';
    
    //-----------------------------------------------------
    //  Binding S61.6: #show-bizsegment-expand
    //      Launch the Expander Form for Business Segments
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Customer Segment link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandPastProjForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-past-proj-form';
    var upd_form    = '#past-proj-expand-form';
    var upd_btn     = '#show-past-proj-expand';
    
    //-----------------------------------------------------
    //  Binding S61.7: #show-past-proj-expand
    //      Launch the Expander Form for Past Projects
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Future Growth Initiative link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandFutureInitForm()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-future-growth-form';
    var upd_form    = '#future-growth-expand-form';
    var upd_btn     = '#show-future-growth-expand';
    
    //-----------------------------------------------------
    //  Binding S61.8: #show-future-growth-expand
    //      Launch the Expander Form for Future Initiative
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function expandInsurance2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-insurance2-form';
    var upd_form    = '#insurance-expand-form';
    var upd_btn     = '#show-insurance2-expand';
    
    //-----------------------------------------------------
    //  Binding S62.10: #show-insurance-expand
    //      Launch the Expander Form for Insurance
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function expandCertifications2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-certifications2-form';
    var upd_form    = '#certifications-expand-form';
    var upd_btn     = '#show-certifications2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.8: #show-certifications-expand
    //      Launch the Expander Form for Certifications
    //-----------------------------------------------------
    /** Launch the Add Major Customer Form */
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Main Locations link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandMainLoc2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-main-loc2-form';
    var upd_form    = '#main-loc2-expand-form';
    var upd_btn     = '#show-main-loc2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.4: #show-main-loc-expand
    //      Launch the Expander Form for Main Location
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Capital Details link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandCapDetails2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-cap-details2-form';
    var upd_form    = '#cap-details-expand-form';
    var upd_btn     = '#show-cap-details2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.3: #show-cap-details-expand
    //      Launch the Expander Form for Capital Details
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Affiliates / Subsidiaries link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandRelatedComp2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-affiliates2-form';
    var upd_form    = '#affiliates-premium-expand-form';
    var upd_btn     = '#show-affiliates2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.1: #show-affiliates-expand
    //      Launch the Expander Form for Affiliates
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function bindCustomerYears(type)
{
    if (1 == type) {
        var input_years     = '.customer_started_years';
        var input_years_id  = '#customer_started_years';
        var output_cntr     = '#customer_years_doing_business';
    }
    else {
        var input_years     = '.supplier_started_years';
        var input_years_id  = '#supplier_started_years';
        var output_cntr     = '#supplier_years_doing_business';
    }
    
    $("#registration2").on('blur', input_years, function() {
        var input_idx       = $(this).data('idnumber');
        var today           = new Date();
        var dateestablished = new Date($(input_years_id+input_idx+'').val());
        
		if(dateestablished.getFullYear() > today.getFullYear()){
			dateestablished = today;
			$(input_years_id+input_idx+'').val(today.getFullYear());
		}
		
		var age             = today.getFullYear() - dateestablished.getFullYear();
        var m               = today.getMonth() - dateestablished.getMonth();
        
        if (m < 0 || (m === 0 && today.getDate() < dateestablished.getDate())) {
            age--;
        }

        if (isNaN(age)) {
            age = 'Invalid Years';
        }
        
        $(output_cntr+input_idx+'').val(age);
    });
}


/**------------------------------------------------------------------------
|	Binds the Add Directors link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandDirectors2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-directors2-form';
    var upd_form    = '#directors-expand-form';
    var upd_btn     = '#show-directors2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.6: #show-directors-expand
    //      Launch the Expander Form for Directors
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Shareholders link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandShareholders2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-shareholders2-form';
    var upd_form    = '#shareholders-expand-form';
    var upd_btn     = '#show-shareholders2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.7: #show-shareholders-expand
    //      Launch the Expander Form for Shareholders
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

/**------------------------------------------------------------------------
|	Binds the Add Shareholders link to the Expander Form
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function expandKeymanager2Form()
{
    /** Variable Definition    */
    var tab_cntr    = $('#registration2');
    var expand_form = '#expand-keymanager2-form';
    var upd_form    = '#keymanager-expand-form';
    var upd_btn     = '#show-keymanager2-expand';
    
    //-----------------------------------------------------
    //  Binding S560.7: #show-shareholders-expand
    //      Launch the Expander Form for Shareholders
    //-----------------------------------------------------
    tab_cntr.on('click', upd_btn, function() {
        initExpandForm(expand_form, upd_form, $(this).attr('href'));
        return false;
    });
}

function checkNoMajorCustomer()
{
    //-----------------------------------------------------
    //  Binding S61.9: .no-major-customer
    //      Saves the Major Customer tickbox details to database
    //-----------------------------------------------------
    $('#major-cust-list-cntr').on('change', '.no-major-customer', function(){
        
        var ajax_url        = BaseURL+'/rest/update_major_account/customer';
        var refresh_cntr    = '.reload-no-major-cust';
        var refresh_url     = BaseURL+'/summary/smesummary/'+$('#entityids').val()+' '+refresh_cntr;
        var major_customer  = 0;
        
        $(this).prop('disabled', true);
        
        if ($('.no-major-customer:checked').length) {
            major_customer = 1;
        }
        
        $.post(ajax_url, {'no_major_customer': major_customer, 'entity_id': $('#entityids').val() }, function(){
            $('.no-major-cust-cntr').html('<img src = "'+BaseURL+'/images/preloader.gif" />');
            $('.no-major-cust-cntr').load(refresh_url, function() {
                update_progress_signs();
            }); 
        });
    });
}

function checkNoMajorSupplier()
{
    //-----------------------------------------------------
    //  Binding S61.10: .no-major-supplier
    //      Saves the Major Supplier tickbox details to database
    //-----------------------------------------------------
    $('#major-supp-list-cntr').on('change', '.no-major-supplier', function(){
        
        var ajax_url        = BaseURL+'/rest/update_major_account/supplier';
        var refresh_cntr    = '.reload-no-major-supp';
        var refresh_url     = BaseURL+'/summary/smesummary/'+$('#entityids').val()+' '+refresh_cntr;
        var major_supplier  = 0;
        
        $(this).prop('disabled', true);
        
        if ($('.no-major-supplier:checked').length) {
            major_supplier = 1;
        }
        
        $.post(ajax_url, {'no_major_supplier': major_supplier, 'entity_id': $('#entityids').val() }, function(){
            $('.no-major-supp-cntr').html('<img src = "'+BaseURL+'/images/preloader.gif" />');
            $('.no-major-supp-cntr').load(refresh_url, function(){
                update_progress_signs();
            });
        });
    });
}

expandMajorCustomerForm();
expandMajorSupplierForm();
expandMachineEquipmentForm();
expandMajorLocationForm();
expandCompetitorForm();
expandBizDrvrForm();
expandCustSegmentForm();
expandPastProjForm();
expandFutureInitForm();
checkNoMajorCustomer();
checkNoMajorSupplier();

//Added from business details 1 for premium report
expandInsurance2Form();
expandCertifications2Form();
expandMainLoc2Form();
expandCapDetails2Form();
expandRelatedComp2Form();
expandDirectors2Form();
expandShareholders2Form();
expandKeymanager2Form();
});