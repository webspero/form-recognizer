//======================================================================
//  Script S10: bsis_upload.js
//      Old js file for balance sheet and income statements uploading
//======================================================================
$(document).ready(function(){
	$('.datepicker').datepicker({
		dateFormat: 'mm/dd/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '-30:+0',
		onChangeMonthYear: function(y, m, i) {
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m - 1, d));
		}
	});
	
	$('#upload-file-container').on('change', '.upload_type', function(){
		if ($(this).val()==1) {
			$(this).parent().parent().find('.is_add').hide();
			$(this).parent().parent().find('.bs_add').css('display', 'inline-block');
		} 
        else if ($(this).val()==5) {
            $(this).parent().parent().find('.is_add').hide();
			$(this).parent().parent().find('.bs_add').hide();
        }
        else {
			$(this).parent().parent().find('.is_add').css('display', 'inline-block');
			$(this).parent().parent().find('.bs_add').hide();
		}
	});
	
	$('.add-more-btn').on('click', function(e){
		e.preventDefault();
		$('.datepicker').each(function(){
			$(this).parent().removeClass('has-error');
		});
		var uiTemplate = $('.upload_template').clone().removeClass('upload_template');
		uiTemplate.find('input[type=file]').val('');
		uiTemplate.find('.upload_type').val(5);
		uiTemplate.find('.datepicker').val('');
		uiTemplate.appendTo('#upload-file-container');	
		$('#upload-file-container > div:last-child .bs_add').hide();
		$('#upload-file-container > div:last-child .is_add').hide();
		$('#upload-file-container > div:last-child .datepicker').removeAttr('id').removeClass('hasDatepicker').datepicker({
			dateFormat: 'mm/dd/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: '-30:+0',
			onChangeMonthYear: function(y, m, i) {
                var d = i.selectedDay;
                $(this).datepicker('setDate', new Date(y, m - 1, d));
            }
		});
	});
	
	$('.upload-btn').on('click', function(e){
		e.preventDefault();
		$('.datepicker').each(function(){
			$(this).parent().removeClass('has-error');
		});
		var bValid = true;
		$('input[type=file]').each(function(){
			if($(this).val()==""){
				bValid = false;
			}
		});
		if(bValid){
			
			$('.datepicker').each(function(){
				if($(this).is(':visible') && $.trim($(this).val())==""){
					$(this).parent().addClass('has-error');
					bValid = false;
				}
			});
			if(bValid){
				$('.upload-btn').text('Saving Information...').prop('disabled', true);
				$('.form-back-btn').hide();
				$('.add-more-btn').hide();
				$('#upload_form').submit();
			}
		} else {
			alert('No File selected. Please select file to upload.');
		}
	});

});