//======================================================================
//  Script S51: signup.js
//      Js helper for signup process
//======================================================================
var valid_email = true;

var is_custom = 0;
var custom_price = 0;
var premiumReportPrice = 3500;
var provincialPrice = 0;
var cityPrice = 0;
var bankName = "";
var counter = true;
var val = $("#entity").val();
var valObj = JSON.parse(val);
var search_row = null;
$(document).ready(function() {
    let ent = JSON.parse($("#entity").val());

    document.getElementById('industry_sub').value = ent.industry_sub_id;
    document.getElementById('industry_row').value = ent.industry_row_id;
    
    $("#bank_dd").val(valObj.current_bank);
    $("#discount_code").val(valObj.discount_code);
    $("#serial_key").val(valObj.serial);

    if(valObj.countrycode == "PH"){
        $("#country").val(valObj.countrycode);
    }else{
        $("#country").val("INT");
        $('#province_city').hide(); // hide province fields
            $('.premiumToggle').hide(); //hide premium option
    
            bankName = $('#bank_dd').find(":selected").text();
            // get price base on selected bank only
            if($('#serial_key').val() !== ''){
                serial_key = $('#serial_key').val();
            }else{
                serial_key = false;
            }

            $.ajax({
                type: 'get',
                url: BaseURL + '/signup/getReportPriceInternational/' + bankName + '/' + serial_key,
                success: function(data){

                    bankReportType = JSON.parse(data.report_type);

                    if(bankReportType.simplified_type == 0 && bankReportType.standalone_type == 0 && bankReportType.premium_type == 1){
                        $('#country').val("PH").change();
                        $('#country').prop('disabled', true);
                    }else{
                        $('#country').prop('disabled', false);
                    }

                    let who = 0;
                    if(bankReportType.premium_type == 0){
                        $('.premiumToggle').hide();
                    }else{
                        $('.premiumToggle').show();
                        who = 1;
                    }

                    if(bankReportType.standalone_type == 0){
                        $('.standAloneToggle').hide();
                    }else{
                        $('.standAloneToggle').show();
                        who = 0;
                    }

                    if(bankReportType.simplified_type == 0){
                        $('.simplifiedToggle').hide();
                    }else{
                        $('.simplifiedToggle').show();
                        who = 2;
                    }

                    if($('#serial_key').val().length == ''){
                        $('.standAloneToggle').removeClass('active').addClass('notActive');
                        $('.simplifiedToggle').removeClass('active').addClass('notActive');
                        $('.premiumToggle').removeClass('active').addClass('notActive');
    
                        if(who == 0){
                            $('.standAloneToggle').removeClass('notActive').addClass('active');
                        }else if(who == 2){
                            $('.simplifiedToggle').removeClass('notActive').addClass('active');
                        }else{
                            $('.premiumToggle').removeClass('notActive').addClass('active');
                        }
                    }

                    if(data){
                        if(data.is_custom == 1 ){
                            $('.standAloneToggle').html('Standalone: Php <br>' + data.custom_price.toFixed(2));
                            $('.simplifiedToggle').html('Simplified: Php <br>' + data.custom_price.toFixed(2));
                        }else{
                            $('.standAloneToggle').html('Standalone: Php <br>' + data.standalone_price.toFixed(2));
                            $('.simplifiedToggle').html('Simplified: Php <br>' + data.simplified_price.toFixed(2));
                        }
                    }else{
                        //set default value
                        $('.standAloneToggle').html('Standalone: Php <br>' + 1767 + '.50');
                        $('.simplifiedToggle').html('Simplified: Php <br>' + 3535 + '.00');
                    }
                },
                error: function(){
                    console.log('error!');
                }
        
            });
    }
    jQuery("#formbpo").validationEngine('attach');

    //checking if retrieve account is premium
    if(valObj.is_premium == 0){
        $('.standAloneToggle').click();
    } else if(valObj.is_premium == 2) {
        $('.simplifiedToggle').click();
    } else if(valObj.is_premium == 3){
        $('.premiumToggle').click();
    }

    if(valObj.entity_type == 0){
        $('.corporation').click();
    } else{
        $('.soleProprietorship').click();
    }

	$("#bank_dd").select2();

	$("input[name=email]").change(function(){
		var uiThis = $(this);

        if (0 < uiThis.val().length) {
            $.ajax({
                url: BaseURL + '/api/validate_email',
                data: { email: uiThis.val() },
                dataType: 'json',
                beforeSend: function(){
                    uiThis.parent().find('span').remove();
                    uiThis.parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating email address...</span>');
                },
                success: function(oReturn){
                    uiThis.parent().find('span').not('.errormessage').remove();
                    if(oReturn.body.result == 'valid'){
                        valid_email = true;
                        uiThis.parent().append('<span style="color: GREEN;" class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span>Valid email address.</span>');
                    } else {
                        valid_email = false;
                        uiThis.parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">Email address is not valid.</span>');
                    }
                }
            });
        }
        else {
            uiThis.parent().find('span').remove();
            uiThis.parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">Email address is not valid.</span>');
            valid_email = false;
        }
	});

	if($("input[name=email]").val()!=""){
		if($("input[name=email]").parent().find('span.errormessage').length == 0){
			$("input[name=email]").change();
		}
	}

	$("#submit_btn").click(function(e){
		var ret = 0;

        if(valid_email){
			ret = $("#formbpo").validationEngine('validate')

			if (ret) {
				$(this).val('Saving Information...').prop('disabled', true);
				$('#formbpo').submit();
			}
		}
		else {
			$('html, body').animate({
				scrollTop: 0
			}, 500);
		}

        return false;
	});

	$('#bank_dd').change(function(){
		$('#bank').val($('#bank_dd').val());
	});

    $('#bank_dd').change();

	$('#serial_key,#province').on('keyup', function(){

        $("#province").children('option').show();


		if($('.simplifiedToggle').hasClass('active'))
            who = 2;

        if($('.standAloneToggle').hasClass('active'))
            who = 0;

        if($('.premiumToggle').hasClass('active'))
            who = 1;

        $(".simplifiedToggle").attr("data-toggle", "who");
        $(".standAloneToggle").attr("data-toggle", "who");
        $(".premiumToggle").attr("data-toggle", "who");

        $(".simplifiedToggle").attr("data-title", 2);
        $(".standAloneToggle").attr("data-title", 0);
        $(".premiumToggle").attr("data-title", 1);

		if($('#serial_key').val().length == 17){
			$.ajax({
                url: BaseURL + '/api/check_supervisor_key',
                data: { 
                	key: $('#serial_key').val(),
                	province: $('#province').val(),
                    reportType: who
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    // $('#serial_key').parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating key...</span>');
                },
                success: function(oReturn){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    if(oReturn != undefined && oReturn.bank_id != undefined && oReturn.messages != "This Client Key report type is currently unavailable to the associated organization."){
                       $('#bank_dd').select2('val', oReturn.bank_id);
					   $('#bank_dd').prop('disabled', true);

                       
                    } else {
						$('#bank_dd').prop('disabled', false);
                        $('#serial_key').parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">'+oReturn.messages+'</span>');
                    }

                    if(oReturn.messages != 'That Client Key is invalid.' && oReturn.messages != 'That Client Key has already been used.' && oReturn.messages != 'That Client Key has already been used.' && oReturn.messages != "This Client Key report type is currently unavailable to the associated organization."){
                        $('#bank_dd').prop('disabled', true);
                        if(oReturn.activateReport == 0){
                            $('.standAloneToggle').show();
                        }
    
                        if(oReturn.activateReport == 1){
                            $('.premiumToggle').show();
                        }
    
                        if(oReturn.activateReport == 2){
                            $('.simplifiedToggle').show();
                        }      
                        
                        $('.simplifiedToggle').removeAttr('data-toggle');
                           $('.standAloneToggle').removeAttr('data-toggle');
                           $('.premiumToggle').removeAttr('data-toggle');

                           $('.simplifiedToggle').removeAttr('data-title');
                           $('.standAloneToggle').removeAttr('data-title');
                           $('.premiumToggle').removeAttr('data-title');

                           if(oReturn.report_type == 'Simplified')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.standAloneToggle').removeClass('active');
                                    $('.standAloneToggle').addClass('notActive');
                                    $('.simplifiedToggle').addClass('active');
                                    $('.simplifiedToggle').removeClass('notActive');


                                    $(".simplifiedToggle").attr("data-toggle", "who");
                                    $(".simplifiedToggle").attr("data-title", "2");
                                    
                                    $('#who').val(2);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Standalone')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.simplifiedToggle').removeClass('active');
                                    $('.simplifiedToggle').addClass('notActive');
                                    $('.standAloneToggle').addClass('active');
                                    $('.standAloneToggle').removeClass('notActive');


                                    $(".standAloneToggle").attr("data-toggle", "who");
                                    $(".standAloneToggle").attr("data-title", "1");
                                    
                                    $('#who').val(0);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2' || oReturn.report_type == 'Premium Zone 3'){

                                var provStr = oReturn.prov_str;
                                var provinces = provStr.split('|');
                                
                                if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2'){
                                    if(provStr != '')
                                    {
                                        $('#province option').filter(function(){
                                            return !provinces.includes(this.value);
                                        }).hide();

                                        if(!provinces.includes($('#province').val())){
                                            $('#province').val(provinces[0]);
                                            populateCity2(provinces[0]);
                                        }
                                    }
                                }else{

                                    $('#province option').filter(function(){
                                        return provinces.includes(this.value);
                                    }).hide();

                                    if(provinces.includes($('#province').val())){
                                        $('#province').val('1401');
                                        populateCity2('1401');
                                    }
                                }

                                $('.simplifiedToggle').removeClass('active');
                                $('.standAloneToggle').removeClass('active');
                                $('.simplifiedToggle').addClass('notActive');
                                $('.standAloneToggle').addClass('notActive');
                                $('.premiumToggle').addClass('active');
                                $('.premiumToggle').removeClass('notActive');

                                $(".premiumToggle").attr("data-toggle", "who");
                                $(".premiumToggle").attr("data-title", "1");

                                if($('#who').val() != 1)
                                    $('#who').val(1);
                                $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }
                    }

                }
            });
		} else {
			$('#bank_dd').prop('disabled', false);

            if($('#serial_key').val() != ''){
                $.ajax({
                    url: BaseURL + '/api/check_supervisor_key',
                    data: { 
                        key: $('#serial_key').val(),
                        province: $('#province').val(),
                        reportType: who,
                        country:$('#country').val()
                    },
                    dataType: 'json',
                    beforeSend: function(){
                        $('#serial_key').parent().find('span').not('.errormessage').remove();
                    },
                    success: function(oReturn){
                            $('#serial_key').parent().find('span').not('.errormessage').remove();
                            if(oReturn != undefined && oReturn.bank_id != undefined && oReturn.messages != "This Client Key report type is currently unavailable to the associated organization."){
                                $('#bank_dd').select2('val', oReturn.bank_id);
                                $('#bank_dd').prop('disabled', true);
                            }else {
                                $('#bank_dd').prop('disabled', false);
                                $('#serial_key').parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">'+oReturn.messages+'</span>');
                            }
                    }
                });
            }else{
                $('#serial_key').parent().find('span').not('.errormessage').remove(); //remove error message
                if($('#serial_key').val() !== ''){
                    serial_key = $('#serial_key').val();
                }else{
                    serial_key = false;
                }

                bankName = $('#bank_dd').find(":selected").text();
                $.ajax({
                    type: 'get',
                    url: BaseURL + '/signup/getReportPriceInternational/' + bankName + '/' + serial_key ,
                    success: function(data){
                        bankReportType = JSON.parse(data.report_type);
        
                        if(bankReportType.simplified_type == 0 && bankReportType.standalone_type == 0 && bankReportType.premium_type == 1){
                            $('#country').val("PH").change();
                            $('#country').prop('disabled', true);
                        }else{
                            $('#country').prop('disabled', false);
                        }
        
                        let who = 0;
                        if(bankReportType.premium_type == 0){
                            $('.premiumToggle').hide();
                        }else{
                            $('.premiumToggle').show();
                            who = 1;
                        }
        
                        if(bankReportType.standalone_type == 0){
                            $('.standAloneToggle').hide();
                        }else{
                            $('.standAloneToggle').show();
                            who = 0;
                        }
        
                        if(bankReportType.simplified_type == 0){
                            $('.simplifiedToggle').hide();
                        }else{
                            $('.simplifiedToggle').show();
                            who = 2;
                        }

                        $('.standAloneToggle').removeClass('active').addClass('notActive');
                        $('.simplifiedToggle').removeClass('active').addClass('notActive');
                        $('.premiumToggle').removeClass('active').addClass('notActive');

                        if(who == 0){
                            $('.standAloneToggle').removeClass('notActive').addClass('active');
                        }else if(who == 2){
                            $('.simplifiedToggle').removeClass('notActive').addClass('active');
                        }else{
                            $('.premiumToggle').removeClass('notActive').addClass('active');
                        }
                    },
                    error: function(){
                        console.log('error!');
                    }
            
                });
            }
		}
    }).keyup();

    $('.simplifiedToggle,.standAloneToggle,.premiumToggle').click(function(){

        $("#province").children('option').show();

        if($('.simplifiedToggle').hasClass('active'))
            who = 2;

        if($('.standAloneToggle').hasClass('active'))
            who = 0;

        if($('.premiumToggle').hasClass('active'))
            who = 1;

        $(".simplifiedToggle").attr("data-toggle", "who");
        $(".standAloneToggle").attr("data-toggle", "who");
        $(".premiumToggle").attr("data-toggle", "who");

        $(".simplifiedToggle").attr("data-title", 2);
        $(".standAloneToggle").attr("data-title", 0);
        $(".premiumToggle").attr("data-title", 1);

        if($('#serial_key').val().length == 17){
            $.ajax({
                url: BaseURL + '/api/check_supervisor_key',
                data: { 
                    key: $('#serial_key').val(),
                    province: $('#province').val(),
                    reportType: who
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    // $('#serial_key').parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating key...</span>');
                },
                success: function(oReturn){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    if(oReturn != undefined && oReturn.bank_id != undefined && oReturn.messages != 'That Client Key has already been used.' && oReturn.messages != "This Client Key report type is currently unavailable to the associated organization."){
                       $('#bank_dd').select2('val', oReturn.bank_id);
                       $('#bank_dd').prop('disabled', true);
                    } else {
                        $('#bank_dd').prop('disabled', false);
                        $('#serial_key').parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">'+oReturn.messages+'</span>');
                    }

                    if(oReturn.messages != 'That Client Key is invalid.' && oReturn.messages != 'That Client Key has already been used.' && oReturn.messages != 'That Client Key has already been used.' && oReturn.messages != "This Client Key report type is currently unavailable to the associated organization."){
                           $('.simplifiedToggle').removeAttr('data-toggle');
                           $('.standAloneToggle').removeAttr('data-toggle');
                           $('.premiumToggle').removeAttr('data-toggle');

                           $('.simplifiedToggle').removeAttr('data-title');
                           $('.standAloneToggle').removeAttr('data-title');
                           $('.premiumToggle').removeAttr('data-title');

                           if(oReturn.report_type == 'Simplified')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.standAloneToggle').removeClass('active');
                                    $('.standAloneToggle').addClass('notActive');
                                    $('.simplifiedToggle').addClass('active');
                                    $('.simplifiedToggle').removeClass('notActive');


                                    $(".simplifiedToggle").attr("data-toggle", "who");
                                    $(".simplifiedToggle").attr("data-title", "2");
                                    
                                    $('#who').val(2);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Standalone')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.simplifiedToggle').removeClass('active');
                                    $('.simplifiedToggle').addClass('notActive');
                                    $('.standAloneToggle').addClass('active');
                                    $('.standAloneToggle').removeClass('notActive');


                                    $(".standAloneToggle").attr("data-toggle", "who");
                                    $(".standAloneToggle").attr("data-title", "1");
                                    
                                    $('#who').val(0);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2' || oReturn.report_type == 'Premium Zone 3'){

                                var provStr = oReturn.prov_str;
                                var provinces = provStr.split('|');
                                
                                if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2'){
                                    if(provStr != '')
                                    {
                                        $('#province option').filter(function(){
                                            return !provinces.includes(this.value);
                                        }).hide();

                                        if(!provinces.includes($('#province').val())){
                                            $('#province').val(provinces[0]);
                                            populateCity2(provinces[0]);
                                        }
                                    }
                                }else{

                                    $('#province option').filter(function(){
                                        return provinces.includes(this.value);
                                    }).hide();

                                    if(provinces.includes($('#province').val())){
                                        $('#province').val('1401');
                                        populateCity2('1401');
                                    }
                                }

                                $('.simplifiedToggle').removeClass('active');
                                $('.standAloneToggle').removeClass('active');
                                $('.simplifiedToggle').addClass('notActive');
                                $('.standAloneToggle').addClass('notActive');
                                $('.premiumToggle').addClass('active');
                                $('.premiumToggle').removeClass('notActive');

                                $(".premiumToggle").attr("data-toggle", "who");
                                $(".premiumToggle").attr("data-title", "1");

                                if($('#who').val() != 1)
                                    $('#who').val(1);
                                $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }
                    }
                }
            });
        } else {
            $('#bank_dd').prop('disabled', false);
        }

    });

    $("#industry_main").on("change", function(){
        get_industry("sub");
    });

    $("#industry_sub").on("change", function(){
        get_industry("row");
    });

    $('#province').change(function(){
        var newProvince = $('#province').find(":selected").val();
        populateCity2(newProvince);

        if($('.simplifiedToggle').hasClass('active'))
            who = 2;

        if($('.standAloneToggle').hasClass('active'))
            who = 0;

        if($('.premiumToggle').hasClass('active'))
            who = 1;

        $(".simplifiedToggle").attr("data-toggle", "who");
        $(".standAloneToggle").attr("data-toggle", "who");
        $(".premiumToggle").attr("data-toggle", "who");

        $(".simplifiedToggle").attr("data-title", 2);
        $(".standAloneToggle").attr("data-title", 0);
        $(".premiumToggle").attr("data-title", 1);
        
        if($('#serial_key').val().length == 17){
            $.ajax({
                url: BaseURL + '/api/check_supervisor_key',
                data: { 
                    key: $('#serial_key').val(),
                    province: $('#province').val(),
                    reportType: who
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    $('#serial_key').parent().append('<span style="color: ORANGE;" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> <span>Validating key...</span>');
                },
                success: function(oReturn){
                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                    if(oReturn != undefined && oReturn.bank_id != undefined){
                       $('#bank_dd').select2('val', oReturn.bank_id);
                       $('#bank_dd').prop('disabled', true);
                    } else {
                        $('#bank_dd').prop('disabled', false);
                        $('#serial_key').parent().append('<span style="color: RED;" class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <span style="color: RED;">'+oReturn.messages+'</span>');
                    }

                    if(oReturn.messages != 'That Client Key is invalid.' && oReturn.messages != 'That Client Key has already been used.'){
                           $('.simplifiedToggle').removeAttr('data-toggle');
                           $('.standAloneToggle').removeAttr('data-toggle');
                           $('.premiumToggle').removeAttr('data-toggle');

                           $('.simplifiedToggle').removeAttr('data-title');
                           $('.standAloneToggle').removeAttr('data-title');
                           $('.premiumToggle').removeAttr('data-title');

                           if(oReturn.report_type == 'Simplified')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.standAloneToggle').addClass('active');
                                    $('.standAloneToggle').removeClass('notActive');


                                    $(".simplifiedToggle").attr("data-toggle", "who");
                                    $(".simplifiedToggle").attr("data-title", "2");
                                    
                                    $('#who').val(2);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Standalone')
                           {
                                    $('.premiumToggle').removeClass('active');
                                    $('.premiumToggle').addClass('notActive');
                                    $('.simplifiedToggle').addClass('active');
                                    $('.simplifiedToggle').removeClass('notActive');


                                    $(".standAloneToggle").attr("data-toggle", "who");
                                    $(".standAloneToggle").attr("data-title", "1");
                                    
                                    $('#who').val(0);
                                    $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }

                           if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2' || oReturn.report_type == 'Premium Zone 3'){

                                var provStr = oReturn.prov_str;
                                var provinces = provStr.split('|');
                                
                                if(oReturn.report_type == 'Premium Zone 1' || oReturn.report_type == 'Premium Zone 2'){
                                    if(provStr != '')
                                    {
                                        $('#province option').filter(function(){
                                            return !provinces.includes(this.value);
                                        }).hide();

                                        if(!provinces.includes($('#province').val())){
                                            $('#province').val(provinces[0]);
                                            populateCity2(provinces[0]);
                                        }
                                    }
                                }else{

                                    $('#province option').filter(function(){
                                        return provinces.includes(this.value);
                                    }).hide();

                                    if(provinces.includes($('#province').val())){
                                        $('#province').val('1401');
                                        populateCity2('1401');
                                    }
                                }

                                $('.simplifiedToggle').removeClass('active');
                                $('.standAloneToggle').removeClass('active');
                                $('.simplifiedToggle').addClass('notActive');
                                $('.standAloneToggle').addClass('notActive');
                                $('.premiumToggle').addClass('active');
                                $('.premiumToggle').removeClass('notActive');

                                $(".premiumToggle").attr("data-toggle", "who");
                                $(".premiumToggle").attr("data-title", "1");

                                if($('#who').val() != 1)
                                    $('#who').val(1);
                                $('#serial_key').parent().find('span').not('.errormessage').remove();
                                
                           }
                    }
                }
            });
        } else {
            $('#bank_dd').prop('disabled', false);
        }
        
    });
    
    $('#city').on("change", function(){
        var province = $('#province').find(":selected").text();
        var city = $('#city').find(":selected").val();
        // getCityPremiumReportPrice(city);
        bankName = $('#bank_dd').find(":selected").text();
        provinceCode = $('#province').find(":selected").val();
        getReportPrice(bankName, provinceCode);
    });
    
    $('#bank_dd').change(function(){
        // getCustomReportPrice();
        bankName = $('#bank_dd').find(":selected").text();
        provinceCode = $('#province').find(":selected").val();
        getReportPrice(bankName, provinceCode);
        if($('#serial_key').val() !== ''){
            serial_key = $('#serial_key').val();
        }else{
            serial_key = false;
        }

        $.ajax({
            type: 'get',
            url: BaseURL + '/signup/getReportPriceInternational/' + bankName + '/' + serial_key,
            success: function(data){
                bankReportType = JSON.parse(data.report_type);

                if(bankReportType.simplified_type == 0 && bankReportType.standalone_type == 0 && bankReportType.premium_type == 1){
                    $('#country').val("PH").change();
                    $('#country').prop('disabled', true);
                }else{
                    $('#country').prop('disabled', false);
                }

                let who = 0;
                if(bankReportType.premium_type == 0){
                    $('.premiumToggle').hide();
                }else{
                    $('.premiumToggle').show();
                    who = 1;
                }

                if(bankReportType.standalone_type == 0){
                    $('.standAloneToggle').hide();
                }else{
                    $('.standAloneToggle').show();
                    who = 0;
                }

                if(bankReportType.simplified_type == 0){
                    $('.simplifiedToggle').hide();
                }else{
                    $('.simplifiedToggle').show();
                    who = 2;
                }

                if($('#serial_key').val().length == ''){
                    $('.standAloneToggle').removeClass('active').addClass('notActive');
                    $('.simplifiedToggle').removeClass('active').addClass('notActive');
                    $('.premiumToggle').removeClass('active').addClass('notActive');

                    if(who == 0){
                        $('.standAloneToggle').removeClass('notActive').addClass('active');
                    }else if(who == 2){
                        $('.simplifiedToggle').removeClass('notActive').addClass('active');
                    }else{
                        $('.premiumToggle').removeClass('notActive').addClass('active');
                    }
                }

                if(data){
                    if((data.is_custom == 1 ) && ($('#bank_contractor').val() != 1)){
                        $('.standAloneToggle').html('Standalone: Php <br>' + data.custom_price.toFixed(2));
                        $('.simplifiedToggle').html('Simplified: Php <br>' + data.custom_price.toFixed(2));
                    }else{
                        $('.standAloneToggle').html('Standalone: Php <br>' + data.standalone_price.toFixed(2));
                        $('.simplifiedToggle').html('Simplified: Php <br>' + data.simplified_price.toFixed(2));
                    }
                }else{
                    //set default value
                    $('.standAloneToggle').html('Standalone: Php <br>' + 1767 + '.50');
                    $('.simplifiedToggle').html('Simplified: Php <br>' + 3535 + '.00');
                }
            },
            error: function(){
                console.log('error!');
            }
    
        });
    });
    $('#city').change();
    // getCustomReportPrice();
    bankName = $('#bank_dd').find(":selected").text();
    provinceCode = $('#province').find(":selected").val();
    getReportPrice(bankName, provinceCode);

    $("#live_search").on("click", function(){
        $("#industry_live_search").modal();
    });

    $("#pse_search").on("click", function(){
        $("#pse_search_modal").modal();
    });

    $("#pse_search_modal").on("shown.bs.modal",function(e){
        $('#search_company').val("");
        $.ajax({
            url: BaseURL + '/pse_search',
            type: 'post',
            beforeSend: function(){
                //  do before send page load
            },
            success: function(resp){
                if(resp.count != 0) {
                    $("#show_pse_results").html(resp.html);
                } else {
                    $("#show_pse_results").html("<ul><li><i>No data found!</i></li></ul>");
                }

            }
        });
    });

    $('#country').change(function(){
        if($(this).val() == 'PH'){
            $('#province_city').show();
            $('.premiumToggle').show();
            bankName = $('#bank_dd').find(":selected").text();
            provinceCode = $('#province').find(":selected").val();

            $('.premiumToggle').removeAttr('disabled');
            $('.premiumToggle').click();
        }else{
            $('#province_city').hide(); // hide province fields
            // $('.premiumToggle').hide(); //hide premium option
            $('.premiumToggle').attr('disabled', 'disabled');

            if($('.premiumToggle').hasClass('active')) {
                $('.premiumToggle').removeClass('active').addClass('notActive');
                $('.simplifiedToggle').removeClass('notActive').addClass('active');
                $('#who').attr('value', 0);
            }else if($('.stanAloneToggle').hasClass('active')){
                $('.stanAloneToggle').removeClass('active').addClass('notActive');
                $('.simplifiedToggle').removeClass('notActive').addClass('active');
                $('#who').attr('value', 0);
            }
    
            bankName = $('#bank_dd').find(":selected").text();
            // get price base on selected bank only
            if($('#serial_key').val() !== ''){
                serial_key = $('#serial_key').val();
            }else{
                serial_key = false;
            }

            $.ajax({
                type: 'get',
                url: BaseURL + '/signup/getReportPriceInternational/' + bankName + '/' + serial_key,
                success: function(data){
                    bankReportType = JSON.parse(data.report_type);

                    if(bankReportType.simplified_type == 0 && bankReportType.standalone_type == 0 && bankReportType.premium_type == 1){
                        $('#country').val("PH").change();
                        $('#country').prop('disabled', true);
                    }else{
                        $('#country').prop('disabled', false);
                    }

                    let who = 0;
                    if(bankReportType.premium_type == 0){
                        $('.premiumToggle').hide();
                    }else{
                        $('.premiumToggle').show();
                        who = 1;
                    }

                    if(bankReportType.standalone_type == 0){
                        $('.standAloneToggle').hide();
                    }else{
                        $('.standAloneToggle').show();
                        who = 0;
                    }

                    if(bankReportType.simplified_type == 0){
                        $('.simplifiedToggle').hide();
                    }else{
                        $('.simplifiedToggle').show();
                        who = 2;
                    }

                    if($('#serial_key').val().length == ''){
                        $('.standAloneToggle').removeClass('active').addClass('notActive');
                        $('.simplifiedToggle').removeClass('active').addClass('notActive');
                        $('.premiumToggle').removeClass('active').addClass('notActive');
    
                        if(who == 0){
                            $('.standAloneToggle').removeClass('notActive').addClass('active');
                        }else if(who == 2){
                            $('.simplifiedToggle').removeClass('notActive').addClass('active');
                        }else{
                            $('.premiumToggle').removeClass('notActive').addClass('active');
                        }
                    }

                    if(data){
                        if((data.is_custom == 1 ) && ($('#bank_contractor').val() != 1)){
                            $('.standAloneToggle').html('Standalone: Php <br>' + data.custom_price.toFixed(2));
                            $('.simplifiedToggle').html('Simplified: Php <br>' + data.custom_price.toFixed(2));
                        }else{
                            $('.standAloneToggle').html('Standalone: Php <br>' + data.standalone_price.toFixed(2));
                            $('.simplifiedToggle').html('Simplified: Php <br>' + data.simplified_price.toFixed(2));
                        }
                    }else{
                        //set default value
                        $('.standAloneToggle').html('Standalone: Php <br>' + 1767 + '.50');
                        $('.simplifiedToggle').html('Simplified: Php <br>' + 3535 + '.00');
                    }
                },
                error: function(){
                    console.log('error!');
                }
        
            });
        }
    });

    $("#search_industry").on("keyup",function(ths){
        let str = $(this).val()
        if(str.length >= 3) {
            $.ajax({
                url: BaseURL + '/industry_search',
                type: 'post',
                data: {
                    search: str
                },
                beforeSend: function(){
                    //  do before send page load
                },
                success: function(resp){
                    if(resp.html) {
                        $("#show_results").html(resp.html);
                    } else {
                        $("#show_results").html("<ul><li><i>No data found!</i></li></ul>");
                    }

                }
            });
        }
    });

    $("#search_company").on("keyup",function(ths){
        let str_company = $(this).val();

        if(str_company.length >= 3) {
            $.ajax({
                url: BaseURL + '/pse_search',
                type: 'post',
                data: {
                    search: str_company
                },
                beforeSend: function(){
                    //  do before send page load
                },
                success: function(resp){
                    if(resp.count != 0) {
                        $("#show_pse_results").html(resp.html);
                    } else {
                        $("#show_pse_results").html("<ul><li><i>No data found!</i></li></ul>");
                    }

                }
            });
        }
    });

    // Do things after pse company is selected
    $("#btnSavePse").on("click",function(){4
        let pse_search = $("input[name=pse_name]:checked").val();

        $.ajax({
            url: BaseURL + '/process_pse_search',
            type: 'post',
            data: {
                pse_company: pse_search
            },
            success: function(resp){
                if(resp){
                    if(resp.city && resp.province){
                        select_city_code = resp.city;
                        $('#province').val(resp.province);
                        $('#province').change();
                    }

                    $('#companyname').val(resp.companyname);
                    $("#pse_search_modal").modal("hide");
                }
            }
        }); 
    });	

    $("#search_industry").on("keyup",function(ths){
        let str = $(this).val()
        if(str.length >= 3) {
            $.ajax({
                url: BaseURL + '/industry_search',
                type: 'post',
                data: {
                    search: str
                },
                beforeSend: function(){
                    //  do before send page load
                },
                success: function(resp){
                    if(resp.html) {
                        $("#show_results").html(resp.html);
                    } else {
                        $("#show_results").html("<ul><li><i>No data found!</i></li></ul>");
                    }

                }
            });
        }
    });
    // Do things after search selection
    $("#btnSave").on("click",function(){
        let search = $("input[name=row_title]:checked").val();
        $.ajax({
            url: BaseURL + '/process_search',
            type: 'post',
            data: {
                industry: search
            },
            success: function(resp){
                if(resp.success) {
                    $("#industry_live_search").modal("hide");
                    $("#industry_main").val(resp.main);
                    get_industry("sub", resp.sub, resp.row);
                }
            }
        }); 
    });	
    // Do thing after close
    $('#industry_live_search').on('hidden.bs.modal', function () {
        // do something…
        $("#show_results").html(" ");
        $("#search_industry").val("");
    });

});

function get_industry(tier, sub, row) 
{
    if(row) {
        search_row = row;
    }
    let url = "", index =0;
    if (tier == "sub") {
        index = $("#industry_main").val();
        url = 'signup/getIndustrySub/' + index;
    } else {
        index = $("#industry_sub").val();
        url = 'signup/getIndustryRow/' + index;
    }
    $.ajax({
        type: 'get',
        url: BaseURL + '/' + url,
        success: function(data){
             var industry  = "";
            for (var i = 0; i < data.length; i++){
                if (tier == "sub") {
                    industry += '<option value="'+data[i].sub_code+'">'+data[i].sub_title+'</option>';
                } else {
                    industry += '<option value="'+data[i].group_code+'">'+data[i].group_description+'</option>';
                }
            }
            $('#industry_' + tier).html(" ");
            $('#industry_' + tier).append(industry);
            if(sub && tier == "sub") {
                $("#industry_sub").val(sub).change();
            } else if(search_row && tier == "row"){
                $("#industry_row").val(search_row);
                search_row = null;
            }else{
                if(tier != "row") {
                    $("#industry_" + tier).change();
                }
            }
        },
        error: function(){
            console.log('success');
        }
    });
}



function populateCity(province) {
    if(province) {
        $.ajax({
            type: 'get',
            url: BaseURL + '/signup/getCityList/' + province,
            success: function(data){
                var cityList = "";
                let entity = JSON.parse($("#entity").val());
                selected = "";
    
                for(var i=0; i < data.length; i++) {
                    if(data[i].id == entity.cityid ) {
                        selected = data[i].city;
                    }
                    cityList += '<option value="'+data[i].city+'" ' + selected + '>'+data[i].city+'</option>';
                }
    
                $('#city').html(" ");
                $('#city').append(cityList).change();
                if(selected) {
                    $("#city").val(selected).change();
                }

                if(select_city){
                    $("#city").val(select_city);
                    select_city = null;
                }
            },
            error: function(){
                console.log('error!');
            }
        });
    }
}

function getCustomReportPrice(){
    let bank = $("#bank_dd option[value = " + $("#bank_dd").val() + "]").text();
    $.ajax({
        type: 'get',
        url: BaseURL + '/signup/getCustomReportPrice/' + bank,
        success: function(data){
            is_custom = data.is_custom;
            custom_price = data.custom_price;
            updateReportPrices();
        },
        error: function(){
            console.log('error!');
        }
    });
    }

    function getCityPremiumReportPrice($city = 140101) {
        $.ajax({
            type: 'get',
            url: BaseURL + '/signup/getCityPremiumReportPrice/' + $city, 
            success: function(data){
                cityPrice = data.premiumReportPrice;
    
                if(data.isPremiumAvailable == 0) {
                    $('.premiumToggle').attr('disabled', 'disabled');
    
                    if($('.premiumToggle').hasClass('active')) {
                        $('.premiumToggle').removeClass('active').addClass('notActive');
                        $('.standAloneToggle').removeClass('notActive').addClass('active');
                        $('#who').attr('value', 0);
                    }
    
                } else {
                    $('.premiumToggle').removeAttr('disabled');
                }
    
                updateReportPrices(data.isPremiumAvailable);
            },
            error: function(){
                console.log('error!');
            }
        });
    }

function updateReportPrices(data) {
    var newPremiumPrice = 0;
    var newStandalonePrice = 0;
    var newSimplifiedPrice = 0

    if((data.is_custom == 1 ) && ($('#bank_contractor').val() != 1)){
        newPremiumPrice = data.custom_price;
        newStandalonePrice = data.custom_price;
        newSimplifiedPrice = data.custom_price;
    }else{
        newPremiumPrice = data.premium;
        newStandalonePrice = data.standalone;
        newSimplifiedPrice = data.simplified;
    }

    $('.premiumToggle').html('Premium: Php <br>' + newPremiumPrice.toFixed(2));
    $('.standAloneToggle').html('Standalone: Php <br>' + newStandalonePrice.toFixed(2));
    $('.simplifiedToggle').html('Simplified: Php <br>' + newSimplifiedPrice.toFixed(2));
    // var newPremiumPrice = 0;
    // var newStandalonePrice = 0;
    
    // if(is_custom == 1) { 
    //     newPremiumPrice = custom_price;
    //     newStandalonePrice = custom_price;
    // } else {
    //     newPremiumPrice = premiumReportPrice;
    //     newStandalonePrice = 3500;
    // }
    // if(isNaN(newPremiumPrice) == false  && isNaN(cityPrice) == false ){
    //     newPremiumPrice = newPremiumPrice + cityPrice;
    //     if($isPremiumAvailable === undefined || $isPremiumAvailable == 1) {
    //         $('.premiumToggle').html('Premium: <br> Php ' + newPremiumPrice + '.00');
    //         $('.standAloneToggle').html('Standalone: <br> Php ' + newStandalonePrice + '.00');
    //     } else {
    //         $('.premiumToggle').html('Premium: <br> Not Available');
    //         $('.standAloneToggle').html('Standalone: <br> Php ' + newStandalonePrice + '.00');
    //     }
    // }
}

function populateCity2($provCode = 1401) {
    $.ajax({
        type: 'get',
        url: BaseURL + '/signup/getCityList2/' + $provCode,
        success: function(data){
            var cityList = "";
            for(var i=0; i < data.length; i++) {
                cityList += '<option value="'+data[i].citymunCode+'">'+data[i].citymunDesc+'</option>';
            }

            $('#city').html(" ");
            $('#city').append(cityList);
            $('#city').change();

            if(select_city_code){
                $("#city").val(select_city_code);
                select_city_code = null;
            }
        },
        error: function(){
            console.log('error!');
        }
    });
}

function getReportPrice($bankName = "123 Company", $provinceCode = '1401') {

    $bankName = $bankName.replace(/\//g, '-');
   
    $.ajax({
        type: 'get',
        url: BaseURL + '/signup/getCustomReportPrice/' + $bankName + '/' + $provinceCode,
        success: function(data){
            is_custom = data.is_custom;
            custom_price = data.custom_price;
            updateReportPrices(data);
        },
        error: function(){
            console.log('error!');
        }
    });
}