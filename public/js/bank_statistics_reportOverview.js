//======================================================================
//  Script S4: bank_statistics_reportOverview.js
//      Used for creating report overview table
//======================================================================
function ReportOverview(container, data) {
   container.html('<table id="reportOverview" class="table table-striped table-bordered"></table>');
   dataSet = [];

   data.forEach(function(entry) {
      dataSet.push([entry.entityid, 
                  entry.companyname,
                  entry.report_type,
                  entry.completed_date,
                  entry.score_bank]);
   });

   $('#reportOverview').DataTable({
      data: dataSet,
      scrollY: 400,
      columns: [
         { title: "ID"},
         { title: "Company"},
         { title: "Report Type"},
         { title: "Date Submitted"},
         { title: "Rating"},
      ],
      dom: 'Bfrtip',
      buttons: [
         { "extend": 'csv', "text":'Download CSV', "filename": "Report Overview" }
      ]
   });
}