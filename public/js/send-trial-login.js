//======================================================================
//  Script S50: forgot-password.js
//      Sends the Login information for a Trial Account
//======================================================================

$(document).ready(function() {
    
/**------------------------------------------------------------------------
|	Initialization
|--------------------------------------------------------------------------
|	@param [in]		NONE
|	@param [out] 	NONE
|	@return 		NONE
|------------------------------------------------------------------------*/
function initTrialSendLogin()
{
    var send_links  = '.pop-trial-send-form';
    
    //-----------------------------------------------------
    //  Binding S50.1: .pop-trial-send-form
    //      Displays the Send email form when clicked
    //-----------------------------------------------------
    $('body').on('click', send_links, function() {
        /** Loads the Modal Pop-up  */
        loadTrialSendForm($(this).data('email'), $(this).attr('href'));
        
        return false;
    });
}

function bindTrialSendBtn()
{
    var send_btn    = '#trial-login-send-btn';
    
    //-----------------------------------------------------
    //  Binding S50.2: #trial-login-send-btn
    //      Sends an email when the button is clicked
    //-----------------------------------------------------
    $('body').on('click', send_btn, function() {
        
        sendTrialEmail(send_btn);
        return false;
    });
}

function loadTrialSendForm(client_email, entity_url)
{
    var modal_cntr  = $('#trial-resend-modal');
    var email_fld   = $('#trial-email');
    var pword_fld   = $('#trial-password');
    var pword_lbl   = $('#password-lbl');
    var send_btn    = $('#trial-login-send-btn');
    var error_cntr  = $('.trial-send-error');
    
    //-----------------------------------------------------
    //  Binding S50.3: #trial-resend-modal
    //      Binds the Email form to Bootstrap Modal
    //-----------------------------------------------------
    /** Configure the Modal Pop-up  */
    modal_cntr.modal({
        keyboard: false,
        backdrop: 'static',
    })
        /** Event before Modal Pops-up  */
        .on('show.bs.modal', function(e) {
            /** Loads the Form  */
            email_fld.val(client_email);
            pword_fld.val('');
            pword_lbl.text(client_email);
            send_btn.attr('data-ajax-url', entity_url);
        })
        /** Event before Modal closes  */
        .on('hide.bs.modal', function(e) {
            /** Reset the Form and bindings   */
            email_fld.val('');
            pword_lbl.text('');
            error_cntr.hide();
            send_btn.prop('disabled', false).val('SEND').text('SEND');
        })
        .modal('show');
}

function sendTrialEmail(send_btn)
{
    var error_cntr  = $('.trial-send-error');
    var email_fld   = $('#trial-email');
    var pword_fld   = $('#trial-password');
    var srv_message = '';
    
    $(send_btn).prop('disabled', true).val('SENDING...').text('SENDING...');
    
    $.post($(send_btn).attr('data-ajax-url'), { 'email': email_fld.val(), 'password': pword_fld.val() }, function(srv_resp) {
        
        if (1 == srv_resp.sts) {
            $(send_btn).val('EMAIL SENT').text('EMAIL SENT');
            error_cntr.hide();
        }
        /** There are invalid inputs    */
        else {
            /**	Formats the warnings received from the Server	*/
            for (idx = 0; idx < srv_resp.messages.length; idx++) {
                console.log(idx);
                srv_message += srv_resp.messages[idx] +'<br/>';
            }
            
            /**	Display the warning								*/
            error_cntr.show();
            error_cntr.html(srv_message);
            
            /** Resets the Save button when there are errors    */
            $(send_btn).prop('disabled', false).val('SEND').text('SEND');
        }
    }, 'json');
}

initTrialSendLogin();
bindTrialSendBtn();

});