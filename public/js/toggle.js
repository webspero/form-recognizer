//======================================================================
//  Script S57: toggle.js
//      Scripts for Questionnaire import/export toggle switch
//======================================================================
$(document).ready(function(){
	$('#biztyp-list-cntr .cb_import').change(function(){
		var uiThisValue = 0;
		if($(this).prop('checked')){
			uiThisValue = 1;
		}
		$.ajax({
			url: BaseURL + '/sme/update_businesstype',
			type: 'post',
			data: {
				entity_id: $('#entityids').val(),
				type: 'import',
				value: uiThisValue
			},
			success: function(oReturn) {
				
			}
		});
	});
	
	$('#biztyp-list-cntr .cb_export').change(function(){
		var uiThisValue = 0;
		if($(this).prop('checked')){
			uiThisValue = 1;
		}
		$.ajax({
			url: BaseURL + '/sme/update_businesstype',
			type: 'post',
			data: {
				entity_id: $('#entityids').val(),
				type: 'export',
				value: uiThisValue
			},
			success: function(oReturn) {
				
			}
		});
	});
});