SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for refprovince
-- ----------------------------
DROP TABLE IF EXISTS `refprovince`;
CREATE TABLE `refprovince` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `psgcCode` varchar(255) DEFAULT NULL,
  `provDesc` text,
  `regCode` varchar(255) DEFAULT NULL,
  `provCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of refprovince
-- ----------------------------
INSERT INTO `refprovince` VALUES ('1', '012800000', 'Ilocos Norte', '01', '0128');
INSERT INTO `refprovince` VALUES ('2', '012900000', 'Ilocos Sur', '01', '0129');
INSERT INTO `refprovince` VALUES ('3', '013300000', 'La Union', '01', '0133');
INSERT INTO `refprovince` VALUES ('4', '015500000', 'Pangasinan', '01', '0155');
INSERT INTO `refprovince` VALUES ('5', '020900000', 'Batanes', '02', '0209');
INSERT INTO `refprovince` VALUES ('6', '021500000', 'Cagayan', '02', '0215');
INSERT INTO `refprovince` VALUES ('7', '023100000', 'Isabela', '02', '0231');
INSERT INTO `refprovince` VALUES ('8', '025000000', 'Nueva Vizcaya', '02', '0250');
INSERT INTO `refprovince` VALUES ('9', '025700000', 'Quirino', '02', '0257');
INSERT INTO `refprovince` VALUES ('10', '030800000', 'Bataan', '03', '0308');
INSERT INTO `refprovince` VALUES ('11', '031400000', 'Bulacan', '03', '0314');
INSERT INTO `refprovince` VALUES ('12', '034900000', 'Nueva Ecija', '03', '0349');
INSERT INTO `refprovince` VALUES ('13', '035400000', 'Pampanga', '03', '0354');
INSERT INTO `refprovince` VALUES ('14', '036900000', 'Tarlac', '03', '0369');
INSERT INTO `refprovince` VALUES ('15', '037100000', 'Zambales', '03', '0371');
INSERT INTO `refprovince` VALUES ('16', '037700000', 'Aurora', '03', '0377');
INSERT INTO `refprovince` VALUES ('17', '041000000', 'Batangas', '04', '0410');
INSERT INTO `refprovince` VALUES ('18', '042100000', 'Cavite', '04', '0421');
INSERT INTO `refprovince` VALUES ('19', '043400000', 'Laguna', '04', '0434');
INSERT INTO `refprovince` VALUES ('20', '045600000', 'Quezon', '04', '0456');
INSERT INTO `refprovince` VALUES ('21', '045800000', 'Rizal', '04', '0458');
INSERT INTO `refprovince` VALUES ('22', '174000000', 'Marinduque', '17', '1740');
INSERT INTO `refprovince` VALUES ('23', '175100000', 'Occidental Mindoro', '17', '1751');
INSERT INTO `refprovince` VALUES ('24', '175200000', 'Oriental Mindoro', '17', '1752');
INSERT INTO `refprovince` VALUES ('25', '175300000', 'Palawan', '17', '1753');
INSERT INTO `refprovince` VALUES ('26', '175900000', 'Romblon', '17', '1759');
INSERT INTO `refprovince` VALUES ('27', '050500000', 'Albay', '05', '0505');
INSERT INTO `refprovince` VALUES ('28', '051600000', 'Camarines Norte', '05', '0516');
INSERT INTO `refprovince` VALUES ('29', '051700000', 'Camarines Sur', '05', '0517');
INSERT INTO `refprovince` VALUES ('30', '052000000', 'Catanduanes', '05', '0520');
INSERT INTO `refprovince` VALUES ('31', '054100000', 'Masbate', '05', '0541');
INSERT INTO `refprovince` VALUES ('32', '056200000', 'Sorsogon', '05', '0562');
INSERT INTO `refprovince` VALUES ('33', '060400000', 'Aklan', '06', '0604');
INSERT INTO `refprovince` VALUES ('34', '060600000', 'Antique', '06', '0606');
INSERT INTO `refprovince` VALUES ('35', '061900000', 'Capiz', '06', '0619');
INSERT INTO `refprovince` VALUES ('36', '063000000', 'Iloilo', '06', '0630');
INSERT INTO `refprovince` VALUES ('37', '064500000', 'Negros Occidental', '06', '0645');
INSERT INTO `refprovince` VALUES ('38', '067900000', 'Guimaras', '06', '0679');
INSERT INTO `refprovince` VALUES ('39', '071200000', 'Bohol', '07', '0712');
INSERT INTO `refprovince` VALUES ('40', '072200000', 'Cebu', '07', '0722');
INSERT INTO `refprovince` VALUES ('41', '074600000', 'Negros Oriental', '07', '0746');
INSERT INTO `refprovince` VALUES ('42', '076100000', 'Siquijor', '07', '0761');
INSERT INTO `refprovince` VALUES ('43', '082600000', 'Eastern Samar', '08', '0826');
INSERT INTO `refprovince` VALUES ('44', '083700000', 'Leyte', '08', '0837');
INSERT INTO `refprovince` VALUES ('45', '084800000', 'Northern Samar', '08', '0848');
INSERT INTO `refprovince` VALUES ('46', '086000000', 'Samar (Western Samar)', '08', '0860');
INSERT INTO `refprovince` VALUES ('47', '086400000', 'Southern Leyte', '08', '0864');
INSERT INTO `refprovince` VALUES ('48', '087800000', 'Biliran', '08', '0878');
INSERT INTO `refprovince` VALUES ('49', '097200000', 'Zamboanga Del Norte', '09', '0972');
INSERT INTO `refprovince` VALUES ('50', '097300000', 'Zamboanga Del Sur', '09', '0973');
INSERT INTO `refprovince` VALUES ('51', '098300000', 'Zamboanga Sibugay', '09', '0983');
INSERT INTO `refprovince` VALUES ('52', '099700000', 'Isabela', '09', '0997');
INSERT INTO `refprovince` VALUES ('53', '101300000', 'Bukidnon', '10', '1013');
INSERT INTO `refprovince` VALUES ('54', '101800000', 'Camiguin', '10', '1018');
INSERT INTO `refprovince` VALUES ('55', '103500000', 'Lanao Del Norte', '10', '1035');
INSERT INTO `refprovince` VALUES ('56', '104200000', 'Misamis Occidental', '10', '1042');
INSERT INTO `refprovince` VALUES ('57', '104300000', 'Misamis Oriental', '10', '1043');
INSERT INTO `refprovince` VALUES ('58', '112300000', 'Davao Del Norte', '11', '1123');
INSERT INTO `refprovince` VALUES ('59', '112400000', 'Davao Del Sur', '11', '1124');
INSERT INTO `refprovince` VALUES ('60', '112500000', 'Davao Oriental', '11', '1125');
INSERT INTO `refprovince` VALUES ('61', '118200000', 'Compostela Valley', '11', '1182');
INSERT INTO `refprovince` VALUES ('62', '118600000', 'Davao Occidental', '11', '1186');
INSERT INTO `refprovince` VALUES ('63', '124700000', 'Cotabato (North Cotabato)', '12', '1247');
INSERT INTO `refprovince` VALUES ('64', '126300000', 'South Cotabato', '12', '1263');
INSERT INTO `refprovince` VALUES ('65', '126500000', 'Sultan Kudarat', '12', '1265');
INSERT INTO `refprovince` VALUES ('66', '128000000', 'Sarangani', '12', '1280');
INSERT INTO `refprovince` VALUES ('67', '129800000', 'Cotabato City', '12', '1298');
INSERT INTO `refprovince` VALUES ('69', '133900000', 'Manila', '13', '1339');
INSERT INTO `refprovince` VALUES ('70', '137400000', 'NCR, Second District', '13', '1374');
INSERT INTO `refprovince` VALUES ('71', '137500000', 'NCR, Third District', '13', '1375');
INSERT INTO `refprovince` VALUES ('72', '137600000', 'NCR, Fourth District', '13', '1376');
INSERT INTO `refprovince` VALUES ('73', '140100000', 'Abra', '14', '1401');
INSERT INTO `refprovince` VALUES ('74', '141100000', 'Benguet', '14', '1411');
INSERT INTO `refprovince` VALUES ('75', '142700000', 'Ifugao', '14', '1427');
INSERT INTO `refprovince` VALUES ('76', '143200000', 'Kalinga', '14', '1432');
INSERT INTO `refprovince` VALUES ('77', '144400000', 'Mountain Province', '14', '1444');
INSERT INTO `refprovince` VALUES ('78', '148100000', 'Apayao', '14', '1481');
INSERT INTO `refprovince` VALUES ('79', '150700000', 'Basilan', '15', '1507');
INSERT INTO `refprovince` VALUES ('80', '153600000', 'Lanao Del Sur', '15', '1536');
INSERT INTO `refprovince` VALUES ('81', '153800000', 'Maguindanao', '15', '1538');
INSERT INTO `refprovince` VALUES ('82', '156600000', 'Sulu', '15', '1566');
INSERT INTO `refprovince` VALUES ('83', '157000000', 'Tawi-Tawi', '15', '1570');
INSERT INTO `refprovince` VALUES ('84', '160200000', 'Agusan Del Norte', '16', '1602');
INSERT INTO `refprovince` VALUES ('85', '160300000', 'Agusan Del Sur', '16', '1603');
INSERT INTO `refprovince` VALUES ('86', '166700000', 'Surigao Del Norte', '16', '1667');
INSERT INTO `refprovince` VALUES ('87', '166800000', 'Surigao Del Sur', '16', '1668');
INSERT INTO `refprovince` VALUES ('88', '168500000', 'Dinagat Islands', '16', '1685');
