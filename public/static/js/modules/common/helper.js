define([
  'jquery',
  'underscore'
], function(
  jq,
  _
) {
  'use strict';

  function getTemplate(template, data) {
    if (typeof data !== 'object') {
      data = {};
    }

    return jq(_.template(template)(data));
  }

  return {
    getTemplate: getTemplate
  }
});
