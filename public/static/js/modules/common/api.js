define([
  'jquery',
], function(jq) {
  'use strict';

  function checkFinancialTemplate(entityId) {
    return jq.get(window.BaseURL + '/api/check_fs', { id: entityId })
      .then(function(response) {
        if (!response) {
          var payment_upload = $('#debtcalculatorerror').val();
          throw new Error(
            payment_upload
          );
        }

        return response;
      });
  }

  function saveDscr(data) {
    return jq.post(
      window.BaseURL + '/api/save_entity_dscr',
      {
        entity_id: data.entity_id,
        dscr: data.dscr,
        net_operating_income: data.net_operating_income,
        loan_duration: data.loan_duration,
        loan_amount: data.loan_amount,
        working_interest_rate_local: data.working_interest_rate_local,
        working_interest_rate_foreign: data.working_interest_rate_foreign,
        term_interest_rate_local: data.term_interest_rate_local,
        term_interest_rate_foreign: data.term_interest_rate_foreign,
        is_custom: data.is_custom,
        source: data.source,
        source_amount: parseFloat($('#ds_noi_value').val()),
        _token: $('meta[name="csrf-token"]').attr('content')
      })
      .then(function(response) {
        if(parseFloat($('#ds_noi_value').val()) < 1){
          $('#inadvisable_result').show();
          $('#ds_loan_value').val(0.00);
  
          $('#WCLocalValuePeriod').val('N/A');
          $('#WCLocalValuePayment').val('N/A');
          $('#WCLocalValueTotalPayment').val('N/A');

          $('#WCForeignValuePeriod').val('N/A');
          $('#WCForeignValuePayment').val('N/A');
          $('#WCForeignValueTotalPayment').val('N/A');

          $('#TLLocalValuePeriod').html('N/A')
          $('#TLLocalValuePayment').html('N/A')
          $('#TLLocalValueTotalPayment').html('N/A')

          $('#TLForeignValuePeriod').html('N/A')
          $('#TLForeignValuePayment').html('N/A')
          $('#TLForeignValueTotalPayment').html('N/A')
        }else{
          $('#inadvisable_result').hide();
        }
        // $("#export_dscr_rating").load(location.href+" #export_dscr_rating>*","");
      })
  }

  function updateEntityExportFlag(entityId) {
    return jq.post(
      window.BaseURL + '/exportdscrfinal/' + entityId, 
      {
          _token: $('meta[name="csrf-token"]').attr('content')
      })
      .then(function(response) {
        //console.log(response);
        //let data = JSON.parse(response);

        //console.log(data);
        //window.location = window.BaseURL + 'summary/' + entityId + '/' + data.loginid + '/' + data.company_name;
;	
		//alert(responseUrl);

		window.location.reload();
      })
  }

  return {
    checkFinancialTemplate: checkFinancialTemplate,
    saveDscr: saveDscr,
    updateEntityExportFlag: updateEntityExportFlag,
  };
});
