define([
  'jquery',
  'common/helper',
  'text!templates/valid-user-statistics/main.html'
], function(jq, Helper, MainTemplate) {
  'use strict';

  var template;
  var selectors = {
    parentContainer: '[data-valid-user-statistics]'
  };

  function render() {
    template = Helper.getTemplate(MainTemplate);
    jq(selectors.parentContainer).html(template);
  }

  function initialize() {
    render();
  }

  return {
    initialize: initialize
  };
});
