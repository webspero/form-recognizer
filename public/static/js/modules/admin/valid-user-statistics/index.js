define([
  'admin/valid-user-statistics/components/main',
], function(validUserStatistics) {
  'use strict';

  function initialize() {
    validUserStatistics.initialize();
  }

  return {
    initialize: initialize
  };
});
