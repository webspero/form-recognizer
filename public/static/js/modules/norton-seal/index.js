define([
], function() {
  'use strict';
  function initialize() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = 'https://seal.websecurity.norton.com/getseal?host_name=business.creditbpo.com&size=S&use_flash=NO&use_transparent=NO&lang=en';

    var elem = document.getElementById('norton-seal');
    elem.parentNode.insertBefore(script, elem);
  }

  return {
    initialize: initialize,
  };
});
