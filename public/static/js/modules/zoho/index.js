define([
], function() {
  'use strict';
  function initialize() {
    window.$zoho = window.$zoho || {};
    window.$zoho.salesiq = window.$zoho.salesiq ||
      {
          widgetcode:"a3be2f22ef702a5cd72d9cafbb9153e72b49049dd2265b781f143e083ee772e6",
          values:{},
          ready:function() {},
      };

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.id = 'zsiqscript';
    script.async = true;
    script.src = 'https://salesiq.zoho.com/widget';

    var placeHolder = document.createElement('div');
    placeHolder.id = 'zsiqwidget';

    document.body.appendChild(placeHolder);
    var firstScript = document.getElementsByTagName('script')[0];
    firstScript.parentNode.insertBefore(script,firstScript);

    window.$zoho.salesiq.ready = function() {
      window.$zoho.salesiq.visitor.chat(function(visitid, data) {
        $.ajax({
          url: window.BaseURL + '/pipedrive/add-person',
          type: 'post',
          data: data
        });
      });

      window.$zoho.salesiq.visitor.offlineMessage(function(visitid, data) {
        $.ajax({
          url: window.BaseURL + '/pipedrive/add-person',
          type: 'post',
          data: data
        });
      });
    };
  }

  return {
    initialize: initialize,
  };
});
