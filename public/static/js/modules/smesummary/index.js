define([
  'smesummary/components/dscr-financing',
  'smesummary/components/dscr-procurement'
], function(dscrFinancing, dscrProcurement) {
  'use strict';

  function initialize(entityId, isFinancing) {
    var mod = isFinancing ? dscrFinancing : dscrProcurement;
    mod.initialize(entityId);
  }

  return {
    initialize: initialize
  };
});
