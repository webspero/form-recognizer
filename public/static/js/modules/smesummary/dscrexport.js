define([
    'smesummary/components/dscr-financing-rs',
    'smesummary/components/dscr-procurement-rs'
  ], function(dscrFinancing, dscrProcurement) {
    'use strict';
  
    function initialize(entityId, isFinancing) {
      var mod = isFinancing ? dscrFinancing : dscrProcurement;
      mod.initialize(entityId);
    }
  
    return {
      initialize: initialize
    };
  });
  