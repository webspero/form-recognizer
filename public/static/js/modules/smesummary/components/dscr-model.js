define([
  'jquery',
  'math',
], function(jq, MathHelper) {
  'use strict';

  var initialData;
  var accountsPayableTurnover;
  var bankInterestRates = {
    local: {
      low: 0,
      mid: 0,
      high: 0
    },
    foreign: {
      low: 0,
      mid: 0,
      high: 0
    }
  };
  var customBankInterestRates = [
    { name: 'WCLocalValue', value: 0 },
    { name: 'WCForeignValue', value: 0 },
    { name: 'TLLocalValue', value: 0 },
    { name: 'TLForeignValue', value: 0 }
  ];
  var ccc = 0;
  var currentProvisionsForEmployeeBenefits = [];
  var costOfSales = [];
  var debtService = 0;
  var dscrc = 0;
  var dscrData = {
    dscr: 0,
    is_custom: 0,
    loan_amount: 0,
    loan_duration: 0,
    net_operating_income: 0,
    payment_per_period_foreign: 0,
    payment_per_period_local: 0,
    period: 0,
    working_interest_rate_local: 0,
    working_interest_rate_foreign: 0,
    term_interest_rate_local: 0,
    term_interest_rate_foreign: 0
  };
  var interestExpense = 0;
  var interestExpenses = [];
  var inventories = [];
  var inventoryTurnover = 0;
  var isCustomData = false;
  var loanAmount = 0;
  var noiNoInterestExpense = 0;
  var noiStatic = 0;
  var payableTurnover = 0;
  var receivablesTurnover = 0;
  var revenue = [];
  var tradeAndOtherCurrentReceivables = [];
  var tradeAndOtherCurrentPayables = [];
  var sources = {};
  var source;
  var events = {
    onCalculateDscr: function() {},
    onChangeLoanAmount: function() {},
    onChangePaymentPeriod: function() {},
    onChangePeriod: function() {},
    onChangeTotalPayment: function() {}
  };
  var procReceivablesTurnover;
  var procInventoryTurnover;
  var assetTurnover;
  var currentAssetTurnover;
  var capitalTurnover;
  var cashConversionCycle;

  function number_format(number, decimals, dec_point, thousands_sep) {
    return MathHelper.number_format(number, decimals, dec_point, thousands_sep);
  }

  function setBankInterestRates(rates) {
    bankInterestRates = {
      local: {
        low: rates.local.low,
        mid: rates.local.mid,
        high: rates.local.high
      },
      foreign: {
        low: rates.foreign.low,
        mid: rates.foreign.mid,
        high: rates.foreign.high
      }
    };
  }

  function getBankInterestRates() {
    return bankInterestRates;
  }

  function getCurrentBankInterestRates() {
    if (getCustomData()) {
      return getCustomBankInterestRates();
    }

    return getBankInterestRates();
  }

  function setCustomBankInterestRates(rates) {
    if (!rates) {
      return;
    }

    customBankInterestRates = [
      { name: 'WCLocalValue', value: rates.working_interest_rate_local },
      { name: 'WCForeignValue', value: rates.working_interest_rate_foreign },
      { name: 'TLLocalValue', value: rates.term_interest_rate_local },
      { name: 'TLForeignValue', value: rates.term_interest_rate_foreign },
    ];
  }

  function getCustomBankInterestRates() {
    return customBankInterestRates;
  }

  function setLoanAmount() {
    var loanAmountSource = jq('#ds_loan_amount_source').text();
    loanAmount = jq.trim(loanAmountSource)

    if (!loanAmount) {
      loanAmount = 0;
    }
  }

  function getLoanAmount() {
    return loanAmount;
  }

  function setDebtService(data) {
    debtService = parseFloat(data.cash_flow[0].DebtServiceCapacity);
  }

  function getDebtService() {
    return debtService;
  }

  function setNoiNoInterestExpense(data) {
    noiNoInterestExpense = parseFloat(getNoiStatic()) +
      parseFloat(data.cash_flow[0].InterestExpense);
  }

  function getNoiNoInterestExpense() {
    return noiNoInterestExpense;
  }

  function setNoiStatic(data) {
    if (parseFloat(dscrData.net_operating_income)) {
      noiStatic = parseFloat(dscrData.net_operating_income);
      return;
    }

    noiStatic = data.cash_flow[0].NetOperatingIncome;
  }

  function setNoiStaticSource(data) {
    noiStatic = parseFloat(data);
  }

  function getNoiStatic() {
    return noiStatic;
  }

  function setInterestExpense(data) {
    interestExpense = parseFloat(data.cash_flow[0].InterestExpense);
  }

  function getInterestExpense() {
    return interestExpense;
  }

  function setTradeAndOtherCurrentReceivables(data) {
    tradeAndOtherCurrentReceivables = [
      data.balance_sheets[0].TradeAndOtherCurrentReceivables,
      data.balance_sheets[1].TradeAndOtherCurrentReceivables
    ];
  }

  function getTradeAndOtherCurrentReceivables() {
    return tradeAndOtherCurrentReceivables;
  }

  function setTradeAndOtherCurrentPayables(data) {
    tradeAndOtherCurrentPayables = [
      data.balance_sheets[0].TradeAndOtherCurrentPayables,
      data.balance_sheets[1].TradeAndOtherCurrentPayables
    ];
  }

  function getTradeAndOtherCurrentPayables() {
    return tradeAndOtherCurrentPayables;
  }

  function setCurrentProvisionsForEmployeeBenefits(data) {
    currentProvisionsForEmployeeBenefits = [
      data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits,
      data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits
    ];
  }

  function getCurrentProvisionsForEmployeeBenefits() {
    return currentProvisionsForEmployeeBenefits;
  }

  function setCostOfSales(data) {
    costOfSales = [
      data.income_statements[0].CostOfSales,
      data.income_statements[1].CostOfSales
    ];
  }

  function getCostOfSales() {
    return costOfSales;
  }

  function setRevenue(data) {
    revenue = [
      data.income_statements[0].Revenue,
      data.income_statements[1].Revenue,
    ];
  }

  function getRevenue() {
    return revenue;
  }

  function setInterestExpenses(data) {
    interestExpenses = [
      data.cash_flow[0].InterestExpense,
      data.cash_flow[1].InterestExpense
    ];
  }

  function getInterestExpenses() {
    return interestExpenses;
  }

  function setInventories(data) {
    inventories = [
      data.balance_sheets[0].Inventories,
      data.balance_sheets[1].Inventories
    ];
  }

  function getInventories() {
    return inventories;
  }

  function getLoanDuration() {
    return dscrData.loan_duration;
  }

  function setDscrc(data) {
    if (dscrData.dscr) {
      dscrc = dscrData.dscr;
      return dscrData.dscr;
    }

    if (
      typeof data.debt_service !== 'undefined' &&
      data.debt_service &&
      typeof data.debt_service.dscr !== 'undefined'
    ) {
      dscrc = data.debt_service.dscr;
      return;
    }

    dscrc = (noiNoInterestExpense - interestExpense) / debtService;

    if (isNaN(dscrc) || !isFinite(dscrc) || dscrc <= 0) {
      dscrc = 1.2;
    }
  }

  function getDscrc() {
    return dscrc;
  }

  function setDscrcStatic(data) {
    dscrc = data;
  }

  function setReceivablesTurnover(data) {
    var trade = getTradeAndOtherCurrentReceivables();
    var rev = getRevenue();

    receivablesTurnover = (
      (
        parseFloat(data.balance_sheets[0].TradeAndOtherCurrentReceivables) +
        parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables)
      ) / 2
    ) / (parseFloat(data.income_statements[0].Revenue) / 365);

    receivablesTurnover = isNaN(receivablesTurnover) ? 0 : receivablesTurnover;
  }

  function getReceivablesTurnover() {
    return receivablesTurnover;
  }

  function setPayableTurnover(data) {
    payableTurnover = (
      (
        parseFloat(data.balance_sheets[0].TradeAndOtherCurrentPayables) +
        parseFloat(data.balance_sheets[1].TradeAndOtherCurrentPayables) +
        parseFloat(data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits
      ) + parseFloat(data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits)) / 2) /
      (
        (
          parseFloat(data.income_statements[0].CostOfSales) +
          parseFloat(data.balance_sheets[0].Inventories) -
          parseFloat(data.balance_sheets[1].Inventories)
        ) / 365
      );
    payableTurnover = isNaN(payableTurnover) ? 0 : payableTurnover;
  }

  function getPayableTurnover() {
    return payableTurnover;
  }

  function setInventoryTurnover(data) {
    inventoryTurnover = (
      (
        parseFloat(data.balance_sheets[0].Inventories) +
        parseFloat(data.balance_sheets[1].Inventories)
      ) / 2
    ) / (parseFloat(data.income_statements[0].CostOfSales) / 365);
    inventoryTurnover = isNaN(procInventoryTurnover) ? 0 : procInventoryTurnover;
  }

  function getInventoryTurnover() {
    return inventoryTurnover;
  }

  function setCCC() {
    ccc = getInventoryTurnover() + getReceivablesTurnover() - getPayableTurnover();
    ccc = (ccc < 0) ? ccc * -1 : ccc;
  }

  function getCCC() {
    return ccc;
  }

  function setProcurementReceivablesTurnover(data) {
    procReceivablesTurnover = [0, 0];

    if (
      data.income_statements[0].Revenue > 0 &&
      typeof data.balance_sheets[1] !== 'undefined'
    ) {
      var acp = parseFloat(data.balance_sheets[0].TradeAndOtherCurrentReceivables) + parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables);
      var adp = parseFloat(data.income_statements[0].Revenue) / 365;

      procReceivablesTurnover[0] = (acp / 2) / adp;
    }

    if (
      data.income_statements[1].Revenue > 0 &&
      typeof data.balance_sheets[2] !== 'undefined'
    ) {

      var acp = parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables) + parseFloat(data.balance_sheets[2].TradeAndOtherCurrentReceivables);
      var adp = parseFloat(data.income_statements[1].Revenue) / 365;

      procReceivablesTurnover[1] = (acp / 2) / adp;
    }
  }

  function getProcurementReceivablesTurnover() {
    return procReceivablesTurnover;
  }

  function setAccountsPayableTurnover(data) {
    accountsPayableTurnover = [0, 0];

    for (var i = 0; i < 2; i++) {
      var inv = typeof data.balance_sheets[i + 1] !== 'undefined'
        ? parseFloat(data.balance_sheets[i + 1].Inventories)
        : 0;

      if (
        typeof data.income_statements[i] === 'undefined' ||
        typeof data.balance_sheets[i] === 'undefined' ||
        (
          parseFloat(data.income_statements[i].CostOfSales) +
          parseFloat(data.balance_sheets[i].Inventories) -
          inv
        ) == 0
      ) {
        continue;
      }

      var tcp = 0;
      var cpe = 0;

      if (typeof data.balance_sheets[i + 1] !== 'undefined') {
        tcp = parseFloat(data.balance_sheets[i + 1].TradeAndOtherCurrentPayables);
        cpe = parseFloat(data.balance_sheets[i + 1].CurrentProvisionsForEmployeeBenefits);
      }

      var atc = 0;
      var adr = 0;
      
      atc = parseFloat(data.balance_sheets[i].TradeAndOtherCurrentPayables) + parseFloat(data.balance_sheets[i].CurrentProvisionsForEmployeeBenefits);
      adr = (parseFloat(data.income_statements[i].CostOfSales) + parseFloat(data.balance_sheets[i].Inventories)) - inv;

      accountsPayableTurnover[i] = (atc / 2) / (adr / 365);

    }
  }

  function getAccountsPayableTurnover() {
    return accountsPayableTurnover;
  }

  function setProcInventoryTurnover(data) {
    procInventoryTurnover = [0, 0];

    for (var i = 0; i < 2; i++) {
      if (
        typeof data.income_statements[i] === 'undefined' ||
        data.income_statements[i].CostOfSales == 0
      ) {
        continue;
      }

      var inv = typeof data.balance_sheets[i + 1] !== 'undefined'
        ? parseFloat(data.balance_sheets[i + 1].Inventories)
        : 0;

      var ai = (parseFloat(data.balance_sheets[i].Inventories) + inv) / 2;
      var adc = parseFloat(data.income_statements[i].CostOfSales) / 365;

      procInventoryTurnover[i] = ai / adc;

    }
  }

  function getProcInventoryTurnover() {
    return procInventoryTurnover;
  }

  function setAssetTurnover(data) {
    assetTurnover = [0, 0];

    for (var i = 0; i < 2; i++) {
      if (
        typeof data.income_statements[i].Revenue === 'undefined' ||
        data.income_statements[i].Revenue == 0
      ) {
        continue;
      }

      var asset = typeof data.balance_sheets[i + 1] !== 'undefined'
        ? parseFloat(data.balance_sheets[i + 1].Assets)
        : 0;

      var ata = (parseFloat(data.balance_sheets[i].Assets) + asset) / 2;
      var adr = parseFloat(data.income_statements[i].Revenue) / 365;

      assetTurnover[i] = ata / adr;
    }
  }

  function getAssetTurnover() {
    return assetTurnover;
  }

  function setCurrentAssetTurnover(data) {
    currentAssetTurnover = [0, 0];

    for (var i = 0; i < 2; i++) {
      if (
        typeof data.income_statements[i].Revenue === 'undefined' ||
        data.income_statements[i].Revenue == 0
      ) {
        continue;
      }

      var asset = typeof data.balance_sheets[i + 1] !== 'undefined'
        ? parseFloat(data.balance_sheets[i + 1].CurrentAssets)
        : 0;

      var aca = (parseFloat(data.balance_sheets[i].CurrentAssets) + asset) / 2;
      var adr = parseFloat(data.income_statements[i].Revenue) / 365;

      currentAssetTurnover[i] = aca / adr;

    }
  }

  function getCurrentAssetTurnover() {
    return currentAssetTurnover;
  }

  function setCapitalTurnover(data) {
    capitalTurnover = [0, 0];

    for (var i = 0; i < 2; i++) {
      if (
        typeof data.income_statements[i].Revenue === 'undefined' ||
        data.income_statements[i].Revenue == 0
      ) {
        continue;
      }

      var equity = typeof data.balance_sheets[i + 1] !== 'undefined'
        ? parseFloat(data.balance_sheets[i + 1].Equity)
        : 0;

      var ae = (parseFloat(data.balance_sheets[i].Equity) + equity) / 2;
      var adr = parseFloat(data.income_statements[i].Revenue) / 365;

      capitalTurnover[i] = ae / adr;
    }
  }

  function getCapitalTurnover() {
    return capitalTurnover;
  }

  function setCashConversionCycle(data) {
    cashConversionCycle = [0, 0];
    var rt = getProcurementReceivablesTurnover();
    var it = getProcInventoryTurnover();
    var pt = getAccountsPayableTurnover();

    for (var i = 0; i < 2; i++) {
      cashConversionCycle[i] = rt[i] + it[i] - pt[i];
    }
  }

  function getCashConversionCycle() {
    return cashConversionCycle;
  }

  function setVars(data) {
    var custom = typeof data.debt_service !== 'undefined' &&
      data.debt_service &&
      data.debt_service.is_custom;

    setCustomData(custom);
    setInitialData(data);
    setLoanAmount();
    setDebtService(data);
    setSources(data.sources);
    setSource(data);
    setNoiStatic(data);
    setNoiNoInterestExpense(data);
    setInterestExpense(data);
    setTradeAndOtherCurrentReceivables(data);
    setTradeAndOtherCurrentPayables(data);
    setCurrentProvisionsForEmployeeBenefits(data);
    setCostOfSales(data);
    setRevenue(data);
    setInterestExpenses(data);
    setInventories(data);
    setReceivablesTurnover(data);
    setPayableTurnover(data);
    setInventoryTurnover(data);
    setCCC();
    setCustomBankInterestRates(data.debt_service);
    setProcurementReceivablesTurnover(data);
    setAccountsPayableTurnover(data);
    setProcInventoryTurnover(data);
    setCurrentAssetTurnover(data);
    setAssetTurnover(data);
    setCapitalTurnover(data);
    setCashConversionCycle(data);
    setDscrc(data);
  }

  function getVars() {
    return {
      ccc: getCCC(),
      costOfSales: getCostOfSales(),
      currentProvisionsForEmployeeBenefits: getCurrentProvisionsForEmployeeBenefits(),
      debtService: getDebtService(),
      dscrc: getDscrc(),
      loanAmount: getLoanAmount(),
      loanDuration: getLoanDuration(),
      interestExpense: getInterestExpense(),
      interestExpenses: getInterestExpenses(),
      inventories: getInventories(),
      inventoryTurnover: getInventoryTurnover(),
      noiNoInterestExpense: getNoiNoInterestExpense(),
      noiStatic: getNoiStatic(),
      payableTurnover: getPayableTurnover(),
      tradeAndOtherCurrentReceivables: getTradeAndOtherCurrentReceivables(),
      tradeAndOtherCurrentPayables: getTradeAndOtherCurrentPayables(),
      receivablesTurnover: getReceivablesTurnover(),
      revenue: getRevenue(),
      source: getSource(),
      sources: getSources(),
      procReceivablesTurnover: getProcurementReceivablesTurnover(),
      accountsPayableTurnover: getAccountsPayableTurnover(),
      procInventoryTurnover: getProcInventoryTurnover(),
      assetTurnover: getAssetTurnover(),
      currentAssetTurnover: getCurrentAssetTurnover(),
      capitalTurnover: getCapitalTurnover(),
      cashConversionCycle: getCashConversionCycle()
    };
  }

  function getDscrDuration(monthDuration = 0) {
    var tlLocal;
    var tlForeign;
    var wcLocal;
    var wcForeign;

    if (getCustomData()) {
      return getCustomBankInterestRates();
    }

    var interestRates = getBankInterestRates();

    if (monthDuration > 60) {
      tlLocal = bankInterestRates.local.high;
      tlForeign = bankInterestRates.foreign.high;
    } else {
      tlLocal = bankInterestRates.local.mid;
      tlForeign = bankInterestRates.foreign.mid;
    }

    wcLocal = bankInterestRates.local.low;
    wcForeign = bankInterestRates.foreign.low;

    return [
      { name: 'WCLocalValue', value: wcLocal },
      { name: 'WCForeignValue', value: wcForeign },
      { name: 'TLLocalValue', value: tlLocal },
      { name: 'TLForeignValue', value: tlForeign }
    ];
  }

  function setDscrData(data) {
    Object.assign(dscrData, data);
  }

  function getDscrData() {
    return dscrData;
  }

  function setCustomData(isCustom) {
    isCustomData = isCustom;
  }

  function getCustomData() {
    return isCustomData;
  }

  function calculateDscr(monthDuration, dscr) {
    var data = getDscrDuration(monthDuration);

    for (x in data) {
      var IR = data[x].value;
      var AIR = IR / 100;
      var MIR = AIR / 12;

      if (x < 2) {
        var Period = getCCC();
        var Annualization = 365;
      } else {
        var Period = 30;
        var Annualization = 360;
      }

      var NOI = getNoiStatic();
      var IE = getNoiNoInterestExpense() - getInterestExpense() - NOI;
      var pmt = NOI / dscr;
      var LA = monthDuration * pmt / 12;
      var MonthlyPayment = (LA + (LA * AIR * Period / Annualization)) / pmt;
      var PaymentPerPeriod = 0;

      if (0 == x) {
        onChangeLoanAmount(number_format(LA, 2, '.', ','))
      }

      // working capital
      if (12 >= monthDuration) {
        var Interest = LA * AIR * getCCC() / Annualization;
        var PaymentPerPeriod = (LA + Interest) / monthDuration;
        var InterestPerPeriod = Interest;
        var TotalPayment = Interest * monthDuration;

        if (isNaN(getCCC()) === true) {
          onChangePeriod(data[x].name, 'N/A');
        } else {
          var parsedCCC = parseInt(getCCC());
          onChangePeriod(data[x].name, parsedCCC + ' Days');
        }

        if (InterestPerPeriod == 0 || isNaN(InterestPerPeriod)) {
          onChangePaymentPeriod(data[x].name, 'N/A');
        } else {
          onChangePaymentPeriod(data[x].name, number_format(InterestPerPeriod, 2, '.', ','));
        }

        if (TotalPayment == 0 || isNaN(TotalPayment)) {
          onChangeTotalPayment(data[x].name, 'N/A');
        } else {
          onChangeTotalPayment(data[x].name, number_format(TotalPayment, 2, '.', ','));
        }

        onChangePeriod(data[2].name, 'N/A');
        onChangePeriod(data[3].name, 'N/A');
        onChangePaymentPeriod(data[2].name, 'N/A');
        onChangePaymentPeriod(data[3].name, 'N/A');
        onChangeTotalPayment(data[2].name, 'N/A');
        onChangeTotalPayment(data[3].name, 'N/A');
      } else { // term loan
        var nump1 = -Math.log(1 - (AIR * LA / pmt));
        var nump2  = Math.log(1 + AIR);
        var num_payments = nump1 / nump2;
        var TotalPayment = (LA + (LA * AIR * Period / Annualization));
        var PaymentPerPeriod = TotalPayment / monthDuration;

        if (isNaN(MonthlyPayment) === true || MonthlyPayment === 0) {
          onChangePeriod(data[x].name, 'N/A');
        } else {
          onChangePeriod(data[x].name, '1 Month');
        }

        if (
          isNaN(PaymentPerPeriod) ||
          number_format(PaymentPerPeriod, 2, '.', ',') === '0.00'
        ) {
          onChangePaymentPeriod(data[x].name, 'N/A');
        } else {
          onChangePaymentPeriod(data[x].name, number_format(PaymentPerPeriod, 2, '.', ','));
        }

        if (TotalPayment == 0 || isNaN(TotalPayment)) {
          onChangeTotalPayment(data[x].name, 'N/A');
        } else {
          onChangeTotalPayment(data[x].name, number_format(TotalPayment, 2, '.', ','));
        }

        onChangePeriod(data[0].name, 'N/A');
        onChangePeriod(data[1].name, 'N/A');
        onChangePaymentPeriod(data[0].name, 'N/A');
        onChangePaymentPeriod(data[1].name, 'N/A');
        onChangeTotalPayment(data[0].name, 'N/A');
        onChangeTotalPayment(data[1].name, 'N/A');
      }
    }

    onCalculateDscr(data);
  }

  function setEvents(eventList) {
    Object.assign(events, eventList);
  }

  function onChangeLoanAmount(value) {
    if (typeof events.onChangeLoanAmount === 'function') {
      events.onChangeLoanAmount(value);
    }
  }

  function onChangePeriod(name, value) {
    if (typeof events.onChangePeriod === 'function') {
      events.onChangePeriod(name, value);
    }
  }

  function onChangePaymentPeriod(name, value) {
    if (typeof events.onChangePaymentPeriod === 'function') {
      events.onChangePaymentPeriod(name, value);
    }
  }

  function onCalculateDscr(data) {
    if (typeof events.onCalculateDscr === 'function') {
      events.onCalculateDscr(data);
    }
  }

  function onChangeTotalPayment(name, value) {
    if (typeof events.onChangeTotalPayment === 'function') {
      events.onChangeTotalPayment(name, value);
    }
  }

  function setInitialData(data) {
    initialData = data;
  }

  function getInitialData() {
    return initialData;
  }

  function setSources(data) {
    sources = data;
  }

  function getSources() {
    return sources;
  }

  function setSource(data) {
    if (dscrData.source) {
      source = dscrData.source;
      return;
    }

    source = 'NetOperatingIncome';
  }

  function setStaticSource(data) {
    source = data;
  }

  function getSource() {
    return source;
  }

  function getSourceValue(name) {
    return sources[name];
  }

  function setSourceValue(name, value) {
    sources[name] = value;
  }

  return {
    calculateDscr: calculateDscr,
    setBankInterestRates: setBankInterestRates,
    getBankInterestRates: getBankInterestRates,
    setCustomBankInterestRates: setCustomBankInterestRates,
    getCustomBankInterestRates: getCustomBankInterestRates,
    setDscrData: setDscrData,
    getDscrData: getDscrData,
    setCustomData: setCustomData,
    getCustomData: getCustomData,
    setVars: setVars,
    getVars: getVars,
    setInitialData: setInitialData,
    getInitialData: getInitialData,
    getNoiStatic: getNoiStatic,
    setEvents: setEvents,
    getDscrDuration: getDscrDuration,
    getDscrc: getDscrc,
    setDscrcStatic: setDscrcStatic,
    getSourceValue: getSourceValue,
    setSourceValue: setSourceValue,
    setStaticSource: setStaticSource,
    getSource: getSource,
    setNoiStaticSource: setNoiStaticSource
  };
});
