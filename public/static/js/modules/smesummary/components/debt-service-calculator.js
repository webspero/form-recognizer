define([
  'jquery',
  'underscore',
  'trans',
  'common/api',
  'smesummary/components/debt-service-calculator-model',
  'text!templates/dscr/corporate.html',
  'bootstrapSlider'
], function(jq, _, trans, Api, DscrModel, MainTemplate) {
  'use strict';

  var bankInterestRates;
  var selectors = {
    dscrContainer: '#debt_service_calculator_container',
    dsrcNav: '#nav_pfs',
    dscrSlider: '#dscr_slider',
    dscrSliderDisplay: '#dscr_slider_display',
    dsLoan: '#ds_loan_value',
    dsTenor: '#ds_tenor_value'
  };
  var template;

  function bindTemplateEvents() {
    if (template) {
      DscrModel.setEvents({
        onCalculateDscr: function(data) {
          onCalculateDscr(data);
        },
        onChangeLoanAmount: function(value) {
          template.find(selectors.dsLoan).val(value);
        },
        onChangePaymentPeriod: function(name, value) {
          template.find('#' + name + 'Payment').html(value);
        },
        onChangePeriod: function(name, value) {
          template.find('#' + name + 'Period').html(value);
        }
      });

      template.find(selectors.dscrSlider)
        .bootstrapSlider({
          step: 0.1,
          formatter: function(value) {
            return value + 'x';
          }
        })
        .on('change', onSliderChange);

      template.find(selectors.dsTenor)
        .on('change keyup', function() {
          updateDscr();
        });

      template.find('#custom-interest-rate-btn')
        .on('click', function(event) {
          onToggleCustomFields(parseInt($(this).attr('data-value'), 10) !== 1);
        });
    }
  }

  function bindEvents() {
    jq(document).ready(function() {
      jq(selectors.dsrcNav).click(function(e) {
        e.preventDefault();
      });
    });

    bindTemplateEvents();
  }

  function onSliderChange(event) {
    template.find(selectors.dscrSliderDisplay)
      .text('DSCR: ' + event.value.newValue + 'x');
    updateDscr();
  }

  function getTemplate(data) {
    return jq(_.template(MainTemplate)(data));
  }

  function updateDscr() {
    var duration = template.find(selectors.dsTenor).val();
    var dscr = template.find(selectors.dscrSlider).bootstrapSlider('getValue');

    DscrModel.calculateDscr(duration, dscr);
  }

  function render(data) {
    DscrModel.setDscrData(data.debt_service);
    DscrModel.setVars(data);

    var dscrVars = DscrModel.getVars();
    var bankInterestRates = DscrModel.setBankInterestRates(
      data.bank_interest_rates
    );
    var templateData = {
      'trans': trans,
      'dscrVars': dscrVars,
      'year': data.financial_report.year
    };

    template = getTemplate(templateData);
    jq(selectors.dscrContainer).html(template);
  }

  function initialize(entityId) {
    Api.checkFinancialTemplate(entityId)
      .then(function(response) {
        render(response);
        bindEvents();
        var isCustom = typeof response.debt_service !== 'undefined' &&
          response.debt_service.is_custom;
        //onToggleCustomFields(isCustom);
      })
      .catch(function(error) {
        jq(selectors.dscrContainer)
          .html(error.message);
      });
  }

  function onCalculateDscr(data) {
    var localLow = template.find('#dscr-data-local-low');
    var localMid = template.find('#dscr-data-local-mid');
    var foreignLow = template.find('#dscr-data-foreign-low');
    var foreignMid = template.find('#dscr-data-foreign-mid');

    localLow.find('input').val(data[0].value);
    localLow.find('span').text(data[0].value);
    localMid.find('input').val(data[2].value);
    localMid.find('span').text(data[2].value);
    foreignLow.find('input').val(data[1].value);
    foreignLow.find('span').text(data[1].value);
    foreignMid.find('input').val(data[3].value);
    foreignMid.find('span').text(data[3].value);
  }

  function onToggleCustomFields(custom) {
    if (custom) {
      template.find('#custom-interest-rate-btn')
          .text(trans('rcl.restore_interest_rate'))
          .attr('data-value', 1)
          .removeClass('btn-primary')
          .addClass('btn-warning');
      template.find('#dscr-data-local-low span').hide();
      template.find('#dscr-data-foreign-low span').hide();
      template.find('#dscr-data-local-mid span').hide();
      template.find('#dscr-data-foreign-mid span').hide();
      template.find('#dscr-data-local-low input').show();
      template.find('#dscr-data-foreign-low input').show();
      template.find('#dscr-data-local-mid input').show();
      template.find('#dscr-data-foreign-mid input').show();
      updateDscr();
      return;
    }

    template.find('#custom-interest-rate-btn')
        .attr('data-value', 0)
        .text(trans('rcl.custom_interest_rate'))
        .removeClass('btn-warning')
        .addClass('btn-primary');
    template.find('#dscr-data-local-low span').show();
    template.find('#dscr-data-foreign-low span').show();
    template.find('#dscr-data-local-mid span').show();
    template.find('#dscr-data-foreign-mid span').show();
    template.find('#dscr-data-local-low input').hide();
    template.find('#dscr-data-foreign-low input').hide();
    template.find('#dscr-data-local-mid input').hide();
    template.find('#dscr-data-foreign-mid input').hide();
    template.find(selectors.dsTenor).val(0);
    template.find(selectors.dscrSlider)
      .bootstrapSlider('setValue', 1.2, true);
    updateDscr();
  }

  return {
    initialize: initialize
  };
});
