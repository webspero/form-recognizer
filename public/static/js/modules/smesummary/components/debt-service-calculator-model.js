define([
  'jquery'
], function(jq) {
  'use strict';

  var bankInterestRates = {
    local: {
      low: 0,
      mid: 0,
      high: 0
    },
    foreign: {
      low: 0,
      mid: 0,
      high: 0
    }
  };
  var customBankInterestRates = [
    { name: 'WCLocal', value: 0 },
    { name: 'WCForeign', value: 0 },
    { name: 'TLLocal', value: 0 },
    { name: 'TLForeign', value: 0 }
  ];
  var ccc = 0;
  var currentProvisionsForEmployeeBenefits = [];
  var costOfSales = [];
  var debtService = 0;
  var dscrc = 0;
  var dscrData = {
    dscr: 0,
    is_custom: 0,
    loan_amount: 0,
    loan_duration: 0,
    net_operating_income: 0,
    payment_per_period_foreign: 0,
    payment_per_period_local: 0,
    period: 0,
    working_interest_rate_local: 0,
    working_interest_rate_foreign: 0,
    term_interest_rate_local: 0,
    term_interest_rate_foreign: 0
  };
  var interestExpense = 0;
  var interestExpenses = [];
  var inventories = [];
  var inventoryTurnover = 0;
  var isCustomData = false;
  var loanAmount = 0;
  var noiNoInterestExpense = 0;
  var noiStatic = 0;
  var payableTurnover = 0;
  var receivablesTurnover = 0;
  var revenue = [];
  var tradeAndOtherCurrentReceivables = [];
  var tradeAndOtherCurrentPayables = [];
  var events = {
    onCalculateDscr: function() {},
    onChangeLoanAmount: function() {},
    onChangePaymentPeriod: function() {},
    onChangePeriod: function() {}
  };

  function setBankInterestRates(rates) {
    bankInterestRates = {
      local: {
        low: rates.local.low,
        mid: rates.local.mid,
        high: rates.local.high
      },
      foreign: {
        low: rates.foreign.low,
        mid: rates.foreign.mid,
        high: rates.foreign.high
      }
    };
  }

  function getBankInterestRates() {
    return bankInterestRates;
  }

  function setCustomBankInterestRates(rates) {
    customBankInterestRates = [
      { name: 'WCLocal', value: rates.working_interest_rate_local },
      { name: 'WCForeign', value: rates.working_interest_rate_foreign },
      { name: 'TLLocal', value: rates.term_interest_rate_local },
      { name: 'TLForeign', value: rates.term_interest_rate_foreign },
    ];
  }

  function getCustomBankInterestRates() {
    return customBankInterestRates;
  }

  function setLoanAmount() {
    var loanAmountSource = jq('#ds_loan_amount_source').text();
    loanAmount = jq.trim(loanAmountSource)

    if (!loanAmount) {
      loanAmount = 0;
    }
  }

  function getLoanAmount() {
    return loanAmount;
  }

  function setDebtService(data) {
    debtService = parseFloat(data.cash_flow[0].DebtServiceCapacity);
  }

  function getDebtService() {
    return debtService;
  }

  function setNoiNoInterestExpense(data) {
    noiNoInterestExpense = parseFloat(data.cash_flow[0].NetOperatingIncome) +
      parseFloat(data.cash_flow[0].InterestExpense);
  }

  function getNoiNoInterestExpense() {
    return noiNoInterestExpense;
  }

  function setNoiStatic(data) {
    noiStatic = data.cash_flow[0].NetOperatingIncome;
  }

  function getNoiStatic() {
    return noiStatic;
  }

  function setInterestExpense(data) {
    interestExpense = parseFloat(data.cash_flow[0].InterestExpense);
  }

  function getInterestExpense() {
    return interestExpense;
  }

  function setTradeAndOtherCurrentReceivables(data) {
    tradeAndOtherCurrentReceivables = [
      data.balance_sheets[0].TradeAndOtherCurrentReceivables,
      data.balance_sheets[1].TradeAndOtherCurrentReceivables
    ];
  }

  function getTradeAndOtherCurrentReceivables() {
    return tradeAndOtherCurrentReceivables;
  }

  function setTradeAndOtherCurrentPayables(data) {
    tradeAndOtherCurrentPayables = [
      data.balance_sheets[0].TradeAndOtherCurrentPayables,
      data.balance_sheets[1].TradeAndOtherCurrentPayables
    ];
  }

  function getTradeAndOtherCurrentPayables() {
    return tradeAndOtherCurrentPayables;
  }

  function setCurrentProvisionsForEmployeeBenefits(data) {
    currentProvisionsForEmployeeBenefits = [
      data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits,
      data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits
    ];
  }

  function getCurrentProvisionsForEmployeeBenefits() {
    return currentProvisionsForEmployeeBenefits;
  }

  function setCostOfSales(data) {
    costOfSales = [
      data.income_statements[0].CostOfSales,
      data.income_statements[1].CostOfSales
    ];
  }

  function getCostOfSales() {
    return costOfSales;
  }

  function setRevenue(data) {
    revenue = [
      data.income_statements[0].Revenue,
      data.income_statements[1].Revenue,
    ];
  }

  function getRevenue() {
    return revenue;
  }

  function setInterestExpenses(data) {
    interestExpenses = [
      data.cash_flow[0].InterestExpense,
      data.cash_flow[1].InterestExpense
    ];
  }

  function getInterestExpenses() {
    return interestExpenses;
  }

  function setInventories(data) {
    inventories = [
      data.balance_sheets[0].Inventories,
      data.balance_sheets[1].Inventories
    ];
  }

  function getInventories() {
    return inventories;
  }

  function getLoanDuration() {
    return dscrData.loan_duration;
  }

  function setDscrc(data) {
    if (
      typeof data.debt_service !== 'undefined' &&
      typeof data.debt_service.dscr !== 'undefined'
    ) {
      dscrc = data.debt_service.dscr;
      return;
    }

    if (dscrData.is_custom) {
      return dscrData.dscr;
    }

    dscrc = (noiNoInterestExpense - interestExpense) / debtService;

    if (isNaN(dscrc) || !isFinite(dscrc)) {
      dscrc = 0;
    }
  }

  function getDscrc() {
    return dscrc;
  }

  function setReceivablesTurnover(data) {
    var trade = getTradeAndOtherCurrentReceivables();
    var rev = getRevenue();

    receivablesTurnover = (
      (
        parseFloat(data.balance_sheets[0].TradeAndOtherCurrentReceivables) +
        parseFloat(data.balance_sheets[1].TradeAndOtherCurrentReceivables)
      ) / 2
    ) / (parseFloat(data.income_statements[0].Revenue) / 365);

    receivablesTurnover = isNaN(receivablesTurnover) ? 0 : receivablesTurnover;
  }

  function getReceivablesTurnover() {
    return receivablesTurnover;
  }

  function setPayableTurnover(data) {
    payableTurnover = (
      (
        parseFloat(data.balance_sheets[0].TradeAndOtherCurrentPayables) +
        parseFloat(data.balance_sheets[1].TradeAndOtherCurrentPayables) +
        parseFloat(data.balance_sheets[0].CurrentProvisionsForEmployeeBenefits
      ) + parseFloat(data.balance_sheets[1].CurrentProvisionsForEmployeeBenefits)) / 2) /
      (
        (
          parseFloat(data.income_statements[0].CostOfSales) +
          parseFloat(data.balance_sheets[0].Inventories) -
          parseFloat(data.balance_sheets[1].Inventories)
        ) / 365
      );
    payableTurnover = isNaN(payableTurnover) ? 0 : payableTurnover;
  }

  function getPayableTurnover() {
    return payableTurnover;
  }

  function setInventoryTurnover(data) {
    inventoryTurnover = (
      (
        parseFloat(data.balance_sheets[0].Inventories) +
        parseFloat(data.balance_sheets[1].Inventories)
      ) / 2
    ) / (parseFloat(data.income_statements[0].CostOfSales) / 365);
    inventoryTurnover = isNaN(inventoryTurnover) ? 0 : inventoryTurnover;
  }

  function getInventoryTurnover() {
    return inventoryTurnover;
  }

  function setCCC() {
    ccc = getInventoryTurnover() + getReceivablesTurnover() - getPayableTurnover();
    ccc = (ccc < 0) ? ccc * -1 : ccc;
  }

  function getCCC() {
    return ccc;
  }

  function setVars(data) {
    setLoanAmount();
    setDebtService(data);
    setNoiNoInterestExpense(data);
    setNoiStatic(data);
    setInterestExpense(data);
    setTradeAndOtherCurrentReceivables(data);
    setTradeAndOtherCurrentPayables(data);
    setCurrentProvisionsForEmployeeBenefits(data);
    setCostOfSales(data);
    setRevenue(data);
    setInterestExpenses(data);
    setInventories(data);
    setReceivablesTurnover(data);
    setPayableTurnover(data);
    setInventoryTurnover(data);
    setCCC();


    var custom = typeof data.debt_service !== 'undefined' && data.debt_service.is_custom;
    setCustomData(custom);

    if (custom) {
      setCustomBankInterestRates(data.debt_service);
    }

    setDscrc(data);
  }

  function getVars() {
    return {
      ccc: getCCC(),
      costOfSales: getCostOfSales(),
      currentProvisionsForEmployeeBenefits: getCurrentProvisionsForEmployeeBenefits(),
      debtService: getDebtService(),
      dscrc: getDscrc(),
      loanAmount: getLoanAmount(),
      loanDuration: getLoanDuration(),
      interestExpense: getInterestExpense(),
      interestExpenses: getInterestExpenses(),
      inventories: getInventories(),
      inventoryTurnover: getInventoryTurnover(),
      noiNoInterestExpense: getNoiNoInterestExpense(),
      noiStatic: getNoiStatic(),
      payableTurnover: getPayableTurnover(),
      tradeAndOtherCurrentReceivables: getTradeAndOtherCurrentReceivables(),
      tradeAndOtherCurrentPayables: getTradeAndOtherCurrentPayables(),
      receivablesTurnover: getReceivablesTurnover(),
      revenue: getRevenue()
    };
  }

  function getDscrDuration(monthDuration = 0) {
    var tlLocal;
    var tlForeign;
    var wcLocal;
    var wcForeign;

    if (getCustomData()) {
      return getCustomBankInterestRates();
    }

    var interestRates = getBankInterestRates();

    if (monthDuration > 60) {
      tlLocal = bankInterestRates.local.high;
      tlForeign = bankInterestRates.foreign.high;
    } else {
      tlLocal = bankInterestRates.local.mid;
      tlForeign = bankInterestRates.foreign.mid;
    }

    wcLocal = bankInterestRates.local.low;
    wcForeign = bankInterestRates.foreign.low;

    return [
      { name: 'WCLocalValue', value: wcLocal },
      { name: 'WCForeignValue', value: wcForeign },
      { name: 'TLLocalValue', value: tlLocal },
      { name: 'TLForeignValue', value: tlForeign }
    ];
  }

  function setDscrData(data) {
    Object.assign(dscrData, data);
  }

  function getDscrData() {
    return dscrData;
  }

  function setCustomData(isCustom) {
    isCustomData = isCustom;
  }

  function getCustomData() {
    return isCustomData;
  }

  function calculateDscr(monthDuration, dscr) {
    var data = getDscrDuration(monthDuration);

    for (x in data) {
      var IR = data[x].value;
      var AIR = IR / 100;
      var MIR = AIR / 12;

      if (x < 2) {
        var Period = getCCC();
        var Annualization = 365;
      } else {
        var Period = 30;
        var Annualization = 360;
      }

      var NOI = getNoiStatic();
      var IE = getNoiNoInterestExpense() - getInterestExpense() - NOI;
      var pmt = NOI / dscr;
      var LA = monthDuration * pmt / 12;
      var MonthlyPayment = (LA + (LA * AIR * 30 / Annualization)) / pmt;
      var PaymentPerPeriod = 0;

      if (0 == x) {
        onChangeLoanAmount(number_format(LA, 2, '.', ','))
      }

      // working capital
      if (12 >= monthDuration) {
        var PaymentPerPeriod = (LA + (LA * AIR * CCC / Annualization)) / monthDuration;

        if (isNaN(getCCC()) === true) {
          onChangePeriod(data[x].name, 'N/A');
        } else {
          var parsedCCC = parseInt(getCCC());
          onChangePeriod(data[x].name, parsedCCC + ' Days');
        }

        if (PaymentPerPeriod == 0 || isNaN(PaymentPerPeriod)) {
          onChangePaymentPeriod(data[x].name, 'N/A');
        } else {
          onChangePaymentPeriod(data[x].name, number_format(PaymentPerPeriod, 2, '.', ','));
        }

        onChangePeriod(data[2].name, 'N/A');
        onChangePeriod(data[3].name, 'N/A');
        onChangePaymentPeriod(data[2].name, 'N/A');
        onChangePaymentPeriod(data[3].name, 'N/A');
      } else { // term loan
        var nump1 = -Math.log(1 - (AIR * LA / pmt));
        var nump2  = Math.log(1 + AIR);
        var num_payments = nump1 / nump2;
        var PaymentPerPeriod = (LA + (LA * AIR * 30 / Annualization)) / monthDuration;

        if (isNaN(MonthlyPayment) === true || MonthlyPayment === 0) {
          onChangePeriod(data[x].name, 'N/A');
        } else {
          onChangePeriod(data[x].name, '1 Month');
        }

        if (
          isNaN(PaymentPerPeriod) ||
          number_format(PaymentPerPeriod, 2, '.', ',') === '0.00'
        ) {
          onChangePaymentPeriod(data[x].name, 'N/A');
        } else {
          onChangePaymentPeriod(data[x].name, number_format(PaymentPerPeriod, 2, '.', ','));
        }

        onChangePeriod(data[0].name, 'N/A');
        onChangePeriod(data[1].name, 'N/A');
        onChangePaymentPeriod(data[0].name, 'N/A');
        onChangePaymentPeriod(data[1].name, 'N/A');
      }
    }

    onCalculateDscr(data);
  }

  function setEvents(eventList) {
    Object.assign(events, eventList);
  }

  function onChangeLoanAmount(value) {
    if (typeof events.onChangeLoanAmount === 'function') {
      events.onChangeLoanAmount(value);
    }
  }

  function onChangePeriod(name, value) {
    if (typeof events.onChangePeriod === 'function') {
      events.onChangePeriod(name, value);
    }
  }

  function onChangePaymentPeriod(name, value) {
    if (typeof events.onChangePaymentPeriod === 'function') {
      events.onChangePaymentPeriod(name, value);
    }
  }

  function onCalculateDscr(data) {
    if (typeof events.onCalculateDscr === 'function') {
      events.onCalculateDscr(data);
    }
  }

  return {
    calculateDscr: calculateDscr,
    setBankInterestRates: setBankInterestRates,
    getBankInterestRates: getBankInterestRates,
    setCustomBankInterestRates: setCustomBankInterestRates,
    getCustomBankInterestRates: getCustomBankInterestRates,
    setDscrData: setDscrData,
    getDscrData: getDscrData,
    getDscrDuration: getDscrDuration,
    setVars: setVars,
    getVars: getVars,
    setEvents: setEvents
  };
});
