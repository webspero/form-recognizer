define([
    'jquery',
    'underscore',
    'trans',
    'math',
    'common/api',
    'smesummary/components/dscr-model',
    'text!templates/dscr/procurement-rs.html',
    'bootstrapSlider'
  ], function(jq, _, trans, MathHelper, Api, DscrModel, MainTemplate) {
    'use strict';
  
    var entity;
    var selectors = {
      dscrContainer: '#debt_service_calculator_rs_container',
      dsrcNav: '#nav_pfs',
      dscrSlider: '#dscr_slider',
      dscrSliderDisplay: '#dscr_slider_display',
      dsLoan: '#ds_loan_value',
      dsTenor: '#ds_tenor_value',
      dsSource: '#dscr-source',
      dsAmount: '#ds_noi_value'
    };
    var template;
  
    function bindTemplateEvents() {
      if (template) {
        DscrModel.setEvents({
          onChangeLoanAmount: function(value) {
            template.find(selectors.dsLoan).val(value);
          },
        });
  
        template.find(selectors.dscrSlider)
          .bootstrapSlider({
            step: 0.1,
            formatter: function(value) {
              return value + 'x';
            }
          })
          .on('change', onSliderChange);
  
        template.find(selectors.dsTenor)
          .on('change keyup', function() {
            updateDscr();
          });
  
        template.find(selectors.dsSource)
          .on('change', onChangeSource);
  
        template.find(selectors.dsAmount)
          .on('change', onChangeDsAmount);
  
        template.find('#print-computation-btn')
          .on('click', function() {
            window.print();
          });

        template.find('#pfsdetails')
        .on('click', function(event) {
          $("ul#side-menu li:contains('Rating Summary')").removeClass('active');
          $('ul#side-menu li a').removeClass('active');
          $("ul#side-menu li:contains('Required Credit Line')").find("a").addClass('active');
        });
  
        updateDscr();
      }
    }
  
    function bindEvents() {
      jq(document).ready(function() {
        jq(selectors.dsrcNav).click(function(e) {
          e.preventDefault();
        });
      });
  
      bindTemplateEvents();
    }
  
    function onSliderChange(event) {
      DscrModel.setDscrcStatic(event.value.newValue);
      template.find(selectors.dscrSliderDisplay)
        .text('DSCR: ' + event.value.newValue + 'x');
      updateDscr();
    }
  
    function onChangeSource() {
      var sourceVal = template.find(selectors.dsSource).val();
      var sourceAmount = DscrModel.getSourceValue(sourceVal);
      var parsedAmount = MathHelper.number_format(parseFloat(sourceAmount), 2, '.', ',')
      DscrModel.setNoiStaticSource(sourceAmount);
      DscrModel.setStaticSource(sourceVal);
      DscrModel.setSourceValue(sourceVal, sourceAmount);
      template.find(selectors.dsAmount).val(parsedAmount);
      template.find(selectors.dsAmount).attr('readonly', sourceVal !== 'Custom');
      updateDscr();
    }
  
    function onChangeDsAmount() {
      var sourceVal = template.find(selectors.dsSource).val()
      var dsAmountVal = template.find(selectors.dsAmount)
        .val()
        .split(',')
        .join('');
      var sourceAmount = parseFloat(dsAmountVal, 2);
      DscrModel.setNoiStaticSource(sourceAmount);
      DscrModel.setStaticSource(sourceVal);
      DscrModel.setSourceValue(sourceVal, sourceAmount);
      updateDscr();
    }
  
    function updateDscr() {
      var duration = template.find(selectors.dsTenor).val();
      var dscr = template.find(selectors.dscrSlider).bootstrapSlider('getValue');
  
      DscrModel.calculateDscr(duration, dscr);
      var loanDuration = parseFloat(template.find(selectors.dsTenor).val(), 10);
      var d = DscrModel.getDscrDuration(loanDuration);
  
      Api.saveDscr({
        entity_id: entity,
        dscr: DscrModel.getDscrc(),
        net_operating_income: DscrModel.getNoiStatic(),
        loan_duration: loanDuration,
        loan_amount: parseFloat(template.find(selectors.dsLoan).val().replace(/,/g , "").replace(/$/g , ""), 10),
        working_interest_rate_local: d[0].value,
        working_interest_rate_foreign: d[1].value,
        term_interest_rate_local: d[2].value,
        term_interest_rate_foreign: d[3].value,
        // period: payPeriod,
        // payment_per_period_local: payPeriodAmountLocal,
        // payment_per_period_foreign: payPeriodAmountForeign,
        is_custom: DscrModel.getCustomData() ? 1 : 0,
        source: DscrModel.getSource(),
      });
    }
  
    function render(data) {
      DscrModel.setDscrData(data.debt_service);
      DscrModel.setVars(data);
  
      var dscrVars = DscrModel.getVars();
      var bankInterestRates = DscrModel.setBankInterestRates(
        data.bank_interest_rates
      );
      var templateData = {
        dscrVars: dscrVars,
        number_format: MathHelper.number_format,
        trans: trans,
        year: data.financial_report.year
      };
  
      template = getTemplate(templateData);
      jq(selectors.dscrContainer).html(template);
    }
  
    function getTemplate(data) {
      return jq(_.template(MainTemplate)(data));
    }
  
    function initialize(entityId) {
      entity = entityId;
      Api.checkFinancialTemplate(entityId)
        .then(function(response) {
          render(response);
          bindEvents();
        })
        .catch(function(error) {
          jq(selectors.dscrContainer)
            .html(error.message);
        });
    }
  
    return {
      initialize: initialize
    };
  });
  