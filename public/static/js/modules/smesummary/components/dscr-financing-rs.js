define([
    'jquery',
    'underscore',
    'trans',
    'math',
    'common/api',
    'smesummary/components/dscr-model',
    'text!templates/dscr/financing-rs.html',
    'bootstrapSlider'
  ], function(jq, _, trans, MathHelper, Api, DscrModel, MainTemplate) {
    'use strict';
  
    var bankInterestRates;
    var entity;
    var selectors = {
      dscrContainer: '#debt_service_calculator_rs_container',
      dsrcNav: '#nav_pfs',
      dscrSlider: '#dscr_slider',
      dscrSliderDisplay: '#dscr_slider_display',
      dsLoan: '#ds_loan_value',
      dsTenor: '#ds_tenor_value',
      dsSource: '#dscr-source',
      dsAmount: '#ds_noi_value',
      dsExport: '#export-dscr-btn'
    };
    
    var template;
  
    function bindTemplateEvents() {
      if (template) {
        DscrModel.setEvents({
          onCalculateDscr: function(data) {
            onCalculateDscr(data);
          },
          onChangeLoanAmount: function(value) {
            template.find(selectors.dsLoan).val(value);
          },
          onChangePaymentPeriod: function(name, value) {
            template.find('#' + name + 'Payment').html(value);
          },
          onChangePeriod: function(name, value) {
            template.find('#' + name + 'Period').html(value);
          },
          onChangeTotalPayment: function(name, value) {
            template.find('#' + name + 'TotalPayment').html(value);
          }
        });
  
        template.find(selectors.dscrSlider)
          .bootstrapSlider({
            step: 0.1,
            formatter: function(value) {
              return value + 'x';
            }
          })
          .on('change', onSliderChange);
  
        template.find(selectors.dsTenor)
          .on('change keyup', function() {
            updateDscr();
          });
  
        template.find('#custom-interest-rate-btn')
          .on('click', function(event) {
            onToggleCustomFields(parseInt($(this).attr('data-value'), 10) !== 1);
          });
  
        template.find('#export-dscr-btn')
        .on('click', function(event) {
          Api.updateEntityExportFlag(entity);
          $('#export-dscr-btn').attr("disabled", "disabled");
        });
        
        template.find('#pfsdetails')
        .on('click', function(event) {
          $("ul#side-menu li:contains('Rating Summary')").removeClass('active');
          $('ul#side-menu li a').removeClass('active');
          $("ul#side-menu li:contains('Required Credit Line')").find("a").addClass('active');
        });
  
        var customFields = [
          template.find('#dscr-data-local-low input'),
          template.find('#dscr-data-foreign-low input'),
          template.find('#dscr-data-local-mid input'),
          template.find('#dscr-data-foreign-mid input')
        ];
  
        template.find('#dscr-data-local-low input')
          .on('change', onChangeCustomFields);
        template.find('#dscr-data-foreign-low input')
          .on('change', onChangeCustomFields);
        template.find('#dscr-data-local-mid input')
          .on('change', onChangeCustomFields);
        template.find('#dscr-data-foreign-mid input')
          .on('change', onChangeCustomFields);
  
        template.find(selectors.dsSource)
          .on('change', onChangeSource);
  
        template.find(selectors.dsAmount)
          .on('change', onChangeDsAmount);
  
        template.find('#print-computation-btn')
          .on('click', function() {
            window.print();
          });
  
        updateDscr();
  
        if (DscrModel.getCustomData()) {
          template.find('#custom-interest-rate-btn').trigger('click');
        }
      }
    }
  
    function bindEvents() {
      jq(document).ready(function() {
        jq(selectors.dsrcNav).click(function(e) {
          e.preventDefault();
        });
      });
  
      bindTemplateEvents();
    }
  
    function onChangeSource() {
      var sourceVal = template.find(selectors.dsSource).val();
      var sourceAmount = DscrModel.getSourceValue(sourceVal);
      var parsedAmount = MathHelper.number_format(parseFloat(sourceAmount), 2, '.', ',')
      DscrModel.setNoiStaticSource(sourceAmount);
      DscrModel.setStaticSource(sourceVal);
      DscrModel.setSourceValue(sourceVal, sourceAmount);
      template.find(selectors.dsAmount).val(parsedAmount);
      template.find(selectors.dsAmount).attr('readonly', sourceVal !== 'Custom');
      updateDscr();
    }
  
    function onChangeDsAmount() {
      var sourceVal = template.find(selectors.dsSource).val()
      var dsAmountVal = template.find(selectors.dsAmount)
        .val()
        .split(',')
        .join('');
      var sourceAmount = parseFloat(dsAmountVal, 2);
      DscrModel.setNoiStaticSource(sourceAmount);
      DscrModel.setStaticSource(sourceVal);
      DscrModel.setSourceValue(sourceVal, sourceAmount);
      updateDscr();
    }
  
    function onChangeCustomFields() {
      var rates = {
        working_interest_rate_local: template.find('#dscr-data-local-low input').val(),
        working_interest_rate_foreign: template.find('#dscr-data-foreign-low input').val(),
        term_interest_rate_local: template.find('#dscr-data-local-mid input').val(),
        term_interest_rate_foreign: template.find('#dscr-data-foreign-mid input').val()
      };
      DscrModel.setCustomBankInterestRates(rates);
      updateDscr();
    }
  
    function onSliderChange(event) {
      DscrModel.setDscrcStatic(event.value.newValue);
      template.find(selectors.dscrSliderDisplay)
        .text('DSCR: ' + event.value.newValue + 'x');
      updateDscr();
    }
  
    function getTemplate(data) {
      return jq(_.template(MainTemplate)(data));
    }
  
    function updateDscr() {
      var duration = template.find(selectors.dsTenor).val();
      var dscr = template.find(selectors.dscrSlider).bootstrapSlider('getValue');
  
      DscrModel.calculateDscr(duration, dscr);
      var loanDuration = parseFloat(template.find(selectors.dsTenor).val(), 10);
      var d = DscrModel.getDscrDuration(loanDuration);
  
      Api.saveDscr({
        entity_id: entity,
        dscr: DscrModel.getDscrc(),
        net_operating_income: DscrModel.getNoiStatic(),
        loan_duration: loanDuration,
        loan_amount: parseFloat(template.find(selectors.dsLoan).val().replace(/,/g , "").replace(/$/g , ""), 10),
        working_interest_rate_local: d[0].value,
        working_interest_rate_foreign: d[1].value,
        term_interest_rate_local: d[2].value,
        term_interest_rate_foreign: d[3].value,
        // period: payPeriod,
        // payment_per_period_local: payPeriodAmountLocal,
        // payment_per_period_foreign: payPeriodAmountForeign,
        is_custom: DscrModel.getCustomData() ? 1 : 0,
        source: DscrModel.getSource(),
      });
    }
  
    function render(data) {
      DscrModel.setDscrData(data.debt_service);
      DscrModel.setVars(data);
  
      var dscrVars = DscrModel.getVars();
      var bankInterestRates = DscrModel.setBankInterestRates(
        data.bank_interest_rates
      );
      var templateData = {
        dscrVars: dscrVars,
        number_format: MathHelper.number_format,
        trans: trans,
        year: data.financial_report.year
      };
  
      template = getTemplate(templateData);
      jq(selectors.dscrContainer).html(template);
    }
  
    function initialize(entityId) {
      entity = entityId;
      Api.checkFinancialTemplate(entityId)
        .then(function(response) {
          render(response);
          bindEvents();
        })
        .catch(function(error) {
          jq(selectors.dscrContainer)
            .html(error.message);
        });
    }
  
    function onCalculateDscr(data) {
      var localLow = template.find('#dscr-data-local-low');
      var localMid = template.find('#dscr-data-local-mid');
      var foreignLow = template.find('#dscr-data-foreign-low');
      var foreignMid = template.find('#dscr-data-foreign-mid');
  
      localLow.find('input').val(data[0].value);
      localLow.find('span').text(data[0].value);
      localMid.find('input').val(data[2].value);
      localMid.find('span').text(data[2].value);
      foreignLow.find('input').val(data[1].value);
      foreignLow.find('span').text(data[1].value);
      foreignMid.find('input').val(data[3].value);
      foreignMid.find('span').text(data[3].value);
    }
  
    function onToggleCustomFields(custom) {
      DscrModel.setCustomData(custom);
  
      if (custom) {
        template.find('#custom-interest-rate-btn')
            .text(trans('rcl.restore_interest_rate'))
            .attr('data-value', 1)
            .removeClass('btn-primary')
            .addClass('btn-warning');
        template.find('#dscr-data-local-low span').hide();
        template.find('#dscr-data-foreign-low span').hide();
        template.find('#dscr-data-local-mid span').hide();
        template.find('#dscr-data-foreign-mid span').hide();
        template.find('#dscr-data-local-low input').show();
        template.find('#dscr-data-foreign-low input').show();
        template.find('#dscr-data-local-mid input').show();
        template.find('#dscr-data-foreign-mid input').show();
        updateDscr();
        return;
      }
  
      template.find('#custom-interest-rate-btn')
          .attr('data-value', 0)
          .text(trans('rcl.custom_interest_rate'))
          .removeClass('btn-warning')
          .addClass('btn-primary');
      template.find('#dscr-data-local-low span').show();
      template.find('#dscr-data-foreign-low span').show();
      template.find('#dscr-data-local-mid span').show();
      template.find('#dscr-data-foreign-mid span').show();
      template.find('#dscr-data-local-low input').hide();
      template.find('#dscr-data-foreign-low input').hide();
      template.find('#dscr-data-local-mid input').hide();
      template.find('#dscr-data-foreign-mid input').hide();
      template.find(selectors.dsTenor).val(0);
      template.find(selectors.dscrSlider)
        .bootstrapSlider('setValue', 1.2, true, true);
      updateDscr();
    }
  
    return {
      initialize: initialize
    };
  });
  