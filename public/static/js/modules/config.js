requirejs.config({
  baseUrl: '/static/js/modules',
  paths: {
    bootstrapSlider: 'lib/bootstrap-slider',
    jq: 'lib/jquery',
    jquery: 'lib/jquery-no-conflict',
    math: 'lib/math',
    trans: 'lib/trans',
    underscore: 'lib/underscore'
  }/*,
  urlArgs: "purge=" + (new Date()).getTime()*/
});
