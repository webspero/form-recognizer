# --------------------------------- #
# --------------------------------- #
# ------ HOLT-WINTERS FORECAST ---- #
# --------------------------------- #
# --------------------------------- #

import statsmodels
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
import statsmodels.api as sapi
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from statsmodels.tsa.arima_model import ARIMA, ARMA, ARIMAResults, ARMAResults
from statsmodels.tsa.x13 import x13_arima_analysis, x13_arima_select_order
from pandas.plotting import register_matplotlib_converters
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import normalize, scale
register_matplotlib_converters()
from pmdarima.arima.seasonality import CHTest
from pmdarima.arima import auto_arima
import sys, re
from datetime import datetime
import math
import matplotlib

matplotlib.use('Agg') 
# --------------------------------- #
# ---------- DATA IMPORT ---------- #
# --------------------------------- #
arg = sys.argv
ids = 1
year = None
if(len(arg) > 1):
    ids = sys.argv[1]
    year = sys.argv[2]
rec_dates = []
rec  = []
grp = []
ind=[]
if len(sys.argv) > 3:
    rd = sys.argv[3].split(",")
    tmp = sys.argv[4].split(",")
    for i in tmp:    
        txt =  re.findall(r'\d+', i)
        rec.append(np.int64(txt[0]))
        ind.append(np.int64(txt[0]))
    rd[0] = rd[0].replace("[","")
    rd[len(rd) - 1] = rd[len(rd)-1].replace("]","")
    for i in rd:
        rec_dates.append(datetime.strptime(i, '%Y-%m-%d').date())
    i = 1
    while i <= len(rec_dates):
        grp.append([rec[i-1],rec_dates[i - 1] ])
        i += 1

if(len(rec) == 0):
    # grossvalue = pd.read_csv('c:/xampp/htdocs/holtwinter/data/grossValue2.csv', encoding='iso-8859-1')
    grossvalue = pd.read_csv('/var/cbpo/public/test/Rscripts/data/grossValue2.csv', encoding='iso-8859-1')
    grossvalue['Industry Subgroup'] = grossvalue['Industry Subgroup'].str.replace('\x85', '')
    indselect = int(ids)
    grossvalue.head()

# linear trendline
linear_model = LinearRegression()
if(len(rec) > 0):
    gdata = np.array(list(rec))
else:
    gdata = np.array(list(grossvalue.iloc[indselect])[2:])

gdatax = gdata.reshape(-1, 1)
xdata = np.array(list(range(0, len(gdata))))
xdatax = xdata.reshape(-1, 1)
linear_model.fit(xdatax, gdatax)
trendline = linear_model.predict(xdatax)
linear_model.get_params()
coeffs = round(linear_model.coef_[0][0], 2)
increase = gdata[-1] - gdata[0]
incpercent = (increase / gdata[0])*100

# build date index
startyear = '1998'
if(len(rec) > 0 ):
    endyear = year
else:
    endyear = "2018"

predictstart = startyear

# choose the number of periods to look forward
predperiods = 16

tstart = pd.to_datetime(startyear) + pd.offsets.BQuarterEnd(1)  # start time
tend = pd.to_datetime(endyear) + pd.offsets.BQuarterEnd(4)  # end time
predictend = tend + pd.offsets.BQuarterEnd(predperiods + 1)  # end time
dates = pd.date_range(tstart, tend, freq='Q')
dates2 = pd.date_range(predictstart, predictend, freq='Q')
if(len(rec) > 0):
    xdat = list(rec)
else:
    xdat = list(grossvalue.iloc[indselect])[2:]
# --------------------------------- #
# ----------- MODEL 1 ------------- #
# -- HOLT WINTERS MULTPLICATIVE --- #
# --------------------------------- #
if(len(rec) > 0):
    subt = len(dates) - (len(rec)-2)
    indata = pd.DataFrame(rec)[2:]
    indata.index = dates[:len(dates) -  subt]
else:
    indata = pd.DataFrame(grossvalue.iloc[indselect][2:])
    indata.index = dates
indatanp = np.asarray(indata)
try:
    holtwint = ExponentialSmoothing(indatanp,
                                    'multiplicative',
                                    damped=False,
                                    seasonal='multiplicative',
                                    seasonal_periods=4).fit()
    hwpredict = holtwint.predict(0, len(dates2) - 1)
    check = math.isnan(hwpredict[len(hwpredict) - 1])

    if(check == True):
        holtwint = ExponentialSmoothing(indatanp,
                                    'multiplicative',
                                    damped=False,
                                    seasonal='multiplicative',
                                    seasonal_periods=12).fit()

        hwpredict = holtwint.predict(0, len(dates2) - 1)

    check = math.isnan(hwpredict[len(hwpredict) - 1])
    if(check == True):
        holtwint = ExponentialSmoothing(indatanp,
                                    'multiplicative',
                                    damped=False,
                                    seasonal='multiplicative',
                                    seasonal_periods=24).fit()

        hwpredict = holtwint.predict(0, len(dates2) - 1)
    print("Holt Winter")
    for i in hwpredict:
        print(i)
    print("Dates")
    for i in dates2:
        print(i)
except:
    print('Holt Winters Forcecast:', 'Failed to converge and gave no output. Please try another subclass.')
    pass
