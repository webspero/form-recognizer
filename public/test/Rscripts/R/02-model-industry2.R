# user input 
idx = 2

# extract industry name and most recent year
indust = unique(df$industry)[idx]
cat("Begin modeling for industry:", indust, "\n")
most_recent_yr = max(as.integer(df$year))

# only look at the data for this industry
dat = df %>% filter(industry == indust)                  
plt = mk_lineplot(dat)
plt("year_qtr", "rev", ylab = "revenue", main = indust)

# Remark: possible trend, seasonality exists: peaks at Q1, troughs at Q4


## BEGIN model building ##

rev = ts(dat$rev, frequency = 4, start = c(2014, 3))
fit = HoltWinters(rev)
fit
plot(fit)

## END model building ##




## BEGIN making forecasts for the next 24 more quarters ##

pred = forecast.HoltWinters(fit, h=24)
plot.forecast(pred)

## END making forecasts for the next 24 more quarters ##




## BEGIN goodness of fit and model assumption diagnostics ## 

# we can investigate if the model can be improved upon by checking whether the
# in-sample forecast errors show non-zero autocorrelations at lags 1-3, by
# making a correlogram and carrying out the Ljung-Box test
acf(na.omit(pred$residuals), lag.max = 3)
Box.test(pred$residuals, lag = 3, type = "Ljung-Box")

# Remark: the correlogram shows that the autocorrelations for the in-sample
#       forecast errors do not exceed the significance bounds for lags 1-3.
#       Furthermore, the p-value for Ljung-Box test is 0.6, indicating that
#       there is little evidence of non-zero autocorrelations at lags 1-3.

# check if the forecast errors have constant variance over time, and are
# normally distributed with mean zero
plot.ts(pred$residuals)
plotForecastErrors(na.omit(pred$residuals))

## END goodness of fit and model assumption diagnostics ## 



## BEGIN saving output ##

# extract forecasted revenues
tmp = data.frame(pred)
dat_hat = data.frame(yr_qtr = row.names(tmp), rev = tmp$Point.Forecast) %>% 
        separate(yr_qtr, c("year", "qtr"))
rm(tmp)

# stack on top of the historical observed revenues
out = rbind(dat %>% select(year, qtr, rev), dat_hat)

# calc annual revenue
rev_base_yr = out %>% filter(year == most_recent_yr) %>% 
        summarise(base_yr = sum(rev)) %>% unlist()
rev_forward_yr1 = out %>% filter(year == most_recent_yr+1) %>% 
        summarise(forward_yr1 = sum(rev)) %>% 
        unlist()
rev_forward_yr2 = out %>% filter(year == most_recent_yr+2) %>% 
        summarise(forward_yr2 = sum(rev)) %>% 
        unlist()
rev_forward_yr3 = out %>% filter(year == most_recent_yr+3) %>% 
        summarise(forward_yr3 = sum(rev)) %>% 
        unlist()

# calc revenue multiples
rev_mults = c(rev_forward_yr1, rev_forward_yr2, rev_forward_yr3) / rev_base_yr

# insert row into lst
lst[[indust]] = c(industry = indust, rev_mults)


## END saving output ##

cat("End modeling for industry:", indust, "\n")
