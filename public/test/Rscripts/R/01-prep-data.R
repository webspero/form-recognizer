# read data
df = read_excel(file.path(data_path, "Gross Value Added by Industry Group at Current Prices.xlsx"), sheet="Table", skip = 2)
names(df)[1] = "industry"
names(df)[2] = "sub_industry"

# # there're rows entirely missing, remove them
df = na.omit(df)

# # change to long format
df = df %>% gather(yr_qtr, rev, -industry, -sub_industry)


# # separate year and quarter
df = df %>% mutate(year_qtr = yr_qtr) %>% 
        separate(yr_qtr, c("qtr", "year"), sep = " ")

# # initialize var to collect outputs
lst = vector("list", length = length(unique(df$sub_industry)))
names(lst) = unique(df$sub_industry)
