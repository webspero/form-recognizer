<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FinancialReportGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FinancialReportGenerator:generate {--entity-id=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically generates Financial Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $start = time();
        $option = $this->options();
        $entities = $option['entity-id'];
    }
}
