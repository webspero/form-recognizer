<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Entity;

class CleanupOldInnoErrorFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CleanupInnoError {--reportid=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old error file for done report on inndodata server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start deleting old error files");
        $this->deleteOldErrorFile();
        $this->info("Done");
    }

    public function deleteOldErrorFile(){
        $reportid = $this->option("reportid");

        $entity = Entity::where('entityid', $reportid)->first();
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") == "Production"){
            $dir = opendir('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$reportid);
            $remoteDir = '/FROM_INNO/'.$reportid.'/';
        }else{
            $dir = opendir('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'. $reportid);
            $remoteDir = '/TEST_FOLDER/_From Innodata/'.$reportid.'/';
        }

        $files = array();
        while (false !== ($file = readdir($dir)))
        {
            if ($file == "." || $file == "..")
                continue;
            $files[] = $file;
        }
        $deleted_files = array();

        foreach($files as $key => $err_file){
            if($key != 0 && str_contains($err_file, "Error ")){
                array_push($deleted_files, $err_file);
            }
        }

        //Delete files
        foreach($deleted_files as $key => $value){
            if($key != 0){
                // Delete old error
                $file_directory = $remoteDir . $value;
                ssh2_sftp_unlink($sftp, $file_directory);
            }
        }
        return;
    }
}
