<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Entity;
use User;
use AutomateStandaloneRating;

class SupervisorCleanUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SupervisorCleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Maintain 20 records of supervisor access for supervisor@creditbpo.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** Soft delete reports on test server only as per CDP-1579 - Maintain */
        if(env("APP_ENV") != "Production"){
            $this->info("start process");

            $start_date = date("Y-m-d ");
            $end_date = date("Y-m-d", strtotime("-5 months"));

            $dates_filter = array($start_date);
            // $reports = Entity::where('created_at', '>=', $end_date)->where('created_at', '<=', $start_date)->get();

            for($x=1; $x<=3; $x++){
                $date = date("Y-m-d", strtotime("-". $x." months"));
                array_push($dates_filter,$date);
            }
            
            foreach($dates_filter as $filter){
                //Select reports on the same months
                $month = date("m", strtotime($filter));
                $year = date("Y", strtotime($filter));

                $entityid = array();

                // Done Premium Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', PREMIUM_REPORT)->where('status', 7)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // New Premium Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', PREMIUM_REPORT)->where('status', 0)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // Done Standalone Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', STANDALONE_REPORT)->where('status', 7)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // New Standalone Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', STANDALONE_REPORT)->where('status', 0)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // Done Simplified Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', SIMPLIFIED_REPORT)->where('status', 7)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // New Simplified Report
                $report = Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->where('is_premium', SIMPLIFIED_REPORT)->where('status', 0)->first();
                !empty($report) ? array_push($entityid, $report->entityid) : '';

                // Mark as keep
                Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->whereIn('entityid', $entityid)->update(['is_deleted' => 0]);

                // Mark as soft delete
                Entity::whereYear('created_at', $year)->whereMonth('created_at', $month)->whereNotIn('entityid', $entityid)->update(['is_deleted' => 1]);
                
            }

            $this->info(date("Y-m-d H:i:s"));
            $this->info("Reports updated successfully");
            $this->info("end process");
        }
    }

    //-----------------------------------------------------
    //  Function XX.XX: deleteReports
    //      Delete records of report
    //-----------------------------------------------------
    public function deleteReports($report){
        $delete_reports = array();

        /** If report is more than 20  */
        if(count($report) > 20){
            foreach($report as $key => $value){
                if($key > 19){
                    /** Get reports exceeded to 20 */
                    array_push($delete_reports, $value->entityid );
                }
            }
        }

        /** Delete records report */
        if($delete_reports){
            Entity::whereIn('entityid', $delete_reports)->update(['is_deleted' => 1]);


            foreach($delete_reports as $val){
                echo 'Report ID ' .  $val .' has been deleted' . "\n";
            }
        }

        return;
    }
}
