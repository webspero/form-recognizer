<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Entity;
use Transaction;
use Payment;
use User;
use PremiumUser;
use Zipcode;
use ExternalLink;
use ExternalLinkController;
use QuestionnaireLink;
use URL;
use DB;
use Mail;

class verifyPaymentStatusOfReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'VerifyPaymentStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify payment status of purchased reports';

    /**  Variable declaration */
    private $url;                           // DragonPay URL
	private $merchant_id = 'CREDITBPO';     // Merchant ID to accesss API
    private $merchant_key = 'N6f9rEW3';     // Merchat Key for API access


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        /*  For Production Environment  */
		if (env("APP_ENV") == "Production") {
			$this->url = 'https://gw.dragonpay.ph/MerchantRequest.aspx?op=GETSTATUS';
		}
        /*  For Testing Environment     */
        else {
			$this->url = 'http://test.dragonpay.ph/MerchantRequest.aspx?op=GETSTATUS';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Gell all unpaid reports
        $reports = Entity::where('is_paid', 0)->get();

        // Iterate report
        foreach($reports as $report){

            // Check if report use a client key 
            $this->checkClientKeyTransaction($report->entityid);

            // Check transaction on dragonpay table
            $this->checkDragonpayTransaction($report->entityid);

            // Check transaction on Paypal
            $this->checkPaypalTransaction($report->entityid);
        } 
    }

    public function checkDragonpayTransaction($id){
        $dragonpayTransaction = Transaction::where('entity_id', $id)->get();
        $isReportPaid = Entity::where('entityid', $id)->first();

        if($isReportPaid->isPaid == 1){ // return if report is already paid
            return;
        }

        // Since user can create multiple transactions on dragonpay, get all the records of transaction and check the status of the payment
        foreach($dragonpayTransaction as $transaction){
            /** Get payment status of the transaction */
            $payment_status = file_get_contents($this->url . '&merchantid='.$this->merchant_id . '&merchantpwd='.$this->merchant_key . '&txnid='.$transaction->id.$transaction->txn);

            // Check API Endpoint if the status of transaction is Succesfull
            if($payment_status == 'S'){
                $this->updateReportStatus($isReportPaid);
            }
        }
        return;
    }

    public function checkPaypalTransaction($id){
        $isReportPaid = Entity::where('entityid', $id)->first();

        if($isReportPaid->isPaid == 1){ // return if report is already paid
            return;
        }

        $isCheckPayment = Payment::where('entity_id', $id)->first();
        if(!empty($isCheckPayment)){
            $this->updateReportStatus($isReportPaid);
        }
        return;
    }

    public function checkClientKeyTransaction($id){
        $isReportPaid = Entity::where('entityid', $id)->first();

        if($isReportPaid->isPaid == 1){ // return if report is already paid
            return;
        }

        if($isReportPaid->discount_code != ''){
            $this->updateReportStatus($isReportPaid);
        }
    }

    public function updateReportStatus($entity){
        // Update status to paid
        Entity::where('entityid', $entity->entityid)->update(['is_paid' => 1]);

        if($entity->is_premium == PREMIUM_REPORT){
            // Send premium link
            $this->sendPremiumReportNotification($entity);
        }

        echo "Report " . $entity->entityid . " is marked as paid. \n";

        return;
    }

    public function sendPremiumReportNotification($entity){
        $email = [];
        $pm = PremiumUser::where('is_deleted', 0)->get();
        $city = Zipcode::where('zip_code', $entity->zipcode)->first();
        $receiver = array();
        foreach ($pm as $value) {
            $receiver[]=$value->email;
        }

        if(count($receiver) > 0) {
            $email['email'] = $receiver;
        } else {
            $email['email'] = "ana.liza.bongat@creditbpo.com";
        }

        /*  Update Report to add Agency emails     */
        $entity = Entity::where('loginid', $entity->loginid)
                ->orderBy('entityid', 'desc')
                ->first();
        $entity->premium_report_sent_to = $email['email'];
        $entity->save();
        
        $save_array = array(
            'tab' => "registration1",
            'data' => array('biz-details-cntr')
        );

        do {
            $randomCode = ExternalLinkController::createRandomCode();
            $ql = QuestionnaireLink::where('url', $randomCode)->first();
        } while($ql != null);
        
        $ql = QuestionnaireLink::where("entity_id" , $entity->entityid)->get();
        
        if($ql->count() == 0) {
            QuestionnaireLink::create(array(
                'entity_id' => $entity->entityid,
                'url' => $randomCode,
                'panels' => json_encode($save_array)
            ));
    
            $url = URL::to('/') . '/premium-link/' . $randomCode;
            $loa = DB::table('tbldocument')->where('entity_id', $entity->entityid)->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)->first();

            try{
                Mail::send('emails.notifications.premium-report-notification',
                    array('url' => $url, 'entity' => $entity, 'city' => $city, 'loa' => $loa),
                    function($message) use ($email, $entity, $loa) {
                        $message->to($email['email'])
                                ->subject('CreditBPO Inquiry Requested:'.$entity->companyname);
                    }
                );
            }catch(Exception $e){
            
            }
        }
    }
}
