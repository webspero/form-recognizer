<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Services\ClientKeyAvailNotifService as NotifService;

class ClientKeyAvailableNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:availableclientkeys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notification to supervisors on the count of their available Client Keys.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->notifService = new NotifService($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("Start Supervisor Notification");
        $this->notifService->sendNotification();
        $this->info("Done Notification");
    }
}
