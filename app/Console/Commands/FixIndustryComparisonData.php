<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Services\IndustryComparison;
use Entity;
use DB;

class FixIndustryComparisonData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FixIndustryComparisonData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("BEGIN PROCESS");

        $reports = Entity::where('status', 7)
                            ->where('is_deleted', 0)
                            ->get();
        $industryComparison = new IndustryComparison;
        $total_count = 0;

        foreach($reports as $report){
            $isReadyRatioData = DB::table('tblreadyrationdata')->where('entityid', $report->entityid)->where('is_updated', 0)->first();

            if($isReadyRatioData){
                // Compute industry comparison of the report
                $ic = $industryComparison->getIndustryComparisonReport($report->entityid);
                $total_count += 1;
                echo $report->entityid . ' updated ' . $total_count . "\n";
            }
        }

        $this->info("DONE");
    }
}
