<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Services\ForecastingDataUpdate as UpdateService;

class ForecastingDataUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ForecastingDataUpdate {method=scrape} {--update-db=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new UpdateService();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $start = time();

        $arguments = $this->argument();
        $options = $this->options();

		switch ($arguments['method']) {
			case 'scrape':
			default:
				$data = $this->scrape();
		}

		if ($options['update-db'] !== false) {
			$this->info("\n".'Updating industry quarterly values'."\n");
			$this->service->updateIndustryQuarterValues($data);
		}

        $end = time() - $start;
        $this->info("\n".'Took '.$end.' seconds to complete'."\n");
    }

    protected function scrape() {
		$this->info("\n".'Running scraper'."\n");
		return $this->service->scrape();
	}
}
