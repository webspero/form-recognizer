<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PseCompany;
use PseBalSheet;
use Industrymain;

class AddIndustryDataPseCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AddIndustryDataPseCompany';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add industry data into PSE Company table to autopopulate on report creation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*  Variable Declaration    */
        $pse_db             = new PseCompany;                       // PSE Company Database Class
        $pse_balsheet_db    = new PseBalSheet;                      // PSE Company Balance Sheet Class
        $companies          = $pse_db->getActivePseCompanies();     // Get All PSE Companies from database

        $init_data          = 1;
        $total_companies    = @count($companies);
        $new_list_exist     = $this->checkPseCompanies();            // Check if new companies exists since last extraction

        /*  If PSE companies are already extracted  */
        if (0 < $total_companies) {
            /*  Check if Balance Sheet has been extracted   */
            $init_data      = @count($pse_balsheet_db->getPseBalSheetByPseID($companies[0]['pse_id']));
        }

        if (0 >= count($companies)) {
            $this->info('No PSE Companies available...');
        }
        else {
            $this->info('Extracting PSE Companies...');
            $this->info('Initializing Inserting Industry section and description for PSE Companies...');

            $this->info('Fetching available company information...');

            foreach ($companies as $companyKey => $company):

                $this->info('Inserting for: '.$company->company_name);

                $search_query_string = '?cmpy_id='.$company->pse_id; // Search string to find company information by company id
                $company_information_url    = 'https://edge.pse.com.ph/companyInformation/form.do'.$search_query_string;
                $html   = file_get_html($company_information_url);

                $data_list_div  = $html->find ('#dataList'); // Fetch company description
                $description = '';
                foreach ($data_list_div as $data_list) {
                    foreach ($data_list->find('table') as $data_list_table_key => $data_list_table)
                    {
                        if($data_list_table_key == 0)
                        {
                            // Replace Company Description with space and Remove extra spaces
                            $description = trim(str_replace("Company Description", "", $data_list_table->plaintext));
                        }
                        break;
                    }
                }

                $sector = '';
                $sub_sector = '';
                /* Fetch Security Information table and find sector and subsector */
                foreach($html->find('table', 1)->find('tr') as $security_info_table_key => $security_info_table_value) {
                    if($security_info_table_key == 0)
                    {
                        $sector = $security_info_table_value->children(1)->plaintext;
                    }
                    else if($security_info_table_key == 1)
                    {
                        $sub_sector = $security_info_table_value->children(1)->plaintext;
                    }
                }
                $extract_sub_sector = explode(" ", $sub_sector); // Convert string into an array
                $extracted_sub_sector = $extract_sub_sector[0]; // Get first word from sub sector string

                /* Fetch company's industry classification from https://psa.gov.ph/  */
                $responseHtml = $this->pse_classification_get_contents_curl("strEntry=".$extracted_sub_sector."&button=Search");
        
                $main_industry_section = '';
                /* Find industry section from the fetched HTML */
                foreach ($responseHtml->find('table[@id="classifytable"]') as $classifytable_key => $classifytable_value);
                {
                    foreach ($classifytable_value->find('tr') as $classifytable_tr_key => $classifytable_tr_value) {
                        if (strpos($classifytable_tr_value->children(1)->plaintext, 'Section') !== false) {
                            /* Remove Extra Spaces from String */
                            $main_industry_section = $this->removeExtraSpaces($classifytable_tr_value->children(0)->plaintext);
                        }
                    }
                }

                /* Find industry section from database and insert in pse_company table */
                $query = Industrymain::query();
                if(!empty($main_industry_section)) {
                    $query->orWhere('main_title', 'LIKE', '%'.$main_industry_section.'%');
                }
                $result = $query->first();
                if($result) {
                    $company->update(['pse_company_industry_main_id' => $result->main_code]);
                    // 'description' => $description
                }                
            endforeach;
        }
    }

    //-----------------------------------------------------
    //  Function: checkPseCompanies
    //      Compares extracted companies on database
    //      with current PSE Companies
    //
    //      Companies not existing on the database are
    //      extracted
    //-----------------------------------------------------
    function checkPseCompanies()
    {
        /*  Variable Declaration    */
        $pse_db         = new PseCompany;           // PSE Company Database Class
        $sts            = STS_OK;                   // Processing Status Flag
        $url            = 'http://edge.pse.com.ph/companyDirectory/search.ax?pageNo=1';
        $html_select    = $this->file_get_contents_curl($url);  // Open the HTML Page
        
        $total_init     = strpos($html_select, 'Total');
        
        $total_str      = STR_EMPTY;
        $idx            = (int)$total_init + 6;

        //dd(print_r($html_select, true));
        
        if($html_select != ""){
            /* Extracts the Total number of PSE Companies   */
            while (']' != $html_select[$idx]) {
                $total_str  .= $html_select[$idx];
                $idx++;
            }
        }
        
        /* Count total number of companies on the Database  */
        $extracted_companies    = $pse_db->getActivePseCompanies();
        $total_extracted_comp   = @count($extracted_companies);
        $total_pse_comp         = (int)$total_str;
        
        /*  If total company on PSE Website and total company on Database does not match    */
        if ($total_pse_comp > $total_extracted_comp) {
            $sts = STS_NG;
        }
        
        return $sts;
    }

    //-----------------------------------------------------
    //  Function : file_get_contents_curl
    //      Loads a Company list by page from PSE Edge
    //      for extraction
    //-----------------------------------------------------
    function file_get_contents_curl($url, $retries=5)
    {
        $ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
        if (extension_loaded('curl') === true)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // The URL to fetch. This can also be set when initializing a session with curl_init().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // The number of seconds to wait while trying to connect.
            curl_setopt($ch, CURLOPT_USERAGENT, $ua); // The contents of the "User-Agent: " header to be used in a HTTP request.
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE); // To fail silently if the HTTP code returned is greater than or equal to 400.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // To follow any "Location: " header that the server sends as part of the HTTP header.
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE); // To automatically set the Referer: field in requests where it follows a Location: redirect.
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); // The maximum number of seconds to allow cURL functions to execute.
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5); // The maximum number of redirects
            $result = curl_exec($ch);
            curl_close($ch);
        }
        else
        {
            $result = file_get_contents($url);
        }        

        return $result;
    }

    //-----------------------------------------------------
    //  Function: pse_classification_get_contents_curl
    //      Get content from pse classification URL
    //-----------------------------------------------------
    function pse_classification_get_contents_curl($search_section)
    {   
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://psa.gov.ph/classification/psic/?q=psic/search",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $search_section,
          CURLOPT_HTTPHEADER => array(
            "Accept:  text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Accept-Encoding:  gzip, deflate, br",
            "Accept-Language:  en-GB,en-US;q=0.9,en;q=0.8",
            "Cache-Control:  no-cache",
            "Connection:  keep-alive",
            "Content-Length:  31",
            "Content-Type:  application/x-www-form-urlencoded",
            "Cookie:  has_js=1; _ga=GA1.3.593748872.1598075167; _gid=GA1.3.171655482.1598075167; _gat=1; _gat_gtag_UA_148144233_1=1",
            "Host:  psa.gov.ph",
            "Origin:  https://psa.gov.ph",
            "Pragma:  no-cache",
            "Referer:  https://psa.gov.ph/classification/psic/?q=psic",
            "Sec-Fetch-Dest:  document",
            "Sec-Fetch-Mode:  navigate",
            "Sec-Fetch-Site:  same-origin",
            "Sec-Fetch-User:  ?1",
            "Upgrade-Insecure-Requests:  1",
            "User-Agent:  Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $dom = new \simple_html_dom($response);

        return $dom;
    }

    //-----------------------------------------------------
    //  Function: removeExtraSpaces
    //      Remove extra and white spaces from string
    //-----------------------------------------------------
    function removeExtraSpaces($str) {
        $str = htmlentities($str, null, 'utf-8');
        $str = str_replace("&nbsp;", "", $str);
        $str = html_entity_decode($str);
        $str = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $str)));

        return $str;
    }
}
