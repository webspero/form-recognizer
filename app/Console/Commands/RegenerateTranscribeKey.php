<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB; 
use Keys;
use CreditBPO\Services\MailHandler;

class RegenerateTranscribeKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RegenerateTranscribeKey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate public transcribe key every week ans send it to intended users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** Re-generate public transcribe key  */
        $key = "";
        $charset = "abcdefghijklmnopqrstuvwxyz0123456789";
        
        for($i = 0; $i < 10; $i++){
            $random_int = mt_rand();
            $key .= $charset[$random_int % strlen($charset)];
        }

        Keys::where('name','transcribe_key')->update(array('value'=>$key));  // update key in db

        // Get all the receiver of the transcribe key
        $receiver = DB::table('tblexternal_transcriber_account')->where('is_deleted', 0)->get();
        if(count($receiver) > 0){
            foreach($receiver as $r){
                //send email
                $emailNotif = new MailHandler();
                $emailNotif->sendRegeneratedTranscribeKey($r->email, $key);
            }
        }else{
            echo "There are no receiver for new generated keys.";
        }
    }
}
