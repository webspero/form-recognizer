<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Handlers\GenerateFinancialReportsHandler;

class GenerateAllReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateAllReports {--start=} {--stop=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Existing Financial Reports due to CDP-1096 Update.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $handler = new GenerateFinancialReportsHandler();
		$start = $this->option("start");
		$stop = $this->option("stop");
		if((!is_numeric($start) && $start)|| (!is_numeric($stop) && $stop)){
			$this->error("\n".'Entity start or stop must be numeric!'."\n");
			exit();
		}

		$handler->checkFinancialReports($start,$stop);
    }
}
