<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Entity;
use DB;

class updateIndustryRowOfOldReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateIndustryOfOldReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Industry row of old reports to Group value';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("start process");
        $reports = Entity::where('isIndustry', 1)->get();

        foreach($reports as $report){
            $industry_row = $report->industry_row_id;
            
            // Check if industry_row is not division
            if(strlen($industry_row) != 3){
                // trim the string into three
                $gc = substr($industry_row,0,3);

                // Check if present in psic group
                $isGroupCode = DB::table('tblindustry_group')
                                    ->where('group_code', $gc)
                                    ->first();

                if($isGroupCode){ // true
                    // Update industry row id of report
                    Entity::where('entityid', $report->entityid)->update(['industry_row_id' => $gc]); // update industry row id
                }else{
                    // Print report with no match group division and check the report individually
                    echo 'Check report ' . $report->entityid . ' Industry Row ID - ' . $report->industry_row_id . "\n";
                }
            }
        }
        $this->info("end process");
    }
}
