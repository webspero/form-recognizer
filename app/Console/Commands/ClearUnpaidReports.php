<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Models\Entity;
use CreditBPO\Models\User;
use DateTime;

class ClearUnpaidReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CleanUnpaidReports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete report/s thar are not paid for the past 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start pocess');

        /** Get all basic user accounts */
        $basic_user = User::select('loginid', 'name', 'email')->where('role', 1)->where('is_contractor', 0)->get();

        foreach($basic_user as $key => $value){
            /**Get all reports */
            $entity = Entity::select( 'entityid','created_at', 'companyname')
                            ->where('loginid', $value->loginid)
                            ->where('is_paid', 0)
                            ->where('is_deleted', 0)
                            ->get();

            if($entity){
                foreach($entity as $key => $ent){
                    $curr_date = date('Y-m-d H:i:s');
                    $created_report = $ent->created_at;

                    $interval = date_diff(date_create($curr_date), date_create($created_report));

                    $month = (int) $interval->format('%m');
                    $days = (int) $interval->format('%d');
                    $hrs = (int) $interval->format('%h');
                    
                    $total_hrs =  (($month * 30) * 24) + ($days * 24) + $hrs;

                    if($total_hrs >= 24){
                        Entity::where('entityid', $ent->entityid)->update(['is_deleted' => 1]);
                        echo 'Report ' . $ent . ' is deleted' . "\n";
                    }
                }
            }
        }
        $this->info('end process');
    }
}
