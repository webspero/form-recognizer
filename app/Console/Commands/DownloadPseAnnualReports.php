<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PseCompany;
use PseBalSheet;
use PseCompanyFinancialReport;

class DownloadPseAnnualReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DownloadPseAnnualReports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Financial Financial Statement Documents from PSE Edge for all companies.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*  Variable Declaration    */
        $pse_db             = new PseCompany;                       // PSE Company Database Class
        $pse_balsheet_db    = new PseBalSheet;                      // PSE Company Balance Sheet Class
        $companies          = $pse_db->getActivePseCompanies();     // Get All PSE Companies from database
        
        $init_data          = 1;
        $total_companies    = @count($companies);
        $new_list_exist     = $this->checkPseCompanies();            // Check if new companies exists since last extraction

        /*  If PSE companies are already extracted  */
        if (0 < $total_companies) {
            /*  Check if Balance Sheet has been extracted   */
            $init_data      = @count($pse_balsheet_db->getPseBalSheetByPseID($companies[0]['pse_id']));
        }

        if (0 >= count($companies)) {
            $this->info('No PSE Companies available...');
        }
        else {
            $this->info('Extracting PSE Companies...');
            $this->info('Initializing Downloading Annual Report of PSE Companies...');

            $this->info('Fetching available company information...');

            foreach ($companies as $company):

                $this->info('Downloading Annual Report for: '.$company->company_name);

                $search_query_string = '?keyword='.$company->pse_id.'&tmplNm=Annual%20Report'; // Search string to find Annual report by company id
                $company_disclosures_url    = 'https://edge.pse.com.ph/companyDisclosures/search.ax'.$search_query_string;
                $html   = file_get_html($company_disclosures_url);

                $links  = array_reverse($html->find ('a')); // Find anchor element which contains Annual report edge no and reverse it
                $edge_nos  = [];

                foreach ($links as $link) {
                    /* Check if Found Anchor is of Annual Report */
                    if($link->href === '#viewer' && $link->plaintext === 'Annual Report')
                    {
                        /* Extract edge no from onclick function */
                        $edge_no = $this->get_string_between($link->onclick, "'","'");

                        $openDiscViewerUrl    = 'https://edge.pse.com.ph/openDiscViewer.do?edge_no='.$edge_no;
                        $openDiscHtml   = file_get_html($openDiscViewerUrl);

                        foreach($openDiscHtml->find('#file_list') as $openDiscHtmlElement) { 
                            $attachmentOptions = $openDiscHtmlElement->find('option');
                            foreach($attachmentOptions as $attachmentOption) { 
                                if(!empty($attachmentOption->value))
                                {
                                    $file_id = $attachmentOption->value;

                                    //Check if file for PSE company already exist
                                    $pse_fs = PseCompanyFinancialReport::where('pse_company_id', $company->pse_id)
                                            ->where('attachment_file_id', $file_id)
                                            ->first();

                                    /* Remove &nbsp; from File Name */
                                    $string = preg_replace("/&nbsp;/",'',$attachmentOption->plaintext);
                                    /* Remove &amp; from File Name */
                                    $string = preg_replace("/&amp;/",'',$attachmentOption->plaintext);
                                    /* Remove extra spaces from File Name*/
                                    $content = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $string)));

                                    if(empty($pse_fs)) {
                                        // https://edge.pse.com.ph/downloadFile.do?file_id=744695
                                        // Initialize a file URL to the variable 
                                        $downloadUrl = 'https://edge.pse.com.ph/downloadFile.do?file_id='.$file_id;
                                        $attachment_file_name = $content;

                                        $dir = public_path('pse_fs_attachments/').$company->pse_id.'/';
                                        $this->downloadFromUrl($downloadUrl, $dir, $attachment_file_name, $company->pse_id);

                                        /* Save downloaded report data in database */
                                        PseCompanyFinancialReport::create(['pse_company_id' => $company->pse_id,
                                                        'attachment_file_name'  => $attachment_file_name,
                                                        'edge_no'               => $edge_no,
                                                        'attachment_file_id'    => $file_id,
                                                        'attachment_file_url'   => url('').'/pse_fs_attachments/'.$company->pse_id.'/'.$attachment_file_name]);
                                    }

                                }
                            }
                        }
                    }
                }
            endforeach;
        }
    }

    function checkPseCompanies()
    {
        /*  Variable Declaration    */
        $pse_db         = new PseCompany;           // PSE Company Database Class
        $sts            = STS_OK;                   // Processing Status Flag
        $url            = 'http://edge.pse.com.ph/companyDirectory/search.ax?pageNo=1';
        $html_select    = $this->file_get_contents_curl($url);  // Open the HTML Page
        
        $total_init     = strpos($html_select, 'Total');
        
        $total_str      = STR_EMPTY;
        $idx            = (int)$total_init + 6;

        //dd(print_r($html_select, true));
        
        if($html_select != ""){
            /* Extracts the Total number of PSE Companies   */
            while (']' != $html_select[$idx]) {
                $total_str  .= $html_select[$idx];
                $idx++;
            }
        }
        
        /* Count total number of companies on the Database  */
        $extracted_companies    = $pse_db->getActivePseCompanies();
        $total_extracted_comp   = @count($extracted_companies);
        $total_pse_comp         = (int)$total_str;
        
        /*  If total company on PSE Website and total company on Database does not match    */
        if ($total_pse_comp > $total_extracted_comp) {
            $sts = STS_NG;
        }
        
        return $sts;
        
    }

    function file_get_contents_curl($url, $retries=5)
    {
        $ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
        if (extension_loaded('curl') === true)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // The URL to fetch. This can also be set when initializing a session with curl_init().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // The number of seconds to wait while trying to connect.
            curl_setopt($ch, CURLOPT_USERAGENT, $ua); // The contents of the "User-Agent: " header to be used in a HTTP request.
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE); // To fail silently if the HTTP code returned is greater than or equal to 400.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // To follow any "Location: " header that the server sends as part of the HTTP header.
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE); // To automatically set the Referer: field in requests where it follows a Location: redirect.
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); // The maximum number of seconds to allow cURL functions to execute.
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5); // The maximum number of redirects
            $result = curl_exec($ch);
            curl_close($ch);
        }
        else
        {
            $result = file_get_contents($url);
        }        

        return $result;
    }

    //-----------------------------------------------------
    //  Function: get_string_between
    //      Get string between two string
    //-----------------------------------------------------
    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    //-----------------------------------------------------
    //  Function: downloadFromUrl
    //      Download Annual Report from URL and Save it in pse_fs_attachments folder
    //-----------------------------------------------------
    function downloadFromUrl($url, $dir, $file_name, $pse_id)
    {
        // Initialize the cURL session 
        $ch = curl_init($url);
          
        // Inintialize directory name where 
        // file will be save 
        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        // Use basename() function to return
        // the base name of file  
        $file_name = $file_name;
          
        // Save file into file location 
        $save_file_loc = $dir . $file_name; 
          
        // Open file  
        $fp = fopen($save_file_loc, 'wb'); 
          
        // It set an option for a cURL transfer 
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_HEADER, 0); 
          
        // Perform a cURL session 
        curl_exec($ch); 
          
        // Closes a cURL session and frees all resources 
        curl_close($ch); 
          
        // Close file 
        fclose($fp); 
    }
}