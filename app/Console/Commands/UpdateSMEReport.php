<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Services\UpdateSmeReport as SmeReport;
use \Exception as Exception;

class UpdateSMEReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateSMEReport:generate  {--entity-start=} {--entity-stop=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automate updating SME Report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $start = time();
        $options = $this->options();
        $startId = $options['entity-start'];

		if (!is_numeric($startId)) {
			$this->error("\n".'Entity start must be numeric!'."\n");
			exit();
		}

		$endId = $options['entity-stop'];

		if (!$endId || $endId > $startId) {
			$endId = $startId;
		}

		if (!is_numeric($endId)) {
			$this->error("\n".'Entity stop must be numeric!'."\n");
			exit();
		}

		$this->info("\n".'Starting SME Report Update'."\n");

		$handler = new SmeReport();

		for ($i = $startId; $i <= $endId; $i++) {
			try {
				$entity = $handler->getEntity($i);

				if (!$entity) {
					continue;
				}

				$this->info('Processing Entity '.$entity->companyname."\n");
				$handler->updateExistingReport($i, true);
			} catch (Exception $e) {
				$this->error("\n".$e->getMessage()."\n");
				  echo print_r(debug_backtrace(2),true);
			}

		}

		$this->info('Cron took '.(time() - $start).' second(s) to finish.');
		$this->line("\n");
    }
}
