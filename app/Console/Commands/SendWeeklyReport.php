<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Entity;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use FinancialReport;
use CreditBPO\Handlers\FinancialHandler;
use Exception;
use Mail;

class SendWeeklyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendWeeklyReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly report to Contact Agencies registered on Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start Process");
        /** Get weekly report */
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday midnight",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);
        $file_path = public_path('documents/admin/reports/Weekly Reports as of ' . date("m-d-Y") . '.xlsx');

        $reports = Entity::select('tblentity.entityid', 'tblentity.companyname', 'tbllogin.email', 'tblbank.bank_name', 
									'tblentity.created_at', 'tblentity.completed_date', 'tblentity.is_premium', 'tblentity.discount_code', 
									'bank_keys.serial_key', 'tblentity.status', 'tbllogin.loginid', 'tblentity.province', 'tblentity.current_bank')
										->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
                            ->leftJoin('tblbank', 'tblbank.id', '=', 'tblentity.current_bank')
                            ->leftJoin('bank_keys', 'bank_keys.entity_id', '=', 'tblentity.entityid')
                            ->where('tbllogin.email', '<>', '')
                            ->whereDate('tblentity.created_at',  '>=', $start_week)
                            ->whereDate('tblentity.created_at', '<=', $end_week)
                            // ->whereBetween('tblentity.created_at', [$start_week, $end_week])
                            ->get();

        foreach($reports as $report){
            /** Report Type */
            if($report->is_premium == 2){
                $report->is_premium = 'Simplified';
            }elseif($report->is_premium == 1){
                $report->is_premium = 'Premium';
            }else{
                $report->is_premium = 'Standalone';
            }

            /** Get Rating of the submitted report */
            $report->rating = '';
            if($report->status == 7){
                $rating = $this->getFinancialRating($report);
                $report->rating = $rating['rating_display'];
            }

            /** Check payment  */
            $report->payment_method = '';
            $report->price = 0;
            $is_payment = false;

            $isDragonPay = DB::table('tbltransactions')->where('entity_id', $report->entityid)->get();
            if($isDragonPay){
                foreach($isDragonPay as $dp){
                    if($dp->status == 'S'){
                        $report->payment_method = 'Dragonpay';
                        $receipts = DB::table('payment_receipts')->where('reference_id', $dp->id)->where('payment_method', 2)->first();
                        if($receipts){
                            $report->price = $receipts->total_amount;
                            $is_payment = true;
                        }
                    }
                }
            }

            $isPaypal = DB::table('paypal_transactions')->where('entity_id', $report->entityid)->get();
            if($isPaypal){
                foreach($isPaypal as $pp){
                    $paypalRecepts = DB::table('payment_receipts')->where('reference_id', $pp->id)->where('payment_method', 1)->first();
                    if($paypalRecepts){
                        $report->payment_method = 'Paypal';
                        $report->price = $receipts->total_amount;
                        $is_payment = true;
                    }
                }
            }

            if($is_payment == false){
                /** Get report price base on bank */
                $bank = DB::table('tblbank')->where('id', $report->current_bank)->first();
                if($bank){
                    if($report->is_premium == 2){
                        $report->price = $bank->simplified_price;
                    }elseif($report->is_premium == 1){
                        $provCode = Province2::where('provDesc', $report->province)->pluck('regCode')->first(); //get province code for filters
                        if ($provCode == 13) {                                      // Metro Manila
                            $report->price = $bank->premium_zone1_price;
                        }elseif(in_array($provCode, array(04,03))){                 // Laguna, Rizal, Cavite and Bulacan
                            $report->price = $bank->premium_zone2_price;
                        }else{                                                      // Rest of the Regions
                            $report->price = $bank->premium_zone3_price;
                        }
                    }else{
                        $report->price = $bank->standalone_price;
                    }
                }
                if($report->serial_key){
                    $report->discount_code = '';
                }
            }else{
                $report->discount_code = '';
                $report->serial_key = '';
            }

            // If the report is a trial account
            if($is_payment == false && $report->discount_code == '' && $report->serial_key == ''){
                // $report->payment_method = 'Trial Account - Free';
            }
        }

        $final_reports = [];
        // Arrange data
        foreach($reports as $report){
            $final_reports[] = array(
                'entityid'			=> $report->entityid,
                'companyname'		=> $report->companyname,
                'email'				=> $report->email,
                'organization'		=>	$report->bank_name,
                'date_started'		=>	$report->created_at,
                'date_finished'		=>	$report->date_completed,
                'report_type'		=>	$report->is_premium,
                'price'				=> $report->price,
                'rating'			=> $report->rating,
                'payment_method'	=> $report->payment_method,
                'discount_code'		=> $report->discount_code,
                'client_key'		=> $report->serial_key
            );
        }
        

        if(count($reports) == 0){
            $final_reports = array("No Reports");
        }

        Excel::create('Weekly Reports as of ' . date('m-d-Y'), function($excel) use ($final_reports) {
            $excel->sheet('Weekly Reports', function($sheet) use($final_reports){
                $sheet->fromArray($final_reports);
            });
        })->store('xlsx', public_path('documents/admin/reports'));

        /** Send email notification */
        try{
            $receiver = DB::table('tbl_reports_receiver')->get();
            $email = [];
            foreach($receiver as $re){
                $email[] = $re->email;
            }
            
            Mail::send('emails.notifications.admin-reminder-reports-data', array('data' => $final_reports, 'from' => $start_week, 'to' => $end_week),
                    function($message) use ($email, $file_path) {
                        $message->to("web@creditbpo.com")->cc($email)->subject('Weekly Report')->attach($file_path);
                    }
            );
        }catch(Exception $e){}

        $this->info("End Process");
    }

    public function getFinancialRating($report){
		$financialChecker = false;
		$financialTotal = 0;
		$pdefault = 0;

		$financialReport = FinancialReport::where(['entity_id' => $report->entityid, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
		
		if($financialReport){
			$frHelper = new FinancialHandler();
			$finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
			if($finalRatingScore) {
				$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
				$financialChecker = true;
			}
		}

		$financial_score = 0;
		if($financialChecker){
			$financial_score = $financialTotal;
			$financial_rating = '';

			if($financialTotal >= 1.6) {
				$financial_rating	= 'AAA';
				$fin_rate_display = 'AAA - Excellent';
				$color  = 'green';
				$pdefault = 1;
			} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
				$financial_rating	= 'AA';
				$fin_rate_display = 'AA - Very Good';
				$color = 'green';
				$pdefault = 2;
			} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
				$financial_rating	= 'A';
				$fin_rate_display = 'A - Good';
				$color = 'green';
				$pdefault = 3;
			} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
				$financial_rating	= 'BBB';
				$fin_rate_display = 'AAA - Positive';
				$color = 'orange';
				$pdefault = 4;
			} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
				$financial_rating	= 'BB';
				$fin_rate_display = 'BB - Normal';
				$color = 'orange';
				$pdefault = 6;
			} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
				$financial_rating	= 'B';
				$fin_rate_display = 'B - Satisfactory';
				$color = 'red';
				$pdefault = 8;
			} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
				$financial_rating	= 'CCC';
				$fin_rate_display = 'CCC - Unsatisfactory';
				$color = 'red';
				$pdefault = 15;
			} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
				$financial_rating	= 'CC';
				$fin_rate_display = 'CC - Adverse';
				$color = 'red';
				$pdefault = 20;
			} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
				$financial_rating	= 'C';
				$fin_rate_display = 'C - Bad';
				$color = 'red';
				$pdefault = 25;
			} else {
				$financial_rating	= 'D';
				$fin_rate_display = 'D - Critical';
				$color = 'red';
				$pdefault = 32;
			}
		} elseif ($value->score_sf){
			$cred_rating_exp    = explode('-', $report->score_sf);
			$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
			$rate_val   = intval($rate_val);

			$financial_score = $rate_val;
			$financial_rating = '';

			if($rate_val >= 177){
				$financial_rating = 'AA';
				$fin_rate_display = 'AA - ' . $cred_rating_exp[1];
				$color = 'green';
				$pdefault = 2;
			}
			else if($rate_val >= 150 && $rate_val <= 176) {
				$financial_rating = 'A';
				$fin_rate_display = 'A - ' . $cred_rating_exp[1];
				$color = 'green';
				$pdefault = 3;
			}
			else if($rate_val >= 123 && $rate_val <= 149) {
				$financial_rating = 'BBB';
				$fin_rate_display = 'BBB - ' . $cred_rating_exp[1];
				$color = 'orange';
				$pdefault = 4;
			}
			else if($rate_val >= 96 && $rate_val <= 122) {
				$financial_rating = 'BB';
				$fin_rate_display = 'BB - ' . $cred_rating_exp[1];
				$color = 'orange';
				$pdefault = 6;
			}
			else if($rate_val >= 68 && $rate_val <= 95) {
				$financial_rating = 'B';
				$fin_rate_display = 'B - ' . $cred_rating_exp[1];
				$color = 'red';
				$pdefault = 8;
			}
			else if($rate_val < 68) {
				$financial_rating = 'CCC';
				$fin_rate_display = 'CCC - ' . $cred_rating_exp[1];
				$color = 'red';
				if($rate_val >= 42 && $rate_val <= 68) {
					$pdefault = 15;
				} else {
					$pdefault= 30;
				}
			}
		}

		return array(
			'financial_rating'	=>	$financial_rating,
			'financial_score'	=>	$financial_score,
			'pdefault'			=>	$pdefault,
			'color'				=>  $color,
			'rating_display'	=> $fin_rate_display
		);
	}
}
