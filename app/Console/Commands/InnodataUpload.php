<?php

namespace App\Console\Commands;

use CreditBPO\Handlers\InnodataUploadHandler;
use Illuminate\Console\Command;
use ZipArchive;

class InnodataUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InnodataUpload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload pending financial statement PDFs';

    private $upload;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    //------------------------------------------------------------------
    //  Function 58.0: __construct
    //       Command Initialization
    //------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();
        $this->upload = new InnodataUploadHandler();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

     //------------------------------------------------------------------
    //  Function 58.0: __construct
    //       Command Initialization
    //------------------------------------------------------------------
    public function handle()
    {
        $this->info("start innodata process");
	    $this->info(date("Y-m-d H:i:s"));
        $this->upload->UploadFiles();
        $this->info("end process");
    }
}
