<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;


class AutomateStandaloneRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutomateStandaloneRating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automates Standalone Rating Report Generation.';

	private $automate;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
		$this->automate = new AutomateStandaloneRatingHandler();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("start process");
	    $this->info(date("Y-m-d H:i:s"));
        $this->automate->processReports();
        $this->info("end process");
    }
}
