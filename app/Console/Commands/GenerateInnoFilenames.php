<?php

namespace App\Console\Commands;

use CreditBPO\Handlers\GenerateInnoFilenamesHandler;
use Illuminate\Console\Command;

class GenerateInnoFilenames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateInnoFilenames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Innodata text filenames';

    private $generateFile;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    //------------------------------------------------------------------
    //  Function 58.0: __construct
    //       Command Initialization
    //------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();
        $this->generateFile = new GenerateInnoFilenamesHandler();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    //------------------------------------------------------------------
    //  Function 58.1: handle
    //      Function to get FS Template File from Innodata
    //------------------------------------------------------------------
    public function handle()
    {
        $this->info("Start Get Innodata Files From Shared Folder");
        $this->info(date("Y-m-d H:i:s"));
        $this->generateFile->generateInnoFilenames();
        $this->info("Done");
    }
}
