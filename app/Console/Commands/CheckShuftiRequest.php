<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ShuftiUser;
use ShuftiUserDocument;

class CheckShuftiRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckShuftiRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Pending Shufti Requests And Update Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Checking SHUFTI API Status Start');
        /*  Variable Declaration    */
        $url        = 'https://api.shuftipro.com/status';               // API URL
        $client_id  = env('SHUFTI_CLIENT_ID');                          // Shufti CLIENT ID
        $secret_key = env('SHUFTI_SECRET_KEY');                         // Shufti SECRET KEY
        $users      = ShuftiUser::where('status', 'PENDING')->get();    // Fetch PENDING Requests
        foreach ($users as $user) {
            $this->info('checking status of '.$user->reference);
            $status_request = [
                "reference" => $user->reference,
            ];

            $auth = $client_id . ":" . $secret_key;
            $headers = ['Content-Type: application/json'];
            $post_data = json_encode($status_request);
            $response = $this->send_curl($url, $post_data, $headers, $auth);
            $response_data    = json_decode($response['body']);
            switch ($response_data->event) {
                case 'verification.invalid':
                    $user->status = "INVALID";
                    break;
                case 'verification.accepted':
                    $user->status = "ACCEPTED";
                    $document = ShuftiUserDocument::where('user_id', $user->id)->first();
                    if (!empty($document)) {
                        $url = 'https://api.hellosign.com/v3/signature_request/send';
                        $auth = env('HS_API_KEY');
                        $headers = ['Content-Type: application/json'];
                        $post_data = json_encode([
                            'test_mode' => '1',
                            'title' => 'CreditBPO Document eSignature',
                            'subject' => 'Kindly eSign Following Document',
                            'signers' => [
                                'name' => $user->first_name . ' ' . $user->last_name,
                                'email_address' => $user->email,
                            ],
                            'file_url' => [$document->file_path]
                        ]);

                        $this->send_curl($url, $post_data, $headers, $auth);
                    }
                    break;
                case 'verification.declined':
                    $user->status = "DECLINED";
                    $user->message = $response_data->declined_reason;
                    break;
                case 'verification.received':
                    $user->status = "RECEIVED";
                    break;
            }
            $user->save();
        }
        $this->info('Checking SHUFTI API Status End');
    }

    function send_curl($url, $post_data, $headers, $auth)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $html_response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($html_response, 0, $header_size);
        $body = substr($html_response, $header_size);
        curl_close($ch);
        return ['headers' => $headers, 'body' => $body];
    }
}
