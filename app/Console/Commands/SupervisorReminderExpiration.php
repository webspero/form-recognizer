<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Handlers\AutomateSupervisorExpireHandler;

class SupervisorReminderExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutomateSupervisorAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email supervisor account who will subscription expires before 10 days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $this->info("start process");
        $this->info(date("Y-m-d H:i:s"));

        $sup = new AutomateSupervisorExpireHandler();
        $sup->processSupervisorAccounts();

        $this->info("end process");
    }
}