<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Models\CurrencyRate;
use Excel;

class ScrapeCurrencyValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ScrapeCurrency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape currency rate for different country';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Extracting Currency Rate Companies...');
        $this->info('Fetching available currency information...');

        $this->scrapeNewCurrencyData();

        $this->info('Done.');
    }

    public function scrapeNewCurrencyData(){
        try{
            $currentDate = date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days'));

            $search_query_string = '?from=PHP&date='. $currentDate . '#table-section';
            $currency_url = "https://www.xe.com/currencytables/" . $search_query_string;

            $isCurrency = false;

            $html   = file_get_html($currency_url);
            $data_list_div  = $html->find('.currencytables__Table-xlq26m-3'); // Fetch company description

            $currencyRate = [];
            $allCurrency = [];

            foreach ($html->find('table', 0)->find('tr') as $key => $value){
                if($key == 0){
                    continue;
                }

                $isCurrency = true;

                foreach($value->find('th') as $count => $cell){
                    if($count == 0){
                        $currencyRate['per_currency'] = $cell->plaintext;
                    }
                }

                foreach($value->find('td') as $count => $cell) {
                    // echo $count;
                    if($count == 0){
                        $currencyRate['country_name'] = $cell->plaintext;
                    }
                    if($count == 1){
                        $currencyRate['unit_per_php'] = $cell->plaintext;
                    }
                    if($count == 2){
                        $currencyRate['php_per_unit'] = $cell->plaintext;
                    }
                }
                $allCurrency[] = $currencyRate;
            }

            // Export data to excel format
            Excel::create('Currency Directory Scraper', function($excel) use ($allCurrency) {
                $excel->sheet('Currency Scraper', function($sheet) use($allCurrency){
                    $sheet->fromArray($allCurrency);
                });
            })->store('xlsx', public_path('documents/currency'));

            $filepath = public_path('documents/currency/Currency Directory Scraper.xlsx');

            // Get data from file
            $data = $this->getCurrencyRate($filepath);

            foreach($data as $key => $value){
                // Check if needed fields exist or empty
                if($value['per_currency'] && $value['country_name'] && $value['unit_per_php'] && $value['php_per_unit']
                    && !empty($value['per_currency']) && !empty($value['country_name']) && !empty($value['unit_per_php']) && !empty($value['php_per_unit']) ){
                    
                    $iscRate = CurrencyRate::where('iso_code', $value['per_currency'])->where('year', date('Y'))->first();
                    // Check if year is already saved on the database
                    if(empty($iscRate)){
                        // Save record to the database
                        $cRate = new CurrencyRate;
                        $cRate->year = date('Y');
                        $cRate->iso_code = $value['per_currency'];
                        $cRate->country_name = $value['country_name'];
                        $cRate->unit_per_php = round($value['unit_per_php'], 2);
                        $cRate->php_per_unit = round($value['php_per_unit'], 2);
                        $cRate->save();
                    }else{
                        CurrencyRate::where('year', date('Y'))->where('iso_code', $value['per_currency'])->update([
                            'unit_per_php' => round($value['unit_per_php'], 2),
                            'php_per_unit'  => round($value['php_per_unit'], 2)
                        ]);
                    }
                }else{
                    continue;
                }
            }
        }
        catch(Exception $e){
            $this->info('It looks like the DOM of the site has been updated. Your scraping codes need to be updated as well.');
        }

    }

    public function initialScrape(){
        $filepath = public_path('documents/currency/Currency Directory Scraper.xlsx');

        // Check first if scraped data is available
        if(!file_exists($filepath)){
            // Run scraper function
            $this->scrapeCurrencyRate();
        }

        // Get data from file
        $data = $this->getCurrencyRate($filepath);

        // Process the data
        foreach($data as $key => $value){
            // Check if needed fields exist or empty
            if(isset($value['year']) && $value['per_currency'] && $value['country_name'] && $value['unit_per_php'] && $value['php_per_unit'] 
                && !empty($value['year']) && !empty($value['per_currency']) && !empty($value['country_name']) && !empty($value['unit_per_php']) && !empty($value['php_per_unit']) ){
                
                $iscRate = CurrencyRate::where('iso_code', $value['per_currency'])->where('year', $value['year'])->first();
                // Check if year is already saved on the database
                if(empty($iscRate)){
                    // Save record to the database
                    $cRate = new CurrencyRate;
                    $cRate->year = $value['year'];
                    $cRate->iso_code = $value['per_currency'];
                    $cRate->country_name = $value['country_name'];
                    $cRate->unit_per_php = round($value['unit_per_php'], 2);
                    $cRate->php_per_unit = round($value['php_per_unit'], 2);
                    $cRate->save();
                }
            }else{
                continue;
            }
        }
    }

    public function scrapeCurrencyRate(){
        $year = array(1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011,2012,2013,2014,2015,2016,2017,2018,2019,2020);
    
            /** Initialization */
            $data = [];

            foreach($year as $y){
                $search_query_string = '?from=PHP&date='. $y .'-12-01#table-section';
                $currency_url = "https://www.xe.com/currencytables/" . $search_query_string;

                $html   = file_get_html($currency_url);
                $data_list_div  = $html->find('.currencytables__Table-xlq26m-3'); // Fetch company description

                $newCurrency = [];
                foreach ($html->find('table', 0)->find('tr') as $key => $value){
                    if($key == 0){
                        $newCurrency['year'] = $y;
                        continue;
                    }
                    foreach($value->find('td') as $count => $cell) {
                        if($count == 0){
                            $newCurrency['per_currency'] = $cell->plaintext;
                        }
                        if($count == 1){
                            $newCurrency['country_name'] = $cell->plaintext;
                        }
                        if($count == 2){
                            $newCurrency['unit_per_php'] = $cell->plaintext;
                        }
                        if($count == 3){
                            $newCurrency['php_per_unit'] = $cell->plaintext;
                        }
                    }
                    $data[] = $newCurrency;
                }
            }

            // Export data to excel format
            Excel::create('Currency Directory Scraper', function($excel) use ($data) {
                $excel->sheet('Currency Scraper', function($sheet) use($data){
                    $sheet->fromArray($data);
                });
            })->store('xlsx', public_path('documents/currency'));

            return public_path('documents/currency/Currency Directory Scraper.xlsx');
    }

    public function getCurrencyRate($file){
        $data = Excel::load($file)->get();
        return $data;
    }

}
