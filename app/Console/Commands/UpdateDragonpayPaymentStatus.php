<?php

namespace App\Console\Commands;

use CreditBPO\Services\MailHandler;
use Illuminate\Console\Command;
use BankSubscription;
use PaymentReceipt;
use BankSerialKeys;
use Transaction;
use DateTime;
use Entity;
use User;
use PremiumUser;
use Zipcode;
use ExternalLink;
use ExternalLinkController;
use QuestionnaireLink;
use URL;
use DB;
use Mail;
use Bank;
use GuzzleHttp\Client as GuzzleClient;

class UpdateDragonpayPaymentStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateOTCDragonpayPayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check over-the-counter payment status to update paid reports.';

    /**  variable declaration */
    private $url;                           // DragonPay URL
	private $merchant_id = 'CREDITBPO';     // Merchant ID to accesss API
    private $merchant_key = 'N6f9rEW3';     // Merchat Key for API access

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        /*  For Production Environment  */
		if (env("APP_ENV") == "Production") {
			$this->url = 'https://gw.dragonpay.ph/api/collect/v1';
		}
        /*  For Testing Environment     */
        else {
			$this->url = 'https://test.dragonpay.ph/api/collect/v1';
        }

        $this->transactionDb = new Transaction;
        $this->reportDb = new Entity;
        $this->bankSubscriptionDb = new BankSubscription;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start Checking Payment Status of Pending Payment as of " . date("Y-m-d h:i:sa"));

        /** Get pending payment on Transaction Table */ 
        $transactions = $this->transactionDb->getPendingTransactions();

        foreach($transactions as $trans){
            /** Get payment status of the transaction */
            $apiUrl = $this->url . '/refno/' .$trans->refno;
            $client = new GuzzleClient([
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key)
                ]
            ]);

            $r = $client->request('GET', $apiUrl);
            $data = json_decode($r->getBody()->getContents());
            $payment_status = $data->Status;
            // $payment_status = file_get_contents($this->url . '&merchantid='.$this->merchant_id . '&merchantpwd='.$this->merchant_key . '&txnid='.$trans->id.$trans->txn);

            /** Purchase Basic User Report */
            if($trans->type == PD_BASIC_USER_REPORT){
                /** Get report details */
                $report = Entity::where('entityid', $trans->entity_id)->first();

                /** Check is_paid status of the report 0-Paid, 1-Not Paid */
                if($report->is_paid == 0){ // not paid
                    /** Check payment status */
                    if($payment_status == "S"){
                        /** Update report Status to paid */
                        $this->reportDb->updateReportPaymentStatus($report->entityid);
                        /** Update transaction status */
                        $this->transactionDb->updateTransactionStatus($trans->id, $payment_status);

                        $organization_name = Bank::where('id', $report->current_bank)->pluck('bank_name')->first();
                        $receipt = PaymentReceipt::where('login_id', $report->loginid)
                                    ->where('reference_id', $trans->id)
                                    ->where('payment_method', PAYMENT_METHOD_DRAGONPAY)
                                    ->first();
                        if($organization_name == "Matching Platform" && $receipt->api_match_account_details !== ''){
                            $user = User::where('loginid', $report->loginid)->first();
                            // sent email using api recepient details
                            $mailHandler = new MailHandler();
                            $mailHandler->sendReceiptToAPIUser(
                                $receipt['loginid'],
                                [
                                    'receipt' => $receipt,
                                    'user' => $user
                                ],
                                'CreditBPO Payment Receipt'
                            );
                        }

                        echo "Payment for Report " . $report->entityid . " updated to Succesfull \n";
                    }else{
                        echo "Pending Payment for Report " . $report->entityid . ". \n";
                    }

                    if($report->is_premium == PREMIUM_REPORT){
                        $entity = Entity::where('entityid', $report->entityid)->first();
		                $this->sendPremiumReportNotification($entity);
                    }
                }
            }

            /** Puchase Supervisor Client Keys */
            if($trans->type == PD_SUPERVISOR_CLIENT_KEY){
                if($payment_status == "S") {
                    /*  Email Receipt to Client */
                    $receipt = PaymentReceipt::where('login_id', $trans->loginid)
                            ->where('reference_id', $trans->id)
                            // ->where('sent_status', 0)
                            ->where('payment_method', 2)
                            ->first();

                    if ($receipt) {
                        $receipt->sent_status = STS_OK;
                        $receipt->save();

                        if(env("APP_ENV") != "local"){
                            $user = User::find($trans->loginid);

                            if($user->parent_user_id != 0) {
                                $user = User::find($user->parent_user_id);
                            }

                            $mailHandler = new MailHandler();
                            $mailHandler->sendReceipt(
                                $trans->loginid,
                                [
                                    'receipt' => $receipt,
                                    'user' => $user
                                ],
                                'CreditBPO Payment Receipt'
                            );
                        }

                        BankSerialKeys::createKeys($trans->loginid, $receipt->item_quantity, $receipt->keyType);

                        /** Update transaction status */
                        $this->transactionDb->updateTransactionStatus($trans->id, $payment_status);

                        echo "Client " . $trans->loginid .  " succesfully purchase " . $receipt->item_quantity . " Supervsor CLient Keys " . $receipt->keyType ."Emails sent to the Supervisor account. \n";
                    }
                }
            }

            /** Supervisor Subscription */
            if($trans->type == PD_SUPERVISOR_SUBSCRIPTION){
                if($payment_status == "S"){
                    /** Update Supervsor Subscription */
                    $bank_subscription = $this->bankSubscriptionDb->getBankSubscription($trans->loginid);

                    /**Set expiration Date */
                    $date = new DateTime();
                    $start_day = $date->format('j');

                    $date->modify("+1 month");
                    $end_day = $date->format('j');

                    if ($start_day != $end_day){
                        $date->modify('last day of last month');
                    }

                    /** Check record of subscription if created */
                    if($bank_subscription){
                        /** Update Subscription Date */
                        $this->bankSubscriptionDb->updateSubscriptionDate($trans->loginid, $date->format('Y-m-d H:i:s'));

                    }else{
                        /** Create Subscription Record */
                        $subs = BankSubscription::create([
                            'bank_id' => $trans->loginid,
                            'expiry_date' => $date->format('Y-m-d H:i:s')
                        ]);
                    }

                    /** Send email Notification to User */
                    if(env("APP_ENV") != "local"){
                        $user = User::find($trans->loginid);
                        if($user->parent_user_id != 0) {
                            $user = User::find($user->parent_user_id);
                        }
                        $mailHandler = new MailHandler();
                        $mailHandler->sendReceipt( $trans->loginid,
                            [
                                'receipt' => $receipt,
                                'user' => $user,
                            ], 'CreditBPO Payment Receipt'
                        );
                    }

                    /** Update transaction status */
                    $this->transactionDb->updateTransactionStatus($trans->id, $payment_status);

                    echo "User " . $trans->loginid . " was sucessfully subscribed! \n";
                }
            }
        }
        $this->info("Done");
    }

    public function sendPremiumReportNotification($entity){
        $email = [];
        $pm = PremiumUser::where('is_deleted', 0)->get();
        $city = Zipcode::where('zip_code', $entity->zipcode)->first();
        $receiver = array();
        foreach ($pm as $value) {
            $receiver[]=$value->email;
        }
        if(count($receiver) > 0) {
            $email['email'] = $receiver;
        } else {
            $email['email'] = "ana.liza.bongat@creditbpo.com";
        }

        /*  Update Report to add Agency emails     */
        $entity = Entity::where('loginid', $entity->loginid)
                ->orderBy('entityid', 'desc')
                ->first();
        $entity->premium_report_sent_to = $email['email'];
        $entity->save();
        
        $save_array = array(
            'tab' => "registration1",
            'data' => array('biz-details-cntr')
        );

        do {
            $randomCode = ExternalLinkController::createRandomCode();
            $ql = QuestionnaireLink::where('url', $randomCode)->first();
        } while($ql != null);
        
        $ql = QuestionnaireLink::where("entity_id" , $entity->entityid)->get();
        
        if($ql->count() == 0) {
            QuestionnaireLink::create(array(
                'entity_id' => $entity->entityid,
                'url' => $randomCode,
                'panels' => json_encode($save_array)
            ));
    
            $url = URL::to('/') . '/premium-link/' . $randomCode;
            $loa = DB::table('tbldocument')->where('entity_id', $entity->entityid)->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)->first();

            try{
                Mail::send('emails.notifications.premium-report-notification',
                    array('url' => $url, 'entity' => $entity, 'city' => $city, 'loa' => $loa),
                    function($message) use ($email, $entity, $loa) {
                        $message->to($email['email'])
                                ->subject('CreditBPO Inquiry Requested:'.$entity->companyname);
                    }
                );
            }catch(Exception $e){
            
            }
        }
    }
}
