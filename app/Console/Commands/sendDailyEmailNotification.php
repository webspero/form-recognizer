<?php

namespace App\Console\Commands;

use User;
use Mail;
Use DB;
use Entity;
use \Exception as Exception;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;

class sendDailyEmailNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateDailyNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send list of started, awaiting and generated reports  for the day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $date_today = $now->toDateString();

        $currentDate = date('F d, Y h:i:A');

        $reportsSubmittedToday = [];
        
        /** Done reports */
        $doneReports = Entity::whereIn('is_premium', [SIMPLIFIED_REPORT, STANDALONE_REPORT]) // select simplified and standalone report
                                ->whereIn('status', [7]) // generated successfully(generated)
                                ->whereDate('completed_date', Carbon::today()->toDateString()) // check completed date
                                ->get();
        foreach($doneReports as $a){
            $reportsSubmittedToday[] = $a;
        }

        /** Awaiting Report */
        $awaitingReport = Entity::whereIn('is_premium', [SIMPLIFIED_REPORT, STANDALONE_REPORT]) // select simplified and standalone report
                                        ->whereIn('status', [5]) // awaiting report
                                        ->whereDate('submitted_at', Carbon::today()->toDateString()) // check created date
                                        ->get();
        foreach($awaitingReport as $a){
            $reportsSubmittedToday[] = $a;
        }

        /** Started Report */
        $startedReport = Entity::whereIn('is_premium', [SIMPLIFIED_REPORT, STANDALONE_REPORT]) // select simplified and standalone report
                                ->whereIn('status', [0]) // started report
                                ->whereDate('created_at', Carbon::today()->toDateString()) // check submitted date
                                ->get();
        foreach($startedReport as $a){
            $reportsSubmittedToday[] = $a;
        }

        /** Initialized list of developers */
        $developers = array(
            "leovielyn.aureus@creditbpo.com",
            "ana.liza.bongat@creditbpo.com",
        );

        if(env("APP_ENV") == "Production"){
            array_push($developers, "jose.francisco@creditbpo.com");
        }

        Mail::send('emails.notifications.daily-submitted-notification', 
                    array('report' => $reportsSubmittedToday, 
                          'currentDate' => $currentDate
                        ),
                function($message) use ($developers, $reportsSubmittedToday) {
                    $message->to($developers)
                            ->subject('CreditBPO List of Reports for Risk Rating');
                }
        );
    }
}
