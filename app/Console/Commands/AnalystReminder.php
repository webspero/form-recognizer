<?php

namespace App\Console\Commands;


use CreditBPO\Services\MailHandler;
use Illuminate\Console\Command;
use DateTime;
use DB;

class AnalystReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'AnalystReminder';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Send email notification of submitted reports for risk rating.';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(){
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
        $this->info("start process");
        $this->info(date("Y-m-d H:i:s"));

        //get submitted reports for risk taking
        $entity = DB::table('tblentity')
                ->where('tblentity.status', '=', 5)
                ->groupBy('tblentity.entityid')
                ->get();

        foreach($entity as $ent){
            if($ent->submitted_at == null){
                $submitted_date = $ent->updated_at;
            }else{
                $submitted_date = $ent->submitted_at;
            }

            $date_today     = date('Y-m-d H:i:s');
            $span = strtotime( $date_today) - strtotime($submitted_date);
            $days = floor($span / (60*60*24));
            $hours = floor(($span % (60*60*24)) / (60*60));
            $minutes = floor(($span % (60*60)) / 60);
            $seconds = floor($span % 60);

            $duration = array();

            if($days != 0){
                array_push($duration, $days.'d');
            }
            if($hours != 0){
                array_push($duration, $hours.'h');
            }
            if($minutes != 0){
                array_push($duration, $minutes.'m');
            }

            $queue = implode(" ", $duration);

            $ent->date_diff = $queue;
        }

        $emails = DB::table('analyst_account')->select('email')->where('is_deleted', 0)->get();
        $mail = array();

        for($x=0; $x<count($emails); $x++){
            array_push($mail, $emails[$x]->email);
        }
    
        $mailHandler = new MailHandler();
        $mailHandler->sendReminderNotificationAnalyst($entity, $mail);
        $cnt = count($entity);

        if($cnt == 0){
            echo "    There are no new submitted reports" . "\n";
        }else{
            echo "    There are " . $cnt . " new submitted records for risk rating." . "\n";
        }

        $this->info("end process");
    }
}
