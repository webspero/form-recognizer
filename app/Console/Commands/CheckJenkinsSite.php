<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class CheckJenkinsSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckJenkinsSite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if Jenkins site is up or down';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->jekinsLink = "http://128.199.203.18/jenkins/login?from=%2Fjenkins%2F";
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->isDomainAvailable($this->jekinsLink)) {
            echo "Up and running!";
        }else {
            $receiver = array(
                        "ana.liza.bongat@crditbpo.com", 
                        "leovielyn.aureus@creditbpo.com"
                    );
            Mail::send(
                'emails.notifications.jenkins_site_status',
                array(
                    'receiver' => $receiver
                ),
                function ($message) use ($receiver) {
                    $message->to($receiver)
                            ->subject('CreditBPO Jenkis Site');
                }
            );
        }
     
    }
      
    //-----------------------------------------------------
    //  Function X.XX: isDomainAvailable
    //      Check if URL is available
    //-----------------------------------------------------
    public function isDomainAvailable($url){
        $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";

        $ch=curl_init();
        curl_setopt ($ch, CURLOPT_URL,$url );
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch,CURLOPT_VERBOSE,false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch,CURLOPT_SSLVERSION,3);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
        $page=curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode>=200 && $httpcode<300){
            return true;
        }else{
            return false;
        }
    }
}
