<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Services\ResetEmailNotifSentService as ResetService;

class ResetEmailNotifSent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ResetEmailNotifSent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets All Notification Sent Flag in tblemailnotif';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->resetService = new ResetService($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("Start Reset Supervisor Notification");
        $this->resetService->resetNotifications();
        $this->info("Done Reset");
    }
}
