<?php

namespace App\Console\Commands;

use App\Mail\TranscriptionMailNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TranscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transcription:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transcription Notification First';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$transcription_reviewer = DB::table('tblexternal_transcriber_account')->select('email')->where('status', 1)->get();
        $reports = DB::table('automate_standalone_rating')
            ->join('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')
            ->where('automate_standalone_rating.status', 5)
            ->whereBetween('tblentity.created_at', [Date('Y-m-d' . ' 00:00:59'), Date('Y-m-d' . ' 11:59:59')])
            ->orderBy('entityid', 'DESC')
            ->get();
        if (count($transcription_reviewer) && count($reports)) {
            foreach ($transcription_reviewer as $receiwer) {

                $str = $receiwer->email;
                $start = '+';
                $end = '@';
                $replacement = '';
                $replacement = $start . $replacement . $end;

                $start = preg_quote($start, '/');
                $end = preg_quote($end, '/');
                $regex = "/({$start})(.*?)({$end})/";

                $result = preg_replace($regex, $replacement, $str);
                Mail::to(str_replace('+', '', $result))->send(new TranscriptionMailNotification($reports));
            }
        }
    }
}
