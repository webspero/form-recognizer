<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use PseCompany;

class UpdatePseCompaniesList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdatePseList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** Function to update PSE Company List */

        /**Load list of pse company from excel file */
        $path = public_path('documents/PSE_Edge_Company_List.xlsx');
        $data = Excel::load($path)->get();

        /** Sort values of file */
        if($data->count() > 0){
            foreach($data as $value){
                $pse_edge[] = array(
                    'id'  => $value['company_id'],
                    'companyname'   => $value['company_name'],
                    'code'  => $value['code']
                );
            }
        }
        foreach($pse_edge as $pse){
            $checkPse = PseCompany::where('pse_id', $pse['id'])->first();

            /**Check if company has record */
            if(!$checkPse){
                /** Create new record for new company */
                $pse_new = new PseCompany();
                $pse_new->pse_id = $pse['id'];
                $pse_new->company_name = $pse['companyname'];
                $pse_new->pse_code = $pse['code']; 
                $pse_new->status = 1;
                $pse_new->save();

                echo 'Company' . $pse['companyname'] . 'is added.' . "\n";

            }else{
                PseCompany::where('pse_id', $pse['id'])->update(['company_name' => $pse['companyname'], 'pse_code' => $pse['code'], 'status' => 1]);
                echo 'Company ' . $pse['companyname'] . 'is updated.' . "\n";
            }
        }
    }
}
