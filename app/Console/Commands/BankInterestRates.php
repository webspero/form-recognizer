<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Services\BankInterestRates as Service;
use CreditBPO\Models\BankInterestRates as Model;
class BankInterestRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "BankInterestRates {method=scrape} {--update-db=false}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve interest rate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new Service();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $start = time();
        $arguments = $this->argument();
		$this->checkDefaultValues();
		$options = $this->options();
		switch ($arguments['method']) {
			case 'scrape':
			default:
				$rates = $this->scrape();
		}

		if ($options['update-db'] !== false) {
			$model = new Model();
			$local = $model->getCurrentInterestRateByType('local');

			if (round($rates['local']['low'], 2) != $local->low ||
				round($rates['local']['mid'], 2) != $local->mid ||
				round($rates['local']['high'], 2) != $local->high) {
				$model->addInterestRate(
					round($rates['local']['low'], 2),
					round($rates['local']['mid'], 2),
					round($rates['local']['high'], 2),
					'local'
				);
			}

			$foreign = $model->getCurrentInterestRateByType('foreign');

			if (round($rates['foreign']['low'], 2) != $foreign->low ||
				round($rates['foreign']['mid'], 2) != $foreign->mid ||
				round($rates['foreign']['high'], 2) != $foreign->high) {
				$model->addInterestRate(
					round($rates['foreign']['low'], 2),
					round($rates['foreign']['mid'], 2),
					round($rates['foreign']['high'], 2),
					'foreign'
				);
			}
		}
    }

    protected function checkDefaultValues() {
		$model = new Model();

		if ($model->getPreviousInterestRate()) {
			return;
		}

		$default = $this->service->getDefaultValues();

		$model->addInterestRate(
			round($default['local']['low'], 2),
			round($default['local']['mid'], 2),
			round($default['local']['high'], 2),
			'local'
		);

		$model->addInterestRate(
			round($default['foreign']['low'], 2),
			round($default['foreign']['mid'], 2),
			round($default['foreign']['high'], 2),
			'foreign'
		);
    }
    
    /**
	 * Execute scraper to retrieve interest rates
	 *
	 * @return array
	 */
	protected function scrape() {
		$this->info("\n".'Running scraper'."\n");
		return $this->service->scrapeInterestRates();
	}
}
