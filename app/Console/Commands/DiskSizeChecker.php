<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Handlers\DiskSizeHandler;

class DiskSizeChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DiskChecker';
    protected $handler; 

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitor Disk Size Free Space';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->handler = new DiskSizeHandler();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Check disk free space as of " . date("Y-m-d H:i:s"));
        $this->handler->monitorSize();
        $this->info("end of scan");
    }
}
