<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Services\SendSupervisorNotificationsService as SendSupNotif;

class SendSupervisorNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendSupervisorNotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notification to supervisors on reports started/completed within the day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sendSupNotif = new SendSupNotif($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start Supervisor Daily Reports Notification");
        $this->info(date("Y-m-d H:i:s"));
        $this->sendSupNotif->sendNotification();
        $this->info("Done");
    }
}
