<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Services\SimpleFsInput;
use Maatwebsite\Excel\Facades\Excel;
use CreditBPO\Services\ComplexFsInput;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Document;
use Entity;
use ZipArchive;
use DB;

class RegenerateFSZipFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    /** Variable Declaration */
    const FS_INPUT_COMPLEX = 'files/FS Input Template - Complex Output.xlsx';

    protected $signature = 'RegenerateFSZipFile {--entityid=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate Zip FS File of done report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reportid = $this->option("entityid");

        // Get Report Information
        $report = Entity::where('entityid', $reportid)->first();

        if($report){
            // Check status of the report
            if($report->status == 7){
                // Get the zipfile
                $oldZip = new ZipArchive();
                $oldDocument = DB::table('tbldocument')->where('entity_id', $reportid)->where('is_deleted', 0)->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)->first();

                $old_file_path = public_path('documents/'. $oldDocument->document_rename);

                $oldDocument->document_orig = str_replace("Full","Basic",$oldDocument->document_orig);

                if($oldZip->open($old_file_path) === TRUE){
                    //Unzip Path
                    $oldZip->extractTo(public_path('/temp'));
                    $oldZip->close();

                    // Get the old file
                    if(file_exists(public_path('temp/'. $oldDocument->document_orig))){
                        $file = public_path('temp/'. $oldDocument->document_orig);
                        $this->regenerateZipFile($reportid, $file);
                    }else{
                        echo "File not found";
                    }
                }else{
                    echo "FS Input Template does not exist.";
                }
            }else{
                echo 'Report is in not yet done. We cannot regenerate the financial statement input template.';
            }
        }else{
            echo "Report does not exist.";
        }
    }

    public function regenerateZipFile($id, $file){
        $report = Entity::where('entityid', $id)->first();
        
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $currency = "PHP";
        $unit = 1;
        $document_type = 1;
        $upload_type = 5;
        $industryId = $report->industry_main_id;

        // zip file upload batch
        $destinationPath = public_path().'/documents/';

        /** Create new instance of zip file */
        $zip = new ZipArchive();

        /** Set File Name */
        $zip_filename = str_replace(' ', '_', $id.' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip'); 

        /** Attempt to open Zip File */
        if ($zip->open($destinationPath.$zip_filename, ZipArchive::CREATE)!==TRUE) {
            throw new Exception("cannot open <$zip_filename>\n");
        }

        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $document_group = 21;
        $document_orig = $id.' - FS Input Template for the period of ';

        $filename = rand(11111,99999).time().'.'.$extension;
        \File::copy($file, base_path('temp/'.$filename));

        // ---------------------------------------
        // Process if file uploaded is FS Template
        // ---------------------------------------
        if ($upload_type == 5 && in_array($extension, ['xlsx', 'xls'])) {
            $simpleFsData = [
                'entity_id' => $id,
                'money_factor' => $unit,
                'currency' => $currency,
                'industry_id' => $industryId,
            ];
            $simpleFsInput = new SimpleFsInput('temp/'.$filename, $simpleFsData);
            $result = $simpleFsInput->save();
            if (!$result) {
                $error = array(
                    'status' => STS_NG,
                    'error'  => $simpleFsInput->getErrorMessages()
                );
                return STS_NG;
            } else {
                $complexFsInput = new ComplexFsInput(
                    public_path(self::FS_INPUT_COMPLEX),
                    ['entity_id' => $id]
                );
                $complexFsInput->setDocumentGenerated(
                    $simpleFsInput->getDocumentGenerated()
                );
                $result = $complexFsInput->write($simpleFsInput->getCellValues());

                if ($result) {
                    // Update the old record on tbldocument
                    DB::table('tbldocument')->where('entity_id', $id)->where('is_deleted', 0)->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)->update(['is_deleted' => 1]);

                    $document_orig = $simpleFsInput->getDocumentFileName();
                    $file = $complexFsInput->getDocumentFileName();
                    $assets = $simpleFsInput->getTotalAssets();

                    if($document_group == DOCUMENT_GROUP_FS_TEMPLATE) {
                        $cond = array(
                            "is_deleted" => 0,
                            "entity_id" => $id,
                            "document_group" => DOCUMENT_GROUP_FS_TEMPLATE
                        );
                        $curDocs = Document::where($cond)->get();
                        foreach ($curDocs as $key => $value) {
                            $value->is_deleted = 1;
                            $value->save;
                            Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
                        }
                    }
                    /* create entry on database */
                    $data = [
                        'entity_id' => $id,
                        'document_type'=> $document_type,
                        'upload_type'=> $upload_type,
                        'document_group'=> DOCUMENT_GROUP_FS_TEMPLATE,
                        'document_orig'=> $file,
                        'document_rename'=> $zip_filename,
                        'created_at'=> date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s'),
                    ];
                    $documentid = DB::table('tbldocument')->insertGetId($data);

                    $master_ids[]       = $documentid;
                    $filenames[]        = $file;
                    $display_names[]    = $file;
                    $where = array(
                        "entity_id" => $id,
                        "is_deleted" => 0
                    );
                    $fs = FinancialReport::where($where)->first();
                    $zip->addFile(public_path('temp/'.$file), $file);
                }
            }
        } elseif($upload_type == 5 && $extension != 'xlsx'){
            echo "The file you were trying to upload is not a Financial Statement Template.";
            return STS_NG;
        }
    }
}
