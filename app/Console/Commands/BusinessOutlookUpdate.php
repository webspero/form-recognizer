<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CreditBPO\Services\BusinessOutlookUpdate as UpdateService;

class BusinessOutlookUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BusinessOutlookUpdate:autoUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automate updating of Business Outlook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $start = time();
        echo 'Running autoupdate for Business Outlook'."\n";
        $updater = new UpdateService();
        $result = $updater->autoUpdate();

        if ($result) {
            echo 'Finished updating'."\n";
        } else {
            echo 'Nothing to update'."\n";
        }

        $end = time() - $start;
        echo "\n".'Cron took '.$end. ' seconds to complete'."\n";
    }
}
