<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PseCompany;
use PseBalSheet;
use PseRegisteredCompany;
use PseCompanyFinancialReport;
use \Exception as Exception;
class PSEScraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PSEEdgeScraper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pse_db             = new PseCompany;                       // PSE Company Database Class
        $pse_balsheet_db    = new PseBalSheet;                      // PSE Company Balance Sheet Class
        $companies          = $pse_db->getActivePseCompanies();     // Get All PSE Companies from database

        $this->info('Extracting PSE Companies...');
        $this->info('Fetching available company information...');

        foreach($companies as $company){
            $search_query_string = '?cmpy_id='.$company->pse_id; // Search string to find company information by company id
            $company_information_url    = 'https://edge.pse.com.ph/companyInformation/form.do'.$search_query_string;
            $html   = file_get_html($company_information_url);

            /** COMPANY DESCRIPTION */
            $data_list_div  = $html->find ('#dataList'); // Fetch company description
            $description = '';
            foreach ($data_list_div as $data_list) {
                foreach ($data_list->find('table') as $data_list_table_key => $data_list_table)
                {
                    if($data_list_table_key == 0)
                    {
                        // Replace Company Description with space and Remove extra spacesss
                        $description = trim(str_replace("Company Description", "", $data_list_table->plaintext));
                    }
                    break;
                }
            }

            /** SECURITY INFORMATION */
            $sector = '';
            $sub_sector = '';
            $corporate_life = '';
            $incorporation_date = '';
            $number_of_directors = '';

            foreach ($html->find('table', 1)->find('tr') as $security_info_table_key => $security_info_table_value) {
                if ($security_info_table_key == 0) {
                    $sector = $security_info_table_value->children(1)->plaintext;
                } else if ($security_info_table_key == 1) {
                    $sub_sector = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 2) {
                    $corporate_life = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 3) {
                    $sec_date = null;
                    $incorporation_date = $security_info_table_value->children(1)->plaintext;
                    if($incorporation_date){
                        $sec_date = date('Y-m-d', strtotime($incorporation_date));
                    }
                } else if($security_info_table_key == 4) {
                    $number_of_directors = $security_info_table_value->children(1)->plaintext;
                }
            }

            /** Contact Information */
            $business_address = '';
            $email_address = '';
            $telephone_number = '';
            $website = '';
            $no_of_directors = '';

            foreach ($html->find('table', 2)->find('tr') as $security_info_table_key => $security_info_table_value) {
                if($security_info_table_key == 0) {
                    $business_address = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 1) {
                    $email_address = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 2) {
                    $telephone_number = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 3) {
                    $telephone_number = $telephone_number . '/' . $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 3){
                    $website = $security_info_table_value->children(1)->plaintext;
                } else if($security_info_table_key == 4){
                    $no_of_directors = $security_info_table_value->children(1)->plaintext;
                }
            }

            $company = PseCompany::where('pse_id', $company->pse_id)->first();

            $search_query_string = '?keyword='.$company->pse_id.'&tmplNm=Annual%20Report'; // Search string to find Annual report by company id
			$company_disclosures_url    = 'https://edge.pse.com.ph/companyDisclosures/search.ax'.$search_query_string;
			$html   = file_get_html($company_disclosures_url);

			$links  = array_reverse($html->find ('a')); // Find anchor element which contains Annual report edge no and reverse it

			$reports = PseCompanyFinancialReport::where('pse_company_id', $company->pse_id)->get();
			if (count($reports) > 0) {
				PseCompanyFinancialReport::where('pse_company_id', $company->pse_id)->update(['is_deleted' => 1]);
			}

            $isTinCompany = false;
			foreach($links as $link) {
				if($link->href === '#viewer' && $link->plaintext === 'Annual Report') {
					/* Extract edge no from onclick function */
					$edge_no = $this->get_string_between($link->onclick, "'","'");

					$openDiscViewerUrl    = 'https://edge.pse.com.ph/openDiscViewer.do?edge_no='.$edge_no;
					$openDiscHtml   = file_get_html($openDiscViewerUrl);

                    $annual_report_link = $openDiscHtml->getElementsByTagName('iframe[id=viewContents]')->getAttribute("src");

                    $openFileLink = 'https://edge.pse.com.ph/'.$annual_report_link;
                    $openFileLinkHtml = file_get_html($openFileLink);

                    if($isTinCompany == false){
                        $tin = $openFileLinkHtml->find('span[class=valInput]',2);
                        $postal = $openFileLinkHtml->find('span[class=valInput]',6);
                        if($tin && $postal){
                            $company_tin = $openFileLinkHtml->find('span[class=valInput]',2)->innertext;
                            $postal_code = $openFileLinkHtml->find('span[class=valInput]',6)->plaintext;
                        }
                        if(isset($company_tin) && isset($postal_code)){
                            $isTinCompany = true;
                        }
                    }
				}
			}

            /** Check PSE Registered Company */
            $checkPse = PseRegisteredCompany::where('id', $company->pse_id)->get();

            /** Check if has record on pse_registered_company */
            if(count($checkPse) == 0){
                $new_pse = new PseRegisteredCompany();
                $new_pse->id = $company->pse_id;
                $new_pse->name = $company->company_name;
                $new_pse->email_address = $email_address;
                $new_pse->website = $website;
                $new_pse->tel_num = $telephone_number;
                $new_pse->business_address = $business_address;
                $new_pse->company_description = $description;
                $new_pse->sector = $sector;
                $new_pse->sub_sector = $sub_sector;
                $new_pse->corporate_life = $corporate_life;
                $new_pse->num_directors = $no_of_directors;
                $new_pse->source = $company_information_url;
                if(isset($company_tin)){
                    $new_pse->company_tin = $company_tin;
                }
                if(isset($postal_code)){
                    $new_pse->postal_code = $postal_code;
                }
                if(isset($sec_date)){
                    $new_pse->sec_registration_date = $sec_date;
                }
                $new_pse->save();

                echo 'Company ' . $company->company_name . 'is added.' . "\n";
            }else{
                /** Update values if existing */
                PseRegisteredCompany::where('id', $company->pse_id)->update([
                    'name' => $company->company_name,
                    'email_address' => $email_address,
                    'website' => $website,
                    'tel_num' => $telephone_number,
                    'business_address' => $business_address,
                    'company_description' => $description,
                    'sector' => $sector,
                    'sub_sector' => $sub_sector,
                    'corporate_life' => $corporate_life,
                    'num_directors' => $no_of_directors,
                    'source' => $company_information_url,
                    'company_tin' => $company_tin,
                    'postal_code' => $postal_code,
                    'sec_registration_date' => $sec_date
                ]);
                echo 'Company ' . $company->company_name . 'is updated.' . "\n";
            }
        }
    }

    function get_string_between($string, $start, $end){
    	try{
	        $string = ' ' . $string;
	        $ini = strpos($string, $start);
	        if ($ini == 0) return '';
	        $ini += strlen($start);
	        $len = strpos($string, $end, $ini) - $ini;
	        return substr($string, $ini, $len);
        }catch(Exception $ex){
	        throw $ex;
		}
    }
}
