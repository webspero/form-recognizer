<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Entity;

class FixFreeReportsOfTrialAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FixFreeReportsOfTrialAccounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes for free reports location and industry of created trial accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("start");

        $reports = Entity::where('is_paid', 1)->where('is_independent', 1)->where('is_premium', 0)->get();

        foreach($reports as $report){
            /** Check location */
            if($report->province == ''){
                $this->setLocationToDefault($report->entityid);
                echo 'Update Location of Report ' . $report->entityid . "\n";
            }

            /** Check Industry  */
            if( ($report->industry_main_id == '') ){
                $this->setIndustryToDefault($report->entityid);
                echo 'Update Industry of Report ' . $report->entityid . "\n";
            }
        }
        $this->info("stop");
    }

    public function setLocationToDefault($id){
        Entity::where('entityid', $id)->update([
            'cityid'        => 1380,
            'province'      => "Abra",
            'countrycode'   => "PH",
            'zipcode'       => 2800
        ]);
        return;
    }

    public function setIndustryToDefault($id){
        Entity::where('entityid', $id)->update([
            'industry_main_id'  => "A",
            'industry_sub_id'   => "01",
            'industry_row_id'   => "011",
        ]);
        return;
    }
}
