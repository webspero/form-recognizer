<?php

//======================================================================
//  Class L3: Geocoder
//      Geocoder API Class for mapping addresses
//======================================================================
class Geocoder
{
	//-----------------------------------------------------
    //  Function L3.1: getGeocode
    //      Gets the longitude, latitude of an address and saves to entity
    //-----------------------------------------------------
	public static function getGeocode($entity_id)
	{
		$api_key = GOOGLE_SERVER_API_KEY;
		
		$e = DB::table('tblentity')
		->leftJoin('refcitymun', 'tblentity.cityid', 'refcitymun.id')
		->leftJoin('refprovince', 'refcitymun.provCode', 'refprovince.provCode')
		->where('entityid', $entity_id)
		->first();
		
		$address = $e->address1 .' '. $e->address2 .' '. $e->citymunDesc .' '. $e->provDesc .' Philippines';
		$url = "https://maps.googleapis.com/maps/api/geocode/json?";
		$url .= "address=".urlencode($address);
		$url .= "&key=".$api_key;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$string_data = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($string_data, true);
		
		if(isset($data['results'][0]['geometry']['location']['lat'])){
			$entity = Entity::find($e->entityid);
			$entity->longitude = $data['results'][0]['geometry']['location']['lng'];
			$entity->latitude = $data['results'][0]['geometry']['location']['lat'];
			$entity->save();
		}
	}
	
	//-----------------------------------------------------
    //  Function L3.2: getAddressBreakdown
    //      Gets the breakdown of a specific address
    //-----------------------------------------------------
	public static function getAddressBreakdown($address, $test = false)
	{
		$api_key = GOOGLE_SERVER_API_KEY;
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?";
		$url .= "address=".urlencode($address);
		$url .= "&key=".$api_key;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$string_data = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($string_data, true);
		
		if($test) return $data;
		
		$return = null;
		if(isset($data['results'][0]['address_components'][1]['long_name']) &&
				$data['results'][0]['address_components'][2]['long_name']) {
			$return = array(
				'city' => $data['results'][0]['address_components'][1]['long_name'],
				'province' => $data['results'][0]['address_components'][2]['long_name']
			);
		}

		return $return;
	}
}