<?php

//======================================================================
//  Class L10: Growth Forecast Library
//      Calculation of Growth Forecast
//======================================================================

class GrowthForecastLib
{
    var $entity_id;
    var $industry_id;
    var $standalone;
    
    private $cntForecast = 0;

    //-----------------------------------------------------
    //  Function L10.1: __construct
    //      Initialization
    //-----------------------------------------------------
    public function __construct($entity_id, $industry_id, $standalone) {
        $this->entity_id    = $entity_id;
        $this->industry_id  = $industry_id;
        $this->standalone   = $standalone;
    }

    //-----------------------------------------------------
    //  Function 45.3: computeOverallGrowthQtr
    //      Computes for the Overall Growth per Quarter
    //-----------------------------------------------------
    private function computeOverallGrowthQtr(&$fa_report)
    {
        /*  Variable Declaration        */
        $fa_report_income_statement = array();
        $num_fais                   = 0;
        $fais_idx                   = 0;
        
        $overall_growth             = 0;
        $overall_per_qtr            = 0;
        
        /*  Use only Income Statements with Revenue data    */
        foreach ($fa_report->incomeStatements() as $fais) {
            if (0 != $fais->Revenue) {
                $fa_report_income_statement[] = $fais;
            }
        }
        
        /*  Computes for the Overall Growth                 */
        $num_fais   = @count($fa_report_income_statement);
        
        if (0 < $num_fais) {
            $fais_idx   = $num_fais - 1;

            if (0 != $fa_report_income_statement[$fais_idx]->Revenue) {
                $overall_growth = ($fa_report_income_statement[0]->Revenue / $fa_report_income_statement[$fais_idx]->Revenue) - 1;
            }
            
            /*  Computes for the Overall Growth per Quarter     */
            $overall_per_qtr = $overall_growth / ($num_fais * 4);
        }
        
        return $overall_per_qtr;
    }

    //-----------------------------------------------------
    //  Function 45.3: computeCompanyGrowth
    //      Computes for the Company Growth Forecast
    //-----------------------------------------------------
    private function computeCompanyGrowth(&$revenuepotential, $ind_data)
    {
        $forecast           = array();
        $forecast['sts']    = STS_NG;
        
        /*--------------------------------------------------------------------
        |	When there is an existing Financial Report
        |-------------------------------------------------------------------*/
        $fa_report = FinancialReport::where(['entity_id' => $this->entity_id, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();
        
        if (NULL != $fa_report) {
            /*---------------------------------------------------------------
            |	Compute for the Overall Growth per Quarter
            |--------------------------------------------------------------*/
            $overall_per_qtr    = $this->computeOverallGrowthQtr($fa_report);
            
            if ((0 == $this->standalone)
            && (is_array($revenuepotential))
            && (0 < @count($revenuepotential))) {
                switch ($revenuepotential[0]->current_market) {
                    case 1:
                        $cmc = 0.01;
                        break;
                        
                    case 2: 
                        $cmc = 0.02;
                        break;
                        
                    case 3:
                        $cmc = 0.04;
                        break;
                        
                    default:
                        $cmc = 0.06;
                        break;
                }
                
                switch ($revenuepotential[0]->new_market) {
                    case 1:
                        $fmc = 0.01;
                        break;
                        
                    case 2:
                        $fmc = 0.02;
                        break;
                        
                    case 3:
                        $fmc = 0.04;
                        break;
                        
                    default:
                        $fmc = 0.06;
                        break;
                }
                
                $forecast['q1year1']  = 1 + ((($cmc + $overall_per_qtr) / 2) *  4);
                $forecast['q1year1']  = ($forecast['q1year1'] + $ind_data['gf_year1']) / 2;
                
                $forecast['q1year2']  = $forecast['q1ear1'] + ($overall_per_qtr *  4);
                $forecast['q1year2']  = ($forecast['q1year2'] + $ind_data['gf_year2']) / 2;
                
                $forecast['q1year3']  = $forecast['q1year2'] + ((($fmc + $overall_per_qtr) / 2) *  4);
                $forecast['q1year3']  = ($forecast['q1year3'] + $ind_data['gf_year3']) / 2;
            } else {
                $forecast['q1year1']  = 1 + ($overall_per_qtr *  4);
                $forecast['q1year1']  = ($forecast['q1year1'] + $ind_data['gf_year1']) / 2;
                
                $forecast['q1year2']  = $forecast['q1year1'] + ($overall_per_qtr *  4);
                $forecast['q1year2']  = ($forecast['q1year2'] + $ind_data['gf_year2']) / 2;
                
                $forecast['q1year3']  = $forecast['q1year2'] + ($overall_per_qtr *  4);
                $forecast['q1year3']  = ($forecast['q1year3'] + $ind_data['gf_year3']) / 2;
            }
            
            $forecast['sts']    = STS_OK;
        }
        
        return $forecast;
    }

    public function getGrowthForecastData()
    {
        /*  Variable Declaration        */
        $ind_data               = NULL;
        $forecast_data          = NULL;
        
        $slope                  = 0;
        $company_gain           = 0;
        $gain_desc              = STR_EMPTY;
        $gf_gain_desc           = STR_EMPTY;
        $ind_growth             = array();
        
        $desc[0]                = trans('risk_rating.gf_base_1');
        $desc[1]                = trans('risk_rating.gf_base_2');
        $desc[2]                = trans('risk_rating.gf_base_3');
        $desc[3]                = trans('risk_rating.gf_pre_overall');
        $desc[4]                = trans('risk_rating.gf_base_current');
        $desc[5]                = trans('risk_rating.gf_base_future');

        $forecast_data['description']   = '';
        
        $revenuepotential = DB::table('tblrevenuegrowthpotential')
            ->where('entity_id', '=', $this->entity_id)
            ->get();
            
        /*---------------------------------------------------------------
        |	Get slope data of the Industry
        |--------------------------------------------------------------*/
        if (0 != $this->industry_id) {
            $ind_data  = Industrymain::find($this->industry_id);
        }

        if(Auth::check() && Auth::user()->role != USER_BASIC_USER ){
            $rPath = Request::path();
        }else{
            $rPath = "";
        }

        $fa_report = FinancialReport::where(['entity_id' => $this->entity_id, 'is_deleted' => 0])
            ->where('is_deleted', 0)
            ->orderBy('id', 'desc')
            ->first();
        
        // Call HoltWinter
        $industry = $this->getHoltWinterPython();
        // if($industry && count($industry['industry']) <= 4 && $this->cntForecast == 0) {
        //     $industry = $this->getHoltWinterPython();
        // }

        // echo '<pre>';
        // print_r($industry);
        // echo '</pre>';
        // exit;
        /*---------------------------------------------------------------
        |	Computes for the Company Growth Forecast
        |--------------------------------------------------------------*/

        // $companyGrowth = array();
        // if((strpos($rPath, "summary" ) !== false||strpos($rPath,"check" )!== false || $rPath== "/" || strpos($rPath, "premium-link") !== false) && array_key_exists("industry", $industry))
        // {
        //     $companyGrowth['gf_year1'] = $industry['year1'];
        //     $companyGrowth['gf_year2'] = $industry['year2'];
        //     $companyGrowth['gf_year3'] = $industry['year3'];
        // }  else {
        //     $companyGrowth = $ind_data;
        // }
        $company            = $this->computeCompanyGrowth($revenuepotential, $ind_data);
        if ((STS_OK == $company['sts']) && (NULL != $ind_data)) {
            $forecast_data['sts']   = STS_NG;
            $agf = array();
            $sum = 0;
            if($industry && array_key_exists("industry", $industry)) {
                for($i =0; $i < count($industry['industry']); $i++) {
                    $sum += $industry['industry'][$i];
                    if(($i+1) % 4 == 0) {
                        $agf[] = $sum/4;
                        $sum = 0;
                    }
                }
            }

            // echo '<pre>';
            // print_r($agf);
            // echo '</pre>';
            // exit;
           
            $industry['base']   = 0;
            if(count($agf) > 1  ) {
            	//echo 'agf'; exit;
            	$dif = $agf[2] - $agf[1];
                $industry['q1year1']  = round($agf[1] - 3*$dif/4, 2);
                $industry['q2year1']  = round($agf[1] - 2*$dif/4, 2);
                $industry['q3year1']  = round($agf[1] - $dif/4, 2);
                $industry['q4year1']  = round($agf[1], 2);

                $industry['q1year2']  = round($agf[2] - 3*$dif/4, 2);
                $industry['q2year2']  = round($agf[2] - 2*$dif/4, 2);
                $industry['q3year2']  = round($agf[2] - $dif/4, 2);
                $industry['q4year2']  = round($agf[2], 2);

                $industry['q1year3']  = round($agf[3] - 3*$dif/4, 2);
                $industry['q2year3']  = round($agf[3] - 2*$dif/4, 2);
                $industry['q3year3']  = round($agf[3] - $dif/4, 2);
                $industry['q4year3']  = round($agf[3], 2);
                $industry['q1year4']  = round($agf[3] + $dif/4, 2);
            } else {
            	//echo 'not agf'; exit;
            	$dif = $ind_data['gf_year2'] - $ind_data['gf_year1'];

            	$industry['q1year1']  = round(($ind_data['gf_year1'] - 3*$dif/4) * 100, 2) - 100;
            	$industry['q2year1']  = round(($ind_data['gf_year1'] - 2*$dif/4) * 100, 2) - 100;
            	$industry['q3year1']  = round(($ind_data['gf_year1'] - $dif/4) * 100, 2) - 100;
            	$industry['q4year1']  = round(($ind_data['gf_year1']) * 100, 2) - 100;

            	$industry['q1year2']  = round(($ind_data['gf_year2'] - 3*$dif/4) * 100, 2) - 100;
            	$industry['q2year2']  = round(($ind_data['gf_year2'] - 2*$dif/4) * 100, 2) - 100;
            	$industry['q3year2']  = round(($ind_data['gf_year2'] - $dif/4) * 100, 2) - 100;
            	$industry['q4year2']  = round(($ind_data['gf_year2']) * 100, 2) - 100;

            	$industry['q1year3']  = round(($ind_data['gf_year3'] - 3*$dif/4) * 100, 2) - 100;
            	$industry['q2year3']  = round(($ind_data['gf_year3'] - 2*$dif/4) * 100, 2) - 100;
            	$industry['q3year3']  = round(($ind_data['gf_year3'] - $dif/4) * 100, 2) - 100;
            	$industry['q4year3']  = round(($ind_data['gf_year3']) * 100, 2) - 100;

            	$industry['q1year4']  = round(($ind_data['gf_year3'] + $dif/4) * 100, 2) - 100;
            }

          //print_r($industry); exit;
            
            
            $company['base']    = 0;
            $company['q1year1']   =  round($company['q1year1'] * 100, 2) - 100;
            $company['q1year2']   = $company['q1year1']  + round($company['q1year2'] * 100, 2) - 100;
            $company['q1year3']   = $company['q1year2'] + round($company['q1year3'] * 100, 2) - 100;
            
            /*  Paragraph 1 of Growth Forecast Description   */
            // $ind_growth[0]    = $industry['year1'] - $industry['base'];
            // $ind_growth[1]    = $industry['year2'] - $industry['year1'];
            // $ind_growth[2]    = $industry['year3'] - $industry['year2'];

            $ind_growth[0]    = $industry['q1year1'] - $industry['base'];
            $ind_growth[1]    = $industry['q2year1'] - $industry['q1year1'];
            $ind_growth[2]    = $industry['q3year1'] - $industry['q2year1'];
            $ind_growth[3]    = $industry['q4year1'] - $industry['q3year1'];

            $ind_growth[4]    = $industry['q1year2'] - $industry['q4year1'];
            $ind_growth[5]    = $industry['q2year2'] - $industry['q1year2'];
            $ind_growth[6]    = $industry['q3year2'] - $industry['q2year2'];
            $ind_growth[7]    = $industry['q4year2'] - $industry['q3year2'];

            $ind_growth[8]    = $industry['q1year3'] - $industry['q4year2'];
            $ind_growth[9]    = $industry['q2year3'] - $industry['q1year3'];
            $ind_growth[10]    = $industry['q3year3'] - $industry['q2year3'];
            $ind_growth[11]    = $industry['q4year3'] - $industry['q3year3'];
            $ind_growth[12]    = $industry['q1year4'] - $industry['q4year3'];

            $ind_growth_desc[0] = $industry['q1year1'] - $industry['base'];
            $ind_growth_desc[1] = $industry['q1year2'] - $industry['q1year1'];
            $ind_growth_desc[2] = $industry['q1year3'] - $industry['q1year2'];
   
            
            foreach ($ind_growth_desc as $key => $val) {
                $line_num   = $key + 1;
                $desc[$key] .= ' ';
                
                if ($ind_growth_desc[$key] >= 3) {
                    $desc[$key] .= trans('risk_rating.gf_up_'.$line_num);
                } else if ($ind_growth_desc[$key] <= -3) {
                    $desc[$key] .= trans('risk_rating.gf_down_'.$line_num);
                } else {
                    $desc[$key] .= trans('risk_rating.gf_neutral_'.$line_num);
                }
                $desc[$key] .= '.<br/>';
            }
            
            /*  Paragraph 2 of Growth Forecast Description   */
            $company_gain   =  round($company['q1year3'], 1);
            $gain_desc      = $company_gain.'% ';
            
            if (0 < $company_gain) {
                $gf_gain_desc   = '+'.$gain_desc;
            } else {
                $gf_gain_desc   = '0% ';
            }
            
            $desc[3]    .= ' '.$gf_gain_desc;
            $desc[3]    .= trans('risk_rating.gf_post_overall');
            $desc[3]    .= '.<br/>';
            
            if (0 == $this->standalone) {
                if ((is_array($revenuepotential))
                && (0 < @count($revenuepotential))) {
                    $desc[4]    .= ' ';
                    switch ($revenuepotential[0]->current_market) {
                        case 1:
                            $desc[4] .= trans('risk_rating.gf_current_growth_0');
                            break;
                            
                        case 2: 
                            $desc[4] .= trans('risk_rating.gf_current_growth_less_3');
                            break;
                            
                        case 3:
                            $desc[4] .= trans('risk_rating.gf_current_growth_3_to_5');
                            break;
                            
                        default:
                            $desc[4] .= trans('risk_rating.gf_current_growth_more_than_5');
                            break;
                    }
                    
                    $desc[4]    .= '.<br/>';
                    $desc[5]    .= ' ';
                    
                    switch ($revenuepotential[0]->new_market) {
                        case 1:
                            $desc[5] .= trans('risk_rating.gf_future_growth_0');
                            break;
                            
                        case 2:
                            $desc[5] .= trans('risk_rating.gf_future_growth_less_3');
                            break;
                            
                        case 3:
                            $desc[5] .= trans('risk_rating.gf_future_growth_3_to_5');
                            break;
                            
                        default:
                            $desc[5] .= trans('risk_rating.gf_future_growth_more_than_5');
                            break;
                    }
                    $desc[5]    .= '.<br/>';
                }
                else {
                    $desc[4]    .= trans('risk_rating.gf_current_growth_0').'.<br/>';
                    $desc[5]    .= trans('risk_rating.gf_future_growth_0').'.<br/>';
                }
            }
            
            $forecast_data['description']   = $desc[0];
            $forecast_data['description']   .= $desc[1];
            $forecast_data['description']   .= $desc[2];
            
            $forecast_data['description']   .= '<br/>';
            $forecast_data['description']   .= $desc[3];

			//echo $forecast_data['description']; exit;            
            
            if (0 == $this->standalone) {
                $forecast_data['description']   .= $desc[4];
                $forecast_data['description']   .= $desc[5];
            }
            if((strpos($rPath, "summary" ) !== false||strpos($rPath,"check" )!== false || $rPath== "/" || strpos($rPath, "premium-link") !== false) && array_key_exists("industry", $industry)){
                for($i = 0; $i < count($industry['industry']) - 3; $i++ ) {
                    $forecast_data['industry'][$i] = $industry['industry'][$i];
                }

                if(strpos($rPath, "summary" ) !== false || strpos($rPath, "premium-link") !== false) {
                   $forecast_data['company'][0]    = [0, $company['base']];
                   $forecast_data['company'][1] = NULL;
                    $forecast_data['company'][2] = NULL;
                    $forecast_data['company'][3] = NULL;
                    $forecast_data['company'][4]    = [4, $company['q1year1']];
                    $forecast_data['company'][5] = NULL;
                    $forecast_data['company'][6] = NULL;
                    $forecast_data['company'][7] = NULL;
                    $forecast_data['company'][8]    = [8, $company['q1year2']];
                    $forecast_data['company'][9] = NULL;
                    $forecast_data['company'][10] = NULL;
                    $forecast_data['company'][11] = NULL;
                    $forecast_data['company'][12]    = [12,$company['q1year3']];
                } else {
                    $forecast_data['company'][0]    = ["Q1 " . $fa_report->year ,$company['base']];
                    $forecast_data['company'][1] = NULL;
                    $forecast_data['company'][2] = NULL;
                    $forecast_data['company'][3] = NULL;
                    $forecast_data['company'][4]    = ["Q1 " . ($fa_report->year + 1), $company['q1year1']];
                    $forecast_data['company'][5] = NULL;
                    $forecast_data['company'][6] = NULL;
                    $forecast_data['company'][7] = NULL;
                    $forecast_data['company'][8]    = ["Q1 " . ($fa_report->year + 2), $company['q1year2']];
                    $forecast_data['company'][9] = NULL;
                    $forecast_data['company'][10] = NULL;
                    $forecast_data['company'][11] = NULL;
                    $forecast_data['company'][12]    = ["Q1 " . ($fa_report->year + 3), $company['q1year3']];
                }
                
            } else {
                $forecast_data['company'][0]    = $company['base'];
                $forecast_data['company'][1]    = NULL;
                $forecast_data['company'][2]    = NULL;
                $forecast_data['company'][3]    = NULL;
                $forecast_data['company'][4]    = $company['q1year1'];
                $forecast_data['company'][5]    = NULL;
                $forecast_data['company'][6]    = NULL;
                $forecast_data['company'][7]    = NULL;
                $forecast_data['company'][8]    = $company['q1year2'];
                $forecast_data['company'][9]    = NULL;
                $forecast_data['company'][10]    = NULL;
                $forecast_data['company'][11]    = NULL;
                $forecast_data['company'][12]    = $company['q1year3'];


                // echo '<pre>';
                // print_r($industry);
                // echo '</pre>';
                // exit;
                $forecast_data['industry'][0]   = $industry['q1year1'];
                $forecast_data['industry'][1]   = $industry['q2year1'];
                $forecast_data['industry'][2]   = $industry['q3year1'];
                $forecast_data['industry'][3]   = $industry['q4year1'];

                $forecast_data['industry'][4]   = $industry['q1year2'];
                $forecast_data['industry'][5]   = $industry['q2year2'];
                $forecast_data['industry'][6]   = $industry['q3year2'];
                $forecast_data['industry'][7]   = $industry['q4year2'];

                $forecast_data['industry'][8]   = $industry['q1year3'];
                $forecast_data['industry'][9]   = $industry['q2year3'];
                $forecast_data['industry'][10]   = $industry['q3year3'];
                $forecast_data['industry'][11]   = $industry['q4year3'];
                $forecast_data['industry'][12]   = $industry['q1year4'];

            }
            $forecast_data['gf_icon']       = $this->getGraphIcon($company);
            $forecast_data['sts']           = STS_OK;
        }
        
        // echo '<pre>';
        // print_r($forecast_data);
        // echo '</pre>';
        // exit;
        if(!empty($industry['industry']))
            $forecast_data['industry'] = $industry['industry'];
        unset($forecast_data['industry'][13]);
        unset($forecast_data['industry'][14]);
        unset($forecast_data['industry'][15]);
        //print_r($forecast_data); exit;
        return $forecast_data;
    }
    
    function getGraphIcon($company)
    {
        $gf_type    = '000';
        $graph_icon = STR_EMPTY;
        $year1      = 0;
        $year2      = 0;
        $year3      = 0;
        
        if ($company['q1year1'] >= $company['base']) {
            $year1  = 1;
        }
        
        if ($company['q1year2'] >= $company['q1year1']) {
            $year2  = 1;
        }
        
        if ($company['q1year3'] >= $company['q1year2']) {
            $year3  = 1;
        }
        
        $gf_type    = $year1.$year2.$year3;
        $graph_icon = URL::To('images/gf_graph/gf'.$gf_type.'.png');
        
        return $graph_icon;
    }

    private function getHoltWinter()
    {
        $industry = array();
        $rPath = Request::path();
        $fa_report = FinancialReport::where(['entity_id' => $this->entity_id, 'is_deleted' => 0])
            ->where('is_deleted', 0)
            ->orderBy('id', 'desc')
            ->first();
         $indexInd = 0;
         $subId  = Entity::find($this->entity_id);

        // if(strpos($rPath, "summary") !== false || strpos($rPath,"check" )!== false || $rPath== "/" || strpos($rPath, "premium-link") !== false ){
            //get sub industry id by entity
            $subTitle = ""; 
            $realValue = array();

             
            $gfr = DB::table("growth_forecast_results")->where('main_id',$subId->industry_row_id)->where("year", $fa_report->year)->first();
            if($gfr) {
                $rValue = array($gfr->Q1, $gfr->Q2, $gfr->Q3, $gfr->Q4, $gfr->Q5, $gfr->Q6, $gfr->Q7, $gfr->Q8, $gfr->Q9, $gfr->Q10, $gfr->Q11, $gfr->Q12, $gfr->Q13, $gfr->Q14, $gfr->Q15, $gfr->Q16);
                for($x = 0; $x < count($rValue); $x++){
                    if($x == 0) {
                        $industry['industry'][$x] = 0;
                    } else {
                        $industry['industry'][$x] = ($industry['industry'][$x-1] + $rValue[$x])/4; 
                    }
                }

                // $industry['year1'] = $gfr->gf_year1;
                // $industry['year2'] = $gfr->gf_year2;
                // $industry['year3'] = $gfr->gf_year3;

                // $dif = $gfr[0]->gf_year2 - $gfr[0]->gf_year1;
                // $industry['q1year1'] = $gfr[0]->gf_year1 - 3*$dif/4;
                // $industry['q2year1'] = $gfr[0]->gf_year1 - 2*$dif/4;
                // $industry['q3year1'] = $gfr[0]->gf_year1 - $dif/4;
                // $industry['q4year1'] = $gfr[0]->gf_year1;

                // $industry['q1year2'] = $gfr[0]->gf_year2 - 3*$dif/4;
                // $industry['q2year2'] = $gfr[0]->gf_year2 - 2*$dif/4;
                // $industry['q3year2'] = $gfr[0]->gf_year2 - $dif/4;
                // $industry['q4year2'] = $gfr[0]->gf_year2;

                // $industry['q1year3'] = $gfr[0]->gf_year3 - 3*$dif/4;
                // $industry['q2year3'] = $gfr[0]->gf_year3 - 2*$dif/4;
                // $industry['q3year3'] = $gfr[0]->gf_year3 - $dif/4;
                // $industry['q4year3'] = $gfr[0]->gf_year3;


            } else {
                if($subId){
                    $subIndustry = Industrysub::find($subId["industry_sub_id"]);
                    $rowIndustry = Industryrow::find($subId["industry_row_id"]);
                    $subTitle = $subIndustry["sub_title"];
                    $rowTitle = $rowIndustry["row_title"];
                }
                $latestYear = $fa_report->year;
    
                // execute rscript for forecasting
                $file = public_path("test") . "/Rscripts/pipe.R $latestYear";	
                if(env("APP_ENV") == 'local'){
                    exec("C:\Users\kenneth.manongdo\Documents\R\R-3.5.3\bin\Rscript " . $file, $output );    
                } else {
                    exec("Rscript " . $file, $output );
                }
                $rValue = $output;
                $i = 0;
                $array = array();
                $cnt = count($rValue);
                
                foreach($rValue as $key => $val)
                {
                    $tab = preg_split("/(\s\s+)/", $val);
                    if(count($tab) == 2){
                        if(strpos($tab[1],"forward_yr")!==false || strpos($tab[1],"base_yr")!==false ){
                            $i++;
                        }
                        if($i == 1){
                            $array[0][] = str_replace("<U+0085>", "", $tab[0]);
                        }
                        $array[$i][] = $tab[1];
                    }
                }
               
                if(count($array) != 0 ){
                    
                    $industryClass = DB::table("industry_class")->select("name")->where("main_id",$subId['industry_row_id'])->orWhere("mapping", "like", $subId['industry_row_id'])->first();
                    $industryClassSub = DB::table("industry_class")->select("name")->where("industry_sub",$subId['industry_sub_id'])->first();
                    $industryClassMain = DB::table("industry_class")->select("name")->where("industry_main",$subId['industry_main_id'])->first();
                    $passIndData = array();
                   
                    $mapping = DB::table("industry_class")->select("*")->get();
                    $mapFind = 0;
                    $mapData = array();
                    foreach($mapping as $map) {
                        if($map->mapping){
                            $mappingIds = explode(",", $map->mapping);
                            foreach ($mappingIds as $ids) {
                                $id = str_replace(" ","", $ids);
                                if($subId["industry_row_id"] == $id){
                                    $mapFind = 1;
                                    $mapData = $map;
                                    break;
                                }
                            }
                        }
                    }
                    
                    
                    if($industryClass){
                        $groupData = array();
                        foreach ($industryClass as $indClass) {
                            $cont = array(); 
                            if(gettype($indClass) == 'string') {
                                $indexInd = array_search($indClass,$array[0]); 
                            } else {
                                $indexInd = array_search($indClass->name,$array[0]); 
                            }
                            for ($i=0; $i < count($array) ; $i++) { 
                                if(count($array[$i]) != 73 && $indexInd == 72){
                                    $cont[] = $array[$i][$indexInd -1];
                                } else {
                                    $cont[] = $array[$i][$indexInd];
                                }
                            }
                            $groupData[] = $cont;
                        }
                        
                        /** Compute for weighted here */
                        if (count($groupData) > 1) {
                            $passIndData[] = 0;
                            $realValue[] = 0;
                            for ($i=1; $i < count($groupData[0]); $i++) { 
                                $meanGroup = 0;
                                for ($j=0; $j < count($groupData); $j++) { 
                                    $meanGroup += $groupData[$j][$i];
                                }
    
                                if(empty($passIndData)) {
                                    $passIndData[] = round(($meanGroup/count($groupData)) *100, 2);
                                } else {
                                    end($passIndData);
                                    $key = key($passIndData);
                                    $passIndData[] = $passIndData[$key]  + round(($meanGroup/count($groupData)) *100, 2);
                                }
                                $realValue[] = round(($meanGroup/count($groupData)) *100, 2);
                            }
                        }
                    } elseif ($mapFind > 0){ 
                        $indexInd = array_search($mapData->name,$array[0]);
                    } elseif ($industryClassSub) {
                        $indexInd = array_search($industryClassSub->name,$array[0]);
                    } elseif ($industryClassMain) {
                        $indexInd = array_search($industryClassMain->name,$array[0]);
                    } 
                }  else {
                    $indexInd = 0;
                }
                if((strpos($rPath, "summary" ) !== false || strpos($rPath,"check" )!== false || $rPath== "/" || strpos($rPath, "premium-link") !== false) && $indexInd != 0 && count($array) > 0){
                    if(!empty($passIndData)) {
                        $industry['industry'] = $passIndData;
                    } else {
                        for($x = 0; $x <= 15; $x++) {
                            if($x == 0 ) {
                                $industry['industry'][] = 0;
                                $realValue[] = 0;
                                continue;
                            } elseif ( $indexInd >= count($array[$x]) ) {
                                $indexInd = count($array[$x]) - 1;
                            }
                            end($industry['industry']);
                            $key = key($industry['industry']);
                            $industry['industry'][] = ($industry['industry'][$key] + round(( $array[$x][$indexInd]) * 100, 2))/4;
                            $realValue[] =  round(( $array[$x][$indexInd]) * 100, 2);
                        }
                    }
                    
                    // $industry['base'] = 0;
                    // $industry['year1'] = round((($realValue[4] + $realValue[5] + $realValue[6] + $realValue[7])/4) , 2);
                    // $industry['year2'] = round((($realValue[8] + $realValue[9] + $realValue[10] + $realValue[11])/4) , 2);
                    // $industry['year3'] = round((($realValue[12] + $realValue[13] + $realValue[14] + $realValue[15])/4), 2);
                    
                    // $industry['q1year1'] = round($realValue[1] , 2);
                    // $industry['q2year1'] = round($realValue[2] , 2);
                    // $industry['q3year1'] = round($realValue[3] , 2);
                    // $industry['q4year1'] = round($realValue[4] , 2);

                    // $industry['q1year2'] = round($realValue[5] , 2);
                    // $industry['q2year2'] = round($realValue[6] , 2);
                    // $industry['q3year2'] = round($realValue[7] , 2);
                    // $industry['q4year2'] = round($realValue[8] , 2);

                    // $industry['q1year3'] = round($realValue[9] , 2);
                    // $industry['q2year3'] = round($realValue[10] , 2);
                    // $industry['q3year3'] = round($realValue[11] , 2);
                    // $industry['q4year3'] = round($realValue[12] , 2);


                }
                //insert result to table
                if(array_key_exists('industry', $industry) && count($industry['industry']) > 4) {
                    $insert = array(
                        'Q1' => 0,
                        'Q2' => $realValue[1],
                        'Q3' => $realValue[2],
                        'Q4' => $realValue[3],
                        'Q5' => $realValue[4],
                        'Q6' => $realValue[5],
                        'Q7' => $realValue[6],
                        'Q8' => $realValue[7],
                        'Q9' => $realValue[8],
                        'Q10' => $realValue[9],
                        'Q11' => $realValue[10],
                        'Q12' => $realValue[11],
                        'Q13' => $realValue[12],
                        'Q14' => $realValue[13],
                        'Q15' => $realValue[14],
                        'Q16' => $realValue[15],
                        'gf_year1' => $industry['year1'],
                        'gf_year2' => $industry['year2'],
                        'gf_year3' => $industry['year3'],
                        'main_id' => $subId->industry_row_id,
                        'year' => $fa_report->year,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                    DB::table("growth_forecast_results")->insert($insert);
                }
            }
        //}
        
        $this->cntForecast = 1;
        return $industry;
    }

    private function getHoltWinterPython(){

        $industry = array();
        $ids = array();
        $indexInd = 0;

        $rPath = Request::path();
        $fa_report = FinancialReport::where(['entity_id' => $this->entity_id, 'is_deleted' => 0])
            ->where('is_deleted', 0)
            ->orderBy('id', 'desc')
            ->first();
        $entity = Entity::find($this->entity_id);
       
        // Scan Growth Forecast Results already exist for the same industry and year.
        // if((strpos($rPath, "summary") !== false || strpos($rPath,"check" ) !== false || $rPath== "/" || strpos($rPath, "premium-link") !== false)){

            $gfr = DB::table("growth_forecast_results")->where('main_id',$entity->industry_row_id)->where("year", $fa_report->year)->get();
            if($gfr->count() >= 1 ) {
                $data = array($gfr[0]->Q1, $gfr[0]->Q2, $gfr[0]->Q3, $gfr[0]->Q4, $gfr[0]->Q5, $gfr[0]->Q6, $gfr[0]->Q7, $gfr[0]->Q8, $gfr[0]->Q9, $gfr[0]->Q10, $gfr[0]->Q11, $gfr[0]->Q12, $gfr[0]->Q13, $gfr[0]->Q14, $gfr[0]->Q15, $gfr[0]->Q16);
                for($x = 0; $x < count($data); $x++){
                    if($x == 0) {
                        $industry['industry'][$x] = 0;
                    } else {
                        $industry['industry'][$x] = ($industry['industry'][$x-1] + $data[$x])/4; 
                    }
                }
                //print_r($industry); exit;
                //echo $gfr->Q1; exit;
                // $dif = $gfr[0]->gf_year2 - $gfr[0]->gf_year1;
                // $industry['q1year1'] = $gfr[0]->gf_year1 - 3*$dif/4;
                // $industry['q2year1'] = $gfr[0]->gf_year1 - 2*$dif/4;
                // $industry['q3year1'] = $gfr[0]->gf_year1 - $dif/4;
                // $industry['q4year1'] = $gfr[0]->gf_year1;

                // $industry['q1year2'] = $gfr[0]->gf_year2 - 3*$dif/4;
                // $industry['q2year2'] = $gfr[0]->gf_year2 - 2*$dif/4;
                // $industry['q3year2'] = $gfr[0]->gf_year2 - $dif/4;
                // $industry['q4year2'] = $gfr[0]->gf_year2;

                // $industry['q1year3'] = $gfr[0]->gf_year3 - 3*$dif/4;
                // $industry['q2year3'] = $gfr[0]->gf_year3 - 2*$dif/4;
                // $industry['q3year3'] = $gfr[0]->gf_year3 - $dif/4;
                // $industry['q4year3'] = $gfr[0]->gf_year3;


            } else {

            	//echo 'test'; exit;
                // if no data found in growth forecast results, do external holt winter.
                $indClass = IndustryClass::where("main_id",$entity->industry_row_id)->orWhere("mapping", "like", "%" . $entity->industry_row_id . "%")->get();
                $indData = array();
                $preData = array('data' => array(), 'dates' => array()); 
                if($indClass->count() >= 1) {
                    foreach ($indClass as $key => $value) {
                        $ids[] = $value->id;
                        $indData[] = $value->data;
                    }
                }

                //print_r($indClass); exit;

                $tmpData = array();
                $strList = "";
                $strDate = "";
                $id = null;
                $endYear = 0;
                $pForecast = array("forecast" => array(), "dates" => array());
                if (count($ids) >= 1 && $indClass[0]->data) {
                    // Pass an array of data from industry class to holt winter.
                	//echo 'test1'; exit;
                    foreach ($indData as $key => $value) {
                        $list = array();
                        $str =  explode(',',preg_replace('/["{}]/', '', $value ));
                        foreach ($str as $index => $val) {
                            $tmpStr = explode(':', $val);
                            if(stripos($tmpStr[0], "At Current Prices ")){
                                $yr = 0;
                                if(!stripos($tmpStr[1], "..")){
                                    $str = trim(str_replace("At Current Prices", "", $tmpStr[0]));
                                    $date = substr($str, 0, 4);
                                    if(strpos($str,"Q1")){
                                        $dt = $date . "-". "03";
                                        $n = date("t", strtotime($dt));
                                    } elseif (strpos($str,"Q2")) {
                                        $dt = $date . "-". "06";
                                        $n = date("t", strtotime($date . "-". "06"));
                                    } elseif (strpos($str,"Q3")) {
                                        $dt = $date . "-". "09";
                                        $n = date("t", strtotime($date . "-". "09"));
                                    } elseif (strpos($str,"Q4")) {
                                        $dt = $date . "-". "12";
                                        $n = date("t", strtotime($date . "-". "12"));
                                    }
                                    $dt = $dt . "-" . $n;
                                    if(count( $preData['data']) == 0) {
                                        $tmpData[] = $dt;
                                    }
                                    preg_match_all('!\d+!', $tmpStr[1], $matches);
                                    $list[] = $matches[0][0];
                                    $yr = date("Y", strtotime($dt));
                                    if($yr > $endYear){
                                        $endYear = $yr;
                                    }
                                }
                            }
                        }
                        $preData['data'][]  = $list;
                    }
                    $strDate = json_encode($tmpData);
                }  
                $ave = array();
                // get weighted average
                if(count($preData['data']) > 0) {
                    foreach ($preData['data'] as $key => $pred) {
                        foreach ($pred as $pre => $val) {
                            $w = 0;
                            for($i = 0; $i < count($preData['data']); $i++){
                                if(isset($preData['data'][$i][$pre])){
                                    $w += $preData['data'][$i][$pre];
                                }
                            }
                            $ave[]=$w/4;
                        }
                        break;
                    }
                }

                //print_r($ave); exit;
                $strList = json_encode($ave);
                $year = $fa_report->year;
                if($indClass->count() >= 1 && !empty($indClass[0]->data)) {
                    // use the industry class data field for holt winter
                    $id = $indClass[0]->id;
                    if(env("APP_ENV") == "local"){

                        // exec("C:\\Users\\kenneth.manongdo\\Documents\\python\\python " . public_path("\\test\\holtwinter\\holtwinterModel.py") .  " >> c:\\xampp\\htdocs\\creditbpo\\public\\log.txt 2>&1 " . ($id - 1).  " " . $endYear . " " . $strDate . " " .  $strList  , $pass);
                        exec("python " . public_path("/test/holtwinter/holtwinterModel.py") .  " " . ($id - 1).  " " . $endYear . " " . $strDate . " " .  $strList  , $pass);

                        // echo "python " . public_path("/test/holtwinter/holtwinterModel.py") .  " " . ($id - 1).  " " . $endYear . " " . $strDate . " " .  $strList; exit;

                    } else {
                        exec("python3 " . public_path("/test/holtwinter/holtwinterModel.py") . " " . ($id - 1). " " . $endYear . " " . $strDate . " " .  $strList , $pass);

                        // echo "python3 " . public_path("/test/holtwinter/holtwinterModel.py") . " " . ($id - 1). " " . $endYear . " " . $strDate . " " .  $strList; exit;
                    }
                } else {
                    // use the external CSV file for holt winter
                    if($indClass->count() == 1) {
                        $id = $indClass[0]->id;
                    } 
                    // using Python instead of Rscript for Holt Winter
                    if($id) {
                    	//echo 'test';
                        if(env("APP_ENV") == "local"){
                            exec("python " . public_path("/test/holtwinter/holtwinterModel.py") .  " " . ($id - 1).  " " . $endYear , $pass);
                        } else {
                            exec("python3 " . public_path("/test/holtwinter/holtwinterModel.py") . " " . ($id - 1). " " . $endYear , $pass);
                        }

                        // echo "python3 " . public_path("/test/holtwinter/holtwinterModel.py") . " " . ($id - 1). " " . $endYear; exit;
                    } else {
                    	//echo 'test1';
                        if(env("APP_ENV") == "local"){
                            exec("python " . public_path("/test/holtwinter/holtwinterModel.py"), $pass);
                        } else {
                            exec("python3 " . public_path("/test/holtwinter/holtwinterModel.py")  , $pass);
                        }

                        // echo "python3 " . public_path("/test/holtwinter/holtwinterModel.py"); exit;
                    }
                }

                //print_r($pass); exit;
                $dates = array();
                $hwpredict  = array();
                $tmpDt = array();
                $dtSplit = array_search("Dates", $pass);
                $tmpId = null;

                foreach ($pass as $key => $value) {
                    if($dtSplit && $key > $dtSplit) {
                        $dates[] = date("Y-m-d", strtotime($value));
                        if(!$tmpId) {
                            if(date("Y", strtotime(end($dates))) == $year){
                                $tmpId = array_key_last($dates);
                            }
                        }
                    } else {
                        if($key != 0 && $key != $dtSplit) {
                            $hwpredict[] = $value;
                        }
                    }
                }
                $tmpDt = array_slice($dates, $tmpId, 16);
                $tmpVt = array();
                foreach ($tmpDt as $key => $value) {
                    $y = date('Y', strtotime($value));
                    $m = date('n', strtotime($value));
                    $q = ceil($m / 3);
                    $tmpVt[] = "Q{$q} " . $y;
                }
                $tmpHw = array_slice($hwpredict,$tmpId, 16);
                $pForecast['forecast'] = $tmpHw;
                $pForecast['dates'] = $tmpVt;
                for($x = 0; $x < count($pForecast['forecast']); $x++){
                	//echo 'test3'; exit;
                    if($x == 0) {
                        $industry['industry'][$x] = 0;
                    } else {
                        // get growth using the percentage.
                        $growth = (($pForecast['forecast'][$x] - $pForecast['forecast'][$x-1])/$pForecast['forecast'][$x-1] ) * 100;
                        $industry['industry'][$x] = ($industry['industry'][$x-1] + $growth)/4; 
                    }
                }


                //insert result to table
                if(array_key_exists('industry', $industry)) {
                    $insert = array(
                        'Q1' =>$industry['industry'][0],
                        'Q2' => (!empty($industry['industry'][1])) ? $industry['industry'][1] : 0,
                        'Q3' => (!empty($industry['industry'][2])) ? $industry['industry'][2] : 0,
                        'Q4' => (!empty($industry['industry'][3])) ? $industry['industry'][3] : 0,
                        'Q5' => (!empty($industry['industry'][4])) ? $industry['industry'][4] : 0,
                        'Q6' => (!empty($industry['industry'][5])) ? $industry['industry'][5] : 0,
                        'Q7' => (!empty($industry['industry'][6])) ? $industry['industry'][6] : 0,
                        'Q8' => (!empty($industry['industry'][7])) ? $industry['industry'][7] : 0,
                        'Q9' => (!empty($industry['industry'][8])) ? $industry['industry'][8] : 0,
                        'Q10' => (!empty($industry['industry'][9])) ? $industry['industry'][9] : 0,
                        'Q11' => (!empty($industry['industry'][10])) ? $industry['industry'][10] : 0,
                        'Q12' => (!empty($industry['industry'][11])) ? $industry['industry'][11] : 0,
                        'Q13' => (!empty($industry['industry'][12])) ? $industry['industry'][12] : 0,
                        'Q14' => (!empty($industry['industry'][13])) ? $industry['industry'][13] : 0,
                        'Q15' => (!empty($industry['industry'][14])) ? $industry['industry'][14] : 0,
                        'Q16' => (!empty($industry['industry'][15])) ? $industry['industry'][15] : 0,
                        'main_id' => $entity->industry_row_id,
                        'year' => $fa_report->year,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                    DB::table("growth_forecast_results")->insert($insert);
                }
            }
        //}

        $this->cntForecast = 1;
	        // echo 'industry';
	        // print_r($industry);
	        // exit;
        return $industry;
    }
}