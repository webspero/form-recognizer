<?php

namespace CreditBPO\Libraries;

class StringHelper {
    /**
     * @param string $search
     * @param array $list
     */
    public static function striposInArray($search, array $list = []) {
        foreach ($list as $item) {
            if (stripos($search, $item) !== false) {
                return true;
            }
        }

        return false;
    }
}
