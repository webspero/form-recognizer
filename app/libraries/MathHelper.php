<?php

namespace CreditBPO\Libraries;

use CreditBPO\Exceptions\MathException;

class MathHelper {
    public static function linearRegression(array $x, array $y) {
        if (@count($x) !== @count($y)) {
            throw new MathException('Number of elements in coordinate arrays does not match.');
        }

        $n = @count($x);

        // calculate sums
        $sumX = array_sum($x);
        $sumY = array_sum($y);

        $xSumX = 0;
        $xSumY = 0;

        for ($i = 0; $i < $n; $i++) {
            $xSumY += ($x[$i] * $y[$i]);
            $xSumX += ($x[$i] * $x[$i]);
        }

        if ((($n * $xSumX) - ($sumX * $sumX)) !== 0) {
            $m = (($n * $xSumY) - ($sumX * $sumY)) / (($n * $xSumX) - ($sumX * $sumX));
        } else {
            $m = 0;
        }

        $b = ($sumY - ($m * $sumX)) / $n;

        return ['m' => $m, 'b' => $b];
    }
}