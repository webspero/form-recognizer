<?php

use Illuminate\Http\RedirectResponse;

//======================================================================
//  Class L5: Malicious Attempt Helper
//      Malicious Attempt Library Classes
//======================================================================
class MaliciousAttemptLib
{
    //-----------------------------------------------------
    //  Function L5.1: __construct
    //      Initialization
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing   */
    }

    //-----------------------------------------------------
    //  Function L5.2: check2faAttempts
    //      Checks the account suspension from 2FA Attempts
    //-----------------------------------------------------
    public static function check2faAttempts($login_id)
	{
        /*  Variable Declaration        */
        $resp['message']    = STR_EMPTY;
        $resp['sts']        = STS_NG;
        $date_today         = date('Y-m-d H:i:s');

        /* Instantiate Classes  */
        $login_attempt_db   = new UserLoginAttempt;
        $user_2fa_db        = new User2faAttempt;

        /*  Acquires the user's attempts from the Database  */
        $user_2fa_data  = $user_2fa_db->getUserAttempt($login_id);

        $user_2fa_exist = @count($user_2fa_data);

        if (1 <= $user_2fa_exist) {
            /*  More than 3 failed attempts at 2FA Challenge    */
            if ((3 <= $user_2fa_data->attempts) && (STS_OK != $user_2fa_data->status)) {
                /*  Get the Last Attempt Date       */
                $last_attmpt_exp    = explode(' ', $user_2fa_data->last_attempt);

                /*  Date of the Last attempt    */
                $attmpt_date        = explode('-', $last_attmpt_exp[0]);

                /*  Time of the Last attempt    */
                $attmpt_time        = explode(':', $last_attmpt_exp[1]);

                /*  Add 30 minutes on the Time of Last Attempt  */
                $attmpt_30  = date('Y-m-d H:i:s', mktime($attmpt_time[0], $attmpt_time[1]+30, $attmpt_time[2], $attmpt_date[1], $attmpt_date[2], $attmpt_date[0]));

                /*  Add 2 hours on the Time of Last Attempt     */
                $attmpt_2h  = date('Y-m-d H:i:s', mktime($attmpt_time[0]+2, $attmpt_time[1], $attmpt_time[2], $attmpt_date[1], $attmpt_date[2], $attmpt_date[0]));

                /*  Account is Suspended until Administrator intervention   */
                if (8 <= $user_2fa_data->attempts) {
                    $resp['message']    = "Your account has been locked due to suspected malicious attempts, to unlock, you may email CreditBPO at info@creditbpo.com. <a href = '../logout'> Logout here</a>";
                    $resp['sts']        = STS_LOCK;
                }
                /*  7 attempts suspends account for 2 hours                 */
                else if ((7 <= $user_2fa_data->attempts) && (strtotime($attmpt_2h) >= strtotime($date_today))) {
                    $resp['message']    = "Your account has been locked due to suspected malicious attempts, You may try again at ".date('H:i', strtotime($attmpt_2h))." or email CreditBPO at info@creditbpo.com. <a href = '../logout'> Logout here</a>";
                    $resp['sts']        = STS_LOCK;
                }
                /*  3 or more attempts suspends account for 30 minutes      */
                else if ((3 <= $user_2fa_data->attempts) && (strtotime($attmpt_30) >= strtotime($date_today))) {
                    $resp['message']    = "Your account has been locked due to suspected malicious attempts, You may try again at ".date('H:i', strtotime($attmpt_30))." or email CreditBPO at info@creditbpo.com. <a href = '../logout'> Logout here</a>";
                    $resp['sts']        = STS_LOCK;
                }
                else {
                    /** No Processing   */
                }
            }
            /*  Account is clear for access     */
            else {
                $resp['sts']    = STS_OK;
            }
        }

        return $resp;
	}

    //-----------------------------------------------------
    //  Function L5.3: getClientCountry
    //      Fetches the User's Country via IP Address
    //-----------------------------------------------------
    public static function getClientCountry()
    {
        return [
            'country_code' => 'PH',
        ];

        /*  Variable Declaration        */
        $geoloc_api     = GEOLOC_URL_API;           // URL of External Geographical Webservice
        $geoloc_alt     = GEOLOC_ALT_URL_API;       // Alternative Webservice
        $client_ip 		= $_SERVER['REMOTE_ADDR'];  // Customer's IP Address
        $json_geo_data  = NULL;

        /* Test code for local  */
        if (env("APP_ENV") == "Production") {
            $client_ip  = '46.19.37.108';
        }

        /*  Request geographical details from an external Webservice*/
        $geo_data       = NULL;
        //$json_geo_data  = json_decode($geo_data);

        /*  Use Alternative Webservice in case the 1st one fails        */
        if (!isset($json_geo_data->countryCode)) {
            /*  Requests Alternative Webservice for Geographical details*/
            $geo_data       = file_get_contents($geoloc_alt.$client_ip);

            /*  Converts received JSON response to Array                */
            $json_geo_data  = json_decode($geo_data);

            /*  Save the customer country code                          */
            $country_data['country_code'] = $json_geo_data->country_code;
        }
        /*  Successfully fetched geographical data from Webservice      */
        else {
            $country_data['country_code'] = $json_geo_data->countryCode;
        }

        return $country_data;
    }

    //-----------------------------------------------------
    //  Function L5.4: checkEntityEditable
    //      Check if the report can still be edited
    //-----------------------------------------------------
    public static function checkEntityEditable()
    {
        /*  Variable Declaration        */
        $entity_status  = Session::get('entity')->status;
        $sts            = STS_NG;

        /*  Report status is still editable */
        if (5 > $entity_status) {
            $sts    = STS_OK;
        }

        return $sts;
    }

    //-----------------------------------------------------
    //  Function L5.5: checkUpdatePermission
    //      Check if the report belongs to the User
    //-----------------------------------------------------
    public static function checkUpdatePermission($entity_id)
    {
        $sts            = STS_NG;

        /*  Get the Report information  */
        $entity_data    = Entity::find($entity_id);

        if ($entity_data) {
            if (isset(Auth::user()->loginid)) {
                /*  The report belongs to the User  */
                if ((Auth::user()->loginid == $entity_data->loginid) || (Auth::user()->role == 0)
                && (5 >= $entity_data->status)) {
                    $sts    = STS_OK;
                }
            }
            else {
                $sts    = STS_OK;
            }
        }

        return $sts;
    }
}
