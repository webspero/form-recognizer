<?php

use CreditBPO\Libraries\DropboxApi;

/**
 * Class L2: Dropbox Helper
 * Dropbox API Class for saving file to dropbox
 */
class DropboxHelper {
    /**
     * Function L2.1: sendToDropbox
     * Sends the file to dropbox
     * @param string $prefix
     * @param string $filepath
     * @param string $filename
     * @return bool
     */
    public static function sendToDropbox($prefix, $filepath, $filename) {
        $path = $filepath . '/' . $filename;
        $dropBoxPath = '/' . $prefix . '/' . $filename;
        $api = new DropboxApi();
        return $api->upload($dropBoxPath, $path);
    }
}
