<?php

namespace CreditBPO\Libraries;

use Illuminate\Support\Facades\Config;
use GuzzleHttp\Psr7\Response;
use CreditBPO\Libraries\Scraper;

class HighCharts {
    const COMMAND = 'highcharts-export-server --styledMode 1 --options \'%s\' --type %s --outfile %s';

    private $config;
    private $directory;

    public function __construct() {
        $this->config = Config::get('libraries.highcharts');
        $this->directory = public_path().$this->config['file']['directory'];
    }

    /**
     * @param int $fileId
     * @param string $chartType
     * @param array $chartOptions
     * @return string
     */
    public function generateChart($fieldId, $chartType, $chartOptions = []) {
        if (!$this->shellCommandExists('highcharts-export-server')) {
            return $this->generateUsingServer($fieldId, $chartType, $chartOptions);
        }

        return $this->generateUsingCommandLine($fieldId, $chartType, $chartOptions);
    }

    /**
     * @param int $fieldId
     * @param string $chartType
     */
    protected function getFilename($fieldId, $chartType) {
        return $chartType.'-'.$fieldId.'-'.time().'.'.$this->config['file']['extension'];
    }

    protected function generateUsingCommandLine($fieldId, $chartType, $chartOptions = []) {
        $filename = $this->getFilename($fieldId, $chartType);
        $command = sprintf(
            self::COMMAND,
            json_encode($chartOptions, JSON_HEX_APOS),
            $this->config['file']['extension'],
            $this->directory.$filename
        );
        exec($command);
        return $filename;
    }

    /**
     * @param int $fieldId
     * @param string $chartType
     * @param array $chartOptions
     * @return string
     */
    protected function generateUsingServer($fieldId, $chartType, $chartOptions = []) {
        $this->scraper = new Scraper();
        $filename = $this->getFilename($fieldId, $chartType);
        $result = $this->scraper->fetch(
            $this->config['server']['url'],
            [
                'form_params' => [
                    'type' => $this->config['file']['type'],
                    'options' => json_encode($chartOptions),
                ],
                'sink' => $this->directory.$filename,
            ],
            'POST'
        );
        $arr = (array) $result;
        if(!$arr) {
            \Log::info("Warning HighChart Export Error! Will sleep for 20 seconds.");
            sleep(20);
        } 
        return $filename;
    }

    /**
     * @param string $command
     * @return bool
     */
    protected function shellCommandExists($command) {
        exec($command, $result, $code);
        return (is_array($result) && @count($result) > 0 && $code == 0);
    }
}