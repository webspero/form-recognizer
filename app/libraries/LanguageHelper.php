<?php

//======================================================================
//  Class L4: LanguageHelper
//      LanguageHelper Class for getting translations to clientside scripts
//======================================================================
class LanguageHelper
{
	//-----------------------------------------------------
    //  Function L4.1: getAll
    //      Gets all the translations in language files and convert to json
    //-----------------------------------------------------
	public static function getAll()
	{
		$language_array = array(
			'messages'                  => Lang::get('messages'),
			//'business_details1'         => Lang::get('business_details1'),
			'business_details2'         => Lang::get('business_details2'),
			'ecf'                       => Lang::get('ecf'),
			//'condition_sustainability'  => Lang::get('condition_sustainability'), 
			'rcl'                       => Lang::get('rcl'),
			//'owner_details'             => Lang::get('owner_details'), 
			'risk_rating'               => Lang::get('risk_rating'),
		);
		return json_encode($language_array);
	}
	
	//-----------------------------------------------------
    //  Function L4.2: convert
    //      helper function to translate a string by library
    //-----------------------------------------------------
	public static function convert($library, $string)
	{
		if(Lang::has($library.'.'.$string)){
			return Lang::get($library.'.'.$string);
		} else {
			return $string;
		}
	}
}