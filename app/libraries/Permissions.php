<?php
//======================================================================
//  Class L7: Permissions
//      Contains helper functions for checking supervisor subuser permissions
//======================================================================
class Permissions
{
	//-----------------------------------------------------
    //  Function L7.1: check
    //      Check if current subuser has permission or not
    //-----------------------------------------------------
	public static function check($type, $abort = true)
	{
		if(Auth::user()->parent_user_id == 0){
			return true;
		}
		$permission = explode(',', Auth::user()->permissions);
		if(in_array((string)$type, $permission)){
			return true;
		} else {
			if($abort){
				App::abort(403, 'Unauthorized access.');
			} else {
				return false;
			}
		}
	}

	//-----------------------------------------------------
    //  Function L7.2: get_bank_subscription
    //      Gets the bank/supervisor subscription expiry date
    //-----------------------------------------------------
	public static function get_bank_subscription()
	{
		if(Auth::user() && isset(Auth::user()->loginid)){
			$bank_id = Auth::user()->loginid;
			if(Auth::user()->parent_user_id != 0){
				$bank_id = Auth::user()->parent_user_id;
			}
			$subscription = DB::table('tblbanksubscriptions')->where('bank_id', $bank_id)->orderBy('id', 'desc')->first();
			return ($subscription != null) ? $subscription->expiry_date : null;
		} else {
			Auth::logout();
		}
	}

	//-----------------------------------------------------
    //  Function L7.3: get_available_smes_for_bank_user
    //      Gets all available SMES for bank/supervisor
    //-----------------------------------------------------
	public static function get_available_smes_for_bank_user()
	{
		$entity = DB::table('tblentity')->select('entityid');

		if(Auth::user()->can_view_free_sme==1){
			$entity->where(function($query){
				$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
			});
		} else {
			$entity->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
		}
		$entity->groupBy('entityid');
		$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
		//$entity->take(BANK_SME_LIMIT); //commented out, will check the purpose of this line.

		return $entity->pluck('entityid');	
	}
}
