<?php

namespace CreditBPO\Libraries;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Config;

class DropboxApi {
    const AUTH_HEADER = 'Bearer %s';
    const MODE_ADD = 'add';
    const MODE_OVERWRITE = 'overwrite';

    private $client;
    private $headers = [];

    public function __construct() {
        $this->client = new Client([
            'defaults' => [
                'headers' => $this->getHeaders(),
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getConfig() {
        return Config::get('libraries/dropbox');
    }

    /**
     * @return array
     */
    protected function getAuthorizationHeader() {
        $config = $this->getConfig();
        return sprintf(self::AUTH_HEADER, $config['auth_token']);
    }

    /**
     * @param array $additionalHeaders
     * @return array
     */
    protected function getHeaders($additionalHeaders = []) {
        $headers = [
            'Authorization' => $this->getAuthorizationHeader(),
        ];

        if (is_array($additionalHeaders)) {
            return array_merge($headers, $additionalHeaders);
        }

        return $headers;
    }

    /**
     * @return array
     */
    protected function getResponse($response) {
        $result = $response->getBody()->getContents();
        return json_decode($result, true);
    }

    /**
     * @param string $path
     * @param string $filepath
     * @param string $mode
     * @param bool $autorename
     * @return array
     */
    public function upload($path, $filepath, $mode = self::MODE_OVERWRITE, $autorename = false) {
        try {
            if (!file_exists($filepath)) {
                return false;
            }

            if (substr($path, 0, 1) !== '/') {
                $path = '/' . $path;
            }

            $config = $this->getConfig();
            $uploaduri = $config['upload_uri'];
            $data = json_encode([
                'path' => $path,
                'mode' => $mode,
                'autorename' => $autorename,
                'mute' => false,
                'strict_conflict' => false,
            ]);
            $headers = $this->getHeaders([
                'Content-Type' => 'application/octet-stream',
                'Dropbox-API-Arg' => $data,
            ]);
            $request = $this->client->request(
                'POST',
                $uploaduri . '/files/upload',
                [
                    'headers' => $headers,
                    'body' => fopen($filepath, 'r'),
                ]);

            return true;
        } catch (ClientException $e) {
            Log::error($e->getResponse()->getBody()->getContents());
        } catch (RequestException $e) {
            Log::error($e);
        } catch (Exception $e) {
            Log::error($e);
        }

        return false;
    }
}
