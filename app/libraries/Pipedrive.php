<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Pipedrive {
    const STATUS_OK = 200;
    private $client;

    public function __construct() {
        $this->client = new Client([
            'base_uri' => PIPEDRIVE_API_URL,
            'defaults' => [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ],
        ]);
    }

    public function addPerson($name, $email, $phone) {
        $person = $this->searchPerson($email);

        if ($person) {
            return $this->__updatePerson(
                $person['id'],
                $name,
                $email,
                $phone
            );
        }

        return $this->__addPerson($name, $email, $phone);
    }

    private function __addPerson($name, $email, $phone) {
        try {
            $response = $this->client->request('POST', 'persons', [
                'query' => [
                    'api_token' => PIPEDRIVE_API_TOKEN,
                ],
                'form_params' => [
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    PIPEDRIVE_PERSON_SOURCE_FIELD => PIPEDRIVE_PERSON_SOURCE,
                ],
            ]);
            if ($response->getStatusCode() === self::STATUS_OK) {
                $contents = json_decode($response->getBody()->getContents(), true);
                return $contents['success'];
            }
        } catch (RequestException $e) {
        }

        return false;
    }

    private function __updatePerson($personId, $name, $email, $phone) {
        try {
            $response = $this->client->request('PUT', 'persons/'.$personId, [
                'query' => [
                    'api_token' => PIPEDRIVE_API_TOKEN,
                ],
                'form_params' => [
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    PIPEDRIVE_PERSON_SOURCE_FIELD => PIPEDRIVE_PERSON_SOURCE,
                ],
            ]);

            if ($response->getStatusCode() === self::STATUS_OK) {
                $contents = json_decode($response->getBody()->getContents(), true);
                return $contents['success'];
            }
        } catch (RequestException $e) {}
    }

    public function searchPerson($email) {
        try {
            $response = $this->client->request('GET', 'persons/find', [
                'query' => [
                    'api_token' => PIPEDRIVE_API_TOKEN,
                    'term' => urlencode($email),
                ],
            ]);

            if ($response->getStatusCode() === self::STATUS_OK) {
                $contents = json_decode($response->getBody()->getContents(), true);

                if ($contents['success'] !== true) {
                    return null;
                }

                if (!isset($contents['data']) || @count($contents['data']) <= 0) {
                    return null;
                }

                return $contents['data'][0];
            }
        } catch (RequestException $e) {}

        return null;
    }
}
