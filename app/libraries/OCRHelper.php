<?php

//======================================================================
//  Class L6: OCRHelper
//      OCRHelper Class for OCR using ABBY Cloud OCR
//======================================================================
class OCRHelper
{
	//-----------------------------------------------------
    //  Function L6.1: getSecCertDetails
    //      Send a file to ABBY Cloud OCR via API and parses the result
    //-----------------------------------------------------
	public static function getSecCertDetails($fileName)
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$parsed = [];
		$response = '';
		$searchflag = true;
		
		$filePath = getcwd().'/incorporation-docs/'.$fileName;
		if(!file_exists($filePath))
		{
			Log::error('OCR Error: File not found.');
			return $parsed;
		}
		if(!is_readable($filePath) )
		{
			Log::error('OCR Error: File not readable.');
			return $parsed;
		}
		$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

		// Send HTTP POST request and ret xml response
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
		curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
	
		$post_array = array();
		if((version_compare(PHP_VERSION, '5.5') >= 0)) {
			$post_array["my_file"] = new CURLFile($filePath);
		} else {
			$post_array["my_file"] = "@".$filePath;
		}
		
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array); 
		$response = curl_exec($curlHandle);
	
		if($response == FALSE) {
			$errorText = curl_error($curlHandle);
			curl_close($curlHandle);
			Log::error('OCR Error: '.$errorText);
			return $parsed;
		}
		
		$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
		curl_close($curlHandle);
		
		// Parse xml response
		$xml = simplexml_load_string($response);
		if($httpCode != 200) {
			if(property_exists($xml, "message")) {
				Log::error('OCR Error: '.$xml->message);
				return $parsed;
			}
			Log::error('OCR Error: '."unexpected response ".$response);
			return $parsed;
		}
		
		$arr = $xml->task[0]->attributes();
		$taskStatus = $arr["status"];
		if($taskStatus != "Queued") {
			if (env("APP_ENV") == "Production") {
				try{
					Mail::send('emails.ocr_fail', array(
						'error_message' => "Unexpected task status ".$taskStatus
					), function($message){
						$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
						->subject('Error in OCR SDK!');
					});
				}catch(Exception $e){
                //
                }

			}
			Log::error('OCR Error: '."Unexpected task status ".$taskStatus);
			return $parsed;
		}

		$taskid = $arr["id"];  
		$url = 'http://cloud.ocrsdk.com/getTaskStatus';
		$qry_str = "?taskid=$taskid";
		while(true)
		{
			sleep(5);
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
			$response = curl_exec($curlHandle);
			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// parse xml
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
					Log::error('OCR Error: '.$xml->message);
					return $parsed;
				}
				Log::error('OCR Error: '."unexpected response ".$response);
				return $parsed;
			}
			
			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus == "Queued" || $taskStatus == "InProgress") {
			  // continue waiting
			  continue;
			}
			if($taskStatus == "Completed") {
			  // exit this loop and proceed to handling the result
			  break;
			}
			
			// error handling
			if($taskStatus == "ProcessingFailed") {
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Task processing failed: ".$arr["error"]
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
	                //
	                }
				}
				Log::error('OCR Error: '."Task processing failed: ".$arr["error"]);
				return $parsed;
			}
			if (env("APP_ENV") == "Production") {
				try{
					Mail::send('emails.ocr_fail', array(
						'error_message' => "Unexpected task status ".$taskStatus
					), function($message){
						$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
						->subject('Error in OCR SDK!');
					});
				}catch(Exception $e){
                //
                }
			}
			Log::error('OCR Error: '."Unexpected task status ".$taskStatus);
			return $parsed;
		}
	
		$url = $arr["resultUrl"];   
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curlHandle);
		curl_close($curlHandle);
		
		$response_data = explode("\n", $response);
		$dflag = false;
		$shareholders = [];
		
		
		for($x=0; $x<@count($response_data); $x++){
			//Company Reg. No.
			if(strpos(strtolower($response_data[$x]), 'company') !== false &&
				strpos(strtolower($response_data[$x]), 'reg') !== false &&
				strpos(strtolower($response_data[$x]), 'no') !== false
			){
				$parsed['company_reg_no'] = preg_replace("/[^0-9]/","",$response_data[$x]);
			}
			//company_name
			if(strpos(strtolower($response_data[$x]), 'this is to certify that the amended articles of incorporation') !== false){
				if(trim($response_data[$x+1])!=""){
					$parsed['company_name'] = trim($response_data[$x+1]);
				} else {
					$parsed['company_name'] = trim($response_data[$x+2]);
				}
			}
			//share holders & directors
			if((strpos(strtolower($response_data[$x]), 'names') !== false || 
			strpos(strtolower($response_data[$x]), 'name') !== false) &&
			((strpos(strtolower($response_data[$x]), 'nationality') !== false ||
			strpos(strtolower($response_data[$x]), 'citizenship') !== false) ||
			(strpos(strtolower($response_data[$x]), 'residence') !== false ||
			strpos(strtolower($response_data[$x]), 'address') !== false)
			) && @count(explode("  ", trim($response_data[$x]))) > 1 && $searchflag){
				$searchflag = false;
				$x++;
				while(trim($response_data[$x])=="") $x++;
				
				while(trim($response_data[$x])!=""){
					$pieces = explode("  ", trim($response_data[$x]));
					if(@count($pieces)>1){
						$nflag = true;
						for($y=0;$y<@count($pieces);$y++){
							if(trim($pieces[$y])!="" && $nflag) {
								if(@count(explode(" ", trim($pieces[$y]))) > 1 &&
									preg_match('/^[A-Z]/', trim($pieces[$y]))){
									if(!in_array(trim($pieces[$y]), $shareholders)){
										$shareholders[] = $pieces[$y];
									}
								}									
								$nflag = false;
							}
						}
					}
					$x++;
				}
			}
			
			if(strpos(strtolower($response_data[$x]), 'directors') !== false){
				$dflag = true;
			}
			
		}
		
		$parsed['shareholders'] = $shareholders;
		if($dflag) {
			$parsed['directors'] = $shareholders;
		}
		
		return $parsed;
	}
		// delete a directory and all files recursively
		public static function deleteDir($dirPath) {
			if (! is_dir($dirPath)) {
				throw new InvalidArgumentException("$dirPath must be a directory");
			}
			if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
				$dirPath .= '/';
			}
			$files = glob($dirPath . '*', GLOB_MARK);
			foreach ($files as $file) {
				if (is_dir($file)) {
					self::deleteDir($file);
				} else {
					unlink($file);
				}
			}
			rmdir($dirPath);
		}
        
        // paths are relative to /public/ folder
        public static function extractZip($filepath, $tempFolder) {
			$tmpFolderName = time();
			$extractDir = $tempFolder.'/'.$tmpFolderName;
			
            try {			
                $zip = new ZipArchive;
                if ($zip->open($filepath) === TRUE) {
					mkdir($extractDir);
					
                    $zip->extractTo($extractDir);
                    $zip->close();
					
					return $extractDir;
                } else {
                    return false;
                }
            } catch (Exception $e) {
                    return false;
            }
			
            return false;
        }
        
        // paths are relative to /public/ folder
        public static function extractRar($filepath, $tempFolder) {
			if (!extension_loaded('rar')) {
				return false;
			}
			
			$tmpFolderName = time();
			$extractDir = $tempFolder.'/'.$tmpFolderName;
			
            try {			
                $archive = RarArchive::open($filepath);
                $entries = $archive->getEntries();
				
				mkdir($extractDir);
				
                foreach ($entries as $entry) {
                    $entry->extract($extractDir);
                }
                $archive->close();
				
				return $extractDir;
				
            } catch (Exception $e) {
				return false;
            }
			
            return false;
        }
        
        // helper function to extract jpgs from PDF statements
        public static function pdfToJpg($filepath, $tempFolder) {
			if (!extension_loaded('imagick')) {
				return false;
			}
			
			$tmpFolderName = time();
			$extractDir = $tempFolder.'/'.$tmpFolderName;
			
            try {
				$im = new imagick();
				// increase this if the quality is not good enough (but beware that increasing this also increases the script total processing time)
				$im->setResolution(400, 400);
				$im->readImage($filepath);
				$im->setImageCompression(imagick::COMPRESSION_JPEG); 
				$im->setImageCompressionQuality(100);
				
				mkdir($extractDir);
				
				$num_pages = $im->getNumberImages();
				for($i = 0;$i < $num_pages; $i++) {
					$im->setIteratorIndex($i);
					$im->setImageFormat('jpeg');
					$im->writeImage($extractDir.'/page-'.$i.'.jpg');
				}
				$im->clear();
				$im->destroy();
			
				return $extractDir;
				
            } catch (Exception $e) {
				return false;
            }
			
            return false;
        }

        
		// Scans an image file and returns the scanned text
		public static function scanBankStatement($file){
			try{
				$cmd = 'tesseract "'.$file.'" stdout';
				$textArr = [];
				exec($cmd, $textArr);
				return $textArr;
			}catch(\Exception $e){
				return array();
			}
		}
        //-----------------------------------------------------
        //  Function L6.1: scanBankStatement
        //      scans a file to tesseract OCR, parses the result and returns an average daily balance
        //-----------------------------------------------------
        public static function getAverageDailyBalance($text){
            $lines = [];
            $dailyBalances = [];
            
            $balancesAreFloats = false;
            foreach($text as $rowNumber => $textLine){
                // split row on " " character
                $parts = explode(" ",$textLine);
                
                // remove single symbol parts
                foreach($parts as $key => $part){
                    if(strlen(trim($part)) < 2){
                        unset($parts[$key]);
                    }
                }
                $parts = array_values($parts);
                
                if(@count($parts) == 0)
                    continue;
                
                // check if first part is date
                $dateCandidate1 = $dateCandidate2 = $dateCandidate3 = $dateCandidate4 = $dateCandidate5 = $parts[0];
                
                // check if first two parts are date - for dates without year with empty character at the middle
                if(@count($parts) > 1)
                    $dateCandidate2 = $parts[0]." ".$parts[1];
                
                // check if first two parts are date - for dates with year with empty character at the middle
                if(@count($parts) > 2)
                    $dateCandidate3 = $parts[0]." ".$parts[1]." ".$parts[2];
                // check if first there parts are date
                if(@count($parts) > 2)
                    $dateCandidate4 = $parts[0].$parts[1].$parts[2];  
                 // check if first there parts are date
                if(@count($parts) > 1)
                    $dateCandidate5 = $parts[0].$parts[1];  
                
				
				// check if any of the dateCandidates are vaild dates and record the valid date in $validDate
                $validDate = "";
                if(OCRHelper::isValidDate($dateCandidate1)){
                    $validDate = $dateCandidate1;
                }
                if($validDate == "" && OCRHelper::isValidDate($dateCandidate2)){
                    $validDate = $dateCandidate2;
                }
                if($validDate == "" && OCRHelper::isValidDate($dateCandidate3)){
                    $validDate = $dateCandidate3;
                }
                if($validDate == "" && OCRHelper::isValidDate($dateCandidate4)){
                    $validDate = $dateCandidate4;
                }                      
                if($validDate == "" && OCRHelper::isValidDate($dateCandidate5)){
                    $validDate = $dateCandidate5;
                }   

				
                if($validDate){
					// assume the last number on the row is the balance number
                    $formattedBalance = $parts[@count($parts)-1];

                    // if there is a decimal point before that last two digits on any balance number - that means the balances are floats with decimal part
                    if(strpos($formattedBalance,".") == (strlen($formattedBalance)-3)){
							// this check is done to compensate if the scanning process misses the decimal delimiters characters in some of the numbers
                            $balancesAreFloats = true;
                    }
					
					// add the number to the daily balances for this date
                    $dailyBalances[$validDate][] = $formattedBalance;
					
					// fill debug variable
                    $lines[$rowNumber] = implode(" ", $parts);
                }
            }
            
            // sanitize balances
            foreach($dailyBalances as $validDate => $dayBalances){
                    if(is_array($dayBalances)){
                        foreach($dayBalances as $key => $balance){
							
                            // insert dot, if the scanner missed it on some number, and the number should be all floats
                            if($balancesAreFloats == true && strpos($balance,".") != (strlen($balance)-3) ){
                                $balance = substr_replace($balance, ".", (strlen($balance)-2), 0);
                            }
							
							// filter any other signs (like currency symbols)
                            $balance = filter_var($balance,FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
							
							// replace it in the array
                            $dailyBalances[$validDate][$key] = $balance;
                        }
                    }
            }
            
			// calculate the average daily balance per day
            $averageDailyBalances = [];
            foreach($dailyBalances as $date => $dayBalances){
                $averageBalanceForDay = 0;
                if(is_array($dayBalances)){
                    foreach($dayBalances as $balance){
                        $averageBalanceForDay += $balance;
                    }
                }
                $averageBalanceForDay /= floatval(@count($dayBalances));
                $averageDailyBalances[$date] = $averageBalanceForDay;
            }
            
            
			// calculate the average daily balance for the entire period
            $averageBalanceForEntirePeriod = 0;
            if(@count($averageDailyBalances) > 0){
                foreach($averageDailyBalances as $date => $averageDailyBalance){
                    $averageBalanceForEntirePeriod += $averageDailyBalance;
                }   
                $averageBalanceForEntirePeriod /= floatval(@count($averageDailyBalances));
            }
            
			// Debug variables - enable to debug
           /* echo "<h1>SCANNED DOCUMENT</h1>";     
            echo "<pre>".print_r($text,true)."</pre>";          
            echo "<h1>ROWS WITH BALANCES</h1>";     
            echo "<pre>".print_r($lines,true)."</pre>";
            echo "<h1>BALANCES</h1>";     
            echo "<pre>".print_r($dailyBalances,true)."</pre>";
            echo "<h1>AVERAGE DAILY BALANCES</h1>";
            echo "<pre>".print_r($averageDailyBalances,true)."</pre>";
            echo "<h1>FOR THE ENTIRE PERIOD - $averageBalanceForEntirePeriod</h1>";*/

            
            return $averageBalanceForEntirePeriod;
        }
		
		// helper function for checking a date against an array with acceptable date formats - returns true/false
        private static function isValidDate( $date ) {
            $date = trim($date);
            // acceptable formats for a date      
            $validFormats = 
                    [
                         "Y-m-d","Y-d-m","m-d-Y","d-m-Y",
                        "y-m-d","y-d-m","m-m-y","d-m-y",
                         "Y.m.d","Y.d.m","m.d.Y","d.m.Y",
                        "y.m.d","y.d.m","m.d.y","d.m.y",    
                          "Y/m/d","Y/d/m","m/d/Y","d/m/Y",
                        "y/m/d","y/d/m","m/d/y","d/m/y",     
                        "m-d","d-m",
                        "m.d","d.m",
                        "m/d","d/m",
                        "Y-m-j","Y-j-m","m-j-Y","j-m-Y",
                        "y-m-j","y-j-m","m-m-y","j-m-y",
                         "Y.m.j","Y.j.m","m.j.Y","j.m.Y",
                        "y.m.j","y.j.m","m.j.y","j.m.y",    
                          "Y/m/j","Y/j/m","m/j/Y","j/m/Y",
                        "y/m/j","y/j/m","m/j/y","j/m/y",     
                        "m-j","j-m",
                        "m.j","j.m",
                        "m/j","j/m",
                        'd M y','d M Y',
                        'Y M d','y M d',
                        'j M y','j M Y',
                        'Y M j','y M j',
            ];
            
            $valid = false;

            // date misses a year - add one random year
            if(strlen($date) < 6){

                if(strpos($date,"/") != false){
                    $date .= "/2000";
                }
                else if(strpos($date,".") != false){
                    $date .= ".2000";
                }
                else if(strpos($date,"-") != false){
                    $date .= "-2000";
                }
            }

            foreach($validFormats as $format){
                // experiment to parse the date with all possible delimiters - "/",".","-" and compare if the parsed date and the original date string are equal
                if(
                        date($format, strtotime($date)) == $date 
                        || date($format, strtotime(str_replace("/",".",$date))) == $date
                         || date($format, strtotime(str_replace(".","/",$date))) == $date
                        || date($format, strtotime(str_replace("-","/",$date))) == $date
                    ){
                        $valid = true;
                        break;
                }
            }

            if($valid)
                return true;
            else
                return false;
            
        }
}