<?php

//======================================================================
//  Class L8: PSE Calculation Helper
//      PSE Calculations Library Classes
//======================================================================
class PseCalculationLib
{
    //-----------------------------------------------------
    //  Function L8.1: __construct
    //      Initialization
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing   */
    }
    
    //-----------------------------------------------------
    //  Function L8.2: calculatePercentChange
    //      Calculates the Percent Change
    //-----------------------------------------------------
    public static function calculatePercentChange($current, $previous)
	{
        /*  Variable Declaration                */
        $change             = 0;
        $percent_change     = 0;
        
        /*  Current value over previous value   */
        $change  = $current - $previous;
        
        /*  Calculate the percent change        */
        if (0 < $previous) {
            $percent_change = 100 * ($change / $previous);
        }
        else {
            $percent_change = 0;
        }
        
        return $percent_change;
        
	}
    
}