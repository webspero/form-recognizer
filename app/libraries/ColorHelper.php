<?php

//======================================================================
//  Class L1: Color Helper
//      Color Conversion Class
//======================================================================
class ColorHelper
{
    //-----------------------------------------------------
    //  Function L1.1: colourBrightness
    //      Convert and Sets Brightness
    //-----------------------------------------------------
	public static function colourBrightness($hex, $percent)
    {
        /*  Work out if hash given  */
		$hash   = '';
		
        /*  Color is a Hexadecimal string value */
        if (stristr($hex,'#')) {
			$hex    = str_replace('#','',$hex);
			$hash   = '#';
		}
        
        /*  Converts HEX value to RGB   */
		$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
		
        /*  Calculate Brightness    */
		for ($i = 0; $i < 3; $i++) {
            
            /*  Increase Brightness */
			if ($percent > 0) {
				
				$rgb[$i] = $rgb[$i] + round(255 * (1-$percent));
				
                /*  In case rounding up causes us to go to 256  */
				if ($rgb[$i] > 255) {
					$rgb[$i] = 255;
				}
			}
            /*  Decrease Brightness */
            else {
				$positivePercent    = $percent * (-1);
				$rgb[$i]            = $rgb[$i] - round(255 * (1-$positivePercent));
                
                /*  In case rounding up causes to go negative    */
				if ($rgb[$i] < 0) {
					$rgb[$i] = 0;
				}
			}
		}
        
        /*  Converts RGB value to HEX   */
		$hex    = '';
        
		for ($i = 0; $i < 3; $i++) {
            /*  Convert the decimal digit to hex    */
			$hexDigit = dechex($rgb[$i]);
            
            /*  Add a leading zero if necessary     */
			if (strlen($hexDigit) == 1) {
                $hexDigit = "0" . $hexDigit;
			}
            
            /*  Append to the hex string            */
			$hex .= $hexDigit;
		}
		return $hash.$hex;
	}
}