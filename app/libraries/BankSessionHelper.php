<?php

class BankSessionHelper
{
    public static function createBankSession($supervisor) {
        Session::put('supervisor', $supervisor->toArray());
        return $supervisor->toArray();
	}
}