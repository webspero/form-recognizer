<?php

//======================================================================
//  Class L9: Linear Regression Calculation Helper
//      Linear Regression Calculation Library Classes
//======================================================================
class RegressionCalcLib
{
    const BIZ_OUTLOOK_POS_PIVOT = 0.05;     // Default Positive Pivot point
    const BIZ_OUTLOOK_NEG_PIVOT = -0.05;    // Default Negative Pivot point
    
    //-----------------------------------------------------
    //  Function L9.1: __construct
    //      Initialization
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing   */
    }
    
    //-----------------------------------------------------
    //  Function L9.2: calculateRegression
    //      Calculates the Linear Regression of Business Outlook
    //-----------------------------------------------------
    public static function calculateRegression($xy_list)
	{
        /*  Variable Declaration                */
        $results    = array();
        $regression = 0;
        $intercept  = 0;
        $slope      = 0;
        
        /*  Calculate the Pearson Correlation Coefficient Table */
        $xy_pearson_tb  = self::calculatePearsonCorrelation($xy_list);
        
        /*  Calculate the Y-Intercept                           */
        $intercept      = self::calculateIntercept($xy_pearson_tb);
        
        /*  Calculate the Slope                                 */
        $slope          = self::calculateSlope($xy_pearson_tb);
        
        /*  Regression Equation                                 */
        $regression     = $intercept.' + '.$slope.'x';
        
        /*  Calculate the Slope                                 */
        if ((self::BIZ_OUTLOOK_POS_PIVOT >= $slope) && (self::BIZ_OUTLOOK_NEG_PIVOT <= $slope)) {
            $point_allocation   = BIZ_OUTLOOK_FLAT_POINT;
        }
        else if (self::BIZ_OUTLOOK_POS_PIVOT < $slope) {
            $point_allocation   = BIZ_OUTLOOK_UP_POINT;
        }
        else if (self::BIZ_OUTLOOK_NEG_PIVOT > $slope) {
            $point_allocation   = BIZ_OUTLOOK_DOWN_POINT;
        }
        else {
            /** No Processing   */
        }
        
        $results        = array($intercept, $slope, $regression, $point_allocation);
        
        return $results;
        
	}
    
    //-----------------------------------------------------
    //  Function L9.3: calculateIntercept
    //      Calculates the Y-Intercept
    //-----------------------------------------------------
    public static function calculateIntercept($xy_pearson_tb)
	{
        /*  Variable Declaration                */
        $intercept  = 0;
        
        /*  Determines the number of variables excluding summations */
        $num_var    = @count($xy_pearson_tb) - 1;
        
        /*  (Σy)(Σx2) - (Σx)(Σxy) / n(Σx2) - (Σx)2 Formula          */
        $dividend   = ($xy_pearson_tb[$num_var][PEARSON_SUMM_IND_IDX] * $xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_SQR_IDX]) - ($xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX] * $xy_pearson_tb[$num_var][PEARSON_SUMM_XY_IDX]);
        $divisor    = ($num_var * $xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_SQR_IDX]) - ($xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX] * $xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX]);
        if($divisor != 0) {
            $intercept  = $dividend / $divisor;
        } else {
            $intercept  = 0;
        }
        
        return round($intercept, 2);
        
	}
    
    //-----------------------------------------------------
    //  Function L9.4: calculateSlope
    //      Calculates the Slope
    //-----------------------------------------------------
	public static function calculateSlope($xy_pearson_tb)
	{
        /*  Variable Declaration                */
        $slope      = 0;
        
        /*  Determines the number of variables excluding summations */
        $num_var    = @count($xy_pearson_tb) - 1;
        
        /*  (n(Σxy) - (Σx)(Σy)) / (nΣx2 - (Σx)2) Formula            */
        $dividend   = ($num_var * $xy_pearson_tb[$num_var][PEARSON_SUMM_XY_IDX]) - ($xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX] * $xy_pearson_tb[$num_var][PEARSON_SUMM_IND_IDX]);
        $divisor    = ($num_var * $xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_SQR_IDX]) - ($xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX] * $xy_pearson_tb[$num_var][PEARSON_SUMM_DEP_IDX]);
        
        if($divisor != 0) {
            $slope      = $dividend / $divisor;
        }else {
            $slope = 0;
        }
        
        return round($slope, 2);
	}
    
    //-----------------------------------------------------
    //  Function L9.5: calculatePearsonCorrelation
    //      Calculate the Pearson Correlation Coefficient Table
    //-----------------------------------------------------
    public static function calculatePearsonCorrelation($xy_arr)
    {
        /*  Variable Declaration                */
        $xy_pearson_tb  = array();
        $sum_dep_var    = 0;
        $sum_ind_var    = 0;
        $sum_var_xy     = 0;
        $sum_dep_sqr    = 0;
        $sum_ind_sqr    = 0;
        
        /*  Determines the number of paired variables   */
        $xy_count = @count($xy_arr);
        
        /*  Computes for the Pearson Correlation Table  */
        foreach($xy_arr as $var) {
            $var[PEARSON_XY_VAR_IDX]     = $var[PEARSON_DEP_VAR_IDX] * $var[PEARSON_IND_VAR_IDX];
            $var[PEARSON_DEP_SQR_IDX]     = $var[PEARSON_DEP_VAR_IDX] * $var[PEARSON_DEP_VAR_IDX];
            $var[PEARSON_IND_SQR_IDX]     = $var[PEARSON_IND_VAR_IDX] * $var[PEARSON_IND_VAR_IDX];
            
            /** Summation of Columns    */
            $sum_dep_var    += $var[PEARSON_DEP_VAR_IDX];
            $sum_ind_var    += $var[PEARSON_IND_VAR_IDX];
            $sum_var_xy     += $var[PEARSON_XY_VAR_IDX];
            $sum_dep_sqr    += $var[PEARSON_DEP_SQR_IDX];
            $sum_ind_sqr    += $var[PEARSON_IND_SQR_IDX];
            
            $xy_pearson_tb[]    = $var;
        }
        
        /*  Transfers the Summation to Output Array     */
        $xy_pearson_tb[$xy_count][PEARSON_SUMM_DEP_IDX]         = $sum_dep_var;
        $xy_pearson_tb[$xy_count][PEARSON_SUMM_IND_IDX]         = $sum_ind_var;
        $xy_pearson_tb[$xy_count][PEARSON_SUMM_XY_IDX]          = $sum_var_xy;
        $xy_pearson_tb[$xy_count][PEARSON_SUMM_DEP_SQR_IDX]     = $sum_dep_sqr;
        $xy_pearson_tb[$xy_count][PEARSON_SUMM_IND_SQR_IDX]     = $sum_ind_sqr;
        
        return $xy_pearson_tb;
    }

}