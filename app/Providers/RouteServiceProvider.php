<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
	protected $namespace = null;


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        		//

		// App::before(function($request)
		// {
		// 	//
		// });


		// App::after(function($request, $response)
		// {
		// 	//
		// });

		/*
		|--------------------------------------------------------------------------
		| Authentication Filters
		|--------------------------------------------------------------------------
		|
		| The following filters are used to verify that the user of the current
		| session is logged into this application. The "basic" filter easily
		| integrates HTTP Basic authentication for quick, simple checking.
		|
		*/

		// Route::filter('auth', function()
		// {
		// 	if (Auth::guest())
		// 	{
		// 		if (Request::ajax())
		// 		{
		// 			return Response::make('Unauthorized', 401);
		// 		}
		// 		else
		// 		{
		// 			return Redirect::guest('login');
		// 		}
		// 	}
		// });

		// Route::filter('auth.admin', function()
		// {
		// 	if (Auth::guest())
		// 	{
		// 		if (Request::ajax())
		// 		{
		// 			return Response::make('Unauthorized', 401);
		// 		}
		// 		else
		// 		{
		// 			return Redirect::guest('login');
		// 		}
		// 	}
		// 	elseif (Auth::user() && Auth::user()->role != 0)
		// 	{
		// 		return Response::make('Unauthorized', 401);
		// 	}
		// });

		// Route::filter('auth.investigator', function()
		// {
		// 	if (Auth::guest())
		// 	{
		// 		if (Request::ajax())
		// 		{
		// 			return Response::make('Unauthorized', 401);
		// 		}
		// 		else
		// 		{
		// 			return Redirect::guest('login');
		// 		}
		// 	}
		// 	elseif (Auth::user() && Auth::user()->role != 2)
		// 	{
		// 		return Response::make('Unauthorized', 401);
		// 	}
		// });

		// Route::filter('auth.bank', function()
		// {
		// 	if (Auth::guest())
		// 	{
		// 		if (Request::ajax())
		// 		{
		// 			return Response::make('Unauthorized', 401);
		// 		}
		// 		else
		// 		{
		// 			return Redirect::guest('login');
		// 		}
		// 	}
		// 	elseif (Auth::user() && Auth::user()->role != 4)
		// 	{
		// 		return Response::make('Unauthorized', 401);
		// 	}
		// });

		// Route::filter('paid', function()
		// {
		// 	if (Auth::user() && Auth::user()->role == 1)
		// 	{
		// 		if (Auth::user()->status == 1)
		// 		{
		// 			return Redirect::to('/payment/index');
		// 		}
		// 	}
		// });

		// Route::filter('auth.basic', function()
		// {
		// 	return Auth::basic();
		// });

		// Route::filter('auth.apiclient', 'ApiClientFilter');
		// Route::filter('auth.usertoken', 'UserTokenFilter');

		/*
		|--------------------------------------------------------------------------
		| Guest Filter
		|--------------------------------------------------------------------------
		|
		| The "guest" filter is the counterpart of the authentication filters as
		| it simply checks that the current user is not logged in. A redirect
		| response will be issued if they are, which you may freely change.
		|
		*/

		// Route::filter('guest', function()
		// {
		// 	if (Auth::check()) return Redirect::to('/');
		// });

    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

		$this->mapWebRoutes();

		$this->legacyRoutes();
		$this->apiRoutes();

        //
    }

	protected function legacyRoutes(){
		Route::middleware('web')
			 ->group(base_path('routes/web-legacy.php'));
	}

	protected function apiRoutes(){
		Route::prefix('api/v1')
		     ->namespace('CreditBPO\Controllers\API\v1')
			 ->group(base_path('routes/api-v1.php'));
	}

	protected function adminRoutes(){
		Route::middleware('web')
			->group(base_path('routes/admin.php'));
	}

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace('App\Http\Controllers')
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
			 ->group(base_path('routes/api.php')
			);

	}

}
