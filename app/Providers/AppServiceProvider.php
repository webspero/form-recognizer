<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class AppServiceProvider extends ServiceProvider
{
    private $language;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        App::setLocale($this->language);
        // require app_path().'/constants/constants.php';
        // require app_path().'/constants/cmn_constants.php';
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        /*
        |--------------------------------------------------------------------------
        | Register The Constants File
        |--------------------------------------------------------------------------
        |
        | In the constants.php file are Constants defined when Laravel initializes
        |
        */

        require app_path().'/constants/constants.php';
        require app_path().'/constants/cmn_constants.php';

        /*
        |--------------------------------------------------------------------------
        | Register pChart Library
        |--------------------------------------------------------------------------
        |
        | Loads the pChart library when Laravel initializes
        |
        */

        //require app_path().'/libraries/pChart.class';
        //require app_path().'/libraries/pCache.class';
        //require app_path().'/libraries/pData.class';

        $this->language = Session::get('language', Config::get('app.locale'));

        /*
        |--------------------------------------------------------------------------
        | Register The Laravel Class Loader
        |--------------------------------------------------------------------------
        |
        | In addition to using Composer, you may use the Laravel class loader to
        | load your controllers and models. This is useful for keeping all of
        | your classes in the "global" namespace without Composer updating.
        |
        */
        // \Log::info(base_path());
        // ClassLoader::register(new ClassLoader(array(
        //     app_path().'/Console/Commands',
        //     app_path().'/Http/Controllers',
        //     app_path().'/creditbpo/Controllers',
        //     app_path().'/creditbpo/DTO',
        //     app_path().'/creditbpo/Exceptions',
        //     app_path().'/creditbpo/Handlers',
        //     app_path().'/creditbpo/Libraries',
        //     app_path().'/creditbpo/Models',
        //     app_path().'/creditbpo/Services',
        //     app_path().'/Models',
        //     app_path().'/database/seeds',
        //     app_path().'/libraries',
        //     app_path().'/filters',
        // )));

        /*
        |--------------------------------------------------------------------------
        | Application Error Logger
        |--------------------------------------------------------------------------
        |
        | Here we will configure the error logger setup for the application which
        | is built on top of the wonderful Monolog library. By default we will
        | build a basic log file setup which creates a single file for logs.
        |
        */

        //Log::useFiles(storage_path().'/logs/laravel.log');

        /*
        |--------------------------------------------------------------------------
        | Application Error Handler
        |--------------------------------------------------------------------------
        |
        | Here you may handle any errors that occur in your application, including
        | logging them or displaying custom views for specific errors. You may
        | even register several error handlers to handle different types of
        | exceptions. If nothing is returned, the default error view is
        | shown, which includes a detailed stack trace during debug.
        |
        */

        // App::missing(function($exception)
        // {
        //     Log::error($exception);

        //     if (Request::wantsJson()){
        //         return App::make('CreditBPO\Controllers\API\v1\ApiController')->respondNotFoundException();
        //     }
        // });

        // App::error(function(Exception $exception, $code)
        // {
        //     Log::error($exception);
            
        //     if (Request::wantsJson())
        //     {
        //         if(env("APP_ENV") == "Production"){
        //             return App::make('CreditBPO\Controllers\API\v1\ApiController')->respondInternalError("Please contact CreditBPO at info@creditbpo.com.");
        //         }
        //     }

        //     if(env("APP_ENV") == "Production"){
        //         if($code != 404) {


        //             $user = Auth::user();
        //             $date = new DateTime();
        //             date_default_timezone_set('Asia/Manila');
        //             $date_format = date("Y-m-d h:iA", $date->format('U'));

        //             //send email log
        //             $data = array(
        //                 'server' => (env("APP_ENV") == "Production") ? "Production (business.creditbpo.com)" : "Test (128.199.203.18)",
        //                 'date' => $date_format,
        //                 'exception' => $exception,
        //                 'code' => $code,
        //                 'user' => ($user!=null) ? $user->first_name . ' ' . $user->last_name . ' (' . $user->email . ')' : ''
        //             );

        //             Mail::send('emails.error', $data, function($message)
        //             {
        //                 $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
        //                 ->subject('CreditBPO Business Site Error Notification');
        //             });

        //             Log::info('Error Email sent to notify.user@creditbpo.com');

        //             if($code == 403){
        //                 return Response::view('errors.403', array(), 403);
        //             } else {
        //                 return Response::view('errors.default', array(), $code);
        //             }
        //         }
        //         else {
        //             if (0 == Auth::check()) {
        //                 Session::flash('inactivity', "You have been Redirected here because of inactivity or page you are trying to access does not exist");
        //                 return Redirect::to('/login');
        //             }

        //             return Response::view('errors.default', array(), 404);
        //         }
        //     }

        // });

        /*
        |--------------------------------------------------------------------------
        | Maintenance Mode Handler
        |--------------------------------------------------------------------------
        |
        | The "down" Artisan command gives you the ability to put an application
        | into maintenance mode. Here, you will define what is displayed back
        | to the user if maintenance mode is in effect for the application.
        |
        */

        // App::down(function()
        // {
        //     return Response::make("Be right back!", 503);
        // });

        /*
        |--------------------------------------------------------------------------
        | Require The Filters File
        |--------------------------------------------------------------------------
        |
        | Next we will load the filters file for the application. This gives us
        | a nice separate location to store our route and application filter
        | definitions instead of putting them all in the main routes file.
        |
        */

        //require app_path().'/filters.php';
        
    }//end of register
}
