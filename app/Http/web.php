<?php

// Valid User Statistics
Route::group([
    'prefix' => 'financial-report',
], function() {
    Route::get(
        'download/{financialReportId}',
        [
            'uses' => 'FinancialReportController@getDownloadReport',
        ]
    );
});
