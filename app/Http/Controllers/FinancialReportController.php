<?php

use CreditBPO\Models\BankInterestRates;
use CreditBPO\Models\DebtServiceCapacityCalculator;
use CreditBPO\Handlers\FinancialAnalysis;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
//======================================================================
//  Class 22: FinancialReportController
//  Description: Contains functions for Question Pass
//======================================================================
class FinancialReportController extends BaseController {

	//-----------------------------------------------------
    //  Function 22.1: getFinancialReportList
	//  Description: get list of financial reports
    //-----------------------------------------------------
	public function getFinancialReportList()
	{
		try{
			$fr = FinancialReport::orderBy('id', 'desc')->where('entity_id', 0)->with('keyRatio')->get();
			return View::make('financial.list', array('financial_reports'=>$fr));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.2: getUploadVfa
	//  Description: get page for upload Vfa
    //-----------------------------------------------------
	public function getUploadVfa()
	{
		try{
			return View::make('financial.vfaupload');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.3: postUploadVfa
	//  Description: function to process uploaded vfa to Financial Report stored in Database
    //-----------------------------------------------------
	public function postUploadVfa()
	{
		try{
			$v = Validator::make(Input::all(), array( // validation rules and message
				'file' => 'required'
			), array(
				'file.required' => 'Please select the file.'
			));
			if($v->fails()){ // redirect back if fails
				return Redirect::route('getUploadVfa')->withErrors($v->errors());
			}

			$file = Input::file('file');						// get input file
			$destinationPath = 'temp'; 							// set directory
			$extension = $file->getClientOriginalExtension();	// get file extension name

			if($extension!="vfa"){	// check if file is .vfa
				return Redirect::route('getUploadVfa')->withErrors(array('file'=>'The file you were trying to upload does not have the correct format.'));
			}

			$filename = rand(11111,99999).time().'.'.$extension;			// generate random filename
			$upload_success = $file->move($destinationPath, $filename);		// save file to server

			try // process the vfa file
			{
				$contents = File::get($destinationPath.'/'.$filename);		// file get contents
				$contents = json_decode($contents, true);					// decode json content to php class

				$fs = FinancialReport::create(array(						// crete the Financial report based on data
					'title' => $contents['title'],
					'year' => $contents['YEAR'],
					'industry_id' => $contents['Otrasli'],
					'money_factor' => $contents['money_factor'],
					'currency' => $contents['valedin'],
					'step' => $contents['anstep']
				));

				// create the balance sheets
				$index = 0;
				foreach($contents['bal'] as $bal){
					BalanceSheet::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $index,
						'PropertyPlantAndEquipment' => $bal['PropertyPlantAndEquipment'],
						'InvestmentProperty' => $bal['InvestmentProperty'],
						'Goodwill' => $bal['Goodwill'],
						'IntangibleAssetsOtherThanGoodwill' => $bal['IntangibleAssetsOtherThanGoodwill'],
						'InvestmentAccountedForUsingEquityMethod' => $bal['InvestmentAccountedForUsingEquityMethod'],
						'InvestmentsInSubsidiariesJointVenturesAndAssociates' => $bal['InvestmentsInSubsidiariesJointVenturesAndAssociates'],
						'NoncurrentBiologicalAssets' => $bal['NoncurrentBiologicalAssets'],
						'NoncurrentReceivables' => $bal['NoncurrentReceivables'],
						'NoncurrentInventories' => $bal['NoncurrentInventories'],
						'DeferredTaxAssets' => $bal['DeferredTaxAssets'],
						'CurrentTaxAssetsNoncurrent' => $bal['CurrentTaxAssetsNoncurrent'],
						'OtherNoncurrentFinancialAssets' => $bal['OtherNoncurrentFinancialAssets'],
						'OtherNoncurrentNonfinancialAssets' => $bal['OtherNoncurrentNonfinancialAssets'],
						'NoncurrentNoncashAssetsPledgedAsCollateral' => $bal['NoncurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral'],
						'NoncurrentAssets' => $bal['NoncurrentAssets'],
						'Inventories' => $bal['Inventories'],
						'TradeAndOtherCurrentReceivables' => $bal['TradeAndOtherCurrentReceivables'],
						'CurrentTaxAssetsCurrent' => $bal['CurrentTaxAssetsCurrent'],
						'CurrentBiologicalAssets' => $bal['CurrentBiologicalAssets'],
						'OtherCurrentFinancialAssets' => $bal['OtherCurrentFinancialAssets'],
						'OtherCurrentNonfinancialAssets' => $bal['OtherCurrentNonfinancialAssets'],
						'CashAndCashEquivalents' => $bal['CashAndCashEquivalents'],
						'CurrentNoncashAssetsPledgedAsCollateral' => $bal['CurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral'],
						'NoncurrentAssetsOrDisposalGroups' => $bal['NoncurrentAssetsOrDisposalGroupsClassifiedAsHeldForSaleOrAsHeldForDistributionToOwners'],
						'CurrentAssets' => $bal['CurrentAssets'],
						'Assets' => $bal['Assets'],
						'IssuedCapital' => $bal['IssuedCapital'],
						'SharePremium' => $bal['SharePremium'],
						'TreasuryShares' => $bal['TreasuryShares'],
						'OtherEquityInterest' => $bal['OtherEquityInterest'],
						'OtherReserves' => $bal['OtherReserves'],
						'RetainedEarnings' => $bal['RetainedEarnings'],
						'NoncontrollingInterests' => $bal['NoncontrollingInterests'],
						'Equity' => $bal['Equity'],
						'NoncurrentProvisionsForEmployeeBenefits' => $bal['NoncurrentProvisionsForEmployeeBenefits'],
						'OtherLongtermProvisions' => $bal['OtherLongtermProvisions'],
						'NoncurrentPayables' => $bal['NoncurrentPayables'],
						'DeferredTaxLiabilities' => $bal['DeferredTaxLiabilities'],
						'CurrentTaxLiabilitiesNoncurrent' => $bal['CurrentTaxLiabilitiesNoncurrent'],
						'OtherNoncurrentFinancialLiabilities' => $bal['OtherNoncurrentFinancialLiabilities'],
						'OtherNoncurrentNonfinancialLiabilities' => $bal['OtherNoncurrentNonfinancialLiabilities'],
						'NoncurrentLiabilities' => $bal['NoncurrentLiabilities'],
						'CurrentProvisionsForEmployeeBenefits' => $bal['CurrentProvisionsForEmployeeBenefits'],
						'OtherShorttermProvisions' => $bal['OtherShorttermProvisions'],
						'TradeAndOtherCurrentPayables' => $bal['TradeAndOtherCurrentPayables'],
						'CurrentTaxLiabilitiesCurrent' => $bal['CurrentTaxLiabilitiesCurrent'],
						'OtherCurrentFinancialLiabilities' => $bal['OtherCurrentFinancialLiabilities'],
						'OtherCurrentNonfinancialLiabilities' => $bal['OtherCurrentNonfinancialLiabilities'],
						'LiabilitiesIncludedInDisposalGroups' => $bal['LiabilitiesIncludedInDisposalGroupsClassifiedAsHeldForSale'],
						'CurrentLiabilities' => $bal['CurrentLiabilities'],
						'Liabilities' => $bal['Liabilities'],
						'EquityAndLiabilities' => $bal['EquityAndLiabilities']
					));
					$index++;
				}

				// create the income statements
				$index = 0;
				foreach($contents['prib'] as $prib){
					IncomeStatement::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $index,
						'Revenue' => $prib['Revenue'],
						'CostOfSales' => $prib['CostOfSales'],
						'GrossProfit' => $prib['GrossProfit'],
						'OtherIncome' => $prib['OtherIncome'],
						'DistributionCosts' => $prib['DistributionCosts'],
						'AdministrativeExpense' => $prib['AdministrativeExpense'],
						'OtherExpenseByFunction' => $prib['OtherExpenseByFunction'],
						'OtherGainsLosses' => $prib['OtherGainsLosses'],
						'ProfitLossFromOperatingActivities' => $prib['ProfitLossFromOperatingActivities'],
						'DifferenceBetweenCarryingAmountOfDividendsPayable' => $prib['DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed'],
						'GainsLossesOnNetMonetaryPosition' => $prib['GainsLossesOnNetMonetaryPosition'],
						'GainLossArisingFromDerecognitionOfFinancialAssets' => $prib['GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost'],
						'FinanceIncome' => $prib['FinanceIncome'],
						'FinanceCosts' => $prib['FinanceCosts'],
						'ShareOfProfitLossOfAssociates' => $prib['ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod'],
						'GainsLossesArisingFromDifference' => $prib['GainsLossesArisingFromDifferenceBetweenPreviousCarryingAmountAndFairValueOfFinancialAssetsReclassifiedAsMeasuredAtFairValue'],
						'ProfitLossBeforeTax' => $prib['ProfitLossBeforeTax'],
						'IncomeTaxExpenseContinuingOperations' => $prib['IncomeTaxExpenseContinuingOperations'],
						'ProfitLossFromContinuingOperations' => $prib['ProfitLossFromContinuingOperations'],
						'ProfitLossFromDiscontinuedOperations' => $prib['ProfitLossFromDiscontinuedOperations'],
						'ProfitLoss' => $prib['ProfitLoss'],
						'OtherComprehensiveIncome' => $prib['OtherComprehensiveIncome'],
						'ComprehensiveIncome' => $prib['ComprehensiveIncome']
					));
					$index++;
				}

			} catch (Exception $e) {
	            return Redirect::route('getUploadVfa')->withErrors(array('file'=>$e->getMessage()));
	        }

			return Redirect::to('fr');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.4: getFinancialReport
	//  Description: get selected financial report and display values
    //-----------------------------------------------------
	public function getFinancialReport($id)
	{
		try{
			$fr = FinancialReport::find($id);
			// mapping for RR's industry codes and systems's
			switch($fr->industry_id){
				case "1": $industry_id = 1; break;
				case "5": $industry_id = 2; break;
				case "10": $industry_id = 3; break;
				case "35": $industry_id = 4; break;
				case "36": $industry_id = 5; break;
				case "41": $industry_id = 6; break;
				case "45": $industry_id = 7; break;
				case "49": $industry_id = 8; break;
				case "55": $industry_id = 9; break;
				case "58": $industry_id = 10; break;
				case "64": $industry_id = 11; break;
				case "68": $industry_id = 12; break;
				case "69": $industry_id = 13; break;
				case "77": $industry_id = 14; break;
				case "85": $industry_id = 15; break;
				case "86": $industry_id = 16; break;
				case "90": $industry_id = 17; break;
				case "94": $industry_id = 18; break;
				default: $industry_id = 18;
			}
			$industry = Industrymain::find($industry_id);

			return View::make('financial.view', array(
				'financial_report' => $fr,
				'industry' => $industry->main_title,
				'balance_sheets' => $fr->balanceSheets()->orderBy('index_count', 'asc')->get(),
				'income_statements' => $fr->incomeStatements()->orderBy('index_count', 'asc')->get(),
				'cash_flow' => $fr->cashFlow()->orderBy('index_count', 'asc')->get()
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.5: getFinancialReportRaw
	//  Description: get raw financial report data (BS+IS+CF) as object
    //-----------------------------------------------------
	public function getFinancialReportRaw()
	{
		try{
			$id = Input::get('id');
			$fr = FinancialReport::where(['entity_id' => $id,'is_deleted' => 0])->where('is_deleted', 0)->orderBy('id', 'desc')->first();
	        $debtService = DebtServiceCapacityCalculator::where('entity_id', $id)->first();
	        $bankInterestRatesModel = new BankInterestRates();
	        $bankInterestRates = $bankInterestRatesModel->getCurrentInterestRates();
	        $sme = Smescoring::where('entityid', $id)->first();
	        $sources = [];

	        if ($fr) {
	           $cashFlow = $fr->cashFlow()->orderBy('index_count', 'asc')->get();

	            foreach ($cashFlow as $cash) {
	                if (
	                    !isset($sources['NetOperatingIncome']) &&
	                    $cash['NetOperatingIncome'] != 0
	                ) {
	                    $sources['NetOperatingIncome'] = $cash['NetOperatingIncome'];
	                }

	                if (
	                    !isset($sources['NetCashByOperatingActivities']) &&
	                    $cash['NetCashByOperatingActivities'] != 0
	                ) {
	                    $sources['NetCashByOperatingActivities'] = $cash['NetCashByOperatingActivities'];
	                }
	            }

	        }

	        if (@count($sme) > 0 && $sme['average_daily_balance'] != 0) {
	            $sources['ADB'] = $sme['average_daily_balance'];
	        }

	        if (@count($debtService) > 0 && $debtService->source === 'Custom') {
	        	$sources['Custom'] = $debtService->net_operating_income;
	        } else {
	        	$sources['Custom'] = 0;
	        }

			if ($fr) {
				return array(
					'financial_report' => $fr,
					'balance_sheets' => $fr->balanceSheets()->where('is_deleted', 0)->orderBy('index_count', 'asc')->get(),
	                'debt_service' => $debtService,
					'income_statements' => $fr->incomeStatements()->where('is_deleted', 0)->orderBy('index_count', 'asc')->get(),
					'cash_flow' => $fr->cashFlow()->where('is_deleted', 0)->orderBy('index_count', 'asc')->get(),
	                'bank_interest_rates' => $bankInterestRates,
	                'sources' => $sources,
				);
			}
			else {
				return null;
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.6: getUploadExcel
	//  Description: get upload FS Template page for Financial Analysis Report
    //-----------------------------------------------------
	public function getUploadExcel()
	{
		try{
			return View::make('financial.excelupload');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.7: postUploadExcel
	//  Description: process upload FS Template page for Financial Analysis Report
    //-----------------------------------------------------
	public function postUploadExcel()
	{
		try{
			$v = Validator::make(Input::all(), array( // validation rules
				'file' => 'required|mimes:xlsx'
			), array(
				'file.required' => 'Please select the Excel template to convert.',
				'file.mimes' => 'The file you were trying to upload does not have the correct format.'
			));
			if($v->fails()){
				return Redirect::route('postUploadExcel')->withErrors($v->errors());
			}

			$file = Input::file('file');								// get input file
			$destinationPath = 'temp'; 									// set directory
			$extension = $file->getClientOriginalExtension();			// get file extension
			$filename = rand(11111,99999).time().'.'.$extension;		// generate random filename
			$upload_success = $file->move($destinationPath, $filename);	// save file

			// init excel reader
			$excel = Excel::load('temp/'.$filename);
			$sheet = $excel->getActiveSheet();

			$money_factor = (string)$sheet->getCell('C6')->getValue();
			switch($money_factor){
				case "Ones":
					$money_factor = 1; break;
				case "Thousands":
					$money_factor = 1000; break;
				case "Millions":
					$money_factor = 1000000; break;
			}
			$fs = FinancialReport::create(array(
				'title' => (string)$sheet->getCell('B2')->getValue(),
				'year' => (string)$sheet->getCell('B10')->getValue(),
				'industry_id' => (string)$sheet->getCell('B4')->getCalculatedValue(),
				'money_factor' => $money_factor,
				'currency' => (string)$sheet->getCell('B6')->getCalculatedValue(),
				'step' => 12,
			));

			$count_items = 0;
			// check last balance sheet
			$balance_sheet_column_array = array('B','C','D','E');
			foreach($balance_sheet_column_array as $col){
				// check exist
				if($sheet->getCell($col.'10')->getValue() != null){
					BalanceSheet::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $count_items,
						'PropertyPlantAndEquipment' => ($sheet->getCell($col.'26')->getCalculatedValue()) ? $sheet->getCell($col.'26')->getCalculatedValue() : 0,
						'InvestmentProperty' => ($sheet->getCell($col.'27')->getCalculatedValue()) ? $sheet->getCell($col.'27')->getCalculatedValue() : 0,
						'Goodwill' => ($sheet->getCell($col.'28')->getCalculatedValue()) ? $sheet->getCell($col.'28')->getCalculatedValue() : 0,
						'IntangibleAssetsOtherThanGoodwill' => ($sheet->getCell($col.'29')->getCalculatedValue()) ? $sheet->getCell($col.'29')->getCalculatedValue() : 0,
						'InvestmentAccountedForUsingEquityMethod' => ($sheet->getCell($col.'30')->getCalculatedValue()) ? $sheet->getCell($col.'30')->getCalculatedValue() : 0,
						'InvestmentsInSubsidiariesJointVenturesAndAssociates' => ($sheet->getCell($col.'31')->getCalculatedValue()) ? $sheet->getCell($col.'31')->getCalculatedValue() : 0,
						'NoncurrentBiologicalAssets' => ($sheet->getCell($col.'32')->getCalculatedValue()) ? $sheet->getCell($col.'32')->getCalculatedValue() : 0,
						'NoncurrentReceivables' => ($sheet->getCell($col.'33')->getCalculatedValue()) ? $sheet->getCell($col.'33')->getCalculatedValue() : 0,
						'NoncurrentInventories' => ($sheet->getCell($col.'34')->getCalculatedValue()) ? $sheet->getCell($col.'34')->getCalculatedValue() : 0,
						'DeferredTaxAssets' => ($sheet->getCell($col.'35')->getCalculatedValue()) ? $sheet->getCell($col.'35')->getCalculatedValue() : 0,
						'CurrentTaxAssetsNoncurrent' => ($sheet->getCell($col.'36')->getCalculatedValue()) ? $sheet->getCell($col.'36')->getCalculatedValue() : 0,
						'OtherNoncurrentFinancialAssets' => ($sheet->getCell($col.'37')->getCalculatedValue()) ? $sheet->getCell($col.'37')->getCalculatedValue() : 0,
						'OtherNoncurrentNonfinancialAssets' => ($sheet->getCell($col.'38')->getCalculatedValue()) ? $sheet->getCell($col.'38')->getCalculatedValue() : 0,
						'NoncurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'39')->getCalculatedValue()) ? $sheet->getCell($col.'39')->getCalculatedValue() : 0,
						'NoncurrentAssets' => ($sheet->getCell($col.'40')->getCalculatedValue()) ? $sheet->getCell($col.'40')->getCalculatedValue() : 0,

						'Inventories' => ($sheet->getCell($col.'16')->getCalculatedValue()) ? $sheet->getCell($col.'16')->getCalculatedValue() : 0,
						'TradeAndOtherCurrentReceivables' => ($sheet->getCell($col.'15')->getCalculatedValue()) ? $sheet->getCell($col.'15')->getCalculatedValue() : 0,
						'CurrentTaxAssetsCurrent' => ($sheet->getCell($col.'17')->getCalculatedValue()) ? $sheet->getCell($col.'17')->getCalculatedValue() : 0,
						'CurrentBiologicalAssets' => ($sheet->getCell($col.'18')->getCalculatedValue()) ? $sheet->getCell($col.'18')->getCalculatedValue() : 0,
						'OtherCurrentFinancialAssets' => ($sheet->getCell($col.'19')->getCalculatedValue()) ? $sheet->getCell($col.'19')->getCalculatedValue() : 0,
						'OtherCurrentNonfinancialAssets' => ($sheet->getCell($col.'20')->getCalculatedValue()) ? $sheet->getCell($col.'20')->getCalculatedValue() : 0,
						'CashAndCashEquivalents' => ($sheet->getCell($col.'14')->getCalculatedValue()) ? $sheet->getCell($col.'14')->getCalculatedValue() : 0,
						'CurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'21')->getCalculatedValue()) ? $sheet->getCell($col.'21')->getCalculatedValue() : 0,
						'NoncurrentAssetsOrDisposalGroups' => ($sheet->getCell($col.'22')->getCalculatedValue()) ? $sheet->getCell($col.'22')->getCalculatedValue() : 0,
						'CurrentAssets' => ($sheet->getCell($col.'23')->getCalculatedValue()) ? $sheet->getCell($col.'23')->getCalculatedValue() : 0,

						'Assets' => ($sheet->getCell($col.'42')->getCalculatedValue()) ? $sheet->getCell($col.'42')->getCalculatedValue() : 0,

						'IssuedCapital' => ($sheet->getCell($col.'67')->getCalculatedValue()) ? $sheet->getCell($col.'67')->getCalculatedValue() : 0,
						'SharePremium' => ($sheet->getCell($col.'68')->getCalculatedValue()) ? $sheet->getCell($col.'68')->getCalculatedValue() : 0,
						'TreasuryShares' => ($sheet->getCell($col.'69')->getCalculatedValue()) ? $sheet->getCell($col.'69')->getCalculatedValue() : 0,
						'OtherEquityInterest' => ($sheet->getCell($col.'70')->getCalculatedValue()) ? $sheet->getCell($col.'70')->getCalculatedValue() : 0,
						'OtherReserves' => ($sheet->getCell($col.'71')->getCalculatedValue()) ? $sheet->getCell($col.'71')->getCalculatedValue() : 0,
						'RetainedEarnings' => ($sheet->getCell($col.'72')->getCalculatedValue()) ? $sheet->getCell($col.'72')->getCalculatedValue() : 0,
						'NoncontrollingInterests' => ($sheet->getCell($col.'73')->getCalculatedValue()) ? $sheet->getCell($col.'73')->getCalculatedValue() : 0,
						'Equity' => ($sheet->getCell($col.'74')->getCalculatedValue()) ? $sheet->getCell($col.'74')->getCalculatedValue() : 0,

						'NoncurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'57')->getCalculatedValue()) ? $sheet->getCell($col.'57')->getCalculatedValue() : 0,
						'OtherLongtermProvisions' => ($sheet->getCell($col.'58')->getCalculatedValue()) ? $sheet->getCell($col.'58')->getCalculatedValue() : 0,
						'NoncurrentPayables' => ($sheet->getCell($col.'59')->getCalculatedValue()) ? $sheet->getCell($col.'59')->getCalculatedValue() : 0,
						'DeferredTaxLiabilities' => ($sheet->getCell($col.'60')->getCalculatedValue()) ? $sheet->getCell($col.'60')->getCalculatedValue() : 0,
						'CurrentTaxLiabilitiesNoncurrent' => ($sheet->getCell($col.'61')->getCalculatedValue()) ? $sheet->getCell($col.'61')->getCalculatedValue() : 0,
						'OtherNoncurrentFinancialLiabilities' => ($sheet->getCell($col.'62')->getCalculatedValue()) ? $sheet->getCell($col.'62')->getCalculatedValue() : 0,
						'OtherNoncurrentNonfinancialLiabilities' => ($sheet->getCell($col.'63')->getCalculatedValue()) ? $sheet->getCell($col.'63')->getCalculatedValue() : 0,
						'NoncurrentLiabilities' => ($sheet->getCell($col.'64')->getCalculatedValue()) ? $sheet->getCell($col.'64')->getCalculatedValue() : 0,

						'CurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'47')->getCalculatedValue()) ? $sheet->getCell($col.'47')->getCalculatedValue() : 0,
						'OtherShorttermProvisions' => ($sheet->getCell($col.'48')->getCalculatedValue()) ? $sheet->getCell($col.'48')->getCalculatedValue() : 0,
						'TradeAndOtherCurrentPayables' => ($sheet->getCell($col.'49')->getCalculatedValue()) ? $sheet->getCell($col.'49')->getCalculatedValue() : 0,
						'CurrentTaxLiabilitiesCurrent' => ($sheet->getCell($col.'50')->getCalculatedValue()) ? $sheet->getCell($col.'50')->getCalculatedValue() : 0,
						'OtherCurrentFinancialLiabilities' => ($sheet->getCell($col.'51')->getCalculatedValue()) ? $sheet->getCell($col.'51')->getCalculatedValue() : 0,
						'OtherCurrentNonfinancialLiabilities' => ($sheet->getCell($col.'52')->getCalculatedValue()) ? $sheet->getCell($col.'52')->getCalculatedValue() : 0,
						'LiabilitiesIncludedInDisposalGroups' => ($sheet->getCell($col.'53')->getCalculatedValue()) ? $sheet->getCell($col.'53')->getCalculatedValue() : 0,
						'CurrentLiabilities' => ($sheet->getCell($col.'54')->getCalculatedValue()) ? $sheet->getCell($col.'54')->getCalculatedValue() : 0,

						'Liabilities' => ($sheet->getCell($col.'76')->getCalculatedValue()) ? $sheet->getCell($col.'76')->getCalculatedValue() : 0,
						'EquityAndLiabilities' => ($sheet->getCell($col.'78')->getCalculatedValue()) ? $sheet->getCell($col.'78')->getCalculatedValue() : 0
					));
					$count_items++;
				}
			}

			$index = 0;
			//income statement
			$income_statement_column_array = array('H','I','J','K');
			for($x=0;$x<($count_items-1);$x++){
				IncomeStatement::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $index,
						'Revenue' => ($sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue() : 0,
						'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue() : 0,
						'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue() : 0,

						'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue() : 0,
						'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue() : 0,
						'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue() : 0,
						'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue() : 0,
						'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue() : 0,
						'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue() : 0,

						'DifferenceBetweenCarryingAmountOfDividendsPayable' => ($sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue() : 0,
						'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue() : 0,
						'GainLossArisingFromDerecognitionOfFinancialAssets' => ($sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue() : 0,
						'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue() : 0,
						'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue() : 0,
						'ShareOfProfitLossOfAssociates' => ($sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue() : 0,
						'GainsLossesArisingFromDifference' => ($sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue() : 0,
						'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue() : 0,

						'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue() : 0,
						'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue() : 0,
						'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue() : 0,
						'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x].'35')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'35')->getCalculatedValue() : 0,
						'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'37')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'37')->getCalculatedValue() : 0,
						'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'38')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'38')->getCalculatedValue() : 0
				));
				$index++;
			}


			$index = 0;
			//cashflow
			$income_statement_column_array = array('H','I','J','K');
			for($x=0;$x<($count_items-1);$x++){
				// create cashflow
				$cf = CashFlow::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $index,
						'Amortization' => ($sheet->getCell($income_statement_column_array[$x].'42')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'42')->getCalculatedValue() : 0,
						'Depreciation' => ($sheet->getCell($income_statement_column_array[$x].'43')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'43')->getCalculatedValue() : 0,
						'InterestExpense' => ($sheet->getCell($income_statement_column_array[$x].'44')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'44')->getCalculatedValue() : 0,
						'NonCashExpenses' => ($sheet->getCell($income_statement_column_array[$x].'45')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'45')->getCalculatedValue() : 0,
						'PrincipalPayments' => ($sheet->getCell($income_statement_column_array[$x].'46')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'46')->getCalculatedValue() : 0,
						'InterestPayments' => ($sheet->getCell($income_statement_column_array[$x].'47')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'47')->getCalculatedValue() : 0,
						'LeasePayments' => ($sheet->getCell($income_statement_column_array[$x].'48')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'48')->getCalculatedValue() : 0
				));
				// ---------------------------------
				// compute for Debt Service Capacity
				// ---------------------------------
				$revenue = ($sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue() : 0;
				$cf->NetOperatingIncome = $revenue - ($cf->Amortization + $cf->Depreciation + $cf->InterestExpense +  $cf->NonCashExpenses);
				$cf->DebtServiceCapacity = 	abs($cf->PrincipalPayments) + abs($cf->InterestPayments) + abs($cf->LeasePayments);
				if($cf->DebtServiceCapacity != 0)
					$cf->DebtServiceCapacityRatio = $cf->NetOperatingIncome / $cf->DebtServiceCapacity;
				else
					$cf->DebtServiceCapacityRatio = 0;
				$cf->save();
				$index++;
			}

			unlink('temp/'.$filename); // delete file
			return Redirect::to('fr');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 22.8: faReportVsTemplate
	//  Description: display comparison for Financial Analysis Report
    //-----------------------------------------------------
    public function faReportVsTemplate($fsid)
    {
    	try{
	        $fr             = FinancialReport::find($fsid);
	        $industry_title = 'Cannot Determine';

			switch ($fr->industry_id) {
				case 1:
					$industry_title = 'Agriculture, Forestry, and Fishing';
					break;

				case 5:
					$industry_title = 'Mining and Quarrying';
					break;

				case 10:
					$industry_title = 'Manufacturing';
					break;

				case 35:
					$industry_title = 'Electricity, Gas, Steam, and Air Conditioning Supply';
					break;

				case 36:
					$industry_title = 'Water Supply';
					break;

				case 41:
					$industry_title = 'Construction';
					break;

				case 45:
					$industry_title = 'Wholesale and Retail Trade';
					break;

				case 49:
					$industry_title = 'Transport and Storage';
					break;

				case 55:
					$industry_title = 'Accommodation and Food Service Activities';
					break;

				case 58:
					$industry_title = 'Information and Communication';
					break;

				case 64:
					$industry_title = 'Financial and Insurance Activities';
					break;

				case 68:
					$industry_title = 'Real Estate Activities';
					break;

				case 69:
					$industry_title = 'Professional, Scientific and Technical Activities';
					break;

				case 77:
					$industry_title = 'Administrative and Support Service Activities';
					break;

				case 84:
					$industry_title = 'Public Administration and Defence';
					break;

				case 85:
					$industry_title = 'Education';
					break;

				case 86:
					$industry_title = 'Human Health and Social Work Activities';
					break;

				case 90:
					$industry_title = 'Arts, Entertainment, and Recreation';
					break;

				case 94:
					$industry_title = 'Other Service Activities';
					break;
			}

			$financialAnalysisHandler = new FinancialAnalysisHandler();
			$filePath = $financialAnalysisHandler->getFinancialReportPath($fr->entity_id);

			if (!file_exists($filePath)) {
				$financialAnalysisHandler->createReportFromReadyRatios($fr->entity_id);
			}

	        $fs_docs    = DB::table('tbldocument')
	            ->where('entity_id', '=', $fr->entity_id)
	            ->where('document_group', '=', 21)
	            ->where('upload_type', '!=', 5)
	            ->orderBy('document_type', 'ASC')
	            ->get();

	        return View::make('financial.fa-report-vs-template',
	            array(
	                'financial_report'      => $fr,
	                'filename'				=> basename($filePath),
					'industry'              => $industry_title,
					'industry_id'            => $fr->industry_id,
	                'balance_sheets'        => $fr->balanceSheets()->orderBy('index_count', 'asc')->get(),
	                'income_statements'     => $fr->incomeStatements()->orderBy('index_count', 'asc')->get(),
	                'cash_flow'             => $fr->cashFlow()->orderBy('index_count', 'asc')->get(),
	                'fs_docs'               => $fs_docs
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 22.9: getClearFRTemplates
	//  Description: delete all Financial Report uploaded via fr
    //-----------------------------------------------------
	public function getClearFRTemplates()
	{
		try{
			$fr = FinancialReport::where('entity_id', 0)->get();
			foreach($fr as $f) {
				BalanceSheet::where('financial_report_id', $f->id)->delete();
				IncomeStatement::where('financial_report_id', $f->id)->delete();
				CashFlow::where('financial_report_id', $f->id)->delete();
				ReadyratioKeySummary::where('financial_report_id', $f->id)->delete();
			}
			FinancialReport::where('entity_id', 0)->delete();
			return Redirect::to('fr');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 22.10: getUploadFSInput
	//  Description: Display Page To Upload FS Input
	//-----------------------------------------------------
	public function getUploadFSInput()
	{
		try{
			return View::make('financial.fs-input-upload');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 22.11: getUploadFSInput
	//  Description: Save Upload File data To Database
	//-----------------------------------------------------
	public function postUploadFSInput()
	{
		try{
			$validator = Validator::make(
				[
					'file'      => Input::file('file'),
					'extension' => strtolower(Input::file('file')->getClientOriginalExtension()),
				],
				[
					'file'          => 'required',
					'extension'      => 'required|in:xlsx,xls',
				]
			);

			if ($validator->fails()) {
				return Redirect::route('getUploadFSInput')->withErrors($validator->errors());
			}

			$file = Input::file('file');								// get input file
			$destinationPath = 'temp'; 									// set directory
			$extension = $file->getClientOriginalExtension();			// get file extension
			$filename = rand(11111, 99999) . time() . '.' . $extension;		// generate random filename
			$upload_success = $file->move($destinationPath, $filename);	// save file

			// init excel reader
			$excel = Excel::load('temp/' . $filename);
			$sheet = $excel->getActiveSheet();

			
			$fs = FinancialReport::create(array(
				'title' => (string)$sheet->getCell('B2')->getValue(),
				'year' => (string)$sheet->getCell('B6')->getValue(),
				'industry_id' => 0,
				'money_factor' => 1,
				'currency' => 'PHP',
				'step' => 12,
			));

			$count_items = 0;
			// check last balance sheet
			$balance_sheet_column_array = array('B','C','D','E');
			foreach($balance_sheet_column_array as $col){
				// check exist
				if($sheet->getCell($col.'6')->getValue() != null){
					BalanceSheet::create(array(
						'financial_report_id' => $fs->id,
						'index_count' => $count_items,
						'Year' => ($sheet->getCell($col . '6')->getCalculatedValue()) ? $sheet->getCell($col . '6')->getCalculatedValue() : 0,
						'PropertyPlantAndEquipment' => ($sheet->getCell($col.'22')->getCalculatedValue()) ? $sheet->getCell($col.'22')->getCalculatedValue() : 0,
						'InvestmentProperty' => ($sheet->getCell($col.'23')->getCalculatedValue()) ? $sheet->getCell($col.'23')->getCalculatedValue() : 0,
						'Goodwill' => ($sheet->getCell($col.'24')->getCalculatedValue()) ? $sheet->getCell($col.'24')->getCalculatedValue() : 0,
						'IntangibleAssetsOtherThanGoodwill' => ($sheet->getCell($col.'25')->getCalculatedValue()) ? $sheet->getCell($col.'25')->getCalculatedValue() : 0,
						'InvestmentAccountedForUsingEquityMethod' => ($sheet->getCell($col.'26')->getCalculatedValue()) ? $sheet->getCell($col.'26')->getCalculatedValue() : 0,
						'InvestmentsInSubsidiariesJointVenturesAndAssociates' => ($sheet->getCell($col.'27')->getCalculatedValue()) ? $sheet->getCell($col.'27')->getCalculatedValue() : 0,
						'NoncurrentBiologicalAssets' => ($sheet->getCell($col.'28')->getCalculatedValue()) ? $sheet->getCell($col.'27')->getCalculatedValue() : 0,
						'NoncurrentReceivables' => ($sheet->getCell($col.'29')->getCalculatedValue()) ? $sheet->getCell($col.'29')->getCalculatedValue() : 0,
						'NoncurrentInventories' => ($sheet->getCell($col.'30')->getCalculatedValue()) ? $sheet->getCell($col.'30')->getCalculatedValue() : 0,
						'DeferredTaxAssets' => ($sheet->getCell($col.'31')->getCalculatedValue()) ? $sheet->getCell($col.'31')->getCalculatedValue() : 0,
						'CurrentTaxAssetsNoncurrent' => ($sheet->getCell($col.'32')->getCalculatedValue()) ? $sheet->getCell($col.'32')->getCalculatedValue() : 0,
						'OtherNoncurrentFinancialAssets' => ($sheet->getCell($col.'33')->getCalculatedValue()) ? $sheet->getCell($col.'33')->getCalculatedValue() : 0,
						'OtherNoncurrentNonfinancialAssets' => ($sheet->getCell($col.'34')->getCalculatedValue()) ? $sheet->getCell($col.'34')->getCalculatedValue() : 0,
						'NoncurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'35')->getCalculatedValue()) ? $sheet->getCell($col.'35')->getCalculatedValue() : 0,
						'NoncurrentAssets' => ($sheet->getCell($col.'36')->getCalculatedValue()) ? $sheet->getCell($col.'36')->getCalculatedValue() : 0,

						'CashAndCashEquivalents' => ($sheet->getCell($col.'10')->getCalculatedValue()) ? $sheet->getCell($col.'10')->getCalculatedValue() : 0,
						'TradeAndOtherCurrentReceivables' => ($sheet->getCell($col.'11')->getCalculatedValue()) ? $sheet->getCell($col.'11')->getCalculatedValue() : 0,
						'Inventories' => ($sheet->getCell($col.'12')->getCalculatedValue()) ? $sheet->getCell($col.'12')->getCalculatedValue() : 0,
						'CurrentTaxAssetsCurrent' => ($sheet->getCell($col.'13')->getCalculatedValue()) ? $sheet->getCell($col.'13')->getCalculatedValue() : 0,
						'CurrentBiologicalAssets' => ($sheet->getCell($col.'14')->getCalculatedValue()) ? $sheet->getCell($col.'14')->getCalculatedValue() : 0,
						'OtherCurrentFinancialAssets' => ($sheet->getCell($col.'15')->getCalculatedValue()) ? $sheet->getCell($col.'15')->getCalculatedValue() : 0,
						'OtherCurrentNonfinancialAssets' => ($sheet->getCell($col.'16')->getCalculatedValue()) ? $sheet->getCell($col.'16')->getCalculatedValue() : 0,
						'CurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'17')->getCalculatedValue()) ? $sheet->getCell($col.'17')->getCalculatedValue() : 0,
						'NoncurrentAssetsOrDisposalGroups' => ($sheet->getCell($col.'18')->getCalculatedValue()) ? $sheet->getCell($col.'18')->getCalculatedValue() : 0,
						'CurrentAssets' => ($sheet->getCell($col.'19')->getCalculatedValue()) ? $sheet->getCell($col.'19')->getCalculatedValue() : 0,

						'Assets' => ($sheet->getCell($col.'36')->getCalculatedValue()) ? $sheet->getCell($col.'36')->getCalculatedValue() : 0,

						'IssuedCapital' => ($sheet->getCell($col.'63')->getCalculatedValue()) ? $sheet->getCell($col.'63')->getCalculatedValue() : 0,
						'SharePremium' => ($sheet->getCell($col.'64')->getCalculatedValue()) ? $sheet->getCell($col.'64')->getCalculatedValue() : 0,
						'TreasuryShares' => ($sheet->getCell($col.'65')->getCalculatedValue()) ? $sheet->getCell($col.'65')->getCalculatedValue() : 0,
						'OtherEquityInterest' => ($sheet->getCell($col.'66')->getCalculatedValue()) ? $sheet->getCell($col.'66')->getCalculatedValue() : 0,
						'OtherReserves' => ($sheet->getCell($col.'67')->getCalculatedValue()) ? $sheet->getCell($col.'67')->getCalculatedValue() : 0,
						'RetainedEarnings' => ($sheet->getCell($col.'68')->getCalculatedValue()) ? $sheet->getCell($col.'68')->getCalculatedValue() : 0,
						'NoncontrollingInterests' => ($sheet->getCell($col.'69')->getCalculatedValue()) ? $sheet->getCell($col.'69')->getCalculatedValue() : 0,
						'Equity' => ($sheet->getCell($col.'70')->getCalculatedValue()) ? $sheet->getCell($col.'70')->getCalculatedValue() : 0,

						'NoncurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'53')->getCalculatedValue()) ? $sheet->getCell($col.'53')->getCalculatedValue() : 0,
						'OtherLongtermProvisions' => ($sheet->getCell($col.'54')->getCalculatedValue()) ? $sheet->getCell($col.'54')->getCalculatedValue() : 0,
						'NoncurrentPayables' => ($sheet->getCell($col.'55')->getCalculatedValue()) ? $sheet->getCell($col.'55')->getCalculatedValue() : 0,
						'DeferredTaxLiabilities' => ($sheet->getCell($col.'56')->getCalculatedValue()) ? $sheet->getCell($col.'56')->getCalculatedValue() : 0,
						'CurrentTaxLiabilitiesNoncurrent' => ($sheet->getCell($col.'57')->getCalculatedValue()) ? $sheet->getCell($col.'57')->getCalculatedValue() : 0,
						'OtherNoncurrentFinancialLiabilities' => ($sheet->getCell($col.'58')->getCalculatedValue()) ? $sheet->getCell($col.'58')->getCalculatedValue() : 0,
						'OtherNoncurrentNonfinancialLiabilities' => ($sheet->getCell($col.'59')->getCalculatedValue()) ? $sheet->getCell($col.'59')->getCalculatedValue() : 0,
						'NoncurrentLiabilities' => ($sheet->getCell($col.'60')->getCalculatedValue()) ? $sheet->getCell($col.'60')->getCalculatedValue() : 0,

						'CurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'43')->getCalculatedValue()) ? $sheet->getCell($col.'43')->getCalculatedValue() : 0,
						'OtherShorttermProvisions' => ($sheet->getCell($col.'44')->getCalculatedValue()) ? $sheet->getCell($col.'44')->getCalculatedValue() : 0,
						'TradeAndOtherCurrentPayables' => ($sheet->getCell($col.'45')->getCalculatedValue()) ? $sheet->getCell($col.'45')->getCalculatedValue() : 0,
						'CurrentTaxLiabilitiesCurrent' => ($sheet->getCell($col.'46')->getCalculatedValue()) ? $sheet->getCell($col.'46')->getCalculatedValue() : 0,
						'OtherCurrentFinancialLiabilities' => ($sheet->getCell($col.'47')->getCalculatedValue()) ? $sheet->getCell($col.'47')->getCalculatedValue() : 0,
						'OtherCurrentNonfinancialLiabilities' => ($sheet->getCell($col.'48')->getCalculatedValue()) ? $sheet->getCell($col.'48')->getCalculatedValue() : 0,
						'LiabilitiesIncludedInDisposalGroups' => ($sheet->getCell($col.'49')->getCalculatedValue()) ? $sheet->getCell($col.'49')->getCalculatedValue() : 0,
						'CurrentLiabilities' => ($sheet->getCell($col.'50')->getCalculatedValue()) ? $sheet->getCell($col.'50')->getCalculatedValue() : 0,

						'Liabilities' => ($sheet->getCell($col.'72')->getCalculatedValue()) ? $sheet->getCell($col.'72')->getCalculatedValue() : 0,
						'EquityAndLiabilities' => ($sheet->getCell($col.'74')->getCalculatedValue()) ? $sheet->getCell($col.'74')->getCalculatedValue() : 0
					));
					$count_items++;
				}
			}

			$f_index = 0;
			//income statement
			$income_statement_column_array = array('H', 'I', 'J');
			for ($x = 0; $x < ($count_items - 1); $x++) {
				IncomeStatement::create(array(
					'financial_report_id' => $fs->id,
					'index_count' => $f_index,
					'Year' => ($sheet->getCell($income_statement_column_array[$x] . '6')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '6')->getCalculatedValue() : 0,
					'Revenue' => ($sheet->getCell($income_statement_column_array[$x] . '8')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '8')->getCalculatedValue() : 0,
					'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x] . '9')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '9')->getCalculatedValue() : 0,
					'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x] . '10')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '10')->getCalculatedValue() : 0,

					'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x] . '12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '12')->getCalculatedValue() : 0,
					'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x] . '13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '13')->getCalculatedValue() : 0,
					'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x] . '14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '14')->getCalculatedValue() : 0,
					'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x] . '15')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '15')->getCalculatedValue() : 0,
					'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x] . '16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '16')->getCalculatedValue() : 0,
					'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x] . '17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '17')->getCalculatedValue() : 0,

					'DifferenceBetweenCarryingAmountOfDividendsPayable' => ($sheet->getCell($income_statement_column_array[$x] . '19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '19')->getCalculatedValue() : 0,
					'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x] . '20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '20')->getCalculatedValue() : 0,
					'GainLossArisingFromDerecognitionOfFinancialAssets' => ($sheet->getCell($income_statement_column_array[$x] . '21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '21')->getCalculatedValue() : 0,
					'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x] . '22')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '22')->getCalculatedValue() : 0,
					'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x] . '23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '23')->getCalculatedValue() : 0,
					'ImpairmentLoss' => ($sheet->getCell($income_statement_column_array[$x] . '24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '24')->getCalculatedValue() : 0,
					'ShareOfProfitLossOfAssociates' => ($sheet->getCell($income_statement_column_array[$x] . '25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '25')->getCalculatedValue() : 0,
					'OtherIncomeFromSubsidiaries' => ($sheet->getCell($income_statement_column_array[$x] . '26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '26')->getCalculatedValue() : 0,
					'GainsLossesArisingFromDifference' => ($sheet->getCell($income_statement_column_array[$x] . '27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '27')->getCalculatedValue() : 0,
					'CumulativeGainPreviouslyRecognized' => ($sheet->getCell($income_statement_column_array[$x] . '28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '28')->getCalculatedValue() : 0,
					'HedgingGainsForHedge' => ($sheet->getCell($income_statement_column_array[$x] . '29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '29')->getCalculatedValue() : 0,
					'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x] . '30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '30')->getCalculatedValue() : 0,

					'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x] . '32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '32')->getCalculatedValue() : 0,
					'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x] . '33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '33')->getCalculatedValue() : 0,
					'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x] . '35')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '35')->getCalculatedValue() : 0,
					'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x] . '36')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '36')->getCalculatedValue() : 0,
					'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x] . '38')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '38')->getCalculatedValue() : 0,
					'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x] . '39')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '39')->getCalculatedValue() : 0
				));
				$f_index++;
			}

			$index = 0;
			//cashflow
			$income_statement_column_array = array('H', 'I', 'J');
			for ($x = 0; $x < ($count_items - 1); $x++) {
				// create cashflow
				$cf = CashFlow::create(array(
					'financial_report_id' => $fs->id,
					'index_count' => $index,
					'Year' => ($sheet->getCell($income_statement_column_array[$x] . '42')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '42')->getCalculatedValue() : 0,
					'AmortisationExpense' => ($sheet->getCell($income_statement_column_array[$x] . '43')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '43')->getCalculatedValue() : 0,
					'DepreciationExpense' => ($sheet->getCell($income_statement_column_array[$x] . '44')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '44')->getCalculatedValue() : 0,
					'NetCashFlowFromOperations' => ($sheet->getCell($income_statement_column_array[$x] . '45')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '45')->getCalculatedValue() : 0,
					'Amortization' => ($sheet->getCell($income_statement_column_array[$x] . '46')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '46')->getCalculatedValue() : 0,
					'Depreciation' => ($sheet->getCell($income_statement_column_array[$x] . '47')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '47')->getCalculatedValue() : 0,
					'InterestExpense' => ($sheet->getCell($income_statement_column_array[$x] . '48')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '48')->getCalculatedValue() : 0,
					'NonCashExpenses' => ($sheet->getCell($income_statement_column_array[$x] . '49')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '49')->getCalculatedValue() : 0,
					'PrincipalPayments' => ($sheet->getCell($income_statement_column_array[$x] . '50')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '50')->getCalculatedValue() : 0,
					'InterestPayments' => ($sheet->getCell($income_statement_column_array[$x] . '51')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '51')->getCalculatedValue() : 0,
					'LeasePayments' => ($sheet->getCell($income_statement_column_array[$x] . '52')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '52')->getCalculatedValue() : 0,
					'NetCashByOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x] . '53')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '53')->getCalculatedValue() : 0,
				));
				// ---------------------------------
				// compute for Debt Service Capacity
				// ---------------------------------
				$revenue = ($sheet->getCell($income_statement_column_array[$x] . '8')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x] . '8')->getCalculatedValue() : 0;
				$cf->NetOperatingIncome = $revenue - ($cf->Amortization + $cf->Depreciation + $cf->InterestExpense +  $cf->NonCashExpenses);
				$cf->DebtServiceCapacity = 	abs($cf->PrincipalPayments) + abs($cf->InterestPayments) + abs($cf->LeasePayments);
				if ($cf->DebtServiceCapacity != 0)
					$cf->DebtServiceCapacityRatio = $cf->NetOperatingIncome / $cf->DebtServiceCapacity;
				else
					$cf->DebtServiceCapacityRatio = 0;
				$cf->save();
				$index++;
			}

			//Key Ratios
			$fa_report = $fs;
			/*----------------------------------------------------------
			/*	Get Financial Statement Data
			/*--------------------------------------------------------*/
			$fa_report->balance_sheets      = $fa_report
				->balanceSheets()
				->orderBy('index_count', 'asc')
				->get();

			$fa_report->income_statements   = $fa_report
				->incomeStatements()
				->orderBy('index_count', 'asc')
				->get();

			$fa_report->cashflow            = $fa_report
				->cashFlow()
				->orderBy('index_count', 'asc')
				->get();
			
			for ($x = 2; $x >= 0; $x--) {
				$intangible_asset = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->Goodwill + $fa_report->balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill) : 0;
				$equity = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Equity : 0;
				$net_tangible_asset = $equity - $intangible_asset;
				$noncurrent_liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentLiabilities : 0;
				$current_liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentLiabilities : 0;
				$liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Liabilities : 0;
				$assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Assets : 0;
				$current_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentAssets : 0;
				$noncurrent_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentAssets : 0;
				$fixed_asset = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->PropertyPlantAndEquipment + $fa_report->balance_sheets[$x]->InvestmentProperty +  $fa_report->balance_sheets[$x]->NoncurrentBiologicalAssets) : 0;
				$NWC = $current_assets - $current_liabilities;
				$inventories = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Inventories : 0;
				$WCD = $NWC - $inventories;
				$cash_and_cash_equivalents = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CashAndCashEquivalents : 0;
				$other_current_financial_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->OtherCurrentFinancialAssets : 0;
				$trade_and_other_current_receivables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentReceivables : 0;
				$retained_earnings = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->RetainedEarnings : 0;

				$profit_loss_before_tax = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ProfitLossBeforeTax : 0;
				$gross_profit = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->GrossProfit : 0;
				$finance_costs = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->FinanceCosts : 0;
				$depreciation_and_amortisation_expense = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->DepreciationAndAmortisationExpense : 0;
				$revenue = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->Revenue : 0;
				$profit_loss = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ProfitLoss : 0;
				$finance_costs = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->FinanceCosts : 0;
				$comprehensive_income = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ComprehensiveIncome : 0;
				$cost_of_sales = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->CostOfSales : 0;

				$OIEeFC = $profit_loss_before_tax - ($gross_profit + $finance_costs);
				$EBIT = $profit_loss_before_tax + $finance_costs;
				$EBITDA = $EBIT + $depreciation_and_amortisation_expense;

				if (0 != $equity) {
					$financial_leverage = $liabilities / $equity;
					$LTD_to_E = $noncurrent_liabilities / $equity;
					$NCA_to_NW = $noncurrent_assets / $equity;
					$FA_to_NW = $fixed_asset / $equity;
				} else {
					$financial_leverage = 0;
					$LTD_to_E = 0;
					$NCA_to_NW = 0;
					$FA_to_NW = 0;
				}

				if (0 != $assets) {
					$debt_ratio = $liabilities / $assets;
				} else {
					$debt_ratio = 0;
				}

				if (0 != $liabilities) {
					$c_liability_ratio = $current_liabilities / $liabilities;
				} else {
					$c_liability_ratio = 0;
				}

				if (0 != $noncurrent_liabilities && 0 != $equity) {
					$capitalization = $noncurrent_liabilities / ($noncurrent_liabilities + $equity);
				} else {
					$capitalization = 0;
				}

				if (0 != $NWC) {
					$inventory_NWC = $inventories / $NWC;
				} else {
					$inventory_NWC = 0;
				}

				if (0 != $current_liabilities) {
					$current_ratio = $current_assets / $current_liabilities;
					$quick_ratio = ($cash_and_cash_equivalents + $other_current_financial_assets + $trade_and_other_current_receivables) / $current_liabilities;
					$cash_ratio = $cash_and_cash_equivalents / $current_liabilities;
				} else {
					$current_ratio = 0;
					$quick_ratio = 0;
					$cash_ratio = 0;
				}

				if (0 != $revenue) {
					$gross_margin = $gross_profit / $revenue;
					$ROS = $EBIT / $revenue;
					$profit_margin = $profit_loss / $revenue;
				} else {
					$gross_margin = 0;
					$ROS = 0;
					$profit_margin = 0;
				}

				if (0 != $finance_costs) {
					$ICR = $EBIT / $finance_costs;
				} else {
					$ICR = 0;
				}

				$beginningEquity = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Equity : 0;
				$endEquity = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Equity : 0;
				if ((0 == $beginningEquity) && (0 == $endEquity)) {
					$ROE = 0;
					$ROE_CI = 0;
				} else {
					$ROE = $profit_loss / (($beginningEquity + $endEquity) / 2);
					$ROE_CI = $comprehensive_income / (($beginningEquity + $endEquity) / 2);
				}

				$beginningAssets = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Assets : 0;
				$endAssets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Assets : 0;
				if ((0 == $beginningAssets) && (0 == $endAssets)) {
					$ROA = 0;
					$ROA_CI = 0;
				} else {
					$ROA = $profit_loss / (($beginningAssets + $endAssets) / 2);
					$ROA_CI = $comprehensive_income / (($beginningAssets + $endAssets) / 2);
				}

				$beginningNoncurrentLiabilities = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->NoncurrentLiabilities : 0;
				$endNoncurrentLiabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentLiabilities : 0;

				if ((0 != $beginningEquity) || (0 != $beginningNoncurrentLiabilities) || (0 != $endEquity) || (0 != $endNoncurrentLiabilities)) {
					$ROCE = $EBIT / (($beginningEquity + $beginningNoncurrentLiabilities + $endEquity + $endNoncurrentLiabilities) / 2);
				} else {
					$ROCE = 0;
				}

				$beginningTradeAndOtherCurrentReceivables = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->TradeAndOtherCurrentReceivables : 0;
				$endTradeAndOtherCurrentReceivables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentReceivables : 0;
				if ($revenue > 0) {
					$receivables_turnover = (($beginningTradeAndOtherCurrentReceivables + $endTradeAndOtherCurrentReceivables) / 2) / ($revenue / 365);
				} else {
					$receivables_turnover = 0;
				}

				$beginningTradeAndOtherCurrentPayables = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->TradeAndOtherCurrentPayables : 0;
				$endTradeAndOtherCurrentPayables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentPayables : 0;
				$beginningCurrentProvisionsForEmployeeBenefits = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->CurrentProvisionsForEmployeeBenefits : 0;
				$endCurrentProvisionsForEmployeeBenefits = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentProvisionsForEmployeeBenefits : 0;
				$beginningInventories = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Inventories : 0;
				$endInventories = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Inventories : 0;

				$totalTAOCP = $beginningTradeAndOtherCurrentPayables + $endTradeAndOtherCurrentPayables;
				$totalCPFEB = $beginningCurrentProvisionsForEmployeeBenefits + $endCurrentProvisionsForEmployeeBenefits;
				$totalBelow = $cost_of_sales + $endInventories - $beginningInventories;
				if ($totalBelow > 0) {
					$payable_turnover = (($totalTAOCP + $totalCPFEB) / 2) / ($totalBelow / 365);
				} else {
					$payable_turnover = 0;
				}

				if ($cost_of_sales > 0) {
					$inventory_turnover = (($beginningInventories + $endInventories) / 2) / ($cost_of_sales / 365);
				} else {
					$inventory_turnover = 0;
				}

				if ($revenue > 0
				) {
					$asset_turnover = (($beginningAssets + $endAssets) / 2) / ($revenue / 365);
				} else {
					$asset_turnover = 0;
				}

				$beginningCurrentAssets = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->CurrentAssets : 0;
				$endCurrentAssets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentAssets : 0;

				if ($revenue > 0) {
					$CAsset_turnover = (($beginningCurrentAssets + $endCurrentAssets) / 2) / ($revenue / 365);
				} else {
					$CAsset_turnover = 0;
				}

				if ($revenue > 0) {
					$capital_turnover = (($beginningEquity + $endEquity) / 2) / ($revenue / 365);
				} else {
					$capital_turnover = 0;
				}

				$CCC = ($inventory_turnover + $receivables_turnover) - $payable_turnover;

				if (0 != $assets) {
					$T1 = ($current_assets - $current_liabilities) / $assets;
					$T2 = $retained_earnings / $assets;
					$T3 = $EBIT / $assets;
					$T5 = $revenue / $assets;
				} else {
					$T1 = 0;
					$T2 = 0;
					$T3 = 0;
					$T5 = 0;
				}
				if (0 != $liabilities) {
					$T4 = $equity / $liabilities;
				} else {
					$T4 = 0;
				}

				$readyratioKeySummary = new ReadyratioKeySummary();
	            $readyratioKeySummary->entityid = 0;
	            $readyratioKeySummary->financial_report_id = $fa_report->id;
	            $readyratioKeySummary->Year = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->Year) : 0;
				$readyratioKeySummary->IntangibleAsset = !empty($intangible_asset) ? round($intangible_asset, 3) : 0;
				$readyratioKeySummary->NetTangibleAsset = !empty($net_tangible_asset) ? round($net_tangible_asset, 3) : 0;
				$readyratioKeySummary->NetAsset = !empty($equity) ? round($equity, 3) : 0;
				$readyratioKeySummary->FinancialLeverage = !empty($financial_leverage) ? round($financial_leverage, 3) : 0;
				$readyratioKeySummary->DebtRatio = !empty($debt_ratio) ? round($debt_ratio, 3) : 0;
				$readyratioKeySummary->LTDtoE = !empty($LTD_to_E) ? round($LTD_to_E, 3) : 0;
				$readyratioKeySummary->NCAtoNW = !empty($NCA_to_NW) ? round($NCA_to_NW, 3) : 0;
				$readyratioKeySummary->FixedAsset = !empty($fixed_asset) ? round($fixed_asset, 3) : 0;
				$readyratioKeySummary->FAtoNW = !empty($FA_to_NW) ? round($FA_to_NW, 3) : 0;
				$readyratioKeySummary->CLiabilityRatio = !empty($c_liability_ratio) ? round($c_liability_ratio, 3) : 0;
				$readyratioKeySummary->Capitalization = !empty($capitalization) ? round($capitalization, 3) : 0;
				$readyratioKeySummary->NWC = !empty($NWC) ? round($NWC, 3) : 0;
				$readyratioKeySummary->WCD = !empty($WCD) ? round($WCD, 3) : 0;
				$readyratioKeySummary->InventoryNWC = !empty($inventory_NWC) ? round($inventory_NWC, 3) : 0;
				$readyratioKeySummary->CurrentRatio = !empty($current_ratio) ? round($current_ratio, 3) : 0;
				$readyratioKeySummary->QuickRatio = !empty($quick_ratio) ? round($quick_ratio, 3) : 0;
				$readyratioKeySummary->CashRatio = !empty($cash_ratio) ? round($cash_ratio, 3) : 0;
				$readyratioKeySummary->OIEeFC = !empty($OIEeFC) ? round($OIEeFC, 3) : 0;
				$readyratioKeySummary->EBIT = !empty($EBIT) ? round($EBIT, 3) : 0;
				$readyratioKeySummary->EBITDA = !empty($EBITDA) ? round($EBITDA, 3) : 0;
				$readyratioKeySummary->GrossMargin = !empty($gross_margin) ? round($gross_margin, 3) : 0;
				$readyratioKeySummary->ROS = !empty($ROS) ? round($ROS, 3) : 0;
				$readyratioKeySummary->ProfitMargin = !empty($profit_margin) ? round($profit_margin, 3) : 0;
				$readyratioKeySummary->ICR = !empty($ICR) ? round($ICR, 3) : 0;
				$readyratioKeySummary->ROE = !empty($ROE) ? round($ROE, 3) : 0;
				$readyratioKeySummary->ROE_CI = !empty($ROE_CI) ? round($ROE_CI, 3) : 0;
				$readyratioKeySummary->ROA = !empty($ROA) ? round($ROA, 3) : 0;
				$readyratioKeySummary->ROA_CI = !empty($ROA_CI) ? round($ROA_CI, 3) : 0;
				$readyratioKeySummary->ROCE = !empty($ROCE) ? round($ROCE, 3) : 0;
				$readyratioKeySummary->ReceivablesTurnover = !empty($receivablesTurnover) ? round($receivablesTurnover, 3) : 0;
				$readyratioKeySummary->PayableTurnover = !empty($payableTurnover) ? round($payableTurnover, 3) : 0;
				$readyratioKeySummary->InventoryTurnover = !empty($inventoryTurnover) ? round($inventoryTurnover, 3) : 0;
				$readyratioKeySummary->AssetTurnover = !empty($asset_turnover) ? round($asset_turnover, 3) : 0;
				$readyratioKeySummary->CAssetTurnover = !empty($CAsset_turnover) ? round($CAsset_turnover, 3) : 0;
				$readyratioKeySummary->CapitalTurnover = !empty($capital_turnover) ? round($capital_turnover, 3) : 0;
				$readyratioKeySummary->CCC = !empty($CCC) ? round($CCC, 3) : 0;
				$readyratioKeySummary->T1 = !empty($T1) ? round($T1, 3) : 0;
				$readyratioKeySummary->T2 = !empty($T2) ? round($T2, 3) : 0;
				$readyratioKeySummary->T3 = !empty($T3) ? round($T3, 3) : 0;
				$readyratioKeySummary->T4 = !empty($T4) ? round($T4, 3) : 0;
				$readyratioKeySummary->T5 = !empty($T5) ? round($T5, 3) : 0;
				$readyratioKeySummary->save();
			}
			
			unlink('temp/' . $filename); // delete file
			return Redirect::to('fr');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 22.11: getFSKeyRatio
	//  Description: Display Page To FS Input Key Ratios
	//-----------------------------------------------------
	public function getFSKeyRatio($id)
	{
		try{
			$fr = FinancialReport::find($id);
			return View::make('financial.fs-key-ratio',[
				'financial_report' => $fr,
				'key_ratios' => $fr->keyRatio,
			]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
}
