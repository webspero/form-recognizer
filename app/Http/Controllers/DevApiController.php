<?php

//======================================================================
//  Class 19: Dev API Controller
//      Developers REST API that returns JSON and is run through HTTP
//======================================================================
class DevApiController extends BaseController
{
    var $section_id;    // Primary ID of the specified Section
    var $email;         // Email account used to access API
    var $pword;         // Password related to the Email Account
    var $action;        // Add, Edit, Delete, Get, Misc
    var $section;       // Specified Section requested
    var $post_fields;   // Post Fields passed to the API
    var $seed;          // Hashed Seed parameter
    
    /*-------------------------------------------------------------------------
	|	Controller Map
	|------------------------------------------------------------------------*/
    private static $tb_controller_map = array(
        /*--------------------------------------------------------------------
        |	Business Details 1
        |-------------------------------------------------------------------*/
        'user_account' => array(
            array ('SignupController', 'postSignupStore'),                  /**  Registration    */
            array ('LoginController', 'postLoginIndex'),                    /**  Login   */
            array ('LoginController', 'postActivationCode'),                /**  Account Activation    */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  DELETE */
        ),
        'business_details' => array(
            array ('tblentity', 'entityid'),                                /**  GET    */
            array ('FormSMEController', 'putSMEBusDetailsUpdate'),           /**  POST   */
            array ('SignupController', 'postSignupStore'),                  /**  PUT    */
        ),
        'related_companies' => array(
            array ('tblrelatedcompanies', 'entity_id'),                     /**  GET    */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblrelatedcompanies', 'relatedcompaniesid'),            /**  DELETE */
        ),
        'products_services' => array(
            array ('tblservicesoffer', 'entity_id'),                        /**  GET    */
            array ('FormSMEController', 'postSMEservicesoffer'),            /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblservicesoffer', 'servicesofferid'),                  /**  DELETE */
        ),
        'capital_details' => array(
            array ('tblcapital', 'entity_id'),                              /**  GET    */
            array ('CapitalController', 'postCapital'),                     /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblcapital', 'capitalid'),                              /**  DELETE */
        ),
        'main_locations' => array(
            array ('tbllocations', 'entity_id'),                            /**  GET    */
            array ('LocationsController', 'postLocations'),                 /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbllocations', 'locationid'),                           /**  DELETE */
        ),
        'branches' => array(
            array ('tblbranches', 'entity_id'),                             /**  GET    */
            array ('BranchesController', 'postBranches'),                   /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblbranches', 'branchid'),                              /**  DELETE */
        ),
        'shareholders' => array(
            array ('tblshareholders', 'entity_id'),                         /**  GET    */
            array ('ManagementController', 'postShareholders'),             /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblshareholders', 'id'),                                /**  DELETE */
        ),
        'directors' => array(
            array ('tbldirectors', 'entity_id'),                            /**  GET    */
            array ('ManagementController', 'postDirectors'),                /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbldirectors', 'id'),                                   /**  DELETE */
        ),
        'certifications' => array(
            array ('tblcertifications', 'entity_id'),                       /**  GET    */
            array ('CertificationController', 'postCertification'),         /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblcertifications', 'certificationid'),                 /**  DELETE */
        ),
        'business_type' => array(
            array ('tblbusinesstype', 'entity_id'),                         /**  GET    */
            array ('BusinesstypeController', 'postBusinesstype'),           /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblbusinesstype', 'businesstypeid'),                    /**  DELETE */
        ),
        'insurance' => array(
            array ('tblinsurance', 'entity_id'),                            /**  GET    */
            array ('InsuranceController', 'postInsurance'),                 /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblinsurance', 'insuranceid'),                          /**  DELETE */
        ),
        'cash_cycle' => array(
            array ('tblcycledetails', 'entity_id'),                         /**  GET    */
            array ('CycleDetailController', 'postCycleDetail'),             /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblcycledetails', 'id'),                                /**  DELETE */
        ),
        'fs_document' => array(
            array ('tbldocument', 'entity_id'),                             /**  GET    */
            array ('DocumentController', 'postUploadDocuments'),            /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbldocument', 'documentid'),                            /**  DELETE */
        ),
        'bank_statements' => array(
            array ('tbldocument', 'entity_id'),                             /**  GET    */
            array ('CashFlowProxyController', 'postBankStatements'),        /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbldocument', 'documentid'),                            /**  DELETE */
        ),
        'utility_bills' => array(
            array ('tbldocument', 'entity_id'),                             /**  GET    */
            array ('CashFlowProxyController', 'postUtilityBills'),          /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbldocument', 'documentid'),                            /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Business Details 2
        |-------------------------------------------------------------------*/
        'major_customers' => array(
            array ('tblmajorcustomer', 'entity_id'),                        /**  GET    */
            array ('MajorcustomerController', 'postMajorcustomer'),         /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblmajorcustomer', 'majorcustomerrid'),                 /**  DELETE */
        ),
        'major_suppliers' => array(
            array ('tblmajorsupplier', 'entity_id'),                        /**  GET    */
            array ('MajorsupplierController', 'postMajorsupplier'),         /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblmajorsupplier', 'majorsupplierid'),                  /**  DELETE */
        ),
        'major_locations' => array(
            array ('tblmajorlocation', 'entity_id'),                        /**  GET    */
            array ('MajorlocationController', 'postMajorlocation'),         /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblmajorlocation', 'marketlocationid'),                 /**  DELETE */
        ),
        'competitors' => array(
            array ('tblcompetitor', 'entity_id'),                           /**  GET    */
            array ('CompetitorController', 'postCompetitor'),               /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblcompetitor', 'competitorid'),                        /**  DELETE */
        ),
        'business_drivers' => array(
            array ('tblbusinessdriver', 'entity_id'),                       /**  GET    */
            array ('BusinessdriverController', 'postBusinessdriver'),       /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblbusinessdriver', 'businessdriverid'),                /**  DELETE */
        ),
        'business_segments' => array(
            array ('tblbusinesssegment', 'entity_id'),                      /**  GET    */
            array ('BusinesssegmentController', 'postBusinesssegment'),     /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblbusinesssegment', 'businesssegmentid'),              /**  DELETE */
        ),
        'past_projects' => array(
            array ('tblprojectcompleted', 'entity_id'),                     /**  GET    */
            array ('PastprojectController', 'postPastproject'),             /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblprojectcompleted', 'projectcompletedid'),            /**  DELETE */
        ),
        'future_initiatives' => array(
            array ('tblfuturegrowth', 'entity_id'),                         /**  GET    */
            array ('FuturegrowthController', 'postFuturegrowth'),           /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblfuturegrowth', 'futuregrowthid'),                    /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Owner Details
        |-------------------------------------------------------------------*/
        'owner_details' => array(
            array ('tblowner', 'entity_id'),                                /**  GET    */
            array ('FormSMEController', 'postSMEBusOwnerStore'),            /**  POST   */
            array ('OwnerController', 'putOwnerUpdate'),                    /**  PUT    */
            array ('tblowner', 'ownerid'),                                  /**  DELETE */
        ),
        'personal_loans' => array(
            array ('tblpersonalloans', 'owner_id'),                         /**  GET    */
            array ('OwnerController', 'postPersonalLoans'),                 /**  POST   */
            array ('OwnerController', 'postPersonalLoans'),                 /**  PUT    */
            array ('tblpersonalloans', 'id'),                               /**  DELETE */
        ),
        'key_manager' => array(
            array ('tblkeymanagers', 'entity_id'),                          /**  GET    */
            array ('OwnerController', 'postKeyManager'),                    /**  POST   */
            array ('OwnerController', 'putKeyManager'),                     /**  PUT    */
            array ('tblkeymanagers', 'keymanagerid'),                       /**  DELETE */
            array ('OwnerController', 'postKeyManagerOnly'),                /**  POST Key Manager Only */
        ),
        'other_business' => array(
            array ('tblmanagebus', 'ownerid'),                              /**  GET    */
            array ('OwnerController', 'postManageBusiness'),                /**  POST   */
            array ('OwnerController', 'postRelatedcompanies'),              /**  PUT    */
            array ('tblmanagebus', 'managebusid'),                          /**  DELETE */
        ),
        'education' => array(
            array ('tbleducation', 'ownerid'),                              /**  GET    */
            array ('OwnerController', 'postEducation'),                     /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tbleducation', 'educationid'),                          /**  DELETE */
        ),
        'spouse' => array(
            array ('tblspouse', 'ownerid'),                                 /**  GET    */
            array ('OwnerController', 'postEducation'),                     /**  POST   */
            array ('OwnerController', 'putSpouseUpdate'),                   /**  PUT    */
            array ('tblspouse', 'spouseid'),                                /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Existing Credit Facilities
        |-------------------------------------------------------------------*/
        'existing_credit' => array(
            array ('tblplanfacility', 'entity_id'),                         /**  GET    */
            array ('FormSMEController', 'postSMEPlanFacilityCreate'),       /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblplanfacility', 'planfacilityid'),                    /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Condition and Sustainability
        |-------------------------------------------------------------------*/
        'business_condition' => array(
            array ('tblsustainability', 'entity_id'),                       /**  GET    */
            array ('FormSMEController', 'postSMEBusConSus'),                /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblsustainability', 'sustainabilityid'),                /**  DELETE */
        ),
        'risk_assessment' => array(
            array ('tblriskassessment', 'entity_id'),                       /**  GET    */
            array ('RiskassessmentController', 'postRiskassessment'),       /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblriskassessment', 'riskassessmentid'),                /**  DELETE */
        ),
        'revenue_potential' => array(
            array ('tblrevenuegrowthpotential', 'entity_id'),               /**  GET    */
            array ('CapacityController', 'postRevenuePotential'),           /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblrevenuegrowthpotential', 'growthpotentialid'),       /**  DELETE */
        ),
        'expansion_investment' => array(
            array ('tblcapacityexpansion', 'entity_id'),                    /**  GET    */
            array ('CapacityController', 'postPhpm'),                       /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),   /**  PUT    */
            array ('tblcapacityexpansion', 'capacityexpansionid'),          /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Requested Credit Line
        |-------------------------------------------------------------------*/
        'required_credit' => array(
            array ('tblplanfacilityrequested', 'entity_id'),                    /**  GET    */
            array ('FormSMEController', 'postSMEPlanFacilityCreateRequested'),  /**  POST   */
            array ('RelatedcompaniesController', 'postRelatedcompanies'),       /**  PUT    */
            array ('tblplanfacilityrequested', 'id'),                           /**  DELETE */
        ),
        
        /*--------------------------------------------------------------------
        |	Submit for Rating
        |-------------------------------------------------------------------*/
        'rating_report' => array(
            array ('tblentity', 'loginid'),                                     /**  GET    */
            array ('SignupController', 'postNewReport'),                        /**  POST   */
            array ('ActionController', 'postCreditInvestigation'),              /**  Submit for Credit Investigation    */
            array ('ProfileSMEController', 'getSmeSummaryTbl'),                 /**  Get Unfilled Reminders             */
        ),
        
    );
    
    //-----------------------------------------------------
    //  Function 19.1: initUserAccntApiAccess
    //      Initialization of User Account
    //      related Post request
    //-----------------------------------------------------
    public function initUserAccntApiAccess($action)
    {
        /*  Variable Declaration    */
        $srv_resp           = array();
        
        $this->section_id   = 0;
        $this->email        = Input::get('email');
        $this->pword        = Input::get('pword');
        $this->action       = Input::get('action');
        $this->section      = Input::get('section');
        $this->post_fields  = Input::get('post_fields');
        $this->seed         = Input::get('seed');
        
        /*  Begin Main Processing   */
        $srv_resp           = self::devApiMng();
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.2: initMiscDataApiAccess
    //      Initialization of Miscellaneous Data
    //      related Post request
    //-----------------------------------------------------
    public function initMiscDataApiAccess($action)
    {
        /*  Variable Declaration    */
        $srv_resp           = array();
        $this->section_id   = 0;
        $this->email        = Input::get('email');
        $this->pword        = Input::get('pword');
        $this->action       = $action;
        $this->section      = Input::get('section');
        $this->seed         = Input::get('seed');
        
        if (Input::has('post_fields')) {
            $this->post_fields  = Input::get('post_fields');
        }
        
        /*  Begin Main Processing   */
        $srv_resp           = self::devApiMng();
        
        return $srv_resp;
    }

    //-----------------------------------------------------
    //  Function 19.3: initPostApiAccess
    //      Initialization of Developer
    //      User API POST request
    //-----------------------------------------------------
    public function initPostApiAccess($entity_id)
    {
        /*  Variable Declaration    */
        $srv_resp           = array();
        $this->section_id   = $entity_id;
        $this->email        = Input::get('email');
        $this->pword        = Input::get('pword');
        $this->action       = Input::get('action');
        $this->section      = Input::get('section');
        $this->post_fields  = Input::get('post_fields');
        $this->seed         = Input::get('seed');
        
        /*  Begin Main Processing   */        
        $srv_resp           = self::devApiMng();
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.4: initGetApiAccess
    //      Initialization of Developer User API GET request
    //-----------------------------------------------------
    public function initGetApiAccess($section, $section_id)
    {
        /*  Variable Declaration    */
        $srv_resp           = array();
        $this->section_id   = $section_id;
        $this->email        = Input::get('email');
        $this->pword        = Input::get('pword');
        $this->action       = 'get';
        $this->section      = $section;
        $this->post_fields  = CONT_NULL;
        $this->seed         = Input::get('seed');
        
        /*  Begin Main Processing   */
        $srv_resp           = self::devApiMng();
        
        /*	関数終亁    */
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.5: initDeleteApiAccess
    //      Initialization of Developer User API DELETE request
    //-----------------------------------------------------
    public function initDeleteApiAccess($section, $section_id)
    {
        /*  Variable Declaration    */
        $srv_resp           = array();
        $this->section_id   = $section_id;
        $this->email        = Input::get('email');
        $this->pword        = Input::get('pword');
        $this->action       = 'delete';
        $this->section      = $section;
        $this->post_fields  = CONT_NULL;
        $this->seed         = Input::get('seed');
        
        /*  Begin Main Processing   */
        $srv_resp           = self::devApiMng();
        
        /*	関数終亁    */
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.6: devApiMng
    //      Main Processing Control
    //-----------------------------------------------------
    private function devApiMng()
    {
        /*  Variable Declaration    */
        $user_sts           = STS_NG;
        $srv_resp['sts']    = STS_NG;
        
        /*  Verify User Access      */
        $user_sts   = self::verifyUserAccess($this->email, $this->pword);
        
        /** User access is allowed  */
        if (STS_OK == $user_sts) {
            /*  Check Action received   */
            switch ($this->action) {
                /** User account            */
                case 'user_registration':
                case 'account_activation':
                case 'user_login':
                case 'submit_report':
                case 'create_report':
                case 'check_requirements':
                    $srv_resp = self::userAccntMng();
                    break;
                
                case 'province':
                case 'city':
                case 'major_cities':
                case 'zipcode':
                case 'industry_main':
                case 'industry_sub':
                case 'industry_row':
                case 'banks':
                case 'completed_reports':
                case 'questionnaire_config':
                    $srv_resp = self::miscDataMng();
                    break;
                    
                /** Add record action       */
                case 'add':
                case 'edit':
                    $srv_resp = self::addDatabaseRecord();
                    break;
                
                /** Forms with Uploads      */
                case 'upload':
                    $srv_resp = self::uploadFileRecord();
                    break;
                
                case 'get':
                    $srv_resp = self::getDatabaseRecord();
                    break;
                
                case 'delete':
                    $srv_resp = self::delDatabaseRecord();
                    break;
                    
                default:
                    $srv_resp['messages']    = 'Invalid Action';
                    break;
            }
            
        }
        /** User access is denied   */
        else {
            $srv_resp['messages']    = 'Access Denied';
        }
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.7: verifyUserAccess
    //      Verify if the API User is allowed access
    //-----------------------------------------------------
    private function verifyUserAccess($email, $pword)
    {
        /*  Variable Declaration    */
        $user_sts       = STS_NG;   // Allowed access status flag
        
        /*  Check Database for the User's access    */
        $api_user_obj   = DeveloperApiUser::where('email', $email)
            ->where('password', $pword)
            ->where('status', ACTIVE)
            ->first();
        
        /*  User exist  */
        if (0 < @count($api_user_obj)) {
            $user_sts   = STS_OK;
        }
        
        /*	関数終亁    */
        return $user_sts;
        
        /*  Code from here below are currently disabled */
        /** User access allowed */
        if (0 < @count($api_user_obj)) {
            if ('upload' == $this->action) {
                $post_data             = Input::get();
                
                foreach ($post_data as $key => $val) {
                    if (is_array($post_data[$key])) {
                        foreach ($post_data[$key] as $mi_key => $mi_val) {
                            $garden[$key.'['.$mi_key.']']       = $mi_val;
                        }
                    }
                    else {
                        if ('seed' != $key) {
                            $garden[$key]       = $val;
                        }
                    }
                }
                
                $garden['api_key']  = $api_user_obj->scare_crow;
            }
            else if (Input::has('post_fields')) {
                $post_data  = Input::get('post_fields');
                
                foreach ($post_data as $key => $val) {
                    if (is_array($post_data[$key])) {
                        foreach ($post_data[$key] as $mi_key => $mi_val) {
                            $garden[$key.'['.$mi_key.']']       = $mi_val;
                        }
                    }
                    else {
                        $garden[$key]       = $val;
                    }
                }
                
                $garden['api_key']  = $api_user_obj->scare_crow;
            }
            else if (('get' == $this->action)
            || ('delete' == $this->action)) {
                $garden = array(
                    $this->section_id,
                    $api_user_obj->scare_crow
                );
            }
            else {
                $user_sts   = STS_OK;
            }
            
            if (STS_NG == $user_sts) {
                $garden = implode('-', $garden);
                $seed   = sha1($garden);
                
                if ($seed == $this->seed) {
                    $user_sts   = STS_OK;
                }
            }
        }
        
        /*	関数終亁    */
        return $user_sts;
    }
    
    //-----------------------------------------------------
    //  Function 19.8: addDatabaseRecord
    //      Add Database record according to specified section
    //-----------------------------------------------------
    private function addDatabaseRecord()
    {
        /*  Variable Declaration    */
        $srv_resp   = array();
        $controller = self::$tb_controller_map[$this->section][1][0];
        $function   = self::$tb_controller_map[$this->section][1][1];
        
        /*  Configure the Post Fields   */
        foreach ($this->post_fields as $key => $val) {
            $fields[$key]   = $val;
        }
        
        /** Insert the Post fields to Laravel's Input   */
        Input::merge($fields);
        
        /*  Call the Laravel function according to section specified    */
        /** Owner Details related   */
        if (isset($fields['owner_id'])) {
            if (isset($fields['owner_type'])) {
                $srv_resp = App::make($controller)->$function($this->section_id, $fields['owner_id'], $fields['owner_type']);
            }
            else {
                
                if ('edit' == $this->action) {
                    $controller = self::$tb_controller_map[$this->section][2][0];
                    $function   = self::$tb_controller_map[$this->section][2][1];
                    
                    if ('personal_loans' == $this->section) {
                        $srv_resp = App::make($controller)->$function($this->section_id, $fields['master_key']);
                    }
                    else {
                        $srv_resp = App::make($controller)->$function($fields['master_key']);
                    }
                }
                else {
                    if (0 == $fields['owner_id']) {
                        $controller = self::$tb_controller_map[$this->section][4][0];
                        $function   = self::$tb_controller_map[$this->section][4][1];
                        
                        $srv_resp = App::make($controller)->$function($this->section_id);
                    }
                    else {
                        $srv_resp = App::make($controller)->$function($this->section_id, $fields['owner_id']);
                    }
                }
                
            }
        }
        /** Business condition and Sustainability related   */
        else if (isset($fields['condition_type'])) {
            $srv_resp = App::make($controller)->$function($fields['condition_type'], $this->section_id);
        }
        else {
            $srv_resp = App::make($controller)->$function($this->section_id);
        }
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.9: getDatabaseRecord
    //      Gets Database record according to specified section
    //-----------------------------------------------------
    private function getDatabaseRecord()
    {
        /*  Variable Declaration    */
        $srv_resp               = array();
        $srv_resp['sts']        = STS_NG;
        $srv_resp['messages']   = STR_EMPTY;
        
        /*  The requested section exist */
        if ((isset(self::$tb_controller_map[$this->section][0][0]))
        && (isset(self::$tb_controller_map[$this->section][0][1]))) {
            /*  Get the Table and ID from the Map   */
            $db_tbl     = self::$tb_controller_map[$this->section][0][0];
            $id_fld     = self::$tb_controller_map[$this->section][0][1];
            
            /*  Query the Database for the requested data   */
            switch ($this->section) {
                /** Financial Document  */
                case 'fs_document':
                    $db_data = DB::table($db_tbl)->where($id_fld, $this->section_id)->where('document_group', 21)->get();
                    break;
                    
                /** Bank Statements     */    
                case 'bank_statements':
                    $db_data = DB::table($db_tbl)->where($id_fld, $this->section_id)->where('document_group', 51)->get();
                    break;
                
                /** Utiity Bills        */
                case 'utility_bills':
                    $db_data = DB::table($db_tbl)->where($id_fld, $this->section_id)->where('document_group', 61)->get();
                    break;
                
                default:
                    $db_data = DB::table($db_tbl)->where($id_fld, $this->section_id)->get();
                    
                    if ('rating_report' == $this->section) {
                        
                        foreach ($db_data as $data) {
                            if (7 == $data->status) {
                                $data->sme_score = PublicAPIController::getCalculatedSmeReport($data->entityid);
                            }
                        }
                        
                    }
                    
                    break;
            }
            
            /*  Records have been found */
            if ($db_data) {
                $srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Records Succesfully Retrieved';
                $srv_resp['data']       = $db_data;
            }
            /*  No records found for requested data */
            else {
                $srv_resp['messages']   = 'No Records found';
            }
        }
        /*  Section requested is invalid    */
        else {
            $srv_resp['messages']   = 'Invalid Section Specified';
        }
        
        return json_encode($srv_resp);
    }
    
    //-----------------------------------------------------
    //  Function 19.10: delDatabaseRecord
    //      Deletes Database record according to specified section
    //-----------------------------------------------------
    private function delDatabaseRecord()
    {
        /*  Variable Declaration    */
        $srv_resp               = array();
        $srv_resp['sts']        = STS_NG;
        $srv_resp['messages']   = STR_EMPTY;
        
        /*  The requested section exist */
        if ((isset(self::$tb_controller_map[$this->section][3][0]))
        && (isset(self::$tb_controller_map[$this->section][3][1]))) {
            /*  Get the Table and ID from the Map   */
            $db_tbl     = self::$tb_controller_map[$this->section][3][0];
            $id_fld     = self::$tb_controller_map[$this->section][3][1];
            
            /*  Query the Database for the requested data   */
            $deleted    = DB::table($db_tbl)->where($id_fld, $this->section_id)->delete();
            
            /*  Records have been found */
            if ($deleted) {
                $srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Records Succesfully Deleted';
            }
            /*  No records found for requested data */
            else {
                $srv_resp['messages']   = 'No Records found';
            }
        }
        /*  Section requested is invalid    */
        else {
            $srv_resp['messages']   = 'Invalid Section Specified';
        }
        
        return json_encode($srv_resp);
    }
    
    //-----------------------------------------------------
    //  Function 19.11: uploadFileRecord
    //      Add Database record according to specified section
    //-----------------------------------------------------
    private function uploadFileRecord()
    {
        /*  Variable Declaration    */
        $srv_resp   = array();
        $controller = self::$tb_controller_map[$this->section][1][0];
        $function   = self::$tb_controller_map[$this->section][1][1];
        
        /*  Configure the Post Fields   */
        foreach (Input::get() as $key => $val) {
            $fields[$key]   = $val;
        }
        
        Input::merge($fields);
        
        /*  Call the Laravel function according to section specified    */
        $srv_resp = App::make($controller)->$function($this->section_id);
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.12: userAccntMng
    //      User Account Management
    //-----------------------------------------------------
    private function userAccntMng()
    {
        /*  Variable Declaration    */
        $srv_resp   = array();
        
        /*  Select the Function to be executed according to action  */
        switch ($this->action) {
            /** User Registration   */
            case 'user_registration':
                $controller = self::$tb_controller_map[$this->section][0][0];
                $function   = self::$tb_controller_map[$this->section][0][1];
                break;
            
            /** User Login          */
            case 'user_login':
                $controller = self::$tb_controller_map[$this->section][1][0];
                $function   = self::$tb_controller_map[$this->section][1][1];
                break;
                
            /** Account Activation  */    
            case 'account_activation':
                $controller = self::$tb_controller_map[$this->section][2][0];
                $function   = self::$tb_controller_map[$this->section][2][1];
                break;
            
            /** Submit Report       */    
            case 'submit_report':
                $controller = self::$tb_controller_map[$this->section][2][0];
                $function   = self::$tb_controller_map[$this->section][2][1];
                break;
            
            /** Create New Report   */    
            case 'create_report':
                $controller = self::$tb_controller_map[$this->section][1][0];
                $function   = self::$tb_controller_map[$this->section][1][1];
                break;
                
            /** Check Required Fields   */    
            case 'check_requirements':
                $controller = self::$tb_controller_map[$this->section][3][0];
                $function   = self::$tb_controller_map[$this->section][3][1];
                break;
        }
        
        /*  Configure the Post Fields   */
        foreach ($this->post_fields as $key => $val) {
            $fields[$key]   = $val;
        }
        
        /** Insert the Post fields to Laravel's Input   */
        Input::merge($fields);
        
        /*  Call the Laravel function according to action specified */
        /**  Account Activation     */
        if ('account_activation' == $this->action) {
            $srv_resp = App::make($controller)->$function($fields['activation_code']);
        }
        /*  Submit Report or Check Requirements */
        else if (('submit_report' == $this->action)
        || ('check_requirements' == $this->action)) {
            $srv_resp = App::make($controller)->$function($fields['entity_id']);
        }
        else {
            $srv_resp = App::make($controller)->$function();
        }
        
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.13: miscDataMng
    //      Miscellaneous Management
    //-----------------------------------------------------
    private function miscDataMng()
    {
        /*  Variable Declaration    */
        $srv_resp   = array();
        
        /*  Configure the Post Fields   */
        if (isset($this->post_fields)) {
            
            foreach ($this->post_fields as $key => $val) {
                $fields[$key]   = $val;
            }
            
            /** Insert the Post fields to Laravel's Input   */
            Input::merge($fields);
        }
        
        /*  Queries the Database for Data   */
        /**  Account Activation     */
        if ('province' == $this->action) {
            $srv_resp['data']       = Zipcode::distinct()->select('major_area')->groupBy('major_area')->get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Provinces found';
            
            json_encode($srv_resp);
        }
        /*  Get Cities  */
        else if ('city' == $this->action) {
            $srv_resp['data']       = Zipcode::select('id', 'city')->where('major_area', Input::get('province_name'))->get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Cities found';
            
            json_encode($srv_resp);
        }
        /*  Get Major Cities  */
        else if ('major_cities' == $this->action) {
            //JM - changed list to pluck
            $srv_resp['data']       = City::all()->pluck('city', 'cityid');
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Major Cities found';
            
            json_encode($srv_resp);
        }
        /*  Get Zipcode */
        else if ('zipcode'  == $this->action) {
            $srv_resp['data']       = Zipcode::where('id', Input::get('city_id'))->select('zip_code')->first();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Zipcode found';
            
            json_encode($srv_resp);
        }
        /*  Get Industries  */
        else if ('industry_main'  == $this->action) {
            $srv_resp['data']       = Industrymain::get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Industries found';
            
            json_encode($srv_resp);
        }
        /*  Get Industries Sub */
        else if ('industry_sub'  == $this->action) {
            $srv_resp['data']       = Industrysub::where('industry_main_id', Input::get('industry_id'))->get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Industries found';
            
            json_encode($srv_resp);
        }
        /*  Get Industries Row  */
        else if ('industry_row'  == $this->action) {
            $srv_resp['data']       = Industryrow::where('industry_sub_id', Input::get('industry_sub_id'))->get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Industries found';
            
            json_encode($srv_resp);
        }
        /*  Get Banks   */
        else if ('banks'  == $this->action) {            
            $srv_resp['data']       = Bank::select('id', 'bank_name')->get();
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Banks found';
            
            json_encode($srv_resp);
        }
        /*  Get Completed Report    */
        else if ('completed_reports'  == $this->action) {            
            $srv_resp['data']           = Entity::where('loginid', Input::get('login_id'))
                ->where('status', 7)
                ->select('entityid', 'companyname', 'completed_date')
                ->get();
                
            if (0 < @count($srv_resp['data'])) {
                $srv_resp['sts']            = STS_OK;
                $srv_resp['messages']       = 'Reports found';
            }
            else {
                $srv_resp['sts']            = STS_NG;
                $srv_resp['messages']       = 'No Reports found';
            }
            
            json_encode($srv_resp);
        }
        /*  Get Questionnaire Config    */
        else if ('questionnaire_config' == $this->action) {
            $entity             = Entity::where('entityid', Input::get('entity_id'))->first();
            $bank               = Bank::where('id', $entity->current_bank)->first();
            $question_db        = new QuestionnaireConfig;
            
            $srv_resp['data']       = $question_db->getQuestionnaireConfigbyBankID($entity->current_bank);
            
            if ($bank) {
                
                if (CMN_ON == $bank->is_standalone) {
                    $srv_resp['data']               = array();
                    $srv_resp['data']['standalone'] = CMN_ON;
                }
            }
            
            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Questionnaire Config found';
        }
        /*  Invalid ACtion */
        else {
            $srv_resp['messages']       = 'Invalid Action';
        }
        
        /*	関数終亁    */
        return $srv_resp;
    }
    
    //-----------------------------------------------------
    //  Function 19.14: showAPISuite
    //      
    //-----------------------------------------------------
    public static function showAPISuite()
    {
        return View::make('api-test');
    }
    
    public static function testGetServiceMapper()
    {
        $test_data          = array();
        $input['service']   = Input::get('service');
        
        $data       = self::$input['service']();
        
        if (isset($data['action'])) {
            $test_url   = 'http://128.199.203.18/cbpo_rest_api/delete_data/'.$data['section'].'/'.$data['section_id'];
        }
        else {
            $test_url   = 'http://128.199.203.18/cbpo_rest_api/get_data/'.$data['section'].'/'.$data['section_id'];
        }
        
        $email      = 'creditbpo.philippines@gmail.com';
        $pword      = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';
        
        $output['full_url'] = $test_url.'?email='.urlencode($email).'&pword='.$pword;
        
        $result = file_get_contents($output['full_url']);
        
        $output['response'] = STR_EMPTY;
        
        foreach ($http_response_header as $key_resp => $val_resp) {
            $output['response'] .= $val_resp.'<br/>';
        }
        
        $output['response'] .= '<br/><br/>'.$result;
        
        return json_encode($output);
    }

    //-----------------------------------------------------
    //  Function 19.15: delAffiliates
    //      Functon to delete related companies
    //-----------------------------------------------------
    public static function delAffiliates()
    {
        $data               = array();
        $data['section']    = 'related_companies';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblrelatedcompanies')
            ->where('entity_id', '565')
            ->orderBy('relatedcompaniesid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->relatedcompaniesid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.16: delProducts
    //      Function to delete services offered
    //-----------------------------------------------------
    public static function delProducts()
    {
        $data               = array();
        $data['section']    = 'products_services';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblservicesoffer')
            ->where('entity_id', '565')
            ->orderBy('servicesofferid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->servicesofferid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.17: delCapital
    //      Function to delete capital
    //-----------------------------------------------------
    public static function delCapital()
    {
        $data               = array();
        $data['section']    = 'capital_details';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblcapital')
            ->where('entity_id', '565')
            ->orderBy('capitalid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->capitalid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.18: delMainLoc
    //      Function to delete main location
    //-----------------------------------------------------
    public static function delMainLoc()
    {
        $data               = array();
        $data['section']    = 'main_locations';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbllocations')
            ->where('entity_id', '565')
            ->orderBy('locationid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->locationid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.19: delBranches
    //      function to delete branches
    //-----------------------------------------------------
    public static function delBranches()
    {
        $data               = array();
        $data['section']    = 'branches';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblbranches')
            ->where('entity_id', '565')
            ->orderBy('branchid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->branchid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.20: delShareholders
    //      funciton to delete Shareholders
    //-----------------------------------------------------
    public static function delShareholders()
    {
        $data               = array();
        $data['section']    = 'shareholders';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblshareholders')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->id;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.21: delDirectors
    //      function to delete directors
    //-----------------------------------------------------
    public static function delDirectors()
    {
        $data               = array();
        $data['section']    = 'directors';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbldirectors')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->id;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.22: delCertifications
    //      Function to delete certifications
    //-----------------------------------------------------
    public static function delCertifications()
    {
        $data               = array();
        $data['section']    = 'certifications';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblcertifications')
            ->where('entity_id', '565')
            ->orderBy('certificationid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->certificationid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.23: delImportExport 
    //      Functon to delete Import and Export
    //-----------------------------------------------------
    public static function delImportExport()
    {
        $data               = array();
        $data['section']    = 'business_type';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblbusinesstype')
            ->where('entity_id', '565')
            ->orderBy('businesstypeid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->businesstypeid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.24: delInsurance
    //      Function to delete Insurance
    //-----------------------------------------------------
    public static function delInsurance()
    {
        $data               = array();
        $data['section']    = 'insurance';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblinsurance')
            ->where('entity_id', '565')
            ->orderBy('insuranceid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->insuranceid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.25: delCashCycle
    //      Function to delete Cash Cycle
    //-----------------------------------------------------
    public static function delCashCycle()
    {
        $data               = array();
        $data['section']    = 'cash_cycle';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblcycledetails')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->id;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.26: delFsDocument
    //      Function to delete Financial Statement Document
    //-----------------------------------------------------
    public static function delFsDocument()
    {
        $data               = array();
        $data['section']    = 'fs_document';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbldocument')
            ->where('entity_id', '565')
            ->where('document_group', 21)
            ->orderBy('documentid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->documentid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.27: delBankStatement
    //      Function to delete bank statement
    //-----------------------------------------------------
    public static function delBankStatement()
    {
        $data               = array();
        $data['section']    = 'bank_statements';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbldocument')
            ->where('entity_id', '565')
            ->where('document_group', 51)
            ->orderBy('documentid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->documentid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.28: delUtilityBills 
    //      Function to delete utility bills
    //-----------------------------------------------------
    public static function delUtilityBills()
    {
        $data               = array();
        $data['section']    = 'utility_bills';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbldocument')
            ->where('entity_id', '565')
            ->where('document_group', 61)
            ->orderBy('documentid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->documentid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.29: delMajorCustomers
    //      Function to delete Major Customers
    //-----------------------------------------------------
    public static function delMajorCustomers()
    {
        $data               = array();
        $data['section']    = 'major_customers';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblmajorcustomer')
            ->where('entity_id', '565')
            ->orderBy('majorcustomerrid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->majorcustomerrid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.30: delMajorSuppliers 
    //      Function to delete Major Suppliers
    //-----------------------------------------------------
    public static function delMajorSuppliers()
    {
        $data               = array();
        $data['section']    = 'major_suppliers';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblmajorsupplier')
            ->where('entity_id', '565')
            ->orderBy('majorsupplierid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->majorsupplierid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.31: delMajorLoc
    //      Function to delete Major Locations
    //-----------------------------------------------------
    public static function delMajorLoc()
    {
        $data               = array();
        $data['section']    = 'major_locations';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblmajorlocation')
            ->where('entity_id', '565')
            ->orderBy('marketlocationid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->marketlocationid;
        }
        
        return $data;
    }

    //-----------------------------------------------------
    //  Function 19.32: delCompetitor 
    //      Function to delete Competitor
    //-----------------------------------------------------
    public static function delCompetitor()
    {
        $data               = array();
        $data['section']    = 'competitors';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblcompetitor')
            ->where('entity_id', '565')
            ->orderBy('competitorid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->competitorid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.33: delBizDriver 
    //      Function to delete business drivers
    //-----------------------------------------------------
    public static function delBizDriver()
    {
        $data               = array();
        $data['section']    = 'business_drivers';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblbusinessdriver')
            ->where('entity_id', '565')
            ->orderBy('businessdriverid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->businessdriverid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.34: delBizSegments 
    //      Function to delete business segments
    //-----------------------------------------------------
    public static function delBizSegments()
    {
        $data               = array();
        $data['section']    = 'business_segments';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblbusinesssegment')
            ->where('entity_id', '565')
            ->orderBy('businesssegmentid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->businesssegmentid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.35: delPastProj 
    //      Function to delete past projects
    //-----------------------------------------------------
    public static function delPastProj()
    {
        $data               = array();
        $data['section']    = 'past_projects';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblprojectcompleted')
            ->where('entity_id', '565')
            ->orderBy('projectcompletedid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->projectcompletedid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.36: delFutureInit 
    //      Function to delete future initiatives
    //-----------------------------------------------------
    public static function delFutureInit()
    {
        $data               = array();
        $data['section']    = 'future_initiatives';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblfuturegrowth')
            ->where('entity_id', '565')
            ->orderBy('futuregrowthid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->futuregrowthid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.37: delOwnerDetails 
    //      Function to delete owner details
    //-----------------------------------------------------
    public static function delOwnerDetails()
    {
        $data               = array();
        $data['section']    = 'owner_details';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblowner')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->ownerid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.38: delPersonalLoans
    //      Function to delete personal loans
    //-----------------------------------------------------
    public static function delPersonalLoans()
    {
        $data               = array();
        $data['section']    = 'personal_loans';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblpersonalloans')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->id;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.39: delKeyManager
    //      Function to delete Key Manager
    //-----------------------------------------------------
    public static function delKeyManager()
    {
        $data               = array();
        $data['section']    = 'key_manager';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblkeymanagers')
            ->where('entity_id', '565')
            ->orderBy('keymanagerid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->keymanagerid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.40: delOtherBusiness
    //      Function to delete other business
    //-----------------------------------------------------
    public static function delOtherBusiness()
    {
        $data               = array();
        $data['section']    = 'other_business';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblmanagebus')
            ->where('entity_id', '565')
            ->orderBy('managebusid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->managebusid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.42: delEducation
    //      Function to delete education
    //-----------------------------------------------------
    public static function delEducation()
    {
        $data               = array();
        $data['section']    = 'education';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tbleducation')
            ->where('entity_id', '565')
            ->orderBy('educationid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->educationid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.42: delSpouse
    //      Function to delete Spouse
    //-----------------------------------------------------
    public static function delSpouse()
    {
        $data               = array();
        $data['section']    = 'spouse';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblspouse')
            ->where('entity_id', '565')
            ->orderBy('spouseid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->spouseid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.43: delEcf
    //      function to delete existing credit
    //-----------------------------------------------------
    public static function delEcf()
    {
        $data               = array();
        $data['section']    = 'existing_credit';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblplanfacility')
            ->where('entity_id', '565')
            ->orderBy('planfacilityid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->planfacilityid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.44: delRcl 
    //      Function to delete required credit
    //-----------------------------------------------------
    public static function delRcl()
    {
        $data               = array();
        $data['section']    = 'required_credit';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblplanfacilityrequested')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->id;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.45: delBusinessCondition
    //      Function to delete business condition
    //-----------------------------------------------------
    public static function delBusinessCondtion()
    {
        $data               = array();
        $data['section']    = 'business_condition';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblsustainability')
            ->where('entity_id', '565')
            ->orderBy('sustainabilityid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->sustainabilityid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.46: delRiskMng
    //      Function to delete Risk Assessment
    //-----------------------------------------------------
    public static function delRiskMng()
    {
        $data               = array();
        $data['section']    = 'risk_assessment';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblriskassessment')
            ->where('entity_id', '565')
            ->orderBy('riskassessmentid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->riskassessmentid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.47: delGrowthPotential
    //      Function to delete growth potential
    //-----------------------------------------------------
    public static function delGrowthPotential()
    {
        $data               = array();
        $data['section']    = 'revenue_potential';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblrevenuegrowthpotential')
            ->where('entity_id', '565')
            ->orderBy('growthpotentialid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->growthpotentialid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.48: delExpansionInvestment
    //      Function to delete expansion investment
    //-----------------------------------------------------
    public static function delExpansionInvestment()
    {
        $data               = array();
        $data['section']    = 'expansion_investment';
        $data['section_id'] = 0;
        $data['action']     = 'delete';
        
        $del_data   = DB::table('tblcapacityexpansion')
            ->where('entity_id', '565')
            ->orderBy('capacityexpansionid', 'asc')
            ->first();
        
        if ($del_data) {
            $data['section_id'] = $del_data->capacityexpansionid;
        }
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.49: getBusinessDetails
    //      Function to get the Business Details
    //-----------------------------------------------------
    public static function getBusinessDetails()
    {
        $data               = array();
        $data['section']    = 'business_details';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.50: getAffiliates
    //      Function to get the Affiliates
    //-----------------------------------------------------
    public static function getAffiliates()
    {
        $data               = array();
        $data['section']    = 'related_companies';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.51: getProducts
    //      Function to get the products
    //-----------------------------------------------------
    public static function getProducts()
    {
        $data               = array();
        $data['section']    = 'products_services';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.52: getCapital
    //      function to get the capital
    //-----------------------------------------------------
    public static function getCapital()
    {
        $data               = array();
        $data['section']    = 'capital_details';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.53: getMainLocations
    //      function to get main locations
    //-----------------------------------------------------
    public static function getMainLocations()
    {
        $data               = array();
        $data['section']    = 'main_locations';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.54: getBranches
    //      function to get branches
    //-----------------------------------------------------
    public static function getBranches()
    {
        $data               = array();
        $data['section']    = 'branches';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.55: getShareholders
    //      Function to get shareholders
    //-----------------------------------------------------
    public static function getShareholders()
    {
        $data               = array();
        $data['section']    = 'shareholders';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.56: getDirectors
    //      function to get directors
    //-----------------------------------------------------
    public static function getDirectors()
    {
        $data               = array();
        $data['section']    = 'directors';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.57: getCertifications
    //      function to get certifications
    //-----------------------------------------------------
    public static function getCertifications()
    {
        $data               = array();
        $data['section']    = 'certifications';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.58: getImportExport
    //      function to get import and export
    //-----------------------------------------------------
    public static function getImportExport()
    {
        $data               = array();
        $data['section']    = 'business_type';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.59: getInsurance
    //      function to get the insurance
    //-----------------------------------------------------
    public static function getInsurance()
    {
        $data               = array();
        $data['section']    = 'insurance';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.60: getCashConversion
    //      function to get cash conversion
    //-----------------------------------------------------
    public static function getCashConversion()
    {
        $data               = array();
        $data['section']    = 'cash_cycle';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.61: getFsDocuments
    //      function to get financial statement documents
    //-----------------------------------------------------
    public static function getFsDocuments()
    {
        $data               = array();
        $data['section']    = 'fs_document';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.62: getBankStatements
    //      function to get bank statements
    //-----------------------------------------------------
    public static function getBankStatements()
    {
        $data               = array();
        $data['section']    = 'bank_statements';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.63: getUtilityBills
    //      function to get Utility Bills
    //-----------------------------------------------------
    public static function getUtilityBills()
    {
        $data               = array();
        $data['section']    = 'utility_bills';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.64: getMajorCustomer
    //      function to get Major Customer
    //-----------------------------------------------------
    public static function getMajorCustomer()
    {
        $data               = array();
        $data['section']    = 'major_customers';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.65: getMajorSupplier
    //      funcion to get Major Supplier
    //-----------------------------------------------------
    public static function getMajorSupplier()
    {
        $data               = array();
        $data['section']    = 'major_suppliers';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.66: getMajorLocations
    //      function to get Major locations
    //-----------------------------------------------------
    public static function getMajorLocations()
    {
        $data               = array();
        $data['section']    = 'major_locations';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.67: getCompetitors
    //      function to get competitors
    //-----------------------------------------------------
    public static function getCompetitors()
    {
        $data               = array();
        $data['section']    = 'competitors';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.68: getBizDriver
    //      function to get business drivers
    //-----------------------------------------------------
    public static function getBizDriver()
    {
        $data               = array();
        $data['section']    = 'business_drivers';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.69: getBizSegment
    //      function to get business segments
    //-----------------------------------------------------
    public static function getBizSegment()
    {
        $data               = array();
        $data['section']    = 'business_segments';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.70: getPastProj
    //      function to get past projects
    //-----------------------------------------------------
    public static function getPastProj()
    {
        $data               = array();
        $data['section']    = 'past_projects';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.71: getFutureInit
    //      function to get future initiatives
    //-----------------------------------------------------
    public static function getFutureInit()
    {
        $data               = array();
        $data['section']    = 'future_initiatives';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.72: getOwnerDetails
    //      function to get owner details
    //-----------------------------------------------------
    public static function getOwnerDetails()
    {
        $data               = array();
        $data['section']    = 'owner_details';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.73: getPersonaloans
    //      function get personal loans
    //-----------------------------------------------------
    public static function getPersonalLoans()
    {
        $data               = array();
        $data['section']    = 'personal_loans';
        $data['section_id'] = '2110009127';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.74: getOtherBusiness
    //      function to get other business
    //-----------------------------------------------------
    public static function getOtherBusiness()
    {
        $data               = array();
        $data['section']    = 'other_business';
        $data['section_id'] = '2110009127';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.75: getEducation
    //      function to get the education
    //-----------------------------------------------------
    public static function getEducation()
    {
        $data               = array();
        $data['section']    = 'education';
        $data['section_id'] = '2110009127';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.76: getSpouse
    //      function to get the spouse
    //-----------------------------------------------------
    public static function getSpouse()
    {
        $data               = array();
        $data['section']    = 'spouse';
        $data['section_id'] = '2110009127';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.77: getKeyManager
    //      function to get the Key Manager
    //-----------------------------------------------------
    public static function getKeyManager()
    {
        $data               = array();
        $data['section']    = 'key_manager';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.78: getEcf
    //      function to get the existing credit
    //-----------------------------------------------------
    public static function getEcf()
    {
        $data               = array();
        $data['section']    = 'existing_credit';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.79: getRcl
    //      function to get the required credit
    //-----------------------------------------------------
    public static function getRcl()
    {
        $data               = array();
        $data['section']    = 'required_credit';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.80: getBusinessCondition
    //      function to get the business condition
    //-----------------------------------------------------
    public static function getBusinessCondtion()
    {
        $data               = array();
        $data['section']    = 'business_condition';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.81: getRiskAssessment
    //      function to get the risk assessment
    //-----------------------------------------------------
    public static function getRiskAssessment()
    {
        $data               = array();
        $data['section']    = 'risk_assessment';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.82: getGrowthPotential
    //      function to get the growth potential  
    //-----------------------------------------------------
    public static function getGrowthPotential()
    {
        $data               = array();
        $data['section']    = 'revenue_potential';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.83: getInvestmentExpansion
    //      function to get the investment expansion
    //-----------------------------------------------------
    public static function getInvestmentExpansion()
    {
        $data               = array();
        $data['section']    = 'expansion_investment';
        $data['section_id'] = '565';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.84: getReports
    //      function to get Reports
    //-----------------------------------------------------
    public static function getReports()
    {
        $data               = array();
        $data['section']    = 'rating_report';
        $data['section_id'] = '254';
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.85: testPostServiceMapper
    //      
    //-----------------------------------------------------
    public static function testPostServiceMapper()
    {
        $test_data          = array();
        $input['service']   = Input::get('service');
        $input['valid']     = Input::get('valid');
        
        $email          = 'creditbpo.philippines@gmail.com';
        $pword          = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';
        
        $data           = self::$input['service']($input['valid']);
        $post_url       = $data['post_url'];
        $action         = $data['action'];
        $section        = $data['section'];
        $post_fields    = $data['post_fields'];
        
        /*
        $post_url   = URL::To('/cbpo_rest_api/sme_data/66');
        $email      = 'squall@seed.balamb.edu';
        $pword      = 'cbfdac6008f9cab4083784cbd1874f76618d2a97';
        */
        
        $input = array(
            'email'         => $email,
            'pword'         => $pword,
            'action'        => $action,
            'section'       => $section,
        );
        
        if ('upload' == $action) {
            foreach ($post_fields as $key => $val) {
                $input[$key]    = $val;
            }
        }
        else {
            $input['post_fields']   = $post_fields;
        }
        
        /*--------------------------------------------------------------------
        /*	Send POST request to Highcharts export Server via CuRL
        /*------------------------------------------------------------------*/
        /* Initializes the CuRL Library */
        $curl = curl_init();
        
        /* Sets the CuRL POST Request configuration  */
        curl_setopt($curl, CURLOPT_URL, $post_url);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        if ('upload' == $action) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $input);
        }
        else {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($input));
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        /* Execute CuRL Request */
        $result = curl_exec($curl);
        
        $output['sent_hdr'] = curl_getinfo($curl);
        
        $json_post  = json_encode(http_build_query($input));
        $json_exp   = explode('&', $json_post);
        
        foreach ($json_exp as $json_fld) {
            $post_fld = explode('=', $json_fld);
            
            $output['post_fld'][] = trim($post_fld[0], '"').' = '.trim($post_fld[1], '"');
        }
        
        $output['response'] = $result;
        
        /*  Closes the CuRL Library */
        curl_close($curl);
        
        echo json_encode($output);
    }
    
    //-----------------------------------------------------
    //  Function 19.86: addAffiliates
    //      function to add affiliates
    //-----------------------------------------------------
    public static function addAffiliates($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'related_companies';
        
        if (STS_OK == $valid) {
            $post_fields['related_company_name'][0]         = 'Company A';
            $post_fields['related_company_address1'][0]     = 'Gotham';
            $post_fields['related_company_address2'][0]     = 'Underground';
            $post_fields['related_company_province'][0]     = 'Manila';
            $post_fields['related_company_cityid'][0]       = '1720';
            $post_fields['related_company_zipcode'][0]      = '1013';
            $post_fields['related_company_phone'][0]        = '632 911 11 11';
            $post_fields['related_company_email'][0]        = 'person@companya.com';
        }
        else {
            $post_fields['related_company_name'][0]         = '';
            $post_fields['related_company_address1'][0]     = 'Gotham';
            $post_fields['related_company_address2'][0]     = 'Underground';
            $post_fields['related_company_province'][0]     = 'Manila';
            $post_fields['related_company_cityid'][0]       = '1720';
            $post_fields['related_company_zipcode'][0]      = '1013';
            $post_fields['related_company_phone'][0]        = '632 911 11 11';
            $post_fields['related_company_email'][0]        = 'person';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.87: addProducts
    //      function to add products
    //-----------------------------------------------------
    public static function addProducts($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'products_services';
        
        if (STS_OK == $valid) {
            $post_fields['servicesoffer_name'][0]           = 'Weapons of Mass Destruction';
            $post_fields['servicesoffer_targetmarket'][0]   = 'Terrorists';
            $post_fields['servicesoffer_share_revenue'][0]  = '77';
            $post_fields['servicesoffer_seasonality'][0]    = '2';
        }
        else {
            $post_fields['servicesoffer_name'][0]           = '';
            $post_fields['servicesoffer_targetmarket'][0]   = '';
            $post_fields['servicesoffer_share_revenue'][0]  = 'abcde';
            $post_fields['servicesoffer_seasonality'][0]    = '2';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.88: addCapitalDetails
    //      function to add capital details
    //-----------------------------------------------------
    public static function addCapitalDetails($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'capital_details';
        
        if (STS_OK == $valid) {
            $post_fields['capital_authorized']      = '1000000';
            $post_fields['capital_issued']          = '200000';
            $post_fields['capital_paid_up']         = '150000';
            $post_fields['capital_ordinary_shares'] = '10000';
            $post_fields['capital_par_value']       = '500';
        }
        else {
            $post_fields['capital_authorized']      = 'abcde';
            $post_fields['capital_issued']          = '';
            $post_fields['capital_paid_up']         = '55t';
            $post_fields['capital_ordinary_shares'] = '9999999999999999999';
            $post_fields['capital_par_value']       = '0';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.89: addMainLocations
    //      function to add main locations
    //-----------------------------------------------------
    public static function addMainLocation($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'main_locations';
        
        if (STS_OK == $valid) {
            $post_fields['location_size'][0]    = '1033';
            $post_fields['location_type'][0]    = 'ml_owned';
            $post_fields['location_map'][0]     = 'ml_residential_area';
            $post_fields['location_used'][0]    = 'ml_warehouse';
        }
        else {
            $post_fields['location_size'][0]    = 'aabcde';
            $post_fields['location_type'][0]    = '';
            $post_fields['location_map'][0]     = '';
            $post_fields['location_used'][0]    = 'ml_warehouse';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.90: addBranches
    //      function to add branches
    //-----------------------------------------------------
    public static function addBranches($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'branches';
        
        if (STS_OK == $valid) {
            $post_fields['branch_address'][0]           = 'The Black Hole';
            $post_fields['branch_owned_leased'][0]      = 'br_owned';
            $post_fields['branch_contact_person'][0]    = 'Branch Person';
            $post_fields['branch_phone'][0]             = '632 922 2222';
            $post_fields['branch_email'][0]             = 'branch@companya.com';
            $post_fields['branch_job_title'][0]         = 'Branch Manager';
        }
        else {
            $post_fields['branch_address'][0]           = '';
            $post_fields['branch_owned_leased'][0]      = 'br_owned';
            $post_fields['branch_contact_person'][0]    = 'Branch Person';
            $post_fields['branch_phone'][0]             = '632 922 2222';
            $post_fields['branch_email'][0]             = 'branch';
            $post_fields['branch_job_title'][0]         = 'Branch Manager';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.91: addShareholders
    //      function add shareholders
    //-----------------------------------------------------
    public static function addShareholders($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'shareholders';
        
        if (STS_OK == $valid) {
            $post_fields['name'][0]             = 'Shareholder A';
            $post_fields['address'][0]          = 'Mansion A';
            $post_fields['amount'][0]           = '77700000';
            $post_fields['percentage_share'][0] = '37';
            $post_fields['id_no'][0]            = '111-111-111';
            $post_fields['nationality'][0]      = 'Italian';
        }
        else {
            $post_fields['name'][0]             = '';
            $post_fields['address'][0]          = 'Mansion A';
            $post_fields['amount'][0]           = '77700000';
            $post_fields['percentage_share'][0] = '37';
            $post_fields['id_no'][0]            = '111-111-111';
            $post_fields['nationality'][0]      = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.92: addDirectors
    //      function to add directors
    //-----------------------------------------------------
    public static function addDirectors($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'directors';
        
        if (STS_OK == $valid) {
            $post_fields['name'][0]             = 'Director A';
            $post_fields['address'][0]          = 'Mansion B';
            $post_fields['percentage_share'][0] = '12';
            $post_fields['id_no'][0]            = '111-111-111';
            $post_fields['nationality'][0]      = 'Japanese';
        }
        else {
            $post_fields['name'][0]             = '';
            $post_fields['address'][0]          = 'Mansion B';
            $post_fields['percentage_share'][0] = 'abc';
            $post_fields['id_no'][0]            = '111111111';
            $post_fields['nationality'][0]      = 'Japanese';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.93: uploadCertification
    //      function to upload certification
    //-----------------------------------------------------
    public static function uploadCertification($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'certifications';
        
        if (STS_OK == $valid) {
            $post_fields['certification_details[0]']    = 'Board of Investments (BOI)';
            $post_fields['certification_reg_no[0]']     = '12345ABCDE';
            $post_fields['certification_reg_date[0]']   = '2016-07-01';
            $post_fields['certification_doc[0]']        = curl_file_create(realpath('sample.pdf'));
        }
        else {
            $post_fields['certification_details[0]']    = '';
            $post_fields['certification_reg_no[0]']     = '12345ABCDE';
            $post_fields['certification_reg_date[0]']   = '2016-07-01';
            $post_fields['certification_doc[0]']        = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.94: addImportExport
    //      function to add imports and exports
    //-----------------------------------------------------
    public static function addImportExport($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_type';
        
        if (STS_OK == $valid) {
            $post_fields['is_import']   = 1;
            $post_fields['is_export']   = 0;
        }
        else {
            $post_fields['is_import']   = '';
            $post_fields['is_export']   = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.95: addInsurance
    //      function to add Insurance
    //-----------------------------------------------------
    public static function addInsurance($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'insurance';
        
        if (STS_OK == $valid) {
            $post_fields['insurance_type'][0]   = 'Zombie Apocalypse';
            $post_fields['insured_amount'][0]   = 77700000;
        }
        else {
            $post_fields['insurance_type'][0]   = '';
            $post_fields['insured_amount'][0]   = 'abcde';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.96: addConversionCycle
    //      function to add conversion cycle
    //-----------------------------------------------------
    public static function addConversionCycle($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'cash_cycle';
        
        if (STS_OK == $valid) {
            $post_fields['credit_sales'][0]             = 77700000;
            $post_fields['accounts_receivables'][0]     = 777000;
            $post_fields['inventory'][0]                = 7770;
            $post_fields['cost_of_sales'][0]            = 777;
            $post_fields['accounts_payable'][0]         = 77700;
            
            $post_fields['credit_sales'][1]             = 88800000;
            $post_fields['accounts_receivables'][1]     = 888000;
            $post_fields['inventory'][1]                = 8880;
            $post_fields['cost_of_sales'][1]            = 888;
            $post_fields['accounts_payable'][1]         = 88800;
            
            $post_fields['credit_sales'][2]             = 99900000;
            $post_fields['accounts_receivables'][2]     = 999000;
            $post_fields['inventory'][2]                = 9990;
            $post_fields['cost_of_sales'][2]            = 999;
            $post_fields['accounts_payable'][2]         = 99900;
        }
        else {
            $post_fields['credit_sales'][0]             = 77700000;
            $post_fields['accounts_receivables'][0]     = 'abcde';
            $post_fields['inventory'][0]                = 7770;
            $post_fields['cost_of_sales'][0]            = 777;
            $post_fields['accounts_payable'][0]         = 77700;
            
            $post_fields['credit_sales'][1]             = 'abcde';
            $post_fields['accounts_receivables'][1]     = 'abcde';
            $post_fields['inventory'][1]                = 'abcde';
            $post_fields['cost_of_sales'][1]            = 888;
            $post_fields['accounts_payable'][1]         = 'abcde';
            
            $post_fields['credit_sales'][2]             = 99900000;
            $post_fields['accounts_receivables'][2]     = 'abcde';
            $post_fields['inventory'][2]                = 9990;
            $post_fields['cost_of_sales'][2]            = 999;
            $post_fields['accounts_payable'][2]         = 'abcde';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.97: uploadFsTemplate
    //      function to upload financial statement template
    //-----------------------------------------------------
    public static function uploadFsTemplate($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'fs_document';
        
        if (STS_OK == $valid) {
            $post_fields['document_type[0]']    = '1';
            $post_fields['upload_type[0]']      = '5';
            $post_fields['fs_file[0]']          = curl_file_create(realpath('images/DBM_FS_TEMPLATE.xlsx'));
        }
        else {
            $post_fields['document_type[0]']    = '1';
            $post_fields['upload_type[0]']      = '5';
            $post_fields['fs_file[0]']          = curl_file_create(realpath('sample.pdf'));
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.98: uploadDocument
    //      function to upload FS document
    //-----------------------------------------------------
    public static function uploadDocument($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'fs_document';
        
        if (STS_OK == $valid) {
            $post_fields['document_type[0]']    = '2';
            $post_fields['upload_type[0]']      = '3';
            $post_fields['period_from[0]']      = '08/23/2016';
            $post_fields['period_to[0]']        = '09/23/2016';
            $post_fields['fs_file[0]']          = curl_file_create(realpath('sample.pdf'));
        }
        else {
            $post_fields['document_type[0]']    = '2';
            $post_fields['upload_type[0]']      = '3';
            $post_fields['period_from[0]']      = '';
            $post_fields['period_to[0]']        = '';
            $post_fields['fs_file[0]']          = curl_file_create(realpath('sample.pdf'));
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.99: uploadBankStatement
    //      function to upload bank statement
    //-----------------------------------------------------
    public static function uploadBankStatement($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'bank_statements';
        
        if (STS_OK == $valid) {
            $post_fields['as_of[0]']            = '08/20/2016';
            $post_fields['bank_statements[0]']  = curl_file_create(realpath('sample.pdf'));
        }
        else {
            $post_fields['as_of[0]']            = '';
            $post_fields['bank_statements[0]']  = curl_file_create(realpath('sample.pdf'));
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.100: uploadUtilityBills
    //      function to upload utility bills
    //-----------------------------------------------------
    public static function uploadUtilityBills($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'utility_bills';
        
        if (STS_OK == $valid) {
            $post_fields['as_of[0]']            = '08/20/2016';
            $post_fields['utility_bills[0]']    = curl_file_create(realpath('sample.pdf'));
        }
        else {
            $post_fields['as_of[0]']            = '';
            $post_fields['utility_bills[0]']    = curl_file_create(realpath('sample.pdf'));
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.101: addMajorCustomer
    //      function to add Major Customers
    //-----------------------------------------------------
    public static function addMajorCustomer($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'major_customers';
        
        if (STS_OK == $valid) {
            $post_fields['customer_name'][0]                    = 'Customer A';
            $post_fields['customer_address'][0]                 = 'Classified';
            $post_fields['customer_contact_person'][0]          = 'Mr Customer';
            $post_fields['customer_phone'][0]                   = '632 933 3333';
            $post_fields['customer_email'][0]                   = 'customera@companyab.com';
            $post_fields['customer_started_years'][0]           = '1999';
            $post_fields['customer_years_doing_business'][0]    = '17';
            $post_fields['customer_share_sales'][0]             = '3';
            $post_fields['customer_order_frequency'][0]         = 'mcof_often';
            $post_fields['customer_settlement'][0]              = '4';
        }
        else {
            $post_fields['customer_name'][0]                    = '';
            $post_fields['customer_address'][0]                 = 'Classified';
            $post_fields['customer_contact_person'][0]          = 'Mr Customer';
            $post_fields['customer_phone'][0]                   = '632 933 3333';
            $post_fields['customer_email'][0]                   = 'customera.com';
            $post_fields['customer_started_years'][0]           = 'eee';
            $post_fields['customer_years_doing_business'][0]    = 'abc';
            $post_fields['customer_share_sales'][0]             = '3';
            $post_fields['customer_order_frequency'][0]         = 'mcof_often';
            $post_fields['customer_settlement'][0]              = '4';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.102: addMajorSupplier
    //      function to add major suppliers
    //-----------------------------------------------------
    public static function addMajorSupplier($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'major_suppliers';
        
        if (STS_OK == $valid) {
            $post_fields['supplier_name'][0]                    = 'The Joker';
            $post_fields['supplier_address'][0]                 = 'Gotham City';
            $post_fields['supplier_contact_person'][0]          = 'The Joker';
            $post_fields['supplier_phone'][0]                   = 'Joker Card';
            $post_fields['supplier_email'][0]                   = 'thejoker@gotham.net';
            $post_fields['supplier_started_years'][0]           = '2006';
            $post_fields['supplier_years_doing_business'][0]    = '10';
            $post_fields['supplier_share_sales'][0]             = '12';
            $post_fields['supplier_order_frequency'][0]         = 'ms_daily';
            $post_fields['supplier_settlement'][0]              = '1';
            $post_fields['average_monthly_volume'][0]           = '34534534';
            $post_fields['item_1'][0]                           = 'Item 1';
            $post_fields['item_2'][0]                           = 'Item 2';
            $post_fields['item_3'][0]                           = '';
        }
        else {
            $post_fields['supplier_name'][0]                    = '';
            $post_fields['supplier_address'][0]                 = 'Gotham City';
            $post_fields['supplier_contact_person'][0]          = 'The Joker';
            $post_fields['supplier_phone'][0]                   = 'Joker Card';
            $post_fields['supplier_email'][0]                   = 'thejoker.net';
            $post_fields['supplier_started_years'][0]           = 'aaaa';
            $post_fields['supplier_years_doing_business'][0]    = 'vcd';
            $post_fields['supplier_share_sales'][0]             = '12';
            $post_fields['supplier_order_frequency'][0]         = 'ms_daily';
            $post_fields['supplier_settlement'][0]              = '1';
            $post_fields['average_monthly_volume'][0]           = 'abcde';
            $post_fields['item_1'][0]                           = '';
            $post_fields['item_2'][0]                           = '';
            $post_fields['item_3'][0]                           = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.103: addMajorLocation
    //      function to add Major Locations
    //-----------------------------------------------------
    public static function addMajorLocation($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'major_locations';
        
        if (STS_OK == $valid) {
            $post_fields['marketlocation'][0]   = '10';
        }
        else {
            $post_fields['marketlocation'][0]   = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.104: addCompetitors
    //      function to add Competitors
    //-----------------------------------------------------
    public static function addCompetitors($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'competitors';
        
        if (STS_OK == $valid) {
            $post_fields['competitor_name'][0]      = 'Competitor A';
            $post_fields['competitor_address'][0]   = 'Competitor Building';
            $post_fields['competitor_phone'][0]     = '632 944 4444';
            $post_fields['competitor_email'][0]     = 'competitor@companyc.com';
            $post_fields['master_key'][0]           = '333';
        }
        else {
            $post_fields['competitor_name'][0]      = '';
            $post_fields['competitor_address'][0]   = '';
            $post_fields['competitor_phone'][0]     = '632 944 4444';
            $post_fields['competitor_email'][0]     = 'competitor';
            $post_fields['master_key'][0]           = '333';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.105: addBusinessDrivers 
    //      function to add business drivers
    //-----------------------------------------------------
    public static function addBusinessDrivers($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_drivers';
        
        if (STS_OK == $valid) {
            $post_fields['businessdriver_name'][0]          = 'Driver A';
            $post_fields['businessdriver_total_sales'][0]   = '37';
        }
        else {
            $post_fields['businessdriver_name'][0]          = '';
            $post_fields['businessdriver_total_sales'][0]   = 'abcde';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.106: addBusinessSegments
    //      function to add business segments
    //-----------------------------------------------------
    public static function addBusinessSegments($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_segments';
        
        if (STS_OK == $valid) {
            $post_fields['businesssegment_name'][0]         = 'Segment A';
        }
        else {
            $post_fields['businesssegment_name'][0]         = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.107: addPastProject
    //      function to add past projects
    //-----------------------------------------------------
    public static function addPastProject($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'past_projects';
        
        if (STS_OK == $valid) {
            $post_fields['projectcompleted_name'][0]            = 'Project A';
            $post_fields['projectcompleted_cost'][0]            = '450000000';
            $post_fields['projectcompleted_source_funding'][0]  = 'ppc_source_ipo';
            $post_fields['projectcompleted_year_began'][0]      = '2005';
            $post_fields['projectcompleted_result'][0]          = 'ppc_result_s';
        }
        else {
            $post_fields['projectcompleted_name'][0]            = '';
            $post_fields['projectcompleted_cost'][0]            = 'abcde';
            $post_fields['projectcompleted_source_funding'][0]  = 'ppc_source_ipo';
            $post_fields['projectcompleted_year_began'][0]      = '12';
            $post_fields['projectcompleted_result'][0]          = 'ppc_result_s';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.108: addFutureGrowth
    //      function to add future growth
    //-----------------------------------------------------
    public static function addFutureGrowth($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'future_initiatives';
        
        if (STS_OK == $valid) {
            $post_fields['futuregrowth_name'][0]                    = 'Project A';
            $post_fields['futuregrowth_estimated_cost'][0]          = '450000000';
            $post_fields['futuregrowth_implementation_date'][0]     = '2017-02-15';
            $post_fields['futuregrowth_source_capital'][0]          = 'fgi_source_partners';
            $post_fields['planned-proj-goal'][0]                    = 'fgi_increase_sales';
            $post_fields['planned-goal-increase'][0]                = '0-5';
            $post_fields['proj-benefit-date'][0]                    = '2017-03';
        }
        else {
            $post_fields['futuregrowth_name'][0]                    = '';
            $post_fields['futuregrowth_estimated_cost'][0]          = 'abcde';
            $post_fields['futuregrowth_implementation_date'][0]     = '12345';
            $post_fields['futuregrowth_source_capital'][0]          = 'fgi_source_partners';
            $post_fields['planned-proj-goal'][0]                    = 'fgi_increase_sales';
            $post_fields['planned-goal-increase'][0]                = '0-5';
            $post_fields['proj-benefit-date'][0]                    = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.109: addCompanySuccession
    //      function to add company succession
    //-----------------------------------------------------
    public static function addCompanySuccession($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_condition';
        
        if (STS_OK == $valid) {
            $post_fields['succession_plan']			= '2';
            $post_fields['succession_timeframe']	= '3';
            $post_fields['condition_type']			= 'succession';
        }
        else {
            $post_fields['succession_plan']			= '';
            $post_fields['succession_timeframe']	= '';
            $post_fields['condition_type']			= 'succession';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.110: addCompetitiveLandscape
    //      function to add competitive landscape
    //-----------------------------------------------------
    public static function addCompetitiveLandscape($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_condition';
        
        if (STS_OK == $valid) {
            $post_fields['competition_landscape']       = '2';
            $post_fields['condition_type']              = 'competitionlandscape';
        }
        else {
            $post_fields['competition_landscape']       = '';
            $post_fields['condition_type']              = 'competitionlandscape';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.111: addEconomicFactor
    //      Function to add economic factor
    //-----------------------------------------------------
    public static function addEconomicFactor($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_condition';
        
        if (STS_OK == $valid) {
            $post_fields['ev_susceptibility_economic_recession']		= '2';
            $post_fields['ev_foreign_exchange_interest_sensitivity']	= '1';
            $post_fields['ev_commodity_price_volatility']				= '3';
            $post_fields['ev_governement_regulation']					= '1';
            $post_fields['condition_type']								= 'economicfactors';
        }
        else {
            $post_fields['ev_susceptibility_economic_recession']		= '';
            $post_fields['ev_foreign_exchange_interest_sensitivity']	= '';
            $post_fields['ev_commodity_price_volatility']				= '';
            $post_fields['ev_governement_regulation']					= '';
            $post_fields['condition_type']								= 'economicfactors';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.112: addTaxPayments
    //      function to add tax payments    
    //-----------------------------------------------------
    public static function addTaxPayments($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'business_condition';
        
        if (STS_OK == $valid) {
            $post_fields['tax_payments']		= '3';
            $post_fields['condition_type']		= 'taxpayments';
        }
        else {
            $post_fields['tax_payments']		= '';
            $post_fields['condition_type']		= 'taxpayments';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.113: addRiskAssement
    //      function to add risk assessments
    //-----------------------------------------------------
    public static function addRiskAssement($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'risk_assessment';
        
        if (STS_OK == $valid) {
            $post_fields['risk_assessment_name'][0]		= 'threat_of_substitute';
            $post_fields['risk_assessment_solution'][0]	= 'product_innovation';
            
            $post_fields['risk_assessment_name'][1]		= 'rivalry_intensity';
            $post_fields['risk_assessment_solution'][1]	= 'cost_leadership';
            
            $post_fields['risk_assessment_name'][2]		= 'buyer_bargainer_pow';
            $post_fields['risk_assessment_solution'][2]	= 'focus';
        }
        else {
            $post_fields['risk_assessment_name'][0]		= '';
            $post_fields['risk_assessment_solution'][0]	= 'product_innovation';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.114: addRevenueGrowth
    //      function to add revenue growth
    //-----------------------------------------------------
    public static function addRevenueGrowth($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'revenue_potential';
        
        if (STS_OK == $valid) {
            $post_fields['current_market'][0]	= '2';
            $post_fields['new_market'][0]		= '4';
        }
        else {
            $post_fields['current_market'][0]	= '';
            $post_fields['new_market'][0]		= '4';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.115: addCapitalExpansion
    //      function to add capital expansion  
    //-----------------------------------------------------
    public static function addCapitalExpansion($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'expansion_investment';
        
        if (STS_OK == $valid) {
            $post_fields['current_year'][0]	= '20000';
            $post_fields['new_year'][0]		= '40000';
        }
        else {
            $post_fields['current_year'][0]	= '';
            $post_fields['new_year'][0]		= 'abcde';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.116: addRequiredCredit 
    //      function to add required credit
    //-----------------------------------------------------
    public static function addRequiredCredit($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'required_credit';
        
        if (STS_OK == $valid) {
            $post_fields['plan_credit_availment']           = '25234234234';
            $post_fields['purpose_credit_facility']         = '1';
            $post_fields['purpose_credit_facility_others']  = '';
            $post_fields['credit_terms']                    = '';
            $post_fields['other_remarks']                   = 'Good doing business with';
            $post_fields['cash_deposit_details']            = 'CA';
            $post_fields['cash_deposit_amount']             = '10000';
            $post_fields['securities_details']              = 'BPI Stocks';
            $post_fields['securities_estimated_value']      = '23423423';
            $post_fields['property_details']                = 'Really Big Island';
            $post_fields['property_estimated_value']        = '10000000';
            $post_fields['chattel_details']                 = 'Good';
            $post_fields['chattel_estimated_value']         = '34444444';
            $post_fields['others_details']                  = 'Other';
            $post_fields['others_estimated_value']          = '123123123';
        }
        else {
            $post_fields['plan_credit_availment']           = '25234234234';
            $post_fields['purpose_credit_facility']         = '1';
            $post_fields['purpose_credit_facility_others']  = '';
            $post_fields['credit_terms']                    = '';
            $post_fields['other_remarks']                   = 'Good doing business with';
            $post_fields['cash_deposit_details']            = 'CA';
            $post_fields['cash_deposit_amount']             = '10000';
            $post_fields['securities_details']              = 'BPI Stocks';
            $post_fields['securities_estimated_value']      = 'abbcde';
            $post_fields['property_details']                = 'Really Big Island';
            $post_fields['property_estimated_value']        = '';
            $post_fields['chattel_details']                 = 'Good';
            $post_fields['chattel_estimated_value']         = '';
            $post_fields['others_details']                  = 'Other';
            $post_fields['others_estimated_value']          = 'aaa';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.117: addOwnerDetails
    //      function to add owner details
    //-----------------------------------------------------
    public static function addOwnerDetails($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'owner_details';
        
        if (STS_OK == $valid) {
            $post_fields['firstname']                   = 'Bruce';
            $post_fields['middlename']                  = 'Batman';
            $post_fields['lastname']                    = 'Wayne';
            $post_fields['nationality']                 = 'American';
            $post_fields['birthdate']                   = '1988-01-21';
            $post_fields['civilstatus']                 = '2';
            $post_fields['profession']                  = 'Vigilante';
            $post_fields['email']                       = 'bruce@wayne-enterprise.com';
            $post_fields['address1']                    = 'Gotham';
            $post_fields['address2']                    = '';
            $post_fields['cityid']                      = '2';
            $post_fields['province']                    = 'Makati';
            $post_fields['zipcode']                     = '1013';
            $post_fields['present_address_status']      = '1';
            $post_fields['no_yrs_present_address']      = '24';
            $post_fields['phone']                       = '632 955 5555';
            $post_fields['tin_num']                     = '111-111-111';
            $post_fields['percent_of_ownership']        = '32';
            $post_fields['number_of_year_engaged']      = '9';
            $post_fields['position']                    = 'CEO';
            
            $post_fields['bus_name'][0]                 = 'Business A';
            $post_fields['bus_type'][0]                 = 'Crime Fighting';
            $post_fields['bus_location'][0]             = '3';
            
            $post_fields['km_firstname']                = 'James';
            $post_fields['km_middlename']               = '007';
            $post_fields['km_lastname']                 = 'Bond';
            $post_fields['km_nationality']              = 'British';
            $post_fields['km_birthdate']                = '1988-09-22';
            $post_fields['km_civilstatus']              = '1';
            $post_fields['km_profession']               = 'Awoo Awoo Awoo';
            $post_fields['km_email']                    = 'bond@agent.com';
            $post_fields['km_address1']                 = 'Classified';
            $post_fields['km_address2']                 = '';
            $post_fields['km_cityid']                   = '3';
            $post_fields['km_province']                 = 'Manila';
            $post_fields['km_zipcode']                  = '1013';
            $post_fields['km_present_address_status']   = '1';
            $post_fields['km_no_yrs_present_address']   = '34';
            $post_fields['km_phone']                    = '632 966 7777';
            $post_fields['km_tin_num']                  = '222-222-222';
            $post_fields['km_percent_of_ownership']     = '3';
            $post_fields['km_number_of_year_engaged']   = '3';
            $post_fields['km_position']                 = 'Spy';
            
            $post_fields['school_name'][0]              = 'Far Easter University';
            $post_fields['educ_degree'][0]              = 'Graduated';
            $post_fields['educ_type'][0]                = '2';
            
            $post_fields['sfirstname']                  = 'Ruri';
            $post_fields['smiddlename']                 = '';
            $post_fields['slastname']                   = 'Saijo';
            $post_fields['snationality']                = 'Japanese';
            $post_fields['sbirthdate']                  = '1988-09-23';
            $post_fields['sprofession']                 = 'Super Hero';
            $post_fields['emailspouse']                 = 'saijo@ruri.com';
            $post_fields['sphone']                      = '632 977 7777';
            $post_fields['stin_num']                    = '333-333-333';
            
            $post_fields['sbus_name'][0]                = 'Business B';
            $post_fields['sbus_type'][0]                = 'Laundry';
            $post_fields['sbus_location'][0]            = '4';
            
            $post_fields['sschool_name'][0]             = 'School of Martial Arts';
            $post_fields['seduc_degree'][0]             = 'Graduated';
            $post_fields['seduc_type'][0]               = '2';
        }
        else {
            $post_fields['firstname']                   = '';
            $post_fields['middlename']                  = '';
            $post_fields['lastname']                    = '';
            $post_fields['nationality']                 = '';
            $post_fields['birthdate']                   = '';
            $post_fields['civilstatus']                 = '2';
            $post_fields['profession']                  = '';
            $post_fields['email']                       = 'bruce@wayne-enterprise.com';
            $post_fields['address1']                    = 'Gotham';
            $post_fields['address2']                    = '';
            $post_fields['cityid']                      = '2';
            $post_fields['province']                    = 'Makati';
            $post_fields['zipcode']                     = '1013';
            $post_fields['present_address_status']      = '1';
            $post_fields['no_yrs_present_address']      = '24 years';
            $post_fields['phone']                       = '632 955 5555';
            $post_fields['tin_num']                     = '111111111';
            $post_fields['percent_of_ownership']        = 'abcde';
            $post_fields['number_of_year_engaged']      = 'asd';
            $post_fields['position']                    = 'CEO';
            
            $post_fields['bus_name'][0]                 = 'Business A';
            $post_fields['bus_type'][0]                 = 'Crime Fighting';
            $post_fields['bus_location'][0]             = '3';
            
            $post_fields['km_firstname']                = 'James';
            $post_fields['km_middlename']               = '007';
            $post_fields['km_lastname']                 = 'Bond';
            $post_fields['km_nationality']              = 'British';
            $post_fields['km_birthdate']                = '1988-09-22';
            $post_fields['km_civilstatus']              = '1';
            $post_fields['km_profession']               = 'Awoo Awoo Awoo';
            $post_fields['km_email']                    = 'bond@agent.com';
            $post_fields['km_address1']                 = 'Classified';
            $post_fields['km_address2']                 = '';
            $post_fields['km_cityid']                   = '3';
            $post_fields['km_province']                 = 'Manila';
            $post_fields['km_zipcode']                  = '1013';
            $post_fields['km_present_address_status']   = '1';
            $post_fields['km_no_yrs_present_address']   = '34';
            $post_fields['km_phone']                    = '632 966 7777';
            $post_fields['km_tin_num']                  = '222-222-222';
            $post_fields['km_percent_of_ownership']     = '3';
            $post_fields['km_number_of_year_engaged']   = '3';
            $post_fields['km_position']                 = 'Spy';
            
            $post_fields['school_name'][0]              = 'Far Easter University';
            $post_fields['educ_degree'][0]              = 'Graduated';
            $post_fields['educ_type'][0]                = '2';
            
            $post_fields['sfirstname']                  = 'Ruri';
            $post_fields['smiddlename']                 = '';
            $post_fields['slastname']                   = 'Saijo';
            $post_fields['snationality']                = 'Japanese';
            $post_fields['sbirthdate']                  = '1988-09-23';
            $post_fields['sprofession']                 = 'Super Hero';
            $post_fields['emailspouse']                 = 'saijo@ruri.com';
            $post_fields['sphone']                      = '632 977 7777';
            $post_fields['stin_num']                    = '333333333';
            
            $post_fields['sbus_name'][0]                = 'Business B';
            $post_fields['sbus_type'][0]                = 'Laundry';
            $post_fields['sbus_location'][0]            = '4';
            
            $post_fields['sschool_name'][0]             = 'School of Martial Arts';
            $post_fields['seduc_degree'][0]             = 'Graduated';
            $post_fields['seduc_type'][0]               = '2';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.118: addPersonalLoans
    //      function to add personal loans
    //-----------------------------------------------------
    public static function addPersonalLoans($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'personal_loans';
        
        if (STS_OK == $valid) {
            $post_fields['purpose'][0]                  = 'Auto';
            $post_fields['balance'][0]                  = '50000000';
            $post_fields['monthly_amortization'][0]     = '1000000';
            $post_fields['creditor'][0]                 = 'PNB';
            $post_fields['due_date'][0]                 = '2016-12-25';
            $post_fields['owner_id']                    = '2110009127';
        }
        else {
            $post_fields['purpose'][0]                  = '';
            $post_fields['balance'][0]                  = 'abcde';
            $post_fields['monthly_amortization'][0]     = '100 pesos';
            $post_fields['creditor'][0]                 = '';
            $post_fields['due_date'][0]                 = '2016';
            $post_fields['owner_id']                    = '2110009127';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.119: addKeyManager
    //      function to add Key Manager
    //-----------------------------------------------------
    public static function addKeyManager($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'key_manager';
        
        if (STS_OK == $valid) {
            $post_fields['owner_id']                = '2110009127'; // 0 if Business is Corporation
            $post_fields['firstname']               = 'Jack';
            $post_fields['middlename']              = 'the';
            $post_fields['lastname']                = 'Black';
            $post_fields['nationality']             = 'Canadian';
            $post_fields['birthdate']               = '1988-09-25';
            $post_fields['civilstatus']             = '2';
            $post_fields['profession']              = 'Celebrity';
            $post_fields['email']                   = 'jack@black.com';
            $post_fields['address1']                = 'Awesome town';
            $post_fields['address2']                = '';
            $post_fields['cityid']                  = '2';
            $post_fields['province']                = 'Manila';
            $post_fields['zipcode']                 = '1013';
            $post_fields['present_address_status']  = '2';
            $post_fields['no_yrs_present_address']  = '23';
            $post_fields['phone']                   = '632 988 8888';
            $post_fields['tin_num']                 = '444-444-444';
            $post_fields['percent_of_ownership']    = '8';
            $post_fields['number_of_year_engaged']  = '2';
            $post_fields['position']                = 'Mechanic';
        }
        else {
            $post_fields['owner_id']                = '2110009127'; // 0 if Business is Corporation
            $post_fields['firstname']               = '';
            $post_fields['middlename']              = '';
            $post_fields['lastname']                = '';
            $post_fields['nationality']             = '';
            $post_fields['birthdate']               = '1234';
            $post_fields['civilstatus']             = '2';
            $post_fields['profession']              = 'Celebrity';
            $post_fields['email']                   = 'jack.com';
            $post_fields['address1']                = 'Awesome town';
            $post_fields['address2']                = '';
            $post_fields['cityid']                  = '2';
            $post_fields['province']                = 'Manila';
            $post_fields['zipcode']                 = '1013';
            $post_fields['present_address_status']  = '2';
            $post_fields['no_yrs_present_address']  = '23';
            $post_fields['phone']                   = '632 988 8888';
            $post_fields['tin_num']                 = '444444444';
            $post_fields['percent_of_ownership']    = 'abc';
            $post_fields['number_of_year_engaged']  = 'def';
            $post_fields['position']                = 'Mechanic';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.120: addOwnerOtherBusiness
    //      function to add other business of the owner
    //-----------------------------------------------------
    public static function addOwnerOtherBusiness($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'other_business';
        
        if (STS_OK == $valid) {
            $post_fields['bus_name'][0]             = 'Auto';
            $post_fields['bus_type'][0]             = 'Very Good Business';
            $post_fields['bus_location'][0]         = '3';
            $post_fields['owner_id']                = '2110009127';
            $post_fields['owner_type']              = '1';
        }
        else {
            $post_fields['bus_name'][0]             = '';
            $post_fields['bus_type'][0]             = '';
            $post_fields['bus_location'][0]         = '3';
            $post_fields['owner_id']                = '2110009127';
            $post_fields['owner_type']              = '1';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.121:addSpouseOtherBusiness
    //      function to add other business of spouse
    //-----------------------------------------------------
    public static function addSpouseOtherBusiness($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'other_business';
        
        if (STS_OK == $valid) {
            $post_fields['bus_name'][0]             = 'Auto';
            $post_fields['bus_type'][0]             = 'Very Good Business';
            $post_fields['bus_location'][0]         = '3';
            $post_fields['owner_id']                = '2144103771';
            $post_fields['owner_type']              = '2';
        }
        else {
            $post_fields['bus_name'][0]             = '';
            $post_fields['bus_type'][0]             = '';
            $post_fields['bus_location'][0]         = '3';
            $post_fields['owner_id']                = '2144103771';
            $post_fields['owner_type']              = '2';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.122: addOwnerEducation
    //      function to add education of owner
    //-----------------------------------------------------
    public static function addOwnerEducation($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'education';
        
        if (STS_OK == $valid) {
            $post_fields['school_name'][0]          = 'FEU - East Asia College';
            $post_fields['educ_degree'][0]          = 'Delayed';
            $post_fields['educ_type'][0]            = '3';
            $post_fields['owner_id']                = '2110009127';
            $post_fields['owner_type']              = '1';
        }
        else {
            $post_fields['school_name'][0]          = '';
            $post_fields['educ_degree'][0]          = '';
            $post_fields['educ_type'][0]            = '';
            $post_fields['owner_id']                = '2110009127';
            $post_fields['owner_type']              = '1';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.123: addSpouseEducation
    //      Function to add education of owner's spouse
    //-----------------------------------------------------
    public static function addSpouseEducation($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'education';
        
        if (STS_OK == $valid) {
            $post_fields['school_name'][0]          = 'College of Kung Fu';
            $post_fields['educ_degree'][0]          = 'Delayed';
            $post_fields['educ_type'][0]            = '3';
            $post_fields['owner_id']                = '2144103771';
            $post_fields['owner_type']              = '2';
        }
        else {
            $post_fields['school_name'][0]          = '';
            $post_fields['educ_degree'][0]          = '';
            $post_fields['educ_type'][0]            = '';
            $post_fields['owner_id']                = '2144103771';
            $post_fields['owner_type']              = '2';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.124: addExisitingCredit
    //      function to add existing credit
    //-----------------------------------------------------
    public static function addExistingCredit($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'add';
        $data['section']    = 'existing_credit';
        
        if (STS_OK == $valid) {
            $post_fields['lending_institution']             = 'The Joker';
            $post_fields['plan_credit_availment']           = '25234234234';
            $post_fields['purpose_credit_facility']         = '1';
            $post_fields['purpose_credit_facility_others']  = '';
            $post_fields['credit_terms']                    = '';
            $post_fields['outstanding_balance']             = '1000000';
            $post_fields['experience']                      = '1';
            $post_fields['other_remarks']                   = 'Good doing business with';
            $post_fields['cash_deposit_details']            = 'CA';
            $post_fields['cash_deposit_amount']             = '10000';
            $post_fields['securities_details']              = 'BPI Stocks';
            $post_fields['securities_estimated_value']      = '23423423';
            $post_fields['property_details']                = 'Really Big Island';
            $post_fields['property_estimated_value']        = '10000000';
            $post_fields['chattel_details']                 = 'Good';
            $post_fields['chattel_estimated_value']         = '34444444';
            $post_fields['others_details']                  = 'Other';
            $post_fields['others_estimated_value']          = '123123123';
        }
        else {
            $post_fields['lending_institution']             = '';
            $post_fields['plan_credit_availment']           = '';
            $post_fields['purpose_credit_facility']         = '1';
            $post_fields['purpose_credit_facility_others']  = '';
            $post_fields['credit_terms']                    = '';
            $post_fields['outstanding_balance']             = '1000000';
            $post_fields['experience']                      = '1';
            $post_fields['other_remarks']                   = 'Good doing business with';
            $post_fields['cash_deposit_details']            = 'CA';
            $post_fields['cash_deposit_amount']             = 'abcde';
            $post_fields['securities_details']              = 'BPI Stocks';
            $post_fields['securities_estimated_value']      = '';
            $post_fields['property_details']                = 'Really Big Island';
            $post_fields['property_estimated_value']        = '10000000';
            $post_fields['chattel_details']                 = 'Good';
            $post_fields['chattel_estimated_value']         = '34444444';
            $post_fields['others_details']                  = 'Other';
            $post_fields['others_estimated_value']          = '123123123';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.125: getQuestionnaireConfig
    //      function to get questionnaire configuration
    //-----------------------------------------------------
    public static function getQuestionnaireConfig($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'questionnaire_config';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['entity_id']             = '565';
        }
        else {
            $post_fields['entity_id']             = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.126: getReportCompletion
    //      function to get rating report  
    //-----------------------------------------------------
    public static function getReportCompletion($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'check_requirements';
        $data['section']    = 'rating_report';
        
        if (STS_OK == $valid) {
            $post_fields['entity_id']             = '565';
        }
        else {
            $post_fields['entity_id']             = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.127: editBusinessDetails
    //      function to edit business details
    //-----------------------------------------------------
	public static function editBusinessDetails($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'upload';
        $data['section']    = 'business_details';
        
        if (STS_OK == $valid) {
            $input['companyname']                   = 'Company AA';
			$input['entity_type']                   = '1';
			$input['company_tin']                   = '123123123';
			$input['tin_num']                       = '2342342';
			$input['sec_reg_date']                  = '2016-08-01';

		}
        else {
            $input['companyname']                   = '';
			$input['entity_type']                   = '';
			$input['company_tin']                   = '';
			$input['tin_num']                       = '';
			$input['sec_reg_date']                  = '';
        }
		
		$input['industrymain']                  = '2';
		$input['industrysub']                   = '2';
		$input['industryrow']                   = '2';
		$input['total_asset_grouping']          = '1';
		$input['total_assets']                  = '777700000';
		$input['emailalt']                      = 'ruffy.collado@creditbpo.com';
		$input['website']                       = '';
		$input['street']                        = 'Hermosa';
		$input['province']                      = 'Manila';
		$input['city']                          = '1703';
		$input['zipcode']                       = '1013';
		$input['phone']                         = '632 999 9999';
		$input['no_yrs_present_address']        = '23';
		$input['dateestablished']               = '2016-09-23';
		$input['numberyear']                    = '32';
		$input['description']                   = 'Hello People';
		$input['employeesize']                  = '23';
		$input['updated_date']                  = '2016-09-22';
		$input['number_year_management_team']   = '4';
		$input['requested_by']                  = '0';
		$input['bank']                          = '0';
		$input['sec_generation']        		= curl_file_create(realpath('sample.pdf'));
		$input['sec_cert']              		= curl_file_create(realpath('sample.pdf'));
		$input['permit_to_operate']    			= curl_file_create(realpath('sample.pdf'));
		$input['income_tax']            		= curl_file_create(realpath('sample.pdf'));
        
        $data['post_fields']    = $input;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.128: editAffiliates
    //      funtion to edit affiliates
    //-----------------------------------------------------
	public static function editAffiliates($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'related_companies';
        
        $upd_data   = DB::table('tblrelatedcompanies')
            ->where('entity_id', '565')
            ->orderBy('relatedcompaniesid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->relatedcompaniesid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['related_company_name'][0]         = 'Company AA';
            $post_fields['related_company_address1'][0]     = 'Gotham';
            $post_fields['related_company_address2'][0]     = 'Underground';
            $post_fields['related_company_province'][0]     = 'Manila';
            $post_fields['related_company_cityid'][0]       = '1720';
            $post_fields['related_company_zipcode'][0]      = '1013';
            $post_fields['related_company_phone'][0]        = '632 911 11 11';
            $post_fields['related_company_email'][0]        = 'person@companya.com';
        }
        else {
            $post_fields['related_company_name'][0]         = '';
            $post_fields['related_company_address1'][0]     = 'Gotham';
            $post_fields['related_company_address2'][0]     = 'Underground';
            $post_fields['related_company_province'][0]     = 'Manila';
            $post_fields['related_company_cityid'][0]       = '1720';
            $post_fields['related_company_zipcode'][0]      = '1013';
            $post_fields['related_company_phone'][0]        = '632 911 11 11';
            $post_fields['related_company_email'][0]        = 'person@companya.com';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.129: editProducts
    //      function to edit products
    //-----------------------------------------------------
	public static function editProducts($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'products_services';
        
        $upd_data   = DB::table('tblservicesoffer')
            ->where('entity_id', '565')
            ->orderBy('servicesofferid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->servicesofferid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['servicesoffer_name'][0]           = 'Weapons of Mass Destruction';
			$post_fields['servicesoffer_targetmarket'][0]   = 'Terrorists';
			$post_fields['servicesoffer_share_revenue'][0]  = '75';
			$post_fields['servicesoffer_seasonality'][0]    = '2';
        }
        else {
            $post_fields['servicesoffer_name'][0]           = '';
			$post_fields['servicesoffer_targetmarket'][0]   = 'Terrorists';
			$post_fields['servicesoffer_share_revenue'][0]  = '75';
			$post_fields['servicesoffer_seasonality'][0]    = '2';
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.130: editMainLocation
    //      function to edit main locations
    //-----------------------------------------------------
	public static function editMainLocation($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'main_locations';
        
        $upd_data   = DB::table('tbllocations')
            ->where('entity_id', '565')
            ->orderBy('locationid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->locationid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['location_size'][0]    = '1035';
			$post_fields['location_type'][0]    = 'ml_owned';
			$post_fields['location_map'][0]     = 'ml_residential_area';
			$post_fields['location_used'][0]    = 'ml_warehouse';
        }
        else {
            $post_fields['location_size'][0]    = '';
			$post_fields['location_type'][0]    = 'ml_owned';
			$post_fields['location_map'][0]     = 'ml_residential_area';
			$post_fields['location_used'][0]    = 'ml_warehouse';
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.131: editBranches
    //      function to edit branches
    //-----------------------------------------------------
	public static function editBranches($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'branches';
        
        $upd_data   = DB::table('tblbranches')
            ->where('entity_id', '565')
            ->orderBy('branchid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->branchid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['branch_address'][0]           = 'The White Hole';
			$post_fields['branch_owned_leased'][0]      = 'br_owned';
			$post_fields['branch_contact_person'][0]    = 'Branch Person';
			$post_fields['branch_phone'][0]             = '632 922 2222';
			$post_fields['branch_email'][0]             = 'branch@companya.com';
			$post_fields['branch_job_title'][0]         = 'Branch Manager';
        }
        else {
            $post_fields['branch_address'][0]           = '';
			$post_fields['branch_owned_leased'][0]      = 'br_owned';
			$post_fields['branch_contact_person'][0]    = 'Branch Person';
			$post_fields['branch_phone'][0]             = '632 922 2222';
			$post_fields['branch_email'][0]             = 'branch@companya.com';
			$post_fields['branch_job_title'][0]         = 'Branch Manager';
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.getProvinces: getProvinces
    //      function to get provinces
    //-----------------------------------------------------
    public static function getProvinces($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/province';
        $data['action']     = 'province';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['entity_id']             = '565';
        }
        else {
            $post_fields['entity_id']             = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.133: getCities
    //      function to get cities
    //-----------------------------------------------------
    public static function getCities($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/city';
        $data['action']     = 'city';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['province_name']   = 'Bohol';
        }
        else {
            $post_fields['province_name']   = 'Bohol';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.134: editShareholders
    //      function to edit Shareholders
    //-----------------------------------------------------
	public static function editShareholders($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'shareholders';
        
        $upd_data   = DB::table('tblshareholders')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->id;
        }
        
        if (STS_OK == $valid) {
            $post_fields['name'][0]             = 'Shareholder A';
			$post_fields['address'][0]          = 'Mansion A';
			$post_fields['amount'][0]           = '77700000';
			$post_fields['percentage_share'][0] = '37';
			$post_fields['id_no'][0]            = '111-111-111';
			$post_fields['nationality'][0]      = 'Italiano';
        }
        else {
            $post_fields['name'][0]             = '';
			$post_fields['address'][0]          = 'Mansion A';
			$post_fields['amount'][0]           = '77700000';
			$post_fields['percentage_share'][0] = '37';
			$post_fields['id_no'][0]            = '111-111-111';
			$post_fields['nationality'][0]      = 'Italiano';
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.135: editDirectors
    //      function to edit directors
    //-----------------------------------------------------
	public static function editDirectors($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'directors';
        
        $upd_data   = DB::table('tbldirectors')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->id;
        }
        
        if (STS_OK == $valid) {
            $post_fields['name'][0]             = 'Director A';
			$post_fields['address'][0]          = 'Mansion B';
			$post_fields['percentage_share'][0] = '12';
			$post_fields['id_no'][0]            = '111-111-111';
			$post_fields['nationality'][0]      = 'Japanese';
        }
        else {
            $post_fields['name'][0]             = '';
			$post_fields['address'][0]          = 'Mansion B';
			$post_fields['percentage_share'][0] = '12';
			$post_fields['id_no'][0]            = '111-111-111';
			$post_fields['nationality'][0]      = 'Japanese';
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.136: editImportExport
    //      function to edit import and export
    //-----------------------------------------------------
	public static function editImportExport($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'business_type';
        
        $upd_data   = DB::table('tblbusinesstype')
            ->where('entity_id', '565')
            ->orderBy('businesstypeid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->businesstypeid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['is_import']       = 1;
			$post_fields['is_export']       = 0;
        }
        else {
            $post_fields['is_import']       = '';
			$post_fields['is_export']       = 0;
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.137: editInsurance
    //      function to edit insurance
    //-----------------------------------------------------
	public static function editInsurance($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'insurance';
        
        $upd_data   = DB::table('tblinsurance')
            ->where('entity_id', '565')
            ->orderBy('insuranceid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->insuranceid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['insurance_type'][0]   = 'Zombie Apocalypse';
			$post_fields['insured_amount'][0]   = 77700000;
        }
        else {
            $post_fields['insurance_type'][0]   = '';
			$post_fields['insured_amount'][0]   = 77700000;
		}
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.138: editMajorCustomer
    //      function to edit major customer
    //-----------------------------------------------------
	public static function editMajorCustomer($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'major_customers';
        
        $upd_data   = DB::table('tblmajorcustomer')
            ->where('entity_id', '565')
            ->orderBy('majorcustomerrid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->majorcustomerrid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['customer_name'][0]                    = 'Customer AA';
        }
        else {
            $post_fields['customer_name'][0]                    = '';
		}
        
        $post_fields['customer_address'][0]                 = 'Classified';
        $post_fields['customer_contact_person'][0]          = 'Mr Customer';
        $post_fields['customer_phone'][0]                   = '632 933 3333';
        $post_fields['customer_email'][0]                   = 'customera@companyab.com';
        $post_fields['customer_started_years'][0]           = '1999';
        $post_fields['customer_years_doing_business'][0]    = '17';
        $post_fields['customer_share_sales'][0]             = '3';
        $post_fields['customer_order_frequency'][0]         = 'mcof_often';
        $post_fields['customer_settlement'][0]              = '4';
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.139: editMajorSupplier
    //      function to edit Major Suppliers
    //-----------------------------------------------------
	public static function editMajorSupplier($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'major_suppliers';
        
        $upd_data   = DB::table('tblmajorsupplier')
            ->where('entity_id', '565')
            ->orderBy('majorsupplierid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->majorsupplierid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['supplier_name'][0]                = 'Supplier 1';
            $post_fields['supplier_address'][0]             = 'Gotham City';
        }
        else {
            $post_fields['supplier_name'][0]                = 'Supplier 1';
            $post_fields['supplier_address'][0]             = '';
		}
		
        $post_fields['supplier_contact_person'][0]          = 'The Joker';
        $post_fields['supplier_phone'][0]                   = 'Joker Card';
        $post_fields['supplier_email'][0]                   = 'thejoker@gotham.net';
        $post_fields['supplier_started_years'][0]           = '2006';
        $post_fields['supplier_years_doing_business'][0]    = '10';
        $post_fields['supplier_share_sales'][0]             = '12';
        $post_fields['supplier_order_frequency'][0]         = 'ms_daily';
        $post_fields['supplier_settlement'][0]              = '1';
        $post_fields['average_monthly_volume'][0]           = '34534534';
        $post_fields['item_1'][0]                           = 'Hello';
        $post_fields['item_2'][0]                           = 'World';
        $post_fields['item_3'][0]                           = 'Bye';
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.140: editMajorLocation
    //      function to edit Major Locations
    //-----------------------------------------------------
	public static function editMajorLocation($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'major_locations';
        
        $upd_data   = DB::table('tblmajorlocation')
            ->where('entity_id', '565')
            ->orderBy('marketlocationid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->marketlocationid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['marketlocation'][0]   = '12';
        }
        else {
            $post_fields['marketlocation'][0]   = '';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.141: editCompetitors
    //      function to edit competitors
    //-----------------------------------------------------
	public static function editCompetitors($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'competitors';
        
        $upd_data   = DB::table('tblcompetitor')
            ->where('entity_id', '565')
            ->orderBy('competitorid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->competitorid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['competitor_name'][0]      = 'Competitor AA';
			$post_fields['competitor_address'][0]   = 'Competitor Building';
			$post_fields['competitor_phone'][0]     = '632 944 4444';
			$post_fields['competitor_email'][0]     = 'competitor@companyc.com';
        }
        else {
            $post_fields['competitor_name'][0]      = '';
			$post_fields['competitor_address'][0]   = 'Competitor Building';
			$post_fields['competitor_phone'][0]     = '632 944 4444';
			$post_fields['competitor_email'][0]     = 'competitor@companyc.com';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.142:editBusinessDrivers
    //      function to edit business drivers
    //-----------------------------------------------------
	public static function editBusinessDrivers($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'business_drivers';
        
        $upd_data   = DB::table('tblbusinessdriver')
            ->where('entity_id', '565')
            ->orderBy('businessdriverid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->businessdriverid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['businessdriver_name'][0]          = 'Driver AA';
			$post_fields['businessdriver_total_sales'][0]   = '37';
        }
        else {
            $post_fields['businessdriver_name'][0]          = '';
			$post_fields['businessdriver_total_sales'][0]   = '37';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.143: editPastProject
    //      function to edit past project
    //-----------------------------------------------------
	public static function editPastProject($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'past_projects';
        
        $upd_data   = DB::table('tblprojectcompleted')
            ->where('entity_id', '565')
            ->orderBy('projectcompletedid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->projectcompletedid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['projectcompleted_name'][0]            = 'Project A';
			$post_fields['projectcompleted_cost'][0]            = '450000000';
			$post_fields['projectcompleted_source_funding'][0]  = 'ppc_source_ipo';
			$post_fields['projectcompleted_year_began'][0]      = '2005';
			$post_fields['projectcompleted_result'][0]          = 'ppc_result_s';
        }
        else {
            $post_fields['projectcompleted_name'][0]            = '';
			$post_fields['projectcompleted_cost'][0]            = '450000000';
			$post_fields['projectcompleted_source_funding'][0]  = 'ppc_source_ipo';
			$post_fields['projectcompleted_year_began'][0]      = '2005';
			$post_fields['projectcompleted_result'][0]          = 'ppc_result_s';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.144: editFutureGrowth
    //      function to edit future growth
    //-----------------------------------------------------
	public static function editFutureGrowth($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'future_initiatives';
        
        $upd_data   = DB::table('tblfuturegrowth')
            ->where('entity_id', '565')
            ->orderBy('futuregrowthid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->futuregrowthid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['futuregrowth_name'][0]                    = 'Project A';
			$post_fields['futuregrowth_estimated_cost'][0]          = '450000000';
			$post_fields['futuregrowth_implementation_date'][0]     = '2017-02-15';
			$post_fields['futuregrowth_source_capital'][0]          = 'fgi_source_partners';
			$post_fields['planned-proj-goal'][0]                    = 'fgi_increase_sales';
			$post_fields['planned-goal-increase'][0]                = '0-5';
			$post_fields['proj-benefit-date'][0]                    = '2017-03';
        }
        else {
            $post_fields['futuregrowth_name'][0]                    = '';
			$post_fields['futuregrowth_estimated_cost'][0]          = '450000000';
			$post_fields['futuregrowth_implementation_date'][0]     = '2017-02-15';
			$post_fields['futuregrowth_source_capital'][0]          = 'fgi_source_partners';
			$post_fields['planned-proj-goal'][0]                    = 'fgi_increase_sales';
			$post_fields['planned-goal-increase'][0]                = '0-5';
			$post_fields['proj-benefit-date'][0]                    = '2017-03';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.145: editRiskAssement
    //      function to edit risk assessment
    //-----------------------------------------------------
	public static function editRiskAssement($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'risk_assessment';
        
        $upd_data   = DB::table('tblriskassessment')
            ->where('entity_id', '565')
            ->orderBy('riskassessmentid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->riskassessmentid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['risk_assessment_name'][0]		= 'threat_of_substitute';
			$post_fields['risk_assessment_solution'][0]	= 'product_innovation';
        }
        else {
            $post_fields['risk_assessment_name'][0]		= '';
			$post_fields['risk_assessment_solution'][0]	= 'product_innovation';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.146: editRevenueGrowth
    //      function to edit revenue growth
    //-----------------------------------------------------
	public static function editRevenueGrowth($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'revenue_potential';
        
        $upd_data   = DB::table('tblrevenuegrowthpotential')
            ->where('entity_id', '565')
            ->orderBy('growthpotentialid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->growthpotentialid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['current_market'][0]	= '2';
			$post_fields['new_market'][0]		= '2';
        }
        else {
            $post_fields['current_market'][0]	= '';
			$post_fields['new_market'][0]		= '';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.147: editCapitalExpansion
    //      function to edit capital expansion
    //-----------------------------------------------------
	public static function editCapitalExpansion($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'expansion_investment';
        
        $upd_data   = DB::table('tblcapacityexpansion')
            ->where('entity_id', '565')
            ->orderBy('capacityexpansionid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key'][0]   = $upd_data->capacityexpansionid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['current_year'][0]	= '200000';
			$post_fields['new_year'][0]		= '400000';
        }
        else {
            $post_fields['current_year'][0]	= '';
			$post_fields['new_year'][0]		= '';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.148: editRequiredCredit
    //      function to edit required credit
    //-----------------------------------------------------
	public static function editRequiredCredit($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'required_credit';
        
        $upd_data   = DB::table('tblplanfacilityrequested')
            ->where('entity_id', '565')
            ->orderBy('id', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key']  = $upd_data->id;
        }
        
        if (STS_OK == $valid) {
            $post_fields['plan_credit_availment']           = '25234234234';
			$post_fields['purpose_credit_facility']         = '2';
			$post_fields['purpose_credit_facility_others']  = '';
			$post_fields['credit_terms']                    = '';
        }
        else {
            $post_fields['plan_credit_availment']           = '';
			$post_fields['purpose_credit_facility']         = '';
			$post_fields['purpose_credit_facility_others']  = '';
			$post_fields['credit_terms']                    = '';
		}
        
		$post_fields['other_remarks']                   = 'Good doing business with';
		$post_fields['cash_deposit_details']            = 'CA';
		$post_fields['cash_deposit_amount']             = '10000';
		$post_fields['securities_details']              = 'BPI Stocks';
		$post_fields['securities_estimated_value']      = '23423423';
		$post_fields['property_details']                = 'Really Big Island';
		$post_fields['property_estimated_value']        = '10000000';
		$post_fields['chattel_details']                 = 'Good';
		$post_fields['chattel_estimated_value']         = '34444444';
		$post_fields['others_details']                  = 'Other';
		$post_fields['others_estimated_value']          = '123123123';
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.149: editOwnerDetails
    //      function to edit owner details
    //-----------------------------------------------------
	public static function editOwnerDetails($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'owner_details';
        
        $upd_data   = DB::table('tblowner')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key']                  = $upd_data->ownerid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['firstname']                   = 'Bruce';
			$post_fields['middlename']                  = 'Batman';
			$post_fields['lastname']                    = 'Wayne';
        }
        else {
            $post_fields['firstname']                   = '';
			$post_fields['middlename']                  = '';
			$post_fields['lastname']                    = '';
		}
		
		$post_fields['nationality']                 = 'Americano';
		$post_fields['birthdate']                   = '1988-01-21';
		$post_fields['civilstatus']                 = '2';
		$post_fields['profession']                  = 'Vigilante';
		$post_fields['email']                       = 'bruce@wayne-enterprise.com';
		$post_fields['address1']                    = 'Gotham';
		$post_fields['address2']                    = '';
		$post_fields['cityid']                      = '2';
		$post_fields['province']                    = 'Makati';
		$post_fields['zipcode']                     = '1013';
		$post_fields['present_address_status']      = '1';
		$post_fields['no_yrs_present_address']      = '24';
		$post_fields['phone']                       = '632 955 5555';
		$post_fields['tin_num']                     = '111-111-111';
		$post_fields['percent_of_ownership']        = '32';
		$post_fields['number_of_year_engaged']      = '9';
		$post_fields['position']                    = 'CEO';
		
		$post_fields['owner_id']                    = '2110009127';
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.150: editSpouseDetails
    //      function to edit spouse details
    //-----------------------------------------------------
    public static function editSpouseDetails($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'spouse';
        
        $upd_data   = DB::table('tblspouse')
            ->where('entity_id', '565')
            ->orderBy('spouseid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key']                  = $upd_data->spouseid;
        }
        else {
            $post_fields['master_key']                  = '2144103771';
        }
        
        if (STS_OK == $valid) {
            $post_fields['firstname']                   = 'Rosa';
			$post_fields['middlename']                  = 'Joanna';
			$post_fields['lastname']                    = 'Farrell';
        }
        else {
            $post_fields['firstname']                   = '';
			$post_fields['middlename']                  = '';
			$post_fields['lastname']                    = '';
		}
		
		$post_fields['nationality']                 = 'Americano';
		$post_fields['birthdate']                   = '1988-01-21';
		$post_fields['profession']                  = 'White Mage';
		$post_fields['email']                       = 'rosa@baron-empire.com';
		$post_fields['phone']                       = '632 955 5555';
		$post_fields['tin_num']                     = '111-111-111';
		
        $post_fields['owner_id']                    = '2110009127';
        
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.151: editPersonalLoans
    //      function to edit personal loans
    //-----------------------------------------------------
	public static function editPersonalLoans($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'personal_loans';
        
        $owner_data   = DB::table('tblowner')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($owner_data) {
            
            $upd_data   = DB::table('tblpersonalloans')
                ->where('entity_id', '565')
                ->orderBy('id', 'asc')
                ->first();
            
            $post_fields['owner_id']        = $owner_data->ownerid;
            $post_fields['master_key'][0]   = 0;
            
            if ($upd_data) {
                $post_fields['master_key'][0]   = $upd_data->id;
            }
        }
        
        if (STS_OK == $valid) {
            $post_fields['purpose'][0]                  = 'Auto';
			$post_fields['balance'][0]                  = '50000000';
			$post_fields['monthly_amortization'][0]     = '1000000';
			$post_fields['creditor'][0]                 = 'PNB';
			$post_fields['due_date'][0]                 = '2016-12-25';
        }
        else {
            $post_fields['purpose'][0]                  = '';
			$post_fields['balance'][0]                  = '';
			$post_fields['monthly_amortization'][0]     = '';
			$post_fields['creditor'][0]                 = '';
			$post_fields['due_date'][0]                 = '';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.152: editKeyManager
    //      function to edit key manager    
    //-----------------------------------------------------
	public static function editKeyManager($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'key_manager';
        
        $upd_data   = DB::table('tblkeymanagers')
            ->where('entity_id', '565')
            ->orderBy('keymanagerid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key']  = $upd_data->keymanagerid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['owner_id']                = '2110009127'; // 0 if Business is Corporation
			$post_fields['firstname']               = 'Jack';
			$post_fields['middlename']              = 'the';
			$post_fields['lastname']                = 'Black';
        }
        else {
            $post_fields['owner_id']                = '2110009127'; // 0 if Business is Corporation
			$post_fields['firstname']               = '';
			$post_fields['middlename']              = '';
			$post_fields['lastname']                = '';
		}
        
		$post_fields['nationality']             = 'Canadian';
        $post_fields['birthdate']               = '1988-09-25';
        $post_fields['civilstatus']             = '2';
        $post_fields['profession']              = 'Celebrity';
        $post_fields['email']                   = 'jack@black.com';
        $post_fields['address1']                = 'Awesome town';
        $post_fields['address2']                = '';
        $post_fields['cityid']                  = '2';
        $post_fields['province']                = 'Manila';
        $post_fields['zipcode']                 = '1013';
        $post_fields['present_address_status']  = '2';
        $post_fields['no_yrs_present_address']  = '23';
        $post_fields['phone']                   = '632 988 8888';
        $post_fields['tin_num']                 = '444-444-444';
        $post_fields['percent_of_ownership']    = '8';
        $post_fields['number_of_year_engaged']  = '2';
        $post_fields['position']                = 'Mechanic';
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.153: editOwnerOtherBusiness
    //      function to edit other businesses of owner
    //-----------------------------------------------------
	public static function editOwnerOtherBusiness($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'other_business';
        
        $owner_data   = DB::table('tblowner')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($owner_data) {
            
            $upd_data   = DB::table('tblmanagebus')
                ->where('entity_id', '565')
                ->orderBy('managebusid', 'asc')
                ->first();
            
            $post_fields['owner_id']        = $owner_data->ownerid;
            $post_fields['master_key'][0]   = 0;
            
            if ($upd_data) {
                $post_fields['master_key'][0]   = $upd_data->managebusid;
            }
        }
        
        if (STS_OK == $valid) {
            $post_fields['bus_name'][0]             = 'Auto';
			$post_fields['bus_type'][0]             = 'Very Good Business';
			$post_fields['bus_location'][0]         = '2';
			$post_fields['owner_type']              = '1';
        }
        else {
            $post_fields['bus_name'][0]             = '';
			$post_fields['bus_type'][0]             = '';
			$post_fields['bus_location'][0]         = '';
			$post_fields['owner_type']              = '1';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.154: editSpouseOtherBusiness
    //      function to edit other businesses of owner's spouse
    //-----------------------------------------------------
    public static function editSpouseOtherBusiness($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'other_business';
        
        $owner_data   = DB::table('tblspouse')
            ->where('entity_id', '565')
            ->orderBy('spouseid', 'asc')
            ->first();
        
        if ($owner_data) {
            
            $upd_data   = DB::table('tblmanagebus')
                ->where('ownerid', $owner_data->spouseid)
                ->orderBy('managebusid', 'asc')
                ->first();
            
            $post_fields['owner_id']        = $owner_data->spouseid;
            $post_fields['master_key'][0]   = 0;
            
            if ($upd_data) {
                $post_fields['master_key'][0]   = $upd_data->managebusid;
            }
        }
        
        if (STS_OK == $valid) {
            $post_fields['bus_name'][0]             = 'Auto';
			$post_fields['bus_type'][0]             = 'Very Good Business';
			$post_fields['bus_location'][0]         = '2';
			$post_fields['owner_type']              = '2';
        }
        else {
            $post_fields['bus_name'][0]             = '';
			$post_fields['bus_type'][0]             = '';
			$post_fields['bus_location'][0]         = '';
			$post_fields['owner_type']              = '2';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.155: editOwnerEducation
    //      function to edit education of owner
    //-----------------------------------------------------
	public static function editOwnerEducation($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'education';
        
        $owner_data   = DB::table('tblowner')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($owner_data) {
            
            $upd_data   = DB::table('tbleducation')
                ->where('entity_id', '565')
                ->orderBy('educationid', 'asc')
                ->first();
            
            $post_fields['owner_id']        = $owner_data->ownerid;
            $post_fields['master_key'][0]   = 0;
            
            if ($upd_data) {
                $post_fields['master_key'][0]   = $upd_data->educationid;
            }
        }
        
        if (STS_OK == $valid) {
            $post_fields['school_name'][0]          = 'FEU - East Asia College';
			$post_fields['educ_degree'][0]          = 'Delayed';
			$post_fields['educ_type'][0]            = '3';
			$post_fields['owner_type']              = '1';
        }
        else {
            $post_fields['school_name'][0]          = '';
			$post_fields['educ_degree'][0]          = '';
			$post_fields['educ_type'][0]            = '';
			$post_fields['owner_type']              = '1';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.156: editSpouseOwnerEducation
    //      function to edit education of owner's spouse
    //-----------------------------------------------------
    public static function editSpouseOwnerEducation($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'education';
        
        $owner_data   = DB::table('tblspouse')
            ->where('entity_id', '565')
            ->orderBy('ownerid', 'asc')
            ->first();
        
        if ($owner_data) {
            
            $upd_data   = DB::table('tbleducation')
                ->where('ownerid', $owner_data->spouseid)
                ->orderBy('educationid', 'asc')
                ->first();
            
            $post_fields['owner_id']        = $owner_data->spouseid;
            $post_fields['master_key'][0]   = 0;
            
            if ($upd_data) {
                $post_fields['master_key'][0]   = $upd_data->educationid;
            }
        }
        
        if (STS_OK == $valid) {
            $post_fields['school_name'][0]          = 'Centro Escolar University';
			$post_fields['educ_degree'][0]          = 'Delayed';
			$post_fields['educ_type'][0]            = '3';
			$post_fields['owner_type']              = '2';
        }
        else {
            $post_fields['school_name'][0]          = '';
			$post_fields['educ_degree'][0]          = '';
			$post_fields['educ_type'][0]            = '';
			$post_fields['owner_type']              = '2';
		}
		
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.157: editExistingCredit
    //      function to edit existing credit
    //-----------------------------------------------------
	public static function editExistingCredit($valid)
	{
		$data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $data['action']     = 'edit';
        $data['section']    = 'existing_credit';
        
        $upd_data   = DB::table('tblplanfacility')
            ->where('entity_id', '565')
            ->orderBy('planfacilityid', 'asc')
            ->first();
        
        if ($upd_data) {
            $post_fields['master_key']  = $upd_data->planfacilityid;
        }
        
        if (STS_OK == $valid) {
            $post_fields['lending_institution']             = 'The Joker';
			$post_fields['plan_credit_availment']           = '25234234234';
			$post_fields['purpose_credit_facility']         = '1';
        }
        else {
            $post_fields['lending_institution']             = '';
			$post_fields['plan_credit_availment']           = '';
			$post_fields['purpose_credit_facility']         = '';
		}
		$post_fields['purpose_credit_facility_others']  = '';
		$post_fields['credit_terms']                    = '';
		$post_fields['outstanding_balance']             = '1000000';
		$post_fields['experience']                      = '1';
		$post_fields['other_remarks']                   = 'Good doing business with';
		$post_fields['cash_deposit_details']            = 'CA';
		$post_fields['cash_deposit_amount']             = '10000';
		$post_fields['securities_details']              = 'BPI Stocks';
		$post_fields['securities_estimated_value']      = '23423423';
		$post_fields['property_details']                = 'Really Big Island';
		$post_fields['property_estimated_value']        = '10000000';
		$post_fields['chattel_details']                 = 'Good';
		$post_fields['chattel_estimated_value']         = '34444444';
		$post_fields['others_details']                  = 'Other';
		$post_fields['others_estimated_value']          = '123123123';
        $data['post_fields']    = $post_fields;
        
        return $data;
	}
    
    //-----------------------------------------------------
    //  Function 19.158: getZipcode
    //      function to get Zipcode
    //-----------------------------------------------------
    public static function getZipcode($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/zipcode';
        $data['action']     = 'zipcode';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['city_id']     = '7';
        }
        else {
            $post_fields['city_id']     = '7';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.159: getIndustries
    //      function to get industries
    //-----------------------------------------------------
    public static function getIndustries($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/industry_main';
        $data['action']     = 'industry_main';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['city_id']     = '';
        }
        else {
            $post_fields['city_id']     = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.160: getIndustriesSub
    //      functino to get industry sub
    //-----------------------------------------------------
    public static function getIndustriesSub($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/industry_sub';
        $data['action']     = 'industry_sub';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['industry_id']     = '7';
        }
        else {
            $post_fields['industry_id']     = '7';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.161: getIndustriesRow
    //      function to get industry row
    //-----------------------------------------------------
    public static function getIndustriesRow($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/industry_row';
        $data['action']     = 'industry_row';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['industry_sub_id']     = '7';
        }
        else {
            $post_fields['industry_sub_id']     = '7';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.162: getBankLlist
    //      function to get list of banks
    //-----------------------------------------------------
    public static function getBankList($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/banks';
        $data['action']     = 'banks';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['bank_id']     = '';
        }
        else {
            $post_fields['bank_id']     = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function 19.163:getMajorCities
    //      function to get major cities
    //-----------------------------------------------------
    public static function getMajorCities($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/major_cities';
        $data['action']     = 'major_cities';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['city_id']     = '';
        }
        else {
            $post_fields['city_id']     = '';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }
    //-----------------------------------------------------
    //  Function 19.164: getCompletedReports
    //      function to get completed reports
    //-----------------------------------------------------
    public static function getCompletedReports($valid)
    {
        $data['post_url']   = 'http://128.199.203.18/cbpo_rest_api/misc/completed_reports';
        $data['action']     = 'completed_reports';
        $data['section']    = 'misc';
        
        if (STS_OK == $valid) {
            $post_fields['login_id']     = '254';
        }
        else {
            $post_fields['login_id']     = '254';
        }
        
        $data['post_fields']    = $post_fields;
        
        return $data;
    }

}