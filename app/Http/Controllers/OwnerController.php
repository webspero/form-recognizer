<?php

use App\Countries;
//======================================================================
//  Class 40: Owner Controller
//      Functions related to Owner Details on the Questionnaire
//      are routed to this class
//======================================================================
class OwnerController extends BaseController {

	//-----------------------------------------------------
    //  Function 40.1: getSpouseedit
    //      Displays the Spouse Form
    //-----------------------------------------------------
	public function getSpouseedit($id)
	{
        /*  Get the spouse details from the Database    */
		$entity = DB::table('tblspouse')
            ->where('spouseid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->first();
            
		return View::make('sme.spouse')->with('entity', $entity);
	}
	
    //-----------------------------------------------------
    //  Function 40.2: putSpouseUpdate
    //      HTTP Post request to validate and save spouse
    //      data on the Database
    //-----------------------------------------------------
	public function putSpouseUpdate($id)
	{
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        $messages = array();                        // Validation Messages
        $messages['phone.regex'] = trans('validation.phoneregex', array('attribute'=>'Contact Phone Number'));

		$rules = array(
			'firstname'     => 'required',
			'middlename'	=> 'required',
			'lastname'	    => 'required',
			'nationality'	=> 'required',
			'birthdate'	    => array('required', 'date_format:"Y-m-d"', 'before:today'),
			'profession'	=> 'required',
			'email'         => 'required|email',
			'phone'         => 'required|regex:/^.{7,20}$/',
			'tin_num'       => array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i')
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else
		{
            /*  Saves Input to the database     */
			$dataFields = array(
				'firstname'     => Input::get('firstname'),
				'middlename'	=> Input::get('middlename'),
				'lastname'      => Input::get('lastname'),
				'nationality'	=> Input::get('nationality'),
				'birthdate'     => Input::get('birthdate'),
				'profession'	=> Input::get('profession'),
				'email'	        => Input::get('email'),
				'phone'	        => Input::get('phone'),
				'tin_num'       => Input::get('tin_num')
			);

			$updateProcess = Spouse::where('spouseid', '=', $id)
                ->update($dataFields);
            
            /*  Update Successful   */
            if ($updateProcess) {
                $master_ids[]           = $id;
                
                $srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Updated Record';
			}
            /*  Update Failed       */
			else {
                $srv_resp['messages']   = 'An error occured while updating the user.';
			}
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 40.3: putKeyManager
    //      HTTP Post request to validate and save Key
    //      Manager data on the Database
    //-----------------------------------------------------
    public function putKeyManager($id)
	{
        /*  Variable Declaration    */
        $messages       = array();                  // Validation Messages according to rules
        $rules          = array();                  // Validation Rules
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
		$messages = array(
            'cityid.required' => 'The city field is required',
            'phone.regex'     => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
        );
        
		$rules = array(
			'firstname'	                => 'required',
			'middlename'	            => 'required',
			'lastname'	                => 'required',
			'nationality'	            => 'required',
			'birthdate'	                => array('required', 'date_format:"Y-m-d"', 'before:today'),
			'civilstatus'	            => 'required|numeric',
			'profession'	            => 'required',
			'email'	                    => 'required|email',
			'address1'	                => 'required',
			'cityid'	                => 'required|numeric',
			'province'	                => 'required',
			'zipcode'	                => 'digits:4',
			'present_address_status'	=> 'required',
			'no_yrs_present_address'	=> 'required|numeric|min:0|max:100',
			'phone'	                    => 'required|regex:/^.{7,20}$/',
			'tin_num'	                => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'percent_of_ownership'	    => 'required|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'required|numeric|min:0|max:100',
			'position'	                => 'required',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Saves Input to the database     */
			$km                         = KeyManager::find($id);
			$km->firstname              = Input::get('firstname');
			$km->middlename             = Input::get('middlename');
			$km->lastname               = Input::get('lastname');
			$km->nationality            = Input::get('nationality');
			$km->birthdate              = Input::get('birthdate');
			$km->civilstatus            = Input::get('civilstatus');
			$km->profession             = Input::get('profession');
			$km->email                  = Input::get('email');
			$km->address1               = Input::get('address1');
			$km->address2               = Input::get('address2');
			$km->cityid                 = Input::get('cityid');
			$km->province               = Input::get('province');
			$km->zipcode                = Input::get('zipcode');
			$km->present_address_status = Input::get('present_address_status');
			$km->no_yrs_present_address = Input::get('no_yrs_present_address');
			$km->phone                  = Input::get('phone');
			$km->tin_num                = Input::get('tin_num');
			$km->percent_of_ownership   = Input::get('percent_of_ownership');
			$km->number_of_year_engaged = Input::get('number_of_year_engaged');
			$km->position               = Input::get('position');
            
            /*  Update Successful   */
			if($km->save())
			{
				$srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Updated Record';
                
                /*  Sets the master id for output when request came from API    */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $km->keymanagerid;
                }
			}
            /*  Update failed       */
			else {
				$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
			}
		}
        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
	//-----------------------------------------------------
    //  Function 40.4: putOwnerUpdate
    //      HTTP Post request to validate and save Owner
    //      data on the Database
    //-----------------------------------------------------
	public function putOwnerUpdate($id)
	{
        /*  Sets the master id for output when request came from API    */
        if (Input::has('master_key')) {
            $primary_id     = Input::get('master_key');
        }
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
		$messages = array(
            'cityid.required'   => 'The city field is required',
            'phone.regex'     => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
        );

		$rules = array(
			'firstname'                 => 'required',
			'middlename'                => 'required',
			'lastname'	                => 'required',
			'nationality'	            => 'required',
			'birthdate'	                => array('required', 'date_format:"Y-m-d"', 'before:today'),
			'civilstatus'	            => 'required|numeric',
			'profession'	            => 'required',
			'email'                     => 'required|email',
			'address1'	                => 'required',
			/* 'address2'	=> 'required', */
			'cityid'	                => 'required|numeric',
			'province'	                => 'required',
			'zipcode'	                => 'digits:4',
			'present_address_status'	=> 'required',
			'no_yrs_present_address'	=> 'required|numeric|min:0|max:100',
			'phone'	                    => 'required|regex:/^.{7,20}$/',
			'tin_num'                   => array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'percent_of_ownership'	    => 'required|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'required|numeric|min:0|max:100',
			'position'	                => 'required',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Saves Input to the database     */
			$dataFields = array(
				'firstname'                 => Input::get('firstname'),
				'middlename'                => Input::get('middlename'),
				'lastname'	                => Input::get('lastname'),
				'nationality'	            => Input::get('nationality'),
				'birthdate'	                => Input::get('birthdate'),
				'civilstatus'	            => Input::get('civilstatus'),
				'profession'	            => Input::get('profession'),
				'email'	                    => Input::get('email'),
				'address1'	                => Input::get('address1'),
				'address2'	                => Input::get('address2'),
				'cityid'	                => Input::get('cityid'),
				'province'	                => Input::get('province'),
				'zipcode'	                => Input::get('zipcode'),
				'present_address_status'	=> Input::get('present_address_status'),
				'no_yrs_present_address'	=> Input::get('no_yrs_present_address'),
				'phone'	                    => Input::get('phone'),
				'tin_num'                   => Input::get('tin_num'),
				'percent_of_ownership'      => Input::get('percent_of_ownership'),
				'number_of_year_engaged'    => Input::get('number_of_year_engaged'),
				'position'                  => Input::get('position')
			);

			$updateProcess = Owner::where('ownerid', '=', $id)
                ->update($dataFields);
            
            /*  Update Successful   */
			if ($updateProcess) {
                $master_ids[]           = $primary_id;
                
                $srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Updated Record';
			}
            /*  Update Failed       */
			else {
                $srv_resp['messages']   = 'An error occured while updating the user.';
			}
		}
        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 40.5: getOwneredit
    //      Displays the Owner Form
    //-----------------------------------------------------
	public function getOwneredit($id)
	{
        /*  Gets the owner details from the datatbase   */
		$entity = DB::table('tblowner')
            ->where('ownerid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->first();
        
        /*  Gets all the cities from the database       */
        //JM - changed list to pluck
		$dataCity       = City::all()->pluck('city', 'cityid');
        
        /*  Gets all the provinces from the database    */
        //JM - changed list to pluck
		$dataProvince   = Province::all()->pluck('province', 'provinceid');
        
        /*  Pass the data to the UI                     */
		return View::make('sme.owner', 
			array(
				'cityList' => $dataCity,
				'provinceList' => $dataProvince, 
				)
			)->with('entity', $entity);
	}
    
    //-----------------------------------------------------
    //  Function 40.6: getRiskassessmentDelete
    //      HTTP Get request to delete specified risk
    //      assessment
    //          (Currently not in use)
    //-----------------------------------------------------
	public function getRiskassessmentDelete($id)
	{
        /*  Delete the specified Risk Assessment from the database  */
		DB::table('tblowner')
            ->where('ownerid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->delete();
        
        /*  Adds a one-off session variable for redirection */
		Session::flash('redirect_tab', 'tab3');
		
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 40.7: getEducation
    //      Displays the Education form
    //-----------------------------------------------------
	public function getEducation($entity_id, $owner_id, $type)
	{
		return View::make('sme.education', array(
                'entity_id' => $entity_id,
                'owner_id'  => $owner_id,
                'type'      => $type
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 40.8: postEducation
    //      HTTP Post request to validate and save education
    //      data to the database
    //-----------------------------------------------------
	public function postEducation($entity_id, $owner_id, $type)
	{
        /*  Variable Declaration    */
        $schools    = Input::get('school_name');        // School Name
        $messages   = array();                          // Validation Messages
        $rules      = array();                          // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Refresh specified Container
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            
            /* Editing Owner Education Details      */
            if (1 == $type) {
                $srv_resp['refresh_cntr']   = '#demo3_'.$owner_id;                      // Container element where changed data are located
                $srv_resp['refresh_elems']  = '.reload-owner-educ-'.$owner_id;          // Elements that will be refreshed
            }
            /* Editing Spouse Education Details     */
            else {
                $srv_resp['refresh_cntr']   = '#demo6_'.$owner_id;                      // Container element where changed data are located
                $srv_resp['refresh_elems']  = '.reload-spouse-educ-'.$owner_id;         // Elements that will be refreshed
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($schools); $idx++) {
            /*  Always validate the first input set and 
            *   for the succeeding validate those with School name
            */
            if ((STR_EMPTY != $schools[$idx])
            || (0 == $idx)) {

                $messages['school_name.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'School'));
                $messages['educ_degree.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'Level of Completion'));
            
                $rules['school_name.'.$idx]	= 'required';
                $rules['educ_degree.'.$idx]	= 'required';
            }
        }
        
        /*  Run the Laravel Validation  */
        $validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
        if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
        else {
            /*  Stores all inputs from the form to an array for saving and looping  */
            $arraydata = Input::only('educ_type', 'school_name', 'educ_degree');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            $educ_type      = $arraydata['educ_type'];
            $school_name    = $arraydata['school_name'];
            $educ_degree    = $arraydata['educ_degree'];
            
            /*  Saves Input to the database     */
            foreach ($school_name as $key => $value) {
                if ($value) {
                    
                    $data = array(
                        'ownerid'       => $owner_id, 
                        'entity_id'     => $entity_id, 
                        'educ_type'     => $educ_type[$key], 
                        'school_name'   => $school_name[$key],					   	
                        'educ_degree'   => $educ_degree[$key], 
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                    );
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tbleducation')
                            ->where('educationid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tbleducation')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
                }
            }
            
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
        }
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 40.9: deleteEducationBg
    //      HTTP Get request to delete specified education
    //-----------------------------------------------------
    public function deleteEducationBg($id)
	{
        /*  Deletes specified Education */
		Education::find($id)->delete();
		
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 40.10: getManageBusiness
    //      Display the Other Business Manage Form
    //-----------------------------------------------------
	public function getManageBusiness($entity_id, $owner_id, $type)
	{
        /*  Gets all the cities from the database   */
        
		$dataCity = City::all()->pluck('city', 'cityid');
		
        return View::make('sme.managebus',
			array(
                'cityList'  => $dataCity,
                'owner_id'  => $owner_id,
                'entity_id' => $entity_id,
                'type'      => $type,
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 40.11: postManageBusiness
    //      HTTP Post request to validate and save Other
    //      Business Manage
    //-----------------------------------------------------
	public function postManageBusiness($entity_id, $owner_id, $type)
	{
        /*  Variable Declaration    */
        $business   = Input::get('bus_name');   // Business Name
        $messages   = array();                  // Validation Messages
        $rules      = array();                  // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Refresh the specified container
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            
            /* Owner Other Business */
            if (1 == $type ) {
                $srv_resp['refresh_cntr']   = '#demo2_'.$owner_id;                      // Container element where changed data are located
                $srv_resp['refresh_elems']  = '.reload-owner-biz-'.$owner_id;           // Elements that will be refreshed
            }
            /* Spouse Other Business */
            else {
                $srv_resp['refresh_cntr']   = '#demo5_'.$owner_id;                      // Container element where changed data are located
                $srv_resp['refresh_elems']  = '.reload-spouse-biz-'.$owner_id;          // Elements that will be refreshed
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($business); $idx++) {
            
            if ((STR_EMPTY != $business[$idx])
            || (0 == $idx)) {

                $messages['bus_name.'.$idx.'.required']	    = trans('validation.required', array('attribute'=>'Business Name'));
                $messages['bus_type.'.$idx.'.required']	    = trans('validation.required', array('attribute'=>'Business Type'));
                $messages['bus_location.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'Business Location'));
                $messages['bus_location.'.$idx.'.numeric']	= trans('validation.numeric', array('attribute'=>'Business Location'));
            
                $rules['bus_name.'.$idx]	    = 'required';
                $rules['bus_type.'.$idx]	    = 'required';
                $rules['bus_location.'.$idx]	= 'required|numeric';
            }
        }
        
        /*  Run the Laravel Validation  */
        $validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('bus_name', 'bus_type', 'bus_location');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
			$bus_name       = $arraydata['bus_name'];
			$bus_type       = $arraydata['bus_type'];
			$bus_location   = $arraydata['bus_location'];
            
            /*  Saves Input to the database     */
			foreach ($bus_name as $key => $value) {
				if ($value) {
					$data = array(
					    'ownerid'       => $owner_id, 
					    'entity_id'     => $entity_id, 
					   	'bus_name'      => $bus_name[$key], 
					   	'bus_type'      => $bus_type[$key],					   	
					   	'bus_location'  => $bus_location[$key], 
					   	'created_at'    => date('Y-m-d H:i:s'),
				    	'updated_at'    => date('Y-m-d H:i:s')
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblmanagebus')
                            ->where('managebusid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblmanagebus')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
            
            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 40.12: deleteManageBusiness
    //      HTTP Get request to delete specified Business
    //-----------------------------------------------------
    public function deleteManageBusiness($id)
	{
        /*  Delete Specified Other Business */
		ManageBusiness::find($id)->delete();
		
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}
	
    //-----------------------------------------------------
    //  Function 40.13: getKeyManager
    //      Displays the Key Manager Form
    //-----------------------------------------------------
	public function getKeyManager($entity_id, $owner_id)
	{
        /*  Gets all Provinces from the Database    */
		$dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		
        /*  Gets all Cities from the Database       */
        $dataCity = Zipcode::select('id', 'city', 'zip_code')->where('major_area', Input::old('province'))->get();
        
        /*  Pass data to the UI */
		return View::make('sme.keymanager', array(
			'cityList'      => $dataCity,
			'provinceList'  => $dataProvince,
			'ownerid'       => $owner_id,
            'entity_id'     => $entity_id
		));
	}
	
    //-----------------------------------------------------
    //  Function 40.14: postKeyManager
    //      HTTP Post to validate and save Key Manager
    //      data to the database
    //-----------------------------------------------------
	public function postKeyManager($entity_id, $owner_id)
	{
        /*  Variable Declaration    */
        $messages       = array();                  // Validation Messages
        $rules          = array();                  // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#demo7_'.$owner_id;
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-key-manager-'.$owner_id;
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        $messages = array(
            'cityid.required'   => 'The city field is required',
            'phone.regex'     => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
        );
        
		$rules = array(
			'firstname'                 => 'required',
			'middlename'	            => 'required',
			'lastname'                  => 'required',
			'nationality'	            => 'required',
			'birthdate'	                => array('required', 'date_format:"Y-m-d"', 'before:today'),
			'civilstatus'	            => 'required|numeric',
			'profession'	            => 'required',
			'email'	                    => 'required|email',
			'address1'	                => 'required',
			'cityid'	                => 'required|numeric',
			'province'	                => 'required',
			'zipcode'	                => 'digits:4',
			'present_address_status'	=> 'required',
			'no_yrs_present_address'	=> 'required|numeric|min:0|max:100',
			'phone'	                    => 'required|regex:/^.{7,20}$/',
			'tin_num'	                => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'percent_of_ownership'	    => 'required|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'required|numeric|min:0|max:100',
			'position'	                => 'required',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else
		{
            /*  Saves Input to the database     */
			$km                         = new KeyManager();
			$km->ownerid                = $owner_id;
			$km->entity_id              = $entity_id;
			$km->firstname              = Input::get('firstname');
			$km->middlename             = Input::get('middlename');
			$km->lastname               = Input::get('lastname');
			$km->nationality            = Input::get('nationality');
			$km->birthdate              = Input::get('birthdate');
			$km->civilstatus            = Input::get('civilstatus');
			$km->profession             = Input::get('profession');
			$km->email                  = Input::get('email');
			$km->address1               = Input::get('address1');
			$km->address2               = Input::get('address2');
			$km->cityid                 = Input::get('cityid');
			$km->province               = Input::get('province');
			$km->zipcode                = Input::get('zipcode');
			$km->present_address_status = Input::get('present_address_status');
			$km->no_yrs_present_address = Input::get('no_yrs_present_address');
			$km->phone                  = Input::get('phone');
			$km->tin_num                = Input::get('tin_num');
			$km->percent_of_ownership   = Input::get('percent_of_ownership');
			$km->number_of_year_engaged = Input::get('number_of_year_engaged');
			$km->position               = Input::get('position');
            
            /*  Update successful   */
			if ($km->save()) {
				$srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Added Record';
                
                /*  When an edit request comes from the API */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $km->keymanagerid;
                }
			}
            /*  Update failed       */
			else {
				$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
			}
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
	
    //-----------------------------------------------------
    //  Function 40.15: deleteKeyManager
    //      HTTP Get request to delete specified Key Manager
    //-----------------------------------------------------
	public function deleteKeyManager($id)
	{
        $manager = KeyManager::where('keymanagerid', $id)
                    ->where('entity_id', Session::get('entity')->entityid)
                    ->first();
        $manager->is_deleted = 1;
        $manager->save();
        
        $entity  = Entity::find(Session::get('entity')->entityid);
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}
	
    //-----------------------------------------------------
    //  Function 40.16: getPersonalLoans
    //      Displays the Personal Loans Form
    //-----------------------------------------------------
	public function getPersonalLoans($entity_id, $owner_id)
	{
        /*  Displays the Personal Loans Form    */
		return View::make('sme.personalloans', array(
			'auto'      => PersonalLoan::where('purpose', 'Auto')->where('owner_id', $owner_id)->count(),
			'housing'   => PersonalLoan::where('purpose', 'Housing')->where('owner_id', $owner_id)->count(),
			'owner_id'  => $owner_id,
            'entity_id' => $entity_id
		));
	}
	
    //-----------------------------------------------------
    //  Function 40.17: postPersonalLoans
    //      Displays the Personal Loans Form
    //-----------------------------------------------------
	public function postPersonalLoans($entity_id, $owner_id)
	{
        /*  Variable Declaration    */
        $balance    = Input::get('balance');            // Outstanding Balance
        $purpose    = Input::get('purpose');            // Purpose of Loan
        $messages   = array();                          // Validation Messages
        $rules      = array();                          // Validation Rules
        $master_ids = array();                          // Master IDs of existing Loans
        $pidx       = 0;
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Refresh the specified container
            $srv_resp['refresh_cntr']   = '.owner-loans-'.$owner_id;                    // Container element where changed data are located
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            $srv_resp['refresh_elems']  = '.reload-loans-'.$owner_id;                   // Elements that will be refreshed
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Check existing Auto Loan    */
        $auto_loan  = PersonalLoan::where('purpose', 'Auto')
            ->where('owner_id', $owner_id)
            ->count();
        
        /*  Check existing Housing Loan */
		$house_loan = PersonalLoan::where('purpose', 'Housing')
            ->where('owner_id', $owner_id)
            ->count();
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = $pidx; $idx < @count($balance); $idx++) {
            
            if (STR_EMPTY != $balance[$idx]) {
                
				$rules['purpose.'.$idx]          		= 'required';
				$rules['balance.'.$idx]          		= 'required|numeric|max:9999999999999.99';
                $rules['monthly_amortization.'.$idx]    = 'required|numeric|max:9999999999999.99';
                $rules['creditor.'.$idx]  				= 'required';
                $rules['due_date.'.$idx]           		= 'required|date_format:"Y-m-d"';
				
                $messages['purpose.'.$idx.'.required']     				= trans('validation.required', array('attribute'=>'Purpose'));
                $messages['balance.'.$idx.'.required']     				= trans('validation.required', array('attribute'=>'Outstanding Balance'));
                $messages['balance.'.$idx.'.numeric']     				= trans('validation.numeric', array('attribute'=>'Outstanding Balance'));
                $messages['balance.'.$idx.'.max']                       = trans('validation.max.numeric', array('attribute'=>'Outstanding Balance'));
                $messages['monthly_amortization.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'Monthly Amortization'));
                $messages['monthly_amortization.'.$idx.'.numeric']      = trans('validation.numeric', array('attribute'=>'Monthly Amortization'));
                $messages['monthly_amortization.'.$idx.'.max']          = trans('validation.max.numeric', array('attribute'=>'Monthly Amortization'));
                $messages['creditor.'.$idx.'.required']    				= trans('validation.required', array('attribute'=>'Creditor'));
                $messages['due_date.'.$idx.'.required']     			= trans('validation.required', array('attribute'=>'Due Date'));
                $messages['due_date.'.$idx.'.date_format']     			= 'Invalid Date format';
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
                        /*  Stores data into an array for loop traversal    */
			$purpose                = Input::get('purpose');
			$monthly_amortization   = Input::get('monthly_amortization');
			$creditor               = Input::get('creditor');
			$due_date               = Input::get('due_date');
			
            /*  Saves Input to the database     */
			foreach ($balance as $key=>$value) {
				if ($value) {
					$data = array(
					    'entity_id'    				=> $entity_id, 
					   	'owner_id'     				=> (int)$owner_id,
					   	'purpose'      				=> $purpose[$key],
					   	'balance'    				=> $value,
					   	'monthly_amortization'    	=> $monthly_amortization[$key],
					   	'creditor'                	=> $creditor[$key],
				    	'due_date'                	=> $due_date[$key]
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblpersonalloans')
                            ->where('id', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblpersonalloans')->insertGetId($data);;
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
	
    //-----------------------------------------------------
    //  Function 40.18: deletePersonalLoans
    //      Delete specified Personal Loan
    //-----------------------------------------------------
	public function deletePersonalLoans($id)
	{
        /*  Delete specified Personal Loan  */
		PersonalLoan::find($id)->delete();
		
        /*  Logs the action to the database */
        ActivityLog::saveLog();
		
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}
    
    //-----------------------------------------------------
    //  Function 40.19: getKeyManagerOnly
    //      Displays form for Key Manager when Business is
    //      a Corporation
    //-----------------------------------------------------
	public function getKeyManagerOnly($entity_id)
	{
        /*  Fetch all Provinces */
		$dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		
        /*  Fetch all Cities    */
        $dataCity = Zipcode::select('id', 'city', 'zip_code')->where('major_area', Input::old('province'))->get();
		
        /*  Pass data into View */
        return View::make('sme.keymanageronly', array(
			'cityList'      => $dataCity,
			'provinceList'  => $dataProvince,
            'entity_id'     => $entity_id
		));
	}
	
    //-----------------------------------------------------
    //  Function 40.20: postKeyManagerOnly
    //      HTTP Post request to validate and save
    //      Key Manager data into the database when
    //      business is a Corporation
    //-----------------------------------------------------
	public function postKeyManagerOnly($entity_id)
	{
        /*  Variable Declaration    */
        $messages       = array();
        $rules          = array();
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            /*  Request did not come from API set the server response   */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
                return json_encode($srv_resp);
            }
            /*  Request came from Web Application via browser   */
            else {
                return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
            }
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        $messages = array(
            'cityid.required' => 'The city field is required',
            'phone.regex'     => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
        );
        
		$rules = array(
			'firstname'	                => 'required',
			'middlename'                => 'required',
			'lastname'	                => 'required',
			'nationality'	            => 'required',
			'birthdate'                 => array('sometimes', 'nullable', 'date_format:"Y-m-d"', 'before:today'),
			'civilstatus'	            => 'required|numeric',
			'profession'	            => 'required',
			'email'	                    => 'required|email',
			'address1'	                => 'required',
			'cityid'	                => 'required|numeric',
			'province'	                => 'required',
			'zipcode'	                => 'digits:4',
			'present_address_status'	=> 'required',
			'no_yrs_present_address'	=> 'required|numeric|min:0|max:100',
			'phone'	                    => 'required|regex:/^.{7,20}$/',
			'tin_num'	                => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'percent_of_ownership'	    => 'required|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'required|numeric|min:0|max:100',
            'position'	                => 'required',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules);
		
		/*  Input validation failed     */
        if ($validator->fails()) {
            /*  Encodes Output in JSON format when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['messages']	= $validator->messages()->all();
                return json_encode($srv_resp);
            }
            /*  Request came from Web Application via browser   */
            else {
                return Redirect::route('getKeyManagerOnly', array($entity_id))->withErrors($validator)->withInput();
            }
		}
        /*  Input validation success    */
		else
		{
            /*  Saves Input to the database     */
			$km                         = new KeyManager();
			$km->ownerid                = 0;
			$km->entity_id              = $entity_id;
			$km->firstname              = Input::get('firstname');
			$km->middlename             = Input::get('middlename');
			$km->lastname               = Input::get('lastname');
			$km->nationality            = Input::get('nationality');
			$km->birthdate              = Input::get('birthdate') ? request('birthdate') : '';
			$km->civilstatus            = Input::get('civilstatus');
			$km->profession             = Input::get('profession');
			$km->email                  = Input::get('email');
			$km->address1               = Input::get('address1');
			$km->address2               = Input::get('address2') ? request('address2') : '';
			$km->cityid                 = Input::get('cityid');
			$km->province               = Input::get('province');
			$km->zipcode                = Input::get('zipcode');
			$km->present_address_status = Input::get('present_address_status');
			$km->no_yrs_present_address = Input::get('no_yrs_present_address');
			$km->phone                  = Input::get('phone');
			$km->tin_num                = Input::get('tin_num');
			$km->percent_of_ownership   = Input::get('percent_of_ownership');
			$km->number_of_year_engaged = Input::get('number_of_year_engaged');
			$km->position               = Input::get('position');
            
            /*  Update Successful   */
			if($km->save()) {
				$srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Added Record';
                
                /*  Encodes Output in JSON format when request came from API    */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $km->keymanagerid;
                    
                    return json_encode($srv_resp);
                }
                /*  Request came from Web Application via browser   */
                else {
                    return Redirect::to('/summary/smesummary/'.$entity_id);
                }
			}
            /*  Update Failed       */
			else {
                /*  Encodes Output in JSON format when request came from API    */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
                    
                    return json_encode($srv_resp);
                }
                /*  Request came from Web Application via browser   */
                else {
                    return Redirect::to('/sme/confirmation/tab5')->with('fail', 'An error occured while creating the user. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.');
                }
			}
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
    }
    
   //-----------------------------------------------------
    //  Function 40.21: getKeyManager2
    //      Displays form for Key Manager when Business is
    //      a Corporation
    //-----------------------------------------------------
	public function getKeyManager2($entity_id)
	{
        /*  Fetch all Provinces */
        $dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
        $nationalities = Countries::all()->pluck('enNationality', 'enNationality');
		
        /*  Fetch all Cities    */
        $dataCity = Zipcode::select('id', 'city', 'zip_code')->where('major_area', Input::old('province'))->get();
		
        /*  Pass data into View */
        return View::make('sme.key_manager_premium', array(
			'cityList'      => $dataCity,
			'provinceList'  => $dataProvince,
            'entity_id'     => $entity_id,
            'nationalities' => $nationalities,
		));
    }
    
    //-----------------------------------------------------
    //  Function 40.20: postKeyManagerOnly
    //      HTTP Post request to validate and save
    //      Key Manager data into the database when
    //      business is a Corporation
    //-----------------------------------------------------
	public function postKeyManager2($entity_id)
	{
        /*  Variable Declaration    */
        $messages       = array();
        $rules          = array();
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        $entity = Entity::find($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            /*  Request did not come from API set the server response   */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
                return json_encode($srv_resp);
            }
            /*  Request came from Web Application via browser   */
            else {
                return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
            }
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        $messages = array(
            'cityid.required' => 'The city field is required',
            'phone.regex'     => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
        );
        
		$rules = array(
			'firstname'	                => 'required',
			'lastname'	                => 'required',
			'civilstatus'	            => 'sometimes|nullable|numeric',
            'phone'                     => 'sometimes|nullable|regex:/^.{7,20}$/',
			'tin_num'	                => array('sometimes', 'nullable', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i'),
			'percent_of_ownership'	    => 'sometimes|nullable|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'sometimes|nullable|numeric|min:0|max:100',
			'position'	                => 'required',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
		
		/*  Input validation failed     */
        if ($validator->fails()) {
            /*  Encodes Output in JSON format when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['messages']	= $validator->messages()->all();
                return json_encode($srv_resp);
            }
            /*  Request came from Web Application via browser   */
            else {
			    $srv_resp['messages']	= $validator->messages()->all();
                return json_encode($srv_resp);
            }
		}
        /*  Input validation success    */
		else
		{
            /*  Saves Input to the database     */
			$km                         = new KeyManager();
			$km->ownerid                = 0;
			$km->entity_id              = $entity_id;
			$km->firstname              = Input::get('firstname');
			$km->lastname               = Input::get('lastname');
			$km->nationality            = request('nationality') == null ? '' : Input::get('nationality');
			$km->civilstatus            = request('civilstatus') == null ? '' : Input::get('civilstatus');
			$km->profession             = request('profession') == null ? '' : Input::get('profession');
			$km->phone                  = request('phone') == null ? '' : Input::get('phone');
			$km->middlename             = request('middlename') == null ? '' : Input::get('middlename');
			$km->birthdate              = Input::get('birthdate') ? request('birthdate') : '';
			$km->address1              = Input::get('address1') ? request('address1') : '';
			$km->tin_num                = request('tin_num') == null ? '' : Input::get('tin_num');
			$km->percent_of_ownership   = request('percent_of_ownership') == null ? '' : Input::get('percent_of_ownership');
			$km->number_of_year_engaged = request('number_of_year_engaged') == null ? '' : Input::get('number_of_year_engaged');
			$km->position               = request('position') == null ? '' : Input::get('position');
            
            /*  Update Successful   */
			if($km->save()) {
                
                
				$srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Added Record';
                
                /*  Encodes Output in JSON format when request came from API    */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $km->keymanagerid;
                    
                    return json_encode($srv_resp);
                }
                /*  Request came from Web Application via browser   */
                else {
                    if($entity->is_premium == PREMIUM_REPORT) {
                        $srv_resp['refresh_url'] =  $srv_resp['refresh_url']    = URL::to('premium-link/summary'.$entity_id);
                        $srv_resp['action']         = REFRESH_CNTR_ACT;
                        $srv_resp['refresh_elems']  = '.reload-key-manager';
                        return json_encode($srv_resp);
                    }
                }
			}
            /*  Update Failed       */
			else {
                /*  Encodes Output in JSON format when request came from API    */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
                    
                    return json_encode($srv_resp);
                }
                /*  Request came from Web Application via browser   */
                else {
                    return Redirect::to('/sme/confirmation/tab5')->with('fail', 'An error occured while creating the user. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.');
                }
			}
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
    }
    

}
