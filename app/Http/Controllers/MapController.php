<?php
//======================================================================
//  Class 38: MapController
//  Description: Contains functions regarding Supervisor maps
//======================================================================
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
class MapController extends BaseController {
	
	private $bank_subscription;			// variable declaration
	private $accountDetails = [];


	//-----------------------------------------------------
    //  Function 38.0: __construct
	//  Description: class constructor
    //-----------------------------------------------------
	public function __construct()
	{
		try{
			if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor') == false){
				$this->accountDetails = BankSessionHelper::createBankSession(Auth::user());
			} else if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			} else if (Auth::user() && Auth::user()->role === 0 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 38.1: getGeocode
	//  Description: Cron job to process all entity and supply geocode values 
    //-----------------------------------------------------
	public function getGeocode()
	{
		try{
			$entity = DB::table('tblentity')
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->where('address1', '!=', '')
			->get();	// get entities

			// cycle through entities and update geocoding
			foreach($entity as $e){
				//if($e->longitude == 0) {
					echo 'Processed entity '.$e->entityid.'<br />';
					Geocoder::getGeocode($e->entityid);
					ob_flush();
				//}
				usleep(100000); // to prevent quota locking
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 38.2: getViewMap
	//  Description: displays the supervisor map panel
    //-----------------------------------------------------
	public function getViewMap()
	{
		try{
	        $this->bank_subscription = Permissions::get_bank_subscription();			// get subscription status
			
			Permissions::check(7);
	        
					$result = $this->_checkAllSubscriptionConditions();
	            if($result != "") 
	                    return $result;
	        
			// filters
			//$this->bank_subscription = Permissions::get_bank_subscription();
			$sme_list = DB::table('tblentity')
	            ->select('tblentity.entityid','tblentity.companyname')
					->where('tblentity.status', 7)
	            ->where('tblentity.address1', '!=', '')
	            ->where('tblentity.cityid', '!=', '0')
	            ->where('tblentity.province', '!=', '')
	            ->where('tblentity.zipcode', '!=', '0');
	            
			if($this->accountDetails['can_view_free_sme']==1){
				$sme_list->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$sme_list->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			/* if($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
				//do nothing
			} else {
				$sme_list->take(BANK_SME_LIMIT);
			} */
			$sme_list = $sme_list->groupBy('entityid')->pluck('companyname', 'entityid');
			
			$main_industry_list = Industrymain::all()->pluck('main_title', 'industry_main_id');
			$region = Region::select('regDesc', 'regCode')->pluck('regDesc', 'regCode');
			
			return View::make('bank.map', array(
				'sme_list' => $sme_list->toArray(),
				'sme_selected' => array(),
				'main_industry_list' => $main_industry_list->toArray(),
				'main_industry_selected' => '',
				'sub_industry_list' => array(),
				'sub_industry_selected' => '',
				'industry_list' => array(),
				'industry_selected' => '',
				'region_selected' => '',
				'bcc_from' => '',
				'bcc_to' => '',
				'mq_from' => '',
				'mq_to' => '',
				'fc_from' => '',
				'fc_to' => '',
				'rating_from' => '',
				'rating_to' => '',
				'brating_from' => '',
				'brating_to' => '',
				'data' => null,
				'region_list' => $region->toArray()
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 38.3: postViewMap
	//  Description: process and get map data
    //-----------------------------------------------------
	public function postViewMap()
	{
		try{
			// filters
			//$this->bank_subscription = Permissions::get_bank_subscription();

	        $sme_list = DB::table('tblentity')
	            ->select('tblentity.entityid','tblentity.companyname')
					->where('tblentity.status', 7)
	            ->where('tblentity.address1', '!=', '')
	            ->where('tblentity.cityid', '!=', '0')
	            ->where('tblentity.province', '!=', '')
	            ->where('tblentity.zipcode', '!=', '0');
	            
			if($this->accountDetails['can_view_free_sme']==1){
				$sme_list->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$sme_list->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			$sme_list = $sme_list->groupBy('entityid')->pluck('companyname', 'entityid');
			
			
			
			$sme_selected = Input::get('sme');
			$main_industry_selected = Input::get('industry1');
			$sub_industry_selected = Input::get('industry2');
			$industry_selected = Input::get('industry3');
			$region_selected = Input::get('region');
			
			$main_industry_list = Industrymain::all()->pluck('main_title', 'industry_main_id');
			$sub_industry_list = Industrysub::where('industry_main_id', $main_industry_selected)->pluck('sub_title', 'industry_sub_id');
			$industry_list = Industryrow::where('industry_sub_id', $sub_industry_selected)->pluck('row_title', 'industry_row_id');
			$region = Region::select('regDesc', 'regCode')->pluck('regDesc', 'regCode');
			
			$data = DB::table('tblentity')->select(DB::raw("
				CONCAT_WS(' ', tblentity.address1, tblentity.address2, zipcodes.city, zipcodes.major_area) as full_address,
				tblentity.companyname, tblentity.longitude, tblentity.latitude
			"))
			->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
			->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
			->groupBy('tblentity.industry_main_id')->groupBy('tblentity.industry_sub_id')->groupBy('tblentity.industry_row_id')
			->orderBy('tblindustry_main.main_title')->orderBy('tblindustry_sub.sub_title')->orderBy('tblindustry_row.row_title');
			
			if($this->accountDetails['can_view_free_sme']==1){
				$data->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$data->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			
			//filters
			if(@count($sme_selected)>0){
				$data->whereIn('tblentity.entityid', $sme_selected);
			}
			if($main_industry_selected!=""){
				$data->where('tblentity.industry_main_id', $main_industry_selected);
			}
			if($sub_industry_selected!=""){
				$data->where('tblentity.industry_sub_id', $sub_industry_selected);
			}
			if($industry_selected!=""){
				$data->where('tblentity.industry_row_id', $industry_selected);
			}
			if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
				$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
			}
			if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
				$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
			}
			if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
				$data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
			}
			if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
				$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
			}
			if($region_selected!=""){
				$data->where('refcitymun.regDesc', $region_selected);
			}
			
			/* if($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
				//do nothing
			} else {
				$data->take(BANK_SME_LIMIT);
			} */
			$data->where('tblentity.longitude', '!=', 0)->where('tblentity.latitude', '!=', 0);
			$data = $data->get();

			return View::make('bank.map', array(
				'sme_list' => $sme_list->toArray(),
				'sme_selected' => ($sme_selected!=null) ? $sme_selected : array(),
				'main_industry_list' => $main_industry_list->toArray(),
				'main_industry_selected' => ($main_industry_selected!=null) ? $main_industry_selected : array(),
				'sub_industry_list' => $sub_industry_list->toArray(),
				'sub_industry_selected' => ($sub_industry_selected!=null) ? $sub_industry_selected : array(),
				'industry_list' => $industry_list->toArray(),
				'industry_selected' => ($industry_selected!=null) ? $industry_selected : array(),
				'region_selected' => ($region_selected!=null) ? $region_selected : array(),
				'bcc_from' => Input::get('bcc_from'),
				'bcc_to' => Input::get('bcc_to'),
				'mq_from' => Input::get('mq_from'),
				'mq_to' => Input::get('mq_to'),
				'fc_from' => Input::get('fc_from'),
				'fc_to' => Input::get('fc_to'),
				'rating_from' => Input::get('rating_from'),
				'rating_to' => Input::get('rating_to'),
				'brating_from' => Input::get('brating_from'),
				'brating_to' => Input::get('brating_to'),
				'data' => json_encode($data),
				'region_list' => $region->toArray()
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	//-----------------------------------------------------
    //  Function 38.4: _checkAllSubscriptionConditions
	//  Description: returns nothing if everything is ok, otherwise it returns the correct view to show
    //-----------------------------------------------------
    private function _checkAllSubscriptionConditions(){

    	try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();

			$supAccess = true;
			if($this->accountDetails['role'] == 4 && $organization->is_custom == 0) {
				$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",$this->accountDetails['loginid'])->get();
				foreach($supSubscription as $sup) {
					//set 30 days access for unsubscribe supervisor
					$days =  strtotime(date('c')) - strtotime($sup->updated_at);
					$days = $days/86400;
					// if less than or equal to 30 allow access
					if($days > 30 && $sup->status !== null) {
						$supAccess = false;
					}
					break;
				}
			}

			if($this->accountDetails['bypass_subscriptions_checks'] == 0){
				if ($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c')) || ($supAccess == true && $organization->is_custom == 1)) {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					/*----------------------------------------------------------
					|	Check Paypal Subscription
					|---------------------------------------------------------*/
					$subs_sts   = BankController::checkSubscriptionSts($bank_login_id);
					
					$user = User::find($bank_login_id);

					if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !(strtotime($this->bank_subscription) > strtotime(date('c')))) {
						
						$error = array('Please complete your billing details before purchasing.');
						return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
					}

					if (STS_NG == $subs_sts) {
						return Redirect::to('/bank/subscribe');
					}
					else if (2 == $subs_sts) {
						$subscription   = Transaction::where('loginid', $bank_login_id)
							->where('description', 'Bank User Subscription')
							->orderBy('id', 'desc')
							->first();

						/* Redirect to Pending Screen */
						return View::make('payment.pending', array('refno' => $subscription['refno']));
					}
					else {
						/** No Processing   */
					}
				}
				else {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					$user = User::find($bank_login_id);

					if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !(strtotime($this->bank_subscription) > strtotime(date('c')))) {
						
						$error = array('Please complete your billing details before purchasing.');
						return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
					}

					return Redirect::to('/bank/subscribe');
				}
			}  
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
}