<?php
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class DropboxController extends BaseController
{
    public function getDropboxUpload()
	{

		try{
	        $loginid = Auth::user()->loginid;

	        $userFiles = DB::table('tbldropboxuser')->where('is_deleted', 0)->where('login_id', $loginid)->get();
	        $folderUrl = DB::table('tbldropboxuser')->where('is_deleted', 0)->where('login_id', $loginid)->first();

	        if($folderUrl == null || $folderUrl->is_deleted == 1) {
	            $folderUrl = '';
	        }

	        return View::make('dropbox_upload',
	            array(
	                'userFiles' => $userFiles,
	                'folderUrl'     => $folderUrl,
	            )
	        );
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function postDropboxUpload()
    {
    	try{
	        $arraydata = Input::file('dropbox_file');			// get input file
	        $loginid = Auth::user()->loginid;         
	        //print_r($arraydata); exit;
	                       //login id

	        foreach($arraydata as $key => $value) {
	            if (($value)
	            || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['dropbox_file.'.$key] = 'required';
	            	$rules['dropbox_file.'.$key] = 'mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff';
	                $messages['dropbox_file.'.$key.'.required'] = "File is required.";
	                $messages['dropbox_file.'.$key.'.mimes'] = "Only accepts pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff files.";
	            }
	        }

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) {	// return error message if fail
	            //$srv_resp['messages']	= $validator->messages()->all();
	        	//print_r($validator->messages()->all()); exit;
	            return redirect('dropbox_upload')->with('errors', $validator->messages()->all());
	        }
	        else {

		        foreach ($arraydata as $key => $value) {

		            if($value){  
		                $orig_filename = '';
		                $fileCount = 0;
		                // $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
		                // //upload
		                // $client = new Spatie\Dropbox\Client($authorizationToken);
		                $source = $value;
		                $file = fopen($source, 'r');
		                $fileName = $value->getClientOriginalName();
		                
		                //get file count of deleted same file name
		                $sameFileDeleted = DB::table('tbldropboxuser')->where('is_deleted', 1)->where('login_id', $loginid)->where('file_name', $fileName)->first();
		                if($sameFileDeleted){
		                    DB::table('tbldropboxuser')->where('id', $sameFileDeleted->id)->increment('is_deleted');
		                    $fileCount = $sameFileDeleted->file_count;
		                }

		                //if same file exist
		                $sameFile = DB::table('tbldropboxuser')->where('is_deleted', 0)->where('login_id', $loginid)->where('file_name', $fileName)->first();
		                if($sameFile){
		                    $fileCount = $sameFile->file_count;
		                    $orig_filename = $fileName;
		                    $deletedDuplicate = DB::table('tbldropboxuser')->where('login_id', $loginid)->where('orig_filename', $orig_filename)->where('is_deleted', 1)->first();
		                    if($deletedDuplicate) {
		                        DB::table('tbldropboxuser')->where('id', $deletedDuplicate->id)->increment('is_deleted');
		                        $fileName = $deletedDuplicate->file_name;
		                    } else {
		                        $fileNumber = $fileCount + 1;
		                        $fileName = pathinfo($fileName, PATHINFO_FILENAME).' ('.$fileNumber.').'.pathinfo($fileName, PATHINFO_EXTENSION);
		                    }
		                    DB::table('tbldropboxuser')->where('is_deleted', 0)->where('login_id', $loginid)->where('file_name', $orig_filename)->increment('file_count');
		                }

		                //$uploadFile = request()->file('uploadedfile');
				        // $extension = $uploadFile->extension();
				        // $filename = $uploadFile->getClientOriginalName().$extension;

				        $folder = public_path('user-files/'.$loginid.'/');
				        
				        if (!file_exists(public_path('user-files/'))) {
				            mkdir(public_path('user-files/'.$loginid.'/'));
				        }

				        if (!file_exists($folder)) {
				            mkdir($folder,0777);
				        }

				        if($value){
					        $value->move($folder, $fileName);
				    	}   

		                // dropbox upload
		                // $client->upload('public-uploads/'.$loginid.'/'.$fileName, $file);
		                // if($client->listSharedLinks('public-uploads/'.$loginid.'/'.$fileName, true) == null){
		                //     $client->createSharedLinkWithSettings('public-uploads/'.$loginid.'/'.$fileName);
		                // }
		                
		                // $listSharedLinks = $client->listSharedLinks('public-uploads/'.$loginid.'/'.$fileName, true);
		                // if($client->listSharedLinks('public-uploads/'.$loginid, true) == null){
		                //     $client->createSharedLinkWithSettings('public-uploads/'.$loginid);
		                // }
		                //$folder = $client->listSharedLinks('public-uploads/'.$loginid, true);
		                //download link
		                //$url = $listSharedLinks[0]["url"];

				    	$listSharedLinks = 'user-files/'.$loginid.'/'.$fileName;
		                //$downloadLink = substr_replace($url, STS_OK, -1);
		                // create entry on database
		                $data = [
		                    'login_id'=> $loginid,
		                    'folder_url'=> $folder,
		                    'file_url'=> $listSharedLinks,
		                    'file_name'=> $fileName,
		                    'file_download'=> $listSharedLinks,
		                    'type'=> 'file',
		                    'orig_filename' => $fileName,
		                    'file_count' => $fileCount,
		                    'is_deleted' => 0,
		                    'created_at'=> date('Y-m-d H:i:s'),
		                    'updated_at'=> date('Y-m-d H:i:s'),
		                ];
		                DB::table('tbldropboxuser')->insertGetId($data);

		            }
		        }
	        
	        }
	        return redirect('dropbox_upload')->with('status', 'File Uploaded');
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function downloadUserFile($id){

    	try{
	    	$file = DB::table('tbldropboxuser')->where('id',$id)->first();

	    	if(filter_var($file->file_download, FILTER_VALIDATE_URL)){
	    		
	    		response()->download($file->file_download,$file->file_name);
	    	}else{
	    		echo public_path($file->file_download); 
	    		response()->download(public_path($file->file_download));
	    	}
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function postDropboxDelete()
    {
    	try{
	        //initiate variable
	        $fileId = request('fileId');
	        $userid = Auth::user()->loginid;

	        // get file details
	        $file = DB::table('tbldropboxuser')->where('id', $fileId)->first();
	        $fileName = $file->file_name;

			if($file){
				if(filter_var($file->file_download, FILTER_VALIDATE_URL)){
		            $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
		            //delete
		            $client = new Spatie\Dropbox\Client($authorizationToken);
		            $client->delete('public-uploads/'.$userid.'/'.$fileName);

		        }else{
		        	$filePath = public_path('user-files/'.$file->login_id.'/'.$fileName);
		        	if(file_exists($filePath)){
		        		unlink($filePath);
		        	}
		        }
		            //update file count of orig filename
		            $orig_filename = $file->orig_filename;
		            DB::table('tbldropboxuser')->where('login_id', $userid)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

		            // update and save
		            DB::table('tbldropboxuser')->where('id', $fileId)->update(array('is_deleted' => 1));

	        }
	        
			return redirect('dropbox_upload')->with('delete', 'File Deleted');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 20.5: getAssocFilesUpload
	//  Description: get form for FS associated files upload
    //-----------------------------------------------------
    public function getAssocFilesUpload($entity_id)
	{
		try{
			return View::make('sme.upload-assocfiles', [
				'entity_id'     => $entity_id,
			]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 20.5: getAssocFilesUpload
	//  Description: get form for FS associated files upload
    //-----------------------------------------------------
    public function getFspdfUpload($entity_id)
	{
		try{
			return View::make('sme.upload-fspdf', [
				'entity_id'     => $entity_id,
			]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function postUploadAssocFiles($entity_id)
	{
		try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $arraydata          = Input::file('dropbox_file');			// get input file

	        $messages   = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::where('entityid', $entity_id)->first();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#dropbox-upload-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-dropbox-upload';
	        }

	        /*--------------------------------------------------------------------
	        /*	Checks if the User is allowed to update the company
	        /*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        foreach($arraydata as $key => $value) {
	            if (($value)
	            || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['dropbox_file.'.$key] = 'required';
	                $messages['dropbox_file.'.$key.'.required'] = "File is required.";
	            }
	        }

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) {	// return error message if fail
	            $srv_resp['messages']	= $validator->messages()->all();
	        }
	        else {
	            foreach ($arraydata as $key=>$value) {

	                if($value){  
	                    $orig_filename = '';
	                    $fileCount = 0;
	                    $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
	                    //upload
	                    $client = new Spatie\Dropbox\Client($authorizationToken);
	                    $source = $value;
	                    $entityid = $entity_id;
	                    $file = fopen($source, 'r');
	                    $fileName = $value->getClientOriginalName();
	                    
	                    //get file count of deleted same file name
	                    $sameFileDeleted = DB::table('tbldropboxentity')->where('is_deleted', 1)->where('entity_id', $entity->entityid)->where('file_name', $fileName)->first();
	                    if($sameFileDeleted){
	                        DB::table('tbldropboxentity')->where('id', $sameFileDeleted->id)->increment('is_deleted');
	                        $fileCount = $sameFileDeleted->file_count;
	                    }

	                    //if same file exist
	                    $sameFile = DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $fileName)->first();
	                    if($sameFile){
	                        $fileCount = $sameFile->file_count;
	                        $orig_filename = $fileName;
	                        $deletedDuplicate = DB::table('tbldropboxentity')->where('entity_id', $entity->entityid)->where('orig_filename', $orig_filename)->where('is_deleted', 1)->first();
	                        if($deletedDuplicate) {
	                            DB::table('tbldropboxentity')->where('id', $deletedDuplicate->id)->increment('is_deleted');
	                            $fileName = $deletedDuplicate->file_name;
	                        } else {
	                            $fileNumber = $fileCount + 1;
	                            $fileName = pathinfo($fileName, PATHINFO_FILENAME).' ('.$fileNumber.').'.pathinfo($fileName, PATHINFO_EXTENSION);
	                        }
	                        DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $orig_filename)->increment('file_count');
	                    }

	                    // dropbox upload
	                    $client->upload('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/'.$fileName, $file);
	                    if($client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/'.$fileName, true) == null){
	                        $client->createSharedLinkWithSettings('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/'.$fileName);
	                    }
	                    
	                    $listSharedLinks = $client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/'.$fileName, true);
	                    if($client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid, true) == null){
	                        $client->createSharedLinkWithSettings('public-uploads/'.Auth::user()->loginid.'/'.$entityid);
	                    }
	                    $folder = $client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid, true);
	                    //download link
	                    $url = $listSharedLinks[0]["url"];
	                    $downloadLink = substr_replace($url, STS_OK, -1);
	                    // create entry on database
	                    $data = [
	                        'entity_id' => $entity_id,
	                        'login_id'=> Auth::user()->loginid,
	                        'folder_url'=> $folder[0]["url"],
	                        'file_url'=> $listSharedLinks[0]["url"],
	                        'file_name'=> $listSharedLinks[0]["name"],
	                        'file_download'=> $downloadLink,
	                        'type'=> $listSharedLinks[0][".tag"],
	                        'orig_filename' => $orig_filename,
	                        'file_count' => $fileCount,
	                        'is_deleted' => 0,
	                        'created_at'=> date('Y-m-d H:i:s'),
	                        'updated_at'=> date('Y-m-d H:i:s'),
	                    ];
	                    DB::table('tbldropboxentity')->insertGetId($data);
	                    $srv_resp['sts'] = STS_OK;

	                }
	            }

	            if ($srv_resp['sts'] == STS_OK) {
	                $srv_resp['messages']   = 'Successfully Added Record';
	            }
	        }

	        ActivityLog::saveLog();				// log activity to database
			return json_encode($srv_resp);		// return json results
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    public function postUploadFspdfFiles($entity_id)
	{
		try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $arraydata          = Input::file('fspdf_file');			// get input file

	        $messages   = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::where('entityid', $entity_id)->first();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#fspdf-upload-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-fspdf-upload';
	        }

	        /*--------------------------------------------------------------------
	        /*	Checks if the User is allowed to update the company
	        /*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        foreach($arraydata as $key => $value) {
	            if (($value)
	            || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['fspdf_file.'.$key] = 'required|max:40000';
	                $messages['fspdf_file.'.$key.'.required'] = "File is required.";
	                $messages['fspdf_file.'.$key.'.max'] = "The file you're uploading is too big.";
	            }
	        }

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) {	// return error message if fail
	            $srv_resp['messages']	= $validator->messages()->all();
	        }
	        else {
	            foreach ($arraydata as $key=>$value) {

	                if($value){  


	                    //create innodata folder if not exist
	                    $destinationPath = public_path('documents/innodata/to_innodata/');
	                    if (!file_exists(public_path('documents/innodata/'))) {
	                        mkdir(public_path('documents/innodata/'));
	                    }
	                    if (!file_exists($destinationPath)) {
	                        mkdir($destinationPath);
	                    }

	                    $localFileName = $entity_id."-".$value->getClientOriginalName();
	                    // save file
	                    $value->move($destinationPath, $localFileName);

	                    // create database entry
	                    $innodataFile = new InnodataFile;
	                    $innodataFile->entity_id = $entity_id;
	                    $innodataFile->file_name = $localFileName;
	                    $innodataFile->save();

	                    $orig_filename = '';
	                    $fileCount = 0;
	                    $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
	                    //upload
	                    $client = new Spatie\Dropbox\Client($authorizationToken);
	                    $source = $destinationPath.'/'.$localFileName;
	                    $entityid = $entity_id;
	                    $file = fopen($source, 'r');
	                    $fileName = $value->getClientOriginalName();
	                    
	                    //get file count of deleted same file name
	                    $sameFileDeleted = DB::table('tbldropboxfspdf')->where('is_deleted', 1)->where('entity_id', $entity->entityid)->where('file_name', $fileName)->first();
	                    if($sameFileDeleted){
	                        DB::table('tbldropboxfspdf')->where('id', $sameFileDeleted->id)->increment('is_deleted');
	                        $fileCount = $sameFileDeleted->file_count;
	                    }

	                    //if same file exist
	                    $sameFile = DB::table('tbldropboxfspdf')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $fileName)->first();
	                    if($sameFile){
	                        $fileCount = $sameFile->file_count;
	                        $orig_filename = $fileName;
	                        $deletedDuplicate = DB::table('tbldropboxfspdf')->where('entity_id', $entity->entityid)->where('orig_filename', $orig_filename)->where('is_deleted', 1)->first();
	                        if($deletedDuplicate) {
	                            DB::table('tbldropboxfspdf')->where('id', $deletedDuplicate->id)->increment('is_deleted');
	                            $fileName = $deletedDuplicate->file_name;
	                        } else {
	                            $fileNumber = $fileCount + 1;
	                            $fileName = pathinfo($fileName, PATHINFO_FILENAME).' ('.$fileNumber.').'.pathinfo($fileName, PATHINFO_EXTENSION);
	                        }
	                        DB::table('tbldropboxfspdf')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $orig_filename)->increment('file_count');
	                    }

	                    // try{
	                    //     Self::cleanupDropboxLocal();
	                    // }catch(Exception $e){
	                    //     //
	                    // }
	                    // dropbox upload
	                    $client->upload('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/FS-PDF/'.$fileName, $file);
	                    if($client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/FS-PDF/'.$fileName, true) == null){
	                        $client->createSharedLinkWithSettings('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/FS-PDF/'.$fileName);
	                    }
	                    
	                    $listSharedLinks = $client->listSharedLinks('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/FS-PDF/'.$fileName, true);
	                    //download link
	                    $url = $listSharedLinks[0]["url"];
	                    $downloadLink = substr_replace($url, STS_OK, -1);
	                    // create entry on database
	                    $data = [
	                        'entity_id' => $entity_id,
	                        'login_id'=> Auth::user()->loginid,
	                        'file_url'=> $listSharedLinks[0]["url"],
	                        'file_name'=> $listSharedLinks[0]["name"],
	                        'file_download'=> $downloadLink,
	                        'type'=> $listSharedLinks[0][".tag"],
	                        'orig_filename' => $orig_filename,
	                        'file_count' => $fileCount,
	                        'is_deleted' => 0,
	                        'created_at'=> date('Y-m-d H:i:s'),
	                        'updated_at'=> date('Y-m-d H:i:s'),
	                    ];
	                    DB::table('tbldropboxfspdf')->insertGetId($data);
	                    $srv_resp['sts'] = STS_OK;

	                }
	            }

	            if ($srv_resp['sts'] == STS_OK) {
	                $srv_resp['messages']   = 'Successfully Added Record';
	            }
	        }

	        ActivityLog::saveLog();				// log activity to database
			return json_encode($srv_resp);		// return json results
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 20.8: getDeleteDocument
	//  Description: function for deleting uploaded documents
    //-----------------------------------------------------
	public function getDeleteAssocFiles($id)
	{
		try{
			// get file details
	        $file = DB::table('tbldropboxentity')->where('id', $id)->first();
	        $fileName = $file->file_name;
	        $entityid = Session::get('entity')->entityid;
			if($file){
	            $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
	            //delete
	            $client = new Spatie\Dropbox\Client($authorizationToken);
	            $client->delete('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/'.$fileName);

	            //update file count of orig filename
	            $orig_filename = $file->orig_filename;
	            DB::table('tbldropboxentity')->where('entity_id', $entityid)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

	            // update and save
	            DB::table('tbldropboxentity')->where('id', $id)->update(array('is_deleted' => 1));
	        }
	        
			ActivityLog::saveLog();		// log activity in db
			return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 20.8: getDeleteDocument
	//  Description: function for deleting uploaded documents
    //-----------------------------------------------------
	public function getDeleteFspdfFiles($id)
	{
		try{
			// get file details
	        $file = DB::table('tbldropboxfspdf')->where('id', $id)->first();
	        $fileName = $file->file_name;
	        $entityid = Session::get('entity')->entityid;
			if($file){
	            $authorizationToken = env('APP_ENV') == "Production" ? env('DROPBOX_TOKEN_PROD') : env('DROPBOX_TOKEN_LOCAL');
	            //delete
	            $client = new Spatie\Dropbox\Client($authorizationToken);
	            $client->delete('public-uploads/'.Auth::user()->loginid.'/'.$entityid.'/FS-PDF/'.$fileName);

	            //update file count of orig filename
	            $orig_filename = $file->orig_filename;
	            DB::table('tbldropboxfspdf')->where('entity_id', $entityid)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

	            // update and save
	            DB::table('tbldropboxfspdf')->where('id', $id)->update(array('is_deleted' => 1));
	        }
	        
			ActivityLog::saveLog();		// log activity in db
			return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function cleanupDropboxProduction(){
        echo env('APP_ENV').'<br/>';
        if(env('APP_ENV') == "Production"){

            set_time_limit(0);

            $where = 'is_deleted = 0 AND is_cleanedup = 0 AND (companyname LIKE "%[] [] []%" OR companyname LIKE "%[][][]%")';

            $for_cleanup_entities = DB::table('tblentity')->whereRaw($where)->orderBy('created_at', 'desc')->groupBy('entityid')->get()->toArray();

            $ctr = 0;
            $ctr2 = 0;
            foreach ($for_cleanup_entities as $entity) {
                
                $tbldropboxfspdf = DB::table('tbldropboxfspdf')->where(['entity_id'=>$entity->entityid,'is_deleted' => 0])->get()->toArray();

                $tbldropboxentity = DB::table('tbldropboxentity')->where(['entity_id'=>$entity->entityid,'is_deleted' => 0])->get()->toArray();

                if(!empty($tbldropboxentity)){
                    
                    foreach ($tbldropboxentity as $enfile) {
                        
                        $fileName = $enfile->file_name;                       
                        $authorizationToken = env('DROPBOX_TOKEN_PROD');
                        
                        //delete
                        $client = new Spatie\Dropbox\Client($authorizationToken);

                        try{
                            $client->delete('public-uploads/'.$enfile->login_id.'/'.$enfile->entity_id.'/FS-PDF/'.$fileName);

                            sleep(10);
                        }catch(Exception $e){
                            //error here
                        }  

                        $orig_filename = $enfile->orig_filename;
                        DB::table('tbldropboxentity')->where('entity_id', $enfile->entity_id)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

                        // update and save
                        DB::table('tbldropboxentity')->where('id', $enfile->id)->update(array('is_deleted' => 1));

                        $ctr++;

                        if($ctr%100)
                            sleep(10);
                    }
                
                }

                if(!empty($tbldropboxfspdf)){
                    
                    foreach ($tbldropboxfspdf as $fspdf) {
                        
                        $fileName = $fspdf->file_name;                       
                        $authorizationToken = env('DROPBOX_TOKEN_PROD');
                        
                        //delete
                        $client = new Spatie\Dropbox\Client($authorizationToken);

                        try{
                            $client->delete('public-uploads/'.$fspdf->login_id.'/'.$fspdf->entity_id.'/FS-PDF/'.$fileName);

                            sleep(10);
                        }catch(Exception $e){
                            //error here
                        }  

                        $orig_filename = $fspdf->orig_filename;
                        DB::table('tbldropboxfspdf')->where('entity_id', $fspdf->entity_id)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

                        // update and save
                        DB::table('tbldropboxfspdf')->where('id', $fspdf->id)->update(array('is_deleted' => 1));

                        DB::table('tblentity')->where('entityid',$entity->entityid)->update(['is_cleanedup' => 1,'is_deleted' => 1]);


                        $ctr2++;

                        if($ctr2%100)
                            sleep(10);
                    }
                    
                }


                DB::table('tblentity')->where('entityid',$entity->entityid)->update(['is_cleanedup'=>1,'is_deleted' => 1]);
                

            }

        }
    }

    public function cleanupDropboxLocal(){
        echo env('APP_ENV').'<br/>';
        if(env('APP_ENV') == "local" || env('APP_ENV') == "test"){

            set_time_limit(0);

            $entities = DB::table('tblentity')->where('is_cleanedup',0)->orderBy('created_at', 'desc')->groupBy('entityid')->limit(20)->get()->toArray();

            $entity_ids = array_column($entities, 'entityid');
            $entity_ids_imp = implode(',',$entity_ids);

            $for_cleanup_entities = DB::table('tblentity')->whereRaw('entityid NOT IN('.$entity_ids_imp.') AND is_cleanedup = 0')->orderBy('created_at', 'asc')->groupBy('entityid')->get()->toArray();
            $ctr = 0;
            $ctr2 = 0;
            foreach ($for_cleanup_entities as $entity) {
                
                $tbldropboxfspdf = DB::table('tbldropboxfspdf')->where(['entity_id'=>$entity->entityid,'is_deleted' => 0])->get()->toArray();

                $tbldropboxentity = DB::table('tbldropboxentity')->where(['entity_id'=>$entity->entityid,'is_deleted' => 0])->get()->toArray();

                if(!empty($tbldropboxentity)){
                    
                    foreach ($tbldropboxentity as $enfile) {
                        
                        $fileName = $enfile->file_name;                       
                        $authorizationToken = env('DROPBOX_TOKEN_LOCAL');
                        
                        //delete
                        $client = new Spatie\Dropbox\Client($authorizationToken);

                        try{
                            $client->delete('public-uploads/'.$enfile->login_id.'/'.$enfile->entity_id.'/FS-PDF/'.$fileName);

                            //echo 'cleaned enfile'.$enfile->id.'<br/>';

                            sleep(10);
                        }catch(Exception $e){
                            //error here
                        }  

                        $orig_filename = $enfile->orig_filename;
                        DB::table('tbldropboxentity')->where('entity_id', $enfile->entity_id)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

                        // update and save
                        DB::table('tbldropboxentity')->where('id', $enfile->id)->update(array('is_deleted' => 1));

                        $ctr++;

                        if($ctr%100)
                            sleep(10);
                    }
                    

                }

                if(!empty($tbldropboxfspdf)){
                    
                    foreach ($tbldropboxfspdf as $fspdf) {
                        
                        $fileName = $fspdf->file_name;                       
                        $authorizationToken = env('DROPBOX_TOKEN_LOCAL');
                        
                        //delete
                        $client = new Spatie\Dropbox\Client($authorizationToken);

                        try{
                            $client->delete('public-uploads/'.$fspdf->login_id.'/'.$fspdf->entity_id.'/FS-PDF/'.$fileName);

                            //echo 'cleaned fspdf'.$fspdf->id.'<br/>';

                            sleep(10);
                        }catch(Exception $e){
                            //error here
                        }  

                        $orig_filename = $fspdf->orig_filename;
                        DB::table('tbldropboxfspdf')->where('entity_id', $fspdf->entity_id)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

                        // update and save
                        DB::table('tbldropboxfspdf')->where('id', $fspdf->id)->update(array('is_deleted' => 1));

                        DB::table('tblentity')->where('entityid',$entity->entityid)->update(['is_cleanedup' => 1,'is_deleted' => 1]);


                        $ctr2++;

                        if($ctr2%100)
                            sleep(10);
                    }
                    

                }


                DB::table('tblentity')->where('entityid',$entity->entityid)->update(['is_cleanedup'=>1,'is_deleted' => 1]);
                

            }

        }
    }
}