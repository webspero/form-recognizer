<?php
//======================================================================
//  Class 60    : Shufti Controller
//  Description : Functions related to Shufti APIs
//======================================================================

use Illuminate\Http\Request;

class ShuftiController extends BaseController
{
    //-----------------------------------------------------
    //  Function 60.1   : getUserForm
    //  Description     : Display Form to User
    //-----------------------------------------------------
    public function getUserForm () {
        $data["province"] = DB::table('refprovince')->orderBy('provDesc')->pluck('provDesc', 'provCode');
        $data["city"] = DB::table('refcitymun')->where('provCode', '1401')->orderBy('citymunDesc')->pluck('citymunDesc', 'citymunCode');
        return view('shufti.user_form', $data);
    }

    //-----------------------------------------------------
    //  Function 60.2   : postUserForm
    //  Description     : Save Form Data in DB
    //-----------------------------------------------------
    public function postUserForm(Request $request)
    {
        $post_data = $request->all();
        
        $messages = array( // validation message
            'birth_date.date_format'    => 'Please Enter Valid Birth Date',
            'gender.required'           => 'Please Select Gender',
            'civil_status.required'     => 'Please Select Civil Status',
            'birth_province.required'   => 'Please Select Birth Provience',
            'birth_city.required'       => 'Please Select Birth City',
            'citizenship.required'      => 'Please Enter Citizenship',
            'family_members.required'   => 'Please Enter Family Members',
            'monthly_income.required'   => 'Please Enter Monthly Income',
            'cbpo_user.required'        => 'Please Select Answer',
        );

        $rules = array( // validation rules
            'email'             => 'required|email',
            'birth_date'        => 'nullable|date_format:Y-m-d',
            'gender'            => 'required',
            'civil_status'      => 'required',
            'birth_province'    => 'required',
            'birth_city'        => 'required',
            'citizenship'       => 'required',
            'family_members'    => 'required',
            'monthly_income'    => 'required',
            'cbpo_user'         => 'required',
            'document'          => 'mimes:jpg,jpeg,png|max:16000',
            'video'             => 'mimes:mp4,mpeg|max:20000'
        );

        $validator = Validator::make($post_data, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::route('getUserForm')->withErrors($validator->errors())->withInput();
        } else {
            $user = new ShuftiUser();
            $user->email          = $post_data['email'];
            $user->last_name      = $post_data['last_name'];
            $user->first_name     = $post_data['first_name'];
            $user->middle_name    = $post_data['middle_name'];
            $user->middle_name    = $post_data['middle_name'];
            $user->sufix          = $post_data['sufix'];
            $user->birth_date     = $post_data['birth_date'];
            $user->gender         = $post_data['gender'];
            $user->civil_status   = $post_data['civil_status'];
            $user->birth_province = $post_data['birth_province'];
            $user->birth_city     = $post_data['birth_city'];
            $user->citizenship    = $post_data['citizenship'];
            $user->tin            = $post_data['tin'];
            $user->family_members = $post_data['family_members'];
            $user->monthly_income = $post_data['monthly_income'];
            $user->cards_owned    = $post_data['cards_owned'];
            $user->cars_owned     = $post_data['cars_owned'];
            $user->cbpo_user      = $post_data['cbpo_user'];
            $user->reference      = 'ref-' . rand(1, 99999) . rand(1, 9999);
            $user->status         = 'PENDING';
            $user->is_deleted     = 0;
            $user->save();

            if ($request->has('document')) {
                $file = $request->file('document');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file_path = public_path('shufti');
                $file->move($file_path, $file_name);
                $document = new ShuftiUserDocument();
                $document->user_id      = $user->id;
                $document->file_name    = $file_name;
                $document->file_path    = url('') . '/shufti/' . $file_name;
                $document->save();
            }

            if ($request->has('video')) {
                $video_file = $request->file('video');
                $video_file_name = time() . '.' . $video_file->getClientOriginalExtension();
                $video_file_path = public_path('shufti');
                $video_file->move($video_file_path, $video_file_name);
                $video = new ShuftiUserDocument();
                $video->user_id      = $user->id;
                $video->file_name    = $video_file_name;
                $video->file_path    = url('') . '/shufti/' . $video_file_name;
                $video->save();
            }
            return Redirect::route('sendShuftiRequest', $user->id);
        }
        
    }

    //-----------------------------------------------------
    //  Function 60.3   : sendShuftiRequest
    //  Description     : Send Data To Shufti API
    //-----------------------------------------------------
    public function sendShuftiRequest($id = null) {
        $user = ShuftiUser::find($id);
        $document = ShuftiUserDocument::where('user_id', $id)->first();
        if($document) {
            $url = 'https://shuftipro.com/api/';
            //your Shufti Pro account Client ID
            $client_id = env('SHUFTI_CLIENT_ID');
            //your Shufti Pro account Secret Key
            $secret_key = env('SHUFTI_SECRET_KEY');

            $verification_request = [
                'reference'         => !empty($user) ? $user->reference : 'ref-' . rand(1, 99999) . rand(1, 9999),
                'country'           => '',
                'language'          => '',
                'email'             => '',
                'callback_url'      => route('getShuftiResponse'),
                'verification_mode' => 'any',
                'show_results'      => '1',
            ];

            //Use this key if you want to perform document verification with OCR
            $verification_request['document'] = [
                'proof'                 => base64_encode(file_get_contents($document->file_path)),
                'additional_proof'      => '',
                'name'                  => '',
                'dob'                   => '',
                'gender'                => '',
                'document_number'       => '',
                'expiry_date'           => '',
                'issue_date'            => '',
                'fetch_enhanced_data'   => '0',
                'supported_types'       => ['id_card', 'passport', 'driving_license'],
            ];

            //Use this key if you want to perform address verification with OCR
            /* $verification_request['address'] = [
                'proof'                 => base64_encode(file_get_contents($document->file_path)),
                'name'                  => '',
                'full_address'          => '',
                'address_fuzzy_match'   => '1',
                'issue_date'            => '',
                'supported_types'       => ['id_card', 'driving_license', 'utility_bill', 'passport', 'bank_statement']
            ]; */

            $auth = $client_id . ":" . $secret_key; // remove this in case of Access Token
            $headers = ['Content-Type: application/json'];
            // if using Access Token then add it into headers as mentioned below otherwise remove access token
            // array_push($headers, 'Authorization: Bearer ' . $access_token); 
            $post_data = json_encode($verification_request);
            //Calling Shufti Pro request API using curl
            $response = $this->send_curl($url, $post_data, $headers, $auth); // remove $auth in case of Access Token
        } 
        return view('shufti.confirmation');
    }

    //-----------------------------------------------------
    //  Function 60.4   : getShuftiResponse
    //  Description     : Get Data From Shufti API And Update DB
    //-----------------------------------------------------
    public function getShuftiResponse(Request $request) {
        $post_data = $request->all();
        if(!empty($post_data)) {
            $reference = $post_data['reference'];
            if(!empty($reference)) {
                $user = ShuftiUser::where('reference', $reference)->first();
                if($user) {
                    switch($post_data['event']) {
                        case 'verification.invalid' :
                            $user->status = "INVALID";
                            break;
                        case 'verification.accepted':
                            $user->status = "ACCEPTED";
                            $document = ShuftiUserDocument::where('user_id', $user->id)->first();
                            if(!empty($document)) {
                                $url = 'https://api.hellosign.com/v3/signature_request/send';
                                $auth = env('HS_API_KEY');
                                $headers = ['Content-Type: application/json'];
                                $post_data = json_encode([
                                    'test_mode' => '1',
                                    'title' => 'CreditBPO Document eSignature',
                                    'subject' => 'Kindly eSign Following Document',
                                    'signers' => [
                                        'name' => $user->first_name.' '. $user->last_name,
                                        'email_address' => $user->email,
                                    ],
                                    'file_url' => [$document->file_path]
                                ]);
                                $this->send_curl($url, $post_data, $headers, $auth);
                            }
                            break;
                        case 'verification.declined':
                            $user->status = "DECLINED";
                            $user->message = $post_data['declined_reason'];
                            break;
                        case 'verification.received':
                            $user->status = "RECEIVED";
                            break;
                    }
                    $user->save();
                }
            }
        }
    }

    //-----------------------------------------------------
    //  Function 60.5   : send_curl
    //  Description     : Curl Helper for Shifti API
    //-----------------------------------------------------
    public function send_curl($url, $post_data, $headers, $auth)
    { 
        // remove $auth in case of Access Token
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERPWD, $auth); // remove this in case of Access Token
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); // remove this in case of Access Token
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $html_response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($html_response, 0, $header_size);
        $body = substr($html_response, $header_size);
        curl_close($ch);
        return ['headers' => $headers, 'body' => $body];
    }
}
