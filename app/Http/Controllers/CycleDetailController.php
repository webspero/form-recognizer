<?php
//======================================================================
//  Class 17: CycleDetailController
//  Description: Contains functions for Cash Conversion Cycle
//======================================================================
class CycleDetailController extends BaseController {

	//-----------------------------------------------------
    //  Function 17.1: getCycleDetail
	//  Description: get Cash Conversion Cycle update page
    //-----------------------------------------------------
	public function getCycleDetail($entity_id)
	{
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.$entity_id);
        }

		$created = Entity::where('entityid', $entity_id)->first();
		$cycledetails = CycleDetail::where('entity_id', $entity_id)
				->orderBy('index', 'ASC')->get();
		if($cycledetails){
			// compute for CCC (days)
			foreach($cycledetails as $key => $value){
				if($value->cost_of_sales!=0 && $value->credit_sales!=0){
					$dio = ($value->inventory / $value->cost_of_sales) * 30;
					$dso = ($value->accounts_receivable / $value->credit_sales) * 30;
					$dpo = $value->accounts_payable / ($value->cost_of_sales / 30);
					$ccc = $dio + $dso - $dpo;
					$cycledetails[$key]->ccc = $ccc <= 0 ? '-' : number_format($ccc, 2, '.', '');
				}
				else {
					$cycledetails[$key]->ccc = '-';
				}
			}
		}
		// show page
		return View::make('sme.cycledetail', array(
			'start_date' => date('Y-m-01', strtotime($created->created_at)),
			'entity' => $cycledetails,
            'entity_id' => $entity_id
		));
	}

	//-----------------------------------------------------
    //  Function 17.2: postCycleDetail
	//  Description: save Cash Conversion Cycle data
    //-----------------------------------------------------
	public function postCycleDetail($entity_id)
	{
		$return = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $return['sts']          = STS_NG;
			$return['messages'][]   = 'You are not allowed to edit this entity.';
			return json_encode($return);
        }

		// set validation rules
		for($x=0; $x<3; $x++){
			$rules['credit_sales.'.$x] = 'numeric|max:9999999999999.99';
			$rules['accounts_receivable.'.$x] = 'numeric|max:9999999999999.99';
			$rules['inventory.'.$x] = 'numeric|max:9999999999999.99';
			$rules['cost_of_sales.'.$x] = 'numeric|max:9999999999999.99';
			$rules['accounts_payable.'.$x] = 'numeric|max:9999999999999.99';
		}

		// set validation error messages
		for($x=0; $x<3; $x++){
			$message['credit_sales.'.$x.'.numeric'] = trans('validation.numeric', array('attribute'=>'Credit Sales in Month '.($x+1)));
			$message['accounts_receivable.'.$x.'.numeric'] = trans('validation.numeric', array('attribute'=>'Accounts Receivables in Month '.($x+1)));
			$message['inventory.'.$x.'.numeric'] = trans('validation.numeric', array('attribute'=>'Inventory in Month '.($x+1)));
			$message['cost_of_sales.'.$x.'.numeric'] = trans('validation.numeric', array('attribute'=>'Cost of Sales in Month '.($x+1)));
			$message['accounts_payable.'.$x.'.numeric'] = trans('validation.numeric', array('attribute'=>'Accounts Payable in Month '.($x+1)));
			$message['credit_sales.'.$x.'.max'] = trans('validation.max.numeric', array('attribute'=>'Credit Sales in Month '.($x+1)));
			$message['accounts_receivable.'.$x.'.max'] = trans('validation.max.numeric', array('attribute'=>'Accounts Receivables in Month '.($x+1)));
			$message['inventory.'.$x.'.max'] = trans('validation.max.numeric', array('attribute'=>'Inventory in Month '.($x+1)));
			$message['cost_of_sales.'.$x.'.max'] = trans('validation.max.numeric', array('attribute'=>'Cost of Sales in Month '.($x+1)));
			$message['accounts_payable.'.$x.'.max'] = trans('validation.max.numeric', array('attribute'=>'Accounts Payable in Month '.($x+1)));
		}

		// run validation
		$validator = Validator::make(Input::all(), $rules, $message);
		ActivityLog::saveLog(); 		// log activity in db

		if($validator->fails())
		{
			$return['sts']      = STS_NG;
			$return['messages'] = $validator->messages()->all();
			return json_encode($return);
		}
		else
		{
			// get input data
			$credit_sales = Input::get('credit_sales');
			$accounts_receivable = Input::get('accounts_receivable');
			$inventory = Input::get('inventory');
			$cost_of_sales = Input::get('cost_of_sales');
			$accounts_payable = Input::get('accounts_payable');

			// save input data to database
			for($x=0; $x<3; $x++){
				$cycledetail = CycleDetail::firstOrNew(array(
					'entity_id' => $entity_id,
					'index' => $x
				));
				$cycledetail->credit_sales = $credit_sales[$x];
				$cycledetail->accounts_receivable = $accounts_receivable[$x];
				$cycledetail->inventory = $inventory[$x];
				$cycledetail->cost_of_sales = $cost_of_sales[$x];
				$cycledetail->accounts_payable = $accounts_payable[$x];
				$cycledetail->save();
			}

			$return['sts']          = STS_OK;
			$return['messages']     = 'Successfully Added Record';
			return json_encode($return); // return json result
		}

	}
}
