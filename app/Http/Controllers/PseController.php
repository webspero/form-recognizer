<?php

//======================================================================
//  Class 46: PSE Controller
//      Functions related to PSE Extraction on the Administrator page
//      are routed to this class
//======================================================================
class PseController extends BaseController
{
    //-----------------------------------------------------
    //  Function 46.1: extractionPoint
    //      Displays the PSE Page
    //-----------------------------------------------------
    public function extractionPoint()
    {
        /*  Variable Declaration    */
        $pse_db             = new PseCompany;                       // PSE Company Database Class
        $pse_balsheet_db    = new PseBalSheet;                      // PSE Company Balance Sheet Class
        $companies          = $pse_db->getActivePseCompanies();     // Get All PSE Companies from database
        
        $init_data          = 1;
        $total_companies    = @count($companies);
        $new_list_exist     = self::checkPseCompanies();            // Check if new companies exists since last extraction
        
        /*  If PSE companies are already extracted  */
        if (0 < $total_companies) {
            /*  Check if Balance Sheet has been extracted   */
            $init_data      = @count($pse_balsheet_db->getPseBalSheetByPseID($companies[0]['pse_id']));
        }
        
        return View::make('pse.extraction_point',
            array(
                'companies'         => $companies,
                'init_data'         => $init_data,
                'new_list_exist'    => $new_list_exist,
                'total_companies'   => $total_companies
            )
        );
    }
    
    //-----------------------------------------------------
    //  Function 46.2: testPseExtraction
    //      Loads a company from PSE Edge for extraction
    //-----------------------------------------------------
    public function testPseExtraction($edge_no)
    {
        $url            = 'http://edge.pse.com.ph/openDiscViewer.do?'.$edge_no;
        $html_select    = $this->file_get_contents_curl($url);
        echo $html_select;
    }
    
    //-----------------------------------------------------
    //  Function 46.3: testPseExtractDoc
    //      Loads a financial report from PSE Edge for extraction
    //-----------------------------------------------------
    public function testPseExtractDoc($pse_id)
    {
        $url            = 'http://edge.pse.com.ph/companyPage/financial_reports_view.do?cmpy_id='.$pse_id;
        $html_select    = $this->file_get_contents_curl($url);
        echo $html_select;
    }
    
    //-----------------------------------------------------
    //  Function 46.4: testPseCompanyList
    //      Loads a Company list by page from PSE Edge
    //      for extraction
    //-----------------------------------------------------
    public function testPseCompanyList($page)
    {
        $url            = 'http://edge.pse.com.ph/companyDirectory/search.ax?pageNo='.$page;
        $html_select    = $this->file_get_contents_curl($url);
        
        echo $html_select;
    }

    function file_get_contents_curl($url, $retries=5)
    {
        $ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
        if (extension_loaded('curl') === true)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // The URL to fetch. This can also be set when initializing a session with curl_init().
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); // The number of seconds to wait while trying to connect.
            curl_setopt($ch, CURLOPT_USERAGENT, $ua); // The contents of the "User-Agent: " header to be used in a HTTP request.
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE); // To fail silently if the HTTP code returned is greater than or equal to 400.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // To follow any "Location: " header that the server sends as part of the HTTP header.
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE); // To automatically set the Referer: field in requests where it follows a Location: redirect.
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); // The maximum number of seconds to allow cURL functions to execute.
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5); // The maximum number of redirects
            $result = curl_exec($ch);
            curl_close($ch);
        }
        else
        {
            $result = file_get_contents($url);
        }        

        return $result;
    }
    
    //-----------------------------------------------------
    //  Function 46.5: checkPseCompanies
    //      Compares extracted companies on database
    //      with current PSE Companies
    //
    //      Companies not existing on the database are
    //      extracted
    //-----------------------------------------------------
    public function checkPseCompanies()
    {
        /*  Variable Declaration    */
        $pse_db         = new PseCompany;           // PSE Company Database Class
        $sts            = STS_OK;                   // Processing Status Flag
        $url            = 'http://edge.pse.com.ph/companyDirectory/search.ax?pageNo=1';
        $html_select    = $this->file_get_contents_curl($url);  // Open the HTML Page
        
        $total_init     = strpos($html_select, 'Total');
        
        $total_str      = STR_EMPTY;
        $idx            = (int)$total_init + 6;

        //dd(print_r($html_select, true));
        
        if($html_select != ""){
            /* Extracts the Total number of PSE Companies   */
            while (']' != $html_select[$idx]) {
                $total_str  .= $html_select[$idx];
                $idx++;
            }
        }
        
        /* Count total number of companies on the Database  */
        $extracted_companies    = $pse_db->getActivePseCompanies();
        $total_extracted_comp   = @count($extracted_companies);
        $total_pse_comp         = (int)$total_str;
        
        /*  If total company on PSE Website and total company on Database does not match    */
        if ($total_pse_comp > $total_extracted_comp) {
            $sts = STS_NG;
        }
        
        return $sts;
        
    }
    
    //-----------------------------------------------------
    //  Function 46.6: addPseCompany
    //      Adds PSE Company to the database
    //-----------------------------------------------------
    public function addPseCompany()
    {
        /*  Variable Declaration    */
        $pse_db     = new PseCompany;                   // PSE Company Database class
        $companies  = Input::get('pse-companies');      // Extracted Companies from Website
        $sts        = STS_OK;                           // Processing Status Flag
        
        /*  Saves the extracted companies to the Database   */
        foreach ($companies as $company) {
            $data['pse_id']         = $company[0];
            $data['company_name']   = $company[1];
            $data['pse_code']       = $company[2];
            
            $sts = $pse_db->addPseCompany($data);
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function 46.7: addPseAnnualReport
    //      Adds PSE Annual Report to the Database
    //-----------------------------------------------------
    public function addPseAnnualReport()
    {
        /*  Instantiate Database Classes    */
        $pse_db             = new PseCompany;
        $pse_balsheet_db    = new PseBalSheet;
        $pse_income_db      = new PseIncomeStatement;
        
        /*  Variable Declaration    */
        $sts                = STS_OK;                           // Processing Status Flag
        $bal_sheet_data     = Input::get('bal-sheet');          // Extracted Balance Sheet Data
        $income_data        = Input::get('income-statement');   // Extracted Income Statement Data
        
        /*  Fetch the specified Company from the Database   */
        $company            = $pse_db->getPseCompany($bal_sheet_data[0]);
        
        /*  Check if Balance sheet and Income Statement already exists in the Database  */
        $bal_sheet_exist    = $pse_balsheet_db->getPseBalSheetByYear($bal_sheet_data[0], $bal_sheet_data[1]);
        $income_exist       = $pse_income_db->getPseIncomeByYear($income_data[0], $income_data[1]);
        
        /*  Balance Sheet has already been extracted    */
        if ($bal_sheet_exist) {
            $sts = STS_NG;
        }
        /*  Balance Sheet extracted data is garbage     */
        else if (('NaN' == $bal_sheet_data[1]) ||
        ('cy(a' == $bal_sheet_data[1])) {
            $sts = STS_NG;
        }
        /*  Save Balance Sheet data to Database         */
        else {
            $data['pse_id']                     = $bal_sheet_data[0];
            $data['year']                       = $bal_sheet_data[1];
            $data['period']                     = $bal_sheet_data[2];
            $data['current_asset']              = str_replace(',', '', $bal_sheet_data[3]);
            $data['total_asset']                = str_replace(',', '', $bal_sheet_data[4]);
            $data['current_liabilities']        = str_replace(',', '', $bal_sheet_data[5]);
            $data['total_liabilities']          = str_replace(',', '', $bal_sheet_data[6]);
            $data['retained_earnings']          = str_replace(',', '', $bal_sheet_data[7]);
            $data['stockholders_equity']        = str_replace(',', '', $bal_sheet_data[8]);
            $data['parent_stockholders_equity'] = str_replace(',', '', $bal_sheet_data[9]);
            $data['book_value']                 = $bal_sheet_data[10];
            
            $sts = $pse_balsheet_db->addPseBalSheet($data);
        }
        
        /*  Income Statement has already been extracted */
        if ($income_exist) {
            $sts = STS_NG;
        }
        /*  Income Statement extracted data is garbage  */
        else if (('NaN' == $income_data[1]) ||
        ('cy(a' == $income_data[1])) {
            $sts = STS_NG;
        }
        /*  Save Income Statement data to Database      */
        else {
            $data['pse_id']                     = $income_data[0];
            $data['year']                       = $income_data[1];
            $data['period']                     = $income_data[2];
            $data['gross_revenue']              = str_replace(',', '', $income_data[3]);
            $data['gross_expense']              = str_replace(',', '', $income_data[4]);
            $data['net_income_before_tax']      = str_replace(',', '', $income_data[5]);
            $data['net_income_after_tax']       = str_replace(',', '', $income_data[6]);
            $data['net_income_parent_attrib']   = str_replace(',', '', $income_data[7]);
            $data['basic_earnings_per_share']   = $income_data[8];
            $data['diluted_earnings_per_share'] = $income_data[9];
            
            $sts = $pse_income_db->addPseIncome($data);
        }
        
        $company['add_status'] = $sts;
        
        /*  Encode Server response to JSON Format   */
        $company = json_encode($company);
        
        return $company;
    }
    
    //-----------------------------------------------------
    //  Function 46.8: getPseCompanyReports
    //      Gets the PSE Company Balance Sheet
    //-----------------------------------------------------
    function getPseCompanyReports($pse_id)
    {
        /*  Instantiate Database Classes    */
        $pse_balsheet_db    = new PseBalSheet;
        
        /*  Get companies Balance Sheet */
        $bal_sheets = $pse_balsheet_db->getPseBalSheetByPseID($pse_id);
        
        /*  Consolidate data in the output variable */
        $reports['pse_id']  = $pse_id;
        $reports['annuals'] = $bal_sheets;
        
        /*  Encode Server response to JSON Format   */
        $reports = json_encode($reports);
        
        return $reports;
    }
    
    //-----------------------------------------------------
    //  Function 46.9: getPseCompanyAnnualReport
    //      Gets the PSE Annual Report
    //-----------------------------------------------------
    function getPseCompanyAnnualReport()
    {
        /*  Instantiate Database Classes    */
        $pse_lib            = new PseCalculationLib;
        $pse_balsheet_db    = new PseBalSheet;
        $pse_income_db      = new PseIncomeStatement;
        
        /*  Variable Declaration    */
        $pse_id         = Input::get('pse_id'); // PSE Company ID on the database
        $cur_year       = Input::get('year');   // Year of extracted report
        $prev_year      = (int)$cur_year - 1;   // Previous year
        
        /*  Get Previous year Balance Sheet and Income Statement    */
        $prev_balsheet  = $pse_balsheet_db->getPseBalSheetByYear($pse_id, $prev_year);
        $prev_income    = $pse_income_db->getPseIncomeByYear($pse_id, $prev_year);
        
        /*  Get Curenmt year Balance Sheet and Income Statement     */
        $cur_balsheet   = $pse_balsheet_db->getPseBalSheetByYear($pse_id, $cur_year);
        $cur_income     = $pse_income_db->getPseIncomeByYear($pse_id, $cur_year);
        
        /*  Current balance sheet exists    */
        if (0 < @count($cur_balsheet)) {
            /*  Compute Current ratio when Current Liabilities is greater than 0 
            *   to avoid division by zero error
            */
            if (0 < (float)$cur_balsheet->current_liabilities) {
                $cur_balsheet['current_ratio']     = (float)$cur_balsheet->current_asset / (float)$cur_balsheet->current_liabilities;
            }
            else {
                $cur_balsheet['current_ratio']     = (float)$cur_balsheet->current_asset;
            }
            
            /*  Compute Debt to Equity ratio when Stockholder's Equity is greater than 0 
            *   to avoid division by zero error
            */
            if (0 < (float)$cur_balsheet->stockholders_equity) {
                $cur_balsheet['debt_equity_ratio'] = (float)$cur_balsheet->total_liabilities / (float)$cur_balsheet->stockholders_equity;
            }
            else {
                $cur_balsheet['debt_equity_ratio'] = (float)$cur_balsheet->total_liabilities;
            }
            
            /*  Balance Sheet for Previous year exists  */
            if (0 < @count($prev_balsheet)) {
                /*  Compute Current ratio when Current Liabilities is greater than 0 
                *   to avoid division by zero error
                */
                if (0 < (float)$prev_balsheet->current_liabilities) {
                    $prev_current_ratio         = (float)$prev_balsheet->current_asset / (float)$prev_balsheet->current_liabilities;
                }
                else {
                    $prev_current_ratio         = (float)$prev_balsheet->current_asset;
                }
                
                /*  Compute Debt to Equity ratio when Stockholder's Equity is greater than 0 
                *   to avoid division by zero error
                */
                if (0 < (float)$prev_balsheet->stockholders_equity) {
                    $prev_debt_equity_ratio     = (float)$prev_balsheet->total_liabilities / (float)$prev_balsheet->stockholders_equity;
                }
                else {
                    $prev_debt_equity_ratio     = (float)$prev_balsheet->total_liabilities;
                }
                
                $cur_balsheet['current_ratio_change']   = $pse_lib->calculatePercentChange($cur_balsheet['current_ratio'], $prev_current_ratio);
                $cur_balsheet['debt_equity_change']     = $pse_lib->calculatePercentChange($cur_balsheet['debt_equity_ratio'], $prev_debt_equity_ratio);
            }
            /*  Balance Sheet for Previous year does not exist  */
            else {
                $cur_balsheet['current_ratio_change']   = 0;
                $cur_balsheet['debt_equity_change']     = 0;
            }
            
            /*  Income Statement for Previous year exists           */
            if (0 < @count($prev_income)) {
                $cur_income['gross_revenue_change'] = $pse_lib->calculatePercentChange((float)$cur_income->gross_revenue, (float)$prev_income->gross_revenue);
                $cur_income['net_income_change']    = $pse_lib->calculatePercentChange((float)$cur_income->net_income_after_tax, (float)$prev_income->net_income_after_tax);
            }
            /*  Income Statement for Previous year does not exist   */
            else {
                $cur_income['gross_revenue_change'] = 0;
                $cur_income['net_income_change']    = 0;
            }
        }
        
        return View::make('pse.annual_report',
            array(
                'bal_sheet' => $cur_balsheet,
                'income'    => $cur_income
            )
        );
    }
}
