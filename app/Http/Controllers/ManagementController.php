<?php

use App\Countries;
use Illuminate\Support\Facades\Redirect;

//======================================================================
//  Class 37: Management Controller
//      Functions related to Directors and Shareholders on the Questionnaire
//      are routed to this class
//======================================================================
class ManagementController extends BaseController
{
    //-----------------------------------------------------
    //  Function 37.1: getShareholders
    //      Displays the Shareholders Form
    //-----------------------------------------------------
	public function getShareholders($entity_id)
	{
        $nationalities = Countries::all()->pluck('enNationality', 'enNationality');
        $entity = Entity::find($entity_id);

		return View::make('sme.shareholder',
            array(
                'entity_id' => $entity_id,
                'entity' => $entity,
                'nationalities' => $nationalities,
            )
        );
	}
	
    //-----------------------------------------------------
    //  Function 37.2: postShareholders
    //      HTTP Post request to validate and save
    //      Shareholders data in the database
    //-----------------------------------------------------
	public function postShareholders($entity_id)
	{
        /*  Variable Declaration    */
        $sharers    = Input::get('firstName');           // Shareholder firstName
        $messages   = array();                      // Validation Messages according to rules
        $rules      = array();                      // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $entity = Entity::find($entity_id);
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['refresh_cntr']   = '#management-list-cntr';
            $srv_resp['refresh_elems']  = '.reload-management';
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            if($entity->is_premium == PREMIUM_REPORT) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($sharers); $idx++) {
            
            /*  Always validate the first input set and
            *   for the succeeding validate those with Shareholder firstName
            */
            if ((STR_EMPTY != $sharers[$idx])
            || (0 == $idx)) {

                if($entity->is_premium == PREMIUM_REPORT) {
                    $messages['firstName.'.$idx.'.required']            = trans('validation.required', array('attribute'=>'First Name'));
                    $messages['lastName.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Last Name'));
                    $messages['amount.'.$idx.'.required']               = trans('validation.required', array('attribute'=>'Amount of Shareholdings'));
                    $messages['amount.'.$idx.'.numeric']                = trans('validation.numeric', array('attribute'=>'Amount of Shareholdings'));
                    $messages['amount.'.$idx.'.max']                    = trans('validation.max.numeric', array('attribute'=>'Amount of Shareholdings'));
                    $messages['percentage_share.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.numeric']      = trans('validation.numeric', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.min']          = trans('validation.min.numeric', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.max']          = trans('validation.max.numeric', array('attribute'=>'% Share'));
                    $messages['nationality.'.$idx.'.required']          = trans('validation.required', array('attribute'=>'Nationality'));
                    $messages['id_no.'.$idx.'.regex']                   = 'Invalid TIN No. Format';
                    $messages['id_no.'.$idx.'.required']                = trans('validation.required', array('attribute'=>'Tin No.'));
    
                    $rules['firstName.'.$idx]           = 'required';
                    $rules['lastName.'.$idx]            = 'required';
                    $rules['nationality.'.$idx]         = 'required';
                    $rules['amount.'.$idx]	            = 'sometimes|nullable|numeric|max:9999999999999.99';
                    $rules['percentage_share.'.$idx]    = 'sometimes|nullable|numeric|min:0|max:100';
                    $rules['id_no.'.$idx]               = array('sometimes', 'nullable', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i');
                } else {
                    $messages['firstName.'.$idx.'.required']            = trans('validation.required', array('attribute'=>'First Name'));
                    $messages['lastName.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Last Name'));
                    $messages['address.'.$idx.'.required']              = trans('validation.required', array('attribute'=>'Address'));
                    $messages['amount.'.$idx.'.required']               = trans('validation.required', array('attribute'=>'Amount of Shareholdings'));
                    $messages['amount.'.$idx.'.numeric']                = trans('validation.numeric', array('attribute'=>'Amount of Shareholdings'));
                    $messages['amount.'.$idx.'.max']                    = trans('validation.max.numeric', array('attribute'=>'Amount of Shareholdings'));
                    $messages['percentage_share.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.numeric']      = trans('validation.numeric', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.min']          = trans('validation.min.numeric', array('attribute'=>'% Share'));
                    $messages['percentage_share.'.$idx.'.max']          = trans('validation.max.numeric', array('attribute'=>'% Share'));
                    $messages['nationality.'.$idx.'.required']          = trans('validation.required', array('attribute'=>'Nationality'));
                    $messages['id_no.'.$idx.'.regex']                   = 'Invalid TIN No. Format';
                    $messages['id_no.'.$idx.'.required']                = trans('validation.required', array('attribute'=>'Tin No.'));
    
                    $rules['firstName.'.$idx]           = 'required';
                    $rules['lastName.'.$idx]            = 'required';
                    $rules['address.'.$idx]	            = 'required';
                    $rules['amount.'.$idx]	            = 'required|numeric|max:9999999999999.99';
                    $rules['percentage_share.'.$idx]    = 'required|numeric|min:0|max:100';
                    $rules['nationality.'.$idx]         = 'required';
                    $rules['id_no.'.$idx]               = array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i');
                }
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
            $firstName          = Input::get('firstName');
            $middleName         = Input::get('middleName');
            $lastName           = Input::get('lastName');
            $birthDate          = Input::get('birthDate');
			$address            = Input::get('address');
			$nationality        = Input::get('nationality');
			$amount             = Input::get('amount');
			$percentage_share   = Input::get('percentage_share');
			$id_no              = Input::get('id_no');
            /*  Saves Input to the database     */
			foreach ($firstName as $key => $value) {
				if ($value) {
					$data = array(
					    'entity_id'         => $entity_id, 
                        'firstname'         => $firstName[$key],
                        'middlename'        => $middleName[$key] == null ? '' : $middleName[$key],
                        'lastname'          => $lastName[$key],
                        'birthdate'         => $birthDate[$key] == null ? '' : $birthDate[$key],		   	
					   	'address'           => $address[$key] == null ? '' : $address[$key],
                        'nationality'       => $nationality[$key],
					   	'amount'            => $amount[$key] == null ? '' : $amount[$key],
				    	'percentage_share'  => $percentage_share[$key] == null ? '' : $percentage_share[$key],
						'id_no'             => $id_no[$key] == null ? '' : $id_no[$key],
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblshareholders')
                            ->where('id', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblshareholders')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 37.3: getShareholdersDelete
    //      HTTP GET request to delete specified Major Customer
    //          (Currently not in used)
    //-----------------------------------------------------
	public function getShareholdersDelete($id)
	{
        $shareholder = Shareholder::where('id', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->first();
        $shareholder->is_deleted = 1;
        $shareholder->save();
        
		Session::flash('redirect_tab', 'tab1');
		
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}
	
    //-----------------------------------------------------
    //  Function 37.4: getDirectors
    //      Displays the Director Form
    //-----------------------------------------------------
	public function getDirectors($entity_id)
	{
        $entity = Entity::find($entity_id);
        $nationalities = Countries::all()->pluck('enNationality', 'enNationality');
		return View::make('sme.director',
            array(
                'entity_id' => $entity_id,
                'entity' => $entity,
                'nationalities' => $nationalities,
            )
        );
	}
	
    //-----------------------------------------------------
    //  Function 37.5: postDirectors
    //      HTTP Post request to validate and save
    //      Directors data in the database
    //-----------------------------------------------------
	public function postDirectors($entity_id)
	{
        /*  Variable Declaration    */
        $directors  = Input::get('firstName');               // Customer Name
        $messages   = array();                          // Validation Messages according to rules
        $rules      = array();                          // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;      // Instance of the Malicious Attempt Library
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $entity = Entity::find($entity_id);
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#management-list-cntr';                      // Refresh the specified container
            $srv_resp['refresh_elems']  = '.reload-management';                         // Elements that will be refreshed

            if($entity->is_premium == PREMIUM_REPORT) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);    // URL to fetch data from
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($directors); $idx++) {
            
            /*  Always validate the first input set and 
            *   for the succeeding validate those with Director name
            */
            if ((STR_EMPTY != $directors[$idx])
            || (0 == $idx)) {
                
                $set = $idx + 1;
                

                if($entity->is_premium == PREMIUM_REPORT) {
                    $messages['firstName.'.$idx.'.required']                 = trans('validation.required', array('attribute'=>'First Name')).' for set '.$set;
                    $messages['lastName.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Last Name'));
                    $messages['nationality.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>'Nationality')).' for set '.$set;
                    $messages['id_no.'.$idx.'.regex']                   = 'Invalid TIN No. Format for set '.$set;
                    $messages['id_no.'.$idx.'.required']                = trans('validation.required', array('attribute'=>'Tin No.')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.numeric']	    = trans('validation.numeric', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.min']	        = trans('validation.min.numeric', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.max']	        = trans('validation.max.numeric', array('attribute'=>'% Share')).' for set '.$set;


                    $rules['firstName.'.$idx]                = 'required';
                    $rules['lastName.'.$idx]            = 'required';
                    $rules['nationality.'.$idx]	        = 'required';
                    $rules['percentage_share.'.$idx]    = 'sometimes|nullable|numeric|min:0|max:100';
                    $rules['id_no.'.$idx]               = array('sometimes', 'nullable', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i');
                } else {
                    $messages['firstName.'.$idx.'.required']                 = trans('validation.required', array('attribute'=>'First Name')).' for set '.$set;
                    $messages['lastName.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Last Name'));
                    $messages['address.'.$idx.'.required']              = trans('validation.required', array('attribute'=>'Address')).' for set '.$set;
                    $messages['nationality.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>'Nationality')).' for set '.$set;
                    $messages['id_no.'.$idx.'.regex']                   = 'Invalid TIN No. Format for set '.$set;
                    $messages['id_no.'.$idx.'.required']                = trans('validation.required', array('attribute'=>'Tin No.')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.numeric']	    = trans('validation.numeric', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.min']	        = trans('validation.min.numeric', array('attribute'=>'% Share')).' for set '.$set;
                    $messages['percentage_share.'.$idx.'.max']	        = trans('validation.max.numeric', array('attribute'=>'% Share')).' for set '.$set;


                    $rules['firstName.'.$idx]                = 'required';
                    $rules['lastName.'.$idx]            = 'required';
                    $rules['address.'.$idx]             = 'required';
                    $rules['nationality.'.$idx]	        = 'required';
                    $rules['id_no.'.$idx]               = array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i');
                    $rules['percentage_share.'.$idx]	= 'numeric|min:0|max:100';
                }
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
			/*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
            $firstName               = Input::get('firstName');
            $middleName         = Input::get('middleName');
            $lastName           = Input::get('lastName');
            $birthDate          = Input::get('birthDate');
			$address            = Input::get('address');
			$id_no              = Input::get('id_no');
			$nationality        = Input::get('nationality');
			$percentage_share   = Input::get('percentage_share');
            
            /*  Saves Input to the database     */
			foreach ($firstName as $key => $value) {
				if ($value) {
					$data = array(
					    'entity_id'         =>$entity_id, 
                        'firstName'         => $firstName[$key],
                        'middleName'        => $middleName[$key] == null ? '' : $middleName[$key],
                        'lastName'          => $lastName[$key],
                        'birthDate'         => $birthDate[$key] == null ? '' : $birthDate[$key],					   	
					   	'address'           =>$address[$key] == null ? '' : $address[$key], 
                        'nationality'       =>$nationality[$key],
				    	'percentage_share'  => $percentage_share[$key] == null ? '' : $percentage_share[$key],
						'id_no'             => $id_no[$key] == null ? '' : $id_no[$key],
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tbldirectors')
                            ->where('id', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[] =  DB::table('tbldirectors')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 37.6: getDirectorsDelete
    //      HTTP GET request to delete specified Director
    //          (Currently not in used)
    //-----------------------------------------------------
	public function getDirectorsDelete($id)
	{
        $director = Director::where('id', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->first();

        $director->is_deleted = 1;
        $director->save();
            
		Session::flash('redirect_tab', 'tab1');
        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}
}