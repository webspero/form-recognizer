<?php

use function GuzzleHttp\json_decode;

use CreditBPO\Models\Entity;
use CreditBPO\Models\Organization;
use CreditBPO\Models\ReportMatchingSME;
use CreditBPO\Models\ReportMatchingBank;
use Illuminate\Support\Facades\Redirect;
use CreditBPO\Services\MailHandler;
use CreditBPO\Services\ErrorHandler;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use CreditBPO\Models\Document;
use CreditBPO\Libraries\StringHelper;
use PseCompany as PSE;
use CreditBPO\Models\BlockAddress;
use \Exception as Exception;
use Illuminate\Support\Facades\Log;

//======================================================================
//  Class 51: Signup Controller
//      Functions related to Registration are routed to this class
//======================================================================
class SignupController extends BaseController
{
	// public function __construct(){

	// 	set_error_handler(ErrorHandler::errorRedirect());
	// }


    //-----------------------------------------------------
    //  Function 51.1: getSignupCreate
    //      Display the Signup Form
    //-----------------------------------------------------
	public function getSignupCreate()
	{
		try{
			//JM - changed list to pluck
			$data['bankList'] = Bank::all()->pluck('bank_name', 'id');
			$data['bankList'] = (array) $data['bankList'];
	        asort($data['bankList']);
			$data['cityList'] = City::all()->pluck('city', 'cityid');
			$data['provinceList'] = Province::all()->pluck('province', 'provinceid');
			$data['industryList'] = Industrymain::all()->pluck('main_title', 'main_code');
			return View::make('signup', $data);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.2: postSignupStore
    //      HTTP Post request to validate and save Registration
    //      details to the database
    //-----------------------------------------------------
	public function postSignupStore()
	{
		try {
			/** Initialize Server Response  */
			$serverResponse['sts']            = STS_NG;
			$serverResponse['messages']       = STR_EMPTY;

			$messages = array(
				'name.required' => 'The Name field is required.',
				'email.required' => 'The Email field is required.',
				'email.unique' => 'The Email you are trying to register is already taken.',
				'password.required' => 'The Password field is required.',
				'password_confirmation.required' => 'The Re-type Password field is required.'
			);

			$rules = array(
				'name'	=> 'required',
				'email'	=> 'required|unique:tbllogin,email|email|unique:supervisor_application,email',
				'password'	=> 'required|min:6|confirmed',
				'password_confirmation'	=> 'required|min:6'
			);

			if(env("APP_ENV") == "Production") {
				$recaptcha = request('recaptcha');

				if(empty($recaptcha)){
					return Redirect::to('/signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}
											
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
					'secret' => '6LdX1qUUAAAAAMBNQ9c4qEgqfQMw7pnnjGo8z0mF',
					'response' => $recaptcha
				)));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec ($ch);
				curl_close ($ch);

				$server_output = json_decode($server_output);

				/** Check if response is success */
				if(!$server_output->success){
					return Redirect::to('/signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}

				/** Check limit of score to 0.5 */
				elseif($server_output->score < 0.5){
					return Redirect::to('/signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails()) {
				if ((Input::has('action')) && (Input::has('section'))) {
					$serverResponse['messages']	= $validator->messages()->all();
					return json_encode($serverResponse);
				}
				else {
					return Redirect::route('getSignupCreate')->withErrors($validator)->withInput();
				}
			}
			else
			{
				/** Check if email is blocked */
				$is_blocked = BlockAddress::where('email_address', Input::get('email'))->first();
				if($is_blocked){
					return Redirect::route('getSignupCreate')->withErrors(['error' => "The email you're trying to register has been blocked. Please contact the site administrator for more information."])->withInput();
				}

				/* Generate activation code */
				$activationdigit = rand(00000,99999);

				$user = new User();
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->name = Input::get('name');
				$user->activation_code = $activationdigit;
				$user->activation_date = '0000-00-00';
				$user->role = 1;
				$user->status = 0;
				$user->save();

				$loginid = $user->loginid;

				if($loginid != null || $loginid != 0)
				{
					$rm_sme_id = session('rm_sme_id');
					if(!empty($rm_sme_id)){
						$sme = ReportMatchingSME::find($rm_sme_id);
						$sme->loginid = $loginid;
						$sme->save();

						session(['rm_sme_id' => '']);
					}

					$rm_bank_id = session('rm_bank_id');
					if(!empty($rm_bank_id)){
						$bank = ReportMatchingBank::find($rm_bank_id);
						$bank->loginid = $loginid;
						$bank->save();

						session(['rm_bank_id' => '']);
					}

					if (env("APP_ENV") != "local") {

						try{
							$this->sendActivationLinkEmail($activationdigit, Input::get('email'));
						}catch(Exception $ex){
			            	if (env("APP_ENV") == "Production"){
			            		Log::error($ex->getMessage());
		                   		return Redirect::to('dashboard/error');
		               		}else{
		               			throw $ex;
		               		}
			            }

						if (env("APP_ENV") == "Production") {

							try{
								Mail::send('emails.signup-notice', array('name' => Input::get('name'), 'email_add' => Input::get('email')), function($message) {
								$message->to('notify.user@creditbpo.com')
										->subject('New registered member');
								});
							}catch(Exception $ex){
				            	Log::error($ex->getMessage());
				            }
						}
					}

					if ((Input::has('action')) && (Input::has('section'))) {
						$serverResponse['sts'] = STS_OK;
						$serverResponse['messages'] = 'Successfully Added Record';
						$serverResponse['loginid'] = $user->loginid;
						return json_encode($serverResponse);
					}
					else {
						return Redirect::route('getSignupConfirmation');
					}
				}
				else
				{
					return Redirect::route('getSignupConfirmation')->with('fail', 'An error occured while creating the user. <a href="login">click here to Login Page</a>.');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
		// } catch(\Exception $e) {
		// 	$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 19f7b57");

  //           $logParams = array("activity" => "User Signup", "stackTrace" => $e, "errorCode" => "19f7b57");

		// 	AuditLogs::saveActivityLog($logParams);
			
		// 	\Log::info("19f7b57: " . $e);

		// 	return Redirect::route('getSignupCreate')->withErrors($error)->withInput();
		// }
	}

    //-----------------------------------------------------
    //  Function 51.3: getNewReport
    //      Display Create New Report Page
    //-----------------------------------------------------
	public function getNewReport($type = null)
	{
		try{
			//JM - changed list to pluck
			$banklist = Bank::pluck('bank_name', 'id');

			$data['bankList'] = $banklist->toArray();
			asort($data['bankList']);


			$data["province"] = DB::table('refprovince')->orderBy('provDesc')->pluck('provDesc', 'provCode');
			$data["city"] = DB::table('refcitymun')->where('provCode','1401')->orderBy('citymunDesc')->pluck('citymunDesc', 'citymunCode');

			$data['industry'] = Industrymain::pluck('main_title','main_code');
			$data['industrySub'] = Industrysub::where('main_code', 'A')->pluck('sub_title','sub_code');
			// $data['industryRow'] = Industryrow::where('row_status', 1)->where('industry_sub_id', 562)->pluck('row_title','industry_row_id');
			
			$div = Industrysub::where('main_code', 'A')->pluck('sub_code');			// get sub value of A
			$data['industryRow'] = DB::table('tblindustry_group')->where('sub_code', '01')->pluck('group_description', 'group_code');   // get industry
			// $data['industryRow']	= DB::table('tblindustry_class')->whereIn('group_code', $group)->pluck('class_description','class_code');
			
			$data['countries'] = [
					'PH' => 'Philippines',
					'INT' => 'International',
			];

			/** Check Report Pricing for all reports */
			$count = 0;
			foreach($data['bankList'] as $key => $value){
				$count += 1;
				if($count == 1){
					$firstBankId = $key; // Get first selected bank
				}
				continue;
			}

			// Get premium price based on selected bank and location by default
			$count = 0;
			foreach($data['province'] as $key => $value){
				$count += 1;
				if($count == 1){
					$firtProvinceId = $key; // Get first selected province
				}
				continue;
			}
			$firstRegionId = DB::table('refprovince')->where('provCode', $firtProvinceId)->pluck('regCode')->first();
			$reportPrices = $this->getPremiumReportPriceByLocationAndBank($firstRegionId, $firstBankId);
			$data['reportPrices'] = $reportPrices;

			// get available report type according to first bank selected
			$reportType = Bank::where('id', $firstBankId)->first();
			$data['reportType'] = json_decode($reportType->report_type);

			$data['isFirst'] = false;
			if ($type != null && $type == 'first'){
				$data['isFirst'] = true;
				return View::make('signup_new', $data);
			} else{
				$data['reportList'] = Entity::where('loginid', Auth::user()->loginid)->where('status', 7)->get();
				return View::make('signup_new', $data);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getDiscountOrganization(){
		$discount_code = Input::get('discount_code');
		$discount = DB::table('discounts')->where(['discount_code'=>$discount_code,'status' => 1])->first();

		if($discount)
			return $discount->bank;
		return ''; 
	}

	public function getSerialKeyOrganization(){
		$serial_key_code = Input::get('serial_key');
		$serial_key = DB::table('bank_keys')->where(['serial_key'=>$serial_key_code])->first();

		if($serial_key)
			return $serial_key->bank_id;
		return ''; 
	}

	public function checkClientKeyValidity($key,$province,$reportType)
	{

	    $result = DB::table('bank_keys')
	    ->where('serial_key', $key)
	    // ->where('is_used', 0)
	    ->first();

		$provCode = [];
		// echo $reportType; exit;

	    if(!empty($result->type)){
	        if($result->type == 'Premium Zone 1'){
	            $provCode = ['1376','1374','1375'];
	        }

	        if($result->type == 'Premium Zone 2'){
	            $provCode = ['0434','0458','0421'];
	        }

	        if($result->type == 'Premium Zone 3'){
	            $provCode = ['1376','1374','1375','0434','0458','0421'];
	        }

	        
	        if($result->type == 'Standalone' && $reportType != 0){
	            $srv_resp['serial_key'] = $key;
	            $srv_resp['report_type'] = $result->type;
	            $srv_resp['messages'] = 'This Client Key is only valid for standalone reports.';
	            return $srv_resp;          
	        }

	        if($result->type == 'Simplified' && $reportType != 2){
	            $srv_resp['serial_key'] = $key;
	            $srv_resp['report_type'] = $result->type;
	            $srv_resp['messages'] = 'This Client Key is only valid for simplified reports.';
	            return $srv_resp;          
	        }

	        if(($result->type == 'Premium Zone 1' || $result->type == 'Premium Zone 2' || $result->type == 'Premium Zone 3')  && ($reportType == 2 || $reportType == 0)){
	        	
	            $srv_resp['serial_key'] = $key;
	            $srv_resp['report_type'] = $result->type;
	            $srv_resp['messages'] = 'This Client Key is only valid for premium reports.';
	            return $srv_resp;          
	        }
	    }

		if(!empty($result->type)){
			
	        if($result->type == 'Premium Zone 1' || $result->type == 'Premium Zone 2'){
	            if(!in_array($province,$provCode)){
	                $srv_resp['serial_key'] = $key;
	                $srv_resp['report_type'] = $result->type;
	                $srv_resp['messages'] = 'That Client Key is out of zone.';
	                return $srv_resp;
	            }    
	        }elseif($result->type == 'Premium Zone 3'){
	            if(in_array($province,$provCode)){
	                $srv_resp['serial_key'] = $key;
	                $srv_resp['report_type'] = $result->type;
	                $srv_resp['messages'] = 'That Client Key is out of zone.';
	                return $srv_resp;
	            }
	        }
		}
		
	    if($result==null) {
			$srv_resp['serial_key'] = $key;
			$srv_resp['report_type'] = 'None';
			$srv_resp['messages'] = 'That Client Key is invalid.';
			return $srv_resp;
			
		}else{
			if($result->is_used == 1){
				$srv_resp['serial_key'] = $key;
				$srv_resp['report_type'] = $result->type;
				$srv_resp['messages'] = 'That Client Key has already been used.';
					return $srv_resp;
				
			}
		}

		return [];
	}

    //-----------------------------------------------------
    //  Function 51.4: postNewReport
    //      HTTP Post to validate and create a New SME Report
    //-----------------------------------------------------
	public function postNewReport($counter = null)
	{
		try {
			
			/* Check if first report for redirection purpose */
			$isFirstReport = false;
			if ($counter != null && $counter == 'first'){
				$isFirstReport = true;
			}

			/* Initialize Server Response */
			$srv_resp['sts'] = STS_NG;
			$srv_resp['messages'] = STR_EMPTY;

			if ((Input::has('action')) && (Input::has('section'))) {
				if (Input::has('login_id')) {
					$login_id = Input::get('login_id');
				}
				else {
					$srv_resp['messages'] = 'Login ID is required';
					return json_encode($srv_resp);
				}
			}			

			/* Validate required fields */
			$messages = array(
				'companyname.required' => 'The Company Name field is required.'
			);
			$rules = array(
				'companyname' => 'required'
			);
			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails()) {
				if ((!Input::has('action')) && (!Input::has('section'))) {
					if ($isFirstReport){
						return Redirect::route('getNewReport', 'first')->withErrors($validator)->withInput();
					} else{
						return Redirect::route('getNewReport')->withErrors($validator)->withInput();
					}
				} else {
					$srv_resp['messages'] = $validator->messages()->all();
					return json_encode($srv_resp);
				}
			} else {

				/* If user inputted all required fields */
				/* Stored data to variables */
				$entity_db = new Entity;
				$company_name = Input::get('companyname');
				$free_trial = 0;
				$login_id = Auth::user()->loginid;
				$validName = STS_NG;
				$serial_key = null;
				$discount_code = null;
				$pse_id = request('pse_id');

				//get city id when saving

				if(Input::get('country') == 'PH'){
					$city = DB::table('refcitymun')->where('citymunCode', Input::get('city'))->first();
					$province = DB::table('refprovince')->where('provCode', Input::get('province'))->first();
				}
				$pse_company = PseRegisteredCompany::where('id', $pse_id)->first();
				
				/* Check input data in database */
				$errors = new Illuminate\Support\MessageBag;
				
				/* Check company name validity 
				 * Comment block script to allow same company name.
				 */
				// $validName = $entity_db->checkCompanyNameValidity($login_id, $company_name);

				// if (STS_NG == $validName) {
				// 	$errors->add("companyname", 'Company Name already exist.');
				// 	if ((Input::has('action')) && (Input::has('section'))) {
				// 		$srv_resp['messages'] = 'Company Name already exist';
				// 		return json_encode($srv_resp);
				// 	}
				// }
				

				/* Check serial code */
				if(Input::has('serial_key') && Input::get('serial_key')){
					
					$serial_key = BankSerialKeys::where('serial_key', Input::get('serial_key'))->where('bank_id', Input::get('bank'))->first();

					// Check client key report type availablity
					$serial_key_bank = Bank::where('id', $serial_key->bank_id)->first();
					$bankRT = json_decode($serial_key_bank->report_type);

					// Check simplified
					if($serial_key->type == "Simplified" && $bankRT->simplified_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}

					// Check standalone
					if($serial_key->type == "Standalone" && $bankRT->standalone_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}

					// Check premium
					if(($serial_key->type == "Premium Zone 1" || $serial_key_bank->type == "Premium Zone 2" || $serial_key_bank->type == "Premium Zone 3")  && $bankRT->premium_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}


					if(Input::get('country') == 'PH'){
						
						$srv_resp_val = Self::checkClientKeyValidity(Input::get('serial_key'),Input::get('province'),Input::get('who'));

						if(!empty($srv_resp_val['messages'])){
							$errors->add("serial_key", $srv_resp_val['messages']);

							if ((Input::has('action')) && (Input::has('section'))) {
								$srv_resp['messages'] = $srv_resp_val['messages'];
								return json_encode($srv_resp);
							}		
						}
					}

					if($serial_key==null) {
						$errors->add("serial_key", 'That Client Key is invalid.');
						if ((Input::has('action')) && (Input::has('section'))) {
							$srv_resp['messages'] = 'That Client Key is invalid.';
							return json_encode($srv_resp);
						}
					}else{
						if($serial_key->is_used == 1){
							$errors->add("serial_key", 'That Client Key has already been used.');
							if ((Input::has('action')) && (Input::has('section'))) {
								$srv_resp['messages'] = 'That Client Key has already been used.';
								return json_encode($srv_resp);
							}
						}
					}

					// $today = date("Y-m-d H:i:s");	
					// if($serial_key < $today){
					// }				

					// $serial_key = BankSerialKeys::where('serial_key', Input::get('serial_key'))
					// 							->where('bank_id', Input::get('bank'))->where('is_used', 0)->first();
					// if($serial_key==null) {
					// 	$serial_key = IndependentKey::where('serial_key', Input::get('serial_key'))
					// 								->where('is_used', 0)->first();
					// 	if($serial_key==null) {
					// 		$errors->add("serial_key", 'Incorrect SME Key.');
					// 		if ((Input::has('action')) && (Input::has('section'))) {
					// 			$srv_resp['messages'] = 'Incorrect SME Key.';
					// 			return json_encode($srv_resp);
					// 		}
					// 	}
					// }
				}

				
				/* Check discount code */
				if(Input::has('discount_code') && Input::get('discount_code') ){
					$discount = Discount::where('discount_code', Input::get('discount_code'))
										->where('status', 1)->where('valid_from', '<=', date('Y-m-d H:i:s'))
										->where('valid_to', '>=', date('Y-m-d H:i:s'))->first();
					if($discount){
						$discount_code = Input::get('discount_code');

						/* Free Trials */
						if ((1 == $discount['type']) && (100 == $discount['amount'])) {
							$free_trial = 1;
						}
					} else {
						$errors->add("discount_code", 'Incorrect Discount Code.');
						if ((Input::has('action')) && (Input::has('section'))) {
							$srv_resp['messages'] = 'Incorrect Discount Code.';
							return json_encode($srv_resp);
						}
					}
				}				
				
				//print_r($errors); exit;
				/* Return to view if there's error */
				if (!$errors->isEmpty()){
					if ($isFirstReport){
						return Redirect::route('getNewReport', 'first')->withErrors($errors)->withInput();
					} else{
						return Redirect::route('getNewReport')->withErrors($errors)->withInput();
					}
				}				

				/* Check business type */
				/* Sole Proprietorship */
				if(Input::get('entity_type')=='on' || Input::get('entity_type')=='Sole Proprietorship'){
					$entity_type_selected = 1;
				}
				/* Corporation */
				else {
					$entity_type_selected = 0;
				}				

				/* Save new report */
				$entity = new Entity();
				$entity->loginid = $login_id;
				$entity->companyname = Input::get('companyname');
				$entity->entity_type = $entity_type_selected;
				// $entity->website = Input::get('website');
				// $entity->email = Input::get('email');
				$entity->is_agree = 1;
				$entity->status = 0;
				$entity->is_independent = 1;
				$entity->discount_code = $discount_code;
				$entity->is_premium = Input::get('who');
				$entity->industry_main_id = Input::get('industry_main');
				$entity->industry_sub_id = Input::get('industry_sub');
				$entity->industry_row_id = Input::get('industry_row');

				if(Input::get('country') == 'PH'){
					$entity->province = $province->provDesc;
					$entity->countrycode = Input::get('country');
					$entity->cityid = $city->id;
				}else{
					$entity->province = "Abra";
					$entity->countrycode = "";
					$entity->cityid = 1380;
				}

				$entity->isIndustry = 1;

				// pse data
				if ($pse_company) {
					$entity->pse_company_id = $pse_company->id;
					
					if ($pse_company->email_address) {
						$entity->email = $pse_company->email_address;
					}
	
					if ($pse_company->business_address) {
						$entity->address1 = $pse_company->business_address;
					}
	
					if ($pse_company->tel_num) {
						$entity->phone = $pse_company->tel_num;
					}
	
					if ($pse_company->company_description) {
						$entity->description = $pse_company->company_description;
					}
					
					if ($pse_company->website) {
						$entity->website = $pse_company->website;
					}
					
					if ($pse_company->company_description) {
						$entity->description = $pse_company->company_description;
					}

					if (($pse_company->company_tin) && ($pse_company->company_tin != "N/A")) {
						
						$entity->company_tin = $pse_company->company_tin;
					}

					if ($pse_company->postal_code) {

						$city_pse = DB::table('refcitymun')->where('zipcode', $pse_company->postal_code)->first();
						if($city_pse){
							$province_pse = DB::table('refprovince')->where('provCode',$city_pse->provCode)->first();
							$entity->cityid = $city_pse->id;
							$entity->province = $province_pse->provDesc;
							$entity->countrycode = 'PH';
							$entity->zipcode = $pse_company->postal_code;
						}
					}

					if($pse_company->sec_registration_date){
						$entity->sec_reg_date = $pse_company->sec_registration_date;
					}
				}


				if (Auth::user()->is_contractor == 1) {
					$bankReport = Organization::where('bank_name', 'Contractors Platform')->first();
					$entity->current_bank = $bankReport->id;
				} else  {
					$entity->current_bank = Input::get('bank');
				}

				if(Input::get('who') == null){
					$entity->is_premium = SIMPLIFIED_REPORT;
				}elseif(Input::get('who') == 0){
					$entity->is_premium = STANDALONE_REPORT;
				}elseif(Input::get('who') == 1){
					$entity->is_premium = PREMIUM_REPORT;
				}else{
					$entity->is_premium = Input::get('who');
				}
				
				//get zip code base on city id
				if(!empty($city)) {
					$entity->zipcode = $city->zipcode;
				}
				
				/* If free trial by using discount code or report already paid by supervisor using client key */
				if (1 == $free_trial || $serial_key) {
					$entity->is_paid = 1;
					
					/* Set user as paid */
					if ($isFirstReport){
						$userDetails = User::where('loginid', $login_id)->first();
						$userDetails->status = 2;
						$userDetails->save();
					}
				}

				// Enabled report matching by default
				$entity->matching_optin = "Yes";

				$entity->save();

				$letter_of_authorization = Input::file('letter_of_auth'); //get input file for Letter of Authorization
				
				/* move file to letter_authorization folder */
				if($letter_of_authorization && $entity->is_premium == PREMIUM_REPORT){
				    $name = $letter_of_authorization->getClientOriginalName();
				    $letter_of_authorization->move('documents/letter_authorization',$entity->entityid . ' - ' .$name);
				    $document = new Document;
				    $document->entity_id = $entity->entityid;
				    $document->is_origin = 0;
				    $document->document_type = 0;
				    $document->document_group = DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION;
				    $document->document_orig = $name;
				    $document->document_rename =  $entity->entityid . ' - ' .$name;
				    $document->upload_type = UPLOAD_TYPE_LETTER_OF_AUTHORIZATION;
				    $document->is_valid = 0;
				    $document->is_deleted = 0;
					$document->save();
					$email = Auth::user()->email;

					$cbpoPdf = public_path('documents/letter_authorization/' .$entity->entityid . ' - ' .$name ) ;

					try{
						Mail::send('emails.letter_of_authorization', 
							array('entity' => $entity),
							function($message) use ($email, $cbpoPdf) {
								$message->to($email)
										->subject('Letter of Authorization')
										->attach($cbpoPdf);
						});	
					}catch(Exception $e){
		            	//
		            }
				}

				/* Check if PSE company with pse_id exist */
				if(!empty($pse_company)) {
					/* Upload Company's Download PSE Annual Report in Financial Statement Report Section for this Report */
					$this->postUploadFspdfFilesOnCreateReport($entity->entityid, $pse_id);
				}

				/* Check if client key was used */
				if($serial_key){
					$this->serial_notification($serial_key,$login_id,  $entity);
				}

				/* Get old report information */
				$oldEntity = null;
				if(Input::has('use_previous') &&  Input::get('use_previous') == "1"){
					$oldEntity = Entity::where('entityid', Input::get('use_previous_entity'))->first();
				}

				/* Get old report information and save to new report */
				if($oldEntity != null) {
					$this->getOldReport( $entity->entityid, $oldEntity);
				}

				/* Send email notification for new report to CreditBPO admin */
				if (env("APP_ENV") == "Production"){

					try{
						Mail::send('emails.newreport-notice', array('company_name' => Input::get('companyname'), 'email_add' => Input::get('email')), function($message) {
							$message->to('notify.user@creditbpo.com', Input::get('companyname'))
							->subject('New Report Created: ' . Input::get('companyname'));
						});
					}catch(Exception $e){
		    

						if(env("APP_ENV") != "Production"){
	            		
				            throw $ex;

				        }
		            }

				}
				if($entity->is_paid == 1 && ($entity->is_premium == STANDALONE_REPORT || $entity->is_premium == SIMPLIFIED_REPORT)) {
					if ($isFirstReport && ($entity->is_premium == STANDALONE_REPORT || $entity->is_premium == SIMPLIFIED_REPORT)) {
						return View::make('signup_confirmation');
					} else{
						if ((!Input::has('action')) && (!Input::has('section'))) {
							if (Input::get('who') == STS_OK){
								
								return Redirect::to('/summary/smesummary/'.$entity->entityid);
							}
							else{
								return Redirect::route('getDashboardIndex');
							}
						}
						else {
							$srv_resp['sts'] = STS_OK;
							$srv_resp['messages'] = 'New Report Created';
							$srv_resp['master_ids'][] = $entity->entityid;
							return json_encode($srv_resp);
						}
					}
				} else {
					if ((!Input::has('action')) && (!Input::has('section')) || $entity->is_paid != 1) {
						return Redirect::intended('/payment/selection?id='.$entity->entityid);
					}
					else {
						$srv_resp['messages'] = 'Report needs payment';
						return json_encode($srv_resp);
					}

					if($entity->is_premium == PREMIUM_REPORT) {
						
						$mailHandler = new MailHandler();
						
						$mailHandler->sendPremiumReportNotification($entity);
												
						return Redirect::intended("/sme/premiumsuccess/" . $entity->entityid);
						
					}
				}
			}

		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}

		//}
		//catch(\Exception $e) {
		// 	$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 62f2487");

  //           $logParams = array("activity" => "Basic User New Report", "stackTrace" => $e, "errorCode" => "62f2487", "loginid" => Auth::user()->loginid);

		// 	AuditLogs::saveActivityLog($logParams);

		// 	\Log::info("62f2487: " . $e);

		// 	if ($isFirstReport){
		// 		return Redirect::route('getNewReport', 'first')->withErrors($error)->withInput();
		// 	} else{
		// 		return Redirect::route('getNewReport')->withErrors($error)->withInput();
		// 	}
		// }
	}

    //-----------------------------------------------------
    //  Function 51.5: getSignupConfirmation
    //      Display Signup Confirmation Page
    //-----------------------------------------------------
	public function getSignupConfirmation()
	{
		try{
			return View::make('si-confirmation');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.6: getEmailUnlock
    //      Display Email Unlock Page
    //-----------------------------------------------------
	public function getEmailUnlock()
	{
		try{
			return View::make('email-unlock');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.7: postEmailUnlock
    //      HTTP Post delete an email so it can be used
    //      again
    //-----------------------------------------------------
	public function postEmailUnlock()
	{
		try{
			$messages = array(
				'email.required' => 'The Email Address field is required.',
			);

			$rules = array(
				'email'	=> 'required|email',
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails()) {
	            return Redirect::to('/email-unlock')->withErrors($validator)->withInput();
			}
			else {
				$user = User::where('email', '=', Input::get('email')) ->first();
				if ($user) {
					$user->delete();
					return Redirect::route('getEmailUnlock')->with('success', 'The email has been deleted!');
				} else {
					return Redirect::route('getEmailUnlock')->with('fail', 'The email does not exists!');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.8: getSupervisorSignup
    //      Display the Supervisor Signup form
    //-----------------------------------------------------
    public function getSupervisorSignup()
	{
		try{
	        if (isset(Auth::user()->role)) {
	            if (0 != Auth::user()->role) {
	                return Redirect::to('/login');
	            }
	        }

	        $banks = array();

			foreach (Bank::all() as $b) {
				$banks[$b->id] = $b->bank_name;
			}

			return View::make('sup-signup',
	            array(
	                'banks' => $banks
	            )
	        );
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.9: postSupervisorSignup
    //      HTTP Request to save and validate Supervisor
    //      application
    //-----------------------------------------------------
    public function postSupervisorSignup()
	{
		try{
	        $rules          = array();
	        $messages       = array();
	        $validator      = array();
			$application_db = new SupervisorApplication;

	        $rules = array(
	            'email'             => 'required|email|unique:tbllogin,email|unique:supervisor_application,email',
		        'firstname'	        => 'required',
				'lastname'          => 'required',
				'role'              => 'required',
				'password'          => 'required',
				'confirm_password'  => 'required|same:password',
				'bank_id'           => 'required_if:role,4',
	            'valid_id_picture'  => 'required|mimes:pdf,jpeg,png',
	            //'question_type'     => 'required',
			);

	        $messages = array(
	            'email.required'            => 'Email is required',
	            'email.email'               => 'Invalid Email format',
	            'email.unique'              => 'Email has already been registered',
	            'firstname.required'        => 'First Name is required',
	            'lastname.required'         => 'Last Name is required',
	            'role.required'             => 'Organization type is required',
	            'password.required'         => 'Password type is required',
	            'confirm_password.required' => 'Confirm Password type is required',
	            'confirm_password.same'     => 'Password does not match',
	            'bank_id.required_if'       => 'Bank is required',
	            'valid_id_picture.required' => 'Government ID Upload is required',
	            'valid_id_picture.mimes'    => 'Uploaded Goverment ID should be PDF, JPEG or PNG',
	            //'question_type.required'    => 'Questionnaire Type is required',
	        );

	        // if (ENVIRONMENT != ENV_PRODUCTION) {
	        //     if (Input::has('recaptcha')) {
	        //         $ch = curl_init();

	        //         curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
	        //         curl_setopt($ch, CURLOPT_POST, 1);
	        //         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
	        //             'secret' => '6LdX1qUUAAAAAMBNQ9c4qEgqfQMw7pnnjGo8z0mF',
			// 			'response' => Input::get('recaptcha')
	        //         )));
	        //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        //         $server_output = curl_exec ($ch);
	        //         curl_close ($ch);

	        //         $server_output = json_decode($server_output);
	        //         if(!$server_output->success){
	        //            return Redirect::to('/supervisor-signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
	        //         }
	        //     }
	        //     else {
	        //         return Redirect::to('/supervisor-signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
	        //     }
	        // }

	        $validator = Validator::make(Input::all(), $rules, $messages);

	        if ($validator->fails()) {
				return Redirect::to('/supervisor-signup')->withErrors($validator->errors())->withInput();
			}
	        else {
				$input_data = Input::all();

				/*Convert bank_type to supervisor_application table data*/
				/*when all the system process that gets the bank information from supervisor_application
				were removed/changed, remove this. see CDP-948 for details*/
				$role = 0;
				switch($input_data['role']){
					case 'Bank';
						$role = 4;
						break;
					case 'Non-Bank Financing';
						$role = 5;
						break;
					case 'Corporate';
						$role = 6;
						break;
					case 'Government';
						$role = 7;
						break;
				}

				$input_data['role'] = $role;

	            /*  Saves Valid ID Picture to the Server    */
	            $input_file     = Input::file('valid_id_picture');
	            $destination    = 'images';
	            $extension      = $input_file->getClientOriginalExtension();
	            $filename       = 'sup-valid-id-'.rand(11111,99999).'.'.$extension;

	            /* Checks if a file with the same name already exists   */
	            while (file_exists($destination.'/'.$filename)) {
					$filename   = 'sup-valid-id-'.rand(11111,99999);
				}

	            $input_file->move($destination, $filename);

				$input_data['valid_id_picture'] = $filename;

	            $db_sts     = $application_db->addSupervisorApplication($input_data);

	            if (STS_OK == $db_sts) {
	                if (isset(Auth::user()->role)) {
	                    if (0 == Auth::user()->role) {
	                        return Redirect::to('/supervisor/list');
	                    }
	                    else {
	                        return Redirect::to('/login');
	                    }
	                }
	                else {
	                    if (env("APP_ENV") == "Production") {
	                    	try{
		                        Mail::send('emails.supervisor-reg-success', array('input_data' => $input_data), function($message) use ($input_data) {
		                            $message->to($input_data['email'], $input_data['email'])
		                                ->bcc('notify.user@creditbpo.com', 'CreditBPO')
		                                ->subject('CreditBPO registration successful');

		                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
		                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
								});
							}catch(Exception $e){
				            //
				            }
							
							try{
								Mail::send('emails.signup-supervisor-notice', array('input_data' => $input_data), function($message) {
									$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
										->subject('Alert: New Supervisor has registered!');
								});
							}catch(Exception $e){
				            //
				            }

	                    } else if (env("APP_ENV") != "local") {
	                    	try{
								Mail::send('emails.supervisor-reg-success', array('input_data' => $input_data), function($message) use ($input_data) {
		                            $message->to($input_data['email'], $input_data['email'])
		                                ->subject('CreditBPO registration successful');

		                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
		                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
								});
							}catch(Exception $e){
				            //
				            }
						}

	                    return Redirect::route('getSignupConfirmation')->with('success', 'Your application will now be approved by our staff. Please send any inquiries to info@creditbpo.com or notify.user@creditbpo.com.');
	                }
	            }
	            else {
	                return Redirect::route('getSignupConfirmation')->with('fail', 'Registration Failed');
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 51.10: getEula
    //      Display the EULA
    //-----------------------------------------------------
	public function getEula(){
		try{
			return View::make('eula', ['hide'=>true]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.11: getTermsOfUse
    //      Display the Terms of Use
    //-----------------------------------------------------
	public function getTermsOfUse(){
		try{
			return View::make('terms_of_use', ['hide'=>true]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.12: getPrivacyPolicy
    //      Display the Privacy Policy
    //-----------------------------------------------------
	public function getPrivacyPolicy(){
		try{
			return View::make('privacy_policy', ['hide'=>true]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.13: getOrganizationsByType
    //      functinon to get organizations ordered by type
    //-----------------------------------------------------
	public function getOrganizationsByType($type){
		try{
			$orgnanizationDb = new Bank;
			$orgnanization = $orgnanizationDb->getOrganizationByType($type);
			return $orgnanization;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.14 getOrganizationsQuestionType
    //      function to get organization question type
    //-----------------------------------------------------
	public function getOrganizationsQuestionType($bankId){
		try{
			$orgnanizationDb = new Bank;
			$questionType = $orgnanizationDb->getOrganizationQuestionType($bankId);
			return $questionType;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.15 getIndustryMain
    //      function to get Industry Main for New Report
    //-----------------------------------------------------
	public function getIndustryMain()
	{
		try{
			$industry = Industrymain::all();
			return $industry;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 51.16 getIndustrySub
    //      function to get Industry Sub for New Report
    //-----------------------------------------------------
	public function getIndustrySub($mainId)
	{
		try{
			if($mainId == false){
				return;
			}
			$where = array(
				"main_code" => $mainId
			);
			$industry = Industrysub::where($where)->get();
			return $industry;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.17 getIndustryRow
    //      function to get Industry Row for New Report
    //-----------------------------------------------------
	public function getIndustryRow($subId) 
	{	
		try{
			if($subId == false){
				return;
			}
			$group = DB::table('tblindustry_group')->where('sub_code', $subId)->get();

			// $industry = DB::table('tblindustry_class')->whereIn('group_code', $group)->get();
			return $group;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.18 getProvinceList
    //      function to get Province
    //-----------------------------------------------------
	public function getProvinceList() {
		try{
			$provinceList = Zipcode::select('major_area')->distinct()->get();
			return $provinceList;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getProvinceList2() {
		try{
			$provinceList = DB::table('refprovince')->select(['provDesc', 'provCode'])->orderBy('provDesc')->get();
			return $provinceList;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.19 getCityList
    //      function to get City List
    //-----------------------------------------------------
	public function getCityList($province) {
		try{
			$cityList = Zipcode::select('*')
						->where('major_area', $province)
						->get();
			return $cityList;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getCityList2($provCode) {
		try{
			$cityList = DB::table('refcitymun')->select(['citymunDesc', 'citymunCode'])
						->where('provCode', $provCode)
						->orderBy('citymunDesc')
						->get();
			return $cityList;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.20 getCustomReportPrice
    //      function to get Custom Report Price
    //-----------------------------------------------------
	public function getCustomReportPrice($bankName, $provinceCode) {
		try{
			// $bankName = str_replace("-", "/", $bankName);
			// $bank = Bank::where('bank_name', $bankName)->first();
			// return $bank;

			$bankName = str_replace("-", "/", $bankName);
			$bank = Bank::where('bank_name', $bankName)->first();
			$provCode = DB::table('refprovince')->where('provCode', $provinceCode)->pluck('regCode')->first();
			$reportPrice = $this->getPremiumReportPriceByLocationAndBank($provCode, $bank->id);
			return $reportPrice;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------------
    //  Function XX.XX getReportPriceInternational
    //      function to get Report Price for International Report
    //-----------------------------------------------------------
	public function getReportPriceInternational($bankName, $serialKey){
		$bankName = str_replace("-", "/", $bankName);
		$bank = Bank::where('bank_name', $bankName)->first();

		if($serialKey != false){
			$bank_key = DB::table('bank_keys')->where('serial_key', $serialKey)->where('is_used', 0)->first();
			if(!empty($bank_key)){
				$reportType = json_decode($bank->report_type);
				if($bank_key->type == "Simplified" && $reportType->simplified_type == 0){
					$reportType->simplified_type = 2;
				}
				if($bank_key->type == "Standalone" && $reportType->standalone_type == 0){
					$reportType->standalone_type = 2;
				}
				if((($bank_key->type == "Premium Zone 1") || ($bank_key->type == "Premium Zone 2") || ($bank_key->type == "Premium Zone 3")) && $reportType->premium_type == 0){
					$reportType->premium_type = 2;
				}

				$bank->report_type = json_encode($reportType);
			}
		}
		return $bank;
	}

	//-----------------------------------------------------
    //  Function 51.21 getProvincePremiumReportPrice
    //      function to get price per province
    //-----------------------------------------------------
	public function getProvincePremiumReportPrice($province) {
		try{
			$province = Zipcode::where('major_area', $province)->first();
			return $province;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.22 getCityPremiumPrice
    //      function to get price per city
    //-----------------------------------------------------
	public function getCityPremiumPrice($city) {
		try{

			$city = DB::table('refcitymun')->where('citymunCode', $city)->first();
			return response()->json($city);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.23 getProvinceCityViaCityId
    //      function to get province city by city id
    //-----------------------------------------------------
	public function getProvinceCityViaCityId($cityid){
		try{
			$data = Zipcode::where('id', $cityid)->first();
			return $data;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}


	//-----------------------------------------------------
    //  Function 51.24 getPremiumReportDetails
    //      function to get details for premium report edit
    //-----------------------------------------------------
	public function getPremiumReportDetails($entity_id){
		try{
			$entity = Entity::where('entityid', $entity_id)->first();

		 	//lists deprecated - changed to pluck
			$data['bankList'] = Bank::pluck('bank_name', 'id')->toArray();
	        asort($data['bankList']);
			
			$data['reportList'] = Entity::where('loginid', Auth::user()->loginid)->where('status', 7)->get();

			$data['countries'] = [
				'PH' => 'Philippines',
				'INT' => 'International',
			];
			
			$province = DB::table('refprovince')->orderBy('provDesc')->pluck('provDesc', 'provCode');
			$selectedProvince = DB::table('refprovince')->where('provDesc', $entity->province)->first();

			$data['cityList'] = "";
			if(!empty($selectedProvince->provCode)){
				$data['cityList'] = DB::table('refcitymun')->where('provCode', $selectedProvince->provCode)->orderBy('citymunDesc')->pluck('citymunDesc', 'citymunCode');
			}

			$industry = Industrymain::pluck('main_title','main_code');
			$industrySub = Industrysub::where('main_code', $entity->industry_main_id)->pluck('sub_title','sub_code');

			$industryRow = DB::table('tblindustry_group')->where('group_code', $entity->industry_row_id)->pluck('group_description','group_code');
			// $industryRow = DB::table('tblindustry_class')->where('group_code', $class)->pluck('class_description','class_code');
			
			$bankId = $entity->current_bank;

			if($entity->countrycode == "PH"){
				$city = DB::table('refcitymun')->where('id',$entity->cityid)->first();
				$provCode = DB::table('refprovince')->where('provDesc', $entity->province)->pluck('regCode')->first();
				$reportPrices = $this->getPremiumReportPriceByLocationAndBank($provCode, $bankId);
			}else{
				$data["city"] = DB::table('refcitymun')->where('provCode','1401')->orderBy('citymunDesc')->pluck('citymunDesc', 'citymunCode'); //select city by default

				$bankName = Bank::where('id', $bankId)->pluck('bank_name')->first();
				$internationalPrice = $this->getReportPriceInternational($bankName, false);
				$reportPrices = array(
					'simplified'	=> $internationalPrice['simplified_price'],
					'standalone'	=> $internationalPrice['standalone_price'],
					'premium'		=> $internationalPrice['premium_zone1_price']
				);
			}

			$reportType = Bank::where('id', $bankId)->first();

			return View::make('signup_edit', 
							array(
								'entity' 			=> $entity, 
								'bankList' 			=> $data['bankList'], 
								'reportList' 		=> $data['reportList'],
								'province' 			=> $province,
								'industry_main' 	=> $industry,
								'city' 				=> !(empty($data['cityList'])) ? $data['cityList']:[],
								'industrySub'		=> $industrySub,
								'industryRow'		=> $industryRow,
								'currentCity' 		=> !empty($city->citymunCode) ? $city->citymunCode:"",
								'currentProvince' 	=> !empty($selectedProvince->provCode) ? $selectedProvince->provCode:"",
								'countries' 		=> $data['countries'],
								'reportPrices'		=> $reportPrices,
								'reportType'		=> json_decode($reportType->report_type)
						)
				);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.25 postUpdateNewReport
    //      function to update report.
    //-----------------------------------------------------
	public function postUpdateNewReport(){

		try{
			$entity_id = Input::get('entity_id');
			$login_id = Auth::user()->loginid;
			/* Validate required fields */
			$messages = array(
				'companyname.required' => 'The Company Name field is required.'
			);
			$rules = array(
				'companyname' => 'required'
			);
			$validator = Validator::make(Input::all(), $rules, $messages);
			
			/* Check input data in database */
			$errors = new Illuminate\Support\MessageBag;

			if ($validator->fails()) {
				if ((!Input::has('action')) && (!Input::has('section'))) {
					return Redirect::route('getNewReport')->withErrors($validator)->withInput();
				} else {
					$srv_resp['messages'] = $validator->messages()->all();
					return json_encode($srv_resp);
				}
			}else{
				$discount_code = null;
				$free_trial = 0;
				//get city id when saving
				// $city = DB::table('refcitymun')->where('citymunCode', Input::get('city'))->first();
    //             $province = DB::table('refprovince')->where('provCode', Input::get('province'))->first();
				$login_id = Auth::user()->loginid;
				$serial_key = null;
				$free_trial = 0;
				
				$entity = Entity::find($entity_id);
				/* Check discount code */
				if(Input::has('discount_code') && request('discount_code') !== null){
					$discount = Discount::where('discount_code', Input::get('discount_code'))
										->where('status', 1)->where('valid_from', '<=', date('Y-m-d H:i:s'))
										->where('valid_to', '>=', date('Y-m-d H:i:s'))->first();
					if($discount){
						$discount_code = Input::get('discount_code');

						/* Free Trials */
						if ((1 == $discount['type']) && (100 == $discount['amount'])) {
							$free_trial = 1;
						}
					} else {
						$errors->add("discount_code", 'Incorrect Discount Code.');
						if ((Input::has('action')) && (Input::has('section'))) {
							$srv_resp['messages'] = 'Incorrect Discount Code.';
							return json_encode($srv_resp);
						}
					}
				}


				/* Check serial code */
				if(Input::has('serial_key') && (request('serial_key') != null)){
					$serial_key = BankSerialKeys::where('serial_key', Input::get('serial_key'))->where('is_used', 0)->first();
					if($serial_key==null) {
						$serial_key = IndependentKey::where('serial_key', Input::get('serial_key'))
													->where('is_used', 0)->first();
						if($serial_key==null) {
							$errors->add("serial_key", 'Incorrect SME Key.');
							if ((Input::has('action')) && (Input::has('section'))) {
								$srv_resp['messages'] = 'Incorrect SME Key.';
								return json_encode($srv_resp);
							}
						}
					}

					// Check client key report type availablity
					$serial_key_bank = Bank::where('id', $serial_key->bank_id)->first();
					$bankRT = json_decode($serial_key_bank->report_type);

					// Check simplified
					if($serial_key->type == "Simplified" && $bankRT->simplified_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}

					// Check standalone
					if($serial_key->type == "Standalone" && $bankRT->standalone_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}

					// Check premium
					if(($serial_key->type == "Premium Zone 1" || $serial_key_bank->type == "Premium Zone 2" || $serial_key_bank->type == "Premium Zone 3")  && $bankRT->premium_type == 0){
						$errors->add("serial_key", 'This Client Key report type is currently unavailable to the associated organization.');
						if ((Input::has('action')) && (Input::has('section'))){
							$srv_resp['messages'] = "This Client Key report type is currently unavailable to the associated organization.";
							return json_encode($srv_resp);
						}
					}
				}

				/* Return to view if there's error */
				if (!$errors->isEmpty()){
						return Redirect::route("getPremiumReportDetails", $entity_id)->withErrors($errors)->withInput();
				}

				/* Sole Proprietorship */
				if(Input::get('entity_type')=='on' || Input::get('entity_type')=='Sole Proprietorship' ){
					$entity_type_selected = 1;
				}
				/* Corporation */
				else {
					$entity_type_selected = 0;
				}
				
                /* If free trial by using discount code or report already paid by supervisor using client key */
                if (1 == $free_trial || $serial_key) {
                    $entity->is_paid = 1;
                }
				// /* Update new report */
				$company = Input::get('companyname');
				$bankid = Input::get('bank');
				$is_premium = Input::get('who');
				$industrymain = Input::get('industry_main');
				$industrysub = Input::get('industry_sub');
				$industryrow = Input::get('industry_row');

				$entity->companyname = $company;
				$entity->entity_type = $entity_type_selected;
				$entity->is_agree = 1;
				$entity->status = 0;
				$entity->is_independent = 1;
				$entity->discount_code = $discount_code;
				$entity->is_premium = $is_premium;
				$entity->industry_main_id = $industrymain;
				$entity->industry_sub_id = $industrysub;
				$entity->industry_row_id = $industryrow;
				if(Input::get('country') == 'PH') {
					$city = DB::table('refcitymun')->where('citymunCode', Input::get('city'))->first();
                	$province = DB::table('refprovince')->where('provCode', Input::get('province'))->first();
					$entity->province = $province->provDesc;
					$entity->countrycode = Input::get('country');
					$entity->cityid = $city->id;
				}else{
					$entity->cityid = 1380;
					$entity->province = "Abra";
					$entity->countrycode = "";
				}

				if (Auth::user()->is_contractor == 1) {
					$bankReport = Organization::where('bank_name', 'Contractors Platform')->first();
					$entity->current_bank = $bankReport->id;
				} else {
					$entity->current_bank = $bankid;
				}
				
				/* If free trial by using discount code or report already paid by supervisor using client key */
				if (1 == $free_trial || $serial_key) {
					$entity->is_paid = 1;
				}

				$entity->save();

				$letter_of_authorization = Input::file('letter_of_auth'); //get input file for Letter of Authorization
				
				/* move file to letter_authorization folder */
				if($letter_of_authorization && $entity->is_premium == PREMIUM_REPORT){
				    $name = $letter_of_authorization->getClientOriginalName();
				    $letter_of_authorization->move('documents/letter_authorization',$entity->entityid . ' - ' .$name);

					// Update old uploaded loa
					$is_uploaded = Document::where('entity_id', $entity->entityid)->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)->first();
					if($is_uploaded){
						Document::where('entity_id', $entity->entityid)->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)->update(['is_deleted'=> 1]);
					}

				    $document = new Document;
				    $document->entity_id = $entity->entityid;
				    $document->is_origin = 0;
				    $document->document_type = 0;
				    $document->document_group = DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION;
				    $document->document_orig = $name;
				    $document->document_rename =  $entity->entityid . ' - ' .$name;
				    $document->upload_type = UPLOAD_TYPE_LETTER_OF_AUTHORIZATION;
				    $document->is_valid = 0;
				    $document->is_deleted = 0;
					$document->save();
					$email = Auth::user()->email;

					$cbpoPdf = public_path('documents/letter_authorization/' .$entity->entityid . ' - ' .$name ) ;

					try{
						Mail::send('emails.letter_of_authorization', 
							array('entity' => $entity),
							function($message) use ($email, $cbpoPdf) {
								$message->to($email)
										->subject('Letter of Authorization')
										->attach($cbpoPdf);
						});	
					}catch(Exception $e){
		            	//
		            }
				}
				
				if($serial_key){
                    $this->serial_notification($serial_key, $login_id, $entity);
                }
				/* Get old report information */
				$old_entity = null;

				if(Input::has('use_previous') &&  Input::get('use_previous') == "1"){
					$old_entity = Entity::where('entityid', Input::get('use_previous_entity'))->first();
				}

				if($old_entity != null){
					$this->getOldReport($entity_id, $old_entity);
				}

				$fr = FinancialReport::where(["entity_id" => $entity_id, 'is_deleted' => 0])->first();

				// Return to payment selection if report is not paid
				if ($entity->is_paid != 1) {
					return Redirect::to('/payment/selection?id=' . $entity_id );
				}

				if( Input::get('who') == 1) {
					if($fr) {
						return Redirect::to("/sme/premiumsuccess/{$entity_id}");
					}else{
						return Redirect::intended("/sme/premiumsuccess/" . $entity->entityid);
					}
				}else {
					if((!Input::has('action')) && (!Input::has('section'))) {
                        if(Input::get('who') == STS_OK){
                            return Redirect::to('/summary/smesummary/'.$entity->entityid);
                        }else{
							return Redirect::route('getDashboardIndex');
                        }
                    }else{
                        return Redirect::route('getDashboardIndex');
                    }
				}
			}
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.25 getOldReport
    //      function to get old report.
    //-----------------------------------------------------
	public function getOldReport($entityid, $old_entity){

		try{

				$entity = Entity::find($entityid);
				
				/* Get old report information and save to new report */
					$entity->company_tin = $old_entity->company_tin;
					$entity->industry_main_id = $old_entity->industry_main_id;
					$entity->industry_sub_id = $old_entity->industry_sub_id;
					$entity->industry_row_id = $old_entity->industry_row_id;
					$entity->total_assets = $old_entity->total_assets;
					$entity->address1 = $old_entity->address1;
					$entity->address2 = $old_entity->address2;
					$entity->cityid = $old_entity->cityid;
					$entity->province = $old_entity->province;
					$entity->zipcode = $old_entity->zipcode;
					$entity->phone = $old_entity->phone;
					$entity->former_address1 = $old_entity->former_address1;
					$entity->former_address2 = $old_entity->former_address2;
					$entity->former_cityid = $old_entity->former_cityid;
					$entity->former_province = $old_entity->former_province;
					$entity->former_zipcode = $old_entity->former_zipcode;
					$entity->former_phone = $old_entity->former_phone;
					$entity->no_yrs_present_address = $old_entity->no_yrs_present_address;
					$entity->date_established = $old_entity->date_established;
					$entity->number_year = $old_entity->number_year;
					$entity->description = $old_entity->description;
					$entity->employee_size = $old_entity->employee_size;
					$entity->updated_date = $old_entity->updated_date;
					$entity->number_year_management_team = $old_entity->number_year_management_team;
					$entity->related_companies = $old_entity->related_companies;
					$entity->tin_num = $old_entity->tin_num;
					$entity->sec_num = $old_entity->sec_num;
					$entity->is_subscribe = $old_entity->is_subscribe;
					$entity->sec_reg_date = $old_entity->sec_reg_date;
					$entity->sec_cert = $old_entity->sec_cert;
					$entity->entity_type = $old_entity->entity_type;
					$entity->permit_to_operate = $old_entity->permit_to_operate;
					$entity->authority_to_borrow = $old_entity->authority_to_borrow;
					$entity->authorized_signatory = $old_entity->authorized_signatory;
					$entity->tax_registration = $old_entity->tax_registration;
					$entity->income_tax = $old_entity->income_tax;
					$entity->total_asset_grouping = $old_entity->total_asset_grouping;
					$entity->fs_input_who = $old_entity->fs_input_who;
					$entity->email = $old_entity->email;
					$entity->website = $old_entity->website;
					$entity->companyname = $old_entity->companyname;
					$entity->save();

					// Associated File
					$old_associatedFile = DB::table('tbldropboxfspdf')->where('entity_id', $old_entity->entityid)->where('is_deleted', 0)->get();
					foreach($old_associatedFile as $associated){
						$destinationPath = public_path('documents/innodata/to_innodata/'.$entity->entityid);
						if (!file_exists($destinationPath)) {
							mkdir($destinationPath);
							chmod($destinationPath, 0775);
						}

						$new_file_name = str_replace($old_entity->entityid.'-', "", $entity->entityid . '-' . $associated->file_name);
						if(file_exists(public_path('documents/innodata/to_innodata/'.$old_entity->entityid. '/'.$associated->file_name))){
							$success = \File::copy(public_path('documents/innodata/to_innodata/'.$old_entity->entityid. '/'.$associated->file_name),public_path('documents/innodata/to_innodata/'. $entity->entityid .'/'.$new_file_name));
							$data = array(
								'entity_id' 	=> $entity->entityid,
								'login_id'		=> Auth::user()->loginid,
								'file_name'		=> $new_file_name,
								'file_url'		=> url('sme/download?directory=documents/innodata/to_innodata/'.$entity->entityid.'/&filename='.basename($new_file_name)),
								'file_download'	=> url('sme/download?directory=documents/innodata/to_innodata/'.$entity->entityid.'/&filename='.basename($new_file_name)),
								'is_deleted'	=> 0,
								'created_at'    => date('Y-m-d H:i:s'),
								'updated_at'    => date('Y-m-d H:i:s'),
							);
							DB::table('tbldropboxfspdf')->insert($data);
						}
					}

					// Financial Statement Raw File
					$old_fspdf_file = DB::table('innodata_files')->where('entity_id', $old_entity->entityid)->where('is_deleted', 0)->get();
					foreach($old_fspdf_file as $fspdf){
						$destinationPath = public_path('documents/innodata/to_innodata/'.$entity->entityid);
						if (!file_exists($destinationPath)) {
							mkdir($destinationPath);
							chmod($destinationPath, 0775);
						}
						if(file_exists(public_path('documents/innodata/to_innodata/'.$old_entity->entityid. '/'.$fspdf->file_name))){
							$new_file_name = str_replace($old_entity->entityid.'-', "", $entity->entityid . '-' . $fspdf->file_name);
							$success = \File::copy(public_path('documents/innodata/to_innodata/'.$old_entity->entityid. '/'.$fspdf->file_name),public_path('documents/innodata/to_innodata/'. $entity->entityid .'/'.$new_file_name));
							$data = array(
								'entity_id' 	=> $entity->entityid,
								'file_name'		=> $new_file_name,
								'is_uploaded'	=> 0,
								'is_deleted'	=> 0,
								'created_at'    => date('Y-m-d H:i:s'),
								'updated_at'    => date('Y-m-d H:i:s'),
							);
							DB::table('innodata_files')->insert($data);
						}
					}

					// Financial Statement Input Template
					$old_fs_file = DB::table('tbldocument')->where('entity_id', $old_entity->entityid)->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)->where('is_deleted', 0)->get();
					if($old_fs_file){
						//Populate financial report details
						$old_financial_report = FinancialReport::where('entity_id', $old_entity->entityid)->where('is_deleted', 0)->first();
						if($old_financial_report){
							$old_financial_data = array(
								'entity_id'			=> $entity->entityid,
								'title'				=> $old_financial_report->title,
								'year'				=> $old_financial_report->year,
								'industry_id'   	=> $old_financial_report->industry_id,
								'money_factor'		=> $old_financial_report->money_factor,
								'currency'			=> $old_financial_report->currency,
								'step'				=> $old_financial_report->step,
								'created_at'    	=> date('Y-m-d H:i:s'),
								'updated_at'    	=> date('Y-m-d H:i:s'),
								'fstemplate_file'	=> $old_financial_report->fstemplate_file,
								'is_deleted'		=> 0
							);
							DB::table('financial_reports')->insert($old_financial_data);
						}

						//Populate ReadyRatio Data
						$old_readyratio = DB::table('tblreadyrationdata')->where('entityid',$old_entity->entityid)->first();
						if($old_readyratio){
							$old_readyratio_data = array(
								'loginid'	=> Auth::user()->loginid,
								'entityid'	=> $entity->entityid,
								'year1'		=> $old_readyratio->year1,
								'year2'		=> $old_readyratio->year2,
								'gross_revenue_growth1'	=> $old_readyratio->gross_revenue_growth1,
								'gross_revenue_growth2'	=> $old_readyratio->gross_revenue_growth2,
								'net_income_growth1'	=> $old_readyratio->net_income_growth1,
								'net_income_growth2'	=> $old_readyratio->net_income_growth2,
								'gross_profit_margin1'	=> $old_readyratio->gross_profit_margin1,
								'gross_profit_margin2'	=> $old_readyratio->gross_profit_margin2,
								'net_cash_margin1'		=> $old_readyratio->net_cash_margin1,
								'net_cash_margin2'		=> $old_readyratio->net_cash_margin1,
								'current_ratio1'		=> $old_readyratio->current_ratio2,
								'debt_equity_ratio1'	=> $old_readyratio->debt_equity_ratio1,
								'debt_equity_ratio2'	=> $old_readyratio->debt_equity_ratio2,
								'cc_year1'				=> $old_readyratio->cc_year1,
								'cc_year2'				=> $old_readyratio->cc_year2,
								'cc_year3'				=> $old_readyratio->cc_year3,
								'receivables_turnover1'	=> $old_readyratio->receivables_turnover1,
								'receivables_turnover2'	=> $old_readyratio->receivables_turnover2,
								'receivables_turnover3'	=> $old_readyratio->receivables_turnover3,
								'inventory_turnover1'	=> $old_readyratio->inventory_turnover1,
								'inventory_turnover2'	=> $old_readyratio->inventory_turnover2,
								'inventory_turnover3'	=> $old_readyratio->inventory_turnover3,
								'accounts_payable_turnover1'	=> $old_readyratio->accounts_payable_turnover1,
								'accounts_payable_turnover2'	=> $old_readyratio->accounts_payable_turnover2,
								'accounts_payable_turnover3'	=> $old_readyratio->accounts_payable_turnover3,
								'cash_conversion_cycle1'		=> $old_readyratio->cash_conversion_cycle1,
								'created_at'    	=> date('Y-m-d H:i:s'),
								'updated_at'    	=> date('Y-m-d H:i:s'),
							);
							DB::table('tblreadyrationdata')->insert($old_readyratio_data);
						}

						$current_fin_report = FinancialReport::where('entity_id', $entity->entityid)->where('is_deleted',0)->first();
						// Populate Income Statement
						$old_income_statement = DB::table('fin_income_statements')->where('financial_report_id',$old_financial_report->id)->get();
						foreach($old_income_statement as $income_statement){
							$income_statement_data = array(
								'financial_report_id'									=> $current_fin_report->id,
								'index_count'											=> $income_statement->index_count,
								'Revenue'												=> $income_statement->Revenue,
								'CostOfSales'											=> $income_statement->CostOfSales,
								'GrossProfit'											=> $income_statement->GrossProfit,
								'OtherIncome'											=> $income_statement->OtherIncome,
								'DistributionCosts'										=> $income_statement->DistributionCosts,
								'AdministrativeExpense'									=> $income_statement->AdministrativeExpense,
								'OtherExpenseByFunction'								=> $income_statement->OtherExpenseByFunction,
								'OtherGainsLosses'										=> $income_statement->OtherGainsLosses,
								'ProfitLossFromOperatingActivities'						=> $income_statement->ProfitLossFromOperatingActivities,
								'DifferenceBetweenCarryingAmountOfDividendsPayable'		=> $income_statement->DifferenceBetweenCarryingAmountOfDividendsPayable,
								'GainsLossesOnNetMonetaryPosition' 						=> $income_statement->GainsLossesOnNetMonetaryPosition,
								'GainLossArisingFromDerecognitionOfFinancialAssets'  	=> $income_statement->GainLossArisingFromDerecognitionOfFinancialAssets,
								'FinanceIncome'											=> $income_statement->FinanceIncome,
								'FinanceCosts'											=> $income_statement->FinanceCosts,
								'ShareOfProfitLossOfAssociates'							=> $income_statement->ShareOfProfitLossOfAssociates,
								'GainsLossesArisingFromDifference'						=> $income_statement->GainsLossesArisingFromDifference,
								'ProfitLossBeforeTax'									=> $income_statement->ProfitLossBeforeTax,
								'IncomeTaxExpenseContinuingOperations'					=> $income_statement->IncomeTaxExpenseContinuingOperations,
								'ProfitLossFromContinuingOperations'					=> $income_statement->ProfitLossFromContinuingOperations,
								'ProfitLossFromDiscontinuedOperations'					=> $income_statement->ProfitLossFromDiscontinuedOperations,
								'ProfitLoss'											=> $income_statement->ProfitLoss,
								'OtherComprehensiveIncome'								=> $income_statement->OtherComprehensiveIncome,
								'ComprehensiveIncome'									=> $income_statement->ComprehensiveIncome,
								'ImpairmentLoss'										=> $income_statement->ImpairmentLoss,
								'OtherIncomeFromSubsidiaries' 							=> $income_statement->OtherIncomeFromSubsidiaries,
								'CumulativeGainPreviouslyRecognized'					=> $income_statement->CumulativeGainPreviouslyRecognized,
								'HedgingGainsForHedge'									=> $income_statement->HedgingGainsForHedge,
								'DepreciationExpense'									=> $income_statement->DepreciationExpense,
								'AmortisationExpense'									=> $income_statement->AmortisationExpense,
								'DepreciationAndAmortisationExpense'					=> $income_statement->DepreciationAndAmortisationExpense,
								'Year'													=> $income_statement->Year,
								'is_deleted'											=> 0,
								'created_at'    										=> date('Y-m-d H:i:s'),
								'updated_at'    										=> date('Y-m-d H:i:s'),
							);
							DB::table('fin_income_statements')->insert($income_statement_data);
						}

						// Populate Cashflow
						$old_cashflow = DB::table('fin_cashflow')->where('financial_report_id',$old_financial_report->id)->get();
						foreach($old_cashflow as $cashflow){
							$cashflow_data = array(
								'financial_report_id'					=> $current_fin_report->id,
								'index_count'							=> $cashflow->index_count,
								'Amortization'							=> $cashflow->Amortization,
								'Depreciation'							=> $cashflow->Depreciation,
								'InterestExpense'						=> $cashflow->InterestExpense,
								'NonCashExpenses'						=> $cashflow->NonCashExpenses,
								'NetOperatingIncome'					=> $cashflow->NetOperatingIncome,
								'PrincipalPayments'						=> $cashflow->PrincipalPayments,
								'InterestPayments'						=> $cashflow->InterestPayments,
								'LeasePayments'							=> $cashflow->LeasePayments,
								'DebtServiceCapacity'					=> $cashflow->DebtServiceCapacity,
								'DebtServiceCapacityRatio'				=> $cashflow->DebtServiceCapacityRatio,
								'NetCashByOperatingActivities'			=> $cashflow->NetCashByOperatingActivities,
								'DepreciationExpense'					=> $cashflow->DepreciationExpense,
								'AmortisationExpense'					=> $cashflow->AmortisationExpense,
								'DepreciationAndAmortisationExpense'	=> $cashflow->DepreciationAndAmortisationExpense,
								'NetCashFlowFromOperations'				=> $cashflow->NetCashFlowFromOperations,
								'Year'									=> $cashflow->Year,
								'is_deleted' 							=> 0,
								'created_at'    						=> date('Y-m-d H:i:s'),
								'updated_at'    						=> date('Y-m-d H:i:s'),
							);
							DB::table('fin_cashflow')->insert($cashflow_data);
						}
						
						//Populate Balance Sheets
						$old_balance_sheets = DB::table('fin_balance_sheets')->where('financial_report_id',$old_financial_report->id)->get();
						foreach($old_balance_sheets as $balance_sheets){
							$balance_sheets_data = array(
								'financial_report_id'									=> $current_fin_report->id,
								'Year' 													=> $balance_sheets->Year,
								'CashAndCashEquivalents' 								=> $balance_sheets->CashAndCashEquivalents,
								'TradeAndOtherCurrentReceivables' 						=> $balance_sheets->TradeAndOtherCurrentReceivables,
								'Inventories' 											=> $balance_sheets->Inventories,
								'CurrentTaxAssetsCurrent' 								=> $balance_sheets->CurrentTaxAssetsCurrent,
								'CurrentBiologicalAssets' 								=> $balance_sheets->CurrentBiologicalAssets,
								'OtherCurrentFinancialAssets' 							=> $balance_sheets->OtherCurrentFinancialAssets,
								'OtherCurrentNonfinancialAssets' 						=> $balance_sheets->OtherCurrentNonfinancialAssets,
								'CurrentNoncashAssetsPledgedAsCollateral' 				=> $balance_sheets->CurrentNoncashAssetsPledgedAsCollateral,
								'NoncurrentAssetsOrDisposalGroups' 						=> $balance_sheets->NoncurrentAssetsOrDisposalGroups,
								'CurrentAssets' 										=> $balance_sheets->CurrentAssets,
								'PropertyPlantAndEquipment' 							=> $balance_sheets->PropertyPlantAndEquipment,
								'InvestmentProperty'	 								=> $balance_sheets->InvestmentProperty,
								'Goodwill' 												=> $balance_sheets->Goodwill,
								'IntangibleAssetsOtherThanGoodwill' 					=> $balance_sheets->IntangibleAssetsOtherThanGoodwill,
								'InvestmentAccountedForUsingEquityMethod' 				=> $balance_sheets->InvestmentAccountedForUsingEquityMethod,
								'InvestmentsInSubsidiariesJointVenturesAndAssociates' 	=> $balance_sheets->InvestmentsInSubsidiariesJointVenturesAndAssociates,
								'NoncurrentBiologicalAssets' 							=> $balance_sheets->NoncurrentBiologicalAssets,
								'NoncurrentReceivables' 								=> $balance_sheets->NoncurrentReceivables,
								'NoncurrentInventories' 								=> $balance_sheets->NoncurrentInventories,
								'DeferredTaxAssets' 									=> $balance_sheets->DeferredTaxAssets,
								'CurrentTaxAssetsNoncurrent'	 						=> $balance_sheets->CurrentTaxAssetsNoncurrent,
								'OtherNoncurrentFinancialAssets' 						=> $balance_sheets->OtherNoncurrentFinancialAssets,
								'OtherNoncurrentNonfinancialAssets' 					=> $balance_sheets->OtherNoncurrentNonfinancialAssets,
								'NoncurrentNoncashAssetsPledgedAsCollateral' 			=> $balance_sheets->NoncurrentNoncashAssetsPledgedAsCollateral,
								'NoncurrentAssets' 										=> $balance_sheets->NoncurrentAssets,
								'Assets' 												=> $balance_sheets->Assets,
								'CurrentProvisionsForEmployeeBenefits' 					=> $balance_sheets->CurrentProvisionsForEmployeeBenefits,
								'OtherShorttermProvisions' 								=> $balance_sheets->OtherShorttermProvisions,
								'TradeAndOtherCurrentPayables' 							=> $balance_sheets->TradeAndOtherCurrentPayables,
								'CurrentTaxLiabilitiesCurrent' 							=> $balance_sheets->CurrentTaxLiabilitiesCurrent,
								'OtherCurrentFinancialLiabilities' 						=> $balance_sheets->OtherCurrentFinancialLiabilities,
								'OtherCurrentNonfinancialLiabilities' 					=> $balance_sheets->OtherCurrentNonfinancialLiabilities,
								'LiabilitiesIncludedInDisposalGroups' 					=> $balance_sheets->LiabilitiesIncludedInDisposalGroups,
								'CurrentLiabilities' 									=> $balance_sheets->CurrentLiabilities,
								'NoncurrentProvisionsForEmployeeBenefits' 				=> $balance_sheets->NoncurrentProvisionsForEmployeeBenefits,
								'OtherLongtermProvisions' 								=> $balance_sheets->OtherLongtermProvisions,
								'NoncurrentPayables' 									=> $balance_sheets->NoncurrentPayables,
								'DeferredTaxLiabilities' 								=> $balance_sheets->DeferredTaxLiabilities,
								'CurrentTaxLiabilitiesNoncurrent' 						=> $balance_sheets->CurrentTaxLiabilitiesNoncurrent,
								'OtherNoncurrentFinancialLiabilities' 					=> $balance_sheets->OtherNoncurrentFinancialLiabilities,
								'OtherNoncurrentNonfinancialLiabilities' 				=> $balance_sheets->OtherNoncurrentNonfinancialLiabilities,
								'NoncurrentLiabilities' 								=> $balance_sheets->NoncurrentLiabilities,
								'IssuedCapital' 										=> $balance_sheets->IssuedCapital,
								'SharePremium' 											=> $balance_sheets->SharePremium,
								'TreasuryShares' 										=> $balance_sheets->TreasuryShares,
								'OtherEquityInterest' 									=> $balance_sheets->OtherEquityInterest,
								'OtherReserves' 										=> $balance_sheets->OtherReserves,
								'RetainedEarnings' 										=> $balance_sheets->RetainedEarnings,
								'NoncontrollingInterests' 								=> $balance_sheets->NoncontrollingInterests,
								'Equity' 												=> $balance_sheets->Equity,
								'Liabilities' 											=> $balance_sheets->Liabilities,
								'EquityAndLiabilities' 									=> $balance_sheets->EquityAndLiabilities,
								'created_at'    										=> date('Y-m-d H:i:s'),
								'updated_at'    										=> date('Y-m-d H:i:s'),
							);
							DB::table('fin_balance_sheets')->insert($balance_sheets_data);
						}

						//Populate smescore
						$old_smescore = DB::table('tblsmescore')->where('entityid',$old_entity->entityid)->orderBy('smescoreid', 'desc')->first();
						if($old_smescore){
							$smescore_data = array(
								'loginid'					=> Auth::user()->loginid,
								'entityid'					=> $old_smescore->entityid,
								'score_rm'					=> $old_smescore->score_rm,
								'score_cd'					=> $old_smescore->score_cd,
								'score_sd'					=> $old_smescore->score_sd,
								'score_bois'				=> $old_smescore->score_bois,
								'score_boe'					=> $old_smescore->score_boe,
								'score_mte'					=> $old_smescore->score_mte,
								'score_bdms'				=> $old_smescore->score_bdms,
								'score_sp'					=> $old_smescore->score_sp,
								'score_ppfi'				=> $old_smescore->score_ppfi,
								'score_rfp'					=> $old_smescore->score_rfp,
								'score_rfpm'				=> $old_smescore->score_rfpm,
								'score_fars'				=> $old_smescore->score_fars,
								'score_facs'				=> $old_smescore->score_facs,
								'score_sf'					=> $old_smescore->score_sf,
								'average_daily_balance'		=> $old_smescore->average_daily_balance,
								'operating_cashflow_margin'	=> $old_smescore->operating_cashflow_margin,
								'operating_cashflow_ratio'	=> $old_smescore->operating_cashflow_ratio,
								'positive_points'			=> $old_smescore->positive_points,
								'negative_points'			=> $old_smescore->negative_points,
								'created_at'    			=> date('Y-m-d H:i:s'),
								'updated_at'    			=> date('Y-m-d H:i:s'),
							);
							DB::table('tblsmescore')->insert($smescore_data);
						}

						foreach($old_fs_file as $fs_file){
							if(file_exists(public_path('documents/'.$fs_file->document_rename))){
								$success = \File::copy(public_path('documents/'.$fs_file->document_rename),public_path('documents/'.$entity->entityid.'_-_Balance_Sheet_Income_Statement-'. date('Y-m-d'). '.zip'));

								$document_orig = '';
								if (strpos($fs_file->document_orig, 'Full FS Input Template') !== false){
									$document_orig = 'Full FS Input Template';
								}else{
									$document_orig = 'Basic FS Input Tempplate';
								}

								// Populate document details to the database	
								$document_data = array(
									'entity_id'		=> $entity->entityid,
									'is_origin'		=> 0,
									'document_type'	=> 1,
									'document_group'	=> DOCUMENT_GROUP_FS_TEMPLATE,
									'document_orig'		=> $document_orig . ' - ' . $entity->companyname . ' as of ' . date('Y-m-d') . '.xls',
									'document_rename'	=> $entity->entityid.'_-_Balance_Sheet_Income_Statement-'. date('Y-m-d'). '.zip',
									'created_at'    => date('Y-m-d H:i:s'),
									'updated_at'    => date('Y-m-d H:i:s'),
									'upload_type'	=> 5,
									'is_valid'		=> 0,
									'is_deleted'	=> 0
								);
								DB::table('tbldocument')->insert($document_data);
							}
						}
					}

					// Bank Statements
					$old_bank_statements = DB::table('tbldocument')->where('entity_id', $old_entity->entityid)->where('document_group', DOCUMENT_GROUP_BANK_STATEMENT)->where('is_deleted', 0)->get();
					foreach($old_bank_statements as $bank_statements){
						$destinationPath = public_path('documents/'.$entity->entityid);
						if (!file_exists($destinationPath)) {
							mkdir($destinationPath);
							chmod($destinationPath, 0775);
						}
						$bank_file_name = str_replace($old_entity->entityid . '/', "", $bank_statements->document_rename);
						if(file_exists(public_path('documents/'.$old_entity->entityid . '/'. $bank_file_name))){
							$success = \File::copy(public_path('documents/'.$old_entity->entityid . '/'. $bank_file_name),public_path('documents/'. $entity->entityid . '/' . $bank_file_name));
							$bankStatement_data = array(
								'entity_id'			=> $entity->entityid,
								'is_origin'			=> 0,
								'document_type' 	=> 0,
								'document_group'	=> DOCUMENT_GROUP_BANK_STATEMENT,
								'document_orig'		=> $bank_statements->document_orig,
								'document_rename'	=> $entity->entityid . '/' . $bank_file_name,
								'created_at'    => date('Y-m-d H:i:s'),
								'updated_at'    => date('Y-m-d H:i:s'),
								'upload_type'	=> 0,
								'is_valid'		=> 0,
								'is_deleted'	=> 0
							);
							DB::table('tbldocument')->insert($bankStatement_data);
						}
					}

					//Utility Bills
					$old_bank_statements = DB::table('tbldocument')->where('entity_id', $old_entity->entityid)->where('document_group', DOCUMENT_GROUP_UTILITY_BILLS)->where('is_deleted', 0)->get();
					foreach($old_bank_statements as $bank_statements){
						$destinationPath = public_path('documents/'.$entity->entityid);
						if (!file_exists($destinationPath)) {
							mkdir($destinationPath);
							chmod($destinationPath, 0775);
						}
						$bank_file_name = str_replace($old_entity->entityid . '/', "", $bank_statements->document_rename);

						if(file_exists(public_path('documents/'.$old_entity->entityid . '/'. $bank_file_name))){
							$success = \File::copy(public_path('documents/'.$old_entity->entityid . '/'. $bank_file_name),public_path('documents/'. $entity->entityid . '/' . $bank_file_name));
							$bankStatement_data = array(
								'entity_id'			=> $entity->entityid,
								'is_origin'			=> 0,
								'document_type' 	=> 0,
								'document_group'	=> DOCUMENT_GROUP_UTILITY_BILLS,
								'document_orig'		=> $bank_statements->document_orig,
								'document_rename'	=> $entity->entityid . '/' . $bank_file_name,
								'created_at'    => date('Y-m-d H:i:s'),
								'updated_at'    => date('Y-m-d H:i:s'),
								'upload_type'	=> 0,
								'is_valid'		=> 0,
								'is_deleted'	=> 0
							);
							DB::table('tbldocument')->insert($bankStatement_data);
						}
					}

					// major_customers
					$major_customers = Majorcustomer::where('entity_id', $old_entity->entityid)->get();
					foreach($major_customers as $row){
						$data = array(
							'entity_id' =>$entity->entityid,
							'customer_name' => $row->customer_name,
							'customer_share_sales' => $row->customer_share_sales,
							'customer_address' => $row->customer_address,
							'customer_contact_person' => $row->customer_contact_person,
							'customer_contact_details' => $row->customer_contact_details,
							'customer_email' => $row->customer_email,
							'customer_phone' => $row->customer_phone,
							'customer_experience' => $row->customer_experience,
							'customer_started_years' => $row->customer_started_years,
							'customer_years_doing_business' => $row->customer_years_doing_business,
							'customer_settlement' => $row->customer_settlement,
							'customer_order_frequency' => $row->customer_order_frequency,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						DB::table('tblmajorcustomer')->insert($data);
					}

					$major_cust_account = DB::table('tblmajorcustomeraccount')
						->where('entity_id', $old_entity->entityid)
						->first();
					

					if (is_countable($major_cust_account) && 0 < count($major_cust_account)) {
						if (STS_OK == $major_cust_account->is_agree) {
							$new_major_cust_account             = new Majorcustomeraccount();

							$new_major_cust_account->entity_id  = $entity->entityid;
							$new_major_cust_account->is_agree   = STS_OK;
							$new_major_cust_account->save();
						}
					}

					// major_supplier
					$major_supplier = Majorsupplier::where('entity_id', $old_entity->entityid)->get();
					foreach($major_supplier as $row){
						$data = array(
							'entity_id' =>$entity->entityid,
							'supplier_name' => $row->supplier_name,
							'supplier_share_sales' => $row->supplier_share_sales,
							'supplier_address' => $row->supplier_address,
							'supplier_contact_person' => $row->supplier_contact_person,
							'supplier_contact_details' => $row->supplier_contact_details,
							'supplier_email' => $row->supplier_email,
							'supplier_phone' => $row->supplier_phone,
							'supplier_experience' => $row->supplier_experience,
							'supplier_started_years' => $row->supplier_started_years,
							'supplier_years_doing_business' => $row->supplier_years_doing_business,
							'supplier_settlement' => $row->supplier_settlement,
							'supplier_order_frequency' => $row->supplier_order_frequency,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						DB::table('tblmajorsupplier')->insert($data);
					}

					$major_supp_account = DB::table('tblmajorsupplieraccount')
						->where('entity_id', $old_entity->entityid)
						->first();

					if (is_countable($major_supp_account) && 0 < count($major_supp_account)) {
						if (STS_OK == $major_supp_account->is_agree) {
							$new_major_supp_account             = new Majorsupplieraccount();

							$new_major_supp_account->entity_id  = $entity->entityid;
							$new_major_supp_account->is_agree   = STS_OK;
							$new_major_supp_account->save();
						}
					}

					// planfacility
					$planfacility = Planfacility::where('entity_id', $old_entity->entityid)->first();
					if($planfacility){
						$data = new Planfacility();
						$data->entity_id                        = $entity->entityid;
						$data->lending_institution              = $planfacility->lending_institution;
						$data->plan_credit_availment            = $planfacility->plan_credit_availment;
						$data->cash_deposit_details             = $planfacility->cash_deposit_details;
						$data->cash_deposit_amount              = $planfacility->cash_deposit_amount;
						$data->securities_details               = $planfacility->securities_details;
						$data->securities_estimated_value       = $planfacility->securities_estimated_value;
						$data->property_details                 = $planfacility->property_details;
						$data->property_estimated_value         = $planfacility->property_estimated_value;
						$data->chattel_details                  = $planfacility->chattel_details;
						$data->chattel_estimated_value          = $planfacility->chattel_estimated_value;
						$data->others_details                   = $planfacility->others_details;
						$data->others_estimated_value           = $planfacility->others_estimated_value;
						$data->purpose_credit_facility          = $planfacility->purpose_credit_facility;
						$data->credit_terms                     = $planfacility->credit_terms;
						$data->experience                       = $planfacility->experience;
						$data->other_remarks                    = $planfacility->other_remarks;
						$data->outstanding_balance              = $planfacility->outstanding_balance;
						$data->purpose_credit_facility_others   = $planfacility->purpose_credit_facility_others;
						$data->created_at						= date('Y-m-d H:i:s');
						$data->updated_at						= date('Y-m-d H:i:s');
						$data->save();
					}

					// planfacilityrequested
					$planfacilityrequested = PlanfacilityRequested::where('entity_id', $old_entity->entityid)->first();
					if($planfacilityrequested){
						$data = new PlanfacilityRequested();
						$data->entity_id = $entity->entityid;
						$data->plan_credit_availment = $planfacilityrequested->plan_credit_availment;
						$data->cash_deposit_details = $planfacilityrequested->cash_deposit_details;
						$data->cash_deposit_amount = $planfacilityrequested->cash_deposit_amount;
						$data->securities_details = $planfacilityrequested->securities_details;
						$data->securities_estimated_value = $planfacilityrequested->securities_estimated_value;
						$data->property_details = $planfacilityrequested->property_details;
						$data->property_estimated_value = $planfacilityrequested->property_estimated_value;
						$data->chattel_details = $planfacilityrequested->chattel_details;
						$data->chattel_estimated_value = $planfacilityrequested->chattel_estimated_value;
						$data->others_details = $planfacilityrequested->others_details;
						$data->others_estimated_value = $planfacilityrequested->others_estimated_value;
						$data->purpose_credit_facility = $planfacilityrequested->purpose_credit_facility;
						$data->purpose_credit_facility_others = $planfacilityrequested->purpose_credit_facility_others;
						$data->credit_terms = $planfacilityrequested->credit_terms;
						$data->outstanding_balance = $planfacilityrequested->outstanding_balance;
						$data->experience = $planfacilityrequested->experience;
						$data->other_remarks = $planfacilityrequested->other_remarks;
						$data->created_at						= date('Y-m-d H:i:s');
						$data->updated_at						= date('Y-m-d H:i:s');
						$data->save();
					}

					// sustainability
					$sustainability = Sustainability::where('entity_id', $old_entity->entityid)->first();
					if($sustainability){
						$data = new Sustainability();
						$data->entity_id = $entity->entityid;
						$data->succession_plan = $sustainability->succession_plan;
						$data->succession_timeframe = $sustainability->succession_timeframe;
						$data->competition_details = $sustainability->competition_details;
						$data->competition_landscape = $sustainability->competition_landscape;
						$data->competition_timeframe = $sustainability->competition_timeframe;
						$data->ev_susceptibility_economic_recession = $sustainability->ev_susceptibility_economic_recession;
						$data->ev_foreign_exchange_interest_sensitivity = $sustainability->ev_foreign_exchange_interest_sensitivity;
						$data->ev_commodity_price_volatility = $sustainability->ev_commodity_price_volatility;
						$data->ev_governement_regulation = $sustainability->ev_governement_regulation;
						$data->cg_structure = $sustainability->cg_structure;
						$data->mt_executive_team = $sustainability->mt_executive_team;
						$data->mt_middle_management = $sustainability->mt_middle_management;
						$data->mt_staff = $sustainability->mt_staff;
						$data->crm_Structure = $sustainability->crm_Structure;
						$data->bpsf_Regular_cycle_business = $sustainability->bpsf_Regular_cycle_business;
						$data->tax_payments = $sustainability->tax_payments;
						$data->save();
					}

					//risk_assessment
					$risk_assessment = Riskassessment::where('entity_id', $old_entity->entityid)->get();
					foreach($risk_assessment as $row){
						$data = array(
							'entity_id' =>$entity->entityid,
							'risk_assessment_name' => $row->risk_assessment_name,
							'risk_assessment_solution' => $row->risk_assessment_solution,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						DB::table('tblriskassessment')->insert($data);
					}

					//revenue_growth
					$revenue_growth = Revenuegrowth::where('entity_id', $old_entity->entityid)->get();
					foreach($revenue_growth as $row){
						$data = array(
							'entity_id' =>$entity->entityid,
							'current_market' => $row->current_market,
							'new_market' => $row->new_market,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						DB::table('tblrevenuegrowthpotential')->insert($data);
					}

					//capacityexpansion
					$capacityexpansion = Capacityexpansion::where('entity_id', $old_entity->entityid)->get();
					foreach($capacityexpansion as $row){
						$data = array(
							'entity_id' =>$entity->entityid,
							'current_year' => $row->current_year,
							'new_year' => $row->new_year,
							'within_five_years' => $row->within_five_years,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						DB::table('tblcapacityexpansion')->insert($data);
					}

					if($entity->is_premium == PREMIUM_REPORT){
						// related company
						$related_company = Relatedcompanies::where('entity_id', $old_entity->entityid)->get();
						foreach($related_company as $row){
							$data = array(
								'entity_id'             	=>$entity->entityid,
								'related_company_name'      =>$row->related_company_name,
								'related_company_address1'  =>$row->related_company_address1,
								'related_company_address2'  =>$row->related_company_address2,
								'related_company_cityid'    =>$row->related_company_cityid,
								'related_company_province'  =>$row->related_company_province,
								'related_company_zipcode'   =>$row->related_company_zipcode,
								'related_company_phone'     =>$row->related_company_phone,
								'related_company_email'     =>$row->related_company_email,
								'created_at'            	=>date('Y-m-d H:i:s'),
								'updated_at'            	=>date('Y-m-d H:i:s')
							);
							DB::table('tblrelatedcompanies')->insert($data);
						}

						// services offer
						$servicesoffer = Servicesoffer::where('entity_id', $old_entity->entityid)->get();
						foreach($servicesoffer as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'servicesoffer_name' =>$row->servicesoffer_name,
								'servicesoffer_targetmarket' =>$row->servicesoffer_targetmarket,
								'servicesoffer_share_revenue' =>$row->servicesoffer_share_revenue,
								'servicesoffer_seasonality' =>$row->servicesoffer_seasonality,
								'created_at'            =>date('Y-m-d H:i:s'),
								'updated_at'            =>date('Y-m-d H:i:s')
							);
							DB::table('tblservicesoffer')->insert($data);
						}
						// capital
						$capital = Capital::where('entity_id', $old_entity->entityid)->get();
						foreach($capital as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'capital_authorized' =>$row->capital_authorized,
								'capital_issued' =>$row->capital_issued,
								'capital_paid_up' =>$row->capital_paid_up,
								'capital_ordinary_shares' =>$row->capital_ordinary_shares,
								'capital_par_value' =>$row->capital_par_value,
								'created_at'            =>date('Y-m-d H:i:s'),
								'updated_at'            =>date('Y-m-d H:i:s')
							);
							DB::table('tblcapital')->insert($data);
						}

						//locations
						$locations = Locations::where('entity_id', $old_entity->entityid)->get();
						foreach($locations as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'location_size' =>$row->location_size,
								'location_type' =>$row->location_type,
								'location_map' =>$row->location_map,
								'location_used' =>$row->location_used,
								'created_at'            =>date('Y-m-d H:i:s'),
								'updated_at'            =>date('Y-m-d H:i:s')
							);
							DB::table('tbllocations')->insert($data);
						}

						// branches
						$branches = Branches::where('entity_id', $old_entity->entityid)->get();
						foreach($branches as $row){
							$data = array(
								'entity_id'             =>$entity->entityid,
								'branch_address'        =>$row->branch_address,
								'branch_owned_leased'   =>$row->branch_owned_leased,
								'branch_contact_person' =>$row->branch_contact_person,
								'branch_phone'          =>$row->branch_phone,
								'branch_email'          =>$row->branch_email,
								'branch_job_title'      =>$row->branch_job_title,
								'created_at'            =>date('Y-m-d H:i:s'),
								'updated_at'            =>date('Y-m-d H:i:s')
							);
							DB::table('tblbranches')->insert($data);
						}

						// shareholders
						$shareholders = Shareholder::where('entity_id', $old_entity->entityid)->get();
						foreach($shareholders as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'name' => $row->name,
								'address' => $row->address,
								'amount' => $row->amount,
								'percentage_share' => $row->percentage_share,
								'id_no' => $row->id_no,
								'nationality' => $row->nationality,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblshareholders')->insert($data);
						}

						// directors
						$directors = Director::where('entity_id', $old_entity->entityid)->get();
						foreach($directors as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'name' => $row->name,
								'address' => $row->address,
								'id_no' => $row->id_no,
								'percentage_share' => $row->percentage_share,
								'nationality' => $row->nationality,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tbldirectors')->insert($data);
						}

						// certifications
						$certifications = Certifications::where('entity_id', $old_entity->entityid)->get();
						foreach($certifications as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'certification_details' => $row->certification_details,
								'certification_reg_no' => $row->certification_reg_no,
								'certification_reg_date' => $row->certification_reg_date,
								'certification_doc' => $row->certification_doc,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblcertifications')->insert($data);
						}

						// businesstype
						$businesstype = Businesstype::where('entity_id', $old_entity->entityid)->get();
						foreach($businesstype as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'is_import' => $row->is_import,
								'is_export' => $row->is_export,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblbusinesstype')->insert($data);
						}

						// insurance
						$insurance = Insuarance::where('entity_id', $old_entity->entityid)->get();
						foreach($insurance as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'insurance_type' => $row->insurance_type,
								'insured_amount' => $row->insured_amount,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblinsurance')->insert($data);
						}

						// cycledetail
						$cycledetail = CycleDetail::where('entity_id', $old_entity->entityid)->get();
						foreach($cycledetail as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'index' => $row->index,
								'credit_sales' => $row->credit_sales,
								'accounts_receivable' => $row->accounts_receivable,
								'inventory' => $row->inventory,
								'cost_of_sales' => $row->cost_of_sales,
								'accounts_payable' => $row->accounts_payable,
								'start_date' => $row->start_date,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblcycledetails')->insert($data);
						}

						// major_location
						$major_location = Majorlocation::where('entity_id', $old_entity->entityid)->get();
						foreach($major_location as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'marketlocation' => $row->marketlocation,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblmajorlocation')->insert($data);
						}

						// competitor
						$competitor = Competitor::where('entity_id', $old_entity->entityid)->get();
						foreach($competitor as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'competitor_name' => $row->competitor_name,
								'competitor_address' => $row->competitor_address,
								'competitor_contact_details' => $row->competitor_contact_details,
								'competitor_email' => $row->competitor_email,
								'competitor_phone' => $row->competitor_phone,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblcompetitor')->insert($data);
						}

						// business_driver
						$business_driver = Businessdriver::where('entity_id', $old_entity->entityid)->get();
						foreach($business_driver as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'businessdriver_name' => $row->businessdriver_name,
								'businessdriver_total_sales' => $row->businessdriver_total_sales,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblbusinessdriver')->insert($data);
						}

						// business_segment
						$business_segment = Businesssegment::where('entity_id', $old_entity->entityid)->get();
						foreach($business_segment as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'businesssegment_name' => $row->businesssegment_name,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblbusinesssegment')->insert($data);
						}

						// past_project
						$past_project = Projectcompleted::where('entity_id', $old_entity->entityid)->get();
						foreach($past_project as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'projectcompleted_name' => $row->projectcompleted_name,
								'projectcompleted_cost' => $row->projectcompleted_cost,
								'projectcompleted_source_funding' => $row->projectcompleted_source_funding,
								'projectcompleted_year_began' => $row->projectcompleted_year_began,
								'projectcompleted_result' => $row->projectcompleted_result,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblprojectcompleted')->insert($data);
						}

						//future_growth
						$future_growth = Futuregrowth::where('entity_id', $old_entity->entityid)->get();
						foreach($future_growth as $row){
							$data = array(
								'entity_id' =>$entity->entityid,
								'futuregrowth_name' => $row->futuregrowth_name,
								'futuregrowth_estimated_cost' => $row->futuregrowth_estimated_cost,
								'futuregrowth_implementation_date' => $row->futuregrowth_implementation_date,
								'futuregrowth_source_capital' => $row->futuregrowth_source_capital,
								'planned_proj_goal' => $row->planned_proj_goal,
								'planned_goal_increase' => $row->planned_goal_increase,
								'proj_benefit_date' => $row->proj_benefit_date,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							DB::table('tblfuturegrowth')->insert($data);
						}

						if($old_entity->entity_type == 0){
							// Key Manager
							$keymanager = KeyManager::where('entity_id', $old_entity->entityid)->get();
							foreach($keymanager as $km){
								$kdata = new KeyManager();
								$kdata->ownerid = 0;
								$kdata->entity_id = $entity->entityid;
								$kdata->firstname = $km->firstname;
								$kdata->middlename = $km->middlename;
								$kdata->lastname = $km->lastname;
								$kdata->nationality = $km->nationality;
								$kdata->birthdate = $km->birthdate;
								$kdata->civilstatus = $km->civilstatus;
								$kdata->profession = $km->profession;
								$kdata->email = $km->email;
								$kdata->address1 = $km->address1;
								$kdata->address2 = $km->address2;
								$kdata->cityid = $km->cityid;
								$kdata->province = $km->province;
								$kdata->zipcode = $km->zipcode;
								$kdata->present_address_status = $km->present_address_status;
								$kdata->no_yrs_present_address = $km->no_yrs_present_address;
								$kdata->phone = $km->phone;
								$kdata->tin_num = $km->tin_num;
								$kdata->percent_of_ownership = $km->percent_of_ownership;
								$kdata->number_of_year_engaged = $km->number_of_year_engaged;
								$kdata->position = $km->position;
								$kdata->save();
							}
						} else {
							// Owner
							$owner = Owner::where('entity_id', $old_entity->entityid)->get();
							foreach($owner as $row){
								$data = new Owner();
								$data->entity_id = $entity->entityid;
								$data->firstname =  $row->firstname;
								$data->middlename =  $row->middlename;
								$data->lastname =  $row->lastname;
								$data->nationality =  $row->nationality;
								$data->birthdate =  $row->birthdate;
								$data->civilstatus =  $row->civilstatus;
								$data->profession =  $row->profession;
								$data->email =  $row->email;
								$data->address1 =  $row->address1;
								$data->address2 =  $row->address2;
								$data->cityid =  $row->cityid;
								$data->province =  $row->province;
								$data->zipcode =  $row->zipcode;
								$data->present_address_status =  $row->present_address_status;
								$data->no_yrs_present_address =  $row->no_yrs_present_address;
								$data->phone =  $row->phone;
								$data->tin_num =  $row->tin_num;
								$data->percent_of_ownership =  $row->percent_of_ownership;
								$data->number_of_year_engaged =  $row->number_of_year_engaged;
								$data->position =  $row->position;
								$data->save();

								$spouse = Spouse::where('ownerid', $row->ownerid)->first();
								if($spouse){
									$sdata = new Spouse();
									$sdata->ownerid = $data->ownerid;
									$sdata->entity_id = $entity->entityid;
									$sdata->firstname = $spouse->firstname;
									$sdata->middlename = $spouse->middlename;
									$sdata->lastname = $spouse->lastname;
									$sdata->nationality = $spouse->nationality;
									$sdata->birthdate = $spouse->birthdate;
									$sdata->profession = $spouse->profession;
									$sdata->email = $spouse->email;
									$sdata->phone = $spouse->phone;
									$sdata->tin_num = $spouse->tin_num;
									$sdata->save();

									$seducation = Education::where('ownerid', $spouse->ownerid)->first();
									if($seducation){
										$sedata = new Education();
										$sedata->ownerid = $sdata->spouseid;
										$sedata->entity_id = $entity->entityid;
										$sedata->educ_type = $seducation->educ_type;
										$sedata->school_name = $seducation->school_name;
										$sedata->educ_degree = $seducation->educ_degree;
										$sedata->save();
									}
								}

								$education = Education::where('ownerid', $row->ownerid)->first();
								if($education){
									$edata = new Education();
									$edata->ownerid = $data->ownerid;
									$edata->entity_id = $entity->entityid;
									$edata->educ_type = $education->educ_type;
									$edata->school_name = $education->school_name;
									$edata->educ_degree = $education->educ_degree;
									$edata->save();
								}

								$managedbusiness = ManageBusiness::where('ownerid', $row->ownerid)->get();
								foreach($managedbusiness as $mb){
									$mdata = new ManageBusiness();
									$mdata->ownerid = $data->ownerid;
									$mdata->entity_id = $entity->entityid;
									$mdata->bus_name = $mb->bus_name	;
									$mdata->bus_type = $mb->bus_type;
									$mdata->bus_location = $mb->bus_location;
									$mdata->save();
								}

								$personalloans = PersonalLoan::where('owner_id', $row->ownerid)->get();
								foreach($personalloans as $pl){
									$pdata = new PersonalLoan();
									$pdata->owner_id = $data->ownerid;
									$pdata->entity_id = $entity->entityid;
									$pdata->purpose = $pl->purpose;
									$pdata->balance = $pl->balance;
									$pdata->as_of = $pl->as_of;
									$pdata->monthly_amortization = $pl->monthly_amortization;
									$pdata->creditor = $pl->creditor;
									$pdata->due_date = $pl->due_date;
									$pdata->save();
								}

							}

							// Key Manager
							$keymanager = KeyManager::where('entity_id', $old_entity->entityid)->get();
							foreach($keymanager as $km){
								$kdata = new KeyManager();
								$kdata->ownerid = $data->ownerid;
								$kdata->entity_id = $entity->entityid;
								$kdata->firstname = $km->firstname;
								$kdata->middlename = $km->middlename;
								$kdata->lastname = $km->lastname;
								$kdata->nationality = $km->nationality;
								$kdata->birthdate = $km->birthdate;
								$kdata->civilstatus = $km->civilstatus;
								$kdata->profession = $km->profession;
								$kdata->email = $km->email;
								$kdata->address1 = $km->address1;
								$kdata->address2 = $km->address2;
								$kdata->cityid = $km->cityid;
								$kdata->province = $km->province;
								$kdata->zipcode = $km->zipcode;
								$kdata->present_address_status = $km->present_address_status;
								$kdata->no_yrs_present_address = $km->no_yrs_present_address;
								$kdata->phone = $km->phone;
								$kdata->tin_num = $km->tin_num;
								$kdata->percent_of_ownership = $km->percent_of_ownership;
								$kdata->number_of_year_engaged = $km->number_of_year_engaged;
								$kdata->position = $km->position;
								$kdata->save();
							}
						}
					}
				return;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.26 checkExistingLocation
    //      function to check location
    //----------------------------------------------------
	function checkExistingLocation(){

		try{
			$address = $_GET['address'];
			$provCode = null;
			$regCode = null;


			// check if there is a province
			if (array_key_exists('province', $address)) {
				$province = $address['province'];
				$province = DB::table('refprovince')->select('provCode')
					->where('provDesc', 'like', '%'.$province[0].'%')
					->first();

				if ($province) {
					$provCode = $province->provCode;
				}
			}

			// check if there is a region
			if (array_key_exists('region', $address)) {
				$region = $address['region'];
				$region = DB::table('refregion')->select('regCode')
					->where('regDesc', 'like', '%'.$region[0].'%')
					->first();
				
				if ($region) {
					$regCode = $region->regCode;
				}
			}

			// check if there is a city
			if (array_key_exists('city', $address)) {
				$possibleCities = $address['city'];
				$city = null;
				foreach ($possibleCities as $key => $possbileCity) {
					$is_city = DB::table('refcitymun')->select(['citymunDesc', 'citymunCode',
											 'refcitymun.provCode', 'refprovince.provDesc'])
						->leftJoin('refprovince', 'refcitymun.provCode', '=', 'refprovince.provCode')
						->where('citymunDesc', 'like', '%'.$possbileCity.'%');

					if ($provCode){
						$is_city->where('refcitymun.provCode', $provCode);
					}

					if ($regCode) {
						$is_city->where('refcitymun.regDesc', $regCode);
					}

					$is_city = $is_city->first();

					if($is_city) {
						$city = $is_city;
					break;
					}
				}

				if ($city) {
					return response()->json($city);
				} else {
					return;
				}
			} else {
				return;
			}

		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.27 getSignupUser
    //      
    //----------------------------------------------------
	public function getSignupUser()
	{
		try{
			return View::make('signup.create_basic_user');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.28 postSignupUser
    //      
    //----------------------------------------------------
	public function postSignupUser()
	{
		try{
			$file = Input::file('basic_user');
			$extension = $file->getClientOriginalExtension();
			
			if($extension != 'xlsx'){
				return Redirect::route('getSignupUser')->withErrors("Please upload an excel file.")->withInput();
			}else{
				$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!()-.?[]_;:!@$&";
				$current_date = date('Y-m-d');
				$path = $file->getRealPath();
				$data = Excel::load($path)->get();

				if($data->count() > 0){
					foreach($data as $value){
						$insert_data[] = array(
							'user'  => $value['user'],
							'email'   => $value['email']
						);
					}
				}

				foreach($insert_data as $account){
					$password = "";
					if ($account['email'] != '' && $account['user'] != '') {

						$check_existing_email = User::where('email', $account['email'])->get();

						/** Check if email exists */
						if(count($check_existing_email) > 0){
							$final_record[] = array(
								'user'  => $account['user'],
								'email'   => $account['email'],
								'password' => $password
							);
						}else{
							/** Generate random password */
							for($i = 0; $i < 10; $i++){
								$random_int = mt_rand();
								$password .= $charset[$random_int % strlen($charset)];
							}

							$final_record[] = array(
								'user'  => $account['user'],
								'email'   => $account['email'],
								'password' => $password
							);

							$user = new User();
							$user->email = $account['email'];
							$user->password = Hash::make($password);
							$user->name = $account['user'];
							$user->activation_date = $current_date;
							$user->role = 1;
							$user->status = 1;
							$user->save();

							$receiver = $account['email'];

							try{
								Mail::send('emails.email_automated_basic_user', 
									array('password' => $password, 'user' => $account['user'], 'email' => $account['email']),
										function($message) use ($receiver) {
											$message->to($receiver)
												->bcc(["andrian.fortuno@creditbpo.com"])
												->subject('CreditBPO Access');
										}
								);
							}catch(Exception $e){
				            //
				            }
						}
						
					}else{
						$final_record[] = array(
							'user'  => $account['user'],
							'email'   => $account['email'],
							'password' => $password
						);
					}
				}

				Excel::create('Registered Accounts', function($excel) use ($final_record) {
					$excel->sheet('Registered Accounts', function($sheet) use($final_record){
						$sheet->fromArray($final_record);
					});
				})->download('xlsx');
			}

			return;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.29 getSignupContractor
    //      
	//----------------------------------------------------
	public function getSignupContractor()
	{
		try{
			return View::make('signup.create_contractor_user');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 51.29 postSignupContractor
    //      
	//----------------------------------------------------
	public function postSignupContractor()
	{

		try {
			/** Initialize Server Response  */
			$serverResponse['sts']            = STS_NG;
			$serverResponse['messages']       = STR_EMPTY;

			$messages = array(
				'name.required' => 'The Name field is required.',
				'email.required' => 'The Email field is required.',
				'email.unique' => 'The Email you are trying to register is already taken.',
				'password.required' => 'The Password field is required.',
				'password_confirmation.required' => 'The Re-type Password field is required.'
			);

			$rules = array(
				'name'	=> 'required',
				'email'	=> 'required|unique:tbllogin,email|email|unique:supervisor_application,email',
				'password'	=> 'required|min:6|confirmed',
				'password_confirmation'	=> 'required|min:6'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails()) {
				if ((Input::has('action')) && (Input::has('section'))) {
					$serverResponse['messages']	= $validator->messages()->all();
					return json_encode($serverResponse);
				}
				else {
					return Redirect::route('getSignupContractor')->withErrors($validator)->withInput();
				}
			} else {
				/* Generate activation code */
				$activationdigit = rand(00000,99999);

				$user = new User();
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->name = Input::get('name');
				$user->activation_code = $activationdigit;
				$user->activation_date = '0000-00-00';
				$user->role = 1;
				$user->status = 0;

				/** set user as contractor */
				$user->is_contractor = 1;
				
				$user->save();

				$loginid = $user->loginid;

				if($loginid != null || $loginid  != 0) {
					if (env("APP_ENV") != "local") {

						try{
							Mail::send('emails.signup', array('activationcode'=>$activationdigit, 'email'=>Input::get('email')), function($message){
								$message->to(Input::get('email'), Input::get('email'))
									->subject('Welcome to CreditBPO!');
								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							});

						}catch(Exception $e){
		                //
		                }

						if (env("APP_ENV") == "Production") {

							try{
								Mail::send('emails.signup-notice', array('name' => Input::get('name'), 'email_add' => Input::get('email')), function($message) {
								$message->to('notify.user@creditbpo.com')
										->subject('New registered member');
								});

							}catch(Exception $e){
			                //
			                }

						}
					}

					if ((Input::has('action')) && (Input::has('section'))) {
						$serverResponse['sts'] = STS_OK;
						$serverResponse['messages'] = 'Successfully Added Record';
						$serverResponse['loginid'] = $user->loginid;
						return json_encode($serverResponse);
					}
					else {
						return Redirect::route('getSignupConfirmation');
					}
				} else{
					return Redirect::route('getSignupConfirmation')->with('fail', 'An error accured while creating the user. <a href="login"> </a>');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
		// } catch(\Exception $e) {
		// 	$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 19f7b57");

  //           $logParams = array("activity" => "User Signup", "stackTrace" => $e, "errorCode" => "19f7b57");

		// 	AuditLogs::saveActivityLog($logParams);
			
		// 	\Log::info("19f7b57: " . $e);

		// 	return Redirect::route('getSignupContractor')->withErrors($error)->withInput();
		// }
	}
	
    public function companySearch(Request $request)
    {
    	try{
			$location = null;
			$address = null;
			$var = null;

			$input      = Input::get('term');
	        $companies  = PseRegisteredCompany::where('name', 'LIKE', '%'.$input.'%')
						->select('id','name','business_address')
						->get();

			$output     = array();

			foreach($companies as $c){
				$location = $c->business_address;
			}

			if($location){
				$address = explode(',', $location);
			}

			if($address){
				for($x=count($address)-1; $x>=0; $x--){
					$var = $address[$x];
					
					/** Check if city is present in the address start from last array*/
					if(strpos($var,'City') == true || strpos($var,'city') == true){
						break;
					}else{
						$var = null;
					}
				}
			}

			if($var){
				$var = substr($var, 0, strpos($var, "City"));
				$var = preg_replace('/[0-9]+/','',$var);
				$var = trim($var);

				$city = DB::table('refcitymun')
							->select('citymunCode', 'provCode')
							->where('citymunDesc', 'LIKE', '%'.$var.'%')
							->get();

				foreach($city as $c){
					$city_code = $c->citymunCode;
					$prov = $c->provCode;
				}

				if(count($city) > 0){
					foreach ($companies as $company) {
						$pseCompany = $this->getPseCompanyIndestyData($company->id);
						$output[]   = [
							"value" 	=> $company->name,
							"label" 	=> $company->name,
							"id" 		=> $company->id,
							"address"	=> $company->business_address,
							"city" 		=> $city_code,
							"province"	=> $prov,
							// "industry_main_id"	=> $pseCompany->pse_company_industry_main_id
							];
					}
					return json_encode($output);
				}else{
					foreach ($companies as $company) {
						$pseCompany = $this->getPseCompanyIndestyData($company->id);
						$output[]   = [
							"value" 	=> $company->name,
							"label" 	=> $company->name,
							"id" 		=> $company->id,
							"address"	=> $company->business_address,
							"province"		=> 1401,
							"city"			=> 140101,
							// "industry_main_id"	=> $pseCompany->pse_company_industry_main_id
						];}
					return json_encode($output);
				}
			}else{
				foreach ($companies as $company) {
					$pseCompany = $this->getPseCompanyIndestyData($company->id);
					$output[]   = [
						"value" 	=> $company->name,
						"label" 	=> $company->name,
						"id" 		=> $company->id,
						"address"	=> $company->business_address,
						"province"		=> 1401,
						"city"			=> 140101,
						// "industry_main_id"	=> $pseCompany->pse_company_industry_main_id
					];
					}
				return json_encode($output);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	
    public function serial_notification($serial_key, $login_id, $entity){

	    	try{
	        $serial_key->is_used = 1;
	        $serial_key->login_id = $login_id;
	        $serial_key->entity_id = $entity->entityid;
	        $serial_key->save();

	        if (env("APP_ENV") == "Production"){
	            //send email notification to emails set in supervisor dashboard under notifications.
	            $bankNotifSettings = BankNotifications::where('bank_id', $serial_key->bank_id)->first();
	            $emailList = BankEmailNotifications::where('bank_id', $serial_key->bank_id)->where('status', 1)->get();
	            $bankDetails = Bank::where('id', $serial_key->bank_id)->first();
	            $clientKeyCount = BankSerialKeys::where('bank_id', $serial_key->bank_id)->where('is_used', 0)->count();
	            $loginDetails = User::where('loginid', $entity->loginid)->first();

	            $user = new User();
	            $user->firstname = $bankDetails->bank_name;
	            $user->lastname = "Supervisor";
	            $user->clientKey = $serial_key->serial_key;
	            $user->clientKeyCount = $clientKeyCount;
	            $user->contactname = $loginDetails->firstname . " " . $loginDetails->lastname;
	            $user->contactemail = $entity->email;
	            $user->phonenumber = $entity->phone;
	            $user->companyname = $entity->companyname;

	            if ($bankNotifSettings){
	                if($bankNotifSettings->notif_new_report == 1){
	                    foreach($emailList as $email){

	                    	try{
		                        Mail::send('emails.notifications.new-report-notification',
		                            array(
		                                'user' => $user
		                            ),
		                            function($message) use ($email) {
		                                $message->to($email['email'], $email['email'])
		                                    ->subject('CreditBPO Notification: New Report');
		                            }
		                        );
		                    }catch(Exception $e){
				                //
				                }

	                    }
	                }

	                if($bankNotifSettings->notif_client_key_use == 1){
	                    foreach($emailList as $email){
	                    	try{
		                        Mail::send('emails.notifications.client-key-used-notification',
		                            array(
		                                'user' => $user
		                            ),
		                            function($message) use ($email) {
		                                $message->to($email['email'], $email['email'])
		                                    ->subject('CreditBPO Notification: Client Key Used');
		                            }
		                        );
	                        }catch(Exception $e){
			                //
			                }
	                    }
	                }

	                if($bankNotifSettings->notif_keys_almost_out == 1){
	                    if($clientKeyCount <= $bankNotifSettings->keys_number_trigger){
	                        foreach($emailList as $email){
	                        	try{
		                            Mail::send('emails.notifications.client-key-alm-out-notification',
		                                array(
		                                    'user' => $user
		                                ),
		                                function($message) use ($email) {
		                                    $message->to($email['email'], $email['email'])
		                                        ->subject('CreditBPO Notification: Client Keys Almost Out');
		                                }
		                            );
	                            }catch(Exception $e){
				                //
				                }
	                        }
	                    }
	                }

	                if($bankNotifSettings->notif_no_more_keys == 1){
	                    if($clientKeyCount <= 0){
	                        foreach($emailList as $email){

	                        	try{
		                            Mail::send('emails.notifications.client-key-out-notification',
		                                array(
		                                    'user' => $user
		                                ),
		                                function($message) use ($email) {
		                                    $message->to($email['email'], $email['email'])
		                                        ->subject('CreditBPO Notification: Client Keys Are All Used');
		                                }
		                            );
	                            }catch(Exception $e){
				                //
				                }
	                        }
	                    }
	                }
	            }
	        }
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	public function generatePcabPDF(){

		try{
			$pdf = PDF::loadView('pdf.pcab');
			return $pdf->download('PCAB.pdf');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guest('login');
	        }else{
	            throw $ex;
	        }
		}
		// return view('pdf.pcab_html');
	}

	function postUploadFspdfFilesOnCreateReport($entity_id, $pse_id)
    {
    	try{
			$this->downloadPseEdgeAnnualReport($entity_id, $pse_id);

			/** Copy Files to Innodata Folder */
			$entity = Entity::where('entityid', $entity_id)->first();
	        /* Get Company Reports Group By Edge No */
	        $pse_fs_file_group_by_edge_nos = PseCompanyFinancialReport::where(['pse_company_id' => $pse_id, 'is_deleted' => 0])
	        								->groupBy('edge_no')
	        								->pluck('edge_no')->toArray();

	        foreach ($pse_fs_file_group_by_edge_nos as $pse_fs_file_group_by_edge_no) {

				$pse_fs_files = PseCompanyFinancialReport::where('edge_no', $pse_fs_file_group_by_edge_no)
								->where(['pse_company_id' => $pse_id, 'is_deleted' => 0])
	        					->orderBy('id', 'desc')
								->get();

		        foreach ($pse_fs_files as $key => $pse_fs_file) {

		            if($pse_fs_file){  
		                //create innodata folder if not exist
		                $destinationPath = public_path('documents/innodata/to_innodata/');
		                if (!file_exists(public_path('documents/innodata/'))) {
		                    mkdir(public_path('documents/innodata/'));
		                }
		                if (!file_exists($destinationPath)) {
		                    mkdir($destinationPath);
		                }

		                $localFileName = $entity_id."-".$pse_fs_file->attachment_file_name;
		                // save file
		                File::copy(public_path('pse_fs_attachments/').$pse_fs_file->pse_company_id.'/'.$pse_fs_file->attachment_file_name, $destinationPath.$localFileName);

		                // create database entry
		                $innodataFile = new InnodataFile;
		                $innodataFile->entity_id = $entity_id;
		                $innodataFile->file_name = $localFileName;
						$innodataFile->save();
						
		            }
		        }
			}

	        ActivityLog::saveLog();             // log activity to database
	        return true;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    function getPseCompanyIndestyData($pse_id)
    {
    	try{
    		return PseCompany::find($pse_id);
    	}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guess('login');
	        }else{
	            throw $ex;
	        }
		}
    }

    private function sendActivationLinkEmail($activationdigit, $email)
    {
    	try{
	    	Mail::send('emails.signup', array('activationcode'=>$activationdigit, 'email'=>$email), function($message) use($email){
					$message->to($email, $email)
						->subject('Welcome to CreditBPO!');
					$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
					$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
				});
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::guess('login');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function downloadPseEdgeAnnualReport($entity_id, $pse_id) {
		try{
			$company = PSE::where('pse_id', $pse_id)->first();
			
			$search_query_string = '?keyword='.$company->pse_id.'&tmplNm=Annual%20Report'; // Search string to find Annual report by company id
			$company_disclosures_url    = 'https://edge.pse.com.ph/companyDisclosures/search.ax'.$search_query_string;
			$html   = file_get_html($company_disclosures_url);

			$links  = array_reverse($html->find ('a')); // Find anchor element which contains Annual report edge no and reverse it
			$edge_nos  = [];

			$reports = PseCompanyFinancialReport::where('pse_company_id', $pse_id)->get();
			if (count($reports) > 0) {
				PseCompanyFinancialReport::where('pse_company_id', $pse_id)->update(['is_deleted' => 1]);
			}

			foreach($links as $link) {
				if($link->href === '#viewer' && $link->plaintext === 'Annual Report') {
					/* Extract edge no from onclick function */
					$edge_no = $this->get_string_between($link->onclick, "'","'");

					$openDiscViewerUrl    = 'https://edge.pse.com.ph/openDiscViewer.do?edge_no='.$edge_no;
					$openDiscHtml   = file_get_html($openDiscViewerUrl);

					foreach($openDiscHtml->find('#file_list') as $openDiscHtmlElement) { 
						$attachmentOptions = $openDiscHtmlElement->find('option');

						foreach($attachmentOptions as $attachmentOption) { 
							if(!empty($attachmentOption->value)) {
								$file_id = $attachmentOption->value;

								/* Remove &nbsp; from File Name */
								$string = preg_replace("/&nbsp;/",'',$attachmentOption->plaintext);
								/* Remove &amp; from File Name */
								$string = preg_replace("/&amp;/",'',$attachmentOption->plaintext);
								/* Remove extra spaces from File Name*/
								$content = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $string)));

								// https://edge	.pse.com.ph/downloadFile.do?file_id=744695
								// Initialize a file URL to the variable 
								$downloadUrl = 'https://edge.pse.com.ph/downloadFile.do?file_id='.$file_id;
								$attachment_file_name = $content;

								$dir = public_path('pse_fs_attachments/').$company->pse_id.'/';
								$this->downloadFromUrl($downloadUrl, $dir, $attachment_file_name, $pse_id);

								/* Save downloaded report data in database */
								PseCompanyFinancialReport::create(['pse_company_id' => $company->pse_id,
												'attachment_file_name'  => $attachment_file_name,
												'edge_no'               => $edge_no,
												'attachment_file_id'    => $file_id,
												'status'				=> 0,
												'attachment_file_url'   => url('').'/pse_fs_attachments/'.$company->pse_id.'/'.$attachment_file_name]);

							}
						}
					}
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function: get_string_between
    //      Get string between two string
    //-----------------------------------------------------
    function get_string_between($string, $start, $end){
    	try{
	        $string = ' ' . $string;
	        $ini = strpos($string, $start);
	        if ($ini == 0) return '';
	        $ini += strlen($start);
	        $len = strpos($string, $end, $ini) - $ini;
	        return substr($string, $ini, $len);
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function: downloadFromUrl
    //      Download Annual Report from URL and Save it in pse_fs_attachments folder
    //-----------------------------------------------------
    function downloadFromUrl($url, $dir, $file_name, $pse_id)
    {
    	try{
	        // Initialize the cURL session 
	        $ch = curl_init($url);
	          
	        // Inintialize directory name where 
	        // file will be save 
	        if(!is_dir($dir)) {
	            mkdir($dir, 0777, true);
	        }

	        // Use basename() function to return
	        // the base name of file  
	        $file_name = $file_name;
	          
	        // Save file into file location 
	        $save_file_loc = $dir . $file_name; 
	          
	        // Open file  
	        $fp = fopen($save_file_loc, 'wb'); 
	          
	        // It set an option for a cURL transfer 
	        curl_setopt($ch, CURLOPT_FILE, $fp); 
	        curl_setopt($ch, CURLOPT_HEADER, 0); 
	          
	        // Perform a cURL session 
	        curl_exec($ch); 
	          
	        // Closes a cURL session and frees all resources 
	        curl_close($ch); 
	          
	        // Close file 
			fclose($fp); 
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function: postSearchPseCompany
    //     Search Company Name on PSE Lisst
    //-----------------------------------------------------
	public function postSearchPseCompany(){
		$search = Input::get("search");

		if($search){
			$companies  = PseRegisteredCompany::where('name', 'LIKE', '%'.$search.'%')
												->get();
			
			$str = "";
			$str .= "<ul>";
			foreach($companies as $key => $company){
                $str .= "<li class='search_pse'> <input type='radio' name='pse_name' value='". $company->name ."'> ". $company->name . "</li>";
			}
			$str .= "<ul>";

			$cnt = count($companies);

			$data = array(
				"success"	=>true,
				"data"		=>$companies,
				"html"		=> $str,
				'count' 	=> $cnt
			);
		}else{
			$companies  = PseRegisteredCompany::all();
			$cnt = count($companies);

			$str = "";
			$str .= "<ul>";

			foreach($companies as $key => $company){
                $str .= "<li class='search_pse'> <input type='radio' name='pse_name' value='". $company->name ."'> ". $company->name . "</li>";
			}
			$str .= "<ul>";

			$data = array(
				"success"	=>true,
				"data"		=>$companies,
				"html"		=> $str,
				'count' 	=> $cnt
			);
		}

		return $data;
	}

	//-----------------------------------------------------
    //  Function: processPseSearch
    //     Process Search PSE Company
    //-----------------------------------------------------
	public function processPseSearch(){
		$pse_company = Input::get('pse_company');

		$company = PseRegisteredCompany::where('name', $pse_company)->first();

		$location = $company->business_address;

		if($location){
			$address = explode(',', $location);
		}

		if($address){
			for($x=count($address)-1; $x>=0; $x--){
				$var = $address[$x];
				
				/** Check if city is present in the address start from last array*/
				if(strpos($var,'City') == true || strpos($var,'city') == true){
					break;
				}else{
					$var = null;
				}
			}
		}

		if($var){
			$var = substr($var, 0, strpos($var, "City"));
			$var = preg_replace('/[0-9]+/','',$var);
			$var = trim($var);

			$city = DB::table('refcitymun')
						->select('citymunCode', 'provCode')
						->where('citymunDesc', 'LIKE', '%'.$var.'%')
						->get();

			foreach($city as $c){
				$city_code 	= $c->citymunCode;
				$prov 		= $c->provCode;
			}

			if(count($city) > 0){
				$data = array(
					'companyname' 	=> $pse_company,
					'city' 			=> $city_code,
					'province'		=> $prov
				);
			}else{
				$data = array(
					'companyname' => $pse_company
				);
			}
		}

		return $data;
	}

	public function getPremiumReportPriceByLocationAndBank($provCode, $bankId){
		// Get Bank Custom Price
		$organization = Bank::where('id', $bankId)->first();
		$simplifiedPrice = 0;
		$standalonePrice = 0;
		$premiumPrice = 0;

		// Filter prizes according to premium zone
		if($provCode == 13){ // Metro Manila
			$premiumPrice = $organization->premium_zone1_price;
		}elseif(in_array($provCode, array(04,03))){ // Laguna, Rizal, Cavite and Bulacan
			$premiumPrice = $organization->premium_zone2_price;
		}else{ // Rest of the regions
			$premiumPrice = $organization->premium_zone3_price;
		}
		$simplifiedPrice = $organization->simplified_price;
		$standalonePrice = $organization->standalone_price;

		$data = array(
			'simplified'	=> $simplifiedPrice,
			'standalone'	=> $standalonePrice,
			'premium'		=> $premiumPrice,
			'is_custom'		=> $organization->is_custom,
			'custom_price'  => $organization->custom_price
		);
		return $data;
	}
}
