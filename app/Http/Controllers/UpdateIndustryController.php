<?php

class UpdateIndustryController extends BaseController {

    /*-------------------------------------------------------------------------
	|	Editable field tables
	|------------------------------------------------------------------------*/
    private static $tb_industry_field = array(
        'Agriculture'   => array(
            'id'        => 1,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Crop and animal production, hunting and related service activities',
                    'groups'    => array(
                        'Growing of non-perrenial crops',
                        'Growing of  perrenial crops',
                        'Plant propagation',
                        'Animal production',
                        'Support activities to agriculture and post-harvest crop activities',
                        'Hunting, trapping and related service activities',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Forestry and Logging',
                    'groups'    => array(
                        'Silviculture and other forestry activities',
                        'Logging',
                        'Gathering of non-wood forest products',
                        'Support services to forestry'
                    )
                ),
                'div3'  => array(
                    'name'      => 'Fishing and Aquaculture',
                    'groups'    => array(
                        'Fishing',
                        'Aquaculture'
                    )
                )
            )
        ),
        'Mining'   => array(
            'id'        => 2,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Mining of coal and lignite',
                    'groups'    => array(
                        'Mining of hard coal',
                        'Mining of lignite',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Extraction of crude petroleum and natural gas',
                    'groups'    => array(
                        'Extraction of crude petroleum',
                        'Extraction of natural gas',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Mining of metal ores',
                    'groups'    => array(
                        'Mining of iron ores',
                        'Mining of non-ferrous metal ores except precious metals'
                    )
                ),
                'div4'  => array(
                    'name'      => 'Other mining and quarrying',
                    'groups'    => array(
                        'Quarrying of stone, sand and clay',
                        'Mining and quarrying, n.e.c.'
                    )
                ),
                'div5'  => array(
                    'name'      => 'Mining support service activities',
                    'groups'    => array(
                        'Support activities for petroleum and gas extraction',
                        'Suport activities for other mining and quarrying'
                    )
                ),
            )
        ),
        'Manufacturing'   => array(
            'id'        => 3,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Manufacture of food products',
                    'groups'    => array(
                        'Processing and preserving of meat',
                        'Processing and preserving of fish, crustaceans and mollusks',
                        'Processing and preserving of fruits and vegetables',
                        'Manufacture of vegetables and animal oils and fats',
                        'Manufacture of dairy products',
                        'Manufacture of grain mill products , starches and starch products',
                        'Manufacture of other food products',
                        'Manufacture of prepared animal feeds',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Manufacture of beverages',
                    'groups'    => array(
                        'Manufacture of beverages',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Manufacture of tobacco products',
                    'groups'    => array(
                        'Manufacture of tobacco products',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Manufacture of Textiles',
                    'groups'    => array(
                        'Spinning, weaving and finishing of textiles',
                        'Manufacture of other textiles'
                    )
                ),
                'div5'  => array(
                    'name'      => 'Manufacture of wearing apparel',
                    'groups'    => array(
                        'Manufacture of wearing apparel, except fur apparel',
                        'Custom tailoring and dressmaking',
                        'Manufacture of knitted and crocheted apparel',
                        'Manufacture of articles of fur',
                    )
                ),
                'div6'  => array(
                    'name'      => 'Manufacture of leather and related products',
                    'groups'    => array(
                        'Tanning and dressing of leather; manufacture of luggage and handbags',
                        'Manufacture of footwear',
                    )
                ),
                'div7'  => array(
                    'name'      => 'Manufacture of wood and products of wood and cork, except furniture; Manufacture of articles of bamboo, cane, rattan and the like; Manufacture of articles of straw and plaiting materials',
                    'groups'    => array(
                        'Sawmilling and planning of wood',
                        'Manufacture of products of wood, cork, straw and plaiting materials',
                    )
                ),
                'div8'  => array(
                    'name'      => 'Manufacture of paper and paper products',
                    'groups'    => array(
                        'Manufacture of paper and paper products',
                    )
                ),
                'div9'  => array(
                    'name'      => 'Printing and reproduction of recorded media',
                    'groups'    => array(
                        'Printing and service activities related to printing',
                        'Reproduction of recorded media',
                    )
                ),
                'div10'  => array(
                    'name'      => 'Manufacture of coke and refined petroleum products',
                    'groups'    => array(
                        'Manufacture of coke oven products',
                        'Manufacture of refined petroleum products',
                        'Manufacture of other fuel products',
                    )
                ),
                'div11'  => array(
                    'name'      => 'Manufacture of chemicals and chemical products',
                    'groups'    => array(
                        'Manufacture of basic chemicals',
                        'Manufacture of other chemical products, n.e.c.',
                        'Manufacture of man-made fibers',
                    )
                ),
                'div12'  => array(
                    'name'      => 'Manufacture of pharmaceutical products and pharmaceutical preparations',
                    'groups'    => array(
                        'Manufacture of pharmaceuticals, medicinal checmical and botanical products',
                    )
                ),
                'div13'  => array(
                    'name'      => 'Manufacture of rubber and plastic products',
                    'groups'    => array(
                        'Manufacture of rubber products',
                        'Manufacture plastic products',
                    )
                ),
                'div14'  => array(
                    'name'      => 'Manufacture of other non-metallic mineral products',
                    'groups'    => array(
                        'Manufacture of glass and glass products',
                        'Manufacture of non-metallic mineral products, n.e.c.',
                    )
                ),
                'div15'  => array(
                    'name'      => 'Manufacture of basic metals',
                    'groups'    => array(
                        'Manufacture of basic iron and steel',
                        'Manufacture of basic precious and other non-ferrous metals',
                        'Casting of metals',
                    )
                ),
                'div16'  => array(
                    'name'      => 'Manufacture of frabricated metal products, except machinery and equipment',
                    'groups'    => array(
                        'Manufacture of structural metal products, tanks, reservoirs and steam generators',
                        'Manufacture of weapons and ammunition',
                        'Manufacture of other fabricated metal products ; metal working service activities',
                    )
                ),
                'div17'  => array(
                    'name'      => 'Manufacture of computer, electronic and optical products',
                    'groups'    => array(
                        'Manufacture of electronic components',
                        'Manufacture of computers and peripheral equipment and accessories',
                        'Manufacture of communication equipment',
                        'Manufacture of consumer electronics',
                        'Manufacture of measuring, testing, navigating and control equipment; watches and clocks',
                        'Manufacture of optical instruments and photographic equipment',
                        'Manufacture of magnetic and optical media',
                    )
                ),
                'div18'  => array(
                    'name'      => 'Manufacture of electrical equipment',
                    'groups'    => array(
                        'Manufacture of electric motors, generators, transformers and electricity distribution and control apparatus',
                        'Manufacture of batteries and accumulators',
                        'Manufacture of fiber optic cables',
                        'Manufacture of electric lighting equipment',
                        'Manufacture of domestic appliances',
                        'Manufacture of other electrical equipment',
                    )
                ),
                'div19'  => array(
                    'name'      => 'Manufacture of motor vehicles, trailers and semi-trailers',
                    'groups'    => array(
                        'Manufacture of motor vehicles',
                        'Manufacture of bodies (coachwork) for motor vehicles; manufacture of trailers and semi-trailers',
                        'Manufacture of parts and accessoies for motor vehicles',
                        'Manufacture of electric lighting equipment',
                        'Manufacture of domestic appliances',
                        'Manufacture of other electrical equipment',
                    )
                ),
                'div20'  => array(
                    'name'      => 'Manufacture of other transport equipment',
                    'groups'    => array(
                        'Building of ships and boats',
                        'Manufacture of railway locomotive and rolling stock',
                        'Manufacture of air and spacecraft and related machinery',
                        'Manufacture of military fighting vehicles',
                        'Manuacture of transport equipment, n.e.c.',
                    )
                ),
                'div21'  => array(
                    'name'      => 'Manufacture of furniture',
                    'groups'    => array(
                        'Manufacture of furniture',
                    )
                ),
                'div22'  => array(
                    'name'      => 'Other Manufacturing',
                    'groups'    => array(
                        'Manufacture of jewelry, bijouterie and related articles',
                        'Manufacture of musical instruments',
                        'Manufacture of sports goods',
                        'Manufacture of games and toys',
                        'Manufacture of medical and dental instruments and supplies',
                        'Other manufacturing, n.e.c.',
                    )
                ),
                'div23'  => array(
                    'name'      => 'Repair and installation of machinery and equipment',
                    'groups'    => array(
                        'Repair of fabricated metal products, machinery and equipment',
                        'Installation of industrial machinery and equipment',
                        'Manufacture of sports goods',
                        'Manufacture of games and toys',
                        'Manufacture of medical and dental instruments and supplies',
                        'Other manufacturing, n.e.c.',
                    )
                ),
            )
        ),
        'Electric'   => array(
            'id'        => 4,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Electricity, gas, steam and air conditioning supply',
                    'groups'    => array(
                        'Electric power generation, transmission and distribution',
                        'Manufacture of gas; distribution of gaseous fuels through mains',
                        'Steam, air conditioning supply and production of ice',
                    )
                ),
            )
        ),
        'WaterSupply'   => array(
            'id'        => 5,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Water collection, treatment and supply',
                    'groups'    => array(
                        'Water collection, treatment and supply',
                        'Sewerage',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Waste collection, treatment and disposal activities; Materials recovery',
                    'groups'    => array(
                        'Waste collection',
                        'Waste treatment and disposal',
                        'Materials recovery',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Remediation activities and other waste management services',
                    'groups'    => array(
                        'Remediation activities and other waste management services',
                    )
                ),
            )
        ),
        'Construction'   => array(
            'id'        => 6,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Construction of Buildings',
                    'groups'    => array(
                        'Construction of Buildings',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Civil Engineering',
                    'groups'    => array(
                        'Construction of roads and railways',
                        'Constrution of utility projects',
                        'Construction of other civil engineering projects',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Specialized construction activities',
                    'groups'    => array(
                        'Demolition and site preparation',
                        'Electrical, plumbing and other construction installation activities',
                        'Building completion and finishing',
                        'Other specialized construction activities',
                    )
                ),
            )
        ),
        'Wholesale'   => array(
            'id'        => 7,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Wholesale and retails trade and repair of motor vehicles and motorcycles',
                    'groups'    => array(
                        'Sales of motor vehicles',
                        'Maintenance and repair of motor vehicles',
                        'Sale of motor vehicle parts and accessories',
                        'Sale, maintenance and repair of motorcycles and related parts and accessories',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Wholesale trade,  except of motor vehicles and motorcycles',
                    'groups'    => array(
                        'Wholesale on a fee or contract basis',
                        'Wholesale of agricultural raw materials and live animals',
                        'Wholesale of food, beverages and tobacco',
                        'Wholesale of household goods',
                        'Wholesale of machinery, equipment and supplies',
                        'Other specialized wholesale',
                        'Non-specialized wholesale',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Retail trade, except of motor vehicles and motorcycles',
                    'groups'    => array(
                        'Retail sale in non-specialized stores',
                        'Retail sale of food, beverages and tobacco in specialized stores',
                        'Retail sale of automotive fuel in specialized stores',
                        'Retail sale of information and communications equipment in specialized stores',
                        'Retail sale of other household equipment in specialized store',
                        'Retail sale of cultural and recreation goods in specialized stores',
                        'Retail sale of other goods in specialized stores',
                        'Retail sale via stalls and markets',
                        'Retail trade not in stores, stalls or markets',
                    )
                ),
            )
        ),
        'Transportation'   => array(
            'id'        => 8,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Land transport and transport via pipelines',
                    'groups'    => array(
                        'Transport via railways',
                        'Transport via buses',
                        'Other land transport',
                        'Transport via pipeline',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Water Transport',
                    'groups'    => array(
                        'Sea coastal water transport',
                        'Inland water transport',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Air Transport',
                    'groups'    => array(
                        'Passenger air transport',
                        'Freight air transport',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Warehousing and support activities for transportation',
                    'groups'    => array(
                        'Warehousing and storage',
                        'Support activities for transportation',
                    )
                ),
                'div5'  => array(
                    'name'      => 'Postal and courier activities',
                    'groups'    => array(
                        'Postal activities',
                        'Courier activities',
                    )
                ),
            )
        ),
        'Accomodation'   => array(
            'id'        => 9,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Accomodation',
                    'groups'    => array(
                        'Short term accommodation activities',
                        'Other accomodation',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Food and beverage service activities',
                    'groups'    => array(
                        'Restaurants and mobile food service activities',
                        'Event catering and other food service activities',
                        'Beverage serving activities',
                    )
                ),
            )
        ),
        'Information'   => array(
            'id'        => 10,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Publishing activities',
                    'groups'    => array(
                        'Publishing of books, periodicals and other publishing activities',
                        'Software publishing',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Motion picture, video and television programme production, sound recording and music publishing activities',
                    'groups'    => array(
                        'Motion picture, video and television programme activities',
                        'Sound recording and music publishing activities',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Programming and broadcasting activities',
                    'groups'    => array(
                        'Radio broadcasting',
                        'Television programming and broadcasting activities',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Telecommunications',
                    'groups'    => array(
                        'Wired telecommunications activities',
                        'Wireless telecommunications activities',
                        'Satellite telecommunications activities',
                        'Other telecommunications activities',
                    )
                ),
                'div5'  => array(
                    'name'      => 'Computer programming, consultancy and related activities',
                    'groups'    => array(
                        'Computer programming, consultancy and related activities',
                    )
                ),
                'div6'  => array(
                    'name'      => 'Information service activities',
                    'groups'    => array(
                        'Data processing, hosting and related activities; web portals',
                        'Other information service activities',
                    )
                ),
            )
        ),
        'Finance'   => array(
            'id'        => 11,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Financial service activities, except insurance and pension functing',
                    'groups'    => array(
                        'Monetary intermediation',
                        'Activities of holding companies',
                        'Trusts, funds and other financial vehicles',
                        'Other financial service activities, except insurance and pension functing activities',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Insurance, reinsurance and pension funding, except compulsory social security',
                    'groups'    => array(
                        'Insurance',
                        'Reinsurance',
                        'Pension funding',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Activities auxiliary to financial service and insurance activities',
                    'groups'    => array(
                        'Activities auxiliary to financial service, except insurance and pension funding',
                        'Activities auxiliary to insurance and pension funding',
                        'Fund management activities',
                    )
                ),
            )
        ),
        'RealEstate'   => array(
            'id'        => 12,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Real estate activities',
                    'groups'    => array(
                        'Real estate activities with own or leased property',
                        'Real estate activities on a fee or contract basis',
                    )
                ),
            )
        ),
        'Professional'   => array(
            'id'        => 13,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Legal and accounting activities',
                    'groups'    => array(
                        'Legal activities',
                        'Accounting, bookkeeping and auditing activities; tax consultancy',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Activities of head offices; Management consultancy activities',
                    'groups'    => array(
                        'Activities of head offices',
                        'Management consultancy activities',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Architecture and Engineering activities; technical testing and analysis',
                    'groups'    => array(
                        'Architectural and engineering activities and related technical consultancy',
                        'Technical testing and analysis',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Scientific research and development',
                    'groups'    => array(
                        'Research and experimental development on natural sciences and engineering',
                        'Research and experimental development on social sciences and humanities',
                        'Research and experimental development in information technology',
                    )
                ),
                'div5'  => array(
                    'name'      => 'Advertising and market research',
                    'groups'    => array(
                        'Advertising',
                        'Market research and public opinion polling',
                    )
                ),
            )
        ),
        'Administrative'   => array(
            'id'        => 14,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Rental and leasing activities',
                    'groups'    => array(
                        'Renting and leasing of motor vehicles',
                        'Renting and leasing of personal and household',
                        'Renting and leasing of other machinery, equipment and tangible goods, n.e.c.',
                        'Leasing of intellectual property and similar products, except copyrighted works',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Employment Activities',
                    'groups'    => array(
                        'Activities of employment placement agencies',
                        'Temporary employment agency activities',
                        'Other human resources provision',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Travel agency, tour operator, reservation service and related activities',
                    'groups'    => array(
                        'Travel agency and tour operator activities',
                        'Other reservation service and related activities',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Security and Investigation activities',
                    'groups'    => array(
                        'Private security activities',
                        'Security systems service activities',
                        'Investigation activities',
                    )
                ),
                'div5'  => array(
                    'name'      => 'Services to buildings and landscape activities',
                    'groups'    => array(
                        'Combined facilities support activities',
                        'Cleaning activities',
                        'Landscape care and maintenance service activities',
                    )
                ),
                'div6'  => array(
                    'name'      => 'Office administrative, office support and other business support activities',
                    'groups'    => array(
                        'Office administrative and support activities',
                        'Call centers and other related activities',
                        'Organization of conventions and trade shows',
                        'Business support service activities, n.e.c.',
                    )
                ),
            )
        ),
        'Education'   => array(
            'id'        => 15,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Education',
                    'groups'    => array(
                        'Pre-primary / pre-school education',
                        'Primary / elemetary education',
                        'Secondary / high school education',
                        'Higher education',
                        'Other education services',
                        'Educational support services',
                    )
                ),
            )
        ),
        'HumanHealth'   => array(
            'id'        => 16,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Human health activities',
                    'groups'    => array(
                        'Hospital activities',
                        'Medical and dental practice activities',
                        'Other human health activities',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Residential care activities',
                    'groups'    => array(
                        'Residential nursing care facilities',
                        'Residential care activities for mental retardation, mental health and substance abuse',
                        'Residential care activities for the elderly and disabled',
                        'Other residential care activites, n.e.c.',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Social work activities without accommodation',
                    'groups'    => array(
                        'Social work activities without accommodation for the elderly and disabled',
                        'Other social work activities without accommodation, n.e.c.',
                    )
                ),
            )
        ),
        'Arts'   => array(
            'id'        => 17,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Creative, arts and entertainment activities',
                    'groups'    => array(
                        'Creative, arts and entertainment activities',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Libraries, archives, museums and other cultural activities',
                    'groups'    => array(
                        'Libraries, archives, museums and other cultural activities',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Gambling and betting activities',
                    'groups'    => array(
                        'Gambling and betting activities',
                    )
                ),
                'div4'  => array(
                    'name'      => 'Sports activities and amusement and recreation activities',
                    'groups'    => array(
                        'Sports activities',
                        'Other amusement and recreation activities',
                    )
                ),
            )
        ),
        'Others'   => array(
            'id'        => 18,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Activities of membership organizations',
                    'groups'    => array(
                        'Activities of business, employers and professional membership organizations',
                        'Activities of trade unions',
                        'Activities of other membership organizations',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Repair of computers and personal and household goods',
                    'groups'    => array(
                        'Repair of computers and communications equipment',
                        'Repair of personal and household goods',
                    )
                ),
                'div3'  => array(
                    'name'      => 'Other personal service activities',
                    'groups'    => array(
                        'Personal services for wellness, except sports activities',
                        'Laundry services',
                        'Funeral and related activities',
                        'Domestic services',
                        'Other personal service activities, n.e.c.',
                    )
                ),
            )
        ),
        'Households'   => array(
            'id'        => 19,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Activities of households as employers of domestic personnel',
                    'groups'    => array(
                        'Activities of households as employers of domestic personnel',
                    )
                ),
                'div2'  => array(
                    'name'      => 'Undifferentiated goods-and-services-producing activities of private households for own use',
                    'groups'    => array(
                        'Undifferentiated goods-producing activities of private household for own use',
                        'Undifferentiated services-producing activities of private households for own use',
                    )
                ),
            )
        ),
        'Extra'   => array(
            'id'        => 20,
            'divisions' => array(
                'div1'  => array(
                    'name'      => 'Activities of extra-territorial organizations and bodies',
                    'groups'    => array(
                        'Activities of extra-territorial organizations and bodies',
                    )
                ),
            )
        ),
    );

    //-----------------------------------------------------
    //  Function 60.1: addIndustries
    //      function to add industry data
    //-----------------------------------------------------
    public function addIndustries()
    {
        foreach (self::$tb_industry_field as $industry) {

            foreach ($industry['divisions'] as $division) {
                echo '<br/>'.$division['name'].'<br/>';
                $master_id  = DB::table('tblindustry_sub')->insertGetId(
                    array(
                        'industry_main_id'  => $industry['id'],
                        'sub_title'         => $division['name'],
                        'sub_status'        => STS_OK,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    )
                );

                foreach ($division['groups'] as $group) {
                    DB::table('tblindustry_row')->insert(
                        array(
                            'industry_sub_id'  => $master_id,
                            'row_title'         => $group,
                            'row_status'        => STS_OK,
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }
    }
}
