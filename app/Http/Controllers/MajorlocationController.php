<?php

//======================================================================
//  Class 35: Major Locations Controller
//      Functions related to Major Locations on the Questionnaire
//      are routed to this class
//======================================================================
class MajorlocationController extends BaseController
{
	//-----------------------------------------------------
    //  Function 35.1: getMajorlocation
    //      Displays the Major Locations Form
    //-----------------------------------------------------
	public function getMajorlocation($entity_id)
	{
        /*  Variable Declaration    */
        $items = array();               // Existing Major locations of the report
        
        /*  Fetch the exisiting Major Locatoins of the report   */
		$currentCities = DB::table('tblmajorlocation')->where('entity_id', $entity_id)->where('is_deleted', 0)->get();
		
        /*  Stores the existing Major Locations in the items array  */
		foreach ($currentCities as $c) {
			$items[] = $c->marketlocation;
        }
        
        /*  Remove the existing locations from the items array
        *   and pass the data to the dropdown selection
        */
		$dataCity = City2::whereNotIn('id', $items)
            ->pluck('citymunDesc', 'id');
		return View::make('sme.majorlocation', 
			array(
				'cityList'  => $dataCity,
                'entity_id' => $entity_id
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 35.2: postMajorlocation
    //      HTTP Post request to validate and save Major
    //      Locations data in the database
    //-----------------------------------------------------
	public function postMajorlocation($entity_id)
	{
        /*  Variable Declaration    */
        $messages       = array();                  // Validation Messages
        
        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $entity = Entity::find($entity_id);

        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#major-loc-list-cntr';                       // Refresh the specified container
            $srv_resp['refresh_elems']  = '.reload-major-loc';                          // Elements that will be refreshed

            if($entity->is_premium == PREMIUM_REPORT ) {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
		$rules = array(
	        'marketlocation'	=> 'required|numeric',
	    );
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata  = Input::only('marketlocation');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
			$marketlocation = $arraydata['marketlocation'];
            
            /*  Saves Input to the database     */
            
            if (gettype($marketlocation) == "string") {
                $data = array(
                    'entity_id'         =>$entity_id,
                       'marketlocation'    =>$marketlocation, 
                       'created_at'        =>date('Y-m-d H:i:s'),
                    'updated_at'        =>date('Y-m-d H:i:s')
                );
                if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                            
                    DB::table('tblmajorlocation')
                        ->where('marketlocationid', $primary_id[$key])
                        ->update($data);
                    
                    $master_ids[]           = $primary_id[$key];
                    $srv_resp['messages']   = 'Successfully Updated Record';
                }
                /*  Request came from Web Application via browser   */
                else {
                    $master_ids[]           = DB::table('tblmajorlocation')->insertGetId($data);
                    $srv_resp['messages']   = 'Successfully Added Record';
                }
            } else {
                foreach ($marketlocation as $key => $value) {
                    if ($value) {
                        
                        $data = array(
                            'entity_id'         =>$entity_id,
                               'marketlocation'    =>$marketlocation[$key], 
                               'created_at'        =>date('Y-m-d H:i:s'),
                            'updated_at'        =>date('Y-m-d H:i:s')
                        );
                        
                        /*  When an edit request comes from the API */
                        if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                            
                            DB::table('tblmajorlocation')
                                ->where('marketlocationid', $primary_id[$key])
                                ->update($data);
                            
                            $master_ids[]           = $primary_id[$key];
                            $srv_resp['messages']   = 'Successfully Updated Record';
                        }
                        /*  Request came from Web Application via browser   */
                        else {
                            $master_ids[]           = DB::table('tblmajorlocation')->insertGetId($data);
                            $srv_resp['messages']   = 'Successfully Added Record';
                        }
                    }
                }
            }
			
            
            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 35.3: getMajorlocationDelete
    //      HTTP GET request to delete specified Major Locations
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getMajorlocationDelete($id)
	{
        $location = Majorlocation::where('marketlocationid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();

        $location->is_deleted = 1;
        $location->save();
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        $entity = Entity::find(Session::get('entity')->entityid);

        if ($entity->is_premium == PREMIUM_REPORT ) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}

}
