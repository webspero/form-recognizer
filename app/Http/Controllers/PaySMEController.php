<?php

use Omnipay\Omnipay;    // Omnipay Library
use CreditBPO\Services\MailHandler;
use CreditBPO\Libraries\PaymentCalculator;

//======================================================================
//  Class 42: Pay SME Controller
//      Functions related to PayPal Payment
//      are routed to this class
//======================================================================
class PaySMEController extends BaseController
{
	private $data;

    //-----------------------------------------------------
    //  Function 42.1: getCancelPayment
    //      Displays the Cancel Payment Page
    //-----------------------------------------------------
	public function getCancelPayment()
	{
		return View::make('payment.cancel_order');
	}

    //-----------------------------------------------------
    //  Function 42.2: getPaymentIndex
    //      Displays the PayPal Payment Page
    //-----------------------------------------------------
	public function getPaymentIndex()
	{
		return View::make('payment.index', $data);
	}

    //-----------------------------------------------------
    //  Function 42.3: postPayViaPaypal
    //      HTTP Post request to validate and save PayPal
    //      payments
    //-----------------------------------------------------
	public function postPayViaPaypal($id)
	{
        /*  Fetch the specified report from the database    */
        $entity = Entity::where('entityid', $id)
                ->where('is_paid', 0)
                ->first();
            
        /* Set price information */
        $paymentCalculator = new PaymentCalculator;

        $bank = Bank::where('id', '=', $entity->current_bank)->first();
        $locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

        if($bank->is_custom != 0) {
            if ($entity->is_premium == PREMIUM_REPORT) { // Premium Report
                $price = $bank->custom_price + $locPremiumReportPrice->premiumReportPrice;
            } elseif ($entity->is_premium == SIMPLIFIED_REPORT) { // Simplified Report
                $price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
            } else {    // Standalone Report
                $price = $bank->custom_price;
            }
        } else {
            if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
                $price = $paymentCalculator::REPORT_PRICE + $locPremiumReportPrice->premiumReportPrice; //$paymentCalculator::PREMIUM_REPORT_PRICE;
            } elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
                $price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
            } else { // Standalone Report
                $price = $paymentCalculator::REPORT_PRICE;
            }
        }

        $discount = null;
		$discountType = null;

        /*  When there is a discount code   */
		if ($entity->discount_code!=null) {
            $discount_data = Discount::where('discount_code', $entity->discount_code)->first();

            if ($discount_data){
                $discount = $discount_data->amount;
				$discountType = $discount_data->type;
            }
        }
        /* Compute Payment */
		$result = $paymentCalculator->paymentCompute($price, 1, $discount, $discountType);

        /*  Create a payment receipt record on the database */
        $paymentReceiptDb = new PaymentReceipt;
       
        $paymentReceipt['price'] = $price;
        $paymentReceipt['amount'] = $result['amount'];
        $paymentReceipt['discount'] = $result['discountAmount'];
        $paymentReceipt['subtotal'] = $result['subTotal'];
        $paymentReceipt['vat_amount'] = $result['taxAmount'];
        $paymentReceipt['total_amount'] = $result['totalAmount'];
        $paymentReceipt['item_quantity'] = 1;
        $paymentReceipt['login_id'] = Auth::user()->loginid;
        $paymentReceipt['reference_id'] = 0;
        $paymentReceipt['payment_method'] = 1;
        $paymentReceipt['item_type'] = 1;            /*  SME Account   */
        $paymentReceipt['sent_status'] = STS_NG;
        $paymentReceipt['date_sent'] = date('Y-m-d');
        /* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
        $paymentReceipt['vatable_amount'] = null;
        $paymentReceiptSts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

        /*  Configure the PayPal required parameters    */
		$params = array(
			'cancelUrl' => URL::to('/').'/payment/cancel_order',
			'returnUrl' => URL::to('/').'/payment/paymentsuccess',
			'name' => 'CreditBPO Services',
			'description' => 'CreditBPO Rating Report®',
			'amount' => number_format($result['totalAmount'],2,'.',''),
			'currency'      => 'PHP',
            'reference_id'  => $paymentReceiptSts // Store receipt id for reference after PayPal responds
		);

        /*  Put parameters into Session for reference after submitting to PayPal    */
        session()->put("params",$params);
        session()->save();

        /** Save transaction to db */
        $newParams = array(
            "created_at"  => date("Y-m-d H:i:s"),
            "entity_id"  => $id,
            "token"  => session()->get("_token"),
            "cancel_url" => $params['cancelUrl'],
            "return_url" => $params['returnUrl'],
            "name"  => $params['name'],
            "description"  => $params['description'],
            "amount" => $params['amount'],
            "currency" => $params['currency'],
            "reference_id"  => $params['reference_id']
        );

        DB::table("paypal_transactions")->insert($newParams);
        

        /*  Create an Instance of OmniPay library used for PayPal   */
		$gateway = Omnipay::create('PayPal_Express');

        /*  Configuration for Production Environment    */
		if(env("APP_ENV") == "Production"){
			$gateway->setUsername('lia.francisco_api1.gmail.com');
			$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
			$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
			$gateway->setLogoImageUrl(URL::To('images/creditbpo-logo.png'));
			$gateway->setTestMode(false);
		}
        /*  Configuration for Testing Environment       */
        else {
			$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
			$gateway->setPassword('87DWVXH4UGHD46W3');
			$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
			$gateway->setLogoImageUrl(URL::To('images/creditbpo-logo.png'));
			$gateway->setTestMode(true);
		}

        /*  Send data to PayPal     */
		$response = $gateway->purchase($params)->send();

        /*  PayPal payment sucessful    */
		if ($response->isSuccessful()) {
			print_r($response);
		}
        /*  Redirect to PayPal form     */
        else if ($response->isRedirect()) {
			$response->redirect();
		}
        /*  Error occured during Payment    */
        else {
			echo $response->getMessage();
		}
	}

    //-----------------------------------------------------
    //  Function 42.4: getSuccessPayment
    //      PayPal Callback function, after PayPal receives
    //      and processes the parameters, it will call this
    //      function
    //-----------------------------------------------------
	public function getSuccessPayment()
	{
        /*  Create an Instance of OmniPay library used for PayPal   */
		$gateway = Omnipay::create('PayPal_Express');

        /*  Configuration for Production Environment    */
		if(env("APP_ENV") == "Production"){
			$gateway->setUsername('lia.francisco_api1.gmail.com');
			$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
			$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
			$gateway->setTestMode(false);
		}
        /*  Configuration for Testing Environment       */
        else {
			$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
			$gateway->setPassword('87DWVXH4UGHD46W3');
			$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
			$gateway->setTestMode(true);
		}

        /*  Get the session parameters set before passing data to PayPal    */
        
        $entity_id = null;
        $params = session()->get('params');
        if(!$params) {
            $token = session()->get("_token");
            $entity = session()->get("entity");

            if(empty($entity) && empty($token))
                return redirect url('/')->withErrors('Token and Entity data not found');

            if(!empty($entity)){
                $entity_id = $entity->entityid;
                $where = array("token" => $token, "entity_id" => $entity->entityid);
                $params = (array) DB::table("paypal_transactions")->select("cancel_url as CancelUrl, return_url as returnUrl, name, description, amount, currency, reference_id")->where($where)->orderBy("created_at", "desc")->first();
            }
        }

		$response = $gateway->completePurchase($params)->send();
        $paypalResponse = $response->getData();
        
        /*  PayPal payment was succesfull   */
		if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
            /*  Check if there is already a payment for this transaction    */
			$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

			$payment = new Payment();
      		$payment->loginid = Auth::user()->loginid;
            $payment->entity_id = $entity_id;
      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
      		$payment->save();
            
            /*  Get the receipt from the database using the reference_id passed to PayPal   */
            $receipt = PaymentReceipt::where('login_id', Auth::user()->loginid)
                ->where('receipt_id', $params['reference_id'])
                ->where('sent_status', STS_NG)
                ->first();
               
            /*  Update the receipt status to sent       */
            DB::table('payment_receipts')
                ->where('receipt_id', $receipt['receipt_id'])
                ->update(array('sent_status' => STS_OK));
                
            /*  Send the receipt to Customer's email    */
            if(session()->get("entity") && Auth::user()->role != 4 ) {
                $entity = session()->get("entity");
            } else {
                $entity = Entity::where('loginid' , Auth::user()->loginid)
                    ->where('is_paid', 0)
                    ->first();
            }

            if(env("APP_ENV") != "local"){
                $user = User::find(Auth::user()->loginid);

                if($user->parent_user_id != 0) {
					$user = User::find($user->parent_user_id);
                } 

                $mailHandler = new MailHandler();
                $mailHandler->sendReceipt(
                    Auth::user()->loginid,
                    [
                        'receipt' => $receipt,
                        'entity' => $entity,
                        'user' => $user,
                    ],
                    'CreditBPO Payment Receipt'
                );

                if($entity->is_premium == PREMIUM_REPORT) {
                    $mailHandler->sendPremiumReportNotification($entity);
                }
            }

            /* User is a Supervisor */
			if (Auth::user()->role==4) {
                /* Create Keys after Payment */
				if ($payment_check == 0) {
					BankSerialKeys::createKeys($payment->loginid, $receipt['item_quantity']);
				}

				return Redirect::to('/bank/keys');
			}
            /* User is an SME */
            else {
                $isFirst = false;    // User Account is newly created Flag
                $userReports = Entity::where('loginid', Auth::user()->loginid)->get();
				if (!$userReports->isEmpty()){
					if ($userReports->count() == 1){
                        $isFirst = true;
                        $user = User::where('loginid', '=', Auth::user()->loginid)->first();
                        if ($user->status != 2) {
                            $user->status = 2;
                            $user->save();
                        }
					}
                }
                
                /*  Payment is successful and not yet recorded  */
				if ($payment_check == 0) {
                    /*  Update Report to Paid Status    */
					$entity = Entity::where('loginid', Auth::user()->loginid)
                            ->where('is_paid', 0)
                            ->orderBy('entityid', 'desc')
                            ->first();
					$entity->is_paid = 1;
					$entity->save();
				}
                /*  If account is newly created show the MOA */
				if ($isFirst && ($entity->is_premium == STANDALONE_REPORT || $entity->is_premium == SIMPLIFIED_REPORT )) {
					return View::make('signup_confirmation');
                } elseif ($entity->is_premium == PREMIUM_REPORT) {
                    $region = Zipcode::where('id', $entity->cityid)->first();

                    $regionArea = $region->region;

                    if($regionArea == "NCR"){
                        return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 business days.');
                    }else{
                        return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 10 business days.');
                    }
                }
                /*  When user account is not new show the Report List */
                else {
					return Redirect::to('/dashboard/index');
				}
			}
		}
        /* PayPal Payment Failed. Show the error */
        else {
            $pay_sts = $paypalResponse['L_SHORTMESSAGE0'];
            $error = $paypalResponse['L_LONGMESSAGE0'];

            return View::make('errors.payment_error', array('pay_sts' => $pay_sts, 'error' => $error));
		}
	}

}
