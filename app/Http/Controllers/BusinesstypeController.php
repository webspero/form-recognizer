<?php
//======================================================================
//  Class 8: BusinesstypeController
//  Description: Contains functions regarding Businesstype panel in SME questionnaire
//======================================================================
class BusinesstypeController extends BaseController {

	//-----------------------------------------------------
    //  Function 8.1: getBusinesstype
	//  Description: get Business type page 
    //-----------------------------------------------------
	public function getBusinesstype($entity_id)
	{
		return View::make('sme.businesstype',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 8.2: postBusinesstype
	//  Description: function for updating Business type
    //-----------------------------------------------------
	public function postBusinesstype($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $messages   = array();								// variable container
        $rules      = array();								// variable container
        
        $security_lib   = new MaliciousAttemptLib;			// create instance of MaliciousAttemptLib
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#biztyp-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-biztyp';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages']	= $validator->messages()->all();
        }
        
        $messages    = array(		// validator message
            'is_import.required'        => 'Imports is Required',
            'is_export.required'        => 'Exports is Required',
        );
            
        $rules = array(				// validation rules
			'is_import'	=> 'required',
			'is_export'	=> 'required',
	    );

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
            $srv_resp['messages']	= $validator->messages()->all();
		}
        else {
            
            $businesstype   = Businesstype::where('entity_id', $entity_id)->first();
            
            if (0 >= @count($businesstype)) {
                $businesstype   = new Businesstype();
            }
            // populate date
            $businesstype->entity_id = $entity_id;
            $businesstype->is_import = Input::get('is_import');
            $businesstype->is_export = Input::get('is_export');

            if($businesstype->save()) {	// save
                $srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Added Record';
                
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $businesstype->businesstypeid;
                }
            }
            else {
                $srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
            }
        }
        ActivityLog::saveLog();				// log activity in db
        return json_encode($srv_resp);		// return json encoded results
	}

	//-----------------------------------------------------
    //  Function 8.3: getBusinesstypeDelete
	//  Description: function for deleting Business type
    //-----------------------------------------------------
    public function getBusinesstypeDelete($id)
	{
		DB::table('tblbusinesstype')
        ->where('businesstypeid', $id)
		->where('entity_id', Session::get('entity')->entityid)
		->delete();
        ActivityLog::saveLog();
        Session::flash('redirect_tab', 'tab1');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
