<?php
//======================================================================
//  Class 13: CompetitorController
//  Description: Contains functions regarding Competitor
//======================================================================
class CompetitorController extends BaseController {

	//-----------------------------------------------------
    //  Function 13.1: getCompetitor
	//  Description: get Competitor page 
    //-----------------------------------------------------
	public function getCompetitor($entity_id)
	{
        $entity = Entity::find($entity_id);
		return View::make('sme.competitor',
            array(
                'entity_id' => $entity_id,
                'entity' => $entity
            )
        );
	}

	//-----------------------------------------------------
    //  Function 13.2: postCompetitor
	//  Description: update function for Competitor
    //-----------------------------------------------------
	public function postCompetitor($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $competitors    = Input::get('competitor_name');
        $messages       = array();
        $rules          = array();
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $entity = Entity::find($entity_id);
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#competitor-list-cntr';
            $srv_resp['refresh_elems']  = '.reload-competitor';

            if($entity->is_premium == PREMIUM_REPORT) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            }
            
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		/* sets validation messages and rules */
        for ($idx = 0; $idx < @count($competitors); $idx++) {
            
            if ((STR_EMPTY != $competitors[$idx])
            || (0 == $idx)) {

                $messages['competitor_name.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'Name'));
                $messages['competitor_address.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'Address'));
                $messages['competitor_phone.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'Contact Phone Number'));
                $messages['competitor_phone.'.$idx.'.regex'] = trans('validation.phoneregex', array('attribute'=>'Contact Phone Number'));
                $messages['competitor_email.'.$idx.'.required']	= trans('validation.required', array('attribute'=>'Contact Email'));
                $messages['competitor_email.'.$idx.'.email']       = trans('validation.email', array('attribute'=>'Contact Email'));

                $rules['competitor_name.'.$idx]     = 'required';
                $rules['competitor_address.'.$idx]	= 'required';
                $rules['competitor_phone.'.$idx]	= 'required|regex:/^.{7,20}$/';

                if($entity->is_premium == PREMIUM_REPORT) {
                    $rules['competitor_email.'.$idx]	= 'sometimes|nullable|email';
                } else {
                    $rules['competitor_email.'.$idx]	= 'required|email';
                }
            }   
        }
        
		$validator = Validator::make(Input::all(), $rules, $messages);	// run validation

		if($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {	
			$arraydata = Input::only('competitor_name', 'competitor_address', 'competitor_phone', 'competitor_email');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			// set input data
			$competitor_name = $arraydata['competitor_name'];
			$competitor_address = $arraydata['competitor_address'];
			$competitor_phone = $arraydata['competitor_phone'];
			$competitor_email = $arraydata['competitor_email'];

			foreach ($competitor_name as $key => $value) {
				if($value){
					// populate data
					$data = array(
					    'entity_id'=>$entity_id, 
					   	'competitor_name'=>$competitor_name[$key],
					   	'competitor_address'=>$competitor_address[$key],
					   	'competitor_phone'=>$competitor_phone[$key],
					   	'competitor_email'=>$competitor_email[$key],
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblcompetitor')
                            ->where('competitorid', $primary_id[$key])
                            ->update($data);	// update and save
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblcompetitor')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
            
			$srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();			 		// log activity in db
        return json_encode($srv_resp);			// return json result
	}

	//-----------------------------------------------------
    //  Function 13.3: getCompetitorDelete
	//  Description: function for deleting competitor
    //-----------------------------------------------------
    public function getCompetitorDelete($id)
	{
        $competitor = Competitor::where('competitorid', '=', $id)
                        ->where('entity_id', '=', Session::get('entity')->entityid)
                        ->first();
        
        $competitor->is_deleted = 1;
        $competitor->save();

        ActivityLog::saveLog();
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
    }
}
