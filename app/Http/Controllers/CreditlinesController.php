<?php
//======================================================================
//  Class 15: CreditlinesController
//  Description: Contains functions regarding Adverse Record
//======================================================================
class CreditlinesController extends BaseController {

	//-----------------------------------------------------
    //  Function 15.1: getAdverserecord
	//  Description: get Adverse Record page 
    //-----------------------------------------------------
	public function getAdverserecord($entity_id)
	{
		return View::make('sme.adverserecord', array('entity_id' => $entity_id));
	}

	//-----------------------------------------------------
    //  Function 15.2: postAdverserecord
	//  Description: save function for Adverse Record 
    //-----------------------------------------------------
	public function postAdverserecord($entity_id)
	{
		$arraydata = Input::only('adverserecord_name', 'adverserecord_products', 'adverserecord_pastdue', 'adverserecord_dueamount');
		
		// get input data
		$adverserecord_name = $arraydata['adverserecord_name'];
		$adverserecord_products = $arraydata['adverserecord_products'];
		$adverserecord_pastdue = $arraydata['adverserecord_pastdue'];
		$adverserecord_dueamount = $arraydata['adverserecord_dueamount'];
		
		$messages = array();
		$rules = array();
		
		/* validation rules and messages */
		foreach($adverserecord_name as $key=>$value){
			if($key==0 || $value!=""){
			
				$messages['adverserecord_name.'.$key.'.required'] = trans('validation.required', array('attribute'=>'Adverse Record Name for set '.($key + 1)));
				$messages['adverserecord_products.'.$key.'.required'] = trans('validation.required', array('attribute'=>'Adverse Record Product for set '.($key + 1)));
				$messages['adverserecord_pastdue.'.$key.'.required'] = trans('validation.required', array('attribute'=>'Past Due for set '.($key + 1)));
				$messages['adverserecord_dueamount.'.$key.'.required'] = trans('validation.required', array('attribute'=>'Due Amount for set '.($key + 1)));
				$messages['adverserecord_dueamount.'.$key.'.numeric'] = trans('validation.numeric', array('attribute'=>'Due Amount for set '.($key + 1)));
				$messages['adverserecord_dueamount.'.$key.'.min'] = trans('validation.min.numeric', array('attribute'=>'Due Amount for set '.($key + 1)));

				$rules['adverserecord_name.'.$key]	= 'required';
				$rules['adverserecord_products.'.$key]	= 'required';
				$rules['adverserecord_pastdue.'.$key]	= array('required', 'date_format:"Y-m-d"');      
				$rules['adverserecord_dueamount.'.$key]	= 'required|numeric|min:0';
			}
		}
	
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			return Redirect::to('/sme/adverserecord/'.$entity_id)->withErrors($validator)->withInput();
		}
		else
		{	
			// populate data
			foreach ($adverserecord_name as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'=>$entity_id,
					    'adverserecord_name'=>$adverserecord_name[$key], 
					   	'adverserecord_products'=>$adverserecord_products[$key], 
					   	'adverserecord_pastdue'=>$adverserecord_pastdue[$key],
					   	'adverserecord_dueamount'=>$adverserecord_dueamount[$key],
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);
					DB::table('tbladverserecord')->insert($data);
				}
			}
			Session::flash('redirect_tab', 'tab4');
			return Redirect::to('/summary/smesummary/'.$entity_id);
		}
	}

	//-----------------------------------------------------
    //  Function 15.3: getAdverserecordEdit
	//  Description: get Adverse Record edit page
    //-----------------------------------------------------
	public function getAdverserecordEdit($id)
	{
		$entity = DB::table('tbladverserecord')
		->where('adverserecordid', $id)
		->get();
		return View::make('sme.adverserecord')->with('entity', $entity);
	}

	//-----------------------------------------------------
    //  Function 15.4: putAdverserecordUpdate
	//  Description: function to update Adverse Record
    //-----------------------------------------------------
	public function putAdverserecordUpdate($id)
	{
		$messages = array();

		$rules = array( // validation rules
	        'adverserecord_name.0'	=> 'required',
	        'adverserecord_products.0'	=> 'required',
	        'adverserecord_pastdue.0'	=> array('required', 'date_format:"Y-m-d"'),       
	        'adverserecord_dueamount.0'	=> 'required|numeric|min:0',
	    );
		
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			return Redirect::to('/sme/adverserecord/'.$id.'/edit')->withErrors($validator)->withInput();
		}
		else
		{	
			$dataFields = array(
				'adverserecord_name' => Input::get('adverserecord_name.0'),
				'adverserecord_products' => Input::get('adverserecord_products.0'),
				'adverserecord_pastdue' => Input::get('adverserecord_pastdue.0'),
				'adverserecord_dueamount' => Input::get('adverserecord_dueamount.0'),
			);
			
			$adverse_record = Adverserecord::find($id);
			$updateProcess = $adverse_record->update($dataFields);
			
			if($updateProcess)
			{
				Session::flash('redirect_tab', 'tab4');
				return Redirect::to('/summary/smesummary/'.$adverse_record->entity_id);
			}
			else
			{
				return Redirect::to('/sme/confirmation/tab6')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.Entity::getEntityId($adverse_record->loginid).'">Click here to return</a>');
			}
		}
	}
	
	//-----------------------------------------------------
    //  Function 15.5: getAdverserecordDelete
	//  Description: function to delete Adverse Record
    //-----------------------------------------------------
	public function getAdverserecordDelete($id)
	{
		$adverse_record = Adverserecord::find($id);
		DB::table('tbladverserecord')
        ->where('adverserecordid', $id)
		->delete();
                
        Session::flash('redirect_tab', 'tab4');
		return Redirect::to('/summary/smesummary/'.$adverse_record->entity_id);
	}

}
