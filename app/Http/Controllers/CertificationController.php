<?php
use CreditBPO\Handlers\UploadErrorHandler;
use CreditBPO\Models\UploadErrors;

//======================================================================
//  Class 12: CertificationController
//  Description: Contains functions regarding Certifications
//======================================================================
class CertificationController extends BaseController {

	//-----------------------------------------------------
    //  Function 12.1: getCertification
	//  Description: get Certification form
    //-----------------------------------------------------
	public function getCertification($entity_id)
	{   
        $entity = Entity::find($entity_id);
        $boi_peza = DB::table('tblcertifications')
			->where('entity_id', '=', $entity_id)
			->where('is_deleted',  0)
            ->where('certification_details', 'Board of Investments (BOI)')
            ->orWhere(function($query) use ($entity_id)
                {
                    $query->where('entity_id', '=', $entity_id)
                        ->where('certification_details', 'Philippine Export Processing Zone Authority (PEZA)');
                })
			->count();

		return View::make('sme.certification',
            array(
                'entity_id' => $entity_id,
                'boi_peza'  => $boi_peza,
                'entity'    => $entity
            )
        );
	}

	//-----------------------------------------------------
    //  Function 12.2: postCertification
	//  Description: function for adding certifications
    //-----------------------------------------------------
	public function postCertification($entity_id)
	{
		try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $messages   = array();
	        $rules      = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::find($entity_id);
	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#certifications-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-certifications';

	            if($entity->is_premium == PREMIUM_REPORT) {
	                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);
	            } else {    
	                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            }
	           
	        }

	        /*--------------------------------------------------------------------
			/*	Checks if the User is allowed to update the company
			/*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        /*--------------------------------------------------------------------
			/*	Fetch the inputs from the form
			/*------------------------------------------------------------------*/
	        $input_data = Input::except('_token');

	        $certification_details  = $input_data['certification_details'];
	        $certification_reg_no   = $input_data['certification_reg_no'];
	        $certification_reg_date = $input_data['certification_reg_date'];
	        $certification_doc      = Input::file('certification_doc');

	        $certificate_count      = @count($certification_details);

	        /*--------------------------------------------------------------------
			/*	Set Validation rules and messages for first set
			/*------------------------------------------------------------------*/
			

	        if($entity->is_premium == PREMIUM_REPORT) {
	            $rules      = array(
	                'certification_details.0'	=> 'required',
	                'certification_reg_no.0'	=> 'required',
	                'certification_reg_date.0'	=> 'required|date_format:"Y-m-d"|before: today',
	                'certification_doc.0'	    => 'mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt'
	            );
	        } else {
	            $rules      = array(
	                'certification_details.0'	=> 'required',
	                'certification_reg_no.0'	=> 'required',
	                'certification_reg_date.0'	=> 'required|date_format:"Y-m-d"|before: today',
	                'certification_doc.0'	    => 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt'
	            );
	        }

	        $messages    = array(
	            'certification_details.0.required'      => 'Please choose a Certification',
	            'certification_reg_no.0.required'       => 'Registration Number is required',
	            'certification_reg_date.0.required'     => 'Registration Date is required',
	            'certification_reg_date.0.before'       => 'Registration Date must be a date after today',
	            'certification_reg_date.0.date_format'  => 'Invalid Date Format',
	            'certification_doc.0.required'          => 'Document upload is required',

	            'certification_doc.0.mimes'             => 'Uploaded document must be a PDF, Excel, CSV, Zip or Rar file',
	        );

	        /*--------------------------------------------------------------------
			/*	Sets validation rules and messages for 2nd and more sets
			/*------------------------------------------------------------------*/
	        for ($idx = 1; $idx < $certificate_count; $idx++) {
	            $rules['certification_reg_no.'.$idx]     = 'required_with:certification_details.'.$idx;
	            $rules['certification_reg_date.'.$idx]   = 'required_with:certification_details.'.$idx;
	            $rules['certification_doc.'.$idx]        = 'required_with:certification_details.'.$idx.'|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt';

	            $messages['certification_reg_no.'.$idx.'.required_with']    = 'Registration Number is required for certification '.($idx + 1);
	            $messages['certification_reg_date.'.$idx.'.required_with']  = 'Registration Date is required for certification '.($idx + 1);
	            $messages['certification_doc.'.$idx.'.required_with']       = 'Document upload is required for certification '.($idx + 1);

	            $messages['certification_doc.'.$idx.'.mimes']               = 'Uploded document must be a PDF, Excel, CSV, Zip or Rar file for certification '.($idx + 1);
	        }

	        /*--------------------------------------------------------------------
			/*	Validate Form inputs
			/*------------------------------------------------------------------*/
			$validator = Validator::make(Input::all(), $rules, $messages);

	        /*--------------------------------------------------------------------
			/*	Some inputs are invalid
			/*------------------------------------------------------------------*/
			if($validator->fails())
			{

				$srv_resp['messages']	= $validator->messages()->all();

	            if (isset(Auth::user()->loginid)) {
	                if (STS_NG == $_COOKIE['formdata_support']) {
	                    return Redirect::to('/sme/certification/'.$entity_id)->withErrors($validator)->withInput();
	                }
	            }
			}
	        /*--------------------------------------------------------------------
			/*	All inputs are valid
			/*------------------------------------------------------------------*/
			else
			{
				
	            /*---------------------------------------------------------------
	            /*	Loops through all the input sets
	            /*-------------------------------------------------------------*/
				foreach ($certification_details as $key => $value) {

					if ($value) {
						
	                    /*-----------------------------------------------------
	                    /*	Saves the uploaded file
	                    /*---------------------------------------------------*/
	                    $filename ="";
	                    if (Input::hasFile('certification_doc.'.$key)) {

	                        /** Get the file    */
	                        $cert_doc                   = Input::file('certification_doc.'.$key);

	                        /** Sets the filename and destination dir    */
	                        $destination_path           = public_path('certification-docs');
	                        $extension                  = $cert_doc->getClientOriginalExtension();
	                        $filename                   = $certification_reg_date[$key].' '.$certification_details[$key].'-'.$certification_reg_no[$key].'-'.time().'.'.$extension;
	                        
	                        /** Upload the file    */
	                        $upload_success             = $cert_doc->move($destination_path, $filename);


	                        if(!empty($upload_success)){
		                        /*-----------------------------------------------------
			                    /*	Saves data to the database
			                    /*---------------------------------------------------*/
		                        $data = array(
								    'entity_id'                 => $entity_id,
								   	'certification_details'     => $certification_details[$key],
								   	'certification_reg_no'      => $certification_reg_no[$key],
								   	'certification_reg_date'    => $certification_reg_date[$key],
								   	'certification_doc'         => $filename,
								   	'created_at'                => date('Y-m-d H:i:s'),
							    	'updated_at'                => date('Y-m-d H:i:s')
								);

			                    
			                    $master_ids[]       = DB::table('tblcertifications')->insertGetId($data);

			                    $filenames[]        = $filename;
							}

		                    $srv_resp['sts']        = STS_OK;
		                    $srv_resp['messages']   = 'Successfully Added Record';
	                	}
					}

					
				}

	            if (!isset(Auth::user()->loginid)) {
	                $srv_resp['master_ids']                             = $master_ids;
	            }
			}

	        if (isset(Auth::user()->loginid)) {
	            if (STS_NG == $_COOKIE['formdata_support']) {
	                return Redirect::to('/summary/smesummary/'.$entity_id);
	            }
	        }
	        ActivityLog::saveLog();
	        return json_encode($srv_resp);
	    }
	    catch(Exception $ex){
        	$uploadError = [
        		'user_type' => Auth::user()->role,
        		'loginid' => Auth::user()->loginid,
        		'entityid' => $entity_id,
        		'error_details' => $ex->getMessage(),
        		'date_uploading' => date('Y-m-d H:i:s')
        	];

        	UploadErrorHandler::uploadErrorsUpdate($uploadError);
        	
        }

	}

	//-----------------------------------------------------
    //  Function 12.3: putCertificationUpdate
	//  Description: function for updating certifications
    //----------------------------------------------------
	public function putCertificationUpdate($id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
		$messages   = array();
		$rules      = array();

        /*--------------------------------------------------------------------
		/*	Set Validation rules and messages
		/*------------------------------------------------------------------*/
		$rules      = array(
            'certification_details.0'	=> 'required',
            'certification_reg_no.0'	=> 'required',
            'certification_reg_date.0'	=> 'required',

            'certification_doc.0'	    => 'mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt'
        );

        $messages    = array(
            'certification_details.0.required'  => 'Please choose a Certification',
            'certification_reg_no.0.required'   => 'Registration Number is required',
            'certification_reg_date.0.required' => 'Registration Date is required',

            'certification_doc.0.mimes'         => 'Uploaded document must be a PDF, Excel, CSV, Zip or Rar file',
        );

        /*--------------------------------------------------------------------
		/*	Validate Form inputs
		/*------------------------------------------------------------------*/
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*--------------------------------------------------------------------
		/*	Some inputs are invalid
		/*------------------------------------------------------------------*/
		if($validator->fails())
		{
			return Redirect::to('/sme/certification/'.$id.'/edit')->withErrors($validator)->withInput();
		}
        /*--------------------------------------------------------------------
		/*	All inputs are valid
		/*------------------------------------------------------------------*/
		else
		{
            /*--------------------------------------------------------------------
            /*	Get current database record of specified certification
            /*------------------------------------------------------------------*/
            $certification_data = Certifications::where('certificationid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();

            /*--------------------------------------------------------------------
            /*	Upload the file and delete the previous
            /*------------------------------------------------------------------*/
            if (Input::hasFile('certification_doc.0')) {
                /** Get the file    */
                $cert_doc                   = Input::file('certification_doc.0');

                /** Sets the filename and destination dir    */
                $destination_path           = 'certification-docs';
                $extension                  = $cert_doc->getClientOriginalExtension();
                $filename                   = 'cert-doc-'.$certification_data['certification_reg_no'].'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $cert_doc->move($destination_path, $filename);

                /** Delete previous file    */
                File::delete($destination_path.'/'.$certification_data['certification_doc']);

                /** Update database information    */
                $certification_data['certification_doc']    = $filename;

            }

            /*--------------------------------------------------------------------
            /*	Save changes to the database
            /*------------------------------------------------------------------*/
            $certification_data['certification_details']    = Input::get('certification_details.0');
            $certification_data['certification_reg_no']     = Input::get('certification_reg_no.0');
            $certification_data['certification_reg_date']   = Input::get('certification_reg_date.0');

            $update_process = $certification_data->save();

            /*--------------------------------------------------------------------
            /*	Update Successful
            /*------------------------------------------------------------------*/
			if($update_process)
			{
				Session::flash('redirect_tab', 'tab1');
				return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
			}
            /*--------------------------------------------------------------------
            /*	Update Failed
            /*------------------------------------------------------------------*/
			else
			{
				return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return.</a>');
			}
		}
	}

	//-----------------------------------------------------
    //  Function 12.4: getCertificationEdit
	//  Description: get certification update form
    //----------------------------------------------------
	public function getCertificationEdit($id)
	{
		$entity = DB::table('tblcertifications')
		->where('certificationid', $id)
		->where('is_deleted', 0)
		->where('entity_id', Session::get('entity')->entityid)
		->get();

		return View::make('sme.certification')->with('entity', $entity);
	}

	//-----------------------------------------------------
    //  Function 12.5: getCertificationDelete
	//  Description: function to delete certification
    //----------------------------------------------------
    public function getCertificationDelete($id)
	{
        $certification = Certifications::where('certificationid', '=', $id)
                        ->where('entity_id', '=', Session::get('entity')->entityid)
                        ->first();

        $certification->is_deleted = 1;
        $certification->save();

        ActivityLog::saveLog();
        Session::flash('redirect_tab', 'tab1');
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
    }

}
