<?php
//======================================================================
//  Class 6: BusinessdriverController
//  Description: Contains functions regarding Businessdriver panel in SME questionnaire
//======================================================================
class BusinessdriverController extends BaseController {

	//-----------------------------------------------------
    //  Function 6.1: getBusinessdriver
	//  Description: get Businessdriver page 
    //-----------------------------------------------------
	public function getBusinessdriver($entity_id)
	{
		return View::make('sme.businessdriver',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 6.2: postBusinessdriver
	//  Description: function for updating Businessdriver
    //-----------------------------------------------------
	public function postBusinessdriver($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $competitors    = Input::get('competitor_name');
        $messages       = array();
        $rules          = array();
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#bizdriver-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-bizdriver';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		$messages = array(				// validation messages
			'businessdriver_name.0.required'	=> trans('validation.required', array('attribute'=>trans('business_details2.bdriver_name'))),
	        'businessdriver_total_sales.0.required'	=> trans('validation.required', array('attribute'=>trans('business_details2.bdriver_share'))),
	        'businessdriver_total_sales.0.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details2.bdriver_share'))),
	        'businessdriver_total_sales.0.min'	=> trans('validation.min.numeric', array('attribute'=>trans('business_details2.bdriver_share'))),
	        'businessdriver_total_sales.0.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details2.bdriver_share'))),
		);

		$rules = array(					// validation rules
	        'businessdriver_name.0'	=> 'required',
	        'businessdriver_total_sales.0'	=> 'required|numeric|min:0|max:100',
		);
	
		$validator = Validator::make(Input::all(), $rules, $messages);	// run validator

		if($validator->fails())	{
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {	
			$arraydata = Input::only('businessdriver_name', 'businessdriver_total_sales');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			$businessdriver_name = $arraydata['businessdriver_name'];
			$businessdriver_total_sales = $arraydata['businessdriver_total_sales'];

			foreach ($businessdriver_name as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'=>$entity_id, 
					   	'businessdriver_name'=>$businessdriver_name[$key],					   	
					   	'businessdriver_total_sales'=>$businessdriver_total_sales[$key], 
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblbusinessdriver')
                            ->where('businessdriverid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblbusinessdriver')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
			$srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();				// log activity in db
        return json_encode($srv_resp);		// return json encoded results
	}
    
	//-----------------------------------------------------
    //  Function 6.3: getBusinessdriverDelete
	//  Description: function for deleting Businessdriver
    //-----------------------------------------------------
    public function getBusinessdriverDelete($id)
	{
		// DB::table('tblbusinessdriver')
        // ->where('businessdriverid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
		// ->delete();

		$businessdriver = Businessdriver::where('businessdriverid', '=', $id)
							->where('entity_id', '=', Session::get('entity')->entityid)
							->first();
		$businessdriver->is_deleted = 1;
		$businessdriver->save();

		ActivityLog::saveLog();
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
