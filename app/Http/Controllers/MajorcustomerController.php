<?php

use CreditBPO\Handlers\MajorCustomerHandler;

//======================================================================
//  Class 34: Major Customer Controller
//      Functions related to Major Customer on the Questionnaire
//      are routed to this class
//======================================================================
class MajorcustomerController extends BaseController
{
    //-----------------------------------------------------
    //  Function 34.1: getMajorcustomer
    //      Displays the Major Customer Form
    //-----------------------------------------------------
	public function getMajorcustomer($entity_id)
	{
        $entity = Entity::find($entity_id);
		return View::make('sme.majorcustomer',
            [
                'entity_id' => $entity_id,
                'relationship_satisfaction' =>
                    MajorCustomerHandler::getAllRelationshipSatisfaction(),
                'entity' => $entity
            ]
        );
	}

    //-----------------------------------------------------
    //  Function 34.2: postMajorcustomer
    //      HTTP Post request to validate and save Major
    //      Customers data in the database
    //-----------------------------------------------------
	public function postMajorcustomer($entity_id)
	{
        /*  Variable Declaration    */
        $customers  = Input::get('customer_name');  // Customer Name
        $messages   = array();                      // Validation Messages according to rules
        $rules      = array();                      // Validation Rules

        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        $srv_resp['major_customer'] = 1;

        $entity = Entity::find($entity_id);

        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#major-cust-list-cntr';                      // Refresh the specified container
            $srv_resp['refresh_elems']  = '.reload-major-cust';                         // Elements that will be refreshed

            if($entity->is_premium == PREMIUM_REPORT) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);    // URL to fetch data from
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            }
        }

        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($customers); $idx++) {

            /*  Always validate the first input set and
            *   for the succeeding validate those with Major Customer name
            */
            if ((STR_EMPTY != $customers[$idx])
            || (0 == $idx)) {

                $messages['customer_name.'.$idx.'.required']                    = trans('validation.required', array('attribute'=>'Customer Name'));
                $messages['customer_share_sales.'.$idx.'.required']	            = trans('validation.required', array('attribute'=>trans('business_details2.mc_percent_share')));
                $messages['customer_share_sales.'.$idx.'.numeric']              = trans('validation.numeric', array('attribute'=>trans('business_details2.mc_percent_share')));
                $messages['customer_share_sales.'.$idx.'.min']	                = trans('validation.min.numeric', array('attribute'=>trans('business_details2.mc_percent_share')));
                $messages['customer_share_sales.'.$idx.'.max']	                = trans('validation.max.numeric', array('attribute'=>trans('business_details2.mc_percent_share')));
                $messages['customer_address.'.$idx.'.required']                 = trans('validation.required', array('attribute'=>'Address'));
                $messages['customer_contact_person.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>'Contact Person'));
                $messages['customer_phone.'.$idx.'.required']	                = trans('validation.required', array('attribute'=>'Contact Phone Number'));
                $messages['customer_phone.'.$idx.'.regex']	                    = trans('validation.phoneregex', array('attribute'=>'Contact Phone Number'));
                $messages['customer_email.'.$idx.'.required']	                = trans('validation.required', array('attribute'=>'Contact Email'));
                $messages['customer_email.'.$idx.'.email']	                    = trans('validation.email', array('attribute'=>'Contact Email'));
                $messages['customer_started_years.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>trans('business_details2.mc_year_started')));
                $messages['customer_started_years.'.$idx.'.date_format']        = 'Invalid Date Format';
                $messages['customer_started_years.'.$idx.'.after']              = 'Year cannot be lower than 1970';
                $messages['customer_started_years.'.$idx.'.before']             = 'Year cannot exceed the year now';
                $messages['customer_years_doing_business.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details2.mc_year_doing_business')));
                $messages['customer_years_doing_business.'.$idx.'.numeric']     = trans('validation.numeric', array('attribute'=>trans('business_details2.mc_year_doing_business')));
                $messages['customer_settlement.'.$idx.'.required']	            = trans('validation.required', array('attribute'=>trans('business_details2.mc_payment_behavior')));
                $messages['customer_order_frequency.'.$idx.'.required']         = trans('validation.required', array('attribute'=>trans('business_details2.mc_order_frequency')));
                $messages['relationship_satisfaction.'.$idx.'.required'] = trans(
                    'validation.required',
                    [
                        'attribute' => trans('business_details2.mc_relationship_satisfaction')
                    ]
                );
                
                
                if($entity->is_premium == PREMIUM_REPORT) {
                    $rules['customer_name.'.$idx]                   = 'required';
                    $rules['customer_address.'.$idx]                = 'required';
                    $rules['customer_share_sales.'.$idx]            = request('customer_share_sales')[0] == null ? '' : 'numeric|min:0|max:100';
                    $rules['customer_phone.'.$idx]	                = request('customer_phone')[0] == null ? '' : 'regex:/^.{7,20}$/';
                    $rules['customer_email.'.$idx]	                = request('customer_email')[0] == null ? '' : 'email';
                    $rules['customer_started_years.'.$idx]          = request('customer_started_years')[0] == null ? '' : 'date_format:"Y"|after:1969|before:next year';
                    $rules['customer_years_doing_business.'.$idx]	= request('customer_years_doing_business')[0] == null ? '' : 'numeric';
                   
                } else {
                    $rules['customer_name.'.$idx]                   = 'required';
                    $rules['customer_share_sales.'.$idx]            = 'required|numeric|min:0|max:100';
                    $rules['customer_address.'.$idx]                = 'required';
                    $rules['customer_contact_person.'.$idx]         = 'required';
                    $rules['customer_phone.'.$idx]	                = 'required|regex:/^.{7,20}$/';
                    $rules['customer_email.'.$idx]	                = 'required|email';
                    $rules['customer_started_years.'.$idx]          = 'required|date_format:"Y"|after:1969|before:next year';
                    $rules['customer_years_doing_business.'.$idx]	= 'required|numeric';
                    $rules['customer_settlement.'.$idx]             = 'required';
                    $rules['customer_order_frequency.0']            = 'required';
                    $rules['relationship_satisfaction.'.$idx] = 'required';
                }
                
            }
        }

        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else
		{
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('customer_name', 'customer_share_sales', 'customer_address', 'customer_contact_person', 'customer_phone','customer_email', 'customer_started_years', 'customer_years_doing_business', 'customer_settlement', 'customer_order_frequency', 'relationship_satisfaction');

            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }

            /*  Stores data into an array for loop traversal    */
			$customer_name                  = $arraydata['customer_name'];
			$customer_share_sales           = $arraydata['customer_share_sales'];
			$customer_address               = $arraydata['customer_address'];
			$customer_contact_person        = $arraydata['customer_contact_person'];
			$customer_phone                 = $arraydata['customer_phone'];
			$customer_email                 = $arraydata['customer_email'];
			$customer_started_years         = $arraydata['customer_started_years'];
			$customer_years_doing_business  = $arraydata['customer_years_doing_business'];
			$customer_settlement            = $arraydata['customer_settlement'];
			$customer_order_frequency       = $arraydata['customer_order_frequency'];
            $relationship_satisfaction = $arraydata['relationship_satisfaction'];

            $report         = Entity::select('anonymous_report')->where('entityid', $entity_id)->first();

            if (1 == $report->anonymous_report) {
                $max_customer   = Majorcustomer::where('entity_id', $entity_id)->get();
                $max_customer   = @count($max_customer);
            }

            /*  Saves Input to the database     */
			foreach ($customer_name as $key => $value) {
				if ($value) {
                    if (1 == $report->anonymous_report) {
                        $max_customer++;
                        $cust_name  = 'Major Customer '.$max_customer;
                    }
                    else {
                        $cust_name  = $customer_name[$key];
                    }

					$data = array(
					    'entity_id'                     =>$entity_id,
					   	'customer_name'                 =>$cust_name,
					   	'customer_address'              =>$customer_address[$key],
					   	'customer_share_sales'          =>$customer_share_sales[$key] == null ? '' : $customer_share_sales[$key],
					   	'customer_contact_person'       =>$customer_contact_person[$key] == null ? '' : $customer_contact_person[$key],
					   	'customer_phone'                =>$customer_phone[$key] == null ? '' : $customer_phone[$key],
					   	'customer_email'                =>$customer_email[$key] == null ? '' : $customer_email[$key],
					   	'customer_started_years'        =>$customer_started_years[$key] == null ? '' : $customer_started_years[$key],
					   	'customer_years_doing_business' =>$customer_years_doing_business[$key] == null ? '' : $customer_years_doing_business[$key],
					   	'customer_settlement'           =>$customer_settlement[$key] == null ? '' : $customer_settlement[$key],
					   	'customer_order_frequency'      =>$customer_order_frequency[$key] == null ? '' : $customer_order_frequency[$key],
                        'relationship_satisfaction'     =>$relationship_satisfaction[$key] == null ? '' : $relationship_satisfaction[$key],
					   	'created_at'                    =>date('Y-m-d H:i:s'),
				    	'updated_at'                    =>date('Y-m-d H:i:s')
                    );
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {

                        DB::table('tblmajorcustomer')
                            ->where('majorcustomerrid', $primary_id[$key])
                            ->update($data);

                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblmajorcustomer')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}

            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;

            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}

        /*  Logs the action to the database */
        ActivityLog::saveLog();

        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 34.3: putMajorcustomerUpdate
    //      HTTP Post request to validate and save updates
    //      of specified Major Customer data in the database
    //      (Currently not in used)
    //-----------------------------------------------------
	public function putMajorcustomerUpdate($id)
	{
        /*  Sets the Validation Messages    */
		$messages = array(
			'customer_name.0.required'                  => trans('validation.required', array('attribute'=>'Customer Name')),
			'customer_share_sales.0.required'           => trans('validation.required', array('attribute'=>trans('business_details2.mc_percent_share'))),
			'customer_share_sales.0.numeric'            => trans('validation.numeric', array('attribute'=>trans('business_details2.mc_percent_share'))),
			'customer_share_sales.0.min'	            => trans('validation.min.numeric', array('attribute'=>trans('business_details2.mc_percent_share'))),
			'customer_share_sales.0.max'	            => trans('validation.max.numeric', array('attribute'=>trans('business_details2.mc_percent_share'))),
			'customer_address.0.required'	            => trans('validation.required', array('attribute'=>'Address')),
			'customer_contact_person.0.required'	    => trans('validation.required', array('attribute'=>'Contact Person')),
            'customer_phone.0.required'	                => trans('validation.required', array('attribute'=>'Contact Phone Number')),
			'customer_phone.0.regex'	                => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
			'customer_email.0.required'	                => trans('validation.required', array('attribute'=>'Contact Email')),
			'customer_email.0.email'	                => trans('validation.email', array('attribute'=>'Contact Email')),
			'customer_started_years.0.required'	        => trans('validation.required', array('attribute'=>trans('business_details2.mc_year_started'))),
			'customer_started_years.0.digits'	        => trans('validation.digits', array('attribute'=>trans('business_details2.mc_year_started'))),
			'customer_years_doing_business.0.required'	=> '',
			'customer_years_doing_business.0.numeric'	=> '',
			'customer_settlement.0.required'            => trans('validation.required', array('attribute'=>trans('business_details2.mc_payment_behavior'))),
			'customer_order_frequency.0.required'       => trans('validation.required', array('attribute'=>trans('business_details2.mc_order_frequency'))),
		);

        /*  Sets the Validation Rules       */
		$rules = array(
			'customer_name.0'                   => 'required',
			'customer_share_sales.0'	        => 'required|numeric|min:0|max:100',
			'customer_address.0'                => 'required',
			'customer_contact_person.0'         => 'required',
			'customer_phone.0'	                => 'required|regex:/^.{7,20}$/',
			'customer_email.0'	                => 'required|email',
			'customer_started_years.0'	        => 'required|digits:4',
			'customer_years_doing_business.0'   => 'required|numeric',
			'customer_settlement.0'             => 'required',
			'customer_order_frequency.0'        => 'required',
		);

        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if ($validator->fails()) {
			return Redirect::to('/sme/majorcustomer/'.$id.'/edit')->withErrors($validator)->withInput();
		}
        /*  Input validation success    */
		else {
            /*  Sets the user input to they matching database field */
			$dataFields = array(
				'customer_name'                 => Input::get('customer_name.0'),
				'customer_share_sales'          => Input::get('customer_share_sales.0'),
				'customer_address'              => Input::get('customer_address.0'),
				'customer_contact_person'       => Input::get('customer_contact_person.0'),
				'customer_phone'                => Input::get('customer_phone.0'),
				'customer_email'                => Input::get('customer_email.0'),
				'customer_started_years'        => Input::get('customer_started_years.0'),
				'customer_years_doing_business' => Input::get('customer_years_doing_business.0'),
				'customer_settlement'           => Input::get('customer_settlement.0'),
				'customer_order_frequency'      => Input::get('customer_order_frequency.0'),
			);

            /*  Saves the changes to the Database   */
			$updateProcess = Majorcustomer::where('majorcustomerrid', '=', $id)
                ->where('entity_id', '=', Session::get('entity')->entityid)
                ->update($dataFields);

            /*  Major Customer checkbox is ticked   */
			$accountFields          = array('is_agree' => Input::get('agree'));
			$updateAccountProcess   = Majorcustomeraccount::where('entity_id', '=', Session::get('entity')->entityid)->update($accountFields);

            /*  Save to database was successful */
			if ($updateProcess) {
                /*  Redirect to summary page    */
				return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
			}
            /*  Save to database was failed */
			else {
                /*  Redirect to Major Customer Form with error  */
				return Redirect::to('/sme/confirmation/tab2')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return</a>');
			}
		}
	}

    //-----------------------------------------------------
    //  Function 34.4: getMajorcustomerEdit
    //      Displays the Major Customer Edit Form
    //      (Currently not in used)
    //-----------------------------------------------------
	public function getMajorcustomerEdit($id)
	{
        /*  Query the database for the specified Major Customer */
		$entity = DB::table('tblmajorcustomer')
            ->where('majorcustomerrid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->get();

        /*  Query the database for the Major customer tickbox   */
		$mcaccount = DB::table('tblmajorcustomeraccount')
            ->where('majorcustomeraccountid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->first();

        /*  Pass the Data to the Major Customer Form    */
		return View::make('sme.majorcustomer',
                [
                    'relationship_satisfaction' =>
                        MajorCustomerHandler::getAllRelationshipSatisfaction(),
                ]
            )
            ->with('entity', $entity)
            ->with('mcaccount', $mcaccount);
	}

    //-----------------------------------------------------
    //  Function 31.5: getMajorcustomerDelete
    //      HTTP GET request to delete specified Major Customer
    //          (Currently not in used)
    //-----------------------------------------------------
	public function getMajorcustomerDelete($id)
	{
        /*  Query the database for the specified Major Customer and delete it   */
        $majorCustomer = Majorcustomer::where('majorcustomerrid', '=', $id)->first();

        $majorCustomer->is_deleted = 1;
        $majorCustomer->save();

        /*  Logs the action to the database */
        ActivityLog::saveLog();
        Session::flash('redirect_tab', 'tab1');
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}

    //-----------------------------------------------------
    //  Function 34.6: updateMajorAccount
    //      Updates the related Major Customer Tickbox
    //-----------------------------------------------------
    public function updateMajorAccount()
    {
        /*  Variable Declaration    */
        $entity_id  = Input::get('entity_id');          // Report ID
        $major_cust = Input::get('no_major_customer');  // Major Customer Tickbox

        /*  Query the database if Major Customer tickbox record already exist   */
        $major_cust_data = DB::table('tblmajorcustomeraccount')
			->where('entity_id', $entity_id)
			->first();

        /*  If a record exist update it according to checkbox   */
        if (0 < @count($major_cust_data)) {

			$upd_sts = Majorcustomeraccount::where('entity_id', '=', $entity_id)
                ->update(array('is_agree' => $major_cust));
        }
        /*  When a record does not exist create one */
        else {
            $major_cust_data    = new Majorcustomeraccount();

            $major_cust_data->entity_id = $entity_id;
            $major_cust_data->is_agree  = $major_cust;
            $major_cust_data->save();
        }

        /*  Logs the action to the database */
		ActivityLog::saveLog();
    }

}
