<?php
use \Exception as Exception;
use CreditBPO\Handlers\UploadErrorHandler;
use CreditBPO\Models\UploadErrors;


//======================================================================
//  Class 31: Locations Controller
//      Functions related to Main Locations on the Questionnaire
//      are routed to this class
//======================================================================
class LocationsController extends BaseController
{
    //-----------------------------------------------------
    //  Function 31.1: getLocations
    //      Displays the Main Locations Form
    //-----------------------------------------------------
	public function getLocations($entity_id)
	{
		return View::make('sme.locations',
            array(
                'entity_id' => $entity_id
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 31.2: postLocations
    //      HTTP Post request to validate and save Main
    //      Locations data in the database
    //-----------------------------------------------------
	public function postLocations($entity_id)
	{

		try{
	        /*  Variable Declaration    */
	        $locations  = Input::get('location_size');  // Size of the Main Location
	        $messages   = array();                      // Validation Messages according to rules
	        $rules      = array();                      // Validation Rules
	        
	        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
	        $sts            = STS_NG;

	        $entity = Entity::find($entity_id);
	        
	        /*  Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;
	        
	        /*  Request did not come from API set the server response   */
	        if ((!Input::has('action')) && (!Input::has('section'))) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
	            $srv_resp['refresh_cntr']   = '#main-loc-list-cntr';                        // Refresh the specified container
	            $srv_resp['refresh_elems']  = '.reload-main-loc';                           // Elements that will be refreshed

	            if($entity->is_premium == PREMIUM_REPORT) {
	                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);    // URL to fetch data from
	            } else {
	                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
	            }     
	        }
	        
	        /*  Checks if the User is allowed to update the company */
	        $sts            = $security_lib->checkUpdatePermission($entity_id);
	        
	        /*  User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }
	        
	        /*  Sets the validation rules and messages according to received Form Inputs    */
	        for ($idx = 0; $idx < @count($locations); $idx++) {
	            
	            if ((STR_EMPTY != $locations[$idx])
	            || (0 == $idx)) {

	                $messages['location_size.'.$idx.'.required']       = trans('validation.required', array('attribute'=>trans('business_details1.size_of_premises')));
	                $messages['location_size.'.$idx.'.numeric']        = trans('validation.numeric', array('attribute'=>trans('business_details1.size_of_premises')));
	                $messages['location_size.'.$idx.'.min']             = trans('validation.min.numeric', array('attribute'=>trans('business_details1.size_of_premises')));
	                $messages['location_size.'.$idx.'.max']             = trans('validation.max.numeric', array('attribute'=>trans('business_details1.size_of_premises')));
	                $messages['location_type.'.$idx.'.required']       = trans('validation.required', array('attribute'=>trans('business_details1.premises_owned_leased')));
	                $messages['location_map.'.$idx.'.required']        = trans('validation.required', array('attribute'=>trans('business_details1.location')));
	                $messages['location_used.'.$idx.'.required']       = trans('validation.required', array('attribute'=>trans('business_details1.premise_used_as')));
	                $messages['floor_count.'.$idx.'.required']          = trans('validation.required', array('attribute'=>'# of Floors'));
	                $messages['floor_count.'.$idx.'.numeric']          = trans('validation.numeric', array('attribute'=> '# of Floors'));
	                $messages['address.'.$idx.'.required']              = trans('validation.required', array('attribute' => 'Address'));
	                $messages['material.'.$idx.'.required']              = trans('validation.required', array('attribute' => 'Material'));
	                $messages['color.'.$idx.'.required']                 = trans('validation.required', array('attribute' => 'Color'));

	                $messages['map.'.$idx.'.required']                 = trans('validation.required', array('attribute' => 'Map'));
	                $messages['map.'.$idx.'.mimes']                 = trans('validation.mimes', array('attribute' => 'Map', 'value' => 'jpeg,png,jpg,gif'));
	                $messages['location_photo.'.$idx.'.required']      = trans('validation.required', array('attribute' => 'Actual Location Photo'));
	                $messages['location_photo.'.$idx.'.mimes']      = trans('validation.mimes', array('attribute' => 'Actual Location Photo', 'value' => 'jpeg,png,jpg,gif'));

	                $rules['location_size.'.$idx]	= 'required|numeric|min:0|max:2147483647';
	                $rules['location_type.'.$idx]	= 'required';
	                $rules['location_map.'.$idx]	= 'required';
	                $rules['location_used.'.$idx]	= 'required';
	                $rules['floor_count.'.$idx]	= 'required|numeric';
	                $rules['address.'.$idx]	= 'required';
	                $rules['material.'.$idx]	= 'required';
	                $rules['color.'.$idx]	= 'required';
	                $rules['map.'.$idx]     = 'required|mimes:png,jpg,jpeg,gif';
	                $rules['location_photo.'.$idx]    = 'required|mimes:png,jpg,jpeg,gif';
	            }
	        }

	        $newLocationCount = DB::table('tbllocations')
	                            ->where('entity_id', '=', $entity->entityid)
	                            ->where('is_deleted', 0)
	                            ->get();

	        $newLocationCount = @count($newLocationCount) + 1;
	        
	        /*  Run the Laravel Validation  */
			$validator = Validator::make(Input::all(), $rules, $messages);
	        
	        /*  Input validation failed     */
			if ($validator->fails()) {
				$srv_resp['messages']	= $validator->messages()->all();
			}
	        /*  Input validation success    */
			else {
	            /*  Stores all inputs from the form to an array for saving and looping  */
				$arraydata = Input::only('location_size', 'location_type', 'location_map', 'location_used', 'floor_count', 'address', 'material', 'color', 'map', 'location_photo', 'other');
	            
	            /*  Check if there is a Master Key
	            *   Master key input is used for API Editing
	            */
	            if (Input::has('master_key')) {
	                $primary_id     = Input::get('master_key');
	            }

	            //$destinationPath = public_path('documents/location/'.$entity_id);
	            $destinationPath = public_path('images/locations/'.$entity_id);

	            if (!file_exists($destinationPath)) {
	                mkdir($destinationPath, 0777, true);
	                chmod($destinationPath, 0775);
	            }
	            
	            /*  Stores data into an array for loop traversal    */
				$location_size  = $arraydata['location_size'];
				$location_type  = $arraydata['location_type'];
				$location_map   = $arraydata['location_map'];
	            $location_used  = $arraydata['location_used'];
	            $floor_count = $arraydata['floor_count'];
	            $address = $arraydata['address'];
	            $material = $arraydata['material'];
	            $color = $arraydata['color'];
	            $other = $arraydata['other'];

	            $map = Input::file('map');
	            $location_photo = Input::file('location_photo');

	            $newMapFile = "";
	            $newLocationFile = "";

	            //save map photo
	            foreach($map as $key => $value) {
	                if($value) {
	                    $extension = $value->getClientOriginalExtension();
	                    $newFileName = "map for location " . $newLocationCount . " " . date('Y-m-d');
	                    
	                    $file = $newFileName.'.'.$extension;

	                    $value->move($destinationPath, $file);

	                    $newMapFile = $entity_id.'/'.$file;
	                }
	            }
	            
	            //save location photo
	            foreach($location_photo as $key => $value) {
	                if($value) {
	                    $extension = $value->getClientOriginalExtension();
	                    $newFileName = "photo for location " . $newLocationCount . " " . date('Y-m-d');

	                    $file = $newFileName.'.'.$extension;

	                    $value->move($destinationPath, $file);

	                    $newLocationFile = $entity_id.'/'.$file;
	                }
	            }
	            
	            /*  Saves Input to the database     */
				foreach ($location_size as $key => $value) {
					if ($value) {
						$data = array(
						    'entity_id'         =>$entity_id, 
						   	'location_size'     =>$location_size[$key],
						   	'location_type'     =>$location_type[$key],
						   	'location_map'      =>$location_map[$key],
	                        'location_used'     =>$location_used[$key],
	                        'floor_count'       =>$floor_count[$key],
	                        'address'           =>$address[$key],
	                        'material'          =>$material[$key],
	                        'color'             =>$color[$key],
	                        'map'               =>$newMapFile,
	                        'location_photo'    =>$newLocationFile,
						   	'created_at'        =>date('Y-m-d H:i:s'),
	                        'updated_at'        =>date('Y-m-d H:i:s'),
	                        'other'             =>$other[$key]
						);
	                    
	                    /*  When an edit request comes from the API */
	                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
	                        
	                        DB::table('tbllocations')
	                            ->where('locationid', $primary_id[$key])
	                            ->update($data);
	                        
	                        $master_ids[]           = $primary_id[$key];
	                        $srv_resp['messages']   = 'Successfully Updated Record';
	                    }
	                    /*  Request came from Web Application via browser   */
	                    else {
	                        $master_ids[]           = DB::table('tbllocations')->insertGetId($data);
	                        $srv_resp['messages']   = 'Successfully Added Record';
	                    }
					}
				}
				
	            /*  Sets request status to success  */
	            $srv_resp['sts']        = STS_OK;
	            
	            /*  Sets the master id for output when request came from API    */
	            if ((Input::has('action')) && (Input::has('section'))) {
	                $srv_resp['master_ids'] = $master_ids;
	            }
			}
	        
	        /*  Logs the action to the database */
	        ActivityLog::saveLog();
	        
	        /*  Encode server response variable to JSON for output  */
	        return json_encode($srv_resp);
	    }
	    catch(Exception $ex){
        	$uploadError = [
        		'user_type' => Auth::user()->role,
        		'loginid' => Auth::user()->loginid,
        		'entityid' => $entity_id,
        		'error_details' => $ex->getMessage(),
        		'date_uploading' => date('Y-m-d H:i:s')
        	];

        	UploadErrorHandler::uploadErrorsUpdate($uploadError);
        	
        }
	}
    
    //-----------------------------------------------------
    //  Function 31.3: getLocationsDelete
    //      HTTP GET request to delete specified Main Locations
    //          (Currently not in used)
    //-----------------------------------------------------
	public function getLocationsDelete($id)
	{
        $location = Locations::where('locationid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();
        $location->is_deleted = 1;
        $location->save();
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Adds a one-off session variable for redirection */
		Session::flash('redirect_tab', 'tab1');
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
    }

    //-----------------------------------------------------
    //  Function 31.1: getLocations
    //      Displays the Main Locations Form
    //-----------------------------------------------------
	public function getLocationsPremium($entity_id)
	{
		return View::make('sme.locations_premium',
            array(
                'entity_id' => $entity_id
            )
        );
	}

}
