<?php

//======================================================================
//  Class 56: Google API Client Controller
//      Functions related to Google API Client
//======================================================================
class GoogleApiClientController extends BaseController
{
    var $spreadsheet_id     = '1Xq_56nSlkzCnvXVmmTzuk50dKOs0zCs6xWRB7ovJZbU';
    var $mp_lead_list_id    = '12265';
    var $mp_auth_token      = 'WYFz8J6yN8UVzZJg8KZb';
    var $google_email       = 'creditbpo.philippines@gmail.com';
    var $google_timezone    = 'Asia/Manila';
    
    //-----------------------------------------------------
    //  Function 56.1: __construct
    //      Initialization of Class
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing    */
    }
    
    //-----------------------------------------------------
    //  Function 56.2: executeLeadExtraction()
    //      Initialization of Class
    //-----------------------------------------------------
    public function executeLeadExtraction()
    {
        $leads  = $this->getMyPhonerLeads();
        $exist  = $this->checkLeadsExist($leads);
    }
    
    //-----------------------------------------------------
    //  Function 56.3: getMyPhonerLeads
    //      Responds with the Industries and Provinces
    //      according to request
    //-----------------------------------------------------
    public function getMyPhonerLeads()
    {
        /*--------------------------------------------------------------------
        /*	Removes maximum execution time limit
        /*------------------------------------------------------------------*/
        set_time_limit(0);
        
        /*--------------------------------------------------------------------
        /*	Variable Declaration
        /*------------------------------------------------------------------*/
        $sts        = STS_NG;
        $hdr        = array();
        $leads      = array();
        $post_url   = 'https://creditbpo.myphoner.com/api/v1/lists/'.$this->mp_lead_list_id.'/leads';
        
        /*  Sets Header Values of MyPhoner Request  */
        $hdr[]      = 'Accept: application/json';
        $hdr[]      = 'Content-type: application/json';
        $hdr[]      = 'Authorization: Token "'.$this->mp_auth_token.'"';
        
        /*--------------------------------------------------------------------
        /*	Queries MyPhoner API via CuRL
        /*------------------------------------------------------------------*/
        /* Initializes the CuRL Library */
        $curl = curl_init();
        
        /* Sets the CuRL POST Request configuration  */
        curl_setopt($curl, CURLOPT_URL, $post_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$hdr);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        /* Execute CuRL Request */
        $result = curl_exec($curl);
        
        /*  Closes the CuRL Library */
        curl_close($curl);
        
        /*  Converts MyPhoner Response JSON to Array    */
        $leads = json_decode($result);
        
        return $leads;
    }
    
    //-----------------------------------------------------
    //  Function 56.4: checkLeadsExist
    //      Checks if the lead exist in the database
    //      according to request
    //-----------------------------------------------------
    public function checkLeadsExist($leads)
    {
        /*--------------------------------------------------------------------
        /*	Variable Declaration
        /*------------------------------------------------------------------*/
        $date_changed       = STS_NG;
        $myphoner_data      = array();
        $leads_id_arr       = array();
        $leads_trial_arr    = array();
        $lead_count         = @count($leads);
        
        /*--------------------------------------------------------------------
        /*	Gets all MyPhoner Leads from the Database
        /*------------------------------------------------------------------*/
        $myphoner_data  = DB::table('myphoner_leads')
            ->where('status', ACTIVE)
            ->get();
        
        /*  Stores MyPhoner data into an array for comparison   */
        foreach ($myphoner_data as $myphoner) {
            $leads_id_arr[]     = $myphoner->myphoner_id;
            $leads_trial_arr[]  = $myphoner->trial_start_date;
        }
        //print_r($leads);return 1;
        /*--------------------------------------------------------------------
        /*	Determine if Lead already exists in the database
        /*------------------------------------------------------------------*/
        foreach ($leads as $lead) {
            
            $lead_key   = array_search($lead->id, $leads_id_arr);
            
            /* Lead does not exist                  */
            if (FALSE === $lead_key) {
                /*  Add Lead to the Database and Google Sheet   */
                echo $lead->id.' - '.$lead->institute_name.'<br/>';
                $this->addNewLead($lead);
            }
            /* Lead already exists in the database  */
            else {
                /* Append to the Google Sheet       */
                if ((NULL != $lead->trial_start_date)
                || (STR_EMPTY != $lead->trial_start_date)) {
                    $trial_date_mp  = date('Y-m-d', strtotime($lead->trial_start_date));
                    
                    /*  Trial date initially filled in MyPhoner */
                    if (STR_EMPTY == $leads_trial_arr[$lead_key]) {
                        $date_changed   = STS_OK;
                    }
                    /*  Trial date is changed in MyPhoner       */
                    else {
                        $trial_date_db  = date('Y-m-d', strtotime($leads_trial_arr[$lead_key]));
                        
                        if ($trial_date_mp != $trial_date_db) {
                            $date_changed   = STS_OK;
                        }
                    }
                    
                    /*  There are changes in the date   */
                    if (STS_OK == $date_changed) {
                        /*  Determine the row in the Sheet          */
                        $sheet_row = $this->getSheetRowByName($lead->institute_name, $lead_count);
                        
                        /*  Update Database and Google Sheet        */
                        $this->updateLeadData($lead, $sheet_row);
                        
                        /*  Create a Schedule in Google Calendar    */
                        $this->addCalendarSchedule($lead);
                        
                        /*  Reset Date Changed status               */
                        $date_changed       = STS_NG;
                        
                        echo '<br/>'.$lead->institute_name.'<br/>';
                    }
                }
            }
        }
    }
    
    //-----------------------------------------------------
    //  Function 56.5: updateLeadData
    //      Function to update the lead data
    //-----------------------------------------------------
    public function updateLeadData($leads_data, $row)
    {
        $googleClient = Google::getClient();
        $googleClient->setScopes(array("https://www.googleapis.com/auth/drive.readonly", "https://www.googleapis.com/auth/spreadsheets.readonly", "https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/spreadsheets"));
        $googleClient->setAccessType("offline");
        
        $sheets = Google::make('sheets');
        
        $range          = 'Sheet1!A'.$row;
        
        if (0 < @count($leads_data)) {
            if (NULL == $leads_data->trial_start_date) {
                $leads_data->trial_start_date   = STR_EMPTY;
            }
            
            $body = new Google_Service_Sheets_ValueRange(array(
              'values' => array(
                    array(
                        $leads_data->institute_name,
                        $leads_data->email_address,
                        $leads_data->primary_contact_dm,
                        $leads_data->contact_no.', '.$leads_data->contact_no_alt_1.', '.$leads_data->contact_no_alt_2,
                        $leads_data->trial_start_date
                    )
                )
            ));
            
            $params = array(
              'valueInputOption' => 'RAW'
            );

            $response   = $sheets->spreadsheets_values->update($this->spreadsheet_id, $range, $body, $params);
            
            $db_upd     = DB::table('myphoner_leads')
                ->where('myphoner_id', $leads_data->id)
                ->update(
                    array(
                        'trial_start_date'      => $leads_data->trial_start_date,
                        'updated_date'          => date('Y-m-d H:i:s'),
                    )
                );
        }
    }
    
    //-----------------------------------------------------
    //  Function 56.6: addNewLead
    //      function to add new lead
    //-----------------------------------------------------
    public function addNewLead($leads_data)
    {
        /*--------------------------------------------------------------------
        /*	Gets all MyPhoner Leads from the Database
        /*------------------------------------------------------------------*/
        $myphoner_data  = DB::table('myphoner_leads')
            ->where('status', ACTIVE)
            ->get();
        
        $total_leads    = @count($myphoner_data) + 1;
        
        $googleClient = Google::getClient();
        $googleClient->setScopes(array("https://www.googleapis.com/auth/drive.readonly", "https://www.googleapis.com/auth/spreadsheets.readonly", "https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/spreadsheets"));
        $googleClient->setAccessType("offline");
        
        $sheets = Google::make('sheets');
        
        $range          = 'Sheet1!A'.$total_leads;
        
        if (0 < @count($leads_data)) {
            if (NULL == $leads_data->trial_start_date) {
                $leads_data->trial_start_date   = STR_EMPTY;
            }
            else {
                /*  Create a Schedule in Google Calendar    */
                $this->addCalendarSchedule($leads_data);
            }
            
            $data   = new Google_Service_Sheets_ValueRange(array(
                'range'     => $range,
                'values'    => array(
                    array(
                        $leads_data->institute_name,
                        $leads_data->email_address,
                        $leads_data->primary_contact_dm,
                        $leads_data->contact_no.', '.$leads_data->contact_no_alt_1.', '.$leads_data->contact_no_alt_2,
                        $leads_data->trial_start_date
                    )
                )
            ));
            
            $write_values = new Google_Service_Sheets_BatchUpdateValuesRequest(array(
                'valueInputOption'  => 'RAW',
                'data'              => $data
            ));
            
            $response       = $sheets->spreadsheets_values->batchUpdate($this->spreadsheet_id, $write_values);
            
            $lead_id        = DB::table('myphoner_leads')
                ->insertGetId(
                    array(
                        'myphoner_id'           => $leads_data->id,
                        'institute_name'        => $leads_data->institute_name,
                        'email_address'         => $leads_data->email_address,
                        'primary_contact_dm'    => $leads_data->primary_contact_dm,
                        'contact_no'            => $leads_data->contact_no,
                        'trial_start_date'      => $leads_data->trial_start_date,
                        'status'                => STS_OK,
                        'created_date'          => date('Y-m-d H:i:s'),
                        'updated_date'          => date('Y-m-d H:i:s'),
                    )
                );
        }
    }
    
    //-----------------------------------------------------
    //  Function 56.7: addCalendarSchedule
    //      function to add a calendar schedule
    //-----------------------------------------------------
    public function addCalendarSchedule($lead)
    {
        $googleClient = Google::getClient();
        $googleClient->setScopes(
            array(
                "https://www.googleapis.com/auth/drive.readonly",
                "https://www.googleapis.com/auth/spreadsheets.readonly",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly"
            )
        );
        
        $googleClient->setAccessType("offline");
        
        $calendar   = Google::make('calendar');
        
        $start_arr      = explode('-', $lead->trial_start_date);
        
        if (2016 <= (int)$start_arr[0]) {
            $start_arr  = explode(' ', $lead->trial_start_date);
            $start_arr  = explode('-', $start_arr[0]);
            
            $start_datetime = $start_arr[0].'-'.$start_arr[1].'-'.$start_arr[2];
            $end_datetime   = $start_arr[0].'-'.$start_arr[1].'-'.$start_arr[2];
        }
        else {
            $start_datetime = $start_arr[2].'-'.$start_arr[1].'-'.$start_arr[0];
            $end_datetime   = $start_arr[2].'-'.$start_arr[1].'-'.$start_arr[0];
        }
        
        $date_check     = DateTime::createFromFormat('Y-m-d', $start_datetime);
        $checkdate      = $date_check && $date_check->format('Y-m-d') === $start_datetime;
        
        if (FALSE == $checkdate) {
            $start_datetime = date('Y-m-d').'T10:00:00';
            $end_datetime   = date('Y-m-d').'T18:00:00';
        }
        else {
            $start_datetime = $start_datetime.'T10:00:00';
            $end_datetime   = $end_datetime.'T10:00:00';
        }
        
        echo '<br/>  Start Date: '.$start_datetime;
        echo '<br/>  End Date: '.$end_datetime;
        
        $event = new Google_Service_Calendar_Event(
            array(
                'summary'       => $lead->institute_name.' Trial',
                'description'   => 'Scheduled Trial of '.$lead->institute_name,
                'start'         => array(
                    'dateTime'      => $start_datetime,
                    'timeZone'      => $this->google_timezone,
                ),
                'end' => array(
                    'dateTime' => $end_datetime,
                    'timeZone' => $this->google_timezone,
                ),
            )
        );
        
        $event = $calendar->events->insert($this->google_email, $event);
    }
    
    //-----------------------------------------------------
    //  Function 56.58: testCalendarEntry
    //      function to test calendar schedule entry
    //-----------------------------------------------------
    public function testCalendarEntry()
    {
        $lead = new stdClass;
        
        $lead->institute_name = 'Test Bank 1';
        $lead->trial_start_date = '14-02-2017';
        
        $this->addCalendarSchedule($lead);
        
        $googleClient = Google::getClient();
        $googleClient->setScopes(
            array(
                "https://www.googleapis.com/auth/drive.readonly",
                "https://www.googleapis.com/auth/spreadsheets.readonly",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly"
            )
        );
        $googleClient->setAccessType("offline");
        
        $calendar = Google::make('calendar');
        
        $calendarId = 'creditbpo.philippines@gmail.com';
        $optParams = array(
          'maxResults' => 10,
          'orderBy' => 'startTime',
          'singleEvents' => TRUE,
          'timeMin' => date('c'),
        );
        $results = $calendar->events->listEvents($calendarId, $optParams);
        
        if (@count($results->getItems()) == 0) {
          print "No upcoming events found.\n";
        }
        else {
          print "Upcoming events:\n";
          foreach ($results->getItems() as $event) {
            $start = $event->start->dateTime;
            if (empty($start)) {
              $start = $event->start->date;
            }
            printf("%s (%s)\n", $event->getSummary(), $start);
          }
        }
    }
    
    //-----------------------------------------------------
    //  Function 56.9: getSheetRowByName
    //      function to get data ordered by name
    //-----------------------------------------------------
    public function getSheetRowByName($name, $max_range)
    {
        $sheet_data     = array();
        $sheet_idx      = 0;
        $sheet_row      = STS_NG;
        
        $googleClient   = Google::getClient();
        $googleClient->setScopes(
            array(
                "https://www.googleapis.com/auth/drive.readonly",
                "https://www.googleapis.com/auth/spreadsheets.readonly",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly"
            )
        );
        
        $googleClient->setAccessType("offline");
        
        $sheets         = Google::make('sheets');
        $range          = 'Sheet1!A1:A'.$max_range;
        
        $result = $sheets->spreadsheets_values->get($this->spreadsheet_id, $range);
        
        foreach ($result->values as $res) {
            $sheet_data[]  = $res[0];
        }
        
        $sheet_idx  = array_search($name, $sheet_data);
        
        /* Lead does not exist                  */
        if (FALSE !== $sheet_idx) {
            $sheet_row  = $sheet_idx + 1;
        }
        
        return $sheet_row;
    }
}