<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

use App\Models\ApiSecret;
use App\Models\DocumentAnalysis;
use App\Models\AnalysisResult;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Facades\Excel;

class FormRecognizerController extends Controller
{
    private $client;
    private $key;
    private $modelId;
    private $baseUrl;
    private $sasToken;
    private $container;
    private $secret;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
        $this->secret = ApiSecret::first();

        $this->key = $this->secret->api_key ?? '';
        $this->modelId = $this->secret->model_id ?? '';
        $this->baseUrl['formRecognizer'] = $this->secret->end_point ?? '';

        $this->container = $this->secret->container_name ?? '';
        $this->baseUrl['storageAccount'] = $this->secret->container_url ?? '';
        $this->sasToken = $this->secret->sas_token ?? '';

    }

    public function index()
    {
        $files =   DocumentAnalysis::latest()->paginate(7);

        return view('form-recognizer.home-page', ['files' => $files, 'secrets' => $this->secret]);
    }

    public function upload(Request $request)
    {
        $rules = ['file' => 'required|mimes:pdf,xlx,csv,jpg,jpeg|max:20480'];

        $customMessages = [
            'file.required' => 'Please choose a file to upload.'
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if ($validator->fails()) {
            return response()->json([
              'success' => false,
              'errors' => $validator->getMessageBag()->toArray()

            ], 422);
        }

        $file = $request->file;
        $fileName = $file->getClientOriginalName();

        $exists = DocumentAnalysis::where(['name' => $fileName])->exists();
        if ($exists) {
            return response()->json([
                'message' => 'The given data was invalid',
                'errors'  => [
                    'file' => [
                        'A file with name ('.$fileName.') has already been uploaded.'
                    ]
                ]
            ],422);
        }

        $assetsUrl = $this->baseUrl['storageAccount'].'/'.$this->container.'/'.$fileName;

        try {
              $response = $this->client->request('PUT', $assetsUrl.'?'.$this->sasToken,[
                'headers' => [
                  'x-ms-blob-type' => 'BlockBlob'
                ],
                'body' => fopen($file, "r"),
                'http_errors' => false,
              ]);

              if ($response->getStatusCode() != 201) {

                  if ($response->getStatusCode() == 403) {
                      $message = 'Please check your shared access signature (SAS), It seems to have expired';
                  } else {
                      $message = 'Something went wrong. Status code : '.$response->getStatusCode();
                  }

                  return response()->json([
                      'errors'  => [
                        'error' => [
                            $message
                        ]
                      ]
                    ],
                    $response->getStatusCode()
                  );
              }

        }
        catch (\Exception $e) {
            return response()->json([
                'errors'  => [
                    'error' => [
                        // $e->getMessage()
                        'Something went wrong. Try again after some time.'
                    ]
                ]
            ],500);
        }

        $assetsUrl = str_replace(" ","%20",$assetsUrl);
        $endpoint = $this->baseUrl['formRecognizer'].'/formrecognizer/v2.1/custom/models/'.$this->modelId.'/analyze';

        try {
            $response = $this->client->request('POST', $endpoint,[
              'headers' => [
                  'Content-Type'              => 'application/json',
                  'Ocp-Apim-Subscription-Key' => $this->key
              ],
              'json' => [
                  'source' => $assetsUrl
              ]
            ]);

            if ($response->getStatusCode() == 202) {

                $resultUrl = $response->getHeader('Operation-Location')[0];
                $body = $response->getBody();
                $contents = json_decode($body);

                DocumentAnalysis::create([
                  'name' => $fileName,
                  'status' => 0,
                  'result_url' => $resultUrl,
                  'container_url' => $assetsUrl,
                ]);
            }

            return response()->json(['success' => true, 'message' => 'Analyze operation is in process'], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors'  => [
                    'error' => [
                        $e->getMessage()
                    ]
                ]
            ],500);
        }

          return response()->json(['message'=>'The given data was invalid', 'errors'=> 'Something went wrong'], 422);
    }

    public function results(DocumentAnalysis $documentAnalysis)
    {
        $url = $documentAnalysis->result_url;

        try {
            $response = $this->client->request('GET', $url,[
                  'headers' => [
                      'Ocp-Apim-Subscription-Key' => $this->key
                  ],
                  'http_errors' => false,
            ]);

            if ($response->getStatusCode() == 200) {
                $body = $response->getBody();
                $contents = json_decode($body);

                if($contents->status == 'succeeded'){
                    $analyzeResult = $contents->analyzeResult;

                    $documentAnalysis->update([
                      'status' => 1
                    ]);

                    AnalysisResult::create([
                      'document_id' => $documentAnalysis->id,
                      'response' => json_encode($analyzeResult->documentResults),
                    ]);

                    return redirect()->route('form.home');
                }

                return redirect()->back()->with('messages', ['Results are not ready, please try after some time']);
            } elseif ($response->getStatusCode() == 404) {
                return redirect()->back()->withErrors(['messages' => 'Analyze operation results not found, Invalid or expired result identifier.']);
            }

        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['messages' => $e->getMessage()]);
        }

    }

    public function getFile(DocumentAnalysis $documentAnalysis)
    {
      $url = $documentAnalysis->container_url;
      $endpoint = $url.'?'.$this->sasToken;

      try {

        $response = $this->client->request('GET', $endpoint,[
            'headers' => [
                'x-ms-blob-type' => 'BlockBlob'
            ],
            'http_errors' => false,
        ]);

        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();

            $path = Storage::path('public');
            $file = $path.'/'.$documentAnalysis->name;
            file_put_contents($file, $body);

            if (file_exists($file)) {
                return response()->download($file)->deleteFileAfterSend(true);
            }
        }

        if ($response->getStatusCode() == 403) {
            return Redirect::back()->withErrors('Please check your shared access signature (SAS), It seems to have expired');
        } else {
            return Redirect::back()->withErrors('Something went wrong. Status code : '.$response->getStatusCode());
        }

      } catch (\Exception $e) {
          return Redirect::back()->withErrors('Something went wrong.');
      }
    }

    public function getReport(DocumentAnalysis $documentAnalysis)
    {
        $analysisResult = json_decode($documentAnalysis->AnalysisResult->response,  true);

        $fields = [];
        foreach ($analysisResult[0]['fields'] as $key => $value) {
            if ($value['type'] == 'string') {
                $fields[$key] = isset($value['text']) ? $value['text'] : '';
            } elseif($value['type'] == 'object' && isset($value['valueObject'])) {
                $fields[$key] =  str_replace(',', '', $value['valueObject']);
                foreach ($fields[$key] as  $ke => $val) {
                    $fields[$key][$ke] =  str_replace(',', '', $val['valueObject']);
                    foreach ($fields[$key][$ke] as $k => $v) {
                      if ($fields[$key][$ke][$k]  && isset($fields[$key][$ke][$k]['valueString'])) {
                        $fields[$key][$ke][$k] = str_replace(',', '', $fields[$key][$ke][$k]['valueString']);
                      } else {
                        $fields[$key][$ke][$k] = 0;
                      }
                    }
                }
            } else {
                $fields[$key] = null;
            }
        }

        $companyName = $fields['Company Name'];

        $file = Excel::load(public_path('fsTemplate_azure.xls'));

        $sheet = $file->setActiveSheetIndexByName('Data Entry');

        $sheet->setCellValue('B2', $companyName);
        $sheet->setCellValue('B6', 'Year 1');
        $sheet->setCellValue('C6', 'Year 2');

        $year = 1;

        while ($year <= 2) {
          foreach ($fields as $fieldKey => $fieldValue) {
            switch (strtolower($fieldKey)) {
              // BALANCE SHEET
              case 'balance sheet':
              if ($fieldValue) {
                foreach ($fieldValue as $dataKey => $data) {
                  switch (strtolower($dataKey)) {

                    case 'cash and cash equivalents':
                    $entity = array_sum([
                      $data['Year '.$year.'A'],
                      $data['Year '.$year.'B'],
                      $data['Year '.$year.'C'],
                      $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 10, (string)$entity);
                    break;

                    case 'trade and other current receivables':
                    $entity = array_sum([
                      $data['Year '.$year.'A'],
                      $data['Year '.$year.'B'],
                      $data['Year '.$year.'C'],
                      $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 11, (string)$entity);
                    break;

                    case 'current inventories':
                    $entity = array_sum([
                      $data['Year '.$year.'A'],
                      $data['Year '.$year.'B'],
                      $data['Year '.$year.'C'],
                      $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 12, (string)$entity);
                    break;

                    case 'current tax assets':
                    $entity = array_sum([
                      $data['Year '.$year.'A'],
                      $data['Year '.$year.'B'],
                      $data['Year '.$year.'C'],
                      $data['Year '.$year.'D']
                    ]);

                    $sheet->setCellValueByColumnAndRow($year, 13, (string)$entity);
                    break;

                    case 'current biological assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 14, (string)$entity);
                    break;

                    case 'other current financial assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 15, (string)$entity);
                    break;

                    case 'other current non-financial assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 16, (string)$entity);
                    break;

                    case 'current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 17, (string)$entity);
                    break;

                    case 'non-current assets or disposal groups classified as held for sale or as held for distribution to owners':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 18, (string)$entity);
                    break;

                    // NON-CURRENT ASSETS
                    case 'property, plant and equipment':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 22, (string)$entity);
                    break;

                    case 'investment property':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 23, (string)$entity);
                    break;

                    case 'goodwill':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 24, (string)$entity);
                    break;

                    case 'intangible assets other than goodwill':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 25, (string)$entity);
                    break;

                    case 'investment accounted for using equity method':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 26, (string)$entity);
                    break;

                    case 'investments in subsidiaries, joint ventures and associates':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 27, (string)$entity);
                    break;

                    case 'non-current biological assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 28, (string)$entity);
                    break;

                    case 'trade and other non-current receivables':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 29, (string)$entity);
                    break;

                    case 'non-current inventories':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 30, (string)$entity);
                    break;

                    case 'deferred tax assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 31, (string)$entity);
                    break;

                    case 'current tax assets, non-current':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 32, (string)$entity);
                    break;

                    case 'other non-current financial assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 33, (string)$entity);
                    break;

                    case 'other non-current non-financial assets':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 34, (string)$entity);
                    break;

                    case 'non-current non-cash assets pledged as collateral for which transferee has right by contract or custom to sell or repledge collateral':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 35, (string)$entity);
                    break;

                    //CURRENT LIABILITIES
                    case 'current provisions for employee benefits':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 43, (string)$entity);
                    break;

                    case 'other current provisions':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 44, (string)$entity);
                    break;

                    case 'trade and other current payables':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 45, (string)$entity);
                    break;

                    case 'current tax liabilities, current':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);

                    $sheet->setCellValueByColumnAndRow($year, 46, (string)$entity);
                    break;

                    case 'other current financial liabilities':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);

                    $sheet->setCellValueByColumnAndRow($year, 47, (string)$entity);
                    break;

                    case 'other current non-financial liabilities':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);

                    $sheet->setCellValueByColumnAndRow($year, 48, (string)$entity);
                    break;

                    case 'liabilities included in disposal groups classified as held for sale':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 49, (string)$entity);
                    break;

                    // // NON-CURRENT LIABILITIES
                    case 'non-current provisions for employee benefits':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 53, (string)$entity);
                    break;

                    case 'other non-current provisions':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 54, (string)$entity);
                    break;

                    case 'trade and other non-current payables':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 55, (string)$entity);
                    break;

                    case 'deferred tax liabilities':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 56, (string)$entity);
                    break;

                    case 'current tax liabilities, non-current':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 57, (string)$entity);
                    break;

                    case 'other long-term financial liabilities':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 58, (string)$entity);
                    break;

                    case 'other non-current non-financial liabilities':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 59, (string)$entity);
                    break;

                    // EQUITY
                    case 'issued or share capital':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 63, (string)$entity);
                    break;

                    case 'share premium':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 64, (string)$entity);
                    break;

                    case 'treasury shares':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 65, (string)$entity);
                    break;

                    case 'other equity interest':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 66, (string)$entity);
                    break;

                    case 'other reserves':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 67, (string)$entity);
                    break;

                    case 'retained earnings':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 68, (string)$entity);
                    break;

                    case 'non-controlling interests':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($year, 69, (string)$entity);
                    break;

                    default:
                    // code...
                    break;
                  }
                }
                break;
              }

              // INCOME STATEMENT
              case 'income statement':

              $yearColumnIS = $year+6;
              if ($fieldValue) {
                foreach ($fieldValue as $dataKey => $data) {
                  switch (strtolower($dataKey)) {

                    case 'revenue':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 8, (string)$entity);
                    break;

                    case 'cost of sales':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 9, (string)$entity);
                    break;

                    case 'other income':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 12, (string)$entity);
                    break;

                    case 'distribution costs':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 13, (string)$entity);
                    break;

                    case 'administrative expense':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 14, (string)$entity);
                    break;

                    case 'other expense':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 15, (string)$entity);
                    break;

                    case 'other gains or losses':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 16, (string)$entity);
                    break;

                    case 'difference between carrying amount of dividends payable and carrying amount of non-cash assets distributed 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 19, (string)$entity);
                    break;

                    case 'gains or losses on net monetary position 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 20, (string)$entity);
                    break;

                    case 'gain or loss arising from derecognition of financial assets measured at amortised cost 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 21, (string)$entity);
                    break;

                    case 'finance income':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 22, (string)$entity);
                    break;

                    case 'finance costs':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 23, (string)$entity);
                    break;

                    case 'impairment loss or impairment gain and reversal of impairment loss determined in accordance with ifrs 9 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 24, (string)$entity);
                    break;

                    case 'share of profit or loss of associates in joint ventures accounted for using equity method 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 25, (string)$entity);
                    break;

                    case 'other income or expense from subsidiaries, jointly controlled entities and associates 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 26, (string)$entity);
                    break;

                    case 'gains or losses arising form difference between previous carrying amount and fair value of financial assets reclassified as measured at fair value1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 27, (string)$entity);
                    break;

                    case 'cumulative gain or loss previously recognised in other comprehensive income arising from reclassification of financial assets out of fair value through other comprehensive income into fair value through profit or loss measurement category. 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 28, (string)$entity);
                    break;

                    case 'hedging gains or losses for hedge of group of items with offsetting risk positions 1':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 29, (string)$entity);
                    break;

                    case 'income tax expense from continuing operations':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 32, (string)$entity);
                    break;

                    case 'profit or loss from discontinued operations':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 35, (string)$entity);
                    break;

                    case 'other comprehensive income':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnIS, 38, (string)$entity);
                    break;

                    default:
                    // code...
                    break;
                    $yearColumnIS++;
                  }
                }
              }

              // STATEMENTS OF CASH FLOW
              case 'statements of cash flow';
              if ($fieldValue) {
                $yearColumnSOC = $year+6;
                foreach ($fieldValue as $dataKey => $data) {
                  switch (strtolower($dataKey)) {

                    case 'amortization expense':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 43, (string)$entity);
                    break;

                    case 'depreciation expense':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 44, (string)$entity);
                    break;

                    case 'net cashflow from operations per fs':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 45, (string)$entity);
                    break;

                    case 'interest expense':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 48, (string)$entity);
                    break;

                    case 'non-cash expenses':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 49, (string)$entity);
                    break;

                    case 'principal payments':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 50, (string)$entity);
                    break;

                    case 'interest payments':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 51, (string)$entity);
                    break;

                    case 'lease payments':
                    $entity = array_sum([
                    $data['Year '.$year.'A'],
                    $data['Year '.$year.'B'],
                    $data['Year '.$year.'C'],
                    $data['Year '.$year.'D']
                    ]);
                    $sheet->setCellValueByColumnAndRow($yearColumnSOC, 52, (string)$entity);
                    break;

                    default:
                    // code...
                    break;
                  }
                }
                $yearColumnSOC ++;
              }

              break;
              default:
              // $sheet->setCellValueByColumnAndRow(1, 78, 'null');
              break;
            }
          }

          $year++;
        }

        $file->export('xls');

    }

    public function resources()
    {
        return view('form-recognizer.resource-management', ['secret' => $this->secret]);
    }

    public function updateResources(Request $request)
    {
        $request->validate([
            'apiKey'        => 'required',
            'endPoint'      => 'required',
            'modelId'       => 'required',
            'containerName' => 'required',
            'containerUrl'  => 'required',
            'sasToken'      => 'required',
        ]);

        $data = [
            'api_key'        => $request->apiKey,
            'end_point'      => $request->endPoint,
            'model_id'       => $request->modelId,
            'container_name' => $request->containerName,
            'container_url'  => $request->containerUrl,
            'sas_token'      => $request->sasToken,
        ];

        ApiSecret::updateOrCreate(
          ['id' => 1],
          $data
        );

        return Redirect::back()->with('messages', ['Content has been updated successfully!']);
    }

}
