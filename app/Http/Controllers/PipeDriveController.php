<?php

//======================================================================
//  Class 58: PipeDriveController
//      
//======================================================================
class PipeDriveController extends BaseController {
    private $pipedrive;

    //-----------------------------------------------------
    //  Function 58.1: __construct
    //      class constructor
    //-----------------------------------------------------
    public function __construct() {
        $this->pipedrive = new Pipedrive();
    }

    //-----------------------------------------------------
    //  Function 58.2: addPerson
    //      function to add a person
    //-----------------------------------------------------
    public function addPerson() {
        $data = Input::all();

        if (!isset($data['name']) && !isset($data['email']) && !isset($data['phone'])) {
            return;
        }

        if ($this->pipedrive->addPerson($data['name'], $data['email'], $data['phone'])) {
            return json_encode([
                'status' => 'success'
            ]);
        }

        return json_encode([
            'status' => 'false'
        ]);
    }
}
