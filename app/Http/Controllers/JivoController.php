<?php
use \Exception as Exception;
//======================================================================
//  Class 29: Jivo Controller
//      Functions related to JivoChat Web hooks
//      are routed to this class
//======================================================================
class JivoController extends BaseController
{
    //-----------------------------------------------------
    //  Function 29.1: addJivoLeads
    //      Saves JivoChat Leads to the database
    //-----------------------------------------------------
	public function addJivoLeads()
    {
        /*  Variable Declaration    */
        $lead['email']  = Input::get('email');  // Customer Email from JivoChat
        $lead['name']   = Input::get('name');   // Customer Name from JivoChat
        $lead['phone']  = Input::get('phone');  // Customer Phone from JivoChat
        
        /*  When customer inputs an email and name  */
        if ((isset($lead_email['email'])) && (isset($lead['name']))) {
            
            /*  Customer inputted a phone number    */
            if (!isset($lead['phone'])) {
                $lead['phone'] = STS_NG;
            }
            
            /*  Checks if the email already exists in the database  */
            $email_exist = EmailLead::where('email', $lead['email'])->first();
            
            /*  Only save information if email does not exists
            *   this is to prevent multiple copies of emails if customers
            *   uses JivoChat several times
            */
            if (STS_NG == @count($email_exist)) {
                EmailLead::create(array(
                    'email'             => $lead['email'],
                    'first_name'        => $lead['name'],
                    'contact_number'    => $lead['phone'],
                    'submitted_date'    => date('Y-m-d'),
                    'last_id'           => 0,
                    'is_new'            => 1,
                    'activity_id'       => 3,
                ));
            }
        }
    }
	
    //-----------------------------------------------------
    //  Function 29.2: postJivoOfflineMessage
    //      JivoChat webhook for offline messages
    //-----------------------------------------------------
	public function postJivoOfflineMessage()
	{
        /*  Variable Declaration    */
		$event_name = Input::get('event_name'); // Event Name from JivoChat
		
        /*  The event is an Offline message */
		if ($event_name == 'offline_message') {
			
            /*  Gets the information from JivoChat  */
			$visitor    = Input::get('visitor');    // Customer Name
			$text       = Input::get('message');    // Customer Message
			
			/*  Send visitor message to info@creditbpo.com  */
			IF (env("APP_ENV") == "Production"){

				try{
					Mail::send('emails.offline_message', 
						array(
							'name'  => $visitor['name'],
							'email' => $visitor['email'],
							'phone' => $visitor['phone'],
							'text'  => $text
						),
						function($message) {
							$message->to('info@creditbpo.com', 'info@creditbpo.com')
								->subject('CreditBPO Offline Message');
						}
					);
				}catch(Exception $e){
	            //
	            }

			}
			
            /*  Add information to MailChimp    */
            /*  Configure required MailChimp details    */
			$apiKey     = 'a1c34fd63ccd510afd7d915acb882816-us11';
			$listId     = 'cf26c07ae2';
			
            $memberId   = md5(strtolower($visitor['email']));
			$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            
			$url        = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
			
            /*  Extracts the Firstname of the Visitor   */
			$arname = explode(' ', $visitor['name']);
			$fname  = isset($arname[0]) ? $arname[0] : 'unknown';
            
            /*  Extracts the Lastname of the Visitor    */
			if (@count($arname) > 1) {
				unset($arname[0]);
				$lname = implode(' ', $arname);
			}
            else {
				$lname = 'unknown';
			}
			
            /*  Configure the POST FIELDS to be pass to MailChimp API   */
			$json = json_encode([
				'email_address' => $visitor['email'],
				'status'        => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
				'merge_fields'  => [
					'MMERGE3'   => $visitor['phone'],
					'FNAME'     => $fname,
					'LNAME'     => $lname,
				]
			]);
            
            /*  Initialize CURL */
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

			$result     = curl_exec($ch);
			$httpCode   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
            /*  Close CURL Connection   */
            curl_close($ch);
			
			/*  Logs the activity to Jivo log   */
			$file = "temp/jivo.log";
			$msg = date('c') . '=>' . $visitor['email'] . "\r\n"; 
			$msg .= $url . "\r\n"; 
			$msg .= $result . "\r\n"; 
			$msg .= $httpCode . "\r\n"; 
			file_put_contents($file, $msg, FILE_APPEND | LOCK_EX);
		}
		
		return Response::json(array('result' => 'ok'));
	}
	
}