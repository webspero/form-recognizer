<?php
use \Exception as Exception;
//======================================================================
//  Class 54: User Feedback Controller
//      Functions related to User Feedback on the Questionnaire
//      are routed to this class
//======================================================================
class UserFeedbackController extends BaseController
{
    //-----------------------------------------------------
    //  Function 54.1: getFeedbackDashboard
    //      Display all Feedback List
    //-----------------------------------------------------
    public function getFeedbackDashboard()
	{
        /*  Variable Declaration    */
		$limit      = 15;

        /** Instantiate Classes */
        $user_db    = new User;

        /*  Acquire all SME Users from Database */
        $user_data  = $user_db->paginateUserCompanyByRole(USER_ROLE_SME, $limit);

        /*  Display HTML                        */
		return View::make('admin.user-feedback', array(
			'users' => $user_data
		));
	}

    //-----------------------------------------------------
    //  Function 54.2: getUserFeedback
    //      Display a User's Feedback Information
    //-----------------------------------------------------
    public function getUserFeedback($user_id)
    {
        /*  Variable Declaration    */
        $feedback_count     = 0;

        /** Instantiate Classes */
        $user_db            = new User;
        $tab_feedback_db    = new FeedbackTabRating;
        $final_feedback_db  = new FeedbackFinalRating;

        /*  Get SME Information */
        $user_data      = $user_db->getUserCompanyByRole($user_id);

        /*  Get specififed SME Tab Feedbacks    */
        $tab_feedbacks  = $tab_feedback_db->getUserFeedback($user_data['entityid']);
        $tab_count      = @count($tab_feedbacks);

        /*  Get specififed SME Final Feedback   */
        $final_feedback = $final_feedback_db->getFinalUserFeedback($user_data['entityid']);
        $final_count    = @count($final_feedback);

        /** Total SME Feedbacks */
        $feedback_count = $tab_count + $final_count;

        /** Set the Language File and Tab Labels    */
        foreach ($tab_feedbacks as $feedback) {
            switch ($feedback['tab_id']) {
                case FEEDBACK_BIZ_DETAILS_1:
                    $feedback['tab_label'] = trans('business_details1.business_details');
                    $feedback['lang_file'] = 'business_details1';
                    break;

                case FEEDBACK_BIZ_DETAILS_2:
                    $feedback['tab_label'] = trans('business_details2.business_details2');
                    $feedback['lang_file'] = 'business_details2';
                    break;

                case FEEDBACK_OWNER_INFO:
                    $feedback['tab_label'] = trans('owner_details.biz_owner_details');
                    $feedback['lang_file'] = 'owner_details';
                    break;

                case FEEDBACK_ECF:
                    $feedback['tab_label'] = trans('ecf.ecf');
                    $feedback['lang_file'] = 'ecf';
                    break;

                case FEEDBACK_COND_SUSTAIN:
                    $feedback['tab_label'] = trans('condition_sustainability.condition_sustainability');
                    $feedback['lang_file'] = 'condition_sustainability';
                    break;

                case FEEDBACK_RCL:
                    $feedback['tab_label'] = trans('rcl.rcl');
                    $feedback['lang_file'] = 'rcl';
                    break;

                default:
                    break;
            }
        }

        /** Display HTML    */
        return View::make('admin.user-feedback-info', array(
			'user'              => $user_data,
            'feedback_count'    => $feedback_count,
            'final_count'       => $final_count,
            'tab_feedbacks'     => $tab_feedbacks,
            'final_feedback'    => $final_feedback
		));
    }

    //-----------------------------------------------------
    //  Function 54.3: showTabUserFeedback
    //      Show Tab Feedback Form
    //-----------------------------------------------------
    public function showTabUserFeedback($tab_type)
    {
        /*  Most Difficult question table   */
        $tab_questions   = array(
            FEEDBACK_BIZ_DETAILS_1  => array('business_details', 'related_company', 'products_services_offered', 'capital_details', 'main_locations', 'branches', 'management', 'certifications', 'import_export', 'insurance_coverage', 'cash_conversion_cyle', 'document_upload'),
            FEEDBACK_BIZ_DETAILS_2  => array('major_customer', 'major_supplier', 'major_market_location', 'competitors', 'bdriver', 'customer_segment', 'past_proj_completed', 'future_growth_initiatives'),
            FEEDBACK_OWNER_INFO     => array('biz_owner_details', 'educational_background', 'spouse_details', 'spouse_educ_bg'),
            FEEDBACK_ECF            => array('credit_facilities_title', 'existing_collateral'),
            FEEDBACK_COND_SUSTAIN   => array('succession', 'competitive_landscape', 'economic_factor', 'tax_payments', 'risk_assessment', 'revenue_growth', 'capital_expansion'),
            FEEDBACK_RCL            => array('requested_cf_title', 'offered_collateral')
        );

        /*  Language File table     */
        $tab_lang_file  = array('', 'business_details1', 'business_details2', 'owner_details', 'ecf', 'condition_sustainability', 'rcl');

        /*  Display HTML        */
        return View::make('user-feedback/tab-rating', array(
                'tab_id'        => $tab_type,
                'tab_questions' => $tab_questions,
                'lang_file'     => $tab_lang_file
            )
        );
    }

    //-----------------------------------------------------
    //  Function 54.4: getFeedbackSummary
    //      Display Summary of Feedbacks
    //-----------------------------------------------------
    public function getFeedbackSummary()
    {
        /*  Variable Declaration    */
        $prev_tab           = 0;
        $rating_count       = 0;
        $cur_feedback_count = 0;

        $difficulty_question    = array();
        $rating_score           = array();

        /** Instantiate Classes */
        $tab_feedback_db    = new FeedbackTabRating;
        $final_feedback_db  = new FeedbackFinalRating;

        /*  Most Difficult question table   */
        $tab_questions   = array(
            FEEDBACK_BIZ_DETAILS_1  => array('business_details', 'related_company', 'products_services_offered', 'capital_details', 'main_locations', 'branches', 'management', 'certifications', 'import_export', 'insurance_coverage', 'cash_conversion_cyle', 'document_upload'),
            FEEDBACK_BIZ_DETAILS_2  => array('major_customer', 'major_supplier', 'major_market_location', 'competitors', 'bdriver', 'customer_segment', 'past_proj_completed', 'future_growth_initiatives'),
            FEEDBACK_OWNER_INFO     => array('biz_owner_details', 'educational_background', 'spouse_details', 'spouse_educ_bg'),
            FEEDBACK_ECF            => array('credit_facilities_title', 'existing_collateral'),
            FEEDBACK_COND_SUSTAIN   => array('succession', 'competitive_landscape', 'economic_factor', 'tax_payments', 'risk_assessment', 'revenue_growth', 'capital_expansion'),
            FEEDBACK_RCL            => array('requested_cf_title', 'offered_collateral')
        );

        /*  Tab Labels table                */
        $tab_labels     = array(
            STR_EMPTY,
            'business_details',
            'business_details2',
            'biz_owner_details',
            'ecf',
            'condition_sustainability',
            'rcl'
        );

        /*  Language File table             */
        $tab_lang_file  = array(
            STR_EMPTY,
            'business_details1',
            'business_details2',
            'owner_details',
            'ecf',
            'condition_sustainability',
            'rcl'
        );

        /*  Acquires Feedbacks from the database sorted by Tab ID   */
        $feedback_answered  = $tab_feedback_db->getUserFeedbackByIgnorSts(FEEDBACK_AGREED);
        $feedback_ignored   = $tab_feedback_db->getUserFeedbackByIgnorSts(FEEDBACK_DECLINE);
        $final_answered     = $final_feedback_db->getAllAnsweredFinalFeedback();

        /*  Counts number of feedbacks      */
        $feedback_count['answered']     = @count($feedback_answered);
        $feedback_count['ignored']      = @count($feedback_ignored);
        $feedback_count['final']        = @count($final_answered);

        /*  Process tab feedbacks           */
        if (0 < $feedback_count['answered']) {
            /*  Sets the Initial tab requirements   */
            $first_tab                  = $feedback_answered[0]['tab_id'];
            $rating_score[$first_tab]   = 0;
            $prev_tab_arr               = $first_tab;
            $prev_tab_db                = $first_tab;

            foreach($feedback_answered as $feedback) {

                /** Compute average flag        */
                $compute_average = STS_NG;

                /** Total feedback processed    */
                $cur_feedback_count++;

                /** Current Tab being processed */
                $current_tab    = $feedback['tab_id'];

                /*  Initialize Most difficult question votes    */
                if (!isset($difficulty_question[$feedback['user_difficulty']])) {
                    $difficulty_question[$feedback['user_difficulty']] = 1;
                }
                /*  Adds a vote to the Most difficult question  */
                else {
                    $difficulty_question[$feedback['user_difficulty']] += 1;
                }

                /*  Previous Tab Processed from DB is the same with Current Tab */
                if ($prev_tab_db == $feedback['tab_id']) {
                    /** Adds given rating                   */
                    $rating_score[$current_tab] += $feedback['user_rating'];

                    /** Increments number of processed feedback to the tab        */
                    $rating_count++;

                    /** Final record has been reached       */
                    if ($cur_feedback_count == $feedback_count['answered']) {
                        $compute_average = STS_OK;
                    }
                }
                /*  Tab ID has been changed with the current Feedback being processed   */
                else {
                    $compute_average = STS_OK;
                }

                /*  Tab ID has changed or Final record has been reached */
                if (STS_OK == $compute_average) {
                    /*  Gets the Average of the Previous Tab    */
                    $rating_score[$prev_tab_arr]    = $rating_score[$prev_tab_arr] / $rating_count;
                    $rating_score[$prev_tab_arr]    = round($rating_score[$prev_tab_arr], 2);

                    /*  Initialize tab requirement from Current Tab */
                    $rating_score[$current_tab]     = $feedback['user_rating'];
                    $rating_count = 1;

                    /*  Stores the Current Tab for comparison when changing TAB ID  */
                    $prev_tab_arr       = $current_tab;
                }

                /*  Stores the Current Tab from DB  */
                $prev_tab_db   = $current_tab;
            }
        }

        /*  Initialize Final Feedback Data  */
        $final_feedbacks['ease_of_experience']  = 0;
        $final_feedbacks['report_satisfaction'] = 0;
        $final_feedbacks['our_understanding']   = 0;
        $final_feedbacks['will_recommend']      = 0;

        if (0 < $feedback_count['final']) {
            /*  Process Final Feedback  */
            foreach ($final_answered as $feedback) {
                $final_feedbacks['ease_of_experience']  += $feedback['ease_of_experience'];
                $final_feedbacks['report_satisfaction'] += $feedback['report_satisfaction'];
                $final_feedbacks['our_understanding']   += $feedback['our_understanding'];
                $final_feedbacks['will_recommend']      += $feedback['will_recommend'];
            }

            $final_feedbacks['ease_of_experience']  = $final_feedbacks['ease_of_experience'] / $feedback_count['final'];
            $final_feedbacks['report_satisfaction'] = $final_feedbacks['report_satisfaction'] / $feedback_count['final'];
            $final_feedbacks['our_understanding']   = $final_feedbacks['our_understanding'] / $feedback_count['final'];
            $final_feedbacks['will_recommend']      = $final_feedbacks['will_recommend'] / $feedback_count['final'];

            $final_feedbacks['ease_of_experience']  = round($final_feedbacks['ease_of_experience'], 2);
            $final_feedbacks['report_satisfaction'] = round($final_feedbacks['report_satisfaction'], 2);
            $final_feedbacks['our_understanding']   = round($final_feedbacks['our_understanding'], 2);
            $final_feedbacks['will_recommend']      = round($final_feedbacks['will_recommend'], 2);
        }

        /*  Display HTML    */
        return View::make('admin/feedback-summary', array(
                'tab_questions' => $tab_questions,
                'tab_labels'    => $tab_labels,
                'lang_file'     => $tab_lang_file,

                'feedback_answered'     => $feedback_answered,
                'feedback_count'        => $feedback_count,
                'rating_score'          => $rating_score,
                'difficulty_question'   => $difficulty_question,

                'final_feedbacks'       => $final_feedbacks
            )
        );
    }

    //-----------------------------------------------------
    //  Function 54.5: addTabUserFeedback
    //      HTTP Post request to validate and save User
    //      Feedback on a Tab data in the database
    //-----------------------------------------------------
    public function addTabUserFeedback()
    {
		/*  Variable Declaration    */
		$validator				= CONT_NULL;
		$srv_resp['messages']	= CONT_NULL;
		$srv_resp['valid']		= FAILED;

		/*	Form Inputs			*/
		$data['transaction_id']         = Input::get('trans_id');
		$data['tab_id']                 = Input::get('tab_type');

		$data['user_difficulty']        = Input::get('hard_query');
		$data['user_recommendation']    = Input::get('recommend');
		$data['user_rating']            = Input::get('rating');
        $data['ignored']                = FEEDBACK_AGREED;

        /*  Instantiate Classes     */
		$feedback_db           = new FeedbackTabRating;

        /*  Input Validation        */
		$validator	= Validator::make(
			/*	Data to be validated										*/
			$data,

			/*	Validation Rules											*/
			array(
				'user_difficulty'		=> 'required',
				'user_recommendation'   => 'required',
				'user_rating'		    => 'required',
			),

            array(
                'user_difficulty.required'      => 'Most difficult question is required',
                'user_recommendation.required'  => 'What would make it easier for you is required',
                'user_rating.required'          => 'Overall Tab Rating is required',
            )
		);

        /*  There are invalid inputs    */
		if ($validator->fails()) {
			/*	Gets the messages from the validation						*/
			$messages 				= $validator->messages();
			$srv_resp['messages']	= $messages->all();
		}
        /*  All inputs are valid        */
		else {
			/*	Create Notification Code						*/
            $feedback_db->addTabFeedbackRating($data);

			$srv_resp['messages']	= STS_OK;
			$srv_resp['valid']		= SUCCESS;
		}

		return json_encode($srv_resp);
    }

    //-----------------------------------------------------
    //  Function 54.6: ignoreTabUserFeedback
    //      HTTP Post request to save ignored status on
    //      a User Feedback of a Tab
    //-----------------------------------------------------
    public function ignoreTabUserFeedback()
    {
        /*  Variable Declaration    */
		$validator				= CONT_NULL;
		$srv_resp['messages']	= CONT_NULL;
		$srv_resp['valid']		= FAILED;

		/*	Form Inputs			*/
		$data['transaction_id']         = Input::get('trans_id');
		$data['tab_id']                 = Input::get('tab_type');

		$data['user_difficulty']        = STR_EMPTY;
		$data['user_recommendation']    = STR_EMPTY;
		$data['user_rating']            = 0;
        $data['ignored']                = FEEDBACK_DECLINE;

        /*  Instantiate Classes     */
		$feedback_db           = new FeedbackTabRating;

        /*	Add feedback to tab as ignored			*/
        $feedback_db->addTabFeedbackRating($data);

        $srv_resp['messages']	= STS_OK;
        $srv_resp['valid']		= SUCCESS;

		return json_encode($srv_resp);
    }

    //-----------------------------------------------------
    //  Function 54.7: showFinalUserFeedback
    //      Display the Final Feedback Form to the User
    //-----------------------------------------------------
    public function showFinalUserFeedback($trans_id)
    {
        /*  User is Logged in   */
        if (Auth::check()) {
            /*  Instantiate Classes */
            $feedback_db    = new FeedbackFinalRating;

            /*  Variable Declaration    */
            $feedback_count = 0;

            /*  Check if a Feedback has already been answered in the database   */
            $feedback_data  = $feedback_db->checkFinalFeedbackRating($trans_id);
            $user_data      = DB::table('tblentity')->where('entityid', $trans_id)->first();
            $feedback_count = @count($feedback_data);

            /*  Report belongs to the User Logged in    */
            if (Auth::user()->loginid == $user_data->loginid) {
                return View::make('user-feedback.final-rating', array('entity_id' => $trans_id, 'feedback_count' => $feedback_count));
            }
            else {
                return Redirect::to('login');
            }
        }
        else {
            return Redirect::to('login');
        }
    }

    //-----------------------------------------------------
    //  Function 54.8: putSMEOverallFeedback
    //      HTTP Post request to validate and save Final
    //      Feedback data in the database
    //-----------------------------------------------------
    public function putSMEOverallFeedback($trans_id)
	{
        /*  Variable Declaration    */
        $feedback_db    = new FeedbackFinalRating;

        /*  Formats the Error Messages  */
		$messages = array(
			'ease-experience.required' => trans('validation.required', array('attribute'=>'How easy was it to engage our service')),
			'report-satisfaction.required' => trans('validation.required', array('attribute'=>'How happy are you with the report')),
		    'our-understanding.required' => trans('validation.required', array('attribute'=>'How well did we understand what you want')),
		    'will-recommend.required' => trans('validation.required', array('attribute'=>'How likely is it that you would recommend us to a peer or colleague')),
		);

        /*  Validation Inputs           */
		$rules = array(
			'ease-experience'	=> 'required',
			'report-satisfaction'	=> 'required',
	        'our-understanding'	=> 'required',
	        'will-recommend'	=> 'required',
	    );

        $validator = Validator::make(Input::all(), $rules, $messages);

        /*  Some inputs are invalid     */
		if ($validator->fails()) {
			return Redirect::to('/feedback/final_rating/'.$trans_id)->withErrors($validator)->withInput();
		}
        /*  All inputs are valid        */
		else
		{
            $data['transaction_id']         = $trans_id;
            $data['ease_of_experience']     = Input::get('ease-experience');
            $data['report_satisfaction']    = Input::get('report-satisfaction');
            $data['our_understanding']      = Input::get('our-understanding');
            $data['will_recommend']         = Input::get('will-recommend');
            $data['status']                 = FINAL_FEEDBACK_DONE;

            /** Update the database             */
            $rating_result  = $feedback_db->updateFinalFeedbackRating($data);

            /** Redirect to the next page       */
            if ($rating_result) {
                return Redirect::to('/dashboard/index');
            }
            else {
                return Redirect::to('/dashboard/index');
            }
        }
    }

    //-----------------------------------------------------
    //  Function 54.9: putSMEOverallFeedback
    //      Emails the link to the Feedback form
    //-----------------------------------------------------
    public function sendFinalUserFeedback($trans_id)
    {
        /*  Instantiate Classes     */
        $feedback_db    = new FeedbackFinalRating;

        /*  Variable Declaration    */
        $data['transaction_id']         = $trans_id;
        $data['ease_of_experience']     = 0;
        $data['report_satisfaction']    = 0;
        $data['our_understanding']      = 0;
        $data['will_recommend']         = 0;

        $rating_result  = $feedback_db->addFinalFeedbackRating($data);

        /*	Sends a notification email to user				*/
        if (env("APP_ENV") != "local"){
            try{
                Mail::send('emails.final-feedback', array('trans_id'=>$trans_id), function($message) {
                    $message->to('razor_rough007@yahoo.com', 'razor_rough007@yahoo.com')
                        ->subject('[CreditBPO] We value your experience');

                    $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                    $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                });
            }catch(Exception $e){
            //
            }
        }
    }

    //-----------------------------------------------------
    //  Function 54.10: sendFollowUpUserFeedback
    //      Sends the Follow-up request for the Final User Feedback
    //-----------------------------------------------------
    public function sendFollowUpUserFeedback()
    {
        /*  Instantiate Classes     */
        $feedback_db    = new FeedbackFinalRating;

        /*  Checks for emails to be followed up */
        $fback_data     = $feedback_db->checkFollowUpFeedback();

        /*  Sends the mail and update the database  */
        foreach ($fback_data as $fback) {
            /*	Sends a notification email to user				*/
            if (env("APP_ENV") != "local") {
                try{
                    Mail::send('emails.final-feedback', array('trans_id'=>$fback['transaction_id']), function($message) use ($fback, $feedback_db) {
                        $message->to($fback['email'], $fback['email'])
                            ->subject('[CreditBPO] Your feedback will help improve your CreditBPO experience');

                        $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                        $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                    });
                }catch(Exception $e){
                //
                }
            }
            /** Update the Database     */
            $data           = $fback;
            $data['status'] = FINAL_RATING_FOLLOWED;
            $rating_result  = $feedback_db->updateFinalFeedbackRating($data);
        }
    }

}
