<?php
use CreditBPO\Services\MailHandler;
//======================================================================
//  Class 23: FormSMEController
//  Description: Contains functions for multiple questionnaire data
//  (condition sustainability, owner details, existing credit facilities, required credit line)

//======================================================================

class FormSMEController extends BaseController {

	//-----------------------------------------------------
    //  Function 23.1: getSMEBusDetails
	//  Description: get SME Business Details I form
    //-----------------------------------------------------
	public function getSMEBusDetails($id)
	{
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }

		$entity = DB::table('tblentity')->where('entityid', $id)->first();
		//JM - changed list to pluck
		$dataBank = Bank::all()->pluck('bank_name', 'id');
		$dataIndustryMain = Industrymain::all()->pluck('main_title', 'industry_main_id');
		$industry_main_id = ($entity->industry_main_id != 0) ? $entity->industry_main_id : Input::old('industrymain');
		$dataIndustrySub = Industrysub::where('industry_main_id', $industry_main_id)->pluck('sub_title', 'industry_sub_id');
		$industry_sub_id = ($entity->industry_sub_id != 0) ? $entity->industry_sub_id : Input::old('industrysub');
		$dataIndustryRow = Industryrow::where('industry_sub_id', $industry_sub_id)->pluck('row_title', 'industry_row_id');
		$dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		$prov = ($entity->province != '') ? $entity->province : Input::old('province');
		$dataCity = Zipcode::select('id','city')->where('major_area', $prov)->pluck('city','id');

		$entity->dateestablished = $entity->date_established;
		$entity->numberyear = $entity->number_year;
		$entity->employeesize = $entity->employee_size;
		$entity->city = $entity->cityid;
		$entity->bank = $entity->current_bank;
		$entity->requested_by = $entity->is_independent;
		$entity->emailalt = $entity->email;
		$entity->street = $entity->address1;
		$entity->industrymain = $entity->industry_main_id;
		$entity->industrysub = $entity->industry_sub_id;
		$entity->industryrow = $entity->industry_row_id;

		return View::make('sme.busdetails',
			array(
				'cityList' => $dataCity,
				'bankList' => $dataBank,
				'provinceList' => $dataProvince,
				'industrymainList' => $dataIndustryMain,
				'industrysubList' => $dataIndustrySub,
				'industryrowList' => $dataIndustryRow,
				)
			)->with('entity', $entity);
	}

	//-----------------------------------------------------
    //  Function 23.2: putSMEBusDetailsUpdate
	//  Description: update SME Business Details I
    //-----------------------------------------------------
	public function putSMEBusDetailsUpdate($id)
	{
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $doc_req                    = NULL;
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.$id);
        }

		$messages = array(
			'companyname.required' => trans('validation.required', array('attribute'=>'Company Name')),
			'company_tin.required' => trans('validation.required', array('attribute'=>'Company TIN')),
            'tin_num.required' => trans('validation.required', array('attribute'=>'Company TIN')),
		    'industrymain.required' => trans('validation.required', array('attribute'=>'Industry Main')),
		    'industrysub.required' => trans('validation.required', array('attribute'=>'Industry Sub')),
		    'industryrow.required' => trans('validation.required', array('attribute'=>'Industry')),
		    'emailalt.required' => trans('validation.required', array('attribute'=>'Email Address')),
		    'street.required' => trans('validation.required', array('attribute'=>'Primary Business Address')),
		    'city.required' => trans('validation.required', array('attribute'=>'Primary Business Address City')),
		    'province.required' => trans('validation.required', array('attribute'=>'Primary Business Address Province')),
		    'phone.required' => trans('validation.required', array('attribute'=>'Primary Business Phone')),
		    'no_yrs_present_address.required' => trans('validation.required', array('attribute'=>trans('business_details1.no_yrs_present_address'))),
		    'dateestablished.required' => trans('validation.required', array('attribute'=>trans('business_details1.date_established'))),
		    'dateestablished.date_format' => trans('validation.date_format', array('attribute'=>trans('business_details1.date_established'), 'format'=>'YYYY-MM-DD')),
		    'numberyear.required' => trans('validation.required', array('attribute'=>'Years in Business')),
		    'description.required' => trans('validation.required', array('attribute'=>trans('business_details1.company_description'))),
		    'employeesize.required' => trans('validation.required', array('attribute'=>trans('business_details1.employee_size'))),
		    'employeesize.numeric' => trans('validation.numeric', array('attribute'=>trans('business_details1.employee_size'))),
		    'employeesize.max' => trans('validation.max.numeric', array('attribute'=>trans('business_details1.employee_size'))),
		    'updated_date.required' => trans('validation.required', array('attribute'=>trans('business_details1.updated_date'))),
		    'updated_date.date_format' => trans('validation.date_format', array('attribute'=>trans('business_details1.updated_date'), 'format'=>'YYYY-MM-DD')),
			'number_year_management_team.required' => trans('validation.required', array('attribute'=>trans('business_details1.number_year_management_team'))),
			'total_assets.numeric' => trans('validation.numeric', array('attribute'=>trans('business_details1.total_assets'))),
			'total_assets.max' => trans('validation.max.numeric', array('attribute'=>trans('business_details1.total_assets'))),
			'no_yrs_present_address.numeric' => trans('validation.numeric', array('attribute'=>trans('business_details1.no_yrs_present_address'))),
			'sec_reg_date.required' => trans('validation.required', array('attribute'=>'SEC Registration Date')),
			'sec_reg_date.before'   => trans('validation.before', array('attribute'=>'SEC Registration Date')),
		    'sec_reg_date.date_format' => trans('validation.date_format', array('attribute'=>'SEC Registration Date', 'format'=>'YYYY-MM-DD')),
            'total_asset_grouping.required' => trans('validation.required', array('attribute'=>trans('business_details1.total_asset_grouping'))),
            'total_asset_grouping.numeric' => trans('validation.numeric', array('attribute'=>trans('business_details1.total_asset_grouping'))),
		);

		$rules = array(
			'companyname'	=> 'required',
			'entity_type'	=> 'required',
			'company_tin'   => 'required',
			'tin_num'	=> 'required',
	        'industrymain'	=> 'required|numeric',
	        'industrysub'	=> 'required|numeric',
	        'industryrow'	=> 'required|numeric',
	        'emailalt'	=> 'required|email',
	        'street'	=> 'required',
	        'city'	=> 'required|numeric',
	        'province'	=> 'required',
	        'zipcode'	=> 'digits:4',
	        'phone'	=> 'required',
	        'no_yrs_present_address' => 'required|numeric',
	        'dateestablished' => array('required', 'date_format:"Y-m-d"'),
	        'numberyear'	=> 'required|numeric',
	        'description'	=> 'required',
	        'employeesize'	=> 'required|numeric|max:2147483647',
	        'updated_date'	=> array('required', 'date_format:"Y-m-d"'),
	        'number_year_management_team' => 'required|numeric',
	        'bank' => 'required|numeric',
			'total_assets' => 'required|numeric|max:9999999999999.99',
			'total_asset_grouping' => 'required|numeric|max:9999999999999.99',
			'sec_reg_date' => array('required', 'date_format:"Y-m-d"'),
	    );

        $entity_data        = DB::table('tblentity')->where('entityid', $id)->select('sec_cert', 'companyname', 'permit_to_operate', 'authority_to_borrow', 'authorized_signatory', 'tax_registration', 'income_tax')->first();
        $comp_name_exist    = DB::table('tblentity')->where('companyname', Input::get('companyname'))->select('companyname')->first();

        if (Input::has('current_bank')) {
            $doc_req = DB::table('bank_doc_options')
                ->where('bank_id', Input::get('current_bank'))
                ->where('is_active', STS_OK)
                ->first();
        }

        /*--------------------------------------------------------------------
		/*	No article of incorporation uploaded yet
		/*------------------------------------------------------------------*/
        if ((STR_EMPTY == $entity_data->sec_cert || Input::hasFile('sec_cert')) && (BIZ_ENTITY_CORP == Input::get('entity_type'))) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['sec_cert.required']      = trans('validation.required', array('attribute'=>'Registration Document'));
            $messages['sec_cert.mimes']         = 'Registration Document must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['sec_cert']                  = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor does not require SEC Certificate
            /*-------------------------------------------------------------*/
            if ($doc_req) {
                if (STS_NG == $doc_req->corp_sec_reg) {
                    $rules['sec_cert']                  = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

        /*--------------------------------------------------------------------
		/*	No business permit uploaded yet
		/*------------------------------------------------------------------*/
        if (STR_EMPTY == $entity_data->permit_to_operate || Input::hasFile('permit_to_operate')) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['permit_to_operate.required_with'] = trans('validation.required', array('attribute'=>'Business Permit'));
            $messages['permit_to_operate.mimes']         = 'Business Permit must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['permit_to_operate']                  = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor does not require Business Permit
            /*-------------------------------------------------------------*/
            if (($doc_req)
            && (Input::has('entity_type'))) {
                if ((STS_NG == $doc_req->corp_business_license && BIZ_ENTITY_CORP == Input::get('entity_type'))
                || (STS_NG == $doc_req->sole_business_license && BIZ_ENTITY_SOLE == Input::get('entity_type'))) {
                    $rules['permit_to_operate']         = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

        /*--------------------------------------------------------------------
		/*	When Board resolution is uploaded
		/*------------------------------------------------------------------*/
        if ((Input::hasFile('authority_to_borrow')) && (BIZ_ENTITY_CORP == Input::get('entity_type'))) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['authority_to_borrow.required']       = trans('validation.required', array('attribute'=>'Board Resolution'));
            $messages['authority_to_borrow.mimes']          = 'Board Resolution must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['authority_to_borrow']                   = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor requires Authority to borrow
            /*-------------------------------------------------------------*/
            if ($doc_req) {
                if (STS_OK == $doc_req->corp_board_resolution) {
                    $rules['authority_to_borrow']           = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

        /*--------------------------------------------------------------------
		/*	When Authorized Signatory is uploaded
		/*------------------------------------------------------------------*/
        if (Input::hasFile('authorized_signatory')) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['authorized_signatory.required']      = trans('validation.required', array('attribute'=>'Authorized Signatory'));
            $messages['authorized_signatory.mimes']         = 'Authorized Signatory must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['authorized_signatory']                  = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor requires Authorized Signatory
            /*-------------------------------------------------------------*/
            if (($doc_req)
            && (Input::has('entity_type'))) {
                if ((STS_OK == $doc_req->corp_board_resolution_pres && BIZ_ENTITY_CORP == Input::get('entity_type'))
                || (STS_OK == $doc_req->sole_business_owner && BIZ_ENTITY_SOLE == Input::get('entity_type'))) {
                    $rules['authorized_signatory']          = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

        /*--------------------------------------------------------------------
		/*	When Tax Registration is uploaded
		/*------------------------------------------------------------------*/
        if (Input::hasFile('tax_registration')) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['tax_registration.required']      = trans('validation.required', array('attribute'=>'BIR Registration'));;
            $messages['tax_registration.mimes']         = 'BIR Registration must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['tax_registration']                  = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor requires BIR Registration
            /*-------------------------------------------------------------*/
            if (($doc_req)
            && (Input::has('entity_type'))) {
                if ((STS_OK == $doc_req->corp_tax_reg && BIZ_ENTITY_CORP == Input::get('entity_type'))
                || (STS_OK == $doc_req->sole_tax_reg && BIZ_ENTITY_SOLE == Input::get('entity_type'))) {
                    $rules['tax_registration']          = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

        /*--------------------------------------------------------------------
		/*	No Income Tax Return Uploaded
		/*------------------------------------------------------------------*/
        if (STR_EMPTY == $entity_data->income_tax || Input::hasFile('income_tax')) {
            /*---------------------------------------------------------------
            /*	Add messages and rules for upload field
            /*-------------------------------------------------------------*/
            $messages['income_tax.required']        = trans('validation.required', array('attribute'=>trans('business_details1.income_tax_return')));
            $messages['income_tax.mimes']           = trans('business_details1.income_tax_return').' must be a ZIP, RAR, JPG, PNG, PDF, Word Document';
            $rules['income_tax']                    = 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';

            /*---------------------------------------------------------------
            /*	If Supervisor requires BIR Registration
            /*-------------------------------------------------------------*/
            if (($doc_req)
            && (Input::has('entity_type'))) {
                if ((STS_NG == $doc_req->corp_itr && BIZ_ENTITY_CORP == Input::get('entity_type'))
                || (STS_NG == $doc_req->sole_itr && BIZ_ENTITY_SOLE == Input::get('entity_type'))) {
                    $rules['income_tax']          = 'mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt';
                }
            }
        }

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
            if (isset(Auth::user()->loginid)) {
                return Redirect::to('/sme/busdetails/'.$id.'/edit')->withErrors($validator)->withInput();
            }
            else {
                $srv_resp['messages']	= $validator->messages()->all();
                return json_encode($srv_resp);
            }
        }
		else
		{
            if (1 <= @count($comp_name_exist)) {
                if ($entity_data->companyname != $comp_name_exist->companyname) {
                    $errors = new Illuminate\Support\MessageBag;
                    $errors->add("error", 'Company name already exist');

                    return Redirect::to('/sme/busdetails/'.$id.'/edit')->withErrors($errors)->withInput();
                }
            }

			$entityFields = array(
				'companyname' => Input::get('companyname'),
				'entity_type' => Input::get('entity_type'),
				'company_tin' => Input::get('company_tin'),
				'industry_main_id' =>Input::get('industrymain'),
				'industry_sub_id' =>Input::get('industrysub'),
				'industry_row_id' =>Input::get('industryrow'),
				'total_assets' =>Input::get('total_assets'),
				'total_asset_grouping' =>Input::get('total_asset_grouping'),
				'website' =>Input::get('website'),
				'email' =>Input::get('emailalt'),
				'address1' =>Input::get('street'),
				'cityid' =>Input::get('city'),
				'province' =>Input::get('province'),
				'zipcode' =>Input::get('zipcode'),
				'phone' =>Input::get('phone'),
				'former_address1' =>Input::get('former_address1'),
				'former_address2' =>Input::get('former_address2'),
				'former_cityid' =>Input::get('former_cityid'),
				'former_province' =>Input::get('former_province'),
				'former_zipcode' =>Input::get('former_zipcode'),
				'former_phone' =>Input::get('former_phone'),
				'no_yrs_present_address' =>Input::get('no_yrs_present_address'),
				'date_established' =>Input::get('dateestablished'),
				'number_year' =>Input::get('numberyear'),
				'description' =>Input::get('description'),
				'employee_size' =>Input::get('employeesize'),
				'updated_date' =>Input::get('updated_date'),
				'number_year_management_team' =>Input::get('number_year_management_team'),
				'tin_num' =>Input::get('tin_num'),
				'sec_num' =>Input::get('sec_num'),
				'sec_reg_date' =>Input::get('sec_reg_date'),
				'current_bank' => (Input::get('requested_by')==1) ? Input::get('bank') : null,
				'is_independent' =>Input::get('requested_by')
			);

            /*---------------------------------------------------------------
            /*	Upload Article of Incorporation
            /*-------------------------------------------------------------*/
            $inc_doc    = Input::file('sec_cert');

            if (Input::hasFile('sec_cert')) {
                /** Sets the filename and destination dir    */
                $destination_path           = 'incorporation-docs';
                $extension                  = $inc_doc->getClientOriginalExtension();
                $filename                   = 'inc-doc-'.$id.'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $inc_doc->move($destination_path, $filename);

                /** Update database information    */
                $entityFields['sec_cert']   = $filename;
            }

            /*---------------------------------------------------------------
            /*	Upload Permit to Operate
            /*-------------------------------------------------------------*/
            $operate_doc    = Input::file('permit_to_operate');

            if (Input::hasFile('permit_to_operate')) {
                /** Sets the filename and destination dir    */
                $destination_path           = 'documents';
                $extension                  = $operate_doc->getClientOriginalExtension();
                $filename                   = 'permit-to-operate-doc'.$id.'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $operate_doc->move($destination_path, $filename);

                /** Update database information    */
                $entityFields['permit_to_operate']   = $filename;
            }

            /*---------------------------------------------------------------
            /*	Upload Authority to borrow
            /*-------------------------------------------------------------*/
            $borrow_doc    = Input::file('authority_to_borrow');

            if (Input::hasFile('authority_to_borrow')) {
                /** Sets the filename and destination dir    */
                $destination_path           = 'documents';
                $extension                  = $borrow_doc->getClientOriginalExtension();
                $filename                   = 'authority-borrow-doc'.$id.'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $borrow_doc->move($destination_path, $filename);

                /** Update database information    */
                $entityFields['authority_to_borrow']   = $filename;
            }

            /*---------------------------------------------------------------
            /*	Upload Authority to borrow
            /*-------------------------------------------------------------*/
            $auth_sig_doc    = Input::file('authorized_signatory');

            if (Input::hasFile('authorized_signatory')) {
                /** Sets the filename and destination dir    */
                $destination_path           = 'documents';
                $extension                  = $auth_sig_doc->getClientOriginalExtension();
                $filename                   = 'authorized-sig-doc'.$id.'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $auth_sig_doc->move($destination_path, $filename);

                /** Update database information    */
                $entityFields['authorized_signatory']   = $filename;
            }

            /*---------------------------------------------------------------
            /*	Upload Authority to borrow
            /*-------------------------------------------------------------*/
            $tax_reg_doc    = Input::file('tax_registration');

            if (Input::hasFile('tax_registration')) {
                /** Sets the filename and destination dir    */
                $destination_path           = 'documents';
                $extension                  = $tax_reg_doc->getClientOriginalExtension();
                $filename                   = 'tax-reg-doc'.$id.'-'.time().'.'.$extension;

                /** Upload the file    */
                $upload_success             = $tax_reg_doc->move($destination_path, $filename);

                /** Update database information    */
                $entityFields['tax_registration']   = $filename;
            }

			$updateProcess = Entity::where('entityid', '=', $id)->update($entityFields);

            if (!isset(Auth::user()->loginid)) {
                if ($updateProcess) {
                    $srv_resp['sts']        = STS_OK;
                    $srv_resp['messages']   = 'Successfully Updated Record';
                }

                return json_encode($srv_resp);
            }
            else {
                if ($updateProcess) {
                    return Redirect::to('/summary/smesummary/'.$id);
                }
                else {
                    return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.$id.'">Please click here to return</a>');
                }
            }
		}
	}

	//-----------------------------------------------------
    //  Function 23.3: getSMEBusConSus
	//  Description: get condition and sustainablity form
	//-----------------------------------------------------
	public function getSMEBusConSus($type, $entity_id)
	{
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }

		$entity = DB::table('tblsustainability')->where('entity_id', $entity_id)->first();

		return View::make('sme.busconandsus',
            array(
                'type'      => $type,
                'entity_id' => $entity_id
            )
        )->with('entity', $entity);
	}

	//-----------------------------------------------------
    //  Function 23.4: getSMEBusConSus
	//  Description: update condition and sustainablity
	//-----------------------------------------------------
	public function postSMEBusConSus($type, $entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#'.$type.'-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-'.$type;
        }

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

		switch($type){
			case "succession":
				$rules = array(
					'succession_plan'       => 'required',
					'succession_timeframe'	=> 'required'
				);
				break;
			case "competitionlandscape":
				$rules = array(
					'competition_landscape'	=> 'required'
				);
				break;
			case "economicfactors":
				$rules = array(
					'ev_susceptibility_economic_recession'	=> 'required',
					'ev_foreign_exchange_interest_sensitivity'	=> 'required',
					'ev_commodity_price_volatility'	=> 'required',
					'ev_governement_regulation'	=> 'required'
				);
				break;
			case "taxpayments":
				$rules = array(
					'tax_payments'	=> 'required'
				);
				break;
		}

		$validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else
		{
			$sustainability = Sustainability::where('entity_id', '=', $entity_id)->first();

            if($sustainability != null) {

				switch($type){
					case "succession":
						$entityUpdate = array(
							'succession_plan' => Input::get('succession_plan'),
							'succession_timeframe' => Input::get('succession_timeframe')
						);
						break;
					case "competitionlandscape":
						$entityUpdate = array(
							'competition_landscape' => Input::get('competition_landscape'),
							'competition_timeframe' => Input::get('competition_timeframe')
						);
						break;
					case "economicfactors":
						$entityUpdate = array(
							'ev_susceptibility_economic_recession' => Input::get('ev_susceptibility_economic_recession'),
							'ev_foreign_exchange_interest_sensitivity' => Input::get('ev_foreign_exchange_interest_sensitivity'),
							'ev_commodity_price_volatility' => Input::get('ev_commodity_price_volatility'),
							'ev_governement_regulation' => Input::get('ev_governement_regulation')
						);
						break;
					case "taxpayments":
						$entityUpdate = array(
							'tax_payments' => Input::get('tax_payments')
						);
						break;
				}

				if(Sustainability::where('entity_id', '=', $entity_id)->update($entityUpdate))
				{
                    $srv_resp['sts']        = STS_OK;
                    $srv_resp['messages']   = 'Successfully Added Record';

                    if ((Input::has('action')) && (Input::has('section'))) {
                        $srv_resp['master_ids'] = $sustainability->sustainabilityid;
                    }
				}
				else
				{
					$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
				}

			}
            else {
				$sustainability = new Sustainability();
				$sustainability->entity_id = $entity_id;
				$sustainability->succession_plan = Input::get('succession_plan');
				$sustainability->succession_timeframe = Input::get('succession_timeframe');
				$sustainability->competition_landscape = Input::get('competition_landscape');
				$sustainability->competition_timeframe = Input::get('competition_timeframe');
				$sustainability->ev_susceptibility_economic_recession = Input::get('ev_susceptibility_economic_recession');
				$sustainability->ev_foreign_exchange_interest_sensitivity = Input::get('ev_foreign_exchange_interest_sensitivity');
				$sustainability->ev_commodity_price_volatility = Input::get('ev_commodity_price_volatility');
				$sustainability->ev_governement_regulation = Input::get('ev_governement_regulation');
				$sustainability->mt_executive_team = 0;
				$sustainability->mt_middle_management = 0;
				$sustainability->mt_staff = 0;
				$sustainability->tax_payments = Input::get('tax_payments');

				if ($sustainability->save()) {
					$srv_resp['sts']        = STS_OK;
                    $srv_resp['messages']   = 'Successfully Added Record';

                    if ((Input::has('action')) && (Input::has('section'))) {
                        $srv_resp['master_ids'] = $sustainability->sustainabilityid;
                    }
				}
				else {
					$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
				}
			}
		}
        ActivityLog::saveLog();
        return json_encode($srv_resp);
	}

	//-----------------------------------------------------
    //  Function 23.5: getSMEPlanFacility
	//  Description: get Existing Credit Facility form
	//-----------------------------------------------------
	public function getSMEPlanFacility($entity_id)
	{
		return View::make('sme.planfacility',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 23.6: postSMEPlanFacilityCreate
	//  Description: update Existing Credit Facility
	//-----------------------------------------------------
	public function postSMEPlanFacilityCreate($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        $purpose        = Input::get('purpose_credit_facility');

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#payfactor';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-ecf';
        }

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

		$rules = array(
            'lending_institution'           => 'required',
			'plan_credit_availment'	        => 'required|numeric|max:9999999999999.99',
            'outstanding_balance'	        => 'numeric|max:9999999999999.99',
			'purpose_credit_facility'	    => 'required',
			'cash_deposit_details'	        => 'sometimes',
			'cash_deposit_amount'	        => 'required_with:cash_deposit_details|numeric|max:9999999999999.99',
			'securities_details'	        => 'sometimes',
			'securities_estimated_value'	=> 'required_with:securities_details|numeric|max:9999999999999.99',
			'property_details'	            => 'sometimes',
			'property_estimated_value'	    => 'required_with:property_details|numeric|max:9999999999999.99',
			'chattel_details'	            => 'sometimes',
			'chattel_estimated_value'	    => 'required_with:chattel_details|numeric|max:9999999999999.99',
			'others_details'	            => 'sometimes',
			'others_estimated_value'	    => 'required_with:others_details|numeric|max:9999999999999.99',
		);

        $messages    = array(
            'lending_institution.required'              => 'Lending Institution is required',
            'plan_credit_availment.required'            => 'Amount of Line is required',
            'plan_credit_availment.numeric'             => 'Amount of Line must be a number - please do not use any signs ($, %, #, etc)',
            'outstanding_balance.numeric'               => 'Outstanding Balance must be a number - please do not use any signs ($, %, #, etc)',
            'plan_credit_availment.max'            		=> 'Maximum value for Amount of Line is 9999999999999.99',
            'purpose_credit_facility.required'          => 'Purpose of Credit Facility is required',
            'cash_deposit_amount.required_with'         => 'Deposit Amount is required',
            'securities_estimated_value.required_with'  => 'Marketable Securities Estimated Value is required',
            'property_estimated_value.required_with'    => 'Property Estimated Value is required',
            'chattel_estimated_value.required_with'     => 'Chattel Estimated Value is required',
            'others_estimated_value.required_with'      => 'Others Estimated Value is required'
        );

        if (Input::get('purpose_credit_facility')) {
            if (4 == Input::get('purpose_credit_facility'))  {
                $rules['purpose_credit_facility_others']                = 'required';
                $messages['purpose_credit_facility_others.required']    = trans('ecf.purpose_of_cf_others').' is required';
            }
            else if (5 == Input::get('purpose_credit_facility'))  {
                $rules['credit_terms']              = 'required';
                $messages['credit_terms.required']  = trans('ecf.credit_terms').' is required';
            }
            else {

            }
        }

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {

            if ((Input::has('action')) && ('edit' == Input::get('action')) && (Input::has('master_key'))) {
                $planfacility = Planfacility::find(Input::get('master_key'));
                $srv_resp['messages']   = 'Successfully Updated Record';
            }
            else {
                $planfacility = new Planfacility();
                $srv_resp['messages']   = 'Successfully Added Record';
            }

			$planfacility->entity_id                        = $entity_id;
			$planfacility->lending_institution              = Input::get('lending_institution');
			$planfacility->plan_credit_availment            = Input::get('plan_credit_availment');
            $planfacility->credit_terms                     = Input::get('credit_terms')? : 0;
			$planfacility->outstanding_balance              = Input::get('outstanding_balance');
			$planfacility->experience                       = Input::get('experience');
			$planfacility->other_remarks                    = Input::get('other_remarks')? :0;
			$planfacility->cash_deposit_details             = Input::get('cash_deposit_details');
			$planfacility->cash_deposit_amount              = Input::get('cash_deposit_amount');
			$planfacility->securities_details               = Input::get('securities_details');
			$planfacility->securities_estimated_value       = Input::get('securities_estimated_value');
			$planfacility->property_details                 = Input::get('property_details');
			$planfacility->property_estimated_value         = Input::get('property_estimated_value');
			$planfacility->chattel_details                  = Input::get('chattel_details');
			$planfacility->chattel_estimated_value          = Input::get('chattel_estimated_value');
			$planfacility->others_details                   = Input::get('others_details');
			$planfacility->others_estimated_value           = Input::get('others_estimated_value');
			$planfacility->purpose_credit_facility          = Input::get('purpose_credit_facility') ?:0;
			$planfacility->purpose_credit_facility_others   = Input::get('purpose_credit_facility_others')?:0;

			if ($planfacility->save()) {
                $srv_resp['sts']        = STS_OK;

                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $planfacility->planfacilityid;
                }
			}
			else {
				$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
			}
		}
        ActivityLog::saveLog();
        return json_encode($srv_resp);
	}

	//-----------------------------------------------------
    //  Function 23.7: getSMEPlanFacilityDelete
	//  Description: delete Existing Credit Facility
	//-----------------------------------------------------
    public function getSMEPlanFacilityDelete($id)
	{
		// DB::table('tblplanfacility')
		// ->where('planfacilityid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
		// ->delete();

		$plan = Planfacility::where('planfacilityid', '=', $id)
				->where('entity_id', Session::get('entity')->entityid)
				->first();
		$plan->is_deleted = 1;
		$plan->save();

        ActivityLog::saveLog();
		Session::flash('redirect_tab', 'tab4');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
    }

	//-----------------------------------------------------
    //  Function 23.8: getSMEFutureSourceEdit
	//  Description: get Future Source update page
	//-----------------------------------------------------
	public function getSMEFutureSourceEdit($id)
	{
		$planfacility = DB::table('tblfuturesource')->where('entity_id', Session::get('entity')->entityid)->first();
		return View::make('sme.futuresource')->with('planfacility', $planfacility);
	}

	//-----------------------------------------------------
    //  Function 23.9: putSMEFutureSourceUpdate
	//  Description: update Future Source
	//-----------------------------------------------------
	public function putSMEFutureSourceUpdate($id)
	{
		$messages = array();

		$expopg_per_year = Input::get('opg_per_year');
		$expopg_horizontal_year = Input::get('opg_horizontal_year');
		$expfamily_per_year = Input::get('family_per_year');
		$expfamily_horizon_year = Input::get('family_horizon_year');
		$expbp_per_year = Input::get('bp_per_year');
		$expbp_horizon_year = Input::get('bp_horizon_year');
		$expvcc_per_year = Input::get('vcc_per_year');
		$expvcc_horizon_year = Input::get('vcc_horizon_year');
		$expipo_per_year = Input::get('ipo_per_year');
		$expipo_horizon_year = Input::get('ipo_horizon_year');
		$expothers_per_year = Input::get('others_per_year');
		$expothers_horizon_year = Input::get('others_horizon_year');

		if(empty($expopg_per_year) or empty($expopg_horizontal_year)){
			$rules = array(
		        'opg_per_year'	=> 'required|numeric',
		        'opg_horizontal_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expfamily_per_year) or empty($expfamily_horizon_year)){
			$rules = array(
		        'family_per_year'	=> 'required|numeric',
		        'family_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expbp_per_year) or empty($expbp_horizon_year)){
			$rules = array(
		        'bp_per_year'	=> 'required|numeric',
		        'bp_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expvcc_per_year) or empty($expvcc_horizon_year)){
			$rules = array(
		        'vcc_per_year'	=> 'required|numeric',
		        'vcc_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expipo_per_year) or empty($expipo_horizon_year)){
			$rules = array(
		        'ipo_per_year'	=> 'required|numeric',
		        'ipo_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expothers_per_year) or empty($expothers_horizon_year)){
			$rules = array(
		        'others_per_year'	=> 'required|numeric',
		        'others_horizon_year'	=> 'required|numeric|min:4'
		    );
		}else{
			$rules = array();
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			return Redirect::to('/sme/futuresource/'.Session::get('entity')->entityid.'/edit')->withErrors($validator)->withInput();
		}
		else
		{
			$dataFields = array(
				'opg_per_year' => Input::get('opg_per_year'),
				'opg_horizontal_year' =>Input::get('opg_horizontal_year'),
				'family_per_year' =>Input::get('family_per_year'),
				'family_horizon_year' =>Input::get('family_horizon_year'),
				'bp_per_year' =>Input::get('bp_per_year'),
				'bp_horizon_year' =>Input::get('bp_horizon_year'),
				'vcc_per_year' =>Input::get('vcc_per_year'),
				'vcc_horizon_year' =>Input::get('vcc_horizon_year'),
				'ipo_per_year' =>Input::get('ipo_per_year'),
				'ipo_horizon_year' =>Input::get('ipo_horizon_year'),
				'others_per_year' =>Input::get('others_per_year'),
				'others_horizon_year' =>Input::get('others_horizon_year')
			);

			$updateProcess = Futuresource::where('entity_id', '=', Session::get('entity')->entityid)->update($dataFields);

			if($updateProcess)
			{
				Session::flash('redirect_tab', 'tab5');
				return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
			}
			else
			{
				return Redirect::to('/sme/confirmation/tab5')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return</a>');
			}

		}

	}

	//-----------------------------------------------------
    //  Function 23.10: getSMEFutureSource
	//  Description: get Future Source add page
	//-----------------------------------------------------
	public function getSMEFutureSource()
	{
		return View::make('sme.futuresource');
	}

	//-----------------------------------------------------
    //  Function 23.11: postSMEFutureSourceCreate
	//  Description: add and save Future Source
	//-----------------------------------------------------
	public function postSMEFutureSourceCreate()
	{
		$messages = array();

		$expopg_per_year = Input::get('opg_per_year');
		$expopg_horizontal_year = Input::get('opg_horizontal_year');
		$expfamily_per_year = Input::get('family_per_year');
		$expfamily_horizon_year = Input::get('family_horizon_year');
		$expbp_per_year = Input::get('bp_per_year');
		$expbp_horizon_year = Input::get('bp_horizon_year');
		$expvcc_per_year = Input::get('vcc_per_year');
		$expvcc_horizon_year = Input::get('vcc_horizon_year');
		$expipo_per_year = Input::get('ipo_per_year');
		$expipo_horizon_year = Input::get('ipo_horizon_year');
		$expothers_per_year = Input::get('others_per_year');
		$expothers_horizon_year = Input::get('others_horizon_year');

		if(empty($expopg_per_year) or empty($expopg_horizontal_year)){
			$rules = array(
		        'opg_per_year'	=> 'required|numeric',
		        'opg_horizontal_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expfamily_per_year) or empty($expfamily_horizon_year)){
			$rules = array(
		        'family_per_year'	=> 'required|numeric',
		        'family_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expbp_per_year) or empty($expbp_horizon_year)){
			$rules = array(
		        'bp_per_year'	=> 'required|numeric',
		        'bp_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expvcc_per_year) or empty($expvcc_horizon_year)){
			$rules = array(
		        'vcc_per_year'	=> 'required|numeric',
		        'vcc_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expipo_per_year) or empty($expipo_horizon_year)){
			$rules = array(
		        'ipo_per_year'	=> 'required|numeric',
		        'ipo_horizon_year'	=> 'required|numeric|min:4'
		    );
		}elseif(empty($expothers_per_year) or empty($expothers_horizon_year)){
			$rules = array(
		        'others_per_year'	=> 'required|numeric',
		        'others_horizon_year'	=> 'required|numeric|min:4'
		    );
		}else{
			$rules = array();
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			return Redirect::route('getSMEFutureSource')->withErrors($validator)->withInput();
		}
		else
		{

			$futuresource = new Futuresource();
			$futuresource->entity_id = Session::get('entity')->entityid;
			$futuresource->opg_per_year = Input::get('opg_per_year');
			$futuresource->opg_horizontal_year = Input::get('opg_horizontal_year');
			$futuresource->family_per_year = Input::get('family_per_year');
			$futuresource->family_horizon_year = Input::get('family_horizon_year');
			$futuresource->bp_per_year = Input::get('bp_per_year');
			$futuresource->bp_horizon_year = Input::get('bp_horizon_year');
			$futuresource->vcc_per_year = Input::get('vcc_per_year');
			$futuresource->vcc_horizon_year = Input::get('vcc_horizon_year');
			$futuresource->ipo_per_year = Input::get('ipo_per_year');
			$futuresource->ipo_horizon_year = Input::get('ipo_horizon_year');
			$futuresource->others_per_year = Input::get('others_per_year');
			$futuresource->others_horizon_year = Input::get('others_horizon_year');

			if($futuresource->save())
			{
				Session::flash('redirect_tab', 'tab1');
				return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
			}
			else
			{
				return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while creating the user. <a href="../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return</a>.');
			}
		}
	}

	//-----------------------------------------------------
    //  Function 23.12: getSMEBusOwner
	//  Description: get owner details form
	//-----------------------------------------------------
	public function getSMEBusOwner($entity_id)
	{
		$dataCity =  City::all()->pluck('city', 'cityid');
		$dataProvince =   Province::all()->pluck('province', 'provinceid');
        $dataProvince2 =   Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		$dataCity2 =    Zipcode::select('id', 'city', 'zip_code')->where('major_area', Input::old('province'))->get();
		return View::make('sme.busowner',
			array(
                'cityList'      => $dataCity,
                'cityList2'      => $dataCity2,
                'provinceList'  => $dataProvince,
                'provinceList2'  => $dataProvince2,
                'entity_id'     => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 23.13: deleteSMEBusOwner
	//  Description: delete owner details
	//-----------------------------------------------------
	public function deleteSMEBusOwner($id)
	{
		$spouse = DB::table('tblspouse')
		->where('ownerid', $id)
		->where('entity_id', Session::get('entity')->entityid)
		->first();

		$deleteid = array($id);
		if($spouse) {
			array_push($deleteid, $spouse->spouseid);
			DB::table('tblspouse')
			->where('entity_id', Session::get('entity')->entityid)
			->where('spouseid', $spouse->spouseid)
			->delete();
		}

		DB::table('tblowner')
		->where('entity_id', Session::get('entity')->entityid)
		->where('ownerid', $id)
		->delete();

		// delete details
		DB::table('tblmanagebus')
		->where('entity_id', Session::get('entity')->entityid)
		->whereIn('ownerid', $deleteid)
		->delete();
		DB::table('tbleducation')
		->where('entity_id', Session::get('entity')->entityid)
		->whereIn('ownerid', $deleteid)
		->delete();
		ActivityLog::saveLog();
		Session::flash('redirect_tab', 'tab1');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 23.14: postSMEBusOwnerStore
	//  Description: save owner details
	//-----------------------------------------------------
	public function postSMEBusOwnerStore($entity_id)
	{
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        $bank_db        = new Bank;
        $question_type  = $bank_db->getBankQuestionTypeByEntity($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }

		$rules = array(
			'firstname'	=> 'required',
			'middlename'	=> 'required',
			'lastname'	=> 'required',
			'nationality'	=> 'required',
			'birthdate'	=> array('required', 'date_format:"Y-m-d"', 'before: today'),
			'civilstatus'	=> 'required|numeric',
			'profession'	=> 'required',
			'email'	=> 'required|email',
			'address1'	=> 'required',
			/* 'address2'	=> 'required', */
			'cityid'	=> 'required|numeric',
			'province'	=> 'required',
			'zipcode'	=> 'digits:4',
			'present_address_status'	=> 'required',
			'no_yrs_present_address'	=> 'required|numeric|min:0|max:100',
			'phone'	=> 'required',
			'tin_num'	=> array('required','regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'percent_of_ownership'	=> 'required|numeric|min:0|max:100',
			'number_of_year_engaged'	=> 'required|numeric|min:0|max:100',
			'position'	=> 'required',
			'emailspouse'	=> 'email',
			'sbirthdate'	=> array('date_format:"Y-m-d"', 'before: today'),
			'stin_num'	=> array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
			'km_email'	=> 'email',
			'km_tin_num'	=> array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i'),
	    );

        $messages = array(
            'km_email.required'         => 'Key Manager Email is required',
            'km_email.email'            => 'Key Manager has Invalid Email Address format',
            'km_tin_num.regex'          => 'Key Manager tin num is invalid',
            'stin_num.regex'            => 'Spouse tin num invalid',
            'sbirthdate.date_format'    => 'Spouse Birthdate does not match the format Y-m-d',
            'sbirthdate.before'         => 'Spouse Birthdate must be a date after today',
		);

		if ((Input::get('civilstatus') == 1) && (QUESTION_TYPE_BANK == $question_type)) {
			$sparray = array(
				'sfirstname'    => 'required',
				'slastname'     => 'required',
			);
			$rules = array_merge($rules, $sparray);

			$spmessages = array(
				'sfirstname.required' => 'Spouse first name is required',
				'slastname.required' => 'Spouse last name is required',
			);
			$messages = array_merge($messages, $spmessages);
		}

		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails()) {
            if ((!Input::has('action')) && (!Input::has('section'))) {
                return Redirect::route('getSMEBusOwner', array($entity_id))->withErrors($validator)->withInput();
            }
            else {
                $srv_resp['messages']	= $validator->messages()->all();
                return json_encode($srv_resp);
            }
		}
		else
		{
			//$owneridgenerate = mt_rand(); * weird code, as this will create collisions. removing
			//$spouseidgenerate = mt_rand();

			$owner = new Owner();
			//$owner->ownerid = $owneridgenerate;
			$owner->entity_id = $entity_id;
			$owner->firstname = Input::get('firstname');
			$owner->middlename = Input::get('middlename');
			$owner->lastname = Input::get('lastname');
			$owner->nationality = Input::get('nationality');
			$owner->birthdate = Input::get('birthdate');
			$owner->civilstatus = Input::get('civilstatus');
			$owner->profession = Input::get('profession');
			$owner->email = Input::get('email');
			$owner->address1 = Input::get('address1');
			$owner->address2 = Input::get('address2');
			$owner->cityid = Input::get('cityid');
			$owner->province = Input::get('province');
			$owner->zipcode = Input::get('zipcode');
			$owner->present_address_status = Input::get('present_address_status');
			$owner->no_yrs_present_address = Input::get('no_yrs_present_address');
			$owner->phone = Input::get('phone');
			$owner->tin_num = Input::get('tin_num');
			$owner->percent_of_ownership = Input::get('percent_of_ownership');
			$owner->number_of_year_engaged = Input::get('number_of_year_engaged');
			$owner->position = Input::get('position');
			$owner->save();

			$owneridgenerate = DB::getPdo()->lastInsertId(); // get insert id

			$ownedbus1 = Input::only('bus_name', 'bus_type', 'bus_location');

			$bus_name = $ownedbus1['bus_name'];
			$bus_type = $ownedbus1['bus_type'];
			$bus_location = $ownedbus1['bus_location'];

			foreach ($bus_name as $key1 => $value1) {
				if($value1){
					$data1 = array(
							'ownerid'=>$owneridgenerate,
					    	'entity_id'=>$entity_id,
					    	'bus_name'=>$bus_name[$key1],
					    	'bus_type'=>$bus_type[$key1],
					    	'bus_location'=>$bus_location[$key1],
					    	'created_at'=>date('Y-m-d H:i:s'),
					    	'updated_at'=>date('Y-m-d H:i:s')
					);
					DB::table('tblmanagebus')->insert($data1);
				}
			}

			$ownededuc1 = Input::only('educ_type', 'school_name', 'educ_degree');

			$educ_type = $ownededuc1['educ_type'];
			$school_name = $ownededuc1['school_name'];
			$educ_degree = $ownededuc1['educ_degree'];

			foreach ($school_name as $ekey1 => $evalue1) {
				if($evalue1){
					$edata1 = array(
							'ownerid'=>$owneridgenerate,
					    	'entity_id'=>$entity_id,
					    	'educ_type'=> (isset($educ_type[$ekey1])) ? $educ_type[$ekey1] : "",
					    	'school_name'=> (isset($school_name[$ekey1])) ? $school_name[$ekey1] : "",
					    	'educ_degree'=> (isset($educ_degree[$ekey1])) ? $educ_degree[$ekey1] : "",
					    	'created_at'=>date('Y-m-d H:i:s'),
					    	'updated_at'=>date('Y-m-d H:i:s')
					);
					DB::table('tbleducation')->insert($edata1);
				}
			}

			$spouse = new Spouse();
			//$spouse->spouseid = $spouseidgenerate;
			$spouse->entity_id = $entity_id;
			$spouse->ownerid = $owneridgenerate;
			$spouse->firstname = Input::get('sfirstname');
			$spouse->middlename = Input::get('smiddlename');
			$spouse->lastname = Input::get('slastname');
			$spouse->nationality = Input::get('snationality');
			$spouse->birthdate = Input::get('sbirthdate');
			$spouse->profession = Input::get('sprofession');
			$spouse->email = Input::get('emailspouse');
			$spouse->phone = Input::get('sphone');
			$spouse->tin_num = Input::get('stin_num');
			$spouse->save();

			$spouseidgenerate = DB::getPdo()->lastInsertId(); // get insert id

			$ownedbus2 = Input::only('sbus_name', 'sbus_type', 'sbus_location');

			$sbus_name = $ownedbus2['sbus_name'];
			$sbus_type = $ownedbus2['sbus_type'];
			$sbus_location = $ownedbus2['sbus_location'];

			foreach ($sbus_name as $key2 => $value2) {
				if($value2){
					$data2 = array(
							'ownerid'=>$spouseidgenerate,
					    	'entity_id'=>$entity_id,
					    	'bus_name'=> (isset($sbus_name[$key2])) ? $sbus_name[$key2] : "",
					    	'bus_type'=> (isset($sbus_name[$key2])) ? $sbus_type[$key2] : "",
					    	'bus_location'=> (isset($sbus_name[$key2])) ? $sbus_location[$key2] : "",
					    	'created_at'=>date('Y-m-d H:i:s'),
					    	'updated_at'=>date('Y-m-d H:i:s')
					);
					DB::table('tblmanagebus')->insert($data2);
				}
			}

			$ownededuc2 = Input::only('seduc_type', 'sschool_name', 'seduc_degree');

			$seduc_type = $ownededuc2['seduc_type'];
			$sschool_name = $ownededuc2['sschool_name'];
			$seduc_degree = $ownededuc2['seduc_degree'];

			foreach ($sschool_name as $ekey2 => $evalue2) {
				if($evalue2){
					$edata2 = array(
							'ownerid'=>$spouseidgenerate,
					    	'entity_id'=>$entity_id,
					    	'educ_type'=>(isset($seduc_type[$ekey2])) ? $seduc_type[$ekey2] : "",
					    	'school_name'=>(isset($sschool_name[$ekey2])) ? $sschool_name[$ekey2] : "",
					    	'educ_degree'=>(isset($seduc_degree[$ekey2])) ? $seduc_degree[$ekey2] : "",
					    	'created_at'=>date('Y-m-d H:i:s'),
					    	'updated_at'=>date('Y-m-d H:i:s')
					);
					DB::table('tbleducation')->insert($edata2);
				}
			}

			if(Input::get('km_firstname')!=''){
				$km = new KeyManager();
				$km->ownerid = $owneridgenerate;
				$km->entity_id = $entity_id;
				$km->firstname = Input::get('km_firstname');
				$km->middlename = Input::get('km_middlename');
				$km->lastname = Input::get('km_lastname');
				$km->nationality = Input::get('km_nationality');
				$km->birthdate = Input::get('km_birthdate');
				$km->civilstatus = Input::get('km_civilstatus');
				$km->profession = Input::get('km_profession');
				$km->email = Input::get('km_email');
				$km->address1 = Input::get('km_address1');
				$km->address2 = Input::get('km_address2');
				$km->cityid = Input::get('km_cityid');
				$km->province = Input::get('km_province');
				$km->zipcode = Input::get('km_zipcode');
				$km->present_address_status = Input::get('km_present_address_status');
				$km->no_yrs_present_address = Input::get('km_no_yrs_present_address');
				$km->phone = Input::get('km_phone');
				$km->tin_num = Input::get('km_tin_num');
				$km->percent_of_ownership = Input::get('km_percent_of_ownership');
				$km->number_of_year_engaged = Input::get('km_number_of_year_engaged');
				$km->position = Input::get('km_position');
				$km->save();
			}

			/* if(Input::get('km2_firstname')!=''){
				$km2 = new KeyManager();
				$km2->ownerid = $owneridgenerate;
				$km2->entity_id = $entity_id;
				$km2->firstname = Input::get('km2_firstname');
				$km2->middlename = Input::get('km2_middlename');
				$km2->lastname = Input::get('km2_lastname');
				$km2->nationality = Input::get('km2_nationality');
				$km2->birthdate = Input::get('km2_birthdate');
				$km2->civilstatus = Input::get('km2_civilstatus');
				$km2->profession = Input::get('km2_profession');
				$km2->email = Input::get('km2_email');
				$km2->address1 = Input::get('km2_address1');
				$km2->address2 = Input::get('km2_address2');
				$km2->cityid = Input::get('km2_cityid');
				$km2->province = Input::get('km2_province');
				$km2->zipcode = Input::get('km2_zipcode');
				$km2->present_address_status = Input::get('km2_present_address_status');
				$km2->no_yrs_present_address = Input::get('km2_no_yrs_present_address');
				$km2->phone = Input::get('km2_phone');
				$km2->tin_num = Input::get('km2_tin_num');
				$km2->percent_of_ownership = Input::get('km2_percent_of_ownership');
				$km2->number_of_year_engaged = Input::get('km2_number_of_year_engaged');
				$km2->position = Input::get('km2_position');
				$km2->save();
			} */
			ActivityLog::saveLog();
			if ($owner->save()) {
				if ((!Input::has('action')) && (!Input::has('section'))) {
                    return Redirect::to('/summary/smesummary/'.$entity_id);
                }
                else {
                    $srv_resp['sts']        = STS_OK;
                    $srv_resp['messages']   = 'Successfully Added Record';
                    $srv_resp['master_ids'] = $owneridgenerate;
                    return json_encode($srv_resp);
                }
			}
			else {
                if ((!Input::has('action')) && (!Input::has('section'))) {
                    return Redirect::to('/sme/confirmation/tab5')->with('fail', 'An error occured while creating the user. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.');
                }
                else {
                    $srv_resp['messages']   = 'An error occured while creating the user';
                    return json_encode($srv_resp);
                }
			}
		}
	}

	//-----------------------------------------------------
    //  Function 23.15: getSMEservicesoffer
	//  Description: get Products/Services Offered form
	//-----------------------------------------------------
	public function getSMEservicesoffer($entity_id)
	{
		return View::make('sme.servicesoffer',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 23.16: postSMEservicesoffer
	//  Description: save Products/Services Offered data
	//-----------------------------------------------------
	public function postSMEservicesoffer($entity_id)
	{

        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $services_offer = Input::get('servicesoffer_name');
        $rules      = array();
        $messages   = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#offers-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-offers';
        }

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

		for($x = 0; $x < @count($services_offer); $x++) {
			if (($services_offer[$x]!="") || (0 == $x)) {

                $set = $x + 1;
                $messages['servicesoffer_name.'.$x.'.required']             = trans('validation.required', array('attribute'=>trans('business_details1.products_services_offered')));
                $messages['servicesoffer_targetmarket.'.$x.'.required']     = trans('validation.required', array('attribute'=>trans('business_details1.target_market')));
                $messages['servicesoffer_share_revenue.'.$x.'.required']	= trans('validation.required', array('attribute'=>trans('business_details1.share_to_total_sales')));
                $messages['servicesoffer_share_revenue.'.$x.'.numeric']     = trans('validation.numeric', array('attribute'=>trans('business_details1.share_to_total_sales')));
                $messages['servicesoffer_share_revenue.'.$x.'.min']         = trans('validation.min.numeric', array('attribute'=>trans('business_details1.share_to_total_sales')));
                $messages['servicesoffer_share_revenue.'.$x.'.max' ]        = trans('validation.max.numeric', array('attribute'=>trans('business_details1.share_to_total_sales')));
                $messages['servicesoffer_seasonality.'.$x.'.required']      = trans('validation.required', array('attribute'=>trans('business_details1.seasonality')));

                $rules['servicesoffer_name.'.$x]                = 'required';
                $rules['servicesoffer_targetmarket.'.$x]        = 'required';
                $rules['servicesoffer_share_revenue.'.$x]       = 'required|numeric|min:0|max:100';
                $rules['servicesoffer_seasonality.'.$x]         = 'required|numeric';
			}
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {
			$arraydata = Input::only('servicesoffer_name', 'servicesoffer_targetmarket', 'servicesoffer_share_revenue', 'servicesoffer_seasonality');

            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }

			$servicesoffer_name = $arraydata['servicesoffer_name'];
			$servicesoffer_targetmarket = $arraydata['servicesoffer_targetmarket'];
			$servicesoffer_share_revenue = $arraydata['servicesoffer_share_revenue'];
			$servicesoffer_seasonality = $arraydata['servicesoffer_seasonality'];

			foreach ($servicesoffer_name as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'=>$entity_id,
					   	'servicesoffer_name'=>$servicesoffer_name[$key],
					   	'servicesoffer_targetmarket'=>$servicesoffer_targetmarket[$key],
					   	'servicesoffer_share_revenue'=>$servicesoffer_share_revenue[$key],
					   	'servicesoffer_seasonality'=>$servicesoffer_seasonality[$key],
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);

                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {

                        DB::table('tblservicesoffer')
                            ->where('servicesofferid', $primary_id[$key])
                            ->update($data);

                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblservicesoffer')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}

			$srv_resp['sts']        = STS_OK;

            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();
        return json_encode($srv_resp);

	}

	//-----------------------------------------------------
    //  Function 23.17: getSMEservicesofferedit
	//  Description: get Products/Services Offered edit form
	//-----------------------------------------------------
	public function getSMEservicesofferedit($id)
	{
		$entity = DB::table('tblservicesoffer')
		->where('servicesofferid', $id)
		->where('entity_id', Session::get('entity')->entityid)
		->get();
		return View::make('sme.servicesoffer')->with('entity', $entity);
	}

	//-----------------------------------------------------
    //  Function 23.18: getSMEservicesofferDelete
	//  Description: delete Products/Services Offered
	//-----------------------------------------------------
    public function getSMEservicesofferDelete($id)
	{
		// DB::table('tblservicesoffer')
        // ->where('servicesofferid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
		// ->delete();

		$serviceoffer = Servicesoffer::where('servicesofferid', '=', $id)
						->where('entity_id', '=', Session::get('entity')->entityid)
						->first();
		$serviceoffer->is_deleted = 1;
		$serviceoffer->save();

		ActivityLog::saveLog();
        Session::flash('redirect_tab', 'tab1');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 23.19: getConfirmation
	//  Description: function for displaying confirmation page
	//-----------------------------------------------------
	public function getConfirmation($name)
	{
		Session::flash('redirect_tab', $name);
		return View::make('sme.confirmation');
	}

	//-----------------------------------------------------
    //  Function 23.20: getSMEPlanFacilityRequested
	//  Description: get Required Credit Line form
	//-----------------------------------------------------
	public function getSMEPlanFacilityRequested($entity_id)
	{
		return View::make('sme.planfacilityrequested',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 23.21: postSMEPlanFacilityCreateRequested
	//  Description: save Required Credit Line form
	//-----------------------------------------------------
	public function postSMEPlanFacilityCreateRequested($entity_id)
	{
		$messages = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#pfs';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-rcl';
        }

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }

		$rules = array(
			'plan_credit_availment'	=> 'required|numeric|max:9999999999999.99',
			'purpose_credit_facility'	=> 'required',
			'cash_deposit_details'	=> 'required_with:cash_deposit_amount',
			'cash_deposit_amount'	=> 'numeric|max:9999999999999.99|nullable',
			'securities_details'	=> 'required_with:securities_estimated_value',
			'securities_estimated_value'	=> 'numeric|max:9999999999999.99|nullable',
			'property_details'	=> 'required_with:property_estimated_value',
			'property_estimated_value'	=> 'numeric|max:9999999999999.99|nullable',
			'chattel_details'	=> 'required_with:chattel_estimated_value',
			'chattel_estimated_value'	=> 'numeric|max:9999999999999.99|nullable',
			'others_details'	=> 'required_with:others_estimated_value',
			'others_estimated_value'	=> 'numeric|max:9999999999999.99|nullable',
		);

        if (Input::get('purpose_credit_facility')) {
            if (4 == Input::get('purpose_credit_facility'))  {
                $rules['purpose_credit_facility_others']                = 'required';
                $messages['purpose_credit_facility_others.required']    = trans('ecf.purpose_of_cf_others').' is required';
            }
            else if (5 == Input::get('purpose_credit_facility'))  {
                $rules['credit_terms']              = 'required';
                $messages['credit_terms.required']  = trans('ecf.credit_terms').' is required';
            }
            else {

            }
        }

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {

            if ((Input::has('action')) && ('edit' == Input::get('action')) && (Input::has('master_key'))) {
                $planfacility = PlanfacilityRequested::find(Input::get('master_key'));
                $srv_resp['messages']   = 'Successfully Updated Record';
            }
            else {
                $planfacility = new PlanfacilityRequested();
                $srv_resp['messages']   = 'Successfully Added Record';
            }

			$planfacility->entity_id                        = $entity_id;
			$planfacility->plan_credit_availment            = Input::get('plan_credit_availment');
			$planfacility->credit_terms                     = Input::get('credit_terms') ? :"";
			$planfacility->other_remarks                    = Input::get('other_remarks');
			$planfacility->cash_deposit_details             = Input::get('cash_deposit_details');
			$planfacility->cash_deposit_amount              = Input::get('cash_deposit_amount');
			$planfacility->securities_details               = Input::get('securities_details');
			$planfacility->securities_estimated_value       = Input::get('securities_estimated_value');
			$planfacility->property_details                 = Input::get('property_details');
			$planfacility->property_estimated_value         = Input::get('property_estimated_value');
			$planfacility->chattel_details                  = Input::get('chattel_details');
			$planfacility->chattel_estimated_value          = Input::get('chattel_estimated_value');
			$planfacility->others_details                   = Input::get('others_details');
			$planfacility->others_estimated_value           = Input::get('others_estimated_value');
			$planfacility->purpose_credit_facility          = Input::get('purpose_credit_facility');
			$planfacility->purpose_credit_facility_others   = Input::get('purpose_credit_facility_others')?:"";

			if($planfacility->save()) {
				$srv_resp['sts']        = STS_OK;

                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['master_ids'] = $planfacility->id;
                }
			}
			else
			{
				$srv_resp['messages'][0]    = 'An error occured while adding Required Credit Line. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
			}
		}
        ActivityLog::saveLog();
        return json_encode($srv_resp);
	}

	//-----------------------------------------------------
    //  Function 23.22: getSMEPlanFacilityDeleteRequested
	//  Description: delete Required Credit Line
	//-----------------------------------------------------
    public function getSMEPlanFacilityDeleteRequested($id)
	{
		$rcl = PlanfacilityRequested::where('entity_id', '=', Session::get('entity')->entityid)->where('id', '=', $id)->first();

		$rcl->is_deleted = 1;
		$rcl->save();

        ActivityLog::saveLog();
        Session::flash('redirect_tab', 'tab6');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 23.23: getSuccessPremiumReport
	//  Description: display confirmation of premium report creation
	//-----------------------------------------------------
	public function getSuccessPremiumReport($entityId) {
		$entity = Entity::where('entityid', $entityId)->first();

		$mailHandler = new MailHandler();
		if($entity->is_paid == 1){
			$mailHandler->sendPremiumReportNotification($entity);
		}
		$data = '';

		// $region = Zipcode::where('id', $entity->cityid)->first();

		// $regionArea = $region->region;

		// if($regionArea == "NCR"){
		// 	$data = $regionArea;
		// }else{
		// 	$data = $regionArea;
		// }
		return View::make('sme.confirmation', array('entity' => $entity, 'data' => $data));
	}
	
	//-----------------------------------------------------
    //  Function 23.24: getPremiumConfirmation
	//  Description: display confirmation page of premium report creation
	//-----------------------------------------------------
	public function getPremiumConfirmation($name)
	{
		Session::flash('redirect_tab', $name);
		return View::make('report.confirmation');
	}

}
