<?php

use Illuminate\Http\Request;

class IndustryController extends Controller
{
    
    public function __construct(){

    }

    public function postSearch(){
        // search in industry row, sub, main
        $search = trim(Input::get("search"));
        $format = array();
        // variable will be use in the next ticket
        $mainStr = "";
        $subStr = "";
        $rowStr = "";
        $text = array();
        $sub = array();
        $row = array();

        if($search) {
            $text[] = DB::table("psic_subclass")->where("name", "like", "%" . $search . "%")->get()->toArray();
            $text[] = DB::table("psic_class")->where("name", "like", "%" . $search . "%")->orWhere("description", "like", "%" . $search . "%")->get()->toArray();
            $text[] = DB::table("psic_group")->where("name", "like", "%" . $search . "%")->orWhere("description", "like", "%" . $search . "%")->get()->toArray();
            // $text[] = DB::table("psic_division")->where("name", "like", "%" . $search . "%")->orWhere("description", "like", "%" . $search . "%")->get()->toArray();

            if(!empty($text[0])) {
                $isCheck = false;
                foreach ($text[0] as $key => $value) {
                    $isCheck = $this->filterAutocompleteResults($value, $search);
                    if($isCheck === true){
                        $cls = DB::table("psic_group")->where("code", substr_replace($value->code, "", -2))->first();
                        if(!in_array($cls->code, $row) && $cls->row_id){
                            $row[] = $cls->code;
                        }
                    }
                }
            }

            if(!empty($text[1])) {
                $isCheck = false;
                foreach ($text[1] as $key => $value) {
                    $isCheck = $this->filterAutocompleteResults($value, $search);
                    if($isCheck === true){
                        $cls = DB::table("psic_group")->where("code", substr_replace($value->code, "", -1))->first();
                        if($cls->row_id && $cls->row_id) { 
                            if(!in_array($cls->code, $row)){
                                $row[] = $cls->code;
                            }
                        }
                    }
                }
            }

            if(!empty($text[2])) {
                foreach ($text[2] as $key => $value) {
                    $isCheck = $this->filterAutocompleteResults($value, $search);
                    if($isCheck === true){
                        $cls = DB::table("psic_group")->where("code", $value->code)->first();
                        if($cls->row_id && $cls->row_id) { 
                            if(!in_array($cls->code, $row)){
                                $row[] = $cls->code;
                            }
                        }
                    }
                }
            }

            // $rows = Industryrow::whereIn("industry_row_id", $row)->get()->toArray();
            $rows = DB::table('tblindustry_class')->whereIn('group_code', $row)->get()->toArray();
            $group_code = $row;

            foreach($group_code as $group){
                // Get the value of each group code
                $grc = DB::table('tblindustry_group')->where('group_code', $group)->first();

                // get the group code main code
                $sc = DB::table('tblindustry_sub')->where('sub_code', $grc->sub_code)->first();
                $mc = DB::table('tblindustry_main')->where('main_code', $sc->main_code)->first();

                $format[$mc->main_title][$sc->sub_title][]= $grc;

                $rowStr .= "<option value='" . $grc->group_code . "' > " . $grc->group_description . "</option>";
            }

            $str = "";
            foreach ($format as $key => $main) {
                $str .= "<ul>";
                $str .= "<li class='main_industry'> <span> <b> " . $key . "</b> </span>";
                $str .= "<ul>";
                foreach ($main as $key2 => $sub) {
                    $str .= "<li> <span> <b>-</b> <i> " . $key2 . " </i></span> ";
                    if(!empty($sub)) {
                        $str .= "<ul>";
                    }
                    foreach ($sub as $key => $row) {
                        $row = (object) $row;
                        $str .= "<li> <input type='radio' name='row_title' value='" . $row->group_code . "'> " . $row->group_description . "</input> </li>";    
                        
                    }
                    if(!empty($sub)) {
                        $str .= "</ul>";
                    }
                    $str .= "</li>";
                }
                $str .= "</ul>";
                $str .= "</li>";
                $str .= "</ul>";
            }
            
        }

        $data = array(
            "success"=>true,
            "data"=>$format,
            "html"=> $str,
            "main" => $mainStr,
            "sub" => $subStr,
            "row" => $rowStr
        );

        
        return $data;
    }

    public function processSearch(){
        $id = Input::get('industry');
        if($id) {
            // $row = DB::table('tblindustry_class')->where('class_code', $id)->first();
            $sub = DB::table('tblindustry_group')->where('group_code', $id)->pluck('sub_code')->first();
            $main = Industrysub::where('sub_code', $sub)->pluck('main_code')->first();
            return array("success"=>true, "row" => $id, "sub" => $sub, "main" => $main);
        } else {
            return array("success" => false);
        }
       
    }

    public function filterAutocompleteResults($data, $search){
        $isKeyWordPresent = false;

        // Check if property exists for name
        if(property_exists($data,"name")){
            $word = array();
            $word = explode(' ', $data->name);
            foreach($word as $w){
                if(strtolower($w) === strtolower($search)){
                    $isKeyWordPresent = true;
                }
            }
        }


        // Check if description property exists
        if(property_exists($data,"description")){
            $word = array();
            $word = explode(' ', $data->description);
            foreach($word as $w){
                if(strtolower($w) === strtolower($search)){
                    $isKeyWordPresent = true;
                }
            }
        }

        return $isKeyWordPresent;
    }

}
