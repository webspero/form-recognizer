<?php
use \Exception as Exception;
//======================================================================
//  Class 28: Investigator Controller
//      Functions related to Credit Investigator account functionalities
//      are routed to this class
//======================================================================
class InvestigatorController extends BaseController {
	
    //-----------------------------------------------------
    //  Function 28.1: getGroup
    //      Get Investigator tags by Group
    //-----------------------------------------------------
	public function getGroup()
	{
        /*  Request came via AJAX   */
		if (Request::ajax())
		{
			$return = array();
            
            /*  Get Investigator tag by group   */
			$entity = DB::table('investigator_tag')
				->where('entity_id', Input::get('entity_id'))
				->where('group', Input::get('group'))
				->get();
            
            /*  Store the value of each tag for output  */
			foreach($entity as $key=>$value){
				$return[$value->item] = $value;
			}
            
			return Response::json($return);
		}
	}
	
    //-----------------------------------------------------
    //  Function 28.2: postInvestigatorData
    //      Receives the HTTP Post call when a
    //      CI marks a questionnaire item
    //-----------------------------------------------------
	public function postInvestigatorData()
	{
        /*  Request came via AJAX   */
		if (Request::ajax())
		{
            /*  Delete an existing incomplete record of the item    */
            $item_data  = InvestigatorTag::where('entity_id', Input::get('entity_id'))
                ->where('group', Input::get('group'))
                ->where('item', Input::get('item'))
                ->where('status', 'incomplete')
                ->delete();
            
            /*  Get the first record of the item if existing, create a new one if not   */
			$investigator_tag = InvestigatorTag::firstOrCreate(array(
				'entity_id'     => Input::get('entity_id'),
				'group'         => Input::get('group'),
				'group_name'    => Input::get('group_name'),
				'label'         => Input::get('label'),
				'value'         => Input::get('value'),
				'item'          => Input::get('item')
			));
            
			$investigator_tag->status = Input::get('status');
            
            /*  Investigator tagged the item as incomplete  */
			if(Input::get('status') == 'incomplete'){
                
                /*  Investigator Notes and Uploaded files   */
				$investigator_tag->notes    = Input::get('notes');
				$files                      = Input::file('support_docs');
				$filenames                  = array();
                
                /** Investigator uploaded a file    */
				if ($files) {
                    
					foreach ($files as $file_handler) {
						if ($file_handler) {
							/** Uploaded file does not yet exist    */
							if (!in_array($file_handler->getClientOriginalName(), $filenames)) {
                                
                                /*  Configure file for upload   */
								$filenames[]        = $file_handler->getClientOriginalName();
								$destinationPath    = 'cidocuments'; 
								$extension          = $file_handler->getClientOriginalExtension();
								$filename           = rand(11111,99999).time().'.'.$extension;
                                
                                /*  Uploads the file to the server  */
								$upload_success     = $file_handler->move($destinationPath, $filename);
                                
                                /*  Upload successful   */
								if ($upload_success) {
                                    /*  Saves file  information to the database */
									InvestigatorFiles::create(array(
										'investigator_tag_id'   => $investigator_tag->id,
										'old_filename'          => $file_handler->getClientOriginalName(),
										'filename'              => $filename
									));
								}
							}
						}
					}
				}
			}
            /*  Investigator tagged the item as complete    */
            else {
				$investigator_tag->notes = '';
			}
            
            /*  Saves item investigation to the database    */
			$investigator_tag->save();
			
            /*  Response with a JSON    */
            return Response::json($investigator_tag);
		}
	}
	
    //-----------------------------------------------------
    //  Function 28.3: getAllFailed
    //      Fetches all incomplete items
    //-----------------------------------------------------
	public function getAllFailed()
	{
        /*  Request came via AJAX   */
		if (Request::ajax()) {
            /*  Query the database for incomplete items */
			$entity = DB::table('investigator_tag')
				->where('entity_id', Input::get('entity_id'))
				->where('status', 'incomplete')
				->orderBy('group', 'ASC')
				->get();
            
            /*  Sets the output data    */
			foreach ($entity as $key=>$value) {
                /*  Gets the first 20 characters of the notes in an item    */
				$entity[$key]->blurb = substr($value->notes, 0, 20);
                
                /*  Gets the files related to the item  */
				$entity[$key]->files = InvestigatorFiles::where('investigator_tag_id', $value->id)->get();
			}
            
            /*  Output response in JSON format  */
			return Response::json($entity);
		}
	}
	
    //-----------------------------------------------------
    //  Function 28.4: postSaveInvestigation
    //      HTTP POST request to save Investigation item
    //-----------------------------------------------------
	public function postSaveInvestigation()
	{
        /*  Variable Declaration    */
		$requirement    = Input::get('required_docs');  // CI Checked required documents
		$docs_list      = array();                      // Document list for output
        
        /*  When there is a required document checked by the CI */
		if (is_array($requirement) && @count($requirement) > 0) {
            /*  Loops through all required documents
            *   Converts the numerical value of the document into
            *   it's corresponding text description and store it
            *   to the output variable
            *   
            */
			foreach ($requirement as $r) {
				switch($r) {
					case 1:
						$docs_list[] = 'SEC: Certificate of Registration';
						break;
					case 2:
						$docs_list[] = 'SEC: Articles';
						break;
					case 3:
						$docs_list[] = 'SEC: By-laws';
						break;
					case 4:
						$docs_list[] = 'SEC: General Information Sheet';
						break;
					case 5:
						$docs_list[] = 'BIR: Certificate of Registration documents';
						break;
					case 6:
						$docs_list[] = "Updated Mayor's permit";
						break;
					case 7:
						$docs_list[] = 'Certifications, Licenses';
						break;
					case 8:
						$docs_list[] = 'Transfer Certificates of Title (for real property)';
						break;
					case 9:
						$docs_list[] = 'Chattel ownership documents';
						break;
					case 10:
						$docs_list[] = Input::get('investigator_others_docs');
						break;
					case 11:
						$docs_list[] = 'DTI: Registration of Business Name';
						break;
					case 12:
						$docs_list[] = 'Authority to borrow';
						break;
					case 13:
						$docs_list[] = 'Authorized Signatory';
						break;
					case 14:
						$docs_list[] = 'Bank Statements';
						break;
					case 15:
						$docs_list[] = 'Utility Bills';
						break;
					case 16:
						$docs_list[] = 'BIR: Income Tax Return';
						break;
				}
			}
		}
		
		/* Update the investigation record to the database or create a new record   */
		$investigator_notes                 = InvestigatorNotes::firstOrNew(array('entity_id'=>Input::get('entity_id')));
        $investigator_notes->notes          = Input::get('notes');
        $investigator_notes->others_docs    = Input::get('investigator_others_docs');
        
        /*  There are needed documents  */
		if (is_array($requirement) && @count($requirement) > 0) {
            /*  Converts the required docs array to a string
            *   and saves the result to the database
            */
			$investigator_notes->required_docs = implode(',', $requirement);
		}
        /*  No required documents       */
        else {
			$investigator_notes->required_docs = '';
		}
        
		$investigator_notes->save();
		
        /*  When CI choose to send to email */
		if (Input::get('iv_action')=='send_to_email') {
            
            /*  Fetches the report and user information from the database   */
			$entity     = Entity::where('entityid', Input::get('entity_id'))->first();
			$user       = User::where('loginid', $entity->loginid)->first();
            
			/*  Sends the email to the user */
			if (env("APP_ENV") != "local"){

				try{
					Mail::send('emails.requirements', array('docs'=>$docs_list,'company_name'=>$entity->companyname), function($message) use ($user){
						$message->to($user->email, $user->email)
							->subject('Credit investigation requirements from CreditBPO');
						
						$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
						$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
					});

				}catch(Exception $e){
	            //
	            }
			}
            
			return Redirect::to('/sme/confirmation/tab1')->with('success', 'Email has been sent to SME. <a href="../../summary/smesummary/'.Input::get('entity_id').'">Please click here to return</a>');
		}
		
        /*  When CI choose to submit investigation  */
		if (Input::get('iv_action') == 'submit_investigation') {
			$investigator_notes->status = 1;
			$investigator_notes->save();
		}
		
		return Redirect::to('/sme/confirmation/tab1')->with('success', 'Your notes have been saved. <a href="../../summary/smesummary/'.Input::get('entity_id').'">Please click here to return</a>');
	}
}