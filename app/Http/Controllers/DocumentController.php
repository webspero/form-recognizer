<?php

use CreditBPO\Services\ComplexFsInput;
use CreditBPO\Services\SimpleFsInput;
use CreditBPO\Services\FsInput;
use CreditBPO\Services\QuickbooksInput;
use CreditBPO\Services\AssetGrouping;
use Maatwebsite\Excel\Facades\Excel;
use CreditBPO\Models\Document;
use CreditBPO\Handlers\UploadErrorHandler;
use CreditBPO\Models\UploadErrors;
use \Exception as Exception;
//======================================================================
//  Class 20: DocumentController
//  Description: Contains functions for document uploads including FS template
//======================================================================
class DocumentController extends BaseController {
    const FS_INPUT_COMPLEX = 'files/FS Input Template - Complex Output.xlsx';

	//-----------------------------------------------------
    //  Function 20.1: downloadFile
	//  Description: function to create stream file for downloading
    //-----------------------------------------------------
	public function downloadFile($path, $filename){

		if(Auth::check()){		// check if user is logged in

            $download = $path . '/' . $filename;	// get path

			if(file_exists($download)){
				return Response::download($download, $filename);	// stream for download
			} else {
				App::abort(403, 'File not found.');
			}
		}

	}

    //-----------------------------------------------------
    //  Function 20.2: downloadEntityFile
    //  Description:function to create stream file for downloading
    //-----------------------------------------------------
    public function downloadEntityFile($path, $entity, $filename){

        if(Auth::check()){      // check if user is logged in

            $download = $path.'/'.$entity.'/'.$filename;    // get path

            if(file_exists($download)){
                return Response::download($download, $filename);    // stream for download
            } else {
                App::abort(403, 'File not found.');
            }
        }

    }

    //-----------------------------------------------------
    //  Function 20.2: downloadEntityFile
    //  Description:function to create stream file for downloading
    //-----------------------------------------------------
    public function downloadChecklistFile($entity, $filename){

        if(Auth::check()){      // check if user is logged in

            $download = public_path("documents/checklist/{$entity}/{$filename}");   // get path

            if(file_exists($download)){
                return Response::download($download, $filename);    // stream for download
            } else {
                App::abort(403, 'File not found.');
            }
        }

    }

    //-----------------------------------------------------
    //  Function 20.2: downloadQuickbooksFile
    //  Description:function to create stream file for downloading
    //-----------------------------------------------------
    public function downloadQuickbooksFile($entity, $filename){

        if(Auth::check()){      // check if user is logged in

            $download = public_path("documents/quickbooks/{$filename}");   // get path

            if(file_exists($download)){
                return Response::download($download, $filename);    // stream for download
            } else {
                App::abort(403, 'File not found.');
            }
        }

    }

	//-----------------------------------------------------
    //  Function 20.3: getUploadRequestedDocument
	//  Description: display Requested Document upload screen
    //-----------------------------------------------------
	public function getUploadRequestedDocument()
	{
		return View::make('sme.uploadrequested');
	}

	//-----------------------------------------------------
    //  Function 20.4: postUploadRequestedDocument
	//  Description: post process for Requested Document saving
    //-----------------------------------------------------
	public function postUploadRequestedDocument()
	{
		$arraydata = Input::file('file');									// get input file

		foreach ($arraydata as $key=>$value) {

			if($value){
      			$destinationPath = 'cidocuments'; 							// set directory
      			$extension = $value->getClientOriginalExtension();			// get extension name
      			$filename = rand(11111,99999).time().'.'.$extension;		// generate random filename

				$data = array(												// populate data
					'entity_id'=>Session::get('entity')->entityid,
					'document_type'=>0,
					'document_group'=>41,
					'document_orig'=>$value->getClientOriginalName(),
					'document_rename'=>$filename,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s')
				);
				DB::table('tbldocument')->insert($data);					// save data

				$upload_success = $value->move($destinationPath, $filename);	// save file to server

			}

		}

		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 20.5: getUploadFsTemplate
	//  Description: get form for FS template upload
    //-----------------------------------------------------
    public function getUploadFsTemplate($entity_id)
	{
        $fs_template = DB::table('tbldocument')
            ->where('entity_id', '=', $entity_id)
            ->where('document_group', '=', 21)
            ->where('upload_type', '=', 5)
            ->orderBy('document_type', 'ASC')
            ->count();
        $entity = Entity::where('entityid', $entity_id)->first();
        $currency = Currency::orderBy('iso_code', 'asc')->get();
        $currencyList = [];

        foreach ($currency as $item) {
            $currencyList[$item->iso_code] = $item->iso_code.' - '.$item->name;
        }

		return View::make('sme.upload-fstemplate', [
			'entity_id'     => $entity_id,
			'fs_template'   => $fs_template,
            'entity' => $entity,
            'currency' => $currencyList,
		]);
    }

    public function getUploadQuickbooks($entity_id)
	{
		$quickbooks = DB::table('tbldocument')
            ->where('entity_id', '=', $entity_id)
            ->where('document_group', '=', 45)
            ->where('upload_type', '=', 5)
            ->orderBy('document_type', 'ASC')
            ->count();
        $entity = Entity::where('entityid', $entity_id)->first();
        $currency = Currency::orderBy('iso_code', 'asc')->get();
        $currencyList = [];

        foreach ($currency as $item) {
            $currencyList[$item->iso_code] = $item->iso_code.' - '.$item->name;
        }

		return View::make('sme.upload-quickbooks', [
			'entity_id'     => $entity_id,
			'quickbooks'   => $quickbooks,
            'entity' => $entity,
            'currency' => $currencyList,
		]);
    }
    
    public function getUploadFsTemplatePremium($entity_id)
	{
        $fs_template = DB::table('tbldocument')
            ->where('entity_id', '=', $entity_id)
            ->where('document_group', '=', 21)
            ->where('upload_type', '=', 5)
            ->where('is_deleted', 0)
            ->orderBy('document_type', 'ASC')
            ->count();
        $entity = Entity::where('entityid', $entity_id)->first();
        $currency = Currency::orderBy('iso_code', 'asc')->get();
        $currencyList = [];

        foreach ($currency as $item) {
            $currencyList[$item->iso_code] = $item->iso_code.' - '.$item->name;
        }

        if(empty($fs_template)) {
            return View::make('sme.upload-fstemplatePremium', [
                'entity_id'     => $entity_id,
                'fs_template'   => $fs_template,
                'entity' => $entity,
                'currency' => $currencyList,
            ]);    
        } else {
            $company_name = $this->getCellStringValue('B2');

            /** Check special character on company name */
            if( (preg_match('/[\'^£$%&*@~?><>|_+¬-]/', $company_name))){
                throw new InvalidCellException(
                    $this->getTranslatedMessage('fs_input.special_character_found_company_name',
                    ['column' => 'B2'])
                );
            }
            /* check input fields with special characters */
			$sheetFields = array ('B', 'C', 'D', 'E', 'H', 'I', 'J');
			foreach($sheetFields as $sheets){
				if($sheets == 'B' || $sheets == 'C' || $sheets == 'D' || $sheets == 'E'){
					for($count=10; $count<=75; $count++){
						if(($count>19&&$count<22) || ($count>36&&$count<38)  || ($count>38&&$count<43) || ($count>49&&$count<50) || ($count>50&&$count<53) || ($count>60&&$count<63) ||  ($count>70&&$count<72) || ($count>72&&$count<74) || $count>74 ){
							continue;
						}
						$field = $sheets.$count;

						if(is_string($this->getCellCalculatedValue($field, false))){
							throw new InvalidCellException(
								$this->getTranslatedMessage('fs_input.input_numeric',
								['column' => $field])
							);
						}
					}
				}
				if($sheets == 'H' || $sheets == 'I' || $sheets == 'J'){
					for($count=8; $count<=55; $count++){
						if( ($count<8&&$count<10) || ($count>10&&$count<12) || ($count>17&&$count<19) || ($count>30&&$count<32) || ($count>33&&$count<35) || ($count>36&&$count<38) || ($count>39&&$count<43) || $count>53 ){
							continue;
						}
						$field = $sheets.$count;
						if(is_string($this->getCellCalculatedValue($field, false))){
							throw new InvalidCellException(
								$this->getTranslatedMessage('fs_input.input_numeric',
								['column' => $field])
							);
						}
					}
				}
			}

            return Redirect::to('/summary/smesummary/'.$entity_id);
        }
	}

	//-----------------------------------------------------
    //  Function 20.6: getUploadDocuments
	//  Description: get form for BS + IS + CF
    //-----------------------------------------------------
	public function getUploadDocuments($entity_id)
	{
        $fs_template = DB::table('tbldocument')
            ->where('entity_id', '=', $entity_id)
            ->where('document_group', '=', 21)
            ->where('upload_type', '=', 5)
            ->orderBy('document_type', 'ASC')
            ->count();

		return View::make('sme.upload_documents', array(
			'entity_id'     => $entity_id,
			'fs_template'   => $fs_template
		));
	}

	//-----------------------------------------------------
    //  Function 20.7: postUploadDocuments
	//  Description: post process for saving documents uploaded
    //-----------------------------------------------------
	public function postUploadDocuments($entity_id)
	{

		try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $arraydata          = Input::file('fs_file');			// get input file
	        $as_of              = Input::get('as_of');				// get input data
			$period_from        = Input::get('period_from');		// get input data
			$period_to          = Input::get('period_to');			// get input data
			$document_type      = Input::get('document_type');		// get input data
	        $upload_type        = Input::get('upload_type');		// get input data

	        $unit = Input::get('unit');
	        $currency = Input::get('currency');
	        $industryId = Input::get('industry_id');

	        $messages   = array();
	        $rules      = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::where('entityid', $entity_id)->first();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            if($entity->is_premium == PREMIUM_REPORT){
	                $srv_resp['action']         = REFRESH_PAGE_ACT;
	            }else{
	                $srv_resp['action']         = REFRESH_PAGE_ACT;
	            }
	            
	            $srv_resp['refresh_cntr']   = '#financials-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-financials';

	            if (isset($upload_type[0])) {
	                if ('5' == $upload_type[0]) {
	                    $srv_resp['refresh_cntr']   = '#fs-template-list-cntr';
	                    $srv_resp['refresh_elems']  = '.reload-fs-template';
	                }
	            }
	        }

	        
	        /*--------------------------------------------------------------------
	        /*	Checks if the User is allowed to update the company
	        /*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        foreach($arraydata as $key=>$value) {
	            if ( ($value) || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['fs_file.'.$key] = 'required|mimes:xls,xlsx,csv,pdf,doc,docx,jpeg,png';
	                $messages['fs_file.'.$key.'.required'] = "File is required.";
	                $messages['fs_file.'.$key.'.mimes'] = "Invalid Format.";

	                switch ($upload_type[$key]) {
	                    case 1:
	                        $rules['as_of.'.$key] = 'required|date_format:m/d/Y';
	                        $messages['as_of.'.$key.'.required'] = "Date is required";
	                        $messages['as_of.'.$key.'.date_format'] = "Invalid Date Format";
	                        break;

	                    case 5:
	                        $rules['fs_file.'.$key] = 'required';
	                        break;

	                    default:
	                        $rules['period_from.'.$key] = 'required|date_format:m/d/Y';
	                        $rules['period_to.'.$key] = 'required|date_format:m/d/Y';
	                        $messages['period_from.'.$key.'.required'] = "Period from is required";
	                        $messages['period_to.'.$key.'.required'] = "Period to is required";
	                        $messages['period_from.'.$key.'.date_format'] = "Invalid Date Format";
	                        $messages['period_to.'.$key.'.date_format'] = "Invalid Date Format";
	                        break;
	                }
	            }
	        }
	        

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        // additional validation to check date range validity
	        foreach($arraydata as $key=>$value){

	        	if(!isset($period_to[$key])) continue;
	        	
	            if(strtotime($period_from[$key]) > strtotime($period_to[$key])){
	                $srv_resp['messages'] = ['Period from should be less than Period to'];
	                return json_encode($srv_resp);
	            }
	        }


	        if ($validator->fails()) {	// return error message if fail
	            $srv_resp['messages']	= $validator->messages()->all();
	            return json_encode($srv_resp);

	            if (isset(Auth::user()->loginid)) {
	                if (isset($_COOKIE['formdata_support'])) {
	                	return json_encode($srv_resp);
	                    if (STS_NG == $_COOKIE['formdata_support']) {
	                        return Redirect::to('/sme/upload_documents/'.$entity_id)->withErrors($validator)->withInput();
	                    }
	                }
	            }
	        }
	        else {
	            $reportFileFormat = 'Raw FS %s as of %s.%s';

	            // zip file upload batch
	            $destinationPath = './documents/'; 			// get directory
	            $zip = new ZipArchive();					// create new instance of zip file
	            $zip_filename = str_replace(' ', '_', $entity_id.' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip'); 		// set filename
	            if ($zip->open($destinationPath.$zip_filename, ZipArchive::CREATE)!==TRUE) {		// attempt open zip file
	                exit("cannot open <$zip_filename>\n");
	            }

	            foreach ($arraydata as $key=>$value) {

	                if($value){  // select which type of document was uploaded
	                    $extension = $value->getClientOriginalExtension();
	                    switch($upload_type[$key]){
	                        case 1:
	                            $document_group = 21;
	                            $document_orig = sprintf(
	                                $reportFileFormat,
	                                'Balance Sheet',
	                                str_replace('/', '-', $as_of[$key]),
	                                $extension
	                            );
	                        break;
	                        case 2:
	                            $document_group = 21;
	                            $document_orig = sprintf(
	                                $reportFileFormat,
	                                'Income Statement',
	                                str_replace('/', '-', $period_from[$key]) .
	                                    ' to ' . str_replace('/', '-', $period_to[$key]),
	                                $extension
	                            );
	                        break;
	                        case 3:
	                            $document_group = 21;
	                            $document_orig = sprintf(
	                                $reportFileFormat,
	                                'Balance Sheet, Income Statement and Cashflow',
	                                str_replace('/', '-', $period_from[$key]) .
	                                    ' to ' . str_replace('/', '-', $period_to[$key]),
	                                $extension
	                            );
	                        break;
	                        case 4:
	                            $document_group = 21;
	                            $document_orig = sprintf(
	                                $reportFileFormat,
	                                'Cashflow Statement',
	                                str_replace('/', '-', $period_from[$key]) .
	                                    ' to ' . str_replace('/', '-', $period_to[$key]),
	                                $extension
	                            );
	                        break;
	                        case 5:
	                            $document_group = 21;
	                            $document_orig = $entity_id.' - FS Input Template for the period of ';
	                        break;
	                    }

	                    //process if template
	                    $filename = rand(11111,99999).time().'.'.$extension;
	                    $value->move('temp', $filename);

	                    // init excel reader
	                    if($extension == 'xlsx' || $extension == "xls"){
	                    	$excel = Excel::load('temp/'.$filename);
	                    	$sheet = $excel->getActiveSheet();
	                    }else{
	                    	$srv_resp['messages'] = ['Incorrect filename extension, should be xlsx.'];
	                		return json_encode($srv_resp);	                    	
	                    }

	                    // ---------------------------------------
	                    // process if file uploaded is FS Template
	                    // ---------------------------------------
	                    if ($upload_type[$key] == 5 && in_array($extension, ['xlsx', 'xls'])) {
							$industryId = Industrymain::where('main_code', $industryId[$key])->pluck('industry_main_id')->first();
	                        $simpleFsData = [
	                            'entity_id' => $entity_id,
	                            'money_factor' => $unit[$key],
	                            'currency' => $currency[$key],
	                            'industry_id' => $industryId,
								'country_code'	=> $entity->country_code
	                        ];						

	                        $simpleFsInput = new SimpleFsInput('temp/'.$filename, $simpleFsData);
	                        $result = $simpleFsInput->save();

	                        if (!$result) {
	                        	$srv_resp['messages'] = $simpleFsInput->getErrorMessages();
	                            return json_encode($srv_resp);
	                        } else {
	                            $complexFsInput = new ComplexFsInput(
	                                public_path(self::FS_INPUT_COMPLEX),
	                                ['entity_id' => $entity_id]
	                            );
	                            $complexFsInput->setDocumentGenerated(
	                                $simpleFsInput->getDocumentGenerated()
	                            );
	                            $result = $complexFsInput->write($simpleFsInput->getCellValues());

	                            if ($result) {
									// Verify FS Template entry data (atleast 3 years data)
									$simpleFsInput->verifyYearOfUploadedFS();
									if(!empty($simpleFsInput->getErrorMessages())){
										$srv_resp['messages'] = $simpleFsInput->getErrorMessages();
	                            		return json_encode($srv_resp);
									}

	                                $document_orig = $simpleFsInput->getDocumentFileName();
	                                $file = $complexFsInput->getDocumentFileName();
	                                $assets = $simpleFsInput->getTotalAssets();
	                                $srv_resp['total_asset'] = number_format($assets, 2);
	                                $srv_resp['total_asset_grouping'] = AssetGrouping::getByAmount($assets);
	                                if($document_group == 21) {
	                                    $cond = array(
	                                        "is_deleted" => 0,
	                                        "entity_id" => $entity_id,
	                                        "document_group" => $document_group
	                                    );
	                                    $curDocs = Document::where($cond)->get();
	                                    foreach ($curDocs as $key => $value) {
	                                        $value->is_deleted = 1;
	                                        $value->save;
	                                        Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
	                                    }
	                                }
	                                /* create entry on database */
	                                $data = [
	                                    'entity_id' => $entity_id,
	                                    'document_type'=> $document_type[$key],
	                                    'upload_type'=> $upload_type[$key],
	                                    'document_group'=> $document_group,
	                                    'document_orig'=> $file,
	                                    'document_rename'=> $zip_filename,
	                                    'created_at'=> date('Y-m-d H:i:s'),
	                                    'updated_at'=> date('Y-m-d H:i:s'),
	                                ];
	                                $documentid = DB::table('tbldocument')->insertGetId($data);

	                                $master_ids[]       = $documentid;
	                                $filenames[]        = $file;
	                                $display_names[]    = $file;
	                                $where = array(
	                                    "entity_id" => $entity_id,
	                                    "is_deleted" => 0
	                                );
	                                $fs = FinancialReport::where($where)->first();
	                                if($fs->count() >= 1) {
	                                    $srv_resp['fsid'] = $fs->id;
	                                }

	                                $zip->addFile(public_path('temp/'.$file), $file);
	                                $srv_resp['sts'] = STS_OK;
	                            }
	                        }
	                    } elseif($upload_type[$key] == 5 && $extension != 'xlsx'){

	                        $srv_resp['messages'] = ['The file you were trying to upload is not a Financial Statement Template.  Please download the file on the link provided.'];
	                        return json_encode($srv_resp);
	                        break;
	                    } else {

	                        $srv_resp['sts']    = STS_OK;
	                    }



	                    if($srv_resp['sts'] == STS_OK){
	                        // create entry on database
	                        $data = array(
	                            'entity_id'=>$entity_id,
	                            'document_type'=>$document_type[$key],
	                            'upload_type'=>$upload_type[$key],
	                            'document_group'=>$document_group,
	                            'document_orig'=>$document_orig,
	                            'document_rename'=>$zip_filename,
	                            'created_at'=>date('Y-m-d H:i:s'),
	                            'updated_at'=>date('Y-m-d H:i:s')
	                        );
	                        $documentid = DB::table('tbldocument')->insertGetId($data);

	                        $master_ids[]       = $documentid;
	                        $filenames[]        = $document_orig;
	                        $display_names[]    = $document_orig;

	                        $zip->addFile('temp/'.$filename, $document_orig); // add the upload file to zip file
	                    }
	                }
	            }

	            $zip->close(); // close the zip file

	            if ($srv_resp['sts'] == STS_OK) {
	                $srv_resp['messages']   = 'Successfully Added Record';
	                return json_encode($srv_resp);
	            }
	        }


	        if (!isset(Auth::user()->loginid)) {
	            $srv_resp['fields']['filename']         = $filenames;
	            $srv_resp['fields']['display_names']    = $display_names;
	            $srv_resp['master_ids']                 = $master_ids;
	        }

	        if (isset($_COOKIE['formdata_support'])) {
	            if (STS_NG == $_COOKIE['formdata_support']) {
	            	$srv_resp['messages']   = 'Cookie not supported';
	            	return json_encode($srv_resp);
	                return Redirect::to('/summary/smesummary/'.$entity_id);
	            }
	        }

	        if($entity->is_premium == PREMIUM_REPORT){
	            $srv_resp['is_premium']    = true;
	            $questionnaire = QuestionnaireLink::where('entity_id', $entity_id)->first();
	            
	            if($entity->is_paid == 1) {
	                $srv_resp['refresh_url'] = URL::to('premium-link/' . $questionnaire->url);
	                
	            }
	        }

	        ActivityLog::saveLog();				// log activity to database
			return json_encode($srv_resp);		// return json results
		}
		catch(Exception $ex){

			$uploadError = [
        		'user_type' => Auth::user()->role,
        		'loginid' => Auth::user()->loginid,
        		'entityid' => $entity_id,
        		'error_details' => $ex->getMessage(),
        		'date_uploading' => date('Y-m-d H:i:s')
        	];

        	UploadErrorHandler::uploadErrorsUpdate($uploadError);

			if(env("APP_ENV") != "Production"){
				return $ex;        	
        	}
        	
        }
	}

	public function postUploadQuickbooks($entity_id){
		$srv_resp = [];
		$srv_resp['sts']            = STS_NG;               // default status
	    $srv_resp['messages']       = STR_EMPTY;            // variable container
	    $sts    = STS_NG;
		$atleast = 3;

		 /** Initialize Server Response  */
		 if (isset(Auth::user()->loginid)) {
			$srv_resp['action']         = REFRESH_PAGE_ACT;
			   
			$srv_resp['refresh_cntr']   = '#financials-list-cntr';
			$srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
			$srv_resp['refresh_elems']  = '.reload-financials';
		 }

		$rules = [
			'qb_balance_sheet' => 'required|mimes:xls,xlsx',
			'qb_profit_loss' => 'required|mimes:xls,xlsx',
			'qb_cash_flows' => 'required|mimes:xls,xlsx'
		];

		$messages = [
			'qb_balance_sheet.required' => 'Balance Sheet is required.',
			'qb_balance_sheet.mimes' => 'Invalid Balance Sheet file format',
			'qb_profit_loss.required' => 'Profit and Loss is required.',
			'qb_profit_loss.mimes' => 'Invalid Profit and Loss file format',
			'qb_cash_flows.required' => 'Statement of Cash Flows is required',
			'qb_cash_flows.mimes' => 'Invalid Statement of Cash Flows file format'
		];

		$validator = Validator::make(Input::all(), $rules, $messages); // run validation

		$srv_resp = [];
        if ($validator->fails()) {	// return error message if fail
            $srv_resp['messages']	= $validator->messages()->all();

            if (isset(Auth::user()->loginid)) {
                if (isset($_COOKIE['formdata_support'])) {
                    if (STS_NG == $_COOKIE['formdata_support']) {
                    	return json_encode($srv_resp);
                        return Redirect::to('/sme/upload_quickbooks/'.$entity_id)->withErrors($validator)->withInput();
                    }
                }
            }
        }

        $documentPath = public_path('documents');
        $quickbooksPath = public_path('documents/quickbooks');
        if(!file_exists($quickbooksPath)){
        	mkdir($quickbooksPath,0777,true);
        }
        
        $destinationPath = $quickbooksPath.'/'.$entity_id.'/';

        if(!file_exists($destinationPath)){
        	mkdir($destinationPath,0777,true);
        }

        $balance_sheet_filename = 'Quickbooks_Balance_Sheet_'. date('Y-m-d').'.xlsx';
        $profit_loss_filename = 'Quickbooks_Profit_and_Loss_'. date('Y-m-d').'.xlsx';
        $cash_flows_filename = 'Quickbooks_Statement_of_Cash_Flows_'. date('Y-m-d').'.xlsx';

        $qb_balance_sheet_file       = Input::file('qb_balance_sheet');
        $qb_profit_loss_file         = Input::file('qb_profit_loss');
        $qb_cash_flows_file          = Input::file('qb_cash_flows');

        $qb_balance_sheet_file->move($destinationPath, $balance_sheet_filename);
        $qb_profit_loss_file->move($destinationPath, $profit_loss_filename);
        $qb_cash_flows_file->move($destinationPath, $cash_flows_filename);

        $filenames = [
        	'balance_sheets' => $balance_sheet_filename,
        	'profit_loss' => $profit_loss_filename,
        	'cash_flows' => $cash_flows_filename
        ];

		$zip = new ZipArchive();
		$filename = 'Quickbooks_'.$entity_id.'.zip';
		$zipFilename = $quickbooksPath.'/Quickbooks_'.$entity_id.'.zip';
		$res = $zip->open($zipFilename, ZipArchive::CREATE | ZipArchive::OVERWRITE);


		if ($res === TRUE) {
		    foreach (glob($destinationPath . '*') as $file) {
		        $zip->addFile($file, basename($file));
		    }
			$zip->close();

			$documents = array(
	            'entity_id'         => $entity_id,
	            'document_type'     => 1,
	            'document_group'    => 45,
	            'document_orig'     => $filename,
	            'document_rename'   => $filename,
	            'created_at'        => date('Y-m-d H:i:s'),
	            'updated_at'        => date('Y-m-d H:i:s')
	        );

	        DB::table('tbldocument')->where('entity_id',$entity_id)->update(['is_deleted' => 0]);
	        $documentid     = DB::table('tbldocument')->insertGetId($documents);

		}else {
			$srv_resp['sts'] = STS_NG;
            $srv_resp['messages']   = ['Failed to create to quickbooks zip file.'];
            return json_encode($srv_resp);
		}


        $balance_sheet_path = $destinationPath.$balance_sheet_filename;
        $balance_sheet_data = Excel::load($balance_sheet_path)->noHeading()->all()->toArray();

        $income_statement_path = $destinationPath.$profit_loss_filename;
        $income_statement_data = Excel::load($income_statement_path)->noHeading()->all()->toArray();

        $cash_flows_path = $destinationPath.$cash_flows_filename;
        $cash_flows_data = Excel::load($cash_flows_path)->noHeading()->all()->toArray();

		$quickbookData = array(
			'balance_sheet_path'	=> $balance_sheet_path,
			'income_statement_path'	=> $income_statement_path,
			'cashflow_path'	=> $cash_flows_path
		);

        $unit = Input::get('unit');
	    $currency = Input::get('currency');
	    $industryId = Input::get('industry_id');
        $entity = Entity::where('entityid',$entity_id)->first();
		$document_type = Input::get('document_type');

        $entityData = [
        	'entity' => $entity,
        	'unit' => $unit[0],
        	'currency' => $currency[0],
        	'industry_id' => $industryId[0],
			'document_type'	=> $document_type[0],
			'country_code'	=> $entity->country_code
        ];

		// Verify Uploaded QuickBook files
		$quickbook = new QuickBooksInput($quickbooksPath.'/Quickbooks_'.$entity_id.'.zip', $entityData);
		$result = $quickbook->verifyQuickBookFile($quickbookData);

		if(!$result){
			$srv_resp['sts'] = STS_NG;
            $srv_resp['messages']   = $quickbook->getErrorMessages();
            return json_encode($srv_resp);
		}

		// Upload converted FS Input Template
		$basicPath = $quickbooksPath.'/'.$entity_id.'/'. $entity_id . ' - Basic FS Input Template.xlsx';
		
		// zip file upload batch
        $destinationPath = public_path().'/documents/'; 			// get directory

		/** Create new instance of zip file */
		$zip = new ZipArchive();

		/** Set File Name */
		$zip_filename = str_replace(' ', '_', $entity_id.' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip');

		/** Attempt to open Zip File */
		if ($zip->open($destinationPath.$zip_filename, ZipArchive::CREATE)!==TRUE) {
			throw new Exception("cannot open <$zip_filename>\n");
		}

		$extension = pathinfo($basicPath, PATHINFO_EXTENSION);
		$document_group = 21;
		$document_orig = $entity_id.' - FS Input Template for the period of ';

		$filename = rand(11111,99999).time().'.'.$extension;
        \File::copy($basicPath, base_path('temp/'.$filename));

		// ---------------------------------------
		// Process uploaded FS Template
		// ---------------------------------------

		$simpleFsData = [
        	'entity_id' => $entity_id,
        	'money_factor' => $unit[0],
        	'currency' => $currency[0],
        	'industry_id' => $industryId[0],
			'country_code'	=> $entity->country_code
        ];

		$simpleFsInput = new SimpleFsInput('temp/'.$filename, $simpleFsData);
		$result = $simpleFsInput->save();

		if (!$result) {
			$error = array(
				'status' => STS_NG,
				'error'  => $simpleFsInput->getErrorMessages()
			);
			$srv_resp['sts'] = STS_NG;
            $srv_resp['messages']   = $simpleFsInput->getErrorMessages();
            return json_encode($srv_resp);
		}else {
			$complexFsInput = new ComplexFsInput(
				public_path(self::FS_INPUT_COMPLEX),
				['entity_id' => $entity_id]
			);
			$complexFsInput->setDocumentGenerated(
				$simpleFsInput->getDocumentGenerated()
			);
			$result = $complexFsInput->write($simpleFsInput->getCellValues());
			if ($result) {

				$simpleFsInput->verifyYearOfUploadedFS();
				if(!empty($simpleFsInput->getErrorMessages())){
					$error = array(
						'status' => STS_NG,
						'error'  => $simpleFsInput->getErrorMessages()
					);
					$srv_resp['sts'] = STS_NG;
					$srv_resp['messages']   = $simpleFsInput->getErrorMessages();
					return json_encode($srv_resp);
				}

				$document_orig = $simpleFsInput->getDocumentFileName();
				$document_group = 21;
				$file = $complexFsInput->getDocumentFileName();
				$assets = $simpleFsInput->getTotalAssets();
				$srv_resp['total_asset'] = number_format($assets, 2);
				$srv_resp['total_asset_grouping'] = AssetGrouping::getByAmount($assets);
				
				if($document_group == 21) {
					$cond = array(
						"is_deleted" => 0,
						"entity_id" => $entity_id,
						"document_group" => $document_group
					);
					$curDocs = Document::where($cond)->get();
					foreach ($curDocs as $key => $value) {
						$value->is_deleted = 1;
						$value->save;
						Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
					}
				}

				/* create entry on database */
				$data = [
					'entity_id' => $entity_id,
					'document_type'=> $document_type[0],
					'upload_type'=> 5,
					'document_group'=> $document_group,
					'document_orig'=> $file,
					'document_rename'=> $zip_filename,
					'created_at'=> date('Y-m-d H:i:s'),
					'updated_at'=> date('Y-m-d H:i:s'),
				];

				$documentid = Document::insertGetId($data);
				$master_ids[]       = $documentid;
				$filenames[]        = $file;
				$display_names[]    = $file;
				$where = array(
					"entity_id" => $entity_id,
					"is_deleted" => 0
				);
				$fs = FinancialReport::where($where)->first();
				if($fs->count() >= 1) {
					$srv_resp['fsid'] = $fs->id;
				}
				$zip->addFile(public_path('temp/'.$file), $file);
				$srv_resp['sts'] = STS_OK;
			}
		}

		if($srv_resp['sts'] == STS_OK){
			/** create entry on database */
			$data = array(
				'entity_id'=>$entity_id,
				'document_type'=>$document_type[0],
				'upload_type'=>5,
				'document_group'=>$document_group,
				'document_orig'=>$document_orig,
				'document_rename'=>$zip_filename,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s')
			);
			$documentid = DB::table('tbldocument')->insertGetId($data);

			$master_ids[]       = $documentid;
			$filenames[]        = $document_orig;
			$display_names[]    = $document_orig;
			$zip->addFile($basicPath, $document_orig); // full add the upload file to zip file
		}
		
		/** Close the zip file */
		$zip->close();

        $srv_resp['sts'] = STS_OK;
        $srv_resp['messages']   = ['Successfull quickbook upload.'];
        return json_encode($srv_resp);

	}

	public function getDeleteQuickbooks($id){
		DB::table('tbldocument')->where('documentid', $id)->update(['is_deleted' => 0]);
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
	}

	//-----------------------------------------------------
    //  Function 20.8: getDeleteDocument
	//  Description: function for deleting uploaded documents
    //-----------------------------------------------------
	public function getDeleteDocument($id)
	{
		// get document details
		$document = DB::table('tbldocument')->where('documentid', $id)->where('entity_id', Session::get('entity')->entityid)->first();
		if($document){

			$destinationPath = public_path('documents').'/';
			$zip = new ZipArchive();
			// open the zip file containing the document
			if ($zip->open($destinationPath.$document->document_rename)==TRUE) {
				if($zip->locateName($document->document_orig)){
					$zip->deleteName($document->document_orig); // delete in zip file
				}
			}
			$zip->close(); // close zip file
			// update and save
            //DB::table('tbldocument')->where('documentid', $id)->where('entity_id', Session::get('entity')->entityid)->delete();
            $document = Document::where('entity_id', '=', Session::get('entity')->entityid)->first();
            $document->is_deleted = 1;
            $document->save();
        }
        $fs_template = DB::table('tbldocument')
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->where('document_group', '=', 21)
                    ->where('upload_type', '=', 5)
                    ->where('is_deleted', 0)
                    ->orderBy('document_type', 'ASC')
                    ->get();

        if(count($fs_template) === 0){
            $entity = Entity::where('entityid', Session::get('entity')->entityid)->first();
            $entity->total_assets = 0.00;
            $entity->total_asset_grouping = 0;
            $entity->save();
        }

		// Delete if QuickBook File is present
		Document::where('document_group', '=', 45)->where('entity_id', '=', Session::get('entity')->entityid)->update(['is_deleted' => 1]);

        ActivityLog::saveLog();		// log activity in db
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
    }
    
    //-----------------------------------------------------
    //  Function 20.9: getOtherDocuments
    //  Description: function to get form for other documents
    //-----------------------------------------------------
    public function getOtherDocuments($entity_id)
	{
        $docs_db    = new BankCustomDocs;
        $entity     = Entity::find($entity_id);

        $custom_docs    = $docs_db->getDocumentsByBank($entity->current_bank);

		return View::make('sme.other-documents', array(
			'entity_id'     => $entity_id,
			'custom_docs'   => $custom_docs
		));
	}

    //-----------------------------------------------------
    //  Function 20.10: postOtherDocuments
    //  Description: post process for saving other documents
    //-----------------------------------------------------
    public function postOtherDocuments($entity_id)
	{
		/*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $input_lbl      = Input::get('doc_lbl');
        $input_files    = Input::file('other_document');
        $messages       = array();
        $rules          = array();

        $filenames      = array();
        $display_names  = array();
        $master_ids     = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if (isset(Auth::user()->loginid)) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#other-documents-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-other-documents';
        }

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

        /*--------------------------------------------------------------------
		/*	Sets rules and messages
		/*------------------------------------------------------------------*/
		foreach (Input::file('other_document') as $key => $value) {
			if (($value)
            || (0 == $key)) {
				$rules['doc_lbl.'.$key]         = 'required';
				$rules['other_document.'.$key]  = 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt';

				$messages['doc_lbl.'.$key.'.required']          = trans('validation.required', array('attribute'=>'Document Label'));
				$messages['other_document.'.$key.'.required']   = trans('validation.required', array('attribute'=>'File Upload'));
				$messages['other_document.'.$key.'.mimes']      = "File ".($key+1)." should be a PDF, Excel, CSV, Zip or Rar file.";
			}
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

        /*--------------------------------------------------------------------
		/*	Some inputs are invalid
		/*------------------------------------------------------------------*/
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();

            if (isset($_COOKIE['formdata_support'])) {
                if (STS_NG == $_COOKIE['formdata_support']) {
                    return Redirect::to('/sme/upload-bank-statements/'.$entity_id)->withErrors($validator)->withInput();
                }
            }
		}
        /*--------------------------------------------------------------------
		/*	All inputs are valid
		/*------------------------------------------------------------------*/
		else {
            /*---------------------------------------------------------------
            /*	Saves the inputs
            /*-------------------------------------------------------------*/
            foreach ($input_files as $key => $value) {

                if ($value) {
                    /** Sets the filename and destination dir    */
                    $destinationPath    = 'documents';
                    $extension          = $value->getClientOriginalExtension();
                    $filename           = 'other-docs-'.rand(111,999).$entity_id.time().'.'.$extension;

                    /** Saves information to the database    */
                    $data = array(
                        'entity_id'         => $entity_id,
                        'document_type'     => 7,
                        'document_group'    => 71,
                        'document_orig'     => $input_lbl[$key],
                        'document_rename'   => $filename,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    $documentid     = DB::table('tbldocument')->insertGetId($data);

                    $master_ids[]       = $documentid;
                    $filenames[]        = $filename;
                    $display_names[]    = $input_lbl[$key];

					// background process check
					if(env("APP_ENV") != "local"){
                        /** Upload file to the server    */
                        $upload_success = $value->move($destinationPath, $filename);
					}
                    else {
                        /** Upload file to the server    */
                        $upload_success = $value->move($destinationPath, $filename);
                    }
                }
            }

            $srv_resp['sts']        = STS_OK;
            $srv_resp['messages']   = 'Successfully Added Record';

            if (!isset(Auth::user()->loginid)) {
                $srv_resp['fields']['filename']         = $filenames;
                $srv_resp['fields']['display_names']    = $display_names;
                $srv_resp['master_ids']                 = $master_ids;
            }
        }

        if (isset($_COOKIE['formdata_support'])) {
            if (STS_NG == $_COOKIE['formdata_support']) {
                return Redirect::to('/summary/smesummary/'.$entity_id);
            }
        }

        if($srv_resp['sts'] == STS_NG && $srv_resp['messages'] == STR_EMPTY){
			$srv_resp['messages'] = ['Select a file to upload'];
		}

        ActivityLog::saveLog();

        return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 20.11: getDeleteOtherDocs
    //  Description: function for deleting uploaded other documents
    //-----------------------------------------------------
    public function getDeleteOtherDocs($doc_id)
	{
        $destinationPath    = './documents/';
		$document           = DB::table('tbldocument')->where('documentid', $doc_id)->where('entity_id', Session::get('entity')->entityid)->first();

        if ($document) {

			File::delete($destinationPath.$document->document_rename);
			DB::table('tbldocument')->where('documentid', $doc_id)->where('entity_id', Session::get('entity')->entityid)->delete();
		}

        ActivityLog::saveLog();
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

    //-----------------------------------------------------
    //  Function 20.11: downloadFSTemplate
	//  Description: function to create stream file for downloading fs template
    //-----------------------------------------------------
    public function downloadFSTemplate() {
        $file = public_path('files').'/FS Input Template - Simple Input.xls';
        $headers = [
            'Content-Type: application/vnd.ms-excel',
        ];

        return Response::download($file, 'Financial Statement Template.xls', $headers);
    }

    //-----------------------------------------------------
    //  Function 20.12: postSaveDocument
	//  Description: function to save history for contractor document review
    //-----------------------------------------------------
    public function postSaveDocument() {
        $post = Input::post();
        $user = Auth::user()->loginid;
       
        $data = array(
            "data" => implode(",",$post['doc_status']),
            "reviewer" => $user,
            "entity_id" => $post['entity_id'],
            'is_deleted' => 0
        );
        $history = ContractorDocumentHistory::where("entity_id", $post['entity_id'])->where("is_deleted", 0)->where("reviewer", $user)->first();
        if($history && $history->count() > 0) {
            $history->data = implode(",",$post['doc_status']);
            $history->save();
        } else {
            ContractorDocumentHistory::insert($data);
        }

        return array("success"=>true);
    }

    //-----------------------------------------------------
    //  Function 20.12: postSubmitDocument
	//  Description: function to submit contractor document review
    //-----------------------------------------------------
    public function postSubmitDocument() {
        $post = Input::post();
        $denied = 0;

        //get all approve and deny code
        foreach ($post['doc_status'] as $key => $value) {
            //  status 2 for approved and 1 for denied
            $code = "";
            if(strpos($value, "_approve") !== false) {
                $status = 2;
                $code= str_replace("_approve","", $value );
            } else {
                $status = 1;
                $code= str_replace("_deny","", $value );
                $denied += 1;
            }
            
            ChecklistFile::leftJoin("checklist_type as ct","type_id", 'ct.id')
                                ->where('entity_id', $post['entity_id'])
                                ->where('ct.code', $code)
                                ->update([
                                    'status' => $status,
                                    'reviewed_by' => Auth::user()->loginid
                                ]);
        }
        
        $status = 1;
        if($denied > 0) {
            // send email to user
            $entity = User::leftJoin('tblentity as ent', 'tbllogin.loginid', 'ent.loginid')
                        ->where('entityid', $post['entity_id'])->first();
            $user_email = $entity->email;

            $denied_documents = ChecklistFile::where('entity_id', $post['entity_id'])
                                        ->where('reviewed_by', Auth::user()->loginid)
                                        ->where('status', 1)
                                        ->get();
            
            try{
                Mail::send('emails.notifications.contractor_docu_denied', array(
                    'entity' => $entity,
                    'denied_documents' => $denied_documents,
                ), function ($message) use ($user_email, $entity){
                    $message->to($user_email, $user_email)
                    ->subject('CreditBPO: Document denied for company '.$entity->companyname.'.');
                });
            }catch(Exception $e){
            //
            }

            $status = 0;
        } else {
            $done = ChecklistFile::where("entity_id", $post['entity_id'])->whereIn('status',array(0,1))->get();
            if($done && $done->count() == 0) {
                Entity::where("entityid", $post['entity_id'])->update(array("status" => 5));
            }
        }

        // insert to verified documents 
        $insert = array(
            "entity_id" => $post['entity_id'],
            "status" => $status,
            "reviewed_by" => Auth::user()->loginid
        );
        ContractorVerifiedDocument::insert($insert);
        $where = array(
            "reviewer" => Auth::user()->loginid,
            "entity_id" => $post['entity_id']
        );
        $history = ContractorDocumentHistory::where("entity_id", $post['entity_id'])->where("is_deleted", 0)->where("reviewer", Auth::user()->loginid)->first();
        if($history && $history->count() > 0) {
            $history->is_deleted = 1;
            $history->save();
        }
        // redirect to complete tab and show submit message
        return array("success" => true, "refresh_url"=> "/contractor/confirmation");
    }
    
    //-----------------------------------------------------
    //  Function 20.13: getContractorConfirmation
	//  Description: function to get contractor confirmation
    //-----------------------------------------------------
    public function getContractorConfirmation() {
        return view("sme.contractor_confirmation");
    }

    //-----------------------------------------------------
    //  Function 20.14: postSubmitDocument
	//  Description: Autopopulate FS Template by user company name
    //-----------------------------------------------------
    public function getFsTemplateFile($entityid){
        $user = Entity::select('companyname')->where('entityid', $entityid)->first();
        $company = $user->companyname;
        $companyname = preg_replace('/\s+/', '_', $company);

        if(strpos($company,"&") !== false){
            $company  = preg_replace("/[&]/", "And", $company);
        }

        $sourceFilePath=public_path("documents/FSInputTemplate.xls");
        $destinationPath=public_path("documents/fs/" . $companyname . ".xls");
        $success = \File::copy($sourceFilePath,$destinationPath);

        $file_path = public_path("documents/fs/" . $companyname . ".xls");
        Excel::load($file_path, function($doc) use ($company) {
            $sheet = $doc->getActiveSheet(0);
            $sheet->setCellValue('B2', $company);
        })->store('xlsx', public_path('documents/fs'));

        File::delete($file_path);
        $file_name = public_path("documents/fs/{$companyname}.xlsx");
        
        return response()->download($file_name, "FS Input Template.xls")->deleteFileAfterSend(true);;
    }
}
