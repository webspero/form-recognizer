<?php

use CreditBPO\Models\ReportMatchingBank;
use CreditBPO\Models\ReportMatchingSME;

//======================================================================
//  Class 18: DashboardController
//  Description: Contains functions for index page (root page)
//======================================================================
class DashboardController extends BaseController {

	//-----------------------------------------------------
    //  Function 18.1: getDashboardIndex
	//  Description: gets the index page
    //-----------------------------------------------------
	public function getDashboardIndex()
	{
		
		// if(env("APP_ENV") != "Production"){
		// 	$entityIDs = DB::table('tblentity')->select('entityid')->where('is_deleted',0)->orderBy('created_at', 'desc')->groupBy('entityid')->limit(20)->get()->toArray();
		// 	$ids = [];
		// 	foreach ($entityIDs as $k => $v) {
		// 		$ids[] = $v->entityid;
		// 	}

		// 	$ids = implode(',', $ids);
		// 	Entity::whereRaw("entityid NOT IN (".$ids.") AND is_deleted = 0")->update(['is_deleted' => 1]);

		// }

		/* -----------------
		|  Check session for question pass flag and redirects to
		|  Questionnaire if present
		-------------------*/
		if(Session::has('tql')) {
			$code = Session::get('tql');
			$ql = QuestionnaireLink::where('url', $code)->first();
			return Redirect::to('/summary/smesummary/'.$ql->entity_id);
		}

		if(session('rm_notify') != 1)
			session(['rm_notify' => '']);
		// ----------------------------------------------------
		//  User logged in is SME - get owned questionnaires
		// ----------------------------------------------------
		if(Auth::user()->role == 1){
			$user = User::find(Auth::user()->loginid);

			if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name == "") && $user->trial_flag == 0) {
				
				$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
				return Redirect::to('/billingdetails')->withErrors($error)->withInput();
			}

			$entity = DB::table('tblentity')
			->select("tblentity.*","refcitymun.*","tblindustry_main.*","tblindustry_sub.*","tblindustry_class.*", "tbllogin.role","tbllogin.trial_flag","questionnaire_links.url")
			->leftjoin("tbllogin", "tbllogin.loginid", "=", "tblentity.loginid")
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
			->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
			->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
			->leftJoin('questionnaire_links', 'tblentity.entityid', '=', 'questionnaire_links.entity_id')
			->where('tblentity.loginid', '=', Auth::user()->loginid)
			->where('tblentity.is_deleted', '=', 0)
			->where('tblentity.is_cleanedup', '=', 0)
            ->orderBy('tblentity.entityid', 'desc')
			->groupBy('tblentity.entityid')
			->get();

			// Hide Finish Report for Basic User
			$reports = array();
			foreach($entity as $report){
				if($report->role == 1 && $report->status == 7 && $report->trial_flag !=1){
				}else{
					$reports[] = $report;
				}
			}
			//print_r($rm_bank); exit;
			return View::make('dashboard.index')
			->with(['entity' => $reports]);

		// ----------------------------------------------------
		//  User logged in is investigator - get SME list
		// ----------------------------------------------------
		}elseif(Auth::user()->role == 2){
			$entity = DB::table('tblentity')
			->select(
				'tblindustry_main.gross_revenue_growth', 
				'tblindustry_main.net_income_growth', 
				'tblindustry_main.gross_profit_margin', 
				'tblindustry_main.net_profit_margin', 
				'tblindustry_main.net_cash_margin',
				'tblindustry_main.current_ratio', 
				'tblindustry_main.debt_equity_ratio', 
				'tblentity.entityid', 
				'tblentity.companyname', 
				'tblentity.email', 
				'tblentity.date_established', 
				'tblentity.number_year',
				'tblindustry_row.row_title', 
				'refcitymun.citymunDesc', 
				'tblentity.status as status', 
				'tblentity.completed_date', 
				'tblentity.anonymous_report',
				'investigator_tag.id as ci_started',
				'investigator_notes.status as ci_status'
			)
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->leftJoin('investigator_tag', 'investigator_tag.entity_id', '=', 'tblentity.entityid')
			->leftJoin('investigator_notes', 'investigator_notes.entity_id', '=', 'tblentity.entityid')
			->where('tblentity.status', '=', 7)
			->groupBy('tblentity.entityid');

			if(Auth::user()->bank_id != 0){
				if(Auth::user()->can_view_free_sme==1){
					$entity->where(function($query){
						$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$entity->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
				}
			}

			$entity = $entity->get();

			foreach($entity as $report){
				$notes_count = $report->ci_status;
				$tag_count = $report->ci_started;

				if($notes_count && $notes_count > 0){
					$report->ci_verification = '<span style="color:blue;">Verified</span>';
				}else if($tag_count){
					$report->ci_verification = '<span style="color:green;">In Review</span>';
				}else{
					$report->ci_verification = '<span style="color:red;">Awaiting Assignment</span>';
				}
			} 


			return View::make('dashboard.index')->with(['entity' => $entity]);

		// ----------------------------------------------------
		//  User logged in is analyst - get SME list
		// ----------------------------------------------------
		}elseif(Auth::user()->role == 3){
			$entity = DB::table('tblentity')
			->select('tblindustry_main.gross_revenue_growth', 'tblindustry_main.net_income_growth', 'tblindustry_main.gross_profit_margin', 'tblindustry_main.net_profit_margin', 
					 'tblindustry_main.debt_equity_ratio', 'tblindustry_main.net_cash_margin', 'tblindustry_main.current_ratio', 'tblentity.entityid', 'tblentity.companyname', 
					 'tblentity.email', 'tblentity.date_established', 'tblentity.number_year', 'tblindustry_class.class_description', 'refcitymun.citymunDesc', 'tblentity.status', 
					 'tblentity.submitted_at', 'tblentity.updated_at', 'tblentity.anonymous_report' )
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
			->join('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
			->where('tblentity.status', '=', 5)
			->groupBy('tblentity.entityid')
			->get();

			foreach($entity as $ent){
				if($ent->submitted_at == ''){
					$ent->submitted_at = $ent->updated_at;
				}
			}
			
			return View::make('dashboard.index')->with(['entity' => $entity]);

		// ----------------------------------------------------
		//  User logged in is supervisor
		// ----------------------------------------------------
		}elseif(Auth::user()->role == 4){
			return Redirect::to('bank/dashboard'); // redirect to supervisor page
		}elseif(Auth::user()->role == 5){
			$responsibilities = explode(",", Auth::user()->responsibilities);
			if($responsibilities) {
				$entity = DB::table('tblentity')
				->select('tblindustry_main.gross_revenue_growth', 'tblindustry_main.net_income_growth', 'tblindustry_main.gross_profit_margin', 'tblindustry_main.net_profit_margin', 
						'tblindustry_main.debt_equity_ratio', 'tblindustry_main.net_cash_margin', 'tblindustry_main.current_ratio', 'tblentity.entityid', 'tblentity.companyname', 
						'tblentity.email', 'tblentity.date_established', 'tblentity.number_year', 'tblindustry_row.row_title', 'refcitymun.citymunDesc', 'tblentity.status', 
						'tblentity.submitted_at', 'tblentity.loginid', 'tblentity.updated_at', 'tblentity.anonymous_report', 'cf.status as cf_status' )
				->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
				->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
				->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
				->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')

				// get documents by user responsibilities
				->join('checklist_files as cf', 'tblentity.entityid', "cf.entity_id")
				->join('checklist_type as ct', 'ct.id', "cf.type_id")
				->where('cf.reviewed_by', 0)
				->where('cf.is_deleted', 0)
				->where('cf.status', 0)
				->whereIn('ct.group', $responsibilities)
				->where('tblentity.status',  8)
				->where('tblentity.current_bank', Auth::user()->bank_id)
				->groupBy('tblentity.entityid')
				->get();
				
				foreach ($entity as $key => $val) {
					# code...
					$submitted_by = User::select("name")->where("loginid", $val->loginid)->first();
					if($submitted_by) {
						$val->submitted_by = $submitted_by->name;
					}
					if($val->cf_status == 0) {
						$val->status_desc = "Pending";
					}
				}
			}
			return View::make('dashboard.index')->with(['entity' => $entity]);
		} 
	}

	//-----------------------------------------------------
    //  Function 18.1.1: getDashboardIndex
	//  Description: gets the index page
    //-----------------------------------------------------
	public function getDashboardError(){
		return View::make('dashboard.error');
	}

	//-----------------------------------------------------
    //  Function 18.2: getDashboardSmeBucket
	//  Description: gets the Analyst SME Bucket list
    //-----------------------------------------------------
	public function getDashboardSmeBucket()
	{
		if(Auth::user()->role == 2){
			$entity = DB::table('tblinvestigation')
			->join('tblentity', 'tblinvestigation.entityid', '=', 'tblentity.entityid')
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->where('tblinvestigation.loginid', '=', Auth::user()->loginid)
			->where('tblentity.status', '!=', 1)
			->groupBy('tblentity.entityid')
			->get();
			return View::make('dashboard.smebucket')->with('entity', $entity);
		}elseif(Auth::user()->role == 3){
			$entity = DB::table('tblscoring')
			->select(
				'tblentity.entityid',
				'tblentity.companyname',
				'tblentity.anonymous_report',
				'tblentity.email',
				'tblentity.date_established',
				'tblentity.number_year',
				'tblindustry_row.row_title',
				'zipcodes.city',
				'tblentity.status',
				'tbllogin.firstname',
				'tbllogin.lastname',
				'tblscoring.created_at'
			)
			->join('tblentity', 'tblscoring.entityid', '=', 'tblentity.entityid')
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->join('tbllogin', 'tblscoring.loginid', '=', 'tbllogin.loginid')
			->where('tblscoring.loginid', '=', Auth::user()->loginid)
			->where('tblentity.status', '=', 6)
			->groupBy('tblentity.entityid')
			->get();
			return View::make('dashboard.smebucket')->with('entity', $entity);
		}
	}

	//-----------------------------------------------------
    //  Function 18.3: getDashboardSmeScored
	//  Description: gets the Analyst Finish Scored SME list
    //-----------------------------------------------------
	public function getDashboardSmeScored()
	{
		if(Auth::user()->role == 3){
			$entity = DB::table('tblsmescore')
			->select(
				'tblentity.entityid',
				'tblentity.companyname',
				'tblentity.email',
				'tblentity.date_established',
				'tblentity.number_year',
				'tblindustry_row.row_title',
				'zipcodes.city',
				'tblentity.status',
				'tbllogin.firstname',
				'tbllogin.lastname',
				'tblsmescore.score_sf',
				'tblscoring.created_at as start_date',
				'tblsmescore.created_at as end_date'
			)
			->join('tblentity', 'tblsmescore.entityid', '=', 'tblentity.entityid')
			->join('tblscoring', 'tblsmescore.entityid', '=', 'tblscoring.entityid')
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->join('tbllogin', 'tblsmescore.loginid', '=', 'tbllogin.loginid')
			->where('tblsmescore.loginid', '=', Auth::user()->loginid)
			->where('tblentity.status', '=', 7)
			->groupBy('tblentity.entityid')
			->get();
			if($entity!=null){
				foreach($entity as $key=>$value){
					$score = intval($value->score_sf);
					if($score >= 177){
						$entity[$key]->score_sf		= "AA - Excellent";
						$entity[$key]->rating_color	= "green";
					}
					elseif($score >= 150 && $score <= 176){
						$entity[$key]->score_sf		= "A - Strong";
						$entity[$key]->rating_color	= "green";
					}
					elseif($score >= 123 && $score <= 149){
						$entity[$key]->score_sf		= "BBB - Good";
						$entity[$key]->rating_color	= "orange";
					}
					elseif($score >= 96 && $score <= 122){
						$entity[$key]->score_sf		= "BB - Satisfactory";
						$entity[$key]->rating_color	= "orange";
					}
					elseif($score >= 68 && $score <= 95){
						$entity[$key]->score_sf		= "B - Some Vulnerability";
						$entity[$key]->rating_color	= "red";
					}
					elseif($score < 68){
						$entity[$key]->score_sf		= "CCC - Watchlist";
						$entity[$key]->rating_color	= "red";
					}
					$span = strtotime($value->end_date) - strtotime($value->start_date);
					$days = floor($span / (60*60*24));
					$hours = floor(($span % (60*60*24)) / (60*60));
					$minutes = floor(($span % (60*60)) / 60);
					$seconds = floor($span % 60);
					$entity[$key]->process_time =  $days.'d '.$hours.'h '.$minutes.'m '.$seconds.'s';
				}
			}

			return View::make('dashboard.smescored')->with('entity', $entity);
		}
	}

	//-----------------------------------------------------
    //  Function 18.4: getUpgradetoFull
	//  Description: function to update SME trial flag and convert to full version
    //-----------------------------------------------------
	public function getUpgradetoFull()
	{

		$user = User::find(Auth::user()->loginid);

		if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name == "")) {
			
			$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
			return Redirect::to('/billingdetails')->withErrors($error)->withInput();
		}

		$user = User::find(Auth::user()->loginid);
		$user->trial_flag = 0;
		$user->save();

		return Redirect::to('/logout');
	}

	//-----------------------------------------------------
    //  Function 18.5: getBillingDetails
	//  Description: Display the Billing Details Form Page
    //-----------------------------------------------------
	public function getBillingDetails(){
		$user = User::find(Auth::user()->loginid);

		return View::make('dashboard.billing-details')->with('user', $user);
	}

	//-----------------------------------------------------
    //  Function 18.6: postBillingDetails
	//  Description: Function to get billing details info
    //-----------------------------------------------------
	public function postBillingDetails(){
		$messages = array(  							// validation message
			'street'  => 'Please add your street details',
			'city' => 'Please add your city details',
			'province' => 'Please add your province details',
			'zipcode' => 'Please add your zipcode details',
			'phone' => 'Please add your phone number details'
        );

		$rules = array(									// validation rules
			'street'	=> 'required',
			'city'	=> 'required',
			'province'	=> 'required',
			'zipcode'	=> 'required',
			'phone'	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			return Redirect::to('/billingdetails')->withErrors($validator->errors())->withInput();
		}
		else
		{
			$user = User::find(Auth::user()->loginid);

			if(!isset($user->name) || $user->name == null){
				$user->name = $user->firstname . " " . $user->lastname;
			}

			$user->street_address = Input::get('street');
			$user->city = Input::get('city');
			$user->province = Input::get('province');
			$user->zipcode = Input::get('zipcode');
			$user->phone = Input::get('phone');
			$user->save();

			return Redirect::to('/dashboard/index');
		}
	}


	//-----------------------------------------------------
    //  Function 18.7: getDashboardApproved
	//  Description: gets the PCAB approved reports
    //-----------------------------------------------------
	public function getDashboardApproved()
	{
		# Get approved report here!
		$responsibilities = explode(",", Auth::user()->responsibilities);
		$entity = Entity::leftjoin("contractor_verified_documents as c", "entityid", "c.entity_id")->where("reviewed_by", Auth::user()->loginid)->orderBy("id", "DESC")->get();
		return View::make('dashboard.document_approved')->with('entity', $entity);
	}

}
