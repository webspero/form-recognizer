<?php

use CreditBPO\Handlers\UploadErrorHandler;
use CreditBPO\Models\UploadErrors;

use \Exception as Exception;
class FileController extends BaseController
{
    
    //-----------------------------------------------------
    //  Function 20.5: getAssocFilesUpload
	//  Description: get form for FS associated files upload
    //-----------------------------------------------------
    public function getAssocFilesUpload($entity_id)
	{
		return View::make('sme.upload-assocfiles', [
			'entity_id'     => $entity_id,
		]);
    }

    //-----------------------------------------------------
    //  Function 20.5: getAssocFilesUpload
	//  Description: get form for FS associated files upload
    //-----------------------------------------------------
    public function getFspdfUpload($entity_id)
	{
		return View::make('sme.upload-fspdf', [
			'entity_id'     => $entity_id,
		]);
    }

    public function postUploadAssocFiles($entity_id)
	{	
		try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $arraydata          = Input::file('dropbox_file');			// get input file

	        $messages   = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::where('entityid', $entity_id)->first();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#dropbox-upload-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-dropbox-upload';
	        }

	        /*--------------------------------------------------------------------
	        /*	Checks if the User is allowed to update the company
	        /*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        foreach($arraydata as $key => $value) {
	            if (($value)
	            || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['dropbox_file.'.$key] = 'required';
	            	$rules['dropbox_file.'.$key] = 'mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff';
	                $messages['dropbox_file.'.$key.'.required'] = "File is required.";
	                $messages['dropbox_file.'.$key.'.mimes'] = "Only accepts pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff files.";
	            }
	        }

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) {	// return error message if fail
	            $srv_resp['messages']	= $validator->messages()->all();
	        }
	        else {
	            foreach ($arraydata as $key=>$value) {

	                if($value){  
	                    $orig_filename = '';
	                    $fileCount = 0;
	                   
	                    $source = $value;
	                    $entityid = $entity_id;
	                    $file = fopen($source, 'r');
	                    $fileName = $value->getClientOriginalName();

	                    $destinationPath = public_path('associated_files/'.$entity_id.'/');
	                    if (!file_exists(public_path('associated_files/'))) {
	                        mkdir(public_path('associated_files/'.$entity_id.'/'));
	                    }
	                    if (!file_exists($destinationPath)) {
	                        mkdir($destinationPath,0777);
	                    }

	                    $localFileName = $value->getClientOriginalName();

	                    // save file
	                    $value->move($destinationPath, $localFileName);
	                    
	                    //get file count of deleted same file name
	                    $sameFileDeleted = DB::table('tbldropboxentity')->where('is_deleted', 1)->where('entity_id', $entity->entityid)->where('file_name', $localFileName)->first();
	                    if($sameFileDeleted){
	                        DB::table('tbldropboxentity')->where('id', $sameFileDeleted->id)->increment('is_deleted');
	                        $fileCount = $sameFileDeleted->file_count;
	                    }

	                    //if same file exist
	                    $sameFile = DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $localFileName)->first();
	                    if($sameFile){
	                        $fileCount = $sameFile->file_count;
	                        $orig_filename = $fileName;
	                        $deletedDuplicate = DB::table('tbldropboxentity')->where('entity_id', $entity->entityid)->where('orig_filename', $orig_filename)->where('is_deleted', 1)->first();
	                        if($deletedDuplicate) {
	                            DB::table('tbldropboxentity')->where('id', $deletedDuplicate->id)->increment('is_deleted');
	                            $fileName = $deletedDuplicate->file_name;
	                        } else {
	                            $fileNumber = $fileCount + 1;
	                            $fileName = pathinfo($fileName, PATHINFO_FILENAME).' ('.$fileNumber.').'.pathinfo($fileName, PATHINFO_EXTENSION);
	                        }
	                        DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $orig_filename)->increment('file_count');
	                    }

	                    $downloadLink = url('sme/download?directory=associated_files/'.$entity_id.'&filename='.basename($fileName));
	                    // create entry on database
	                    $data = [
	                        'entity_id' => $entity_id,
	                        'login_id'=> Auth::user()->loginid,
	                        'folder_url'=> $entity_id,
	                        'file_url'=> $downloadLink,
	                        'file_name'=> $fileName,
	                        'file_download'=> $downloadLink,
	                        'type'=> 'file',
	                        'orig_filename' => $orig_filename,
	                        'file_count' => $fileCount,
	                        'is_deleted' => 0,
	                        'created_at'=> date('Y-m-d H:i:s'),
	                        'updated_at'=> date('Y-m-d H:i:s'),
	                    ];
	                    DB::table('tbldropboxentity')->insertGetId($data);
	                    $srv_resp['sts'] = STS_OK;

	                }
	            }

	            if ($srv_resp['sts'] == STS_OK) {
	                $srv_resp['messages']   = 'Successfully Added Record';
	            }
	        }

	        ActivityLog::saveLog();				// log activity to database
			return json_encode($srv_resp);		// return json results

		}
		catch(Exception $ex){
        	
        	if(env("APP_ENV") == "Production"){

	        	$uploadError = [
	        		'user_type' => Auth::user()->role,
	        		'loginid' => Auth::user()->loginid,
	        		'entityid' => $entity_id,
	        		'error_details' => $ex->getMessage(),
	        		'date_uploading' => date('Y-m-d H:i:s')
	        	];

	        	UploadErrorHandler::uploadErrorsUpdate($uploadError);

	        	$srv_resp['sts']   = STS_NG;
	        	$srv_resp['messages']   = $ex->getMessage();
	        	return json_encode($srv_resp);	
        	}else{
        		throw $ex;
        	}
        	
        }
	}

    public function postUploadFspdfFiles($entity_id)
	{
        try{
        	/*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $arraydata          = Input::file('fspdf_file');			// get input file

	        $messages   = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        $entity = Entity::where('entityid', $entity_id)->first();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#fspdf-upload-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-fspdf-upload';
	        }

	        /*--------------------------------------------------------------------
	        /*	Checks if the User is allowed to update the company
	        /*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        foreach($arraydata as $key => $value) {
	            if (($value)
	            || (0 == $key)) {
	                // ---------------------------------
	                // set validation rules and messages
	                // ---------------------------------
	                $rules['fspdf_file.'.$key] = 'required|max:40000';
	            	$rules['fspdf_file.'.$key] = 'mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff';
	                $messages['fspdf_file.'.$key.'.required'] = "File is required.";
	                $messages['fspdf_file.'.$key.'.max'] = "The file you are uploading is too big.";
	                $messages['fspdf_file.'.$key.'.mimes'] = "Only accepts pdf,doc,docx,xls,xlsx,jpg,jpeg,png,tif,tiff files.";
	            }
	        }

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) {	// return error message if fail
	            $srv_resp['messages']	= $validator->messages()->all();

	            //print_r($validator->messages()->all());
	        }
	        else {
	            foreach ($arraydata as $key=>$value) {

	                if($value){  

	                    //create innodata folder if not exist
	                    $toInnodataPath = public_path('documents/innodata/to_innodata/');
	                    $destinationPath = public_path('documents/innodata/to_innodata/'.$entity_id.'/');
	                    if (!file_exists(public_path('documents/innodata/'))) {
	                        mkdir(public_path('documents/innodata/'));
	                    }
	                    
	                    if (!file_exists($toInnodataPath)) {
	                        mkdir($toInnodataPath,0777);
	                    }

	                    if (!file_exists($destinationPath)) {
	                        mkdir($destinationPath,0777);
	                    }

	                    $localFileName = $entity_id."-".$value->getClientOriginalName();

	                    // save file
	                    $value->move($destinationPath, $localFileName);

	                    //creat filenames folder
	                    // $filenameTxtPath = $destinationPath.'/FILENAMES/';
	                    // if (!file_exists($filenameTxtPath)) {
	                    //     mkdir($filenameTxtPath,0777);
	                    // }


	                    //create filename file
	                    $filenameTxtValue = $entity_id.' - '.$entity->companyname;
	                    $filenameTxtFile = fopen($destinationPath.$filenameTxtValue.'.txt','w');
	                    fwrite($filenameTxtFile, 'Instruction: Use this filename: '.$filenameTxtValue.'.xlsx');
	                    fclose($filenameTxtFile);

	                    // create database entry
	                    $innodataFile = new InnodataFile;
	                    $innodataFile->entity_id = $entity_id;
	                    $innodataFile->file_name = $localFileName;
	                    $innodataFile->save();

	                    $orig_filename = '';
	                    $fileCount = 0;
	                    $source = $destinationPath.'/'.$localFileName;
	                    $entityid = $entity_id;
	                    $file = fopen($source, 'r');
	                    $fileName = $value->getClientOriginalName();
	                    
	                    //get file count of deleted same file name
	                    $sameFileDeleted = DB::table('tbldropboxfspdf')->where('is_deleted', 1)->where('entity_id', $entity->entityid)->where('file_name', $localFileName)->first();
	                    if($sameFileDeleted){
	                        DB::table('tbldropboxfspdf')->where('id', $sameFileDeleted->id)->increment('is_deleted');
	                        $fileCount = $sameFileDeleted->file_count;
	                    }

	                    //if same file exist
	                    $sameFile = DB::table('tbldropboxfspdf')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $localFileName)->first();
	                    if($sameFile){
	                        $fileCount = $sameFile->file_count;
	                        $orig_filename = $fileName;
	                        $deletedDuplicate = DB::table('tbldropboxfspdf')->where('entity_id', $entity->entityid)->where('orig_filename', $orig_filename)->where('is_deleted', 1)->first();
	                        if($deletedDuplicate) {
	                            DB::table('tbldropboxfspdf')->where('id', $deletedDuplicate->id)->increment('is_deleted');
	                            $fileName = $deletedDuplicate->file_name;
	                        } else {
	                            $fileNumber = $fileCount + 1;
	                            $fileName = pathinfo($fileName, PATHINFO_FILENAME).' ('.$fileNumber.').'.pathinfo($fileName, PATHINFO_EXTENSION);
	                        }
	                        DB::table('tbldropboxfspdf')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('file_name', $orig_filename)->increment('file_count');
	                    }

	                    //download link
	                    $downloadUrl = url('sme/download?directory=documents/innodata/to_innodata/'.$entity_id.'/&filename='.basename($localFileName));
	                    // create entry on database
	                    $data = [
	                        'entity_id' => $entity_id,
	                        'login_id'=> Auth::user()->loginid,
	                        'file_url'=> $downloadUrl,
	                        'file_name'=> $localFileName,
	                        'file_download'=> $downloadUrl,
	                        'type'=> 'file',
	                        'orig_filename' => $orig_filename,
	                        'file_count' => $fileCount,
	                        'is_deleted' => 0,
	                        'created_at'=> date('Y-m-d H:i:s'),
	                        'updated_at'=> date('Y-m-d H:i:s'),
	                    ];
	                    DB::table('tbldropboxfspdf')->insertGetId($data);
	                    $srv_resp['sts'] = STS_OK;

	                }
	            }

	            if ($srv_resp['sts'] == STS_OK) {
	                $srv_resp['messages']   = 'Successfully Added Record';
	            }
	        }

	        ActivityLog::saveLog();				// log activity to database
			return json_encode($srv_resp);		// return json results
        }catch(Exception $ex){

        	if(env("APP_ENV") == "Production"){
	        	$uploadError = [
	        		'user_type' => Auth::user()->role,
	        		'loginid' => Auth::user()->loginid,
	        		'entityid' => $entity_id,
	        		'error_details' => $ex->getMessage(),
	        		'date_uploading' => date('Y-m-d H:i:s')
	        	];

	        	UploadErrorHandler::uploadErrorsUpdate($uploadError);
        	}else{
	            throw $ex;
	        }
        	
        }
        
	}

    //-----------------------------------------------------
    //  Function 20.8: download
    //  Description: function for downloading uploaded documents
    //-----------------------------------------------------
    public function download(){

        $directory = request('directory');
        $fileName = request('filename');
        return response()->download(public_path($directory.'/'.$fileName));
    }

	//-----------------------------------------------------
    //  Function 20.8: getDeleteDocument
	//  Description: function for deleting uploaded documents
    //-----------------------------------------------------
	public function getDeleteAssocFiles($id)
	{
		// get file details
        $file = DB::table('tbldropboxentity')->where('id', $id)->first();
        $fileName = $file->file_name;
        $entityid = Session::get('entity')->entityid;
		if($file){
           
            //update file count of orig filename
            $orig_filename = $file->orig_filename;
            DB::table('tbldropboxentity')->where('entity_id', $entityid)->where('file_name', $orig_filename)->where('is_deleted', 0)->decrement('file_count');

            // update and save
            DB::table('tbldropboxentity')->where('id', $id)->update(array('is_deleted' => 1));
        }
        
		ActivityLog::saveLog();		// log activity in db
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
    }

	//-----------------------------------------------------
    //  Function 20.8: getDeleteDocument
	//  Description: function for deleting uploaded documents
    //-----------------------------------------------------
	public function getDeleteFspdfFiles($id)
	{
        // Update innodata table
        DB::table('innodata_files')->where('id', $id)->update(['is_deleted' => 1]);
        
        
		ActivityLog::saveLog();		// log activity in db
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
    }

    public function getDownloadFspdfFiles($id){
        $fspdf = DB::table('innodata_files')->where('id', $id)->first();

        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/documents/innodata/to_innodata/".$fspdf->entity_id."/" . $fspdf->file_name;

        if(!file_exists($file)){
        	$file= public_path(). "/documents/innodata/to_innodata/" . $fspdf->file_name;
        }

        $filename = str_replace($fspdf->entity_id . '-',"", $fspdf->file_name);
        $filename = str_replace("&nbsp;","", $filename);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $filename, $headers);
    }

}