<?php

//======================================================================
//  Class 33: Logout Controller
//      Functions related to Logging-out are routed to this class
//======================================================================
class LogoutController extends BaseController {
    
    //-----------------------------------------------------
    //  Function 33.1: getLogoutIndex
    //      Resets the Session variable to logout user
    //-----------------------------------------------------
	public function getLogoutIndex()
	{
        /*  Removes Session variables related to Account    */
		Auth::logout();
		Session::flush();
        
        /*  Redirects to Login Page */
	    return Redirect::route('showLoginIndex')
			->header('Cache-Control', 'no-store, no-cache, must-revalidate')
			->header('Pragma', 'no-cache')
			->header('Expires', '0');
	}

}
