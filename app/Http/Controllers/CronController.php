<?php
use \Exception as Exception;
//======================================================================
//  Class 16: CronController
//  Description: Contains functions for cron (scheduled processes)
//======================================================================
class CronController extends BaseController {

	//-----------------------------------------------------
    //  Function 16.1: getLeadsCron
	//  Description: function that pulls email leads from creditbpo.com and mailchimp
    //-----------------------------------------------------
	public function getLeadsCron()
	{
		/*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
        $sts	= STS_NG;
		
		/*--------------------------------------------------------------------
        /*	Checks if Admin called for the script execution
		/*------------------------------------------------------------------*/
		if (isset(Auth::user()->role)) {
			if (0 == Auth::user()->role) {
				$sts	= STS_OK;
			}
		}
		/*--------------------------------------------------------------------
        /*	Check if Cron Job was executed by Server
        /*------------------------------------------------------------------*/
        $serverAddress = array_key_exists("SERVER_ADDR", $_SERVER)? $_SERVER['SERVER_ADDR']:$_SERVER['SERVER_NAME'];
		if ( $_SERVER['REMOTE_ADDR'] == $serverAddress) {
			$sts	= STS_OK;
		}

		/*--------------------------------------------------------------------
        /*	Testing purposes
		/*------------------------------------------------------------------*/
		if (ENV_PRODUCTION != ENVIRONMENT) {
			$sts	= STS_OK;
		}
		
		if (STS_OK == $sts) {
			$drupal_leads	= array();
			$mchimp_leads	= array();
			
			$last_id = EmailLead::select('last_id')->where('activity_id', 1)->where('last_id', '!=', 0)->orderBy('id', 'desc')->first();
			$last_id = ($last_id!=null) ? (int)$last_id->last_id : 0;
			
			$data = file_get_contents("https://creditbpo.com/webform-to-backend?trial=".mt_rand());
			$data = strstr($data, '<div class="item-feed">');
			$data = strstr($data, '</div></div>', true);
			$data = str_replace('<div class="item-feed"> ', '', $data);
			$data_array = explode('</div>', $data);
			$data_array = array_reverse($data_array);
			$ctr = 0;
			foreach($data_array as $d){
				$item = explode('%:', $d);
				$item[0] = trim($item[0]);
				if((int)$item[0]>$last_id){
					EmailLead::create(array(
						'email'             => trim($item[1]),
						'first_name'        => trim($item[2]),
						'last_name'         => trim($item[3]),
						'company_name'      => trim($item[4]),
						'contact_number'    => trim($item[5]),
						'job_title'         => trim($item[6]),
						'submitted_date'    => trim($item[7]),
						'last_id'           => (int)$item[0],
						'is_new'            => 1,
						'activity_id'       => 1
					));
					$ctr++;
					
					$drupal_leads[]	= trim($item[1]);
					
					echo trim($item[1]).'<br/>';
				}
			}
			
			/*--------------------------------------------------------------------
			/*	MailChimp leads
			/*------------------------------------------------------------------*/
			$existing_leads_arr = array();
			
			$url                = 'https://us11.api.mailchimp.com/2.0/lists/members.json';
			
			$data = array(
				'apikey'    => 'a1c34fd63ccd510afd7d915acb882816-us11',
				'id'        => 'cf26c07ae2',
				'opts'      => array(
					'limit' => 100
				)
			);
			
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			
			$context    = stream_context_create($options);
			$result     = file_get_contents($url, false, $context);

			/*--------------------------------------------------------------------
			/*	Saves the generated Chart to the Server
			/*------------------------------------------------------------------*/
			$json_data  = json_decode($result);
			
			$existing_leads = EmailLead::select('email')->orderBy('id', 'desc')->get();
			
			foreach ($existing_leads as $existing_lead) {
				$existing_lead_arr[] = $existing_lead->email;
			}
			
			foreach ($json_data->data as $lead) {
				
				if (FALSE == in_array($lead->email, $existing_lead_arr)) {
					EmailLead::create(array(
						'email'             => $lead->merges->EMAIL,
						'first_name'        => $lead->merges->FNAME,
						'last_name'         => $lead->merges->LNAME,
                        'contact_number'    => $lead->merges->MMERGE3,
						'submitted_date'    => $lead->timestamp_opt,
						'last_id'           => 0,
						'is_new'            => 1,
						'activity_id'       => 2,
					));
					
					$ctr++;
					$mchimp_leads[]	= $lead->merges->EMAIL;
					echo  $lead->merges->EMAIL.'<br/>';
				}
			}
			
			
			if($ctr){
				return "Processed " . $ctr . " leads.";
			} else {
				return "No new leads.";
			}
		}
		else {
            $serverAddress = array_key_exists("SERVER_ADDR", $_SERVER)? $_SERVER['SERVER_ADDR']:$_SERVER['SERVER_NAME'];
		
			return 'Access Denied. Address is: '.$_SERVER['REMOTE_ADDR'].' and '.$serverAddress;
		}
	}
	
	//-----------------------------------------------------
    //  Function 16.2: getSendLeadsCron
	//  Description: function that sends email leads periodically
    //-----------------------------------------------------
	public function getSendLeadsCron()
	{
		if(Input::has('new')){	// process and flag email leads that are tagged as new
			$leads = EmailLead::select('id', 'email', 'first_name', 'last_name', 'company_name', 'contact_number', 'job_title', 'submitted_date')
				->where('is_new', 1)->orderBy('id', 'desc')->get(); // get all new leads
			$to_email = Keys::where('name', 'leads_cron_email')->first()->value;
			if(@count($leads)>0){
				EmailLead::where('is_new', 1)->update(array('is_new' => 0)); // update status of email lead to old
			} else {
                if (env("APP_ENV") == "Production") {
                    // send email if no new leads are found

                    try{
	                    Mail::send('emails.noleads', array(), function($message) use ($to_email){
	                        $message->to($to_email, $to_email)
	                            ->subject('CreditBPO Leads');
	                    });
                    }catch(Exception $e){
		            //
		            }
                }
				
				return "No new leads";
			}
		} else { // sends email leads regardless of new or old
			if(Input::has('email') && Input::get('email')!=''){
				$leads = EmailLead::select('id', 'email', 'first_name', 'last_name', 'company_name', 'contact_number', 'job_title', 'submitted_date')
					->orderBy('id', 'desc')->get();
				$to_email = Input::get('email');
			} else {
				return "Email address is required.";
			}
		}
		
		$leads = $leads->toArray();				// convert leads to array
		$filename = 'temp/leads.csv';			// set filename
		$out = fopen($filename, 'w');			// create file for saving
		fputcsv($out, array_keys($leads[0]));	// save content to file
		foreach($leads as $line)
		{
			fputcsv($out, $line);
		}
		fclose($out);
        // send leads to email
        if (env("APP_ENV") == "Production") {

        	try{
			    Mail::send('emails.leads', array(), function($message) use ($to_email){
					$message->to($to_email, $to_email)
					->subject('CreditBPO Leads')
					->attach('temp/leads.csv');
				});
			}catch(Exception $e){
            //
            }
        }
		return "Email has been sent.";
	}
    
	//-----------------------------------------------------
    //  Function 16.3: smeLoggedReminder
	//  Description: function that sends email to SMEs that have not touched their questionnaires
    //-----------------------------------------------------
    public function smeLoggedReminder()
    {
        /*--------------------------------------------------------------------
        /*	Removes 30 secs Maximum Execution time limit
		/*------------------------------------------------------------------*/
        set_time_limit(0);
        
		/*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
        $user_cntr      = 0;
        $idle_users     = array();
        $log_type       = array();
        
        $user_1day      = array();
        $user_3day      = array();
        $user_1wik      = array();
        $user_2wik      = array();
        $user_1mon      = array();
        
        $start_1day     = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 2, date('Y')));
        $end_1day       = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 1, date('Y')));
        
        $start_3day     = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 4, date('Y')));
        $end_3day       = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 3, date('Y')));
        
        $start_1wik     = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 8, date('Y')));
        $end_1wik       = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 7, date('Y')));
        
        $start_2wik     = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 15, date('Y')));
        $end_2wik       = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 14, date('Y')));
        
        $start_1mon     = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 31, date('Y')));
        $end_1mon       = date('Y-m-d H:i:s', mktime(6, 0, 0, date('m'), (int)date('d') - 30, date('Y')));
        
        $sts	        = STS_NG;
		
		/*--------------------------------------------------------------------
        /*	Check if Cron Job was executed by Server
        /*------------------------------------------------------------------*/
        $serverAddress = array_key_exists("SERVER_ADDR", $_SERVER)? $_SERVER['SERVER_ADDR']:$_SERVER['SERVER_NAME'];
		if ( $_SERVER['REMOTE_ADDR'] == $serverAddress) {
			$sts	= STS_OK;
		}
		
		/*--------------------------------------------------------------------
        /*	Testing purposes
		/*------------------------------------------------------------------*/
		if (ENV_PRODUCTION != ENVIRONMENT) {
			$sts	= STS_OK;
		}
		
		/*--------------------------------------------------------------------
        /*	Get all users that logged in
		/*------------------------------------------------------------------*/
        /*  More than 24 hours ago  */
        $user_1day  = User::where('updated_at', '>=', $start_1day)
			->where('updated_at', '<=', $end_1day)
			->get();
            
        /*  More than 3 days ago    */
        $user_3day  = User::where('updated_at', '>=', $start_3day)
			->where('updated_at', '<=', $end_3day)
			->get();
        
        /*  More than a week        */
        $user_1wik  = User::where('updated_at', '>=', $start_1wik)
			->where('updated_at', '<=', $end_1wik)
			->get();
        
        /*  More than 2 weeks       */
        $user_2wik  = User::where('updated_at', '>=', $start_2wik)
			->where('updated_at', '<=', $end_2wik)
			->get();
        
        /*  More than a month       */
        $user_1mon  = User::where('updated_at', '>=', $start_1mon)
			->where('updated_at', '<=', $end_1mon)
			->get();
        
        foreach ($user_1day as $user) {
            $idle_users[]   = $user;
            $log_type[]     = USER_LOG_1DAY;
        }
        
        foreach ($user_3day as $user) {
            $idle_users[]   = $user;
            $log_type[]     = USER_LOG_3DAY;
        }
        
        foreach ($user_1wik as $user) {
            $idle_users[]   = $user;
            $log_type[]     = USER_LOG_1WIK;
        }
        
        foreach ($user_2wik as $user) {
            $idle_users[]   = $user;
            $log_type[]     = USER_LOG_2WIK;
        }
        
        foreach ($user_1mon as $user) {
            $idle_users[]   = $user;
            $log_type[]     = USER_LOG_1MON;
        }
        
        foreach ($idle_users as $user) {
			/*---------------------------------------------------------------
			/*	Get incomplete report of users
			/*-------------------------------------------------------------*/
            $report = Entity::where('loginid', $user->loginid)
                ->where('status', 0)
                ->first();
            
            if ($report) {
				/*----------------------------------------------------------
				/*	Send email reminder
                /*--------------------------------------------------------*/
                if (env("APP_ENV") != "local") {

                	try{
	                    Mail::send('emails.idle-report-reminder',
	                        array(
	                            'user'      => $user,
	                            'report'    => $report,
	                            'log_type'  => $log_type[$user_cntr]
	                        ),
	                        function($message) use ($user) {
	                            $message->to($user->email, $user->email)
	                                ->subject('CreditBPO Reminder');
	                        }
	                    );
	                }catch(Exception $e){
		            //
		            }
                }
                
				/*----------------------------------------------------------
				/*	Display email for testing purposes
				/*--------------------------------------------------------*/
                echo $user->email.'<br/>';
            }
            
            $user_cntr++;
        }
    }
	
	//-----------------------------------------------------
    //  Function 16.4: extractBankHtml
	//  Description: function that extracts html from BSP page
    //-----------------------------------------------------
	public function extractBankHtml()
	{
		/*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
		$url			= 'http://www.bsp.gov.ph/banking/directory.asp?paging=next&Start=0&Offset=1000&BankName=&Address=&InstitutionTypeID=&submit=Find&ctr=1&i=0';
        $html_select	= file_get_contents($url);
        echo $html_select;
	}
	
	//-----------------------------------------------------
    //  Function 16.5: loadExtractionPoint
	//  Description: gets extraction point page
    //-----------------------------------------------------
	public function loadExtractionPoint()
	{
		return View::Make('bsp.extraction_point');
	}
	
	//-----------------------------------------------------
    //  Function 16.6: updateBankData
	//  Description: function for saving new Bank data 
    //-----------------------------------------------------
	public function updateBankData()
	{
		/*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
		$bank					= Input::get('bank_data');
		$svr_resp['sts']		= STS_NG;
		$svr_resp['bank_name']	= $bank[0];
		$svr_resp['bank_ctr']	= $bank[10];
		
		$bank_exist	= Bank::where('bank_name', $bank[0])->first();
		
		if ($bank_exist) {
			
			$data_update = array(
				'contact_person'				=> $bank[2],
				'contact_person_designation'	=> $bank[3],
				'bank_street_address1'			=> $bank[4],
				'bank_no_of_offices'			=> $bank[5],
				'bank_phone'					=> $bank[6],
				'bank_fax'						=> $bank[7],
				'bank_email'					=> $bank[8],
				'bank_website'					=> $bank[9],
			);
			
			$sts = $bank_exist->update($data_update);
		}
		else {
			$add_bank								= new Bank;
			
			$add_bank->bank_name					= $bank[0];
			$add_bank->bank_type					= $bank[1];
			$add_bank->contact_person				= $bank[2];
			$add_bank->contact_person_designation	= $bank[3];
			$add_bank->bank_street_address1			= $bank[4];
			$add_bank->bank_no_of_offices			= $bank[5];
			$add_bank->bank_phone					= $bank[6];
			$add_bank->bank_fax						= $bank[7];
			$add_bank->bank_email					= $bank[8];
			$add_bank->bank_website					= $bank[9];
			
			$svr_resp['sts'] = $add_bank->save();
		}
		
		return $svr_resp;
	}
	
	//-----------------------------------------------------
    //  Function 16.7: testDisplayBankList
	//  Description: gets test bank list display page
    //-----------------------------------------------------
	public function testDisplayBankList()
	{
		$banks = Bank::get();
		
		return View::make('bsp.test-bank-list',
			array(
				'banks'	=> $banks
			)
		);
	}
    
	//-----------------------------------------------------
    //  Function 16.8: sendTrialSummary
	//  Description: sends the Trial Summary email
    //-----------------------------------------------------
    public function sendTrialSummary()
	{
        set_time_limit(0);
        
		/*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
        $sts            = STS_NG;
        $email_cntr     = 0;
        $report_cntr    = 0;
        $email          = array();
        
        $accounts   = User::where('trial_flag', CMN_ON)
            ->where('updated_at', '>=', date('Y-m-d', strtotime('-1 Days')))
            ->get();
        
        foreach ($accounts as $account) {
            $email[$email_cntr]['account'] = $account->email;
            
            $entities = Entity::where('loginid', $account->loginid)
                ->where('status', 0)
                ->where('is_paid', 1)
                ->get();
            
            foreach ($entities as $entity) {
                
                $answered   = array();
                $email[$email_cntr]['report'][$report_cntr]['company_name'] = $entity->companyname;
                
                Session::put('trial_summary', STS_OK);
                $questionnaire  = App::make('ProfileSMEController')->getSmeSummaryTbl($entity->entityid);
                
                foreach ($questionnaire as $tabs_key => $tabs_val) {
                    
                    switch ($tabs_key) {
                        case 'biz1':
                            $trans_file = 'business_details1';
                            break;
                        
                        case 'biz2':
                            $trans_file = 'business_details2';
                            break;
                        
                        case 'owner':
                            $trans_file = 'owner_details';
                            break;
                        
                        case 'ecf':
                            $trans_file = 'ecf';
                            break;
                        
                        case 'cns':
                            $trans_file = 'condition_sustainability';
                            break;
                            
                        case 'rcl':
                            $trans_file = 'rcl';
                            break;
                    }
                    
                    foreach ($questionnaire[$tabs_key] as $fields) {
                        
                        if (is_array($fields)) {
                            foreach ($fields as $key => $val) {
                                
                                if (1 >= $val) {
                                    $answered[] = trans($trans_file.'.'.$key);
                                }
                            }
                        }
                    }
                }
                
                $email[$email_cntr]['report'][$report_cntr]['answered'][] = $answered;
                $report_cntr++;
            }
            
            $email_cntr++;
        }
        
        if (0 <= @count($accounts)) {
            $receiver           = STR_EMPTY;
            $email_recipients   = Keys::where('name', 'trial_summary_emails')->first();
            
            if ($email_recipients) {
                $receiver   = explode(',', $email_recipients->value);
            }
            else {
                $receiver   = 'notify.user@creditbpo.com';
            }
            
            if (env("APP_ENV") == "Production") {
                if (is_array($receiver)) {
                    foreach ($receiver as $mail_receiver) {

                    	try{
	                        Mail::send('emails.trial_summary', array('email' => $email), function($message) use ($mail_receiver){
	                            $message->to($mail_receiver, $mail_receiver)
	                                ->subject('CreditBPO Trial Account Summary');
	                        });
                        }catch(Exception $e){
			            //
			            }
                    }
                }
                else {

                	try{
	                    Mail::send('emails.trial_summary', array('email' => $email), function($message) use ($receiver){
	                        $message->to($receiver, $receiver)
	                            ->subject('CreditBPO Trial Account Summary');
	                    });
                    }catch(Exception $e){
		            //
		            }
                }
            }
        }
    }
    
    //-----------------------------------------------------
    //  Function 16.9: getBspInterestRatePage 
	//  Description: function to download from bsp during auto update
    //-----------------------------------------------------
	public function getBspInterestRatePage()
	{
		set_time_limit(0);
		// $url       = 'http://www.bsp.gov.ph/statistics/keystat/intrates.htm';	// the url of the site to download
		$url       = 'https://www.bsp.gov.ph/Statistics/Financial%20System%20Accounts/intrates.aspx';	// the url of the site to download
				
		if(file_put_contents(public_path().'/temp/bsp_interestrate.htm', fopen($url, 'r')))
		{
			return "success";
		}	
		
		return "failed";
	}
    
    //-----------------------------------------------------
    //  Function 16.10: updateInterestRate 
	//  Description: function to process downloaded bsp for auto update
    //-----------------------------------------------------
	public function updateInterestRate()
	{
        $this->getBspInterestRatePage();
        
		// background process: run pdf to text parser on the file, convert it to plain text
		//exec('pdftotext -eol dos -layout temp/bsp_interestrate.htm temp/parsed_ir.txt');
		
		// variable declaration
		$start          = false;				// used for flagging in while loop
		//$start_data = false;		// used for flagging in while loop
		//$parsed_array = array();
		$bank_found     = STS_NG;
        $bank_records   = 0;
        
        $total_rates    = 0;
        $rates_counted  = 0;
        $ave_ir         = 0;
        
		//$handle         = fopen('http://www.bsp.gov.ph/statistics/keystat/intrates.htm', "r");
        $file_handle    = file(public_path().'/temp/bsp_interestrate.htm');
        
        $parsed_records = array();
        $parsed_records['local']    = array();
        $parsed_records['foreign']  = array();

        // echo '<pre>';
        // print_r($file_handle);
        // echo '</pre>';
        // exit;
        
        foreach ($file_handle as $line) {
            
            if ((strpos($line, 'LOCAL') !== false) || (strpos($line, 'FOREIGN') !== false)) { // check if text found
                $start = true;
                
                if (strpos($line, 'LOCAL') !== false) {
                    $arr_section    = 'local';
                }
                else {
                    $arr_section    = 'foreign';
                }
            }
            
            if ((strpos($line, 'SUBSIDIARIES') !== false) || (strpos($line, 'UBS:') !== false)) {
                $start = false;
            }
            
            if ($start) {
                    
                if ((FALSE !== strpos($line, 'Bank'))
                || (FALSE !== strpos($line, 'Bnk'))
                || (FALSE !== strpos($line, 'Trust'))
                || (FALSE !== strpos($line, 'Company'))) {
                    $bank_found     = STS_OK;
                }
                
                if (STS_OK == $bank_found) {
                    
                    $line_clean = strip_tags($line);
                    
                    $line_clean = str_replace("style='mso-spacerun:yes'>", ' ', $line_clean);
                    $line_clean = trim(html_entity_decode($line_clean), " \t\n\r\0\x0B\xC2\xA0");
                    
                    if ((is_numeric($line_clean)) || (false !== strpos($line, '..'))) {
                        $bank_records++;
                    }
                    
                    if ((6 >= $bank_records) && (1 <= $bank_records)) {
                        
                        if ('' != $line_clean) {
                            
                            //echo $line_clean.'<br/>';
                            
                            if (is_numeric($line_clean)) {
                                $parsed_records[$arr_section][]   = $line_clean;
                            }
                            else if (false !== strpos($line, '..')) {
                                $parsed_records[$arr_section][]   = 0;
                            }
                            else {
                                /*  Do nothing  */
                            }
                        }
                        
                        if (6 <= $bank_records) {
                            $bank_found     = STS_NG;
                            $bank_records   = 0;
                        }
                    }
                    
                }
            }
        }
        
        for ($idx = 0; @count($parsed_records['local']) > $idx; $idx++) {
            if (0 != ($idx % 2)) {
                $total_rates+= $parsed_records['local'][$idx];
                
                if (0 < $parsed_records['local'][$idx]) {
                    $rates_counted++;
                }
                //echo $parsed_records[$idx];
            }
        }
        
        $average['local'] = $total_rates / $rates_counted;
        
        $total_rates    = 0;
        $rates_counted  = 0;
        
        for ($idx = 0; @count($parsed_records['foreign']) > $idx; $idx++) {
            if (0 != ($idx % 2)) {
                $total_rates+= $parsed_records['foreign'][$idx];
                
                if (0 < $parsed_records['foreign'][$idx]) {
                    $rates_counted++;
                }
            }
        }
        
        $average['foreign'] = $total_rates / $rates_counted;
        
        $local_db = DB::table('config')
            ->where('name', 'local_ir')
            ->first();
        
        if (false == $local_db) {
            $local_db   = DB::table('config')
                ->insert(
                    array(
                        'name'          => 'local_ir',
                        'value'         => $average['local'],
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    )
                );
        }
        else {
            $local_db   = DB::table('config')
                ->where('name', 'local_ir')
                ->update(
                    array(
                        'value'         => $average['local'],
                        'updated_at'    => date('Y-m-d H:i:s'),
                    )
                );
        }
        
        $foreign_db = DB::table('config')
            ->where('name', 'foreign_ir')
            ->first();
        
        if (false == $foreign_db) {
            $foreign_db   = DB::table('config')
                ->insert(
                    array(
                        'name'          => 'foreign_ir',
                        'value'         => $average['foreign'],
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    )
                );
        }
        else {
           $foreign_db   = DB::table('config')
                ->where('name', 'foreign_ir')
                ->update(
                    array(
                        'value'         => $average['foreign'],
                        'updated_at'    => date('Y-m-d H:i:s'),
                    )
                );
        }
        
        return $average;
    }
}
