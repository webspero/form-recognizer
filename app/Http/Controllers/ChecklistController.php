<?php

use CreditBPO\Models\Entity;
use Illuminate\Http\Request;

class ChecklistController extends BaseController
{
    public function postSaveChecklist(Request $request, $entity_id) {
        $inputs = $request->all();
        array_shift($inputs);

        if (!file_exists(public_path('documents/checklist/'))) {
            mkdir(public_path('documents/checklist/'));
        }

        foreach ($inputs as $key => $input) {
            $destinationPath = public_path('documents/checklist/'.$entity_id.'/');
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath);
            }

            // get entity type
            $entity_type = Entity::select('entity_type')->where('entityid', $entity_id)->first();
            
            //get checklist type
            $checklistType = ChecklistType::select('id')->where('code', $key)->where('category', $entity_type->entity_type)->first();

            $localFileName = $input->getClientOriginalName();
            // save file
            $input->move($destinationPath, $localFileName);
            //create database entry
            $checklistFile = new ChecklistFile;
            $checklistFile->entity_id = $entity_id;
            $checklistFile->type_id = $checklistType->id;
            $checklistFile->file_name = $localFileName;
            $checklistFile->save();
        }

        return back();
    }

    public function getDeleteChecklist($id) {
		// get file details
        $file = ChecklistFile::where('id', $id)->first();
		if($file){
            // update and save
            ChecklistFile::where('id', $id)->update(array('is_deleted' => 1));
        }
        
		ActivityLog::saveLog();		// log activity in db
		return back();
    }
}
