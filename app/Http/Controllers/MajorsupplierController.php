<?php

use CreditBPO\Handlers\MajorSupplierHandler;

//======================================================================
//  Class 36: Major Supplier Controller
//      Functions related to Major Supplier on the Questionnaire
//      are routed to this class
//======================================================================
class MajorsupplierController extends BaseController {

	//-----------------------------------------------------
    //  Function 36.1: getMajorsupplier
    //      Displays the Major Customer Form
    //-----------------------------------------------------
	public function getMajorsupplier($entity_id)
	{
        $entity = Entity::find($entity_id);
		return View::make('sme.majorsupplier',
            array(
                'entity_id' => $entity_id,
                'relationship_satisfaction' =>
                    MajorSupplierHandler::getAllRelationshipSatisfaction(),
                'entity' => $entity
            )
        );
	}

    //-----------------------------------------------------
    //  Function 34.2: postMajorsupplier
    //      HTTP Post request to validate and save Major
    //      Suppliers data in the database
    //-----------------------------------------------------
	public function postMajorsupplier($entity_id)
	{
        /*  Variable Declaration    */
        $suppliers  = Input::get('supplier_name');  // Supplier Name
        $messages   = array();                      // Validation Messages according to rules
        $rules      = array();                      // Validation Rules

        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        $srv_resp['major_supplier'] = 1;

        $entity = Entity::find($entity_id);

        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#major-supp-list-cntr';                      // Refresh the specified container
            $srv_resp['refresh_elems']  = '.reload-major-supp';                         // Elements that will be refreshed

            if($entity->is_premium == PREMIUM_REPORT ) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);    // URL to fetch data from
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            }
        }

        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($suppliers); $idx++) {

            if ((STR_EMPTY != $suppliers[$idx])
            || (0 == $idx)) {

                $messages['supplier_name.'.$idx.'.required']                    = trans('validation.required', array('attribute'=>'Supplier Name'));
                $messages['supplier_share_sales.'.$idx.'.required']             = trans('validation.required', array('attribute'=>trans('business_details2.ms_percentage_share')));
                $messages['supplier_share_sales.'.$idx.'.numeric']              = trans('validation.numeric', array('attribute'=>trans('business_details2.ms_percentage_share')));
                $messages['supplier_share_sales.'.$idx.'.min']	                = trans('validation.min.numeric', array('attribute'=>trans('business_details2.ms_percentage_share')));
                $messages['supplier_share_sales.'.$idx.'.max']	                = trans('validation.max.numeric', array('attribute'=>trans('business_details2.ms_percentage_share')));
                $messages['supplier_address.'.$idx.'.required']                 = trans('validation.required', array('attribute'=>'Address'));
                $messages['supplier_contact_person.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>'Contact Person'));
                $messages['supplier_phone.'.$idx.'.required']	                = trans('validation.required', array('attribute'=>'Contact Phone Number'));
                $messages['supplier_phone.'.$idx.'.regex']	                    = trans('validation.phoneregex', array('attribute'=>'Contact Phone Number'));
                $messages['supplier_email.'.$idx.'.required']	                = trans('validation.required', array('attribute'=>'Contact Email'));
                $messages['supplier_email.'.$idx.'.email']                      = trans('validation.email', array('attribute'=>'Contact Email'));
                $messages['supplier_started_years.'.$idx.'.required']	        = trans('validation.required', array('attribute'=>trans('business_details2.ms_year_started')));
                $messages['supplier_started_years.'.$idx.'.date_format']        = 'Invalid Date Format';
                $messages['supplier_started_years.'.$idx.'.after']              = 'Year cannot be lower than 1970';
                $messages['supplier_started_years.'.$idx.'.before']             = 'Year cannot exceed the year now';
                $messages['supplier_years_doing_business.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details2.mc_year_doing_business')));
                $messages['supplier_years_doing_business.'.$idx.'.numeric']     = trans('validation.numeric', array('attribute'=>trans('business_details2.mc_year_doing_business')));
                $messages['supplier_settlement.'.$idx.'.required']	            = trans('validation.required', array('attribute'=>trans('business_details2.ms_payment')));
                $messages['supplier_order_frequency.'.$idx.'.required']         = trans('validation.required', array('attribute'=>trans('business_details2.ms_frequency')));
                $messages['average_monthly_volume.'.$idx.'.required']           = trans('validation.required', array('attribute'=>'Average Monthly Volume'));
                // $messages['average_monthly_volume.'.$idx.'.numeric']            = trans('validation.numeric', array('attribute'=>'Average Monthly Volume'));
                $messages['relationship_satisfaction.'.$idx.'.required'] = trans(
                    'validation.required',
                    [
                        'attribute' => trans('business_details2.ms_relationship_satisfaction')
                    ]
                );

                if($entity->is_premium == PREMIUM_REPORT) {
                    $rules['supplier_name.'.$idx]                   = 'required';
                    $rules['supplier_address.'.$idx]                = 'required';
                    $rules['supplier_phone.'.$idx]	                = request('supplier_phone')[0] == null ? '' : 'regex:/^.{7,20}$/';
                    $rules['supplier_email.'.$idx]	                = request('supplier_email')[0] == null ? '' : 'email';
                    $rules['supplier_started_years.'.$idx]	        = request('supplier_started_years')[0] == null ? '' : 'date_format:"Y"|after:1969|before:next year';
                    $rules['supplier_years_doing_business.'.$idx]	= request('supplier_years_doing_business')[0] == null ? '' : 'numeric';
                    $rules['supplier_share_sales.'.$idx]            = request('supplier_share_sales')[0] == null ? '' : 'numeric|min:0|max:100';
                    $rules['average_monthly_volume.'.$idx]          = 'required';
                } else {
                    $rules['supplier_name.'.$idx]                   = 'required';
                    $rules['supplier_address.'.$idx]                = 'required';
                    $rules['supplier_contact_person.'.$idx]	        = 'required';
                    $rules['supplier_phone.'.$idx]	                = 'required|regex:/^.{7,20}$/';
                    $rules['supplier_email.'.$idx]	                = 'required|email';
                    $rules['supplier_started_years.'.$idx]	        = 'required|date_format:"Y"|after:1969|before:next year';
                    $rules['supplier_years_doing_business.'.$idx]	= 'required|numeric';
                    $rules['supplier_share_sales.'.$idx]            = 'required|numeric|min:0|max:100';
                    $rules['supplier_settlement.'.$idx]	            = 'required';
                    $rules['supplier_order_frequency.'.$idx]	    = 'required';
                    $rules['average_monthly_volume.'.$idx]          = 'required';
                    $rules['relationship_satisfaction.'.$idx] = 'required';
                }
            }
        }

        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if ($validator->fails())	{
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('supplier_name', 'supplier_address', 'supplier_contact_person', 'supplier_phone', 'supplier_email', 'supplier_started_years', 'supplier_years_doing_business', 'supplier_share_sales', 'supplier_settlement', 'supplier_order_frequency', 'item_1', 'item_2', 'item_3', 'average_monthly_volume',
                'relationship_satisfaction');

            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }

            /*  Stores data into an array for loop traversal    */
			$supplier_name                  = $arraydata['supplier_name'];
			$supplier_address               = $arraydata['supplier_address'];
			$supplier_contact_person        = $arraydata['supplier_contact_person'];
			$supplier_phone                 = $arraydata['supplier_phone'];
			$supplier_email                 = $arraydata['supplier_email'];
			$supplier_started_years         = $arraydata['supplier_started_years'];
			$supplier_years_doing_business  = $arraydata['supplier_years_doing_business'];
			$supplier_share_sales           = $arraydata['supplier_share_sales'];
			$supplier_settlement            = $arraydata['supplier_settlement'];
			$supplier_order_frequency       = $arraydata['supplier_order_frequency'];
			$item_1                         = $arraydata['item_1'];
			$item_2                         = $arraydata['item_2'];
			$item_3                         = $arraydata['item_3'];
			$average_monthly_volume         = $arraydata['average_monthly_volume'];
            $relationship_satisfaction = $arraydata['relationship_satisfaction'];

            $report         = Entity::select('anonymous_report')->where('entityid', $entity_id)->first();

            if (1 == $report->anonymous_report) {
                $max_supplier   = Majorsupplier::where('entity_id', $entity_id)->get();
                $max_supplier   = @count($max_supplier);
            }

            /*  Saves Input to the database     */
			foreach ($supplier_name as $key => $value) {
				if ($value) {
                    if (1 == $report->anonymous_report) {
                        $max_supplier++;
                        $supp_name  = 'Major Supplier '.$max_supplier;
                    }
                    else {
                        $supp_name  = $supplier_name[$key];
                    }

					$data = array(
					    'entity_id'                     => $entity_id,
					   	'supplier_name'                 => $supp_name,
					   	'supplier_address'              => $supplier_address[$key],
					   	'supplier_contact_person'       => $supplier_contact_person[$key] == null ? '' : $supplier_contact_person[$key],
					   	'supplier_phone'                => $supplier_phone[$key] == null ? '' : $supplier_phone[$key],
					   	'supplier_email'                => $supplier_email[$key] == null ? '' : $supplier_email[$key],
					   	'supplier_started_years'        => $supplier_started_years[$key] == null ? '' : $supplier_started_years[$key],
					   	'supplier_years_doing_business' => $supplier_years_doing_business[$key] == null ? '' : $supplier_years_doing_business[$key],
					   	'supplier_share_sales'          => $supplier_share_sales[$key] == null ? '' : $supplier_share_sales[$key],
					   	'supplier_settlement'           => $supplier_settlement[$key] == null ? '' : $supplier_settlement[$key],
					   	'supplier_order_frequency'      => $supplier_order_frequency[$key] == null ? '' : $supplier_order_frequency[$key],
					   	'item_1'                        => $item_1[$key] == null ? '' : $item_1[$key],
					   	'item_2'                        => $item_2[$key] == null ? '' : $item_2[$key],
					   	'item_3'                        => $item_3[$key] == null ? '' : $item_3[$key],
					   	'average_monthly_volume'        => $average_monthly_volume[$key] == null ? '' : $average_monthly_volume[$key],
                        'relationship_satisfaction'     => $relationship_satisfaction[$key] == null ? '' : $relationship_satisfaction[$key],
					   	'created_at'                    => date('Y-m-d H:i:s'),
				    	'updated_at'                    => date('Y-m-d H:i:s')
					);

                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {

                        DB::table('tblmajorsupplier')
                            ->where('majorsupplierid', $primary_id[$key])
                            ->update($data);

                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblmajorsupplier')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}

            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;

            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}

        /*  Logs the action to the database */
        ActivityLog::saveLog();

        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}

    //-----------------------------------------------------
    //  Function 36.3: putMajorsupplierUpdate
    //      HTTP Post request to validate and save updates
    //      of specified Major Supplier data in the database
    //      (Currently not in used)
    //-----------------------------------------------------
	public function putMajorsupplierUpdate($id)
	{
        /*  Sets the Validation Messages    */
		$messages = array(
			'supplier_name.0.required'	                => trans('validation.required', array('attribute'=>'Supplier Name')),
			'supplier_share_sales.0.required'	        => trans('validation.required', array('attribute'=>trans('business_details2.ms_percentage_share'))),
			'supplier_share_sales.0.numeric'	        => trans('validation.numeric', array('attribute'=>trans('business_details2.ms_percentage_share'))),
			'supplier_share_sales.0.min'	            => trans('validation.min.numeric', array('attribute'=>trans('business_details2.ms_percentage_share'))),
			'supplier_share_sales.0.max'	            => trans('validation.max.numeric', array('attribute'=>trans('business_details2.ms_percentage_share'))),
			'supplier_address.0.required'	            => trans('validation.required', array('attribute'=>'Address')),
			'supplier_contact_person.0.required'	    => trans('validation.required', array('attribute'=>'Contact Person')),
			'supplier_phone.0.required'	                => trans('validation.required', array('attribute'=>'Contact Phone Number')),
			'supplier_phone.0.regex'	                => trans('validation.phoneregex', array('attribute'=>'Contact Phone Number')),
			'supplier_email.0.required'	                => trans('validation.required', array('attribute'=>'Contact Email')),
			'supplier_email.0.email'	                => trans('validation.email', array('attribute'=>'Contact Email')),
			'supplier_started_years.0.required'	        => trans('validation.required', array('attribute'=>trans('business_details2.ms_year_started'))),
			'supplier_started_years.0.digits'	        => trans('validation.digits', array('attribute'=>trans('business_details2.ms_year_started'))),
			'supplier_years_doing_business.0.required'	=> trans('validation.required', array('attribute'=>trans('business_details2.mc_year_doing_business'))),
			'supplier_years_doing_business.0.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details2.mc_year_doing_business'))),
			'supplier_settlement.0.required'	        => trans('validation.required', array('attribute'=>trans('business_details2.ms_payment'))),
			'supplier_order_frequency.0.required'       => trans('validation.required', array('attribute'=>trans('business_details2.ms_frequency'))),
		);

        /*  Sets the Validation Rules       */
		$rules = array(
	        'supplier_name.0'	=> 'required',
	        'supplier_address.0'	=> 'required',
	        'supplier_contact_person.0'	=> 'required',
	        'supplier_phone.0'	=> 'required|regex:/^.{7,20}$/',
	        'supplier_email.0'	=> 'required|email',
	        'supplier_started_years.0'	=> 'required|digits:4',
	        'supplier_years_doing_business.0'	=> 'required|numeric',
	        'supplier_share_sales.0' => 'required|numeric|min:0|max:100',
	        'supplier_settlement.0'	=> 'required',
	        'supplier_order_frequency.0'	=> 'required',
		);

        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if ($validator->fails()) {
			return Redirect::to('/sme/majorsupplier/'.$id.'/edit')->withErrors($validator)->withInput();
		}
        /*  Input validation success    */
		else {
            /*  Sets the user input to they matching database field */
			$dataFields = array(
				'supplier_name'                 => Input::get('supplier_name.0'),
				'supplier_address'              => Input::get('supplier_address.0'),
				'supplier_contact_person'       => Input::get('supplier_contact_person.0'),
				'supplier_phone'                => Input::get('supplier_phone.0'),
				'supplier_email'                => Input::get('supplier_email.0'),
				'supplier_started_years'        => Input::get('supplier_started_years.0'),
				'supplier_years_doing_business' => Input::get('supplier_years_doing_business.0'),
				'supplier_share_sales'          => Input::get('supplier_share_sales.0'),
				'supplier_settlement'           => Input::get('supplier_settlement.0'),
				'supplier_order_frequency'      => Input::get('supplier_order_frequency.0'),
			);

            /*  Saves the changes to the Database   */
			$updateProcess = Majorsupplier::where('majorsupplierid', '=', $id)
                ->where('entity_id', '=', Session::get('entity')->entityid)
                ->update($dataFields);

            /*  Save to database was successful */
			if ($updateProcess) {
				return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
			}
            /*  Save to database was failed */
			else {
				return Redirect::to('/sme/confirmation/tab2')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return</a>');
			}
		}
	}

    //-----------------------------------------------------
    //  Function 36.4: getMajorsupplierEdit
    //      Displays the Major Supplier Edit Form
    //      (Currently not in used)
    //-----------------------------------------------------
	public function getMajorsupplierEdit($id)
	{
        /*  Query the database for the specified Major Supplier */
		$entity = DB::table('tblmajorsupplier')
            ->where('majorsupplierid', $id)
            ->where('entity_id', Session::get('entity')->entityid)
            ->get();

        /*  Pass the Data to the Major Customer Form    */
		return View::make(
            'sme.majorsupplier',
            [
                'relationship_satisfaction' =>
                    MajorSupplierHandler::getAllRelationshipSatisfaction(),
            ]
        )->with('entity', $entity);
	}

    //-----------------------------------------------------
    //  Function 36.5: getMajorsupplierDelete
    //      HTTP GET request to delete specified Major Supplier
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getMajorsupplierDelete($id)
	{
        /*  Query the database for the specified Major Supplier and delete it   */
        $majorSupplier = Majorsupplier::where('majorsupplierid', '=', $id)->first();

        $majorSupplier->is_deleted = 1;
        $majorSupplier->save();

        /*  Logs the action to the database */
        ActivityLog::saveLog();

		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

    //-----------------------------------------------------
    //  Function 36.6: updateMajorAccount
    //      Updates the related Major Supplier Tickbox
    //          (Currently not in used)
    //-----------------------------------------------------
    public function updateMajorAccount()
    {
        /*  Variable Declaration    */
        $entity_id  = Input::get('entity_id');
        $major_supp = Input::get('no_major_supplier');

        /*  Query the database if Major Supplier tickbox record already exist   */
        $major_supp_data = DB::table('tblmajorsupplieraccount')
			->where('entity_id', $entity_id)
			->first();

        /*  If a record exist update it according to checkbox   */
        if (0 < @count($major_supp_data)) {

			$upd_sts = Majorsupplieraccount::where('entity_id', '=', $entity_id)
                ->update(array('is_agree' => $major_supp));
        }
        /*  When a record does not exist create one */
        else {
            $major_supp_data    = new Majorsupplieraccount();

            $major_supp_data->entity_id = $entity_id;
            $major_supp_data->is_agree  = $major_supp;
            $major_supp_data->save();
        }

        /*  Logs the action to the database */
		ActivityLog::saveLog();
    }
}
