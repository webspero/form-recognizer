<?php

use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Libraries\PaymentCalculator;
use CreditBPO\Models\BankSerialKey;
use CreditBPO\Models\Document;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\FinancialAnalysis as FinancialAnalysisPDF;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use CreditBPO\Exceptions\FinancialAnalysisHandlerException;
use CreditBPO\Models\ReportMatchingBank;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use CreditBPO\Handlers\SMEInternalHandler;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use CreditBPO\Services\IndustryComparison;


//======================================================================
//  Class 3: BankController
//  Description: Contains all the functions regarding Supervisor backend
//======================================================================
class BankController extends BaseController
{

    /* variable declaration */
    private $url; // storage for url
    private $merchant_id = 'CREDITBPO'; // define dragonpay merchant id
    private $merchant_key = 'N6f9rEW3'; // define dragonpay key
    private $bank_subscription; // storage for bank_subscription
    private $paymentCalculator;
    private $accountDetails = [];

    //-----------------------------------------------------
    //  Function 3.1: __construct
	//  Description: class constructor
    //-----------------------------------------------------
	public function __construct()
	{
		try{
			if(env("APP_ENV") == "Production"){ // checks for environment and assigns dragonpay url
				$this->url = 'https://gw.dragonpay.ph/Pay.aspx';
			} else {
				$this->url = 'http://test.dragonpay.ph/Pay.aspx';
			}

			$this->bank_subscription = Permissions::get_bank_subscription(); // check for bank subscription status
			$this->paymentCalculator = new PaymentCalculator;
			if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor') == false){
				$this->accountDetails = BankSessionHelper::createBankSession(Auth::user());
			} else if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			} else if (Auth::user() && Auth::user()->role === 0 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.2: getIndustry
    //  Description: get Industry weights page
    //-----------------------------------------------------
	public function getIndustry()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(2);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			$bank_industry = DB::table('tblbankindustryweights')
				->select('tblbankindustryweights.*', 'tblindustry_main.main_title')
				->join('tblindustry_main', 'tblbankindustryweights.industry_id', '=', 'tblindustry_main.industry_main_id')
				->where('bank_id', Auth::user()->bank_id)
				->get();

			// create records if not existing
			if($bank_industry==null){
				$industry = Industrymain::all();
				foreach($industry as $i){
					BankIndustry::create(array(
						'bank_id' => Auth::user()->bank_id,
						'industry_id' => $i->industry_main_id,
						'business_group' => $i->business_group,
						'management_group' => $i->management_group,
						'financial_group' => $i->financial_group
					));
				}
				// recast
				$bank_industry = DB::table('tblbankindustryweights')
					->select('tblbankindustryweights.*', 'tblindustry_main.main_title')
					->join('tblindustry_main', 'tblbankindustryweights.industry_id', '=', 'tblindustry_main.industry_main_id')
					->where('bank_id', Auth::user()->bank_id)
					->get();
			}

			// bank standalone
			$bank_standalone = Bank::find(Auth::user()->bank_id)->is_standalone;
			if($bank_standalone){
				foreach($bank_industry as $bi) {
					$bi->business_group = 0;
					$bi->management_group = 0;
					$bi->financial_group = 100;
				}
			}

			$history = BankIndustrySetting::where('bank_id', Auth::user()->bank_id)
					->orderBy('id', 'desc')->take(10)->get();

			return View::make('bank.industry', array(
				'industry' => $bank_industry,
				'history' => $history,
				'bank_standalone' => $bank_standalone
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.3: postUpdateStandaloneOption
	//  Description: function to switch industry weights to standalone
    //-----------------------------------------------------
	public function postUpdateStandaloneOption()
	{
		try{
			$bank = Bank::find(Auth::user()->bank_id);
			$type = 0;
			if(Input::get('option')=='true'){ // get input and update
				$bank->is_standalone = 1;
				$type = 1;
			} else {
				$bank->is_standalone = 0;
			}
			$bank->save();

			/**Creating logs for bank type */
			$data = DB::table("supervisor_type_update_logs")->select("id")->where("bank_id", Auth::user()->bank_id)->get();
			foreach ($data as $key => $d) {
				if($d) {
					DB::table("supervisor_type_update_logs")->where("id", $d->id)->update(array("is_deleted" => 1));
				}
			}
			
			$insert = array(
				"bank_id" => Auth::user()->bank_id,
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
				"is_standalone" => $type,
				"updated_by" => Auth::user()->loginid,
				"is_deleted" => 0
			 );

			//insert new logs for future tracking
			 DB::table("supervisor_type_update_logs")->insert($insert);

			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.4: getEditIndustry
    //  Description: get Industry Weights edit page
    //-----------------------------------------------------
	public function getEditIndustry($industry_id)
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(2);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			// get industry
			$industry = DB::table('tblbankindustryweights')
					->select('tblbankindustryweights.*', 'tblindustry_main.main_title')
					->join('tblindustry_main', 'tblbankindustryweights.industry_id', '=', 'tblindustry_main.industry_main_id')
					->where('id', $industry_id)
					->first();
			// show page
			return View::make('bank.editindustry', array(
				'industry' => $industry
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.5: postEditIndustry
	//  Description: post process for Industry Weights edit page
    //-----------------------------------------------------
	public function postEditIndustry($industry_id)
	{
		try{
			// check product plan
			if(Auth::user()->product_plan==3) App::abort(404);
	        $excess_point   = STS_NG;
	        $lack_points    = STS_NG;
	        $total_points   = 0;

			$rules = array(									// validation rules
		        'business_group'	=> 'required|numeric',
		        'management_group'	=> 'required|numeric',
				'financial_group' 	=> 'required|numeric'
			);

			// summation of total points
	        $total_points = Input::get('business_group') + Input::get('management_group') + Input::get('financial_group');

	        if ((100 < Input::get('business_group'))			// should not exceed 100%
	        || (100 < Input::get('management_group'))
	        || (100 < Input::get('financial_group'))) {
	            $excess_point   = STS_OK;
	        }
	        else if (100 < $total_points) {
	            $excess_point   = STS_OK;
	        }

	         if ((0 > Input::get('business_group'))				// should not be 0
	        || (0 > Input::get('management_group'))
	        || (0 > Input::get('financial_group'))) {
	            $lack_points    = STS_OK;
	        }
	        if (100 > $total_points) {
	            $lack_points   = STS_OK;
	        }

			$validator = Validator::make(Input::all(), $rules); // run validation

			if($validator->fails()) {
				return Redirect::to('/bank/industry/edit/' . $industry_id)->withErrors($validator->errors())->withInput();
			} elseif (STS_OK == $excess_point) {
	            $errors = new Illuminate\Support\MessageBag;
	            $errors->add("error", 'Total values cannot exceed 100 points');
	            return Redirect::to('/bank/industry/edit/'.$industry_id)->withErrors($errors)->withInput();
	        } elseif (STS_OK == $lack_points) {
	            $errors = new Illuminate\Support\MessageBag;
	            $errors->add("error", 'Total values must be 100 points');
	            return Redirect::to('/bank/industry/edit/'.$industry_id)->withErrors($errors)->withInput();
	        } else {
				// validation success, update and save
				$industry = BankIndustry::find($industry_id);
				$industry->business_group = Input::get('business_group');
				$industry->management_group = Input::get('management_group');
				$industry->financial_group = Input::get('financial_group');
				$industry->save();

				return Redirect::to('/bank/industry');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.6: postResetWeights
	//  Description: function to reset industry weights in supervisor panel
    //-----------------------------------------------------
	public function postResetWeights()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			// set default data as array
			$reset_data = array(
				1 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25),
				2 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25),
				3 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				4 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				5 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				6 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				7 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				8 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				9 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				10 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				11 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				12 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				13 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				14 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				15 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				16 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				17 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				18 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35)
			);

			// iterate and update
			foreach($reset_data as $key=>$value){
				$industry = BankIndustry::where('industry_id', $key)
				->where('bank_id', Auth::user()->bank_id)
				->update($value);
			}

			return Redirect::to('/bank/industry');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.7: postSaveWeights
	//  Description: function to save industry weights settings to be used in history
    //-----------------------------------------------------
	public function postSaveWeights()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			$bank_industry = DB::table('tblbankindustryweights')
				->where('bank_id', Auth::user()->bank_id)
				->get();

			$json_object = json_encode($bank_industry);
			BankIndustrySetting::create(array(
				'name' => Input::get('name'),
				'bank_id' => Auth::user()->bank_id,
				'setting' => $json_object
			));
			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.8: postRevertToSaveWeights
	//  Description: function to update industry weights based on saved setting
    //-----------------------------------------------------
	public function postRevertToSaveWeights()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			$setting = BankIndustrySetting::find(Input::get('id'));
			$bank_history = json_decode($setting->setting, true);
			foreach($bank_history as $bh){
				$industry = BankIndustry::where('industry_id', $bh['industry_id'])
				->where('bank_id', $bh['bank_id'])
				->update(array(
					'business_group' => $bh['business_group'],
					'management_group' => $bh['management_group'],
					'financial_group' => $bh['financial_group']
				));
			}
			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.9: getBankDashboard
	//  Description: get supervisor Accounts page
    //-----------------------------------------------------
	public function getBankDashboard()
	{
		try{
			$rm_bank = ReportMatchingBank::where('loginid',Auth::user()->loginid)->get()->first();
			$report_matching_bank = 0;
			if(!empty($rm_bank->id)){
				$report_matching_bank = 1;
				session(['report_matching_bank' => 1]);
			}

			/* Admin has an access to supervisor account */
			if (Input::has('bank')) {
				$supervisorId = Input::get('bank');
				/* Check if supervisor id exist in login id, it can be a trial account or approved application */
				$supervisor = User::where('loginid', $supervisorId)
								  ->where('role', 4)
								  ->first();
				if ($supervisor) {
					/* Create a session */
					BankSessionHelper::createBankSession($supervisor);
					$this->accountDetails = Session::get('supervisor');
				}
			}

			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();

	        $isSubscribed = true;

			//supervisor subscription
			$supAccess = true;
			if(Auth::user()->role == 4 && $organization->is_custom == 0) {
				$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",Auth::user()->loginid)->first();
				$now = date("Y-m-d H:i:s"); //created_at

				if($supSubscription){
					if($supSubscription->expiry_date < $now){
						$supAccess = false;
						$isSubscribed = false;
					}
				}else{
					$supAccess = false;
						$isSubscribed = false;
				}
			}

			$company_names      = array();
			$delete_company     = array();

			$entity = DB::table('tblentity', 'tblindustry_sub')
			->select(
				'tblentity.entityid',
				'tblentity.industry_main_id',
				'tblentity.companyname',
				'tblentity.email',
				'tblentity.is_premium',
				'tblentity.date_established',
				'tblentity.number_year',
				'tblentity.anonymous_report',
				'tblentity.approval_status',
				'tblentity.approval_date',
				'tblentity.zipcode',
				'tblentity.status as report_status',
				'zipcodes.city',
				'zipcodes.region',
				'zipcodes.major_area as province',
				'refcitymun.citymunDesc',
				'refregion.regDesc',
				'refregion.regCode',
				'tblsmescore.score_sf',
				'tblsmescore.score_rm',
				'tblsmescore.score_cd',
				'tblsmescore.score_sd',
				'tblsmescore.score_bois',
				'tblsmescore.score_boe',
				'tblsmescore.score_mte',
				'tblsmescore.score_bdms',
				'tblsmescore.score_sp',
				'tblsmescore.score_ppfi',
				'tblsmescore.score_rfp',
				'tblsmescore.score_rfpm',
				'tblsmescore.score_facs',
				'tblindustry_main.main_title',
				'tblsmescore.created_at as completed_date',
				'tblentity.status',
				'tbllogin.status as login_status',
				'tbllogin.loginid',
				'investigator_tag.id as ci_started',
				'investigator_notes.status as ci_status'
			)
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->leftJoin('tblsmescore', 'tblsmescore.entityid', '=', 'tblentity.entityid')
			->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
			->leftJoin('investigator_tag', 'investigator_tag.entity_id', '=', 'tblentity.entityid')
			->leftJoin('investigator_notes', 'investigator_notes.entity_id', '=', 'tblentity.entityid')
			// ->leftJoin('utility_bills', 'utility_bills.entity_id', '=', 'tblentity.entityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->leftJoin('refregion', 'refcitymun.regDesc', '=', 'refregion.regCode')
			->where('is_deleted', 0);

			// subscription
			$bank_subscription_note = '';

			if ($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c')) && $supAccess == true) {
				$bank_login_id = $this->accountDetails['loginid'];
				if ($this->accountDetails['parent_user_id'] != 0) {
					$bank_login_id = $this->accountDetails['parent_user_id'];
				}

				$subs_sts   = self::checkSubscriptionSts($bank_login_id);

				if ($this->accountDetails['email'] != "testbank@palatine.test.com" && STS_NG == $subs_sts) {
					$user = User::find($this->accountDetails['loginid']);

					if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !$isSubscribed) {
						
						$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
						return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
					}
					return Redirect::to('/bank/subscribe');
				}
				else if ($this->accountDetails['email'] != "testbank@palatine.test.com" && 2 == $subs_sts) {
					$subscription   = Transaction::where('loginid', $bank_login_id)
						->where('description', 'Bank User Subscription')
						->orderBy('id', 'desc')
						->first();

					/* Redirect to Pending Screen */
					return View::make('payment.pending', array('refno' => $subscription['refno']));
				}
				else {
					/** No Processing   */
				}

				/* filtering results if filters are present */
				if(Input::has('type'))
				{
					if(Input::get('type')=='location' && Input::get('province')!=''){
						if(Input::get('city')!=''){
							$entity->where('tblentity.cityid', Input::get('city'));
						} else {
							$entity->where('tblentity.province', Input::get('province'));
						}
					}
					elseif(Input::get('type')=='date_established' && Input::get('date_established')!=''){
						switch(Input::get('date_established_compare')){
							case 0: $op = '='; break;
							case 1: $op = '>'; break;
							case 2: $op = '<'; break;
							default: $op = '=';
						}
						$entity->where('tblentity.date_established', $op,Input::get('date_established'));
					}
					elseif(Input::get('type')=='industry' && Input::get('industry')!=''){
						$entity->where('tblindustry_main.industry_main_id', Input::get('industry'));
					}
					elseif(Input::get('type')=='companyname'){
						$entity->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
					}
					elseif(Input::get('type')=='invst_sts' && Input::get('invst_sts')!=''){
						if(Input::get('invst_sts')=='0'){
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('invst_sts')=='1'){
							$entity->where('tblentity.status', '=', 7);
							$entity->whereNull('investigator_tag.id');
							$entity->whereNull('investigator_notes.status');
						}
						elseif(Input::get('invst_sts')=='2'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where(function($query)
								{
									$query->whereNull('investigator_notes.status')
											->orWhere('investigator_notes.status', 0);
								});
						}
						elseif(Input::get('invst_sts')=='3'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where('investigator_notes.status', '>', 0);
						}
					}
					elseif(Input::get('type')=='status' && Input::get('status')!=''){
						if(Input::get('status')=='0'){
							$entity->whereIn('tbllogin.status', array(0, 1));
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('status')=='1'){
							$entity->where('tbllogin.status', 2);
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('status')=='2'){
							$entity->where('tbllogin.status', 2);
							$entity->where('tblentity.status', 7);
						}
					} else {
						$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
					}
				} else {
					$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
				}
			}
			elseif($this->accountDetails['email'] != "testbank@palatine.test.com" && Auth::user()->role !== 0 && $organization->is_custom == 0) {
				$user = User::find($this->accountDetails['loginid']);

				if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !(strtotime($this->bank_subscription) > strtotime(date('c')))) {
					
					$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
					return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
				}
				/* Redirect to Payment */
				return Redirect::to('/bank/subscribe');
			} else if (Auth::user()->role === 0){
				/* User accessing using admin account*/
				/* filtering results if filters are present */
				if(Input::has('type'))
				{
					if(Input::get('type')=='location' && Input::get('province')!=''){
						if(Input::get('city')!=''){
							$entity->where('tblentity.cityid', Input::get('city'));
						} else {
							$entity->where('tblentity.province', Input::get('province'));
						}
					}
					elseif(Input::get('type')=='date_established' && Input::get('date_established')!=''){
						switch(Input::get('date_established_compare')){
							case 0: $op = '='; break;
							case 1: $op = '>'; break;
							case 2: $op = '<'; break;
							default: $op = '=';
						}
						$entity->where('tblentity.date_established', $op,Input::get('date_established'));
					}
					elseif(Input::get('type')=='industry' && Input::get('industry')!=''){
						$entity->where('tblindustry_main.industry_main_id', Input::get('industry'));
					}
					elseif(Input::get('type')=='companyname'){
						$entity->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
					}
					elseif(Input::get('type')=='invst_sts' && Input::get('invst_sts')!=''){
						if(Input::get('invst_sts')=='0'){
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('invst_sts')=='1'){
							$entity->where('tblentity.status', '=', 7);
							$entity->whereNull('investigator_tag.id');
							$entity->whereNull('investigator_notes.status');
						}
						elseif(Input::get('invst_sts')=='2'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where(function($query)
								{
									$query->whereNull('investigator_notes.status')
											->orWhere('investigator_notes.status', 0);
								});
						}
						elseif(Input::get('invst_sts')=='3'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where('investigator_notes.status', '>', 0);
						}
					}
					elseif(Input::get('type')=='status' && Input::get('status')!=''){
						if(Input::get('status')=='0'){
							$entity->whereIn('tbllogin.status', array(0, 1));
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('status')=='1'){
							$entity->where('tbllogin.status', 2);
							$entity->where('tblentity.status', '!=', 7);
						}
						elseif(Input::get('status')=='2'){
							$entity->where('tbllogin.status', 2);
							$entity->where('tblentity.status', 7);
						}
					} else {
						$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
					}
				} else {
					$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
				}
			}

			if ($this->accountDetails['can_view_free_sme'] == 1) {
				$entity->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$entity->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}

			
			$entity = $entity->groupBy('entityid')->get();

			$entity_arr = $entity->toArray();

			$entityIds = array_column($entity_arr, 'entityid');

			$impEntityIds = implode(',',$entityIds);
			
			$bank = Bank::find($this->accountDetails['bank_id']);
			$bank_formula = BankFormula::where('bank_id', $this->accountDetails['bank_id'])->first();
			$bankGroupsRaw = BankIndustry::where('bank_id', $this->accountDetails['bank_id'])
						->groupBy('industry_id')->get();

			$bank_groups = [];
			foreach ($bank_groups as $bank_group) {
				$bank_groups[$bank_group->industry_id] = $bank_group;

			}

			$regions = [];
			
			// process entity and update fields to readable content
			foreach($entity as $key=>$value){

				if ($value->status == 7) {
					$entity[$key]->status = '<span style="color:GREEN;">Completed</span>';
					$notes_count        = $value->ci_status;
					$tag_count          = $value->ci_started;

					if ($notes_count && $notes_count>0) {
						$invst_sts      = '<a href = "'.URL::to('summary/smesummary').'/'.$entity[$key]->entityid.'/ci_view" > <span style="color:blue;">Verified</span> </a>';
					}
					else if ($tag_count) {
						$invst_sts      = '<span style="color:green;">In Review</span>';
					}
					else {
						$invst_sts      = '<span style="color:red;">Awaiting Assignment</span>';
					}
				}
				else {
					if($value->login_status == 0 || $value->login_status == 1){
						$entity[$key]->status       = '<span style="color:GREY;">Not Started</span>';
						$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
					} else {
						$entity[$key]->status       = '<span style="color:RED;">In Progress</span>';
						$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
					}
				}

				$entity[$key]->invst_sts		= $invst_sts;
				$entity[$key]->pdefault			= STR_EMPTY;
				$entity[$key]->cbpo_score_color = STR_EMPTY;
				$entity[$key]->bank_score_color = STR_EMPTY;
				if(isset($entity[$key]->m_score)){
					$entity[$key]->m_score = App::make('ProfileSMEController')->computeBeneishScore($entity[$key]->entityid);
				}
				// update score as readable
				$financialChecker = false;
				$financialTotal = 0;

				$financialReport = FinancialReport::where(['entity_id' => $entity[$key]->entityid,'is_deleted' => 0])->first();

				if($value->report_status == 7 && $financialReport){
					$frHelper = new FinancialHandler();
					$finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
					if($finalRatingScore) {
						$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
						$financialChecker = true;
					}
				}
				if($financialChecker){
					//Financial Position
					if($financialTotal >= 1.6) {
						$entity[$key]->score_sf          = "AAA - Excellent";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '1% - Very Low Risk';
					} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
						$entity[$key]->score_sf          = "AA - Very Good";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '2% - Very Low Risk';
					} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
						$entity[$key]->score_sf          = "A - Good";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '3% - Low Risk';
					} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
						$entity[$key]->score_sf          = "BBB - Positive";
						$entity[$key]->cbpo_score_color  = 'orange';
						$entity[$key]->pdefault          = '4% - Moderate Risk';
					} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
						$entity[$key]->score_sf          = "BB - Normal";
						$entity[$key]->cbpo_score_color  = 'orange';
						$entity[$key]->pdefault          = '6% - Moderate Risk';
					} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
						$entity[$key]->score_sf   	 	 = "B - Satisfactory";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '8% - Moderate Risk';
					} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
						$entity[$key]->score_sf          = "CCC - Unsatisfactory";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '15% - High Risk';
					} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
						$entity[$key]->score_sf          = "CC - Adverse";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '20% - High Risk';
					} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
						$entity[$key]->score_sf          = "C - Bad";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '25% - Very High Risk';
					} else {
						$entity[$key]->score_sf          = "D - Critical";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '32% - Very High Risk';
					}
				} elseif ($value->score_sf){
					$cred_rating_exp    = explode('-', $value->score_sf);
					$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
					$rate_val   = intval($rate_val);

					if($rate_val >= 177){
						$entity[$key]->score_sf			= 'AA - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'green';
						$entity[$key]->pdefault			= '2% - Very Low Risk';
					}
					else if($rate_val >= 150 && $rate_val <= 176) {
						$entity[$key]->score_sf 		= 'A - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'green';
						$entity[$key]->pdefault 		= '3% - Low Risk';
					}
					else if($rate_val >= 123 && $rate_val <= 149) {
						$entity[$key]->score_sf 		= 'BBB - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'orange';
						$entity[$key]->pdefault 		= '4% - Moderate Risk';
					}
					else if($rate_val >= 96 && $rate_val <= 122) {
						$entity[$key]->score_sf			= 'BB - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'orange';
						$entity[$key]->pdefault			= '6% - Moderate Risk';
					}
					else if($rate_val >= 68 && $rate_val <= 95) {
						$entity[$key]->score_sf			= 'B - Some Vulnerability';
						$entity[$key]->cbpo_score_color = 'red';
						$entity[$key]->pdefault			= '8% - High Risk';
					}
					else if($rate_val < 68) {
						$entity[$key]->score_sf 		= 'CCC - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'red';
						if($rate_val >= 42 && $rate_val <= 68) {
							$entity[$key]->pdefault = '15% - High Risk';
						} else {
							$entity[$key]->pdefault = '30% - Very High Risk';
						}
					}
				}

				$entity[$key]->score_bank = $entity[$key]->score_sf;
				$entity[$key]->bank_score_color = $entity[$key]->cbpo_score_color;
				// bank score computation

				$bank_group = [];
				if(!empty($bank_groups[$value->industry_main_id]))
					$bank_group = $bank_groups[$value->industry_main_id];

				if($bank->is_standalone == 0) {	
					$entity[$key]->report_type = "Full Rating";
				} elseif ($bank->is_standalone == 1) {
					if ($value->is_premium == PREMIUM_REPORT) {
						$entity[$key]->report_type = "Premium";
					} elseif($value->is_premium == SIMPLIFIED_REPORT) {
						$entity[$key]->report_type = "Simplified Rating";
					} else {
						$entity[$key]->report_type = "Standalone";
					}
				}

				if($bank_group && $entity[$key]->score_bank && $bank->is_standalone == 0){
					if($bank_formula){
						//bcc
						$bgroup_array = explode(',', $bank_formula->bcc);
						$bgroup_item = 0;
						$bgroup_total = 0;
						if(in_array('rm', $bgroup_array)) {
							$bgroup_item += $value->score_rm;
							$bgroup_total += MAX_RM;
						}
						if(in_array('cd', $bgroup_array)) {
							$bgroup_item += $value->score_cd;
							$bgroup_total += MAX_CD;
						}
						if(in_array('sd', $bgroup_array)) {
							$bgroup_item += $value->score_sd;
							$bgroup_total += MAX_SD;
						}
						if(in_array('boi', $bgroup_array)) {
							$bgroup_item += $value->score_bois;
							$bgroup_total += MAX_BOI;
						}

						if (0 < $bgroup_total) {
							$bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
						}
						else {
							$bgroup = 0;
						}

						//mq
						$mgroup_array = explode(',', $bank_formula->mq);
						$mgroup_item = 0;
						$mgroup_total = 0;
						if(in_array('boe', $mgroup_array)) {
							$mgroup_item += $value->score_boe;
							$mgroup_total += MAX_BOE;
						}
						if(in_array('mte', $mgroup_array)) {
							$mgroup_item += $value->score_mte;
							$mgroup_total += MAX_MTE;
						}
						if(in_array('bd', $mgroup_array)) {
							$mgroup_item += $value->score_bdms;
							$mgroup_total += MAX_BD;
						}
						if(in_array('sp', $mgroup_array)) {
							$mgroup_item += $value->score_sp;
							$mgroup_total += MAX_SP;
						}
						if(in_array('ppfi', $mgroup_array)) {
							$mgroup_item += $value->score_ppfi;
							$mgroup_total += MAX_PPFI;
						}

						if (0 < $mgroup_total) {
							$mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
						}
						else {
							$mgroup = 0;
						}

						//fa
						$fgroup_array = explode(',', $bank_formula->fa);
						if(@count($fgroup_array) == 2){
							$fgroup = $value->score_facs;
						} else {
							if($bank_formula->fa == 'fp'){
								$fgroup = $value->score_rfp;
							} elseif($bank_formula->fa == 'fr'){
								$fgroup = $value->score_rfpm;
							} else {
								$fgroup = 0;
							}
						}
					} else {
						$bgroup = ( $value->score_rm + $value->score_cd + $value->score_sd + $value->score_bois );
						$mgroup = ( $value->score_boe + $value->score_mte + $value->score_bdms + $value->score_sp + $value->score_ppfi );
						$fgroup = $value->score_facs;
					}

					$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

					if($bank_formula && $bank_formula->boost == 0){
						// no boost
					} else {
						//boost
						if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
						if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
						if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
					}

					//convert score
					if($gscore >= 177){
						$entity[$key]->score_bank		= 'AA - Excellent';
						$entity[$key]->bank_score_color = 'green';
					}
					else if($gscore >= 150 && $gscore <= 176) {
						$entity[$key]->score_bank = 'A - Strong';
						$entity[$key]->bank_score_color = 'green';
					}
					else if($gscore >= 123 && $gscore <= 149) {
						$entity[$key]->score_bank = 'BBB - Good';
						$entity[$key]->bank_score_color = 'orange';
					}
					else if($gscore >= 96 && $gscore <= 122) {
						$entity[$key]->score_bank = 'BB - Satisfactory';
						$entity[$key]->bank_score_color = 'orange';
					}
					else if($gscore >= 68 && $gscore <= 95) {
						$entity[$key]->score_bank = 'B - Some Vulnerability';
						$entity[$key]->bank_score_color = 'red';
					}
					else if($gscore < 68) {
						$entity[$key]->score_bank = 'CCC - Watchlist';
						$entity[$key]->bank_score_color = 'red';
					}
				}

				$company_names[]        = $entity[$key]->companyname;
				$search_entity          = array_keys($company_names, $entity[$key]->companyname);

				foreach ($search_entity as $remove_entity) {
					if ($entity[$key]->entityid > $entity[$remove_entity]->entityid) {
						if (!in_array($remove_entity, $delete_company)) {
							$delete_company[]   = $remove_entity;
						}
					}
				}

				//region assigning
				if ((NULL != $financialReport) && ('<span style="color:GREEN;">Completed</span>' == $value->status)) {
					//
				}else {
					$value->forecast_data  = NULL;
				}

				if(!in_array($value->regCode, $regions) && $value->regDesc != null)
				$regions[$value->regCode] = ["regCode" => $value->regCode,
											"regDesc" => $value->regDesc];
			}

			sort($regions);
			
			/**  allow duplicate company name as per CDP-1097 issue.
			 * foreach ($delete_company as $remove) {
			 * 	 unset($entity[$remove]);
			 * }
			 */

			$provinces  = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
			$industries = Industrymain::all()->pluck('main_title', 'industry_main_id');
			
			/* Get the List of Completed Reports*/ 
			$report_overview =  $entity->toArray();
			$completed_reports = array();
			usort($report_overview,function ($a, $b) {
				$t1 = strtotime($a->completed_date);
				$t2 = strtotime($b->completed_date);
				return $t2 - $t1;
			});

			if (!empty($report_overview)){
				if (@count($report_overview) >= 10){
					for($i = 0; $i < 10; $i++)
					{
						if($report_overview[$i]->completed_date && $report_overview[$i]->completed_date != "0000-00-00"){
							$completed_reports[] = $report_overview[$i];
						}
					}
				} else{
					for($i = 0; $i < @count($report_overview); $i++)
					{
						if($report_overview[$i]->completed_date && $report_overview[$i]->completed_date != "0000-00-00"){
							$completed_reports[] = $report_overview[$i];
						}
					}
				}
			}

			$supervisor_keys = DB::table('bank_keys')->select(
				DB::raw('SUM(CASE WHEN is_used = 0 THEN 1 ELSE 0 END) AS is_used_count'),
				DB::raw('COUNT(DISTINCT(bank_keys.serial_key)) AS all_keys_count')
				)
					->leftJoin('tblentity', 'bank_keys.entity_id', '=', 'tblentity.entityid')
					->leftJoin('tbllogin', 'bank_keys.login_id', '=', 'tbllogin.loginid')
					->where('bank_keys.bank_id', $this->accountDetails['bank_id'])
	                ->orderBy('bank_keys.id', 'desc')
					->first();

			$supervisor_count = $supervisor_keys->all_keys_count;
			
			$client_keys = array();
			$is_used = $supervisor_keys->is_used_count;
			$avg = 0;

			if($supervisor_count != 0){
				$avg = round(($is_used/$supervisor_count)*100);
			}
			$client_keys["is_used"] = $is_used;
			$client_keys["total_keys"] = $supervisor_count;
			$client_keys["key_average"] = $avg;
			return View::make('bank.dashboard', array(
				'provinces'                 => $provinces,
				'industries'                => $industries,
				'bank_name'                 => $bank->bank_name,
				'bank_subscription_note'    => $bank_subscription_note,
				'bank_subscription'         => ($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) ? true : false,
				'completed_reports'			=> $completed_reports,
				'client_keys'				=> $client_keys,
				'regions'					=> $regions
			))->with('entity', $entity);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.9: getBankReportList
	//  Description: get supervisor Accounts page. Previously getBankDashboard
    //-----------------------------------------------------
	public function getBankReportList($tertiary = null, $id_industry = null)
	{
		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();

	        $isSubscribed = (
	            $this->bank_subscription != null &&
	            strtotime($this->bank_subscription) > strtotime(date('c'))
			) ? true : false;

			$earningsManipulator = request('earnings_manipulator');
			
			//supervisor subscription
			$supAccess = true;
			if(Auth::user()->role == 4 && $organization->is_custom == 0) {
				$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",Auth::user()->loginid)->first();
				$now = date("Y-m-d H:i:s"); //created_at

				if($supSubscription->expiry_date < $now){
					$supAccess = false;
				}

			}

			if ($this->accountDetails['product_plan']==3 && $organization->is_custom == 0) { 			// if supervisor on plan C
	            $company_names      = array();				// used for container
				$delete_company     = array();				// used for container

				$entity = DB::table('tblentity')
					->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])
					->whereIn('tblentity.status', array(0, 1, 2, 7))
					->orderBy('tblentity.created_at', 'desc');

				$entity = $entity->groupBy('entityid')->get(); // get entities

	            foreach($entity as $key=>$value) {

						 $company_names[]        = $entity[$key]->companyname;
						$search_entity          = array_keys($company_names, $entity[$key]->companyname);

					foreach ($search_entity as $remove_entity) {
						if ($entity[$key]->entityid > $entity[$remove_entity]->entityid) {
							if (!in_array($remove_entity, $delete_company)) {
								$delete_company[]   = $remove_entity;
							}
						}
					}
	            }

				// delete entities flagged
	            foreach ($delete_company as $remove) {
					unset($entity[$remove]);
				}

				return View::make('bank.basic', array(
					'entity'=>$entity
				));
			} else if ($this->accountDetails['role'] === 4 && Auth::user()->role !== 0 && ($supAccess == false || !$isSubscribed) && $organization->is_custom == 0) {
				$bank = Bank::find($this->accountDetails['bank_id']);
	            $entity = DB::table('tblentity')
	               	 ->select(
	                    'tblentity.entityid',
	                    'tblentity.companyname',
						'tblentity.completed_date')
						->whereNotNull(
	                    'tblentity.completed_date');
						//->orderBy('tblentity.completed_date', 'desc');

	            if ($this->accountDetails['can_view_free_sme'] == 1) {
	                $entity->where(function($query){
	                    $query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
	                });
	            } else {
	                $entity->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
	            }

	            $entity = $entity->orderBy('tblentity.created_at', 'desc')->groupBy('entityid')->get();

	            return View::make(
	                'bank.dashboard-unpaid',
	                [
	                    'bank_name' => $bank->bank_name,
						'entity' => $entity
	                ]
	            );
			} else { // if supervisor on plan A or B
				$company_names      = array();
				$delete_company     = array();

				$entity = DB::table('tblentity')
						->select(
							'tbllogin.email as basic_email',
							'tblentity.entityid',
							'tblentity.industry_main_id', 
							'tblentity.industry_sub_id', 
							'tblentity.industry_row_id',
							'tblentity.companyname',
							'tblentity.email',
							'tblentity.date_established',
							'tblentity.number_year',
							'tblentity.anonymous_report',
							'tblentity.approval_status',
							'tblentity.approval_date',
							'tblentity.is_premium',
							'tblentity.utility_bill',
							'tblentity.cityid',
							'tblentity.province',
							'tblentity.sec_reg_date',
							'tblentity.status as report_status',
							'zipcodes.city',
							'tblsmescore.score_sf',
							'tblsmescore.score_rm',
							'tblsmescore.score_cd',
							'tblsmescore.score_sd',
							'tblsmescore.score_bois',
							'tblsmescore.score_boe',
							'tblsmescore.score_mte',
							'tblsmescore.score_bdms',
							'tblsmescore.score_sp',
							'tblsmescore.score_ppfi',
							'tblsmescore.score_rfp',
							'tblsmescore.score_rfpm',
							'tblsmescore.score_facs',
							'tblindustry_main.main_title',
							'tblsmescore.created_at as completed_date',
							'tblentity.status',
							'tbllogin.status as login_status',
							'tbllogin.loginid',
							'investigator_tag.id as ci_started',
							'investigator_notes.status as ci_status'
						)
						->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
						->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
						->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
						->leftJoin('tblsmescore', 'tblsmescore.entityid', '=', 'tblentity.entityid')
						->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
						->leftJoin('investigator_tag', 'investigator_tag.entity_id', '=', 'tblentity.entityid')
						->leftJoin('investigator_notes', 'investigator_notes.entity_id', '=', 'tblentity.entityid')
						->leftJoin('utility_bills', 'utility_bills.entity_id', '=', 'tblentity.entityid')
						->where('tblentity.isIndustry', 1)
						->where('tblentity.is_deleted', 0);

				$sEmail = request('email');

				if(!empty($sEmail)){
					$entity->whereRaw('tbllogin.email LIKE "%'.$sEmail.'%"');	
				}

				$sSmes = request('sme');
				if(!empty($smes)){
					$smeWhere = [];
					foreach ($smes as $sSmes) {
						$smeWhere[] = 'tblentity.entityid = '.$sSmes;
					}

					$smeWhere = implode(" OR ",$smeWhere);

					if(!empty($smeWhere)){
						$entity->whereRaw($smeWhere);
					}
				}
				
				$sRegion = request('region');
				if(!empty($sRegion)){
					$entity->where('tblentity.province',$sRegion);
				}

				$sIndustrySection = request('industry_section');
				if(!empty($sIndustrySection)){
					$entity->where('tblentity.industry_main_id',$sIndustrySection);
				}

				$sIndustryDivision = request('industry_division');
				if(!empty($sIndustryDivision)){
					$entity->where('tblentity.industry_sub_id',$sIndustryDivision);
				}

				$sIndustryGroup = request('industry_group');
				if(!empty($sIndustryGroup)){
					$entity->where('tblentity.industry_row_id',$sIndustryGroup);
				}

				$ratings = ['AAA','AA','A','BBB','BB','B','CCC','CC','C','D','E'];
				$sBccFrom = request('bcc_from');
				$sBccTo = request('bcc_to');

				$ratingsNewArr = [];
				if(!empty($sBccFrom) && !empty($sBccTo)){

					$bccFromIndx = array_search($sBccFrom,$ratings);
					$bccToIndx = array_search($sBccTo,$ratings);

					if($bccToIndx > $bccFromIndx){
						for($i=$bccFromIndx;$i<=$bccToIndx;$i++){
							$ratingsNewArr[$i] = $ratings[$i];
						}
					}else{
						for($i=$bccToIndx;$i<=$bccFromIndx;$i++){
							$ratingsNewArr[$i] = $ratings[$i];
						}
					}
				}

				
				// subscription
				$bank_subscription_note = '';

				if (($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) && $organization->is_custom == 0) {

	                $bank_login_id = $this->accountDetails['loginid'];

	                if ($this->accountDetails['parent_user_id'] != 0) {
	                    $bank_login_id = $this->accountDetails['parent_user_id'];
	                }

	                /*----------------------------------------------------------
	                |	Check Paypal Subscription
	                |---------------------------------------------------------*/
					$subs_sts   = self::checkSubscriptionSts($bank_login_id);

	                if ($this->accountDetails['email'] != "testbank@palatine.test.com" && STS_NG == $subs_sts) {
	                    return Redirect::to('/bank/subscribe');
	                }
	                else if ($this->accountDetails['email'] != "testbank@palatine.test.com" && 2 == $subs_sts) {
	                    $subscription   = Transaction::where('loginid', $bank_login_id)
	                        ->where('description', 'Bank User Subscription')
	                        ->orderBy('id', 'desc')
	                        ->first();

	                    /* Redirect to Pending Screen */
	                    return View::make('payment.pending', array('refno' => $subscription['refno']));
	                } else {
	                    /** No Processing   */
	                }

					
					$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
					
				} else if ($this->accountDetails['email'] != "testbank@palatine.test.com" && Auth::user()->role !== 0 && $organization->is_custom == 0) {
					/* Redirect to Payment */
	                return Redirect::to('/bank/subscribe');
				} else if (Auth::user() !== 0) {
					/* User accessing using admin account*/
					
					$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
					
				}

				if ($this->accountDetails['can_view_free_sme'] == 1) {
					$entity->where(function($query){
						$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$entity->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
				}

				if($id_industry){
					if($tertiary == 1){ //industry main
						$entity->where('tblentity.industry_main_id', '=', $id_industry);

						$industryValue = $id_industry;
					}else if($tertiary == 2){ //industry sub
						$entity->where('tblentity.industry_sub_id', '=', $id_industry);
						$main_code = Industrysub::where('sub_code', $id_industry)->pluck('main_code')->first();
						$industryValue = $main_code;
					}else if($tertiary == 3){ //industry class
						$entity->where('tblentity.industry_row_id', '=', $id_industry);
						$groupcode = DB::table('tblindustry_class')->where('class_code', $id_industry)->pluck('group_code')->first();
						$subcode = DB::table('tblindustry_group')->where('group_code', $groupcode)->pluck('sub_code')->first();
						$main_code = Industrysub::where('sub_code', $subcode)->pluck('main_code')->first();
						$industryValue = $main_code;
					}
				}else if($id_industry == null){
					$industryValue = $id_industry;
				}

				$entity = $entity->orderBy('tblentity.created_at', 'desc')->groupBy('entityid')->get();
				$bank = Bank::find($this->accountDetails['bank_id']);


				// process entity and update fields to readable content
				foreach($entity as $key=>$value){
					if ($value->status == 7) {
						$entity[$key]->status = '<span style="color:GREEN;">Completed</span>';
						$entity[$key]->val = "Completed";
						$notes_count        = $value->ci_status;
						$tag_count          = $value->ci_started;
						if ($notes_count && $notes_count>0) {
							$invst_sts      = '<a href = "'.URL::to('summary/smesummary').'/'.$entity[$key]->entityid.'/ci_view" > <span style="color:blue;">Verified</span> </a>';
						} elseif ($tag_count) {
							$invst_sts      = '<span style="color:green;">In Review</span>';
						} else {
							$invst_sts      = '<span style="color:red;">Awaiting Assignment</span>';
						}
					} else {
						if($value->login_status == 0 || $value->login_status == 1){
							$entity[$key]->status       = '<span style="color:GREY;">Not Started</span>';
							$entity[$key]->val 			= "Not Started";
							$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
						} else {
							$entity[$key]->status       = '<span style="color:RED;">In Progress</span>';
							$entity[$key]->val 			= "In Progress";
							$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
						}
					}
					$entity[$key]->invst_sts		= $invst_sts;
					$entity[$key]->pdefault			= STR_EMPTY;
					$entity[$key]->cbpo_score_color = STR_EMPTY;
					$entity[$key]->bank_score_color = STR_EMPTY;
					$entity[$key]->m_score = App::make('ProfileSMEController')->computeBeneishScore($entity[$key]->entityid);

					$entity[$key]->earnings_manipulator = '';
					if($entity[$key]->val != "In Progress")
		            {    
		                if(number_format($entity[$key]->m_score, 2) >= -2.22){
		                    $entity[$key]->earnings_manipulator = 'Likely';
		                }else{
		                    $entity[$key]->earnings_manipulator = 'Unlikely';
		                }                
		            }

		            $entity[$key]->earnings_manipulator_hidden = 0;
		            
		            if(!empty($earningsManipulator)){
		            	if($earningsManipulator != $entity[$key]->earnings_manipulator){
		            		$entity[$key]->earnings_manipulator_hidden = 1;
		            	}
		            }
					

					// update score as readable
					$financialChecker = false;
					$financialTotal = 0;


					$financialReport = FinancialReport::where(['entity_id' => $entity[$key]->entityid, 'is_deleted' => 0])
								->orderBy('id', 'desc')
								->first();
					if($value->report_status == 7 && $financialReport){
						// $finalRatingScore = ActionController::financialRatingComputation($financialReport->id);
						$ac = new FinancialHandler();
						$finalRatingScore = $ac->financialRatingComputation($financialReport->id);
						if($finalRatingScore) {
							$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
							$financialChecker = true;
						}
					}
					if($financialChecker){
						//Financial Position
						if($financialTotal >= 1.6) {
							$entity[$key]->score_sf          = "AAA - Excellent";
							$entity[$key]->cbpo_score_color  = 'green';
							$entity[$key]->pdefault          = '1% - Very Low Risk';
						} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
							$entity[$key]->score_sf          = "AA - Very Good";
							$entity[$key]->cbpo_score_color  = 'green';
							$entity[$key]->pdefault          = '2% - Very Low Risk';
						} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
							$entity[$key]->score_sf          = "A - Good";
							$entity[$key]->cbpo_score_color  = 'green';
							$entity[$key]->pdefault          = '3% - Low Risk';
						} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
							$entity[$key]->score_sf          = "BBB - Positive";
							$entity[$key]->cbpo_score_color  = 'orange';
							$entity[$key]->pdefault          = '4% - Moderate Risk';
						} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
							$entity[$key]->score_sf          = "BB - Normal";
							$entity[$key]->cbpo_score_color  = 'orange';
							$entity[$key]->pdefault          = '6% - Moderate Risk';
						} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
							$entity[$key]->score_sf   	 	 = "B - Satisfactory";
							$entity[$key]->cbpo_score_color  = 'red';
							$entity[$key]->pdefault          = '8% - Moderate Risk';
						} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
							$entity[$key]->score_sf          = "CCC - Unsatisfactory";
							$entity[$key]->cbpo_score_color  = 'red';
							$entity[$key]->pdefault          = '15% - High Risk';
						} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
							$entity[$key]->score_sf          = "CC - Adverse";
							$entity[$key]->cbpo_score_color  = 'red';
							$entity[$key]->pdefault          = '20% - High Risk';
						} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
							$entity[$key]->score_sf          = "C - Bad";
							$entity[$key]->cbpo_score_color  = 'red';
							$entity[$key]->pdefault          = '25% - Very High Risk';
						} else {
							$entity[$key]->score_sf          = "D - Critical";
							$entity[$key]->cbpo_score_color  = 'red';
							$entity[$key]->pdefault          = '32% - Very High Risk';
						}
					} elseif ($value->score_sf){
						$cred_rating_exp    = explode('-', $value->score_sf);
						$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
						$rate_val   = intval($rate_val);

						if($rate_val >= 177){
							$entity[$key]->score_sf			= 'AA - '.$cred_rating_exp[1];
							$entity[$key]->cbpo_score_color = 'green';
							$entity[$key]->pdefault			= '2% - Very Low Risk';
						}
						else if($rate_val >= 150 && $rate_val <= 176) {
							$entity[$key]->score_sf 		= 'A - '.$cred_rating_exp[1];
							$entity[$key]->cbpo_score_color = 'green';
							$entity[$key]->pdefault 		= '3% - Low Risk';
						}
						else if($rate_val >= 123 && $rate_val <= 149) {
							$entity[$key]->score_sf 		= 'BBB - '.$cred_rating_exp[1];
							$entity[$key]->cbpo_score_color = 'orange';
							$entity[$key]->pdefault 		= '4% - Moderate Risk';
						}
						else if($rate_val >= 96 && $rate_val <= 122) {
							$entity[$key]->score_sf			= 'BB - '.$cred_rating_exp[1];
							$entity[$key]->cbpo_score_color = 'orange';
							$entity[$key]->pdefault			= '6% - Moderate Risk';
						}
						else if($rate_val >= 68 && $rate_val <= 95) {
							$entity[$key]->score_sf			= 'B - Some Vulnerability';
							$entity[$key]->cbpo_score_color = 'red';
							$entity[$key]->pdefault			= '8% - High Risk';
						}
						else if($rate_val < 68) {
							$entity[$key]->score_sf 		= 'CCC - '.$cred_rating_exp[1];
							$entity[$key]->cbpo_score_color = 'red';
							if($rate_val >= 42 && $rate_val <= 68) {
								$entity[$key]->pdefault = '15% - High Risk';
							} else {
								$entity[$key]->pdefault = '30% - Very High Risk';
							}
						}
					}

					if(!empty($ratingsNewArr)){
						$scoreSfRaw = $entity[$key]->score_sf;
						$scoreSfExp = explode('-',$scoreSfRaw);
						$scoreSfRating = trim($scoreSfExp[0]);

						if(!in_array($scoreSfRating,$ratingsNewArr)){
							$entity[$key]->hideRating = 1;
						}else{
							$entity[$key]->hideRating = 0;
						}
					}else{
						$entity[$key]->hideRating = 0;
					}

					$entity[$key]->score_bank = $entity[$key]->score_sf;
					$entity[$key]->bank_score_color = $entity[$key]->cbpo_score_color;
					// bank score computation
					
					$bank_group = BankIndustry::where('bank_id', $this->accountDetails['bank_id'])
							->where('industry_id', $value->industry_main_id)->first();
					$bank_formula = BankFormula::where('bank_id', $this->accountDetails['bank_id'])->first();
					if($bank->is_standalone == 0) {	
						$entity[$key]->report_type = "Full Rating";
					} elseif ($bank->is_standalone == 1) {
						if ($value->is_premium == PREMIUM_REPORT) {
							$entity[$key]->report_type = "Premium";
						} elseif ($value->is_premium == SIMPLIFIED_REPORT) {
							$entity[$key]->report_type = "Simplified Rating";
						} else {
							$entity[$key]->report_type = "Standalone";
						}
					}

					if($bank_group && $entity[$key]->score_bank && $bank->is_standalone == 0){
						if($bank_formula){
							//bcc
							$bgroup_array = explode(',', $bank_formula->bcc);
							$bgroup_item = 0;
							$bgroup_total = 0;
							if(in_array('rm', $bgroup_array)) {
								$bgroup_item += $value->score_rm;
								$bgroup_total += MAX_RM;
							}
							if(in_array('cd', $bgroup_array)) {
								$bgroup_item += $value->score_cd;
								$bgroup_total += MAX_CD;
							}
							if(in_array('sd', $bgroup_array)) {
								$bgroup_item += $value->score_sd;
								$bgroup_total += MAX_SD;
							}
							if(in_array('boi', $bgroup_array)) {
								$bgroup_item += $value->score_bois;
								$bgroup_total += MAX_BOI;
							}

							if (0 < $bgroup_total) {
								$bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
							} else {
								$bgroup = 0;
							}

							//mq
							$mgroup_array = explode(',', $bank_formula->mq);
							$mgroup_item = 0;
							$mgroup_total = 0;
							if(in_array('boe', $mgroup_array)) {
								$mgroup_item += $value->score_boe;
								$mgroup_total += MAX_BOE;
							}
							if(in_array('mte', $mgroup_array)) {
								$mgroup_item += $value->score_mte;
								$mgroup_total += MAX_MTE;
							}
							if(in_array('bd', $mgroup_array)) {
								$mgroup_item += $value->score_bdms;
								$mgroup_total += MAX_BD;
							}
							if(in_array('sp', $mgroup_array)) {
								$mgroup_item += $value->score_sp;
								$mgroup_total += MAX_SP;
							}
							if(in_array('ppfi', $mgroup_array)) {
								$mgroup_item += $value->score_ppfi;
								$mgroup_total += MAX_PPFI;
							}

							if (0 < $mgroup_total) {
								$mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
							} else {
								$mgroup = 0;
							}

							//fa
							$fgroup_array = explode(',', $bank_formula->fa);
							if(@count($fgroup_array) == 2){
								$fgroup = $value->score_facs;
							} else {
								if($bank_formula->fa == 'fp'){
									$fgroup = $value->score_rfp;
								} elseif ($bank_formula->fa == 'fr'){
									$fgroup = $value->score_rfpm;
								} else {
									$fgroup = 0;
								}
							}
						} else {
							$bgroup = ( $value->score_rm + $value->score_cd + $value->score_sd + $value->score_bois );
							$mgroup = ( $value->score_boe + $value->score_mte + $value->score_bdms + $value->score_sp + $value->score_ppfi );
							$fgroup = $value->score_facs;
						}

						$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));
						if($bank_formula && $bank_formula->boost == 0){
							// no boost
						} else {
							//boost
							if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
							if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
							if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
						}

						//convert score
						if($gscore >= 177){
							$entity[$key]->score_bank		= 'AA - Excellent';
							$entity[$key]->bank_score_color = 'green';
						} elseif ($gscore >= 150 && $gscore <= 176) {
							$entity[$key]->score_bank = 'A - Strong';
							$entity[$key]->bank_score_color = 'green';
						} elseif ($gscore >= 123 && $gscore <= 149) {
							$entity[$key]->score_bank = 'BBB - Good';
							$entity[$key]->bank_score_color = 'orange';
						} elseif ($gscore >= 96 && $gscore <= 122) {
							$entity[$key]->score_bank = 'BB - Satisfactory';
							$entity[$key]->bank_score_color = 'orange';
						} elseif ($gscore >= 68 && $gscore <= 95) {
							$entity[$key]->score_bank = 'B - Some Vulnerability';
							$entity[$key]->bank_score_color = 'red';
						} elseif ($gscore < 68) {
							$entity[$key]->score_bank = 'CCC - Watchlist';
							$entity[$key]->bank_score_color = 'red';
						}
					}
					$company_names[]        = $entity[$key]->companyname;
					$search_entity          = array_keys($company_names, $entity[$key]->companyname);

					foreach ($search_entity as $remove_entity) {
						if ($entity[$key]->entityid > $entity[$remove_entity]->entityid) {
							if (!in_array($remove_entity, $delete_company)) {
								$delete_company[]   = $remove_entity;
							}
						}
					}

				}
				
				
				/**  allow duplicate company name as per CDP-1097 issue.
				 * foreach ($delete_company as $remove) {
				 * 	 unset($entity[$remove]);
				 * }
				 */
				$provinces = Province2::select('provDesc', 'provCode')->orderBy('provDesc')->pluck('provDesc', 'provCode');
				$industries = Industrymain::all()->pluck('main_title', 'main_code');

	            /*  Gets the Growth Forecast        */
	            foreach ($entity as $report) {
	                $fa_report = FinancialReport::where(['entity_id' => $report->entityid, 'is_deleted' => 0])
	                    ->orderBy('id', 'desc')
	                    ->first();
	                if ((NULL != $fa_report) && ('<span style="color:GREEN;">Completed</span>' == $report->status)) {
	                    // $gf_lib                 = new GrowthForecastLib($report->entityid, $report->industry_main_id, $bank->is_standalone);
	                    // $report->forecast_data  = $gf_lib->getGrowthForecastData();

						$financial = new FinancialReport();
						$financialStatement = $financial->getFinancialReportById($report->entityid);

						$fa_report->balance_sheets      = $financialStatement['balance_sheets'];
						$fa_report->income_statements   = $financialStatement['income_statements'];
						$fa_report->cashflow            = $financialStatement['cashflow'];

						/*---------------------------------------------------------------
						--------Computation for the Industry Comparison (Company) -------
						-----------------------------------------------------------------*/
						$company_industry = array();
						$max_count = count($fa_report->balance_sheets)-2;
						
						/** Company Gross Revenue Growth */
						$company_industry['gross_revenue_growth'] = 0;
						if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
							$PercentageChange = 0;
							$company_industry['gross_revenue_growth'] = 0;
							$dividend = $fa_report->income_statements[$max_count]->Revenue;
		
							if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
								$PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
							}
		
							if($PercentageChange > 200){
								$company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
							}else{
								$company_industry['gross_revenue_growth'] = round($PercentageChange,2);
							}
						}

						$report->revenue_growth  = $company_industry['gross_revenue_growth'];
	                } else {
	                    // $report->forecast_data  = NULL;
	                }

					if($fa_report != NULL){
						// Compute Years in Business
						$end_year = DB::table('financial_reports')->where('entity_id', $report->entityid)->pluck('year')->first();

						// echo '<pre>';
						// print_r($end_year);
						// echo '</pre>';
						// exit;

						$start_year = explode("-",$report->sec_reg_date);
						//echo print_r($start_year); exit;
						$report->years_in_business =  intval($end_year) - intval($start_year[0]);
					}else{
						$report->years_in_business = '';
					}
				}

				return View::make('bank.reportlist', array(
					'provinces'                 => $provinces->toArray(),
					'industries'                => $industries->toArray(),
					'smeSelected' => $sSmes ? $sSmes : [],
					'bccFrom' => $sBccFrom,
					'bccTo' => $sBccTo,
					'email' => $sEmail,
					'region' => $sRegion,
					'earningsManipulator' => $earningsManipulator,
					'industry_section' => $sIndustrySection,
					'industry_division' => $sIndustryDivision,
					'industry_group' => $sIndustryGroup,
					'bank_name'                 => $bank->bank_name,
					'bank_subscription_note'    => $bank_subscription_note,
					'bank_subscription'         => ($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) ? true : false,
					'industryValue'			    => $industryValue,
				))->with('entity', $entity);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.11: getBankKeys
	//  Description: get supervisor Client Keys page
    //-----------------------------------------------------
	public function getBankKeys()
	{
		try{
			Permissions::check(3);

			if(empty($this->accountDetails['bank_id'])){
				return redirect('/');
			}			

			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();

			\Log::info($this->accountDetails['product_plan']);

			$subscription = 0;
			/*comment out for the mean time to allow unpaid supervisors to access client keys*/
	        if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
	            $subscription = $this->_returnSubscriptions();
	        }

	        $filter = Input::get('filter');

			$entity = DB::table('bank_keys')->select('bank_keys.is_used','bank_keys.serial_key','bank_keys.type','tblentity.email','tblentity.entityid','tblentity.anonymous_report','tblentity.created_at','tbllogin.firstname','tbllogin.lastname','tblentity.companyname','tblentity.phone')
					->leftJoin('tblentity', 'bank_keys.entity_id', '=', 'tblentity.entityid')
					->leftJoin('tbllogin', 'bank_keys.login_id', '=', 'tbllogin.loginid')
					->groupBy('bank_keys.serial_key')
					->where('bank_keys.bank_id', $this->accountDetails['bank_id']);

			if($filter == 'Registered'){
				$entity = $entity->where('bank_keys.is_used', 1);
			}elseif($filter == 'Unregistered'){
				$entity = $entity->where('bank_keys.is_used', 0);
			}

	        $entity = $entity->orderBy('bank_keys.id', 'desc')
	                ->get();

			$used = 0;
			$unused = 0;
			foreach($entity as $e){
				if($e->is_used==1) {
					$used++;
				} else {
					$unused++;
				}
			}
			return View::make('bank.keys', array(
				'filter' => $filter,
				'used' => $used,
				'unused' => $unused,
				'subscription' => $subscription,
				'another' => Session::get('another')
			))->with('entity', $entity);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.12: getPurchaseBankKeys
	//  Description: get purchase Client Keys form page
    //-----------------------------------------------------
	public function getPurchaseBankKeys()
	{
		try{
			Permissions::check(3);

			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			
			//       if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
			//               $result = $this->_checkAllSubscriptionConditions();
			//               if($result != "")
			//                   return $result;
			// }

			return View::make('bank.purchase_keys', array(
				'organization'  => $organization,
				'reportType'	=> json_decode($organization->report_type)
			));	
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getClientKeyPrice(){
		$bankid = Input::get('bankid');
		$keyType = Input::get('keyType');

		$price = 0;

		if($keyType == "Simplified"){
			$price = Bank::where('id', $bankid)->pluck('simplified_price')->first();
		}else if($keyType == "Standalone"){
			$price = Bank::where('id', $bankid)->pluck('standalone_price')->first();
		}else if($keyType == "Premium Zone 1"){
			$price = Bank::where('id', $bankid)->pluck('premium_zone1_price')->first();
		}else if($keyType == "Premium Zone 2"){
			$price = Bank::where('id', $bankid)->pluck('premium_zone2_price')->first();
		}else{
			$price = Bank::where('id', $bankid)->pluck('premium_zone3_price')->first();
		}
		
		return number_format($price,2,".",",");
	}

	public function getClientKeyPriceValue($bank_id,$keyType){
		$calculator = $this->paymentCalculator;
		$price = $calculator::CLIENTKEY_PRICE;

		$bank = Bank::where('id', '=', $bank_id)->first();

		if($bank->is_custom != 0) { //check if custom price for reports is on
			$simplified_standalone_price = $bank->simplified_price;
			$premium_zone1_price = $bank->premium_zone1_price;
			$premium_zone2_price = $bank->premium_zone2_price;
			$premium_zone3_price = $bank->premium_zone3_price;

		} else {
			$simplified_standalone_price = $price;
			$premium_zone1_price = $price;
			$premium_zone2_price = $price;
			$premium_zone3_price = $price;
		}

		switch($keyType){
			case 'Simplified':
				$price = $simplified_standalone_price;
				break;
			case 'Standalone':
				$price = $simplified_standalone_price;
				break;
			case 'Premium Zone 1':
				$price = $premium_zone1_price;
				break;
			case 'Premium Zone 2':
				$price = $premium_zone2_price;
				break;
			case 'Premium Zone 3':
				$price = $premium_zone3_price;
				break;
		}

		return $price;
	}

	//-----------------------------------------------------
    //  Function 3.13: postPurchaseBankKeys
	//  Description: post process for purchase Client Keys form page
    //-----------------------------------------------------
	public function postPurchaseBankKeys()
	{
		
		try{
			$rules = array(							// validation rules
		        'quantity' => 'required|numeric|min:1',
				'payment_option' => 'required',
				'agree' => 'required'
			);

			$customMessages = array(
				'agree.required' => 'Please read and agree to the Terms of Use and Privacy Policy, and then check the box below'
			);

	        $quantity_boom = explode('.', Input::get('quantity'));

			$validator = Validator::make(Input::all(), $rules, $customMessages);		// run validation

			if ($validator->fails()) { // if fail
				return Redirect::to('/bank/purchase_keys')->withErrors($validator->errors())->withInput();
			}
	        else if (isset($quantity_boom[1])) { // check for decimal point and return error
	            return Redirect::to('/bank/purchase_keys')->withErrors(array('quantity' => 'Only integers are allowed'))->withInput();
	        }
			else
			{
				// get input items
				$quantity           = Input::get('quantity');
				$discount_code      = Input::get('discount_code');
				$payment_option     = Input::get('payment_option');

				/* Set price information */
				//JM - paymentcalculator class will be the key to updating the clientkey price or report price
				//I will query in tblbank using loginid and get the setting if the the price per report is activated.
				
				$calculator = $this->paymentCalculator;

				$user = DB::table('tbllogin')
	                    ->where('loginid', $this->accountDetails['loginid'])
						->first();
				
				// if($user->bank_id != 0) { //check if there is an assigned bank to the supervisor
				// 	$bank = Bank::where('id', '=', $user->bank_id)->first();

				// 	if($bank->is_custom != 0) { //check if custom price for reports is on
				// 		$price = $bank->custom_price;
				// 	} else {
				// 		$price = $calculator::CLIENTKEY_PRICE;
				// 	}
				// } else { //default price if there is no assigned bank
				// 	$price = $calculator::CLIENTKEY_PRICE;
				// }

				$price = $calculator::CLIENTKEY_PRICE;
				$keyType = Input::get('keyType');
				if($user->bank_id != 0) { //check if there is an assigned bank to the supervisor
					$price = Self::getClientKeyPriceValue($user->bank_id,$keyType);
				}

				$discount = null;
				$discountType = null;

				if ($discount_code){
					$discountDb = new Discount;
					$discount_data = $discountDb->getDiscountByCode($discount_code);

					if ($discount_data) {										// check if discounted
						$discount = $discount_data->amount;
						$discountType = $discount_data->type;
					} else{
						return Redirect::to('/bank/purchase_keys')->withErrors(array('discount_code' => 'Invalid discount code'))->withInput();
					}
				}
				/* Compute Payment */
				$result = $calculator->paymentCompute($price, $quantity, $discount, $discountType);
				if (0 >= $result['totalAmount']) {		// if amount totals to 0 or free
	                BankSerialKeys::createKeys($this->accountDetails['loginid'], $quantity);
					return Redirect::to('/bank/keys');	// directly create keys
				}
				
				/* Create payment details */
				$paymentReceiptDb = new PaymentReceipt;

				$paymentReceipt['price'] = $price;
				$paymentReceipt['amount'] = $result['amount'];
				$paymentReceipt['discount'] = $result['discountAmount'];
				$paymentReceipt['subtotal'] = $result['subTotal'];
				$paymentReceipt['vat_amount'] = $result['taxAmount'];
				$paymentReceipt['total_amount'] = $result['totalAmount'];
				$paymentReceipt['item_quantity'] = $quantity;
				$paymentReceipt['login_id'] = $this->accountDetails['loginid'];
				$paymentReceipt['item_type'] = 3;            /* Client Keys */
				$paymentReceipt['sent_status'] = STS_NG;
				$paymentReceipt['date_sent'] = date('Y-m-d');
				/* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
				$paymentReceipt['vatable_amount'] = null;

				/** Payment process for Paypal */
				if ($payment_option=="paypal") {

	                $paymentReceipt['reference_id'] = 0;
	                $paymentReceipt['payment_method'] = 1; /*PayPal*/
	                $receipt_sts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

					$params = array(
						'cancelUrl' => URL::to('/').'/bank/keys',
						'returnUrl' => URL::to('/').'/payment/paymentsuccess',
						'name'	=> 'CreditBPO Services',
						'description' => 'Credit Risk Rating Basic Package (x'.$quantity.')',
						'amount' => number_format($result['totalAmount'], 2, '.', ''),
						'currency' => 'PHP',
	                    'reference_id'  => $receipt_sts
					);
					
					Session::put('params', $params);
					Session::save();

					$gateway = Omnipay::create('PayPal_Express');

					if(env("APP_ENV") == "Production"){
						$gateway->setUsername('lia.francisco_api1.gmail.com');
						$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
						$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
						$gateway->setTestMode(false);
					} else {
						$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
						$gateway->setPassword('87DWVXH4UGHD46W3');
						$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
						$gateway->setTestMode(true);
					}
					$response = $gateway->purchase($params)->send();
					$response->redirect();

				}
				/* Payment process for DragonPay */
	            else if ($payment_option=="dragonpay") {
					$transaction = new Transaction();
					$transaction->loginid = $this->accountDetails['loginid'];
					$transaction->amount = $result['totalAmount'];
					$transaction->ccy = "PHP";
					$transaction->description = "Credit Risk Rating Basic Package Keys (x".$quantity.")";
					$transaction->save();

					$user = User::find($this->accountDetails['loginid']);
					$user->transactionid = $transaction->id;
					$user->save();

	                $paymentReceipt['reference_id'] = $transaction->id;
	                $paymentReceipt['payment_method'] = 2; /*DragonPay*/
	                $receipt_sts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);
					$rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
					$data = array(
						'merchant_id' => $this->merchant_id,
						'txn_id' => $transaction->id .$rand,
						'amount' => number_format($transaction->amount, 2, ".", ""),
						'currency' => $transaction->ccy,
						'description' => $transaction->description,
						'email' => $this->accountDetails['email'],
						'key' => $this->merchant_key
					);

					$seed = implode(':', $data);
					$digest = Sha1($seed);

					return Redirect::to(
						$this->url.
						"?merchantid=".$data['merchant_id'].
						"&txnid=".$data['txn_id'].
						"&amount=".$data['amount'].
						"&ccy=".$data['currency'].
						"&description=".urlencode($data['description']).
						"&email=".urlencode($data['email']).
						"&digest=".$digest
					);
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.14: getExportBankKeys
	//  Description: function for Exporting Unused Keys
    //-----------------------------------------------------
	public function getExportBankKeys()
	{
		try{
			Permissions::check(3);

	        // if ($this->accountDetails['product_plan'] != 3) {
	        //         $result = $this->_checkAllSubscriptionConditions();
	        //         if($result != "")
	        //             return $result;
	        // }

			$table = BankSerialKeys::select('serial_key')->where('bank_id', $this->accountDetails['bank_id'])->where('is_used', 0)->get();
			$file = fopen('temp/file.csv', 'w');		// filename
			foreach ($table as $row) {
				fwrite($file, $row->serial_key . "\n");		// create file are write
			}
			fclose($file);
			return Response::download('temp/file.csv', 'unused_serial_keys.csv');  // stream for download
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 3.64: getReportsbyRegion
    //  Description: Get reports based on region
    //-----------------------------------------------------
    
	//-----------------------------------------------------
    //  Function 3.15: getStatistics
	//  Description: get SME statistics page
    //-----------------------------------------------------
	public function getStatistics()
	{
		try{
			$banks = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			$is_standalone = $banks->is_standalone;
			if($this->accountDetails['product_plan'] == 3){
				App::abort(404);
			}
			Permissions::check(4);

			$result = $this->_checkAllSubscriptionConditions();
			if($result != ""){
				return $result;
			}

			// filters
			$sme_list = DB::table('tblentity')->select('tblentity.entityid','tblentity.companyname')->where('status', 7)->where('is_deleted', 0);
			if($this->accountDetails['can_view_free_sme']==1){
				$sme_list->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$sme_list->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			if($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
				//do nothing
			} else {
				$sme_list->take(BANK_SME_LIMIT);
			}
			$sme_list = $sme_list->groupBy('entityid')->pluck('companyname', 'entityid');

			//JM - changed list to pluck
			$main_industry_list = Industrymain::all()->pluck('main_title', 'main_code');
			$region = Region::select('regDesc', 'regCode')->pluck('regDesc', 'regCode');

			return View::make('bank.statistics', array(
				'sme_list' => $sme_list->toArray(),
				'sme_selected' => array(),
				'main_industry_list' => $main_industry_list->toArray(),
				'main_industry_selected' => '',
				'sub_industry_list' => array(),
				'sub_industry_selected' => '',
				'industry_list' => array(),
				'industry_selected' => '',
				'bcc_from' => '',
				'bcc_to' => '',
				'mq_from' => '',
				'mq_to' => '',
				'fc_from' => '',
				'fc_to' => '',
				'rating_from' => '',
				'rating_to' => '',
				'brating_from' => '',
				'brating_to' => '',
				'type' => 1,
				'data' => null,
				'region_list' => $region->toArray(),
				'is_standalone' => $is_standalone
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}


	//-----------------------------------------------------
    //  Function 3.16: postStatistics
	//  Description: function for getting SME statistics graph
    //-----------------------------------------------------
	public function postStatistics()
	{
		try{
			if(Auth::user()->product_plan==3){
				App::abort(404);
			}

			/** Filters  */
			$sme_selected = Input::get('sme');
			$main_industry_selected = Input::get('industry1');
			$sub_industry_selected = Input::get('industry2');
			$industry_selected = Input::get('industry3');
			$region_selected = Input::get('region');
			$pdefault_from = Input::get('pdefault_from');
			$pdefault_to = Input::get('pdefault_to');

			// $industry = array($main_industry_selected, $sub_industry_selected, $industry_selected);

			/** Major Customer Filters */
			$with_majCustFilter = Input::get('with_majCustFilter');
			$yearsDoingBusiness_from = Input::get('yearsDoingBusiness_from');
			$yearsDoingBusiness_to = Input::get('yearsDoingBusiness_to');
			$percentShareSales_from = Input::get('percentShareSales_from');
			$percentShareSales_to = Input::get('percentShareSales_to');
			$orderFrequency = Input::get('orderFrequency');
			$paymentBehavior = Input::get('paymentBehavior');
			$relationshipSatisfaction = Input::get('relationshipSatisfaction');
		
			/** Major SUpplier Filters */
			$with_majSuppFilter = Input::get('with_majSuppFilter');
			$supp_yearsDoingBusiness_from = Input::get('supp_yearsDoingBusiness_from');
			$supp_yearsDoingBusiness_to = Input::get('supp_yearsDoingBusiness_to');
			$supp_percentShareSales_from = Input::get('supp_percentShareSales_from');
			$supp_percentShareSales_to = Input::get('supp_percentShareSales_to');
			$paymentBehaviorSupplier = Input::get('paymentBehaviorSupplier');
			$purchaseFrequency = Input::get('purchaseFrequency');

			$pdfrom = 0;									// default minimum
			$pdto = 250;									// default maximum

			// set score ranges
			if($pdefault_from >= 0 && $pdefault_from <= 2){
				$pdto = 250;
			} elseif($pdefault_from > 2 && $pdefault_from <= 3){
				$pdto = 176;
			} elseif($pdefault_from > 3 && $pdefault_from <= 4){
				$pdto = 149;
			} elseif($pdefault_from > 4 && $pdefault_from <= 6){
				$pdto = 122;
			} elseif($pdefault_from > 6 && $pdefault_from <= 8){
				$pdto = 95;
			} elseif($pdefault_from > 8 && $pdefault_from <= 15){
				$pdto = 67;
			} elseif($pdefault_from > 15 && $pdefault_from <= 30){
				$pdto = 41;
			} elseif($pdefault_from > 30 && $pdefault_from <= 50){
				$pdto = 31;
			} elseif($pdefault_from > 50 && $pdefault_from <= 80){
				$pdto = 21;
			} elseif($pdefault_from > 80 && $pdefault_from <= 100){
				$pdto = 11;
			}

			// set score ranges
			if($pdefault_to >= 0 && $pdefault_to <= 2){
				$pdfrom = 177;
			} elseif($pdefault_to > 2 && $pdefault_to <= 3){
				$pdfrom = 150;
			} elseif($pdefault_to > 3 && $pdefault_to <= 4){
				$pdfrom = 123;
			} elseif($pdefault_to > 4 && $pdefault_to <= 6){
				$pdfrom = 96;
			} elseif($pdefault_to > 6 && $pdefault_to <= 8){
				$pdfrom = 68;
			} elseif($pdefault_to > 8 && $pdefault_to <= 15){
				$pdfrom = 42;
			} elseif($pdefault_to > 15 && $pdefault_to <= 30){
				$pdfrom = 32;
			} elseif($pdefault_to > 30 && $pdefault_to <= 50){
				$pdfrom = 22;
			} elseif($pdefault_to > 50 && $pdefault_to <= 80){
				$pdfrom = 12;
			} elseif($pdefault_to > 80 && $pdefault_to <= 100){
				$pdfrom = 0;
			}

			// Industry
			if(Input::get('type')==1){

				// get data
				$data = DB::table('tblentity')->select(DB::raw('
					tblindustry_main.main_title as industry_main,
					tblindustry_sub.sub_title as industry_sub,
					tblindustry_group.group_description as industry_row,
					count(tblentity.industry_row_id) as value,
					tblsmescore.*,
					(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bgroup,
					(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mgroup,
					tblsmescore.score_facs as fgroup,
					CAST(tblsmescore.score_sf AS DECIMAL) as creditbpo_rating,
					tblentity.industry_main_id
				'))
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
				->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
				->leftJoin('tblindustry_group', 'tblentity.industry_row_id', '=', 'tblindustry_group.group_code')
				->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
				->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
				->leftJoin('tblmajorcustomer', 'tblentity.entityid', '=', 'tblmajorcustomer.entity_id')
				->leftJoin('tblmajorsupplier', 'tblentity.entityid', '=', 'tblmajorsupplier.entity_id')
				->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
				->where('tblentity.isIndustry', 1)
				->where('tblentity.is_deleted', 0)
				->groupBy('tblentity.industry_main_id')->groupBy('tblentity.industry_sub_id')->groupBy('tblentity.industry_row_id')
				->orderBy('tblindustry_main.main_title')->orderBy('tblindustry_sub.sub_title')->orderBy('tblindustry_group.group_description');

				if($this->accountDetails['can_view_free_sme']==1){
					$data->where(function($query){
						$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$data->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
				}

				// apply filters
				if(@count($sme_selected)>0){
					$data->whereIn('tblentity.entityid', $sme_selected);
				}
				if($main_industry_selected!=""){
					$data->where('tblentity.industry_main_id', $main_industry_selected);
				}
				if($sub_industry_selected!=""){
					$data->where('tblentity.industry_sub_id', $sub_industry_selected);
				}
				if($industry_selected!=""){
					$data->where('tblentity.industry_row_id', $industry_selected);
				}
				if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
					$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
				}
				if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
					$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
				}
				if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
					$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
				}

				// major customer filters
				if($with_majCustFilter) {
					// years doing business filter
					$data->whereBetween('customer_years_doing_business', [$yearsDoingBusiness_from, $yearsDoingBusiness_to])->where('tblmajorcustomer.is_deleted', 0);

					// % share of sales to this company vs. total sales filter
					$data->whereBetween('customer_share_sales', [$percentShareSales_from, $percentShareSales_to])->where('tblmajorcustomer.is_deleted', 0);

					// order frequency filter
					if ($orderFrequency != null) {
						$data->whereIn('customer_order_frequency', $orderFrequency)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Payment Behavior Filter
					if ($paymentBehavior != null) {
						$data->whereIn('customer_settlement', $paymentBehavior)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Relationship Satisfaction Filter
					if ($relationshipSatisfaction != null) {
						$data->whereIn('tblmajorcustomer.relationship_satisfaction', $relationshipSatisfaction)->where('tblmajorcustomer.is_deleted', 0);
					}

				}

				// major supplier filters
				if ($with_majSuppFilter) {
					// supplier years doing business filter
					$data->whereBetween('supplier_years_doing_business', [$supp_yearsDoingBusiness_from, $supp_yearsDoingBusiness_to])->where('tblmajorsupplier.is_deleted', 0);

					// % share of total purchases
					$data->whereBetween('supplier_share_sales', [$supp_percentShareSales_from, $supp_percentShareSales_to])->where('tblmajorsupplier.is_deleted', 0);

					// your payment to supplier in last 3 payments filter
					if ($paymentBehaviorSupplier != null) {
						$data->whereIn('supplier_settlement', $paymentBehaviorSupplier)->where('tblmajorsupplier.is_deleted', 0);
					}

					// purchase frequency filter
					if ($purchaseFrequency != null) {
						$data->whereIn('supplier_order_frequency', $purchaseFrequency)->where('tblmajorsupplier.is_deleted', 0);
					}
				}

				// $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
				if($region_selected!=""){
					$data->where('refcitymun.regDesc', $region_selected);
				}

				if($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
					// do nothing
				} else {
					$data->take(BANK_SME_LIMIT);
				}
				$data = $data->get();

				// bank rating computation
				if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
					foreach($data as $k=>$d){
						$bank_group = BankIndustry::where('bank_id', $this->accountDetails['bank_id'])
								->where('industry_id', $d->industry_main_id)->first();
						$bank_formula = BankFormula::where('bank_id', $this->accountDetails['bank_id'])->first();

						if($bank_group){
							if($bank_formula){
								//bcc
								$bgroup_array = explode(',', $bank_formula->bcc);
								$bgroup_item = 0;
								$bgroup_total = 0;
								if(in_array('rm', $bgroup_array)) {
									$bgroup_item += $d->score_rm;
									$bgroup_total += MAX_RM;
								}
								if(in_array('cd', $bgroup_array)) {
									$bgroup_item += $d->score_cd;
									$bgroup_total += MAX_CD;
								}
								if(in_array('sd', $bgroup_array)) {
									$bgroup_item += $d->score_sd;
									$bgroup_total += MAX_SD;
								}
								if(in_array('boi', $bgroup_array)) {
									$bgroup_item += $d->score_bois;
									$bgroup_total += MAX_BOI;
								}

	                            if (0 < $bgroup_total) {
	                                $bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
	                            }
	                            else {
	                                $bgroup = 0;
	                            }

								//mq
								$mgroup_array = explode(',', $bank_formula->mq);
								$mgroup_item = 0;
								$mgroup_total = 0;
								if(in_array('boe', $mgroup_array)) {
									$mgroup_item += $d->score_boe;
									$mgroup_total += MAX_BOE;
								}
								if(in_array('mte', $mgroup_array)) {
									$mgroup_item += $d->score_mte;
									$mgroup_total += MAX_MTE;
								}
								if(in_array('bd', $mgroup_array)) {
									$mgroup_item += $d->score_bdms;
									$mgroup_total += MAX_BD;
								}
								if(in_array('sp', $mgroup_array)) {
									$mgroup_item += $d->score_sp;
									$mgroup_total += MAX_SP;
								}
								if(in_array('ppfi', $mgroup_array)) {
									$mgroup_item += $d->score_ppfi;
									$mgroup_total += MAX_PPFI;
								}

	                            if (0 < $mgroup_total) {
	                                $mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
	                            }
	                            else {
	                                $mgroup = 0;
	                            }

								//fa
								$fgroup_array = explode(',', $bank_formula->fa);
								if(@count($fgroup_array) == 2){
									$fgroup = $d->score_facs;
								} else {
									if($bank_formula->fa == 'fp'){
										$fgroup = $d->score_rfp;
									} elseif($bank_formula->fa == 'fr'){
										$fgroup = $d->score_rfpm;
									} else {
										$fgroup = 0;
									}
								}
							} else {
								$bgroup = ( $d->score_rm + $d->score_cd + $d->score_sd + $d->score_bois );
								$mgroup = ( $d->score_boe + $d->score_mte + $d->score_bdms + $d->score_sp + $d->score_ppfi );
								$fgroup = $d->score_facs;
							}
							$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

							if($bank_formula && $bank_formula->boost == 0){
								// no boost
							} else {
								//boost
								if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
								if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
								if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
							}
						} else {
							$gscore = $d->creditbpo_rating;
						}
						if(($gscore < (int)Input::get('brating_from')) || ($gscore > (int)Input::get('brating_to'))){
							unset($data[$k]); //remove from list
						}
					}
				}

				// get first financial score
				foreach($data as $d){
					$fin_cond = $this->computeFinancialConditionScore($d);
					$d->financial_score = $fin_cond['financial_score'];
					$d->financial_rating = $fin_cond['financial_rating'];
					$d->pdefault = $fin_cond['pdefault'];
				}

				if( (Input::get('fc_from')!="") && (Input::get('fc_to')!="") ) {

					// apply fin filter
					foreach($data as $key => $value){
						$from = array(
							'AAA'=> 1.6,
							'AA' => 1.2,
							'A'	=> 0.8,
							'BBB'	=> 0.4,
							'BB'	=> -0.4,
							'B'	=> 0,
							'CCC'	=> -0.8,
							'CC'	=> -1.2,
							'C'		=> -1.6,
							'D'		=> 2
						);
	
						$to = array(
							'AAA'=> 2,
							'AA' => 1.6,
							'A'	=> 1.2,
							'BBB'	=> 0.8,
							'BB'	=> 0.4,
							'B'	=> 0,
							'CCC'	=> -0.4,
							'CC'	=> -0.8,
							'C'		=> -1.2,
							'D'		=> 1.6
						);
	
						$input_from = Input::get('fc_from');
						$input_to = Input::get('fc_to');

						if($from[$input_from] > $to[$input_to]){
							$input_from = Input::get('fc_to');
							$input_to = Input::get('fc_from');
						}
	
						if( isset($from[$input_from]) && isset($to[$input_to]) ){
							if( $value->financial_score >= $from[$input_from] && $value->financial_score <= $to[$input_to] ){
								$final_data[] = $value;
							}else{
							}
						}
					}

						$data = $final_data;

				}
				
				//** Preliminary Probability of default */

				$input_from = Input::get('pdefault_from');
				$input_to = Input::get('pdefault_to');

				// if from is higher than to, exchange values
				if(Input::get('pdefault_from')!="" > Input::get('pdefault_to')){
					$input_from = Input::get('pdefault_to');
					$input_to = Input::get('pdefault_from');
				}

				foreach($data as $d){
					if( $d->pdefault >= $input_from && $d->pdefault <= $input_to){
						$pdefault_data[] = $d;
					}
				}

				if(isset($pdefault_data)){
					$data = $pdefault_data;
				}


				//** End Preliminary Probability of Default */

				// series
				$color = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1", "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#D809CF", "#0AEE75", "#F8FA49",  "#F79730", "#F730B8", "#88FA69"];
				$total = 0;

				$series1 = array();
				$series1_temp = '';
				$series1_index = -1;
				$series2 = array();
				$series2_temp = '';
				$series2_index = -1;
				$series3 = array();
				$series3_temp = '';
				$series3_index = -1;

	            $report_cntr    = 0;
	            $report2_cntr   = 0;
	            $report3_cntr   = 0;

	            $loop_cntr      = 1;
	            $data_cntr      = @count($data);

				foreach ($data as $d) {

					$credit_exp = explode('-', $d->financial_score);

					if ($series1_temp != $d->industry_main) {
						if (0 <= $series1_index) {
							$series1[$series1_index]->average = $d->financial_rating;
						}

						$series1_index++;
						$series1_temp                       = $d->industry_main;
						$series1[$series1_index]            = new stdClass();
						$series1[$series1_index]->name      = $d->industry_main;
						$series1[$series1_index]->y         = $d->value;
						$series1[$series1_index]->color     = ColorHelper::colourBrightness($color[$series1_index], -0.9);
	                    $series1[$series1_index]->average   = $d->financial_rating;

	                    $report_cntr                        = 1;

	                    if ($data_cntr == $loop_cntr) {
	                        $series1[$series1_index]->average = $d->financial_rating;
	                    }
					}
	                else {
	                    $report_cntr++;

						$series1[$series1_index]->y         = $d->value;
	                    $series1[$series1_index]->average   = $d->financial_rating;

	                    if ($data_cntr == $loop_cntr) {
	                        $series1[$series1_index]->average = $d->financial_rating;
	                    }
					}

					if ($series2_temp != $d->industry_sub) {

	                    if (0 <= $series2_index) {
	                        $series2[$series2_index]->average = $d->financial_rating;
	                    }

						$series2_index++;
						$series2_temp                       = $d->industry_sub;

						$series2[$series2_index]            = new stdClass();
						$series2[$series2_index]->name      = $d->industry_sub;
						$series2[$series2_index]->y         = $d->value;
						$series2[$series2_index]->color     = ColorHelper::colourBrightness($color[$series1_index], 1);
	                    $series2[$series2_index]->average   = $d->financial_rating;

	                    $report2_cntr                        = 1;

	                    if ($data_cntr == $loop_cntr) {
	                        $series2[$series2_index]->average = $d->financial_rating;
	                    }
					}
	                else {
	                    $report2_cntr++;

						$series2[$series2_index]->y         = $d->value;
	                    $series2[$series2_index]->average   = $d->financial_rating;

	                    if ($data_cntr == $loop_cntr) {
	                        $series2[$series2_index]->average = $d->financial_rating;
	                    }
					}

					if ($series3_temp != $d->industry_row) {

	                    if (0 <= $series3_index) {
	                        $series3[$series3_index]->average = $d->financial_rating;
	                    }

						$series3_index++;
						$series3_temp                       = $d->industry_row;

						$series3[$series3_index]            = new stdClass();
						$series3[$series3_index]->name      = $d->industry_row;
						$series3[$series3_index]->y         = $d->value;
						$series3[$series3_index]->color     = ColorHelper::colourBrightness($color[$series1_index], .9);
	                    $series3[$series3_index]->average   = $d->financial_rating;

	                    $report3_cntr                        = 1;

	                    if ($data_cntr == $loop_cntr) {
	                        $series3[$series3_index]->average = $d->financial_rating;
	                    }
					}
	                else {
	                    $report3_cntr++;

						$series3[$series3_index]->y         = $d->value;
	                    $series3[$series3_index]->average   = $d->financial_rating;

	                    if ($data_cntr == $loop_cntr) {
	                        $series3[$series3_index]->average = $d->financial_rating;
	                    }
					}

					$total += $d->value;
	                $loop_cntr++;
				}

				// calculate value /100
				foreach($series1 as $key=>$value){
					$series1[$key]->y = round($value->y / $total * 100, 2);
				}
				foreach($series2 as $key=>$value){
					$series2[$key]->y = round($value->y / $total * 100, 2);
				}
				foreach($series3 as $key=>$value){
					$series3[$key]->y = round($value->y / $total * 100, 2);
				}
				
				return array(
					'type' => 1,
					'data' => array(
						'series1' => $series1,
						'series2' => $series2,
						'series3' => $series3
					)
				);
			}

			// Composite Final Ratings
			if(Input::get('type')==2){
				$data = DB::table('tblentity')->select(DB::raw('
					tblentity.entityid,
					AVG(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bcc_score,
					AVG(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mq_score,
					AVG(tblsmescore.score_facs) as fa_score,
					AVG(CAST(tblsmescore.score_sf AS DECIMAL)) as creditbpo_rating
				'))
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
				->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
				->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
				->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
				->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
				->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
				->leftjoin('tblmajorcustomer', 'tblentity.entityid', '=', 'tblmajorcustomer.entity_id')
				->leftjoin('tblmajorsupplier', 'tblentity.entityid', '=', 'tblmajorsupplier.entity_id')
				->leftJoin('tblbankindustryweights', function($join){
					$join->on('tblentity.industry_main_id', '=', 'tblbankindustryweights.main_code');
					$join->on(DB::raw(Auth::user()->bank_id), '=', 'tblbankindustryweights.bank_id');
				})
				->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.industry_row_id', '!=', '')
				->where('tblentity.industry_main_id', '!=', '')->where('tblentity.industry_sub_id', '!=', '')
				->where('tblentity.isIndustry', 1)
				->where('tblentity.is_deleted', 0)
				->where('tblentity.status', 7);

				if(Auth::user()->can_view_free_sme==1){
					$data->where(function($query){
						$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$data->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
				}

				//filters
				if(@count($sme_selected)>0){
					$data->whereIn('tblentity.entityid', $sme_selected);
				}
				if($main_industry_selected!=""){
					$data->where('tblentity.industry_main_id', $main_industry_selected);
				}
				if($sub_industry_selected!=""){
					$data->where('tblentity.industry_sub_id', $sub_industry_selected);
				}
				if($industry_selected!=""){
					$data->where('tblentity.industry_row_id', $industry_selected);
				}
				if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
					$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
				}
				if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
					$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
				}
				if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
					// $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
				}
				if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
					$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
				}

				// major customer filters
				if($with_majCustFilter) {
					// years doing business filter
					$data->whereBetween('customer_years_doing_business', [$yearsDoingBusiness_from, $yearsDoingBusiness_to])->where('tblmajorcustomer.is_deleted', 0);

					// % share of sales to this company vs. total sales filter
					$data->whereBetween('customer_share_sales', [$percentShareSales_from, $percentShareSales_to])->where('tblmajorcustomer.is_deleted', 0);

					// order frequency filter
					if ($orderFrequency != null) {
						$data->whereIn('customer_order_frequency', $orderFrequency)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Payment Behavior Filter
					if ($paymentBehavior != null) {
						$data->whereIn('customer_settlement', $paymentBehavior)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Relationship Satisfaction Filter
					if ($relationshipSatisfaction != null) {
						$data->whereIn('tblmajorcustomer.relationship_satisfaction', $relationshipSatisfaction)->where('tblmajorcustomer.is_deleted', 0);
					}

				}

				// major supplier filters
				if ($with_majSuppFilter) {
					// supplier years doing business filter
					$data->whereBetween('supplier_years_doing_business', [$supp_yearsDoingBusiness_from, $supp_yearsDoingBusiness_to])->where('tblmajorsupplier.is_deleted', 0);

					// % share of total purchases
					$data->whereBetween('supplier_share_sales', [$supp_percentShareSales_from, $supp_percentShareSales_to])->where('tblmajorsupplier.is_deleted', 0);

					// your payment to supplier in last 3 payments filter
					if ($paymentBehaviorSupplier != null) {
						$data->whereIn('supplier_settlement', $paymentBehaviorSupplier)->where('tblmajorsupplier.is_deleted', 0);
					}

					// purchase frequency filter
					if ($purchaseFrequency != null) {
						$data->whereIn('supplier_order_frequency', $purchaseFrequency)->where('tblmajorsupplier.is_deleted', 0);
					}
				}

				$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
				if($region_selected!=""){
					$data->where('refcitymun.regDesc', $region_selected);
				}
				if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
					$bank_formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
					if($bank_formula){
						//bcc
						$bgroup_array = explode(',', $bank_formula->bcc);
						$bgroup_item = [];
						$bgroup_total = 0;
						if(in_array('rm', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_rm';
							$bgroup_total += MAX_RM;
						}
						if(in_array('cd', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_cd';
							$bgroup_total += MAX_CD;
						}
						if(in_array('sd', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_sd';
							$bgroup_total += MAX_SD;
						}
						if(in_array('boi', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_bois';
							$bgroup_total += MAX_BOI;
						}
						$bgroup = '(' . implode(' + ', $bgroup_item) . ') * ' . (MAX_RM + MAX_CD + MAX_SD + MAX_BOI) . ' / ' . $bgroup_total;

						//mq
						$mgroup_array = explode(',', $bank_formula->mq);
						$mgroup_item = [];
						$mgroup_total = 0;
						if(in_array('boe', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_boe';
							$mgroup_total += MAX_BOE;
						}
						if(in_array('mte', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_mte';
							$mgroup_total += MAX_MTE;
						}
						if(in_array('bd', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_bdms';
							$mgroup_total += MAX_BD;
						}
						if(in_array('sp', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_sp';
							$mgroup_total += MAX_SP;
						}
						if(in_array('ppfi', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_ppfi';
							$mgroup_total += MAX_PPFI;
						}
						$mgroup = '(' . implode(' + ', $mgroup_item) . ') * ' . (MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI) . ' / ' . $mgroup_total;

						//fa
						$fgroup_array = explode(',', $bank_formula->fa);
						if(@count($fgroup_array) == 2){
							$fgroup = 'tblsmescore.score_facs';
						} else {
							if($bank_formula->fa == 'fp'){
								$fgroup = 'tblsmescore.score_rfp';
							} elseif($bank_formula->fa == 'fr'){
								$fgroup = 'tblsmescore.score_rfpm';
							} else {
								$fgroup = '0';
							}
						}
						$where_raw_string = '(
							(('.$bgroup.') * (tblbankindustryweights.business_group / 100)) +
							(('.$mgroup.') * (tblbankindustryweights.management_group / 100)) +
							('.$fgroup.' * (tblbankindustryweights.financial_group / 100))
						)';
					} else {
						$where_raw_string = '(
							((tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) * (tblbankindustryweights.business_group / 100)) +
							((tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) * (tblbankindustryweights.management_group / 100)) +
							(tblsmescore.score_facs * (tblbankindustryweights.financial_group / 100))
						)';
					}
					$data->whereRaw($where_raw_string.' between ' . Input::get('brating_from') . ' and ' . Input::get('brating_to'));
				}

				$data = $data->first();

				return array(
					'type' => 2,
					'data' => array(
						'categories' => array(
							'Business Considerations and Conditions',
							'Management Quality',
							'Financial Condition',
							'CreditBPO Rating'
						),
						'series' => array(
							(float)$data->bcc_score,
							(float)$data->mq_score,
							(float)$data->fa_score,
							(float)$data->creditbpo_rating
						)
					)
				);
			}

			// Composite Final Ratings
			if(Input::get('type')==3){
				$data = DB::table('tblentity')->select(DB::raw('
					tblentity.entityid,
					AVG(tblsmescore.score_rm) as score_rm,
					AVG(tblsmescore.score_cd) as score_cd,
					AVG(tblsmescore.score_sd) as score_sd,
					AVG(tblsmescore.score_bois) as score_bois,

					AVG(tblsmescore.score_boe) as score_boe,
					AVG(tblsmescore.score_mte) as score_mte,
					AVG(tblsmescore.score_bdms) as score_bdms,
					AVG(tblsmescore.score_sp) as score_sp,
					AVG(tblsmescore.score_ppfi) as score_ppfi,

					AVG(tblsmescore.score_rfp) as score_rfp,
					AVG(tblsmescore.score_rfpm) as score_rfpm
				'))
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
				->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
				->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
				->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
				->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
				->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
				->leftjoin('tblmajorcustomer', 'tblentity.entityid', '=', 'tblmajorcustomer.entity_id')
				->leftjoin('tblmajorsupplier', 'tblentity.entityid', '=', 'tblmajorsupplier.entity_id')
				->leftJoin('tblbankindustryweights', function($join){
					$join->on('tblentity.industry_main_id', '=', 'tblbankindustryweights.industry_id');
					$join->on(DB::raw(Auth::user()->bank_id), '=', 'tblbankindustryweights.bank_id');
				})
				->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
				->where('tblentity.is_deleted', 0)
				->where('tblentity.isIndustry', 1);

				if(Auth::user()->can_view_free_sme==1){
					$data->where(function($query){
						$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$data->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
				}

				//filters
				if(@count($sme_selected)>0){
					$data->whereIn('tblentity.entityid', $sme_selected);
				}
				if($main_industry_selected!=""){
					$data->where('tblentity.industry_main_id', $main_industry_selected);
				}
				if($sub_industry_selected!=""){
					$data->where('tblentity.industry_sub_id', $sub_industry_selected);
				}
				if($industry_selected!=""){
					$data->where('tblentity.industry_row_id', $industry_selected);
				}
				if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
					$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
				}
				if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
					$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
				}
				if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
					// $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
				}
				if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
					$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
				}

				// major customer filters
				if($with_majCustFilter) {
					// years doing business filter
					$data->whereBetween('customer_years_doing_business', [$yearsDoingBusiness_from, $yearsDoingBusiness_to])->where('tblmajorcustomer.is_deleted', 0);

					// % share of sales to this company vs. total sales filter
					$data->whereBetween('customer_share_sales', [$percentShareSales_from, $percentShareSales_to])->where('tblmajorcustomer.is_deleted', 0);

					// order frequency filter
					if ($orderFrequency != null) {
						$data->whereIn('customer_order_frequency', $orderFrequency)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Payment Behavior Filter
					if ($paymentBehavior != null) {
						$data->whereIn('customer_settlement', $paymentBehavior)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Relationship Satisfaction Filter
					if ($relationshipSatisfaction != null) {
						$data->whereIn('tblmajorcustomer.relationship_satisfaction', $relationshipSatisfaction)->where('tblmajorcustomer.is_deleted', 0);
					}

				}

				// major supplier filters
				if ($with_majSuppFilter) {
					// supplier years doing business filter
					$data->whereBetween('supplier_years_doing_business', [$supp_yearsDoingBusiness_from, $supp_yearsDoingBusiness_to])->where('tblmajorsupplier.is_deleted', 0);

					// % share of total purchases
					$data->whereBetween('supplier_share_sales', [$supp_percentShareSales_from, $supp_percentShareSales_to])->where('tblmajorsupplier.is_deleted', 0);

					// your payment to supplier in last 3 payments filter
					if ($paymentBehaviorSupplier != null) {
						$data->whereIn('supplier_settlement', $paymentBehaviorSupplier)->where('tblmajorsupplier.is_deleted', 0);
					}

					// purchase frequency filter
					if ($purchaseFrequency != null) {
						$data->whereIn('supplier_order_frequency', $purchaseFrequency)->where('tblmajorsupplier.is_deleted', 0);
					}
				}

				$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
				if($region_selected!=""){
					$data->where('refcitymun.regDesc', $region_selected);
				}
				if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
					$bank_formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
					if($bank_formula){
						//bcc
						$bgroup_array = explode(',', $bank_formula->bcc);
						$bgroup_item = [];
						$bgroup_total = 0;
						if(in_array('rm', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_rm';
							$bgroup_total += MAX_RM;
						}
						if(in_array('cd', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_cd';
							$bgroup_total += MAX_CD;
						}
						if(in_array('sd', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_sd';
							$bgroup_total += MAX_SD;
						}
						if(in_array('boi', $bgroup_array)) {
							$bgroup_item[] = 'tblsmescore.score_bois';
							$bgroup_total += MAX_BOI;
						}
						$bgroup = '(' . implode(' + ', $bgroup_item) . ') * ' . (MAX_RM + MAX_CD + MAX_SD + MAX_BOI) . ' / ' . $bgroup_total;

						//mq
						$mgroup_array = explode(',', $bank_formula->mq);
						$mgroup_item = [];
						$mgroup_total = 0;
						if(in_array('boe', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_boe';
							$mgroup_total += MAX_BOE;
						}
						if(in_array('mte', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_mte';
							$mgroup_total += MAX_MTE;
						}
						if(in_array('bd', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_bdms';
							$mgroup_total += MAX_BD;
						}
						if(in_array('sp', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_sp';
							$mgroup_total += MAX_SP;
						}
						if(in_array('ppfi', $mgroup_array)) {
							$mgroup_item[] = 'tblsmescore.score_ppfi';
							$mgroup_total += MAX_PPFI;
						}
						$mgroup = '(' . implode(' + ', $mgroup_item) . ') * ' . (MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI) . ' / ' . $mgroup_total;

						//fa
						$fgroup_array = explode(',', $bank_formula->fa);
						if(@count($fgroup_array) == 2){
							$fgroup = 'tblsmescore.score_facs';
						} else {
							if($bank_formula->fa == 'fp'){
								$fgroup = 'tblsmescore.score_rfp';
							} elseif($bank_formula->fa == 'fr'){
								$fgroup = 'tblsmescore.score_rfpm';
							} else {
								$fgroup = '0';
							}
						}
						$where_raw_string = '(
							(('.$bgroup.') * (tblbankindustryweights.business_group / 100)) +
							(('.$mgroup.') * (tblbankindustryweights.management_group / 100)) +
							('.$fgroup.' * (tblbankindustryweights.financial_group / 100))
						)';
					} else {
						$where_raw_string = '(
							((tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) * (tblbankindustryweights.business_group / 100)) +
							((tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) * (tblbankindustryweights.management_group / 100)) +
							(tblsmescore.score_facs * (tblbankindustryweights.financial_group / 100))
						)';
					}
					$data->whereRaw($where_raw_string.' between ' . Input::get('brating_from') . ' and ' . Input::get('brating_to'));
				}

				$data = $data->first();

				return array(
					'type' => 3,
					'data' => array(
						'category1' => array(
							'Risk Management Rating',
							'Customer Dependency',
							'Supplier Dependency',
							'Business Outlook Index Score'
						),
						'series1' => array(
							(float)$data->score_rm,
							(float)$data->score_cd,
							(float)$data->score_sd,
							(float)$data->score_bois
						),
						'category2' => array(
							'Bus. Owner Exp.',
							'Mgt. Team Exp.',
							'Business Driver',
							'Succession Plan',
							'Future Growth'
						),
						'series2' => array(
							(float)$data->score_boe,
							(float)$data->score_mte,
							(float)$data->score_bdms,
							(float)$data->score_sp,
							(float)$data->score_ppfi
						),
						'category3' => array(
							'Financial Position',
							'Financial Performance'
						),
						'series3' => array(
							(float)$data->score_rfp * 0.6,
							(float)$data->score_rfpm * 0.4
						)
					)
				);
			}

			// Industry VS SME
			if(Input::get('type')==4){                            
				$data = DB::table('tblentity')->select(DB::raw('
					tblentity.entityid,
					tblentity.companyname,
					tblreadyrationdata.gross_revenue_growth2,
					tblreadyrationdata.net_income_growth2,
					tblreadyrationdata.gross_profit_margin2,
					(tblreadyrationdata.current_ratio2) as current_ratio2,
					(tblreadyrationdata.debt_equity_ratio2) as debt_equity_ratio2,
					tblindustry_main.gross_revenue_growth,
					tblindustry_main.net_income_growth,
					tblindustry_main.gross_profit_margin,
					(tblindustry_main.current_ratio / 100) as current_ratio,
					(tblindustry_main.debt_equity_ratio / 100) as debt_equity_ratio,

					(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bgroup,
					(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mgroup,
					tblsmescore.score_facs as fgroup,
					CAST(tblsmescore.score_sf AS DECIMAL) as creditbpo_rating,
					tblentity.industry_main_id
				'))
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
				->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
				->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
				->leftJoin('tblreadyrationdata', 'tblentity.entityid', '=', 'tblreadyrationdata.entityid')
				->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
				->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
				->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
				->leftjoin('tblmajorcustomer', 'tblentity.entityid', '=', 'tblmajorcustomer.entity_id')
				->leftjoin('tblmajorsupplier', 'tblentity.entityid', '=', 'tblmajorsupplier.entity_id')
				->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
				->where('tblreadyrationdata.gross_revenue_growth2', '!=', 'NULL')
				->where('tblentity.isIndustry', 1)
				->where('tblentity.is_deleted', 0);


                                if(Auth::user()->role == 0 && Session::has('supervisor')){
                                    $bank_id=Session::get('supervisor.bank_id');
                                }else{
                                    $bank_id=Auth::user()->bank_id;
                                }
                                        
				if(Auth::user()->can_view_free_sme==1){
					$data->where(function($query){
						$query->where('tblentity.current_bank', '=', $bank_id)->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$data->where('tblentity.current_bank', '=', $bank_id)->where('tblentity.is_independent', 1);
				}

				//filters
				if(@count($sme_selected)>0){
					$data->whereIn('tblentity.entityid', $sme_selected);
				}
				if($main_industry_selected!=""){
					$data->where('tblentity.industry_main_id', $main_industry_selected);
				}
				if($sub_industry_selected!=""){
					$data->where('tblentity.industry_sub_id', $sub_industry_selected);
				}
				if($industry_selected!=""){
					$data->where('tblentity.industry_row_id', $industry_selected);
				}
				if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
					$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
				}
				if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
					$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
				}
				if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
					// $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
				}
				if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
					$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
				}

				// major customer filters
				if($with_majCustFilter) {
					// years doing business filter
					$data->whereBetween('customer_years_doing_business', [$yearsDoingBusiness_from, $yearsDoingBusiness_to])->where('tblmajorcustomer.is_deleted', 0);

					// % share of sales to this company vs. total sales filter
					$data->whereBetween('customer_share_sales', [$percentShareSales_from, $percentShareSales_to])->where('tblmajorcustomer.is_deleted', 0);

					// order frequency filter
					if ($orderFrequency != null) {
						$data->whereIn('customer_order_frequency', $orderFrequency)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Payment Behavior Filter
					if ($paymentBehavior != null) {
						$data->whereIn('customer_settlement', $paymentBehavior)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Relationship Satisfaction Filter
					if ($relationshipSatisfaction != null) {
						$data->whereIn('tblmajorcustomer.relationship_satisfaction', $relationshipSatisfaction)->where('tblmajorcustomer.is_deleted', 0);
					}

				}

				// major supplier filters
				if ($with_majSuppFilter) {
					// supplier years doing business filter
					$data->whereBetween('supplier_years_doing_business', [$supp_yearsDoingBusiness_from, $supp_yearsDoingBusiness_to])->where('tblmajorsupplier.is_deleted', 0);

					// % share of total purchases
					$data->whereBetween('supplier_share_sales', [$supp_percentShareSales_from, $supp_percentShareSales_to])->where('tblmajorsupplier.is_deleted', 0);

					// your payment to supplier in last 3 payments filter
					if ($paymentBehaviorSupplier != null) {
						$data->whereIn('supplier_settlement', $paymentBehaviorSupplier)->where('tblmajorsupplier.is_deleted', 0);
					}

					// purchase frequency filter
					if ($purchaseFrequency != null) {
						$data->whereIn('supplier_order_frequency', $purchaseFrequency)->where('tblmajorsupplier.is_deleted', 0);
					}
				}

				$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
				if($region_selected!=""){
					$data->where('refcitymun.regDesc', $region_selected);
				}
				if(Auth::user()->role == 0 && Session::has('supervisor'))
                                {
                                        //do nothing
                                }
                                elseif($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
					//do nothing
				} else {
					$data->take(BANK_SME_LIMIT);
				}
				$data = $data->get();
                                $modelic= new IndustryComparison();
                                ini_set('max_execution_time', '300'); 
                                if($data){
                                    
                                   foreach($data as $v => $getind){
                                        $dataic = 			$modelic->getIndustryComparison($getind->entityid);
                                        $data[$v]->gross_revenue_growth = $dataic['gross_revenue_growth'];
                                        $data[$v]->net_income_growth    = $dataic['net_income_growth'];
                                        $data[$v]->gross_profit_margin  = $dataic['gross_profit_margin'];
                                        $data[$v]->current_ratio        = $dataic['current_ratio'];
                                        $data[$v]->debt_equity_ratio    = $dataic['debt_equity_ratio'];
                                   } 
                                }  
                                
				// bank rating computation
				if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
					foreach($data as $k=>$d){
						$bank_group = BankIndustry::where('bank_id', $bank_id)
								->where('industry_id', $d->industry_main_id)->first();
						$bank_formula = BankFormula::where('bank_id', $bank_id)->first();

						if($bank_group){
							if($bank_formula){
								//bcc
								$bgroup_array = explode(',', $bank_formula->bcc);
								$bgroup_item = 0;
								$bgroup_total = 0;
								if(in_array('rm', $bgroup_array)) {
									$bgroup_item += $d->score_rm;
									$bgroup_total += MAX_RM;
								}
								if(in_array('cd', $bgroup_array)) {
									$bgroup_item += $d->score_cd;
									$bgroup_total += MAX_CD;
								}
								if(in_array('sd', $bgroup_array)) {
									$bgroup_item += $d->score_sd;
									$bgroup_total += MAX_SD;
								}
								if(in_array('boi', $bgroup_array)) {
									$bgroup_item += $d->score_bois;
									$bgroup_total += MAX_BOI;
								}
								$bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;

								//mq
								$mgroup_array = explode(',', $bank_formula->mq);
								$mgroup_item = 0;
								$mgroup_total = 0;
								if(in_array('boe', $mgroup_array)) {
									$mgroup_item += $d->score_boe;
									$mgroup_total += MAX_BOE;
								}
								if(in_array('mte', $mgroup_array)) {
									$mgroup_item += $d->score_mte;
									$mgroup_total += MAX_MTE;
								}
								if(in_array('bd', $mgroup_array)) {
									$mgroup_item += $d->score_bdms;
									$mgroup_total += MAX_BD;
								}
								if(in_array('sp', $mgroup_array)) {
									$mgroup_item += $d->score_sp;
									$mgroup_total += MAX_SP;
								}
								if(in_array('ppfi', $mgroup_array)) {
									$mgroup_item += $d->score_ppfi;
									$mgroup_total += MAX_PPFI;
								}
								$mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;

								//fa
								$fgroup_array = explode(',', $bank_formula->fa);
								if(@count($fgroup_array) == 2){
									$fgroup = $d->score_facs;
								} else {
									if($bank_formula->fa == 'fp'){
										$fgroup = $d->score_rfp;
									} elseif($bank_formula->fa == 'fr'){
										$fgroup = $d->score_rfpm;
									} else {
										$fgroup = 0;
									}
								}
							} else {
								$bgroup = ( $d->score_rm + $d->score_cd + $d->score_sd + $d->score_bois );
								$mgroup = ( $d->score_boe + $d->score_mte + $d->score_bdms + $d->score_sp + $d->score_ppfi );
								$fgroup = $d->score_facs;
							}
							$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

							if($bank_formula && $bank_formula->boost == 0){
								// no boost
							} else {
								//boost
								if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
								if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
								if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
							}
						} else {
							$gscore = $d->creditbpo_rating;
						}
						if(($gscore < (int)Input::get('brating_from')) || ($gscore > (int)Input::get('brating_to'))){
							unset($data[$k]); //remove from list
						}
					}
				}

				$selector = Input::get('selector', 1);
				$chart_title = '';
				$chart_data = array();
				$chart_name = array();
				$lowest_standard = 0;
				$highest_standard = 0;

				// select tpye of chart
				switch($selector){
					case 1:
						$chart_title = 'Gross Revenue Growth';
						foreach($data as $d){
							$chart_name[] = $d->companyname;
							$chart_data[] = array((float)$d->gross_revenue_growth, (float)$d->gross_revenue_growth2);
							$highest_standard = ((float)$d->gross_revenue_growth > $highest_standard) ? (float)$d->gross_revenue_growth : $highest_standard;
							$lowest_standard = ((float)$d->gross_revenue_growth < $lowest_standard) ? (float)$d->gross_revenue_growth : $lowest_standard;
						}
						break;
					case 2:
						$chart_title = 'Net Income Growth';
						foreach($data as $d){
							$chart_name[] = $d->companyname;
							$chart_data[] = array((float)$d->net_income_growth, (float)$d->net_income_growth2);
							$highest_standard = ((float)$d->net_income_growth > $highest_standard) ? (float)$d->net_income_growth : $highest_standard;
							$lowest_standard = ((float)$d->net_income_growth < $lowest_standard) ? (float)$d->net_income_growth : $lowest_standard;
						}
						break;
					case 3:
						$chart_title = 'Gross Profit Margin';
						foreach($data as $d){
							$chart_name[] = $d->companyname;
							$chart_data[] = array((float)$d->gross_profit_margin, (float)$d->gross_profit_margin2);
							$highest_standard = ((float)$d->gross_profit_margin > $highest_standard) ? (float)$d->gross_profit_margin : $highest_standard;
							$lowest_standard = ((float)$d->gross_profit_margin < $lowest_standard) ? (float)$d->gross_profit_margin : $lowest_standard;
						}
						break;
					case 4:
						$chart_title = 'Current Ratio';
						foreach($data as $d){
							$chart_name[] = $d->companyname;
							$chart_data[] = array((float)$d->current_ratio, (float)$d->current_ratio2);
							$highest_standard = ((float)$d->current_ratio > $highest_standard) ? (float)$d->current_ratio : $highest_standard;
							$lowest_standard = ((float)$d->current_ratio < $lowest_standard) ? (float)$d->current_ratio : $lowest_standard;
						}
						break;
					case 5:
						$chart_title = 'Debt-to-Equity Ratio';
						foreach($data as $d){
							$chart_name[] = $d->companyname;
							$chart_data[] = array((float)$d->debt_equity_ratio, (float)$d->debt_equity_ratio2);
							$highest_standard = ((float)$d->debt_equity_ratio > $highest_standard) ? (float)$d->debt_equity_ratio : $highest_standard;
							$lowest_standard = ((float)$d->debt_equity_ratio < $lowest_standard) ? (float)$d->debt_equity_ratio : $lowest_standard;
						}
						break;
				}

				return array(
					'type' => 4,
					'selector' => $selector,
					'data' => array(
						'title' => $chart_title,
						'data' => $chart_data,
						'name' => $chart_name,
						'high' => array($highest_standard, $highest_standard),
						'low' => array($lowest_standard, $lowest_standard)
					)
				);
			}

			if (Input::get('type') == 5) {
				// get data
				$data = DB::table('tblentity')
							->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
							->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
							->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
							->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
							->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
							->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
							->leftJoin('tblmajorcustomer', 'tblentity.entityid', '=', 'tblmajorcustomer.entity_id')
							->leftJoin('tblmajorsupplier', 'tblentity.entityid', '=', 'tblmajorsupplier.entity_id')
							->where('tblentity.industry_row_id', '!=', 0)
							->where('tblentity.status', 7)
							->where('tblentity.isIndustry', 1)
							->where('tblentity.is_deleted', 0)
							->groupBy('tblentity.entityid')
							->orderBy('tblentity.entityid', 'desc');

				if(Auth::user()->can_view_free_sme==1){
					$data->where(function($query){
						$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
					});
				} else {
					$data->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
				}

				// apply filters
				if(@count($sme_selected)>0){
					$data->whereIn('tblentity.entityid', $sme_selected);
				}
				
				if($main_industry_selected!=""){
					$data->where('tblentity.industry_main_id', $main_industry_selected);
				}
				if($sub_industry_selected!=""){
					$data->where('tblentity.industry_sub_id', $sub_industry_selected);
				}
				if($industry_selected!=""){
					$data->where('tblentity.industry_row_id', $industry_selected);
				}
				if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
					$data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
				}
				if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
					$data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
				}

				if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
					$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
				}

				// major customer filters
				if($with_majCustFilter) {
					// years doing business filter
					$data->whereBetween('customer_years_doing_business', [$yearsDoingBusiness_from, $yearsDoingBusiness_to])->where('tblmajorcustomer.is_deleted', 0);

					// % share of sales to this company vs. total sales filter
					$data->whereBetween('customer_share_sales', [$percentShareSales_from, $percentShareSales_to])->where('tblmajorcustomer.is_deleted', 0);

					// order frequency filter
					if ($orderFrequency != null) {
						$data->whereIn('customer_order_frequency', $orderFrequency)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Payment Behavior Filter
					if ($paymentBehavior != null) {
						$data->whereIn('customer_settlement', $paymentBehavior)->where('tblmajorcustomer.is_deleted', 0);
					}

					// Relationship Satisfaction Filter
					if ($relationshipSatisfaction != null) {
						$data->whereIn('tblmajorcustomer.relationship_satisfaction', $relationshipSatisfaction)->where('tblmajorcustomer.is_deleted', 0);
					}

				}

				// major supplier filters
				if ($with_majSuppFilter) {
					// supplier years doing business filter
					$data->whereBetween('supplier_years_doing_business', [$supp_yearsDoingBusiness_from, $supp_yearsDoingBusiness_to])->where('tblmajorsupplier.is_deleted', 0);

					// % share of total purchases
					$data->whereBetween('supplier_share_sales', [$supp_percentShareSales_from, $supp_percentShareSales_to])->where('tblmajorsupplier.is_deleted', 0);

					// your payment to supplier in last 3 payments filter
					if ($paymentBehaviorSupplier != null) {
						$data->whereIn('supplier_settlement', $paymentBehaviorSupplier)->where('tblmajorsupplier.is_deleted', 0);
					}

					// purchase frequency filter
					if ($purchaseFrequency != null) {
						$data->whereIn('supplier_order_frequency', $purchaseFrequency)->where('tblmajorsupplier.is_deleted', 0);
					}
				}

				$data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
				if($region_selected!=""){
					$data->where('refcitymun.regDesc', $region_selected);
				}

				if($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))){
					//do nothing
				} else {
					$data->take(BANK_SME_LIMIT);
				}

				$data = $data->get();

				//get rating
				$bank = Bank::find($this->accountDetails['bank_id']);

				foreach ($data as $key => $value) {
					// update score as readable
				$financialChecker = false;
				$financialTotal = 0;


				$financialReport = FinancialReport::where(['entity_id' => $data[$key]->entityid, 'is_deleted' => 0])
							->orderBy('id', 'desc')
							->first();
				if($financialReport){
					$frHelper = new FinancialHandler();
					$finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
					if($finalRatingScore) {
						$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
						$financialChecker = true;
					}
				}
				if($financialChecker){
					$data[$key]->financial_score = $financialTotal;
					//Financial Position
					if($financialTotal >= 1.6) {
						$data[$key]->score_sf          = "AAA - Excellent";
						$data[$key]->cbpo_score_color  = 'green';
						$data[$key]->pdefault          = '1% - Very Low Risk';
					} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
						$data[$key]->score_sf          = "AA - Very Good";
						$data[$key]->cbpo_score_color  = 'green';
						$data[$key]->pdefault          = '2% - Very Low Risk';
					} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
						$data[$key]->score_sf          = "A - Good";
						$data[$key]->cbpo_score_color  = 'green';
						$data[$key]->pdefault          = '3% - Low Risk';
					} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
						$data[$key]->score_sf          = "BBB - Positive";
						$data[$key]->cbpo_score_color  = 'orange';
						$data[$key]->pdefault          = '4% - Moderate Risk';
					} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
						$data[$key]->score_sf          = "BB - Normal";
						$data[$key]->cbpo_score_color  = 'orange';
						$data[$key]->pdefault          = '6% - Moderate Risk';
					} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
						$data[$key]->score_sf   	 	 = "B - Satisfactory";
						$data[$key]->cbpo_score_color  = 'red';
						$data[$key]->pdefault          = '8% - Moderate Risk';
					} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
						$data[$key]->score_sf          = "CCC - Unsatisfactory";
						$data[$key]->cbpo_score_color  = 'red';
						$data[$key]->pdefault          = '15% - High Risk';
					} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
						$data[$key]->score_sf          = "CC - Adverse";
						$data[$key]->cbpo_score_color  = 'red';
						$data[$key]->pdefault          = '20% - High Risk';
					} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
						$data[$key]->score_sf          = "C - Bad";
						$data[$key]->cbpo_score_color  = 'red';
						$data[$key]->pdefault          = '25% - Very High Risk';
					} else {
						$data[$key]->score_sf          = "D - Critical";
						$data[$key]->cbpo_score_color  = 'red';
						$data[$key]->pdefault          = '32% - Very High Risk';
					}
				} elseif ($value->score_sf){
					$cred_rating_exp    = explode('-', $value->score_sf);
					$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
					$rate_val   = intval($rate_val);

					$data[$key]->financial_score = $rate_val;

					if($rate_val >= 177){
						$data[$key]->score_sf			= 'AA - '.$cred_rating_exp[1];
						$data[$key]->cbpo_score_color = 'green';
						$data[$key]->pdefault			= '2% - Very Low Risk';
					}
					else if($rate_val >= 150 && $rate_val <= 176) {
						$data[$key]->score_sf 		= 'A - '.$cred_rating_exp[1];
						$data[$key]->cbpo_score_color = 'green';
						$data[$key]->pdefault 		= '3% - Low Risk';
					}
					else if($rate_val >= 123 && $rate_val <= 149) {
						$data[$key]->score_sf 		= 'BBB - '.$cred_rating_exp[1];
						$data[$key]->cbpo_score_color = 'orange';
						$data[$key]->pdefault 		= '4% - Moderate Risk';
					}
					else if($rate_val >= 96 && $rate_val <= 122) {
						$data[$key]->score_sf			= 'BB - '.$cred_rating_exp[1];
						$data[$key]->cbpo_score_color = 'orange';
						$data[$key]->pdefault			= '6% - Moderate Risk';
					}
					else if($rate_val >= 68 && $rate_val <= 95) {
						$data[$key]->score_sf			= 'B - Some Vulnerability';
						$data[$key]->cbpo_score_color = 'red';
						$data[$key]->pdefault			= '8% - High Risk';
					}
					else if($rate_val < 68) {
						$data[$key]->score_sf 		= 'CCC - '.$cred_rating_exp[1];
						$data[$key]->cbpo_score_color = 'red';
						if($rate_val >= 42 && $rate_val <= 68) {
							$data[$key]->pdefault = '15% - High Risk';
						} else {
							$data[$key]->pdefault = '30% - Very High Risk';
						}
					}
				}

				$data[$key]->score_bank = $data[$key]->score_sf;
				$data[$key]->bank_score_color = $data[$key]->cbpo_score_color;

					if($bank->is_standalone == 0) {	
						$data[$key]->report_type = "Full Rating";
					} elseif ($bank->is_standalone == 1) {
						if ($value->is_premium == PREMIUM_REPORT) {
							$data[$key]->report_type = "Premium";
						} elseif($value->is_premium == SIMPLIFIED_REPORT) {
							$data[$key]->report_type = "Simplified Rating";
						} else {
							$data[$key]->report_type = "Standalone";
						}
					}
				}

				/** Check Financial Condition */
				if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
					$final_data = [];
					foreach($data as $key => $value){
						$from = array(
							'AAA'=> 1.6,
							'AA' => 1.2,
							'A'	=> 0.8,
							'BBB'	=> 0.4,
							'BB'	=> -0.4,
							'B'	=> 0,
							'CCC'	=> -0.8,
							'CC'	=> -1.2,
							'C'		=> -1.6,
							'D'		=> 2
						);
	
						$to = array(
							'AAA'=> 2,
							'AA' => 1.6,
							'A'	=> 1.2,
							'BBB'	=> 0.8,
							'BB'	=> 0.4,
							'B'	=> 0,
							'CCC'	=> -0.4,
							'CC'	=> -0.8,
							'C'		=> -1.2,
							'D'		=> 1.6
						);
	
						$input_from = Input::get('fc_from');
						$input_to = Input::get('fc_to');

						if($from[$input_from] > $to[$input_to]){
							$input_from = Input::get('fc_to');
							$input_to = Input::get('fc_from');
						}
	
						if( isset($from[$input_from]) && isset($to[$input_to]) ){
							if( $value->financial_score >= $from[$input_from] && $value->financial_score <= $to[$input_to] ){
								$final_data[] = $value;
							}else{
							}
						}
					}
					$data = $final_data;
				}

				return array(
						'type' => 5,
						'data' => $data,
					);
				}

			App::abort(404);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function computeFinancialConditionScore($report){
		$financialChecker = false;
		$financialTotal = 0;
		$pdefault = 0;


		$financialReport = FinancialReport::where(['entity_id' => $report->entityid, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
		
		if($financialReport){
			$frHelper = new FinancialHandler();
			$finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
			if($finalRatingScore) {
				$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
				$financialChecker = true;
			}
		}

		$financial_score = 0;
		if($financialChecker){
			$financial_score = $financialTotal;
			$financial_rating = '';

			if($financialTotal >= 1.6) {
				$financial_rating	= 'AAA';
				$pdefault = 1;
			} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
				$financial_rating	= 'AA';
				$pdefault = 2;
			} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
				$financial_rating	= 'A';
				$pdefault = 3;
			} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
				$financial_rating	= 'BBB';
				$pdefault = 4;
			} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
				$financial_rating	= 'BB';
				$pdefault = 6;
			} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
				$financial_rating	= 'B';
				$pdefault = 8;
			} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
				$financial_rating	= 'CCC';
				$pdefault = 15;
			} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
				$financial_rating	= 'CC';
				$pdefault = 20;
			} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
				$financial_rating	= 'C';
				$pdefault = 25;
			} else {
				$financial_rating	= 'D';
				$pdefault = 32;
			}
		} elseif ($value->score_sf){
			$cred_rating_exp    = explode('-', $report->score_sf);
			$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
			$rate_val   = intval($rate_val);

			$financial_score = $rate_val;
			$financial_rating = '';

			if($rate_val >= 177){
				$financial_rating = 'AA';
				$pdefault = 2;
			}
			else if($rate_val >= 150 && $rate_val <= 176) {
				$financial_rating = 'A';
				$pdefault = 3;
			}
			else if($rate_val >= 123 && $rate_val <= 149) {
				$financial_rating = 'BBB';
				$pdefault = 4;
			}
			else if($rate_val >= 96 && $rate_val <= 122) {
				$financial_rating = 'BB';
				$pdefault = 6;
			}
			else if($rate_val >= 68 && $rate_val <= 95) {
				$financial_rating = 'B';
				$pdefault = 8;
			}
			else if($rate_val < 68) {
				$financial_rating = 'CCC';
				if($rate_val >= 42 && $rate_val <= 68) {
					$pdefault = 15;
				} else {
					$pdefault= 30;
				}
			}
		}

		return array(
			'financial_rating'	=>	$financial_rating,
			'financial_score'	=>	$financial_score,
			'pdefault'			=>	$pdefault
		);
	}

	//-----------------------------------------------------
    //  Function 3.17: getCIUsers
	//  Description: get supervisor CI page
    //-----------------------------------------------------
	public function getCIUsers()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			Permissions::check(5);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			$users = DB::table('tbllogin')->where('role', 2)->where('bank_id', $this->accountDetails['bank_id'])->get();
			return View::make('bank.ci_users', array(
				'users' => $users
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.18: getAddCIUsers
	//  Description: get supervisor CI add page
    //-----------------------------------------------------
	public function getAddCIUsers()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(5);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			return View::make('bank.add_ci_user');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.19: postAddCIUsers
	//  Description: post process for supervisor CI add page
    //-----------------------------------------------------
	public function postAddCIUsers()
    {
    	try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
	        $messages = array(											// validator message
	            'email.unique'  => 'The email you are trying to register is already taken.'
	        );

			$rules = array( 											// validation rules
		        'email'	=> 'required|email|unique:tbllogin',
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
		        'job_title'	=> 'required',
				'password'	=> 'required|min:6|confirmed',
				'password_confirmation'	=> 'required|min:6'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bank/add_ci_user')->withErrors($validator->errors())->withInput();
			}
			else
			{

				// if validation success, create user
				$user = new User();
	      		$user->email = Input::get('email');
	      		$user->password = Hash::make(Input::get('password'));
	      		$user->job_title = Input::get('job_title');
	      		$user->firstname = Input::get('firstname');
	      		$user->lastname = Input::get('lastname');
	      		$user->role = 2;
				$user->bank_id = $this->accountDetails['bank_id'];
				$user->can_view_free_sme = $this->accountDetails['can_view_free_sme'];
	      		$user->status = 2;
	      		$user->save();

				return Redirect::to('/bank/ci_users');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.20: getEditCIUsers
	//  Description: get supervisor CI edit page
    //-----------------------------------------------------
	public function getEditCIUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			Permissions::check(5);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			$entity = DB::table('tbllogin')->where('loginid', $id)->where('role', 2)->where('bank_id', $this->accountDetails['bank_id'])->first();
			return View::make('bank.edit_ci_user')->with('entity', $entity);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.21: putCIUsers
	//  Description: post process for supervisor CI edit page
    //-----------------------------------------------------
	public function putCIUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			$rules = array(												//validation rules
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
		        'job_title'	=> 'required',
				'password'	=> 'nullable|min:6|confirmed',
				'password_confirmation'	=> 'nullable|required_with:password|min:6'
			);

			$validator = Validator::make(Input::all(), $rules);			// run validation

			if($validator->fails())
			{
				return Redirect::to('/bank/edit_ci_user/'.$id)->withErrors($validator->errors())->withInput();
			}
			else
			{
				// if success update CI user
				$user = User::where('loginid', $id)->where('role', 2)->where('bank_id', $this->accountDetails['bank_id'])->first();
				if(Input::get('password')!=''){
					if(Input::get('password')!=Input::get('password_confirmation')){
						return Redirect::to('/bank/edit_ci_user/'.$id)->withErrors('password', 'Password confirmtion did not match.')->withInput();
					} elseif(strlen(Input::get('password')) < 6){
						return Redirect::to('/bank/edit_ci_user/'.$id)->withErrors('password', 'Password must be at least 6 characters.')->withInput();
					} else {
						$user->password = Hash::make(Input::get('password'));
					}
				}
	      		$user->job_title = Input::get('job_title');
	      		$user->firstname = Input::get('firstname');
	      		$user->lastname = Input::get('lastname');
	      		$user->save();

				return Redirect::to('/bank/ci_users');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.22: getDeleteCIUsers
	//  Description: function to delete supervisor CI users
    //-----------------------------------------------------
	public function getDeleteCIUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			Permissions::check(5);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			$user = User::where('loginid', $id)->where('role', 2)->where('bank_id', $this->accountDetails['bank_id'])->first();
			if($user){
				$user->delete();
			}
			return Redirect::to('/bank/ci_users');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.23: getSubUsers
	//  Description: get supervisor sub users page
    //-----------------------------------------------------
	public function getSubUsers()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			if($this->accountDetails['parent_user_id'] == 0){
				$users = User::where('parent_user_id', $this->accountDetails['loginid'])->get();
				foreach($users as $k=>$u){
					$permissions = explode(',', $u->permissions);

					// check sub user permision and convert as readable
					$permissions_f = array();
					foreach($permissions as $p){
						switch($p){
							case 1: $permissions_f[] = 'Report Listing'; break;
							case 2: $permissions_f[] = 'Industry Weights'; break;
							case 3: $permissions_f[] = 'Client Keys'; break;
							case 4: $permissions_f[] = 'SME Statistics'; break;
							case 5: $permissions_f[] = 'Bank CI'; break;
							case 6: $permissions_f[] = 'Rating Formula'; break;
							case 7: $permissions_f[] = 'Map'; break;
							case 8: $permissions_f[] = 'Document Options'; break;
							case 11: $permissions_f[] = 'Company Matching'; break;
						}
					}
					$users[$k]->permission_formatted = implode(', ', $permissions_f);
				}
				return View::make('bank.sub_users')->with('users', $users);
			} else {
				App::abort(403, 'Unauthorized access.');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.24: getAddSubUsers
	//  Description: get supervisor sub users add page
    //-----------------------------------------------------
	public function getAddSubUsers()
	{
		try{
			if($this->accountDetails['product_plan']==3){
				App::abort(404);
			}

		    $result = $this->_checkAllSubscriptionConditions();

		    if($result != ""){
				return $result;
			}

			if($this->accountDetails['parent_user_id'] == 0){
				return View::make('bank.add_sub_user');
			} else {
				App::abort(403, 'Unauthorized access.');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.25: postSubUsers
	//  Description: post proccess for supervisor sub users add page
    //-----------------------------------------------------
	public function postSubUsers()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
	        $perm_count = 0;

	        $messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to register is already taken.'
	        );

			$rules = array(									// validation rules
		        'email'	=> 'required|email|unique:tbllogin',
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
				'password'	=> 'required|min:6|confirmed',
				'password_confirmation'	=> 'required|min:6'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bank/add_sub_user')->withErrors($validator->errors())->withInput();
			}
			else
			{

	            $permissions    = Input::get('permissions');			// get input permisions
	            $perm_count     = @count($permissions);
	            $permissions[$perm_count] = 1;

				// create the sub user
				$user = new User();
	      		$user->email = Input::get('email');
	      		$user->password = Hash::make(Input::get('password'));
	      		$user->job_title = '';
	      		$user->firstname = Input::get('firstname');
	      		$user->lastname = Input::get('lastname');
	      		$user->role = 4;
				$user->bank_id = $this->accountDetails['bank_id'];
				$user->can_view_free_sme = $this->accountDetails['can_view_free_sme'];
	      		$user->status = 2;
				$user->parent_user_id = $this->accountDetails['loginid'];
	            $user->permissions = implode(',', $permissions);
	      		$user->save();

				return Redirect::to('/bank/sub_users');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.26: getEditSubUsers
	//  Description: get supervisor sub users edit page
    //-----------------------------------------------------
	public function getEditSubUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			if($this->accountDetails['parent_user_id'] == 0){
				$entity = DB::table('tbllogin')->where('loginid', $id)->where('role', 4)->where('parent_user_id', $this->accountDetails['loginid'])->first();
				$entity->permissions = explode(',', $entity->permissions);
				return View::make('bank.edit_sub_user')->with('entity', $entity);
			} else {
				App::abort(403, 'Unauthorized access.');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.27: putSubUsers
	//  Description: post process supervisor sub users edit page
    //-----------------------------------------------------
	public function putSubUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
	        $perm_count = 0;
			$rules = array(					// validation rules
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
	            'password'	=> 'nullable|min:6|confirmed',
				'password_confirmation'	=> 'nullable|required_with:password|min:6'
			);

	        $validator = Validator::make(Input::all(), $rules);

	        if($validator->fails())
	        {
	            return Redirect::to('/bank/edit_sub_user/'.$id)->withErrors($validator->errors())->withInput();
	        }
	        else
	        {
				//if success
	            $permissions    = Input::get('permissions'); 		// get input permissions
	            $perm_count     = @count($permissions);
				$permissions[$perm_count] = 1;

	            $user = User::where('loginid', $id)->where('role', 4)->where('parent_user_id', $this->accountDetails['loginid']	)->first();
	            if(Input::get('password')!=''){		// check password confirm if match
	                if(Input::get('password')!=Input::get('password_confirmation')){
	                    return Redirect::to('/bank/edit_sub_user/'.$id)->withErrors('password', 'Password confirmtion did not match.')->withInput();
	                } elseif(strlen(Input::get('password')) < 6){
	                    return Redirect::to('/bank/edit_sub_user/'.$id)->withErrors('password', 'Password must be at least 6 characters.')->withInput();
	                } else {
	                    $user->password = Hash::make(Input::get('password'));
	                }
	            }
	            $user->firstname = Input::get('firstname');
	            $user->lastname = Input::get('lastname');
	            $user->permissions = implode(',', $permissions);;
	            $user->save();		/// save

	            return Redirect::to('/bank/sub_users');
	        }
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.28: getDeleteSubUsers
	//  Description: function to delete sub users
    //-----------------------------------------------------
	public function getDeleteSubUsers($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			$user = User::where('loginid', $id)->where('role', 4)->where('parent_user_id', $this->accountDetails['loginid'])->first();
			if($user){
				$user->delete();		// find user and delete
			}
			return Redirect::to('/bank/sub_users');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}


	//-----------------------------------------------------
    //  Function 3.29: getFormula
	//  Description: get Rating Formula page
    //-----------------------------------------------------
	public function getFormula()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(6);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			// get saved history
			$history = BankFormulaHistory::where('bank_id', Auth::user()->bank_id)->orderBy('id', 'desc')->take(10)->get();
			$formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
			// check if standalone
			$bank_standalone = Bank::find(Auth::user()->bank_id)->is_standalone;

			/* create formula defaults if not present */
			if($formula==null){
				$formula = BankFormula::firstOrCreate(
					array(
						'bank_id' => Auth::user()->bank_id,
						'bcc' => 'rm,cd,sd,boi',
						'mq' => 'boe,mte,bd,sp,ppfi',
						'fa' => 'fp,fr',
						'boost' => 1
					)
				);
			}
			return View::make('bank.formula', array(
				'formula' => $formula,
				'history' => $history,
				'bank_standalone' => $bank_standalone
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.30: postFormula
	//  Description: function to update Rating Formula
    //-----------------------------------------------------
	public function postFormula()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);

			// update fields and save
			$formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
			$formula->bcc = Input::get('bcc');
			$formula->mq = Input::get('mq');
			$formula->fa = Input::get('fa');
			$formula->boost = Input::get('boost');
			$formula->save();

			// create history
			BankFormulaHistory::create(
				array(
					'name' => Input::get('name'),
					'bank_id' => Auth::user()->bank_id,
					'bcc' => Input::get('bcc'),
					'mq' => Input::get('mq'),
					'fa' => Input::get('fa'),
					'boost' => Input::get('boost')
				)
			);

			return 'success';
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.31: getResetFormula
	//  Description: function to reset Rating Formula values
    //-----------------------------------------------------
	public function getResetFormula()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(6);
			$formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
			$formula->bcc = 'rm,cd,sd,boi';					// set all as active
			$formula->mq = 'boe,mte,bd,sp,ppfi';
			$formula->fa = 'fp,fr';
			$formula->boost = 1;
			$formula->save();

			return Redirect::to('/bank/formula');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.32: postSetFormula
	//  Description: function to save Rating Formula updates
    //-----------------------------------------------------
	public function postSetFormula()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			$formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
			$history = BankFormulaHistory::find(Input::get('id'));

			$formula->bcc = $history->bcc;
			$formula->mq = $history->mq;
			$formula->fa = $history->fa;
			$formula->boost = $history->boost;
			$formula->save();

			return 'success';
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.33: getBankSubscribe
	//  Description: get Supervisor Subscribe page
    //-----------------------------------------------------
	public function getBankSubscribe()
	{
		try{
			$calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;
			$tax = 0;
			$result = $calculator->paymentCompute($price, 1);
			return View::make('bank.subscribe', 
				array(
					'price'=> number_format($price, 2), 
					'tax'=>$tax, 
					'totalAmount'=> number_format($result['totalAmount'], 2, '.', ','),
				));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.34: postBankPayViaPaypal
	//  Description: function to process subsciption payment via PayPal
    //-----------------------------------------------------
	public function postBankPayViaPaypal()
	{
		try{
	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']                         = 'SetExpressCheckout';
			$paypal_pdata['VERSION']                        = '86';
			$paypal_pdata['LANDINGPAGE']           			= 'Billing';
	        $paypal_pdata['L_BILLINGTYPE0']                 = 'RecurringPayments';
	        $paypal_pdata['L_BILLINGAGREEMENTDESCRIPTION0'] = 'Supervisor Account Subscription';
	        $paypal_pdata['cancelUrl']                      = URL::to('/').'/bank/dashboard';
	        $paypal_pdata['returnUrl']                      = URL::to('/').'/bank_subscribe/recurring_profile';

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl, CURLOPT_URL, $paypal_url);
	        curl_setopt($curl, CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
	        $result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);
	        $resp_exp   = explode('&', $result);
	        $resp_exp   = explode('=', $resp_exp[0]);

	        $response['token']   = $resp_exp[1];

	        if (env("APP_ENV") == "Production") {
	            return Redirect::To('https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response['token']);
	        }
	        else {
	            return Redirect::To('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response['token']);
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.35: getRecurringProfileViaPaypal
	//  Description: get view for recurring subsciption payment via PayPal
    //-----------------------------------------------------
    public function getRecurringProfileViaPaypal()
	{
		try{
			$calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;	// subscription cost
			$result = $calculator->paymentCompute($price, 1);

	        return View::make('bank.confirm-subscription',
	            array(
	                'total_price'   => $result['totalAmount'],
	                'paypal_token'  => Input::get('token'),
	            )
	        );
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.36: postRecurringProfileViaPaypal
	//  Description: post process recurring subsciption payment via PayPal
    //-----------------------------------------------------
    public function postRecurringProfileViaPaypal()
	{
		try{
	        $calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;	// subscription cost
			$calculationResult = $calculator->paymentCompute($price, 1);

	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']                = 'GetExpressCheckoutDetails';
	        $paypal_pdata['VERSION']               = '86';
	        $paypal_pdata['TOKEN']                 = Input::get('paypal_token');

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl,CURLOPT_URL, $paypal_url);
	        curl_setopt($curl,CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl,CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
			$result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);

	        $resp_exp   = explode('&', $result);
	        $resp_exp   = explode('=', $resp_exp[9]);
			$response['payer_id']   = $resp_exp[1];

	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']             = 'CreateRecurringPaymentsProfile';
	        $paypal_pdata['VERSION']            = '86';
	        $paypal_pdata['TOKEN']              = Input::get('paypal_token');
	        $paypal_pdata['PAYERID']            = $response['payer_id'];
	        $paypal_pdata['PROFILESTARTDATE']   = date('Y-m-d', strtotime("+15 days")).'T'.date('H:i:s').'Z';
			$paypal_pdata['DESC']               = 'Supervisor Account Subscription';
			$paypal_pdata['LANDINGPAGE']        = 'Billing';
	        $paypal_pdata['BILLINGPERIOD']      = 'Month';
	        $paypal_pdata['BILLINGFREQUENCY']   = '1';
	        $paypal_pdata['AMT']                = '1';
	        $paypal_pdata['CURRENCYCODE']       = 'PHP';
	        $paypal_pdata['COUNTRYCODE']        = 'PH';
	        $paypal_pdata['MAXFAILEDPAYMENTS']  = '2';
	        $paypal_pdata['AUTOBILLOUTAMT']     = 'AddToNextBilling';
	        $paypal_pdata['AMT']                = $calculationResult['totalAmount'];
	        //$paypal_pdata['TAXAMT']             = $vat_output;

	        $subscr_exist   = BankSubscription::where('bank_id', Auth::user()->loginid)->first();
	        if ($subscr_exist) {
	            $paypal_pdata['INITAMT']  = $calculationResult['totalAmount'];
	        }

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl, CURLOPT_URL, $paypal_url);
	        curl_setopt($curl, CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
			$profile_result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);

	        $resp_explode   = explode('&', $profile_result);

	        foreach ($resp_explode as $exp) {
				$exp_pair   = explode('=', $exp);
				if(@count($exp_pair) > 1)
	            $success_response[$exp_pair[0]]  = urldecode($exp_pair[1]);
			}

			/* succesful payment */
	        if ($success_response['ACK'] == 'Success') {
				$payment                    = new Payment();
	      		$payment->loginid           = Auth::user()->loginid;
	      		$payment->transactionid     = $success_response['PROFILEID'];
	      		$payment->transactiontype   = 'expresscheckout';
	      		$payment->paymenttype       = 'RecurringPayments';
	      		$payment->paymentamount     = $calculationResult['totalAmount'];
	      		$payment->currencycode      = 'PHP';
	      		$payment->save();

				$payment_id                 = $payment->id;
	            $subs = BankSubscription::firstOrCreate(array(
	                'bank_id' => Auth::user()->loginid
	            ));

	            $date = new DateTime();
	            $date->modify("+1 month");
				$subs->expiry_date = $date->format('Y-m-d 23:59:59');
				$subs->status = 1;
	            $subs->save();

	            $bank_user = User::find(Auth::user()->loginid);

	            if (3 == $bank_user->product_plan) {
	                $bank_user->product_plan    = 1;
	                $bank_user->save();
	            }

	            if ($subscr_exist) {
					/*  Create a payment receipt record on the database */
					$paymentReceiptDb = new PaymentReceipt;

					$paymentReceipt['price'] = $price;
					$paymentReceipt['amount'] = $calculationResult['amount'];
					$paymentReceipt['discount'] = $calculationResult['discountAmount'];
					$paymentReceipt['subtotal'] = $calculationResult['subTotal'];
					$paymentReceipt['vat_amount'] = $calculationResult['taxAmount'];
					$paymentReceipt['total_amount'] = $calculationResult['totalAmount'];
					$paymentReceipt['item_quantity'] = 1;
					$paymentReceipt['login_id'] = Auth::user()->loginid;
					$paymentReceipt['reference_id'] = $payment_id;
					$paymentReceipt['payment_method'] = 1; /*PayPal*/
					$paymentReceipt['item_type'] = 2; /* Bank Subscription */
					$paymentReceipt['sent_status'] = STS_OK;
					$paymentReceipt['date_sent'] = date('Y-m-d');
					/* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
					$paymentReceipt['vatable_amount'] = null;
					$paymentReceiptSts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

	                $receipt = PaymentReceipt::where('receipt_id', $paymentReceiptSts)->first();

					$user = User::find(Auth::user()->loginid);

					if($user->parent_user_id != 0) {
						$user = User::find($user->parent_user_id);
					}

					if (env("APP_ENV") != "local") {
						$mailHandler = new MailHandler();
						$mailHandler->sendReceipt(
							Auth::user()->loginid,
							[
								'receipt' => $receipt,
								'user' => $user
							],
							'CreditBPO Payment Receipt'
						);
					}
	            }
	            return Redirect::to('/bank/dashboard');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.37: postBankPayViaDragonPay
	//  Description: post process subsciption payment via DragonPay
    //-----------------------------------------------------
	public function postBankPayViaDragonPay()
	{
		try{
			$calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;	// subscription cost
			$calculationResult = $calculator->paymentCompute($price, 1);

			$transaction = new Transaction();					// create instance of transaction and save
			$transaction->loginid = Auth::user()->loginid;
			$transaction->amount = $calculationResult['totalAmount'];
			$transaction->ccy = "PHP";
			$transaction->description = "Bank User Subscription";
			$transaction->save();

			$user = User::find(Auth::user()->loginid);
			$user->transactionid = $transaction->id;
			$user->save();

			$data = array(											// values passed as parameters to dragon pay
				'merchant_id'   => $this->merchant_id,
				'txn_id'        => $transaction->id,
				'amount'        => number_format($calculationResult['totalAmount'], 2, ".", ""),
				'currency'      => $transaction->ccy,
				'description'   => $transaction->description,
				'email'         => Auth::user()->email,
				'period'        => 'monthly',
				'frequency'     => '12',
				'key'           => $this->merchant_key
			);

			$seed = implode(':', $data);
			$digest = Sha1($seed);

	        if(env("APP_ENV") == "Production"){
				$recur_url = 'https://gw.dragonpay.ph/RecurPay.aspx';
			} else {
				$recur_url = 'http://test.dragonpay.ph/RecurPay.aspx';
			}

			/*  Create a payment receipt record on the database */
			$paymentReceiptDb = new PaymentReceipt;

			$paymentReceipt['price'] = $price;
			$paymentReceipt['amount'] = $calculationResult['amount'];
			$paymentReceipt['discount'] = $calculationResult['discountAmount'];
			$paymentReceipt['subtotal'] = $calculationResult['subTotal'];
			$paymentReceipt['vat_amount'] = $calculationResult['taxAmount'];
			$paymentReceipt['total_amount'] = $calculationResult['totalAmount'];
			$paymentReceipt['item_quantity'] = 1;
			$paymentReceipt['login_id'] = Auth::user()->loginid;
			$paymentReceipt['reference_id'] = $transaction->id;
			$paymentReceipt['payment_method'] = 2; /*DragonPay*/
			$paymentReceipt['item_type'] = 2; /* Bank Subscription */
			$paymentReceipt['sent_status'] = STS_NG;
			$paymentReceipt['date_sent'] = date('Y-m-d');
			/* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
			$paymentReceipt['vatable_amount'] = null;
			$paymentReceiptDb->addPaymentReceipt($paymentReceipt);

			return Redirect::to(
				$recur_url.
				"?merchantid=".$data['merchant_id'].
				"&txnid=".$data['txn_id'].
				"&amount=".$data['amount'].
				"&ccy=".$data['currency'].
				"&description=".urlencode($data['description']).
				"&email=".urlencode($data['email']).
				"&period=".$data['period'].
				"&frequency=".$data['frequency'].
				"&digest=".$digest
			);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.38: getBankSubscriptionPaymentSuccess
	//  Description: function for Paypal success payment
    //-----------------------------------------------------
	public function getBankSubscriptionPaymentSuccess()
	{
		try{
			$gateway = Omnipay::create('PayPal_Express');

			// set variables
			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setTestMode(false);
			} else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setTestMode(true);
			}
			$params = Session::get('params');
			$response = $gateway->completePurchase($params)->send();
			$paypalResponse = $response->getData(); // this is the raw response object

			if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
				// Response
				$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

				$payment = new Payment();
	      		$payment->loginid = Auth::user()->loginid;
	      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
	      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
	      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
	      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
	      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
	      		$payment->save();

				//create subscription
				if ($payment_check == 0) {
					$subs = BankSubscription::firstOrCreate(array(
						'bank_id' => Auth::user()->loginid
					));

					$date = new DateTime();
					$start_day = $date->format('j');

					$date->modify("+1 month");
					$end_day = $date->format('j');

					if ($start_day != $end_day)
						$date->modify('last day of last month');

					$subs->expiry_date = $date->format('Y-m-d 23:59:59');
					$subs->save();

	                $bank_user = User::find(Auth::user()->loginid);

	                if (3 == $bank_user->product_plan) {
	                    $bank_user->product_plan    = 1;
	                    $bank_user->save();
	                }
				}
				return Redirect::to('/bank/dashboard');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.39: getSmeReportsList
	//  Description: get supervisor Report Listing page
    //-----------------------------------------------------
    public function getSmeReportsList($company_name)
    {
    	try{
	        $entity = DB::table('tblentity')
				->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
				->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
				->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
				->where('tblentity.companyname', '=', $company_name)
	            ->orderBy('tblentity.entityid', 'desc');


	        if(Auth::user()->can_view_free_sme==1){
				$entity->where(function($query){
					$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$entity->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
			}

	        $entity = $entity->get();

	        foreach ($entity as $key => $value) {
	          $entity[$key]->m_score = App::make('ProfileSMEController')->computeBeneishScore($entity[$key]->entityid);
	        }

	        return View::make('dashboard.index')->with('entity', $entity);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.40: getSendSMEReportToSupervisor
	//  Description: function to email SME report to supervisor
    //-----------------------------------------------------
	public function getSendSMEReportToSupervisor($id){
		try{
			$entity = DB::table('tblentity')
				->where('tblentity.current_bank', '=', Auth::user()->bank_id)
				->where('tblentity.entityid', $id)->first();

			if($entity!=null && $entity->cbpo_pdf!=""){
				if (env("APP_ENV") != "local") {

					try{
						Mail::send('emails.supervisor-send', array('entity'=>$entity),
							function($message) use ($entity){
								$message->to(Auth::user()->email, Auth::user()->email)
									->from('notify.user@creditbpo.com', 'CreditBPO')
									->subject('CreditBPO - ' . $entity->companyname . ' Report')
									->attach('documents/'.$entity->cbpo_pdf);

								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							}
						);
					}catch(Exception $e){
		            //
		            }
				}
				return "success";
			} else {
				return "error";
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.41: checkSubscriptionSts
	//  Description: function to check subscription status of supervisor
    //-----------------------------------------------------
    public static function checkSubscriptionSts($login_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
	        /*------------------------------------------------------------------*/
			$sts    = STS_NG;

			\Log::info($login_id);

			$loginData = User::where('loginid', '=', $login_id)->first();

			$organization = Bank::where('id', '=', $loginData->bank_id)->first();

			$perm = Permissions::get_bank_subscription();
			
			if($organization->is_custom == 1){
				$sts = STS_OK;
				return $sts;
			}

			if ($loginData->bypass_subscriptions_checks == 1 && strtotime($perm) > strtotime(date('c'))) {
				$sts = STS_OK;
				return $sts;
			} 
			$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id", $login_id)->first();

			$now = date("Y-m-d H:i:s"); //created_at
			if($supSubscription->expiry_date > $now){
				$sts = STS_OK;
			}

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.42: checkPaypalSubscriptionSts
	//  Description: function to check recurring subscription status from Paypal
    //-----------------------------------------------------
    private static function checkPaypalSubscriptionSts($login_id)
    {
        try{
	        $sts            = STS_NG;

	        $subscription   = Payment::where('loginid', $login_id)
	            ->where('paymenttype', 'RecurringPayments')
	            ->orderBy('paymentid', 'desc')
	            ->first();

	        if (0 < @count($subscription)) {

	            if(env("APP_ENV") == "Production") {
	                $paypal_url                     = 'https://api-3t.paypal.com/nvp';
					$paypal_pdata['USER']           = 'lia.francisco_api1.gmail.com';
					$paypal_pdata['PWD']            = 'Z8FEJ5G84FBQNKDJ';
					$paypal_pdata['SIGNATURE']      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
				} else {
	                $paypal_url                     = 'https://api-3t.sandbox.paypal.com/nvp';
					$paypal_pdata['USER']           = 'lia.francisco-facilitator_api1.gmail.com';
					$paypal_pdata['PWD']            = '87DWVXH4UGHD46W3';
					$paypal_pdata['SIGNATURE']      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
				}
				$paypal_pdata['METHOD']             = 'GetRecurringPaymentsProfileDetails';
	            $paypal_pdata['VERSION']            = '86';
	            $paypal_pdata['PROFILEID']          = $subscription->transactionid;

	            /*--------------------------------------------------------------------
	            /*	Send POST request to Highcharts export Server via CuRL
	            /*------------------------------------------------------------------*/
	            /* Initializes the CuRL Library */
	            $curl = curl_init();

	            /* Sets the CuRL POST Request configuration  */
	            curl_setopt($curl,CURLOPT_URL, $paypal_url);
	            curl_setopt($curl,CURLOPT_POST, sizeof($paypal_pdata));
	            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	            curl_setopt($curl,CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	            /* Execute CuRL Request */
	            $profile_result = curl_exec($curl);

	            /*  Closes the CuRL Library */
	            curl_close($curl);

	            $resp_explode   = explode('&', $profile_result);

	            foreach ($resp_explode as $exp) {
					$exp_pair   = explode('=', $exp);
					if(@count($exp_pair) > 1)
	                	$success_response[$exp_pair[0]]  = urldecode($exp_pair[1]);
				}

	            if (isset($success_response) && 'Success' == $success_response['ACK']) {
	                if ('Active' == $success_response['STATUS']) {
	                    $sts = STS_OK;
	                }
	            }
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.43: checkDragonSubscriptionSts
	//  Description: function to check recurring subscription status from Dragonpay
    //-----------------------------------------------------
    private static function checkDragonSubscriptionSts($login_id)
    {
        try{
	        $sts            = STS_NG;

	        $subscription   = Transaction::where('loginid', $login_id)
	            ->where('description', 'Bank User Subscription')
	            ->orderBy('id', 'desc')
	            ->first();

	        if (0 < @count($subscription)) {
	            switch ($subscription->status) {
	                case NULL:
	                case 'S':
	                case 'P':
	                    $sts = STS_OK;
	                    break;
	            }
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.44: getDocumentOptions
	//  Description: get document options page
    //-----------------------------------------------------
	public function getDocumentOptions()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);
			Permissions::check(8);

			$options = BankDocOption::where('bank_id', Auth::user()->bank_id)->where('is_active', 1)->first();

	        /*  Gets the Bank's Custom Documents    */
	        $docs_db        = new BankCustomDocs;
	        $custom_docs    = $docs_db->getDocumentsByBank(Auth::user()->bank_id);

			// create if not found
			if($options==null){
				$options = new stdClass;
				$options->corp_sec_reg                  = CMN_ON;
				$options->corp_sec_gen                  = CMN_ON;
				$options->corp_business_license         = CMN_ON;
				$options->corp_tax_reg                  = CMN_ON;
				$options->corp_itr                      = CMN_ON;
				$options->sole_business_license         = CMN_ON;
				$options->sole_business_owner           = CMN_OFF;
				$options->sole_tax_reg                  = CMN_ON;
				$options->sole_itr                      = CMN_ON;
				$options->boi                           = CMN_OFF;
				$options->peza                          = CMN_OFF;
				$options->fs_template                   = CMN_ON;
				$options->balance_sheet                 = CMN_OFF;
				$options->income_statement              = CMN_OFF;
				$options->cashflow_statement            = CMN_OFF;
				$options->bank_statement                = CMN_OFF;
				$options->utility_bill                  = CMN_OFF;
			}

			$history = BankDocOption::where('bank_id', Auth::user()->bank_id)
				->where('is_active', 0)
	            ->get();
			$bank_standalone = Bank::find(Auth::user()->bank_id)->is_standalone;

			return View::make('bank.doc_options', array(
				'options'           =>$options,
				'history'           =>$history,
				'bank_standalone'   => $bank_standalone,
				'custom_docs'       => $custom_docs
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.45: postDocumentOptions
	//  Description: post process for document options page
    //-----------------------------------------------------
	public function postDocumentOptions()
	{
		try{
			BankDocOption::where('bank_id', Auth::user()->bank_id)->update(array('is_active' => 0));

			// create the bank document options
			$options = BankDocOption::create(array(
				'bank_id'                       => Auth::user()->bank_id,
				'name'                          => Input::get('name'),
				'is_active'                     => 1,
				'corp_sec_reg'                  => Input::get('corp_sec_reg', 0),
				'corp_sec_gen'                  => Input::get('corp_sec_gen', 0),
				'corp_business_license'         => Input::get('corp_business_license', 0),
				'corp_board_resolution'         => 0,
				'corp_board_resolution_pres'    => 0,
				'corp_tax_reg'                  => Input::get('corp_tax_reg', 0),
				'corp_itr'                      => Input::get('corp_itr', 0),
				'sole_business_license'         => Input::get('sole_business_license', 0),
				'sole_business_owner'           => Input::get('sole_business_owner', 0),
				'sole_tax_reg'                  => Input::get('sole_tax_reg', 0),
				'sole_itr'                      => Input::get('sole_itr', 0),
				'boi'                           => Input::get('boi', 0),
				'peza'                          => Input::get('boi', 0),
				'bank_statement'                => Input::get('bank_statement', 0),
				'utility_bill'                  => Input::get('utility_bill', 0),
			));

			return Redirect::to('/bank/document_options');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.46: postDocumentOptionsLoad
	//  Description: function to load previous document options form history
    //-----------------------------------------------------
	public function postDocumentOptionsLoad()
	{
		try{
			$id = Input::get('id');
			BankDocOption::where('bank_id', Auth::user()->bank_id)->update(array('is_active' => 0));
			BankDocOption::where('id', $id)->update(array('is_active' => 1));
			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.47: getNFCC
	//  Description: get COMPUTATION OF NET FINANCIAL CONTRACTING CAPACITY page
    //-----------------------------------------------------
	public function getNFCC()
	{
		try{
			$entity = DB::table('tblentity')
				->leftJoin('financial_reports', 'financial_reports.entity_id', '=', 'tblentity.entityid')
				->whereNotNull('financial_reports.id')
				->where('tblentity.status', 7)
				->groupBy('tblentity.entityid')
				->orderBy('tblentity.entityid', 'desc');

	        if(Auth::user()->can_view_free_sme==1){
				$entity->where(function($query){
					$query->where('tblentity.current_bank', '=', Auth::user()->bank_id)->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$entity->where('tblentity.current_bank', '=', Auth::user()->bank_id)->where('tblentity.is_independent', 1);
			}

	        $entity = $entity->get();

	        return View::make('bank.nfcc')->with('entity', $entity);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.48: getNFCCFSdata
	//  Description: function to auto populate NFCC depending on selected SME
    //-----------------------------------------------------
	public function getNFCCFSdata()
	{
		try{
			$entity_id = Input::get('entity_id');
			if($entity_id){
				$fr =  FinancialReport::select(array('financial_reports.money_factor', 'fin_balance_sheets.CurrentAssets', 'fin_balance_sheets.CurrentLiabilities'))
						->where('entity_id', $entity_id)
						->leftJoin('fin_balance_sheets', 'fin_balance_sheets.financial_report_id', '=', 'financial_reports.id')
						->orderBy('financial_reports.id', 'desc')->first();

				return $fr;
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.49: getQuestionnaireConfigPage
	//  Description: get Supervisor Questionnaire Config page
    //-----------------------------------------------------
    public function getQuestionnaireConfigPage()
    {
    	try{
	        if (Auth::user()->product_plan==3) {
	            App::abort(404);
	        }

			Permissions::check(9);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

	        $config_db      = new QuestionnaireConfig;
	        $config_data    = $config_db->getQuestionnaireConfigbyBankID(Auth::user()->bank_id);
	        $algo_related   = array('major_customer', 'major_supplier', 'owner_details', 'company_succession', 'risk_assessment');

	        $biz1_sections  = array('affiliates', 'products', 'main_locations', 'capital_details', 'branches', 'import_export', 'insurance', 'cash_conversion');
	        $biz1_lbl       = array('Affiliates', 'Products and Services', 'Main Locations', 'Capital Details', 'Branches', 'Import and Export', 'Insurance', 'Cash Conversion Cycle');

	        $biz2_sections  = array('major_customer', 'major_supplier', 'major_market', 'competitors', 'business_drivers', 'customer_segments', 'past_projects', 'future_growth');
	        $biz2_lbl       = array('Major Customer', 'Major Supplier', 'Major Market', 'Competitors', 'Business Drivers', 'Customer Segments', 'Past Projects Completed', 'Future Growth Initiatives');

	        $owner_sections = array('owner_details', 'directors', 'shareholders');
	        $owner_lbl      = array('Owner Details / Key Managers', 'Directors', 'Shareholders');

	        $ecf_sections   = array('ecf');
	        $ecf_lbl        = array('Existing Credit Facilities');

	        $cns_sections   = array('company_succession', 'landscape', 'economic_factors', 'tax_payments', 'risk_assessment', 'growth_potential', 'investment_expansion');
	        $cns_lbl        = array('Company Head Succession', 'Landscape', 'Important Economic Factors', 'Tax Payments', 'Risk Assessment', 'Revenue Growth Potential', 'Capital Investment for Expansion');

	        $rcl_sections   = array('rcl');
	        $rcl_lbl        = array('Required Credit Facilities');

			$bank_standalone = Bank::find(Auth::user()->bank_id)->is_standalone;
	        return View::make('bank.questionnaire-config',
	            array(
	                'config_data'       => $config_data,
	                'algo_related'      => $algo_related,

	                'biz1_sections'     => $biz1_sections,
	                'biz1_lbl'          => $biz1_lbl,

	                'biz2_sections'     => $biz2_sections,
	                'biz2_lbl'          => $biz2_lbl,

	                'owner_sections'    => $owner_sections,
	                'owner_lbl'         => $owner_lbl,

	                'ecf_sections'      => $ecf_sections,
	                'ecf_lbl'           => $ecf_lbl,

	                'cns_sections'      => $cns_sections,
	                'cns_lbl'           => $cns_lbl,

	                'rcl_sections'      => $rcl_sections,
	                'rcl_lbl'           => $rcl_lbl,

					'bank_standalone' => $bank_standalone
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.50: postQuestionnaireConfigPage
	//  Description: function to update changes in Supervisor Questionnaire Config page
    //-----------------------------------------------------
    public function postQuestionnaireConfigPage()
    {
    	try{
	        $sts            = STS_NG;
	        $config_item    = Input::all();

	        $config_db      = new QuestionnaireConfig;
	        $config_exists  = $config_db->checkQuestionnaireConfigExist(Auth::user()->bank_id);

	        if (STS_NG == $config_exists) {
	            $question_upd           = $config_db->addConfigByQuestionType(Auth::user()->bank_id, 0);
	        }

	        foreach ($config_item as $key => $val) {
	            $upd_config = $config_db->setQuestionnaireSectionConfig(Auth::user()->bank_id, $key, $val);
	        }

	        $sts = $upd_config;

	        return $sts;

	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.51: postCustomDocument
	//  Description: Adds / Edit a Custom Document
    //-----------------------------------------------------
    public function postCustomDocument()
    {
    	try{
	        $sts                = STS_NG;
	        $data['doc_id']     = Input::get('doc_id');
	        $data['doc_lbl']    = Input::get('doc_lbl');

	        if (STR_EMPTY != $data['doc_lbl']) {
	            $docs_db    = new BankCustomDocs;
	            $sts        = $docs_db->saveCustomDoc($data);
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.52: deleteCustomDocument
	//  Description: Deletes a Custom Document
    //-----------------------------------------------------
    public function deleteCustomDocument($document_id)
    {
    	try{
	        $sts        = STS_NG;

	        $docs_db    = new BankCustomDocs;
	        $sts        = $docs_db->deleteCustomDoc($document_id);

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.53: getCustomDocument
	//  Description: Gets a Custom Document's data
    //-----------------------------------------------------
    public function getCustomDocument($document_id)
    {
    	try{
	        $data    = BankCustomDocs::find($document_id);

	        return json_encode($data);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.54: setCustomDocumentConfig
	//  Description: Sets the Custom Document to required or not
    //-----------------------------------------------------
    public function setCustomDocumentConfig()
    {
        try{
	        $data['document_id']    = Input::get('document_id');
	        $data['bank_required']  = STS_NG;

	        if ('true' == Input::get('required')) {
	            $data['bank_required']  = STS_OK;
	        }

	        $docs_db    = new BankCustomDocs;
	        $sts        = $docs_db->setCustomDocConfig($data);

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.55: getScoreAverage
	//  Description: Computes and Converts the Average Score
    //-----------------------------------------------------
    public function getScoreAverage($score, $total_count)
    {
    	try{
	        $fscore_avg     = 0;
	        $fscore_conv    = STR_EMPTY;

	        if (0 != $total_count) {
	            $fscore_avg   = $score / $total_count;

	            if($fscore_avg >= 177) {
	                $fscore_conv   = 'AA';
	            }
	            else if ($fscore_avg >= 150 && $fscore_avg <= 176) {
	                $fscore_conv   = 'A';
	            }
	            else if ($fscore_avg >= 123 && $fscore_avg <= 149) {
	                $fscore_conv   = 'BBB';
	            }
	            else if ($fscore_avg >= 96 && $fscore_avg <= 122) {
	                $fscore_conv   = 'BB';
	            }
	            else if ($fscore_avg >= 69 && $fscore_avg <= 95) {
	                $fscore_conv   = 'B';
	            }
	            else if ($fscore_avg < 69){
	                $fscore_conv   = 'CCC';
	            }
	        }

	        return $fscore_conv;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 3.56: updateApprovalSts
	//  Description: Update Approval Status of SMEs
    //-----------------------------------------------------
    public function updateApprovalSts()
    {

    	try{
	        /*  Variable Declaration    */
	        $report_id      = Input::get('upd-approval-id');
	        $report_sts     = Input::get('upd-sts-select');
	        $rules          = array();
	        $messages       = array();

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        $rules  = array(
	            'upd-approval-id'   => 'required',
	            'upd-sts-select'    => 'required',
	        );

	        $messages   = array(
	            'upd-sts-select.required'   => 'Please select a Status'
	        );

	        /*  Run the Laravel Validation  */
			$validator = Validator::make(Input::all(), $rules, $messages);

	        /*  Input validation failed     */
			if ($validator->fails()) {
				$srv_resp['messages']	= $validator->messages()->all();
			}
	        else {
	            $data = array(
	                'approval_status'   => $report_sts,
	                'approval_date'     => date('Y-m-d')
	            );

	            switch ($report_sts) {
	                case LOAN_APPROVED_STS:
	                    $srv_resp['sts_lbl']    = 'Loan Approved';
	                    break;

	                case PROJ_APPROVED_STS:
	                    $srv_resp['sts_lbl']    = 'Project Approved';
	                    break;

	                case LOAN_DENIED_STS:
	                    $srv_resp['sts_lbl']    = 'Loan Denied';
	                    break;

	                case PROJ_DENIED_STS:
	                    $srv_resp['sts_lbl']    = 'Project Denied';
	                    break;

	                case IN_PROCESS_STS:
	                default:
	                    $srv_resp['sts_lbl']    = 'In Process';
	                    break;
	            }

	            DB::table('tblentity')
	                ->where('entityid', $report_id)
	                ->update($data);

	            $srv_resp['report_id']      = $report_id;
	            $srv_resp['sts']            = STS_OK;
	            $srv_resp['messages']     = 'Successfully Updated Record';
	        }

	        /*  Encode server response variable to JSON for output  */
	        return json_encode($srv_resp);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 3.57: checkInProcessSts
	//  Description: 
    //-----------------------------------------------------
    public function checkInProcessSts()
    {
    	try{
	        $week_ago   = date('Y-m-d', strtotime('-7 days'));
	        $companies  = array();

	        $reports    = DB::table('tblentity')
	            ->where('is_independent', STS_OK)
	            ->where('current_bank', Auth::user()->bank_id)
	            ->where('approval_status', IN_PROCESS_STS)
	            ->where('approval_date', '<=', $week_ago)
	            ->get();

	        foreach ($reports as $report) {
	            $companies[]    = $report->companyname;
	        }

	        return json_encode($companies);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 3.58: _checkAllSubscriptionConditions
	//  Description: Returns nothing if everything is ok, otherwise it returns the correct view to show
    //-----------------------------------------------------
	private function _checkAllSubscriptionConditions()
	{
		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			//supervisor subscription
			$supAccess = true;
			if(Auth::user()->role == 4 && $organization->is_custom == 0) {
				$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",Auth::user()->loginid)->get();
				foreach($supSubscription as $sup) {
					//set 30 days access for unsubscribe supervisor
					$days =  strtotime(date('c')) - strtotime($sup->updated_at);
					$days = $days/86400;
					// if less than or equal to 30 allow access
					if($days > 30 && $sup->status !== null) {
						$supAccess = false;
					}
					break;
				}
			}
			if($this->accountDetails['bypass_subscriptions_checks'] == 0 && Auth::user()->role != 0){
				if (($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) || ($supAccess == true && $organization->is_custom == 1)) {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					/*----------------------------------------------------------
					|	Check Paypal Subscription
					|---------------------------------------------------------*/
					$subs_sts   = self::checkSubscriptionSts($bank_login_id);
					
					$user = User::find($bank_login_id);

					if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !(strtotime($this->bank_subscription) > strtotime(date('c')))) {
						
						$error = array('Please complete your billing details before purchasing.');
						return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
					}

					if (STS_NG == $subs_sts) {
						return Redirect::to('/bank/subscribe');
					}
					else if (2 == $subs_sts) {
						$subscription   = Transaction::where('loginid', $bank_login_id)
							->where('description', 'Bank User Subscription')
							->orderBy('id', 'desc')
							->first();

						/* Redirect to Pending Screen */
						return View::make('payment.pending', array('refno' => $subscription['refno']));
					}
					else {
						/** No Processing   */
					}
				}
				else {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					$user = User::find($bank_login_id);

					if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name = "") && !(strtotime($this->bank_subscription) > strtotime(date('c')))) {
						
						$error = array('Please complete your billing details before purchasing.');
						return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
					}

					return Redirect::to('/bank/subscribe');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}	
	}

	//-----------------------------------------------------
    //  Function 3.58: _returnSubscriptions
	//  Description: Returns nothing if everything is ok, otherwise it returns the correct view to show
    //-----------------------------------------------------
	private function _returnSubscriptions()
	{

		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			//supervisor subscription
			$supAccess = true;
			if(Auth::user()->role == 4 && $organization->is_custom == 0) {
				$supSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",Auth::user()->loginid)->get();
				foreach($supSubscription as $sup) {
					//set 30 days access for unsubscribe supervisor
					$days =  strtotime(date('c')) - strtotime($sup->updated_at);
					$days = $days/86400;
					// if less than or equal to 30 allow access
					if($days > 30 && $sup->status !== null) {
						$supAccess = false;
					}
					break;
				}
			}
			if($this->accountDetails['bypass_subscriptions_checks'] == 0 && Auth::user()->role != 0){
				if (($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) || ($supAccess == true && $organization->is_custom == 1)) {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					/*----------------------------------------------------------
					|	Check Paypal Subscription
					|---------------------------------------------------------*/
					$subs_sts   = self::checkSubscriptionSts($bank_login_id);
					
					$user = User::find($bank_login_id);

					if (STS_NG == $subs_sts) {
						return 0;
					}
				}
				else {
					$bank_login_id = $this->accountDetails['loginid'];

					if ($this->accountDetails['parent_user_id'] != 0) {
						$bank_login_id = $this->accountDetails['parent_user_id'];
					}

					$user = User::find($bank_login_id);

					return 0;
				}
			}

			return 1;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.59: getNotificationOptions
	//  Description: Shows the notification options
    //-----------------------------------------------------
	public function getNotificationOptions()
	{
		try{
			$timezoneList = array();
			$this->bank_subscription = Permissions::get_bank_subscription();			// get subscription status
			
			Permissions::check(10);
	        
			$result = $this->_checkAllSubscriptionConditions();
			if($result != "") 
				return $result;
				
			$bankNotifSettings = BankNotifications::where('bank_id', $this->accountDetails['bank_id'])->first();

			if(!$bankNotifSettings){
				$newBankNotifSettings = new BankNotifications();
				$newBankNotifSettings->bank_id = $this->accountDetails['bank_id'];
				$newBankNotifSettings->notif_new_report = 0;
				$newBankNotifSettings->notif_client_key_use = 0;
				$newBankNotifSettings->notif_keys_available = 0;
				$newBankNotifSettings->notif_keys_almost_out = 0;
				$newBankNotifSettings->notif_no_more_keys = 0;
				$newBankNotifSettings->keys_number_trigger = 5;
				$newBankNotifSettings->created_date = date('Y-m-d H:i:s');
				$newBankNotifSettings->updated_date = date('Y-m-d H:i:s');
				$newBankNotifSettings->save();
				$bankNotifSettings = $newBankNotifSettings;
			}

			$bankEmailNotifList = BankEmailNotifications::where('bank_id', $this->accountDetails['bank_id'])->where('status', 1)->get();

			$timezones = DateTimeZone::listIdentifiers();
			foreach ($timezones as $timezone) {
				$timezoneList[$timezone] = $timezone;
			}

			return View::make('bank.notifications')->with(
				array('bankNotifSettings'=>$bankNotifSettings, 'bankEmailNotifList'=>$bankEmailNotifList, 'timezoneList' => $timezoneList));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.60: deleteNotificationEmailSub
	//  Description: Set the status of notification to read
    //-----------------------------------------------------
	public function deleteNotificationEmailSub($emailnotifid)
	{
		try{
			$bankEmailNotif = BankEmailNotifications::where('email_notif_id', $emailnotifid)->first();
			$bankEmailNotif->status = 0;
			$bankEmailNotif->save();

			return Redirect::to('/bank/notifications');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.61: postNewNotificationEmailSub
	//  Description: Generates a new notification email
    //-----------------------------------------------------
	public function postNewNotificationEmailSub()
	{
		try{
	        $messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to add is already added.'
	        );

			$rules = array(									// validation rules
		        'email'	=> 'required|email|unique:tblemailnotif,email,0,status'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bank/notifications')->withErrors($validator->errors())->withInput();
			}
			else
			{
				// add email
				$notifSub = new BankEmailNotifications();
	      		$notifSub->email = Input::get('email');
	      		$notifSub->status = 1;
				$notifSub->bank_id = $this->accountDetails['bank_id'];
				$notifSub->created_date = date('Y-m-d H:i:s');
				$notifSub->updated_date = date('Y-m-d H:i:s');
	      		$notifSub->save();

				return Redirect::to('/bank/notifications');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.62: postNotificationSettings
	//  Description: Post process to save notification settings
    //-----------------------------------------------------
	public function postNotificationSettings()
	{
		try{
			$messages = array(  							// validation message
				'numberkeytrigger.required'  => 'The number of keys should not be empty.',
	        );

			$rules = array(									// validation rules
				'numberkeytrigger'	=> 'required',
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bank/notifications')->withErrors($validator->errors())->withInput();
			}
			else
			{
				$bankNotifSettings = BankNotifications::where('bank_id', $this->accountDetails['bank_id'])->first();
				$time = request('time_trigger');
				$timezone = request('timezone');
				$convertedTime = Carbon::parse($time, $timezone)->setTimezone('Asia/Manila');

				if(Input::get('notif_daily')){
					$bankNotifSettings->notif_daily = 1;
					$bankNotifSettings->time_trigger = $convertedTime;
					$bankNotifSettings->timezone = $timezone;
					$bankNotifSettings->original_time = $time;
					
					$bankNotifSettings->notif_reports_started = 0;
					$bankNotifSettings->notif_reports_completed = 0;

					if (request('notif_reports_started')) {
						$bankNotifSettings->notif_reports_started = 1;
					}

					if (request('notif_reports_completed')) {
						$bankNotifSettings->notif_reports_completed = 1;
					}
				}else{
					$bankNotifSettings->notif_daily = 0;
					$bankNotifSettings->notif_reports_started = 0;
					$bankNotifSettings->notif_reports_completed = 0;
					$bankNotifSettings->time_trigger = null;
					$bankNotifSettings->timezone = null;
					$bankNotifSettings->original_time = null;
				}

				if(Input::get('newreport')){
					$bankNotifSettings->notif_new_report = 1;
				}else{
					$bankNotifSettings->notif_new_report = 0;
				}

				if(Input::get('keyuse')){
					$bankNotifSettings->notif_client_key_use = 1;
				}else{
					$bankNotifSettings->notif_client_key_use = 0;
				}

				if(Input::get('keysavail')){
					$bankNotifSettings->notif_keys_available = 1;
				}else{
					$bankNotifSettings->notif_keys_available = 0;
				}

				if(Input::get('keysalmostout')){
					$bankNotifSettings->notif_keys_almost_out = 1;
				}else{
					$bankNotifSettings->notif_keys_almost_out = 0;
				}

				if(Input::get('nomorekeys')){
					$bankNotifSettings->notif_no_more_keys = 1;
				}else{
					$bankNotifSettings->notif_no_more_keys = 0;
				}

				$bankNotifSettings->keys_number_trigger = Input::get('numberkeytrigger');
				$bankNotifSettings->updated_date = date('Y-m-d H:i:s');
				$bankNotifSettings->save();

				return Redirect::to('/bank/notifications');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.63: getReportsSubmitted
	//  Description: Gets the Submitted Reports
    //-----------------------------------------------------
	public function getReportsSubmitted()
	{
		try{
			$entity = DB::table('tblentity')
				->select(
					'tblentity.entityid',
					'tblentity.industry_main_id',
					'tblentity.industry_sub_id',
					'tblentity.industry_row_id',
					'tblentity.created_at',
					'tblindustry_main.main_title',
					'zipcodes.region',
					'tblsmescore.created_at as completed_date',
					'tbllogin.loginid'
				)
				->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
				->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
				->leftJoin('tblsmescore', 'tblsmescore.entityid', '=', 'tblentity.entityid')
				->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid');
				//->leftJoin('investigator_tag', 'investigator_tag.entity_id', '=', 'tblentity.entityid')
				//->leftJoin('investigator_notes', 'investigator_notes.entity_id', '=', 'tblentity.entityid');


				/* filtering results if filters are present */
				if(Input::has('type'))
				{
					if(Input::get('type')=='location' && Input::get('province')!='') {
						if(Input::get('city')!=''){
							$entity->where('tblentity.cityid', Input::get('city'));
						} else {
							$entity->where('tblentity.province', Input::get('province'));
						}
					} elseif (Input::get('type')=='date_established' && Input::get('date_established')!='') {
						switch(Input::get('date_established_compare')){
							case 0: $op = '='; break;
							case 1: $op = '>'; break;
							case 2: $op = '<'; break;
							default: $op = '=';
						}
						$entity->where('tblentity.date_established', $op,Input::get('date_established'));
					} elseif (Input::get('type')=='industry' && Input::get('industry')!='') {
						$entity->where('tblindustry_main.industry_main_id', Input::get('industry'));
					} elseif (Input::get('type')=='companyname') {
						$entity->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
					} elseif (Input::get('type')=='invst_sts' && Input::get('invst_sts')!='') {
						if(Input::get('invst_sts')=='0'){
							$entity->where('tblentity.status', '!=', 7);
						} elseif (Input::get('invst_sts')=='1'){
							$entity->where('tblentity.status', '=', 7);
							$entity->whereNull('investigator_tag.id');
							$entity->whereNull('investigator_notes.status');
						} elseif (Input::get('invst_sts')=='2'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where(function($query)
								{
									$query->whereNull('investigator_notes.status')
										  ->orWhere('investigator_notes.status', 0);
								});
						} elseif (Input::get('invst_sts')=='3'){
							$entity->where('tblentity.status', '=', 7);
							$entity->where('investigator_tag.id', '>', 0);
							$entity->where('investigator_notes.status', '>', 0);
						}
					} elseif (Input::get('type')=='status' && Input::get('status')!=''){
						if(Input::get('status')=='0'){
							$entity->whereIn('tbllogin.status', array(0, 1));
							$entity->where('tblentity.status', '!=', 7);
						} elseif (Input::get('status')=='1'){
							$entity->where('tbllogin.status', 2);
							$entity->where('tblentity.status', '!=', 7);
						} elseif (Input::get('status')=='2'){
							$entity->where('tbllogin.status', 2);
						$entity->where('tblentity.status', 7);
						}
					} else {
						$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
					}
				} else {
					$entity->whereIn('tblentity.status', array(0, 1, 2, 7));
				}

				if ($this->accountDetails['can_view_free_sme'] == 1) {
				$entity->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$entity->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			$entity->where('completed_date', '>=', \Carbon\Carbon::now()->startOfWeek());
			$entity->where('completed_date', '<=',  \Carbon\Carbon::now()->endOfWeek());

			$entity = $entity->groupBy('entityid')->get();

			if(@count($entity) == 0) {
				return array("success" => false, "msg" => "no data found.");
			}
			$results = array(
					"Mon"=>array(),
					"Tue"=>array(),
					"Wed"=>array(),
					"Thu"=>array(),
					"Fri"=>array(),
					"Sat"=>array(),
					"Sun"=>array()
				);
			$time_avg = $results;
			$temp = array();
			$industries = array();
			foreach($entity as $reports)
			{
				$index = date("D", strtotime($reports->completed_date));
				/* Get Completed Time */
				$date1 = new DateTime($reports->created_at);
				$date2 = new DateTime($reports->completed_date);
				$avg = date_diff( $date1, $date2, TRUE);
				// Convert hour to mins
				$reports->completion_time =($avg->days * 24 * 60) + ($avg->h * 60) + $avg->i;
				$temp[] = $reports->completion_time;
				$reports->time_info = $avg;
				$time_avg[$index][] = $reports->completion_time;

				// Group Industry
				$industries[$reports->industry_main_id][] = $reports;

				$results[$index][] = $reports;
			}
			$time_avg_per_day = array();
			
			foreach($time_avg  as $key => $day)
			{
				if(@count($time_avg[$key] ) !=0 ){
					$time_avg_per_day[$key] = ceil(array_sum($time_avg[$key])/@count($time_avg[$key]));
				} else {
					$time_avg_per_day[$key] = 0;
				}
			}
			
			// Try Compute All Data in a Week
			if(@count($temp) != 0){
				$time_avg_per_day['total'] = array_sum($temp)/@count($temp);
			}

			$industry_sub = array();
			$industry_main = array();
			$industry_row = array();

			/** Get Industry Composition for Reports */
			foreach($industries as $key => $industry){
				$industry_main[$key] = $industry;
				foreach($industry as $sub_industry){
					$sub_title = DB::table("tblindustry_sub")->select("sub_title")->where("industry_sub_id",$sub_industry->industry_sub_id)->first();
					$sub_industry->sub_title = !empty($sub_title->sub_title) ? $sub_title->sub_title:"";
					$industry_sub[$sub_industry->industry_sub_id][]= $sub_industry;
					$row_title = DB::table("tblindustry_row")->select("row_title")->where("industry_row_id",$sub_industry->industry_row_id)->first();
					$sub_industry->row_title = !empty($row_title->row_title) ? $row_title->row_title:'';
					$industry_row[$sub_industry->industry_row_id][]= $sub_industry;
				}
			}
			$data['main']= $industry_main;
			$data["sub"] = $industry_sub;
			$data['row'] = $industry_row;
			return array("success"=>true,"data"=>$results, "time_avg" => $time_avg_per_day,"industry"=>$data);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.64: getReportsbyRegion
	//  Description: Get reports based on region
    //-----------------------------------------------------
	public function getReportsbyRegion()
	{
		try{
			$get  = Input::get("filter");
			$industry = Input::get("industry");
			if(!empty($get)) {
				$zips = DB::table('refcitymun')->select('zipcode')->whereIn('regDesc', $get)->get();
			} else {
				$zips = DB::table('refcitymun')->select('zipcode')->get();
			}
			$where = array();
			foreach ($zips as $zip) {
				if(!in_array($zip->zipcode,$where)){
					array_push($where, $zip->zipcode);
				}
			}

			// get data
			$data = DB::table('tblentity')->select(DB::raw('
				tblentity.entityid,
				tblindustry_main.main_title as industry_main,
				tblindustry_sub.sub_title as industry_sub,
				tblindustry_group.group_description as industry_row,
				tblindustry_main.main_code as industry_main_id,
				tblindustry_sub.sub_code as industry_sub_id,
				tblindustry_group.group_code as industry_row_id,
				count(tblentity.industry_row_id) as value,
				tblsmescore.*,
				(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bgroup,
				(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mgroup,
				tblsmescore.score_facs as fgroup,
				CAST(tblsmescore.score_sf AS DECIMAL) as creditbpo_rating,
				tblentity.industry_main_id
			'))
			->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
			->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
			->leftJoin('tblindustry_group', 'tblentity.industry_row_id', '=', 'tblindustry_group.group_code')
			->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
			->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
			->where('tblentity.isIndustry', 1)
			->where('tblentity.is_deleted', 0)
			->groupBy('tblentity.industry_main_id')->groupBy('tblentity.industry_sub_id')->groupBy('tblentity.industry_row_id')
			->orderBy('tblindustry_main.main_title')->orderBy('tblindustry_sub.sub_title')->orderBy('tblindustry_group.group_description');

			if($this->accountDetails['can_view_free_sme'] ==1){
				$data->where(function($query){
					$query->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->orwhere('tblentity.is_independent', 0);
				});
			} else {
				$data->where('tblentity.current_bank', '=', $this->accountDetails['bank_id'])->where('tblentity.is_independent', 1);
			}
			if(@count($get) > 0){
				$data->whereIn('refcitymun.regDesc', $get);
			}

			$data = $data->get();

			// series
			$color = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1", "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#D809CF", "#0AEE75", "#F8FA49",  "#F79730", "#F730B8", "#88FA69"];
			$total = 0;

			$series1 = array();
			$series1_temp = '';
			$series1_index = -1;
			$series2 = array();
			$series2_temp = '';
			$series2_index = -1;
			$series3 = array();
			$series3_temp = '';
			$series3_index = -1;

			$report_cntr    = 0;
			$report2_cntr   = 0;
			$report3_cntr   = 0;

			$loop_cntr      = 1;
			$data_cntr      = @count($data);
			$cnt_label = 0;


			foreach ($data as $d) {
				$credit_exp = explode('-', $d->score_sf);

				if ($series1_temp != $d->industry_main) {
					if (0 <= $series1_index) {
						$series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
					}

					$series1_index++;
					$series1_temp                       = $d->industry_main;

					$series1[$series1_index]            = new stdClass();
					$series1[$series1_index]->name      = $d->industry_main;
					$series1[$series1_index]->y         = $d->value;
					$series1[$series1_index]->color     = ColorHelper::colourBrightness($color[$series1_index], -0.9);
					$series1[$series1_index]->average   = (int)$credit_exp[0];
					$series1[$series1_index]->i			= 1;
					$series1[$series1_index]->id		= $d->industry_main_id;

					$report_cntr                        = 1;
					if ($data_cntr == $loop_cntr) {
						$series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
					}
				} else {
					$report_cntr++;
					$series1[$series1_index]->y         += $d->value;
					$series1[$series1_index]->average   += (int)$credit_exp[0];
					$series1[$series1_index]->i			= 1;
					$series1[$series1_index]->id		= $d->industry_main_id;
					if ($data_cntr == $loop_cntr) {
						$series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
					}
				}

				if ($series2_temp != $d->industry_sub) {
					if (0 <= $series2_index) {
						$series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
					}

					$series2_index++;
					$series2_temp                       = $d->industry_sub;
					$series2[$series2_index]            = new stdClass();
					$series2[$series2_index]->name      = $d->industry_sub;
					$series2[$series2_index]->y         = $d->value;
					$series2[$series2_index]->color     = ColorHelper::colourBrightness($color[$series1_index], 1);
					$series2[$series2_index]->average   = (int)$credit_exp[0];
					$series2[$series2_index]->i			= 2;
					$series2[$series2_index]->id		= $d->industry_sub_id;
					$report2_cntr                        = 1;

					if ($data_cntr == $loop_cntr) {
						$series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
					}
				} else {
					$report2_cntr++;
					$series2[$series2_index]->y         += $d->value;
					$series2[$series2_index]->average   += (int)$credit_exp[0];

					$series2[$series2_index]->i			= 2;
					$series2[$series2_index]->id		= $d->industry_sub_id;
					if ($data_cntr == $loop_cntr) {
						$series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
					}
				}

				if ($series3_temp != $d->industry_row) {
					if (0 <= $series3_index) {
						$series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
					}

					$series3_index++;
					$series3_temp                       = $d->industry_row;
					$series3[$series3_index]            = new stdClass();
					$series3[$series3_index]->name      = $d->industry_row;
					$series3[$series3_index]->y         = $d->value;
					$series3[$series3_index]->color     = ColorHelper::colourBrightness($color[$series1_index], .9);
					$series3[$series3_index]->average   = (int)$credit_exp[0];

					$series3[$series3_index]->i			= 3;
					$series3[$series3_index]->id		= $d->industry_row_id;
					$series3[$series3_index]->count     = $cnt_label;
					$cnt_label += 1;
					$report3_cntr                        = 1;

					if ($data_cntr == $loop_cntr) {
						$series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
					}
				} else {
					$report3_cntr++;
					$series3[$series3_index]->y         += $d->value;
					$series3[$series3_index]->average   += (int)$credit_exp[0];
					$series3[$series3_index]->i			= 3;
					$series3[$series3_index]->id		= $d->industry_row_id;
					$series3[$series3_index]->count     = $cnt_label;
					$cnt_label += 1;
					if ($data_cntr == $loop_cntr) {
						$series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
					}
				}

				$total += $d->value;
				$loop_cntr++;
			}

			// calculate value /100
			foreach($series1 as $key=>$value){
				$series1[$key]->y = round($value->y / $total * 100, 2);
			}
			foreach($series2 as $key=>$value){
				$series2[$key]->y = round($value->y / $total * 100, 2);
			}
			foreach($series3 as $key=>$value){
				$series3[$key]->y = round($value->y / $total * 100, 2);
			}

			// Ready for MultiSelect Industry
			$return = array();
			$data = array();
			if(gettype($industry) == "array"){
				$return = array("type"=>1);
				if(in_array(1,$industry)){
					$data['series1'] = $series1;
				}
				if(in_array(2,$industry)){
					$data['series2'] = $series2;
				}
				if(in_array(3,$industry)){
					$data['series3'] = $series3;
				}
				if(in_array("All",$industry)){
					$data = array(
						"series1" => $series1,
						"series2" => $series2,
						"series3" => $series3
					);
				}
				$return['data'] = $data;
			} else {
				switch ($industry) {
					case '1':
						$return = array(
							'type' => 1,
							'data' => array(
								'series' => $series1,
							)
						);
						break;
					case '2':
						$return = array(
							'type' => 1,
							'data' => array(
								'series' => $series2,
							)
						);
						break;
					case '3':
						$return = array(
							'type' => 1,
							'data' => array(
								'series' => $series3,
							)
						);
						break;
					default:
						$return = array(
							'type' => 1,
							'data' => array(
								'series1' => $series1,
								'series2' => $series2,
								'series3' => $series3
							)
						);
						break;
				}
			}
			return $return;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.65: getBankBillingDetails
	//  Description: Shows the Billing Details Page
    //-----------------------------------------------------
	public function getBankBillingDetails(){
		try{
			$user = User::find(Auth::user()->loginid);

			$subscription = 0;

			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
	            $subscription = $this->_returnSubscriptions();
	        }

			return View::make('dashboard.billing-details')->with(['user' => $user,'subscription' => $subscription]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.65: getAdminSupBankBillingDetails
	//  Description: Shows the Billing Details Page
    //-----------------------------------------------------
	public function getAdminSupBankBillingDetails(){
		try{
			$supervisor = session('supervisor');
			$user = User::find($supervisor['loginid']);

			$subscription = 0;

			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
	            $subscription = $this->_returnSubscriptions();
	        }

			return View::make('dashboard.admin-sup-billing-details')->with(['user' => $user,'subscription' => $subscription]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	//-----------------------------------------------------
    //  Function 3.66: postBankBillingDetails
	//  Description: Post Process for Billing Details Page
    //-----------------------------------------------------
	public function postBankBillingDetails()
	{
		try{
			$messages = array(  							// validation message
				'street'  => 'Please add your street details',
				'city' => 'Please add your city details',
				'province' => 'Please add your province details',
				'zipcode' => 'Please add your zipcode details',
				'phone' => 'Please add your phone number details',
				'name' => 'Please add a biller name/company name'
	        );

			$rules = array(									// validation rules
				'street'	=> 'required',
				'city'	=> 'required',
				'province'	=> 'required',
				'zipcode'	=> 'required',
				'phone'	=> 'required',
				'name' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bankbillingdetails')->withErrors($validator->errors())->withInput();
			}
			else
			{
				$user = User::find(Auth::user()->loginid);

				$user->name = Input::get('name');
				$user->street_address = Input::get('street');
				$user->city = Input::get('city');
				$user->province = Input::get('province');
				$user->zipcode = Input::get('zipcode');
				$user->phone = Input::get('phone');
				$user->save();

				return Redirect::to('/dashboard/index');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.66: postAdminSupBankBillingDetails
	//  Description: Post Process for Billing Details Page
    //-----------------------------------------------------
	public function postAdminSupBankBillingDetails()
	{
		try{
			$messages = array(  							// validation message
				'street'  => 'Please add your street details',
				'city' => 'Please add your city details',
				'province' => 'Please add your province details',
				'zipcode' => 'Please add your zipcode details',
				'phone' => 'Please add your phone number details',
				'name' => 'Please add a biller name/company name'
	        );

			$rules = array(									// validation rules
				'street'	=> 'required',
				'city'	=> 'required',
				'province'	=> 'required',
				'zipcode'	=> 'required',
				'phone'	=> 'required',
				'name' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bankbillingdetails')->withErrors($validator->errors())->withInput();
			}
			else
			{
				$supervisor = session('supervisor');
				$user = User::find($supervisor['loginid']);

				$user->name = Input::get('name');
				$user->street_address = Input::get('street');
				$user->city = Input::get('city');
				$user->province = Input::get('province');
				$user->zipcode = Input::get('zipcode');
				$user->phone = Input::get('phone');
				$user->save();

				return Redirect::to('/bank/dashboard');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.67: getAdminSupervisorSubscription
	//  Description: Change subscription status and button based on current status
    //-----------------------------------------------------
	public function getAdminSupervisorSubscription()
	{
		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			
			$subscription = 0;
			if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
	            $subscription = $this->_returnSubscriptions();
	        }

			$user = (object) session('supervisor');

			$bankSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",$user->loginid)->get();
			foreach($bankSubscription as $sub){
				if($sub->expiry_date){
					$expDate = date('Y-m-d', strtotime($sub->expiry_date));
					$expStart = date('Y-m-d', strtotime($sub->created_at));
					if(strtotime($sub->expiry_date) > strtotime(date('c')) ){
						$sub->subscription_status = "Active";
						$sub->subscription_btn = "Unsubscribe";
					} else {
						$sub->subscription_status = "Inactive";
						$sub->subscription_btn = "Subscribe";
					}
				}else{
					$sub->subscription_btn = "Subscribe";
					$sub->subscription_status = "Inactive";
				}

				if($sub->status === 0) {
					$sub->subscription_btn = "Subscribe";
					$sub->subscription_status = "Inactive";
				} 
			}

			return View::make('bank.supervisor_subscribe', array(
				'user' => $user,
				'subscription' => $bankSubscription,
				'currentSubscription' => $subscription
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.67: getSupervisorSubscription
	//  Description: Change subscription status and button based on current status
    //-----------------------------------------------------
	public function getSupervisorSubscription()
	{
		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			
			$subscription = 0;
			if ($this->accountDetails['product_plan'] != 3 && $organization->is_custom == 0) {
	            $subscription = $this->_returnSubscriptions();
	        }

			$user = Auth::user();
			$bankSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",$user->loginid)->get();
			foreach($bankSubscription as $sub){
				if($sub->expiry_date){
					$expDate = date('Y-m-d', strtotime($sub->expiry_date));
					$expStart = date('Y-m-d', strtotime($sub->created_at));
					if(strtotime($sub->expiry_date) > strtotime(date('c')) ){
						$sub->subscription_status = "Active";
						$sub->subscription_btn = "Unsubscribe";
					} else {
						$sub->subscription_status = "Inactive";
						$sub->subscription_btn = "Subscribe";
					}
				}else{
					$sub->subscription_btn = "Subscribe";
					$sub->subscription_status = "Inactive";
				}

				if($sub->status === 0) {
					$sub->subscription_btn = "Subscribe";
					$sub->subscription_status = "Inactive";
				} 
			}

			return View::make('bank.supervisor_subscribe', array(
				'user' => $user,
				'subscription' => $bankSubscription,
				'currentSubscription' => $subscription
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.68: postSupervisorSubscripton
	//  Description: Post process to update the subscription status
    //-----------------------------------------------------
	public function postSupervisorSubscription() 
	{
		try{
			$id = Input::get("id");
			$bankSubscription = DB::table("tblbanksubscriptions")->select("*")->where("bank_id",$id)->get();
			if($id){
				$now = date("Y-m-d H:i:s"); //created_at
				foreach ($bankSubscription as $sub) {
					if($sub->status == 1 || $sub->status == null){
						$update = array(
							"updated_at" => $now,
							"status" => 0,
							"updated_by" => Auth::user()->loginid
						);
						DB::table('tblbanksubscriptions')->where('bank_id', $id)->update($update);
					}
				}
			}
			Redirect::to("/bank/subscription");
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.69: postSupervisorSubscripton
	//  Description: Post process to send Enail in Report Listing of Supervisor
    //-----------------------------------------------------
	public function sendEmailReportlist()
	{
		try{
			$report = Input::get('entity');
		
			foreach($report as $record){
				$entityId = $record['id'];
				$entity = Entity::where('entityid', '=', $entityId)->first();
				// $document = Document::where('entity_id', $entityId)
				// 					->where('document_type', Document::TYPE_FINANCIAL_REPORT)
				// 					->orderBy('created_at', 'desc')
				// 					->first();
				// $period = date('Y-m-d', strtotime($document->created_at));
				// $fname = $entity->companyname;
				// if(strpos($fname,"&") !== false){
				// 	$fname  = preg_replace("/[&]/", "And", $fname);
				// }
				// $pdf = sprintf(
				// 	FinancialAnalysisPDF::FILENAME,
				// 	trim(rtrim($fname, '.')),
				// 	$period
				// );

				$pdfPath = public_path('financial_analysis').'/'.$entity->financial_analysis_pdf;
				if (!file_exists($pdfPath)) {
					$insert = array(
						"entity_id" => $entityId,
						"data" => "Financial Analysis report does not exist",
						"error" => "Financial Analysis report does not exist",
						"status" => 0,
						"is_deleted" => 0,
						"created_at" => date('Y-m-d H:i:s'),
						"updated_at" => date('Y-m-d H:i:s')
					);
				}

				$cbpoPdf = public_path('documents').'/'.$entity->cbpo_pdf;
				if (!file_exists($cbpoPdf)) {
					$insert = array(
						"entity_id" => $entityId,
						"data" => "Rating report does not exist",
						"error" => "Rating report does not exist",
						"status" => 0,
						"is_deleted" => 0,
						"created_at" => date('Y-m-d H:i:s'),
						"updated_at" => date('Y-m-d H:i:s')
					);
				}

				$emailCc = array();
				$userBank = null;
				
				if ($entity->current_bank != 0) {
					$userBank  = User::where('tbllogin.bank_id', $entity->current_bank)
						->leftJoin('tblbanksubscriptions as bs', "tbllogin.loginid", 'bs.bank_id')
						->where('role', 4)
						->where('parent_user_id', 0)
						->whereRaw(" bs.expiry_date > NOW()")
						->where('tbllogin.is_delete', 0)
						->get()->toArray();
					if(empty($userBank)) {
						$userBank  = User::where('bank_id', $entity->current_bank)
							->where('role', 4)
							->where('parent_user_id', 0)
							->where('tbllogin.is_delete', 0)
							->get()->toArray();
					}
					foreach ($userBank as $key => $user) {
						$emailCc[]= $user['email'];
					}
				}

				$user = User::where('loginid', '=', $entity->loginid)->first();
				$emailTo = $user->email;

				/** Basic User */
				if(Input::get('basicUser') == 1){

					try{
						Mail::send(
							'emails.finish_scoring_user',
							[
								'type'=>'7'
							],
							function($message) use (
								$entity, $userBank, $pdfPath, $cbpoPdf, $emailTo, $emailCc
							) {
								if ($userBank) {
									$message->to($emailTo, $emailTo)
										->subject($entity->companyname.' Rating Report Is Now Available!');
									if(file_exists($cbpoPdf)){
										$message->attach($cbpoPdf);
									}
									if(file_exists($pdfPath)){
										$message->attach($pdfPath);
									}
									if (!empty($emailCc)) {
										$message->cc($emailCc);
									}
									
									$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
									$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
									return;
								}
			
								$message->to($emailTo, $emailTo)
									->subject('Your CreditBPO Rating Report Is Now Available!')
									->attach($cbpoPdf)
									->attach($pdfPath);
								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
									$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
						});

					}catch(Exception $ex){
						if(env("APP_ENV") == "Production"){
							Log::error($ex->getMessage());
							return Redirect::to('dashboard/error');
						}else{
							throw $ex;
						}
		            }
				}

				/** Supervisor */
				if(Input::get('supervisor') == 1){

					try{
						Mail::send('emails.finishscoring',['type'=>'7', 'entity' => $entity],
							function($message) use ($entity, $userBank, $emailTo, $emailCc) {
								if($emailCc) {
									$message->to($emailCc)
										->subject('Your CreditBPO Rating Report Is Now Available!');
									$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
									$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
								}	
						});
					}catch(Exception $ex){
						if(env("APP_ENV") == "Production"){
							Log::error($ex->getMessage());
							return Redirect::to('dashboard/error');
						}else{
							throw $ex;
						}
		            }
				}

				/** Others */
				if(Input::get('others') == 1){
					$otherEmail = Input::get('otherEmail');

					try{
						Mail::send('emails.finishscoring',['type'=>'7'],
							function($message) use ($pdfPath, $cbpoPdf, $otherEmail) {
								if($otherEmail) {
									$message->to($otherEmail, $otherEmail)
										->subject('Your CreditBPO Rating Report Is Now Available!');
									$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
									$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
								}
						});
					}catch(Exception $ex){
						if(env("APP_ENV") == "Production"){
							Log::error($ex->getMessage());
							return Redirect::to('dashboard/error');
						}else{
							throw $ex;
						}
		            }
				}
			}

			return array('success' => true);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.70: getContractors
	//  Description: get contractors for PCAB
    //-----------------------------------------------------
	public function getContractors()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			if($this->accountDetails['parent_user_id'] == 0 && Auth::user()->getAssociatedBank(Auth::user()->bank_id)->bank_name == "Contractors Platform" ){
				$users = User::where('parent_user_id', $this->accountDetails['loginid'])->where('role', 5)->get();
				foreach($users as $k=>$u){
					$responsibilities = explode(',', $u->responsibilities);
					// check sub user permision and convert as readable
					$responsibilities_f = array();
					foreach($responsibilities as $p){
						switch($p){
							case 1: $responsibilities_f[] = 'Document A'; break;
							case 2: $responsibilities_f[] = 'Document B'; break;
							case 3: $responsibilities_f[] = 'Document C'; break;
							case 4: $responsibilities_f[] = 'Document D'; break;
							case 5: $responsibilities_f[] = 'Document E'; break;
							default: $responsibilities_f[] = "none"; break;
						}
					}
					$users[$k]->responsibilities_formatted = implode(', ', $responsibilities_f);
				}
				return View::make('bank.contractors')->with('users', $users);
			} else {
				return Redirect::to('/');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.71: getAddContractor
	//  Description: get add contractor page
    //-----------------------------------------------------
	public function getAddContractors()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			if($this->accountDetails['parent_user_id'] == 0 && Auth::user()->getAssociatedBank(Auth::user()->bank_id)->bank_name == "Contractors Platform" ){
				return View::make('bank.add_contractors');
			} else {
				return Redirect::to('/');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.72: postAddContractors
	//  Description: Add Contractors for PCAB
    //-----------------------------------------------------
	public function postAddContractors()
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
	        $perm_count = 0;

	        $messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to register is already taken.'
	        );

			$rules = array(									// validation rules
		        'email'	=> 'required|email|unique:tbllogin',
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
				'password'	=> 'required|min:6|confirmed',
				'password_confirmation'	=> 'required|min:6'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/bank/ad_contractors')->withErrors($validator->errors())->withInput();
			}
			else
			{
	            $responsibilities    = Input::get('responsibilities');			// get input permisions
	            $perm_count     = @count($responsibilities);

				// create the contractors for PCAB
				$user = new User();
	      		$user->email = Input::get('email');
	      		$user->password = Hash::make(Input::get('password'));
	      		$user->job_title = '';
	      		$user->firstname = Input::get('firstname');
	      		$user->lastname = Input::get('lastname');
	      		$user->role = 5;
				$user->bank_id = $this->accountDetails['bank_id'];
				$user->can_view_free_sme = $this->accountDetails['can_view_free_sme'];
	      		$user->status = 2;
				$user->parent_user_id = $this->accountDetails['loginid'];
	            $user->responsibilities = ($responsibilities) ? implode(',', $responsibilities): 1;
	      		$user->save();

				return Redirect::to('/bank/contractors');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.73: getEditContractor
	//  Description: get Edit Contractor Page
    //-----------------------------------------------------
	public function getEditContractor($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);

	        $result = $this->_checkAllSubscriptionConditions();
	        if($result != "")
	                return $result;

			if($this->accountDetails['parent_user_id'] == 0 && Auth::user()->getAssociatedBank(Auth::user()->bank_id)->bank_name == "Contractors Platform" ){
				$entity = DB::table('tbllogin')->where('loginid', $id)->where('role', 5)->where('parent_user_id', $this->accountDetails['loginid'])->first();
				$entity->responsibilities = explode(',', $entity->responsibilities);
				return View::make('bank.edit_contractor')->with('entity', $entity);
			} else {
				return Redirect::to('/');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.74: postEditContractor
	//  Description: post process contractor edit page
    //-----------------------------------------------------
	public function postEditContractor($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
	        $perm_count = 0;
			$rules = array(					// validation rules
		        'firstname'	=> 'required',
		        'lastname'	=> 'required',
	            'password'	=> 'nullable|min:6|confirmed',
				'password_confirmation'	=> 'nullable|required_with:password|min:6'
			);

	        $validator = Validator::make(Input::all(), $rules);

	        if($validator->fails())
	        {
	            return Redirect::to('/bank/edit_contractor/'.$id)->withErrors($validator->errors())->withInput();
	        }
	        else
	        {
				//if success
	            $responsibilities    = Input::get('responsibilities'); 		// get input permissions
	            $perm_count     = @count($responsibilities);
	            $user = User::where('loginid', $id)->where('role', 5)->where('parent_user_id', $this->accountDetails['loginid']	)->first();
	            if(Input::get('password')!=''){		// check password confirm if match
	                if(Input::get('password')!=Input::get('password_confirmation')){
	                    return Redirect::to('/bank/edit_contractor/'.$id)->withErrors('password', 'Password confirmtion did not match.')->withInput();
	                } elseif(strlen(Input::get('password')) < 6){
	                    return Redirect::to('/bank/edit_contractor/'.$id)->withErrors('password', 'Password must be at least 6 characters.')->withInput();
	                } else {
	                    $user->password = Hash::make(Input::get('password'));
	                }
	            }
	            $user->firstname = Input::get('firstname');
				$user->lastname = Input::get('lastname');
				$user->responsibilities = ($responsibilities) ?  implode(',', $responsibilities): 1;
				
	            $user->save();		/// save

	            return Redirect::to('/bank/contractors');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.75: getDeleteContractor
	//  Description: function to delete contractor
    //-----------------------------------------------------
	public function getDeleteContractor($id)
	{
		try{
			if($this->accountDetails['product_plan']==3) App::abort(404);
			$user = User::where('loginid', $id)->where('role', 5)->where('parent_user_id', $this->accountDetails['loginid'])->first();
			if($user){
				$user->delete();		// find user and delete
			}
			return Redirect::to('/bank/contractors');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.76: checkEmail
	//  Description: Check email for client side validation
    //-----------------------------------------------------
	public function checkEmail()
	{
		try{
			$input = Input::all();
			$user = User::where('email', $input['email'])->count();
			if ($user > 0) {
				return "false";
			} else {
				return "true";
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 3.77: checkNotificationEmail
	//  Description: Check Notification Email for Client Side Validation
	//-----------------------------------------------------
	public function checkNotificationEmail()
	{
		try{
			$input = Input::all();
			$user = BankEmailNotifications::where('email', $input['email'])->where('status', 1)
				->count();
			if ($user > 0) {
				return "false";
			} else {
				return "true";
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 3.76: postDistributeKeys
	//  Description: Function to add keys to email
    //-----------------------------------------------------
	public function postDistributeKeys()
	{
		try{
			if(Auth::user()->product_plan==3) App::abort(404);

			//get unused bank keys
			$unused_bank_keys = BankSerialKey::getUnusedBankKeys($this->accountDetails['bank_id']);

			$unused_bank_keys_cnt = count($unused_bank_keys) + 1;

	       	// validation rules
	       	$rules = [									
		        'email'	=> 'required|email',
		        'no_of_keys'	=> 'required|numeric|min:1|lt:'.$unused_bank_keys_cnt
			];

			// validation messages
			$messages = [
				'email.required' => 'Please provide an email',
				'no_of_keys.required' => 'Please provide number of keys',
				'no_of_keys.numeric' => 'Number of keys should be numeric',
				'no_of_keys.min' => 'Number of keys should be greater than 1',
				'no_of_keys.lt' => 'Not enough number of available keys',
			];

			$validator = Validator::make(Input::all(), $rules, $messages);

			//if validator failed redirect back to distributed keys page
			if($validator->fails()) {
				Session::flash('another', 1);
				return Redirect::to('/bank/keys')->withErrors($validator->errors())->withInput();
			}

			$another = Input::get('another');
			$email = Input::get('email');
			$no_of_keys = Input::get('no_of_keys');

			$entity_ids = [];
			$serial_keys = [];

			//assign unused bank keys and entity ids on containers
			$count = 0;
			foreach ($unused_bank_keys as $key) {
				
				if($count < $no_of_keys){

					$serial_keys[] = [
						'serial_key' => $key['serial_key'],
					];

					$entity_ids[] = [
						'serial_key' => $key['serial_key'],
						'entity_id' => $key['entity_id'],
					];

				}

				$count++;
			}

			// set filename
			$filename = base_path('temp/keys_'.time().'.csv');				
			$out = fopen($filename, 'w+');

			// iterate keys and store in file
			foreach($serial_keys as $line)					
			{
				fputcsv($out, $line);
			}
			
			fclose($out);

			//if csv is generated, send email notification to client
			if(file_exists($filename)){
				
				//Send email to client
	            Mail::send(
		    		'emails.distributed_keys',
		    		['no_of_keys' => $no_of_keys],
		    		function ($message) use ($email, $filename,$no_of_keys){
		        		$message->to($email, $email)
		        		->subject('CreditBPO: Client Keys has been provided for '.$email)
		        		->attach($filename);
		        	});

	            //if mail is not a failure update entity
	            if(!Mail::failures()){

		        	foreach ($entity_ids as $key) {
		        		
		        		if(!empty($key['entity_id'])){
		        			$key_data = [
		            			'email' => $email
		            		];
		            		
		        			Entity::where('entityid',$key['entity_id'])->update($key_data);
		        		}else{


					        //get entity id
					    //    $entity_id = Entity::where('email',$email)->first();
					         
					        //update bank key
					//		if($entity_id){
		        			//BankSerialKey::where('serial_key',$key['serial_key'])->update(['entity_id' => $entity_id->entityid]);
		        	//	}
						}

		    		}

	    		}

	    	}

	    	Session::flash('another', $another);
	    	
			return Redirect::to('/bank/keys');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getLetterOfAuthorization($id){
		try{
			$loa = Document::where('entity_id', $id)
			->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)
			->where('is_deleted', 0)
			->first();

			return response()->download(public_path('documents/letter_authorization/'.$loa->document_rename), $loa->document_orig);
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function X.XX: downloadFinancialAnalysisPDF
	//  Description: Download Financial Analysis PDF
    //-----------------------------------------------------
	public function downloadFinancialAnalysisPDF($id){
		try{
			$entity = Entity::where('entityid', $id)->first();
			if(Auth::user()->bank_id != $entity->current_bank){
				App::abort(403, 'Unauthorized access.');
			}
			return response()->download(public_path('financial_analysis/'.$entity->financial_analysis_pdf), $entity->financial_analysis_pdf);
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function X.XX: downloadFinancialAnalysisPDF
	//  Description: Download Financial Analysis PDF
    //-----------------------------------------------------
	public function downloadRatingSummaryPDF($id){
		try{
			$entity = Entity::where('entityid', $id)->first();
			if(Auth::user()->bank_id != $entity->current_bank){
				App::abort(403, 'Unauthorized access.');
			}
			return response()->download(public_path('documents/'.$entity->cbpo_pdf), $entity->cbpo_pdf);
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
}
