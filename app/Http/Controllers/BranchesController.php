<?php
//======================================================================
//  Class 5: BranchesController
//  Description: Contains functions regarding Branches panel in SME questionnaire
//======================================================================
class BranchesController extends BaseController {

	//-----------------------------------------------------
    //  Function 5.1: getBranches
	//  Description: get Branches page 
    //-----------------------------------------------------
	public function getBranches($entity_id)
	{
		return View::make('sme.branches',
            array(
                'entity_id' => $entity_id
            )
        );
	}
	
	//-----------------------------------------------------
    //  Function 5.2: postBranches
	//  Description: function for updating Branches
    //-----------------------------------------------------
	public function postBranches($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $branches  = Input::get('branch_address');
        $messages   = array();
        $rules      = array();
        
        $security_lib   = new MaliciousAttemptLib;		// create instance of MaliciousAttemptLib
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#branches-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-branches';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		// iterate for validation rules and messages
        for ($idx = 0; $idx < @count($branches); $idx++) {
            
            if ((STR_EMPTY != $branches[$idx])
            || (0 == $idx)) {
                
                $messages['branch_address.'.$idx.'.required']           = trans('validation.required', array('attribute'=>'Branch Address'));
                $messages['branch_owned_leased.'.$idx.'.required']      = trans('validation.required', array('attribute'=>trans('business_details1.branches_owned_leased')));
                $messages['branch_contact_person.'.$idx.'.required']    = trans('validation.required', array('attribute'=>trans('business_details1.b_contact_person')));
                $messages['branch_phone.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Contact Phone Number'));
                $messages['branch_phone.'.$idx.'.digits_between'] = 'The Phone Number format should be Country Code, Area Code, and phone number. e.g +63021234567';
                $messages['branch_email.'.$idx.'.required']             = trans('validation.required', array('attribute'=>'Contact Email'));
                $messages['branch_email.'.$idx.'.email']                = trans('validation.email', array('attribute'=>'Contact Email'));
                $messages['branch_job_title.'.$idx.'.required']         = trans('validation.required', array('attribute'=>trans('business_details1.b_job_title')));

                $rules['branch_address.'.$idx]          = 'required';
                $rules['branch_owned_leased.'.$idx]     = 'required';
                $rules['branch_contact_person.'.$idx]   = 'required';
                $rules['branch_phone.'.$idx]            = 'required|digits_between:7,12';
                $rules['branch_email.'.$idx]            = 'required|email';
                $rules['branch_job_title.'.$idx]        = 'required';
            }
        }
        
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails()) {		// if validator fails, get error messages
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else
		{	
			$arraydata = Input::only('branch_address', 'branch_owned_leased', 'branch_contact_person', 'branch_phone', 'branch_email', 'branch_job_title');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			$branch_address = $arraydata['branch_address'];
            $branch_owned_leased = $arraydata['branch_owned_leased'];
			$branch_contact_person = $arraydata['branch_contact_person'];
			$branch_phone = $arraydata['branch_phone'];
			$branch_email = $arraydata['branch_email'];
			$branch_job_title = $arraydata['branch_job_title'];

			foreach ($branch_address as $key => $value) {
				if ($value) {
					$data = array(
					    'entity_id'             =>$entity_id, 
					   	'branch_address'        =>$branch_address[$key],
                        'branch_owned_leased'   =>$branch_owned_leased[$key],
					   	'branch_contact_person' =>$branch_contact_person[$key],
					   	'branch_phone'          =>$branch_phone[$key],
					   	'branch_email'          =>$branch_email[$key],
					   	'branch_job_title'      =>$branch_job_title[$key],
					   	'created_at'            =>date('Y-m-d H:i:s'),
				    	'updated_at'            =>date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblbranches')
                            ->where('branchid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblbranches')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            $srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();				// log activity to db
        return json_encode($srv_resp);		// return json encoded result
	}

	//-----------------------------------------------------
    //  Function 5.3: getBranchesDelete
	//  Description: function for deleting Branches
    //-----------------------------------------------------
    public function getBranchesDelete($id)
	{
		// DB::table('tblbranches')
        // ->where('branchid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
        // ->delete();
        
        $branch = Branches::where('branchid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();
        $branch->is_deleted = 1;
        $branch->save();

        ActivityLog::saveLog();
		Session::flash('redirect_tab', 'tab1');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
