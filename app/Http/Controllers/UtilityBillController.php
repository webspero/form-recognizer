<?php

use Google\Cloud\Core\ServiceBuilder;

class UtilityBillController extends BaseController
{
    public function getUbillUpload($entity_id)
	{
		return View::make('sme.upload-ubillFiles', [
			'entity_id'     => $entity_id,
		]);
    }

    public function postUploadUbill($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $arraydata          = Input::file('ubill_file');			// get input file

        $messages   = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        if (isset(Auth::user()->loginid)) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#ubill-upload-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-ubill-upload';
        }

        /*--------------------------------------------------------------------
        /*	Checks if the User is allowed to update the company
        /*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

        foreach($arraydata as $key => $value) {
            if (($value)
            || (0 == $key)) {
                // ---------------------------------
                // set validation rules and messages
                // ---------------------------------
                $rules['ubill_file.'.$key] = 'required';
                $messages['ubill_file.'.$key.'.required'] = "File is required.";
            }
        }

        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

        if ($validator->fails()) {	// return error message if fail
            $srv_resp['messages']	= $validator->messages()->all();
        }
        else {
            foreach ($arraydata as $key=>$value) {
                if($value){
                    $imageName = $value->getClientOriginalName();
                    $destinationPath = public_path('documents/'.$entity_id.'-utility_bill');
        
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath);
                        chmod($destinationPath, 0775);
                    }

                    $value->move($destinationPath, $imageName);
                    $image = $destinationPath.'/'.$imageName;

                    $imagick = new Imagick($image);
                    $imagick->sharpenimage(20,10);
                    $imagick->enhanceImage();
                    $imagick->getImageBlob();

                    file_put_contents($image, $imagick);

                    $cloud = new ServiceBuilder([
                        'keyFilePath' => public_path('visionAPI.json'),
                        'projectId' => 'sixth-edition-253508'
                    ]);

                    $vision = $cloud->vision();
                    $image = $vision->image(file_get_contents($image), ['text']);
                    $textAnnotations = $vision->annotate($image)->text();

                    // Remove first row (full text)
                    array_shift($textAnnotations);

                    /* --------------------------
                    ---get due date Column start--
                    ---------------------------*/
                    $isDueDate = STS_NG;
                    $texts = [];

                    $BillType = '';

                    foreach($textAnnotations as $key => $text)
                    {
                        if($BillType == '') {
                            if($text->info()['description'] === "MERALCO") {
                                $BillType = "meralco";
                            } else if($text->info()['description'] === "MANILA" and $textAnnotations[$key+1]->description() === "WATER") {
                                $BillType = "manila_water";
                            } else if ($text->info()['description'] === "PLDT") {
                                $BillType = "pldt";
                            } else if ($text->info()['description'] === "Maynilad") {
                                $BillType = "maynilad";
                            } else if ($text->info()['description'] === "Globe") {
                                $BillType = "globe";
                            } else if ($text->info()['description'] === "SMART") {
                                $BillType = "smart";
                            } else if ($text->info()['description'] === "SUn" and $textAnnotations[$key+1]->description() === "CELLULAR") {
                                $BillType = "sun";
                            }
                        } else {
                            break;
                        }
                    }

                    if ($BillType == "meralco") {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "Due") {
                                if ($textAnnotations[$key+1]->description() === "Date") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 25;
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 55;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 50;
                                $isDueDate = STS_NG;
                            } else {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "Due" or $txt['text'] == "Oue") and $texts[$key+1]['text'] == "Date")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ( $txt['y1'] > $texts[$textKey]['y1'] && $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            } else if ($isRotated) {
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['y2'] > $texts[$textKey]['y2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "Due" or $txt['text'] == "Date" or $txt["text"] == "Due Date") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            $dueDate = $txt['text'];
                        }
                    }
                    else if ($BillType == "manila_water")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "Due") {
                                if ($textAnnotations[$key+1]->description() === "Date") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 450;
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 20;
                                $isDueDate = STS_NG;
                            } else {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "Due" or $txt['text'] == "Oue") and $texts[$key+1]['text'] == "Date")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "Due" or $txt['text'] == "Date" or $txt["text"] == "Due Date") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if($texts[$key+1]['text']){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                    }
                    else if ($BillType == "pldt")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "DUE") {
                                if ($textAnnotations[$key+1]->description() === "DATE:") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] -20;
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 100;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 70;
                                $isDueDate = STS_NG;
                            } else {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "DUE" or $txt['text'] == "OUE") and $texts[$key+1]['text'] == "DATE:")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "DUE" or $txt['text'] == "DATE:" or $txt["text"] == "DUE DATE:") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if($texts[$key+1]['text']){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                    }
                    else if ($BillType == "maynilad")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "DUE") {
                                if ($textAnnotations[$key+1]->description() === "DATE") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 300;
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 10;
                                $isDueDate = STS_NG;
                            } else {
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "DUE" or $txt['text'] == "OUE") and $texts[$key+1]['text'] == "DATE")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "DUE" or $txt['text'] == "DATE" or $txt["text"] == "DUE DATE") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if(count($texts) > 1){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                    }
                    else if ($BillType == "globe")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "Due") {
                                if ($textAnnotations[$key+1]->description() === "Date") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 80;
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                                $isDueDate = STS_NG;
                            } else {
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "Due" or $txt['text'] == "Oue") and $texts[$key+1]['text'] == "Date")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "Due" or $txt['text'] == "Date" or $txt["text"] == "Due Date") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if(count($texts) > 1){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                    } 
                    else if ($BillType == "smart")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "DUE") {
                                if ($textAnnotations[$key+1]->description() === "DATE:") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 140;
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 100;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 95;
                                $isDueDate = STS_NG;
                            } else {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "DUE" or $txt['text'] == "OUE") and $texts[$key+1]['text'] == "DATE:")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "DUE" or $txt['text'] == "DATE:" or $txt["text"] == "DUE DATE:") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if($texts[$key+1]['text']){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                        
                    }
                    else if ($BillType == "sun")
                    {
                        foreach($textAnnotations as $key => $text)
                        {
                            if ($text->info()['description'] === "Due") {
                                if ($textAnnotations[$key+1]->description() === "Date") {
                                    $isDueDate = STS_OK;
                                }
                            }

                            if($isDueDate == STS_OK) {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 30;
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 20;
                                $isDueDate = STS_NG;
                            } else {
                                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                                $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                            }
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                            $value = ["x1" => $x1, 
                                        "x2" => $x2, 
                                        "y1" => $y1,
                                        "y2" => $y2,
                                        "text" => $text->description()];
                            $texts[] = $value;
                        }

                        $isDueDate = STS_NG;
                        $textKey = 0;
                        foreach($texts as $key => $txt){
                            $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                            if($isDueDate == STS_NG){
                                if ((($txt['text'] == "Due" or $txt['text'] == "Oue") and $texts[$key+1]['text'] == "Date")){
                                    $isDueDate = STS_OK;
                                    $textKey = $key;
                                } else {
                                    unset($texts[$key]);
                                    continue;
                                }
                            }
                            if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                                unset($texts[$key]);
                                continue;
                            } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                                unset($texts[$key]);
                                continue;
                            }
                        }

                        foreach($texts as $key => $txt){
                            if($txt['text'] == "Due" or $txt['text'] == "Date" or $txt["text"] == "Due Date") {
                                unset($texts[$key]);
                            }
                        }
                        
                        foreach($texts as $key => $txt) {
                            if($texts[$key+1]['text']){
                                $dueDate = "{$txt['text']} {$texts[$key+1]['text']} {$texts[$key+2]['text']}";
                            } else {
                                $dueDate = $txt['text'];
                            }
                            break;
                        }
                    }

                    $dateNow = date('m/d/Y');

                    if(strtotime($dueDate) < strtotime($dateNow)) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }

                    // create entry on database
                    $data = [
                        'entity_id' => $entity_id,
                        'status'=> $status,
                        'utility_bill_file'=> $imageName,
                        'is_deleted' => 0,
                        'created_at'=> date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s'),
                    ];
                    DB::table('utility_bills')->insertGetId($data);
                    $srv_resp['sts'] = STS_OK;

                }
            }

            if ($srv_resp['sts'] == STS_OK) {
                $srv_resp['messages']   = 'Successfully Added Record';
            }
        }

        ActivityLog::saveLog();				// log activity to database
		return json_encode($srv_resp);		// return json results
    }

    //-----------------------------------------------------
    //  Function 20.8: getDeleteUbill
	//  Description: function for deleting uploaded utility bill
    //-----------------------------------------------------
	public function getDeleteUbill($id)
	{
		// get file details
        $file = DB::table('utility_bills')->where('id', $id)->first();
		if($file){
            // update and save
            DB::table('utility_bills')->where('id', $id)->update(array('is_deleted' => 1));
        }
        
		ActivityLog::saveLog();		// log activity in db
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);	// redirect to summary page
    }

    //-----------------------------------------------------
    //  Function 20.8: getDeleteUbillPremium
	//  Description: function for deleting uploaded utility bill
    //-----------------------------------------------------
	public function getDeleteUbillPremium($id)
	{
		// get file details
        $file = DB::table('utility_bills')->where('id', $id)->first();
		if($file){
            // update and save
            DB::table('utility_bills')->where('id', $id)->update(array('is_deleted' => 1));
        }
        
		ActivityLog::saveLog();		// log activity in db
		return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);	// redirect to summary page
    }

    //-----------------------------------------------------
    //  Function 20.8: getDownloadUbill
    //  Description: function for downloading uploaded utility bill
    //-----------------------------------------------------
    public function getDownloadUbill($entity_id, $filename)
    {
        return response()->download(public_path("documents/{$entity_id}-utility_bill/{$filename}"));
    }
    
}