<?php
//======================================================================
//  Class 7: BusinesssegmentController
//  Description: Contains functions regarding Businesssegment panel in SME questionnaire
//======================================================================
class BusinesssegmentController extends BaseController {

	//-----------------------------------------------------
    //  Function 7.1: getBusinesssegment
	//  Description: get Businesssegment page 
    //-----------------------------------------------------
	public function getBusinesssegment($entity_id)
	{
		return View::make('sme.businesssegment',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 7.2: postBusinesssegment
	//  Description: function for updating Businesssegment 
    //-----------------------------------------------------
	public function postBusinesssegment($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $messages       = array();						// variable container
        $rules          = array();						// variable container
        
        $security_lib   = new MaliciousAttemptLib;		// create instance of MaliciousAttemptLib
        $sts            = STS_NG;						// status false
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
		
		
		$entity = Entity::find($entity_id);

        if ((!Input::has('action')) && (!Input::has('section'))) {
			$srv_resp['action']         = REFRESH_CNTR_ACT;
			$srv_resp['refresh_cntr']   = '#bizsegment-list-cntr';
			$srv_resp['refresh_elems']  = '.reload-bizsegment';

			if($entity->is_premium == PREMIUM_REPORT ) {
				$srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);
			} else {
				$srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
			}
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		// validator messsage
		$messages = array(
			'businesssegment_name.0'	=> trans('validation.required', array('attribute'=>'Name')),
		);

		$rules = array(		// validation rules
	        'businesssegment_name.0'	=> 'required',
		);
	
		$validator = Validator::make(Input::all(), $rules, $messages); // run validation
		
		if($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {	
			$arraydata = Input::only('businesssegment_name');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			$businesssegment_name = $arraydata['businesssegment_name'];

			foreach ($businesssegment_name as $key => $value) {
				$check = DB::table('tblbusinesssegment')->where('entity_id', $entity_id)
					->where('businesssegment_name', $value)
					->count();
				if($check > 0){			// checks if existing
					$srv_resp['messages'][0]    = 'Duplicate entry.';
				} else {
					if($value){
						$data = array(		// populate data
							'entity_id'=>$entity_id, 
							'businesssegment_name'=>$businesssegment_name[$key],
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s')
						);
                        
						// checks for action (API function)
                        if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                            DB::table('tblbusinesssegment')
                                ->where('businesssegmentid', $primary_id[$key])
                                ->update($data);	// update
                            
                            $master_ids[]           = $primary_id[$key];
                            $srv_resp['messages']   = 'Successfully Updated Record';
                        }
                        else {
                            $master_ids[]           = DB::table('tblbusinesssegment')->insert($data);
                            $srv_resp['messages']   = 'Successfully Added Record';
                        }
					}
				}
				
			}
			
			$srv_resp['sts']        = STS_OK;	// good
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();				// log activity in db
        return json_encode($srv_resp);		// return json encoded results
	}
    
	//-----------------------------------------------------
    //  Function 7.3: getBusinesssegmentDelete
	//  Description: function for deleting Business segment
    //-----------------------------------------------------
	public function getBusinesssegmentDelete($id)
	{
		$segment = Businesssegment::where('businesssegmentid', '=', $id)
					->where('entity_id', '=', Session::get('entity')->entityid)
					->first();

		$segment->is_deleted = 1;
		$segment->save();

        ActivityLog::saveLog();				// log activity in db
		$entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
    }

}
