<?php

use Illuminate\Http\Request;
use CreditBPO\Models\Options;
use CreditBPO\Models\ReportMatchingSME;
use CreditBPO\Models\ReportMatchingBank;
use CreditBPO\Models\EmailLead;
use CreditBPO\Libraries\PaymentCalculator;
use CreditBPO\Handlers\FinancialHandler;
//use Region;
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

//======================================================================
//  Class 56: Related Companies Controller
//      Server Scripts for adding report matching SMEs and Banks
//======================================================================
class ReportMatchingController extends BaseController
{
	
		/* variable declaration */
		private $url;							// storage for url
		private $merchant_id = 'CREDITBPO';		// define dragonpay merchant id
		private $merchant_key = 'N6f9rEW3';		// define dragonpay key
		private $bank_subscription;				// storage for bank_subscription
		private $paymentCalculator;
		private $accountDetails = [];

		//-----------------------------------------------------
	    //  Function 3.1: __construct
		//  Description: class constructor
	    //-----------------------------------------------------
		public function __construct()
		{
			try{
				if(env("APP_ENV") == "Production"){ // checks for environment and assigns dragonpay url
					$this->url = 'https://gw.dragonpay.ph/Pay.aspx';
				} else {
					$this->url = 'http://test.dragonpay.ph/Pay.aspx';
				}

				$this->bank_subscription = Permissions::get_bank_subscription(); // check for bank subscription status
				$this->paymentCalculator = new PaymentCalculator;
				if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor') == false){
					$this->accountDetails = BankSessionHelper::createBankSession(Auth::user());
				} else if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor')){
					$this->accountDetails = Session::get('supervisor');
				} else if (Auth::user() && Auth::user()->role === 0 && Session::has('supervisor')){
					$this->accountDetails = Session::get('supervisor');
				}
			}catch(Exception $ex){
				
				if(env("APP_ENV") == "Production"){
					Log::error($ex->getMessage());
		            return Redirect::to('dashboard/error');
		        }else{
		            throw $ex;
		        }
			}
	}
	//-----------------------------------------------------
    //  Function 56.1: getReportMatchingSignup
    //      Display the Signup Form for Report Matching SME
    //-----------------------------------------------------
	public function getLandingPage(){

		try{
			return View::make('report_matching.landing');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 56.1: getReportMatchingSignup
    //      Display the Signup Form for Report Matching SME
    //-----------------------------------------------------
	public function getSMESignup($entityid){

		$entity = Entity::find($entityid);

		$business_classifications = Options::generate_array('business_classifications');
		$yes_no = Options::generate_array('yes_no');
		$business_loan_plans = Options::generate_array('business_loan_plans');
		$regions = Region::generate_array();

		return View::make('report_matching.sme_signup', [
			'business_classifications' => $business_classifications,
			'yes_no' => $yes_no,
			'business_loan_plans' => $business_loan_plans,
			'regions' => $regions,
			'entity' => $entity
		]);
	}

	//-----------------------------------------------------
    //  Function 56.2: postReportMatchingSMEStore
    //      Post report matching signup for SME
    //-----------------------------------------------------
	public function postReportMatchingSME(){
		$entityid = Input::get('entityid');
			
		try{

			$messages = array(
				'first_name.required' => 'The First Name field is required.',
				'last_name.required' => 'The Last Name field is required.',
				'company.required' => 'The Company field is required.',
				'job_title.required' => 'The Job Title field is required.',
				'business_classification.required' => 'The Business Classification field is required.',
				'business_loan_plan.required' => 'The Business Classification field is required.',
				'talked_to_bank.required' => 'The Business Classification field is required.',
				'regions_served.required' => 'Regions serve is required',
				'loan_approved.required' => '"Was your loan approval?" is required',
			);

			$rules = array(
				'first_name'	=> 'required',
				'last_name'	=> 'required',
				'company'	=> 'required',
				'job_title'	=> 'required',
				'business_classification'	=> 'required',
				'business_loan_plan'	=> 'required',
				'talked_to_bank'	=> 'required',
				'regions_served'	=> 'required',
				'loan_approved'	=> 'required',
			);

			if(Auth::guest()){

				$signup_messages = array(
					'email.required' => 'The Email field is required.',
					'email.unique' => 'The Email you are trying to register is already taken.',
					'password.required' => 'The Password field is required.',
					'password_confirmation.required' => 'The Re-type Password field is required.'
				);

				$signup_rules = array(
					'email'	=> 'required|unique:tbllogin,email|email|unique:supervisor_application,email|unique:report_matching_sme,email',
					'password'	=> 'required|min:6|confirmed',
					'password_confirmation'	=> 'required|min:6'
				);

				$messages = array_merge($messages,$signup_messages);
				$rules = array_merge($rules,$signup_rules);
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails()) {
				return Redirect::to('/report_matching/sme_signup/'.$entityid)->withErrors($validator)->withInput();
			}


			if(env("APP_ENV") == "Production") {

				$recaptcha = request('recaptcha');

				if(empty($recaptcha)){
					return Redirect::to('/report_matching/sme_signup/'.$entityid)->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}
											
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
					'secret' => '6LdX1qUUAAAAAMBNQ9c4qEgqfQMw7pnnjGo8z0mF',
					'response' => $recaptcha

				)));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec ($ch);
				curl_close ($ch);

				$server_output = json_decode($server_output);

				/** Check if response is success */
				if(!$server_output->success){
					return Redirect::to('/report_matching/sme_signup/'.$entityid)->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}

				/** Check limit of score to 0.5 */
				elseif($server_output->score < 0.5){
					return Redirect::to('/report_matching/sme_signup/'.$entityid)->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
				}
			}

			if(Auth::guest()){

				/* Generate activation code */
				$activationdigit = rand(00000,99999);

				$user = new User();
				$user->email = request('email');
				$user->password = Hash::make(request('password'));
				$user->name = request('first_name').' '.request('last_name');
				$user->activation_code = $activationdigit;
				$user->activation_date = '0000-00-00';
				$user->role = 1;
				$user->status = 0;
				$user->save();

				$loginid = $user->loginid;

				Auth::loginUsingId($user->loginid, TRUE);
				
				if (env("APP_ENV") != "local") {

					try{
						$this->sendActivationLinkEmail($activationdigit, Input::get('email'));
					}catch(Exception $ex){
						Log::error($ex->getMessage());
		            	return Redirect::to('dashboard/error');
		            }

					if (env("APP_ENV") == "Production") {

						try{
							Mail::send('emails.signup-notice', array('name' => Input::get('name'), 'email_add' => request('email')), function($message) {
							$message->to('notify.user@creditbpo.com')
									->subject('New registered member');
							});
						}catch(Exception $ex){
			            	Log::error($ex->getMessage());
			            }
					}
				}
				$email = request('email');

			}else{
				$loginid = Auth::id();
				$email = Auth::user()->email;
			}

			$entityid = request('entityid');

			$sme = new ReportMatchingSME();
			$sme->first_name = request('first_name');
			$sme->last_name = request('last_name');
			$sme->email = $email;
			$sme->loginid = $loginid;
			$sme->entityid = $entityid;
			$sme->company = request('company');
			$sme->job_title = request('job_title');
			$sme->business_classification = request('business_classification');
			$sme->business_loan_plan = request('business_loan_plan');
			$sme->talked_to_bank = request('talked_to_bank');
			$sme->regions_served = implode(',',request('regions_served'));
			$sme->loan_approved = request('loan_approved');
			$sme->save();

			$emailExs = EmailLead::where(['email'=>Auth::user()->email,'activity_id' => 4])->first();

			if(empty($emailExs->email)){

				$emailLead = new EmailLead();
				$emailLead->email = $email;
				$emailLead->first_name = request('first_name');
				$emailLead->last_name = request('last_name');
				$emailLead->company_name = request('company');
				$emailLead->job_title = request('job_title');
				$emailLead->last_id = $sme->id;
				$emailLead->is_new = 0;
				$emailLead->submitted_date = date('Y-m-d H:i:s');
				$emailLead->created_at = date('Y-m-d H:i:s');
				$emailLead->updated_at = date('Y-m-d H:i:s');
				$emailLead->activity_id = 4;
				$emailLead->save();

				if (env("APP_ENV") == "Production") {

					try{

						$lead = [
							'fullname' => request('first_name').' '.request('last_name'),
							'email' => $email,
							'company' => request('company'),
							'business_size' => request('business_classification'),
							'job_title' => request('job_title'),
							'business_loan_plan' => request('business_loan_plan'),
							'talked_to_bank' => request('talked_to_bank'),
							'loan_approved' => request('loan_approved')
						];

						Mail::send('emails.matching-sme-notice', $lead, function($message) {
						$message->to('analizabongat@gmail.com')
								->subject('New Bank Matching Registration');
						});
					}catch(Exception $e){
		            	//
		            }
				}
			}

			session(['rm_sme_id' => $sme->id]);
			
			if(!empty($entityid)){
				$entity = Entity::find($entityid);
				return Redirect::to('dashboard/index')->with('success','Report Complete - Matching Started');
			}else{
				return Redirect::to('dashboard/index');
			}

		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{

	            throw $ex;

	        }
		}
		// } catch(\Exception $e) {
		// 	$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 19f7b57");

  //           $logParams = array("activity" => "User Signup", "stackTrace" => $e, "errorCode" => "19f7b57");

		// 	AuditLogs::saveActivityLog($logParams);
			
		// 	\Log::info("19f7b57: " . $e);

		// 	return Redirect::route('getSMESignup')->withErrors($error)->withInput();
		// }
		
	}

	//-----------------------------------------------------
    //  Function 56.3: postReportMatchingSMEStore
    //      Display Signup for Rerport Matching Bank
    //-----------------------------------------------------
	public function getBankSignup($loginid){

		try{
			$bank = DB::table('tbllogin as l')
				->leftJoin('tblbank as b','l.bank_id','b.id')
				->where('l.loginid',$loginid)
				->first();

			$bank_classifications = Options::generate_array('bank_classifications');

			return View::make('report_matching.bank_signup', [
				'bank_classifications' => $bank_classifications,
				'bank' => $bank
			]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 56.3: postReportMatchingSMEStore
    //      Display Signup for Rerport Matching Bank
    //-----------------------------------------------------
	public function postReportMatchingBank(){
		
		try{
			$messages = array(
				'first_name.required' => 'The First Name field is required.',
				'last_name.required' => 'The Last Name field is required.',
				'bank_name.required' => 'The Email field is required.',
				'job_title.required' => 'The Job Title field is required.',
				'bank_classification.required' => 'The Business Classification field is required.',
				'business_loans.required' => 'The Business Classification field is required.'
			);

			$rules = array(
				'first_name'	=> 'required',
				'last_name'	=> 'required',
				'bank_name'	=> 'required',
				'job_title'	=> 'required',
				'bank_classification'	=> 'required',
				'business_loans'	=> 'required'
			);

			if(Auth::guest()){

				$signup_messages = array(
					'email.required' => 'The Email field is required.',
					'email.unique' => 'The Email you are trying to register is already taken.',
					'password.required' => 'The Password field is required.',
					'password_confirmation.required' => 'The Re-type Password field is required.'
				);

				$signup_rules = array(
					'email'	=> 'required|unique:tbllogin,email|email|unique:supervisor_application,email',
					'password'	=> 'required|min:6|confirmed',
					'password_confirmation'	=> 'required|min:6'
				);

				$messages = array_merge($messages,$signup_messages);
				$rules = array_merge($rules,$signup_rules);
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			if(!empty(session('supervisor'))){
				$loginid = session('supervisor.loginid');
			}else{
				$loginid = Auth()->user()->loginid;
			}

			if ($validator->fails()) {
				//return Redirect::route('getBankSignup')->withErrors($validator)->withInput();
				return Redirect::to('/report_matching/bank_signup/'.$loginid)->withErrors($validator)->withInput();
			}

			// if(env("APP_ENV") == "Production") {

			// 	$recaptcha = request('recaptcha');

			// 	if(empty($recaptcha)){
			// 		return Redirect::to('/report_matching/bank_signup/'.$loginid)->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
			// 	}
											
			// 	$ch = curl_init();

			// 	curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
			// 	curl_setopt($ch, CURLOPT_POST, 1);
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
			// 		'secret' => '6LdX1qUUAAAAAMBNQ9c4qEgqfQMw7pnnjGo8z0mF',
			// 		'response' => $recaptcha

			// 	)));
			// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// 	$server_output = curl_exec ($ch);
			// 	curl_close ($ch);

			// 	$server_output = json_decode($server_output);

			// 	/** Check if response is success */
			// 	if(!$server_output->success){
			// 		return Redirect::to('/report_matching/bank_signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
			// 	}

			// 	/** Check limit of score to 0.5 */
			// 	elseif($server_output->score < 0.5){
			// 		return Redirect::to('/report_matching/bank_signup')->withErrors(['captcha'=>'Captcha verification failed.'])->withInput();
			// 	}
				
			// }

			if(Auth::guest()){

				/* Generate activation code */
				$activationdigit = rand(00000,99999);

				$user = new User();
				$user->email = request('email');
				$user->password = Hash::make(request('password'));
				$user->name = request('first_name').' '.request('last_name');
				$user->activation_code = $activationdigit;
				$user->activation_date = '0000-00-00';
				$user->role = 1;
				$user->status = 0;
				$user->save();

				$loginid = $user->loginid;
				Auth::loginUsingId($user->loginid, TRUE);
				
				if (env("APP_ENV") != "local") {

					try{
						$this->sendActivationLinkEmail($activationdigit, Input::get('email'));
					}catch(Exception $ex){
						Log::error($ex->getMessage());
		            	return Redirect::to('dashboard/error');
		            }

					if (env("APP_ENV") == "Production") {

						try{
							Mail::send('emails.signup-notice', array('name' => Input::get('name'), 'email_add' => request('email')), function($message) {
							$message->to('notify.user@creditbpo.com')
									->subject('New registered member');
							});
						}catch(Exception $ex){
			            	Log::error($ex->getMessage());
			            }
					}
				}
				
			}else{
				$loginid = Auth::id();
			}

			$bank = new ReportMatchingBank();
			$bank->loginid = $loginid;
			$bank->first_name = request('first_name');
			$bank->last_name = request('last_name');
			$bank->bank_name = request('bank_name');
			$bank->job_title = request('job_title');
			$bank->bank_classification = request('bank_classification');
			$bank->business_loans = request('business_loans');
			$bank->save();

			$emailExs = EmailLead::where(['email'=>Auth::user()->email,'activity_id' => 5])->first();

			if(empty($emailExs->email)){

				$emailLead = new EmailLead();
				$emailLead->email = Auth::user()->email;
				$emailLead->first_name = request('first_name');
				$emailLead->last_name = request('last_name');
				$emailLead->company_name = request('bank_name');
				$emailLead->job_title = request('job_title');
				$emailLead->last_id = $bank->id;
				$emailLead->is_new = 0;
				$emailLead->submitted_date = date('Y-m-d H:i:s');
				$emailLead->created_at = date('Y-m-d H:i:s');
				$emailLead->updated_at = date('Y-m-d H:i:s');
				$emailLead->activity_id = 5;
				$emailLead->save();

				if (env("APP_ENV") == "Production") {

					try{

						$lead = [
							'fullname' => request('first_name').' '.request('last_name'),
							'email' => Auth::user()->email,
							'company' => request('bank_name'),
							'business_size' => request('bank_classification'),
							'job_title' => request('job_title')
						];

						Mail::send('emails.matching-bank-notice', $lead, function($message) {
						$message->to('analizabongat@gmail.com')
								->subject('New Bank Matching Registration');
						});
					}catch(Exception $e){
		            	//
		            }
				}

			}

			session(['rm_bank_id' => $bank->id]);			
			return Redirect::to('report_matching/sme_reports')->with('success','Matching Started');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
		// } catch(\Exception $e) {
		// 	$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 19f7b57");

  //           $logParams = array("activity" => "User Signup", "stackTrace" => $e, "errorCode" => "19f7b57");

		// 	AuditLogs::saveActivityLog($logParams);
			
		// 	\Log::info("19f7b57: " . $e);

		// 	return Redirect::route('getSMESignup')->withErrors($error)->withInput();
		// }
		
	}

	//-----------------------------------------------------
    //  Function 56.3: getSMEReports
    //      Display Reports from matched SMEs
    //-----------------------------------------------------
	public function getSMEReports($tertiary = null, $id_industry = null){

		try{
			$organization = Bank::where('id', '=', $this->accountDetails['bank_id'])->first();
			$company_names      = array();
			$delete_company     = array();

			$entity = DB::table('tblentity')
			->select(
				'tbllogin.email as basic_email',
				'tblentity.entityid',
				'tblentity.current_bank',
				'tblentity.industry_main_id', 
				'tblentity.industry_sub_id', 
				'tblentity.industry_row_id',
				'tblentity.companyname',
				'tblentity.email',
				'tblentity.date_established',
				'tblentity.number_year',
				'tblentity.anonymous_report',
				'tblentity.approval_status',
				'tblentity.approval_date',
				'tblentity.is_premium',
				'tblentity.utility_bill',
				'tblentity.cityid',
				'tblentity.province',
				'zipcodes.city',
				'tblsmescore.score_sf',
				'tblsmescore.score_rm',
				'tblsmescore.score_cd',
				'tblsmescore.score_sd',
				'tblsmescore.score_bois',
				'tblsmescore.score_boe',
				'tblsmescore.score_mte',
				'tblsmescore.score_bdms',
				'tblsmescore.score_sp',
				'tblsmescore.score_ppfi',
				'tblsmescore.score_rfp',
				'tblsmescore.score_rfpm',
				'tblsmescore.score_facs',
				'tblindustry_main.main_title',
				'tblsmescore.created_at as completed_date',
				'tblentity.status',
				'tbllogin.status as login_status',
				'tbllogin.loginid',
				'investigator_tag.id as ci_started',
				'investigator_notes.status as ci_status',
				DB::raw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as score')
			)
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
					->leftJoin('tblindustry_main', function($join){
			    $join->on(function($query) {
			        $query->on('tblentity.industry_main_id', '=', 'tblindustry_main.main_code');
			        $query->orOn('tblentity.industry_main_id', '=','tblindustry_main.industry_main_id');
			    });
			})
			->leftJoin('tblsmescore', 'tblsmescore.entityid', '=', 'tblentity.entityid')
			->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
			->leftJoin('investigator_tag', 'investigator_tag.entity_id', '=', 'tblentity.entityid')
			->leftJoin('investigator_notes', 'investigator_notes.entity_id', '=', 'tblentity.entityid')
			->leftJoin('utility_bills', 'utility_bills.entity_id', '=', 'tblentity.entityid')
			->where('tblentity.is_deleted', 0);

			$entity->whereRaw('(tblentity.matching_optin = "Yes" OR tblentity.matching_optin = "Pending") AND (tblentity.status = 7)');

			$sEmail = request('email');
			if(!empty($sEmail)){
				$entity->whereRaw('tbllogin.email LIKE "%'.$sEmail.'%"');			
			}

			$sSmes = request('sme');
			if(!empty($smes)){
				$smeWhere = [];
				foreach ($smes as $sSmes) {
					$smeWhere[] = 'tblentity.entityid = '.$sSmes;
				}

				$smeWhere = implode(" OR ",$smeWhere);

				if(!empty($smeWhere)){
					$entity->whereRaw($smeWhere);
				}
			}
			
			$sRegion = request('region');
			if(!empty($sRegion)){
				$entity->where('tblentity.province',$sRegion);
			}

			$sIndustrySection = request('industry_section');
			if(!empty($sIndustrySection)){
				$entity->where('tblentity.industry_main_id',$sIndustrySection);
			}

			$sIndustryDivision = request('industry_division');
			if(!empty($sIndustryDivision)){
				$entity->where('tblentity.industry_sub_id',$sIndustryDivision);
			}

			$sIndustryGroup = request('industry_group');
			if(!empty($sIndustryGroup)){
				$entity->where('tblentity.industry_row_id',$sIndustryGroup);
			}

			$ratings = ['AAA','AA','A','BBB','BB','B','CCC','CC','C','D','E'];
			$sBccFrom = request('bcc_from');
			$sBccTo = request('bcc_to');

			$ratingsNewArr = [];
			if(!empty($sBccFrom) && !empty($sBccTo)){

				$bccFromIndx = array_search($sBccFrom,$ratings);
				$bccToIndx = array_search($sBccTo,$ratings);

				if($bccToIndx > $bccFromIndx){
					for($i=$bccFromIndx;$i<=$bccToIndx;$i++){
						$ratingsNewArr[$i] = $ratings[$i];
					}
				}else{
					for($i=$bccToIndx;$i<=$bccFromIndx;$i++){
						$ratingsNewArr[$i] = $ratings[$i];
					}
				}
			}

			$earningsManipulator = request('earnings_manipulator');
			//subscription
			$bank_subscription_note = '';

			if (($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) && $organization->is_custom == 0) {

	            $bank_login_id = $this->accountDetails['loginid'];

	            if ($this->accountDetails['parent_user_id'] != 0) {
	                $bank_login_id = $this->accountDetails['parent_user_id'];
	            }

	            /*----------------------------------------------------------
	            |	Check Paypal Subscription
	            |---------------------------------------------------------*/
				$subs_sts   = self::checkSubscriptionSts($bank_login_id);

	            if ($this->accountDetails['email'] != "testbank@palatine.test.com" && STS_NG == $subs_sts) {
	                return Redirect::to('/bank/subscribe');
	            }
	            else if ($this->accountDetails['email'] != "testbank@palatine.test.com" && 2 == $subs_sts) {
	                $subscription   = Transaction::where('loginid', $bank_login_id)
	                    ->where('description', 'Bank User Subscription')
	                    ->orderBy('id', 'desc')
	                    ->first();

	                /* Redirect to Pending Screen */
	                return View::make('payment.pending', array('refno' => $subscription['refno']));
	            } else {
	                /** No Processing   */
	            }

			} else if ($this->accountDetails['email'] != "testbank@palatine.test.com" && Auth::user()->role !== 0 && $organization->is_custom == 0) {
				/* Redirect to Payment */
	            return Redirect::to('/bank/subscribe');
			} else if (Auth::user() !== 0) {
				/* User accessing using admin account*/
				/* filtering results if filters are present */
				
			}

			if($id_industry){
				if($tertiary == 1){ //industry main
					$entity->where('tblentity.industry_main_id', '=', $id_industry);

					$industryValue = $id_industry;
				}else if($tertiary == 2){ //industry sub
					$entity->where('tblentity.industry_sub_id', '=', $id_industry);
					$main_code = Industrysub::where('sub_code', $id_industry)->pluck('main_code')->first();
					$industryValue = $main_code;
				}else if($tertiary == 3){ //industry class
					$entity->where('tblentity.industry_row_id', '=', $id_industry);
					$groupcode = DB::table('tblindustry_class')->where('class_code', $id_industry)->pluck('group_code')->first();
					$subcode = DB::table('tblindustry_group')->where('group_code', $groupcode)->pluck('sub_code')->first();
					$main_code = Industrysub::where('sub_code', $subcode)->pluck('main_code')->first();
					$industryValue = $main_code;
				}
			}else if($id_industry == null){
				$industryValue = $id_industry;
			}

			$entity = $entity->groupBy('entityid')->get();
			
			$bank = Bank::find($this->accountDetails['bank_id']);

			// process entity and update fields to readable content
			foreach($entity as $key=>$value){
				if ($value->status == 7) {
					$entity[$key]->status = '<span style="color:GREEN;">Completed</span>';
					$entity[$key]->val = "Completed";
					$notes_count        = $value->ci_status;
					$tag_count          = $value->ci_started;
					if ($notes_count && $notes_count>0) {
						$invst_sts      = '<a href = "'.URL::to('summary/smesummary').'/'.$entity[$key]->entityid.'/ci_view" > <span style="color:blue;">Verified</span> </a>';
					} elseif ($tag_count) {
						$invst_sts      = '<span style="color:green;">In Review</span>';
					} else {
						$invst_sts      = '<span style="color:red;">Awaiting Assignment</span>';
					}
				} else {
					if($value->login_status == 0 || $value->login_status == 1){
						$entity[$key]->status       = '<span style="color:GREY;">Not Started</span>';
						$entity[$key]->val 			= "Not Started";
						$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
					} else {
						$entity[$key]->status       = '<span style="color:RED;">In Progress</span>';
						$entity[$key]->val 			= "In Progress";
						$invst_sts                  = '<span style="color:grey;">Not ready for investigation</span>';
					}
				}
				$entity[$key]->invst_sts		= $invst_sts;
				$entity[$key]->pdefault			= STR_EMPTY;
				$entity[$key]->cbpo_score_color = STR_EMPTY;
				$entity[$key]->bank_score_color = STR_EMPTY;
				$entity[$key]->m_score = App::make('ProfileSMEController')->computeBeneishScore($entity[$key]->entityid);

				$entity[$key]->earnings_manipulator = '';
				if($entity[$key]->val != "In Progress")
	            {    
	                if(number_format($entity[$key]->m_score, 2) >= -2.22){
	                    $entity[$key]->earnings_manipulator = 'Likely';
	                }else{
	                    $entity[$key]->earnings_manipulator = 'Unlikely';
	                }                
	            }

	            $entity[$key]->earnings_manipulator_hidden = 0;
	            
	            if(!empty($earningsManipulator)){
	            	if($earningsManipulator != $entity[$key]->earnings_manipulator){
	            		$entity[$key]->earnings_manipulator_hidden = 1;
	            	}
	            }
				
				// update score as readable
				$financialChecker = false;
				$financialTotal = 0;

				$financialReport = FinancialReport::where(['entity_id' => $entity[$key]->entityid, 'is_deleted' => 0])
							->orderBy('id', 'desc')
							->first();
				if($financialReport){
					// $finalRatingScore = ActionController::financialRatingComputation($financialReport->id);
					$ac = new FinancialHandler();
					$finalRatingScore = $ac->financialRatingComputation($financialReport->id);
					if($finalRatingScore) {
						$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
						$financialChecker = true;
					}
				}
				if($financialChecker){
					//Financial Position
					if($financialTotal >= 1.6) {
						$entity[$key]->score_sf          = "AAA - Excellent";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '1% - Very Low Risk';
					} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
						$entity[$key]->score_sf          = "AA - Very Good";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '2% - Very Low Risk';
					} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
						$entity[$key]->score_sf          = "A - Good";
						$entity[$key]->cbpo_score_color  = 'green';
						$entity[$key]->pdefault          = '3% - Low Risk';
					} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
						$entity[$key]->score_sf          = "BBB - Positive";
						$entity[$key]->cbpo_score_color  = 'orange';
						$entity[$key]->pdefault          = '4% - Moderate Risk';
					} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
						$entity[$key]->score_sf          = "BB - Normal";
						$entity[$key]->cbpo_score_color  = 'orange';
						$entity[$key]->pdefault          = '6% - Moderate Risk';
					} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
						$entity[$key]->score_sf   	 	 = "B - Satisfactory";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '8% - Moderate Risk';
					} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
						$entity[$key]->score_sf          = "CCC - Unsatisfactory";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '15% - High Risk';
					} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
						$entity[$key]->score_sf          = "CC - Adverse";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '20% - High Risk';
					} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
						$entity[$key]->score_sf          = "C - Bad";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '25% - Very High Risk';
					} else {
						$entity[$key]->score_sf          = "D - Critical";
						$entity[$key]->cbpo_score_color  = 'red';
						$entity[$key]->pdefault          = '32% - Very High Risk';
					}
				} elseif ($value->score_sf){
					$cred_rating_exp    = explode('-', $value->score_sf);
					$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
					$rate_val   = intval($rate_val);

					if($rate_val >= 177){
						$entity[$key]->score_sf			= 'AA - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'green';
						$entity[$key]->pdefault			= '2% - Very Low Risk';
					}
					else if($rate_val >= 150 && $rate_val <= 176) {
						$entity[$key]->score_sf 		= 'A - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'green';
						$entity[$key]->pdefault 		= '3% - Low Risk';
					}
					else if($rate_val >= 123 && $rate_val <= 149) {
						$entity[$key]->score_sf 		= 'BBB - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'orange';
						$entity[$key]->pdefault 		= '4% - Moderate Risk';
					}
					else if($rate_val >= 96 && $rate_val <= 122) {
						$entity[$key]->score_sf			= 'BB - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'orange';
						$entity[$key]->pdefault			= '6% - Moderate Risk';
					}
					else if($rate_val >= 68 && $rate_val <= 95) {
						$entity[$key]->score_sf			= 'B - Some Vulnerability';
						$entity[$key]->cbpo_score_color = 'red';
						$entity[$key]->pdefault			= '8% - High Risk';
					}
					else if($rate_val < 68) {
						$entity[$key]->score_sf 		= 'CCC - '.$cred_rating_exp[1];
						$entity[$key]->cbpo_score_color = 'red';
						if($rate_val >= 42 && $rate_val <= 68) {
							$entity[$key]->pdefault = '15% - High Risk';
						} else {
							$entity[$key]->pdefault = '30% - Very High Risk';
						}
					}
				}

				if(!empty($ratingsNewArr)){
					$scoreSfRaw = $entity[$key]->score_sf;
					$scoreSfExp = explode('-',$scoreSfRaw);
					$scoreSfRating = trim($scoreSfExp[0]);

					if(!in_array($scoreSfRating,$ratingsNewArr)){
						$entity[$key]->hideRating = 1;
					}else{
						$entity[$key]->hideRating = 0;
					}
				}else{
					$entity[$key]->hideRating = 0;
				}
				
				$preliminary_probability = request('preliminary_probability'); 
				if(!empty($preliminary_probability)){

					if($entity[$key]->pdefault != $preliminary_probability) continue;
				}

				$entity[$key]->score_bank = $entity[$key]->score_sf;
				$entity[$key]->bank_score_color = $entity[$key]->cbpo_score_color;
				// bank score computation
				
				$bank_group = BankIndustry::where('bank_id', $value->current_bank)
						->where('industry_id', $value->industry_main_id)->first();
				$bank_formula = BankFormula::where('bank_id', $value->current_bank)->first();
				if($bank->is_standalone == 0) {	
					$entity[$key]->report_type = "Full Rating";
				} elseif ($bank->is_standalone == 1) {
					if ($value->is_premium == PREMIUM_REPORT) {
						$entity[$key]->report_type = "Premium";
					} elseif ($value->is_premium == SIMPLIFIED_REPORT) {
						$entity[$key]->report_type = "Simplified Rating";
					} else {
						$entity[$key]->report_type = "Standalone";
					}
				}

				if($bank_group && $entity[$key]->score_bank && $bank->is_standalone == 0){
					if($bank_formula){
						//bcc
						$bgroup_array = explode(',', $bank_formula->bcc);
						$bgroup_item = 0;
						$bgroup_total = 0;
						if(in_array('rm', $bgroup_array)) {
							$bgroup_item += $value->score_rm;
							$bgroup_total += MAX_RM;
						}
						if(in_array('cd', $bgroup_array)) {
							$bgroup_item += $value->score_cd;
							$bgroup_total += MAX_CD;
						}
						if(in_array('sd', $bgroup_array)) {
							$bgroup_item += $value->score_sd;
							$bgroup_total += MAX_SD;
						}
						if(in_array('boi', $bgroup_array)) {
							$bgroup_item += $value->score_bois;
							$bgroup_total += MAX_BOI;
						}

						if (0 < $bgroup_total) {
							$bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
						} else {
							$bgroup = 0;
						}

						//mq
						$mgroup_array = explode(',', $bank_formula->mq);
						$mgroup_item = 0;
						$mgroup_total = 0;
						if(in_array('boe', $mgroup_array)) {
							$mgroup_item += $value->score_boe;
							$mgroup_total += MAX_BOE;
						}
						if(in_array('mte', $mgroup_array)) {
							$mgroup_item += $value->score_mte;
							$mgroup_total += MAX_MTE;
						}
						if(in_array('bd', $mgroup_array)) {
							$mgroup_item += $value->score_bdms;
							$mgroup_total += MAX_BD;
						}
						if(in_array('sp', $mgroup_array)) {
							$mgroup_item += $value->score_sp;
							$mgroup_total += MAX_SP;
						}
						if(in_array('ppfi', $mgroup_array)) {
							$mgroup_item += $value->score_ppfi;
							$mgroup_total += MAX_PPFI;
						}

						if (0 < $mgroup_total) {
							$mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
						} else {
							$mgroup = 0;
						}

						//fa
						$fgroup_array = explode(',', $bank_formula->fa);
						if(@count($fgroup_array) == 2){
							$fgroup = $value->score_facs;
						} else {
							if($bank_formula->fa == 'fp'){
								$fgroup = $value->score_rfp;
							} elseif ($bank_formula->fa == 'fr'){
								$fgroup = $value->score_rfpm;
							} else {
								$fgroup = 0;
							}
						}
					} else {
						$bgroup = ( $value->score_rm + $value->score_cd + $value->score_sd + $value->score_bois );
						$mgroup = ( $value->score_boe + $value->score_mte + $value->score_bdms + $value->score_sp + $value->score_ppfi );
						$fgroup = $value->score_facs;
					}

					$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));
					if($bank_formula && $bank_formula->boost == 0){
						// no boost
					} else {
						//boost
						if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
						if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
						if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
					}

					//convert score
					if($gscore >= 177){
						$entity[$key]->score_bank		= 'AA - Excellent';
						$entity[$key]->bank_score_color = 'green';
					} elseif ($gscore >= 150 && $gscore <= 176) {
						$entity[$key]->score_bank = 'A - Strong';
						$entity[$key]->bank_score_color = 'green';
					} elseif ($gscore >= 123 && $gscore <= 149) {
						$entity[$key]->score_bank = 'BBB - Good';
						$entity[$key]->bank_score_color = 'orange';
					} elseif ($gscore >= 96 && $gscore <= 122) {
						$entity[$key]->score_bank = 'BB - Satisfactory';
						$entity[$key]->bank_score_color = 'orange';
					} elseif ($gscore >= 68 && $gscore <= 95) {
						$entity[$key]->score_bank = 'B - Some Vulnerability';
						$entity[$key]->bank_score_color = 'red';
					} elseif ($gscore < 68) {
						$entity[$key]->score_bank = 'CCC - Watchlist';
						$entity[$key]->bank_score_color = 'red';
					}
				}
				$company_names[]        = $entity[$key]->companyname;
				$search_entity          = array_keys($company_names, $entity[$key]->companyname);

				foreach ($search_entity as $remove_entity) {
					if ($entity[$key]->entityid > $entity[$remove_entity]->entityid) {
						if (!in_array($remove_entity, $delete_company)) {
							$delete_company[]   = $remove_entity;
						}
					}
				}

				$fa_report = FinancialReport::where(['entity_id' => $value->entityid, 'is_deleted' => 0])
	                ->orderBy('id', 'desc')
	                ->first();
	            if ((NULL != $fa_report) && ('<span style="color:GREEN;">Completed</span>' == $value->status)) {
	                $gf_lib                 = new GrowthForecastLib($value->entityid, $value->industry_main_id, $bank->is_standalone);
	                $value->forecast_data  = $gf_lib->getGrowthForecastData();
	            } else {
	                $value->forecast_data  = NULL;
	            }

			}

			$provinces = Province2::select('provDesc','provDesc')->orderBy('provDesc')->pluck('provDesc','provDesc');
			$industries = Industrymain::all()->pluck('main_title', 'main_code');

			// echo '<pre>';
			// print_r($entity);
			// echo '</pre>';
			// exit;
			return View::make('report_matching.sme_reports', array(
				'provinces'                 => $provinces->toArray(),
				'industries'                => $industries->toArray(),
				'smeSelected' => $sSmes ? $sSmes : [],
				'bccFrom' => $sBccFrom,
				'bccTo' => $sBccTo,
				'email' => $sEmail,
				'region' => $sRegion,
				'preliminary_probability' => $preliminary_probability,
				'earningsManipulator' => $earningsManipulator,
				'industry_section' => $sIndustrySection,
				'industry_division' => $sIndustryDivision,
				'industry_group' => $sIndustryGroup,
				'bank_name'                 => $bank->bank_name,
				'bank_subscription_note'    => $bank_subscription_note,
				'bank_subscription'         => ($this->bank_subscription != null && strtotime($this->bank_subscription) > strtotime(date('c'))) ? true : false,
				'industryValue'			    => $industryValue,
			))->with('entity', $entity);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

	}

	public static function checkSubscriptionSts($login_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
	        /*------------------------------------------------------------------*/
			$sts    = STS_NG;

			\Log::info($login_id);

			$loginData = User::where('loginid', '=', $login_id)->first();

			$organization = Bank::where('id', '=', $loginData->bank_id)->first();

			$perm = Permissions::get_bank_subscription();
			
			if($organization->is_custom == 1){
				$sts = STS_OK;
				return $sts;
			}

			if ($loginData->bypass_subscriptions_checks == 1 && strtotime($perm) > strtotime(date('c'))) {
				$sts = STS_OK;
				return $sts;
			} 

	        /*--------------------------------------------------------------------
	        /*	Get latest PayPal Susbscription from the Database
	        /*------------------------------------------------------------------*/
	        $ppal_data      = Payment::where('loginid', $login_id)
	            ->where('paymenttype', 'RecurringPayments')
	            ->orderBy('paymentid', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*	Get latest DragonPay Susbscription from the Database
	        /*------------------------------------------------------------------*/
	        $dragon_data    = Transaction::where('loginid', $login_id)
	            ->where('description', 'Bank User Subscription')
	            ->orderBy('id', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*	Latest subscription is via PayPal
	        /*------------------------------------------------------------------*/
	        if (($ppal_data)
	        && (!$dragon_data)) {
	            $sts    = self::checkPaypalSubscriptionSts($login_id);
	        }
	        else if ((!$ppal_data)
	        && ($dragon_data)) {
	            $sts    = self::checkDragonSubscriptionSts($login_id);
	        }
	        else if (strtotime($ppal_data['created_at']) > strtotime($dragon_data['created_at'])) {
	            $sts    = self::checkPaypalSubscriptionSts($login_id);
	        }
	        /*--------------------------------------------------------------------
	        /*	Latest subscription is via DragonPay
	        /*------------------------------------------------------------------*/
	        else {
	            $sts    = self::checkDragonSubscriptionSts($login_id);
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	public function setOptin($matchingOptin,$entityid){
		try{
			$entity = Entity::find($entityid);
					
			DB::table('tblentity')
					->where('entityid',$entityid)
					->update(['matching_optin'=>$matchingOptin]);
			
			if($matchingOptin == 'Yes'){	

				$user = User::find(Auth::user()->loginid);

				if($user->role !=4){
					
					$matching = ReportMatchingSME::where('entityid',$entity->entityid)->first();
					
					if(empty($matching->entityid)){
						return Redirect::to('/report_matching/sme_signup/'.$entityid);
					}else{
						return Redirect::to('/summary/'.$entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname))->with('success','Matching service active!');
					}
				}else{

					return Redirect::to('/report_matching/bank_signup/'.$entityid);
					
				}

			}else{

				return Redirect::to('/dashboard/index')->with('success','Matching service inactive!');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
}