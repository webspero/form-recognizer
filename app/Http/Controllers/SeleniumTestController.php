<?php
//======================================================================
//  Class 50: Selenium Test Controller
//      Public API Scripts that returns JSON and is run through HTTP
//======================================================================
use \Exception as Exception;

/**--------------------------------------------------------------------------
|   
|----------------------------------------------------------------------------
|	@file	PubicAPIController
|	@brief	Public API Scripts
|	@details DESCRIPTION: Public API Scripts that returns JSON and is run through HTTP
|
|       [PROPERTIES]
|           NONE
|
|       [METHODS]
|           -- Public Methods --
|			getCreditRatingByCode.................Gets the Rating according to the Public Link code
|			getCreditRatingByName.................Get Reports according to Company Name
|			getCreditRatingByLoginID..............Get Reports according to Login ID
|
|           -- Private Methods --
|			getCalculatedSmeReport................Gets the Calculated results according to Entity ID
|			convertFinPosition....................Converts Financial Position Score to Displayed Rating
|			convertFinancialPerformance...........Converts Financial Performance Score to Displayed Rating
|			converFinancialCondition..............Converts Financial Condition Score to Displayed Rating
|			getCreditRating.......................Acquires the Credit Rating Description according to CreditBPO score
|
|--------------------------------------------------------------------------*/

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\Interactions\WebDriverActions;
use Facebook\WebDriver\JavaScriptExecutor;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverWindow;
use Facebook\WebDriver\Chrome\ChromeOptions;

class SeleniumTestController extends BaseController
{
    /*-------------------------------------------------------------------------
	|	Properties Declaration
	|------------------------------------------------------------------------*/
    protected $webdriver;
    protected $webhost;
    protected $username;
    protected $password;
    protected $entity_id;
    protected $browser;
    
    /*-------------------------------------------------------------------------
	|	Business Details 1 Field Inputs
	|------------------------------------------------------------------------*/
    private $tb_biz_details_itm = array(
        array(
            'biz_details.companyname',
            array(
                array(' ', 'The Company Name field is required')
            ),
            array(
                'The Stark Industries'
            )
        ),
        array(
            'biz_details.company_tin',
            array(
                array(' ', 'The Company TIN field is required')
            ),
            array(
                '6325462318'
            )
        ),
        array(
            'biz_details.tin_num',
            array(
                array(' ', 'The Business Registration Number field is required')
            ),
            array(
                'REG123'
            )
        ),
        array(
            'biz_details.total_asset_grouping',
            array(
                array('Choose here', 'The Total Asset Grouping field is required')
            ),
            array(
                'Small - Php 3 million to Php 15 million'
            )
        ),
		 array(
            'biz_details.total_assets',
            array(
                array(' ', 'The Total Assets (PHP) field is required'),
				array('asda', 'The total assets must be a number - please do not use any signs ($, %, #, etc)'),
				array('-1', 'The total assets must be at least 0')
            ),
            array(
                '12'
            )
        ),			
        array(
            'biz_details.email',
            array(
                array(' ', 'The Company Email field is required'),
				array('invalid','The Company Email must be a valid email address')
            ),
            array(
                'test123@gmail.com'
            )
        ),
        array(
            'biz_details.address1',
            array(
                array(' ', 'The Primary Business Address, Room Building Street field is required')
            ),
            array(
                'Test 89'
            )
        ),
        array(
            'biz_details.phone',
            array(
                array(' ', 'The Primary Business Address, Telephone field is required')	
            ),
            array(
                '9782773366'
            )
        ),			
		 array(
            'biz_details.no_yrs_present_address',
            array(
                array(' ', 'The No. of Years in Present Address field is required'),
				array('asds', 'The No. of Years in Present Address must be an integer'),
				array(-1, 'The no yrs present address must be at least 0')
            ),
            array(
                '12'
            )
        ),		
		 array(
            'biz_details.date_established',
            array(
                array(' ', 'The Date Established / Incorporated field is required'),
				array(123123,'Date Established Invalid Format The Date Established / Incorporated must be a date before today')
            ),
            array(
                '2016-09-20'
            )
        ),
		 array(
            'biz_details.number_year',
            array(
                array(' ', 'The No. of Years in Business field is required'),
				array('asds', 'The number year must be an integer'),
				array(-1, 'The number year must be at least 0')
            ),
            array(
                '12'
            )
        ),	
		 array(
            'biz_details.employee_size',
            array(	
                array('Choose here', 'The Employee Size field is required')
            ),
            array(
                '11 to 20'
            )
        ),		
		 array(
            'biz_details.updated_date',
            array(
                array(' ', 'The Updated Date field is required'),
				array('21122312', 'Updated Date Invalid Format')
            ),
            array(
                '2016-09-05'	
            )
        ),
		 array(
            'biz_details.number_year_management_team',
            array(
                array(' ', 'The Number of Years Management Team has been Engaged in this Business field is required'),
				array('asas', 'The Number of Years Management Team has been Engaged in this Business must be an integer')
            ),
            array(
                '12'
            )
        ),
    );
    
    /*--------------------------------------------------------------------
    /*	Affiliates
    /*------------------------------------------------------------------*/
    private $tb_affliates_itm = array(
        array(
            'affiliates.related_company_name',
            array(
                array(' ', 'The Name field is required')
            ),
            array(
                'Affiliate Company'
            )
        ),
		array(
            'affiliates.related_company_address1',
            array(
                array(' ', 'The Address Line 1 field is required')
            ),
            array(
                'FC'
            )
        ),
		array(
            'affiliates.related_company_address2',
            array(
                array(' ')
            ),
            array(
                '89'
            )
        ),
		  array(
            'affiliates.related_company_phone',
            array(
                array(' ', 'The Phone Number field is required')
            ),
            array(
                '7833367363'
            )
        ),	
		 array(
            'affiliates.related_company_email',
            array(
                array(' ', 'The related company email field is required'),
				array('asdas', 'The related company email must be a valid email address')
            ),
            array(
                'test12@gmail.com'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Products and Services
    /*------------------------------------------------------------------*/
    private $tb_products_itm = array(
        array(
            'offers.servicesoffer_name',
            array(
                array(' ', 'The Products/Services Offered field is required')
            ),
            array(
                'Test'
            )
        ),
			array(
            'offers.servicesoffer_targetmarket',
            array(
                array(' ', 'The Target Market field is required')
            ),
            array(
                'Tester'
            )
        ),
			array(
            'offers.servicesoffer_share_revenue',
            array(
                array(' ', 'The Share to Total Sales % field is required'),
				array('asdf', 'The Share to Total Sales % must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The Share to Total Sales % must be at least 0'),
				array(101, 'The Share to Total Sales % may not be greater than 100')
            ),
            array(
                '20'
            )
        ),
		array(
            'offers.servicesoffer_seasonality',
            array(
                array('Choose here', 'The Seasonality field is required')
            ),
            array(
                'Year-Round'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Capital Details
    /*------------------------------------------------------------------*/
    private $tb_capital_itm = array(
        array(
            'capital_details.capital_authorized',
            array(
                array(' ', 'The Authorized Capital (PHP) field is required'),
				array('a', 'The Authorized Capital (PHP) must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The capital authorized must be at least 0')
            ),
            array(
                '12'
            )
        ),
			array(
            'capital_details.capital_issued',
            array(
                array(' ', 'The Issued Capital (PHP) field is required'),
				array('a', 'The Issued Capital (PHP) must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The capital issued must be at least 0')
            ),
            array(
                '12'
            )
        ),
			array(
            'capital_details.capital_paid_up',
            array(
                array(' ', 'The Paid-up Capital (PHP) field is required'),
				array('a', 'The Paid-up Capital (PHP) must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The capital paid up must be at least 0')
            ),
            array(
                '22'
            )
        ),		
		array(
            'capital_details.capital_ordinary_shares',
            array(
                array(' ', 'The Ordinary Shares field is required'),
				array('a', 'The Ordinary Shares must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The capital ordinary shares must be at least 0')
            ),
            array(
                '42'
            )
        ),		
		array(
            'capital_details.capital_par_value',
            array(
                array(' ', 'The Par Value field is required'),
				array('a', 'The Par Value must be a number - please do not use any signs ($, %, #, etc)'),
				array(-1, 'The capital par value must be at least 0')
            ),
            array(
                '42'
            )
        ),
    );
    
    /*--------------------------------------------------------------------
    /*	Main Locations
    /*------------------------------------------------------------------*/
    private $tb_main_loc_itm = array(
        
        array(
            'main_loc.location_size',
            array(
                array(' ', 'The Size of Premises (square meters) field is required'),
				array('a', 'The Size of Premises (square meters) must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '32'
            )
        ),	
		array(
            'main_loc.location_type',
            array(
                array('Choose here', 'The Premises Owned / Leased field is required')
            ),
            array(
                'Owned'
            )
        ),
		array(
            'main_loc.location_map',
            array(
                array('Choose here', 'The Location field is required')
            ),
            array(
                'Commercial area'
            )
        ),
		array(
            'main_loc.location_used',
            array(
                array('Choose here', 'The Premises Used As field is required')
            ),
            array(
                'Sales office'
            )
        ),
    );
    
    /*--------------------------------------------------------------------
    /*	Branches
    /*------------------------------------------------------------------*/
    private $tb_branches_itm = array(
        
        array(
            'branches.branch_address',
            array(
                array(' ', 'The Address field is required')
            ),
            array(
                'F-89 LA'
            )
        ),
		array(
            'branches.branch_owned_leased',
            array(
                array('Choose here', 'The Owned/ Leased field is required')
            ),
            array(
                'Owned'
            )
        ),
     	array(
            'branches.branch_phone',
            array(
                array(' ', 'The Phone Number field is required')
            ),
            array(
                '8643662243'
            )
        ),
	 	array(
            'branches.branch_email',
           array(
                array(' ', 'The Email field is required'),
				array('asds', 'The Email must be a valid email address')
            ),
            array(
                'new1223@gmail.com'
            )
        ),
		array(
            'branches.branch_contact_person',
            array(
                array(' ', 'The Contact Person field is required')
            ),
            array(
                'tester'
            )
        ),	
        array(
            'branches.branch_job_title',
            array(
                array(' ', 'The Job Title field is required')
            ),
            array(
                'Testing'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Certifications
    /*------------------------------------------------------------------*/
    private $tb_certifications_itm = array(
        array(
            'certifications.certification_details',
            array(
                array(' ', 'The Certification Details field is required')
            ),
            array(
                'Board of Investments (BOI)'
            )
        ),	
     	array(
            'certifications.certification_reg_no',
            array(
                array(' ', 'The Registration No. field is required')
            ),
            array(
                '23234555'
            )
        ),
	 	array(
            'certifications.certification_reg_date',
           array(
                array(' ', 'The Registration Date field is required'),
				array('1234567890', 'Invalid Date Format The Registration Date must be a date before today')
            ),
            array(
                '2016-09-07'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Insurance
    /*------------------------------------------------------------------*/
    private $tb_insurance_itm = array(
        array(
            'insurance.insurance_type',
            array(
                array(' ', 'The Insurance Type field is required')
            ),
            array(
                'TYPE 2'
            )
        ),	
		array(
        'insurance.insured_amount',
           array(
                array(' ', 'The Insured Amount field is required'),
				array('a', 'The Insured Amount must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '20000'
            )
        ),
    );
    
    /*-------------------------------------------------------------------------
	|	Business Details 2 Field Inputs
	|------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------
    /*	Major Customers
    /*------------------------------------------------------------------*/
    private $tb_major_cust_itm = array(
        array(
            'major_cust.customer_name',
            array(
                array(' ', 'The Customer Name field is required')
            ),
            array(
                'Customer1'
            )
        ),
		  array(
            'major_cust.customer_address',
            array(
                array(' ', 'The Address field is required')
            ),
            array(
                'address'
            )
        ),
		 array(
            'major_cust.customer_contact_person',
            array(
                array(' ', 'The Contact Person field is required')
            ),
            array(
                'contact person'
            )
        ),
		 array(
            'major_cust.customer_phone',
            array(
                array(' ', 'The Contact Phone Number field is required')
            ),
            array(
                '1234567890'
            )
        ),
		 array(
            'major_cust.customer_email',
            array(
                array(' ', 'The Contact Email field is required'),
				array('jhb', 'The Contact Email must be a valid email address')
            ),
            array(
                'contactemail@yopmail.com'
            )
        ),
        array(
            'major_cust.customer_started_years',
            array(
                array(' ', 'The Year Started Doing Business With field is required'),
				array('1900', 'Year cannot be lower than 1970'),
				array('2020','Year cannot exceed the year now'),
				array('asd', 'Invalid Date Format Year cannot be lower than 1970 Year cannot exceed the year now')
            ),
            array(
                '1998'
            )
        ),
		 array(
            'major_cust.customer_settlement',
            array(
                array('Choose here', 'The Customer payment behavior in last 3 payments field is required')
            ),
            array(
                'On Due Date'
            )
        ),
		
		 array(
            'major_cust.customer_share_sales',
            array(
                array(' ', 'The % Share of Your Sales to this Company vs. Your Total Sales field is required'),
				array('asdas', 'The % Share of Your Sales to this Company vs. Your Total Sales must be a number - please do not use any signs ($, %, #, etc)'),
				array('101', 'The % Share of Your Sales to this Company vs. Your Total Sales may not be greater than 100'),
				array(-1, 'The % Share of Your Sales to this Company vs. Your Total Sales must be at least 0')
			),
            array(
                '20.00'
            )
        ),
		array(
            'major_cust.customer_order_frequency',
            array(
                array('Choose here', 'The Order Frequency field is required')
            ),
            array(
                'Regularly'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Major Supplier
    /*------------------------------------------------------------------*/
    private $tb_major_supp_itm = array(
        array(
            'major_supp.supplier_name',
            array(
                array(' ', 'The Supplier Name field is required')
            ),
            array(
                'supplier name'
            )
        ),
		array(
            'major_supp.supplier_address',
            array(
                array(' ', 'The Address field is required')
            ),
            array(
                'supplier address'
            )
        ),
		array(
            'major_supp.supplier_contact_person',
            array(
                array(' ', 'The Contact Person field is required')
            ),
            array(
                'supplier contract person'
            )
        ),
		array(
            'major_supp.supplier_phone',
            array(
                array(' ', 'The Contact Phone Number field is required')
            ),
            array(
                '1234567890'
            )
        ),
		array(
            'major_supp.supplier_email',
            array(
                array(' ', 'The Contact Email field is required'),
				array('as', 'The Contact Email must be a valid email address')
            ),
            array(
                'suppliercontractemail@yopmail.com'
            )
        ),
		array(
            'major_supp.supplier_started_years',
            array(
                array(' ', 'The Year Started Doing Business With field is required'),
				array('1900', 'Year cannot be lower than 1970'),
				array('2020', 'Year cannot exceed the year now'),
				array('asa', 'Invalid Date Format Year cannot be lower than 1970 Year cannot exceed the year now')
            ),
            array(
                '1992'
            )
        ),
		array(
            'major_supp.supplier_settlement',
            array(
                array('Choose here', 'The Your payment to supplier in last 3 payments field is required')
            ),
            array(
                'Ahead of Due Date'
            )
        ),
		array(
            'major_supp.supplier_share_sales',
            array(
                array(' ', 'The % Share of Total Purchases field is required'),
				array('asd', 'The % Share of Total Purchases must be a number - please do not use any signs ($, %, #, etc)'),
				array(101, 'The % Share of Total Purchases may not be greater than 100'),
				array(-1, 'The % Share of Total Purchases must be at least 0')
            ),
            array(
                '20'
            )
        ),
		array(
            'major_supp.supplier_order_frequency',
            array(
                array('Choose here', 'The Payment Frequency field is required')
            ),
            array(
                'Monthly'
            )
        ),
		array(
            'major_supp.average_monthly_volume',
            array(
                array(' ', 'The Average Monthly Volume field is required'),
                array(
                    '20'
                )
        ),
		array(
            'major_supp.item_1',
            array(
                array(' ', 'The Item Name field is required')
            ),
            array(
                'item1'
            )
        ),
		array(
            'major_supp.item_2',
            array(
                array(' ', 'The Item Name field is required')
            ),
            array(
                'item2'
            )
        ),
		array(
            'major_supp.item_3',
            array(
                array(' ', 'The Item Name field is required')
            ),
            array(
                'item3'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Major Location
    /*------------------------------------------------------------------*/
    private $tb_major_loc_itm = array(
        array(
            'major_loc.marketlocation',
            array(
                array('Angeles')
            ),
            array(
                'San Carlos'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Competitors
    /*------------------------------------------------------------------*/
    private $tb_competitors_itm = array(
        array(
            'competitor.competitor_name',
            array(
                array(' ', 'The Name field is required')
            ),
            array(
                'competitors name'
            )
        ),
		array(
            'competitor.competitor_address',
            array(
                array(' ', 'The Address field is required')
            ),
            array(
                'competitors address'
            )
        ),
		array(
            'competitor.competitor_phone',
            array(
                array(' ', 'The Contact Phone field is required')
            ),
            array(
                '1234567890'
            )
        ),
		array(
            'competitor.competitor_email',
            array(
                array(' ', 'The competitor email field is required'),
				array('a', 'The competitor email must be a valid email address')
            ),
            array(
                'competitorsemail@yopmail.com'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Business Driver
    /*------------------------------------------------------------------*/
    private $tb_biz_drivr_itm = array(
        array(
            'driver.businessdriver_name',
            array(
                array(' ', 'The Business Driver Name field is required'),
				
            ),
            array(
                'business driver name'
            )
        ),
		array(
            'driver.businessdriver_total_sales',
            array(
                array(' ', 'The % Share of Total Sales field is required'),
				array('sad', 'The % Share of Total Sales must be a number - please do not use any signs ($, %, #, etc)'),
				array(101, 'The % Share of Total Sales may not be greater than 100'),
				array(-1, 'The % Share of Total Sales must be at least 0')
            ),
            array(
                '76'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Customer Segment
    /*------------------------------------------------------------------*/
    private $tb_biz_segment_itm = array(
        
		array(
            'bizsegment.businesssegment_name',
            array(
                array(' ', 'The Name field is required')
            ),
            array(
                'customer segment name'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Past Projects Completed
    /*------------------------------------------------------------------*/
    private $tb_past_proj_itm = array(
        array(
            'ppc.projectcompleted_name',
            array(
                array(' ', 'The Purpose of Project field is required')
            ),
            array(
                'purpose of projects'
            )
        ),
		array(
            'ppc.projectcompleted_cost',
            array(
                array(' ', 'The Cost of Project field is required'),
				array(-1, 'The projectcompleted cost must be at least 0')
            ),
            array(
                '700'
            )
        ),
		array(
            'ppc.projectcompleted_source_funding',
            array(
                array('Choose here', 'The Source of Funding field is required')
            ),
            array(
                'Family/Friends'
            )
        ),
		array(
            'ppc.projectcompleted_year_began',
            array(
                array(' ', 'The Year Began field is required'),
				array(1900, 'Year cannot be lower than 1970'),
				array(2020, 'Year began cannot be greater than the year now'),
				array('as', 'Invalid year format Year cannot be lower than 1970')
            ),
            array(
                '2015'
            )
        ),
        array(
            'ppc.projectcompleted_result',
            array(
                array('Choose here', 'The Result field is required')
            ),
            array(
                'Satisfactory'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Future Growth Initiatives
    /*------------------------------------------------------------------*/
    private $tb_future_init_itm = array(
        array(
            'fgi.futuregrowth_name',
            array(
                array(' ', 'The Purpose of Planned Project field is required')
            ),
            array(
                'planned project'
            )
        ),
		array(
            'fgi.futuregrowth_estimated_cost',
            array(
                array(' ', 'The Estimated Cost field is required'),
				array(-1, 'The futuregrowth estimated cost must be at least 0'),
				array('as', 'The Estimated Cost must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '1200'
            )
        ),
		array(
            'fgi.futuregrowth_implementation_date',
            array(
                array(' ', 'The Implementation Date field is required'),
				array(1900, 'The Implementation Date does not match the format Y-m-d')
            ),
            array(
                '2019-09-29'
            )
        ),
		array(
            'fgi.futuregrowth_source_capital',
            array(
                array('Choose here', 'The Sources of capital field is required')
            ),
            array(
                'Family/Friends'
            )
        ),
		array(
            'fgi.planned_goal_increase',
            array(
                array('Choose here', 'The by field is required')
            ),
            array(
                '10 - 15 %'
            )
        )
    );
    
    /*---------------------------------------------------------------
    /*	Succession Plan
    /*-------------------------------------------------------------*/
    private $tb_succession_itm = array(
        array(
            'bcs.succession_plan',
            array(
                array('Choose your Plan', 'The succession plan field is required')
            ),
            array(
                'None of the Above, Less than 5 years'
            )
        ),
        array(
            'bcs.succession_timeframe',
            array(
                array('Choose your Time Frame', 'The succession timeframe field is required')
            ),
            array(
                'No clear Succession Plan'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Competitive Landscape
    /*------------------------------------------------------------------*/
    private $tb_landscape_itm = array(
        array(
            'bcs.competition_landscape',
            array(
                array('Choose your Landscape', 'The competition landscape field is required')
            ),
            array(
                'Limited Market Entry'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Economic Factors
    /*------------------------------------------------------------------*/
    private $tb_eco_factors_itm = array(
        array(
            'bcs.ev_susceptibility_economic_recession',
            array(
                array('Choose here', 'Economic Recession is required')
            ),
            array(
                'Low'
            )
        ),
        array(
            'bcs.ev_foreign_exchange_interest_sensitivity',
            array(
                array('Choose here', 'Foreign Exchange is required')
            ),
            array(
                'Medium'
            )
        ),
        array(
            'bcs.ev_commodity_price_volatility',
            array(
                array('Choose here', 'Commodity Price is required')
            ),
            array(
                'High'
            )
        ),
        array(
            'bcs.ev_governement_regulation',
            array(
                array('Choose here', 'Government Regulation is required')
            ),
            array(
                'Low'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Tax Payments
    /*------------------------------------------------------------------*/
    private $tb_tax_payments_itm = array(
        array(
            'bcs.tax_payments',
            array(
                array('Choose here', 'The tax payments field is required')
            ),
            array(
                'Delinquent'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Risk Assessment
    /*------------------------------------------------------------------*/
    private $tb_risk_payments_itm = array(
        array(
            'risk.risk_assessment_name',
            array(
                array('Choose here', 'Please choose a Threat')
            ),
            array(
                'Intensity of competitive rivalry'
            )
        ),
        array(
            'risk.risk_assessment_solution',
            array(
                array('Choose here', 'Please choose a Counter Measure')
            ),
            array(
                'Focus'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Revenue Growth Potential
    /*------------------------------------------------------------------*/
    private $tb_grwt_potential_itm = array(
        array(
            'rev_growth.current_market',
            array(
                array('Choose here', 'Under Current Market Conditions is required')
            ),
            array(
                'Between 3% to 5%'
            )
        ),
        array(
            'rev_growth.new_market',
            array(
                array('Choose here', 'Expected Future Market Conditions is required')
            ),
            array(
                'Less than 3%'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Capital Investment for Expansion
    /*------------------------------------------------------------------*/
    private $tb_cap_expansion_itm = array(
        array(
            'cap_expand.current_year',
            array(
                array(' ', 'Within next 12 months is required'),
                array('-1', 'The current year must be at least 0'),
                array('999999999999999', 'The The Within next 12 months may not be greater than 9999999999999.99'),
                array('abcde', 'The The Within next 12 months must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '750000'
            )
        ),
        array(
            'cap_expand.new_year',
            array(
                array(' ', 'Within next 2 years is required'),
                array('abcde', 'The The Within next 2 years must be a number - please do not use any signs ($, %, #, etc)'),
                array('-1', 'The new year must be at least 0'),
                array('999999999999999', 'The The Within next 2 years may not be greater than 9999999999999.99')
            ),
            array(
                '1200000'
            )
        ),
    );
    
    private $tb_biz_details1_form = array(
        array(
            array('show-affiliates-expand', 'affiliates-expand-form'),
            array(
                array(
                    'related_company_name[0]', 'related_company_address1[0]', 'related_company_address2[0]',
                    // 'related_company_province[0]', 'related_company_cityid[0]'
                    'related_company_phone[0]', 'related_company_email[0]'
                ),
                array(
                    array(
                        array(
                            STR_EMPTY, STR_EMPTY, STR_EMPTY, STR_EMPTY, STR_EMPTY, STR_EMPTY, STR_EMPTY, STR_EMPTY
                        ),
                        array(
                            'The Company Name field is required', 'The Company Address field is required', 
                            // 'The City field is required', 'The Province field is required', 'The Zipcode field is required', 
                            'The Company Phone field is required', 'The Email field is required'
                        )
                    ),
                    
                ),
                array(
                    'Galactic Empire', 'Outer Rim', 'Dark Moon', 'Tattoine', 'Jabba City', '1123', '911', 'vader@empire.gov'
                ),
            ),
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Key Managers
    /*------------------------------------------------------------------*/
    private $tb_keymanagers_itm = array(
        array(
            'key_managers.firstname',
            array(
                array(' ', 'The First Name field is required')
            ),
            array(
                'Manager A'
            )
        ),
        array(
            'key_managers.middlename',
            array(
                array(' ', 'The Middle Name field is required')
            ),
            array(
                'Middle Manager'
            )
        ),
        array(
            'key_managers.lastname',
            array(
                array(' ', 'The Last Name field is required')
            ),
            array(
                'Last Manager'
            )
        ),
        array(
            'key_managers.birthdate',
            array(
                array(' ', 'The Birthdate field is required'),
                array('1234', 'The birthdate does not match the format Y-m-d The Birthdate must be a date before today')
            ),
            array(
                '1990-02-15'
            )
        ),
        array(
            'key_managers.nationality',
            array(
                array(' ', 'The Nationality field is required')
            ),
            array(
                'Italian'
            )
        ),
        array(
            'key_managers.civilstatus',
            array(
                array('Choose here', 'The Civil Status field is required')
            ),
            array(
                'Married'
            )
        ),
        array(
            'key_managers.profession',
            array(
                array(' ', 'The Profession field is required')
            ),
            array(
                'Criminal'
            )
        ),
        array(
            'key_managers.email',
            array(
                array(' ', 'The Email field is required'),
                array('abcde', 'The Email must be a valid email address')
            ),
            array(
                'manager@company.com'
            )
        ),
        array(
            'key_managers.address1',
            array(
                array(' ', 'The Address 1 field is required')
            ),
            array(
                'Address A'
            )
        ),
        array(
            'key_managers.address2',
            array(
                array(' ')
            ),
            array(
                'Address 2A'
            )
        ),
        array(
            'key_managers.no_yrs_present_address',
            array(
                array(' ', 'The No of Years in Present Address field is required'),
                array('-1', 'The no yrs present address must be at least 0'),
                array('3 Years', 'The No of Years in Present Address must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '99'
            )
        ),
        array(
            'key_managers.phone',
            array(
                array(' ', 'The Phone field is required')
            ),
            array(
                '632 911 1111'
            )
        ),
        array(
            'key_managers.tin_num',
            array(
                array(' ', 'The Tin Number field is required'),
                array('111', 'The Tin Number format is invalid')
            ),
            array(
                '111-111-111'
            )
        ),
        array(
            'key_managers.percent_of_ownership',
            array(
                array(' ', 'The % Ownership of Business field is required'),
                array('-1', 'The % Ownership of Business must be at least 0'),
                array('101', 'The % Ownership of Business may not be greater than 100'),
                array('5%', 'The % Ownership of Business must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '5'
            )
        ),
        array(
            'key_managers.number_of_year_engaged',
            array(
                array(' ', 'The Years of experience in this industry field is required'),
                array('-1', 'The number of year engaged must be at least 0'),
                array('5 years', 'The Years of experience in this industry must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '5'
            )
        ),
        array(
            'key_managers.position',
            array(
                array(' ', 'The Position field is required'),
            ),
            array(
                'Godfather'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Shareholders
    /*------------------------------------------------------------------*/
    private $tb_shareholders_itm = array(
        array(
            'shareholders.name',
            array(
                array(' ', 'The Name field is required'),
            ),
            array(
                'Shareholder A'
            )
        ),
        array(
            'shareholders.address',
            array(
                array(' ', 'The Address field is required'),
            ),
            array(
                'Address A'
            )
        ),
        array(
            'shareholders.amount',
            array(
                array(' ', 'The Amount of Shareholdings field is required'),
                array('-1', 'The amount must be at least 0'),
                array('5 million', 'The Amount of Shareholdings must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '5000000'
            )
        ),
        array(
            'shareholders.percentage_share',
            array(
                array(' ', 'The % Share field is required'),
                array('-1', 'The % Share must be at least 0'),
                array('101', 'The % Share may not be greater than 100'),
                array('10%', 'The % Share must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '10'
            )
        ),
        array(
            'shareholders.id_no',
            array(
                array(' ', 'The Tin Number field is required'),
                array('111111111', 'The Tin Number format is invalid')
            ),
            array(
                '111-111-111'
            )
        ),
        array(
            'shareholders.nationality',
            array(
                array(' ', 'The Nationality field is required')
            ),
            array(
                'Irish'
            )
        )
    );
    
    /*--------------------------------------------------------------------
    /*	Directors
    /*------------------------------------------------------------------*/
    private $tb_directors_itm = array(
        array(
            'directors.name',
            array(
                array(' ', 'The Name field is required')
            ),
            array(
                'Nicholas Fury'
            )
        ),
        array(
            'directors.id_no',
            array(
                array(' ', 'The Tin Number field is required'),
                array('333333333', 'The Tin Number format is invalid')
            ),
            array(
                '333-333-333'
            )
        ),
        array(
            'directors.address',
            array(
                array(' ', 'The Address field is required')
            ),
            array(
                'Classified'
            )
        ),
        array(
            'directors.nationality',
            array(
                array(' ', 'The Nationality field is required')
            ),
            array(
                'African'
            )
        ),
        array(
            'directors.percentage_share',
            array(
                array(' ', 'The % Share field is required'),
                array('-1', 'The % Share must be at least 0'),
                array('101', 'The % Share may not be greater than 100'),
                array('30%', 'The % Share must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '30'
            )
        ),
    );
    
    /*--------------------------------------------------------------------
    /*	Existing Credit Facilities
    /*------------------------------------------------------------------*/
    private $tb_ecf_itm = array(
        array(
            'ecf.lending_institution',
            array(
                array('Choose here', 'Lending Institution is required')
            ),
            array(
                'Banks'
            )
        ),
        array(
            'ecf.plan_credit_availment',
            array(
                array(' ', 'Amount of Line is required'),
                array('-1', 'The plan credit availment must be at least 0'),
                array('1 million', 'Amount of Line must be a number - please do not use any signs ($, %, #, etc)'),
            ),
            array(
                '1000000'
            )
        ),
        array(
            'ecf.purpose_credit_facility',
            array(
                array('Choose here', 'Purpose of Credit Facility is required')
            ),
            array(
                "Suppliers' Credit"
            )
        ),
        array(
            'ecf.credit_terms',
            array(
                array(' ', 'Credit Terms is required')
            ),
            array(
                "60 Days"
            )
        ),
        array(
            'ecf.outstanding_balance',
            array(
                array('-1', 'The outstanding balance must be at least 0'),
                array('100 Thousand', 'The outstanding balance must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                "100000"
            )
        ),
        array(
            'ecf.experience',
            array(
                array('Choose here')
            ),
            array(
                "Fair"
            )
        ),
        array(
            'ecf.other_remarks',
            array(
                array(' ')
            ),
            array(
                "Remarkable"
            )
        ),
        array(
            'ecf.cash_deposit_details',
            array(
                array(' ')
            ),
            array(
                "Savings"
            )
        ),
        // array(
        //     'ecf.cash_deposit_amount',
        //     array(
        //         array('-1', 'The cash deposit amount must be at least 0'),
        //         array('5 Thousand', 'The cash deposit amount must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "5000"
        //     )
        // ),
        array(
            'ecf.securities_details',
            array(
                array(' ')
            ),
            array(
                "Government Bonds"
            )
        ),
        // array(
        //     'ecf.securities_estimated_value',
        //     array(
        //         array('-1', 'The securities estimated value must be at least 0'),
        //         array('50 Thousand', 'The securities estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "50000"
        //     )
        // ),
        array(
            'ecf.property_details',
            array(
                array(' ')
            ),
            array(
                'Private Island'
            )
        ),
        // array(
        //     'ecf.property_estimated_value',
        //     array(
        //         array('-1', 'The property estimated value must be at least 0'),
        //         array('300 Million', 'The property estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "300000000"
        //     )
        // ),
        array(
            'ecf.chattel_details',
            array(
                array(' ')
            ),
            array(
                'Super Computer'
            )
        ),
        // array(
        //     'ecf.chattel_estimated_value',
        //     array(
        //         array('-1', 'The chattel estimated value must be at least 0'),
        //         array('80 Thousand', 'The chattel estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "80000"
        //     )
        // ),
        array(
            'ecf.others_details',
            array(
                array(' ')
            ),
            array(
                'Ancient Katana'
            )
        ),
        // array(
        //     'ecf.others_estimated_value',
        //     array(
        //         array('-1', 'The others estimated value must be at least 0'),
        //         array('120 Million', 'The others estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "120000000"
        //     )
        // ),
    );
    
    /*--------------------------------------------------------------------
    /*	Requested Credit Facilities
    /*------------------------------------------------------------------*/
    private $tb_rcl_itm = array(
        
        array(
            'rcl.plan_credit_availment',
            array(
                array(' ', 'Amount of Line is required'),
                array('-1', 'The plan credit availment must be at least 0'),
                array('3 Million', 'Amount of Line must be a number - please do not use any signs ($, %, #, etc)'),
            ),
            array(
                '3000000'
            )
        ),
        array(
            'rcl.purpose_credit_facility',
            array(
                array('Choose here', 'Purpose of Credit Facility is required')
            ),
            array(
                'Others (Pls Specify)'
            )
        ),
        array(
            'rcl.purpose_credit_facility_others',
            array(
                array(' ', 'Purpose of Credit Facility is required')
            ),
            array(
                'Purpose Driven Life'
            )
        ),
        array(
            'rcl.other_remarks',
            array(
                array(' ')
            ),
            array(
                'Remarkable Again'
            )
        ),
        array(
            'rcl.cash_deposit_details',
            array(
                array(' ')
            ),
            array(
                "Savings"
            )
        ),
        // array(
        //     'rcl.cash_deposit_amount',
        //     array(
        //         array('-1', 'The cash deposit amount must be at least 0'),
        //         array('5 Thousand', 'The cash deposit amount must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "5000"
        //     )
        // ),
        array(
            'rcl.securities_details',
            array(
                array(' ')
            ),
            array(
                "Government Bonds"
            )
        ),
        // array(
        //     'rcl.securities_estimated_value',
        //     array(
        //         array('-1', 'The securities estimated value must be at least 0'),
        //         array('50 Thousand', 'The securities estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "50000"
        //     )
        // ),
        array(
            'rcl.property_details',
            array(
                array(' ')
            ),
            array(
                'Private Island'
            )
        ),
        // array(
        //     'rcl.property_estimated_value',
        //     array(
        //         array('-1', 'The property estimated value must be at least 0'),
        //         array('300 Million', 'The property estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "300000000"
        //     )
        // ),
        array(
            'rcl.chattel_details',
            array(
                array(' ')
            ),
            array(
                'Super Computer'
            )
        ),
        // array(
        //     'rcl.chattel_estimated_value',
        //     array(
        //         array('-1', 'The chattel estimated value must be at least 0'),
        //         array('80 Thousand', 'The chattel estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "80000"
        //     )
        // ),
        array(
            'rcl.others_details',
            array(
                array(' ')
            ),
            array(
                'Ancient Katana'
            )
        ),
        // array(
        //     'rcl.others_estimated_value',
        //     array(
        //         array('-1', 'The others estimated value must be at least 0'),
        //         array('120 Million', 'The others estimated value must be a number - please do not use any signs ($, %, #, etc)'),
        //     ),
        //     array(
        //         "120000000"
        //     )
        // ),
    );
    
    /*-------------------------------------------------------------------------
	|	Owner Details
	|------------------------------------------------------------------------*/
    private $tb_owner_itm = array(
        array(
            'biz_owner.firstname',
            array(
                array(' ', 'The First Name field is required')
            ),
            array(
                "Owner A"
            )
        ),
        array(
            'biz_owner.middlename',
            array(
                array(' ', 'The Middle Name field is required')
            ),
            array(
                "Middle Owner"
            )
        ),
        array(
            'biz_owner.lastname',
            array(
                array(' ', 'The Last Name field is required')
            ),
            array(
                "Last Owner"
            )
        ),
        array(
            'biz_owner.birthdate',
            array(
                array(' ', 'The Birthdate field is required'),
                array('2016', 'The birthdate does not match the format Y-m-d')
            ),
            array(
                "1990-02-15"
            )
        ),
        array(
            'biz_owner.nationality',
            array(
                array(' ', 'The Nationality field is required')
            ),
            array(
                "Kryptonite"
            )
        ),
        array(
            'biz_owner.civilstatus',
            array(
                array('Choose here', 'The Civil Status field is required')
            ),
            array(
                "Married"
            )
        ),
        array(
            'biz_owner.profession',
            array(
                array(' ', 'The Profession field is required')
            ),
            array(
                "Kung Fu Master"
            )
        ),
        array(
            'biz_owner.email',
            array(
                array(' ', 'The email field is required'),
                array('email', 'The email must be a valid email address')
            ),
            array(
                'owner@company.com'
            )
        ),
        array(
            'biz_owner.address1',
            array(
                array(' ', 'The Address 1 field is required')
            ),
            array(
                'Address A'
            )
        ),
        array(
            'biz_owner.address2',
            array(
                array(' ')
            ),
            array(
                'Address 2A'
            )
        ),
        array(
            'biz_owner.no_yrs_present_address',
            array(
                array(' ', 'The No of Years in Present Address field is required'),
                array('-1', 'The no yrs present address must be at least 0'),
                array('4 years', 'The No of Years in Present Address must be a number - please do not use any signs ($, %, #, etc)'),
            ),
            array(
                '28'
            )
        ),
        array(
            'biz_owner.phone',
            array(
                array(' ', 'The Phone field is required')
            ),
            array(
                '632 911 1111'
            )
        ),
        array(
            'biz_owner.tin_num',
            array(
                array(' ', 'The Tin Number field is required'),
                array('444444444', 'The Tin Number format is invalid')
            ),
            array(
                '444-444-444'
            )
        ),
        array(
            'biz_owner.percent_of_ownership',
            array(
                array(' ', 'The % Ownership of Business field is required'),
                array('-1', 'The % Ownership of Business must be at least 0'),
                array('101', 'The % Ownership of Business may not be greater than 100'),
                array('70%', 'The % Ownership of Business must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '70'
            )
        ),
        array(
            'biz_owner.number_of_year_engaged',
            array(
                array(' ', 'The Years of experience in this industry field is required'),
                array('-1', 'The number of year engaged must be at least 0'),
                array('9 Years', 'The Years of experience in this industry must be a number - please do not use any signs ($, %, #, etc)')
            ),
            array(
                '9'
            )
        ),
        array(
            'biz_owner.position',
            array(
                array(' ', 'The Position field is required')
            ),
            array(
                'Kingpin'
            )
        ),
        /*---------------------------------------------------------------
        /*	Personal Loans
        /*-------------------------------------------------------------*/
        array(
            'owner_loans.balance',
            array(
                array(' ', 'The Outstanding Balance field is required'),
                array('-1', 'The balance must be at least 0'),
                array('50 Thousand', 'The Outstanding Balance must be a number - please do not use any signs ($, %, #, etc)'),
            ),
            array(
                '50000'
            )
        ),
        array(
            'owner_loans.monthly_amortization',
            array(
                array(' ', 'The Monthly Amortization field is required'),
                array('-1', 'The monthly amortization must be at least 0'),
                array('5 Thousand', 'The Monthly Amortization must be a number - please do not use any signs ($, %, #, etc)'),
            ),
            array(
                '5000'
            )
        ),
        array(
            'owner_loans.creditor',
            array(
                array(' ', 'The Creditor field is required')
            ),
            array(
                'BPI'
            )
        ),
        array(
            'owner_loans.due_date',
            array(
                array(' ', 'The Due Date field is required'),
                array('2016', 'Invalid Date format'),
            ),
            array(
                '2017-03-09'
            )
        ),
        /*---------------------------------------------------------------
        /*	Other Business
        /*-------------------------------------------------------------*/
        array(
            'other_biz.bus_name',
            array(
                array(' ', 'The Business Name field is required')
            ),
            array(
                'Muay Thai Gym'
            )
        ),
        array(
            'other_biz.bus_type',
            array(
                array(' ', 'The Business Type field is required')
            ),
            array(
                'Martial Arts'
            )
        ),
        array(
            'other_biz.bus_location',
            array(
                array('Angeles')
            ),
            array(
                'Makati'
            )
        ),
        /*---------------------------------------------------------------
        /*	Educational Background
        /*-------------------------------------------------------------*/
        array(
            'owner_educ.school_name',
            array(
                array(' ', 'The School Name field is required')
            ),
            array(
                'Highschool A'
            )
        ),
        array(
            'owner_educ.educ_degree',
            array(
                array('Choose here', 'The Level of Completion field is required')
            ),
            array(
                'Graduated'
            )
        ),
        /*---------------------------------------------------------------
        /*	Spouse Details
        /*-------------------------------------------------------------*/
        array(
            'owner_spouse.firstname',
            array(
                array(' ', 'The First Name field is required')
            ),
            array(
                'Spouse A'
            )
        ),
        array(
            'owner_spouse.middlename',
            array(
                array(' ', 'The Middle Name field is required')
            ),
            array(
                'Middle Spouse'
            )
        ),
        array(
            'owner_spouse.lastname',
            array(
                array(' ', 'The Last Name field is required')
            ),
            array(
                'Last Spouse'
            )
        ),
        array(
            'owner_spouse.birthdate',
            array(
                array(' ', 'The Birthdate field is required'),
                array('1990', 'The birthdate does not match the format Y-m-d')
            ),
            array(
                '1990-02-15'
            )
        ),
        array(
            'owner_spouse.nationality',
            array(
                array(' ', 'The Nationality field is required')
            ),
            array(
                'Filipino'
            )
        ),
        array(
            'owner_spouse.profession',
            array(
                array(' ', 'The Profession field is required')
            ),
            array(
                'Spouse'
            )
        ),
        array(
            'owner_spouse.phone',
            array(
                array(' ', 'The Phone field is required')
            ),
            array(
                '632 922 2222'
            )
        ),
    );
    
    /*-------------------------------------------------------------------------
	|	Selenium Test Configuration Initialization
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		NONE
	|------------------------------------------------------------------------*/
    public function initSeleniumWebdrvr()
    {
        set_time_limit(0);
        
        /*---------------------------------------------------------------
        /*	Variable Declaration
        /*-------------------------------------------------------------*/
        $this->username     = Input::get('username');
        $this->password     = Input::get('password');
        $this->entity_id    = Input::get('report_id');
        $this->browser      = Input::get('browser');
        $this->webhost      = 'http://localhost:4444/wd/hub';
        
        try {
            /*---------------------------------------------------------------
            /*	Open Browser
            /*-------------------------------------------------------------*/
            /** Firefox */
            if (1 == $this->browser) {
                $this->webdriver    = RemoteWebDriver::create($this->webhost, DesiredCapabilities::firefox());
            }
            /** Chrome */
            else if (2 == $this->browser) {
                $this->webdriver    = RemoteWebDriver::create($this->webhost, DesiredCapabilities::chrome());
            }
        
            $this->webdriver->manage()->window()->maximize();
            
            $this->seleniumTestMng();
        }
        catch(Exception $err) {
            echo "An error occured: ".$err->getMessage();
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Selenium Test Management
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		rating      --- Rating Result
	|------------------------------------------------------------------------*/
    private function seleniumTestMng()
    {
        $login          = $this->seleniumLoginUser();
        $open_report    = $this->seleniumOpenReport();
        
        if (STS_OK == $open_report['sts']) {
            $this->webdriver->navigate()->to('http://ruffycolladopc:1337/CreditBPO_Mk1.00/public/summary/smesummary/'.$open_report['entity_id']);
            $this->fillBusinessDetails1();
            $this->fillBusinessDetails2();
            $this->fillKeyManagers();
            $this->fillOwnerDetails();
            $this->fillCns();
            $this->fillEcf();
            $this->fillRcl();
        }
        else {
            echo $open_report['msg'];
        }
        
        $this->destructSeleniumWebdrvr();
    }
    
    /*-------------------------------------------------------------------------
	|	User Login
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		NONE
	|------------------------------------------------------------------------*/
    private function seleniumLoginUser()
    {
        /*---------------------------------------------------------------
        /*	Open login page of website
        /*-------------------------------------------------------------*/
        $this->webdriver->get('http://ruffycolladopc:1337/CreditBPO_Mk1.00/public');
        
        /*---------------------------------------------------------------
        /*	Find the Login Form
        /*-------------------------------------------------------------*/
        $form = $this->webdriver->findElement(WebDriverBy::id('login'));
        
        /*---------------------------------------------------------------
        /*	Fill the Login Form
        /*-------------------------------------------------------------*/
        $form->findElement(WebDriverBy::name('email'))->sendKeys($this->username);
        $form->findElement(WebDriverBy::name('password'))->sendKeys($this->password);
        
        /*---------------------------------------------------------------
        /*	Login user
        /*-------------------------------------------------------------*/
        $form->submit();
    }
    
    /*-------------------------------------------------------------------------
	|	Pick Report that has not been submitted
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- Choosing report response status
	|------------------------------------------------------------------------*/
    private function seleniumOpenReport()
    {
        /*---------------------------------------------------------------
        /*	Variable Declaration
        /*-------------------------------------------------------------*/
        $resp['sts']        = STS_NG;
        $resp['msg']        = STR_EMPTY;
        $resp['entity_id']  = 0;
        
        /*---------------------------------------------------------------
        /*	Get the latest Report that has no Rating
        /*-------------------------------------------------------------*/
        $entity = Entity::where('entityid', $this->entity_id)
                ->first();
        
        /*---------------------------------------------------------------
        /*	A report that has no rating exist
        /*-------------------------------------------------------------*/
        if ($entity) {
            $resp['sts']        = STS_OK;
            $resp['entity_id']  = $entity->entityid;
        }
        /*---------------------------------------------------------------
        /*	All reports are rated
        /*-------------------------------------------------------------*/
        else {
            $resp['msg']        = 'Please Login and Create a Report first';
        }
        
        /*	関数終亁    */
        return $resp;
    }
    
    /*-------------------------------------------------------------------------
	|	Fill an item on the AJAX Form
	|--------------------------------------------------------------------------
	|	@param [in] 	item    --- Item for evaluation
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    private function fillAjaxFormItem($item)
    {
        /*---------------------------------------------------------------
        /*	Variable Declaration
        /*-------------------------------------------------------------*/
        $resp['sts']    = STS_NG;
        $resp['msg']    = STR_EMPTY;
        
        /*---------------------------------------------------------------
        /*	Click on an item in the Questionnaire
        /*-------------------------------------------------------------*/
        $form_item      = $this->webdriver->findElement(WebDriverBy::xpath("//span[@data-name='".$item[0]."']"));
        
        $actions        = new WebDriverActions($this->webdriver);
        $actions->moveToElement($form_item);
        $actions->perform();
        
        $this->webdriver->executeScript('$("[data-name='."'".$item[0]."'".']").click()');
        // Item[0] is the name of span
        
        /*---------------------------------------------------------------
        /*	Wait for the Form to appear
        /*-------------------------------------------------------------*/
        $this->webdriver->wait(30, 500)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('editable-field-form'))
        );
        
        /*---------------------------------------------------------------
        /*	Find the Form
        /*-------------------------------------------------------------*/
        $form   = $this->webdriver->findElement(WebDriverBy::id('editable-field-form'));
        
        /*---------------------------------------------------------------
        /*	Fill the form with expected invalid inputs
        /*-------------------------------------------------------------*/
        foreach ($item[1] as $error) {
            /*---------------------------------------------------------------
            /*	Fill the item with Inputs and submit the form
            /*-------------------------------------------------------------*/
            $field      = $form->findElement(WebDriverBy::cssSelector('.edit-input-field'));
            //$this->webdriver->executeScript('$("#'.$form[0][0].'").click()');
            
            $tag_name   = $field->getTagName();
            
            if ($tag_name !== 'select') {
                $field  = $field->clear();
                $field  = $field->sendKeys($error[0]);
            }
            else {
                
                $field  = new WebDriverSelect($form->findElement(WebDriverBy::cssSelector('.edit-input-field')));
                $field->selectByVisibleText($error[0]);
            }
            // Error[0] is data inputted
            
            if (isset($error[1])) {
                /** Submit the item                 */
                $form->submit();
                
                /** Wait for AJAX to complete       */
                $this->webdriver->wait(30, 500)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('editable-error-msg'))
                );
                
                /** Get Error message       */
                $err_msg = $this->webdriver->findElement(WebDriverBy::className('editable-error-msg'));
                
                
                /** Error message is as expected    */
                if (preg_replace('/\s+/', '', $err_msg->getText()) == preg_replace('/\s+/', '', $error[1])) {
                    $resp['sts']    = STS_OK;
                }
                /** Invalid Error Message           */
                else {
                    $resp['sts']    = STS_NG;
                    $resp['msg']    = 'ERROR: '.trim($err_msg->getText()).' does not match '.trim($error[1]); // Error[1] is expected message
                    break;
                }
            }
            else {
                $resp['sts']    = STS_OK;
            }
        }
        
        /*---------------------------------------------------------------
        /*	Fill the form with the valid input
        /*-------------------------------------------------------------*/
        if (STS_OK == $resp['sts']) {
            $field      = $form->findElement(WebDriverBy::cssSelector('.edit-input-field'));
            $tag_name   = $field->getTagName();
        
            if ($tag_name !== 'select') {
                $field  = $field->clear();
                $field  = $field->sendKeys($item[2][0]);
            }
            else {
                $field  = new WebDriverSelect($field);
                $field->selectByVisibleText($item[2][0]);
            }
            // Item[2] is the valid input
            
            $form->submit();
            
            /** Wait for AJAX to complete       */
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::invisibilityOfElementLocated(WebDriverBy::className("edit-input-cntr"))
            );
            
            /** Item has been changed as expected    */
            if ($form_item->getText() == $item[2][0]) {
                $resp['sts']    = STS_OK;
            }
            /** Invalid Error Message           */
            else {
                $resp['msg']    = 'ERROR: '.$form_item->getText().' does not match '.$item[2][0]; // Error[1] is expected message
            }
        }
        
        /*	関数終亁    */
        return $resp;
    }
    
    /*-------------------------------------------------------------------------
	|	Fill an item on the AJAX Form
	|--------------------------------------------------------------------------
	|	@param [in] 	item    --- Item for evaluation
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillExpanderForm($form)
    {
        /*---------------------------------------------------------------
        /*	Variable Declaration
        /*-------------------------------------------------------------*/
        $resp['sts']    = STS_NG;
        $resp['msg']    = STR_EMPTY;
        
        /*---------------------------------------------------------------
        /*	Click the button to open the Form
        /*-------------------------------------------------------------*/
        $this->webdriver->executeScript('$("#'.$form[0][0].'").click()');
        
        /*---------------------------------------------------------------
        /*	Wait for the Form to appear
        /*-------------------------------------------------------------*/
        $this->webdriver->wait(30, 500)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id($form[0][1]))
        );
        
        /*---------------------------------------------------------------
        /*	Find the Form
        /*-------------------------------------------------------------*/
        $ui_form   = $this->webdriver->findElement(WebDriverBy::id($form[0][1]));
        
        /*---------------------------------------------------------------
        /*	Fill the form with expected invalid inputs
        /*-------------------------------------------------------------*/
        foreach ($form[1][1] as $invalid_data) {
            
            for ($field = 0; $field < @count($form[1][0]); $field++) {
                $item   = $ui_form->findElement(WebDriverBy::name($form[1][0][$field]))->sendKeys($invalid_data[0][$field]);
            }
            
            /** Submit the item                 */
            $ui_form->submit();
            
            /** Wait for AJAX to complete       */
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::className('editable-error-msg'))
            );
            
            /** Get Error message       */
            $err_msg = $this->webdriver->findElement(WebDriverBy::className('editable-error-msg'));
        }
        
        /*	関数終亁    */
        return $resp;
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Business Details 1 Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillBusinessDetails1()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        $this->fillEditForms($this->tb_biz_details_itm);
        
        if (2 != $config->affiliates) {
            $this->fillEditForms($this->tb_affliates_itm);
        }
        
        if (2 != $config->products) {
            $this->fillEditForms($this->tb_products_itm);
        }
        
        if ((2 != $config->capital_details)
        && (0 == $entity->entity_type)){
            $this->fillEditForms($this->tb_capital_itm);
        }
        
        if (2 != $config->main_locations) {
            $this->fillEditForms($this->tb_main_loc_itm);
        }
        
        if (2 != $config->branches) {
            $this->fillEditForms($this->tb_branches_itm);
        }
        
        $this->fillEditForms($this->tb_certifications_itm);
        
        if (2 != $config->insurance) {
            $this->fillEditForms($this->tb_insurance_itm);
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Business Details 2 Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillBusinessDetails2()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_registration2"))->click();
        
        /*---------------------------------------------------------------
        /*	Wait for the Form to appear
        /*-------------------------------------------------------------*/
        $this->webdriver->wait(30, 500)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('tab-msg-warning'))
        );
        
        $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
        $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
        
        if (2 != $config->major_customer) {
            $this->fillEditForms($this->tb_major_cust_itm);
        }
        
        if (2 != $config->major_supplier) {
            $this->fillEditForms($this->tb_major_supp_itm);
        }
        
        if (2 != $config->major_market) {
            $this->fillEditForms($this->tb_major_loc_itm);
        }
        
        if (2 != $config->competitors) {
            $this->fillEditForms($this->tb_competitors_itm);
        }
        
        if (2 != $config->business_drivers) {
            $this->fillEditForms($this->tb_biz_drivr_itm);
        }
        
        if (2 != $config->customer_segments) {
            $this->fillEditForms($this->tb_biz_segment_itm);
        }
        
        if (2 != $config->past_projects) {
            $this->fillEditForms($this->tb_past_proj_itm);
        }
        
        if (2 != $config->future_growth) {
            $this->fillEditForms($this->tb_future_init_itm);
        }

    }
    
    /*-------------------------------------------------------------------------
	|	Fill Condition and Sustainability Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillCns()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_bcs"))->click();
        
        /*---------------------------------------------------------------
        /*	Wait for the Form to appear
        /*-------------------------------------------------------------*/
        $this->webdriver->wait(30, 500)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name("confirm-tab-msg"))
        );
        
        $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
        $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
        
        if (2 != $config->company_succession) {
            $this->fillEditForms($this->tb_succession_itm);
        }
        
        if (2 != $config->landscape) {
            $this->fillEditForms($this->tb_landscape_itm);
        }
        
        if (2 != $config->economic_factors) {
            $this->fillEditForms($this->tb_eco_factors_itm);
        }
        
        if (2 != $config->tax_payments) {
            $this->fillEditForms($this->tb_tax_payments_itm);
        }
        
        if (2 != $config->risk_assessment) {
            $this->fillEditForms($this->tb_risk_payments_itm);
        }
        
        if (2 != $config->growth_potential) {
            $this->fillEditForms($this->tb_grwt_potential_itm);
        }
        
        if (2 != $config->investment_expansion) {
            $this->fillEditForms($this->tb_cap_expansion_itm);
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Key Managers Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillKeyManagers()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        if (0 == $entity->entity_type) {
            $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_ownerdetails"))->click();
            
            /*---------------------------------------------------------------
            /*	Wait for the Form to appear
            /*-------------------------------------------------------------*/
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name("confirm-tab-msg"))
            );
            
            $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
            $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
                
            $this->webdriver->executeScript('$("a[data-parent='."'#accordion'".']").click()');
            
            if (2 != $config->owner_details) {
                $this->fillEditForms($this->tb_keymanagers_itm);
            }
            
            if (2 != $config->shareholders) {
                $this->fillEditForms($this->tb_shareholders_itm);
            }
            
            if (2 != $config->directors) {
                $this->fillEditForms($this->tb_keymanagers_itm);
            }
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Owner Details Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillOwnerDetails()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        if (1 == $entity->entity_type) {
            $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_ownerdetails"))->click();
            
            /*---------------------------------------------------------------
            /*	Wait for the Form to appear
            /*-------------------------------------------------------------*/
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name("confirm-tab-msg"))
            );
            
            $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
            $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
            
            $this->webdriver->executeScript('$("a[data-parent='."'#accordion'".']").click()');
            $this->webdriver->executeScript('$("button:contains('."'Owner Details'".')").click()');
            $this->webdriver->executeScript('$("button:contains('."'Key Manager'".')").click()');
            $this->webdriver->executeScript('$("button:contains('."'Other Business Currently Managed/Owned'".')").click()');
            $this->webdriver->executeScript('$("button:contains('."'Educational Background'".')").click()');
            $this->webdriver->executeScript('$("button:contains('."'Spouse Personal Details'".')").click()');
            
            if (2 != $config->owner_details) {
                $this->fillEditForms($this->tb_owner_itm);
                $this->fillEditForms($this->tb_keymanagers_itm);
            }
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Existing Credit Facilities Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillEcf()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        if (2 != $config->ecf) {
            $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_payfactor"))->click();
            
            /*---------------------------------------------------------------
            /*	Wait for the Form to appear
            /*-------------------------------------------------------------*/
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name("confirm-tab-msg"))
            );
            
            $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
            $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
        
            $this->fillEditForms($this->tb_ecf_itm);
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Required Credit Line Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillRcl()
    {
        $entity     = Entity::find($this->entity_id)->first();
        $config_db  = new QuestionnaireConfig;
        $config     = $config_db->getQuestionnaireConfigbyBankID($entity->current_bank);
        
        if (2 != $config->rcl) {
            $biz2_tab       = $this->webdriver->findElement(WebDriverBy::id("nav_pfs"))->click();
            
            /*---------------------------------------------------------------
            /*	Wait for the Form to appear
            /*-------------------------------------------------------------*/
            $this->webdriver->wait(30, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::name("confirm-tab-msg"))
            );
            
            $this->webdriver->findElement(WebDriverBy::name("confirm-tab-msg"))->click();
            $this->webdriver->executeScript('$("#modal-next-tab-continue").click()');
            
            $this->fillEditForms($this->tb_rcl_itm);
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Edit Forms according to the table
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function fillEditForms($tb_itm)
    {
        foreach ($tb_itm as $item) {
            $valid_check = $this->fillAjaxFormItem($item);
            
            if (STS_NG == $valid_check['sts']) {
                echo $valid_check['msg'];
                break;
            }
        }
        
        return $valid_check;
    }
    
    /*-------------------------------------------------------------------------
	|	Fill Business Details 1 Forms
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		resp    --- AJAX form response status
	|------------------------------------------------------------------------*/
    public function addBusinessDetails1Data()
    {
        foreach ($this->tb_biz_details1_form as $form) {
            $valid_check = $this->fillExpanderForm($form);
            
            if (STS_NG == $valid_check['sts']) {
                echo $valid_check['msg'];
                break;
            }
        }
    }
    
    /*-------------------------------------------------------------------------
	|	Selenium Test Configuration Destroy Session
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		NONE
	|------------------------------------------------------------------------*/
    public function destructSeleniumWebdrvr()
    {
        $this->webdriver->quit();
    }
    
    /*-------------------------------------------------------------------------
	|	UI to set Username, Password, Report and Browser
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		NONE
	|------------------------------------------------------------------------*/
    public function setSeleniumWebdrvr()
    {        
        return View::make('selenium-set-user'
        
        );
    }
    
    /*-------------------------------------------------------------------------
	|	Verify inputs for Automated Testing
	|--------------------------------------------------------------------------
	|	@param [in] 	NONE
	|	@param [out] 	NONE
	|	@return 		NONE
	|------------------------------------------------------------------------*/
    public function execSeleniumWebdrvr()
    {        
        $rules = array(
            'username'  => 'required',
            'password'  => 'required',
            'report_id' => 'required',
            'browser'   => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
            return Redirect::to('/selenium_test')->withErrors($validator)->withInput();
		}
        else {
            $this->initSeleniumWebdrvr();
        }
    }
}