<?php

//======================================================================
//  Class 55: Webservices Controller
//      Functions related to Javascript Callback are routed to this class
//======================================================================
class WebServiceController extends BaseController
{
    //-----------------------------------------------------
    //  Function 55.1: __construct
    //      Initialization of Class
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing    */
    }
    
    //-----------------------------------------------------
    //  Function 55.2: getWebService
    //      Responds with the Industries and Provinces
    //      according to request
    //-----------------------------------------------------
    public function getWebService()
    {
        /*  Variable Declaration    */
        $json_data  = CONT_NULL;
        $method     = htmlentities(Input::get('action'));
        $callback   = htmlentities(Input::get('callback'));
        
        /*  Retrieves all Industries (Tier 1)   */
        if ('getIndustries' == $method) {
            $json_data  = Industrymain::select('main_title', 'industry_score', 'industry_main_id')->get();
            
            /** Set points according to Industry Outlook    */
            foreach ($json_data as $data) {
                if (BIZ_OUTLOOK_UP_POINT == $data['industry_score']) {
                    $data['industry_points']    = 60;
                }
                else if (BIZ_OUTLOOK_FLAT_POINT == $data['industry_score']) {
                    $data['industry_points']    = 42;
                }
                else if (BIZ_OUTLOOK_DOWN_POINT == $data['industry_score']) {
                    $data['industry_points']    = 24;
                }
                else {
                    /** No Processing   */
                }
            }
        }
        /*  Retrieves all Provinces */
        else if ('getProvinces' == $method) {
            $json_data  = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
        }
        else {
            
        }
        
        /*  Encode server response variable to JSON for output  */
        echo $callback.'('.json_encode($json_data).');';
    }
    
    //-----------------------------------------------------
    //  Function 55.3: WebCalcFacebookLike
    //      Responds with the Like status of a Facebook User
    //-----------------------------------------------------
    public function WebCalcFacebookLike()
    {
        /*  Variable Declaration    */
        $json_data  = CONT_NULL;
        $fb_user_id = htmlentities(Input::get('fbid'));
        $method     = htmlentities(Input::get('action'));
        $callback   = htmlentities(Input::get('callback'));
        
        $fb_like_db = new WebCalculatorLike;
        
        /*  Retrieves all FB Like according to FBID */
        if ('getFbLike' == $method) {
            $json_data  = $fb_like_db->getUserFbLike($fb_user_id);
            
            $data_count = @count($json_data);
            
            /** User already liked the page     */
            if (1 <= $data_count) {
                $json_data['valid'] = STS_OK;
            }
            /** User did not like the page yet  */
            else {
                $json_data['valid'] = STS_NG;
            }

        }
        /*  Saves the User's FBID to the Database   */
        else if ('addFbLike' == $method) {
            $add_sts  = $fb_like_db->addCalculatorFbLike($fb_user_id);
            
            /** FBID saved to Database successfully */
            if (STS_OK == $add_sts) {
                $json_data['valid'] = STS_OK;
            }
            /** FBID failed to save to Database     */
            else {
                $json_data['valid'] = STS_NG;
            }    
        }
        else {
            /** No Processing   */
        }
        
        /*  Encode server response variable to JSON for output  */
        echo $callback.'('.json_encode($json_data).');';
    }
    
    //-----------------------------------------------------
    //  Function 55.4: testGoogleQuickstart1
    //      
    //-----------------------------------------------------
    public function testGoogleQuickstart1()
    {
        $googleClient = Google::getClient();
        $googleClient->setScopes(array("https://www.googleapis.com/auth/drive.readonly", "https://www.googleapis.com/auth/spreadsheets.readonly", "https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/spreadsheets"));
        $googleClient->setAccessType("offline");
        
        $sheets = Google::make('sheets');
        
        //print_r($sheets);
        //return 1;
        // Prints the names and majors of students in a sample spreadsheet:
        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
        $spreadsheetId  = '1Xq_56nSlkzCnvXVmmTzuk50dKOs0zCs6xWRB7ovJZbU';
        $range          = "Sheet1!A1:E";
        $response       = $sheets->spreadsheets_values->get($spreadsheetId, $range);
        $values         = $response->getValues();
        
        if (@count($values) == 0) {
            print "No data found.\n";
        }
        else {
            //print "Name, Major:\n";
            
            foreach ($values as $row) {
                // Print columns A and E, which correspond to indices 0 and 4.
                //printf("%s", $row[0]);
                print_r($row);
                echo '<br/><br/>';
            }
        }
    }
    
    //-----------------------------------------------------
    //  Function 55.5: getAutoUpdateParse 
	//  Description: function to process downloaded bsp for auto update
    //-----------------------------------------------------
	public function convertFrPdftoText()
	{
		$filename  = 'Colorado.pdf';
		exec('pdftotext -eol dos -layout images/'.$filename.' temp/fr_converted.txt');
		
		// variable declaration
		$start          = false;				// used for flagging in while loop
		$start_data     = false;		// used for flagging in while loop
		$start_data_1   = false;		// used for flagging in while loop
		$positive   = false;		// used for flagging in while loop
		$negative   = false;		// used for flagging in while loop
		$start_now   = false;		// used for flagging in while loop
		$positive_arr   = array();		// used for flagging in while loop
		$negative_arr   = array();		// used for flagging in while loop
		$parsed_array   = array();
		$start_cntr   = 0;
		$handle         = fopen('temp/fr_converted.txt', "r");
        $line_p = STR_EMPTY;
        $start_line = 0;
        
		if ($handle) {
            
			while (($line = fgets($handle)) !== false) {
				
				if (FALSE !== strpos($line, '3.1. Key Ratios Summary')) {
                    $start_cntr++;
                    if (2 <= $start_cntr) {
                        $start = true;
                    }
				}
                
				if (($start)
                && (false == $start_data)) {
                    
                    if ((FALSE !== stripos($line, 'bad'))
                    || (FALSE !== stripos($line, 'negative'))
                    || (FALSE !== stripos($line, 'critical'))
                    || (FALSE !== stripos($line, 'unacceptable'))
                    || (FALSE !== stripos($line, 'fail'))
                    || (FALSE !== stripos($line, 'abnormal'))
                    || (FALSE !== stripos($line, 'unsatisfactory'))) {
                        $positive = false;
                        $negative = true;
                    }
                    else if ((FALSE !== stripos($line, 'good'))
                    || (FALSE !== stripos($line, 'positive'))
                    || (FALSE !== stripos($line, 'acceptable'))
                    || (FALSE !== stripos($line, 'excellent'))
                    || (FALSE !== stripos($line, 'high'))
                    || (FALSE !== stripos($line, 'normal'))
                    || (FALSE !== stripos($line, 'outstanding'))
                    || (FALSE !== stripos($line, 'success'))
                    || (FALSE !== stripos($line, 'satisfactory'))) {
                        $positive = true;
                        $negative = false;
                    }

					if (FALSE !== strpos($line, ':')) {
						$start_data = true;
					}
				}
                
				if ($start_data) {
                    
                    if (FALSE !== strpos($line, '●')) {
                        
                        if (1 == $start_line) {
                            
                            $start_line = 2;                            
                        }
                        else {
                            $start_line = 1;
                        }
                        
                    }
                    
                    if (1 == $start_line) {
                        $line_p .= trim(str_replace('<br />', '', nl2br($line))).' ';
                    }
                    else if (2 == $start_line) {
                        if ($positive) {
                            $positive_arr[]   = $line_p;
                        }
                        else {
                            $negative_arr[]   = $line_p;
                        }
                        
                        $line_p     = $line;
                        $start_line = 1;
                    }
                    
                    if (('<br />' == trim(nl2br($line)))
                    && (1 == $start_line)) {
                        
                        if ($positive) {
                            $positive_arr[]   = $line_p;
                        }
                        else {
                            $negative_arr[]   = $line_p;
                        }
                        
                        $line_p         = STR_EMPTY;
                        $start_line     = 0;
                        $start_data     = false;
                    }
				}
            
				if ($start && strpos($line, '3.2.')!==false) {
					break;
				}
			}

			fclose($handle); // close file
		}
        else {
			// error opening the file.
		}
        
        echo 'Positive<br/>';
        foreach ($positive_arr as $parsed) {
            $parsed_clean   = trim($parsed, "\x00..\x1F");
            $parsed_clean   = trim($parsed_clean, '●');
            $parsed_clean   = trim($parsed_clean);
            $parsed_clean   = ucfirst($parsed_clean);
            echo $parsed_clean.'<br/>';
        }
        
        echo 'Negative<br/>';
        foreach ($negative_arr as $parsed) {
            $parsed_clean   = trim($parsed, "\x00..\x1F");
            $parsed_clean   = trim($parsed_clean, '●');
            $parsed_clean   = trim($parsed_clean);
            $parsed_clean   = ucfirst($parsed_clean);
            echo $parsed_clean.'<br/>';
        }
    }

    //-----------------------------------------------------
    //  Function 55.6: getCalculatorService 
	//  Description: function to show Credit Score Calculator
    //-----------------------------------------------------
    public function getCalculatorService() {
        return View::make('webservice.credit_score_calculator');
    }
    
}