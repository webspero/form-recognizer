<?php

//======================================================================
//  Class 59: RScriptController Controller
//      Functions related to R Scripts
//======================================================================
class RScriptController extends BaseController
{
    //-----------------------------------------------------
    //  Function 59.1: __construct
    //      Initialization of Class
    //-----------------------------------------------------
    public function __construct()
    {
        /** No Processing    */
    }
    
    //-----------------------------------------------------
    //  Function 59.2: testRscript
    //      Responds with the Industries and Provinces
    //      according to request
    //-----------------------------------------------------
    public function testRscript()
    {
        $rscript_path   = public_path('temp/testR.R');
        $N              = 1;
        $a              = 1;
        $b              = 2;
        
        exec("\"C:\\Program Files\\R\\R-3.3.2\\bin\\Rscript.exe\" temp/testR.R $N 2>&1", $response);
        print_r($response);
        //$str = $response[0];
        //$myobj = json_decode($str);

        //echo $myobj->first_name;
    }
}