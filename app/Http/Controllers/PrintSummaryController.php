<?php

use CreditBPO\Handlers\SMEInternalHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use CreditBPO\Services\MailHandler;

//======================================================================
//  Class 43: Print Summary Controller
//      Functions related to the printed PDF are routed to this class
//======================================================================
class PrintSummaryController extends BaseController
{
    //-----------------------------------------------------
    //  Function 43.1: getSMEReport
    //      Displays the CBPO PDF Report
    //          (Currently not in use)
    //-----------------------------------------------------
	public function getSMEReport($entity_id)
	{
		try{
	        $entity = DB::table('tblentity')
	            ->where('entityid', '=', $entity_id)
	            ->first();

	        $dscr_standalone = App::make('ProfileSMEController')->calculateStaticDscr($entity_id);

	        $pdf = PDF::loadView('summary.print-dscr-result',
	            array(
	                'dscr_standalone'   => $dscr_standalone,
	                'entity'            => $entity,
	            )
	        );

	        return $pdf->stream();
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
    
    //-----------------------------------------------------
    //  Function 43.2: getPDFTest
    //      Displays the PDF
    //-----------------------------------------------------
	public function getPDFTest()
	{
		try{
			$user_id = Auth::user()->loginid;
			$entity = DB::table('tblentity')
	            ->where('loginid', '=', $user_id)
	            ->first();

	        $industry = DB::table('tblindustry_main')
	            ->where('industry_main_id', $entity->industry_main_id)
	            ->first();

	        $finalscore = DB::table('tblsmescore')
				->where('entityid', '=', $entity->entityid)
				->first();

	        $readyratiodata = DB::table('tblreadyrationdata')
				->where('entityid', '=', $entity->entityid)
				->first();

			$businessdriver = DB::table('tblbusinessdriver')
	            ->where('loginid', $user_id)
	            ->get();

	        $revenuepotential = DB::table('tblrevenuegrowthpotential')
				->where('loginid', '=', $user_id)
				->get();

	        $bizsegment = DB::table('tblbusinesssegment')
	            ->where('loginid', $user_id)
	            ->get();

	        $past_projs = DB::table('tblprojectcompleted')
	            ->where('loginid', $user_id)
	            ->get();

	        $future_growths  = DB::table('tblfuturegrowth')
	            ->where('loginid', $user_id)
	            ->get();

	        $planfacility   = DB::table('tblplanfacilityrequested')
				->where('loginid', '=', $user_id)
				->first();

			/* return View::make('summary.print-rating-summary',
	            array(
	                'entity'            => $entity,
	                'industry'          => $industry,
	                'finalscore'        => $finalscore,
	                'readyratiodata'    => $readyratiodata,

	                'businessdriver'    => $businessdriver,
	                'revenuepotential'  => $revenuepotential,
	                'bizsegment'        => $bizsegment,
	                'past_projs'        => $past_projs,
	                'future_growths'    => $future_growths,
	                'planfacility'      => $planfacility,
	            )
	        );
			//*/

			$pdf = PDF::loadView('summary.print-rating-summary',
	            array(
	                'date_created'      => '',
	                'entity'            => $entity,
	                'industry'          => $industry,
	                'finalscore'        => $finalscore,
	                'readyratiodata'    => $readyratiodata,

	                'businessdriver'    => $businessdriver,
	                'revenuepotential'  => $revenuepotential,
	                'bizsegment'        => $bizsegment,
	                'past_projs'        => $past_projs,
	                'future_growths'    => $future_growths,
	                'planfacility'      => $planfacility,
	            ));

			
			return $pdf->save('temp/sample.pdf')->stream('sample.pdf');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 43.3: chmodR
    //      Change file and folders to writable
    //-----------------------------------------------------
	public function chmodR($Path) {

		try{
		    $dp = opendir($Path);
		     while($File = readdir($dp)) {
		       if($File != "." AND $File != "..") {
		         if(is_dir($File)){
		            chmod($File, 0777);
		            chmod_r($Path."/".$File);
		         }else{
		             chmod($Path."/".$File, 0777);
		         }
		       }
		     }
		   closedir($dp);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 43.3: showCbpoPdf
    //      Displays the CBPO PDF Report on the browser
    //-----------------------------------------------------
    public function showCbpoPdf($entity_id)
    {
    	try{
    		
	        /*  Fetch the report information from the database  */
	        $lang = app()->getLocale();
	        $entity_data    = Entity::where('entityid', $entity_id)->first();

	        //Self::chmodR(public_path('images/rating-report-graphs'));

	        if(empty($entity_data->cbpo_pdf)){

		        $SME = new SMEInternalHandler();
		        $file = $SME->createSMEInternalReport($entity_id);

		        $pdfMerge = new PDFMerger();        

		        if ($file) {

		        	if($entity_data->is_premium != SIMPLIFIED_REPORT){
			            if($entity_data->financial_analysis_pdf){
			                $fname = $entity_data->financial_analysis_pdf;
			                if(strpos($fname,"&") !== false){
			                    $fname  = preg_replace("/[&]/", "And", $fname);
			                }
			                $finAnFile = public_path().'/financial_analysis/'.$fname;
			        
			                $pdfMerge->addPDF($file, 'all');
			                $pdfMerge->addPDF($finAnFile);
			        
			                $pdfMerge->merge('file', $file);

			                // Entity::where('entityid',$entity_id)->update(['cbpo_pdf'=>basename($file)]);
			            }else{
			                $financialAnalysis = new FinancialAnalysisInternalHandler();
			                $financial_report_id = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])->pluck('id')[0];
			                $fr = FinancialReport::find($financial_report_id);
			                $frfile = $financialAnalysis->createFinancialAnalysisInternalReport($financial_report_id,$lang);
			                $fname = basename($frfile);

			                if($lang == "fil"){
			                    Entity::where('entityid',$entity_id)->update(['financial_analysis_pdf_tl'=>basename($frfile)]);
			                }else{
			                    Entity::where('entityid',$entity_id)->update(['financial_analysis_pdf'=>basename($frfile)]);
			                }   

			                $finAnFile = public_path().'/financial_analysis/'.$fname;
			        
			                $pdfMerge->addPDF($file, 'all');
			                $pdfMerge->addPDF($finAnFile);
			        
			                $pdfMerge->merge('file', $file);

			                // Entity::where('entityid',$entity_id)->update(['cbpo_pdf'=>basename($file)]);
			            }

		        	}

		        	Entity::where('entityid',$entity_id)->update(['cbpo_pdf'=>basename($file)]);

		            $filepath = public_path().'/documents/'.basename($file);
		        }
	    	}else{
	    		$filepath = public_path().'/documents/'.basename($entity_data->cbpo_pdf);
	    	}

	        $newfile = File::get($filepath);

	        $response = Response::make($newfile, 200);

	        /*  Sets header to PDF  */
	        $response->header('Content-Type', 'application/pdf');

	        return $response;
	    }catch(Exception $ex){

			$data = array(
				"entity_id" => $entity_id,
				"data" => '',
				"error" => $ex->getMessage(),
				"status" => 1,
				"is_deleted" => 0,
				"created_at" => date('Y-m-d H:i:s'),
				"updated_at" => date('Y-m-d H:i:s')
			);

			$mail = new MailHandler;
			$mail->sendAutomateError($data);


			if(env("APP_ENV") == "Production"){

				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

}