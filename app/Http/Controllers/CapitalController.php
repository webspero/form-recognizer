<?php
//======================================================================
//  Class 10: CapitalController
//  Description: Contains functions regarding Capital Details
//======================================================================
class CapitalController extends BaseController {

	//-----------------------------------------------------
    //  Function 10.1: postCapital
	//  Description: get Capital details page 
    //-----------------------------------------------------
	public function getCapital($entity_id)
	{
		return View::make('sme.capital',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	
	//-----------------------------------------------------
    //  Function 10.2: postCapital
	//  Description: function for updating Capital details
    //-----------------------------------------------------
	public function postCapital($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $messages       = array();
        $rules          = array();						// variable container
        
        $security_lib   = new MaliciousAttemptLib;		// instantiate class MaliciousAttemptLib
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
		$srv_resp['messages']       = STR_EMPTY;
		
		$entity = Entity::find($entity_id);
        
        if (isset(Auth::user()->loginid)) {
			$srv_resp['action']         = REFRESH_CNTR_ACT;
			$srv_resp['refresh_cntr']   = '#cap-details-list-cntr';
			$srv_resp['refresh_elems']  = '.reload-cap-details';

			if($entity->is_premium == PREMIUM_REPORT) {
				$srv_resp['refresh_url']    = URL::to('premium-link/summary/'.$entity_id);
			} else {
				$srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
			}
            
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		$messages = array(		// validation massages
			'capital_authorized.required'	=> trans('validation.required', array('attribute'=>trans('business_details1.authorized_capital'))),
			'capital_authorized.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details1.authorized_capital'))),
			'capital_authorized.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details1.authorized_capital'))),
			'capital_issued.required'	=> trans('validation.required', array('attribute'=>trans('business_details1.issued_capital'))),
			'capital_issued.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details1.issued_capital'))),
			'capital_issued.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details1.issued_capital'))),
			'capital_paid_up.required'	=> trans('validation.required', array('attribute'=>trans('business_details1.paid_up_capital'))),
			'capital_paid_up.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details1.paid_up_capital'))),
			'capital_paid_up.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details1.paid_up_capital'))),
			'capital_ordinary_shares.required'	=> trans('validation.required', array('attribute'=>trans('business_details1.ordinary_shares'))),
			'capital_ordinary_shares.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details1.ordinary_shares'))),
			'capital_ordinary_shares.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details1.ordinary_shares'))),
			'capital_par_value.required'	=> trans('validation.required', array('attribute'=>trans('business_details1.par_value'))),
			'capital_par_value.numeric'	=> trans('validation.numeric', array('attribute'=>trans('business_details1.par_value'))),
			'capital_par_value.max'	=> trans('validation.max.numeric', array('attribute'=>trans('business_details1.par_value'))),
		);
		if($entity->is_premium == PREMIUM_REPORT) {
			$rules = array(			// validation rules
				'capital_authorized'	=> 'required|numeric|max:9999999999999.99',
				'capital_issued'	=> 'sometimes|nullable|numeric|max:9999999999999.99',
				'capital_paid_up'	=> 'sometimes|nullable|numeric|max:9999999999999.99',
				'capital_ordinary_shares'	=> 'sometimes|nullable|numeric|max:9999999999999.99',
				'capital_par_value'	=> 'sometimes|nullable|numeric|max:9999999999999.99',
			);
		} else {
			$rules = array(			// validation rules
				'capital_authorized'	=> 'required|numeric|max:9999999999999.99',
				'capital_issued'	=> 'required|numeric|max:9999999999999.99',
				'capital_paid_up'	=> 'required|numeric|max:9999999999999.99',
				'capital_ordinary_shares'	=> 'required|numeric|max:9999999999999.99',
				'capital_par_value'	=> 'required|numeric|max:9999999999999.99',
			);
		}
		

		
	
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else
		{	
			$capital = Capital::firstOrNew(	// update if found, create if not
                array(
					'entity_id' => $entity_id,
					'is_deleted' => 0
                )
            );
			
			// populate data values
			$capital->entity_id = $entity_id;
			$capital->capital_authorized = Input::get('capital_authorized');
			if (request('capital_issued') != null) {
				$capital->capital_issued = Input::get('capital_issued');
			}
			if (request('capital_paid_up') != null) {
				$capital->capital_paid_up = Input::get('capital_paid_up');
			}
			if (request('capital_ordinary_shares') != null) {
				$capital->capital_ordinary_shares = Input::get('capital_ordinary_shares');
			}
			if (request('capital_par_value') != null) {
				$capital->capital_par_value = Input::get('capital_par_value');
			}
			
			if($capital->save()) { // save
				$srv_resp['sts']        = STS_OK;
                $srv_resp['messages']   = 'Successfully Added Record';
                
                if (!isset(Auth::user()->loginid)) {
                    $srv_resp['master_ids']     = $capital->capitalid;
                }
			}
			else {
				$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.$entity_id.'">Please click here to return</a>.';
			}
		}
		  ActivityLog::saveLog();				// log activity in db
        return json_encode($srv_resp);		// return json results
	}

	//-----------------------------------------------------
    //  Function 10.3: getCapitalDelete
	//  Description: function for deleting Capital Details
    //-----------------------------------------------------
	public function getCapitalDelete($id)
	{
		$capital = Capital::where('capitalid', '=', $id)
					->where('entity_id', '=', Session::get('entity')->entityid)
					->first();
		
		$capital->is_deleted = 1;
		$capital->save();

        ActivityLog::saveLog();
		Session::flash('redirect_tab', 'tab1');
		$entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}

	//-----------------------------------------------------
    //  Function 10.4: getCapitalPremium
	//  Description: get Premium Capital details page 
    //-----------------------------------------------------
	public function getCapitalPremium($entity_id)
	{
		return View::make('report.capital_details_premium',
            array(
                'entity_id' => $entity_id
            )
        );
	}
}