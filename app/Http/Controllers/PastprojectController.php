<?php

//======================================================================
//  Class 41: Past Project Controller
//      Functions related to Past Project on the Questionnaire
//      are routed to this class
//======================================================================
class PastprojectController extends BaseController
{
    //-----------------------------------------------------
    //  Function 41.1: getPastproject
    //      Displays the Past Project Form
    //-----------------------------------------------------
	public function getPastproject($entity_id)
	{
		return View::make('sme.pastprojectscompleted',
            array(
                'entity_id' => $entity_id
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 41.2: postPastproject
    //      HTTP Post request to validate and save Past
    //      Projects data in the database
    //-----------------------------------------------------
	public function postPastproject($entity_id)
	{
        /*  Variable Declaration    */        
        $proj_names     = Input::get('projectcompleted_name');  // Past Project Name
        $messages       = array();                              // Validation Messages according to rules
        $rules          = array();                              // Validation Rules
        $year_now       = (int)date('Y');                       // Current Year
        
        $security_lib   = new MaliciousAttemptLib;              // Instance of the Malicious Attempt Library
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#past-proj-list-cntr';                       // Refresh the specified container
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            $srv_resp['refresh_elems']  = '.reload-past-proj';                          // Elements that will be refreshed
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($proj_names); $idx++) {
            
            /*  Always validate the first input set and 
            *   for the succeeding validate those with Past Project name
            */
            if ((STR_EMPTY != $proj_names[$idx])
            || (0 == $idx)) {
                
                $messages['projectcompleted_name.'.$idx.'.required']            = trans('validation.required', array('attribute'=>trans('business_details2.ppc_purpose')));
                $messages['projectcompleted_cost.'.$idx.'.required']            = trans('validation.required', array('attribute'=>trans('business_details2.ppc_cost')));
                $messages['projectcompleted_cost.'.$idx.'.numeric']             = trans('validation.numeric', array('attribute'=>trans('business_details2.ppc_cost')));
                $messages['projectcompleted_cost.'.$idx.'.max']                 = trans('validation.max.numeric', array('attribute'=>trans('business_details2.ppc_cost')));
                $messages['projectcompleted_source_funding.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details2.ppc_source')));
                $messages['projectcompleted_year_began.'.$idx.'.required']      = trans('validation.required', array('attribute'=>trans('business_details2.ppc_year')));
                $messages['projectcompleted_year_began.'.$idx.'.date_format']   = 'Invalid year format';
                $messages['projectcompleted_year_began.'.$idx.'.after']         = 'Year cannot be lower than 1970';
                $messages['projectcompleted_result.'.$idx.'.required']          = trans('validation.required', array('attribute'=>trans('business_details2.ppc_result')));

                $rules['projectcompleted_name.'.$idx]           = 'required';
                $rules['projectcompleted_cost.'.$idx]           = 'required|numeric|max:9999999999999.99';
                $rules['projectcompleted_source_funding.'.$idx]	= 'required';
                $rules['projectcompleted_year_began.'.$idx]     = 'required|date_format:"Y"|after:1969';
                $rules['projectcompleted_result.'.$idx]         = 'required';
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator  = Validator::make(Input::all(), $rules, $messages);
        
        /*  Stores all inputs from the form to an array for saving and looping  */
        $arraydata  = Input::only('projectcompleted_name', 'projectcompleted_cost', 'projectcompleted_source_funding', 'projectcompleted_year_began', 'projectcompleted_result');
        
        /*  Get the Year Began and converts it to Integer to be compared with current year   */
        if (isset($arraydata['projectcompleted_year_began'][0])) {
            $year_began = (int)$arraydata['projectcompleted_year_began'][0];
        }
        else {
            $year_began = 0;
        }
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Year began business is greater than current year    */
        else if ($year_began > $year_now)	{
            $srv_resp['messages'][0]    = 'Year began cannot be greater than the year now';
        }
        /*  Input validation success    */
		else {
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
            $projectcompleted_name              = $arraydata['projectcompleted_name'];
            $projectcompleted_cost              = $arraydata['projectcompleted_cost'];
            $projectcompleted_source_funding    = $arraydata['projectcompleted_source_funding'];
            $projectcompleted_year_began        = $arraydata['projectcompleted_year_began'];
            $projectcompleted_result            = $arraydata['projectcompleted_result']; 
            
            /*  Saves Input to the database     */
            foreach ($projectcompleted_name as $key => $value) {
                if ($value) {
                    $data = array(
                        'entity_id'                         => $entity_id, 
                        'projectcompleted_name'             => $projectcompleted_name[$key],
                        'projectcompleted_cost'             => $projectcompleted_cost[$key],
                        'projectcompleted_source_funding'   => $projectcompleted_source_funding[$key],
                        'projectcompleted_year_began'       => $projectcompleted_year_began[$key],
                        'projectcompleted_result'           => $projectcompleted_result[$key],
                        'created_at'                        => date('Y-m-d H:i:s'),
                        'updated_at'                        => date('Y-m-d H:i:s')
                    );
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblprojectcompleted')
                            ->where('projectcompletedid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblprojectcompleted')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
                }
            }
            
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 41.3: getPastprojectDelete
    //      HTTP GET request to delete specified Past Project
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getPastprojectDelete($id)
	{
        /*  Queries the Database for the specified Past Project and delete it  */
		// DB::table('tblprojectcompleted')
        //     ->where('projectcompletedid', $id)
        //     ->where('entity_id', Session::get('entity')->entityid)
        //     ->delete();

        $projects = Projectcompleted::where('projectcompletedid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();
        $projects->is_deleted = 1;
        $projects->save();
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
		
        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
