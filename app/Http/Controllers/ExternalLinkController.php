<?php
//======================================================================
//  Class 21: ExternalLinkController
//  Description: Contains functions for Question Pass
//======================================================================
class ExternalLinkController extends BaseController {
	
	//-----------------------------------------------------
    //  Function 21.1: getExternalLink
	//  Description: function to get/load the questionnaire selected in question pass
    //-----------------------------------------------------	
	public function getExternalLink($code)
	{
		$ql = QuestionnaireLink::where('url', $code)->first();
		if($ql){
			if(Session::has('tql')){	// check for question pass flag in session
				if(Session::get('tql')==$code){
					return Redirect::to('/summary/smesummary/'.$ql->entity_id);
				} else {
					return View::make('errors.link', array(
						'title' => 'Existing Link Detected',
						'message' => 'Another link is still open. Please answer and close them before accessing this link.'
					));
				}
			} else {
				//if not logged in
				if(!Auth::check()){
					//login user 
					$entity = Entity::where('entityid', $ql->entity_id)->first();
					$user = User::find($entity->loginid);
					Auth::login($user);
					Cookie::forever('tql_temp', true);
				} else {
					if(Cookie::get('tql_temp')){
						//do nothing
						Session::forget('tql_owner');
					} else {
						//if logged in and is the owner
						$entity = Entity::where('entityid', $ql->entity_id)->first();
						if(Auth::user()->loginid == $entity->loginid){
							Session::put('tql_owner', true);
						}
					}
				}
				
				if($ql->is_active == 1){
					// add to session vars
					Session::put('tql', $code);
					Session::put('tql_data', $ql->panels);
					sleep(1);
					return Redirect::to('/summary/smesummary/'.$ql->entity_id);
				} else {
					return View::make('errors.link', array(
						'title' => 'Link Expired',
						'message' => 'The link you are trying to access already expired.'
					));
				}
				
			}
		} else {
			return View::make('errors.link', array(
				'title' => 'Link Error',
				'message' => 'The link you are trying to access does not exist.'
			));
		}
	}
	
	//-----------------------------------------------------
    //  Function 21.2: getSaveAndExit
	//  Description: function to update questionnaire with question pass data
    //-----------------------------------------------------	
	public function getSaveAndExit()
	{	
		if(Session::has('tql')){
			$code = Session::get('tql');
			$ql = QuestionnaireLink::where('url', $code)->first();
			// update
			if(Session::has('tql_owner')){
				// release session vars
				Session::forget('tql_owner');
				Session::forget('tql');
				Session::forget('tql_data');
				sleep(1);
				return Redirect::to('/summary/smesummary/'.$ql->entity_id);
			} else {
				$ql->is_active=0;
				$ql->save();
			}
		}
		
		//logout
		Session::flush();
		Auth::logout();

	    return View::make('errors.link', array(
			'title' => 'Form Submitted',
			'message' => 'Thank you for answering and saving the form.'
		));
	}
	
	//-----------------------------------------------------
    //  Function 21.3: postCreateExternalLink
	//  Description: function to generate question pass link
    //-----------------------------------------------------	
	public function postCreateExternalLink()
	{
		$save_array = array(
			'tab' => Input::get('tab'),										// input tabs
			'data' => Input::get('data')									// input data
		);
		do {
			$random_code = self::createRandomCode();						// generate random code
			$ql = QuestionnaireLink::where('url', $random_code)->first();
		} while($ql != null);												// checks if code already exists
		QuestionnaireLink::create(array(									// if not, create the link
			'entity_id' => Session::get('entity')->entityid,
			'url' => $random_code,
			'panels' => json_encode($save_array)
		));
		
		return $random_code; // return the code for url
	}
	
	//-----------------------------------------------------
    //  Function 21.4: getAnsweredLinks
	//  Description: function to get the question pass links from db
    //-----------------------------------------------------	
	public function getAnsweredLinks()
	{
		$entity_id = Input::get('entity_id');
		$ql = QuestionnaireLink::where('entity_id', $entity_id)->get();
		
		return $ql;
	}
	
	//-----------------------------------------------------
    //  Function 21.5: createRandomCode
	//  Description: function to generate random code
    //-----------------------------------------------------	
	public static function createRandomCode(){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 16; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
    
	//-----------------------------------------------------
    //  Function 21.6: getQuestionnaireConfig
	//  Description: function to match if panel is hidden based on Questionnaire config
    //-----------------------------------------------------	
    public static function getQuestionnaireConfig($bank_id)
    {
        $lbl_conversion = array(
            array('affiliates', 'products', 'main_locations', 'capital_details', 'branches', 'import_export', 'insurance', 'cash_conversion', 'major_customer', 'major_supplier', 'major_market', 'competitors', 'business_drivers', 'customer_segments', 'past_projects', 'future_growth', 'owner_details', 'directors', 'shareholders', 'ecf', 'company_succession', 'landscape', 'economic_factors', 'tax_payments', 'risk_assessment', 'growth_potential', 'investment_expansion', 'rcl'),
            array('affiliates-list-cntr', 'offers-list-cntr', 'cap-details-list-cntr', 'main-loc-list-cntr', 'branches-list-cntr', 'biztyp-list-cntr', 'insurance-list-cntr', 'cash-conversion-cntr', 'major-cust-list-cntr', 'major-supp-list-cntr', 'major-loc-list-cntr', 'competitor-list-cntr', 'bizdriver-list-cntr', 'bizsegment-list-cntr', 'past-proj-list-cntr', 'future-growth-list-cntr', 'ownerdetails', 'management-list-cntr', 'management-list-cntr', 'payfactor', 'succession-list-cntr', 'competitionlandscape-list-cntr', 'economicfactors-list-cntr', 'taxpayments-list-cntr', 'ra-list-cntr', 'rev-growth-list-cntr', 'phpm-list-cntr', 'pfs'),
        );
        
        $svr_resp['sts']    = STS_NG;
        $svr_resp['hidden'] = array();													// container
        $panels             = Input::get('panel');										// get input data
        $question_db        = new QuestionnaireConfig;									// create instance
        $custom_db          = new BankCustomDocs;                                       // Create Instance of Custom Documents Model
        $question_data      = $question_db->getQuestionnaireConfigbyBankID($bank_id);	// get config from DB
        $custom_docs_data   = $custom_db->getDocumentsByBank($bank_id);	// get config from DB
		
		/* do the match */
        if (0 < @count($question_data)) {
            foreach ($panels as $panel) {
				$lbl_idx    = array_search($panels, $lbl_conversion[1]);
                if (STS_NG != $lbl_idx) {
                    if (QUESTION_CNFG_HIDDEN == $question_data->$lbl_conversion[0][$lbl_idx]) {
                        $svr_resp['hidden'][] = $panel['panel'];
                    }
                }
            }
            $svr_resp['sts']    = STS_OK;
        }
        
        if ((0 == $bank_id) || (0 == @count($custom_docs_data))) {
            $svr_resp['hidden'][] = 'other-docs-cntr';
        }
        
        // return json results
        return json_encode($svr_resp);
    }
}