<?php
//======================================================================
//  Class 9: CapacityController
//  Description: Contains functions regarding Revenue Growth Potential and pphm
//======================================================================
class CapacityController extends BaseController {

	//-----------------------------------------------------
    //  Function 9.1: getRevenuePotential
	//  Description: get Revenue potential page 
    //-----------------------------------------------------
	public function getRevenuePotential($entity_id)
	{
		return View::make('sme.revenuepotential',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 9.2: postRevenuePotential
	//  Description: function for updating Revenue Potential
    //-----------------------------------------------------
	public function postRevenuePotential($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $security_lib   = new MaliciousAttemptLib; 		// create instance of MaliciousAttemptLib
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#rev-growth-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-rev-growth';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		$messages = array(			// validation messages
            'current_market.0.required' => 'Under Current Market Conditions is required',
            'new_market.0.required' => 'Expected Future Market Conditions is required'
        );

		$rules = array(				// validation rules
	        'current_market.0'	=> 'required|numeric',
	        'new_market.0'	=> 'required|numeric',
		);
	
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else
		{	
			$arraydata = Input::only('current_market', 'new_market');		// get input
            
            if (Input::has('master_key')) {									// master key used for API
                $primary_id     = Input::get('master_key');
            }
            
			$current_market = $arraydata['current_market'];
			$new_market = $arraydata['new_market'];

			foreach ($current_market as $key => $value) {
				if($value){
					// populate data
					$data = array(
					    'entity_id'=>$entity_id, 
					   	'current_market'=>$current_market[$key], 
					   	'new_market'=>$new_market[$key],
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblrevenuegrowthpotential')
                            ->where('growthpotentialid', $primary_id[$key])
                            ->update($data);	// update
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblrevenuegrowthpotential')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            $srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();			// log activiy in db
        return json_encode($srv_resp);	// return json encoded results
	}

	//-----------------------------------------------------
    //  Function 9.3: getRevenuePotentialDelete
	//  Description: function for deleting Revenue potential
    //-----------------------------------------------------
	public function getRevenuePotentialDelete($id)
	{
		// DB::table('tblrevenuegrowthpotential')
        // ->where('growthpotentialid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
        // ->delete();
        
        $revenue = Revenuegrowth::where('growthpotentialid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();
        $revenue->is_deleted = 1;
        $revenue->save();

        ActivityLog::saveLog();
		Session::flash('redirect_tab', 'tab5');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);		 
	}
        
	//-----------------------------------------------------
    //  Function 9.4: getPhpm
	//  Description: get Capacity Expansion Target and Investment Plan (PHPM)
    //-----------------------------------------------------
	public function getPhpm($entity_id)
	{
		return View::make('sme.phpm',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 9.5: postPhpm
	//  Description: post process for Capacity Expansion Target and Investment Plan (PHPM)
    //-----------------------------------------------------
	public function postPhpm($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $security_lib   = new MaliciousAttemptLib;		// create instance of MaliciousAttemptLib
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#phpm-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-phpm';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		$messages = array(	// validator messages
            'current_year.0.required' => 'Within next 12 months is required',
            'new_year.0.required' => 'Within next 2 years is required',
            'current_year.0.numeric' => trans('validation.numeric', array('attribute'=>'Within next 12 months')),
            'current_year.0.max' => trans('validation.max.numeric', array('attribute'=>'Within next 12 months')),
            'new_year.0.numeric' => trans('validation.numeric', array('attribute'=>'Within next 2 years')),
            'new_year.0.max' => trans('validation.max.numeric', array('attribute'=>'Within next 2 years')),
        );

		$rules = array(		// validation rules
	        'current_year.0'	=> 'required|numeric|max:9999999999999.99',
	        'new_year.0'	=> 'required|numeric|max:9999999999999.99',
		);
	
		$validator = Validator::make(Input::all(), $rules, $messages); // run validator

		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else {	
			$arraydata = Input::only('current_year', 'new_year');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			$current_year = $arraydata['current_year'];
			$new_year = $arraydata['new_year'];

			foreach ($current_year as $key => $value) {
				if($value){
					$data = array(
						// populate data
					    'entity_id'=>$entity_id, 
					   	'current_year'=>$current_year[$key], 
					   	'new_year'=>$new_year[$key],
					   	'created_at'=>date('Y-m-d H:i:s'),
				    	'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblcapacityexpansion')
                            ->where('capacityexpansionid', $primary_id[$key])
                            ->update($data);	// and update
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblcapacityexpansion')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            $srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        return json_encode($srv_resp); // return json result
	}

	//-----------------------------------------------------
    //  Function 9.6: getPhpmDelete
	//  Description: function for deleting Capacity Expansion Target and Investment Plan (PHPM)
    //-----------------------------------------------------
    public function getPhpmDelete($id)
	{
		// DB::table('tblcapacityexpansion')
        // ->where('capacityexpansionid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
        // ->delete();
        
        $capital = Capacityexpansion::where('capacityexpansionid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();

        $capital->is_deleted = 1;
        $capital->save();
                
        Session::flash('redirect_tab', 'tab5');
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
