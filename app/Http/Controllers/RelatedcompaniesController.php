<?php

//======================================================================
//  Class 48: Related Companies Controller
//      Functions related to Affiliates on the Questionnaire
//      are routed to this class
//======================================================================
class RelatedcompaniesController extends BaseController
{
    //-----------------------------------------------------
    //  Function 48.1: getRelatedcompanies
    //      Display Affiliates Form
    //-----------------------------------------------------
	public function getRelatedcompanies($entity_id)
	{
        /*  Gets all Province from Database */
		$dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		
        /*  Gets all Cities from Database   */
        $dataCity = array(
			0 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.0'))->pluck('city','city'),
			1 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.1'))->pluck('city','city'),
			2 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.2'))->pluck('city','city')
		);
			
		return View::make('sme.relatedcompanies', 
			array(
				'cityList' => $dataCity,
				'provinceList' => $dataProvince,
                'entity_id'     => $entity_id
				)
			);
	}
    
    //-----------------------------------------------------
    //  Function 48.2: postRelatedcompanies
    //      HTTP POST Request to validate and save
    //      Affiliates data on the Database
    //-----------------------------------------------------
	public function postRelatedcompanies($entity_id)
	{
        /*  Variable Declaration    */
        $companies = Input::get('related_company_name');    // Affiliates Name
        $messages   = array();                              // Validation Messages
        $rules      = array();                              // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;          // Instance of the Malicious Attempt Library
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $entity = Entity::find($entity_id);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            $url = URL::to('premium-link/summary/'.$entity_id);
            if ((!Input::has('action')) && (!Input::has('section'))) {
                $srv_resp['action']         = REFRESH_CNTR_ACT; 
                $srv_resp['refresh_url']    = $url;    // URL to fetch data from
                $srv_resp['refresh_cntr']   = '#affiliates-list-cntr';                      // Refresh the specified container
                $srv_resp['refresh_elems']  = '.reload-affiliates';                         // Elements that will be refreshed
            }
        } else {
            $url = URL::to('summary/smesummary/'.$entity_id);
             /*  Request did not come from API set the server response   */
            if ((!Input::has('action')) && (!Input::has('section'))) {
                $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
                $srv_resp['refresh_cntr']   = '#affiliates-list-cntr';                      // Refresh the specified container
                $srv_resp['refresh_url']    = $url;    // URL to fetch data from
                $srv_resp['refresh_elems']  = '.reload-affiliates';                         // Elements that will be refreshed
            }
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($companies); $idx++) {
            
            /*  Always validate the first input set and 
            *   for the succeeding validate those with Affiliates name
            */
            if ((STR_EMPTY != $companies[$idx])
            || (0 == $idx) ) {
                if($entity->is_premium == PREMIUM_REPORT) {
                    $messages['related_company_name.'.$idx.'.required']        = trans('validation.required', array('attribute'=>'Company Name'));
                    $messages['related_company_address1.'.$idx.'.required']    = trans('validation.required', array('attribute'=>'Company Address'));
                    $messages['related_company_phone.'.$idx.'.regex']          = trans('validation.phoneregex', array('attribute'=>'Company Phone'));
                    $messages['related_company_email.'.$idx.'.email']          = trans('validation.email', array('attribute'=>'Email'));
                
                    $rules['related_company_name.'.$idx]        = 'required';
                    $rules['related_company_address1.'.$idx]	= 'required';
                    $rules['related_company_phone.'.$idx]       = 'sometimes|nullable|regex:/^.{7,20}$/';
                    $rules['related_company_email.'.$idx]       = 'sometimes|nullable|email';
                } else {
                    $messages['related_company_name.'.$idx.'.required']        = trans('validation.required', array('attribute'=>'Company Name'));
                    $messages['related_company_address1.'.$idx.'.required']    = trans('validation.required', array('attribute'=>'Company Address'));
                    $messages['related_company_cityid.'.$idx.'.required']      = trans('validation.required', array('attribute'=>'City'));
                    $messages['related_company_province.'.$idx.'.required']    = trans('validation.required', array('attribute'=>'Province'));
                    $messages['related_company_zipcode.'.$idx.'.required']     = trans('validation.required', array('attribute'=>'Zipcode'));
                    $messages['related_company_zipcode.'.$idx.'.digits']       = trans('validation.digits', array('attribute'=>'Zipcode'));
                    $messages['related_company_phone.'.$idx.'.required']       = trans('validation.required', array('attribute'=>'Company Phone'));
                    $messages['related_company_phone.'.$idx.'.regex']          = trans('validation.phoneregex', array('attribute'=>'Company Phone'));
                    $messages['related_company_email.'.$idx.'.required']       = trans('validation.required', array('attribute'=>'Email'));
                    $messages['related_company_email.'.$idx.'.email']          = trans('validation.email', array('attribute'=>'Email'));
                
                    $rules['related_company_name.'.$idx]        = 'required';
                    $rules['related_company_address1.'.$idx]	= 'required';
                    $rules['related_company_cityid.'.$idx]      = 'required';
                    $rules['related_company_province.'.$idx]	= 'required';
                    $rules['related_company_zipcode.'.$idx]     = 'required|digits:4';
                    $rules['related_company_phone.'.$idx]       = 'required|regex:/^.{7,20}$/';
                    $rules['related_company_email.'.$idx]       = 'required|email';
                }
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('related_company_name', 'related_company_address1', 'related_company_address2', 'related_company_cityid', 'related_company_zipcode', 'related_company_province', 'related_company_phone', 'related_company_email');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
			$related_company_name       = $arraydata['related_company_name'];
            $related_company_address1   = $arraydata['related_company_address1'];
			$related_company_phone      = $arraydata['related_company_phone'];
            $related_company_email      = $arraydata['related_company_email'];
            
            if($entity->is_premium != PREMIUM_REPORT) {
                $related_company_cityid     = $arraydata['related_company_cityid'];
                $related_company_province   = $arraydata['related_company_province'];
                $related_company_zipcode    = $arraydata['related_company_zipcode'];
			    $related_company_address2   = $arraydata['related_company_address2'];
            }
            
            /*  Saves Input to the database     */
			foreach ($related_company_name as $key => $value) {
				if ($value) {
                    if($entity->is_premium != PREMIUM_REPORT) {
                        $data = array(
                            'entity_id'=>$entity_id, 
                               'related_company_name'      => $related_company_name[$key],
                               'related_company_address1'  => $related_company_address1[$key],
                               'related_company_address2'  => $related_company_address2[$key],
                               'related_company_cityid'    => $related_company_cityid[$key],
                               'related_company_province'  => $related_company_province[$key],
                               'related_company_zipcode'   => $related_company_zipcode[$key],
                               'related_company_phone'     => $related_company_phone[$key],
                               'related_company_email'     => $related_company_email[$key],
                               'created_at'                => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s')
                        );
                    } else {
                        $data = array(
                            'entity_id'=>$entity_id, 
                            'related_company_name'      => $related_company_name[$key],
                            'related_company_address1'  => $related_company_address1[$key],
                            'related_company_phone'     => $related_company_phone[$key] == null ? ' ' : $related_company_phone[$key],
                            'related_company_email'     => $related_company_email[$key] == null ? ' ' : $related_company_email[$key],
                            'created_at'                => date('Y-m-d H:i:s'),
                            'updated_at'                => date('Y-m-d H:i:s')
                        );
                    }
					
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblrelatedcompanies')
                            ->where('relatedcompaniesid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblrelatedcompanies')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
            
            /*  Sets request status to success  */
			$srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
		
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 48.3: getRelatedcompaniesDelete
    //      HTTP GET request to delete specified Affiliates
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getRelatedcompaniesDelete($id)
	{
        $data = Relatedcompanies::find($id);
        $relatedcompany = Relatedcompanies::where('relatedcompaniesid', '=', $id)
                        ->where('entity_id', '=', Session::get('entity')->entityid)
                        ->first();
        $relatedcompany->is_deleted = 1;
        $relatedcompany->save();

        $entity = Entity::find($data->entity_id);
        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        
        /*  Adds a one-off session variable for redirection */
        Session::flash('redirect_tab', 'tab1');
        
        /*  Request did not come from API set the server response   */
        if ($entity->is_premium == PREMIUM_REPORT ) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
            return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
    }
    
    //-----------------------------------------------------
    //  Function 48.4: getPremiumRelatedCompanies
    //      Display Affiliates Form
    //-----------------------------------------------------
	public function getPremiumRelatedCompanies($entity_id)
	{
        /*  Gets all Province from Database */
		$dataProvince = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
		
        /*  Gets all Cities from Database   */
        $dataCity = array(
			0 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.0'))->pluck('city','city'),
			1 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.1'))->pluck('city','city'),
			2 => Zipcode::select('city')->where('major_area', Input::old('related_company_province.2'))->pluck('city','city')
		);
			
		return View::make('report.related_companies_premium', 
			array(
				'cityList' => $dataCity,
				'provinceList' => $dataProvince,
                'entity_id'     => $entity_id
				)
			);
    }

}
