<?php
use \Exception as Exception;
//======================================================================
//  Class 39: OCRController
//  Description: Contains functions for OCR
//======================================================================
class OCRController extends BaseController {

	//-----------------------------------------------------
    //  Function 39.1: getPDFtoCSV
	//  Description: Display PDF to CSV converter page using tesseract
    //-----------------------------------------------------
	public function getPDFtoCSV()
	{
		return View::make('tools.pdf2csv');
	}

	//-----------------------------------------------------
    //  Function 39.2: postPDFtoCSV
	//  Description: post process for  PDF to CSV converter page using tesseract
    //-----------------------------------------------------
	public function postPDFtoCSV()
	{
		$content = '';								// viarable container
		$image = Input::file('imagefile');			// input file

		$validator = Validator::make(Input::all(), [	// validation rules
			'imagefile' => 'required|mimes:pdf,jpeg'
		]);

		if ($validator->fails()) {
			return Redirect::to('/tools/ocr')->withErrors($validator->errors());
		}

		if($image){		// check if file exists

			$image->move('temp', $image->getClientOriginalName());	// copy file to server

			$resolution = 300;	// set default resolution

			if($image->getClientOriginalExtension() == 'pdf'){
				$is_pdf = true;
				$imagick = new Imagick();
				$imagick->setResolution($resolution, $resolution);
				$imagick->readImage('temp/'.$image->getClientOriginalName());

				// ImageMagick settings
				$imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
				$imagick->setImageCompressionQuality(100);
				$imagick->writeImages('temp/rasterized.jpg', false);
				$filename = 'rasterized.jpg';
			} else {
				$filename = $image->getClientOriginalName();
			}

			// Textcleaner settings
			exec('bash /scripts/textcleaner -g -e stretch -f 25 -o 10 -s 1 temp/'.$filename.' temp/rasterized2.jpg');
			$filename = 'rasterized2.jpg';

			$tesseract = new TesseractOCR('temp/' . $filename);
			$tesseract->setTempDir('temp');
			$tesseract->setPsm(1);
			$content = $tesseract->recognize();

			$array = explode("\n", $content);
			$parsed_data = array();

			$fp = fopen('temp/converted_file.csv', 'w');

			foreach($array as $a){
				$array2 = explode(' ', $a);
				fputcsv($fp, $array2);
			}

			fclose($fp);

			return Response::download('temp/converted_file.csv', 'converted_file.csv');

		} else {
			return Redirect::to('/tools/ocr')->withErrors(['imagefile'=>'No file selected.']);
		}
	}

	//-----------------------------------------------------
    //  Function 39.3: getTest
	//  Description: display test page for OCR
    //-----------------------------------------------------
	public function getTest()
	{
		if(ENVIRONMENT != ENV_PRODUCTION){
			return View::make('sample_ocr');
		} else {
			App::abort(404);
		}
	}

	//-----------------------------------------------------
    //  Function 39.4: postTest
	//  Description: post process for test page for OCR (tesseract)
    //-----------------------------------------------------
	public function postTest()
	{
		$start_time = microtime(TRUE);
		$content = '';
		$image = Input::file('imagefile');

		if($image){

			$image->move('temp', $image->getClientOriginalName());
			$is_pdf = false;
			$used_textcleaner = false;
			$has_thumbs = false;
			$resolution = intval(Input::get('resolution'));

			if($image->getClientOriginalExtension() == 'pdf'){
				$is_pdf = true;
				$imagick = new Imagick();
				$imagick->setResolution($resolution, $resolution);
				$imagick->readImage('temp/'.$image->getClientOriginalName());

				// ImageMagick settings
				$imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
				$imagick->setImageCompressionQuality(intval(Input::get('quality')));
				$imagick->writeImages('temp/rasterized.jpg', false);
				$filename = 'rasterized.jpg';
			} else {
				$filename = $image->getClientOriginalName();
			}

			if(Input::has('thumbs')){
				$has_thumbs = true;
				// Create crop
				$crop = new Imagick('temp/rasterized.jpg');
				$crop->cropImage($resolution, $resolution / 2, $resolution * 1.5, $resolution);
				$crop->writeImages('temp/rasterized_crop.jpg', false);
			}

			if(Input::has('noclean')){
				// Textcleaner settings
				exec('bash /scripts/textcleaner '.Input::get('options').' temp/'.$filename.' temp/rasterized2.jpg');
				$filename = 'rasterized2.jpg';
				$used_textcleaner = true;

				if(Input::has('thumbs')){
					// Create crop
					$crop = new Imagick('temp/rasterized2.jpg');
					$crop->cropImage($resolution, $resolution / 2, $resolution * 1.5, $resolution);
					$crop->writeImages('temp/rasterized2_crop.jpg', false);
				}
			}

			$tesseract = new TesseractOCR('temp/' . $filename);
			$tesseract->setTempDir('temp');
			$tesseract->setPsm(1);
			$content = $tesseract->recognize();
			$filesize = filesize('temp/' . $filename);
		} else {
			$content = "No file selected.";
		}


		$end_time = microtime(TRUE);
		return View::make('sample_ocr', array(
			'resolution' => $resolution,
			'quality' => Input::get('quality'),
			'options' => Input::get('options'),
			'benchmark' => $end_time - $start_time,
			'filesize' => $filesize,
			'is_pdf' => $is_pdf,
			'used_textcleaner' => $used_textcleaner,
			'has_thumbs' => $has_thumbs,
			'content' => $content
		));
	}

	//-----------------------------------------------------
    //  Function 39.5: postTestParse
	//  Description: function to parse data from test function (tesseract)
    //-----------------------------------------------------
	public function postTestParse()
	{
		$content = Input::get('content');
		$array = explode("\n", $content);
		$parsed_data = array();

		foreach($array as $a){
			if(preg_match('/^\d{2}.\s\d{2}/',$a)){
				$array2 = explode(' ', $a);
				$length = @count($array2);
				$debit = false;

				if(intval($array2[$length - 3]) > 0 && strlen($array2[$length - 3]) > 9){
					$debit = true;
				}

				$desc = '';
				$desc_end = ($debit) ? 3 : 2;
				for($x=2; $x<($length-$desc_end); $x++){
					$desc .= $array2[$x] . ' ';
				}
				$parsed_data[] = array(
					'date' => $array2[0] . $array2[1],
					'desc' => $desc,
					'check' => ($debit) ? $array2[$length - 3] : '',
					'debit' => ($debit) ? $array2[$length - 2] : '',
					'credit' => ($debit) ? '' : $array2[$length - 2],
					'balance' => $array2[$length - 1]
				);
			}
		}
		return $parsed_data;
	}

	//-----------------------------------------------------
    //  Function 39.6: getTestDropbox
	//  Description: function to test dropbox saving
    //-----------------------------------------------------
	public function getTestDropbox()
	{
		// auth1
		$appInfo = Dropbox\AppInfo::loadFromJsonFile("temp/app-info.json");
		$webAuth = new Dropbox\WebAuthNoRedirect($appInfo, "CreditBPO");

		// auth2
		$access_token = "yweFCPIbH1AAAAAAAAACIz85iks61isVerIxME--SSBXrQVVts87SfDw1tLHpl_f";
		$dbxClient = new Dropbox\Client($access_token, "CreditBPO");

		// To get account info
		//$accountInfo = $dbxClient->getAccountInfo();

		// To get directory
		//$folderMetadata = $dbxClient->getMetadataWithChildren("/");

		// To upload files
		// $filename = "sample.pdf";
		// $file = fopen("temp/".$filename, "rb");
		// $result = $dbxClient->uploadFile("/testdir/".$filename, Dropbox\WriteMode::add(), $file);
		// fclose($file);

		return Entity::getCompanyName(Auth::user()->loginid);
	}

	//-----------------------------------------------------
    //  Function 39.7: getOCRSDK
	//  Description: get test page for OCR using ABBY Cloud API
    //-----------------------------------------------------
	public function getOCRSDK()
	{
		return View::make('sample_ocrsdk', ['response'=>'','transactions'=>[],'average'=>0]);
	}

	//-----------------------------------------------------
    //  Function 39.8: postOCRSDK
	//  Description: post processing for test page using ABBY Cloud API
    //-----------------------------------------------------
	public function postOCRSDK()
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$image = Input::file('imagefile');
		$count = 0;
		$total = 0;
		$average = 0;
		$transactions = [];


		$image->move('temp', $image->getClientOriginalName());
		$fileName = $image->getClientOriginalName();
		$filePath = getcwd().'/temp/'.$fileName;

		$za = new ZipArchive();
		$za->open($filePath);
		print_r($za);
		var_dump($za);
		echo "numFiles: " . $za->numFiles . "<br>";
		echo "status: " . $za->status  . "<br>";
		echo "statusSys: " . $za->statusSys . "<br>";
		echo "filename: " . $za->filename . "<br>";
		echo "comment: " . $za->comment . "<br>";

		for ($i=0; $i<$za->numFiles;$i++) {
			echo "index: $i<br>";
			print_r($za->statIndex($i));
		}
		echo "numFile:" . $za->numFiles . "<br>";

		exit;

		if($image){

			$image->move('temp', $image->getClientOriginalName());
			$fileName = $image->getClientOriginalName();
			$filePath = getcwd().'/temp/'.$fileName;
			if(!file_exists($filePath))
			{
			die('File '.$filePath.' not found.');
			}
			if(!is_readable($filePath) )
			{
			 die('Access to file '.$filePath.' denied.');
			}
			$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

			// Send HTTP POST request and ret xml response
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

			$post_array = array();
			if((version_compare(PHP_VERSION, '5.5') >= 0)) {
				$post_array["my_file"] = new CURLFile($filePath);
			} else {
				$post_array["my_file"] = "@".$filePath;
			}

			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
			$response = curl_exec($curlHandle);

			if($response == FALSE) {
				$errorText = curl_error($curlHandle);
				curl_close($curlHandle);
				die($errorText);
			}

			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// Parse xml response
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
				   die($xml->message);
				}
				die("unexpected response ".$response);
			}

			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus != "Queued") {
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Task id
			$taskid = $arr["id"];

			// 4. Get task information in a loop until task processing finishes
			// 5. If response contains "Completed" staus - extract url with result
			// 6. Download recognition result (text) and display it
			$url = 'http://cloud.ocrsdk.com/getTaskStatus';
			$qry_str = "?taskid=$taskid";
			// Check task status in a loop until it is finished
			// Note: it's recommended that your application waits
			// at least 2 seconds before making the first getTaskStatus request
			// and also between such requests for the same task.
			// Making requests more often will not improve your application performance.
			// Note: if your application queues several files and waits for them
			// it's recommended that you use listFinishedTasks instead (which is described
			// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
			while(true)
			{
				sleep(5);
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
				curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
				curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curlHandle);
				$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
				curl_close($curlHandle);

				// parse xml
				$xml = simplexml_load_string($response);
				if($httpCode != 200) {
				  if(property_exists($xml, "message")) {
					die($xml->message);
				  }
				  die("Unexpected response ".$response);
				}
				$arr = $xml->task[0]->attributes();
				$taskStatus = $arr["status"];
				if($taskStatus == "Queued" || $taskStatus == "InProgress") {
				  // continue waiting
				  continue;
				}
				if($taskStatus == "Completed") {
				  // exit this loop and proceed to handling the result
				  break;
				}

				// error handling
				if($taskStatus == "ProcessingFailed") {
					if (env("APP_ENV") == "Production") {
						try{
							Mail::send('emails.ocr_fail', array(
								'error_message' => "Task processing failed: ".$arr["error"]
							), function($message){
								$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
								->subject('Error in OCR SDK!');
							});
						}catch(Exception $e){
			            //
			            }
					}
					die("Task processing failed: ".$arr["error"]);
				}
				if (env("APP_ENV") == "Production") {

					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Result is ready. Download it
			$url = $arr["resultUrl"];
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// Warning! This is for easier out-of-the box usage of the sample only.
			// The URL to the result has https:// prefix, so SSL is required to
			// download from it. For whatever reason PHP runtime fails to perform
			// a request unless SSL certificate verification is off.
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curlHandle);
			curl_close($curlHandle);

			$response_data = explode("\n", $response);
			foreach($response_data as $rd){
				$trimmed = trim($rd);
				$date = date_parse(str_replace(' ', '', $trimmed));

				$last_word = trim(substr($trimmed, strrpos($trimmed, "  ")));
				$last_word = str_replace(' ', '.', $last_word);

				if(isset($date["month"]) && $date["month"] != false && $date["day"] != false)
				{
					//cleanup numbers
					$last_word = str_replace('.', ',', $last_word);
					if(substr($last_word, -3, 1) == ','){
						$last_word = substr_replace($last_word, '.', -3, 1);
					}
					if(preg_match ("/^\d{1,3}(,\d{3})*(\.\d{2}+)$/", $last_word) == 1){
						$transactions[$date['month'] . '-' . $date['day']] = $last_word;
					}
				}
			}

			foreach($transactions as $t){
				$amount = str_replace(',', '', $t);
				$total += (float)$amount;
				$count++;
			}

			if($count > 0){
				$average = $total / $count;
			}

		}

		return View::make('sample_ocrsdk', [
			'response' => $response,
			'transactions' => $transactions,
			'average' => $average
		]);
	}

	//-----------------------------------------------------
    //  Function 39.9: getAutoOCR
	//  Description: function to get Daily Average Balance by Analyst using OCR
    //-----------------------------------------------------
	public function getAutoOCR($entity_id)
	{
		// Currently this function works only for bank-statements

		/*
		It takes approximately 15-30 seconds for scanning one image file (scanning done with Tesseract OCR). It takes approximately 30 seconds to 1 minute for extracting the pages from a PDF file to images (convertion done with Imagemagick)
		So based on that it should take approximately 10 minutes to unpack and scan a PDF with 20 pages
		*/
		// increased the max execution time for 10 minutes
		ini_set('max_execution_time', 600);


		$bank_statements = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', 51)->get();
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$total = 0;
		$count = 0;
		$transactions = [];

		// create temp folder to extract some files like the temporary images from the PDF statment, the bank-statements in archives etc...
		$tempFolder = getcwd()."/temp/".time();
		mkdir($tempFolder);

		// we gather in this variable the scanned text from all bank-statements (Images, Pages in PDF statements, Statements from archives, etc...
		$allBankStatementsArr = [];
		foreach($bank_statements as $bs){
			$filePath = getcwd().'/documents/'.$bs->document_rename;
			// a recursive function is called to parse and scan each file
			$this->parseFile($filePath,$allBankStatementsArr, $tempFolder);
		}

		// delete temp fir
		OCRHelper::deleteDir($tempFolder);

		// calculate the average daily balance, based on the text from all statements and return a formatted float number

		$averageDailyBalance = OCRHelper::getAverageDailyBalance($allBankStatementsArr);

		if (!$averageDailyBalance) {
			OcrPossibleFailure::create(['entity_id' => $entity_id]);
		}

		return number_format($averageDailyBalance, 2,".","");
	}

	//-----------------------------------------------------
    //  Function 39.10: parseFile
	//  Description: helper function for parsing (and scanning) of the statements
    //-----------------------------------------------------
	private function parseFile($filePath,&$allBankStatementsArr, $tempFolder) 
	{
		// get info about the path, that is passed
		$path_parts = pathinfo($filePath);


		// ZIP - extract the archive and call this function for each of the files in the archive
		if($path_parts['extension'] == 'zip'){
			$extractedFolder = OCRHelper::extractZip($filePath, $tempFolder);

			if($extractedFolder != false) {
				if ($handle = opendir($extractedFolder)) {
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							$this->parseFile($extractedFolder."/".$entry, $allBankStatementsArr, $tempFolder);
						}
					}
					closedir($handle);
				}
			}
		}
		// RAR - extract the archive and call this function for each of the files in the archive ( requires that the PHP RAR plugins is installed and enabled  http://php.net/manual/en/book.rar.php)
		else if($path_parts['extension'] == 'rar'){
			$extractedFolder = OCRHelper::extractRar($filePath, $tempFolder);

			if($extractedFolder != false) {
				if ($handle = opendir($extractedFolder)) {
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							$this->parseFile($extractedFolder."/".$entry, $allBankStatementsArr, $tempFolder);
						}
					}
					closedir($handle);
				}
			}
		}
		// PDF - extract the pages to images and parse the images (requires ImageMagick plugin is installed and enabled
		else if($path_parts['extension'] == 'pdf'){
			$extractedFolder = OCRHelper::pdfToJpg($filePath, $tempFolder);

			if($extractedFolder != false) {
				if ($handle = opendir($extractedFolder)) {
					while (false !== ($entry = readdir($handle))) {
						if ($entry != "." && $entry != "..") {
							$this->parseFile($extractedFolder."/".$entry, $allBankStatementsArr, $tempFolder);
						}
					}
					closedir($handle);
				}
			}
		}
		// normal image - just scan it and fill the result in the output array
		else if($path_parts['extension'] == 'jpg' || $path_parts['extension'] == 'jpeg' || $path_parts['extension'] == 'png'){
			$bankStatementArr = OCRHelper::scanBankStatement($filePath);
			$allBankStatementsArr = array_merge($allBankStatementsArr,$bankStatementArr);
		}
	}

	//-----------------------------------------------------
    //  Function 39.11: getCheckUtilityBill
	//  Description: function to check utility bills using OCR
    //-----------------------------------------------------
	public function getCheckUtilityBill($id)
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$document = DB::table('tbldocument')->where('documentid', $id)->first();
		$fileName = $document->document_rename;

		if($fileName){

			// Using OCR SDK
			$filePath = getcwd().'/documents/'.$fileName;

			if(!file_exists($filePath))
			{
				die('File '.$filePath.' not found.');
			}
			if(!is_readable($filePath) )
			{
				die('Access to file '.$filePath.' denied.');
			}
			$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

			// Send HTTP POST request and ret xml response
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

			$post_array = array();
			if((version_compare(PHP_VERSION, '5.5') >= 0)) {
				$post_array["my_file"] = new CURLFile($filePath);
			} else {
				$post_array["my_file"] = "@".$filePath;
			}

			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
			$response = curl_exec($curlHandle);

			if($response == FALSE) {
				$errorText = curl_error($curlHandle);
				curl_close($curlHandle);
				die($errorText);
			}

			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// Parse xml response
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
				   die($xml->message);
				}
				die("unexpected response ".$response);
			}

			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus != "Queued") {
				if (env("APP_ENV") == "Production") {

					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Task id
			$taskid = $arr["id"];

			// 4. Get task information in a loop until task processing finishes
			// 5. If response contains "Completed" staus - extract url with result
			// 6. Download recognition result (text) and display it
			$url = 'http://cloud.ocrsdk.com/getTaskStatus';
			$qry_str = "?taskid=$taskid";
			// Check task status in a loop until it is finished
			// Note: it's recommended that your application waits
			// at least 2 seconds before making the first getTaskStatus request
			// and also between such requests for the same task.
			// Making requests more often will not improve your application performance.
			// Note: if your application queues several files and waits for them
			// it's recommended that you use listFinishedTasks instead (which is described
			// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
			while(true)
			{
				sleep(5);
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
				curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
				curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curlHandle);
				$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
				curl_close($curlHandle);

				// parse xml
				$xml = simplexml_load_string($response);
				if($httpCode != 200) {
				  if(property_exists($xml, "message")) {
					die($xml->message);
				  }
				  die("Unexpected response ".$response);
				}
				$arr = $xml->task[0]->attributes();
				$taskStatus = $arr["status"];
				if($taskStatus == "Queued" || $taskStatus == "InProgress") {
				  // continue waiting
				  continue;
				}
				if($taskStatus == "Completed") {
				  // exit this loop and proceed to handling the result
				  break;
				}

				// error handling
				if($taskStatus == "ProcessingFailed") {
					if (env("APP_ENV") == "Production") {
						try{
							Mail::send('emails.ocr_fail', array(
								'message' => "Task processing failed: ".$arr["error"]
							), function($message){
								$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
								->subject('Error in OCR SDK!');
							});
						}catch(Exception $e){
			            //
			            }
					}
					die("Task processing failed: ".$arr["error"]);
				}
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Result is ready. Download it
			$url = $arr["resultUrl"];
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// Warning! This is for easier out-of-the box usage of the sample only.
			// The URL to the result has https:// prefix, so SSL is required to
			// download from it. For whatever reason PHP runtime fails to perform
			// a request unless SSL certificate verification is off.
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curlHandle);
			curl_close($curlHandle);

			// Using TesseractOCR
			/* if(pathinfo($fileName, PATHINFO_EXTENSION) == 'pdf'){
				$imagick = new Imagick();
				$imagick->setResolution(300, 300);
				$imagick->readImage('documents/'.$fileName);

				// ImageMagick settings
				$imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
				$imagick->setImageCompressionQuality(100);
				$imagick->writeImages('temp/rasterized.jpg', false);
				$filePath = 'temp/rasterized.jpg';
			} else {
				$filePath = 'documents/'.$fileName;
			}

			// Textcleaner settings
			exec('bash /scripts/textcleaner -g -e stretch -f 25 -o 10 -s 1 '.$filePath.' temp/rasterized2.jpg');
			$filePath = 'temp/rasterized2.jpg';
			$tesseract = new TesseractOCR($filePath);
			$tesseract->setTempDir('temp');
			$tesseract->setPsm(1);
			$response = $tesseract->recognize(); */

			$valid = false;

			// conditional checks
			if (stripos($response, 'meralco') !== false ||
				stripos($response, 'globe') !== false ||
				stripos($response, 'smart') !== false ||
				stripos($response, 'digitel') !== false ||
				stripos($response, 'maynilad') !== false ||
				stripos($response, 'telephone') !== false ||
				stripos($response, 'statement') !== false ||
				stripos($response, 'total kwh') !== false ||
				stripos($response, 'electric bill') !== false ||
				stripos($response, 'account') !== false
			) {
				$valid = true;
			}

			if (stripos($response, 'pay immediately') !== false) {
				// Flag for Payment Issue with Utility Bills
				//
			}

			if($valid){
				DB::table('tbldocument')->where('documentid', $id)->update(['is_valid'=>1]);
			} else {
				DB::table('tbldocument')->where('documentid', $id)->update(['is_valid'=>2]);
			}

		}
	}

	//-----------------------------------------------------
    //  Function 39.12: getCheckBankStement
	//  Description: function to check bank statements using OCR
    //-----------------------------------------------------
	public function getCheckBankStement($id)
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$document = DB::table('tbldocument')->where('documentid', $id)->first();
		$fileName = $document->document_rename;

		if($fileName){

			// Using OCR SDK
			$filePath = getcwd().'/documents/'.$fileName;

			if(!file_exists($filePath))
			{
				die('File '.$filePath.' not found.');
			}
			if(!is_readable($filePath) )
			{
				die('Access to file '.$filePath.' denied.');
			}
			$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

			// Send HTTP POST request and ret xml response
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

			$post_array = array();
			if((version_compare(PHP_VERSION, '5.5') >= 0)) {
				$post_array["my_file"] = new CURLFile($filePath);
			} else {
				$post_array["my_file"] = "@".$filePath;
			}

			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
			$response = curl_exec($curlHandle);

			if($response == FALSE) {
				$errorText = curl_error($curlHandle);
				curl_close($curlHandle);
				die($errorText);
			}

			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// Parse xml response
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
				   die($xml->message);
				}
				die("unexpected response ".$response);
			}

			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus != "Queued") {
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Task id
			$taskid = $arr["id"];

			// 4. Get task information in a loop until task processing finishes
			// 5. If response contains "Completed" staus - extract url with result
			// 6. Download recognition result (text) and display it
			$url = 'http://cloud.ocrsdk.com/getTaskStatus';
			$qry_str = "?taskid=$taskid";
			// Check task status in a loop until it is finished
			// Note: it's recommended that your application waits
			// at least 2 seconds before making the first getTaskStatus request
			// and also between such requests for the same task.
			// Making requests more often will not improve your application performance.
			// Note: if your application queues several files and waits for them
			// it's recommended that you use listFinishedTasks instead (which is described
			// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
			while(true)
			{
				sleep(5);
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
				curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
				curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curlHandle);
				$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
				curl_close($curlHandle);

				// parse xml
				$xml = simplexml_load_string($response);
				if($httpCode != 200) {
				  if(property_exists($xml, "message")) {
					die($xml->message);
				  }
				  die("Unexpected response ".$response);
				}
				$arr = $xml->task[0]->attributes();
				$taskStatus = $arr["status"];
				if($taskStatus == "Queued" || $taskStatus == "InProgress") {
				  // continue waiting
				  continue;
				}
				if($taskStatus == "Completed") {
				  // exit this loop and proceed to handling the result
				  break;
				}

				// error handling
				if($taskStatus == "ProcessingFailed") {
					if (env("APP_ENV") == "Production") {
						try{
							Mail::send('emails.ocr_fail', array(
								'message' => "Task processing failed: ".$arr["error"]
							), function($message){
								$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
								->subject('Error in OCR SDK!');
							});
						}catch(Exception $e){
			            //
			            }
					}
					die("Task processing failed: ".$arr["error"]);
				}
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Result is ready. Download it
			$url = $arr["resultUrl"];
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// Warning! This is for easier out-of-the box usage of the sample only.
			// The URL to the result has https:// prefix, so SSL is required to
			// download from it. For whatever reason PHP runtime fails to perform
			// a request unless SSL certificate verification is off.
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curlHandle);
			curl_close($curlHandle);

			$valid_count = 0;

			// conditional checks
			if (stripos($response, 'date') !== false){
				$valid_count++;
			}
			if (stripos($response, 'balance') !== false){
				$valid_count++;
			}
			if (stripos($response, 'deposit') !== false){
				$valid_count++;
			}
			if (stripos($response, 'withdrawal') !== false){
				$valid_count++;
			}
			if (stripos($response, 'debit') !== false){
				$valid_count++;
			}
			if (stripos($response, 'credit') !== false){
				$valid_count++;
			}

			if($valid_count >= 4){
				$total = 0;
				$response_data = explode("\n", $response);
				foreach($response_data as $rd){
					$trimmed = trim($rd);
					$pieces = explode(" ", $trimmed);
					$last_word = array_pop($pieces);
					if(preg_match ("/^\d{1,3}(,\d{3})*(\.\d{2}+)$/", $last_word) == 1){
						$last_word = str_replace(',', '', $last_word);
						$total += (float)$last_word;
					}
				}

				if($total > 0){
					DB::table('tbldocument')->where('documentid', $id)->update(['is_valid'=>1]);
				} else {
					DB::table('tbldocument')->where('documentid', $id)->update(['is_valid'=>2]);
				}
			} else {
				DB::table('tbldocument')->where('documentid', $id)->update(['is_valid'=>2]);
			}

		}
	}

	//-----------------------------------------------------
    //  Function 39.13: computeOperatingCashflow
	//  Description: Computes the Operating Cashflow Margin and Ratio
    //-----------------------------------------------------
    public function computeOperatingCashflow($entity_id)
    {
        /*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
        $net_sales          = 0;
        $net_sales_6m       = 0;

        /** Server Response Initialization  */
        $srv_response                       = array();
        $srv_response['sts']                = STS_NG;
        $srv_response['msg']                = STR_EMPTY;
        $srv_response['cashflow_margin']    = 0;
        $srv_response['cashflow_ratio']     = 0;

        /** Fetch Post Inputs   */
        $adb_val            = Input::get('adb_val');

        /*--------------------------------------------------------------------
        /*	Acquires Financial Report from the Database
		/*------------------------------------------------------------------*/
        $fin_report     = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        /*--------------------------------------------------------------------
        /*	Financial report exists for the current user
		/*------------------------------------------------------------------*/
        if ($fin_report) {

            /*---------------------------------------------------------------
            /*	Acquires Balance Sheet
            /*-------------------------------------------------------------*/
            $bal_report = $fin_report->balanceSheets()
                ->orderBy('index_count', 'asc')
                ->get();

            /*---------------------------------------------------------------
            /*	Acquires Income Statement
            /*-------------------------------------------------------------*/
            $inc_report = $fin_report->incomeStatements()
                ->orderBy('index_count', 'asc')
                ->get();

            /*---------------------------------------------------------------
            /*	Net Sales Computation
            /*-------------------------------------------------------------*/
            $net_sales      = (float)$inc_report[0]->Revenue;
            $net_sales_6m   = $net_sales / 2;

            /*---------------------------------------------------------------
            /*	Current Liability Computation
            /*-------------------------------------------------------------*/
            $curr_liab      = (float)$bal_report[0]->CurrentLiabilities;

            /*---------------------------------------------------------------
            /*	The Money inserted on the Template is by thousands
            /*-------------------------------------------------------------*/
            if ('1000' == $fin_report->money_factor) {
                $net_sales_6m   = $net_sales_6m * 1000;
                $curr_liab      = $curr_liab * 1000;
            }

            $cashFlow = $fin_report->cashFlow()
            	->get();

            $cashFlowValue = 0;
           	foreach ($cashFlow as $cf) {
           		if ($cf->NetCashByOperatingActivities > 0) {
           			$cashFlowValue = $cf->NetCashByOperatingActivities;
           			break;
           		}
           	}

           	$adb_val = $adb_val ?: $cashFlowValue;

            /*---------------------------------------------------------------
            /*	Prevents division by zero of net sales
            /*-------------------------------------------------------------*/
            if (0 != $net_sales_6m) {
                $srv_response['cashflow_margin']    = $adb_val / $net_sales_6m;
            }

            /*---------------------------------------------------------------
            /*	Prevents division by zero of current liability
            /*-------------------------------------------------------------*/
            if (0 != $curr_liab) {
                $srv_response['cashflow_ratio']     = $adb_val / $curr_liab;
            }

            /*---------------------------------------------------------------
            /*	Calculation Successful
            /*-------------------------------------------------------------*/
            $srv_response['sts']    = STS_OK;
            $srv_response['msg']    = ' (Auto-compute Successful) ';
        }
        /*--------------------------------------------------------------------
        /*	No Financial Statement Template was uploaded
		/*------------------------------------------------------------------*/
        else {
            $srv_response['msg']    = 'Invalid data found on FS Template. Please Re-upload';
        }

        $srv_response['cashflow_margin']    = round($srv_response['cashflow_margin'], 2);
        $srv_response['cashflow_ratio']     = round($srv_response['cashflow_ratio'], 2);

        return json_encode($srv_response);
    }

	//-----------------------------------------------------
    //  Function 39.14: getOCRSDK2
	//  Description: another page for OCR testing
    //-----------------------------------------------------
	public function getOCRSDK2()
	{
		return View::make('sample_ocrsdk2', ['response'=>'', 'parsed'=>[]]);
	}

	//-----------------------------------------------------
    //  Function 39.15: postOCRSDK2
	//  Description: post process for another page for OCR testing
    //-----------------------------------------------------
	public function postOCRSDK2()
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$image = Input::file('imagefile');
		$parsed = [];
		$response = '';
		$searchflag = true;

		if($image){

			$image->move('temp', $image->getClientOriginalName());
			$fileName = $image->getClientOriginalName();
			$filePath = getcwd().'/temp/'.$fileName;
			if(!file_exists($filePath))
			{
			die('File '.$filePath.' not found.');
			}
			if(!is_readable($filePath) )
			{
			 die('Access to file '.$filePath.' denied.');
			}
			$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

			// Send HTTP POST request and ret xml response
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

			$post_array = array();
			if((version_compare(PHP_VERSION, '5.5') >= 0)) {
				$post_array["my_file"] = new CURLFile($filePath);
			} else {
				$post_array["my_file"] = "@".$filePath;
			}

			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
			$response = curl_exec($curlHandle);

			if($response == FALSE) {
				$errorText = curl_error($curlHandle);
				curl_close($curlHandle);
				die($errorText);
			}

			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// Parse xml response
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
				   die($xml->message);
				}
				die("unexpected response ".$response);
			}

			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus != "Queued") {
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Task id
			$taskid = $arr["id"];

			// 4. Get task information in a loop until task processing finishes
			// 5. If response contains "Completed" staus - extract url with result
			// 6. Download recognition result (text) and display it
			$url = 'http://cloud.ocrsdk.com/getTaskStatus';
			$qry_str = "?taskid=$taskid";
			// Check task status in a loop until it is finished
			// Note: it's recommended that your application waits
			// at least 2 seconds before making the first getTaskStatus request
			// and also between such requests for the same task.
			// Making requests more often will not improve your application performance.
			// Note: if your application queues several files and waits for them
			// it's recommended that you use listFinishedTasks instead (which is described
			// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
			while(true)
			{
				sleep(5);
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
				curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
				curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curlHandle);
				$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
				curl_close($curlHandle);

				// parse xml
				$xml = simplexml_load_string($response);
				if($httpCode != 200) {
				  if(property_exists($xml, "message")) {
					die($xml->message);
				  }
				  die("Unexpected response ".$response);
				}
				$arr = $xml->task[0]->attributes();
				$taskStatus = $arr["status"];
				if($taskStatus == "Queued" || $taskStatus == "InProgress") {
				  // continue waiting
				  continue;
				}
				if($taskStatus == "Completed") {
				  // exit this loop and proceed to handling the result
				  break;
				}

				// error handling
				if($taskStatus == "ProcessingFailed") {
					if (env("APP_ENV") == "Production") {
						try{
							Mail::send('emails.ocr_fail', array(
								'error_message' => "Task processing failed: ".$arr["error"]
							), function($message){
								$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
								->subject('Error in OCR SDK!');
							});
						}catch(Exception $e){
			            //
			            }

					}
					die("Task processing failed: ".$arr["error"]);
				}
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Result is ready. Download it
			$url = $arr["resultUrl"];
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// Warning! This is for easier out-of-the box usage of the sample only.
			// The URL to the result has https:// prefix, so SSL is required to
			// download from it. For whatever reason PHP runtime fails to perform
			// a request unless SSL certificate verification is off.
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curlHandle);
			curl_close($curlHandle);

			$response_data = explode("\n", $response);
			$search_flag = '';
			$shareholders = [];

			for($x=0; $x<@count($response_data); $x++){
				//Company Reg. No.
				if(strpos(strtolower($response_data[$x]), 'company') !== false &&
					strpos(strtolower($response_data[$x]), 'reg') !== false &&
					strpos(strtolower($response_data[$x]), 'no') !== false
				){
					$parsed['company_reg_no'] = preg_replace("/[^0-9]/","",$response_data[$x]);
				}
				//company_name
				if(strpos(strtolower($response_data[$x]), 'this is to certify that the amended articles of incorporation') !== false){
					if(trim($response_data[$x+1])!=""){
						$parsed['company_name'] = trim($response_data[$x+1]);
					} else {
						$parsed['company_name'] = trim($response_data[$x+2]);
					}
				}
				//share holders & directors
				if((strpos(strtolower($response_data[$x]), 'names') !== false ||
				strpos(strtolower($response_data[$x]), 'name') !== false) &&
				((strpos(strtolower($response_data[$x]), 'nationality') !== false ||
				strpos(strtolower($response_data[$x]), 'citizenship') !== false) ||
				(strpos(strtolower($response_data[$x]), 'residence') !== false ||
				strpos(strtolower($response_data[$x]), 'address') !== false)
				) && @count(explode("  ", trim($response_data[$x]))) > 1 && $searchflag){
					$searchflag = false;
					$x++;
					while(trim($response_data[$x])=="") $x++;

					while(trim($response_data[$x])!=""){
						$pieces = explode("  ", trim($response_data[$x]));
						if(@count($pieces)>1){
							$nflag = true;
							for($y=0;$y<@count($pieces);$y++){
								if(trim($pieces[$y])!="" && $nflag) {
									if(@count(explode(" ", trim($pieces[$y]))) > 1 &&
										preg_match('/^[A-Z]/', trim($pieces[$y]))){
										if(!in_array(trim($pieces[$y]), $shareholders)){
											$shareholders[] = $pieces[$y];
										}
									}
									$nflag = false;
								}
							}
						}
						$x++;
					}
				}

			}

			$parsed['shareholders'] = $shareholders;

		}

		return View::make('sample_ocrsdk2', [
			'response' => $response,
			'parsed' => $parsed
		]);
	}

	//-----------------------------------------------------
    //  Function 39.16: getTestAddressBreakdown
	//  Description: get test page for Geocoder address breakdown
    //-----------------------------------------------------
	public function getTestAddressBreakdown()
	{
		return View::make('address_breakdown', array('address'=>'', 'data'=>null));
	}

	//-----------------------------------------------------
    //  Function 39.17: postTestAddressBreakdown
	//  Description: post process for Geocoder address breakdown
    //-----------------------------------------------------
	public function postTestAddressBreakdown()
	{
		$data = Geocoder::getAddressBreakdown(Input::get('address'), true);
		return View::make('address_breakdown', array('address'=>Input::get('address'), 'data'=>$data));
	}

	//-----------------------------------------------------
    //  Function 39.18: getTestImagick
	//  Description: get test OCR page for articles of incorporation
    //-----------------------------------------------------
	public function getTestImagick()
	{
		return View::make('test_imagick', [
			'response' => $response,
			'parsed' => $parsed
		]);
	}

	//-----------------------------------------------------
    //  Function 39.19: postTestImagick
	//  Description: post process for OCR page for articles of incorporation
    //-----------------------------------------------------
	public function postTestImagick()
	{
		$applicationId = 'Document Reader CBPO';
		$password = 'VFOCNtT9o8600d3enZJ6zXeY';
		$image = Input::file('imagefile');
		if($image){
			$image->move('temp', $image->getClientOriginalName());
			$cfileName = $image->getClientOriginalName();
			$cfilePath = getcwd().'/temp/'.$cfileName;

			exec('convert '.$cfilePath.'[0] '.getcwd().'/temp/output.jpg');

			$filePath = getcwd().'/temp/output.jpg';

			if(!file_exists($filePath))
			{
			die('File '.$filePath.' not found.');
			}
			if(!is_readable($filePath) )
			{
			 die('Access to file '.$filePath.' denied.');
			}
			$url = 'http://cloud.ocrsdk.com/processImage?language=english&exportFormat=txt';

			// Send HTTP POST request and ret xml response
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
			curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

			$post_array = array();
			if((version_compare(PHP_VERSION, '5.5') >= 0)) {
				$post_array["my_file"] = new CURLFile($filePath);
			} else {
				$post_array["my_file"] = "@".$filePath;
			}

			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
			$response = curl_exec($curlHandle);

			if($response == FALSE) {
				$errorText = curl_error($curlHandle);
				curl_close($curlHandle);
				die($errorText);
			}

			$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);

			// Parse xml response
			$xml = simplexml_load_string($response);
			if($httpCode != 200) {
				if(property_exists($xml, "message")) {
				   die($xml->message);
				}
				die("unexpected response ".$response);
			}

			$arr = $xml->task[0]->attributes();
			$taskStatus = $arr["status"];
			if($taskStatus != "Queued") {
				if (env("APP_ENV") == "Production") {

					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Task id
			$taskid = $arr["id"];

			// 4. Get task information in a loop until task processing finishes
			// 5. If response contains "Completed" staus - extract url with result
			// 6. Download recognition result (text) and display it
			$url = 'http://cloud.ocrsdk.com/getTaskStatus';
			$qry_str = "?taskid=$taskid";
			// Check task status in a loop until it is finished
			// Note: it's recommended that your application waits
			// at least 2 seconds before making the first getTaskStatus request
			// and also between such requests for the same task.
			// Making requests more often will not improve your application performance.
			// Note: if your application queues several files and waits for them
			// it's recommended that you use listFinishedTasks instead (which is described
			// at http://ocrsdk.com/documentation/apireference/listFinishedTasks/).
			while(true)
			{
				sleep(5);
				$curlHandle = curl_init();
				curl_setopt($curlHandle, CURLOPT_URL, $url.$qry_str);
				curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlHandle, CURLOPT_USERPWD, "$applicationId:$password");
				curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
				curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curlHandle);
				$httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
				curl_close($curlHandle);

				// parse xml
				$xml = simplexml_load_string($response);
				if($httpCode != 200) {
				  if(property_exists($xml, "message")) {
					die($xml->message);
				  }
				  die("Unexpected response ".$response);
				}
				$arr = $xml->task[0]->attributes();
				$taskStatus = $arr["status"];
				if($taskStatus == "Queued" || $taskStatus == "InProgress") {
				  // continue waiting
				  continue;
				}
				if($taskStatus == "Completed") {
				  // exit this loop and proceed to handling the result
				  break;
				}

				// error handling
				if($taskStatus == "ProcessingFailed") {
					if (env("APP_ENV") == "Production") {
						try{
							Mail::send('emails.ocr_fail', array(
								'error_message' => "Task processing failed: ".$arr["error"]
							), function($message){
								$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
								->subject('Error in OCR SDK!');
							});
						}catch(Exception $e){
			            //
			            }

					}
					die("Task processing failed: ".$arr["error"]);
				}
				if (env("APP_ENV") == "Production") {
					try{
						Mail::send('emails.ocr_fail', array(
							'error_message' => "Unexpected task status ".$taskStatus
						), function($message){
							$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('Error in OCR SDK!');
						});
					}catch(Exception $e){
		            //
		            }
				}
				die("Unexpected task status ".$taskStatus);
			}

			// Result is ready. Download it
			$url = $arr["resultUrl"];
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
			// Warning! This is for easier out-of-the box usage of the sample only.
			// The URL to the result has https:// prefix, so SSL is required to
			// download from it. For whatever reason PHP runtime fails to perform
			// a request unless SSL certificate verification is off.
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curlHandle);
			curl_close($curlHandle);

			$response_data = explode("\n", $response);
			$search_flag = '';
			$shareholders = [];

			for($x=0; $x<@count($response_data); $x++){
				//Company Reg. No.
				if(strpos(strtolower($response_data[$x]), 'company') !== false &&
					strpos(strtolower($response_data[$x]), 'reg') !== false &&
					strpos(strtolower($response_data[$x]), 'no') !== false
				){
					$parsed['company_reg_no'] = preg_replace("/[^0-9]/","",$response_data[$x]);
				}
				//company_name
				if(strpos(strtolower($response_data[$x]), 'this is to certify that the amended articles of incorporation') !== false){
					if(trim($response_data[$x+1])!=""){
						$parsed['company_name'] = trim($response_data[$x+1]);
					} else {
						$parsed['company_name'] = trim($response_data[$x+2]);
					}
				}
			}

		}

		return View::make('test_imagick', [
			'response' => $response,
			'parsed' => $parsed
		]);
	}

}
