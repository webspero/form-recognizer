<?php

use CreditBPO\Services\MailHandler;
use CreditBPO\Libraries\PaymentCalculator;
use CreditBPO\Models\Zipcode;
use \Exception as Exception;

//======================================================================
//  Class 52: Transaction Controller
//      Functions related to DragonPay are routed to this class
//======================================================================
class TransactionController extends BaseController {

	private $url;                           // DragonPay URL
	private $merchant_id = 'CREDITBPO';     // Merchant ID to accesss API
	private $merchant_key = 'N6f9rEW3';     // Merchat Key for API access

    //-----------------------------------------------------
    //  Function 52.1: __construct
    //      Initialization
    //-----------------------------------------------------
	public function __construct()
	{
        /*  For Production Environment  */
		if (env("APP_ENV") == "Production") {
			$this->url = 'https://gw.dragonpay.ph/Pay.aspx';
		}
        /*  For Testing Environment     */
        else {
			$this->url = 'http://test.dragonpay.ph/Pay.aspx';
		}
	}

    //-----------------------------------------------------
    //  Function 52.2: paymentIndex
    //      Displays the payment needed page
    //-----------------------------------------------------
	public function paymentIndex()
	{
        /*  Not logged in   */
		if (Input::has('id')) {
			$entity_id = Input::get('id');
		}
        /*  User is logged in   */
        else {
			$entity     = Entity::where('loginid', Auth::user()->loginid)->where('is_paid', 0)->first();
			$entity_id  = $entity->entityid;
		}

		return View::make('payment.index', ['entity_id'=>$entity_id]);
	}

    //-----------------------------------------------------
    //  Function 52.3: paymentSelect
    //      Displays the choose payment method page
    //-----------------------------------------------------
	public function paymentSelect()
	{
		$reportId = Input::get('id');

		$user = User::find(Auth::user()->loginid);

		if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name == "")) {
				
			$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
			
			if($user->role == 4) { //supervisor
				return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
			} else { //basic user
				return Redirect::to('/billingdetails')->withErrors($error)->withInput();
			}
			
		}

		/* URL doesn't have report id parameter */
		if (!$reportId){
			/* Get the first unpaid report */
			$entity = Entity::where('loginid', Auth::user()->loginid)->where('is_paid', 0)->first();
			$reportId = $entity->entityid;
		}
		
		$entity = Entity::where('entityid', $reportId);		
		$entity = $entity->first();

        /*  Report exists   */
		if ($entity) {
			/* Set price information */
			$paymentCalculator = new PaymentCalculator;
			$tax = $paymentCalculator::TAX;

			$bank = Bank::where('id', '=', $entity->current_bank)->first();
			$locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

			if($bank->is_custom != 0) {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $bank->custom_price + $locPremiumReportPrice->premiumReportPrice;
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
					$price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
				} else { //Standalone Report
					$price = $bank->custom_price;
				}
			} else {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $paymentCalculator::REPORT_PRICE + $locPremiumReportPrice->premiumReportPrice; //$paymentCalculator::PREMIUM_REPORT_PRICE;
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Premium Report
					$price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
				} else { // Standalone Report
					$price = $paymentCalculator::REPORT_PRICE;
				}
			}
			
			/* Price and amount are same here since the quantity is 1 in purchasing report.
			But I'll still declare amount variable for using right terms during calculation */
			$amount = $price;
			$subTotal = $amount;
			$displayDiscount = null;

            /*  Check Discount  */
			$discount = Discount::where('discount_code', $entity->discount_code)->first();

			/*  There is a discount */
			if ($discount){
				if ($discount->type == $paymentCalculator::DISCOUNT_TYPE_PERCENTAGE){
					$displayDiscount = '(less: '. $discount->amount .'%)';
				} else{
					$displayDiscount = '(less: '. number_format($discount->amount, 2, '.', ',').')';
				}

				/* Get subtotal*/
				$subTotal = $paymentCalculator->computeSubtotal($amount, $discount->amount, $discount->type);

                /*  Free Trial  */
                if (0 >= $subTotal) {
                    $entity_upd  = DB::table('tblentity')
                        ->where('loginid', Auth::user()->loginid)
                        ->update(
                            array(
                                'is_paid' => 1
                            )
                        );

                    $account_upd    = DB::table('tbllogin')
                        ->where('loginid', Auth::user()->loginid)
                        ->update(
                            array(
                                'status'    => 2
                            )
                        );

                    return Redirect::to('/dashboard/index');
                }
			}
			$totalAmount = $paymentCalculator->computeTotalAmountBySubTotal($subTotal);
			
			/*set up display message*/
			$displayMessage = sprintf("Php %s %s (+ %s%% tax) = Php %s*", number_format($amount,2), ($displayDiscount != null ? $displayDiscount : ''), ($tax * 100), number_format($totalAmount,2));

			return View::make('payment.payment_selection', array(
				'display_amount' => 'CreditBPO Rating Report®: ' . $displayMessage,
				'entity_id' => $reportId
			));

		}
        /*  Report does not exist   */
        else {
			return Redirect::to('/');
		}
	}

    //-----------------------------------------------------
    //  Function 52.4: paymentCheckout
    //      Checkout via DragonPay
    //-----------------------------------------------------
	public function paymentCheckout($id)
	{
        /*  Get report information  */
		$entity = Entity::where('entityid', $id)
            ->where('is_paid', 0)
            ->first();

		/* Set price information */
		$paymentCalculator = new PaymentCalculator;
		
		$bank = Bank::where('id', '=', $entity->current_bank)->first();

		if($bank->is_custom != 0){
			$price = $bank->custom_price;	
		} else {
			$price = $paymentCalculator::REPORT_PRICE;
		}
		
		$discount = null;
		$discountType = null;

        /*  When there is a discount code   */
		if ($entity->discount_code!=null) {
            $discount_data = Discount::where('discount_code', $entity->discount_code)->first();

            if ($discount_data){
				$discount = $discount_data->amount;
				$discountType = $discount_data->type;
            }
        }
        /* Compute Payment */
		$result = $paymentCalculator->paymentCompute($price, 1, $discount, $discountType);
		$rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
		
        /*  Save transaction record to the database */
		$transaction                = new Transaction();
		$transaction->loginid       = Auth::user()->loginid;
		$transaction->entity_id     = $id;
		$transaction->amount        = $result['totalAmount'];
		$transaction->ccy           = "PHP";
		$transaction->description   = "CreditBPO Rating Report®";
		$transaction->save();

		$entity->transaction_id = $transaction->id;
		$entity->save();

        /*  Sets parameters to be passed to DragonPay   */
		$data = array(
			'merchant_id'   => $this->merchant_id,
			'txn_id'        => $transaction->id . $rand,
			'amount'        => number_format($transaction->amount, 2, ".", ""),
			'currency'      => $transaction->ccy,
			'description'   => $transaction->description,
			'email'         => Auth::user()->email,
			'key'           => $this->merchant_key
		);

		$seed   = implode(':', $data);
		$digest = Sha1($seed);
		
		/*  Create a payment receipt record on the database */
        $paymentReceiptDb = new PaymentReceipt;

        $paymentReceipt['price'] = $price;
        $paymentReceipt['amount'] = $result['amount'];
        $paymentReceipt['discount'] = $result['discountAmount'];
        $paymentReceipt['subtotal'] = $result['subTotal'];
        $paymentReceipt['vat_amount'] = $result['taxAmount'];
        $paymentReceipt['total_amount'] = $result['totalAmount'];
        $paymentReceipt['item_quantity'] = 1;
        $paymentReceipt['login_id'] = Auth::user()->loginid;
        $paymentReceipt['reference_id'] = $transaction->id;
        $paymentReceipt['payment_method'] = 2; /*DragonPay*/
        $paymentReceipt['item_type'] = 1;            /*  SME Account   */
        $paymentReceipt['sent_status'] = STS_NG;
        $paymentReceipt['date_sent'] = date('Y-m-d');
        /* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
        $paymentReceipt['vatable_amount'] = null;
        $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

        /*  Send data to DragonPay  */
		return Redirect::to(
			$this->url.
			"?merchantid=".$data['merchant_id'].
			"&txnid=".$data['txn_id'].
			"&amount=".$data['amount'].
			"&ccy=".$data['currency'].
			"&description=".urlencode($data['description']).
			"&email=".urlencode($data['email']).
			"&digest=".$digest
		);
	}

    //-----------------------------------------------------
    //  Function 52.5: paymentReturn
    //      Callback function for DragonPay after payment
    //-----------------------------------------------------
	public function paymentReturn()
	{
		/*  Get the response from DragonPay */
		$ctxnId = substr(Input::get('txnid'),0,-10);
		$response = array(
			'id'        => Input::get('txnid'),
			'refno'     => Input::get('refno'),
			'status'    => Input::get('status'),
			'message'   => Input::get('message'),
			'key'       => $this->merchant_key
		);
		
		$digest_check   = Input::get('digest');
		$seed           = implode(':', $response);
		$digest         = Sha1($seed);

		/*  Log DragonPay activity  */
		$file   = public_path()."/dragonpay_logs/dragonpay.log";
		$string = date('c') . " => " . Request::server('HTTP_HOST') . Request::server('REQUEST_URI') . "\r\n";

        if (file_exists($file)) {
			if (filesize($file) > 5242880) {
				rename($file, "dragonpay_logs/" . date('ymd') . "_dragonpay.log");
			}
		}

		file_put_contents($file, $string , FILE_APPEND | LOCK_EX);
		$response['id'] = $ctxnId;
		if ($digest_check == $digest) {
			$transaction = Transaction::find($response['id']);
			$create_keys = false;

			if ($transaction->status == NULL OR $transaction->status == "P") {
				/*  Update Database Record  */
				$create_keys            = true;

				$transaction->refno     = $response['refno'];
				$transaction->status    = $response['status'];
				$transaction->message   = $response['message'];
				$transaction->save();
			}

            /*  Get Account Information */
			$user = User::where('loginid', $transaction->loginid)->first();

            /*  Account is Supervisor   */
			if ($user->role==4) {
                /*  Transaction Successful   */
				if ($response['status'] == "S") {

                    /*  Bank Subscription   */
					if ($transaction->description == "Bank User Subscription") {

                        /*  Create or extend bank Subscription  */
						if ($create_keys) {
							$subs = BankSubscription::firstOrCreate(array(
								'bank_id' => $user->loginid
							));

							$date = new DateTime();
							$start_day = $date->format('j');

							$date->modify("+1 month");
							$end_day = $date->format('j');

							if ($start_day != $end_day)
								$date->modify('last day of last month');

							$subs->expiry_date = $date->format('Y-m-d H:i:s');
							$subs->save();
						}

                        /*  Create or extend bank Subscription  */
                        if (3 == $user->product_plan) {
                            $user->product_plan    = 1;
                            $user->save();
						}

						/*  Email Receipt to Client */
                        $receipt = PaymentReceipt::where('login_id', $user->loginid)
                            ->where('reference_id', $response['id'])
                            ->where('sent_status', STS_NG)
                            ->where('payment_method', 2)
							->first();

                        if ($receipt) {
                            $receipt->sent_status = STS_OK;
                            $receipt->save();
						
							if(env("APP_ENV") != "local"){
								$user = User::find(Auth::user()->loginid);

								if($user->parent_user_id != 0) {
									$user = User::find($user->parent_user_id);
								}

								$mailHandler = new MailHandler();
								$mailHandler->sendReceipt(
									Auth::user()->loginid,
									[
										'receipt' => $receipt,
										'user' => $user,
									],
									'CreditBPO Payment Receipt'
								);
							}
						}

						return Redirect::to('/bank/dashboard');
					}
                    /*  Supervisor Keys Payment */
					else {
                        /*  Email Receipt to Client */
                        $receipt = PaymentReceipt::where('login_id', $user->loginid)
                            ->where('reference_id', $response['id'])
                            ->where('sent_status', STS_NG)
                            ->where('payment_method', 2)
							->first();

                        if ($receipt) {
                            $receipt->sent_status = STS_OK;
                            $receipt->save();

							if(env("APP_ENV") != "local"){
								$user = User::find(Auth::user()->loginid);

								if($user->parent_user_id != 0) {
									$user = User::find($user->parent_user_id);
								}

								$mailHandler = new MailHandler();
								$mailHandler->sendReceipt(
									Auth::user()->loginid,
									[
										'receipt' => $receipt,
										'user' => $user
									],
									'CreditBPO Payment Receipt'
								);
							}
                            BankSerialKeys::createKeys($user->loginid, $receipt->item_quantity);
                        }
						return Redirect::to('/bank/keys');
					}
				}
                /*  Transaction Pending */
				else if ($response['status'] == "P") {
					return View::make('payment.pending', array('refno' => $response['refno']));
				}
                /*  Transaction Error   */
				else {
					return View::make('payment.error');
				}
			}
            /*  Account is SME  */
            else {
                /*  Transaction Successful  */
				if ($response['status'] == "S") {
					/* Check if this is the first report of user */
					$isFirst = false;
					$userReports = Entity::where('loginid', $user->loginid)->get();
					if (!$userReports->isEmpty()){
						if ($userReports->count() == 1){
							$isFirst = true;
							$user->status = 2;
							$user->save();
						}
					}
						
					$entity = Entity::where('entityid', $transaction->entity_id)
							->where('is_paid', 0)->first();

					if ($create_keys) {
						// set paid on first unpaid entity
						$entity->is_paid = 1;
						$entity->save();
					}

                    /*--------------------------------------------------------------------
                    /*	Email Receipt to Client
                    /*------------------------------------------------------------------*/
					$receipt = PaymentReceipt::where('login_id', Auth::user()->loginid)
							 ->where('reference_id', $response['id'])
							 ->where('sent_status', STS_NG)
							 ->where('payment_method', 2)
							 ->first();
                    $receipt->sent_status = STS_OK;
					$receipt->save();
					
					if (env("APP_ENV") != "local") {
						$user = User::find(Auth::user()->loginid);

						if($user->parent_user_id != 0) {
							$user = User::find($user->parent_user_id);
						}

						$mailHandler = new MailHandler();
                    	$mailHandler->sendReceipt(
							Auth::user()->loginid,
							[
								'receipt' => $receipt,
								'entity' => $entity,
								'user' => $user
							],
							'CreditBPO Payment Receipt'
						);

						if($entity->is_premium == 1){
							$mailHandler->sendPremiumReportNotification($entity);
						}

					}

					if ($isFirst && ($entity->is_premium == STANDALONE_REPORT || $entity->is_premium = SIMPLIFIED_REPORT)) {
						return View::make('signup_confirmation');
					} elseif ($entity->is_premium == PREMIUM_REPORT) {
						$region = Zipcode::where('id', $entity->cityid)->first();

						$regionArea = $region->region;
	
						if($regionArea == "NCR"){
							return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 business days.');
						}else{
							return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 10 business days.');
						}
					}
                    else {
						return Redirect::to('/dashboard/index');
					}
				}
                /*  Transaction Pending */
				else if ($response['status'] == "P") {
					return View::make('payment.pending', array('refno' => $response['refno']));
				}
                /*  Transaction Error   */
				else{
					return View::make('payment.error');
				}
			}
		}
		else {
			return Response::make('Incorrect digest.', 401);
		}
	}

    //-----------------------------------------------------
    //  Function 52.6: paymentPostback
    //      Postback function for DragonPay after payment
    //      callback is called
    //-----------------------------------------------------
	public function paymentPostback()
	{
        /*  Get the response from DragonPay */
		$response = array(
			'id'        => Input::get('txnid'),
			'refno'     => Input::get('refno'),
			'status'    => Input::get('status'),
			'message'   => Input::get('message'),
			'key'       => $this->merchant_key
		);

		$digest_check   = Input::get('digest');
		$seed           = implode(':', $response);
		$digest         = Sha1($seed);

		/*  Log DragonPay activity  */
		$file   = "dragonpay_logs/dragonpay_postback.log";
		$string = date('c') . " => " . $seed . "\r\n";

		if (file_exists($file)) {
			if (filesize($file) > 5242880) {
				rename($file, "dragonpay_logs/" . date('ymd') . "_dragonpay_postback.log");
			}
		}

		file_put_contents($file, $string , FILE_APPEND | LOCK_EX);

		if ($digest_check == $digest) {
			$transaction = Transaction::find($response['id']);
			$create_keys = false;

			if ($transaction->status == NULL OR $transaction->status == "P") {
				/*  Update Database Record  */
				$create_keys            = true;

				$transaction->refno     = $response['refno'];
				$transaction->status    = $response['status'];
				$transaction->message   = $response['message'];
				$transaction->save();

			}

            /*  Get Account Information */
			$user = User::where('loginid', $transaction->loginid)
                ->first();

            /*  Account is for Independent key buyers   */
            if ($transaction->loginid == 0) {
				if ($response['status'] == "S") {
					IndependentKey::where('email', $transaction->email)
                        ->where('batch', $transaction->batch)
                        ->update(array('is_paid'=>1));

					$oldhash    = base64_encode($transaction->email);
					$hash       = substr_replace($oldhash, $transaction->batch, 10, 0);

					/*  Email to Keys to the buyer  */
					if (env("APP_ENV") != "local") {
						try{
							Mail::send('emails.buyer_keys_paid', array(
									'hash'=>$hash
								), function($message){
									$message->to($transaction->email, $transaction->email)
										->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

									$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
									$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							});
						}catch(Exception $e){
		                //
		                }
					}
				}
			}
            /*  Account is Supervisor   */
            else if ($user->role == 4) {
                /*  Transaction Successful   */
				if ($response['status'] == "S") {

					if ($create_keys) {
						if ($transaction->description == "Bank User Subscription") {

							if ($create_keys) {
								$subs = BankSubscription::firstOrCreate(array(
									'bank_id' => $user->loginid
								));

								$date = new DateTime();
								$start_day = $date->format('j');

								$date->modify("+1 month");
								$end_day = $date->format('j');

								if ($start_day != $end_day)
									$date->modify('last day of last month');

								$subs->expiry_date = $date->format('Y-m-d H:i:s');
								$subs->save();
							}

                            if (3 == $user->product_plan) {
                                $user->product_plan    = 1;
                                $user->save();
                            }

						}
                        else {
                            /*--------------------------------------------------------------------
                            /*	Email Receipt to Client
                            /*------------------------------------------------------------------*/
                            $receipt    = PaymentReceipt::where('login_id', $user->loginid)
                                ->where('reference_id', $response['id'])
                                ->where('sent_status', STS_NG)
                                ->where('payment_method', 2)
                                ->first();

                            $receipt->sent_status = STS_OK;
                            $receipt->save();

							$user = User::find(Auth::user()->loginid);

							if($user->parent_user_id != 0) {
								$user = User::find($user->parent_user_id);
							}

							if (env("APP_ENV") != "local") {
								$mailHandler = new MailHandler();
								$mailHandler->sendReceipt(
									Auth::user()->loginid,
									[
										'receipt' => $receipt,
										'user' => $user
									],
									'CreditBPO Payment Receipt'
								);
							}
							
                            BankSerialKeys::createKeys($user->loginid, $receipt->item_quantity);
						}
					}
				}
			}
            /*  Account is SME  */
            else {
				if ($response['status'] == "S") {
					// update login status
					User::where('loginid', '=', $user->loginid)->update(array('status' => 2));

					if($create_keys){
						// set paid on first unpaid entity
						$entity = Entity::where('entityid', $transaction->entity_id)
										->where('is_paid', 0)->first();
						$entity->is_paid = 1;
						$entity->save();
					}
				}
			}
		}

		return Response::make("complete");
	}

	//-----------------------------------------------------
    //  Function 52.7: showRequirements
    //      function to display sign up confirmation
    //-----------------------------------------------------
    public function showRequirements()
    {
        return View::make('signup_confirmation');
    }
}