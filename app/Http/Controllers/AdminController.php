<?php

use CreditBPO\Models\BusinessOutlookUpdate;
use CreditBPO\Models\EmailLead;
use CreditBPO\Models\AutomateStandaloneRating;
use CreditBPO\Models\TranscribeFS;
use Illuminate\Http\Request;
use CreditBPO\Services\FinancialRating;
use CreditBPO\Models\DeveloperApiUser as ApiDeveloper;
use Illuminate\Support\Facades\Redirect;
//JM - added support paginator class
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use CreditBPO\Services\MailHandler;
use CreditBPO\Models\BlockAddress;
use \Exception as Exception;
use CreditBPO\Models\Bank;
use CreditBPO\Models\BankSerialKey;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Entity;
use Illuminate\Support\Facades\Log;
use CreditBPO\Handlers\FinancialHandler;

//======================================================================
//  Class 2: AdminController
//  Description: Contains all the functions regarding Admin Dashboard
//======================================================================
class AdminController extends BaseController {
    const BANK_TYPE_GOVERNMENT = 'Government';
	const BANK_TYPE_CORPORATE = 'Corporate';
	
	public function __construct(){
		if (Session::has('supervisor')){
			Session::forget('supervisor');
		}
	}

	//-----------------------------------------------------
    //  Function 2.1: getAdminDashboard
	//  Description: Gets the admin dashboard page
    //-----------------------------------------------------
	public function getAdminDashboard()
	{
		try{
			/* Variable declaration */
	        $mal_attmpt_lib     = new MaliciousAttemptLib; 		// Initialization of Library: MaliciousAttemptLib
	        $login_attmpt_db    = new UserLoginAttempt;			// Initialization of Library: UserLoginAttempt

	        $date_today     = date('Y-m-d H:i:s');				// Defaulted to date today with format: 2016-01-01 23:59:59
	        $login_lock     = 0;								// Variable to flag login lock
	        $user_2fa_lock  = 0;								// Variable to flag 2-factor authentication lock
	        $user_lock      = 0;								// Variable to flag user lock

			$limit = 50;										// Limit for pagination default: 15
			$users = User::where('role', '!=', '0');

			/* Applying filter to results */
			if(Input::has('role')) 								// filter for roles
			{
				$users->where('role', '=', Input::get('role'));
			}
			if(Input::has('search')) 							// filter for search ID, Email, Firstname or Lastname
			{
				$users->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
			}

			/* Pagination Code */
			//JM - updated paginator code
			$paginator = new Paginator($users->get()->toArray(), $limit); //Paginator::make($users->get()->toArray(), $users->count(), $limit);  // Laravel paginator class
			if(Input::has('page'))
			{
				$users->offset((Input::get('page') - 1) * $limit);		// compute for which page to display
			}
			$users = $users->orderBy('created_at', 'desc')->take($limit)->get(); 	// order users by descending created date

			$paginator->setPath('admin');

			/* Iterate each user to check for failed login attempts */
	        foreach ($users as $user) {
	            $user_2fa_lock      = 0; 				// Variable declaration; reset for each user to 0
	            $login_lock         = 0;				// Variable declaration; reset for each user to 0

	            $mal_attmpt_data    = $mal_attmpt_lib->check2faAttempts($user['loginid']);  // Call library function to check 2 factor authentication
	            $log_attmpt_data    = $login_attmpt_db->getUserAttempt($user['loginid']);   // Call library function to count login attempts

	            if (STS_LOCK == $mal_attmpt_data['sts']) {			// checks if user is locked for 2 factor authentication
	                $user_2fa_lock  = 1;
	            }

	            if (1 <= @count($log_attmpt_data)) {					// checks for number of login attempts
	                $log_data['attempt_id'] = $log_attmpt_data['login_attempt_id'];

	                if (MAX_LOGIN_FAILED <= $log_attmpt_data->attempts) {  // checks if login attempts exceeds MAX_LOGIN_FAILED

						// parses date for last login attmept
	                    $last_attmpt_exp    = explode(' ', $log_attmpt_data->last_login);
	                    $attmpt_date        = explode('-', $last_attmpt_exp[0]);
	                    $attmpt_time        = explode(':', $last_attmpt_exp[1]);

						// create a date that is 30 away from last login attempt
	                    $attmpt_30  = date('Y-m-d H:i:s', mktime($attmpt_time[0], $attmpt_time[1]+30, $attmpt_time[2], $attmpt_date[1], $attmpt_date[2], $attmpt_date[0]));

	                    /*----------------------------------------------------------
	                    /*	Checks if last login attempt is within 30 mins. If so, user is locked
	                    /*--------------------------------------------------------*/
	                    if (strtotime($attmpt_30) >= strtotime($date_today)) {
	                        $login_lock = 1;
	                    }
	                }
	            }

				// Apply column for locked status
	            $user_lock = $user_2fa_lock + $login_lock;

	            $user['lock'] = $user_lock;
	        }

			// display the view /views/admin/accounts.blade.php
			return View::make('admin.accounts', array(
				'users' => $users,
				'paginator' => $paginator
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.2: getAddUser
	//  Description: Gets the Add user form page
    //-----------------------------------------------------
	public function getAddUser()
	{
		try{
			$banks = array();				// variable container for banks list
			foreach(Bank::all() as $b) 		// gets all the list of banks
			{
				$banks[$b->id] = $b->bank_name;
			}
			// display the view /views/admin/adduser.blade.php
			return View::make('admin.adduser', array('banks' => $banks));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.3: postAddUser
	//  Description: Post process for adding user
    //-----------------------------------------------------
	public function postAddUser()
	{
		try{
			/* validation rules */
			$rules = array(
		        'email'	=> 'required|email',
		        'firstname'	=> 'required',
				'lastname' => 'required',
				'job_title' => 'required',
				'password' => 'required'
			);

			// run and check for validation
			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails()) 		// if validator fails, redirect to getAddUser form page and show error
			{
				return Redirect::to('/admin/add')->withErrors($validator->errors())->withInput();
			}
			else							// if success
			{
				// checks if password and confirm password match
				if(Input::get('password')!="" || Input::get('confirm_password')!="")
				{
					if(Input::get('password') != Input::get('confirm_password'))
					{
						return Redirect::to('/admin/add')->withErrors('Passwords do not match.')->withInput();
					}
				}

				// checks if email already existing
				$check_user = User::where('email', Input::get('email'))->get();
				if($check_user->count() > 0)
				{
					return Redirect::to('/admin/add')->withErrors('New email is already in use.')->withInput();
				}

				// if roles are non-bank financing, corporate, or government, Entity name should be present
				if(in_array(Input::get('role'), array('5','6','7'))){
					if(Input::get('other')==""){
						return Redirect::to('/admin/add')->withErrors('Please input entity name.')->withInput();
					}
				}

				// create new instance of class User and populate with input
				$user = new User;
				$user->firstname = Input::get('firstname');
				$user->lastname = Input::get('lastname');
				$user->job_title = Input::get('job_title');
				$user->email = Input::get('email');

				/* if user is supervisor,  set bank_id */
				if(Input::get('role') == 4)
				{
					$user->role = Input::get('role');
					$user->bank_id = Input::get('bank_id');
					if(Input::has('can_view_free_sme')){
						$user->can_view_free_sme = Input::get('can_view_free_sme');
					}
				/* if non-bank financing, corporate, or government:
				   create new entity and store in banks list */
				} elseif(in_array(Input::get('role'), array('5','6','7'))) {
					$user->role = 4;

					$bank = Bank::where('bank_name', Input::get('other'))
						->where('bank_type', 'Others')->first();		// checks if bank entity already exists
					if($bank==null){
						$bank = Bank::create(array(
							'bank_name' => Input::get('other'),
							'bank_type' => 'Others'
						));
					}
					$user->bank_id = $bank->id;
					$user->can_view_free_sme = 1;

				} else {
					$user->role = Input::get('role');
				}
				$user->status = 2;
				$user->password = Hash::make(Input::get('password'));
				$user->save();

				// save and redirect to admin dashbaord
				return Redirect::to('/admin');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.4: getEditUser
	//  Description: Get edit user form
    //-----------------------------------------------------
	public function getEditUser($loginid)
	{
		try{
			$user = User::find($loginid); 	// find the user using loginid
			$banks = array();				// variable container for banks list
			foreach(Bank::all() as $b)		// store all banks to container to be passed to view
			{
				$banks[$b->id] = $b->bank_name;
			}
			// display the view /views/admin/edituser.blade.php
			return View::make('admin.edituser', array(
				'user' => $user,
				'banks' => $banks
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.5: postEditUser
	//  Description: Post process for edit user
    //-----------------------------------------------------
	public function postEditUser($loginid)
	{
		try{
			$rules = array(  					// validation rules
		        'email'	=> 'required|email',
				'name' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);		// Run laravel validator

			if($validator->fails()) 		// if fail, redirect back to form and display errors
			{
				return Redirect::to('/admin/edit/' . $loginid)->withErrors($validator->errors())->withInput();
			}
			else
			{
				// checks if password and confirm password match
				if(Input::get('password')!="" || Input::get('confirm_password')!="")
				{
					if(Input::get('password') != Input::get('confirm_password'))
					{
						return Redirect::to('/admin/edit/' . $loginid)->withErrors('Passwords do not match.')->withInput();
					}
				}

				/* get user from db and update based on edit inputs */
				$user = User::find($loginid);
				if($user->email != Input::get('email')){
					$check_user = User::where('email', Input::get('email'))->get();
					// if email is edited, check if new email already exist
					// if so, redirect back to form and show error
					if($check_user->count() > 0)
					{
						return Redirect::to('/admin/edit/' . $loginid)->withErrors('New email is already in use.')->withInput();
					}
					else
					{
						$user->email = Input::get('email');
					}
				}
				// populate input fields
				$user->name = Input::get('name');
				$user->job_title = Input::get('job_title');
				if($user->role == 4)	// if user role is bank/supervisor update "Bank user can view SMEs without requesting bank" field
				{
					$user->bank_id = Input::get('bank_id');
					if(Input::has('can_view_free_sme') && Input::has('can_view_free_sme') == 1){
						$user->can_view_free_sme = 1;
					}
					else
					{
						$user->can_view_free_sme = 0;
					}
				}
				if(Input::get('password')!=""){
					$user->password = Hash::make(Input::get('password')); 	// encrypt new password
				}

				$user->save();
				// save and redirect to admin dashbaord
				return Redirect::to('/admin');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.6: getIndustry
	//  Description: get industry weights page
    //-----------------------------------------------------
	public function getIndustry($id=0)
	{
		try{
			if($id==0){
				$industry = Industrymain::all();	// get all industry main from db
				$is_main = true;
			}
			else { // if id is passed, get industry weights for bank id
				$industry = DB::table('tblbankindustryweights')
				->select('tblbankindustryweights.*', 'tblindustry_main.main_title')
				->join('tblindustry_main', 'tblindustry_main.industry_main_id', '=', 'tblbankindustryweights.industry_id')
				->where('bank_id', $id)->get();
				$is_main = false;
			}

			// get all banks and add starting item "Credit BPO Standards"
			//JM - changed lists to pluck
			$banks = array('0' => 'Credit BPO Standards') + Bank::all()->pluck('bank_name', 'id')->toArray();
			// display the view /views/admin/industry.blade.php
			return View::make('admin.industry', array(
				'industry' => $industry,
				'banks' => $banks,
				'is_main' => $is_main,
				'bank_id' => $id
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.7: getEditIndustry
	//  Description: get industry weights edit form page
    //-----------------------------------------------------
	public function getEditIndustry($industry_id)
	{
		try{
			// get industry details from db
			$industry = Industrymain::where('industry_main_id', $industry_id)
	                ->leftJoin('business_outlooks', 'business_outlooks.industry_id', '=', 'tblindustry_main.industry_main_id')
	                ->first();
	        // display the view /views/admin/editindustry.blade.php
			return View::make('admin.editindustry', array(
				'industry' => $industry
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.8: postEditIndustry
	//  Description: post process for edit industry form submit
    //-----------------------------------------------------
	public function postEditIndustry($industry_id)
	{
		try{
	        $biz_outlook_db     = new BusinessOutlook;			// create new instance of BusinessOutlook
	        $regression_lib     = new RegressionCalcLib;		// create new instance of RegressionCalcLib

			$rules = array(										// validation rules
		        'business_group'	=> 'required|numeric',
		        'management_group'	=> 'required|numeric',
				'financial_group' 	=> 'required|numeric',
				'biz-outlook-q1' 	=> 'required|numeric',
				'biz-outlook-q2' 	=> 'required|numeric',
				'biz-outlook-q3' 	=> 'required|numeric',
				'biz-outlook-q4' 	=> 'required|numeric',
				'biz-outlook-q5' 	=> 'required|numeric',
				'biz-outlook-q6' 	=> 'required|numeric',
				'biz-outlook-q7' 	=> 'required|numeric',
				'biz-outlook-q8' 	=> 'required|numeric'
			);

			$validator = Validator::make(Input::all(), $rules);		// run validator

			if($validator->fails()) // if validator fails redirect back and display error
			{
				return Redirect::to('/admin/industry/edit/' . $industry_id)->withErrors($validator->errors())->withInput();
			}
			else
			{
				$industry = Industrymain::find($industry_id);						// get current industry and
				$industry->business_group = Input::get('business_group');			// populate with input
				$industry->management_group = Input::get('management_group');
				$industry->financial_group = Input::get('financial_group');
				$industry->industry_trend = Input::get('industry_trend');

	            $biz_outlook_data['biz-outlook-q1'] = Input::get('biz-outlook-q1');
	            $biz_outlook_data['biz-outlook-q2'] = Input::get('biz-outlook-q2');
	            $biz_outlook_data['biz-outlook-q3'] = Input::get('biz-outlook-q3');
	            $biz_outlook_data['biz-outlook-q4'] = Input::get('biz-outlook-q4');
	            $biz_outlook_data['biz-outlook-q5'] = Input::get('biz-outlook-q5');
	            $biz_outlook_data['biz-outlook-q6'] = Input::get('biz-outlook-q6');
	            $biz_outlook_data['biz-outlook-q7'] = Input::get('biz-outlook-q7');
	            $biz_outlook_data['biz-outlook-q8'] = Input::get('biz-outlook-q8');

	            /*----------------------------------------------------------------
	            /*	Saves the business outlook data to the Database
	            /*--------------------------------------------------------------*/
	            $biz_outlook_db->addUpdateBusinessOutlook($industry_id, $biz_outlook_data);

	            /*----------------------------------------------------------------
	            /*	Calculate slope of Business Outlook
	            /*--------------------------------------------------------------*/
	            $xy_list    = array(
	                0   => array($biz_outlook_data['biz-outlook-q1'], 1),
	                1   => array($biz_outlook_data['biz-outlook-q2'], 2),
	                2   => array($biz_outlook_data['biz-outlook-q3'], 3),
	                3   => array($biz_outlook_data['biz-outlook-q4'], 4),
	                4   => array($biz_outlook_data['biz-outlook-q5'], 5),
	                5   => array($biz_outlook_data['biz-outlook-q6'], 6),
	                6   => array($biz_outlook_data['biz-outlook-q7'], 7),
	                7   => array($biz_outlook_data['biz-outlook-q8'], 8)
	            );

	            $regression_results = $regression_lib->calculateRegression($xy_list);

	            /*----------------------------------------------------------------
	            /*	Determines the Industry Trend and save into database
	            /*--------------------------------------------------------------*/
	            $industry->industry_score   = $regression_results[REGRESSION_PTS_ALLOC];

				switch($regression_results[REGRESSION_PTS_ALLOC]){
	                case BIZ_OUTLOOK_UP_POINT:
	                    $industry->industry_trend = BIZ_OUTLOOK_UP_TREND;
	                    break;

	                case BIZ_OUTLOOK_FLAT_POINT:
	                    $industry->industry_trend = BIZ_OUTLOOK_FLAT_TREND;
	                    break;

	                case BIZ_OUTLOOK_DOWN_POINT:
	                    $industry->industry_trend = BIZ_OUTLOOK_DOWN_TREND;
	                    break;
				}

				$industry->save();
				// save and redirect to industry weights page
				return Redirect::to('/admin/industry');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.9: postResetIndustry
	//  Description: post process for resetting industry weights to default
    //-----------------------------------------------------
	public function postResetIndustry()
	{
		try{
			if(Input::get('bank_id')==0){		// reset for industry main
				// initialize variable for defaults
			$reset_data = array(
				1 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25, 'industry_trend'=>'Flat', 'industry_score'=>53),
				2 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25, 'industry_trend'=>'Flat', 'industry_score'=>53),
				3 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Flat', 'industry_score'=>53),
				4 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Flat', 'industry_score'=>53),
				5 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Flat', 'industry_score'=>53),
				6 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Flat', 'industry_score'=>53),
				7 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Flat', 'industry_score'=>53),
				8 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				9 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Flat', 'industry_score'=>53),
				10 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Upward', 'industry_score'=>80),
				11 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Upward', 'industry_score'=>80),
				12 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40, 'industry_trend'=>'Upward', 'industry_score'=>80),
				13 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				14 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				15 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				16 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				17 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80),
				18 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35, 'industry_trend'=>'Upward', 'industry_score'=>80)
			);

				foreach($reset_data as $key=>$value){
					$industry = Industrymain::find($key)->update($value);
				}

	            $businessOutlookUpdate = new BusinessOutlookUpdate();
	            $businessOutlookUpdate->reset();
			}
			else {								// reset for bank industry
				// initialize variable for defaults
			$reset_data = array(
				1 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25),
				2 => array('business_group'=>40, 'management_group'=>35, 'financial_group'=>25),
				3 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				4 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				5 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				6 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				7 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				8 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				9 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				10 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				11 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				12 => array('business_group'=>30, 'management_group'=>30, 'financial_group'=>40),
				13 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				14 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				15 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				16 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				17 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35),
				18 => array('business_group'=>35, 'management_group'=>30, 'financial_group'=>35)
			);
				foreach($reset_data as $key=>$value){
					$industry = BankIndustry::where('industry_id', $key)
					->where('bank_id', Input::get('bank_id'))
					->update($value);
				}
			}
			// update and redirect back to industry weights
			return Redirect::to('/admin/industry/'.Input::get('bank_id'));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.10: getConverter
	//  Description: get page for excel to vfa converter
    //-----------------------------------------------------
	public function getConverter()
	{
		try{
			// get password to show on display
			$password = Keys::where('name','converter_key')->first()->value;
			return View::make('admin.converter', array(
				'converter_key' => $password
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.11: postConverter
	//  Description: converts FS Template to vfa, post process after FS Template upload
    //-----------------------------------------------------
	public function postConverter()
	{
		try{
			$v = Validator::make(Input::all(), array(		// validation rules
				'file'          => 'required|mimes:xlsx'
			), array (
				'file.required' => 'Please select the Excel template to convert.',
				'file.mimes'    => 'The file you were trying to upload does not have the correct format.'
			));

			if ($v->fails()) {
				// redirect to converter page then show errors
				return Redirect::route('getConverter')->withErrors($v->errors());
			}

			$file               = Input::file('file');									// input for uploaded file
			$destinationPath    = 'temp'; 										// directory for saving, temporary directory
			$extension          = $file->getClientOriginalExtension();				// get the extension name of uploaded file
			$filename           = rand(11111,99999).time().'.'.$extension;			// generate random filename
			$upload_success     = $file->move($destinationPath, $filename);		// save uploaded file to server for processing

			// init excel reader
			$excel  = Excel::load('temp/'.$filename);
			$sheet  = $excel->getActiveSheet();

			/* init vfa array */

			// get money factor
			switch((string)$sheet->getCell('C6')->getValue()) {
				case 'Millions':
					$money_factor = '1000000'; break;
				case 'Thousands':
					$money_factor = '1000'; break;
				default:
					$money_factor = '1';
			}

			/* array to be converted to json for RR format */
			$vfa = array(
				'bal' => array(),
				'prib' => array(),
				'prib_s' => array(),
				'ubal1' => array(),
				'ubal2' => array(),
				'ubal2_s' => array(),
				'report' => '57',
				'TSID' => '=AFCG8lRG5QBQF0DBlgAEgNQRMxAWdFBIQxA',		// Ready Ratios Key
				'anstep' => '12',										// 12 = Yearly
				'repern' => '12',
				'YEAR' => (string)$sheet->getCell('B10')->getValue(),	// get latest year
				'money_factor' => $money_factor,
				'valedin' => (string)$sheet->getCell('B6')->getCalculatedValue(),		// industry value
				'Otrasli' => (string)$sheet->getCell('B4')->getCalculatedValue(),
				'OrgFoma' => '',
				'title' => (string)$sheet->getCell('B2')->getValue(),	// get title
				'GGrafQuality' => '2',
				'GColumnsNumber' => '4',
				'GReportStyle' => '0',
				'GCritica' => '0',
				'GFont' => 'Arial, Helvetica, sans-serif',
				'GFontSize' => '10',
				'GOpenReport' => '4',
				'finyear_start' => '0',
				'printOglavlen' => '1',
				'GUseDefaultFormuls' => '0',
				'GCriticaPresume' => '1',
				'GColored' => '1',
				'GBold' => '0',
				'GExplodeNumbers' => '1',
				'GUseGraph' => '1',
				'GOpenReportNewWindow' => '0',
				'GSaveRawData' => '0',
				'GAnaliticBalance' => '0',
				'GPrintUserIndicators' => '0',
				'GAnaliticPribUb' => '0',
				'GAmerican' => '1',
				'og' => array(
					'oglavlen_1' => 'on',
					'oglavlen_1_1' => 'on',
					'oglavlen_1_2' => 'on',
					'oglavlen_1_3' => 'on',
					'oglavlen_1_3_1' => 'on',
					'oglavlen_1_3_2' => 'on',
					'oglavlen_1_4' => 'on',
					'oglavlen_2' => 'on',
					'oglavlen_2_1' => 'on',
					'oglavlen_2_2' => 'on',
					'oglavlen_2_3' => 'on',
					'oglavlen_2_4' => 'on',
					'oglavlen_3' => 'on',
					'oglavlen_3_1' => 'on',
					'oglavlen_3_2' => 'on',
					'oglavlen_4' => 'on',
					'oglavlen_4_1' => 'on',
					'oglavlen_4_2' => 'on'
				)
			);

			$count_items = 0;	// variable to count how many years in template
			// check last balance sheet
			$balance_sheet_column_array = array('B','C','D','E');  // excel columns
			foreach($balance_sheet_column_array as $col){
				// check exist
				if($sheet->getCell($col.'42')->getCalculatedValue() > 0){
					$count_items++;
					$vfa['bal'][] = array(
						/* populate elements depending on position in excel sheet eg column A row 1 */
						'PropertyPlantAndEquipment' => ($sheet->getCell($col.'26')->getCalculatedValue()) ? $sheet->getCell($col.'26')->getCalculatedValue() : "0",
							'InvestmentProperty' => ($sheet->getCell($col.'27')->getCalculatedValue()) ? $sheet->getCell($col.'27')->getCalculatedValue() : "0",
							'Goodwill' => ($sheet->getCell($col.'28')->getCalculatedValue()) ? $sheet->getCell($col.'28')->getCalculatedValue() : "0",
							'IntangibleAssetsOtherThanGoodwill' => ($sheet->getCell($col.'29')->getCalculatedValue()) ? $sheet->getCell($col.'29')->getCalculatedValue() : "0",
							'InvestmentAccountedForUsingEquityMethod' => ($sheet->getCell($col.'30')->getCalculatedValue()) ? $sheet->getCell($col.'30')->getCalculatedValue() : "0",
							'InvestmentsInSubsidiariesJointVenturesAndAssociates' => ($sheet->getCell($col.'31')->getCalculatedValue()) ? $sheet->getCell($col.'31')->getCalculatedValue() : "0",
							'NoncurrentBiologicalAssets' => ($sheet->getCell($col.'32')->getCalculatedValue()) ? $sheet->getCell($col.'32')->getCalculatedValue() : "0",
							'NoncurrentReceivables' => ($sheet->getCell($col.'33')->getCalculatedValue()) ? $sheet->getCell($col.'33')->getCalculatedValue() : "0",
							'NoncurrentInventories' => ($sheet->getCell($col.'34')->getCalculatedValue()) ? $sheet->getCell($col.'34')->getCalculatedValue() : "0",
							'DeferredTaxAssets' => ($sheet->getCell($col.'35')->getCalculatedValue()) ? $sheet->getCell($col.'35')->getCalculatedValue() : "0",
							'CurrentTaxAssetsNoncurrent' => ($sheet->getCell($col.'36')->getCalculatedValue()) ? $sheet->getCell($col.'36')->getCalculatedValue() : "0",
							'OtherNoncurrentFinancialAssets' => ($sheet->getCell($col.'37')->getCalculatedValue()) ? $sheet->getCell($col.'37')->getCalculatedValue() : "0",
							'OtherNoncurrentNonfinancialAssets' => ($sheet->getCell($col.'38')->getCalculatedValue()) ? $sheet->getCell($col.'38')->getCalculatedValue() : "0",
							'NoncurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral' => ($sheet->getCell($col.'39')->getCalculatedValue()) ? $sheet->getCell($col.'39')->getCalculatedValue() : "0",
							'NoncurrentAssets' => ($sheet->getCell($col.'40')->getCalculatedValue()) ? $sheet->getCell($col.'40')->getCalculatedValue() : "0",

							'Inventories' => ($sheet->getCell($col.'16')->getCalculatedValue()) ? $sheet->getCell($col.'16')->getCalculatedValue() : "0",
							'TradeAndOtherCurrentReceivables' => ($sheet->getCell($col.'15')->getCalculatedValue()) ? $sheet->getCell($col.'15')->getCalculatedValue() : "0",
							'CurrentTaxAssetsCurrent' => ($sheet->getCell($col.'17')->getCalculatedValue()) ? $sheet->getCell($col.'17')->getCalculatedValue() : "0",
							'CurrentBiologicalAssets' => ($sheet->getCell($col.'18')->getCalculatedValue()) ? $sheet->getCell($col.'18')->getCalculatedValue() : "0",
							'OtherCurrentFinancialAssets' => ($sheet->getCell($col.'19')->getCalculatedValue()) ? $sheet->getCell($col.'19')->getCalculatedValue() : "0",
							'OtherCurrentNonfinancialAssets' => ($sheet->getCell($col.'20')->getCalculatedValue()) ? $sheet->getCell($col.'20')->getCalculatedValue() : "0",
							'CashAndCashEquivalents' => ($sheet->getCell($col.'14')->getCalculatedValue()) ? $sheet->getCell($col.'14')->getCalculatedValue() : "0",
							'CurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral' => ($sheet->getCell($col.'21')->getCalculatedValue()) ? $sheet->getCell($col.'21')->getCalculatedValue() : "0",
							'NoncurrentAssetsOrDisposalGroupsClassifiedAsHeldForSaleOrAsHeldForDistributionToOwners' => ($sheet->getCell($col.'22')->getCalculatedValue()) ? $sheet->getCell($col.'22')->getCalculatedValue() : "0",
							'CurrentAssets' => ($sheet->getCell($col.'23')->getCalculatedValue()) ? $sheet->getCell($col.'23')->getCalculatedValue() : "0",

							'Assets' => ($sheet->getCell($col.'42')->getCalculatedValue()) ? $sheet->getCell($col.'42')->getCalculatedValue() : "0",

							'IssuedCapital' => ($sheet->getCell($col.'67')->getCalculatedValue()) ? $sheet->getCell($col.'67')->getCalculatedValue() : "0",
							'SharePremium' => ($sheet->getCell($col.'68')->getCalculatedValue()) ? $sheet->getCell($col.'68')->getCalculatedValue() : "0",
							'TreasuryShares' => ($sheet->getCell($col.'69')->getCalculatedValue()) ? $sheet->getCell($col.'69')->getCalculatedValue() : "0",
							'OtherEquityInterest' => ($sheet->getCell($col.'70')->getCalculatedValue()) ? $sheet->getCell($col.'70')->getCalculatedValue() : "0",
							'OtherReserves' => ($sheet->getCell($col.'71')->getCalculatedValue()) ? $sheet->getCell($col.'71')->getCalculatedValue() : "0",
							'RetainedEarnings' => ($sheet->getCell($col.'72')->getCalculatedValue()) ? $sheet->getCell($col.'72')->getCalculatedValue() : "0",
							'NoncontrollingInterests' => ($sheet->getCell($col.'73')->getCalculatedValue()) ? $sheet->getCell($col.'73')->getCalculatedValue() : "0",
							'Equity' => ($sheet->getCell($col.'74')->getCalculatedValue()) ? $sheet->getCell($col.'74')->getCalculatedValue() : "0",

							'NoncurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'57')->getCalculatedValue()) ? $sheet->getCell($col.'57')->getCalculatedValue() : "0",
							'OtherLongtermProvisions' => ($sheet->getCell($col.'58')->getCalculatedValue()) ? $sheet->getCell($col.'58')->getCalculatedValue() : "0",
							'NoncurrentPayables' => ($sheet->getCell($col.'59')->getCalculatedValue()) ? $sheet->getCell($col.'59')->getCalculatedValue() : "0",
							'DeferredTaxLiabilities' => ($sheet->getCell($col.'60')->getCalculatedValue()) ? $sheet->getCell($col.'60')->getCalculatedValue() : "0",
							'CurrentTaxLiabilitiesNoncurrent' => ($sheet->getCell($col.'61')->getCalculatedValue()) ? $sheet->getCell($col.'61')->getCalculatedValue() : "0",
							'OtherNoncurrentFinancialLiabilities' => ($sheet->getCell($col.'62')->getCalculatedValue()) ? $sheet->getCell($col.'62')->getCalculatedValue() : "0",
							'OtherNoncurrentNonfinancialLiabilities' => ($sheet->getCell($col.'63')->getCalculatedValue()) ? $sheet->getCell($col.'63')->getCalculatedValue() : "0",
							'NoncurrentLiabilities' => ($sheet->getCell($col.'64')->getCalculatedValue()) ? $sheet->getCell($col.'64')->getCalculatedValue() : "0",

							'CurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'47')->getCalculatedValue()) ? $sheet->getCell($col.'47')->getCalculatedValue() : "0",
							'OtherShorttermProvisions' => ($sheet->getCell($col.'48')->getCalculatedValue()) ? $sheet->getCell($col.'48')->getCalculatedValue() : "0",
							'TradeAndOtherCurrentPayables' => ($sheet->getCell($col.'49')->getCalculatedValue()) ? $sheet->getCell($col.'49')->getCalculatedValue() : "0",
							'CurrentTaxLiabilitiesCurrent' => ($sheet->getCell($col.'50')->getCalculatedValue()) ? $sheet->getCell($col.'50')->getCalculatedValue() : "0",
							'OtherCurrentFinancialLiabilities' => ($sheet->getCell($col.'51')->getCalculatedValue()) ? $sheet->getCell($col.'51')->getCalculatedValue() : "0",
							'OtherCurrentNonfinancialLiabilities' => ($sheet->getCell($col.'52')->getCalculatedValue()) ? $sheet->getCell($col.'52')->getCalculatedValue() : "0",
							'LiabilitiesIncludedInDisposalGroupsClassifiedAsHeldForSale' => ($sheet->getCell($col.'53')->getCalculatedValue()) ? $sheet->getCell($col.'53')->getCalculatedValue() : "0",
							'CurrentLiabilities' => ($sheet->getCell($col.'54')->getCalculatedValue()) ? $sheet->getCell($col.'54')->getCalculatedValue() : "0",

							'Liabilities' => ($sheet->getCell($col.'76')->getCalculatedValue()) ? $sheet->getCell($col.'76')->getCalculatedValue() : "0",
							'EquityAndLiabilities' => ($sheet->getCell($col.'78')->getCalculatedValue()) ? $sheet->getCell($col.'78')->getCalculatedValue() : "0"
					);
				}
			}

			//income statement
			$income_statement_column_array = array('H','I','J','K'); // excel columns
			for($x=0;$x<($count_items-1);$x++){
				$vfa['prib'][] = array(
					/* populate elements depending on position in excel sheet eg column A row 1 */
					'Revenue' => ($sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue() : "0",
					'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue() : "0",
					'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue() : "0",

					'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue() : "0",
					'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue() : "0",
					'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue() : "0",
					'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue() : "0",
					'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue() : "0",
					'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue() : "0",
	                'DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed' => ($sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue() : "0",
	                'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue() : "0",
	                'GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost' => ($sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue() : "0",
	                'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue() : "0",
	                'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue() : "0",
	                'ImpairmentLossImpairmentGainAndReversalOfImpairmentLossDeterminedInAccordanceWithIFRS9' => ($sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue() : "0",

					'ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod' => ($sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue() : "0",
	                'OtherIncomeExpenseFromSubsidiariesJointlyControlledEntitiesAndAssociates' => ($sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue() : "0",
					'GainsLossesArisingFromDifferenceBetweenPreviousCarryingAmountAndFairValueOfFinancialAssetsReclassifiedAsMeasuredAtFairValue' => ($sheet->getCell($income_statement_column_array[$x].'31')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'31')->getCalculatedValue() : "0",
	                'CumulativeGainLossPreviouslyRecognisedInOtherComprehensiveIncomeArisingFromReclassificationOfFinancialAssetsOutOfFairValueThroughOtherComprehensiveIncomeIntoFairValueThroughProfitOrLossMeasurementCategory' => ($sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue() : "0",
	                'HedgingGainsLossesForHedgeOfGroupOfItemsWithOffsettingRiskPositions' => ($sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue() : "0",
					'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue() : "0",

					'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'36')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'36')->getCalculatedValue() : "0",
					'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'37')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'37')->getCalculatedValue() : "0",
					'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x].'39')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'39')->getCalculatedValue() : "0",
					'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x].'40')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'40')->getCalculatedValue() : "0",
					'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'42')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'42')->getCalculatedValue() : "0",
					'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'43')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'43')->getCalculatedValue() : "0"
				);

				$vfa['prib_s'][] = array(
					'Revenue' => ($sheet->getCell($income_statement_column_array[$x+1].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'12')->getCalculatedValue() : "0",
					'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x+1].'13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'13')->getCalculatedValue() : "0",
					'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x+1].'14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'14')->getCalculatedValue() : "0",

					'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'16')->getCalculatedValue() : "0",
					'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x+1].'17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'17')->getCalculatedValue() : "0",
					'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x+1].'18')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'18')->getCalculatedValue() : "0",
					'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x+1].'19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'19')->getCalculatedValue() : "0",
					'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x+1].'20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'20')->getCalculatedValue() : "0",
					'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x+1].'21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'21')->getCalculatedValue() : "0",

					'DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed' => ($sheet->getCell($income_statement_column_array[$x+1].'23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'23')->getCalculatedValue() : "0",
					'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x+1].'24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'24')->getCalculatedValue() : "0",
					'GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost' => ($sheet->getCell($income_statement_column_array[$x+1].'25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'25')->getCalculatedValue() : "0",
					'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'26')->getCalculatedValue() : "0",
					'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x+1].'27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'27')->getCalculatedValue() : "0",
	                'ImpairmentLossImpairmentGainAndReversalOfImpairmentLossDeterminedInAccordanceWithIFRS9' => ($sheet->getCell($income_statement_column_array[$x+1].'28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'28')->getCalculatedValue() : "0",
					'ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod' => ($sheet->getCell($income_statement_column_array[$x+1].'29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'29')->getCalculatedValue() : "0",
	                'OtherIncomeExpenseFromSubsidiariesJointlyControlledEntitiesAndAssociates' => ($sheet->getCell($income_statement_column_array[$x+1].'30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'30')->getCalculatedValue() : "0",
					'GainsLossesArisingFromDifferenceBetweenPreviousCarryingAmountAndFairValueOfFinancialAssetsReclassifiedAsMeasuredAtFairValue' => ($sheet->getCell($income_statement_column_array[$x+1].'31')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'31')->getCalculatedValue() : "0",
	                'CumulativeGainLossPreviouslyRecognisedInOtherComprehensiveIncomeArisingFromReclassificationOfFinancialAssetsOutOfFairValueThroughOtherComprehensiveIncomeIntoFairValueThroughProfitOrLossMeasurementCategory' => ($sheet->getCell($income_statement_column_array[$x+1].'32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'32')->getCalculatedValue() : "0",
	                'HedgingGainsLossesForHedgeOfGroupOfItemsWithOffsettingRiskPositions' => ($sheet->getCell($income_statement_column_array[$x+1].'33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'33')->getCalculatedValue() : "0",
					'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x+1].'34')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'34')->getCalculatedValue() : "0",

					'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'36')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'36')->getCalculatedValue() : "0",
					'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'37')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'37')->getCalculatedValue() : "0",
					'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'39')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'39')->getCalculatedValue() : "0",
					'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x+1].'40')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'40')->getCalculatedValue() : "0",
					'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'42')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'42')->getCalculatedValue() : "0",
					'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'43')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'43')->getCalculatedValue() : "0"
				);
			}

			// additional for RR format purposes
			for($x=0;$x<($count_items);$x++){
				$vfa['ubal1'][] = array(
					'staff' => '0'
				);
			}

	        $cashFlowColumn = array('H','I','J','K'); // excel columns
			for($x=0;$x<($count_items-1);$x++){
	            $depreciation = $sheet->getCell($cashFlowColumn[$x].'47')->getCalculatedValue() ?: 0;
	            $amortisation = $sheet->getCell($cashFlowColumn[$x].'48')->getCalculatedValue() ?: 0;

				$vfa['ubal2'][] = array(
					'DepreciationAndAmortisationExpense' => $depreciation + $amortisation,
				);

	            if ($x !== 0) {
	                $vfa['ubal2_s'][] = array(
	                    'DepreciationAndAmortisationExpense' => $depreciation + $amortisation,
	                );
	            }
			}

			unlink('temp/'.$filename);	// delete temp file
			file_put_contents('temp/temporary_vfa.vfa', json_encode($vfa));		// save the file as temporary_vfa.vfa
			return Response::download('temp/temporary_vfa.vfa', 'creditbpo_converted.vfa'); 	// stream vfa file for download
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

	}

	//-----------------------------------------------------
    //  Function 2.12: postUpdateConverter
	//  Description: post process for updating converter public key
    //-----------------------------------------------------
	public function postUpdateConverter()
	{
		try{
			$new_password = Input::get('new_key');		// get new key from input
			Keys::where('name','converter_key')->update(array('value'=>$new_password));  // update key in db

			// send email about details of the new key
			if (env("APP_ENV") == "Production") {

				try{
					Mail::send('emails.converter', array('converter_key'=>$new_password), function($message){
						$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('CreditBPO change in converter link');
					});

				}catch(Exception $e){
	            //
	            }
			}
			// redirect to converter page
			return Redirect::to('admin/converter');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.12: postTranscribeKey
	//  Description: post process for updating converter public key
    //-----------------------------------------------------
	public function postUpdateTranscribeKey()
	{
		try{
			$new_password = Input::get('new_key');		// get new key from input
			Keys::where('name','transcribe_key')->update(array('value'=>$new_password));  // update key in db

			// send email about details of the new key
			if (env("APP_ENV") == "Production") {

				try{
					Mail::send('emails.converter', array('transcribe_key'=>$new_password), function($message){
						$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
							->subject('CreditBPO change in converter link');
					});

				}catch(Exception $e){
	            //
	            }
			}
			// redirect to converter page
			return Redirect::to('admin/transcribe_files');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.13: showBSPExpectation
	//  Description: get BSP expectation page
    //-----------------------------------------------------
    public function showBSPExpectation()
    {
    	try{
	        $bsp_db     = new BspBusinessExpectation; // create instance of BspBusinessExpectation class

	        $bsp_data   = $bsp_db->getBusinessExpectation();
	        //$fname_exp  = explode('.', $bsp_data['filename']);

			//$view_file  = $fname_exp[0];

	        return \File::get(public_path() . '/bsp-expectations/' . $bsp_data['filename']); //View::make($view_file); // show bsp busness expectaion result
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.14: uploadBusinessOutlook
	//  Description: post process for BSP expectation page
    //-----------------------------------------------------
    public function uploadBusinessOutlook()
    {
    	try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
			$validator				= CONT_NULL;
			$srv_resp['messages']	= CONT_NULL;
			$srv_resp['valid']		= STS_NG;

	        $bsp_db = new BspBusinessExpectation; // create instance of BspBusinessExpectation

			//	variable declaration containers
	        $rules      = array();
	        $messages   = array();

	        /*	Form Inputs			*/
			$data['bsp_file']       = Input::file('bsp-expectation-file'); // get input file

	        $rules      = array(	// validation rules
	            'bsp-expectation-file'  => 'required|mimes:html'
	        );

	        $messages   = array(	// validator error messages
	            'bsp-expectation-file.required' => 'The file to upload is required',
	            'bsp-expectation-file.mimes'    => 'File to upload must be an HTML format',
	        );

	        $validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        /*--------------------------------------------------------------------
			/*	There are invalid inputs
			/*------------------------------------------------------------------*/
	        if ($validator->fails()) {
	            /*	Gets the messages from the validation						*/
				$messages 				= $validator->messages();
				$srv_resp['messages']	= $messages->all();
	        }
	        /*--------------------------------------------------------------------
			/*	All inputs are valid
			/*------------------------------------------------------------------*/
	        else {
	            /** Sets the filename and destination dir    */
	            $destination_path           = 'bsp-expectations';
	            $extension                  = $data['bsp_file']->getClientOriginalExtension();
	            $filename                   = 'BES_'.date('mY').'.'.$extension;

	            /** Upload the file    */
	            $upload_success             = $data['bsp_file']->move($destination_path, $filename);

	            $data['filename']   = $filename;
	            $bsp_db->addBusinessExpectation($data);

	            $srv_resp['messages']	= STS_OK;
				$srv_resp['valid']		= STS_OK;
	        }

	        // return json encoded results
			return json_encode($srv_resp);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.15: calculateBusinessOutlooks
	//  Description: post process for BSP expectation page
    //-----------------------------------------------------
    public function calculateBusinessOutlooks()
    {
    	try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $industry_count = 18;									// number of industries
	        $bsp_data       = Input::get('bsp-expectations');		// get value from input

	        $biz_outlook_db     = new BusinessOutlook;		// create instance of BusinessOutlook
	        $regression_lib     = new RegressionCalcLib;	// create instance of RegressionCalcLib

	        /*--------------------------------------------------------------------
			/*	Match the BSP Industries with Government industries list
			/*------------------------------------------------------------------*/
	        for ($idx = 1; $idx <= $industry_count; $idx++) {
	            $industry_db    = Industrymain::find($idx);

	            switch($idx) {
	                case INDUSTRY_AGRICULTURE:
	                case INDUSTRY_MINING:
	                case INDUSTRY_MANUFACTURING:
	                case INDUSTRY_ELECTRICITY:
	                case INDUSTRY_WATER:
	                    $bsp_idx = INDUSTRY_BSP_INDUSTRY;
	                    break;

	                case INDUSTRY_CONSTRUCTION:
	                    $bsp_idx = INDUSTRY_BSP_CONSTRUCTION;
	                    break;

	                case INDUSTRY_WHOLESALE:
	                    $bsp_idx = INDUSTRY_BSP_WHOLESALE;
	                    break;

	                case INDUSTRY_TRANSPORTATION:
	                    $bsp_idx = INDUSTRY_BSP_TRANSPORTATION;
	                    break;

	                case INDUSTRY_ACCOMODATION:
	                    $bsp_idx = INDUSTRY_BSP_ACCOMODATIONS;
	                    break;

	                case INDUSTRY_COMMUNICATION:
	                case INDUSTRY_SCIENTIFIC:
	                case INDUSTRY_OTHER_SERVICES:
	                    $bsp_idx = INDUSTRY_BSP_SERVICES;
	                    break;

	                case INDUSTRY_FINANCE:
	                    $bsp_idx = INDUSTRY_BSP_FINANCE;
	                    break;

	                case INDUSTRY_REAL_ESTATE:
	                    $bsp_idx = INDUSTRY_BSP_REAL_ESTATE;
	                    break;

	                case INDUSTRY_ADMINISTRATIVE:
	                    $bsp_idx = INDUSTRY_BSP_BUSINESS;
	                    break;

	                case INDUSTRY_HEALTH:
	                case INDUSTRY_EDUCATION:
	                case INDUSTRY_ENTERTAINMENT:
	                    $bsp_idx = INDUSTRY_BSP_SOCIAL_SERVICE;
	                    break;
	            }

	            $biz_outlook_data['biz-outlook-q1'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_1];
	            $biz_outlook_data['biz-outlook-q2'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_2];
	            $biz_outlook_data['biz-outlook-q3'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_3];
	            $biz_outlook_data['biz-outlook-q4'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_4];
	            $biz_outlook_data['biz-outlook-q5'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_5];
	            $biz_outlook_data['biz-outlook-q6'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_6];
	            $biz_outlook_data['biz-outlook-q7'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_7];
	            $biz_outlook_data['biz-outlook-q8'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_8];

	            /*----------------------------------------------------------------
	            /*	Saves the business outlook data to the Database
	            /*--------------------------------------------------------------*/
	            $biz_outlook_db->addUpdateBusinessOutlook($idx, $biz_outlook_data);

	            /*----------------------------------------------------------------
	            /*	Calculate slope of Business Outlook
	            /*--------------------------------------------------------------*/
	            $xy_list    = array(
	                0   => array($biz_outlook_data['biz-outlook-q1'], 1),
	                1   => array($biz_outlook_data['biz-outlook-q2'], 2),
	                2   => array($biz_outlook_data['biz-outlook-q3'], 3),
	                3   => array($biz_outlook_data['biz-outlook-q4'], 4),
	                4   => array($biz_outlook_data['biz-outlook-q5'], 5),
	                5   => array($biz_outlook_data['biz-outlook-q6'], 6),
	                6   => array($biz_outlook_data['biz-outlook-q7'], 7),
	                7   => array($biz_outlook_data['biz-outlook-q8'], 8)
	            );

	            $regression_results = $regression_lib->calculateRegression($xy_list);

	            /*----------------------------------------------------------------
	            /*	Determines the Industry Trend and save into database
	            /*--------------------------------------------------------------*/
	            $industry_db->industry_score   = $regression_results[REGRESSION_PTS_ALLOC];

				switch($regression_results[REGRESSION_PTS_ALLOC]){
	                case BIZ_OUTLOOK_UP_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_UP_TREND;
	                    break;

	                case BIZ_OUTLOOK_FLAT_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_FLAT_TREND;
	                    break;

	                case BIZ_OUTLOOK_DOWN_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_DOWN_TREND;
	                    break;
				}

				$industry_db->save(); // save industry values
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}   
    }

	//-----------------------------------------------------
    //  Function 2.16: getClientKeys
	//  Description: get Independent Keys listing page
    //-----------------------------------------------------
	public function getClientKeys()
	{
		try{
			/* variable declaration */
			$keys = IndependentKey::all(); 	// get all independent keys
			$used = 0;						// used for counting used keys
			$unused = 0;					// used for counting unused keys

			/* iterate through all keys */
			foreach($keys as $key=>$value){
				if($value->is_used == 1){
					$used++;
					$keys[$key]->used = 'YES'; // show yes if used
				} else {
					$unused++;
					$keys[$key]->used = 'NO';	// show no if unused
				}
				if($value->is_paid == 1){
					$keys[$key]->status = '<span style="color: GREEN;">Paid</span>';	// show Paid if status is 1
				} else {
					$keys[$key]->status = '<span style="color: GREY;">Unpaid</span>';	// show Unpaid if status is 0
				}
				$oldhash = base64_encode($value->email); 		// encode email
				$hash = substr_replace($oldhash, '1', 10, 0);
				$keys[$key]->code = $hash;
			}

			// show page for admin/clientkeys.blade.php
			return View::make('admin.clientkeys', array(
				'entity' => $keys,
				'used' => $used,
				'unused' => $unused
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.17: getCreateClientKeys
	//  Description: get Independent Keys create form page
    //-----------------------------------------------------
	public function getCreateClientKeys()
	{
		try{
			// show view in admin/create_keys.blade.php
			return View::make('admin.create_keys');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.18: postCreateClientKeys
	//  Description: post process from Independent Keys create form page
    //-----------------------------------------------------
	public function postCreateClientKeys()
	{
		try{
			$rules = array(		// validation rules
		        'email'	=> 'required|email',
		        'quantity'	=> 'required|numeric'
			);

			$validator = Validator::make(Input::all(), $rules); // run validation

			if($validator->fails()) // if validation fails, redirect back with errors displayed
			{
				return Redirect::to('admin/create_keys')->withErrors($validator->errors())->withInput();
			}
			else // successfully validated
			{
				$email = Input::get('email');
				$keys = IndependentKey::createAdminKeys($email, Input::get('quantity'), 1);

				$oldhash = base64_encode($email); 		// encode email
				$hash = substr_replace($oldhash, '1', 10, 0);
				$code = $hash;

				try{
					Mail::send('emails.buyer_keys_paid_generated', array(
							'hash'=>$code
						), function($message) use ($email){
							$message->to($email, $email)
								->subject('Client Serial Keys Generated from CreditBPO!');

							$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
							$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
					});
					
				}
				catch(Exception $ex){
					//
				}

				// back to Indenpendent keys list
				return Redirect::to('admin/clientkeys');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.19: detectBspExpectation
	//  Description: function for detecting and updating BSP expectation
    //-----------------------------------------------------
    public function detectBspExpectation()
    {
    	try{
	        /*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $srv_resp['messages']	= CONT_NULL;
			$srv_resp['valid']		= STS_NG;

	        /** Month and Year  */
	        $month          = date('m');
	        $year           = date('Y');

	        /** Download file   */
	        $site           = 'http://www.bsp.gov.ph/downloads/Publications/'.$year.'/';
	        $filename       = 'BES_';
	        $ext            = '.pdf';
	        $qtr            = STR_EMPTY;
	        $url            = STR_EMPTY;

	        /** Status          */
	        $url_sts        = STS_NG;
	        $curl_exec      = STS_NG;
	        $curl           = CONT_NULL;

	        /** Database        */
	        $bsp_data       = CONT_NULL;
	        $bsp_count      = 0;
	        $bsp_file_db    = new BspExpectationDownload;

	        /*--------------------------------------------------------------------
			/*	Determine the Quarter of the report to detect
			/*------------------------------------------------------------------*/
	        switch ($month) {

	            case MONTH_JAN:
	            case MONTH_FEB:
	            case MONTH_MAR:
	                $qtr    = '1qtr';
	                break;

	            case MONTH_APR:
	            case MONTH_MAY:
	            case MONTH_JUN:
	                $qtr    = '2qtr';
	                break;

	            case MONTH_JUL:
	            case MONTH_AUG:
	            case MONTH_SEP:
	                $qtr    = '3qtr';
	                break;

	            case MONTH_OCT:
	            case MONTH_NOV:
	            case MONTH_DEC:
	                $qtr    = '4qtr';
	                break;
	        }

	        /*--------------------------------------------------------------------
			/*	Format the Filename
			/*------------------------------------------------------------------*/
	        $filename   = $filename.$qtr.$year.$ext;
	        $filename = 'BES_4qtr2017.pdf';

	        /*--------------------------------------------------------------------
			/*	Check Database if the file being detected is already downloaded
			/*------------------------------------------------------------------*/
	        $bsp_data   = $bsp_file_db->getBspFile($filename);
	        $bsp_count  = @count($bsp_data);

	        /*--------------------------------------------------------------------
			/*	File not yet downloaded
			/*------------------------------------------------------------------*/
	        if (0 >= $bsp_count) {

	            $url        = $site.$filename;
	            $url = 'http://www.bsp.gov.ph/downloads/Publications/2017/'.$filename;
	            echo $url;

	            /*---------------------------------------------------------------
	            /*	Use CURL to detect file
	            /*-------------------------------------------------------------*/
	            $curl       = curl_init($url);

	            /** Check file header   */
	            curl_setopt($curl, CURLOPT_NOBODY, true);

	            $curl_exec  = curl_exec($curl);

	            /** CURL Operation successful   */
	            if (STS_OK == $curl_exec) {
	                $url_sts  = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	                /** File exist   */
	                if (200 == $url_sts) {
	                    $srv_resp['valid']          = STS_OK;
	                    $srv_resp['filename']       = $filename;
	                    $srv_resp['messages'][0]    = 'The System has detected a new release of BSP Business Expectation';
	                }
	                /** File does not exist   */
	                else {
	                    $srv_resp['messages'][0]    = $url.' does not exist';
	                }
	            }
	        }

	        // return json encoded results
			return json_encode($srv_resp);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

    }

	//-----------------------------------------------------
    //  Function 2.20: downloadBspExpectation
	//  Description: function for downloading BSP expectation from site
    //-----------------------------------------------------
    public function downloadBspExpectation($filename)
    {
    	try{
	        /*--------------------------------------------------------------------
			/*	Variable Definition
			/*------------------------------------------------------------------*/
	        $site       = 'http://www.bsp.gov.ph/downloads/Publications/'.date('Y').'/';
	        $url        = $site.$filename;

	        /*--------------------------------------------------------------------
			/*	Format the Headers for download
			/*------------------------------------------------------------------*/
	        header('Content-Type: application/octet-stream');
	        header("Content-Transfer-Encoding: Binary");
	        header("Content-disposition: attachment; filename=\"" . basename($url) . "\"");

	        readfile($url);   // stream read file
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.21: addBspDownloadReport
	//  Description: function for downloading BSP expectation from site
    //-----------------------------------------------------
    public function addBspDownloadReport()
    {
    	try{
	        /*--------------------------------------------------------------------
			/*	Variable Definition
			/*------------------------------------------------------------------*/
	        $sts                = STS_NG;
	        $data['filename']   = Input::get('filename');

	        /** Instantiate Classes */
	        $bsp_download_db    = new BspExpectationDownload;

	        /*--------------------------------------------------------------------
			/*	Add file to the Database
			/*------------------------------------------------------------------*/
	        $sts    = $bsp_download_db->addBspFile($data);

	        return json_encode($sts);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.22: unlockUserAccount
	//  Description: function for unlocking user account
    //-----------------------------------------------------
    public function unlockUserAccount($login_id)
    {
    	try{
			// variable declaration
	        $sts                = STS_OK;
	        $user_2fa_db        = new User2faAttempt;		// create instance of User2faAttempt
	        $login_attempt_db   = new UserLoginAttempt;		// create instance of UserLoginAttempt

	        /*--------------------------------------------------------------------
			/*	Unlock Failed 2FA
			/*------------------------------------------------------------------*/
	        $user_2fa_db->unlock2faAttempt($login_id);

	        /*--------------------------------------------------------------------
			/*	Unlock Failed Login
			/*------------------------------------------------------------------*/
	        $log_attempt_data   = $login_attempt_db->getUserAttempt($login_id);

			// check login attempts count
	        if (1 <= @count($log_attempt_data)) {
	            $log_data['attempt_id'] = $log_attempt_data['login_attempt_id'];
	            $login_attempt_db->updateAttemptSts($log_data, STS_OK);
	        }

			// return status
	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.23: deleteKey
	//  Description: function to delete independent keys
    //-----------------------------------------------------
	public function deleteKey($id)
	{
		try{
			IndependentKey::find($id)->delete(); // find key and delete
			return Redirect::to('admin/clientkeys'); // redirect back
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.24: getDiscounts
	//  Description: get Discounts page
    //-----------------------------------------------------
	public function getDiscounts()
	{
		try{
			$discounts = Discount::all(); // get all discounts on db
			// show page
			return View::make('admin.discounts', array(
				'discounts' => $discounts
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.25: getAddDiscount
	//  Description: form for adding discount
    //-----------------------------------------------------
	public function getAddDiscount()
	{
		try{

			$organizations = DB::table('tblbank')->select('bank_name','id')->get();
			
			return View::make('admin.add_discount',array(
				'organizations' => $organizations
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.26: getEditDiscount
	//  Description: form for editting discount
    //-----------------------------------------------------
	public function getEditDiscount($id)
	{
		try{
			$discount = Discount::find($id);
			$organizations = DB::table('tblbank')->select('bank_name','id')->get();
			return View::make('admin.edit_discount', array(
				'discount' => $discount,
				'organizations' => $organizations));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.27: postAddDiscount
	//  Description: post process after submit on add discount
    //-----------------------------------------------------
	public function postAddDiscount()
	{
		try{
			$rules = array(		// validation rules
		        'discount_code'	=> 'required|unique:discounts',
		        'email'	=> 'required|email',
		        'business'	=> 'required',
				'amount' => 'required|numeric',
				'valid_from' => array('required', 'date_format:"Y-m-d"'),
				'valid_to' => array('required', 'date_format:"Y-m-d"')
			);

			$validator = Validator::make(Input::all(), $rules); // run validator

			if($validator->fails())	// if fail redirect back with error messages
			{
				return Redirect::to('/admin/add_discount')->withErrors($validator->errors())->withInput();
			}
			else
			{
				// create instance and populate inputs
				$discount = new Discount;
				$discount->bank = Input::get('organization');
				$discount->discount_code = Input::get('discount_code');
				$discount->email = Input::get('email');
				$discount->business = Input::get('business');
				$discount->type = Input::get('type');
				$discount->amount = Input::get('amount');
				$discount->status = 0;
				$discount->valid_from = Input::get('valid_from') . ' 00:00:00';
				$discount->valid_to = Input::get('valid_to') . ' 23:59:59';
				$discount->save(); // save instance
				//redirect to discounts list
				return Redirect::to('/admin/discounts');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.28: postEditDiscount
	//  Description: post process after submit on edit discount
    //-----------------------------------------------------
	public function postEditDiscount($id)
	{
		try{
			$rules = array(		//validation rules
		        'discount_code'	=> 'required',
		        'email'	=> 'required|email',
		        'business'	=> 'required',
				'amount' => 'required|numeric',
				'valid_from' => array('required', 'date_format:"Y-m-d"'),
				'valid_to' => array('required', 'date_format:"Y-m-d"')
			);

			$validator = Validator::make(Input::all(), $rules); // run validation

			if($validator->fails()) // if fail, redirect back with error message
			{
				return Redirect::to('/admin/edit_discount/'.$id)->withErrors($validator->errors())->withInput();
			}
			else
			{
				// get discount instance in db, udpate fields and save
				$discount = Discount::find($id);
				$discount->bank = Input::get('organization');
				$discount->discount_code = Input::get('discount_code');
				$discount->email = Input::get('email');
				$discount->business = Input::get('business');
				$discount->type = Input::get('type');
				$discount->amount = Input::get('amount');
				$discount->valid_from = Input::get('valid_from') . ' 00:00:00';
				$discount->valid_to = Input::get('valid_to') . ' 23:59:59';
				$discount->save();

				// redirect back to page
				return Redirect::to('/admin/discounts');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.29: getSwitchDiscount
	//  Description: function to turn on/off discount
    //-----------------------------------------------------
	public function getSwitchDiscount($type, $id)
	{
		try{
			$discount = Discount::find($id);  // get discount
			switch($type)
			{
				case "on":
					$discount->status = 1;
					break;
				case "off":
					$discount->status = 0;
					break;
			}
			$discount->save(); // update status and save
			//redirect back
			return Redirect::to('/admin/discounts');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.30: getDeleteDiscount
	//  Description: function to delete discount
    //-----------------------------------------------------
	public function getDeleteDiscount($id)
	{
		try{
			$discount = Discount::find($id)->delete(); // get discount in db and delete
			return Redirect::to('/admin/discounts'); // return back
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.31: getAutoUpdateDownload
	//  Description: function to download from bsp during auto update
    //-----------------------------------------------------
	public function getAutoUpdateDownload($filename)
	{
		try{
			set_time_limit(0);
			$site       = 'http://www.bsp.gov.ph/downloads/Publications/2017/';	// the url of the site to download
	        $url        = $site.$filename;

			if(!file_exists('temp/'.$filename)){
				file_put_contents(public_path().'/temp/'.$filename, fopen($url, 'r')); // save in server
			}

			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.32: getAutoUpdateParse
	//  Description: function to process downloaded bsp for auto update
    //-----------------------------------------------------
	public function getAutoUpdateParse($filename)
	{
		try{
			// background process: run pdf to text parser on the file, convert it to plain text
			exec('pdftotext -eol dos -layout temp/'.$filename.' temp/parsed.txt');

			// variable declaration
			$start = false;				// used for flagging in while loop
			$start_data = false;		// used for flagging in while loop
			$parsed_array = array();
			$handle = fopen('temp/parsed.txt', "r");
			if ($handle) {
				while (($line = fgets($handle)) !== false) { //open file and iterate line
					// process the line read.
					if(strpos($line, 'Business Outlook Index on Own Operations: Current Quarter')!==false){ // check if text found
						$start = true; // flag update
					}
					if($start){
						if(strpos($line, 'Industry Sector')!==false){  // check if text found
							$start_data = true; // flag update
						}
					}
					if($start_data){
						$parsed_array[] = $line; //save parsed line to array
					}
					if($start && strpos($line, 'Transportation')!==false){ // check if text found
						$start = false; // flag update
						$start_data = false; // flag update
					}
				}

				fclose($handle); // close file
			} else {
				// error opening the file.
			}

			$bsp_data = array();  // var used as container
			foreach($parsed_array as $key=>$pa){ //for each stored line in parsed array
				$parsed = explode(' ', preg_replace( '/\s+/', ' ', $pa )); // remove whitespace and break to array
				if(trim($parsed[@count($parsed)-1])=="") unset($parsed[@count($parsed)-1]);  //remove end

				$length = @count($parsed);	// get length of array
				for($ctr = 0; $ctr < 8; $ctr++){
					$bsp_data[$key][$ctr] = (float)$parsed[$length-(8-$ctr)];
				}

			}

			/*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $industry_count = 18;

	        $biz_outlook_db     = new BusinessOutlook;
	        $regression_lib     = new RegressionCalcLib;

	        /*--------------------------------------------------------------------
			/*	Match the BSP Industries with Government industries list
			/*------------------------------------------------------------------*/
	        for ($idx = 1; $idx <= $industry_count; $idx++) {
	            $industry_db    = Industrymain::find($idx);

	            switch($idx) {
	                case INDUSTRY_AGRICULTURE:
	                case INDUSTRY_MINING:
	                case INDUSTRY_MANUFACTURING:
	                case INDUSTRY_ELECTRICITY:
	                case INDUSTRY_WATER:
	                    $bsp_idx = INDUSTRY_BSP_INDUSTRY;
	                    break;

	                case INDUSTRY_CONSTRUCTION:
	                    $bsp_idx = INDUSTRY_BSP_CONSTRUCTION;
	                    break;

	                case INDUSTRY_WHOLESALE:
	                    $bsp_idx = INDUSTRY_BSP_WHOLESALE;
	                    break;

	                case INDUSTRY_TRANSPORTATION:
	                    $bsp_idx = INDUSTRY_BSP_TRANSPORTATION;
	                    break;

	                case INDUSTRY_ACCOMODATION:
	                    $bsp_idx = INDUSTRY_BSP_ACCOMODATIONS;
	                    break;

	                case INDUSTRY_COMMUNICATION:
	                case INDUSTRY_SCIENTIFIC:
	                case INDUSTRY_OTHER_SERVICES:
	                    $bsp_idx = INDUSTRY_BSP_SERVICES;
	                    break;

	                case INDUSTRY_FINANCE:
	                    $bsp_idx = INDUSTRY_BSP_FINANCE;
	                    break;

	                case INDUSTRY_REAL_ESTATE:
	                    $bsp_idx = INDUSTRY_BSP_REAL_ESTATE;
	                    break;

	                case INDUSTRY_ADMINISTRATIVE:
	                    $bsp_idx = INDUSTRY_BSP_BUSINESS;
	                    break;

	                case INDUSTRY_HEALTH:
	                case INDUSTRY_EDUCATION:
	                case INDUSTRY_ENTERTAINMENT:
	                    $bsp_idx = INDUSTRY_BSP_SOCIAL_SERVICE;
	                    break;
	            }

	            $biz_outlook_data['biz-outlook-q1'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_1];
	            $biz_outlook_data['biz-outlook-q2'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_2];
	            $biz_outlook_data['biz-outlook-q3'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_3];
	            $biz_outlook_data['biz-outlook-q4'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_4];
	            $biz_outlook_data['biz-outlook-q5'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_5];
	            $biz_outlook_data['biz-outlook-q6'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_6];
	            $biz_outlook_data['biz-outlook-q7'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_7];
	            $biz_outlook_data['biz-outlook-q8'] = $bsp_data[$bsp_idx][BSP_REPORT_QUARTER_8];

	            /*----------------------------------------------------------------
	            /*	Saves the business outlook data to the Database
	            /*--------------------------------------------------------------*/
	            $biz_outlook_db->addUpdateBusinessOutlook($idx, $biz_outlook_data);

	            /*----------------------------------------------------------------
	            /*	Calculate slope of Business Outlook
	            /*--------------------------------------------------------------*/
	            $xy_list    = array(
	                0   => array($biz_outlook_data['biz-outlook-q1'], 1),
	                1   => array($biz_outlook_data['biz-outlook-q2'], 2),
	                2   => array($biz_outlook_data['biz-outlook-q3'], 3),
	                3   => array($biz_outlook_data['biz-outlook-q4'], 4),
	                4   => array($biz_outlook_data['biz-outlook-q5'], 5),
	                5   => array($biz_outlook_data['biz-outlook-q6'], 6),
	                6   => array($biz_outlook_data['biz-outlook-q7'], 7),
	                7   => array($biz_outlook_data['biz-outlook-q8'], 8)
	            );

	            $regression_results = $regression_lib->calculateRegression($xy_list);

	            /*----------------------------------------------------------------
	            /*	Determines the Industry Trend and save into database
	            /*--------------------------------------------------------------*/
	            $industry_db->industry_score   = $regression_results[REGRESSION_PTS_ALLOC];

				switch($regression_results[REGRESSION_PTS_ALLOC]){
	                case BIZ_OUTLOOK_UP_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_UP_TREND;
	                    break;

	                case BIZ_OUTLOOK_FLAT_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_FLAT_TREND;
	                    break;

	                case BIZ_OUTLOOK_DOWN_POINT:
	                    $industry_db->industry_trend = BIZ_OUTLOOK_DOWN_TREND;
	                    break;
				}

				$industry_db->save(); // save
	        }

			return "success";
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.33: updateBizOutlookCron
	//  Description: function for scheduled process
    //-----------------------------------------------------
    public function updateBizOutlookCron()
    {
        try{
	        $file_data  = $this->detectBspExpectation(); // call detectBspExpectation function

	        $file_data  = json_decode($file_data);
	        var_dump($file_data);
	        if (STS_OK == $file_data->valid) { // check if detected update is req
	            $dl_sts = $this->getAutoUpdateDownload($file_data->filename); // call getAutoUpdateDownload

	            if ('success' == $dl_sts) { // if downloaded
	                $this->getAutoUpdateParse($file_data->filename); // call getAutoUpdateParse
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.34: getLeads
	//  Description: get leads page
    //-----------------------------------------------------
	public function getLeads()
	{
		try{
			$leads = EmailLead::orderBy('submitted_date', 'desc')->get(); 		// get saved leads in db
			$config = Keys::where('name', 'leads_cron_email')->first();			// get leads email in db
			$cron_email = ($config!=null) ? $config->value : '';				// if null set default value
			return View::make('admin.leads' , array(							// show page
				'leads' => $leads,
				'cron_email' => $cron_email
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.35: getLeadsExport
	//  Description: function for exporting leads as csv
    //-----------------------------------------------------
	public function getLeadsExport()
	{
		try{
			$leads = EmailLead::select(
							'id as ID', 
							'email as Email', 
							'first_name as First Name', 
							'last_name as Last Name', 
							'company_name as Company Name', 
							'contact_number as Contact Number', 
							'job_title as Job Title',
							'submitted_date as Submitted Date')
					->orderBy('id', 'desc')->get();		// get leads
			$leads = $leads->toArray();					// convert to array

			Excel::create('Leads', function($excel) use ($leads) {
				$excel->sheet('Leads', function($sheet) use($leads){
					$sheet->fromArray($leads);
				});
			})->download('xlsx');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.36: postSetLeadsEmail
	//  Description: function to update leads email
    //-----------------------------------------------------
	public function postSetLeadsEmail()
	{
		try{
			$email = Input::get('email');		// get new email for leads from input
			$config = Keys::where('name', 'leads_cron_email')->update(array('value'=>$email));  // update and save
			return "Updated new leads email to ". $email;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.37: getSupervisorApplications
	//  Description: get Supervisor Application page
    //-----------------------------------------------------
    public function getSupervisorApplications()
	{
		try{
			if (Session::has('supervisor')){
				Session::forget('supervisor');
			}

			$approvedWhere = [
				'tbllogin.role = 4'
			];

			$notApproveddWhere = [
				'supervisor_application.status = 0'
			];

			/*Check if search/filter was used*/
			if(Input::has('type')) {
				if(Input::get('type') == 'email') {
					/*Get supervisor created from trial accounts and supervisor that are approved in supervisor application with the given email*/
					$approvedWhere[] = 'tbllogin.email LIKE "%' . Input::get('search') . '%"';

					/*Get supervisor created from supervisor application but not yet approved with the given email*/
					$notApproveddWhere[] = 'supervisor_application.email LIKE "%' . Input::get('search') . '%"';
					

				} else if (Input::get('type') == 'organization'){
					/*Get supervisor created from trial accounts and supervisor that are approved in supervisor application with the given organization*/
					$approvedWhere[] = 'tblbank.bank_name LIKE "%' . Input::get('search') . '%"';

					/*Get supervisor created from supervisor application but not yet approved with the given organization*/
					$notApproveddWhere[] = 'tblbank.bank_name LIKE "%' . Input::get('search') . '%"';
				}
			}

			$approvedWhere = implode(' AND ',$approvedWhere);
			$notApproveddWhere = implode(' AND ',$notApproveddWhere);

			/*Get supervisor created for trial accounts and supervisor that are approved in supervisor application*/
			$approvedAccount = DB::table('tbllogin')
			->select(DB::raw("
				tbllogin.loginid,
				supervisor_application.application_id,
				CONCAT(tbllogin.firstname, ' ', tbllogin.lastname) as name_opt,
				tbllogin.name,
				tbllogin.email,
				DATE(tbllogin.created_at) as date_registered,
				tblbank.bank_type as organization_type,
				tblbank.bank_name as organization,
				tblbank.questionnaire_type as questionnaire_type,
				supervisor_application.product_features as product,
				supervisor_application.valid_id_picture as picture,
				supervisor_application.status,
				supervisor_application.application_date,
				tblbanksubscriptions.expiry_date,
				tblbanksubscriptions.status as expiry_status,
				tblbanksubscriptions.updated_at as sub_updated_at,
	            tblbanksubscriptions.created_at as expiry_start,
	            (CASE
	            	WHEN tblbanksubscriptions.expiry_date > NOW() THEN 'Active'
	            	WHEN tblbanksubscriptions.status = 0  THEN 'Unpaid'
	            	WHEN (tblbanksubscriptions.status != 1 AND DATEDIFF(CURDATE(),tblbanksubscriptions.updated_at) <= 30 AND tblbanksubscriptions.expiry_date > CURDATE()) THEN 'Unpaid'
	            	ELSE 'Unpaid'
	            END) AS subscription_status
	            "
	        ))
			->whereRaw($approvedWhere)
			->leftJoin('tblbank', 'tbllogin.bank_id', '=', 'tblbank.id')
			->leftJoin('supervisor_application', 'tbllogin.email', '=', 'supervisor_application.email')
			->leftJoin('tblbanksubscriptions', 'tblbanksubscriptions.bank_id', '=', 'tbllogin.loginid');

			/*Get supervisor created from supervisor application but not yet approved */
			$notApprovedAccount = DB::table('supervisor_application')
						->select(DB::raw("
							0 as loginid,
							supervisor_application.application_id,
							CONCAT(supervisor_application.firstname, ' ', supervisor_application.lastname) as name_opt,
							CONCAT(supervisor_application.firstname, ' ', supervisor_application.lastname) as name,
							supervisor_application.email,
							supervisor_application.application_date as date_registered,
							tblbank.bank_type as organization_type,
							tblbank.bank_name as organization,
							tblbank.questionnaire_type as questionnaire_type,
							supervisor_application.product_features as product,
							supervisor_application.valid_id_picture as picture,
							supervisor_application.status,
							supervisor_application.application_date,
							'0000-00-00 00:00' AS expiry_date,
							'0' AS expiry_status,
							'0000-00-00 00:00' AS sub_updated_at,
							'0000-00-00 00:00' AS expiry_start,					
							'Unpaid' AS subscription_status"
						))
						->whereRaw($notApproveddWhere)
						->leftJoin('tblbank', 'supervisor_application.bank_id', '=', 'tblbank.id');

			/* Merged and sort supervisor account */
			$supervisors = $approvedAccount->union($notApprovedAccount);

			$limit = 50;
			/* Pagination Code */
			$paginator = new Paginator($supervisors->get()->toArray(), $limit);
			if(Input::has('page'))
			{
				$supervisors->offset((Input::get('page') - 1) * $limit); // compute for which page to display
			}
			$supervisors = $supervisors->orderBy('application_date','DESC')->take($limit)->get(); 	// order users by descending created date

			$paginator->setPath(url('supervisor/list'));

			return View::make('admin.supervisor-application-list',
			array(
				'supervisors'=> $supervisors,
				'paginator' => $paginator
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.38: approveSupervisorApplication
	//  Description: function to approve supervisor application
    //-----------------------------------------------------
    public function approveSupervisorApplication($status,$application_id)
	{
		try{
			if($status == 'reject'){
				$supervisor = SupervisorApplication::find($application_id);
				$supervisor->status     = 2;
				$supervisor->save();
	        }

	        if($status == 'delete'){
				$supervisor = SupervisorApplication::find($application_id);
				$supervisor->status     = 3;
				$supervisor->save();
	        }

	        if($status == 'approve'){

	        	$application_db = new SupervisorApplication;		// create SupervisorApplication instance
		        $user_db        = new User;							// create User instance

		        $supervisor     = $application_db->getSupervisorApplication($application_id);  // get application by id
				// populate user fields
		        $user_db->name = $supervisor->firstname. ' ' .$supervisor->lastname;
		        $user_db->email         = $supervisor->email;
		        $user_db->role          = USER_ROLE_BANK;
		        $user_db->status        = STS_NG;
		        $user_db->password      = $supervisor->password;
				$user_db->product_plan  = $supervisor->product_features;
				$user_db->firstname     = $supervisor->firstname;
				$user_db->lastname      = $supervisor->lastname;

				$user_db->bank_id   = $supervisor->bank_id;

		        $user_db->save(); // save user

				// create instance of questionnaire config used for supervisor backend
		        $question_db            = new QuestionnaireConfig;
		        $question_upd           = $question_db->addConfigByQuestionType($supervisor->bank_id, $supervisor->question_type);

		        $supervisor->status     = STS_OK;
		        $db_sts                 = $supervisor->save();

				// email supervisor if approved
		        if (STS_OK == $db_sts) {
					if (env("APP_ENV") == "Production") {

						try{
							Mail::send('emails.supervisor-approved', array('input_data' => $supervisor), function($message) use ($supervisor) {
								$message->to($supervisor->email, $supervisor->email)
									->bcc('notify.user@creditbpo.com', 'CreditBPO')
									->subject('CreditBPO account activated');

								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							});
						}catch(Exception $e){
			            //
			            }

					} else if (env("APP_ENV") != "local") {
						
						try{
							Mail::send('emails.supervisor-approved', array('input_data' => $supervisor), function($message) use ($supervisor) {
								$message->to($supervisor->email, $supervisor->email)
									->subject('CreditBPO account activated');

								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							});
						}catch(Exception $e){
			            //
			            }
					}
				}
	        }
	        return Redirect::to('/supervisor/list');
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.39: getAdminTransactions
	//  Description: get dragonpay and paypal transactions list
    //-----------------------------------------------------
	public function getAdminTransactions(){

		try{
			// variable declaration
			$limit = 20;							// page limit

			// filtering
			$vector = Input::get('vector');			// get vector from input
			$status = Input::get('status');			// get status from input
			$currency = Input::get('currency');		// get currency from input
			$amount = Input::get('amount');			// get amount from input

			// sql style db get
			if($vector == 'dragonpay') {
				$sql_string = "
					SELECT * FROM
					(SELECT t.loginid, t.refno as reference, 'DragonPay' as vector, l.email,
					t.description, IF(t.status = 'S', 'Paid', IF(t.status = 'P', 'Pending', '')) as status, t.amount,
					t.ccy as currency, t.created_at
					FROM tbltransactions t
					LEFT JOIN tbllogin l ON t.loginid = l.loginid) tr WHERE status != ''";
			} elseif($vector == 'paypal') {
				$sql_string = "
					SELECT * FROM
					(SELECT p.loginid, p.transactionid as reference, 'Paypal' as vector,
					l.email, '' as description, 'Paid' as status,  p.paymentamount as amount,
					p.currencycode as currency, p.created_at
					FROM tblpayment p
					LEFT JOIN tbllogin l ON p.loginid = l.loginid) tr WHERE 1 = 1";
			} else {
				$sql_string = "
					SELECT * FROM
					((SELECT p.loginid, p.transactionid as reference, 'Paypal' as vector,
					l.email, '' as description, 'Paid' as status,  p.paymentamount as amount,
					p.currencycode as currency, p.created_at
					FROM tblpayment p
					LEFT JOIN tbllogin l ON p.loginid = l.loginid)

					UNION

					(SELECT t.loginid, t.refno as reference, 'DragonPay' as vector, l.email,
					t.description, IF(t.status = 'S', 'Paid', IF(t.status = 'P', 'Pending', '')) as status, t.amount,
					t.ccy as currency, t.created_at
					FROM tbltransactions t
					LEFT JOIN tbllogin l ON t.loginid = l.loginid)) tr WHERE status != ''";
			}

			if($status == 'Paid'){  // if status filter is on
				$sql_string .= " AND tr.status = 'Paid'";
			} elseif($status == 'Pending') {
				$sql_string .= " AND tr.status = 'Pending'";
			}

			if($currency) { // if status currency is on
				$sql_string .= " AND tr.currency = '".$currency."'";
			}

			if($amount) { // if status amount is on
				$sql_string .= " AND tr.amount = ".$amount."";
			}

			$sql_string .= " ORDER BY tr.created_at DESC";

			$transactions_raw = DB::select(DB::raw($sql_string));
			//JM - applied new paginator code
			$paginator = new Paginator($transactions_raw, $limit); //Paginator::make($transactions_raw, @count($transactions_raw), $limit); // make pagination
			if(Input::has('page')) // compute for page to show
			{
				$sql_string .= " LIMIT ".$limit." OFFSET ".((Input::get('page') - 1) * $limit);
			} else {
				$sql_string .= " LIMIT ".$limit;
			}

			$transactions = DB::select(DB::raw($sql_string));  // get results

			//show page
			return View::make('admin.transactions',
	            array(
	                'transactions'   => $transactions,
					'paginator' => $paginator
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.40: getDevRegForm
	//  Description: Displays the Developer Registration Page
    //-----------------------------------------------------
    public static function getDevRegForm()
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
			/*------------------------------------------------------------------*/

	        /*	show page    */
	        return View::make('admin.register-developer');
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 2.41: getDevUserList
	//  Description: Displays the Developer List
    //-----------------------------------------------------
    public static function getDevUserList()
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $dev_obj    = new ApiDeveloper;

	        $developers = $dev_obj->getApiDevelopers();

	        /*	show page    */
	        return View::make('admin.developer-list',
	            array(
	                'developers' => $developers
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.42: postDevRegForm
	//  Description: Adds the Developer to the Database
    //-----------------------------------------------------
    public static function postDevRegForm()
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $sts            = STS_NG;
	        $data['lname']  = Input::get('lname');
	        $data['fname']  = Input::get('fname');
	        $data['email']  = Input::get('email');
	        $data['pword']  = Input::get('pword');

	        /** Sets validation rules and messages  */
			$rules = array(
				'lname'             => 'required',
		        'fname'             => 'required',
		        'email'             => 'required|email|unique:developer_api_users',
		        'pword'             => 'required'
		    );

	        $messages = array(
			    'lname.required'    => 'The Lastname field is required.',
			    'fname.required'    => 'The Firstname field is required.',
			    'email.required'    => 'The Email Address field is required.',
			    'email.email'       => 'Invalid Email Format.',
			    'email.unique'      => 'The Email provided is already used.',
			    'pword.required'    => 'The Password field is required.',
			);

	        /*--------------------------------------------------------------------
	        /*	Validate inputs
	        /*------------------------------------------------------------------*/
	        $validator = Validator::make(Input::all(), $rules, $messages);

	        /*--------------------------------------------------------------------
	        /*	There are invalid fields
	        /*------------------------------------------------------------------*/
			if ($validator->fails()) {
				return Redirect::to('/register/developers')->withErrors($validator)->withInput();
			}
	        /*--------------------------------------------------------------------
	        /*	All fields are valid
	        /*------------------------------------------------------------------*/
			else {
	            $dev_obj    = new ApiDeveloper;
	            $sts        = $dev_obj->addNewApiUser($data);

	            /*---------------------------------------------------------------
	            /*	Database entry is successful
	            /*-------------------------------------------------------------*/
	            if (STS_OK == $sts) {
	                return Redirect::to('/developers/list');
	            }
	            /*---------------------------------------------------------------
	            /*	Data was not saved to Database
	            /*-------------------------------------------------------------*/
	            else {
	                return Redirect::to('/register/developers')->withErrors($validator)->withInput();
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.43: changeDevSts
	//  Description: Activate or Deactivate a Developer API User's Account
    //-----------------------------------------------------
    public static function changeDevSts($sts, $client_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $dev_obj        = new ApiDeveloper;
	        $process_sts    = $dev_obj->updateDeveloperStatus($sts, $client_id);

	        /*	redirect to developers list    */
	        return Redirect::to('/developers/list');
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.44: getAdminReviews
	//  Description: get Admin Review page
    //-----------------------------------------------------
	public function getAdminReviews()
	{
		try{
			$where = [
				'tblentity.status = 7',
				'tblentity.admin_review_flag = 1'
			];

			/*Check if search/filter was used*/
			if(Input::has('type')) {
				if(Input::get('type') == 'email') {
					
					$where[] = 'tblentity.email LIKE "%' . Input::get('search') . '%"';
					

				} else if (Input::get('type') == 'organization'){
					
					$where[] = 'tblentity.companyname LIKE "%' . Input::get('search') . '%"';
				}
			}

			$where = implode(' AND ', $where);

			// get questionnaire lists to review
			$entity = DB::table('tblsmescore')
			->select(
				'tblentity.entityid',
				'tblentity.companyname',
				'tblentity.email',
				'tblentity.date_established',
				'tblentity.number_year',
				'tblentity.is_premium',
				'tblindustry_row.row_title',
				'refcitymun.citymunDesc',
				'tblentity.status',
				'tbllogin.firstname',
				'tbllogin.lastname',
				'tblsmescore.score_sf',
	            'tblsmescore.score_rfp',
	            'tblsmescore.score_facs',
				'tblscoring.created_at as start_date',
				'tblsmescore.created_at as end_date'
			)
			->join('tblentity', 'tblsmescore.entityid', '=', 'tblentity.entityid')
			->join('tblscoring', 'tblsmescore.entityid', '=', 'tblscoring.entityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
			->join('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->join('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->join('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->join('tbllogin', 'tblsmescore.loginid', '=', 'tbllogin.loginid')
			->whereRaw($where)
			->groupBy('tblentity.entityid')
			->simplePaginate(50);

			// Update score as readable
			if($entity!=null){
				foreach($entity as $key=>$value){
	                $rating = FinancialRating::getPositionRating($value->score_facs);
	                $entity[$key]->score_sf = $rating['score'];
	                $entity[$key]->rating_color = $rating['color'];

					/* Compute for time elapsed (process time)  for display */
					$span = strtotime($value->end_date) - strtotime($value->start_date);
					$days = floor($span / (60*60*24));
					$hours = floor(($span % (60*60*24)) / (60*60));
					$minutes = floor(($span % (60*60)) / 60);
					$seconds = floor($span % 60);
					$entity[$key]->process_time =  $days.'d '.$hours.'h '.$minutes.'m '.$seconds.'s';
				}
			}
			// show page
			return View::make('admin.admin_review')->with('entity', $entity);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.45: publishEntity
	//  Description: function to set Admin reviewed Report summary as published
    //-----------------------------------------------------
	public function publishEntity($entity_id)
	{
		try{
			if($entity_id > 0){
				Entity::where('entityid', '=', $entity_id)
	                ->update([
	                    'admin_review_flag' => 0,
	                    'completed_date' => date('c'),
	                ]);  // update flag and save
			}
			return Redirect::to('/admin/reviews');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.46: redactEntity
	//  Description: function to redact Admin reviewed Report summary
    //-----------------------------------------------------
	public function redactEntity($entity_id)
	{
		try{
			if($entity_id > 0){
				Entity::where('entityid', '=', $entity_id)->update(array('status'=>6)); // update entity status back
			}
			return Redirect::to('/admin/reviews');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.47: getTrialAccounts
	//  Description: get trial accounts page
    //-----------------------------------------------------
	public function getTrialAccounts()
	{
		try{
			// get the accounts
			$accounts = DB::table('tbllogin')
				->select(['tbllogin.loginid', 'tbllogin.email', 'tblentity.companyname',
					'tblentity.entityid', 'tblentity.is_paid', 'tblindustry_main.main_title',
					'tbllogin.created_at', 'tbllogin.updated_at', 'tblentity.completion',
					'activity_logs.first_activity', 'activity_logs.last_activity', 'activity_logs.first_submission', 'tblbank.bank_name'])
				->leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
				->leftJoin('tblindustry_main', 'tblindustry_main.main_code', '=', 'tblentity.industry_main_id')
				->leftJoin('activity_logs', 'tblentity.entityid', '=', 'activity_logs.entity_id')
	            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
				->where('tbllogin.trial_flag', 1)
				->orderBy('tbllogin.loginid', 'desc')
				->simplePaginate(50);

			// get trial summary email
	        $email_recipients   = Keys::where('name', 'trial_summary_emails')->first();

	        $industry_list      = Industrymain::get();

	        if ($email_recipients) { // check if exist
	            $receiver   = $email_recipients->value;
	        }
	        else { // if not set default value
	            $receiver   = 'notify.user@creditbpo.com';
	            //$receiver   = 'ruffy.collado@creditbpo.com';
	        }

			// show page
			return View::make('admin.trial_accounts',
	            array(
	                'receiver'      => $receiver,
	                'industry_list' => $industry_list
	            )
	        )->with('accounts', $accounts);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.48: getCreateTrialAccounts
	//  Description: get trial accounts create page
    //-----------------------------------------------------
	public function getCreateTrialAccounts()
	{
		try{
			return View::make('admin.create_trial_account');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.49: postCreateTrialAccounts
	//  Description: post process to create trail accounts
    //-----------------------------------------------------
	public function postCreateTrialAccounts()
	{
		try{
			$messages = array(	// validation message
				'bank_id.required' => 'The organization is required.',
				'email.required' => 'The Email field is required.',
				'email.unique' => 'The Email you are trying to register is already taken.',
				'password.required' => 'The Password field is required.'
			);

			$rules = array(		// validation rules
				'bank_id'=>'required',
				'email'	=> 'required|unique:tbllogin,email|email|unique:supervisor_application,email',
				'password'	=> 'required|min:6'
		    );

			$validator = Validator::make(Input::all(), $rules, $messages);  // run validation

			if($validator->fails()) {	// if error redirect back and show error
	            return Redirect::route('getCreateTrialAccounts')->withErrors($validator)->withInput();
			}
			else
			{
				/*Get bank details*/
				$bankDb= new Bank;
				$bank = $bankDb->getOrganizationById(Input::get('bank_id'));

	            if (1 == Input::get('create_supervisor_account')) {
					$mail_exp   = explode('@', Input::get('email'));
					
					if(Input::get('supervisorEmail') != null || Input::get('supervisorEmail') != ""){
						$sup_email = Input::get('supervisorEmail');
					}else{
						$sup_email  = 'web+trial_'.$mail_exp[0].'@creditbpo.com';
					}

					$sup_pass = "";

					if(Input::get('supervisorPassword') != null || Input::get('supervisorPassword') != ""){
						$sup_pass = Hash::make(Input::get('supervisorPassword'));
					}else{
						$sup_pass = Hash::make("tR^iAl&22");
					}

	                $supervisor                 = new User();
	                $supervisor->email          = $sup_email;
	                $supervisor->password       = $sup_pass;
	                $supervisor->job_title      = "Supervisor";
	                $supervisor->name      		= "Supervisor ".$bank->bank_name;
	                $supervisor->role           = 4;
	                $supervisor->status         = 2;
	                $supervisor->trial_flag     = 0;
	                $supervisor->bank_id        = $bank->id;

	                $supervisor->save();

	                $payment                    = new Transaction();
	                $payment->loginid           = $supervisor->loginid;
	                $payment->amount            = 10000.00;
	                $payment->ccy               = 'PHP';
	                $payment->status            = 'S';
	                $payment->description       = 'Bank User Subscription';
	                $payment->created_at        = date('Y-m-d H:i:s');
	                $payment->updated_at        = date('Y-m-d H:i:s');

	                $payment->save();

	                $subscribe                  = new BankSubscription();
	                $subscribe->bank_id         = $supervisor->loginid;
	                $subscribe->expiry_date     = date('Y-m-d H:i:s', strtotime('+1 month'));
	                $subscribe->created_at      = date('Y-m-d H:i:s');
	                $subscribe->updated_at      = date('Y-m-d H:i:s');

	                $subscribe->save();
	            }

				// create the user, pre-populate some fields
				$user = new User();
				$user->email = Input::get('email');
				$user->name = "First Name Last Name";
				$user->password = Hash::make(Input::get('password'));
				$user->job_title = "Owner Title";
				$user->role = 1;
				$user->status = 2;
				$user->trial_flag = 1;
				$user->save();

				$loginid = $user->loginid;

				// create the enitity/questionnaire with pre populated fields
				$entity = new Entity();
				$entity->loginid = $loginid;
				$entity->companyname = $bank->bank_name . ' Company Name';
				$entity->entity_type = 0;
				$entity->website = '';
				$entity->is_agree = 1;
				$entity->is_paid = 1;
				$entity->status = 0;
				$entity->current_bank = $bank->id;
				$entity->is_independent = 1;
				$entity->cityid = 1380; /** Location */
				$entity->province = "Abra";
				$entity->countrycode = "PH";
				$entity->zipcode = 2800;
				$entity->industry_main_id = "A"; /** Industry */
				$entity->industry_sub_id = "01";
				$entity->industry_row_id = "011";
				$entity->save();

				// create 2nd entity as free questionnaire
				$entity = new Entity();
				$entity->loginid = $loginid;
				$entity->companyname = $bank->bank_name . ' Company Name 2';
				$entity->entity_type = 0;
				$entity->website = '';
				$entity->is_agree = 1;
				$entity->is_paid = 1;
				$entity->status = 0;
				$entity->current_bank = $bank->id;
				$entity->is_independent = 1;
				$entity->cityid = 1380; /** Location */
				$entity->province = "Abra";
				$entity->countrycode = "PH";
				$entity->zipcode = 2800;
				$entity->industry_main_id = "A"; /** Industry */
				$entity->industry_sub_id = "01";
				$entity->industry_row_id = "011";
				$entity->save();

				//redirect back on finish
				return Redirect::route('getTrialAccounts');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.50: postSendTrialLogin
	//  Description: function to email the trial account credentials
    //-----------------------------------------------------
    public function postSendTrialLogin($entity_id)
	{
		try{
			// get inputs
	        $email  = Input::get('email');
	        $pword  = Input::get('password');

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

			$rules = array(		// validation rules
				'email'     => 'required',
				'password'	=> 'required',
		    );

	        $messages = array(		// validation message
				'email.required'    => 'The Email field is required.',
				'password.required' => 'The Password field is required.',
			);

			$validator = Validator::make(Input::all(), $rules, $messages); // run validation

	        if ($validator->fails()) { // if fail store error messages in response
	            $srv_resp['messages']	= $validator->messages()->all();
	        }
	        else {
	            $account_data   = DB::table('tblentity')
	                ->select(['tblentity.companyname', 'tbllogin.email'])
	                ->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
	                ->where('tblentity.entityid', $entity_id)
	                ->first();

	            $exp_email  = explode(',', $email);		//  break down email addresses separated by comma

				// for each email addresses
				if (env("APP_ENV") != "local") {
					foreach($exp_email as $recipient) {

						try{
							// send email regarding credentials
							Mail::send('emails.trial-login', array(
								'company_name'  => $account_data->companyname,
								'username'      => $account_data->email,
								'password'      => $pword
							),
							function($message) use ($recipient) {
								$message->to($recipient, $recipient)
									->subject('CreditBPO Login Request')
									->attach('images/cbpo_free_trial_mk1.00.pdf');

								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
							});

						}catch(Exception $e){
			            	Log::error($e->getMessage());
			            }
					}
				}

	            $srv_resp['sts']    = STS_OK;
	        }

			// return json response
	        return json_encode($srv_resp);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

	//-----------------------------------------------------
    //  Function 2.51: getAdminScrapper
	//  Description: get BSP scrapper page
    //-----------------------------------------------------
	public function getAdminScrapper()
	{
		try{
			return View::make('admin.scrapper');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.52: getRunAdminScrapper
	//  Description: function for scrapper processing
    //-----------------------------------------------------
	public function getRunAdminScrapper()
	{
		try{
			error_reporting(E_ALL);
			ini_set('display_errors', 0);
			ini_set('log_errors', 1);
			ini_set('max_execution_time', 0);
			ini_set('memory_limit', '1024M');
			libxml_use_internal_errors(true);
			date_default_timezone_set('UTC');
			ini_set('output_buffering', 'off');
			// Turn off PHP output compression
			ini_set('zlib.output_compression', false);
			// Implicitly flush the buffer(s)
			ini_set('implicit_flush', true);
			ob_implicit_flush(true);

			#Refresh Cookie File
			define('COOKIE_FILE', 'temp/cookies.txt');
			if(file_exists(COOKIE_FILE)) {
				unlink(COOKIE_FILE);
			}

			$bsp_directory = array(
				// Universal and Commercial Banks
				array(
					"bank_type"	=> "Bank",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID%20eq%20%271%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Thrift Banks
				array(
					"bank_type"	=> "Bank",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID%20eq%20%2710%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Rural & Cooperative Banks
				array(
					"bank_type"	=> "Bank",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID%20eq%20%2711%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Non-Banks with Quasi-Banking Functions
				array(
					"bank_type"	=> "Non-Bank Financing",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID2%20eq%20%2714%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Non-Stock Savings and Loan Associations (NS)
				array(
					"bank_type"	=> "Non-Stock Savings and Loan Associations",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID2%20eq%20%2715%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Offshore Banking Units in the Philippines
				array(
					"bank_type"	=> "Bank",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID2%20eq%20%2716%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Representative Offices in the Philippines
				array(
					"bank_type"	=> "Bank",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID2%20eq%20%2717%27&%24top=5000&%24orderby=Title%20asc"
				),
				// Non-Banks without Quasi-Banking Functions
				array(
					"bank_type"	=> "Non-Bank Financing",
					"bank_url"	=> "https://www.bsp.gov.ph/_api/web/lists/getByTitle('Institutions')/items?%24select=*&%24filter=InstitutionTypeID2%20eq%20%2718%27&%24top=5000&%24orderby=Title%20asc"
				),
			);

			$bank = [];
			$count = 0;

			foreach($bsp_directory as $key => $bsp){

				/** Variable Declaraation */
				$url = $bsp['bank_url'];

				$options = array(
					'follow_location' => true,
					'cookie_file' => COOKIE_FILE,
					'wait_duration' => rand(3,5),
				);

				/** Run CURL Request */
				list($success, $result, $curl_info) = self::http_curl_request($url, $options);
				$new_results = (explode("&#xD", $result));

				foreach($new_results as $res){
					/** Check if all fields needed for organization is present in the array */
					if( (strpos($res, '<d:Title') !== false) && (strpos($res, '<d:President') !== false) && (strpos($res, '<d:Address') !== false) && (strpos($res, '<d:NumOffice') !== false) &&  
					(strpos($res, '<d:Authority') !== false) && (strpos($res, '<d:Contact') !== false) && (strpos($res, '<d:Status') !== false) && (strpos($res, '<d:Email') !== false) &&
					(strpos($res, '<d:Website') !== false) && (strpos($res, '<d:Fax') !== false) ){

						/** Organization Name */
						if (preg_match('/<d:Title>(.*?):Title>/', $res, $title) == 1) {
							if(isset($title[0])){
								$organization_name = $title[0];
								$bank[$count]['organization_name'] = str_replace(array("<d:Title>", "</d:Title>"),"",$organization_name);
							}
						}

						/** Bank Type */
						$bank[$count]['bank_type'] = $bsp['bank_type'];

						/** Contact Person */
						if (preg_match('/<d:President>(.*?)d:President>/', $res, $contact) == 1) {
							if(isset($contact[0])){
								$contact_person = $contact[0];
								$bank[$count]['contact_person'] = str_replace(array("<d:President>", "</d:President>"),"",$contact_person);	
							}
						}

						/** Designation of the Contact Person */
						if (preg_match('/<d:Authority>(.*?)d:Authority>/', $res, $authority) == 1) {
							if(isset($authority[0])){
								$contact_person_designation = $authority[0];
								$bank[$count]['contact_person_designation'] = str_replace(array("<d:Authority>", "</d:Authority>"),"",$contact_person_designation);
							}else{
								$bank[$count]['contact_person_designation'] = '';
							}
						}

						/** Bank Street Address */
						if (preg_match('/<d:Address>(.*?)d:Address>/', $res, $address) == 1) {
							if(isset($address[0])){
								$bank_street_address = $address[0];
								$bank[$count]['bank_street_address'] = str_replace(array("<d:Address>", "</d:Address>"),"",$bank_street_address);
							}else{
								$bank[$count]['bank_street_address'] = '';
							}
						}

						/** Number of Offices */
						if (preg_match('/<d:NumOffice>(.*?)d:NumOffice>/', $res, $numOffice) == 1) {
							if(isset($numOffice[0])){
								$bank_no_of_offices = $numOffice[0];
								$bank[$count]['bank_no_of_offices'] = str_replace(array("<d:NumOffice>", "</d:NumOffice>"),"",$bank_no_of_offices);
							}else{
								$bank[$count]['bank_no_of_offices'] = '';
							}
						}

						/** Bank Phone Number */
						if (preg_match('/<d:Contact>(.*?)d:Contact>/', $res, $phone_contact) == 1) {
							if(isset($phone_contact[0])){
								$bank_phone = $phone_contact[0];
								$bank[$count]['bank_phone'] = str_replace(array("<d:Contact>", "</d:Contact>"),"",$bank_phone);
							}else{
								$bank[$count]['bank_phone'] = '';
							}
						}

						/** Bak Email */
						if (preg_match('/<d:Email>(.*?)d:Email>/', $res, $email_bank) == 1) {
							if(isset($email_bank[0])){
								$bank_email = $email_bank[0];
								$bank[$count]['bank_email'] = str_replace(array("<d:Email>", "</d:Email>"),"",$bank_email);
							}else{
								$bank[$count]['bank_email'] = '';
							}
						}

						/** Bank Website */
						if (preg_match('/<d:Website>(.*?)d:Website>/', $res, $website) == 1) {
							if(isset($website[0])){
								$bank_website = $website[0];
								$bank[$count]['bank_website'] = str_replace(array("<d:Website>", "</d:Website>"),"",$bank_website);
							}else{
								$bank[$count]['bank_website'] = '';
							}
						}

						/** Bank FAX */
						if (preg_match('/<d:Fax>(.*?)d:Fax>/', $res, $fax) == 1) {
							if(isset($fax[0])){
								$bank_fax = $fax[0];
								$bank[$count]['bank_fax'] = str_replace(array("<d:Fax>", "</d:Fax>"),"",$bank_fax);
								$bank[$count]['bank_fax'] = '';
							}
						}
						$count += 1;
					}
				}
			}

			// Check organization on the database
			foreach($bank as $bank_key => $value){
				if(!isset($value['organization_name'])){
					continue;
				}

				$isBank = Bank::where('bank_name', $value['organization_name'])->orWhere('bank_name', 'like',  '%' . $value['organization_name'] . '%')->first();
				if($isBank){
					continue;
				}else{
					// Create new bank Ogranization
					if(isset($value['organization_name'])){
						$organization = new Bank;
						$organization->bank_name 					= $value['organization_name'];
						$organization->bank_type 					= $value['bank_type'];
						$organization->contact_person 				= !empty($value['contact_person']) ? $value['contact_person'] : '';
						$organization->contact_person_designation 	= !empty($value['designation']) ? $value['designation'] : '';
						$organization->bank_street_address1			= !empty($value['bank_street_address']) ? $value['bank_street_address'] : '';
						$organization->bank_no_of_offices 			= !empty($value['bank_no_of_offices']) ? $value['bank_no_of_offices'] : '';
						$organization->bank_phone 					= !empty($value['bank_phone']) ? $value['bank_phone'] : '';
						$organization->bank_email 					= !empty($value['bank_email']) ? $value['bank_email'] : '';
						$organization->bank_website 				= !empty($value['bank_website']) ? $value['bank_website'] : '';
						$organization->bank_fax 					= !empty($value['bank_fax']) ? $value['bank_fax'] : '';
						$organization->is_standalone 				= 0;
						$organization->is_custom 					= 0;
						$organization->custom_price 				= 0;
						$organization->deliverables 				= "";
						$organization->simplified_price 			= 1767.5;
						$organization->standalone_price 			= 3535;
						$organization->premium_zone1_price 			= 4343;
						$organization->premium_zone2_price 			= 5353;
						$organization->premium_zone3_price 			= 6363;
						$organization->white_label_name 			= "";
						$organization->brand_img 					= "";
						$organization->isLabel 						= 0;
						$organization->created_at 					= date('Y-m-d');
						$organization->updated_at 					= date('Y-m-d');
						$organization->save();
					}
				}
			}

			// Export to excel format the scraped data
			Excel::create('BSP Directory Scraper', function($excel) use ($bank) {
				$excel->sheet('BSP Directory Scraper', function($sheet) use($bank){
					$sheet->fromArray($bank);
				});
			})->store('xlsx', public_path('documents/bsp'));

			return STS_OK;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.53: http_curl_request
	//  Description: Function to get data from remote API.
    //-----------------------------------------------------
	function http_curl_request($url, $options = array())
	{
		try{
			if((isset($options['wait_duration'])) && ($options['wait_duration']) && ($options['wait_duration'] != ''))
			{
				sleep($options['wait_duration']);
			}

			$success = true;
			$result = '';
			$curl_info = array();

			##Add the headers
			if(isset($options['headers']))
			{
				$headers = $options['headers'];
			}

			##Add default user agent if it is not set.
			if(!isset($options['user_agent']))
			{
				if(isset($_SERVER['HTTP_USER_AGENT']))
				{
					$options['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
				}
				else
				{
					$options['user_agent'] = 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36';
				}
			}
			$headers[] = "User-Agent: {$options['user_agent']}";

			##Add referer
			if(isset($options['referer']))
			{
				$headers[] = "Referer: {$options['referer']}";
			}

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			##Add File to Download
			if(isset($options['file']))
			{
				curl_setopt($ch, CURLOPT_FILE, $options['file']);
			}

			##Add User Credentials
			if(isset($options['credentials']))
			{
				curl_setopt($ch, CURLOPT_USERPWD, $options['credentials']);
			}

			##Add Authentication Type
			if(isset($options['auth_type']))
			{
				curl_setopt($ch, CURLOPT_HTTPAUTH, $options['auth_type']);
			}

			##Add Follow Location
			if(isset($options['follow_location']))
			{
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			}

			##Add Connect Timeout
			if(isset($options['connect_timeout']))
			{
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $options['connect_timeout']);
			}

			##Add Execute Timeout
			if(isset($options['execute_timeout']))
			{
				curl_setopt($ch, CURLOPT_TIMEOUT, $options['execute_timeout']);
			}

			##Use Cookies
			if(isset($options['cookie_file']))
			{
				curl_setopt($ch, CURLOPT_COOKIEJAR, $options['cookie_file']);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $options['cookie_file']);
			}

			##Use Post
			if(isset($options['post_data']))
			{
				// curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $options['post_data']);
			}

			##Is Binary data
			if(isset($options['is_binary']))
			{
				curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
			}

			##Include Header
			if(isset($options['get_header']))
			{
				curl_setopt($ch, CURLOPT_HEADER, 1);
			}

			##Include Proxy URL
			if(isset($options['proxy_url']))
			{
				curl_setopt($ch, CURLOPT_PROXY, $options['proxy_url']);
			}

			##Include Proxy Credentials
			if(isset($options['proxy_credentials']))
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $options['proxy_credentials']);
			}

			##Include Encoding details
			if(isset($options['encoding']))
			{
				curl_setopt($ch, CURLOPT_ENCODING, $options['encoding']);
			}

			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

			$result = curl_exec($ch);

			if(curl_errno($ch))
			{
				$success = false;
				$result = curl_error($ch);
			}
			else
			{
				$curl_info = curl_getinfo($ch);
			}

			curl_close($ch);
			return array($success, $result, $curl_info);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.54: flush_out
	//  Description: Function to flush the content
    //-----------------------------------------------------
	function flush_out()
	{
		try{
			echo str_pad('',4096)."\n";
			ob_flush();
			flush();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.55: format_phone
	//  Description: Function to format phone number
    //-----------------------------------------------------
	function format_phone($data)
	{
		try{
			$data = preg_replace('|^.*:|s', '', $data);
			$data = self::clean_data($data);
			return($data);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.56: clean_data
	//  Description: Function to clean data
    //-----------------------------------------------------
	function clean_data($data)
	{
		try{
			$data = str_replace('<br>', ' | ', $data);
			$data = strip_tags($data, '<br>');
			$data = str_replace('&nbsp;', ' ', $data);
			$data = trim($data);
			$data = str_replace(chr(194),'',$data);
			$data = trim($data);
			return($data);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.57: clean_breaks
	//  Description: Function to clean new lines and breaks
    //-----------------------------------------------------
	function clean_breaks($yourString){
		try{
			return preg_replace( "/\r|\n/", " ", $yourString );
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.58: getPSAForecasting
	//  Description: get PSA forecasting data page
    //-----------------------------------------------------
	public function getPSAForecasting()
	{
		try{
			$data = DB::table('tblindustry_main')->get();
			return View::make('admin.psa_forecasting', array('data' => $data));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.59: postPSAForecasting
	//  Description: post process for PSA forecasting data page
    //-----------------------------------------------------
	public function postPSAForecasting()
	{
		try{
			$v = Validator::make(Input::all(), array( // validation rules
				'template.*' => 'required|mimes:xlsx'
			), array(
				'template.required' => 'Please select the Excel template.',
				'template.mimes' => 'The file you were trying to upload does not have the correct format.'
			));

			if($v->fails()){ // if fail redirect back with error
				return Redirect::route('getPSAForecasting')->withErrors($v->errors());
			}

			$file = Input::file('template');							// get input file
			$destinationPath = 'temp'; 									// set save directory
			$extension = $file->getClientOriginalExtension();			// get extension name
			$filename = rand(11111,99999).time().'.'.$extension;		// generate random filename
			$upload_success = $file->move($destinationPath, $filename); // save file to server

			// init excel reader
			$excel = Excel::load('temp/'.$filename);
			$sheet = $excel->getActiveSheet();

			$industries = Industrymain::all(); // get all industries

			foreach($industries as $k => $i){ // get values in excel file
				$i->q1 = ($sheet->getCell('B'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('B'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q2 = ($sheet->getCell('C'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('C'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q3 = ($sheet->getCell('D'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('D'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q4 = ($sheet->getCell('E'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('E'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q5 = ($sheet->getCell('F'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('F'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q6 = ($sheet->getCell('G'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('G'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q7 = ($sheet->getCell('H'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('H'.(string)($k+2))->getCalculatedValue() : 0;
				$i->q8 = ($sheet->getCell('I'.(string)($k+2))->getCalculatedValue()) ? $sheet->getCell('I'.(string)($k+2))->getCalculatedValue() : 0;

				//calc slope
				$result = self::linear_regression(array(1,2,3,4,5,6,7,8), array(
					$i->q1,
					$i->q2,
					$i->q3,
					$i->q4,
					$i->q5,
					$i->q6,
					$i->q7,
					$i->q8
				));
				if($i->q8 != 0){
					$i->slope = $result['m'] / $i->q8;
				} else {
					$i->slope = 0;
				}

				$i->save();

			}

			unlink('temp/'.$filename); //delete temp file
			return Redirect::to('/admin/upload_psa_forecasting');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	 //-----------------------------------------------------
    //  Function 2.60: linear_regression
	//  Description: linear regression function
    //-----------------------------------------------------
	function linear_regression($x, $y) {

		try{
		  // calculate number points
		  $n = @count($x);

		  // ensure both arrays of points are the same size
		  if ($n != @count($y)) {

			trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

		  }

		  // calculate sums
		  $x_sum = array_sum($x);
		  $y_sum = array_sum($y);

		  $xx_sum = 0;
		  $xy_sum = 0;

		  for($i = 0; $i < $n; $i++) {

			$xy_sum+=($x[$i]*$y[$i]);
			$xx_sum+=($x[$i]*$x[$i]);

		  }

		  // calculate slope
		  if((($n * $xx_sum) - ($x_sum * $x_sum))!=0){
			  $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
		  } else {
			  $m = 0;
		  }

		  // calculate intercept
		  $b = ($y_sum - ($m * $x_sum)) / $n;

		  // return result
		  return array("m"=>$m, "b"=>$b);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

	}

	//-----------------------------------------------------
    //  Function 2.61: setTrialSummaryEmail
	//  Description: function to update Trial Summary email
    //-----------------------------------------------------
    public function setTrialSummaryEmail()
    {
        try{
	        $emails = Input::get('emails');

	        $rules = array(		// validation rules
				'emails'	=> 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);  // run validation

			if ($validator->fails()) { // if fail get error messages
				$srv_resp['messages']	= $validator->messages()->all();
			}
			else
			{
	            $receiver = Keys::where('name', 'trial_summary_emails')->first();

				// update email if found
	            if ($receiver) {
	                Keys::where('name', 'trial_summary_emails')
	                    ->update(
	                        array(
	                            'value' => $emails
	                        )
	                    );
	            }
	            else { // create one if not
	                Keys::insert(
	                        array(
	                            'name'  => 'trial_summary_emails',
	                            'value' => $emails
	                        )
	                    );
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 2.62: filterTrialAccounts
	//  Description: Filters the trial Accounts List
    //-----------------------------------------------------
    public function filterTrialAccounts($field, $order)
    {
    	try{
	        $email      = Input::get('filter-email');
	        $company    = Input::get('filter-company-name');
	        $industry   = Input::get('filter-industry');
	        $reg_date   = Input::get('filter-registration-date');
	        $fa_cmp     = Input::get('filter-fa-cmp');
	        $first_act  = Input::get('filter-first-activity');
	        $la_cmp     = Input::get('filter-la-activity');
	        $last_act   = Input::get('filter-last-activity');
	        $ll_cmp     = Input::get('filter-ll-activity');
	        $last_log   = Input::get('filter-last-login');
	        $cc_cmp     = Input::get('filter-comp-cmp');
	        $completion = Input::get('filter-completion');
	        $bankName   = Input::get('filter-bank-name');

	        $accounts = DB::table('tbllogin')
				->select(['tblbank.bank_name', 'tbllogin.loginid', 'tbllogin.email', 'tblentity.companyname',
					'tblentity.entityid', 'tblentity.is_paid', 'tblindustry_main.main_title',
					'tbllogin.created_at', 'tbllogin.updated_at', 'tblentity.completion',
					'activity_logs.first_activity', 'activity_logs.last_activity', 'activity_logs.first_submission'])
				->leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
				->leftJoin('tblindustry_main', 'tblindustry_main.industry_main_id', '=', 'tblentity.industry_main_id')
				->leftJoin('activity_logs', 'tblentity.entityid', '=', 'activity_logs.entity_id')
	            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
				->where('tbllogin.trial_flag', STS_OK)
				->orderBy($field, $order);

	        if (STR_EMPTY != $email) {
	            $accounts->where('tbllogin.email', 'LIKE', '%'.$email.'%');
	        }

	        if (STR_EMPTY != $company) {
	            $accounts->where('tblentity.companyname', 'LIKE', '%'.$company.'%');
	        }

	        if (STR_EMPTY != $industry) {
	            $accounts->where('tblindustry_main.industry_main_id', $industry);
	        }

	        if (STR_EMPTY != $reg_date) {
	            $accounts->where('tblindustry_main.industry_main_id', $reg_date);
	        }

	        if (STR_EMPTY != $first_act) {
	            $accounts->where('activity_logs.first_activity', $fa_cmp, $first_act);
	        }

	        if (STR_EMPTY != $last_act) {
	            $accounts->where('activity_logs.last_activity', $la_cmp, $last_act);
	        }

	        if (STR_EMPTY != $last_log) {
	            $accounts->where('tbllogin.updated_at', $ll_cmp, $last_log);
	        }

	        if (STR_EMPTY != $completion) {
	            $accounts->where('tblentity.completion', $cc_cmp, $completion);
	        }

	        if (STR_EMPTY != $bankName) {
	            $accounts->where('tblbank.bank_name', 'LIKE', '%'.$bankName.'%');
	        }

	        $accounts = $accounts->get();

	        foreach ($accounts as $account) {
	            $account->created_at        = date('Y-m-d H:i:s', strtotime($account->created_at.' +8 hours'));

	            if (null == $account->main_title) {
	                $account->main_title    = '-';
	            }

	            if ($account->bank_name === null) {
	                $account->bank_name = 'Independent Report';
	            }

	            if (null != $account->first_activity) {
	                $account->first_activity    = date('Y-m-d H:i:s', strtotime($account->first_activity.' +8 hours'));
	            }
	            else {
	                $account->first_activity    = '-';
	            }

	            if (null != $account->last_activity) {
	                $account->last_activity     = date('Y-m-d H:i:s', strtotime($account->last_activity.' +8 hours'));
	            }
	            else {
	                $account->last_activity    = '-';
	            }

	            $account->updated_at        = date('Y-m-d H:i:s', strtotime($account->updated_at.' +8 hours'));
	        }

	        return json_encode($accounts);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 2.63: getPublicTrialList
	//  Description: Shows the Trial Accounts List
    //-----------------------------------------------------
	public function getPublicTrialList($hash)
	{
		try{
			$password = Keys::where('name','trial_key')->first()->value; 	// get hash saved on db
			if ($password != $hash) {
	            App::abort(403, 'Unauthorized action.');	 	// compare hash input and db hash, abort if not equal
	        }

			return $this->getTrialAccounts();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}	
	}

    //-----------------------------------------------------
    //  Function 2.64: searchCompanies
	//  Description: Search existing companies
    //-----------------------------------------------------
    public function searchCompanies()
    {
    	try{
	        $input      = Input::get('term');
	        $companies  = Bank::where('bank_name', 'LIKE', '%'.$input.'%')
					->select('bank_name')
					->orderBy('bank_name')
	            ->get();

	        $output     = array();

	        foreach ($companies as $company) {
	            $output[]   = $company->bank_name;
	        }

	        return json_encode($output);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }


    	//-----------------------------------------------------
    //  Function 2.65: getOCROptions
	//  Description: gets OCR Options page
    //-----------------------------------------------------
	public function getOCROptions()
	{
        try{
        	$enableOCRProcessing = GlobalSettings::getGlobalSetting("EnableOCRProcessing");

            return View::make('admin.ocr_options')->with('enableOCRProcessing', $enableOCRProcessing);
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

            	//-----------------------------------------------------
    //  Function 2.66: postOCROptions
	//  Description: Post process for OCR Options
    //-----------------------------------------------------
	public function postOCROptions()
	{
		try{
			ini_set('max_execution_time', 600);
			$image = Input::file('imagefile');
			$additionalOutput = "";
			if($image != null && $image->getRealPath()) {
				$additionalOutput = "<pre>";
				$text = OCRHelper::scanBankStatement($image->getRealPath());
				$additionalOutput .= "<h1>OCR Scanning Result Text</h1>";
				$additionalOutput .= print_r($text,true);
				$additionalOutput .= "<h1>Average Balance For The Period ".OCRHelper::getAverageDailyBalance($text)."</h1>";
				$additionalOutput .= "</pre>";
			}

			$enableOCRProcessing      = Input::get('enable_ocr_processing');
			if($enableOCRProcessing == 1){
				GlobalSettings::setGlobalSetting("EnableOCRProcessing",1);
			}
			else{
				GlobalSettings::setGlobalSetting("EnableOCRProcessing",0);
			}

			return Redirect::to('/admin/ocr_options/')->withErrors('Settings are saved!'.$additionalOutput)->withInput();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.67: getOrganizations
	//  Description: Displays the Organizations List
    //-----------------------------------------------------
    public function getOrganizations()
    {
    	try{
	    	$where = [
	    		'tblbank.id IS NOT NULL'
	    	];

			/*Check if search/filter was used*/
			if(Input::has('search')) {
				
				$where[] = 'tblbank.bank_name LIKE "%' . Input::get('search') . '%"';
				
			}

			$where = implode(' AND ', $where);

			$organizations = Bank::selectRaw('tblbank.*, count(tbllogin.loginid) as supervisor_count')
	                        ->leftJoin('tbllogin', 'tblbank.id','=','tbllogin.bank_id', 'tbllogin.role', '=', 4)
	                        ->whereRaw($where)
	                        ->groupBy('tblbank.id')
	                        ->orderBy('bank_name', 'asc')
	                        ->simplePaginate(50);

	        return View::make('admin.organization-list',
	            array(
	                'organizations' => $organizations
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 2.68: getCreateOrganizations
	//  Description: Displays the Organizations Create Form
    //-----------------------------------------------------
    public function getCreateOrganizations()
    {
    	try{
        	return View::make('admin.create-organization');
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 2.69: postOrganizations
	//  Description: Save Organization details
    //-----------------------------------------------------
    public function postOrganizations()
    {
    	try{
			/*validate inputs*/
			$rules = array(
	            'organization_name' => 'required|unique:tblbank,bank_name',
		        'organization_type' => 'required',
				// 'questionnaire_type' => 'required',
				'customPrice' => 'required_if:custom_price_switch,1|numeric',
				'simplified_price' => 'required|numeric|min:0',
				'standalone_price' => 'required|numeric|min:0',
				'premium_zone_one_price' => 'required|numeric|min:0',
				'premium_zone_two_price' => 'required|numeric|min:0',
				'premium_zone_three_price' => 'required|numeric|min:0',
				'white_label_org_name_field' => 'required_if:white_label_switch,on',
				'brand_image' => 'required_if:white_label_switch,on',
			);
			$messages = array(
				'organization_name.required' => 'Organization name is required.',
				'organization_name.unique' => 'Organization name already exist.',
				'organization_type.required' => 'Organization type is required.',
				// 'questionnaire_type.required' => 'Questionnaire type is required.',
				'customPrice.numeric' => 'Custom Price must be a number.',
				'simplified_price.required' => "Simplified Report Price field is required.",
				'simplified_price.numeric' => "Simplified Report Price field must be numeric.",
				'standalone_price.required' => "Standalone Report Price field is required.",
				'standalone_price.numeric' => "Standalone Report Price field must be numeric.",
				'premium_zone_three_price.required' => "Premium Zone Report Price field is required.",
				'premium_zone_three_price.numeric' => "Premium Zone Report Price field must be numeric.",
				'premium_zone_one_price.required' => "Premium Zone 1 Report Price field is required.",
				'premium_zone_one_price.numeric' => "Premium Zone 1 Report Price field must be numeric.",
				'premium_zone_two_price.required' => "Premium Zone 2 Report Price field is required.",
				'premium_zone_two_price.numeric' => "Premium Zone 2 Report Price field must be numeric.",
			);
			
			// Deliverables
			$count = 0;
			$deliverables = array(
				'financial_rating' 		=> 0,
				'growth_forecast'		=> 0,
				'industry_forecasting'	=> 0,
			);

			if( Input::has('financial_rating') ){
				$count += 1;
				$deliverables['financial_rating'] = 1;
			}
			if( Input::has('growth_forecast') ){
				$count += 1;
				$deliverables['growth_forecast'] = 1;
			}
			if( Input::has('industry_forecasting') ){
				$count += 1;
				$deliverables['industry_forecasting'] = 1;
			}

			if( $count == 0 ){
				$rules['deliverables'] = 'required';
				$messages['deliverables.required'] = 'Please select atleast one deliverables';
			}

			// Report types
			$rtCount = 0;
			$reportType = array(
				'simplified_type'	=> 0,
				'standalone_type' => 0,
				'premium_type'	=> 0
			);

			if( Input::has('simplified_type') ){
				$rtCount += 1;
				$reportType['simplified_type'] = 1;
			}

			if( Input::has('standalone_type') ){
				$rtCount += 1;
				$reportType['standalone_type'] = 1;
			}

			if( Input::has('premium_type') ){
				$rtCount += 1;
				$reportType['premium_type'] = 1;
			}

			if( $rtCount == 0 ){
				$rules['reportType'] = 'required';
				$messages['reportType.required'] = 'Please select atleast one report type';
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			/*Input Validation failed*/
			if ($validator->fails()) {
				return Redirect::to('/admin/create_organization')->withErrors($validator->errors())->withInput();
			}
	        else {

				$checked_deliverables = json_encode((object)$deliverables);
				$checked_reportType = json_encode((object)$reportType);
				$is_custom = Input::get('custom_price_switch');

				if((Input::get('custom_price_switch') == "on")) {
					$is_custom = true;
				} else {
					$is_custom = false;
				}
				
				$custom_price = Input::get('customPrice');

				if(!$is_custom){
					$is_custom 		= 0;
					$custom_price 	= 0;
				}

				$data = array(
					'bank_name' 			=> Input::get('organization_name'),
					'bank_type' 			=> Input::get('organization_type'),
					//'questionnaire_type' 	=> Input::get('questionnaire_type'),
					'is_standalone' 		=> 1,
					'is_custom' 			=> $is_custom,
					'custom_price' 			=> $custom_price,
					'deliverables'			=> $checked_deliverables,
					'report_type'			=> $checked_reportType,
					'simplified_price'		=> Input::get('simplified_price'),
					'standalone_price'		=> Input::get('standalone_price'),
					'premium_zone1_price'	=> Input::get('premium_zone_one_price'),
					'premium_zone2_price'	=> Input::get('premium_zone_two_price'),
					'premium_zone3_price'	=> Input::get('premium_zone_three_price'),
				);
				
				if((Input::get('white_label_switch') == "on")){
					$brand_image     =  Input::file('brand_image');
					$extension      = $brand_image->getClientOriginalExtension();
					if(!in_array($extension, array('png', 'jpeg', 'jfif', 'jpg'))){
						return Redirect::to('/admin/create_organization')->withErrors('Please upload an image file.');
					}

					$destination    = 'images/brand_images';
					$filename = 'while-label-logo-'.rand(11111,99999).'.'.$extension;

					/* Checks if a file with the same name already exists   */
					while (file_exists($destination.'/'.$filename)) {
						$filename   = 'while-label-logo-'.rand(11111,99999);
					}

					$brand_image->move($destination, $filename);

					$data['isLabel'] = 1;
					$data['brand_img'] = $filename;
					$data['white_label_name'] = Input::get('white_label_org_name_field');
				}else{
					$data['isLabel'] = 0;
				}

				/*Save bank details to database*/
				$organizationDb = new Bank;
				$result = $organizationDb->addOrganization($data);

				/*Organization successlfully saved*/
				if ($result == STS_OK){
					Session::flash('message', "Successfully added new organization.");
					return Redirect::to('/admin/organizations');
				} else{
					return Redirect::to('/admin/create_organization')->withErrors('Registration failed.');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.70: getUpdateOrganizations
	//  Description: Displays the Organizations Update Form
    //-----------------------------------------------------
    public function getUpdateOrganizations($organizationId)
    {
    	try{
			$organizationDb = new Bank;
			$organization = $organizationDb->getOrganizationById($organizationId);
			/*Display remminder if there's supervisor uses the bank*/
	        /*Check in supervisor_application*/
			$supervisorApplicationDb = new SupervisorApplication;
			$isUsedSupApplication = $supervisorApplicationDb->checkIfOrganizationUsed($organizationId);
			/*Check in user*/
			$userDb = new User;
			$isUsedUser = $userDb->checkIfOrganizationUsed($organizationId);
			$isUsed = false;
			if ($isUsedSupApplication || $isUsedUser){
				$isUsed = true;
			}
			

			$deliverables = array(
				'financial_rating' 		=> 0,
				'growth_forecast'		=> 0,
				'industry_forecasting'	=> 0
			);

			$is_deliverables = json_encode((object)$deliverables);

			if(!empty($organization->deliverables)){
				$deliverables = json_decode($organization->deliverables);
			}else{
				$deliverables = json_decode($is_deliverables);
			}

			$reportType = json_decode($organization->report_type);

			return View::make('admin.update-organization',
				array(
					'organization' 	=> $organization,
					'isUsed'		=> $isUsed,
					'deliverables' 	=> $deliverables,
					'reportType'	=> $reportType
				)
			);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 2.71: putOrganizations
	//  Description: Update Organization details
    //-----------------------------------------------------
    public function putOrganizations($organizationId)
    {
    	try{
			/*validate inputs*/
			$rules = array(
		        'organization_type' => 'required',
				//'questionnaire_type' => 'required',
				'customPrice' => 'required_if:custom_price_switch,1|numeric',
				'simplified_price' => 'required|numeric|min:0',
				'standalone_price' => 'required|numeric|min:0',
				'premium_zone_one_price' => 'required|numeric|min:0',
				'premium_zone_two_price' => 'required|numeric|min:0',
				'premium_zone_three_price' => 'required|numeric|min:0',
			);
			$messages = array(
				'organization_type.required' => 'Organization type is required.',
				//'questionnaire_type.required' => 'Questionnaire type is required.',
				'customPrice.numeric' => 'Custom Price must be a number.',
				'simplified_price.required' => "Simplified Report Price field is required.",
				'simplified_price.numeric' => "Simplified Report Price field must be numeric.",
				'standalone_price.required' => "Standalone Report Price field is required.",
				'standalone_price.numeric' => "Standalone Report Price field must be numeric.",
				'premium_zone_three_price.required' => "Premium Zone Report Price field is required.",
				'premium_zone_three_price.numeric' => "Premium Zone Report Price field must be numeric.",
				'premium_zone_one_price.required' => "Premium Zone 1 Report Price field is required.",
				'premium_zone_one_price.numeric' => "Premium Zone 1 Report Price field must be numeric.",
				'premium_zone_two_price.required' => "Premium Zone 2 Report Price field is required.",
				'premium_zone_two_price.numeric' => "Premium Zone 2 Report Price field must be numeric.",
			);

			$count = 0;
			$deliverables = array(
				'financial_rating' 		=> 0,
				'growth_forecast'		=> 0,
				'industry_forecasting'	=> 0,
			);

			if( Input::has('financial_rating') ){
				$count += 1;
				$deliverables['financial_rating'] = 1;
			}
			if( Input::has('growth_forecast') ){
				$count += 1;
				$deliverables['growth_forecast'] = 1;
			}
			if( Input::has('industry_forecasting') ){
				$count += 1;
				$deliverables['industry_forecasting'] = 1;
			}

			if( $count == 0 ){
				$rules['deliverables'] = 'required';
				$messages['deliverables.required'] = 'Please select atleast one deliverables';
			}

			// Report types
			$rtCount = 0;
			$reportType = array(
				'simplified_type'	=> 0,
				'standalone_type' => 0,
				'premium_type'	=> 0
			);

			if( Input::has('simplified_type') ){
				$rtCount += 1;
				$reportType['simplified_type'] = 1;
			}

			if( Input::has('standalone_type') ){
				$rtCount += 1;
				$reportType['standalone_type'] = 1;
			}

			if( Input::has('premium_type') ){
				$rtCount += 1;
				$reportType['premium_type'] = 1;
			}

			if( $rtCount == 0 ){
				$rules['reportType'] = 'required';
				$messages['reportType.required'] = 'Please select atleast one report type';
			}

			$oldBankIsLabel = Bank::where('id', $organizationId)->pluck('isLabel')->first();
			if((Input::get('white_label_switch') == "on") && $oldBankIsLabel == 1){
				$data['white_label_org_name_field'] = 'required_if:white_label_switch,on';
			}else{
				$data['white_label_org_name_field'] = 'required_if:white_label_switch,on';
				$data['brand_image'] = 'required_if:white_label_switch,on';
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

			/*Input Validation failed*/
			if ($validator->fails()) {
				return Redirect::route('getUpdateOrganizations', $organizationId)->withErrors($validator->errors())->withInput();
			}
	        else {
				$checked_deliverables = json_encode((object)$deliverables);
				$checked_reportType = json_encode((object)$reportType);
				$is_custom = Input::get('custom_price_switch');

				if((Input::get('custom_price_switch') == "on")) {
					$is_custom = true;
				} else {
					$is_custom = false;
				}

				$custom_price = Input::get('customPrice');

				if(!$is_custom){
					$is_custom = 0;
					$custom_price = 0;
				}

				$data = array(
					'bank_type' => Input::get('organization_type'),
					//'questionnaire_type' => Input::get('questionnaire_type'),
					'is_custom' 			=> $is_custom,
					'custom_price' 			=> $custom_price,
					'deliverables'			=> $checked_deliverables,
					'report_type'			=> $checked_reportType,
					'simplified_price'		=> Input::get('simplified_price'),
					'standalone_price'		=> Input::get('standalone_price'),
					'premium_zone1_price'	=> Input::get('premium_zone_one_price'),
					'premium_zone2_price'	=> Input::get('premium_zone_two_price'),
					'premium_zone3_price'	=> Input::get('premium_zone_three_price'),
				);

				if((Input::get('white_label_switch') == "on")){
					if($oldBankIsLabel == 1 && empty(Input::file('brand_image')) ){
						// use old saved logo
					}else{
						$brand_image     =  Input::file('brand_image');
						$extension      = $brand_image->getClientOriginalExtension();
						if(!in_array($extension, array('png', 'jpeg', 'jfif', 'jpg'))){
							return Redirect::to('/admin/create_organization')->withErrors('Please upload an image file.');
						}

						$destination    = 'images/brand_images';
						$filename = 'while-label-logo-'.rand(11111,99999).'.'.$extension;

						/* Checks if a file with the same name already exists   */
						while (file_exists($destination.'/'.$filename)) {
							$filename   = 'while-label-logo-'.rand(11111,99999);
						}

						$brand_image->move($destination, $filename);

						$data['isLabel'] = 1;
						$data['brand_img'] = $filename;
						$data['white_label_name'] = Input::get('white_label_org_name_field');
					}
					
				}else{
					$data['isLabel'] = 0;
				}

				/*Update organization details to database*/
				$organizationDb = new Bank;
				$result = $organizationDb->updateOrganization($organizationId, $data);

				/*Update all supervisor_application data that uses the organization name*/
				/*when all the system process that gets the bank information from supervisor_application
				were removed/changed, remove this. see CDP-948 for details*/
				/*Check in supervisor_application*/
				$supervisorApplicationDb = new SupervisorApplication;
				$isUsedSupApplication = $supervisorApplicationDb->checkIfOrganizationUsed($organizationId);
				if ($isUsedSupApplication){
					$role = 0;
					switch($data['bank_type']){
						case 'Bank';
							$role = 4;
							break;
						case 'Non-Bank Financing';
							$role = 5;
							break;
						case 'Corporate';
							$role = 6;
							break;
						case 'Government';
							$role = 7;
							break;
					}
					$data['bank_type'] = $role;
					$supervisorApplicationDb->updateOrganizationDetails($organizationId, $data);
				}

				/*Organization successlfully saved*/
				if ($result){
					Session::flash('message', "Successfully updated organization details.");
					return Redirect::to('/admin/organizations');
				} else if ($result == null){
					return Redirect::route('getUpdateOrganizations', $organizationId)->withErrors('Organization ID doesn\'t exist.');
				} else{
					return Redirect::route('getUpdateOrganizations', $organizationId)->withErrors('Registration failed.');
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function 2.72: updateSubscriptionStatus
	//  Description: Updates the Subscription Status
    //-----------------------------------------------------
	public function updateSubscriptionStatus(){
		try{
			$post = Input::get();
			$now = date("Y-m-d H:i:s"); //created_at
			$expiry = date("Y-m-d H:i:s", strtotime("+1 month", strtotime($now)));

			/** Check if has record on tblbanksubscription  */
			$bank = DB::table('tblbanksubscriptions')->where('bank_id', $post["id"] )->first();
			if(!$bank){
				DB::table('tblbanksubscriptions')->insert(
					array('bank_id' 	=> $post["id"], 
						'expiry_date' => $expiry, 
						'updated_at'  =>$now, 
						'updated_by' 	=> Auth::user()->loginid)
				);
			}

			/** Check if subscribing or unsubscribing */
			if($post["subscription"] == "true"){
				$update = array("status"=> 0, "expiry_date" => $expiry, "updated_by" => Auth::user()->loginid);
			}else{
				$expiry = date("Y-m-d H:i:s", strtotime("-1 day", strtotime($now)));
				$update = array("status"=> 1, "updated_at"=>$now, "updated_by" => Auth::user()->loginid, 'expiry_date' => $expiry);
			}
			
			// Update tblbanksubscriptions
			DB::table('tblbanksubscriptions')->where('bank_id', $post["id"])->update($update);

			return array("success" => true);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.73: getAdminThirdParty
	//  Description: Gets the admin third party page
    //-----------------------------------------------------
	public function getAdminThirdParty()
	{
		try{
	        $date_today     = date('Y-m-d H:i:s');				// Defaulted to date today with format: 2016-01-01 23:59:59

			$limit = 15;										// Limit for pagination default: 15
			$users = PremiumUser::where('is_deleted',0);
			
			if(Input::has('search')) 							// filter for search ID, Email, Firstname or Lastname
			{
				$users->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
			}

			/* Pagination Code */

			$paginator = new Paginator($users->get()->toArray(), $users->count(), $limit);  // $paginator = Paginator::make($users->get()->toArray(), $users->count(), $limit); Laravel paginator class
			if(Input::has('page'))
			{
				$users->offset((Input::get('page') - 1) * $limit);		// compute for which page to display
			}
			$users = $users->orderBy('created_at', 'asc')->take($limit)->get(); 	// order users by descending created date
			return View::make('admin.third_party', array(
				'users' => $users,
				'paginator' => $paginator
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.74: getAddFormPremium
	//  Description: Gets the Add Premium User form page
    //-----------------------------------------------------
	public function getAddFormPremium()
	{
		try{
			$banks = array();				// variable container for banks list
			foreach(Bank::all() as $b) 		// gets all the list of banks
			{
				$banks[$b->id] = $b->bank_name;
			}
			// display the view /views/admin/adduser.blade.php
			return View::make('admin.add_premium_account', array('banks' => $banks));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.75: postAddPremiumUser
	//  Description: Post process for adding premium user
    //-----------------------------------------------------
	public function postAddPremiumUser()
	{
		try{
			/* validation rules */
			$rules = array(
		        'email'	=> 'required|email',
		        'name'	=> 'required'
			);

			// run and check for validation
			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails()) 		// if validator fails, redirect to getAddUser form page and show error
			{
				return Redirect::to('/admin/thrid_party/add')->withErrors($validator->errors())->withInput();
			}
			else							// if success
			{

				// checks if email already existing
				$check_user = PremiumUser::where('email', Input::get('email'))
										->where('is_deleted',0)->get();
				if($check_user->count() > 0)
				{
					return Redirect::to('/admin/third_party/add')->withErrors('New email is already in use.')->withInput();
				}

				// create new instance of class User and populate with input
				$pm = new PremiumUser;
				$pm->name = Input::get('name');
				$pm->email = Input::get('email');

				
				$pm->save();

				// save and redirect to admin dashbaord
				return Redirect::to('/admin/third_party');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.76: getPremiumUserDelete
	//  Description: Soft delete premium user
    //-----------------------------------------------------
	public function getPremiumUserDelete($id)
	{
		try{
			PremiumUser::where('id', $id)->update(array("is_deleted"=>1));
			return Redirect::to("/admin/third_party");
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}


	//-----------------------------------------------------
    //  Function 2.77: generateRandomPassword
	//  Description: Generate random password for supervisor trial account
	//-----------------------------------------------------
	
	public function generateRandomPassword()
	{
		try{
			$password = "";
			$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!()-.?[]_'~;:!@#$%^&*+=";
			 
			for($i = 0; $i < 10; $i++){
				$random_int = mt_rand();
				$password .= $charset[$random_int % strlen($charset)];
			}

			return $password;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.78: getAnalystThirdParty
	//  Description: Get active account of allowed poeple to received list of submitted reports
	//-----------------------------------------------------
	public function getAnalystThirdParty(){
		try{
			$date_today = date('Y-m-d H:i:s');

			/**Get account */
			$users = AnalystReport::where('is_deleted', 0);

			if(Input::has('search')){
				$users->where(Input::get('type'), 'like'. '%' . Input::get('search') . '%');
			}

			$users = $users->orderBy('created_at', 'asc')->get();
			return View::make('admin.analyst_third_party', array(
				'users'		=>  $users
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.79: getAnalystAccountDelete
	//  Description: Delete selected account
	//-----------------------------------------------------
	public function getAnalystAccountDelete($id){
		try{
			AnalystReport::where('id', $id)->update(array("is_deleted"=>1));
			return Redirect::to('/admin/analyst_report');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.80: getAddAccountAnalyst
	//  Description: 
	//-----------------------------------------------------
	public function getAddAccountAnalyst(){
		try{
			return View::make('admin.add_analyst_account');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.81: getAddAccountAnalyst
	//  Description: Add account on analyst_account
	//-----------------------------------------------------
	public function postAddAccountAnalyst(){
		try{
			/** Validation rules */
			$rules = array(
				'email'	=> 'required|email',
				'name'	=> 'required'
			);

			/**Check validation */
			$validator = Validator::make(Input::all(), $rules);
		
			if($validator->fails()){
				return Redirect::to('/admin/analyst_account/add')->withErrors($validator->errors())->withInput();
			}else{
				$check_email = AnalystReport::where('email', Input::get('email'))->where('is_deleted', 0)->get();

				if($check_email->count() > 0){
					return Redirect::to('/admin/analyst_account/add')->withErrors('Emails is already in use.')->withInput();
				}

				$ap = new AnalystReport();
				$ap->name = Input::get('name');
				$ap->email = Input::get('email');
				$ap->save();

				return Redirect::to('/admin/analyst_report');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getCmapSearchCount(){
		try{
			// get date now
			$now = Carbon::now();

			// set year and month from search or current year and month
			$year = Input::has('year') ? Input::get('year') : $now->year;
			$month = Input::has('month') ? Input::get('month') : $now->month;

			// for drop down
			$years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
			$months = array(
				"1" => "January", "2" => "February", "3" => "March", "4" => "April",
				"5" => "May", "6" => "June", "7" => "July", "8" => "August",
				"9" => "September", "10" => "October", "11" => "November", "12" => "December",
			);

			// get data from database
			$queries = CmapSearchQuery::whereMonth('created_at', $month)->whereYear('created_at', $year)->get();

			// make view
			return View::make('admin.cmap_search_count', ['years' => $years,
				'months' => $months,
				'month' => $month,
				'queries' => $queries]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.83: getCustomNotification
	//  Description: Add custom notification for admin
	//-----------------------------------------------------
	public function getCustomNotification() {
		try{
			$data = CustomNotification::where("is_deleted", 0)->get();
			$emails = NotificationEmails::where("is_deleted",0)->get();
			
			return View::make("admin.custom_notification")->with(array("data" => $data, "emails" => $emails));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.84: updateCustomNotification
	//  Description: Update custom notification for admin
	//-----------------------------------------------------
	public function updateCustomNotification(){
		try{
			$post = Input::post();
			$update = array(
				'is_running' => $post['status']
			);

			$where = array("id" => $post['id']);
			CustomNotification::where($where)->update($update);
			return array("status" => True);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.85: getEditUser
	//  Description: Get edit user form
    //-----------------------------------------------------
	public function editCustomNotifications($custom_id)
	{
		try{
			$data = CustomNotification::where("is_deleted", 0)->where('id', $custom_id)->get();
			// display the view /views/admin/edituser.blade.php
			$notifications = array();
			foreach ($data as $key => $value) {
				$notifications = $value;
			}

			return View::make('admin.edit_custom_notifications', array(
				'notifications' => $notifications
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.86: saveCustomNotifications
	//  Description: Save Custom Notifiications
	//-----------------------------------------------------
	public function saveCustomNotifications(){
		try{
			$post = Input::post();
			
			if(array_key_exists("name", $post)) {
				CustomNotification::where('id', $post['id'])->update(array("receiver" => $post['receiver']));
			} else {
				if(array_key_exists("ids", $post)) {
					CustomNotification::whereIn("id",$post['ids'])->update(array("receiver"=>$post['receiverEmail']));
				} else {
					CustomNotification::where("is_deleted", 0)->update(array("receiver" => $post['receiverEmail']));
				}
			}
			return Redirect::to('/admin/notifications');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.87: addNotificationEmails
	//  Description: Add Email Receiver
	//-----------------------------------------------------
	public function addNotificationEmails(){
		try{
			$messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to add is already exist.'
	        );

			$rules = array(									// validation rules
		        'email'	=> 'required|email|unique:notification_emails,email,0,id,is_deleted,0'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::to('/admin/notifications')->withErrors($validator->errors())->withInput();
			}
			else
			{
				$emails = new NotificationEmails();
	      		$emails->email = Input::get('email');
	      		$emails->is_deleted = 0;
	      		$emails->save();
				return Redirect::to('/admin/notifications');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.88: deleteNotificationEmails
	//  Description: Delete Email Receiver
	//-----------------------------------------------------
	public function deleteNotficationEmails($id){
		try{
			NotificationEmails::where("id",$id)->update(array("is_deleted"=> 1));
			return Redirect::to('/admin/notifications');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 2.89: getInnodataRecipients
	//  Description: Get Innodata and CreditBPO Recipients List
	//-----------------------------------------------------
	public function getInnodataRecipients()
	{
		try{
			$limit = 15; // Limit for pagination default: 15

			$innodata = InnodataRecipient::where('is_deleted', 0)
			->where('type', InnodataRecipient::INNODATA);
			$creditbpo = InnodataRecipient::where('is_deleted', 0)
			->where('type', InnodataRecipient::CREDITBPO);
			/* Filter Data Based On Email Or Name */
			if (Input::has('search')) {
				$innodata->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
				
				$creditbpo->where(Input::get('type'), 'like', '%' . Input::get('search') . '%');
			} 
			$innodata = $innodata->orderBy('created_at', 'desc')->paginate($limit,['*'],'innodata_page');
			$creditbpo = $creditbpo->orderBy('created_at', 'desc')->paginate($limit,['*'], 'creditbpo_page');
			
			return View::make("admin.innodata_recipient", compact('innodata', 'creditbpo'));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 2.90: addInnodataRecipient
	//  Description: Form To Add Innodata/CreditBPO Recipient
	//-----------------------------------------------------
	public function addInnodataRecipient()
	{
		try{
			return View::make("admin.add_innodata_recipient");
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 2.91: saveInnodataRecipient
	//  Description: Save Innodata/CreditBPO recipient To Database
	//-----------------------------------------------------
	public function saveInnodataRecipient()
	{
		try{
			$post_data = Input::all();
			
			$messages = array(  							// validation message
				'type.required' => 'Please select recipient tpye',
				'email.unique'  => 'The email you are trying to add is already exist.'
			);

			$rules = array(									// validation rules
				'type' => 'required',
				'email'	=> 'required|email|unique:innodata_recipients,email,0,id,is_deleted,0,type,'. $post_data['type']
			);

			$validator = Validator::make($post_data, $rules, $messages);

			if ($validator->fails()) {
				return Redirect::route('addInnodataRecipient')->withErrors($validator->errors())->withInput();
			} else {
				$emails = new InnodataRecipient();
				$emails->name 		= $post_data['name'];
				$emails->email 		= $post_data['email'];
				$emails->type 		= $post_data['type'];
				$emails->is_deleted = 0;
				$emails->save();
				return Redirect::route('getInnodataRecipients');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
	//  Function 2.91: deleteInnodataRecipient
	//  Description: Delete Innodata/CreditBPO Recipient
	//-----------------------------------------------------
	public function deleteInnodataRecipient($id)
	{
		try{
			InnodataRecipient::where('id', $id)->update(array("is_deleted" => 1));
			return Redirect::back();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.92: getPremiumReports
	//  Description: get Premium Reports page
    //-----------------------------------------------------
	public function getPremiumReports()
	{
		try{
			$entity = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
	            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
	            ->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
	            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
	            ->where('tblentity.is_premium', '=', 1)
				// ->groupBy('tblentity.entityid')
				->orderBy('tblentity.entityid', 'DESC')
				->paginate(15);

			// Update score as readable
			if($entity!=null){
				foreach($entity as $key=>$value){
					$entity[$key]->fs_template =  DB::table('tbldocument')
										            ->where('entity_id', '=', $value->entityid)
										            ->where('document_group', '=', 21)
										            ->where('upload_type', '=', 5) /* Financial Statment Upload Type */
										            ->where('is_deleted', 0)
										            ->orderBy('document_type', 'ASC')
										            ->get();
					$entity[$key]->letter_authorization_template = DB::table('tbldocument')
										            ->where('entity_id', '=', $value->entityid)
										            ->where('document_group', '=', 11)
										            ->where('upload_type', '=', 6) /*Letter of Authorization Type*/
										            ->where('is_deleted', 0)
										            ->orderBy('document_type', 'ASC')
										            ->get();
				}
			}

			$third_party_emails = PremiumUser::where('is_deleted', 0)->get();

			$admin_setting = AdminSetting::find(1);
			foreach($entity as $ent){
				//email
				$ent->premium_report_sent_to = json_decode($ent->premium_report_sent_to);
				//status
				if($ent->status == 7){
					$ent->status_description = '<span style="color:GREEN;">Completed</span>';
				}else{
					$userLogin = User::where('loginid', $ent->loginid)->first();
					if($userLogin->status == 1 || $userLogin->status == 0){
						$ent->status_description = '<span style="color:GREY;">Not Started</span>';
					}else{
						$ent->status_description = '<span style="color:RED;">In Progress</span>';
					}
				}
			}

			// show page
			return View::make('admin.premium_report_process')->with('entity', $entity)->with('third_party_emails', $third_party_emails)->with('admin_setting', $admin_setting);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.93: addThirdPartyEmails
	//  Description: Add Third Party Email
	//-----------------------------------------------------
	public function addThirdPartyEmails(){

		try{
			$messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to add is already exist.'
	        );

			$rules = array(									// validation rules
		        // 'name'	=> 'required',
		        'email'	=> 'required|email|unique:third_party_account,email,0,id,is_deleted,0'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if($validator->fails())
			{
				return Redirect::back()->withErrors($validator->errors())->withInput();
			}
			else
			{
				$emails = new PremiumUser();
	      		$emails->name = 'Agency  Email Address';//Input::get('name')
	      		$emails->email = Input::get('email');
	      		$emails->is_deleted = 0;
	      		$emails->save();
				return Redirect::back();
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.94: deleteThirdPartyEmails
	//  Description: Delete Email Third Party Email
	//-----------------------------------------------------
	public function deleteThirdPartyEmails($id){
		try{
			PremiumUser::where("id",$id)->update(array("is_deleted"=> 1));
			return Redirect::back();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function 2.94: changeInnodataSetting
	//  Description: Enable/Disable the the Innodata Process for Premium Reports.
	//-----------------------------------------------------
	public function changeInnodataSetting(){
		try{
			AdminSetting::updateOrCreate(['id' => 1], [ 'id' => 1, "is_innodata_process_premium_reports"=> Input::get('isProcessPremiumReport')]);
			return 'Success';
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getIndustryUpload(){
		try{
        	return View::make('industry.industry_forecast');
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function postIndustryForecast(){
    	try{
	        $file = Input::file('industry_file');
	        $extension = $file->getClientOriginalExtension();
	        
	        if($extension != 'xlsx'){
				return Redirect::route('getIndustryUpload')->withErrors("Please upload an excel file.")->withInput();
			}else{
				$path = $file->getRealPath();

				/** Get Class Section sheet of uploaded file */
				$class = Excel::selectSheets('Class Section')->load($path)->get();
				if($class){
					foreach($class as $c){
						$c_check = DB::table('tblindustryclass_forecast')
										->where('year', $c->year)
										->where('class_code', $c->industry_class)
										->first();
						/** Check if db has no record of same class and year. */
						if(!$c_check){
							if((!$c->industry_class) && (!$c->year) && 
								(!$c->gross_revenue) && (!$c->net_income) && 
								(!$c->gross_profit_margin) && (!$c->current_ratio) && 
								(!$c->debt_to_equity_ratio)){
								break;
							}else{
								try { 
									/** Insert record to tblindustryclass_forecast */
									$class_growth = DB::table('tblindustryclass_forecast')
										->insert([
										'class_code' 			=> $c->industry_class, 
										'year' 					=> $c->year, 
										'gross_revenue_growth' 	=> $c->gross_revenue,
										'net_income' 			=> $c->net_income, 
										'gross_profit_margin'	=> $c->gross_profit_margin,
										'current_ratio'			=> $c->current_ratio,
										'debt_equity_ratio'		=> $c->debt_to_equity_ratio
									]);
								}catch(\Illuminate\Database\QueryException $ex){ 
									return Redirect::to('/admin/industry/comparison')->with('error', 'Data not updated. Please check format of your file');
								}
							}
						}
					}
				}

				/** Get Group Section sheet of uploaded file */
				$group = Excel::selectSheets('Group Section')->load($path)->get();

				if($group){
					foreach($group as $c){
						$g_check = DB::table('tblindustrygroup_forecast')
										->where('year', $c->year)
										->where('group_code', $c->industry_group)
										->first();
						/** Check if db has no record of same group and year. */
						if(!$g_check){
							if((!$c->industry_group) && (!$c->year) && 
								(!$c->gross_revenue) && (!$c->net_income) && 
								(!$c->gross_profit_margin) && (!$c->current_ratio) && 
								(!$c->debt_to_equity_ratio)){
								break;
							}else{
								try { 
									/** Insert record to tblindustrygroup_forecast */
									$group_growth = DB::table('tblindustrygroup_forecast')
										->insert([
										'group_code' 			=> $c->industry_group, 
										'year' 					=> $c->year, 
										'gross_revenue_growth' 	=> $c->gross_revenue,
										'net_income' 			=> $c->net_income, 
										'gross_profit_margin'	=> $c->gross_profit_margin,
										'current_ratio'			=> $c->current_ratio,
										'debt_equity_ratio'		=> $c->debt_to_equity_ratio
									]);
								}catch(\Illuminate\Database\QueryException $ex){ 
									return Redirect::to('/admin/industry/comparison')->with('error', 'Data not updated. Please check format of your file');
								}
							}
						}
					}
				}

				/** Get Division Section of uploaded file */
				$division = Excel::selectSheets('Division Section')->load($path)->get();
				if($division){
					foreach($division as $d){
						$d_check = DB::table('tblindustrysub_forecast')
										->where('year', $d->year)
										->where('sub_code', $d->industry_division)
										->first();
						/** Check if db has no record of same division code and year. */
						if(!$d_check){
							if((!$d->industry_class) && (!$d->year) && 
								(!$d->gross_revenue) && (!$d->net_income) && 
								(!$d->gross_profit_margin) && (!$d->current_ratio) && 
								(!$d->debt_to_equity_ratio)){
								break;
							}else{
								try { 
									/** Insert record to tblindustrysub_forecast */
									$class_growth = DB::table('tblindustrysub_forecast')->insert([
										'sub_code' 				=> $d->industry_division, 
										'year' 					=> $d->year, 
										'gross_revenue_growth' 	=> $d->gross_revenue,
										'net_income' 			=> $d->net_income, 
										'gross_profit_margin'	=> $d->gross_profit_margin,
										'current_ratio'			=> $d->current_ratio,
										'debt_equity_ratio'		=> $d->debt_to_equity_ratio
									]);
								}catch(\Illuminate\Database\QueryException $ex){ 
										return Redirect::to('/admin/industry/comparison')->with('error', 'Data not updated. Please check format of your file');
								}
							}
						}
					}
				}
				
				/** Get Main Section of uploaded file */
				$main = Excel::selectSheets('Main Section')->load($path)->get();
				if($main){
					foreach($main as $m){
						$m_check = DB::table('tblindustrymain_forecast')
										->where('year', $m->year)
										->where('main_code', $m->industry_main)
										->first();
						/** Check if db has no record of same division code and year. */
						if(!$m_check){
							if((!$m->industry_class) && (!$m->year) && 
								(!$m->gross_revenue) && (!$m->net_income) && 
								(!$m->gross_profit_margin) && (!$m->current_ratio) && 
								(!$m->debt_to_equity_ratio)){
								break;
							}else{
								try { 
									/** Insert record to tblindustrymain_forecast */
									$class_growth = DB::table('tblindustrymain_forecast')->insert([
										'main_code' 			=> $m->industry_main, 
										'year' 					=> $m->year, 
										'gross_revenue_growth' 	=> $m->gross_revenue,
										'net_income' 			=> $m->net_income, 
										'gross_profit_margin'	=> $m->gross_profit_margin,
										'current_ratio'			=> $m->current_ratio,
										'debt_equity_ratio'		=> $m->debt_to_equity_ratio
									]);
								}catch(\Illuminate\Database\QueryException $ex){ 
										return Redirect::to('/admin/industry/comparison')->with('error', 'Data not updated. Please check format of your file');
								}	
							}
						}
					}
				}
			}
			return Redirect::to('/admin/industry/comparison')->with('message', 'Updated Succesfully');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
	
	//-----------------------------------------------------
    //  Function x.xx: downloadPSETemplate
	//  Description: Download Forecasting Data Template
	//-----------------------------------------------------
	public function downloadPSETemplate() {
		try{
			$download = public_path('cbpo_template_files/Forecasting_Data_Template.xlsx');	// get path
			$filename = "Template.xlsx";

			if(file_exists($download)){
				return Response::download($download, $filename);	// stream for download
			} else {
				App::abort(403, 'File not found.');
			}
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function x.xx: downloadPSETemplate
	//  Description: Download Forecasting Data Template
	//-----------------------------------------------------
	public function postTranscribeLogin(){

		$userTranscribeKey = Input::get('transcribe_key');
		$transcribe_key = Keys::where('name','transcribe_key')->first()->value;

		if($userTranscribeKey != $transcribe_key){
			session(['error' => 'Invalid transcribe key']);
			return redirect('transcribe_files');

		}else{
			session(['transcribe_key' => $userTranscribeKey]);

			return redirect('transcribe_files');
		}
	}

public function transcriptionNotificationstatus(Request $request){

	$receiver = DB::table('tblexternal_transcriber_account')->where('email', $request->email)->update(['status' => $request->status]);
} 
	//-----------------------------------------------------
    //  Function x.xx: downloadPSETemplate
	//  Description: Download Forecasting Data Template
	//-----------------------------------------------------
	public function getCreditBpoFTP(){
		try{
			$userTranscribeKey = session('transcribe_key');
			$transcribe_key = Keys::where('name','transcribe_key')->first()->value;

			$reports = DB::table('automate_standalone_rating')
						->join('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')
						->join('tblbank', 'tblbank.id', '=', 'tblentity.current_bank')
						->where('automate_standalone_rating.status', 5)
						->orderBy('entityid', 'DESC')
						->get();
			$receiver = DB::table('tblexternal_transcriber_account')->where('is_deleted', 0)->get();
			
			foreach($reports as $r){ //get status of the report
				$files = TranscribeFS::where('entityid', $r->entity_id)->where('is_deleted', 0)->get();
				if(count($files) > 0){
					//Check if folder has error file
					$error_file_name = 'Error ' . preg_replace("/[^a-zA-Z]+/", " ", $r->companyname).'.txt';
					if(file_exists(public_path('documents/innodata/from_innodata/'.$r->entityid. '/'. $error_file_name))){
						$r->report_status = "Upload Error";
						$r->error_file_name = $error_file_name;
						$r->path = public_path('documents/innodata/from_innodata/'.$r->entityid. '/');
					}else{
						$r->report_status = "Transcription Uploaded Successfully";
					}
				}else{
					$r->report_status = "Awaiting Transcription";
				}
			}

			// Select all currency available
			$currency = Currency::orderBy('iso_code', 'asc')->get();
			$currencyList = [];
			foreach ($currency as $item) {
				$currencyList[$item->iso_code] = $item->iso_code.' - '.$item->name;
			}
		
			return View::make('admin.upload_transcribe_fs', 
								array(
									'reports' => $reports,
									'transcribe_key' => $transcribe_key, 
									'userTranscribeKey' => $userTranscribeKey, 
									'receiver' => $receiver,
									'currency' => $currencyList
								));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getUploadFTP($id){
		try{
			$entity = Entity::where('entityid', $id)->first();
			if($entity->status != 7){
				return View::make('admin.upload_fs', array('entity' => $entity));
			}
			return Redirect::route('getCreditBpoFTP');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function postCreditBpoFTP($id){
		try{
			$post = Input::all();

			if(!isset($post['file'])){
				return STS_NG;
			}

			$file = $post['file'];
	        $extension = $file->getClientOriginalExtension();
			$filepath = public_path('documents/innodata/from_innodata/'.$id);
			$entity = Entity::where('entityid', $id)->first();
		
			$unit = $post['unit']; // get unit
	        $currency = $post['currency']; //get currency
			$document_type = $post['document_type']; //get document type

			$fs_setting = array(
				'unit'	=> $unit,
				'currency'	=> $currency,
				'document_type'	=> $document_type
			);

	        if($extension == "xlsx" || $extension == "xls" ){
				// Move file to creditbpo folder
				if(!file_exists($filepath)){
					mkdir($filepath,0777);
				}

				$filename = $entity->entityid . ' - ' . $entity->companyname . '.xlsx';

				// check if it has the same file name on the record
				$check_match = TranscribeFS::where('file_name', $filename)->where('entityid', $id)->where('is_deleted', 0)->first();

				if(empty($check_match)){
					$transcribe_fs = new TranscribeFS;
					$transcribe_fs->entityid = $id;
					$transcribe_fs->file_name = $filename;
					$transcribe_fs->is_deleted = 0;
					$transcribe_fs->fs_setting = json_encode($fs_setting);
					$transcribe_fs->save();
				}else{
					//update file
					TranscribeFS::where('id', $check_match->id) ->update(['file_name' => $filename,'is_deleted' => 0,'fs_setting' => json_encode($fs_setting)]);
				}

				\File::copy($file, public_path('documents/innodata/from_innodata/' . $id . '/' . $filename));

				return STS_OK;
			}else{
				return STS_NG;
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function x.xx: getBlockAddress
	//  Description: Get Admin Security Page
	//-----------------------------------------------------
	public function getBlockAddress(){
		$address = BlockAddress::where('is_active', 0)->paginate(10);
		return View::make('admin.security')->with('address', $address);
	}

	//-----------------------------------------------------
    //  Function x.xx: postBlockAddress
	//  Description: Save Block Address
	//-----------------------------------------------------
	public function postBlockAddress(){
		$post = Input::get();

		$messages = array(
			'email.required' => 'The Email field is required.',
			'email.unique' => 'The Email you are trying to register is already blocked.'
		);

		$rules = array(
			'email'	=> 'email|required|unique:block_email_address,email_address',
		);

		$validator = Validator::make($post, $rules, $messages);

		if ($validator->fails()) {
			$error = $validator->messages()->all();
			return $error;
		}else{
			// Add new blocked email address
			$block_user = new BlockAddress;
			$block_user->email_address = $post['email'];
			$block_user->is_active = 0;
			$block_user->save();

			return STS_OK;
		}
	}

	//-----------------------------------------------------
    //  Function x.xx: postBlockAddress
	//  Description: Update Block Address
	//-----------------------------------------------------
	public function updateBlockAddress(){
		$post = Input::get();
		BlockAddress::where('id', $post['id'])->update(['email_address' => $post['email']]);
		return STS_OK;
	}

	//-----------------------------------------------------
    //  Function x.xx: postBlockAddress
	//  Description: Delete Block Address
	//-----------------------------------------------------
	public function deleteBlockAddress(){
		$post = Input::get();
		BlockAddress::where('id', $post['id'])->delete();
		return STS_OK;
	}

	//-----------------------------------------------------
    //  Function x.xx: getFsRawFile
	//
	//-----------------------------------------------------
	public function getFsRawFile(){
		$post = Input::get();

		/*------------------------------------------
		--------Financial Statement PDF files-------
		-------------------------------------------*/
		$fsPdfFiles = DB::table('innodata_files')->where('is_deleted', 0)->where('entity_id', $post['id'])->get();
		foreach($fsPdfFiles as $fspdf){
			$fspdf->file_name = str_replace(($post['id'] . '-'),"",$fspdf->file_name);
		}

		return $fsPdfFiles;
	}

	//-----------------------------------------------------
    //  Function x.xx: getTranscribeFile
	//
	//-----------------------------------------------------
	public function getTranscribeFile(){
		$id = Input::get('id');
		$files = TranscribeFS::where('entityid', $id)->where('is_deleted', 0)->get();

		// Check if the uploaded file has an error
		foreach($files as $file){
			$report = Entity::where('entityid', $id)->first();
			if(count($files) > 0){
				//Check if folder has error file
				$error_file_name = 'Error ' . preg_replace("/[^a-zA-Z]+/", " ", $report->companyname).'.txt';
				$file_path = public_path('documents/innodata/from_innodata/'.$report->entityid. '/'. $error_file_name);

				if(file_exists($file_path)){
					$file->error = 1;
				}else{
					$file->error = 0;
				}
			}
		}
		return $files;
	}

	//-----------------------------------------------------
    //  Function x.xx: downloadTranscribeFS
	//
	//-----------------------------------------------------
	public function downloadTranscribeFS($id){
		$transcibeFile = TranscribeFS::where('id', $id)->where('is_deleted', 0)->first();
		$file = public_path(). "/documents/innodata/from_innodata/".$transcibeFile->entityid . '/' . $transcibeFile->file_name;

		$headers = array(
			'Content-Type: application/vnd.ms-excel',
		);

		if(file_exists($file)){
			return Response::download($file, $transcibeFile->file_name, $headers);
		}
	}

	//-----------------------------------------------------
    //  Function x.xx: getSupervisorClientKeys
	//
	//-----------------------------------------------------
	public function getSupervisorClientKeys(){
		$overall_used = 0;
		$overall_unused = 0;
		$overall_keys = 0;

		// Get All organization
		$organization = Bank::all();

		foreach($organization as $org){

			// Get client keys or the organization
			$client_keys = DB::table('tblbank')
							->leftJoin('bank_keys', 'bank_keys.bank_id', '=', 'tblbank.id')
							->where('tblbank.id', $org->id)
							->get();

			$supervisor = DB::table('tbllogin')
						->select(DB::raw("
							tbllogin.loginid,
							supervisor_application.application_id,
							CONCAT(tbllogin.firstname, ' ', tbllogin.lastname) as name_opt,
							tbllogin.name,
							tbllogin.email"
						))
						->where('tbllogin.role', 4)
						->where('tbllogin.parent_user_id', 0)
						->where('tblbank.id', $org->id)
						->leftJoin('tblbank', 'tbllogin.bank_id', '=', 'tblbank.id')
						->leftJoin('supervisor_application', 'tbllogin.email', '=', 'supervisor_application.email')
						->leftJoin('tblbanksubscriptions', 'tblbanksubscriptions.bank_id', '=', 'tbllogin.loginid')
						->get();

			$org->supervisor = $supervisor;

			$used = 0;
			$unused = 0;
			$total_use = 0;
			// Iterate keys and count registered and unregistered client keys
			foreach($client_keys as $keys){
				if($keys->is_used === 1){
					$used += 1;
					$overall_used += 1;
				}else{
					$unused += 1;
					$overall_unused += 1;
				}

				$total_use += 1;
				$overall_keys += 1;
			}

			// Set overall count per organization
			$org->used = $used;
			$org->unused = $unused;
			$org->total_use = $total_use;
		}

		$data = array(
			'organization' => $organization,
			'overall_used'	=> $overall_used,
			'overall_unused' => $overall_unused,
			'overall_keys'	=> $overall_keys
		);

		return View::make('admin.supervisor-client-keys', $data);
	}

	//-----------------------------------------------------
    //  Function X.XX: addExternalTranscriberEmail
	//  
	//-----------------------------------------------------
	public function addExternalTranscriberEmail(){
		try{
			$messages = array(  							// validation message
	            'email.unique'  => 'The email you are trying to add is already exist.'
	        );

			$rules = array(									// validation rules
		        'email'	=> 'required|email|unique:tblexternal_transcriber_account,email,0,id,is_deleted,0'
			);

			$validator = Validator::make(Input::all(), $rules, $messages);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator->errors())->withInput();
			} else {
				$emails = new ExternalTranscriber();
				$emails->name = "External Transcriber";
	      		$emails->email = Input::get('email');
	      		$emails->is_deleted = 0;
	      		$emails->save();
				return Redirect::back();
			}
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function X.XX: deleteTranscribeReceiverEmail
	// 
	//-----------------------------------------------------
	public function deleteTranscribeReceiverEmail($id){
		try{
			ExternalTranscriber::where("id",$id)->update(array("is_deleted"=> 1));
			return Redirect::back();
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function X.XX: getErrorFile
	// 
	//-----------------------------------------------------
	public function getErrorFile($id){
		try{
			$report = Entity::where('entityid', $id)->first();
			$files = TranscribeFS::where('entityid', $id)->where('is_deleted', 0)->get();
			if(count($files) > 0){
				//Check if folder has error file
				$error_file_name = 'Error ' . preg_replace("/[^a-zA-Z]+/", " ", $report->companyname).'.txt';
				$file_path = public_path('documents/innodata/from_innodata/'.$report->entityid. '/'. $error_file_name);

				$errorText = "";
				if(file_exists($file_path)){
					$content = fopen($file_path, 'r');
					while(!feof($content)){

						$line = fgets($content);
			
						$errorText =  $errorText . $line;
					}
					fclose($content);
					return $errorText;
				}else{
					return '';
				}
			}
		}catch(Exception $ex){
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	//-----------------------------------------------------
    //  Function X.XX: deleteTranscribeFs
	// 
	//-----------------------------------------------------
	public function deleteTranscribeFs($id){
		try{
			TranscribeFS::where('id', $id)->update(['is_deleted' => 1]);
			return;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

	}

	//-----------------------------------------------------
    //  Function X.XX: downloadBspScrapedData
	// 
	//-----------------------------------------------------
	public function downloadBspScrapedData(){
		$download = public_path('documents/bsp/' . 'BSP Directory Scraper.xlsx');
		$filename = "BSP Directory Scraper.xlsx";

		if(file_exists($download)){
			return Response::download($download, $filename);	// stream for download
		} else {
			App::abort(403, 'File not found.');
		}
	}

	public function getReports(){
		try{
			/* Variable declaration */
	        $date_today     = date('Y-m-d H:i:s');
			$limit = 50;
			$reports = Entity::select('tblentity.entityid', 'tblentity.companyname', 'tbllogin.email', 'tblbank.bank_name', 
											'tblentity.created_at', 'tblentity.completed_date', 'tblentity.is_premium', 'tblentity.discount_code', 
											'bank_keys.serial_key', 'tblentity.status', 'tbllogin.loginid', 'tblentity.current_bank', 'tblentity.province', 'tblentity.is_paid')
									->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
									->leftJoin('tblbank', 'tblbank.id', '=', 'tblentity.current_bank')
									->leftJoin('bank_keys', 'bank_keys.entity_id', '=', 'tblentity.entityid')
									->where('tbllogin.email', '<>', '');


			/** Apply filter */
			if(Input::has('search'))
			{
				/** Filters for Report id */
				if(Input::get('type') == "reportid"){
					$reports->where('tblentity.entityid', 'like', '%' . Input::get('search') . '%');
				}

				/** Filters for Company Name */
				if(Input::get('type') == "companyname"){
					$reports->where('tblentity.companyname', 'like', '%' . Input::get('search') . '%');
				}

				/** Basic User Email */
				if(Input::get('type') == "basic_email"){
					$reports->where('tbllogin.email', 'like', '%' . Input::get('search') . '%');
				}

				/** Organization */
				if(Input::get('type') == "organization"){
					$reports->where('tblbank.bank_name', 'like', '%' . Input::get('search') . '%');
				}

				/** Discount Code */
				if(Input::get('type') == "discount_code"){
					$reports->where('tblentity.discount_code', 'like', '%' . Input::get('search') . '%');
				}

				/** Date Started */
				if(Input::get('type') == "date_started"){
					$reports->whereDate('tblentity.created_at', '=', Input::get('search'));
				}

				/** Date Finished */
				if(Input::get('type') == "date_finished"){
					$reports->whereDate('tblentity.completed_date', '=', Input::get('search'));
				}
			}

			/* Pagination Code */
			$paginator = new Paginator($reports->get()->toArray(), $limit); 
			{
				$reports->offset((Input::get('page') - 1) * $limit);	
			}
			$reports = $reports->orderBy('tblentity.entityid', 'desc')->take($limit)->get(); 
			$paginator->setPath('reports');

			foreach($reports as $report){
				/** Get Rating of the submitted report */
				$report->rating = '';
				if($report->status == 7){
					$rating = $this->getFinancialRating($report);
					$report->rating = '<span style="color:' . $rating['color']. ';">'.  $rating['rating_display'] . '</span>';
				}

				/** Check payment  */
				$report->payment_method = '';
				$report->price = 0;
				$is_payment = false;

				$isDragonPay = DB::table('tbltransactions')->where('entity_id', $report->entityid)->get();
				if($isDragonPay){
					foreach($isDragonPay as $dp){
						if($dp->status == 'S'){
							$report->payment_method = 'Dragonpay';
							$receipts = DB::table('payment_receipts')->where('reference_id', $dp->id)->where('payment_method', 2)->first();
							if($receipts){
								$report->price = $receipts->total_amount;
								$is_payment = true;
							}
						}
					}
				}

				$isPaypal = DB::table('paypal_transactions')->where('entity_id', $report->entityid)->get();
				if($isPaypal){
					foreach($isPaypal as $pp){
						$paypalRecepts = DB::table('payment_receipts')->where('reference_id', $pp->id)->where('payment_method', 1)->first();
						if($paypalRecepts){
							$report->payment_method = 'Paypal';
							$report->price = $receipts->total_amount;
							$is_payment = true;
						}
					}
				}

				if($is_payment == false){
					/** Get report price base on bank */
					$bank = DB::table('tblbank')->where('id', $report->current_bank)->first();
					if($bank){
						if($report->is_premium == 2){
							$report->price = $bank->simplified_price;
						}elseif($report->is_premium == 1){
							$provCode = Province2::where('provDesc', $report->province)->pluck('regCode')->first(); //get province code for filters
							if ($provCode == 13) {                                      // Metro Manila
								$report->price = $bank->premium_zone1_price;
							}elseif(in_array($provCode, array(04,03))){                 // Laguna, Rizal, Cavite and Bulacan
								$report->price = $bank->premium_zone2_price;
							}else{                                                      // Rest of the Regions
								$report->price = $bank->premium_zone3_price;
							}
						}else{
							$report->price = $bank->standalone_price;
						}
					}
					if($report->serial_key){
						$report->discount_code = '';
					}
				}else{
					$report->discount_code = '';
					$report->serial_key = '';
				}

				// If the report is a trial account
				if($is_payment == false && $report->discount_code == '' && $report->serial_key == ''){
					// $report->payment_method = 'Trial Account - Free';
				}

				// Check if report is paid
				if($report->is_paid == 0){
					$report->is_paid = "Not Paid";
				}else{
					$report->is_paid = "Paid";
				}

			}

			$reportsReceiver = DB::table('tbl_reports_receiver')->get();

			return View::make('admin.reports', array(
				'reports' => $reports,
				'paginator' => $paginator,
				'reports_receiver' => $reportsReceiver
			));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

	public function getFinancialRating($report){
		$financialChecker = false;
		$financialTotal = 0;
		$pdefault = 0;

		$financialReport = FinancialReport::where(['entity_id' => $report->entityid, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
		
		if($financialReport){
			$frHelper = new FinancialHandler();
			$finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
			if($finalRatingScore) {
				$financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
				$financialChecker = true;
			}
		}

		$financial_score = 0;
		if($financialChecker){
			$financial_score = $financialTotal;
			$financial_rating = '';

			if($financialTotal >= 1.6) {
				$financial_rating	= 'AAA';
				$fin_rate_display = 'AAA - Excellent';
				$color  = 'green';
				$pdefault = 1;
			} elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
				$financial_rating	= 'AA';
				$fin_rate_display = 'AA - Very Good';
				$color = 'green';
				$pdefault = 2;
			} elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
				$financial_rating	= 'A';
				$fin_rate_display = 'A - Good';
				$color = 'green';
				$pdefault = 3;
			} elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
				$financial_rating	= 'BBB';
				$fin_rate_display = 'AAA - Positive';
				$color = 'orange';
				$pdefault = 4;
			} elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
				$financial_rating	= 'BB';
				$fin_rate_display = 'BB - Normal';
				$color = 'orange';
				$pdefault = 6;
			} elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
				$financial_rating	= 'B';
				$fin_rate_display = 'B - Satisfactory';
				$color = 'red';
				$pdefault = 8;
			} elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
				$financial_rating	= 'CCC';
				$fin_rate_display = 'CCC - Unsatisfactory';
				$color = 'red';
				$pdefault = 15;
			} elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
				$financial_rating	= 'CC';
				$fin_rate_display = 'CC - Adverse';
				$color = 'red';
				$pdefault = 20;
			} elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
				$financial_rating	= 'C';
				$fin_rate_display = 'C - Bad';
				$color = 'red';
				$pdefault = 25;
			} else {
				$financial_rating	= 'D';
				$fin_rate_display = 'D - Critical';
				$color = 'red';
				$pdefault = 32;
			}
		} elseif ($value->score_sf){
			$cred_rating_exp    = explode('-', $report->score_sf);
			$rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
			$rate_val   = intval($rate_val);

			$financial_score = $rate_val;
			$financial_rating = '';

			if($rate_val >= 177){
				$financial_rating = 'AA';
				$fin_rate_display = 'AA - ' . $cred_rating_exp[1];
				$color = 'green';
				$pdefault = 2;
			}
			else if($rate_val >= 150 && $rate_val <= 176) {
				$financial_rating = 'A';
				$fin_rate_display = 'A - ' . $cred_rating_exp[1];
				$color = 'green';
				$pdefault = 3;
			}
			else if($rate_val >= 123 && $rate_val <= 149) {
				$financial_rating = 'BBB';
				$fin_rate_display = 'BBB - ' . $cred_rating_exp[1];
				$color = 'orange';
				$pdefault = 4;
			}
			else if($rate_val >= 96 && $rate_val <= 122) {
				$financial_rating = 'BB';
				$fin_rate_display = 'BB - ' . $cred_rating_exp[1];
				$color = 'orange';
				$pdefault = 6;
			}
			else if($rate_val >= 68 && $rate_val <= 95) {
				$financial_rating = 'B';
				$fin_rate_display = 'B - ' . $cred_rating_exp[1];
				$color = 'red';
				$pdefault = 8;
			}
			else if($rate_val < 68) {
				$financial_rating = 'CCC';
				$fin_rate_display = 'CCC - ' . $cred_rating_exp[1];
				$color = 'red';
				if($rate_val >= 42 && $rate_val <= 68) {
					$pdefault = 15;
				} else {
					$pdefault= 30;
				}
			}
		}

		return array(
			'financial_rating'	=>	$financial_rating,
			'financial_score'	=>	$financial_score,
			'pdefault'			=>	$pdefault,
			'color'				=>  $color,
			'rating_display'	=> $fin_rate_display
		);
	}

	public function exportReports(){
		$input = Input::all();

		if(isset($input['from']) && isset($input['to'])){
			$reports = Entity::select('tblentity.entityid', 'tblentity.companyname', 'tbllogin.email', 'tblbank.bank_name', 
									'tblentity.created_at', 'tblentity.completed_date', 'tblentity.is_premium', 'tblentity.discount_code', 
									'bank_keys.serial_key', 'tblentity.status', 'tbllogin.loginid', 'tblentity.province', 'tblentity.current_bank')
										->leftJoin('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
						->leftJoin('tblbank', 'tblbank.id', '=', 'tblentity.current_bank')
						->leftJoin('bank_keys', 'bank_keys.entity_id', '=', 'tblentity.entityid')
						->where('tbllogin.email', '<>', '')
						->whereBetween('tblentity.created_at', [$input['from'], $input['to']])
						->get();

			foreach($reports as $report){
				/** Report Type */
				if($report->is_premium == 2){
					$report->is_premium = 'Simplified';
				}elseif($report->is_premium == 1){
					$report->is_premium = 'Premium';
				}else{
					$report->is_premium = 'Standalone';
				}

				/** Get Rating of the submitted report */
				$report->rating = '';
				if($report->status == 7){
					$rating = $this->getFinancialRating($report);
					$report->rating = $rating['rating_display'];
				}

				/** Check payment  */
				$report->payment_method = '';
				$report->price = 0;
				$is_payment = false;

				$isDragonPay = DB::table('tbltransactions')->where('entity_id', $report->entityid)->get();
				if($isDragonPay){
					foreach($isDragonPay as $dp){
						if($dp->status == 'S'){
							$report->payment_method = 'Dragonpay';
							$receipts = DB::table('payment_receipts')->where('reference_id', $dp->id)->where('payment_method', 2)->first();
							if($receipts){
								$report->price = $receipts->total_amount;
								$is_payment = true;
							}
						}
					}
				}

				$isPaypal = DB::table('paypal_transactions')->where('entity_id', $report->entityid)->get();
				if($isPaypal){
					foreach($isPaypal as $pp){
						$paypalRecepts = DB::table('payment_receipts')->where('reference_id', $pp->id)->where('payment_method', 1)->first();
						if($paypalRecepts){
							$report->payment_method = 'Paypal';
							$report->price = $paypalRecepts->total_amount;
							$is_payment = true;
						}
					}
				}

				if($is_payment == false){
					/** Get report price base on bank */
					$bank = DB::table('tblbank')->where('id', $report->current_bank)->first();
					if($bank){
						if($report->is_premium == 2){
							$report->price = $bank->simplified_price;
						}elseif($report->is_premium == 1){
							$provCode = Province2::where('provDesc', $report->province)->pluck('regCode')->first(); //get province code for filters
							if ($provCode == 13) {                                      // Metro Manila
								$report->price = $bank->premium_zone1_price;
							}elseif(in_array($provCode, array(04,03))){                 // Laguna, Rizal, Cavite and Bulacan
								$report->price = $bank->premium_zone2_price;
							}else{                                                      // Rest of the Regions
								$report->price = $bank->premium_zone3_price;
							}
						}else{
							$report->price = $bank->standalone_price;
						}
					}
					if($report->serial_key){
						$report->discount_code = '';
					}
				}else{
					$report->discount_code = '';
					$report->serial_key = '';
				}

				// If the report is a trial account
				if($is_payment == false && $report->discount_code == '' && $report->serial_key == ''){
					// $report->payment_method = 'Trial Account - Free';
				}
			}

			$final_reports = [];
			// Arrange data
			foreach($reports as $report){
				$final_reports[] = array(
					'entityid'			=> $report->entityid,
					'companyname'		=> $report->companyname,
					'email'				=> $report->email,
					'organization'		=>	$report->bank_name,
					'date_started'		=>	$report->created_at,
					'date_finished'		=>	$report->date_completed,
					'report_type'		=>	$report->is_premium,
					'price'				=> $report->price,
					'rating'			=> $report->rating,
					'payment_method'	=> $report->payment_method,
					'discount_code'		=> $report->discount_code,
					'client_key'		=> $report->serial_key
				);
			}

			Excel::create('Reports', function($excel) use ($final_reports) {
				$excel->sheet('Reports', function($sheet) use($final_reports){
					$sheet->fromArray($final_reports);
				});
			})->store('xlsx', public_path('documents/admin/reports'));
		}

		return;
	}

	public function downloadExportedReports(){
		if(Auth::user()->role == USER_ADMINISTRATOR){
			$filePath = public_path('documents/admin/reports/Reports.xlsx');
			return response()->download($filePath);
		}
		App::abort(403, 'Unauthorized action.');
	}

	public function postReportsReceiver(){
		$agency_email = Input::get('agency_email');

		$messages = array(
			'agency_email.required' => 'The Email field is required.',
			'agency_email.unique' => 'The Email you are trying to register is already taken.',
		);

		$rules = array(
			'agency_email'	=> 'required|unique:tbl_reports_receiver,email|email',
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails()) {
			$error = $validator->messages()->all();
			return array('error' => $error[0]);
		}else{
			/** Save users */
			DB::table('tbl_reports_receiver')->insert(['email' => $agency_email]);
		}

		$users = DB::table('tbl_reports_receiver')->get();
		return $users;
	}

	public function deleteReportsReceiver($id){
		$idDel = DB::table('tbl_reports_receiver')->where('id', $id)->delete();
		return redirect('/admin/reports');
	}
}
