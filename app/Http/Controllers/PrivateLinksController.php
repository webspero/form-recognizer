<?php
//======================================================================
//  Class 44: PrivateLinksController
//  Description: Contains functions regarding private links
//======================================================================
class PrivateLinksController extends BaseController {
	
	//-----------------------------------------------------
    //  Function 44.1: getPrivateLinkSummary
	//  Description: gets the private link summary page
    //-----------------------------------------------------
	public function getPrivateLinkSummary($code)
	{
        $base_year      = 0;
        
		$code = explode('$',base64_decode(base64_decode($code)));	// decode hashed link
		if(!ctype_digit($code[0]) || $code[1]!='encode'){
			App::abort(404);
		}
		$paramid = $code[0];
		
		$entity = DB::table('tblentity')	// get entity
			->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
			->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
			->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
			->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
			->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
			->where('tblentity.entityid', '=', $paramid)
			->first();
		
		$bizdrivers = DB::table('tblbusinessdriver')				// get busines drivers
		->where('entity_id', $paramid)
		->get();
        
        $bizsegments = DB::table('tblbusinesssegment')				// get business segment
		->where('entity_id', $paramid)
		->get();
        
        $pastprojects = DB::table('tblprojectcompleted')			// get projects completed
		->where('entity_id', $paramid)
		->get();
        
        $futuregrowths = DB::table('tblfuturegrowth')				// get future growth initiative
		->where('entity_id', $paramid)
		->get();
		
		$revenuepotential = DB::table('tblrevenuegrowthpotential')	// get revenue growth potential
			->where('entity_id', '=', $paramid)
			->get();
			
		$planfacilityrequested = DB::table('tblplanfacilityrequested')	// get required credit line
			->where('entity_id', '=', $paramid)
			->get();
			
		$documents9 = DB::table('tbldocument')						// get corresponding documents
			->where('entity_id', '=', $entity->entityid)
			->where('document_group', '=', 99999)
			->orderBy('document_type', 'ASC')
			->get();
		
		$finalscore = DB::table('tblsmescore')
            ->where('entityid', '=', $entity->entityid)
            ->orderBy('smescoreid', 'desc')
            ->take(1)
            ->get();

		$readyratiodata = DB::table('tblreadyrationdata')			// get RR saved data
			->where('entityid', '=', $entity->entityid)
            ->orderBy('readyrationdataid', 'desc')
            ->take(1)
            ->get();
			
		// bank rating
		$show_bank_rating = false;
		$bank_standalone = 0;
		$gscore = 0;
		if(@count($finalscore)>0){
			if($entity->current_bank != 0){
				$bank_standalone = Bank::find($entity->current_bank)->is_standalone;
			}
			if($entity->current_bank != 0 && $entity->is_independent == 0){
				
				$show_bank_rating = true;
				$bank_group = BankIndustry::where('bank_id', $entity->current_bank)
						->where('industry_id', $entity->industry_main_id)->first();
				$bank_formula = BankFormula::where('bank_id', $entity->current_bank)->first();
			}
			
			if($show_bank_rating){
				
				if($bank_group){
					if($bank_formula){
						//bcc
						$bgroup_array = explode(',', $bank_formula->bcc);
						$bgroup_item = 0;
						$bgroup_total = 0;
						if(in_array('rm', $bgroup_array)) {
							$bgroup_item += $finalscore[0]->score_rm;
							$bgroup_total += MAX_RM;
						}
						if(in_array('cd', $bgroup_array)) {
							$bgroup_item += $finalscore[0]->score_cd;
							$bgroup_total += MAX_CD;
						}
						if(in_array('sd', $bgroup_array)) {
							$bgroup_item += $finalscore[0]->score_sd;
							$bgroup_total += MAX_SD;
						}
						if(in_array('boi', $bgroup_array)) {
							$bgroup_item += $finalscore[0]->score_bois;
							$bgroup_total += MAX_BOI;
						}
                        
                        if (0 < $bgroup_total) {
                            $bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
                        }
                        else {
                            $bgroup = 0;
                        }
						
						//mq
						$mgroup_array = explode(',', $bank_formula->mq);
						$mgroup_item = 0;
						$mgroup_total = 0;
						if(in_array('boe', $mgroup_array)) {
							$mgroup_item += $finalscore[0]->score_boe;
							$mgroup_total += MAX_BOE;
						}
						if(in_array('mte', $mgroup_array)) {
							$mgroup_item += $finalscore[0]->score_mte;
							$mgroup_total += MAX_MTE;
						}
						if(in_array('bd', $mgroup_array)) {
							$mgroup_item += $finalscore[0]->score_bdms;
							$mgroup_total += MAX_BD;
						}
						if(in_array('sp', $mgroup_array)) {
							$mgroup_item += $finalscore[0]->score_sp;
							$mgroup_total += MAX_SP;
						}
						if(in_array('ppfi', $mgroup_array)) {
							$mgroup_item += $finalscore[0]->score_ppfi;
							$mgroup_total += MAX_PPFI;
						}
                        
                        if (0 < $mgroup_total) {
                            $mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
                        }
                        else {
                            $mgroup = 0;
                        }
						
						//fa
						$fgroup_array = explode(',', $bank_formula->fa);
						if(@count($fgroup_array) == 2){
							$fgroup = $finalscore[0]->score_facs;
						} else {
							if($bank_formula->fa == 'fp'){
								$fgroup = $finalscore[0]->score_rfp;
							} elseif($bank_formula->fa == 'fr'){
								$fgroup = $finalscore[0]->score_rfpm;
							} else {
								$fgroup = 0;
							}
						}
					} else {
						$bgroup = ( $finalscore[0]->score_rm + $finalscore[0]->score_cd + $finalscore[0]->score_sd + $finalscore[0]->score_bois );
						$mgroup = ( $finalscore[0]->score_boe + $finalscore[0]->score_mte + $finalscore[0]->score_bdms + $finalscore[0]->score_sp + $finalscore[0]->score_ppfi );
						$fgroup = $finalscore[0]->score_facs;
					}
					
					$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

					if($bank_formula && $bank_formula->boost == 0){
						// no boost
					} else {
						//boost
						if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
						if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
						if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
					}
				} else {
					$gscore = intval($finalscore[0]->score_sf);
				}
			}
        }
		
        $fa_report      = FinancialReport::where(['entity_id' => $entity->entityid, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();
            
		/*  Forecast Slope Calculation  */
		$forecast_data  = NULL;
		
        if (NULL != $fa_report) {
            $base_year      = $fa_report->year;
            $gf_lib         = new GrowthForecastLib($entity->entityid, $entity->industry_main_id, $bank_standalone);
            $forecast_data  = $gf_lib->getGrowthForecastData();
        }
        
		return View::make('summary.privatelinksummary', 
			array(
				'documents9' => $documents9,
				'finalscore' => $finalscore,
				'readyratiodata' => $readyratiodata,
				'grandscore' => $gscore,
				'grdp_year' => GRDP::getYear(),
				'show_bank_rating' => $show_bank_rating,
				'bizdrivers'        => $bizdrivers,
				'bizsegments'       => $bizsegments,
				'pastprojects'      => $pastprojects,
				'futuregrowths'     => $futuregrowths,
				'revenuepotential' => $revenuepotential,
				'planfacilityrequested' => $planfacilityrequested,
				'bank_standalone' => $bank_standalone,
				'forecast_data' => $forecast_data,
                'base_year'     => $base_year
			)
		)
		->with('entity', $entity);
	}
}