<?php

//======================================================================
//  Class 47: Public API Controller
//      APIs for public usage
//======================================================================
class PublicAPIController extends BaseController
{    
    //-----------------------------------------------------
    //  Function 47.1: getCreditRatingByCode
    //      Gets the Rating according to the Public Link code
    //-----------------------------------------------------
    public static function getCreditRatingByCode()
    {
        /*  Variable Declaration        */
        $code                   = $_GET['key_code'];
        $email                  = $_GET['account_email'];
        
        $json_data              = array();
        $json_data['status']    = STS_NG;
        $code                   = explode('$',base64_decode(base64_decode($code)));
		
        /*  Checks the validity of the code     */
        if (!ctype_digit($code[0]) || $code[1]!='encode') {
			/*  Creates a message for invalid code  */
            $json_data['message']   = 'Invalid SME';
		}
        else {
            $entity_id  = $code[0];
            
            /*  Acquires the SME Data       */
            $entity = DB::table('tblentity')
                ->where('entityid', $entity_id)
                ->first();
            
            /*  Checks the status of the SME    */
            if (0 >= @count($entity)) {
                /*  Creates a message for invalid code  */
                $json_data['message']   = 'SME does not exist';
            }
            /*  Rating is not yet finished      */
            else if (7 > $entity->status) {
                $json_data['message']   = 'The Credit Rating of the SME is not yet available';
            }
            /*  Rating is available             */
            else {
                /*  Acquires the User Data      */
                $user   = DB::table('tbllogin')
                    ->where('loginid', $entity->loginid)
                    ->first();
                
                /*  Invalid Login Credentials   */
                if ($email != sha1($user->email)) {
                    $json_data['message']   = 'Invalid Credentials';
                }
                /*  Valid Credentials           */
                else {
                    /*  Acquires the SME Rating from the database       */
                    $sme_score = DB::table('tblsmescore')
                        ->where('entityid', '=', $entity_id)
                        ->first();
                    
                    $json_data['status']            = STS_OK;
                    $json_data['message']           = 'Credit Rating is Available';
                    $json_data['rating']            = self::getCalculatedSmeReport($entity_id);
                }
            }
        }
        
        return json_encode($json_data);
    }
    
    //-----------------------------------------------------
    //  Function 47.2: getCreditRatingByName
    //      Get Reports according to Company Name
    //-----------------------------------------------------
    public static function getCreditRatingByName()
    {
        $email                  = $_GET['account_email'];
        $name                   = rawurldecode($_GET['company_name']);
        
        $json_data              = array();
        $json_data['status']    = STS_NG;
		$reports_count          = 0;
        
        /*  Checks the validity of the code     */
        if (!isset($name)) {
            $json_data['message']   = 'No Company given';
        }
        if (STR_EMPTY == $name) {
			/*  Creates a message for invalid code  */
            $json_data['message']   = 'Please indicate company name';
		}
        else {
            /*  Acquires the SME Data       */
            $entities = DB::table('tblentity')
                ->where('companyname', $name)
                ->orderBy('entityid', 'desc')
                ->get();
            
            /*  Checks the number of Reports        */
            if (0 >= @count($entities)) {
                /*  Creates a message for no record exists  */
                $json_data['message']   = 'No record exists for the specified Company';
            }
            else {
                /*  Acquires the User Data          */
                $user   = DB::table('tbllogin')
                    ->where('loginid', $entities[0]->loginid)
                    ->first();
                
                /*  Invalid Login Credentials       */
                if ($email != sha1($user->email)) {
                    $json_data['message']   = 'Invalid Credentials';
                }
                /*  Valid Credentials               */
                else {
                    /*  Save Rating results to Output Array     */
                    foreach ($entities as $entity) {
                        /**  SME has completed rating process   */
                        if (7 <= $entity->status) {
                            $rated_reports[] = self::getCalculatedSmeReport($entity->entityid);
                            $reports_count++;
                        }
                    }
                    
                    /*  There are completed ratings         */
                    if (0 < $reports_count) {
                        $json_data['status']    = STS_OK;
                        $json_data['message']   = 'The Credit Ratings are available';
                        $json_data['ratings']   = $rated_reports;
                    }
                    /*  No completed ratings yet            */
                    else {
                        $json_data['message']   = 'The Credit Rating of the SME is not yet available';
                    }
                }
            }
        }
        
        return json_encode($json_data);
    }
    
    //-----------------------------------------------------
    //  Function 47.3: getCreditRatingByLoginID
    //      Get Reports according to Login ID
    //-----------------------------------------------------
    public static function getCreditRatingByLoginID()
    {
        /*  Variable Declaration        */
        $code                   = $_GET['key_code'];
        $email                  = $_GET['account_email'];
        
        $json_data              = array();
        $json_data['status']    = STS_NG;
        $valid_creds            = STS_NG;
		$reports_count          = 0;
        $decoded_code           = explode('$', base64_decode(base64_decode($code)));
        
        /*  Checks the validity of the code     */
        if (!ctype_digit($decoded_code[0]) || $decoded_code[1]!='account') {
			/*  Creates a message for invalid code  */
            $json_data['message']   = 'Invalid Key Code';
		}
        else {
            /*  Acquires the SME Data       */
            $login_id   = $decoded_code[0];
            $user       = DB::table('tbllogin')
                ->where('loginid', $login_id)
                ->first();
            
            /*  Invalid Login Credentials       */
            if ($email != sha1($user->email)) {
                $json_data['message']   = 'Invalid Credentials';
            }
            /*  Valid Credentials               */
            else {
                $valid_creds    = STS_OK;
                
                /** Supervisor Account  */
                if (4 == $user->role) {
                    $entities   = DB::table('tblentity')
                        ->where('current_bank', $user->bank_id)
                        ->orderBy('companyname', 'asc')
                        ->get();
                }
                /** SME Account         */
                else {
                    $entities   = DB::table('tblentity')
                        ->where('loginid', $login_id)
                        ->orderBy('entityid', 'desc')
                        ->get();
                }
                
                /*  No Report exists        */
                if (0 >= @count($entities)) {
                    /*  Creates a message for no record exists  */
                    $json_data['message']   = 'No record exists for the specified Login ID';
                }
                /*  Reports exists          */
                else {
                    /*  Saves the report data to the output array   */
                    foreach ($entities as $entity) {
                        /*  The report already has Credit Rating        */
                        if (7 <= $entity->status) {
                            $rated_reports[] = self::getCalculatedSmeReport($entity->entityid);
                            $reports_count++;
                        }
                    }
                    
                    /*  There are reports with completed Ratings        */
                    if (0 < $reports_count) {
                        $json_data['status']    = STS_OK;
                        $json_data['message']   = 'The Credit Ratings are available';
                        $json_data['ratings']   = $rated_reports;
                    }
                    /*  No Report has ratings yet       */
                    else {
                        $json_data['message']   = 'The Credit Rating of the SME is not yet available';
                    }
                }
            }
        }
        
        return json_encode($json_data);
    }
    
    //-----------------------------------------------------
    //  Function 47.4: getCalculatedSmeReport
    //      Gets the Calculated results according to Entity ID
    //-----------------------------------------------------
    public static function getCalculatedSmeReport($entity_id)
    {
        /*  Variable Declaration        */
        $cbpo_rating    = array();
        $rating         = array();
        
        /*  Acquires the SME Rating from the database   */
        $entity     = DB::table('tblentity')->where('entityid', $entity_id)->first();
        $sme_score  = DB::table('tblsmescore')
            ->where('entityid', '=', $entity_id)
            ->first();
        
        /*  Calculates for the Ratings and store in the output array    */
        $cbpo_rating                    = self::getCreditRating($sme_score->score_sf);
        $rating['company_name']         = $entity->companyname;
        $rating['fin_position']         = self::convertFinPosition($sme_score->score_rfp);
        $rating['fin_performance']      = self::convertFinancialPerformance($sme_score->score_rfpm);
        $rating['fin_condition']        = self::converFinancialCondition($sme_score->score_facs);
        $rating['cbpo_rating_score']    = $cbpo_rating['rating_txt'];
        $rating['cbpo_rating_desc']     = $cbpo_rating['rating_desc'];
        $rating['rating_date']          = $sme_score->updated_at;
        
        return $rating;
    }
    
    //-----------------------------------------------------
    //  Function 47.5: convertFinPosition
    //      Converts Financial Position Score to
    //      Displayed Rating
    //-----------------------------------------------------
    private static function convertFinPosition($fin_pos)
    {
        /*  Variable Declaration    */
        $conv_pos   = STR_EMPTY;
        
        /*  Financial Position Rating Calculation   */
        if ($fin_pos >= 162) {
            $conv_pos = "AAA";
        }
        else if ($fin_pos >= 145) {
            $conv_pos = "AA";
        }
        else if ($fin_pos >= 127) {
            $conv_pos = "A";
        }
        else if ($fin_pos >= 110) {
            $conv_pos = "BBB";
        }
        else if ($fin_pos >= 92) {
            $conv_pos = "BB";
        }
        else if ($fin_pos >= 75) {
            $conv_pos = "B";
        }
        else if ($fin_pos >= 57) {
            $conv_pos = "CCC";
        }
        else if ($fin_pos >= 40) {
            $conv_pos = "CC";
        }
        else if ($fin_pos >= 22) {
            $conv_pos = "C";
        }
        else if ($fin_pos >= 4) {
            $conv_pos = "D";
        }
        else if ($fin_pos < 4) {
            $conv_pos = "E";
        }
        
        return $conv_pos;
    }
    
    //-----------------------------------------------------
    //  Function 47.6: convertFinancialPerformance
    //      Converts Financial Performance Score to
    //      Displayed Rating
    //-----------------------------------------------------
    private static function convertFinancialPerformance($fin_perf)
    {
        /*  Variable Declaration    */
        $conv_perf  = STR_EMPTY;
        
        /*  Financial Performance Rating Calculation    */
        if ($fin_perf >= 162) {
            $conv_perf = "AAA";
        }
        else if ($fin_perf >= 145) {
            $conv_perf = "AA";
        }
        else if ($fin_perf >= 127) {
            $conv_perf = "A";
        }
        else if ($fin_perf >= 110) {
            $conv_perf = "BBB";
        }
        else if ($fin_perf >= 92) {
            $conv_perf = "BB";
        }
        else if ($fin_perf >= 75) {
            $conv_perf = "B";
        }
        else if ($fin_perf >= 57) {
            $conv_perf = "CCC";
        }
        else if ($fin_perf >= 40) {
            $conv_perf = "CC";
        }
        else if ($fin_perf >= 22) {
            $conv_perf = "C";
        }
        else if ($fin_perf >= 4) {
            $conv_perf = "D";
        }
        else if ($fin_perf < 4) {
            $conv_perf = "E";
        }
        
        return $conv_perf;
    }
    
    //-----------------------------------------------------
    //  Function 47.7: converFinancialCondition
    //      Converts Financial Condition Score to
    //      Displayed Rating
    //-----------------------------------------------------
    private static function converFinancialCondition($fin_cond)
    {
        /*  Variable Declaration        */
        $conv_cond    = STR_EMPTY;
        
        /*  Sets the Chart data to be displayed according to points */
        if ($fin_cond >= 162) {
            $conv_cond    = 'AAA';
        }
        else if ($fin_cond >= 145) {
            $conv_cond    = 'AA';
        }
        else if ($fin_cond >= 127) {
            $conv_cond    = 'A';
        }
        else if ($fin_cond >= 110) {
            $conv_cond    = 'BBB';
        }
        else if ($fin_cond >= 92) {
            $conv_cond    = 'BB';
        }
        else if ($fin_cond >= 75) {
            $conv_cond    = 'B';
        }
        else if ($fin_cond >= 57) {
            $conv_cond    = 'CCC';
        }
        else if ($fin_cond >= 40) {
            $conv_cond    = 'CC';
        }
        else if ($fin_cond >= 22) {
            $conv_cond    = 'C';
        }
        else if ($fin_cond >= 4) {
            $conv_cond    = 'D';
        }
        else if ($fin_cond < 4) {
            $conv_cond    = 'E';
        }
        
        return $conv_cond;
    }
    
    //-----------------------------------------------------
    //  Function 47.8: getCreditRating
    //      Acquires the Credit Rating Description
    //      according to CreditBPO score
    //-----------------------------------------------------
    private static function getCreditRating($cbpo_rating)
    {
        /*  Variable Declaration    */
        $cbpo_rate  = array();
        $credit_exp = explode('-', $cbpo_rating);
        $gscore     = $credit_exp[0];
        $gtxt       = $credit_exp[1];
        
        /*  CreditBPO Rating Calculation    */
        if($gscore >= 177) {
            $cbpo_rate['rating_txt']    = "AA";
            $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
        }
        else if ($gscore >= 150 && $gscore <= 176) {
            $cbpo_rate['rating_txt']    = "A";
            $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
        }
        else if ($gscore >= 123 && $gscore <= 149) {
            $cbpo_rate['rating_txt']    = "BBB";
            $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
        }
        else if ($gscore >= 96 && $gscore <= 122) {
            $cbpo_rate['rating_txt']    = "BB";
            $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
        }
        else if ($gscore >= 69 && $gscore <= 95) {
            $cbpo_rate['rating_txt']    = "B";
            $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
        }
        else if($gscore < 69){
            $cbpo_rate['rating_txt']    = "CCC";
            $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
        }
        
        return $cbpo_rate;
    }
    
    //-----------------------------------------------------
    //  Function 47.9: login APITest
    //      function for testing login
    //-----------------------------------------------------
    public static function loginAPITest()
    {
        $test_url                   = URL::To('cbpo_pub_rest/get_account_rating');
        $test_data                  = array();
        $test_data['account_email'] = sha1('bank@user.com');
        $test_data['key_code']      = 'TlRra1lXTmpiM1Z1ZEE9PQ==';
        
        $result = file_get_contents(
            $test_url.
            '?account_email='.$test_data['account_email'].
            '&key_code='.$test_data['key_code']
        );
        
        $json_data = json_decode($result);
        
        echo $json_data->message;
    }
    
    //-----------------------------------------------------
    //  Function 47.10: RatingAPITest
    //      function for testing rating
    //-----------------------------------------------------
    public static function RatingAPITest()
    {
        /*--------------------------------------------------------------------
        /*	Variable Declaration
		/*------------------------------------------------------------------*/
        $test_url                   = URL::To('cbpo_pub_rest/get_rating_result');
        $test_data                  = array();
        $test_data['account_email'] = sha1('ruffy.collado@creditbpo.com');
        $test_data['key_code']      = 'T0RNa1pXNWpiMlJs';
        
        /*--------------------------------------------------------------------
        /*	Send an HTTP GET request to CreditBPO along with the parameters
		/*------------------------------------------------------------------*/
        $result = file_get_contents(
            $test_url.
            '?account_email='.$test_data['account_email'].
            '&key_code='.$test_data['key_code']
        );
        
        /*--------------------------------------------------------------------
        /*	Convert the JSON data response to an Object
		/*------------------------------------------------------------------*/
        $json_data = json_decode($result);
        
        /*--------------------------------------------------------------------
        /*	Displays the Status of the request
		/*------------------------------------------------------------------*/
        echo $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.11: companyAPITest
    //      function for testing company API
    //-----------------------------------------------------
    public static function CompanyAPITest()
    {
        $test_url                   = URL::To('cbpo_pub_rest/get_company_rating');
        $test_data                  = array();
        $test_data['account_email'] = sha1('ruffy.collado@creditbpo.com');
        $test_data['company_name']  = rawurlencode('Stark Industries 1');
        
        $result = file_get_contents(
            $test_url.
            '?account_email='.$test_data['account_email'].
            '&company_name='.$test_data['company_name']
        );
        
        $json_data = json_decode($result);
        
        echo $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.12: testPassToGet
    //      
    //-----------------------------------------------------
    public static function testPassToGet()
    {
        $test_data  = array();
        
        $section    = 'spouse';
        $section_id = '2110009127';
        //$api_key    = 'mSelXRlr';
        
        $test_url   = URL::To('cbpo_rest_api/get_data/capital_details/101');
        //$test_url   = 'http://128.199.203.18/cbpo_rest_api/get_data/'.$section.'/'.$section_id;
        
        $email      = 'squall@seed.balamb.edu';
        $pword      = 'cbfdac6008f9cab4083784cbd1874f76618d2a97';
        //$email      = 'creditbpo.philippines@gmail.com';
        //$pword      = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';

        //creditbpo.philippines+bloomcs@gmail.com
        $result = file_get_contents(
            $test_url.
            '?email='.urlencode($email).
            '&pword='.$pword
        );
        
        $sent_hdr = get_headers($test_url.
            '?email='.urlencode($email).
            '&pword='.$pword,
        1
        );
        
        $output['sent_hdr'] = STR_EMPTY;
        
        foreach ($sent_hdr as $key_resp => $val_resp) {
            $output['sent_hdr'] .= $val_resp.'<br/>';
        }
        
        $output['response'] = STR_EMPTY;
        
        foreach ($http_response_header as $key_resp => $val_resp) {
            $output['response'] .= $val_resp.'<br/>';
        }
        
        echo $output['sent_hdr'].'<br/><br/><br/>';
        echo $output['response'];
        
        //$json_data = json_decode($result);
        
        //echo $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.13: testPassToDelete
    //      
    //-----------------------------------------------------
    public static function testPassToDelete()
    {
        $test_url   = URL::To('cbpo_rest_api/delete_data/existing_credit/700');
        $test_data  = array();
        $email      = 'squall@seed.balamb.edu';
        $pword      = 'cbfdac6008f9cab4083784cbd1874f76618d2a97';
        
        $garden     = array(
            '61',
            'carrots'
        );
        
        $garden = implode('-', $garden);
        $seed   = sha1($garden);
        
        $result = file_get_contents(
            $test_url.
            '?email='.$email.
            '&pword='.$pword.
            '&seed='.$seed
        );
        
        $json_data = json_decode($result);
        
        echo $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.14 testPassToLogin
    //      
    //-----------------------------------------------------
    public static function testPassTologin()
    {
        $post_url   = URL::To('/cbpo_rest_api/user_login');
        
        /*
        $email      = 'creditbpo.philippines+bloomcs@gmail.com';
        $pword      = sha1('password');
        $action     = 'province';
        $section    = 'province';
        */
        
        $post_url   = 'http://128.199.203.18/cbpo_rest_api/user_login';
        $test_data  = array();
        $email      =  'ruffy.collado+sme01@creditbpo.com';
        $pword      = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';
        $action     = 'user_login';
        $section    = 'user_account';
        
        //$post_fields['email']       = 'creditbpo.philippines+bloomcs@gmail.com';
        $post_fields['email']       = 'ruffy.collado@creditbpo.com';
        $post_fields['password']    = 'password';
        
        $input = array(
            'email'         => $email,
            'pword'         => $pword,
            'action'        => $action,
            'section'       => $section,
            'post_fields'   => $post_fields
        );
        
        $post_fields['api_key']     = 'cabbage';
        
        $garden                     = implode('-', $post_fields);
        $input['seed']              = sha1($garden);
        
        /*--------------------------------------------------------------------
        /*	Send POST request to Highcharts export Server via CuRL
        /*------------------------------------------------------------------*/
        /* Initializes the CuRL Library */
        $curl = curl_init();
        
        /* Sets the CuRL POST Request configuration  */
        curl_setopt($curl, CURLOPT_URL, $post_url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($input));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        $verbose = fopen('php://temp', 'w+');
        curl_setopt($curl, CURLOPT_STDERR, $verbose);
        
        /* Execute CuRL Request */
        $result = curl_exec($curl);
        
        rewind($verbose);
        //$verboseLog = stream_get_contents($verbose);
        //echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
        
        /*  Closes the CuRL Library */
        curl_close($curl);
        
        return $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.15: testPassToPost
    //      
    //-----------------------------------------------------
    public static function testPassToPost()
    {
        $test_data  = array();
        
        $post_url   = URL::To('/cbpo_rest_api/misc/questionnaire_config');
        $email      = 'squall@seed.balamb.edu';
        $pword      = 'cbfdac6008f9cab4083784cbd1874f76618d2a97';
        
        //$post_url   = 'http://128.199.203.18/cbpo_rest_api/misc/questionnaire_config';
        //$email      = 'creditbpo.philippines@gmail.com';
        //$pword      = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';
        $action     = 'questionnaire_config';
        $section    = 'misc';
        
        //$post_fields['email']                 = 'creditbpo.philippines+bloomcs@gmail.com';
        $post_fields['entity_id']             = '134';

        $input = array(
            'email'         => $email,
            'pword'         => $pword,
            'action'        => $action,
            'section'       => $section,
            'post_fields'   => $post_fields
        );
        
        /*--------------------------------------------------------------------
        /*	Send POST request to Highcharts export Server via CuRL
        /*------------------------------------------------------------------*/
        /* Initializes the CuRL Library */
        $curl = curl_init();
        
        /* Sets the CuRL POST Request configuration  */
        curl_setopt($curl, CURLOPT_URL, $post_url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($input));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        $verbose = fopen('php://temp', 'w+');
        curl_setopt($curl, CURLOPT_STDERR, $verbose);
        
        /* Execute CuRL Request */
        $result = curl_exec($curl);
        
        rewind($verbose);
        //$verboseLog = stream_get_contents($verbose);
        //echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
        
        /*  Closes the CuRL Library */
        curl_close($curl);
        
        return $result;
    }
    
    //-----------------------------------------------------
    //  Function 47.16: testUploadToPost
    //      function to test uploading
    //-----------------------------------------------------
    public static function testUploadToPost()
    {
        $test_data  = array();
        
        $post_url   = URL::To('/cbpo_rest_api/sme_data/134');
        $email      = 'squall@seed.balamb.edu';
        $pword      = 'cbfdac6008f9cab4083784cbd1874f76618d2a97';
        $action     = 'upload';
        $section    = 'bank_statements';
        
        $post_url   = 'http://128.199.203.18/cbpo_rest_api/sme_data/565';
        $email      = 'creditbpo.philippines@gmail.com';
        $pword      = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';
        $action     = 'upload';
        $section    = 'business_details';
        
        $input = array(
            'email'         => $email,
            'pword'         => $pword,
            'action'        => $action,
            'section'       => $section,
        );
        
        $input['companyname']                   = 'Company AA';
        $input['entity_type']                   = '1';
        $input['company_tin']                   = '123123123';
        $input['tin_num']                       = '2342342';
        $input['sec_reg_date']                  = '2016-08-01';
        
        $input['industrymain']                  = '2';
        $input['industrysub']                   = '2';
        $input['industryrow']                   = '2';
        $input['total_asset_grouping']          = '1';
        $input['total_assets']                  = '777700000';
        $input['emailalt']                      = 'ruffy.collado@creditbpo.com';
        $input['website']                       = '';
        $input['street']                        = 'Hermosa';
        $input['province']                      = 'Manila';
        $input['city']                          = '1703';
        $input['zipcode']                       = '1013';
        $input['phone']                         = '632 999 9999';
        $input['no_yrs_present_address']        = '23';
        $input['dateestablished']               = '2016-09-23';
        $input['numberyear']                    = '32';
        $input['description']                   = 'Hello People';
        $input['employeesize']                  = '23';
        $input['updated_date']                  = '2016-09-22';
        $input['number_year_management_team']   = '4';
        $input['requested_by']                  = '0';
        $input['bank']                          = '0';
        
        $input['api_key']   = 'carrots';
        $input['api_key']   = 'mSelXRlr';
        
        $garden                     = implode('-', $input);
        $input['seed']              = sha1($garden);
        
        $input['sec_generation']        = curl_file_create(realpath('C:\IMG_ENTITY.png'));
        $input['sec_cert']              = curl_file_create(realpath('C:\IMG_ENTITY.png'));
        $input['permit_to_operate']     = curl_file_create(realpath('C:\IMG_ENTITY.png'));
        $input['income_tax']            = curl_file_create(realpath('C:\IMG_ENTITY.png'));
        
        /*--------------------------------------------------------------------
        /*	Send POST request to Highcharts export Server via CuRL
        /*------------------------------------------------------------------*/
        /* Initializes the CuRL Library */
        $curl = curl_init();
        
        /* Sets the CuRL POST Request configuration  */
        curl_setopt($curl, CURLOPT_URL, $post_url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $input);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        /* Execute CuRL Request */
        $result = curl_exec($curl);

        /*  Closes the CuRL Library */
        curl_close($curl);
        
        return $result;
    }
    
}