<?php
//======================================================================
//  Class 4: BaseController
//  Description: The base controller used by all other controllers
//======================================================================
class BaseController extends Controller {

	//-----------------------------------------------------
    //  Function 4.1: setupLayout
	//  Description: Setup the layout used by the controller
    //-----------------------------------------------------
	protected function setupLayout()
	{
        /*----------------------------------------------------------------
        /*	Enables loading of HTML files to the View
        /*--------------------------------------------------------------*/
        View::addExtension('html', 'php');
        
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
