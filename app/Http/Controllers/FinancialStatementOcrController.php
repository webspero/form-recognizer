<?php

use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FinancialStatementOcrController extends BaseController
{
    public function displayForm(){
        return view('fs_ocr');
    }

    public function pdfToImage($img)
    {
        //create financial folder if not exist
        $destinationPath = public_path('documents/vision/financialStatement/');
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath);
        }

        // convert pdf to image
        $imageName = "temp.".$img->getClientOriginalExtension();
        $img->move($destinationPath, $imageName);
        if (env("APP_ENV") == "local") {
            exec("magick convert -verbose -density 300 -trim ".$destinationPath.$imageName." ".$destinationPath."temp.png");
        } else {
            exec("convert -verbose -density 300 -trim ".$destinationPath.$imageName." ".$destinationPath."temp.png");
        }
        unlink($destinationPath.$imageName);

        return $destinationPath;
    }

    public function postFsOCR(Request $request)
    {
        $fs_data = array();
        $images = (array)$request->images;
        $images = array_reverse($images);
        
        foreach ($images as $img) {
            // get destination path and convert pdf to image
            $destinationPath = $this->pdfToImage($img);

            //get converted images generated
            $images = glob($destinationPath."/*.png");
            natsort($images);
            
            // variable to check if needed data is already detected
            $financial_position = STS_NG;
            $income_and_expenses = STS_NG;
            $changes_shareholders_equity = STS_NG;
            $cash_flows = STS_NG;

            // set constants - category
            $finPstn = "Statement of Financial Position";
            $incAndExp = "Statement of Income and Expenses";
            $CashFlows ="Statement of Cash Flows";

            $cash = "cash";
            $tradAndRec = "trade and receivables";
            $othCurAss = "Other Current Assets";
            $propAndEqui = "Property and Equipment";
            $deposits = "Deposits";
            $AccPayAndAccru = "Accounts payable and accruals";
            $tradAndOthPay = "Trade and other payables";
            $incTaxPay = "Income Tax Payable";
            $shareCap = "Share Capital";
            $retEarnings = "Retained Earnings";

            $revenue = "revenue";
            $costOfProj = "Cost of Projects";
            $sellAndAdmExp = "Selling and administrative expenses";
            $provForIncTax = "Provision for income tax";

            $depandAmor = "Depreciation and Amortization";
            $nonTradPay = "Non-Trade Payable";
            $ncGeneFromOperation = "Net cash generated from operation";

            //set default values for statement of financial position fs data
            $cash_primary_year = STR_EMPTY;
            $cash_secondary_year = STR_EMPTY;
            $trade_and_receivables_primary_year = STR_EMPTY;
            $trade_and_receivables_secondary_year = STR_EMPTY;
            $other_current_assets_primary_year = STR_EMPTY;
            $other_current_assets_secondary_year = STR_EMPTY;
            $total_current_assets_primary_year = STR_EMPTY;
            $total_current_assets_secondary_year = STR_EMPTY;
            $property_equipment_primary_year = STR_EMPTY;
            $property_equipment_secondary_year = STR_EMPTY;
            $total_noncurrent_assets_primary_year = STR_EMPTY;
            $total_noncurrent_assets_secondary_year = STR_EMPTY;
            $deposits_primary_year = STR_EMPTY;
            $deposits_secondary_year = STR_EMPTY;
            $total_assets_primary_year = STR_EMPTY;
            $total_assets_secondary_year = STR_EMPTY;
            $payable_and_accruals_primary_year = STR_EMPTY;;
            $payable_and_accruals_secondary_year = STR_EMPTY;
            $trade_and_payables_primary_year = STR_EMPTY;
            $trade_and_payables_secondary_year = STR_EMPTY;
            $income_tax_payable_primary_year = STR_EMPTY;
            $income_tax_payable_secondary_year = STR_EMPTY;
            $total_current_liabilities_primary_year = STR_EMPTY;
            $total_current_liabilities_secondary_year = STR_EMPTY;
            $share_capital_primary_year = STR_EMPTY;
            $share_capital_secondary_year = STR_EMPTY;
            $retained_earnings_primary_year = STR_EMPTY;
            $retained_earnings_secondary_year = STR_EMPTY;
            $total_shareholders_equity_primary_year = STR_EMPTY;
            $total_shareholders_equity_secondary_year = STR_EMPTY;
            $total_liabilities_and_shareholders_equity_primary_year = STR_EMPTY;
            $total_liabilities_and_shareholders_equity_secondary_year = STR_EMPTY;
            
            //set default values for statement of income and expenses fs data
            $revenue_primary_year = STR_EMPTY;
            $revenue_secondary_year = STR_EMPTY;
            $cost_of_projects_primary_year = STR_EMPTY;
            $cost_of_projects_secondary_year = STR_EMPTY;
            $gross_profit_primary_year = STR_EMPTY;
            $gross_profit_secondary_year = STR_EMPTY;
            $selling_administrative_expenses_primary_year = STR_EMPTY;
            $selling_administrative_expenses_secondary_year = STR_EMPTY;
            $net_income_before_tax_primary_year = STR_EMPTY;
            $net_income_before_tax_secondary_year = STR_EMPTY;
            $provision_for_income_tax_primary_year = STR_EMPTY;
            $provision_for_income_tax_secondary_year = STR_EMPTY;
            $income_tax_for_period_primary_year = STR_EMPTY;
            $income_tax_for_period_secondary_year = STR_EMPTY;
            
            //set default values for statement of cash flows fs data
            $net_income_primary_year = STR_EMPTY;
            $net_income_secondary_year = STR_EMPTY;
            $depreciation_and_amortization_primary_year = STR_EMPTY;
            $depreciation_and_amortization_secondary_year = STR_EMPTY;
            $income_before_capital_primary_year = STR_EMPTY;
            $income_before_capital_secondary_year = STR_EMPTY;
            $accounts_receivable_primary_year = STR_EMPTY;
            $accounts_receivable_secondary_year = STR_EMPTY;
            $inventories_primary_year = STR_EMPTY;
            $inventories_secondary_year = STR_EMPTY;
            $other_current_assets_cs_primary_year = STR_EMPTY;
            $other_current_assets_cs_secondary_year = STR_EMPTY;
            $accounts_payable_accrued_expenses_primary_year = STR_EMPTY;
            $accounts_payable_accrued_expenses_secondary_year = STR_EMPTY;
            $nontrade_payable_primary_year = STR_EMPTY;
            $nontrade_payable_secondary_year = STR_EMPTY;
            $net_cash_generated_from_operation_primary_year = STR_EMPTY;
            $net_cash_generated_from_operation_secondary_year = STR_EMPTY;
            $acquisition_properties_equipment_primary_year = STR_EMPTY;
            $acquisition_properties_equipment_secondary_year = STR_EMPTY;
            $net_cash_inventory_activities_primary_year = STR_EMPTY;
            $net_cash_inventory_activities_secondary_year = STR_EMPTY;
            $initial_additional_capital_primary_year = STR_EMPTY;
            $initial_additional_capital_secondary_year = STR_EMPTY;
            $net_cash_financing_activities_primary_year = STR_EMPTY;
            $net_cash_financing_activities_secondary_year = STR_EMPTY;
            $net_increase_decrease_cash_primary_year = STR_EMPTY;
            $net_increase_decrease_cash_secondary_year = STR_EMPTY;
            $cash_beginning_year_primary_year = STR_EMPTY;
            $cash_beginning_year_secondary_year = STR_EMPTY;
            $cash_end_year_primary_year = STR_EMPTY;
            $cash_end_year_secondary_year = STR_EMPTY;

            // set default values for statement of changes in shareholders equity
            $share_capital_se_primary_year = STR_EMPTY;
            $share_capital_se_secondary_year = STR_EMPTY;
            $balance_primary_year = STR_EMPTY;
            $balance_secondary_year = STR_EMPTY;
            $balance_beginning_primary_year = STR_EMPTY;
            $balance_beginning_secondary_year = STR_EMPTY;
            $net_income_period_primary_year = STR_EMPTY;
            $net_income_period_secondary_year = STR_EMPTY;
            $balance_ending_primary_year = STR_EMPTY;
            $balance_ending_secondary_year = STR_EMPTY;
            $total_shareholders_equity_se_primary_year = STR_EMPTY;
            $total_shareholders_equity_se_secondary_year = STR_EMPTY;

            $primary_year = null; // primary year of pdf
            $secondary_year = null; // secondary year of pdf

            // set default values of important vertices
            $cash_y1 = null;
            $trade_and_receivables_y1 = null;
            $other_current_assets_y1 = null;
            $property_equipment_y1 = null;
            $deposits_y1 = null;
            $payable_and_accruals_y1 = null;
            $trade_and_payables_y1 = null;
            $income_tax_payable_y1 = null;
            $share_capital_y1 = null;
            $retained_earnings_y1 = null;
            $revenue_y1 = null;
            $cost_of_projects_y1 = null;
            $selling_administrative_expenses_y1 = null;
            $provision_for_income_tax_y1 = null;
            $depreciation_and_amortization_y1 = null;
            $nontrade_payable_y1 = null;
            $net_cash_generated_from_operation_y1 = null;

            foreach ($images as $image_key => $image){ // iterate through pdf pages / images
                $words = array();

                // initiate vision
                $cloud = new ServiceBuilder([
                    'keyFilePath' => public_path('visionAPI.json'),
                    'projectId' => 'sixth-edition-253508'
                ]);

                $vision = $cloud->vision();
                
                //ocr image
                $imageVision = $vision->image(file_get_contents($image), ['text']);
                $textAnnotations = $vision->annotate($imageVision)->text();

                //disregard full text
                array_shift($textAnnotations);

                
                foreach ($textAnnotations as $text) {
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                    $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                    $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    $value = ["x1" => $x1, 
                                "x2" => $x2, 
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $words[] = $value;
                }

                foreach ($words as $key => $text) {
                    $pageKey = null;
                    $pageType = null; // 0 = financial position, 1 = income and expenses, 2 = changes in shareholders' equity, 3 = cash flows
                    
                    // financial position not yet detected
                    if ($financial_position == STS_NG) {
                        // continue if array key is in word count of phrase to be detected elements
                        if ($key == array_key_last($words)
                        or $key + 1 == array_key_last($words)
                        or $key + 2 == array_key_last($words)
                        or $key + 3 == array_key_last($words)) {
                        continue;
                        }

                        //check if page has statement of financial position label
                        if (strcasecmp("statement", $text['text']) == 0
                        and strcasecmp("of", $words[$key + 1]['text']) == 0
                        and strcasecmp("financial", $words[$key + 2]['text']) == 0
                        and strcasecmp("position", $words[$key + 3]['text']) == 0) {
                            $pageKey = $image_key;
                            $pageType = 0;
                        break;
                        }
                        continue;
                    }

                    // statement of income and expenses not yet detected
                    if ($income_and_expenses == STS_NG) {
                        // continue if array key is in  word count of phrase to be detected
                        if ($key == array_key_last($words)
                        or $key + 1 == array_key_last($words)
                        or $key + 2 == array_key_last($words)
                        or $key + 3 == array_key_last($words)
                        or $key + 4 == array_key_last($words)) {
                        continue;
                        }

                        //check if page has statement of income and expenses label
                        if (strcasecmp("statement", $text['text']) == 0
                        and strcasecmp("of", $words[$key + 1]['text']) == 0
                        and strcasecmp("income", $words[$key + 2]['text']) == 0
                        and strcasecmp("and", $words[$key + 3]['text']) == 0
                        and (strcasecmp("expenses", $words[$key + 4]['text']) == 0
                        or (substr(strtolower($words[$key + 4]['text']), 0, 8) == "expenses"))) {
                            $pageKey = $image_key;
                            $pageType = 1;
                        break;
                        }
                        continue;
                    }
                    
                    /*------------------------------------
                    --- commented out for later use-------
                    ------------------------------------- */
                    // statement of changes in shareholders' equity not yet detected
                    // if ($changes_shareholders_equity == STS_NG) {
                    //     // continue if array key is in  word count of phrase to be detected
                    //     if ($key == array_key_last($words)
                    //     or $key + 1 == array_key_last($words)
                    //     or $key + 2 == array_key_last($words)
                    //     or $key + 3 == array_key_last($words)
                    //     or $key + 4 == array_key_last($words)
                    //     or $key + 5 == array_key_last($words)) {
                    //     continue;
                    //     }

                    //     //check if page has statement of income and expenses label
                    //     if (strcasecmp("statement", $text['text']) == 0
                    //     and strcasecmp("of", $words[$key + 1]['text']) == 0
                    //     and strcasecmp("changes", $words[$key + 2]['text']) == 0
                    //     and strcasecmp("in", $words[$key + 3]['text']) == 0
                    //     and ((strcasecmp("shareholders\'", $words[$key + 4]['text']) == 0
                    //     and strcasecmp("equity", $words[$key + 5]['text']) == 0)
                    //     or strcasecmp("shareholders'equity", $words[$key + 4]['text']) == 0)) {
                    //         $pageKey = $image_key;
                    //         $pageType = 2;
                    //     break;
                    //     }
                    //     continue;
                    // }

                    // statement of cash flows not yet detected
                    if ($cash_flows == STS_NG) {
                        // continue if array key is in  word count of phrase to be detected
                        if ($key == array_key_last($words)
                        or $key + 1 == array_key_last($words)
                        or $key + 2 == array_key_last($words)
                        or $key + 3 == array_key_last($words)) {
                        continue;
                        }

                        //check if page has statement of income and expenses label
                        if (strcasecmp("statement", $text['text']) == 0
                        and strcasecmp("of", $words[$key + 1]['text']) == 0
                        and strcasecmp("cash", $words[$key + 2]['text']) == 0
                        and strcasecmp("flows", $words[$key + 3]['text']) == 0) {
                            $pageKey = $image_key;
                            $pageType = 3;
                        break;
                        }
                        continue;
                    }
                }

                // page is included in fs template
                if ($pageKey !== null) {
                    if ($pageType == 0){// if page is statement of financial position
                        // set Y vertices of row. Used for determining location of data
                        foreach($words as $key => $text) {
                            //primary year
                            if($text['text'] === "NOTES") {
                                    $notes_y1 = $text['y1'] - 30;
                                    $notes_y2 = $text['y2'] + 30;
                                    $notes_x2 = $text['x2'];
                                    $extended_notes_x2 = $notes_x2 + 400;
                            continue;
                            }

                            //cash
                            if (strcasecmp("cash", $text['text']) == 0) {
                                $cash_y1 = (int)$text['y1'] - 30;
                                $cash_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            //trade and other receivables
                            if(strcasecmp("trade", $text['text']) == 0
                            and strcasecmp("and", $words[$key + 1]['text']) == 0
                            and strcasecmp("other", $words[$key + 2]['text']) == 0
                            and strcasecmp("receivables", $words[$key + 3]['text']) == 0) {
                                $trade_and_receivables_y1 = (int)$text['y1'] - 30;
                                $trade_and_receivables_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            //other current assets
                            if(strcasecmp("other", $text['text']) == 0
                            and strcasecmp("current", $words[$key + 1]['text']) == 0
                            and strcasecmp("assets", $words[$key + 2]['text']) == 0
                            and strcasecmp("and", $words[$key - 1]['text']) != 0) {
                                $other_current_assets_y1 = (int)$text['y1'] - 30;
                                $other_current_assets_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            //total current assets
                            // if(strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("current", $words[$key + 1]['text']) == 0
                            // and strcasecmp("assets", $words[$key + 2]['text']) == 0) {
                            //     $total_current_assets_y1 = (int)$text['y1'] - 30;
                            //     $total_current_assets_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            //property and equipment
                            if(strcasecmp("property", $text['text']) == 0
                            and strcasecmp("and", $words[$key + 1]['text']) == 0
                            and strcasecmp("equipment", $words[$key + 2]['text']) == 0) {
                                $property_equipment_y1 = (int)$text['y1'] - 30;
                                $property_equipment_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            //total non-current assets
                            // if (strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("non-current", $words[$key + 1]['text']) == 0
                            // and strcasecmp("assets", $words[$key + 2]['text']) == 0) {
                            //     $total_noncurrent_assets_y1 = (int)$text['y1'] - 30;
                            //     $total_noncurrent_assets_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            //deposits
                            if(strcasecmp("deposits", $text['text']) == 0) {
                                $deposits_y1 = (int)$text['y1'] - 30;
                                $deposits_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            //total assets
                            // if(strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("assets", $words[$key + 1]['text']) == 0) {
                            //     $total_assets_y1 = (int)$text['y1'] - 30;
                            //     $total_assets_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            //accounts payable and accruals
                            if ((strcasecmp("accounts", $text['text']) == 0
                            and strcasecmp("payable", $words[$key + 1]['text']) == 0
                            and strcasecmp("and", $words[$key + 2]['text']) == 0
                            and strcasecmp("accruals", $words[$key + 3]['text']) == 0)
                            or (strcasecmp("short", $text['text']) == 0
                            and strcasecmp("term", $words[$key + 1]['text']) == 0
                            and strcasecmp("payables", $words[$key + 2]['text']) == 0)) {
                                $payable_and_accruals_y1 = (int)$text['y1'] - 30;
                                $payable_and_accruals_y2 = (int)$text['y2'] + 15;
                            }

                            // trade and other payables
                            if (strcasecmp("trade", $text['text']) == 0
                            and strcasecmp("and", $words[$key + 1]['text']) == 0
                            and strcasecmp("other", $words[$key + 2]['text']) == 0
                            and strcasecmp("payables", $words[$key + 3]['text']) == 0) {
                                $trade_and_payables_y1 = (int)$text['y1'] - 30;
                                $trade_and_payables_y2 = (int)$text['y2'] + 15;
                            }

                            // income tax payable
                            if (strcasecmp("income", $text['text']) == 0
                            and strcasecmp("tax", $words[$key + 1]['text']) == 0
                            and strcasecmp("payable", $words[$key + 2]['text']) ==0) {
                                $income_tax_payable_y1 = (int)$text['y1'] - 30;
                                $income_tax_payable_y2 = (int)$text['y2'] + 15;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // total current liabilities
                            // if (strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("current", $words[$key + 1]['text']) == 0
                            // and strcasecmp("liabilities", $words[$key + 2]['text']) == 0) {
                            //     $total_current_liabilities_y1 = (int)$text['y1'] - 30;
                            //     $total_current_liabilities_y2 = (int)$text['y2'] + 15;
                            // }

                            // share capital
                            if (strcasecmp("share", $text['text']) == 0
                            and strcasecmp("capital", $words[$key + 1]['text']) == 0) {
                                $share_capital_y1 = (int)$text['y1'] - 30;
                                $share_capital_y2 = (int)$text['y2'] + 15;
                            }

                            // retained earnings
                            if (strcasecmp("retained", $text['text']) == 0
                            and strcasecmp("earnings", $words[$key + 1]['text']) == 0) {
                                $retained_earnings_y1 = (int)$text['y1'] - 30;
                                $retained_earnings_y2 = (int)$text['y2'] + 20;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // total shareholders equity
                            // if (strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("shareholder's", $words[$key + 1]['text']) == 0
                            // and strcasecmp("equity", $words[$key + 2]['text']) == 0) {
                            //     $total_shareholders_equity_y1 = (int)$text['y1'] - 30;
                            //     $total_shareholders_equity_y2 = (int)$text['y2'] + 15;
                            // }

                            // total liabilities and shareholder's equity
                            // if (strcasecmp("total", $text['text']) == 0
                            // and strcasecmp("liabilities", $words[$key + 1]['text']) == 0
                            // and strcasecmp("and", $words[$key + 2]['text']) == 0) {
                            //     $total_liabilities_and_shareholders_equity_y1 = (int)$text['y1'] - 15;
                            //     $total_liabilities_and_shareholders_equity_y2 = (int)$text['y2'] + 80;
                            // }

                        }

                        // get location of primary and secondary year
                        foreach ($words as $key => $text) {
                            if ($text['y1'] >= $notes_y1
                            and $text['y2'] <= $notes_y2
                            and $text['x1'] > $notes_x2
                            and $text['x2'] < $extended_notes_x2) {
                                $primary_year = $text['text'];
                                $secondary_year = $primary_year - 1;
                                $primary_year_x1 = (int)$text['x1'] - 100;
                                $primary_year_x2 = (int)$text['x2'] + 135;
                            }
                        }

                        // get data
                        if ($primary_year != null) {
                            foreach ($words as $key => $text) {
                                //get cash
                                if (isset($cash_y1)) {
                                    // if text is inside cash row
                                    if ($text['y1'] >= $cash_y1 and $text['y2'] <= $cash_y2) {
                                        // if text is inside primary year column
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            // continue if primary year already has data
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($cash, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$cash] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                            // set data for primary year
                                            $cash_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$cash] = $cash_primary_year;
                                            continue;
                                        }
                                        //set data for secondary year
                                        if ($text['x1'] > $primary_year_x2) {
                                            $cash_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$cash] = $cash_secondary_year;
                                            continue;
                                        }
                                        // if no data for secondary year
                                        if ($cash_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$cash] = "";
                                        }
                                        // if no data for primary year
                                        if ($cash_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($cash, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$cash] = "";
                                        }
                                    }
                                }
                                
                                // if no cash detected
                                if (!isset($cash_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$cash] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($cash, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$cash] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$cash] = "";
                                }
                
                                //get trade and receivables
                                if (isset($trade_and_receivables_y1)) {
                                    if ($text['y1'] >= $trade_and_receivables_y1 and $text['y2'] <= $trade_and_receivables_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($tradAndRec, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$tradAndRec] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $trade_and_receivables_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$tradAndRec] = $trade_and_receivables_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $trade_and_receivables_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$tradAndRec] = $trade_and_receivables_secondary_year;
                                            continue;
                                        }
                    
                                        if ($trade_and_receivables_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$tradAndRec] = "";
                                        }
            
                                        if ($trade_and_receivables_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($tradAndRec, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$tradAndRec] = "";
                                        }
                                    }
                                }

                                // if no trade and receivables detected
                                if (!isset($trade_and_receivables_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$tradAndRec] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($tradAndRec, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$tradAndRec] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$tradAndRec] = "";
                                }
                
                                //get other current assets
                                if (isset($other_current_assets_y1)) {
                                    if ($text['y1'] >= $other_current_assets_y1 and $text['y2'] <= $other_current_assets_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($othCurAss, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$othCurAss] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $other_current_assets_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$othCurAss] = $other_current_assets_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $other_current_assets_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$othCurAss] = $other_current_assets_secondary_year;
                                            continue;
                                        }
                    
                                        if ($other_current_assets_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$othCurAss] = "";
                                        }

                                        if ($other_current_assets_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($othCurAss, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$othCurAss] = "";
                                        }
                                    }
                                }

                                // if no other current assets
                                if (!isset($other_current_assets_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$othCurAss] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($othCurAss, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$othCurAss] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$othCurAss] = "";
                                }
                                
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                //get total current assets
                                // if ($text['y1'] >= $total_current_assets_y1 and $text['y2'] <= $total_current_assets_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_current_assets_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Current Assets"] = $total_current_assets_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_current_assets_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Current Assets"] = $total_current_assets_secondary_year;
                                //     }

                                //     if ($total_current_assets_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Current Assets"] = "";
                                //     }
                
                                //     if ($total_current_assets_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Current Assets"] = "";
                                //     }
                                // }
                
                                // get property and equipment
                                if (isset($property_equipment_y1)) {
                                    if ($text['y1'] >= $property_equipment_y1 and $text['y2'] <= $property_equipment_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($propAndEqui, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$propAndEqui] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $property_equipment_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$propAndEqui] = $property_equipment_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $property_equipment_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$propAndEqui] = $property_equipment_secondary_year;
                                            continue;
                                        }
                    
                                        if ($property_equipment_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$propAndEqui] = "";
                                        }
            
                                        if ($property_equipment_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($propAndEqui, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$propAndEqui] = "";
                                        }
                                    }
                                }

                                // if no property and equipment
                                if (!isset($property_equipment_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$propAndEqui] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($propAndEqui, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$propAndEqui] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$propAndEqui] = "";
                                }
                                
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get total non-current assets
                                // if ($text['y1'] >= $total_noncurrent_assets_y1 and $text['y2'] <= $total_noncurrent_assets_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_noncurrent_assets_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Non-Current Assets"] = $total_noncurrent_assets_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_noncurrent_assets_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Non-Current Assets"] = $total_noncurrent_assets_secondary_year;
                                //     }

                                //     if ($total_noncurrent_assets_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Non-Current Assets"] = "";
                                //     }
                
                                //     if ($total_noncurrent_assets_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Non-Current Assets"] = "";
                                //     }
                                // }
                
                                // get deposits
                                if (isset($deposits_y1)) {
                                    if ($text['y1'] >= $deposits_y1 and $text['y2'] <= $deposits_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($deposits, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$deposits] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $deposits_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$deposits] = $deposits_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $deposits_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$deposits] = $deposits_secondary_year;
                                            continue;
                                        }
                    
                                        if ($deposits_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$deposits] = "";
                                        }
        
                                        if ($deposits_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($deposits, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$deposits] = "";
                                        }
                                    }
                                }

                                // if no deposits
                                if (!isset($deposits_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$deposits] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($deposits, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$deposits] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$deposits] = "";
                                }
                                
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get total_assets
                                // if ($text['y1'] >= $total_assets_y1 and $text['y2'] <= $total_assets_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_assets_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Assets"] = $total_assets_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_assets_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Assets"] = $total_assets_secondary_year;
                                //     }

                                //     if ($total_assets_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Assets"] = "";
                                //     }
                
                                //     if ($total_assets_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Assets"] = "";
                                //     }
                                // }
                
                                // get accounts payable and accruals
                                if (isset($payable_and_accruals_y1)) {
                                    if ($text['y1'] >= $payable_and_accruals_y1 and $text['y2'] <= $payable_and_accruals_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($AccPayAndAccru, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$AccPayAndAccru] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $payable_and_accruals_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$AccPayAndAccru] = $payable_and_accruals_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $payable_and_accruals_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$AccPayAndAccru] = $payable_and_accruals_secondary_year;
                                            continue;
                                        }
                    
                                        if ($payable_and_accruals_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$AccPayAndAccru] = "";
                                        }
        
                                        if ($payable_and_accruals_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($AccPayAndAccru, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$AccPayAndAccru] = "";
                                        }
                                    }
                                }

                                // if no accounts payable and accruals
                                if (!isset($payable_and_accruals_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$AccPayAndAccru] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($AccPayAndAccru, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$AccPayAndAccru] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$AccPayAndAccru] = "";
                                }
                
                                // get trade and other payables
                                if (isset($trade_and_payables_y1)) {
                                    if ($text['y1'] >= $trade_and_payables_y1 and $text['y2'] <= $trade_and_payables_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($tradAndOthPay, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$tradAndOthPay] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $trade_and_payables_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$tradAndOthPay] = $trade_and_payables_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $trade_and_payables_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$tradAndOthPay] = $trade_and_payables_secondary_year;
                                            continue;
                                        }
                    
                                        if ($trade_and_payables_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$tradAndOthPay] = "";
                                        }
        
                                        if ($trade_and_payables_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($tradAndOthPay, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$tradAndOthPay] = "";
                                        }
                                    }
                                }

                                // if no trade and other payables
                                if (!isset($trade_and_payables_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$tradAndOthPay] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($tradAndOthPay, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$tradAndOthPay] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$tradAndOthPay] = "";
                                }
                
                                // get income tax payable
                                if (isset($income_tax_payable_y1)) {
                                    if ($text['y1'] >= $income_tax_payable_y1 and $text['y2'] <= $income_tax_payable_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($incTaxPay, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$incTaxPay] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $income_tax_payable_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$incTaxPay] = $income_tax_payable_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $income_tax_payable_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$incTaxPay] = $income_tax_payable_secondary_year;
                                            continue;
                                        }
                    
                                        if ($income_tax_payable_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$incTaxPay] = "";
                                        }
        
                                        if ($income_tax_payable_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($incTaxPay, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$incTaxPay] = "";
                                        }
                                    }
                                }

                                // if no income tax payable
                                if (!isset($income_tax_payable_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$incTaxPay] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($incTaxPay, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$incTaxPay] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$incTaxPay] = "";
                                }
                                
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get total current liabilities
                                // if ($text['y1'] >= $total_current_liabilities_y1 and $text['y2'] <= $total_current_liabilities_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_current_liabilities_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Current Liabilities"] = $total_current_liabilities_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_current_liabilities_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Current Liabilities"] = $total_current_liabilities_secondary_year;
                                //     }

                                //     if ($total_current_liabilities_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Current Liabilities"] = "";
                                //     }
                
                                //     if ($total_current_liabilities_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Current Liabilities"] = "";
                                //     }
                                // }
                
                                // get share capital
                                if (isset($share_capital_y1)) {
                                    if ($text['y1'] >= $share_capital_y1 and $text['y2'] <= $share_capital_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($shareCap, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$shareCap] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $share_capital_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$shareCap] = $share_capital_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $share_capital_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$shareCap] = $share_capital_secondary_year;
                                            continue;
                                        }
                    
                                        if ($share_capital_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$shareCap] = "";
                                        }
        
                                        if ($share_capital_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($shareCap, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$shareCap] = "";
                                        }
                                    }
                                }

                                // if no share capital
                                if (!isset($share_capital_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$shareCap] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($shareCap, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$shareCap] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$shareCap] = "";
                                }
                
                                // get retained earnings
                                if (isset($retained_earnings_y1)) {
                                    if ($text['y1'] >= $retained_earnings_y1 and $text['y2'] <= $retained_earnings_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($retEarnings, $fs_data[$finPstn][$primary_year])) {
                                                        if ($fs_data[$finPstn][$primary_year][$retEarnings] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            $retained_earnings_primary_year = $text['text'];
                                            $fs_data[$finPstn][$primary_year][$retEarnings] = $retained_earnings_primary_year;
                                            continue;
                                        }
                    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $retained_earnings_secondary_year = $text['text'];
                                            $fs_data[$finPstn][$secondary_year][$retEarnings] = $retained_earnings_secondary_year;
                                            continue;
                                        }
                    
                                        if ($retained_earnings_secondary_year == STR_EMPTY) {
                                            $fs_data[$finPstn][$secondary_year][$retEarnings] = "";
                                        }
        
                                        if ($retained_earnings_primary_year == STR_EMPTY) {
                                            if (array_key_exists($finPstn, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                                    if (array_key_exists($retEarnings, $fs_data[$finPstn][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$finPstn][$primary_year][$retEarnings] = "";
                                        }
                                    }
                                }

                                // if no retained earnings
                                if (!isset($retained_earnings_y1)) {
                                    $fs_data[$finPstn][$secondary_year][$retEarnings] = "";
                                    if (array_key_exists($finPstn, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$finPstn])) {
                                            if (array_key_exists($retEarnings, $fs_data[$finPstn][$primary_year])) {
                                                if ($fs_data[$finPstn][$primary_year][$retEarnings] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$finPstn][$primary_year][$retEarnings] = "";
                                }
                                
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get total shareholders equity
                                // if ($text['y1'] >= $total_shareholders_equity_y1 and $text['y2'] <= $total_shareholders_equity_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_shareholders_equity_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Shareholders Equity"] = $total_shareholders_equity_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_shareholders_equity_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Shareholders Equity"] = $total_shareholders_equity_secondary_year;
                                //     }

                                //     if ($total_shareholders_equity_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Shareholders Equity"] = "";
                                //     }
                
                                //     if ($total_shareholders_equity_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Shareholders Equity"] = "";
                                //     }
                                // }
                
                                // get total liabilities and shareholders equity
                                // if ($text['y1'] >= $total_liabilities_and_shareholders_equity_y1 and $text['y2'] <= $total_liabilities_and_shareholders_equity_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $total_liabilities_and_shareholders_equity_primary_year = $text['text'];
                                //         $fs_data[$finPstn][$primary_year]["Total Liabilities and Shareholders Equity"] = $total_liabilities_and_shareholders_equity_primary_year;
                                //         continue;
                                //     }
                
                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $total_liabilities_and_shareholders_equity_secondary_year = $text['text'];
                                //         $fs_data[$finPstn][$secondary_year]["Total Liabilities and Shareholders Equity"] = $total_liabilities_and_shareholders_equity_secondary_year;
                
                                //     }

                                //     if ($total_liabilities_and_shareholders_equity_primary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$primary_year]["Total Liabilities and Shareholders Equity"] = "";
                                //     }
                
                                //     if ($total_liabilities_and_shareholders_equity_secondary_year == STR_EMPTY) {
                                //         $fs_data[$finPstn][$secondary_year]["Total Liabilities and Shareholders Equity"] = "";
                                //     }
                                // }
                            }
                        }

                        $print[] = "Statement of financial position|| ".$image_key;
                        $financial_position = STS_OK;
                    }
                    
                    if ($pageType == 1) { // if page is statement of income and expenses
                        // set Y vertices of row. Used for determining location of data
                        foreach($words as $key => $text) {
                            //primary year
                            if($text['text'] === "NOTES") {
                                    $notes_y1 = $text['y1'] - 30;
                                    $notes_y2 = $text['y2'] + 30;
                                    $notes_x2 = $text['x2'];
                                    $extended_notes_x2 = $notes_x2 + 400;
                            continue;
                            }

                            // revenue
                            if (strcasecmp("revenue", $text['text']) == 0
                            or (
                                strcasecmp("sales", $text['text']) == 0
                                and strcasecmp("of", $words[$key - 1]['text']) != 0
                            )) {
                                $revenue_y1 = (int)$text['y1'] - 30;
                                $revenue_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            // cost of projects
                            if(strcasecmp("cost", $text['text']) == 0
                            and strcasecmp("of", $words[$key + 1]['text']) == 0
                            and (
                                strcasecmp("projects", $words[$key + 2]['text']) == 0
                                or strcasecmp("sales", $words[$key + 2]['text']) == 0
                            )) {
                                $cost_of_projects_y1 = (int)$text['y1'] - 30;
                                $cost_of_projects_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // gross profit
                            // if(strcasecmp("gross", $text['text']) == 0
                            // and strcasecmp("profit", $words[$key + 1]['text']) == 0) {
                            //     $gross_profit_y1 = (int)$text['y1'] - 30;
                            //     $gross_profit_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // selling and administrative expenses
                            if(strcasecmp("administrative", $text['text']) == 0
                            and strcasecmp("expenses", $words[$key + 1]['text']) == 0
                            and substr(strtolower($words[$key - 3]['text']), 0, 4) != "less") {
                                $selling_administrative_expenses_y1 = (int)$text['y1'] - 30;
                                $selling_administrative_expenses_y2 = (int)$text['y2'] + 110;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // net income before tax
                            // if(strcasecmp("net", $text['text']) == 0
                            // and strcasecmp("income", $words[$key + 1]['text']) == 0
                            // and strcasecmp("before", $words[$key + 2]['text']) == 0
                            // and strcasecmp("tax", $words[$key + 3]['text']) == 0) {
                            //     $net_income_before_tax_y1 = (int)$text['y1'] - 30;
                            //     $net_income_before_tax_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // provision for income tax
                            if(strcasecmp("provision", $text['text']) == 0
                            and strcasecmp("for", $words[$key + 1]['text']) == 0
                            and strcasecmp("income", $words[$key + 2]['text']) == 0
                            and strcasecmp("tax", $words[$key + 3]['text']) == 0
                            or (
                                substr(strtolower($text['text']), 0, 4) == "less"
                                and strcasecmp("income", $words[$key + 1]['text']) == 0
                                and strcasecmp("tax", $words[$key + 2]['text']) == 0
                            )) {
                                $provision_for_income_tax_y1 = (int)$text['y1'] - 30;
                                $provision_for_income_tax_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // income tax for the period
                            // if(strcasecmp("income", $text['text']) == 0
                            // and strcasecmp("tax", $words[$key + 1]['text']) == 0
                            // and strcasecmp("for", $words[$key + 2]['text']) == 0
                            // and strcasecmp("the", $words[$key + 3]['text']) == 0
                            // and strcasecmp("period", $words[$key + 4]['text']) == 0) {
                            //     $income_tax_for_period_y1 = (int)$text['y1'] - 30;
                            //     $income_tax_for_period_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }
                        }

                        // get location of primary and secondary year
                        foreach ($words as $key => $text) {
                            if ($text['y1'] >= $notes_y1
                            and $text['y2'] <= $notes_y2
                            and $text['x1'] > $notes_x2
                            and $text['x2'] < $extended_notes_x2) {
                                $primary_year = $text['text'];
                                $secondary_year = $primary_year - 1;
                                $primary_year_x1 = (int)$text['x1'] - 100;
                                $primary_year_x2 = (int)$text['x2'] + 135;
                            }
                        }

                        if ($primary_year != null) {
                            foreach ($words as $key => $text) {
                                // get revenue
                                if (isset($revenue_y1)) {
                                    if ($text['y1'] >= $revenue_y1 and $text['y2'] <= $revenue_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($revenue, $fs_data[$incAndExp][$primary_year])) {
                                                        if ($fs_data[$incAndExp][$primary_year][$revenue] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            $revenue_primary_year = $text['text'];
                                            $fs_data[$incAndExp][$primary_year][$revenue] = $revenue_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $revenue_secondary_year = $text['text'];
                                            $fs_data[$incAndExp][$secondary_year][$revenue] = $revenue_secondary_year;
                                            continue;
                                        }
    
                                        if ($revenue_secondary_year == STR_EMPTY) {
                                            $fs_data[$incAndExp][$secondary_year][$revenue] = "";
                                        }
        
                                        if ($revenue_primary_year == STR_EMPTY) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($revenue, $fs_data[$incAndExp][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$incAndExp][$primary_year][$revenue] = "";
                                        }
                                    }
                                }

                                // if no revenue
                                if (!isset($revenue_y1)) {
                                    $fs_data[$incAndExp][$secondary_year][$revenue] = "";
                                    if (array_key_exists($incAndExp, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                            if (array_key_exists($revenue, $fs_data[$incAndExp][$primary_year])) {
                                                if ($fs_data[$incAndExp][$primary_year][$revenue] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$incAndExp][$primary_year][$revenue] = "";
                                }

                                // cost of projects
                                if (isset($cost_of_projects_y1)) {
                                    if ($text['y1'] >= $cost_of_projects_y1 and $text['y2'] <= $cost_of_projects_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($costOfProj, $fs_data[$incAndExp][$primary_year])) {
                                                        if ($fs_data[$incAndExp][$primary_year][$costOfProj] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $cost_of_projects_primary_year = $text['text'];
                                            $fs_data[$incAndExp][$primary_year][$costOfProj] = $cost_of_projects_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $cost_of_projects_secondary_year = $text['text'];
                                            $fs_data[$incAndExp][$secondary_year][$costOfProj] = $cost_of_projects_secondary_year;
                                            continue;
                                        }
    
                                        if ($cost_of_projects_secondary_year == STR_EMPTY) {
                                            $fs_data[$incAndExp][$secondary_year][$costOfProj] = "";
                                        }
        
                                        if ($cost_of_projects_primary_year == STR_EMPTY) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($costOfProj, $fs_data[$incAndExp][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$incAndExp][$primary_year][$costOfProj] = "";
                                        }
                                    }
                                }

                                // if no cost of projects
                                if (!isset($cost_of_projects_y1)) {
                                    $fs_data[$incAndExp][$secondary_year][$costOfProj] = "";
                                    if (array_key_exists($incAndExp, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                            if (array_key_exists($costOfProj, $fs_data[$incAndExp][$primary_year])) {
                                                if ($fs_data[$incAndExp][$primary_year][$costOfProj] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$incAndExp][$primary_year][$costOfProj] = "";
                                }

                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get gross profit
                                // if ($text['y1'] >= $gross_profit_y1 and $text['y2'] <= $gross_profit_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $gross_profit_primary_year = $text['text'];
                                //         $fs_data[$incAndExp][$primary_year]["Gross Profit"] = $gross_profit_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $gross_profit_secondary_year = $text['text'];
                                //         $fs_data[$incAndExp][$secondary_year]["Gross Profit"] = $gross_profit_secondary_year;
                                //     }

                                //     if ($gross_profit_primary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$primary_year]["Gross Profit"] = "";
                                //     }

                                //     if ($gross_profit_secondary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$secondary_year]["Gross Profit"] = "";
                                //     }
                                // }

                                // get selling and administrative expenses
                                if (isset($selling_administrative_expenses_y1)) {
                                    if ($text['y1'] >= $selling_administrative_expenses_y1 and $text['y2'] <= $selling_administrative_expenses_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($sellAndAdmExp, $fs_data[$incAndExp][$primary_year])) {
                                                        if ($fs_data[$incAndExp][$primary_year][$sellAndAdmExp] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $selling_administrative_expenses_primary_year = $text['text'];
                                            $fs_data[$incAndExp][$primary_year][$sellAndAdmExp] = $selling_administrative_expenses_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $selling_administrative_expenses_secondary_year = $text['text'];
                                            $fs_data[$incAndExp][$secondary_year][$sellAndAdmExp] = $selling_administrative_expenses_secondary_year;
                                            continue;
                                        }
    
                                        if ($selling_administrative_expenses_secondary_year == STR_EMPTY) {
                                            $fs_data[$incAndExp][$secondary_year][$sellAndAdmExp] = "";
                                        }
        
                                        if ($selling_administrative_expenses_primary_year == STR_EMPTY) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($sellAndAdmExp, $fs_data[$incAndExp][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$incAndExp][$primary_year][$sellAndAdmExp] = "";
                                        }
                                    }
                                }

                                // if no selling and administrative expenses
                                if (!isset($selling_administrative_expenses_y1)) {
                                    $fs_data[$incAndExp][$secondary_year][$sellAndAdmExp] = "";
                                    if (array_key_exists($incAndExp, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                            if (array_key_exists($sellAndAdmExp, $fs_data[$incAndExp][$primary_year])) {
                                                if ($fs_data[$incAndExp][$primary_year][$sellAndAdmExp] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$incAndExp][$primary_year][$sellAndAdmExp] = "";
                                }

                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get net income before tax
                                // if ($text['y1'] >= $net_income_before_tax_y1 and $text['y2'] <= $net_income_before_tax_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $net_income_before_tax_primary_year = $text['text'];
                                //         $fs_data[$incAndExp][$primary_year]["Net Income before Tax"] = $net_income_before_tax_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $net_income_before_tax_secondary_year = $text['text'];
                                //         $fs_data[$incAndExp][$secondary_year]["Net Income before Tax"] = $net_income_before_tax_secondary_year;
                                //     }

                                //     if ($net_income_before_tax_primary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$primary_year]["Net Income before Tax"] = "";
                                //     }

                                //     if ($net_income_before_tax_secondary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$secondary_year]["Net Income before Tax"] = "";
                                //     }
                                // }

                                // get total provision for income tax
                                if (isset($provision_for_income_tax_y1)) {
                                    if ($text['y1'] >= $provision_for_income_tax_y1 and $text['y2'] <= $provision_for_income_tax_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($provForIncTax, $fs_data[$incAndExp][$primary_year])) {
                                                        if ($fs_data[$incAndExp][$primary_year][$provForIncTax] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            $provision_for_income_tax_primary_year = $text['text'];
                                            $fs_data[$incAndExp][$primary_year][$provForIncTax] = $provision_for_income_tax_primary_year;
                                            continue;
                                        }
                                        if ($text['x1'] > $primary_year_x2) {
                                            $provision_for_income_tax_secondary_year = $text['text'];
                                            $fs_data[$incAndExp][$secondary_year][$provForIncTax] = $provision_for_income_tax_secondary_year;
                                            continue;
                                        }
    
                                        if ($provision_for_income_tax_secondary_year == STR_EMPTY) {
                                            $fs_data[$incAndExp][$secondary_year][$provForIncTax] = "";
                                        }
        
                                        if ($provision_for_income_tax_primary_year == STR_EMPTY) {
                                            if (array_key_exists($incAndExp, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                                    if (array_key_exists($provForIncTax, $fs_data[$incAndExp][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$incAndExp][$primary_year][$provForIncTax] = "";
                                        }
                                    }
                                }

                                // if no total provision for income tax
                                if (!isset($provision_for_income_tax_y1)) {
                                    $fs_data[$incAndExp][$secondary_year][$provForIncTax] = "";
                                    if (array_key_exists($incAndExp, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$incAndExp])) {
                                            if (array_key_exists($provForIncTax, $fs_data[$incAndExp][$primary_year])) {
                                                if ($fs_data[$incAndExp][$primary_year][$provForIncTax] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$incAndExp][$primary_year][$provForIncTax] = "";
                                }

                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get income tax for the period
                                // if ($text['y1'] >= $income_tax_for_period_y1 and $text['y2'] <= $income_tax_for_period_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $income_tax_for_period_primary_year = $text['text'];
                                //         $fs_data[$incAndExp][$primary_year]["Income tax for the period"] = $income_tax_for_period_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $income_tax_for_period_secondary_year = $text['text'];
                                //         $fs_data[$incAndExp][$secondary_year]["Income tax for the period"] = $income_tax_for_period_secondary_year;
                                //     }

                                //     if ($income_tax_for_period_primary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$primary_year]["Income tax for the period"] = "";
                                //     }

                                //     if ($income_tax_for_period_secondary_year == STR_EMPTY) {
                                //         $fs_data[$incAndExp][$secondary_year]["Income tax for the period"] = "";
                                //     }
                                // }
                            }
                        }

                        $print[] = "Statement of income and expenses detected || ".$image_key;
                        $income_and_expenses = STS_OK;
                    }
                    
                    /*------------------------------------
                    --- commented out for later use-------
                    ------------------------------------- */
                    // if ($pageType == 2) { // if page is statement of changes in shareholders equity
                    //     // set Y vertices of row. Used for determining location of data
                    //     $is_balance = STS_NG;
                    //     foreach($words as $key => $text) {
                    //         // primary year and share capital
                    //         if(strcasecmp("share", $text['text']) == 0
                    //         and strcasecmp("capital", $words[$key + 1]['text']) == 0) {
                    //                 //primary year
                    //                 $sharecapital_y1 = $text['y1'] - 135;
                    //                 $sharecapital_y2 = $text['y1'] - 20;
                    //                 $sharecapital_x2 = $text['x2'];
                    //                 $extended_sharecapital_x2 = $sharecapital_x2 + 1250;

                    //                 //share capital
                    //                 $share_capital_y1 = (int)$text['y1'] - 30;
                    //                 $share_capital_y2 = (int)$text['y2'] + 100;
                    //         continue;
                    //         }
                            
                    //         // balance
                    //         if (!$is_balance) {
                    //             if(strcasecmp("balance", $text['text']) == 0) {
                    //                 $balance_y1 = (int)$text['y1'] - 30;
                    //                 $balance_y2 = (int)$text['y2'] + 15;
                    //                 $is_balance = STS_OK;
                    //             continue;
                    //             }
                    //         }

                    //         // balance beginning
                    //         if (strcasecmp("balance,", $text['text']) == 0
                    //         and strcasecmp("beginning", $words[$key + 1]['text']) == 0) {
                    //             $balance_beginning_y1 = (int)$text['y1'] - 30;
                    //             $balance_beginning_y2 = (int)$text['y2'] + 15;
                    //         continue;
                    //         }

                    //         // net income for the period
                    //         if (strcasecmp("net", $text['text']) == 0
                    //         and strcasecmp("income", $words[$key + 1]['text']) == 0
                    //         and strcasecmp("for", $words[$key + 2]['text']) == 0
                    //         and strcasecmp("the", $words[$key + 3]['text']) == 0
                    //         and strcasecmp("period", $words[$key + 4]['text']) == 0) {
                    //             $net_income_period_y1 = (int)$text['y1'] - 30;
                    //             $net_income_period_y2 = (int)$text['y2'] + 15;
                    //         continue;
                    //         }

                    //         // balance ending
                    //         if (strcasecmp("balance", $text['text']) == 0
                    //         and strcasecmp("ending", $words[$key + 1]['text']) == 0) {
                    //             $balance_ending_y1 = (int)$text['y1'] - 30;
                    //             $balance_ending_y2 = (int)$text['y2'] + 15;
                    //         continue;
                    //         }

                    //         // total shareholders equity
                    //         if (strcasecmp("total", $text['text']) == 0
                    //         and strcasecmp("shareholders'", $words[$key + 1]['text']) == 0
                    //         and strcasecmp("equity", $words[$key + 2]['text']) == 0) {
                    //             $total_shareholders_equity_y1 = (int)$text['y1'] - 30;
                    //             $total_shareholders_equity_y2 = (int)$text['y2'] + 15;
                    //         continue;
                    //         }
                    //     }

                    //     //' get location of primary and secondary year
                    //     foreach ($words as $key => $text) {
                    //         if ($text['y1'] >= $sharecapital_y1
                    //         and $text['y2'] <= $sharecapital_y2
                    //         and $text['x1'] > $sharecapital_x2
                    //         and $text['x2'] < $extended_sharecapital_x2) {
                    //             $primary_year = $text['text'];
                    //             $secondary_year = $primary_year - 1;
                    //             $primary_year_x1 = (int)$text['x1'] - 100;
                    //             $primary_year_x2 = (int)$text['x2'] + 135;
                    //         }
                    //     }
                        
                    //     if ($primary_year != null) {
                    //         foreach ($words as $key => $text) {
                    //             // get share capital
                    //             if ($text['y1'] >= $share_capital_y1 and $text['y2'] <= $share_capital_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $share_capital_se_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["share capital"] = $share_capital_se_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $share_capital_se_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["share capital"] = $share_capital_se_secondary_year;
                    //                 }

                    //                 if ($share_capital_se_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["share capital"] = "";
                    //                 }

                    //                 if ($share_capital_se_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["share capital"] = "";
                    //                 }
                    //             }

                    //             // get balance
                    //             if ($text['y1'] >= $balance_y1 and $text['y2'] <= $balance_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $balance_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["balance"] = $balance_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $balance_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["balance"] = $balance_secondary_year;
                    //                 }

                    //                 if ($balance_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["balance"] = "";
                    //                 }

                    //                 if ($balance_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["balance"] = "";
                    //                 }
                    //             }

                    //             // get balance beginning
                    //             if ($text['y1'] >= $balance_beginning_y1 and $text['y2'] <= $balance_beginning_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $balance_beginning_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Balance Beginning"] = $balance_beginning_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $balance_beginning_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Balance Beginning"] = $balance_beginning_secondary_year;
                    //                 }

                    //                 if ($balance_beginning_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Balance Beginning"] = "";
                    //                 }

                    //                 if ($balance_beginning_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Balance Beginning"] = "";
                    //                 }
                    //             }

                    //             // get net income for the period
                    //             if ($text['y1'] >= $net_income_period_y1 and $text['y2'] <= $net_income_period_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $net_income_period_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Net income for the period"] = $net_income_period_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $net_income_period_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Net income for the period"] = $net_income_period_secondary_year;
                    //                 }

                    //                 if ($net_income_period_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Net income for the period"] = "";
                    //                 }

                    //                 if ($net_income_period_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Net income for the period"] = "";
                    //                 }
                    //             }

                    //             // get balance ending
                    //             if ($text['y1'] >= $balance_ending_y1 and $text['y2'] <= $balance_ending_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $balance_ending_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Balance ending"] = $balance_ending_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $balance_ending_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Balance ending"] = $balance_ending_secondary_year;
                    //                 }

                    //                 if ($balance_ending_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Balance ending"] = "";
                    //                 }

                    //                 if ($balance_ending_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Balance ending"] = "";
                    //                 }
                    //             }

                    //             // get total shareholders' equity
                    //             if ($text['y1'] >= $total_shareholders_equity_y1 and $text['y2'] <= $total_shareholders_equity_y2) {
                    //                 if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                    //                     $total_shareholders_equity_se_primary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Total Shareholders equity"] = $total_shareholders_equity_se_primary_year;
                    //                     continue;
                    //                 }

                    //                 if ($text['x1'] > $primary_year_x2) {
                    //                     $total_shareholders_equity_se_secondary_year = $text['text'];
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Total Shareholders equity"] = $total_shareholders_equity_se_secondary_year;
                    //                 }

                    //                 if ($total_shareholders_equity_se_primary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$primary_year]["Total Shareholders equity"] = "";
                    //                 }

                    //                 if ($total_shareholders_equity_se_secondary_year == STR_EMPTY) {
                    //                     $fs_data["Statement of Changes in Shareholders Equity"][$secondary_year]["Total Shareholders equity"] = "";
                    //                 }
                    //             }
                    //         }
                    //     }
                    //     $print[] = "Statement of changes in shareholders' equity || ".$image_key;
                    //     $changes_shareholders_equity = STS_OK;
                    // }
                    
                    if ($pageType == 3) { // if page is statement of cash flows
                        // set Y vertices of row. Used for determining location of data
                        foreach($words as $key => $text) {
                            // primary year
                            if($text['text'] == "NOTES") {
                                    $notes_y1 = $text['y1'] - 30;
                                    $notes_y2 = $text['y2'] + 30;
                                    $notes_x2 = $text['x2'];
                                    $extended_notes_x2 = $notes_x2 + 400;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // net income
                            // if (strcasecmp("net", $text['text']) == 0
                            // and strcasecmp("income", $words[$key + 1]['text']) == 0) {
                            //     $net_income_y1 = (int)$text['y1'] - 30;
                            //     $net_income_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // depreciation and amortization
                            if(strcasecmp("depreciation", $text['text']) == 0
                            and strcasecmp("and", $words[$key + 1]['text']) == 0
                            and strcasecmp("amortization", $words[$key + 2]['text']) == 0) {
                                $depreciation_and_amortization_y1 = (int)$text['y1'] - 30;
                                $depreciation_and_amortization_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // income before working capital
                            // if(strcasecmp("income", $text['text']) == 0
                            // and strcasecmp("before", $words[$key + 1]['text']) == 0
                            // and strcasecmp("working", $words[$key + 2]['text']) == 0
                            // and strcasecmp("capital", $words[$key + 3]['text']) == 0) {
                            //     $income_before_capital_y1 = (int)$text['y1'] - 30;
                            //     $income_before_capital_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // accounts receivable
                            // if(strcasecmp("accounts", $text['text']) == 0
                            // and strcasecmp("receivable", $words[$key + 1]['text']) == 0) {
                            //     $accounts_receivable_y1 = (int)$text['y1'] - 30;
                            //     $accounts_receivable_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // inventories
                            // if(strcasecmp("inventories", $text['text']) == 0) {
                            //     $inventories_y1 = (int)$text['y1'] - 30;
                            //     $inventories_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // other current assets
                            // if(strcasecmp("other", $text['text']) == 0
                            // and strcasecmp("current", $words[$key + 1]['text']) == 0
                            // and strcasecmp("assets", $words[$key + 2]['text']) == 0) {
                            //     $other_current_assets_y1 = (int)$text['y1'] - 30;
                            //     $other_current_assets_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // accounts payable / accrued expenses
                            // if(strcasecmp("accounts", $text['text']) == 0
                            // and strcasecmp("payable/accrued", $words[$key + 1]['text']) == 0
                            // and strcasecmp("expenses", $words[$key + 2]['text']) == 0) {
                            //     $accounts_payable_accrued_expenses_y1 = (int)$text['y1'] - 30;
                            //     $accounts_payable_accrued_expenses_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // non-trade payable
                            if(strcasecmp("non-trade", $text['text']) == 0
                            and strcasecmp("payable", $words[$key + 1]['text']) == 0) {
                                $nontrade_payable_y1 = (int)$text['y1'] - 30;
                                $nontrade_payable_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            // net cash generated from operation
                            if(strcasecmp("net", $text['text']) == 0
                            and strcasecmp("cash", $words[$key + 1]['text']) == 0
                            and strcasecmp("generated", $words[$key + 2]['text']) == 0
                            and strcasecmp("from", $words[$key + 3]['text']) == 0
                            and strcasecmp("operation", $words[$key + 4]['text']) == 0) {
                                $net_cash_generated_from_operation_y1 = (int)$text['y1'] - 30;
                                $net_cash_generated_from_operation_y2 = (int)$text['y2'] + 15;
                            continue;
                            }

                            /*------------------------------------
                            --- commented out for later use-------
                            ------------------------------------- */
                            // acquisition of propoerties and equipment
                            // if(strcasecmp("acquisition", $text['text']) == 0
                            // and strcasecmp("of", $words[$key + 1]['text']) == 0
                            // and strcasecmp("properties", $words[$key + 2]['text']) == 0
                            // and strcasecmp("and", $words[$key + 3]['text']) == 0
                            // and strcasecmp("equipment", $words[$key + 4]['text']) == 0) {
                            //     $acquisition_properties_equipment_y1 = (int)$text['y1'] - 30;
                            //     $acquisition_properties_equipment_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // net cash provided by inventory activities
                            // if(strcasecmp("net", $text['text']) == 0
                            // and strcasecmp("cash", $words[$key + 1]['text']) == 0
                            // and strcasecmp("provided", $words[$key + 2]['text']) == 0
                            // and strcasecmp("by", $words[$key + 3]['text']) == 0
                            // and strcasecmp("inventory", $words[$key + 4]['text']) == 0
                            // and strcasecmp("activities", $words[$key + 5]['text']) == 0) {
                            //     $net_cash_inventory_activities_y1 = (int)$text['y1'] - 30;
                            //     $net_cash_inventory_activities_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // initial / additional capital
                            // if(strcasecmp("initial", $text['text']) == 0
                            // and strcasecmp("/", $words[$key + 1]['text']) == 0
                            // and strcasecmp("additional", $words[$key + 2]['text']) == 0
                            // and strcasecmp("capital", $words[$key + 3]['text']) == 0) {
                            //     $initial_additional_capital_y1 = (int)$text['y1'] - 30;
                            //     $initial_additional_capital_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // net cash provided by financing activities
                            // if(strcasecmp("net", $text['text']) == 0
                            // and strcasecmp("cash", $words[$key + 1]['text']) == 0
                            // and strcasecmp("provided", $words[$key + 2]['text']) == 0
                            // and strcasecmp("by", $words[$key + 3]['text']) == 0
                            // and strcasecmp("financing", $words[$key + 4]['text']) == 0
                            // and strcasecmp("activities", $words[$key + 5]['text']) == 0) {
                            //     $net_cash_financing_activities_y1 = (int)$text['y1'] - 30;
                            //     $net_cash_financing_activities_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // net increase (decrese) in cash
                            // if(strcasecmp("net", $text['text']) == 0
                            // and strcasecmp("increase", $words[$key + 1]['text']) == 0
                            // and strcasecmp("(decrease)", $words[$key + 2]['text']) == 0
                            // and strcasecmp("in", $words[$key + 3]['text']) == 0
                            // and strcasecmp("cash", $words[$key + 4]['text']) == 0) {
                            //     $net_increase_decrease_cash_y1 = (int)$text['y1'] - 30;
                            //     $net_increase_decrease_cash_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // cash beginning of the year
                            // if(strcasecmp("cash", $text['text']) == 0
                            // and strcasecmp("beginning", $words[$key + 1]['text']) == 0
                            // and strcasecmp("of", $words[$key + 2]['text']) == 0
                            // and strcasecmp("the", $words[$key + 3]['text']) == 0
                            // and strcasecmp("year", $words[$key + 4]['text']) == 0) {
                            //     $cash_beginning_year_y1 = (int)$text['y1'] - 30;
                            //     $cash_beginning_year_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }

                            // cash at the end of the year
                            // if(strcasecmp("cash", $text['text']) == 0
                            // and strcasecmp("at", $words[$key + 1]['text']) == 0
                            // and strcasecmp("the", $words[$key + 2]['text']) == 0
                            // and strcasecmp("end", $words[$key + 3]['text']) == 0
                            // and strcasecmp("of", $words[$key + 4]['text']) == 0
                            // and strcasecmp("the", $words[$key + 5]['text']) == 0
                            // and strcasecmp("year", $words[$key + 6]['text']) == 0) {
                            //     $cash_end_year_y1 = (int)$text['y1'] - 30;
                            //     $cash_end_year_y2 = (int)$text['y2'] + 15;
                            // continue;
                            // }
                        }

                        // get location of primary and secondary year
                        foreach ($words as $key => $text) {
                            if ($text['y1'] >= $notes_y1
                            and $text['y2'] <= $notes_y2
                            and $text['x1'] > $notes_x2
                            and $text['x2'] < $extended_notes_x2) {
                                $primary_year = $text['text'];
                                $secondary_year = $primary_year - 1;
                                $primary_year_x1 = (int)$text['x1'] - 100;
                                $primary_year_x2 = (int)$text['x2'] + 135;
                            }
                        }

                        if ($primary_year != null) {
                            foreach ($words as $key => $text) {
                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get net income
                                // if ($text['y1'] >= $net_income_y1 and $text['y2'] <= $net_income_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $net_income_primary_year = $text['text'];
                                //         $fs_data["Statement of Cash Flows"][$primary_year]["net income"] = $net_income_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $net_income_secondary_year = $text['text'];
                                //         $fs_data["Statement of Cash Flows"][$secondary_year]["net income"] = $net_income_secondary_year;
                                //     }

                                //     if ($net_income_primary_year == STR_EMPTY) {
                                //         $fs_data["Statement of Cash Flows"][$primary_year]["net income"] = "";
                                //     }

                                //     if ($net_income_secondary_year == STR_EMPTY) {
                                //         $fs_data["Statement of Cash Flows"][$secondary_year]["net income"] = "";
                                //     }
                                // }

                                // get depreciation and amortization
                                if (isset($depreciation_and_amortization_y1)) {
                                    if ($text['y1'] >= $depreciation_and_amortization_y1 and $text['y2'] <= $depreciation_and_amortization_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($depandAmor, $fs_data[$CashFlows][$primary_year])) {
                                                        if ($fs_data[$CashFlows][$primary_year][$depandAmor] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $depreciation_and_amortization_primary_year = $text['text'];
                                            $fs_data[$CashFlows][$primary_year][$depandAmor] = $depreciation_and_amortization_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $depreciation_and_amortization_secondary_year = $text['text'];
                                            $fs_data[$CashFlows][$secondary_year][$depandAmor] = $depreciation_and_amortization_secondary_year;
                                            continue;
                                        }
    
                                        if ($depreciation_and_amortization_secondary_year == STR_EMPTY) {
                                            $fs_data[$CashFlows][$secondary_year][$depandAmor] = "";
                                        }
        
                                        if ($depreciation_and_amortization_primary_year == STR_EMPTY) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($depandAmor, $fs_data[$CashFlows][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$CashFlows][$primary_year][$depandAmor] = "";
                                        }
                                    }
                                }

                                // if no depreciation and amortization
                                if (!isset($depreciation_and_amortization_y1)) {
                                    $fs_data[$CashFlows][$secondary_year][$depandAmor] = "";
                                    if (array_key_exists($CashFlows, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                            if (array_key_exists($depandAmor, $fs_data[$CashFlows][$primary_year])) {
                                                if ($fs_data[$CashFlows][$primary_year][$depandAmor] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$CashFlows][$primary_year][$depandAmor] = "";
                                }

                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get income before working capital
                                // if ($text['y1'] >= $income_before_capital_y1 and $text['y2'] <= $income_before_capital_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $income_before_capital_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["Income Before Working Capital"] = $income_before_capital_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $income_before_capital_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["Income Before Working Capital"] = $income_before_capital_secondary_year;
                                //     }

                                //     if ($income_before_capital_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["Income Before Working Capital"] = "";
                                //     }

                                //     if ($income_before_capital_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["Income Before Working Capital"] = "";
                                //     }
                                // }

                                // get accounts receivable
                                // if ($text['y1'] >= $accounts_receivable_y1 and $text['y2'] <= $accounts_receivable_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $accounts_receivable_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["Accounts Receivable"] = $accounts_receivable_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $accounts_receivable_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["Accounts Receivable"] = $accounts_receivable_secondary_year;
                                //     }

                                //     if ($accounts_receivable_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["Accounts Receivable"] = "";
                                //     }

                                //     if ($accounts_receivable_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["Accounts Receivable"] = "";
                                //     }
                                // }

                                // get inventories
                                // if ($text['y1'] >= $inventories_y1 and $text['y2'] <= $inventories_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $inventories_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["Inventories"] = $inventories_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $inventories_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["Inventories"] = $inventories_secondary_year;
                                //     }

                                //     if ($inventories_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["Inventories"] = "";
                                //     }

                                //     if ($inventories_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["Inventories"] = "";
                                //     }
                                // }

                                // get other current assets
                                // if ($text['y1'] >= $other_current_assets_y1 and $text['y2'] <= $other_current_assets_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $other_current_assets_cs_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["Other Current Assets"] = $other_current_assets_cs_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $other_current_assets_cs_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["Other Current Assets"] = $other_current_assets_cs_secondary_year;
                                //     }

                                //     if ($other_current_assets_cs_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["Other Current Assets"] = "";
                                //     }

                                //     if ($other_current_assets_cs_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["Other Current Assets"] = "";
                                //     }
                                // }

                                // get accuounts payable / accrued expenses
                                // if ($text['y1'] >= $accounts_payable_accrued_expenses_y1 and $text['y2'] <= $accounts_payable_accrued_expenses_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $accounts_payable_accrued_expenses_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["accounts payable/accrued expenses"] = $accounts_payable_accrued_expenses_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $accounts_payable_accrued_expenses_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["accounts payable/accrued expenses"] = $accounts_payable_accrued_expenses_secondary_year;
                                //     }

                                //     if ($accounts_payable_accrued_expenses_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["accounts payable/accrued expenses"] = "";
                                //     }

                                //     if ($accounts_payable_accrued_expenses_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["accounts payable/accrued expenses"] = "";
                                //     }
                                // }

                                // get non-trade payable
                                if (isset($nontrade_payable_y1)) {
                                    if ($text['y1'] >= $nontrade_payable_y1 and $text['y2'] <= $nontrade_payable_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($nonTradPay, $fs_data[$CashFlows][$primary_year])) {
                                                        if ($fs_data[$CashFlows][$primary_year][$nonTradPay] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $nontrade_payable_primary_year = $text['text'];
                                            $fs_data[$CashFlows][$primary_year][$nonTradPay] = $nontrade_payable_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $nontrade_payable_secondary_year = $text['text'];
                                            $fs_data[$CashFlows][$secondary_year][$nonTradPay] = $nontrade_payable_secondary_year;
                                            continue;
                                        }
    
                                        if ($nontrade_payable_secondary_year == STR_EMPTY) {
                                            $fs_data[$CashFlows][$secondary_year][$nonTradPay] = "";
                                        }
        
                                        if ($nontrade_payable_primary_year == STR_EMPTY) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($nonTradPay, $fs_data[$CashFlows][$primary_year])) {
                                                        if ($fs_data[$CashFlows][$primary_year][$nonTradPay] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                            $fs_data[$CashFlows][$primary_year][$nonTradPay] = "";
                                        }
                                    }
                                }

                                // if no non-trade payable
                                if (!isset($nontrade_payable_y1)) {
                                    $fs_data[$CashFlows][$secondary_year][$nonTradPay] = "";
                                    if (array_key_exists($CashFlows, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                            if (array_key_exists($nonTradPay, $fs_data[$CashFlows][$primary_year])) {
                                                if ($fs_data[$CashFlows][$primary_year][$nonTradPay] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$CashFlows][$primary_year][$nonTradPay] = "";
                                }

                                // get net cash generated from operation
                                if (isset($net_cash_generated_from_operation_y1)) {
                                    if ($text['y1'] >= $net_cash_generated_from_operation_y1 and $text['y2'] <= $net_cash_generated_from_operation_y2) {
                                        if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($ncGeneFromOperation, $fs_data[$CashFlows][$primary_year])) {
                                                        if ($fs_data[$CashFlows][$primary_year][$ncGeneFromOperation] != ""){
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }

                                            $net_cash_generated_from_operation_primary_year = $text['text'];
                                            $fs_data[$CashFlows][$primary_year][$ncGeneFromOperation] = $net_cash_generated_from_operation_primary_year;
                                            continue;
                                        }
    
                                        if ($text['x1'] > $primary_year_x2) {
                                            $net_cash_generated_from_operation_secondary_year = $text['text'];
                                            $fs_data[$CashFlows][$secondary_year][$ncGeneFromOperation] = $net_cash_generated_from_operation_secondary_year;
                                            continue;
                                        }
    
                                        if ($net_cash_generated_from_operation_secondary_year == STR_EMPTY) {
                                            $fs_data[$CashFlows][$secondary_year][$ncGeneFromOperation] = "";
                                        }
        
                                        if ($net_cash_generated_from_operation_primary_year == STR_EMPTY) {
                                            if (array_key_exists($CashFlows, $fs_data)) {
                                                if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                                    if (array_key_exists($ncGeneFromOperation, $fs_data[$CashFlows][$primary_year])) {
                                                        continue;
                                                    }
                                                }
                                            }
                                            $fs_data[$CashFlows][$primary_year][$ncGeneFromOperation] = "";
                                        }
                                    }
                                }

                                // if no net cash generated from operation
                                if (!isset($net_cash_generated_from_operation_y1)) {
                                    $fs_data[$CashFlows][$secondary_year][$ncGeneFromOperation] = "";
                                    if (array_key_exists($CashFlows, $fs_data)) {
                                        if (array_key_exists($primary_year, $fs_data[$CashFlows])) {
                                            if (array_key_exists($ncGeneFromOperation, $fs_data[$CashFlows][$primary_year])) {
                                                if ($fs_data[$CashFlows][$primary_year][$ncGeneFromOperation] != ""){
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    $fs_data[$CashFlows][$primary_year][$ncGeneFromOperation] = "";
                                }

                                /*------------------------------------
                                --- commented out for later use-------
                                ------------------------------------- */
                                // get acquisition of properties and equipment
                                // if ($text['y1'] >= $acquisition_properties_equipment_y1 and $text['y2'] <= $acquisition_properties_equipment_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $acquisition_properties_equipment_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["acquisition of properties and equipment"] = $acquisition_properties_equipment_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $acquisition_properties_equipment_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["acquisition of properties and equipment"] = $acquisition_properties_equipment_secondary_year;
                                //     }

                                //     if ($acquisition_properties_equipment_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["acquisition of properties and equipment"] = "";
                                //     }

                                //     if ($acquisition_properties_equipment_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["acquisition of properties and equipment"] = "";
                                //     }
                                // }

                                // get net cash provided by inventory activities
                                // if ($text['y1'] >= $net_cash_inventory_activities_y1 and $text['y2'] <= $net_cash_inventory_activities_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $net_cash_inventory_activities_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["net cash provided by inventory activities"] = $net_cash_inventory_activities_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $net_cash_inventory_activities_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["net cash provided by inventory activities"] = $net_cash_inventory_activities_secondary_year;
                                //     }

                                //     if ($net_cash_inventory_activities_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["net cash provided by inventory activities"] = "";
                                //     }

                                //     if ($net_cash_inventory_activities_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["net cash provided by inventory activities"] = "";
                                //     }
                                // }

                                // get Initial / additional capital
                                // if ($text['y1'] >= $initial_additional_capital_y1 and $text['y2'] <= $initial_additional_capital_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $initial_additional_capital_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["Initial / additional capital"] = $initial_additional_capital_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $initial_additional_capital_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["Initial / additional capital"] = $initial_additional_capital_secondary_year;
                                //     }

                                //     if ($initial_additional_capital_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["Initial / additional capital"] = "";
                                //     }

                                //     if ($initial_additional_capital_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["Initial / additional capital"] = "";
                                //     }
                                // }

                                // get net cash provided by financing activities
                                // if ($text['y1'] >= $net_cash_financing_activities_y1 and $text['y2'] <= $net_cash_financing_activities_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $net_cash_financing_activities_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["net cash provided by financing activities"] = $net_cash_financing_activities_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $net_cash_financing_activities_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["net cash provided by financing activities"] = $net_cash_financing_activities_secondary_year;
                                //     }

                                //     if ($net_cash_financing_activities_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["net cash provided by financing activities"] = "";
                                //     }

                                //     if ($net_cash_financing_activities_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["net cash provided by financing activities"] = "";
                                //     }
                                // }

                                // get net increase (decrease) in cash
                                // if ($text['y1'] >= $net_increase_decrease_cash_y1 and $text['y2'] <= $net_increase_decrease_cash_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $net_increase_decrease_cash_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["net increase (decrease) in cash"] = $net_increase_decrease_cash_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $net_increase_decrease_cash_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["net increase (decrease) in cash"] = $net_increase_decrease_cash_secondary_year;
                                //     }

                                //     if ($net_increase_decrease_cash_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["net increase (decrease) in cash"] = "";
                                //     }

                                //     if ($net_increase_decrease_cash_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["net increase (decrease) in cash"] = "";
                                //     }
                                // }

                                // get cash beginning of the year
                                // if ($text['y1'] >= $cash_beginning_year_y1 and $text['y2'] <= $cash_beginning_year_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $cash_beginning_year_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["cash beginning of the year"] = $cash_beginning_year_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $cash_beginning_year_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["cash beginning of the year"] = $cash_beginning_year_secondary_year;
                                //     }

                                //     if ($cash_beginning_year_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["cash beginning of the year"] = "";
                                //     }

                                //     if ($cash_beginning_year_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["cash beginning of the year"] = "";
                                //     }
                                // }

                                // get cash at the end of the year
                                // if ($text['y1'] >= $cash_end_year_y1 and $text['y2'] <= $cash_end_year_y2) {
                                //     if ($text['x1'] >= $primary_year_x1 and $text['x2'] <= $primary_year_x2) {
                                //         $cash_end_year_primary_year = $text['text'];
                                //         $fs_data[$CashFlows][$primary_year]["cash at the end of the year"] = $cash_end_year_primary_year;
                                //         continue;
                                //     }

                                //     if ($text['x1'] > $primary_year_x2) {
                                //         $cash_end_year_secondary_year = $text['text'];
                                //         $fs_data[$CashFlows][$secondary_year]["cash at the end of the year"] = $cash_end_year_secondary_year;
                                //     }

                                //     if ($cash_end_year_primary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$primary_year]["cash at the end of the year"] = "";
                                //     }

                                //     if ($cash_end_year_secondary_year == STR_EMPTY) {
                                //         $fs_data[$CashFlows][$secondary_year]["cash at the end of the year"] = "";
                                //     }
                                // }
                            }
                        }
                        $print[] = "Statement of cash flows || ".$image_key;
                        $cash_flows = STS_OK;
                    }
                }
                unlink($image); // delete image after detection
                
                // stop if all needed data is successfully detected
                if ($financial_position == STS_OK
                and $income_and_expenses == STS_OK
                and $cash_flows == STS_OK) {
                  $this->delTree($destinationPath);
                break;
                }
            }
        }

        
        //create folder for generated fs if not exist
        $generatedFSPath = public_path('documents/vision/generatedFinancialStatementfromOCR');
        if (!file_exists($generatedFSPath)) {
            mkdir($generatedFSPath);
        }

        $companyName = "Tektwin Marketing Corporation"; // sample
        $yearRow = 6; // from fs template
        $yearColumn = 1; // from fs template

        $file = Excel::load(public_path('fsTemplate.xls')); // fs template
        //set sheet index
        $sheet = $file->setActiveSheetIndex(0);
        //set company name
        $sheet->setCellValue('B2', $companyName);
        
        //set most recent year
        $sheet->setCellValueByColumnAndRow($yearColumn, $yearRow, array_key_first($fs_data));
        
        // set cell values
        foreach ($fs_data as $year) {
            foreach ($year as $dataCategory => $data) {
                switch ($dataCategory) {
                    case "Statement of Financial Position":
                        foreach ($data as $datumKey => $datum) {
                            switch ($datumKey) {
                                case "cash":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 10, $datum);
                                break;
                                case "trade and receivables":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 11, $datum);
                                break;
                                case "Other Current Assets":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 15, $datum);
                                break;
                                case "Property and Equipment":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 22, $datum);
                                break;
                                case "Deposits":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 33, $datum);
                                break;
                                case "Accounts payable and accruals":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 47, $datum);
                                break;
                                case "Trade and other payables":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 45, $datum);
                                break;
                                case "Income Tax Payable":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 46, $datum);
                                break;
                                case "Share Capital":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 63, $datum);
                                break;
                                case "Retained Earnings":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 68, $datum);
                                break;
                                default:
                            break;
                            }
                        }
                    break;
                    
                    case "Statement of Income and Expenses":
                        $SIEyearColumn = $yearColumn + 6;
                        foreach ($data as $datumKey => $datum) {
                            switch ($datumKey) {
                                case "revenue":
                                    $sheet->setCellValueByColumnAndRow($SIEyearColumn, 8, $datum);
                                break;
                                case "Cost of Projects":
                                    $sheet->setCellValueByColumnAndRow($SIEyearColumn, 9, $datum);
                                break;
                                case "Selling and administrative expenses":
                                    $sheet->setCellValueByColumnAndRow($SIEyearColumn, 14, $datum);
                                break;
                                case "Provision for income tax":
                                    $sheet->setCellValueByColumnAndRow($SIEyearColumn, 34, $datum);
                                break;
                                default: 
                            break;
                            }
                        }
                    break;
                    
                    case "Statement of Changes in Shareholders Equity":
                        $balanceBeginningColumn = $yearColumn + 1;
                        foreach ($data as $datumKey => $datum) {
                            switch ($datumKey) {
                                case "share capital":
                                    $sheet->setCellValueByColumnAndRow($yearColumn, 63, $datum);
                                break;
                                case "Balance Beginning":
                                    $sheet->setCellValueByColumnAndRow($balanceBeginningColumn, 68, $datum);
                                break;
                                default:
                            break;
                            }
                        }
                    break;

                    case "Statement of Cash Flows":
                        $SCFyearColumn = $yearColumn + 6;
                        foreach ($data as $datumKey => $datum) {
                            switch ($datumKey) {
                                case "Depreciation and Amortization":
                                    $sheet->setCellValueByColumnAndRow($SCFyearColumn, 19, $datum);
                                break;
                                default:
                                break;
                            }
                        }
                    break;
                    
                    default:
                break;
                }
            }

            // set column to previous year
            $yearColumn++;
        }
        
        // set formulas
        $sheet->setCellValue('B19', $this->columnSum('B', 10, 18));
        $sheet->setCellValue('C19', $this->columnSum('C', 10, 18));
        $sheet->setCellValue('D19', $this->columnSum('D', 10, 18));
        $sheet->setCellValue('E19', $this->columnSum('E', 10, 18));
        
        $sheet->setCellValue('B36', $this->columnSum('B', 22, 35));
        $sheet->setCellValue('C36', $this->columnSum('C', 22, 35));
        $sheet->setCellValue('D36', $this->columnSum('D', 22, 35));
        $sheet->setCellValue('E36', $this->columnSum('E', 22, 35));

        $sheet->setCellValue('B50', $this->columnSum('B', 43, 49));
        $sheet->setCellValue('C50', $this->columnSum('C', 43, 49));
        $sheet->setCellValue('D50', $this->columnSum('D', 43, 49));
        $sheet->setCellValue('E50', $this->columnSum('E', 43, 49));

        $sheet->setCellValue('B60', $this->columnSum('B', 53, 59));
        $sheet->setCellValue('C60', $this->columnSum('C', 53, 59));
        $sheet->setCellValue('D60', $this->columnSum('D', 53, 59));
        $sheet->setCellValue('E60', $this->columnSum('E', 53, 59));

        $sheet->setCellValue('H52', "=H32-({$this->columnSum('H', 45, 51)})");
        $sheet->setCellValue('I52', "=I32-({$this->columnSum('I', 45, 51)})");
        $sheet->setCellValue('J52', "=J32-({$this->columnSum('J', 45, 51)})");
        
        $sheet->setCellValue('H46', '=H19');
        $sheet->setCellValue('H45', '=H20');

        $sheet->setCellValue('I46', '=I19');
        $sheet->setCellValue('I45', '=I20');

        // set styling
        $sheet->getStyle('B10:E74')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $sheet->getStyle('H8:J41')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $sheet->getStyle('H45:J52')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        // set file name and save
        $file->setFileName("{$companyName}-ocr_generated");
        $file->store('xls', $generatedFSPath);
        
        //return view
        return View::make('excel_generated', ['generatedFSPath' => $generatedFSPath,
            'companyName' => $companyName]);
    }

    public function delTree($dir) 
    {
        $files = array_diff(scandir($dir), array('.','..'));
         foreach ($files as $file) {
           (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
         }
         return;
    }

    public function columnSum($columnLetter, $from, $to) {
        $formula = "={$columnLetter}{$from}";

        if (($columnLetter == "H" or $columnLetter == "I" or $columnLetter == "J")
            and $from == 45 and $to == 51) {
            $formula = "{$columnLetter}{$from}";
        }

        $from++;

        for ($i = $from; $i <= $to; $i++) {
            $formula .= "+{$columnLetter}{$i}";
        }
        
        return $formula;
    }
    
    public function getDownloadGeneratedFS($companyName)
    {
        return response()->download(public_path("documents/vision/generatedFinancialStatementfromOCR/{$companyName}-ocr_generated.xls"));
    }
}