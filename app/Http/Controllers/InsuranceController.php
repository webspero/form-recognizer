<?php

//======================================================================
//  Class 27: Insurance Controller
//      Functions related to Insurance on the Questionnaire
//      are routed to this class
//======================================================================
class InsuranceController extends BaseController
{    
    //-----------------------------------------------------
    //  Function 27.1: getInsurance
    //      Displays the Insurance Form
    //-----------------------------------------------------
	public function getInsurance($entity_id)
	{
		return View::make('sme.insurance',
            array(
                'entity_id' => $entity_id
            )
        );
	}

    //-----------------------------------------------------
    //  Function 27.2: postInsurance
    //  
    //  Validates and saves the data to the database
    //  when Insurance "Add" form is submitted
    //
    //  The same function is responsible for the API
    //  "Add / Edit Insurance"
    //-----------------------------------------------------
	public function postInsurance($entity_id)
	{
        /*  Variable Declaration    */
        $insurance  = Input::get('insurance_type'); // Insurance Name on the Form
        $messages   = array();                      // Validation Messages according to rules
        $rules      = array();                      // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;

        $entity = Entity::find($entity_id);
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;       // Server Process Status for Output
        $srv_resp['messages']       = STR_EMPTY;    // Server Process Message for Output
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#insurance-list-cntr';
            $srv_resp['refresh_elems']  = '.reload-insurance';

            if($entity->is_premium == PREMIUM_REPORT ) {
                $srv_resp['refresh_url']    = URL::to('premium-link/smesummary/'.$entity_id);
            } else {
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            }
            
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /*  User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($insurance); $idx++) {
            
            /*  Validates input set with data on Insurance Type
            *   Also, always validate the first Form input set
            */
            if ((STR_EMPTY != $insurance[$idx])
            || (0 == $idx)) {

                $messages['insurance_type.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details1.insurance_type')));
                $messages['insured_amount.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details1.insured_amount')));
                $messages['insured_amount.'.$idx.'.numeric']	= trans('validation.numeric', array('attribute'=>trans('business_details1.insured_amount')));
                $messages['insured_amount.'.$idx.'.max']	= trans('validation.max.numeric', array('attribute'=>trans('business_details1.insured_amount')));
            
                $rules['insurance_type.'.$idx]	= 'required';
                $rules['insured_amount.'.$idx]	= 'required|numeric|max:9999999999999.99';
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else
		{
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('insurance_type', 'insured_amount');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
			$insurance_type = $arraydata['insurance_type'];
            $insured_amount = $arraydata['insured_amount'];

            /*  Saves Input to the database     */
			foreach ($insurance_type as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'=>$entity_id, 
						'insurance_type'=>$insurance_type[$key],
						'insured_amount'=>$insured_amount[$key],
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblinsurance')
                            ->where('insuranceid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblinsurance')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 27.3: getInsuranceDelete
    //      HTTP GET request to delete specified Insurance
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getInsuranceDelete($id)
	{
        $insurance = Insuarance::where('insuranceid', '=', $id)
                                ->where('entity_id', '=', Session::get('entity')->entityid)
                                ->first();
        $insurance->is_deleted = 1;
        $insurance->save();
        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        
        /*  Adds a one-off session variable for redirection */
		Session::flash('redirect_tab', 'tab1');
        
        $entity = Entity::find(Session::get('entity')->entityid);
        
        if($entity->is_premium == PREMIUM_REPORT) {
            return Redirect::to('/premium-link/summary/'.Session::get('entity')->entityid);
        } else {
		    return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
        }
	}
}
