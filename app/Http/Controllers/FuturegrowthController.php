<?php
//======================================================================
//  Class 24: FuturegrowthController
//  Description: Contains functions for Future Growth Initiatives
//======================================================================
class FuturegrowthController extends BaseController {

	//-----------------------------------------------------
    //  Function 24.1: getFuturegrowth
	//  Description: get Future Growth Initiatives form
    //-----------------------------------------------------	
	public function getFuturegrowth($entity_id)
	{
		return View::make('sme.futuregrowthinitiative',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 24.2: postFuturegrowth
	//  Description: save Future Growth Initiatives
    //-----------------------------------------------------	
	public function postFuturegrowth($entity_id)
	{
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $future_growth  = Input::get('futuregrowth_name');
        $messages       = array();
        $rules          = array();
        
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#future-growth-list-cntr';
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            $srv_resp['refresh_elems']  = '.reload-future-growth';
        }
        
        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
		/* validation rules and messages */
		for($x=0; $x < @count($future_growth); $x++) {
			if (($future_growth[$x] != "")
            || (0 == $x)) {

                $messages['futuregrowth_name.'.$x.'.required']                  = trans('validation.required', array('attribute'=>trans('business_details2.fgi_purpose')));
                $messages['futuregrowth_estimated_cost.'.$x.'.required']	    = trans('validation.required', array('attribute'=>trans('business_details2.fgi_cost')));
                $messages['futuregrowth_estimated_cost.'.$x.'.numeric']         = trans('validation.numeric', array('attribute'=>trans('business_details2.fgi_cost')));
                $messages['futuregrowth_estimated_cost.'.$x.'.max']             = trans('validation.max.numeric', array('attribute'=>trans('business_details2.fgi_cost')));
                $messages['futuregrowth_implementation_date.'.$x.'.required']   = trans('validation.required', array('attribute'=>trans('business_details2.fgi_date')));
                $messages['futuregrowth_implementation_date.'.$x.'.date_format'] = trans('validation.date_format', array('attribute'=>trans('business_details2.fgi_date')));
                $messages['futuregrowth_implementation_date.'.$x.'.after']      = trans('validation.after', array('attribute'=>trans('business_details2.fgi_date')));
                $messages['futuregrowth_source_capital.'.$x.'.required']        = trans('validation.required', array('attribute'=>trans('business_details2.fgi_source')));
                $messages['planned-proj-goal.'.$x.'.required']                  = trans('validation.required', array('attribute'=>trans('business_details2.fgi_proj_goal')));
                $messages['planned-goal-increase.'.$x.'.required']	            = trans('validation.required', array('attribute'=>trans('business_details2.fgi_goal_increase')));
                $messages['proj-benefit-date.'.$x.'.required']                  = trans('validation.required', array('attribute'=>trans('business_details2.fgi_benefit_date')));
                $messages['proj-benefit-date.'.$x.'.date_format']               = 'Invalid format for '.trans('business_details2.fgi_benefit_date');

                $rules['futuregrowth_name.'.$x]                 = 'required';
                $rules['futuregrowth_estimated_cost.'.$x]       = 'required|numeric|max:9999999999999.99';
                $rules['futuregrowth_implementation_date.'.$x]  = array('required', 'date_format:"Y-m-d"', 'after: today');
                $rules['futuregrowth_source_capital.'.$x]       = 'required';
                $rules['planned-proj-goal.'.$x]                 = 'required';
                $rules['planned-goal-increase.'.$x]             = 'required';
                $rules['proj-benefit-date.'.$x]                 = 'required|date_format:"Y-m"';

			}
		}
	
		$validator = Validator::make(Input::all(), $rules, $messages);

		if($validator->fails())
		{
			$srv_resp['messages']	= $validator->messages()->all();
		}
		else
		{	
			$arraydata = Input::except('_token');
            
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
			$futuregrowth_name                  = $arraydata['futuregrowth_name'];
			$futuregrowth_estimated_cost        = $arraydata['futuregrowth_estimated_cost'];
			$futuregrowth_implementation_date   = $arraydata['futuregrowth_implementation_date'];
			$futuregrowth_source_capital        = $arraydata['futuregrowth_source_capital'];
			$planned_proj_goal                  = $arraydata['planned-proj-goal'];
			$planned_goal_increase              = $arraydata['planned-goal-increase'];
			$proj_benefit_date                  = $arraydata['proj-benefit-date'];
            
			foreach ($futuregrowth_name as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'                         => $entity_id, 
					   	'futuregrowth_name'                 => $futuregrowth_name[$key],
					   	'futuregrowth_estimated_cost'       => $futuregrowth_estimated_cost[$key],
					   	'futuregrowth_implementation_date'  => $futuregrowth_implementation_date[$key],
					   	'futuregrowth_source_capital'       => $futuregrowth_source_capital[$key],
					   	'planned_proj_goal'                 => $planned_proj_goal[$key],
					   	'planned_goal_increase'             => $planned_goal_increase[$key],
					   	'proj_benefit_date'                 => $proj_benefit_date[$key].'-01',
					   	'created_at'                        => date('Y-m-d H:i:s'),
				    	'updated_at'                        => date('Y-m-d H:i:s')
					);
                    
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblfuturegrowth')
                            ->where('futuregrowthid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    else {
                        $master_ids[]           = DB::table('tblfuturegrowth')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            $srv_resp['sts']        = STS_OK;
            
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        ActivityLog::saveLog();				// log activity to db
        return json_encode($srv_resp);
	}
    
	//-----------------------------------------------------
    //  Function 24.3: getFuturegrowthDelete
	//  Description: delete Future Growth Initiatives
    //-----------------------------------------------------	
    public function getFuturegrowthDelete($id)
	{
		// DB::table('tblfuturegrowth')
        // ->where('futuregrowthid', $id)
		// ->where('entity_id', Session::get('entity')->entityid)
        // ->delete();
        
        $future = Futuregrowth::where('futuregrowthid', '=', $id)
                    ->where('entity_id', '=', Session::get('entity')->entityid)
                    ->first();
        $future->is_deleted = 1;
        $future->save();

        ActivityLog::saveLog();
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}
}
