<?php

use CreditBPO\Models\Location;
use CreditBPO\Models\BankSerialKey;
use CreditBPO\Models\Province2;
use CreditBPO\Models\BankInterestRate;
use CreditBPO\Models\GRDP;
use CreditBPO\Services\MailHandler;
use CreditBPO\Services\IndustryComparison;
use CreditBPO\Handlers\FinancialConditionHandler;
use CreditBPO\Services\Report\Charts\FinancialCondition;
use Illuminate\Support\Facades\Redirect;
use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\GenerateFSZipFileHandler;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use CreditBPO\Handlers\SMEInternalHandler;
use CreditBPO\Models\ReadyratioKeySummary;
use CreditBPO\Services\KeyRatioService;
use CreditBPO\Models\FinancialReport;
use \Exception as Exception;
use Illuminate\Support\Facades\Log;

//======================================================================
//  Class 45: Profile Controller
//      Functions related to SME Profile pages are routed to this class
//======================================================================
class ProfileSMEController extends BaseController
{

    public $fh ;

    public function __construct() {
        $this->fh = new FinancialHandler();
    }
    //-----------------------------------------------------
    //  Function 45.1: getSMESummary
    //      Old SME Summary route, now it will redirect
    //      to new URL route
    //-----------------------------------------------------
    
	public function getSMESummary($id, $view_typ = SUMMARY_NORMAL)
	{
		try{
	        /*  Get Report information  */
			$entity = Entity::where('entityid', $id)->first();

	        /*  Entity exists   */
			if ($entity) {
	            
	            if($entity->is_premium == PREMIUM_REPORT) {
	                $fs_template = DB::table('tbldocument')
	                        ->where('entity_id', '=', $entity->entityid)
	                        ->where('document_group', '=', 21)
	                        ->where('upload_type', '=', 5)
	                        ->where('is_deleted', 0)
	                        ->orderBy('document_type', 'ASC')
	                        ->get();

	                if(empty($fs_template)) {
	                    return Redirect::to('/sme/upload_fs_template_premium/' . $entity->entityid);
	                }
	            }
	            

	            if (1 == $entity->anonymous_report) {
	                $company_name = 'Company - '.$entity->entityid;
	            }
	            else {
	                $company_name = $entity->companyname;
	            }

	            $company_name = str_replace('/', '',$company_name);

	            /*  SME view report */
				if ($view_typ == SUMMARY_NORMAL) {
					return Redirect::to('/summary/'.$id.'/'.$entity->loginid.'/'.urlencode($company_name));
				}
	            /*  Banks, CI or FA view report */

	            if (Auth::user()->role === 4) {
	                $bankSubscription = Permissions::get_bank_subscription();
	                $subscriptionExpired = !$bankSubscription ||
	                    strtotime($bankSubscription) < strtotime(date('c'));

	                if (
	                    $subscriptionExpired &&
	                    $entity->completed_date &&
	                    strtotime($entity->completed_date) <= strtotime('-2 week')
	                ) {
	                    return Redirect::to('/bank/subscribe');
	                }
	            }
				return Redirect::to('/summary/'.$id.'/'.$entity->loginid.'/'.urlencode($company_name).'/'.$view_typ);

			}
	        /*  Report does not exists   */
	        else {
				App:abort(403, 'Unknown url');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 45.2: getSummaryLongUrl
    //      Report Information Page
    //-----------------------------------------------------
	public function getSummaryLongUrl($id, $user_id, $company_name, $view_typ = SUMMARY_NORMAL)
	{

		try{
	        /*  Variable Declaration    */
	        $bank_ci_view       = STS_NG;   // Bank Viewing the report
	        $bank_id            = STS_NG;   // Bank ID associated with the report
	        $base_year          = 0;
	        $dscr_standalone    = array();

	        /*  When Bank is viewing the report, set flag to ON */
	        if ((USER_ROLE_BANK == Auth::user()->role)
	        && (SUMMARY_BANK_CI == $view_typ)) {
	            $bank_ci_view = STS_OK;
	        }

	        /*  When Bank is viewing the report */
			if (USER_ROLE_BANK == Auth::user()->role) {
	            /*  Check Bank Subscription Expiration  */
	            if(Auth::user()->bypass_subscriptions_checks == 0){
	                if (Auth::user()->product_plan != 3) {

	                    /*  Get Bank Subscription   */
	                    $bank_subscription  = Permissions::get_bank_subscription();

	                    /*  Bank Subscription expired or not yet subscribed  */
	                    if ($bank_subscription != null && strtotime($bank_subscription) > strtotime(date('c'))) {
	                        $bank_login_id = Auth::user()->loginid;

	                        /*  Check parent account subscription   */
	                        if (Auth::user()->parent_user_id != 0) {
	                            $bank_login_id = Auth::user()->parent_user_id;
	                        }

	                        /*  Check Paypal Subscription   */
	                        $subs_sts   = BankController::checkSubscriptionSts($bank_login_id);

	                        /*  Redirect to subscription page   */
	                        if (STS_NG == $subs_sts) {
	                            return Redirect::to('/bank/subscribe');
	                        }
	                        /*  Payment is Pending  */
	                        else if (2 == $subs_sts) {
	                            $subscription   = Transaction::where('loginid', $bank_login_id)
	                                ->where('description', 'Bank User Subscription')
	                                ->orderBy('id', 'desc')
	                                ->first();

	                            /* Redirect to Pending Screen */
	                            return View::make('payment.pending', array('refno' => $subscription['refno']));
	                        }
	                        else {
	                            /** No Processing   */
	                        }
	                    }
	                    /*  Bank is subscribed  */
	                    else {
	                        /*  Check user access permission    */
	                        $permission = Permissions::get_available_smes_for_bank_user();
	                        if(gettype($permission) == "object") {
	                            $permission = Permissions::get_available_smes_for_bank_user()->toArray();
	                        }
	                        if(!in_array($id, $permission)){
	                            App::abort(403, 'Unauthorized access.');
	                        }
	                    }
	                }
	            }
	        }

	        \Log::info(Auth::user()->loginid);

	        /*  Get Report Information  */
			$entity = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
	            ->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
	            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
	            ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
	            ->leftJoin('refregion', 'refcitymun.regDesc', 'refregion.regCode')
	            ->where('tblentity.entityid', '=', $id);

	        /*  User is the SME  */
			if (Auth::user()->role == 1) {
	            $entity->where('tblentity.loginid', '=', Auth::user()->loginid);
			}

	        $entity = $entity->first();
	        /*  User is FA. Do not allow access when account is finished or not yet submitted  */
			if (Auth::user()->role == USER_ROLE_FA &&
			($entity->status == 0 || $entity->status == 7)) {
				App::abort(403, 'Unauthorized access.');
	        }

	        /*  User is CI. Do not allow access when account is not yet finished    */
			if(Auth::user()->role == USER_ROLE_CI && $entity->status != 7){
				App::abort(403, 'Unauthorized access.');
	        }

	        /*  Put report details in a session variable    */
			if ($entity != null) {
				// Store entity in session
				Session::put('entity', $entity);
				// Usage: Session::get('entity')->entityid
			}
	        else {
				App::abort(404);
	        }

	        /*  Check if entity is paid, if not, proceed to payment */
			if ($entity->is_paid == 0) {
	            $user = User::find(Auth::user()->loginid);
	            
	            if(($user->street_address == "" || $user->city == ""|| $user->phone == "" || $user->name == "") && $user->trial_flag == 0 && Auth::user()->role == 1) {
					$error = array('Please complete your billing details before purchasing.');
					return Redirect::to('/billingdetails')->withErrors($error)->withInput();
	            }

	            if(Auth::user()->role == 1){
	                return Redirect::intended('/payment/selection?id='.$entity->entityid);
	            }
	        }

	        /*  Revert if in admin review*/
			if(Auth::user()->role == USER_ROLE_SME && $entity->status == 7 && $entity->admin_review_flag == 1){
				$entity->status = 5;
			}

	        if (isset($entity->current_bank)) {
	            $bank_id = $entity->current_bank;
	        }

	        $questionnaire_db   = new QuestionnaireConfig;
	        $questionnaire_cnfg = $questionnaire_db->getQuestionnaireConfigbyBankID($bank_id);
			$bank_standalone    = 0;
			$custom_docs        = array();

	        /*  Override questionnaire config for Financial Analysis Report Standalone  */
			if ($bank_id) {
				$bank_standalone = Bank::find($bank_id)->is_standalone;

				if ($bank_standalone) {
					$questionnaire_cnfg->affiliates = 2;
					$questionnaire_cnfg->products = 2;
					$questionnaire_cnfg->main_locations = 2;
					$questionnaire_cnfg->capital_details = 2;
					$questionnaire_cnfg->branches = 2;
					$questionnaire_cnfg->import_export = 2;
					$questionnaire_cnfg->insurance = 2;
					$questionnaire_cnfg->cash_conversion = 2;
					$questionnaire_cnfg->major_customer = 0;
					$questionnaire_cnfg->major_supplier = 0;
					$questionnaire_cnfg->major_market = 2;
					$questionnaire_cnfg->competitors = 2;
					$questionnaire_cnfg->business_drivers = 2;
					$questionnaire_cnfg->customer_segments = 2;
					$questionnaire_cnfg->past_projects = 2;
					$questionnaire_cnfg->future_growth = 2;
					$questionnaire_cnfg->owner_details = 2;
					$questionnaire_cnfg->directors = 2;
					$questionnaire_cnfg->shareholders = 2;
					$questionnaire_cnfg->ecf = 2;
					$questionnaire_cnfg->company_succession = 2;
					$questionnaire_cnfg->landscape = 2;
					$questionnaire_cnfg->economic_factors = 2;
					$questionnaire_cnfg->tax_payments = 2;
					$questionnaire_cnfg->risk_assessment = 2;
					$questionnaire_cnfg->growth_potential = 2;
					$questionnaire_cnfg->investment_expansion = 2;
					//$questionnaire_cnfg->rcl = 2;
				}

	            $custom_docs    = BankCustomDocs::getDocumentsByBank($bank_id);
	        }

	        /* Fetch Report Details from the Database  */
			$login = DB::table('tbllogin')
	            ->where('status', '=', 2)
	            ->orWhere('status', '=', 1)
	            ->where('loginid', '=', $entity->loginid)
	            ->first();

	        $doc_req = DB::table('bank_doc_options')
	            ->where('bank_id', $entity->current_bank)
	            ->where('is_active', STS_OK)
	            ->first();

	        $prod_services = DB::table('tblservicesoffer')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$majorcustomeraccount = DB::table('tblmajorcustomeraccount')
	            ->where('entity_id', '=', $entity->entityid)
	            ->get();

	        $majorsupplieraccount = DB::table('tblmajorsupplieraccount')
	            ->where('entity_id', '=', $entity->entityid)
	            ->get();

			$majorcustomers = DB::table('tblmajorcustomer')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $majorsuppliers = DB::table('tblmajorsupplier')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $majorlocations = DB::table('tblmajorlocation')
	            ->join('refcitymun', 'tblmajorlocation.marketlocation', '=', 'refcitymun.id')
	            ->where('tblmajorlocation.entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $competitors = DB::table('tblcompetitor')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $bizdrivers = DB::table('tblbusinessdriver')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $bizsegments = DB::table('tblbusinesssegment')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $pastprojects = DB::table('tblprojectcompleted')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $futuregrowths = DB::table('tblfuturegrowth')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$revenuepotential = DB::table('tblrevenuegrowthpotential')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$capacityexpansion = DB::table('tblcapacityexpansion')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$relatedcompanies = DB::table('tblrelatedcompanies')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$capital = DB::table('tblcapital')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$certifications = DB::table('tblcertifications')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $boi_cert   = DB::table('tblcertifications')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('certification_details', 'Board of Investments (BOI)')
	            ->where('is_deleted', 0)
	            ->first();

	        $peza_cert   = DB::table('tblcertifications')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('certification_details', 'Philippine Export Processing Zone Authority (PEZA)')
	            ->where('is_deleted', 0)
	            ->first();
			$businesstype = DB::table('tblbusinesstype')
	            ->where('entity_id', '=', $entity->entityid)
	            ->get();

			if (is_countable($businesstype) && count($businesstype)==0) {
				DB::table('tblbusinesstype')
				->insert(array(
					'entity_id' => $entity->entityid,
					'is_import' => 2,
					'is_export' => 2
				));

				$businesstype = DB::table('tblbusinesstype')
				->where('entity_id', '=', $entity->entityid)
				->get();
			}

			$insuarance = DB::table('tblinsurance')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $machineEquipments = MachineEquipment::where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$slocation = DB::table('tbllocations')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$branches = DB::table('tblbranches')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$sustainability = DB::table('tblsustainability')
	            ->where('entity_id', '=', $entity->entityid)
	            ->first();

			$planfacility = DB::table('tblplanfacility')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$planfacilityrequested = DB::table('tblplanfacilityrequested')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

	        $riskassessment        = DB::table('tblriskassessment')
	            ->where('entity_id', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$owner = DB::table('tblowner')
	            ->select(DB::raw('tblowner.*, zipcodes.city as city_name'))
	            ->leftJoin('zipcodes', 'tblowner.cityid', '=', 'zipcodes.id')
	            ->where('tblowner.entity_id', '=', $entity->entityid)
	            ->get();

			foreach($owner as $k=>$v) {
				$owner[$k]->loans = DB::table('tblpersonalloans')->where('owner_id', $v->ownerid)->get();

	            $owner[$k]->keymanage = DB::table('tblkeymanagers')
	                ->select(DB::raw('tblkeymanagers.*, zipcodes.city as city_name'))
	                ->leftJoin('zipcodes', 'tblkeymanagers.cityid', '=', 'zipcodes.id')
	                ->where('tblkeymanagers.ownerid', $v->ownerid)
	                ->where('tblkeymanagers.is_deleted', 0)
	                ->get();

	            $owner[$k]->own_bizs = DB::table('tblmanagebus')
	                ->join('tblcity', 'tblmanagebus.bus_location', '=', 'tblcity.cityid')
	                ->where('tblmanagebus.ownerid', $v->ownerid)
	                ->get();

	            $owner[$k]->owner_educs = DB::table('tbleducation')
	                ->where('ownerid', $v->ownerid)
	                ->get();

	            $owner[$k]->spouse = DB::table('tblspouse')
	                ->where('ownerid', $v->ownerid)
	                ->first();

				if($owner[$k]->spouse){
					$owner[$k]->sp_bizs = DB::table('tblmanagebus')
						->join('tblcity', 'tblmanagebus.bus_location', '=', 'tblcity.cityid')
						->where('tblmanagebus.ownerid', $owner[$k]->spouse->spouseid)
						->get();

					$owner[$k]->sp_educs = DB::table('tbleducation')
						->where('ownerid', $owner[$k]->spouse->spouseid)
						->get();
				} else {
					$owner[$k]->sp_bizs = null;
					$owner[$k]->sp_educs = null;
				}
			}

			$keymanagers = DB::table('tblkeymanagers')
	                ->select(DB::raw('tblkeymanagers.*, zipcodes.city as city_name'))
	                ->leftJoin('zipcodes', 'tblkeymanagers.cityid', '=', 'zipcodes.id')
	                ->where('entity_id', '=', $entity->entityid)
	                ->where('tblkeymanagers.is_deleted', 0)
	                ->get();

			$documents2 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 21)
	            ->orderBy('document_type', 'ASC')
	            ->get();

	        $fs_template = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 21)
	            ->where('upload_type', '=', 5)
	            ->where('is_deleted', 0)
	            ->orderBy('document_type', 'ASC')
	            ->get();

	        $fs_template_arr = $fs_template->toArray();

	        if(empty($fs_template_arr) || count($fs_template_arr) < 2){
	        	
	        	$fs_template = GenerateFSZipFileHandler::generateFSZipFile($entity->entityid); 
	        }

	        $quickbooks_reports = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 45)
	            ->where('is_deleted', 0)
	            ->orderBy('document_type', 'ASC')
	            ->get();

	        $fs_supporting = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 21)
	            ->where('upload_type', '!=', 5)
	            ->where('is_deleted', 0)
	            ->orderBy('document_type', 'ASC')
	            ->get();

			$documents3 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 31)
	            ->where('is_deleted', 0)
	            ->orderBy('document_type', 'ASC')
	            ->get();

			$documents5 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 51)
	            ->where('is_deleted', 0)
	            ->orderBy('updated_at', 'asc')
	            ->get();

			$documents6 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 61)
	            ->where('is_deleted', 0)
	            ->orderBy('updated_at', 'asc')
	            ->get();

	        $documents7 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 71)
	            ->where('is_deleted', 0)
	            ->orderBy('updated_at', 'asc')
	            ->get();

	        $custom_doc = $this->getRequiredCustomDocs($entity->entityid);

			$documents9 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 99999)
	            ->where('is_deleted', 0)
	            ->orderBy('documentid', 'DESC')
	            ->orderBy('document_type', 'ASC')
	            ->first();

			$riskassessment = DB::table('tblriskassessment')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$riskassessmentforcount = DB::table('tblriskassessment')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->groupBy('risk_assessment_name', 'risk_assessment_solution')
	            ->get();

			$finalscore = DB::table('tblsmescore')
	            ->where('entityid', '=', $entity->entityid)
	            ->orderBy('smescoreid', 'desc')
	            ->take(1)
	            ->get();

			$readyratiodata = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity->entityid)
	            ->orderBy('readyrationdataid', 'desc')
	            ->take(1)
	            ->get();

	        //Rating Summary calculations
	        if(empty($readyratiodata->toArray())){
	            $readyratiodata[0] = SMEInternalHandler::getOverwriteReadyRatio($entity->entityid);
	        }

	        $readyratiodata = array();
	        $readyratiodata[0] = SMEInternalHandler::getOverwriteReadyRatio($entity->entityid);

			$cycledetails = DB::table('tblcycledetails')
	            ->where('entity_id', '=', $entity->entityid)
	            ->orderBy('index', 'ASC')
	            ->get();

			$shareholders = DB::table('tblshareholders')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$directors = DB::table('tbldirectors')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->get();

			$documents4 = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('is_deleted', 0)
	            ->where('document_group', '=', 41)
	            ->get();

	        //JM - changed list to pluck
			$readyrationList            = Rationscore::all()->pluck('score_range', 'score_value');
			$readyrationPerformanceList = Rationscore::all()->pluck('score_range', 'score_financial_value');

			/*  Progress Bar Calculation */
			$checkid    = $entity->entityid;
			$progress   = array(
				'majorcustomer'             => (DB::table('tblmajorcustomer')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'majorsupplier'             => (DB::table('tblmajorsupplier')->where('entity_id', $checkid)->count() >= 3) ? 1 : 0,
				'businessdriver'            => (DB::table('tblbusinessdriver')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'businesssegment'           => (DB::table('tblbusinesssegment')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'pastprojectscompleted'     => (DB::table('tblprojectcompleted')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'futuregrowthinitiative'    => (DB::table('tblfuturegrowth')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'riskassessment'            => (DB::table('tblriskassessment')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'servicesoffer'             => (DB::table('tblservicesoffer')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'competitor'                => (DB::table('tblcompetitor')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
				'majormarketlocation'       => (DB::table('tblmajorlocation')->where('entity_id', $checkid)->count() > 0) ? 1 : 0,
	        );

	        /*  Get Investigator Notes  */
			$investigator_notes = InvestigatorNotes::where('entity_id', $entity->entityid)->first();

	        if ($investigator_notes) {
				$investigator_notes->required_docs = explode(',', $investigator_notes->required_docs);
	        }

			$created            = Entity::where('loginid', $entity->loginid)->first();
			$requested_files    = ($investigator_notes) ? $investigator_notes->required_docs : array();

	        /*  Compute Cash Conversion Cycle   */
			if ($cycledetails) {
				foreach($cycledetails as $key => $value){
					if($value->cost_of_sales!=0 && $value->credit_sales!=0){
						$dio = ($value->inventory / $value->cost_of_sales) * 30;
						$dso = ($value->accounts_receivable / $value->credit_sales) * 30;
						$dpo = $value->accounts_payable / ($value->cost_of_sales / 30);
	                    $ccc = $dio + $dso - $dpo;
						$cycledetails[$key]->ccc = $ccc <= 0 ? '-' : number_format($ccc, 2, '.', '');
					}
					else {
						$cycledetails[$key]->ccc = '-';
					}
				}
	        }

	        /*  Tab Rating  */
	        $feedback_db        = new FeedbackTabRating;
	        $feedback_data      = array();
	        $feedback_tab_val   = array();
	        $feedback_tab_name  = array('', 'registration1', 'registration2', 'ownerdetails', 'payfactor', 'bcs', 'pfs');

	        for ($i = 1 ; $i <= 6; $i++) {
	            $feedback_data      = $feedback_db->checkTabFeedbackRating($entity->entityid, $i);
	            $feedback_tab_val[$i]   = @count($feedback_data);
	        }

	        /*  Display Bank Credit Rating  */
			$show_bank_rating   = false;
			$gscore             = 0;

			if (is_countable($finalscore) &&count($finalscore)>0) {
				if (Auth::user()->role == 4) {
					$show_bank_rating = true;
					$bank_group = BankIndustry::where('bank_id', Auth::user()->bank_id)
							->where('industry_id', $entity->industry_main_id)->first();
					$bank_formula = BankFormula::where('bank_id', Auth::user()->bank_id)->first();
				}
				else if($entity->current_bank != 0 && $entity->is_independent == 0){
					$show_bank_rating = true;
					$bank_group = BankIndustry::where('bank_id', $entity->current_bank)
							->where('industry_id', $entity->industry_main_id)->first();
					$bank_formula = BankFormula::where('bank_id', $entity->current_bank)->first();
				}

				if ($show_bank_rating) {

					if ($bank_group) {
						if ($bank_formula) {
							//bcc
							$bgroup_array = explode(',', $bank_formula->bcc);
							$bgroup_item = 0;
							$bgroup_total = 0;
							if (in_array('rm', $bgroup_array)) {
								$bgroup_item += $finalscore[0]->score_rm;
								$bgroup_total += MAX_RM;
							}
							if (in_array('cd', $bgroup_array)) {
								$bgroup_item += $finalscore[0]->score_cd;
								$bgroup_total += MAX_CD;
							}
							if (in_array('sd', $bgroup_array)) {
								$bgroup_item += $finalscore[0]->score_sd;
								$bgroup_total += MAX_SD;
							}
							if (in_array('boi', $bgroup_array)) {
								$bgroup_item += $finalscore[0]->score_bois;
								$bgroup_total += MAX_BOI;
							}

	                        if (0 < $bgroup_total) {
	                            $bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
	                        }
	                        else {
	                            $bgroup = 0;
	                        }

							//mq
							$mgroup_array = explode(',', $bank_formula->mq);
							$mgroup_item = 0;
							$mgroup_total = 0;
							if(in_array('boe', $mgroup_array)) {
								$mgroup_item += $finalscore[0]->score_boe;
								$mgroup_total += MAX_BOE;
							}
							if(in_array('mte', $mgroup_array)) {
								$mgroup_item += $finalscore[0]->score_mte;
								$mgroup_total += MAX_MTE;
							}
							if(in_array('bd', $mgroup_array)) {
								$mgroup_item += $finalscore[0]->score_bdms;
								$mgroup_total += MAX_BD;
							}
							if(in_array('sp', $mgroup_array)) {
								$mgroup_item += $finalscore[0]->score_sp;
								$mgroup_total += MAX_SP;
							}
							if(in_array('ppfi', $mgroup_array)) {
								$mgroup_item += $finalscore[0]->score_ppfi;
								$mgroup_total += MAX_PPFI;
							}

	                        if (0 < $mgroup_total) {
	                            $mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
	                        }
	                        else {
	                            $mgroup = 0;
	                        }

							//fa
							$fgroup_array = explode(',', $bank_formula->fa);
							if(is_countable($fgroup_array) && count($fgroup_array) == 2){
								$fgroup = $finalscore[0]->score_facs;
							} else {
								if($bank_formula->fa == 'fp'){
									$fgroup = $finalscore[0]->score_rfp;
								} elseif($bank_formula->fa == 'fr'){
									$fgroup = $finalscore[0]->score_rfpm;
								} else {
									$fgroup = 0;
								}
							}
						} else {
							$bgroup = ( $finalscore[0]->score_rm + $finalscore[0]->score_cd + $finalscore[0]->score_sd + $finalscore[0]->score_bois );
							$mgroup = ( $finalscore[0]->score_boe + $finalscore[0]->score_mte + $finalscore[0]->score_bdms + $finalscore[0]->score_sp + $finalscore[0]->score_ppfi );
							$fgroup = $finalscore[0]->score_facs;
						}

						$gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

						if($bank_formula && $bank_formula->boost == 0){
							// no boost
						} else {
							//boost
							if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
							if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
							if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
						}
					} else {
						$gscore = intval($finalscore[0]->score_sf);
					}
				}
	        }

	        /*  Debt Ratio Calculator  */
			$fa_report = FinancialReport::where(['entity_id' => $entity->entityid, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
	        if($fa_report!=null){
				$fa_report->balance_sheets = $fa_report->balanceSheets()->orderBy('index_count', 'asc')->get();
				$fa_report->income_statements = $fa_report->incomeStatements()->orderBy('index_count', 'asc')->get();
				$fa_report->cash_flows = $fa_report->cashFlow()->orderBy('index_count', 'asc')->get();

				$cf_net_profit_margin = 0;
				if($fa_report->income_statements[0]->Revenue != 0){
					$cf_net_profit_margin = $fa_report->income_statements[0]->ProfitLossFromOperatingActivities/$fa_report->income_statements[0]->Revenue;	
				}

		        $cf_net_profit_margin_percentage = number_format($cf_net_profit_margin*100,2,".","");
		        
		        $sumAssets = 0;
		        foreach($fa_report->balance_sheets as $bs){
		            $sumAssets += floatval($bs->Assets);
		        }

		        $aveAssets = $sumAssets/3;
		        
		        $sumEquity = 0;
		        foreach($fa_report->balance_sheets as $bs){
		            $sumEquity += floatval($bs->Equity);
		        }

		        $aveEquity = $sumEquity/3;
		        
		        $cf_asset_turnover = 0;
		        $cf_asset_turnover_percentage = 0;
		        if(!empty($aveAssets)){
		        	$cf_asset_turnover = $fa_report->income_statements[0]->Revenue/$aveAssets;
		        	$cf_asset_turnover_percentage = number_format($cf_asset_turnover*100,2,".","");	
		        }
		        
				$cf_equity_multiplier = 1;
		        if(!empty($aveEquity)){
		        	$cf_equity_multiplier = $aveAssets/$aveEquity;
		        	$cf_equity_multiplier_percentage = number_format($cf_equity_multiplier*100,2,".","");	
		        }
		        
		        $cf_dupont_analysis = $cf_net_profit_margin*$cf_asset_turnover*$cf_asset_turnover*$cf_equity_multiplier;

		        $cf_dupont_analysis_percentage = number_format($cf_dupont_analysis*100,2,".","");

	            $divbyzero = false;
	            
				//Financial Position
				if(!$divbyzero){
					//Debt Ratio
					$DebtRatio = array();
					for($x=@count($fa_report->balance_sheets)-1; $x >= 0 ; $x--){
						if($fa_report->balance_sheets[$x]->Assets != 0)
							$DebtRatio[] = $fa_report->balance_sheets[$x]->Liabilities / $fa_report->balance_sheets[$x]->Assets;
						else
							$DebtRatio[] = 0;
					}
					$DebtRatioS = [
						$this->fh->CalculateSp($DebtRatio),
						$DebtRatio[@count($fa_report->balance_sheets)-1],
						$this->fh->CalculateSf($DebtRatio)
					];
					//∁E< good < 0.3 ≤ excel. ≤ 0.5 < good ≤ 0.6 < unsat. ≤ 1 < crit. < ∁E
					foreach($DebtRatioS as $k=>$v){
						if($v < 0.3) $DebtRatioS[$k] = 1;
						elseif($v >= 0.3 && $v <= 0.5) $DebtRatioS[$k] = 2;
						elseif($v > 0.5 && $v <= 0.6) $DebtRatioS[$k] = 1;
						elseif($v > 0.6 && $v <= 1) $DebtRatioS[$k] = -1;
						else $DebtRatioS[$k] = -2;
					}
					$DebtRatioAv = $this->fh->CalculateAverageScore($DebtRatioS);

					//Non-current Assets to Net Worth
					$NCAtoNW = array();
					for($x=@count($fa_report->balance_sheets)-1; $x >= 0 ; $x--){
						if($fa_report->balance_sheets[$x]->Equity != 0)
							$NCAtoNW[] = $fa_report->balance_sheets[$x]->NoncurrentAssets / $fa_report->balance_sheets[$x]->Equity;
						else
							$NCAtoNW[] = 0;
					}
					$NCAtoNWS = [
						$this->fh->CalculateSp($NCAtoNW),
						$NCAtoNW[@count($fa_report->balance_sheets)-1],
						$this->fh->CalculateSf($NCAtoNW)
					];
					//∁E< crit. < 0 ≤ excel. ≤ 1 < good ≤ 1.25 < unsat. ≤ 2 < crit. < ∁E
					foreach($NCAtoNWS as $k=>$v){
						if($v < 0) $NCAtoNWS[$k] = -2;
						elseif($v >= 0 && $v <= 1) $NCAtoNWS[$k] = 2;
						elseif($v > 1 && $v <= 1.25) $NCAtoNWS[$k] = 1;
						elseif($v > 1.25 && $v <= 2) $NCAtoNWS[$k] = -1;
						else $NCAtoNWS[$k] = -2;
					}
					$NCAtoNWAv = $this->fh->CalculateAverageScore($NCAtoNWS);

					//Current Ratio
					$CurrentRatio = array();
					for($x=@count($fa_report->balance_sheets)-1; $x >= 0 ; $x--){
						if($fa_report->balance_sheets[$x]->CurrentLiabilities != 0)
							$CurrentRatio[] = $fa_report->balance_sheets[$x]->CurrentAssets / $fa_report->balance_sheets[$x]->CurrentLiabilities;
						else
							$CurrentRatio[] = 0;
					}
					$CurrentRatioS = [
						$this->fh->CalculateSp($CurrentRatio),
						$CurrentRatio[@count($fa_report->balance_sheets)-1],
						$this->fh->CalculateSf($CurrentRatio)
					];
					//∁E< crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∁E
					foreach($CurrentRatioS as $k=>$v){
						if($v < 1) $CurrentRatioS[$k] = -2;
						elseif($v >= 1 && $v < 2) $CurrentRatioS[$k] = -1;
						elseif($v >= 2 && $v < 2.1) $CurrentRatioS[$k] = 1;
						else $CurrentRatioS[$k] = 2;
					}
					$CurrentRatioAv = $this->fh->CalculateAverageScore($CurrentRatioS);

					//QuickRatio
					$QuickRatio = array();
					for($x=@count($fa_report->balance_sheets)-1; $x >= 0 ; $x--){
						if($fa_report->balance_sheets[$x]->CurrentLiabilities != 0)
							$QuickRatio[] = ($fa_report->balance_sheets[$x]->CashAndCashEquivalents +
											$fa_report->balance_sheets[$x]->OtherCurrentFinancialAssets +
											$fa_report->balance_sheets[$x]->TradeAndOtherCurrentReceivables) /
											$fa_report->balance_sheets[$x]->CurrentLiabilities;
						else
							$QuickRatio[] = 0;
					}
					$QuickRatioS = [
						$this->fh->CalculateSp($QuickRatio),
						$QuickRatio[@count($fa_report->balance_sheets)-1],
						$this->fh->CalculateSf($QuickRatio)
					];
					//∁E< crit. < 0.5 ≤ unsat. < 1 ≤ good < 1.1 ≤ excel. < ∁E
					foreach($QuickRatioS as $k=>$v){
						if($v < 0.5) $QuickRatioS[$k] = -2;
						elseif($v >= 0.5 && $v < 1) $QuickRatioS[$k] = -1;
						elseif($v >= 1 && $v < 1.1) $QuickRatioS[$k] = 1;
						else $QuickRatioS[$k] = 2;
					}
					$QuickRatioAv = $this->fh->CalculateAverageScore($QuickRatioS);

					//Cash Ratio
					$CashRatio = array();
					for($x=@count($fa_report->balance_sheets)-1; $x >= 0 ; $x--){
						if($fa_report->balance_sheets[$x]->CurrentLiabilities != 0)
							$CashRatio[] = $fa_report->balance_sheets[$x]->CashAndCashEquivalents /
											$fa_report->balance_sheets[$x]->CurrentLiabilities;
						else
							$CashRatio[] = 0;
					}
					$CashRatioS = [
						$this->fh->CalculateSp($CashRatio),
						$CashRatio[@count($fa_report->balance_sheets)-1],
						$this->fh->CalculateSf($CashRatio)
					];
					//∁E< crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∁E
					foreach($CashRatioS as $k=>$v){
						if($v < 0.05) $CashRatioS[$k] = -2;
						elseif($v >= 0.05 && $v < 0.2) $CashRatioS[$k] = -1;
						elseif($v >= 0.2 && $v < 0.25) $CashRatioS[$k] = 1;
						else $CashRatioS[$k] = 2;
					}
					$CashRatioAv = $this->fh->CalculateAverageScore($CashRatioS);


					//Financial Condition

					//Return on equity (ROE)
					$ROE = [];
					$ROEDrivers = [];

					for ($x = @count($fa_report->income_statements) - 1; $x >= 0 ; $x--) {
						/** EQUITY AVERAGE */
						isset($fa_report->balance_sheets[$x]) ? $balanceSheetEquity1 = $fa_report->balance_sheets[$x]->Equity : $balanceSheetEquity1 = 0;
						isset($fa_report->balance_sheets[$x + 1]) ? $balanceSheetEquity2 = $fa_report->balance_sheets[$x + 1]->Equity : $balanceSheetEquity2 = 0;

	                    $equityAverage = ($balanceSheetEquity1 + $balanceSheetEquity2) / 2;

						/** ASSETS AVERAGE */
						isset($fa_report->balance_sheets[$x]) ? $balanceSheetAssets1 = $fa_report->balance_sheets[$x]->Assets : $balanceSheetAssets1 = 0;
						isset($fa_report->balance_sheets[$x + 1]) ? $balanceSheetAssets2 = $fa_report->balance_sheets[$x + 1]->Assets : $balanceSheetAssets2 = 0;

	                    $assetsAverage = ($balanceSheetAssets1 + $balanceSheetAssets2) / 2;

						/** REVENUE */
						isset($fa_report->income_statements[$x]->Revenue) ? $revenue = $fa_report->income_statements[$x]->Revenue : $revenue = 0;

						/** ROE */
						if ($equityAverage != 0) {
							$roeTemp = $fa_report->income_statements[$x]->ProfitLoss / $equityAverage;
							$ROE[] = $roeTemp;
	                    } else {
							$ROE[] = 0;
							$roeTemp = 0;
	                    }

						/** Financia Leverage */
						$sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
						$financialLeverage = 0;
						foreach($sustainability['financial_table'] as $key => $value){
							if($value->year == $fa_report->income_statements[$x]->Year){
								$financialLeverage = $value->FinancialLeverage;
							}
						}

						/** Net Income */
						isset($fa_report->income_statements[$x]->ProfitLossFromOperatingActivities) ? $net_income = $fa_report->income_statements[$x]->ProfitLossFromOperatingActivities : $net_income = 0;

						/** Profit Margin */
						$ProfitMargin = 0;
						if($fa_report->income_statements[$x]->Revenue != 0){
							$ProfitMargin = ($fa_report->income_statements[$x]->ProfitLoss / $fa_report->income_statements[$x]->Revenue) * 100;
						}

						/** Turnover Asset */	
						$AssetTurnover = 0.1;
						$turnoverRatios = FinancialAnalysisInternal::turnoverRatios($fa_report);
						foreach($turnoverRatios as $t){
							if($t->year == $fa_report->income_statements[$x]->Year){
								$AssetTurnover = $t->AssetTurnover;

								if($AssetTurnover != 0 ){
									if($t->num_days/$t->AssetTurnover >= 0.1) {
										$AssetTurnover = $t->num_days/$t->AssetTurnover;
									}
								}
							}
						}

						$ROEDrivers[] = array(
							'year' => $fa_report->income_statements[$x]->Year,
							'roe' => $roeTemp,
							'equity_average'		=> $equityAverage,
							'assets_average'		=> $assetsAverage,
							'revenue'				=> $revenue,
							'financial_leverage'	=> $financialLeverage,
							'net_income'			=> $net_income,
							'profit_margin'			=> $ProfitMargin,
							'turnover_asset'		=> $AssetTurnover
						);
					}

					$remove_array = array();
					foreach($ROEDrivers as $key => $value){
						$isZero = false;
						foreach($value as $key_value => $a){
							if($key_value == 'year' || $key_value == 'turnover_asset'){
								//
							}else{
								if((int)$a !== 0){
									$isZero = true;
								}
							}
						}
						if($isZero == false){
							array_push($remove_array, $key);
						}
					}
					foreach($remove_array as $rmv){
						unset($ROEDrivers[$rmv]);
					}

					$turnoverRatios = FinancialAnalysisInternal::turnoverRatios($fa_report);

					$ROES = [
						$this->fh->CalculateSp($ROE),
						$ROE[@count($fa_report->income_statements)-1],
						$this->fh->CalculateSf($ROE)
					];

					//∁E< crit. < 0 ≤ unsat. < 0.12 ≤ good < 0.2 ≤ excel. < ∁E
					foreach($ROES as $k=>$v){
						if($v < 0) $ROES[$k] = -2;
						elseif($v >= 0 && $v < 0.112) $ROES[$k] = -1;
						elseif($v >= 0.112 && $v < 0.128) $ROES[$k] = 0;
						elseif($v >= 0.128 && $v < 0.2) $ROES[$k] = 1;
						else $ROES[$k] = 2;
					}
					$ROEAv = $this->fh->CalculateAverageScore($ROES);

					//Return on assets (ROA)
					$ROA = [];

					for ($x = @count($fa_report->income_statements) - 1; $x >= 0 ; $x--) {
	                    if (isset($fa_report->balance_sheets[$x])) {
	                        $balanceSheetAssets1 = $fa_report->balance_sheets[$x]->Assets;
	                    } else {
	                        $balanceSheetAssets1 = 0;
	                    }

	                    if (isset($fa_report->balance_sheets[$x + 1])) {
	                        $balanceSheetAssets2 = $fa_report->balance_sheets[$x + 1]->Assets;
	                    } else {
	                        $balanceSheetAssets2 = 0;
	                    }

	                    $assetsAverage = ($balanceSheetAssets1 + $balanceSheetAssets2) / 2;

						if ($assetsAverage != 0) {
							$ROA[] = $fa_report->income_statements[$x]->ProfitLoss / $assetsAverage;
	                    } else {
							$ROA[] = 0;
	                    }
					}

					$ROAS = [
						$this->fh->CalculateSp($ROA),
						$ROA[@count($fa_report->income_statements)-1],
						$this->fh->CalculateSf($ROA)
					];

					//∁E< crit. < 0 ≤ unsat. < 0.06 ≤ good < 0.1 ≤ excel. < ∁E
					foreach($ROAS as $k=>$v){
						if($v < 0) $ROAS[$k] = -2;
						elseif($v >= 0 && $v < 0.056) $ROAS[$k] = -1;
						elseif($v >= 0.056 && $v < 0.064) $ROAS[$k] = 0;
						elseif($v >= 0.064 && $v < 0.1) $ROAS[$k] = 1;
						else $ROAS[$k] = 2;
					}
					$ROAAv = $this->fh->CalculateAverageScore($ROAS);

					//Sales Growth
					$SalesGrowth = array();
					$SGtotal = 0;
					for($x=@count($fa_report->income_statements)-1; $x >= 0 ; $x--){
						$SalesGrowth[] = $fa_report->income_statements[$x]->Revenue;
						$SGtotal += $fa_report->income_statements[$x]->Revenue;
					}
					$SGaverage = $SGtotal / @count($fa_report->income_statements);
					if($SGaverage != 0)
						$SalesGrowthS = [
							$this->fh->CalculateSlope($SalesGrowth, false) / $SGaverage,
							$this->fh->CalculateSlope($SalesGrowth, true) / $SGaverage,
							$this->fh->CalculateSf($SalesGrowth, true) / $SGaverage
						];
					else
						$SalesGrowthS = [0,0,0];
					/*
					<-0.3  Escore "-2";
					-0.3  E0.04  Escore "-1";
					-0.04  E0.04  Escore "0";
					0.04  E0.3  Escore "+1";
					> 0.3  Escore "+2"
					*/
					foreach($SalesGrowthS as $k=>$v){
						if($v < -0.3) $SalesGrowthS[$k] = -2;
						elseif($v >= -0.3 && $v < -0.04) $SalesGrowthS[$k] = -1;
						elseif($v >= -0.04 && $v < 0.04) $SalesGrowthS[$k] = 0;
						elseif($v >= 0.04 && $v < 0.3) $SalesGrowthS[$k] = 1;
						else $SalesGrowthS[$k] = 2;
					}
					$SalesGrowthAv = $this->fh->CalculateAverageScore($SalesGrowthS);

					// Final Scores

					//$DebtRatioAv $NCAtoNWAv $CurrentRatioAv $QuickRatioAv $CashRatioAv

					$FinancialPosition = ($DebtRatioAv * 0.3) +
										($NCAtoNWAv * 0.15) +
										($CurrentRatioAv * 0.2) +
										($QuickRatioAv * 0.2) +
										($CashRatioAv * 0.15);

					//$ROEAv $ROAAv $SalesGrowthAv

					$FinancialPerformance = ($ROEAv * 0.5) +
										($ROAAv * 0.3) +
										($SalesGrowthAv * 0.2);


					$FinalRating = ($FinancialPosition * 0.6) + ($FinancialPerformance * 0.4);
				} else {
					$fa_report = null;
				}
	        }
	        $financialChecker = false;
	        $finalRatingScore = array();
	        if(isset($fa_report->balance_sheets) == false && $fa_report){
	            $finalRatingScore = $this->fh->financialRatingComputation($fa_report->id);
	            if($finalRatingScore) {
	                $finalRatingScore[] = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
	                $financialChecker = true;
	            } 
	        }
			if($fa_report!=null && Auth::user()->role == 3){
				$FinancialPosition = Rationscore::raw('WHERE ROUND(score_range) = '.round($FinancialPosition,1))->first()->score_value;
				$FinancialPerformance = Rationscore::raw('WHERE ROUND(score_range) = '.round($FinancialPerformance,1))->first()->score_value;
			} else {
				$FinancialPosition = 0;
				$FinancialPerformance = 0;
	        }

	        $financialConditionHandler = new FinancialConditionHandler();
	        $financialCondition = $financialConditionHandler->getFinancialConditionByEntityId($entity->entityid);

	        /*  Forecast Slope Calculation  */
	        $forecast_data = NULL;
	        
	        $main_id = DB::table('tblentity')
	                        ->join('tblindustry_main', 'tblindustry_main.main_code', '=', 'tblentity.industry_main_id')
	                        ->where('entityid', $entity->entityid)
	                        ->pluck('tblindustry_main.industry_main_id')->first();

			if (NULL != $fa_report) {
	            $base_year      = $fa_report->year;
	            $gf_lib         = new GrowthForecastLib($entity->entityid, $main_id, $bank_standalone);
	            $forecast_data  = $gf_lib->getGrowthForecastData();

	           //print_r($forecast_data); exit;
	        }

	        $m_score    = array();
	        if (7 == $entity->status) {
	            $m_score    = $this->computeBeneishScore($entity->entityid);
	        }

	        if ((1 == $bank_standalone) && (7 == $entity->status) && (NULL != $fa_report)) {
	            $dscr_standalone = $this->calculateStaticDscr($entity->entityid);
	        }

	        $local_rate   = DB::table('config')
	            ->where('name', 'local_ir')
	            ->first();

	        $foreign_rate   = DB::table('config')
	            ->where('name', 'foreign_ir')
	            ->first();

	        $procurementTypes = [
	            AdminController::BANK_TYPE_CORPORATE,
	            AdminController::BANK_TYPE_GOVERNMENT,
	        ];
	        $isFinancing = !in_array($entity->bank_type, $procurementTypes);
	        $exportDSCR = $entity->export_DSCR;
	        if($bank_standalone == 0 ) $contBank = "no";
	        else $contBank = "yes";
	        array_push($finalRatingScore, $contBank);
	        // & in filename creates error
	        if (!empty($documents9)) {
	            if(strpos($documents9->document_rename,"&") !== false){
	                $fname  = preg_replace("/[&]/", "And", $documents9->document_rename);
	                $path = public_path()."/financial_analysis/";
	                if(!file_exists($path . $fname)){
	                    rename($path . $documents9->document_rename, $path . $fname );
	                }
	                $documents9->document_rename = $fname;
	            }
	        }

	        /*---------------------------
	        --------dropbox files-------
	        -----------------------------*/
	        $entityFiles = DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->get();
	        $folderUrl = DB::table('tbldropboxentity')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->first();
	        if($folderUrl == null || $folderUrl->is_deleted == 1) {
	            $folderUrl = '';
	        }

	        /*---------------------------
	        --------Financial Statement PDF files-------
	        -----------------------------*/
	        $fsPdfFiles = DB::table('innodata_files')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->get();
	        foreach($fsPdfFiles as $fspdf){
	            $fspdf->file_name = str_replace($entity->entityid . '-',"",$fspdf->file_name);
	        }

	        /*---------------------------
	        --------Utility Bill files-------
	        -----------------------------*/
	        $ubillFiles = DB::table('utility_bills')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->get();

	        $countEntityFiles = count($entityFiles);

	        $checklistFiles = ChecklistFile::select('checklist_files.id', 'type_id', 'file_name', 'code')->where('entity_id', $entity->entityid)->where('is_deleted', 0)->where('category', $entity->entity_type)
	            ->leftJoin('checklist_type', 'checklist_files.type_id', 'checklist_type.id')
	            ->get();

	        $files = array();
	        $files_id = array();

	        foreach ($checklistFiles as $checklistFile) {
	            $files[$checklistFile->code] = $checklistFile->file_name;
	        }
	        
	        foreach ($checklistFiles as $checklistFile) {
	            $files_id[$checklistFile->code] = $checklistFile->id;
	        }

	        $requiredChecklistTypes = ChecklistType::where('category', $entity->entity_type)->get();

	        $requiredChecklistCounter = array();

	        foreach ($requiredChecklistTypes as $requiredChecklistType) {
	            $reqChecklistFile = ChecklistFile::where('entity_id', $entity->entityid)->where('type_id', $requiredChecklistType->id)->where('is_deleted', 0)->first();
	            
	            $requiredChecklistCounter[$requiredChecklistType->code] = $reqChecklistFile ? 1 : 0;
	        }

	        /*---------------------------------------
			--------Letter of Authorization ---------
			---------------------------------------*/
	        $loa = DB::table('tbldocument')->where('entity_id', $entity->entityid)->where('document_group', 11)->where('is_deleted', 0)->first();

	        /*-------------------------------------------
			--------Industry Comparison Line Graph-------
	        ---------------------------------------------*/

			$industryComaprison = new IndustryComparison();
			$industryData = $industryComaprison->getIndustryComparison($id);

	        /** Set Value for Industry Line Graph */
	        $entity->gross_revenue_growth = $industryData['gross_revenue_growth'];
	        $entity->net_income_growth    = $industryData['net_income_growth'];
	        $entity->gross_profit_margin  = $industryData['gross_profit_margin'];
	        $entity->current_ratio        = $industryData['current_ratio'];
	        $entity->debt_equity_ratio    = $industryData['debt_equity_ratio'];
			$industry_year 				  = $industryData['year'];

			/** Get GRDP Values */
			$regionCode = Province2::where('provDesc', $entity->province)->pluck('regCode')->first();
			$industryGrdp = GRDP::where('regCode', $regionCode)->where('year', $industry_year)->first();

			if(empty($industryGrdp)){
				$GrdpYear = GRDP::where('regCode', $regionCode)->pluck('year');

				/** Get the closest year */
				$closest = null;
				foreach ($GrdpYear as $item) {
					if ($closest === null || abs((int)$industry_year - $closest) > abs($item - (int)$industry_year)) {
						$closest = $item;
					}
				}
				$industryGrdp = GRDP::where('regCode', $regionCode)->where('year', $closest)->first();
			}


	        if($fa_report != null && $fa_report != '' && !empty($fa_report)){

	            $financial = new FinancialReport();
	            $financialStatement = $financial->getFinancialReportById($id);

		        $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
	            $fa_report->income_statements   = $financialStatement['income_statements'];
		        $fa_report->cashflow            = $financialStatement['cashflow'];

	            /*---------------------------------------------------------------
	            --------Computation for the Industry Comparison (Company) -------
	            -----------------------------------------------------------------*/
	            $company_industry = array();

	            $max_count = count($fa_report->income_statements)-1;

	            /** Company Gross Revenue Growth */
	            $company_industry['gross_revenue_growth'] = 0;
	            if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
					$PercentageChange = 0;
	                $company_industry['gross_revenue_growth'] = 0;
	                $dividend = $fa_report->income_statements[$max_count]->Revenue;

					if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
                        $PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
                    }

					if($PercentageChange > 200){
						$company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
					}else{
						$company_industry['gross_revenue_growth'] = round($PercentageChange,2);
					}
	            }

	            /** Company Net Income Growth */
	            $company_industry['net_income_growth'] = 0;
	            if( isset($fa_report->income_statements[$max_count]->ProfitLoss) && (($fa_report->income_statements[$max_count]->ProfitLoss) != 0)){
					$PercentageChange = 0;
	                $dividend = ($fa_report->income_statements[$max_count]->ProfitLoss);

					if($dividend != 0  && $fa_report->income_statements[$max_count]->ProfitLoss != 0){
                        $PercentageChange = (($fa_report->income_statements[0]->ProfitLoss - $fa_report->income_statements[$max_count]->ProfitLoss) /  $dividend) * 100;
                    }

					if($PercentageChange > 200){
						$company_industry['net_income_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
					}else{
						$company_industry['net_income_growth'] = round($PercentageChange,2);
					}
	            }

	            /** Company Gross Profit Margin */
	            $company_industry['gross_profit_margin'] = 0;
				if($max_count >= 0){
					for($x=$max_count; $x >= 0 ; $x--){
						if($fa_report->income_statements[$x]->Revenue != 0){
							$company_industry['gross_profit_margin'] = ($fa_report->income_statements[$x]->GrossProfit / $fa_report->income_statements[$x]->Revenue) * 100;

							if($company_industry['gross_profit_margin'] == 0){
								$company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
							}else{
								$company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
								$company_industry['gross_profit_margin'] = (int)$company_industry['gross_profit_margin'];
							}
							
						}
					}
				}

	            /** Company Current Ratio */    
	            $company_industry['current_ratio'] = 0;
	            $i=0;
	            for($x=@count($fa_report->balance_sheets) - 1; $x >= 0 ; $x--){
	                $currentAssets = $fa_report->balance_sheets[$x]->CurrentAssets;
	                $currentLiabilities = $fa_report->balance_sheets[$x]->CurrentLiabilities;

	                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
	                if($currentLiabilities > 0){
	                    $company_industry['current_ratio'] = $currentAssets / $currentLiabilities;
	                    $company_industry['current_ratio'] = round($company_industry['current_ratio'], 2, PHP_ROUND_HALF_DOWN);
	                }
	                $i++;
	            }

	            /** Company Debt-to-Equity Ratio */
	            $company_industry['debt_to_equity'] = 0;
	            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
	            $company_industry['debt_to_equity'] = $sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage;
	        }

	        /*---------------------------
	        --------The Good and the Bad on Rating Summary Tab-------
			-----------------------------*/

			$keys = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();

	        /** Check KeyRatios Value for old report*/
	        if(count($keys) == 0){
	            $keyRatio = new KeyRatioService();
	            $keyRatio->generateKeyRatioByEntityId($entity->entityid);
	            $keySummary = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();
	        }else{
	            $keySummary = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();
			}

			$good_points = false;
			$bad_points= false;

			if(!empty($keySummary[0])){
				 /** Good */
				 if( ($keySummary[0]->NCAtoNW >= 0) && ($keySummary[0]->NCAtoNW <= 1) || ($keySummary[0]->NCAtoNW > 1) && ($keySummary[0]->NCAtoNW <= 1.25) ||
					 ($keySummary[0]->DebtRatio < 0.30) || ($keySummary[0]->DebtRatio >= 0.30 && $keySummary[0]->DebtRatio <= 0.50) ||
					 ($keySummary[0]->DebtRatio > 0.50 && $keySummary[0]->DebtRatio <= 0.60) ||
					 ($keySummary[0]->NWC > 0) && ((($keySummary[0]->InventoryNWC >= 0) && ($keySummary[0]->InventoryNWC <= 0.9)) || (($keySummary[0]->InventoryNWC > 0.9) && ($keySummary[0]->InventoryNWC <= 1.0))) ||
					 (($keySummary[0]->CurrentRatio >= 2) && ($keySummary[0]->CurrentRatio < 2.1)) || ($keySummary[0]->CurrentRatio >= 2.1) ||
					 (($keySummary[0]->QuickRatio >= 1) && ($keySummary[0]->QuickRatio < 1.1)) || ($keySummary[0]->QuickRatio >= 1.1) ||
					 (($keySummary[0]->CashRatio >= 0.20) && ($keySummary[0]->CashRatio < 0.25)) || ($keySummary[0]->CashRatio > 0.25) ||
					 (($keySummary[0]->ROE >= 0.12) && ($keySummary[0]->ROE <= 0.2)) || ($keySummary[0]->ROE > 0.2) ||
					 (($keySummary[0]->ROA >= 0.06) && ($keySummary[0]->ROA < 0.1)) || ($keySummary[0]->ROA > 0.1) ||
					 ($fa_report && $fa_report->balance_sheets && $fa_report->balance_sheets[0]->Equity > $fa_report->balance_sheets[count($fa_report->balance_sheets)-1]->Equity) ||
					 (($keySummary[0]->EBIT > 0) && ($keySummary[0]->EBIT > $keySummary[1]->EBIT)) || ($fa_report && ($fa_report->income_statements[0]->ComprehensiveIncome > 0))
				){ $good_points = true; }

				/** Bad  */
				if( (($keySummary[0]->DebtRatio > 0.6) && ($keySummary[0]->DebtRatio <= 1)) || (($keySummary[0]->NWC > 0) && ($keySummary[0]->InventoryNWC > 1.0)) ||
					($keySummary[0]->InventoryNWC > 1.0) || ($keySummary[0]->CurrentRatio < 1) || (($keySummary[0]->CurrentRatio >= 1) && ($keySummary[0]->CurrentRatio < 2)) ||
					($keySummary[0]->QuickRatio < 0.5) || ($keySummary[0]->QuickRatio >= 0.5 && $keySummary[0]->QuickRatio < 1) ||
					($keySummary[0]->CashRatio < 0.05) || (($keySummary[0]->CashRatio >= 0.05) && ($keySummary[0]->CashRatio < 0.20)) ||
					($keySummary[0]->ROE < 0) || (($keySummary[0]->ROE >= 0) && ($keySummary[0]->ROE < 0.12)) ||
					($keySummary[0]->ROA < 0) || (($keySummary[0]->ROA >= 0) && ($keySummary[0]->ROA < 0.06)) ||
					(($fa_report) && ($fa_report->balance_sheets[0]->IssuedCapital > 0) && ($keySummary[0]->NetAsset < $fa_report->balance_sheets[0]->IssuedCapital)) ||
					($keySummary[0]->EBIT < 0) || (($fa_report) && ($fa_report->income_statements[0]->ComprehensiveIncome < 0))
				){ $bad_points = true; }

			}

			// Check if report uses a client key
			$is_clientkey = false;
			$bank_keys = BankSerialKeys::where('entity_id', $entity->entityid)->first();
			if(!empty($bank_keys->entity_id)){
				$is_clientkey = true;
			}

			$is_discount = false;
			$discount = DB::table('discounts')->where(['discount_code'=>$entity->discount_code,'status' => 1])->first();

			if(!empty($discount->discount_code)){
				$is_discount = true;
			}

			$fpos = new FinancialCondition();
			$graphs = [];
			if($entity->status == 7)
				$graphs['fpos_graph'] = $fpos->createFCondition($entity->entityid);
			else
				$graphs['fpos_graph'] = '';
			
			$report_sustainability = DB::table('tblsustainability')
									->where('entity_id', '=', $entity->entityid)
									->first();

	        $returnValue = array(
	            'count'		                    => $countEntityFiles,
	            'loa'				            => $loa,
	            'files'                         => $files,
	            'requiredChecklistCounter'      => $requiredChecklistCounter,	
	            'files_id'                      => $files_id,
	            'entityFiles'                   => $entityFiles,
	            'folderUrl'                     => $folderUrl,
	            'fsPdfFiles'                    => $fsPdfFiles,
	            'ubillFiles'                    => $ubillFiles,
	            'login'                         => $login,
	            'majorcustomeraccount'          => $majorcustomeraccount,
	            'majorsupplieraccount'          => $majorsupplieraccount,
	            'prod_services'                 => $prod_services,
	            'majorcustomers'                => $majorcustomers,
	            'majorsuppliers'                => $majorsuppliers,
	            'majorlocations'                => $majorlocations,
	            'competitors'                   => $competitors,
	            'bizdrivers'                    => $bizdrivers,
	            'bizsegments'                   => $bizsegments,
	            'pastprojects'                  => $pastprojects,
	            'futuregrowths'                 => $futuregrowths,
	            'revenuepotential'              => $revenuepotential,
	            'capacityexpansion'             => $capacityexpansion,
	            'relatedcompanies'              => $relatedcompanies,
	            'capital'                       => $capital,
	            'certifications'                => $certifications,
	            'businesstype'                  => $businesstype,
	            'insuarance'                    => $insuarance,
	            'machineEquipments'              => $machineEquipments,
	            'slocation'                     => $slocation,
	            'branches'                      => $branches,
	            'sustainability'                => $report_sustainability,
	            'planfacility'                  => $planfacility,
	            'planfacilityrequested'         => $planfacilityrequested,
	            'owner'                         => $owner,
	            'fs_template'                   => $fs_template,
	            'fs_supporting'                 => $fs_supporting,
	            'documents2'                    => $documents2,
	            'documents3'                    => $documents3,
	            'documents9'                    => $documents9,
	            'readyrationList'               => $readyrationList->toArray(),
	            'readyrationPerformanceList'    => $readyrationPerformanceList,
	            'riskassessmentforcount'        => $riskassessmentforcount,
	            'riskassessment'                => $riskassessment,
	            'finalscore'                    => $finalscore,
	            'readyratiodata'                => $readyratiodata,
	            'progress'                      => $progress,
	            'investigator_notes'            => $investigator_notes,
	            'cycledetails'                  => $cycledetails,
	            'cycle_start_date'              => date('Y-m-01', strtotime($created->created_at)),
	            'shareholders'                  => $shareholders,
	            'directors'                     => $directors,
	            'requested_files'               => $requested_files,
	            'documents4'                    => $documents4,
	            'documents5'                    => $documents5,
	            'documents6'                    => $documents6,
	            'documents7'                    => $documents7,
	            'custom_doc'                    => $custom_doc,
	            'feedback_tab_val'              => $feedback_tab_val,
	            'feedback_tab_name'             => $feedback_tab_name,
	            'bank_ci_view'                  => $bank_ci_view,
	            'show_bank_rating'              => $show_bank_rating,
	            'grandscore'                    => $gscore,
	            'grdp_year'                     => GRDP::getYear(),
	            'private_link'                  => URL::to('/') . '/view_result/' . base64_encode(base64_encode($entity->entityid.'$encode')),
	            'fa_report'                     => $fa_report,
	            'fa_position'                   => $FinancialPosition,
	            'fa_performance'                => $FinancialPerformance,
	            'doc_req'                       => $doc_req,
	            'boi_cert'                      => $boi_cert,
	            'peza_cert'                     => $peza_cert,
	            'keymanagers'                   => $keymanagers,
	            'questionnaire_cnfg'            => $questionnaire_cnfg,
	            'forecast_data'                 => $forecast_data,
	            'bank_standalone'               => $bank_standalone,
	            'custom_docs'                   => $custom_docs,
	            'm_score'                       => $m_score,
	            'base_year'                     => $base_year,
	            'dscr_standalone'               => $dscr_standalone,
	            'local_rate'                    => $local_rate,
	            'foreign_rate'                  => $foreign_rate,
	            'is_financing'                  => $isFinancing,
	            'export_dscr'                   => $exportDSCR,
	            'finalRatingScore'              => $finalRatingScore,
	            'financialChecker'              => $financialChecker,
	            'financialCondition'            => $financialCondition,
	            'premium' 						=> false,
				'keySummary'					=> $keySummary,
				'good_points'					=> $good_points,
				'bad_points'					=> $bad_points,
				'balance_sheets'				=> !empty($fa_report->balance_sheets) ? $fa_report->balance_sheets : [],
				'income_statements'				=> !empty($fa_report->income_statements) ? $fa_report->income_statements : [],
	            'companyIndustry'               => (($fa_report != null) && ($fa_report != '') && (!empty($fa_report))) ? $company_industry : [],
	            'cash_flows' 					=> !empty($fa_report->cash_flows) ? $fa_report->cash_flows : [], 
	            'cf_net_profit_margin_percentage' => isset($cf_net_profit_margin_percentage) ? $cf_net_profit_margin_percentage:0,
	            'cf_asset_turnover_percentage'=> isset($cf_asset_turnover_percentage) ? $cf_asset_turnover_percentage : 0,
	            'cf_equity_multiplier_percentage' => isset($cf_equity_multiplier_percentage) ? $cf_equity_multiplier_percentage : 0,
	            'cf_dupont_analysis_percentage' => isset($cf_dupont_analysis_percentage) ? $cf_dupont_analysis_percentage : 0,
				'isClientKey'					=> $is_clientkey,
				'isDiscount'					=> $is_discount,
				'industryComparisonYear'		=> $industry_year,
				'industryGrdp'					=> $industryGrdp,
				'quickbooks_reports'			=> $quickbooks_reports,
				'graphs' 						=> $graphs,
				'ROEDrivers'					=> isset($ROEDrivers) ? $ROEDrivers : [],
				'turnoverRatios'				=> isset($turnoverRatios) ? $turnoverRatios : []
	        );
		
			$associatedBank = '';
			if(isset(Auth::user()->getAssociatedBank($entity->current_bank)->bank_name))
				$associatedBank = Auth::user()->getAssociatedBank($entity->current_bank)->bank_name;
			
	        $linkSummary = 'summary.smesummary';
	        if ($entity->current_bank == 1236 || $associatedBank == "Contractors Platform" || $entity->bank_name == "Contractors Platform" ) {
	            $returnValue['contractor_saved'] = array();
	            if(Auth::user()->role==5) {
	                $history = ContractorDocumentHistory::where("entity_id", $entity->entityid)->where("reviewer",Auth::user()->loginid)->where("is_deleted", 0)->first();
	                if($history && $history->count() > 0) {
	                    $saveData = explode(",", $history->data);
	                    $returnValue['contractor_saved'] = $saveData;
	                }
	            }
	            $linkSummary = 'summary.smesummary_contractor';
	        }

	        if($entity->is_premium == PREMIUM_REPORT){
	            $linkSummary = 'report.premium';
	            $returnValue['count']  = $countEntityFiles;
	            $returnValue['loa'] = $loa;
	        }

	        $deliverables = array(
	            'financial_rating'		=> 0,
	            'growth_forecast'		=> 0,
	            'industry_forecasting'	=> 0
	        );

	        $is_deliverables = json_encode($deliverables);

	        /** User logged as Supervisor */
			if(4 == Auth::user()->role){
				/**get supervisor organization */
				$spvsr_bank = User::where('loginid', Auth::user()->loginid)->first();
				$spvsr_organization = Bank::where('id', $spvsr_bank->bank_id)->first();

				if(!empty($spvsr_organization->deliverables)){
					$returnValue['deliverables'] = json_decode($spvsr_organization->deliverables);
				}else{
					$returnValue['deliverables'] = json_decode($is_deliverables);
				}
	        }
	        
	        /**User logged as admin */
			if(0 == Auth::user()->role){
				/**get clicked supervisor organization in admin */
				$spvsr_organization = Bank::where('id', Session::get('supervisor.bank_id'))->first();
	            if(Session::get('supervisor.bank_id') == null){
	                $var = Session::get('entity');
					/* Check if deliverables is empty */
	                $del = !empty($var->deliverables) ? $var->deliverables : $is_deliverables;

					$returnValue['deliverables'] = json_decode($del);
	            }else{
	                if(!empty($spvsr_organization->deliverables)){
	                    $returnValue['deliverables'] = json_decode($spvsr_organization->deliverables);
	                }else{
	                    $returnValue['deliverables'] = json_decode($is_deliverables);
	                }
	            }
	        }
	        // Contractor PCAB Document verifier
	        $responsibility = array();
	        if(Auth::user()->role == 5 ) {
	            $responsibility = explode(",", Auth::user()->responsibilities);
	        }
	        $entity->responsibilities = $responsibility;

	        /** Check if user is analyst */
	        if(Auth::user()->role == 3){
	            $linkSummary = 'report.premium_finan';
	        }

	        $entity_data    = DB::table('tblentity')
	                        ->select('main_title', 'sub_title', 'group_description', 'tblentity.industry_row_id', 'tblentity.industry_sub_id', 'tblentity.industry_main_id')
	                        ->where('entityid', $id)
	                        ->leftJoin('tblindustry_main', 'tblindustry_main.main_code', '=', 'tblentity.industry_main_id')
	                        ->leftJoin('tblindustry_sub', 'tblindustry_sub.sub_code', '=', 'tblentity.industry_sub_id')
	                        ->leftJoin('tblindustry_group', 'tblindustry_group.group_code', '=', 'tblentity.industry_row_id')
	                        ->first();

	        $entity->industry_row_id = $entity_data->industry_row_id;
	        $entity->industry_sub_id = $entity_data->industry_sub_id;
	        $entity->industry_main_id = $entity_data->industry_main_id;
	        $entity->main_title = $entity_data->main_title;
	        $entity->sub_title = $entity_data->sub_title;
	        $entity->row_title = $entity_data->group_description;

	        $keys = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();

	        /** Check KeyRatios Value for old report*/
	        if(count($keys) == 0){
	            $keyRatio = new KeyRatioService();
	            $keyRatio->generateKeyRatioByEntityId($entity->entityid);
	            $keySummary = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();
	        }else{
	            $keySummary = ReadyratioKeySummary::where('entityid',$entity->entityid)->orderBy('id', 'desc')->get();
	        }

			$zipcode = Entity::where('entityid', $entity->entityid)->pluck('zipcode')->first();
			$entity->zipcode = $zipcode;
			
	        $returnValue['keySummary'] = $keySummary;

	        if($fa_report != null && $fa_report != ''){
	        	$returnValue['balance_sheets'] = $returnValue['fa_report']->balance_sheets;
	        	$returnValue['income_statements'] = $returnValue['fa_report']->income_statements;
	    	}

	        // echo '<pre>';
	        // print_r($returnValue['fa_report']->balance_sheets);
	        // echo '</pre>';
	        // exit;
	    	// print_r($forecast_data); exit;
	        return View::make($linkSummary, $returnValue)->with('entity', $entity);
    	}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 43.24: exportToSummary
    //      exports the data to summary
    //-----------------------------------------------------
    public function exportToSummary($id)
    {
    	try{
	        $entity = Entity::where('entityid', $id)->first();

	        $entity->export_DSCR = 1;
	        $entity->save();
	        $company_name = $entity->companyname;

	        return Redirect::to('/summary/'.$id.'/'.$entity->loginid.'/'.urlencode($company_name));
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 43.25: exportToSummaryFinal
    //      exports the data to summary
    //-----------------------------------------------------
    public function exportToSummaryFinal($id)
    {
    	try{
	        $entity = Entity::where('entityid', $id)->first();

	        if($entity->export_DSCR == 1){
				// Regenerate Rating Summary PDF
				if(($entity->cbpo_pdf != "") && (file_exists(public_path('documents/' . $entity->cbpo_pdf)))){
					unlink(public_path('documents/' . $entity->cbpo_pdf));
				}
				Entity::where('entityid', $id)->update(['cbpo_pdf' => '']);
				$entity->export_DSCR = 0;
			}else{
				$entity->export_DSCR = 1;
			}

	        $entity->save();
	        $company_name = $entity->companyname;

	        $data = [
	        	'company_name' => urlencode($company_name),
	        	'loginid' => $entity->loginid
	        ];

	        return json_encode($data);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.3: getRequestAdditionalInfo
    //      Display CI Request Additional info form
    //-----------------------------------------------------
    public function getRequestAdditionalInfo($id)
	{
		try{
        	return View::make('sme.request-additional-info', array('entityid' => $id));
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 45.4: postRequestAdditionalInfo
    //      HTTP Post to validate and save Request
    //      for additional information
    //-----------------------------------------------------
	public function postRequestAdditionalInfo()
	{
		try{
	        /*  Variable Declaration    */
	        $entityid = Input::get('entityid');

	        /*  Sets validation rules and messages  */
	        $messages = array(
	            'message_body.required' => 'The message field is required.',
	        );

	        $rules = array(
		        'message_body'	=> 'required',
		    );

	        $validator = Validator::make(Input::all(), $rules, $messages);

	        /*  Validation rules failed */
	        if ($validator->fails()) {
	            return Redirect::to('/sme/request-additional-info/' . $entityid)->withErrors($validator)->withInput();
	        }
	        /*  Validation rules Successful */
	        else {
	            /*  Get report information  */
	            $entity = DB::table('tblentity')
	                ->where('tblentity.entityid', '=', $entityid)
	                ->first();

	            /*  Get account information  */
	            $user   = DB::table('tbllogin')
	                ->where('loginid', $entity->loginid)
	                ->first();

	            /*  Sets email configuration    */
	            $GLOBALS['toEmail']     = $entity->email;
	            $GLOBALS['toName']      = $entity->companyname;
	            $GLOBALS['toUserName']  = $user->firstname.' '.$user->lastname;

	            $messageBody = nl2br(Input::get('message_body'));

	            /*  Send email to the user  */
	            if (env("APP_ENV") != "local") {
	                try{
	                    Mail::send('emails.request-additional-info', array('message_body' => $messageBody, 'username' => $GLOBALS['toUserName']), function($message){
	                        $message->to($GLOBALS['toEmail'], $GLOBALS['toName']);
	                        $message->subject('CreditBPO Analysis: Additional Documents Needed');

	                        $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
	                        $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
	                    });
	                }catch(Exception $e){
	                //
	                }

	            }

	            return Redirect::to('/sme/request-additional-info/' . $entityid)->with('success', 'Email has been sent.');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.5: getSmeSummaryTbl
    //      Summary Table on the Submit for Risk Rating Tab
    //-----------------------------------------------------
    public function getSmeSummaryTbl($entity_id)
    {

    	try{
	        /** Variable Declaration    */
	        $sme_form_complete      = STS_OK;   // Form Complete Flag
	        $required               = array();  // Required Sections
	        $hidden                 = array();  // Hidden Sections
	        $msg                    = trans('messages.summary_tb_reminder_ok');

	        /** Initialize Server Response  */
	        $srv_resp['sts']        = STS_NG;
	        $srv_resp['messages']   = STR_EMPTY;

	        /* Get Report data from the database    */
	        $entity_data    = DB::table('tblentity')
	            ->where('entityid', $entity_id)
	            ->first();

	        /* Get Bank data from the database    */
	        $bank_data      = DB::table('tblbank')
	            ->where('id', $entity_data->current_bank)
	            ->first();

	        /* Business Details 1   */
	        $biz1['counter']['company_tin']                 = 1;
	        $biz1['counter']['business_registration']       = 1;
	        $biz1['counter']['registration_date']           = 1;
	        $biz1['counter']['registration_docs']           = 1;
	        $biz1['counter']['permit_to_operate']           = 1;
	        $biz1['counter']['authorized_signatory_owner']  = 1;
	        $biz1['counter']['tax_registration']            = 1;
	        $biz1['counter']['income_tax_return']           = 1;
	        // $biz1['counter']['industry']                    = 1;
	        $biz1['counter']['total_asset_grouping']        = 1;
	        $biz1['counter']['company_email']               = 1;
	        $biz1['counter']['company_address']             = 1;

	        if($entity_data->countrycode == 'PH')
	        	$biz1['counter']['primary_business_province']   = 1;
	        
	        $biz1['counter']['primary_business_telephone']  = 1;
	        $biz1['counter']['employee_size']               = 1;
	        $biz1['counter']['fs_input_who']                = 1;
	        $biz1['counter']['number_year_management_team'] = 1;

	        if ((NULL == $entity_data->company_tin) || (STR_EMPTY == $entity_data->company_tin)) {
	            $biz1['counter']['company_tin']  = 0;
	        }

	        if ((NULL == $entity_data->tin_num) || (STR_EMPTY == $entity_data->tin_num) ){
	            $biz1['counter']['business_registration']  = 0;
	        }

	        if ('0000-00-00' == $entity_data->sec_reg_date) {
	            $biz1['counter']['registration_date']  = 0;
	        }

	        if (STR_EMPTY == $entity_data->permit_to_operate) {
	            $biz1['counter']['permit_to_operate']  = 0;
	        }

	        if ((NULL == $entity_data->authority_to_borrow) || (STR_EMPTY == $entity_data->authority_to_borrow)) {
	            $biz1['authority_to_borrow']    = 0;
	        }

	        if (STR_EMPTY == $entity_data->tax_registration) {
	            $biz1['counter']['tax_registration']  = 0;
	        }

	        if (STR_EMPTY == $entity_data->income_tax) {
	            $biz1['counter']['income_tax_return']  = 0;
	        }

	        // if (0 == $entity_data->industry_main_id) {
	        //     $biz1['counter']['industry']  = 0;
	        // }

	        if ((NULL == $entity_data->total_asset_grouping) || (0 == $entity_data->total_asset_grouping)) {
	            $biz1['counter']['total_asset_grouping']  = 0;
	        }

	        if ((NULL == $entity_data->email) || (STR_EMPTY == $entity_data->email)) {
	            $biz1['counter']['company_email']  = 0;
	        }

	        if ((NULL == $entity_data->address1) || (STR_EMPTY == $entity_data->address1)) {
	            $biz1['counter']['company_address']  = 0;
	        }

	        if ($entity_data->countrycode == 'PH' && ((NULL == $entity_data->province) || (STR_EMPTY == $entity_data->province))) {
	             $biz1['counter']['primary_business_province']  = 0;
	        }

	        if ((NULL == $entity_data->phone) || (STR_EMPTY == $entity_data->phone)) {
	            $biz1['counter']['primary_business_telephone']  = 0;
	        }

	        if ((NULL === $entity_data->employee_size) || (STR_EMPTY === $entity_data->employee_size)) {
	            $biz1['counter']['employee_size']  = 0;
	        }

	        if (((NULL === $entity_data->fs_input_who) || (STR_EMPTY === $entity_data->fs_input_who)) && $entity_data->is_premium == PREMIUM_REPORT) {
	            $biz1['counter']['fs_input_who']  = 0;
	        }

	        $biz1['counter']['related_company']             = DB::table('tblrelatedcompanies')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['products_services_offered']   = DB::table('tblservicesoffer')->where('entity_id', $entity_id)->count();

	        $biz1['counter']['main_locations']              = DB::table('tbllocations')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['branches']                    = DB::table('tblbranches')->where('entity_id', $entity_id)->count();

	        if (BIZ_ENTITY_CORP == $entity_data->entity_type) {

	            if (STR_EMPTY == $entity_data->sec_cert) {
	                $biz1['counter']['registration_docs']  = 0;
	            }

	            $biz1['counter']['capital_details']             = DB::table('tblcapital')->where('entity_id', $entity_id)->count();
	        }
	        else {
	            if ((NULL == $entity_data->authorized_signatory) || (STR_EMPTY == $entity_data->authorized_signatory)) {
	                $biz1['counter']['authorized_signatory_owner']  = 0;
	            }
	        }

	        $biz1['counter']['certifications']              = DB::table('tblcertifications')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['import_export']               = DB::table('tblbusinesstype')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['insurance_coverage']          = DB::table('tblinsurance')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['cash_conversion']             = DB::table('tblcycledetails')->where('entity_id', $entity_id)->count();
	        $biz1['counter']['fspdf_file']                  = DB::table('innodata_files')->where('entity_id', $entity_id)->where('is_deleted', 0)->count();
	        $biz1['counter']['fs_template']                 = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', '=', 5)->count();
	        $biz1['counter']['balance_sheet']               = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', 1)->count();
	        $biz1['counter']['income_statement']            = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', 2)->count();
	        $biz1['counter']['bank_statement']              = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 51)->count();
	        $biz1['counter']['utility_bill']                = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 61)->count();

	        /* Business Details 2   */
	        $biz2['counter']['major_customer']              = DB::table('tblmajorcustomer')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['major_supplier']              = DB::table('tblmajorsupplier')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['major_market_location']       = DB::table('tblmajorlocation')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['competitors']                 = DB::table('tblcompetitor')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['bdriver']                     = DB::table('tblbusinessdriver')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['customer_segment']            = DB::table('tblbusinesssegment')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['past_proj_completed']         = DB::table('tblprojectcompleted')->where('entity_id', $entity_id)->count();
	        $biz2['counter']['future_growth_initiatives']   = DB::table('tblfuturegrowth')->where('entity_id', $entity_id)->count();

	        /* Assoc Files   */
	        $requiredChecklistTypes = ChecklistType::where('category', $entity_data->entity_type)->get();

	        $requiredChecklistCounter = array();

	        foreach ($requiredChecklistTypes as $requiredChecklistType) {
	            $reqChecklistFile = ChecklistFile::where('entity_id', $entity_id)->where('type_id', $requiredChecklistType->id)->where('is_deleted', 0)->first();

	            $assocFiles['counter'][$requiredChecklistType->code] = $reqChecklistFile ? 1 : 0;
	        }

	        /* Owner Details    */
	        $owner['counter']['key_manager']    = 0;
	        $owner['counter']['owner_educ']     = 0;
	        $owner['counter']['owner_wife']     = 0;
	        $owner['counter']['wife_educ']      = 0;

	        if (BIZ_ENTITY_SOLE == $entity_data->entity_type) {
	            $owner_inf                          = DB::table('tblowner')->where('entity_id', $entity_id)->first();
	            $owner['counter']['owner_inf']      = @count($owner_inf);
	            $owner['counter']['shareholder']    = 1;
	            $owner['counter']['director']       = 1;

	            if (1 <= $owner['counter']['owner_inf']) {
	                $owner['counter']['key_manager']    = DB::table('tblkeymanagers')->where('entity_id', $entity_id)->count();
	                $owner['counter']['owner_educ']     = DB::table('tbleducation')->where('entity_id', $entity_id)->where('ownerid', $owner_inf->ownerid)->count();

	                $spouse_inf = DB::table('tblspouse')->where('entity_id', $entity_id)->where('ownerid', $owner_inf->ownerid)->first();
	                $owner['counter']['owner_wife']  = @count($spouse_inf);

	                if (1 <= $owner['counter']['owner_wife']) {
	                    $owner['counter']['wife_educ']   = DB::table('tbleducation')->where('entity_id', $entity_id)->where('ownerid', $spouse_inf->spouseid)->count();
	                }
	            }
	        }
	        else {
	            $owner['counter']['owner_inf']      = 1;
	            $owner['counter']['owner_educ']     = 1;
	            $owner['counter']['owner_wife']     = 1;
	            $owner['counter']['wife_educ']      = 1;
	            $owner['counter']['key_manager']    = DB::table('tblkeymanagers')->where('entity_id', $entity_id)->count();
	            $owner['counter']['shareholder']    = DB::table('tblshareholders')->where('entity_id', $entity_id)->count();
	            $owner['counter']['director']       = DB::table('tbldirectors')->where('entity_id', $entity_id)->count();
	        }

	        /* Existing Credit Facilities   */
	        $ecf['counter']['credit_facilities']     = DB::table('tblplanfacility')->where('entity_id', $entity_id)->count();

	        
	        /* Condition and Sustainability */
	        $cns['counter']['succession']               = 0;
	        $cns['counter']['competitive_landscape']    = 0;
	        $cns['counter']['economic_factor']          = 0;
	        $cns['counter']['tax_payments']             = 0;

	        $cns_data   = DB::table('tblsustainability')->where('entity_id', $entity_id)->first();

	        if (1 <= @count($cns_data)) {
	            if (NULL != $cns_data->succession_plan) {
	                $cns['counter']['succession']    = 1;
	            }

	            if (NULL != $cns_data->competition_landscape) {
	                $cns['counter']['competitive_landscape']    = 1;
	            }

	            if (NULL != $cns_data->ev_susceptibility_economic_recession) {
	                $cns['counter']['economic_factor']  = 1;
	            }

	            if (NULL != $cns_data->tax_payments) {
	                $cns['counter']['tax_payments']  = 1;
	            }
	        }

	        $cns['counter']['risk_assessment']      = DB::table('tblriskassessment')->where('entity_id', $entity_id)->count();
	        $cns['counter']['revenue_growth']       = DB::table('tblrevenuegrowthpotential')->where('entity_id', $entity_id)->count();
	        $cns['counter']['capital_expansion']    = DB::table('tblcapacityexpansion')->where('entity_id', $entity_id)->count();

	        /* Required Credit Line */
	        $rcl['counter']['requested_cf']         = DB::table('tblplanfacilityrequested')->where('entity_id', $entity_id)->count();

	        $sme_tabs[] = $biz1;
	        $sme_tabs[] = $biz2;

	        if(!empty($assocFiles))
	            $sme_tabs[] = $assocFiles;
	        
	        $sme_tabs[] = $owner;
	        $sme_tabs[] = $ecf;
	        $sme_tabs[] = $cns;
	        $sme_tabs[] = $rcl;

	        self::getRequiredFields($entity_id, $required, $hidden);

			/*  Override for standalone */
	        $bank_standalone = ($bank_data && $bank_data->is_standalone==1) ? 1 : 0;
			if($bank_standalone==1){
				$required   = array(
					// 'industry',
					//'primary_business_province',
	                'employee_size',
	                'fs_input_who',
	                'fspdf_file',
				);

				if($entity_data->countrycode == 'PH'){
					$required[] = 'primary_business_province';
				}

	            foreach ($requiredChecklistTypes as $key => $requiredChecklistType){
	                $required[] = $requiredChecklistType->code;
	            }
	            
	            if (0 == $entity_data->anonymous_report) {
	                $required[] = 'company_tin';
	                $required[] = 'business_registration';
	                $required[] = 'registration_date';
	                $required[] = 'company_address';
	                $required[] = 'company_email';
	                $required[] = 'primary_business_telephone';
	                $required[] = 'number_year_management_team';
	                $required[] = 'date_established';
	            }

				$hidden = array(
					'permit_to_operate',
					'authority_to_borrow',
					'tax_registration',
					'income_tax_return',
					'related_company',
					'products_services_offered',
					'main_locations',
					'branches',
					'capital_details',
					'registration_docs',
					'authorized_signatory_owner',
					'certifications',
					'import_export',
					'insurance_coverage',
				);

				foreach ($biz1['counter'] as $key => $field) {
					if(in_array($key, $required)){
						if (0 >= $field) {
							$sme_form_complete = STS_NG;
							$msg    = trans('messages.summary_tb_reminder_ng');

							/** Break the loop when one incomplete field is found   */
							break;
						}
					}
	            }
	            $is_major = 1;
	            $machineEquipCheck = STS_OK;
	            $assocFilesCheck = STS_OK;
	            if (Auth::user()->is_contractor == 1) {
	                
	                /** For associated files */
	                if(!empty($assocFiles)){
	                    foreach ($assocFiles['counter'] as $key => $field) {
	                        if(in_array($key, $required)){
	                            if (0 >= $field) {
	                                $sme_form_complete = STS_NG;
	                                $msg    = trans('messages.summary_tb_reminder_ng');
	                                
	                                $assocFilesCheck = STS_NG;
	                                /** Break the loop when one incomplete field is found   */
	                                break;
	                            }
	                        }
	                    }
	                }

	                /** Add two major fields as required */
	                $required[] = 'major_customer';
	                $required[] = 'major_supplier';
	                $required[] = 'machine_equipment';

	                $major_customer = DB::table('tblmajorcustomer')->where('entity_id', $entity_id)->where('is_deleted', 0)->count();
	                $major_supplier = DB::table('tblmajorsupplier')->where('entity_id', $entity_id)->where('is_deleted', 0)->count();
	                $machineEquipmentCount = MachineEquipment::where('entity_id', $entity_id)->where('is_deleted', 0)->count();

	                if($major_customer < 1){
	                    $biz2['counter']['major_customer'] = 0;
	                    $is_major = 0;
	                }else{
	                    $biz2['counter']['major_customer'] = 1;
	                }

	                if($major_supplier < 1){
	                    $biz2['counter']['major_supplier'] = 0;
	                    $is_major = 0;
	                }else{
	                    $biz2['counter']['major_supplier'] = 1;
	                }

	                if($machineEquipmentCount < 1){
	                    $biz2['counter']['machine_equipment'] = 0;
	                    $machineEquipCheck = STS_NG;
	                }else{
	                    $biz2['counter']['machine_equipment'] = 1;
	                }

	                foreach($biz2['counter'] as $key => $value){
	                    if(in_array($key, $required)){
	                        if(0 >= $value){
	                            $sme_form_complete = STS_NG;
	                            $msg    = trans('messages.summary_tb_reminder_ng');

	                            /** Break the loop when one incomplete field is found   */
	                            break;
	                        }
	                    }
	                }
	            }

			}
	        else {
				/*--------------------------------------------------------------------
				/*	Determine if there is at least one incomplete field
				/*------------------------------------------------------------------*/
				foreach ($sme_tabs as $tab) {

					foreach ($tab['counter'] as $key => $field) {
						if (0 >= $field && in_array($key, $required)) {
							$sme_form_complete = STS_NG;
							$msg    = trans('messages.summary_tb_reminder_ng');

							/** Break the loop when one incomplete field is found   */
							break;
						}
					}

					if (STS_NG == $sme_form_complete) {
						/** Break the loop when one incomplete field is found   */
						break;
					}
				}
			}

	        $custom_docs    = $this->getRequiredCustomDocs($entity_id);

	        if (1 <= @count($custom_docs)) {
	            $sme_form_complete  = STS_NG;
	            $msg                = trans('messages.summary_tb_reminder_ng');
	        }

	        /* For API Call. Encode output in JSON Format   */
	        if ((Input::has('action')) && (Input::has('section'))) {
	            $srv_resp['sts']        = STS_OK;
	            $srv_resp['messages']   = $msg;

	            $srv_resp['data']['biz1']   = $biz1;
	            $srv_resp['data']['biz2']   = $biz2;
	            $srv_resp['data']['assocFiles']   = $assocFiles;
	            $srv_resp['data']['owner']  = $owner;
	            $srv_resp['data']['ecf']    = $ecf;
	            $srv_resp['data']['cns']    = $cns;
	            $srv_resp['data']['rcl']    = $rcl;
	            $srv_resp['data']['req']    = $required;

	            return json_encode($srv_resp);
	        }
	        else if (Session::has('trial_summary')) {
	            Session::forget('trial_summary');

	            $data['biz1']   = $biz1;
	            $data['biz2']   = $biz2;
	            $data['assocFiles']   = !(empty($assocFiles)) ? $assocFiles:[];
	            $data['owner']  = $owner;
	            $data['ecf']    = $ecf;
	            $data['cns']    = $cns;
	            $data['rcl']    = $rcl;

	            return $data;
	        }
	        else {
	            return View::make('summary.inc-summary-tb',
	                array(
	                    'biz1'              => $biz1,
	                    'biz2'              => $biz2,
	                    'assocFilesCheck'   => $assocFilesCheck,
	                    'assocFiles'        => !(empty($assocFiles)) ? $assocFiles:[],
	                    'owner'             => $owner,
	                    'ecf'               => $ecf,
	                    'cns'               => $cns,
	                    'rcl'               => $rcl,
	                    'comp'              => $sme_form_complete,
	                    'msg'               => $msg,
	                    'req'               => $required,
	                    'hide'              => $hidden,
	                    'bank_standalone'   => $bank_standalone,
	                    'custom_docs'       => $custom_docs,
	                    'is_major'          => $is_major,
	                    'machineEquipCheck' => $machineEquipCheck,
	                )
	            );
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

        //-----------------------------------------------------
    //  Function 45.5: getSmeSummaryTblPremium
    //      Summary Table on the Submit for Risk Rating Tab
    //-----------------------------------------------------
    public function getSmeSummaryTblPremium($entity_id)
    {
    	try{
	        /** Variable Declaration    */
	        $sme_form_complete      = STS_OK;   // Form Complete Flag
	        $required               = array();  // Required Sections
	        $hidden                 = array();  // Hidden Sections
	        $msg                    = trans('messages.summary_tb_reminder_ok_premium');
	        $msgOk                  = 1;

	        /** Initialize Server Response  */
	        $srv_resp['sts']        = STS_NG;
	        $srv_resp['messages']   = STR_EMPTY;

	        /* Get Report data from the database    */
	        $entity_data    = DB::table('tblentity')
	            ->where('entityid', $entity_id)
	            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
	            ->first();

	        $procurementTypes = [
	            AdminController::BANK_TYPE_CORPORATE,
	            AdminController::BANK_TYPE_GOVERNMENT,
	        ];
	        $isFinancing = !in_array($entity_data->bank_type, $procurementTypes);

	        /* Business Details 1   */
	        $biz1['counter']['company_tin']                 = 1;
	        $biz1['counter']['business_registration']       = 1;
	        $biz1['counter']['registration_date']           = 1;
	        $biz1['counter']['company_email']               = 1;
	        $biz1['counter']['company_address']             = 1;
	        //$biz1['counter']['primary_business_province']   = 1;
	        $biz1['counter']['primary_business_telephone']  = 1;
	        $biz1['counter']['employee_size']               = 1;

	        if ((NULL == $entity_data->company_tin) || (STR_EMPTY == $entity_data->company_tin)) {
	            $biz1['counter']['company_tin']  = 0;
	        }

	        if ((NULL == $entity_data->tin_num) || (STR_EMPTY == $entity_data->tin_num) ){
	            $biz1['counter']['business_registration']  = 0;
	        }

	        if ('0000-00-00' == $entity_data->sec_reg_date) {
	            $biz1['counter']['registration_date']  = 0;
	        }

	        if ((NULL == $entity_data->email) || (STR_EMPTY == $entity_data->email)) {
	            $biz1['counter']['company_email']  = 0;
	        }

	        if ((NULL == $entity_data->address1) || (STR_EMPTY == $entity_data->address1)) {
	            $biz1['counter']['company_address']  = 0;
	        }

	        $biz1['counter']['fspdf_file'] = DB::table('innodata_files')->where('entity_id', $entity_id)->where('is_deleted', 0)->count();

			$biz1['counter']['fs_input_template'] = DB::table('tbldocument')->where('entity_id', $entity_id)->where('is_deleted', 0)->where('document_group', 21)->count();

			/** Check if disabled premium report */
			$isPremiumProcess = DB::table('admin_settings')->where('id', 1)->first();
			if($isPremiumProcess){
				if($isPremiumProcess->is_innodata_process_premium_reports == 2){
					//
				}else{
					$biz1['counter']['fs_input_template']	= 1;
				}
			}

	        // if ((NULL == $entity_data->province) || (STR_EMPTY == $entity_data->province)) {
	        //     $biz1['counter']['primary_business_province']  = 0;
	        // }

	        if ((NULL == $entity_data->phone) || (STR_EMPTY == $entity_data->phone)) {
	            $biz1['counter']['primary_business_telephone']  = 0;
	        }

	        if ((NULL === $entity_data->employee_size) || (STR_EMPTY === $entity_data->employee_size)) {
	            $biz1['counter']['employee_size']  = 0;
	        }

	        $sme_tabs[] = $biz1;

	        self::getRequiredFieldsPremium($entity_id, $required, $hidden);

				/*--------------------------------------------------------------------
				/*	Determine if there is at least one incomplete field
				/*------------------------------------------------------------------*/
				foreach ($sme_tabs as $tab) {

					foreach ($tab['counter'] as $key => $field) {
						if (0 >= $field && in_array($key, $required)) {
							$sme_form_complete = STS_NG;
	                        $msg    = trans('messages.summary_tb_reminder_ng');
	                        $msgOk  = 0;

							/** Break the loop when one incomplete field is found   */
							break;
						}
					}

					if (STS_NG == $sme_form_complete) {
						/** Break the loop when one incomplete field is found   */
						break;
					}
				}

	        /* For API Call. Encode output in JSON Format   */
	        if ((Input::has('action')) && (Input::has('section'))) {
	            $srv_resp['sts']        = STS_OK;
	            $srv_resp['messages']   = $msg;

	            $srv_resp['data']['biz1']   = $biz1;
	            $srv_resp['data']['req']    = $required;

	            return json_encode($srv_resp);
	        }
	        else {
	            return View::make('summary.inc-summary-tb-premium',
	                array(
	                    'biz1'              => $biz1,
	                    'comp'              => $sme_form_complete,
	                    'msg'               => $msg,
	                    'msgOk'             => $msgOk,
	                    'req'               => $required,
	                    'hide'              => $hidden,
	                    'entity'            => $entity_data,
	                    'is_financing'      => $isFinancing,
	                )
	            );
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}    
    }

    //-----------------------------------------------------
    //  Function 45.6: getGRDPBoost
    //      GRDP Boost Calculation
    //-----------------------------------------------------
	public function getGRDPBoost()
	{
		try{
	        /* Variable Declaration */
			$year       = Input::get('year');           /*  Year chosen */
			$entity_id  = Input::get('entity_id');      /*  Report      */

	        /* Get Report Information from the database */
	        $entity     = Entity::where('entityid', $entity_id)
	            ->leftJoin('refcitymun', 'tblentity.cityid', 'refcitymun.id')
	            ->leftjoin('refregion', 'refcitymun.regDesc', 'refregion.regCode')
				->first();

	        /* Calculate GRDP according to the Region of Business  */
	        $region     = $entity->regCode;
			$boost      = GRDP::getBoost($year, $region);

			return array(
				'boost' => $boost,
				'region' => $region
			);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 45.7: getUnfilledReminder
    //      Tab required fields reminder
    //-----------------------------------------------------
    public function getUnfilledReminder($entity_id, $tab_name)
    {
    	try{
	        /*  Variable Declaration    */
	        $sme_form_complete  = STS_OK;       // Tab Complete Flag
	        $trans_file         = STR_EMPTY;
	        $required           = array();      // Required Fields
	        $hidden             = array();      // Hidden Fields
	        $unfilled           = array();      // Unfilled Fields
	        $custom_docs        = array();      // Required Custom Documents
	        $msg                = 'Thank you for completing this tab.';

	        $required           = self::getRequiredFields($entity_id, $required, $hidden);
	        $entity_data        = DB::table('tblentity')
	            ->where('entityid', $entity_id)
	            ->first();

	        /*  Business Details 1  */
	        if ('nav_registration1' == $tab_name) {
	            $trans_file = 'business_details1';

	            $unfilled['company_tin']                    = 1;
	            $unfilled['business_registration']          = 1;
	            $unfilled['registration_date']              = 1;
	            $unfilled['registration_docs']              = 1;
	            $unfilled['permit_to_operate']              = 1;
	            $unfilled['authorized_signatory_owner']     = 1;
	            $unfilled['tax_registration']               = 1;
	            $unfilled['income_tax_return']              = 1;
	            $unfilled['industry']                       = 1;
	            $unfilled['total_asset_grouping']           = 1;
	            $unfilled['company_email']                  = 1;
	            $unfilled['company_address']                = 1;
	            //$unfilled['primary_business_province']      = 1;
	            $unfilled['primary_business_telephone']     = 1;
	            $unfilled['date_established']               = 1;
	            $unfilled['employee_size']                  = 1;
	            $unfilled['fs_input_who']                   = 1;

	            $unfilled['number_year_management_team']    = 1;

	            if ((NULL == $entity_data->company_tin) || (STR_EMPTY == $entity_data->company_tin)) {
	                $unfilled['company_tin']            = 0;
	            }

	            if ((NULL == $entity_data->tin_num) || (STR_EMPTY == $entity_data->tin_num)) {
	                $unfilled['business_registration']  = 0;
	            }

	            if ('0000-00-00' == $entity_data->sec_reg_date) {
	                $unfilled['registration_date']  = 0;
	            }

	            if (STR_EMPTY == $entity_data->permit_to_operate) {
	                $unfilled['permit_to_operate']  = 0;
	            }

	            if ((NULL == $entity_data->authorized_signatory) || (STR_EMPTY == $entity_data->authorized_signatory)) {
	                if (BIZ_ENTITY_SOLE == $entity_data->entity_type) {
	                    $unfilled['authorized_signatory_owner'] = 0;
	                }
	            }

	            if (STR_EMPTY == $entity_data->tax_registration) {
	                $unfilled['tax_registration']   = 0;
	            }

	            if (STR_EMPTY == $entity_data->income_tax) {
	                $unfilled['income_tax_return']  = 0;
	            }

	            if (0 == $entity_data->industry_main_id) {
	                $unfilled['industry']  = 0;
	            }

	            if ((NULL == $entity_data->total_asset_grouping) || (0 == $entity_data->total_asset_grouping)) {
	                $unfilled['total_asset_grouping']  = 0;
	            }

	            if ((NULL == $entity_data->email) || (STR_EMPTY == $entity_data->email)) {
	                $unfilled['company_email']  = 0;
	            }

	            if ((NULL == $entity_data->address1) || (STR_EMPTY == $entity_data->address1)) {
	                $bunfilled['company_address']  = 0;
	            }

	            // if ((NULL == $entity_data->province) || (STR_EMPTY == $entity_data->province)) {
	            //     $unfilled['primary_business_province']  = 0;
	            // }

	            if ((NULL == $entity_data->phone) || (STR_EMPTY == $entity_data->phone)) {
	                $unfilled['primary_business_telephone']  = 0;
	            }

	            // if (('0000-00-00' == $entity_data->date_established) || (NULL == $entity_data->date_established) || (STR_EMPTY == $entity_data->date_established) ){
	            //     $unfilled['date_established']  = 0;
	            // }

	            if ((NULL === $entity_data->employee_size) || (STR_EMPTY === $entity_data->employee_size) ){
	                $unfilled['employee_size']  = 0;
	            }

	            if ((NULL === $entity_data->fs_input_who) || (STR_EMPTY === $entity_data->fs_input_who) ){
	                $unfilled['fs_input_who']  = 0;
	            }

	            $unfilled['related_company']             = DB::table('tblrelatedcompanies')->where('entity_id', $entity_id)->count();
	            $unfilled['products_services_offered']   = DB::table('tblservicesoffer')->where('entity_id', $entity_id)->count();
	            $unfilled['main_locations']              = DB::table('tbllocations')->where('entity_id', $entity_id)->count();
	            $unfilled['branches']                    = DB::table('tblbranches')->where('entity_id', $entity_id)->count();



	            if (BIZ_ENTITY_CORP == $entity_data->entity_type) {
	                if (STR_EMPTY == $entity_data->sec_cert) {
	                    $unfilled['registration_docs']  = 0;
	                }

	                $unfilled['capital_details']        = DB::table('tblcapital')->where('entity_id', $entity_id)->count();
	            }

	            $unfilled['certifications']             = DB::table('tblcertifications')->where('entity_id', $entity_id)->count();
	            $unfilled['import_export']              = DB::table('tblbusinesstype')->where('entity_id', $entity_id)->count();
	            $unfilled['insurance_coverage']         = DB::table('tblinsurance')->where('entity_id', $entity_id)->count();
	            $unfilled['cash_conversion']            = DB::table('tblcycledetails')->where('entity_id', $entity_id)->count();
	            $unfilled['fspdf_file']                 = DB::table('innodata_files')->where('entity_id', $entity_id)->where('is_deleted', 0)->count();

	            $unfilled['fs_template']                = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', '=', 5)->count();
	            $unfilled['balance_sheet']              = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', 1)->count();
	            $unfilled['income_statement']           = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 21)->where('upload_type', 2)->count();
	            $unfilled['bank_statement']             = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 51)->count();
	            $unfilled['utility_bill']               = DB::table('tbldocument')->where('entity_id', $entity_id)->where('document_group', '=', 61)->count();

	            $custom_docs    = $this->getRequiredCustomDocs($entity_id);
	        }
	        /*  Business Details 2  */
	        else if ('nav_registration2' == $tab_name) {
	            $trans_file = 'business_details2';

	            $unfilled['major_customer']              = DB::table('tblmajorcustomer')->where('entity_id', $entity_id)->count();
	            $unfilled['major_supplier']              = DB::table('tblmajorsupplier')->where('entity_id', $entity_id)->count();
	            $unfilled['major_market_location']       = DB::table('tblmajorlocation')->where('entity_id', $entity_id)->count();
	            $unfilled['competitors']                 = DB::table('tblcompetitor')->where('entity_id', $entity_id)->count();
	            $unfilled['bdriver']                     = DB::table('tblbusinessdriver')->where('entity_id', $entity_id)->count();
	            $unfilled['customer_segment']            = DB::table('tblbusinesssegment')->where('entity_id', $entity_id)->count();
	            $unfilled['past_proj_completed']         = DB::table('tblprojectcompleted')->where('entity_id', $entity_id)->count();
	            $unfilled['future_growth_initiatives']   = DB::table('tblfuturegrowth')->where('entity_id', $entity_id)->count();
	        }
	        /*  Owner Details   */
	        else if ('nav_ownerdetails' == $tab_name) {
	            $trans_file = 'owner_details';

	            $unfilled['key_manager']    = 0;
	            $unfilled['owner_educ']     = 0;
	            $unfilled['owner_wife']     = 0;
	            $unfilled['wife_educ']      = 0;

	            if (BIZ_ENTITY_SOLE == $entity_data->entity_type) {
	                $owner_inf                  = DB::table('tblowner')->where('entity_id', $entity_id)->first();
	                $unfilled['owner_inf']      = @count($owner_inf);

	                $unfilled['shareholder']    = 1;
	                $unfilled['director']       = 1;

	                if (1 <= $unfilled['owner_inf']) {
	                    $unfilled['key_manager']    = DB::table('tblkeymanagers')->where('entity_id', $entity_id)->count();
	                    $unfilled['owner_educ']     = DB::table('tbleducation')->where('entity_id', $entity_id)->where('ownerid', $owner_inf->ownerid)->count();

	                    $spouse_inf = DB::table('tblspouse')->where('entity_id', $entity_id)->where('ownerid', $owner_inf->ownerid)->first();
	                    $unfilled['owner_wife']  = @count($spouse_inf);

	                    if (1 <= $unfilled['owner_wife']) {
	                        $unfilled['wife_educ']   = DB::table('tbleducation')->where('entity_id', $entity_id)->where('ownerid', $spouse_inf->spouseid)->count();
	                    }
	                }
	            }
	            else {
	                $unfilled['owner_inf']      = 1;
	                $unfilled['owner_educ']     = 1;
	                $unfilled['owner_wife']     = 1;
	                $unfilled['wife_educ']      = 1;
	                $unfilled['key_manager']    = DB::table('tblkeymanagers')->where('entity_id', $entity_id)->count();
	                $unfilled['shareholder']    = DB::table('tblshareholders')->where('entity_id', $entity_id)->count();
	                $unfilled['director']       = DB::table('tbldirectors')->where('entity_id', $entity_id)->count();
	            }
	        }
	        /*  Condition and Sustainability    */
	        else if ('nav_bcs' == $tab_name) {
	            $trans_file = 'condition_sustainability';

	            $unfilled['succession']               = 0;
	            $unfilled['competitive_landscape']    = 0;
	            $unfilled['economic_factor']          = 0;
	            $unfilled['tax_payments']             = 0;

	            $cns_data   = DB::table('tblsustainability')->where('entity_id', $entity_id)->first();

	            if (1 <= @count($cns_data)) {
	                if (NULL != $cns_data->succession_plan) {
	                    $unfilled['succession']    = 1;
	                }

	                if (NULL != $cns_data->competition_landscape) {
	                    $unfilled['competitive_landscape']    = 1;
	                }

	                if (NULL != $cns_data->ev_susceptibility_economic_recession) {
	                    $unfilled['economic_factor']  = 1;
	                }

	                if (NULL != $cns_data->tax_payments) {
	                    $unfilled['tax_payments']  = 1;
	                }
	            }

	            $unfilled['risk_assessment']      = DB::table('tblriskassessment')->where('entity_id', $entity_id)->count();
	            $unfilled['revenue_growth']       = DB::table('tblrevenuegrowthpotential')->where('entity_id', $entity_id)->count();
	            $unfilled['capital_expansion']    = DB::table('tblcapacityexpansion')->where('entity_id', $entity_id)->count();
	        }

			/*  Override configuration for Standalone report    */
			$bank_data          = DB::table('tblbank')->where('id', $entity_data->current_bank)->first();
			$bank_standalone    = ($bank_data && $bank_data->is_standalone==1) ? 1 : 0;

			if ($bank_standalone==1) {
				$required   = array(
	                'industry',
					//'primary_business_province',
					'employee_size',
	                'fs_input_who',
	                'fspdf_file',
				);

	            if (0 == $entity_data->anonymous_report) {
	                $required[] = 'company_tin';
	                $required[] = 'business_registration';
	                $required[] = 'registration_date';
	                $required[] = 'company_address';
	                $required[] = 'company_email';
	                $required[] = 'primary_business_telephone';
	                $required[] = 'number_year_management_team';
	                $required[] = 'date_established';
	            }

				$hidden = array(
					'permit_to_operate',
					'authority_to_borrow',
					'tax_registration',
					'income_tax_return',
					'related_company',
					'products_services_offered',
					'main_locations',
					'branches',
					'capital_details',
					'registration_docs',
					'authorized_signatory_owner',
					'certifications',
					'import_export',
					'insurance_coverage',
					'cash_conversion',
					'balance_sheet',
					'income_statement',
					'bank_statement',
					'utility_bill'
				);

				foreach ($unfilled as $key => $field) {
					if(in_array($key, $required)){
						if (0 >= $field) {
							$sme_form_complete = STS_NG;
							$msg    = trans('messages.please_remember');

							/** Break the loop when one incomplete field is found   */
							break;
						}
					}
				}

			}
	        else {
	            /*  Determine if there is at least one incomplete field */
				foreach ($unfilled as $field) {

					if (0 >= $field) {
						$sme_form_complete = STS_NG;
						$msg    = trans('messages.please_remember');
					}

					if (STS_NG == $sme_form_complete) {
						/** Break the loop when one incomplete field is found   */
						break;
					}
				}
			}

	        return View::make('summary.unfilled-reminder',
				array(
					'unfilled'                  => $unfilled,
					'msg'                       => $msg,
					'sme_form_complete'         => $sme_form_complete,
					'trans_file'                => $trans_file,
					'required'                  => $required,
	                'hidden'                    => $hidden,
	                'custom_docs'               => $custom_docs,
	            )
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.8: getRequiredFields
    //      Get required and hidden fields
    //-----------------------------------------------------
    private static function getRequiredFields($entity_id, &$required, &$hidden)
    {
    	try{
	        $entity_data    = DB::table('tblentity')
	            ->where('entityid', $entity_id)
	            ->first();

	        /*  Sets the Required Fields    */
	        $required   = array(
	            'company_tin',
	            'business_registration',
	            'registration_date',
	            // 'industry',
	            'total_asset_grouping',
	            'company_email',
	            'company_address',
	            //'primary_business_province',
	            'primary_business_telephone',
	            'date_established',
	            'employee_size',
	            'fs_input_who',
	            'cash_conversion',
	            'balance_sheet',
	            'income_statement',
	            'bank_statement',
	            'utility_bill',
	            'number_year_management_team',
	            'major_customer',
	            'major_supplier',
	            'bdriver',
	            'customer_segment',
	            'future_growth_initiatives',
	            'owner_inf',
	            'key_manager',
	            'succession',
	            'risk_assessment',

	        );

	        /*  Match the corresponding Questionnaire label used in translation */
	        self::setQuestionnaireConfigLabel($entity_data->current_bank, $required, $hidden);

	        /*  Check if Major Customer tickbox is clicked   */
	        $major_cust_checked = DB::table('tblmajorcustomeraccount')
	            ->where('entity_id', $entity_id)
	            ->where('is_agree', STS_OK)
	            ->first();

	        /*  Automatic highest point when clicked and Major Customer is no longer required   */
	        if ($major_cust_checked) {
	            unset($required[array_search('major_customer', $required)]);
	        }

	        /*  Removes requirements when report is anonymous   */
	        if (1 == $entity_data->anonymous_report) {
	            unset($required[array_search('company_tin', $required)]);
	            unset($required[array_search('registration_date', $required)]);
	            unset($required[array_search('business_registration', $required)]);
	            unset($required[array_search('company_email', $required)]);
	            unset($required[array_search('company_address', $required)]);
	            unset($required[array_search('primary_business_telephone', $required)]);
	            unset($required[array_search('number_year_management_team', $required)]);
	            unset($required[array_search('date_established', $required)]);
	        }

	        if (BIZ_ENTITY_SOLE == $entity_data->entity_type) {
	            unset($required[array_search('key_manager', $required)]);
	        }

	        /*  Sets the document requirements according to Bank    */
	        $doc_req = DB::table('bank_doc_options')
	            ->where('bank_id', $entity_data->current_bank)
	            ->where('is_active', STS_OK)
	            ->first();

	        /** Bank Document requirements are specified    */
	        if ($doc_req) {

	            foreach ($doc_req as $key => $val) {
	                /*  Add Document to array if it is required */
	                if (STS_OK == $val) {

	                    /** Corporation     */
	                    if (BIZ_ENTITY_CORP == $entity_data->entity_type) {
	                        switch ($key) {

	                            case 'corp_sec_reg':
	                                $required[] = 'registration_docs';
	                                break;

	                            case 'corp_business_license':
	                                $required[] = 'permit_to_operate';
	                                break;

	                            case 'corp_tax_reg':
	                                $required[] = 'tax_registration';
	                                break;

	                            case 'corp_itr':
	                                $required[] = 'income_tax_return';
	                                break;
	                        }
	                    }

	                    /** Sole Proprietorship     */
	                    if (BIZ_ENTITY_SOLE == $entity_data->entity_type) {
	                        switch ($key) {
	                            case 'sole_business_license':
	                                $required[] = 'permit_to_operate';
	                                break;

	                            case 'sole_business_owner':
	                                $required[] = 'authorized_signatory_owner';
	                                break;

	                            case 'sole_tax_reg':
	                                $required[] = 'tax_registration';
	                                break;

	                            case 'sole_itr':
	                                $required[] = 'income_tax_return';
	                                break;
	                        }
	                    }

	                    /** Any Entity Type         */
	                    switch ($key) {
	                        case 'boi':
	                        case 'peza':
	                            $required[] = 'certifications';
	                            break;

	                        case 'fs_template':
	                            $required[] = 'fs_template';
	                            break;

	                        case 'bank_statement':
	                            $required[] = 'bank_statement';
	                            break;

	                        case 'utility_bill':
	                            $required[] = 'utility_bill';
	                            break;
	                    }
	                }
	            }
	        }
	        /** No Bank document requirements are specified */
	        else {

	            if (BIZ_ENTITY_CORP == $entity_data->entity_type) {
	                $required[] = 'key_manager';
	            }
	            else {
	                $required[] = 'owner_inf';
	            }

	            $required[] = 'registration_docs';
	            $required[] = 'permit_to_operate';
	            $required[] = 'tax_registration';
	            $required[] = 'income_tax_return';
	            $required[] = 'fs_template';
	        }

	        return $required;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.8: getRequiredFieldsPremium
    //      Get required and hidden fields
    //-----------------------------------------------------
    private static function getRequiredFieldsPremium($entity_id, &$required)
    {
    	try{
	        $entity_data    = DB::table('tblentity')
	            ->where('entityid', $entity_id)
	            ->first();

	        /*  Sets the Required Fields    */
	        $required   = array(
	            'company_tin',
	            'business_registration',
	            'registration_date',
	            'company_email',
	            'company_address',
	            //'primary_business_province',
	            'primary_business_telephone',
	            'employee_size',
	            'fspdf_file',
				'fs_input_template'
	        );

	        /*  Removes requirements when report is anonymous   */
	        if (1 == $entity_data->anonymous_report) {
	            unset($required[array_search('company_tin', $required)]);
	            unset($required[array_search('business_registration', $required)]);
	            unset($required[array_search('registration_date', $required)]);
	            unset($required[array_search('company_email', $required)]);
	            unset($required[array_search('company_address', $required)]);
	            unset($required[array_search('primary_business_telephone', $required)]);
	        }

	        return $required;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }


    //-----------------------------------------------------
    //  Function 45.9: setQuestionnaireConfigLabel
    //      Match the corresponding Configuration label
    //      with the Questionnaire label used in translation
    //-----------------------------------------------------
    private static function setQuestionnaireConfigLabel($bank_id, &$required, &$hidden)
    {
    	try{
	        /*  Variable Declaration    */
	        $sts                = STS_OK;
	        $config_lbl         = array('question_type', 'affiliates', 'products', 'main_locations', 'capital_details', 'branches', 'import_export', 'insurance', 'cash_conversion', 'major_customer', 'major_supplier', 'major_market', 'competitors', 'business_drivers', 'customer_segments', 'past_projects', 'future_growth', 'owner_details', 'directors', 'shareholders', 'ecf', 'company_succession', 'landscape', 'economic_factors', 'tax_payments', 'risk_assessment', 'growth_potential', 'investment_expansion', 'rcl');
	        $questionnaire_db   = new QuestionnaireConfig;

	        /*  Get configuration data of the bank  */
	        $config_data        = $questionnaire_db->getQuestionnaireConfigbyBankID($bank_id);

	        /*  Match each label accordingly with the Questionnaire labels  */
	        foreach($config_lbl as $cnf_lbl) {

	            $label = $cnf_lbl;

	            switch ($cnf_lbl) {
	                case 'affiliates':
	                    $label = 'related_company';
	                    break;

	                case 'products':
	                    $label = 'products_services_offered';
	                    break;

	                case 'insurance':
	                    $label = 'insurance_coverage';
	                    break;

	                case 'major_market':
	                    $label = 'major_market_location';
	                    break;

	                case 'business_drivers':
	                    $label = 'bdriver';
	                    break;

	                case 'customer_segments':
	                    $label = 'customer_segment';
	                    break;

	                case 'past_projects':
	                    $label = 'past_proj_completed';
	                    break;

	                case 'future_growth':
	                    $label = 'future_growth_initiatives';
	                    break;

	                case 'owner_details':
	                    $label = 'owner_inf';
	                    break;

	                case 'shareholders':
	                    $label = 'shareholder';
	                    break;

	                case 'directors':
	                    $label = 'director';
	                    break;

	                case 'ecf':
	                    $label = 'credit_facilities';
	                    break;

	                case 'company_succession':
	                    $label = 'succession';
	                    break;

	                case 'landscape':
	                    $label = 'competitive_landscape';
	                    break;

	                case 'economic_factors':
	                    $label = 'economic_factor';
	                    break;

	                case 'growth_potential':
	                    $label = 'revenue_growth';
	                    break;

	                case 'investment_expansion':
	                    $label = 'capital_expansion';
	                    break;

	                case 'rcl':
	                    $label = 'requested_cf';
	                    break;
	            }

	            /*  Put to the Hidden output if Config is hidden    */
	            if (QUESTION_CNFG_HIDDEN == $config_data->$cnf_lbl) {
	                $hidden[]   = $label;
	            }

	            /*  The section is required by default  */
	            if (in_array($label, $required)) {
	                /*  Remove default required fields as set by Bank configuration */
	                if (QUESTION_CNFG_REQUIRED != $config_data->$cnf_lbl) {
	                    unset($required[array_search($label, $required)]);
	                }
	            }
	            /*  Add Required fields as set by the Bank  */
	            else {
	                if (QUESTION_CNFG_REQUIRED == $config_data->$cnf_lbl) {
	                    $required[] = $label;
	                }
	            }
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.10: showErrorPage
    //      Display the error page
    //-----------------------------------------------------
    public function showErrorPage()
    {
    	try{
        	return Response::view('errors.default', array(), 404);
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.11: postPotentialCreditQualityIssues
    //      HTTP Post request for to validate and save
    //      potential credit quality issues
    //-----------------------------------------------------
	public function postPotentialCreditQualityIssues()
	{
		try{
	        /*  Get report information  */
			$entity = Entity::find(Input::get('entity_id'));

	        /*  Save Credit Issues to the database  */
	        $entity->potential_credit_quality_issues = Input::get('potential_credit_quality_issues');

			$entity->save();

			return 1;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 46.16: postUpdateBusinessType
    //      HTTP Post request to validate and save
    //      import and export data into the database
    //-----------------------------------------------------
	public function postUpdateBusinessType()
	{
		try{
	        /*  Variable Declaration    */
			$entity_id  = Input::get('entity_id');  /* Report ID        */
			$type       = Input::get('type');       /* Import or Export */
			$value      = Input::get('value');      /* Yes or No        */

	        /*  Get the report information from the database            */
			$entity     = Businesstype::where('entity_id', $entity_id)->first();

	        /*  Save Import details */
			if ($type == 'import') {
				/*  No */
	            if ($value) {
					$entity->is_import = 1;
				}
	            /*  Yes */
	            else {
					$entity->is_import = 2;
				}
			}
	        /*  Save Export details */
	        else if ($type == 'export') {
	            /*  No */
				if ($value) {
					$entity->is_export = 1;
				}
	            /*  Yes */
	            else {
					$entity->is_export = 2;
				}
			}

			$entity->save();

	        /*  Logs the action to the database */
			ActivityLog::saveLog();

	        return "success";
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 47.17: postUpdatePercentComplete
    //      Updates the Percent completion of a report
    //-----------------------------------------------------
	public function postUpdatePercentComplete()
	{
		try{
	        /*  Variable Declaration    */
			$entity_id  = Input::get('entity_id');  // Report
			$completion = Input::get('completion'); // Completion Percentage

	        /*  Save completion update to the database  */
			$entity     = Entity::where('entityid', $entity_id)
	            ->update(
	                array(
	                    'completion' => $completion
	                )
	            );

			return 'success';
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 47.18: getRequiredCustomDocs
    //      Gets the Required Custom Documents
    //-----------------------------------------------------
    public function getRequiredCustomDocs($entity_id)
    {
	    try{
	        /*  Variable Declaration    */
	        $sts        = STS_OK;
	        $required   = array();                      //  Array of Required Custom Documents for Output
	        $upload_arr = array();                      //  Array of Uploaded Documents for Comparison

	        $docs_db    = new BankCustomDocs;
	        $entity     = Entity::find($entity_id);

	        /*  Gets all the Uploaded Custom Documents of SME   */
	        $upload_docs    = DB::table('tbldocument')
	            ->where('entity_id', '=', $entity->entityid)
	            ->where('document_group', '=', 71)
	            ->get();

	        foreach ($upload_docs as $document) {
	            $upload_arr[]   = $document->document_orig;
	        }

	        /*  Gets all the Custom Docs of a Bank              */
	        $custom_docs    = $docs_db->getDocumentsByBank($entity->current_bank);

	        /*  Put the Document in the output array if it is required and no
	        *   document of its kind has been uploaded yet
	        */
	        foreach ($custom_docs as $custom) {

	            if (CMN_ON == $custom->bank_required) {
	                if (!in_array($custom->document_label, $upload_arr)) {
	                    $required[] = $custom->document_label;
	                }
	            }
	        }

	        return $required;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 47.19: computeBeneishScore
    //      Calculates Beneish M-Score
    //-----------------------------------------------------
    function computeBeneishScore($entity_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
	        /*------------------------------------------------------------------*/
	        $other_lt_assets    = array();
	        $sts                = STS_OK;
	        $dsri               = 0;
	        $gmi                = 0;
	        $aqi                = 0;
	        $sgi                = 0;
	        $depi               = 0;
	        $depi_1             = 0;
	        $depi_2             = 0;
	        $depi_3             = 0;
	        $sgai_inc_1         = 0;
	        $sgai_inc_2         = 0;
	        $tata_profit_1      = 0;
	        $tata_profit_2      = 0;
	        $lvgi_liab_1        = 0;
	        $lvgi_liab_2        = 0;
	        $sgai               = 0;
	        $tata               = 0;
	        $lvgi               = 0;
	        $v5_model           = 0;
	        $v8_model           = 0;
	        $m_score            = 0;

	        /*--------------------------------------------------------------------
	        /*	Get the Uploaded Financial Statement Template
	        /*------------------------------------------------------------------*/
	        $fa_report = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])
	            ->orderBy('id', 'desc')
	            ->first();

	        if (NULL != $fa_report) {

	            /*----------------------------------------------------------
	            /*	Get Financial Statement Data
	            /*--------------------------------------------------------*/
	            $fa_report->balance_sheets      = $fa_report
	                ->balanceSheets()
	                ->orderBy('index_count', 'asc')
	                ->get();

				$fa_report->income_statements   = $fa_report
	                ->incomeStatements()
	                ->orderBy('index_count', 'asc')
	                ->get();

	            $fa_report->cashflow            = $fa_report
	                ->cashFlow()
	                ->orderBy('index_count', 'asc')
	                ->get();

	            if ((!isset($fa_report->balance_sheets[1]))
	            || (!isset($fa_report->income_statements[1]))
	            || (!isset($fa_report->cashflow[1]))) {
	                $sts    = STS_NG;
	            }

	            if ((0 == $fa_report->income_statements[0]->Revenue)
	            || (0 == $fa_report->income_statements[1]->Revenue)) {
	                $sts    = STS_NG;
	            }

	            if ((0 == $fa_report->balance_sheets[0]->Assets)
	            || (0 == $fa_report->balance_sheets[1]->Assets)) {
	                $sts    = STS_NG;
	            }

	            if (STS_OK == $sts) {
	                /*----------------------------------------------------------
	                /*	Compute Other L/T Assets
	                /*--------------------------------------------------------*/
	                $other_lt_assets[0] = $fa_report->balance_sheets[0]->Assets - $fa_report->balance_sheets[0]->PropertyPlantAndEquipment;
	                $other_lt_assets[0] = round($other_lt_assets[0], 2) - round($fa_report->balance_sheets[0]->CurrentAssets, 2);

	                $other_lt_assets[1] = $fa_report->balance_sheets[1]->Assets - $fa_report->balance_sheets[1]->PropertyPlantAndEquipment;
	                $other_lt_assets[1] = round($other_lt_assets[1], 2) - round($fa_report->balance_sheets[1]->CurrentAssets, 2);

	                if (0 == $other_lt_assets[0]) {
	                    $other_lt_assets[0] = 1;
	                }

	                if (0 == $other_lt_assets[1]) {
	                    $other_lt_assets[1] = 1;
	                }

	                /*----------------------------------------------------------
	                /*	Compute Days Sales in Receivables Index
	                /*--------------------------------------------------------*/
	                $dsri       = $fa_report->balance_sheets[1]->TradeAndOtherCurrentReceivables / $fa_report->income_statements[1]->Revenue;

	                if (0 != $dsri) {
	                    $dsri   = ($fa_report->balance_sheets[0]->TradeAndOtherCurrentReceivables / $fa_report->income_statements[0]->Revenue) / $dsri;
	                }
	                else {
	                    $dsri    = STS_NG;
	                }
	               


	                /*----------------------------------------------------------
	                /*	Compute Gross Margin Index
	                /*--------------------------------------------------------*/
	                $gmi    = ($fa_report->income_statements[1]->Revenue - $fa_report->income_statements[1]->CostOfSales) / $fa_report->income_statements[1]->Revenue;

	                if (0 != $gmi) {

	                    $gmi    = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[0]->CostOfSales) / $fa_report->income_statements[0]->Revenue) / $gmi;
	                }
	                else {
	                    $gmi    = STS_NG;
	                }
	                
	                /*----------------------------------------------------------
	                /*	Compute Asset Quality Index
	                /*--------------------------------------------------------*/
	                $aqi        = $other_lt_assets[1] / $fa_report->balance_sheets[1]->Assets;

	                if (0 != $aqi) {

	                    $aqi    = ($other_lt_assets[0] / $fa_report->balance_sheets[0]->Assets) / $aqi;
	                }
	                else {
	                    $aqi    = STS_NG;
	                }
	                

	                /*----------------------------------------------------------
	                /*	Compute Sales Growth Index
	                /*--------------------------------------------------------*/
	                $sgi    = $fa_report->income_statements[0]->Revenue / $fa_report->income_statements[1]->Revenue;

	                /*----------------------------------------------------------
	                /*	Compute Depreciation Index
	                /*--------------------------------------------------------*/
	                $depi_1 = $fa_report->cashflow[0]->Depreciation + $fa_report->balance_sheets[0]->PropertyPlantAndEquipment;
	                $depi_2 = $fa_report->cashflow[1]->Depreciation + $fa_report->balance_sheets[1]->PropertyPlantAndEquipment;

	                if (0 != $depi_2) {
	                    $depi_3 = $fa_report->cashflow[1]->Depreciation / $depi_2;
	                }

	                if ((0 != $depi_3)
	                && (0 != $depi_1)) {

	                    $depi   = ($fa_report->cashflow[0]->Depreciation / $depi_1) / $depi_3;
	                }
	                else {
	                    $depi    = STS_NG;
	                }
	                

	                /*----------------------------------------------------------
	                /*	Compute M-Score 5 Model Variable
	                /*--------------------------------------------------------*/
	                if (STS_OK == $sts) {
	                    $v5_model   = -6.065 + (0.823 * $dsri) + (0.906 * $gmi) + (0.593 * $aqi) + (0.717 * $sgi) + (0.107 * $depi);
	                }
	               

	                $sgai_inc_1     = $fa_report->income_statements[0]->OtherIncome + $fa_report->income_statements[0]->DistributionCosts + $fa_report->income_statements[0]->AdministrativeExpense + $fa_report->income_statements[0]->OtherExpenseByFunction + $fa_report->income_statements[0]->OtherGainsLosses;
	                $sgai_inc_2     = $fa_report->income_statements[1]->OtherIncome + $fa_report->income_statements[1]->DistributionCosts + $fa_report->income_statements[1]->AdministrativeExpense + $fa_report->income_statements[1]->OtherExpenseByFunction + $fa_report->income_statements[1]->OtherGainsLosses;

	                $tata_profit_2  = $fa_report->income_statements[1]->ComprehensiveIncome - $fa_report->cashflow[1]->NetCashByOperatingActivities;

	                $lvgi_liab_1    = $fa_report->balance_sheets[0]->OtherNoncurrentFinancialLiabilities + $fa_report->balance_sheets[0]->CurrentLiabilities;
	                $lvgi_liab_2    = $fa_report->balance_sheets[1]->OtherNoncurrentFinancialLiabilities + $fa_report->balance_sheets[1]->CurrentLiabilities;

	                if ((STS_OK == $sts)
	                && (0 != $sgai_inc_1)
	                && (0 != $lvgi_liab_1)
	                && (0 != $sgai_inc_2)
	                && (0 != $tata_profit_2)
	                && (0 != $lvgi_liab_2)) {
	                    /*----------------------------------------------------------
	                    /*	Compute Sales, General and Administrative Expenses Index
	                    /*--------------------------------------------------------*/
	                    $sgai   = $sgai_inc_2 / $fa_report->income_statements[1]->Revenue;

	                    if (0 != $sgai) {
	                        $sgai   = ($sgai_inc_1 / $fa_report->income_statements[0]->Revenue) / $sgai;
	                    }
	                    else {
	                        $sgai    = STS_NG;
	                    }

	                    /*----------------------------------------------------------
	                    /*	Compute Total Accruals to Total Assets
	                    /*--------------------------------------------------------*/
	                    $tata   = $tata_profit_2 / $fa_report->balance_sheets[1]->Assets;

	                    /*----------------------------------------------------------
	                    /*	Compute Leverage Index
	                    /*--------------------------------------------------------*/
	                    $lvgi   = $lvgi_liab_2 / $fa_report->balance_sheets[1]->Assets;

	                    if (0 != $lvgi) {
	                        $lvgi   = ($lvgi_liab_1 / $fa_report->balance_sheets[0]->Assets) / $lvgi;
	                    }
	                    else {
	                        $lvgi    = STS_NG;
	                    }

	                    /*----------------------------------------------------------
	                    /*	Compute M-Score 8 Model Variable
	                    /*--------------------------------------------------------*/
	                    if (STS_OK == $sts) {
	                        $v8_model   = -4.84 + (0.92 * $dsri) + (0.528 * $gmi) + (0.404 * $aqi) + (0.892 * $sgi) + (0.115 * $depi) - (0.172 * $sgai) + (4.679 * $tata) - (0.327 * $lvgi);
	                    }
	                }
	            }
	        }
	        

	        if (0 != $v8_model) {
	            $m_score = $v8_model;
	        }
	        else {
	            $m_score = $v5_model;
	        }
	       
	        return $m_score;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 47.20: getAnonymousProfile
    //      Gets the company's anonymous data
    //-----------------------------------------------------
    function getAnonymousProfile($entity_id)
    {
    	try{
	        $data       = array();
	        $entity     = Entity::find($entity_id);

	        $customers  = Majorcustomer::where('entity_id', $entity_id)
	            ->get();

	        $suppliers  = Majorsupplier::where('entity_id', $entity_id)
	            ->get();

	        foreach ($customers as $customer) {
	            $data['cust_id'][]    = $customer->majorcustomerrid;
	            $data['cust_name'][]  = $customer->customer_name;
	        }

	        foreach ($suppliers as $supplier) {
	            $data['supp_id'][]    = $supplier->majorsupplierid;
	            $data['supp_name'][]  = $supplier->supplier_name;
	        }

	        $data['report'] = $entity;

	        return json_encode($data);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 47.21: setReportAnonymous
    //      Sets a report to Anonymous
    //-----------------------------------------------------
    function setReportAnonymous($status, $entity_id)
    {
    	try{
	        $data = Entity::find($entity_id);

	        if ($status != $data->anonymous_report) {
	            $data->anonymous_report = $status;
	            $data->save();

	            /* Auto-Rename Customers and Supplier   */
	            /*
	            if (1 == $status) {
	                $customers  = Majorcustomer::where('entity_id', $entity_id)->get();
	                $suppliers  = Majorsupplier::where('entity_id', $entity_id)->get();

	                $count_loop = 1;

	                foreach ($customers as $customer) {
	                    $customer->customer_name = 'Major Customer '.$count_loop;
	                    $customer->save();
	                    $count_loop++;
	                }

	                $count_loop = 1;

	                foreach ($suppliers as $supplier) {
	                    $supplier->supplier_name = 'Major Supplier '.$count_loop;
	                    $supplier->save();
	                    $count_loop++;
	                }
	            }
	            */
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.22: viewReceipt
    //      function to view the receipt
    //-----------------------------------------------------
    function viewReceipt()
    {
    	try{
	        $receipt = PaymentReceipt::where('reference_id', '!=', 0)
	            ->orderBy('receipt_id', 'desc')
	            ->first();
	        // $mailHandler = new MailHandler();
	        // $mailHandler->sendReceipt(
	        //     Auth::user()->loginid,
	        //     [
	        //         'receipt' => $receipt
	        //     ],
	        //     'CreditBPO Payment Receipt'
	        // );

	        $entity = Entity::where('loginid', Auth::user()->loginid)
	                ->where('is_paid', 1)
	                ->orderBy('entityid', 'desc')
	                ->first();

	       $user = User::find(Auth::user()->loginid);

	        return View::make(
	            'emails.new.payment_receipt',
	            [
	                'receipt' => $receipt,
	                'entity' => $entity,
	                'user' => $user,
	                'subject' => 'CreditBPO Payment Test',
	            ]
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 45.23: Calculate static DSCR
    //      Sets a report to Anonymous
    //-----------------------------------------------------
    public function calculateStaticDscr($entity_id)
    {
    	try{
	        $srv_resp['sts']        = STS_OK;
	        $srv_resp['msg']        = STR_EMPTY;

	        $dscr_data[0]   = 4.255833333;
	        $dscr_data[1]   = 5.568866667;
	        $dscr_data[2]   = 4.255833333;
	        $dscr_data[3]   = 5.568866667;

			$debt_service   = 0;
	        $noi_int_exp    = 0;
	        $noi_static     = 0;
	        $int_exp        = 0;
	        $dscr_c         = 0;
	        $payment        = 0;

			/** Get the saved debt service capacity calculator months */
			$dscr_existing_record = DB::table('debt_service_capacity_calculator')->where('entity_id', $entity_id)->first();

			if($dscr_existing_record){
				$tenor = $dscr_existing_record->loan_duration;
			}else{
				$tenor = 0;
			}

	        $fin_report = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])
	            ->orderBy('id', 'desc')
	            ->first();
			
			$interestRates = new BankInterestRate;
			$interestRates = $interestRates->getCurrentInterestRates();

	        $local_rate   = $interestRates['local']['low'];


	        $foreign_rate   = $interestRates['foreign']['low'];

	        $dscr_data[0]   = $local_rate;
	        $dscr_data[1]   = $foreign_rate;
	        $dscr_data[2]   = $local_rate;
	        $dscr_data[3]   = $foreign_rate;

			if ($fin_report) {
				$fr_data  = array(
					'financial_report'  => $fin_report,
					'balance_sheets'    => $fin_report->balanceSheets()
	                    ->orderBy('index_count', 'asc')
	                    ->get(),
					'income_statements' => $fin_report->incomeStatements()
	                    ->orderBy('index_count', 'asc')
	                    ->get(),
					'cash_flow' => $fin_report->cashFlow()
	                    ->orderBy('index_count', 'asc')
	                    ->get()
				);


	            $debt_service   = (float)$fr_data['cash_flow'][0]->DebtServiceCapacity;
	            $noi_int_exp    = (float)$fr_data['cash_flow'][0]->NetOperatingIncome + (float)$fr_data['cash_flow'][0]->InterestExpense;
	            $noi_static     = (float)$fr_data['cash_flow'][0]->NetOperatingIncome;
	            $int_exp        = (float)$fr_data['cash_flow'][0]->InterestExpense;

	            if (0 < $debt_service) {
	                //$dscr_c         = ($noi_int_exp - $int_exp) / $debt_service;
	            }
	            else {
	                //$srv_resp['sts']    = STS_NG;
	                //$srv_resp['msg']    = 'Division by Zero: Debt Service is 0';
	            }

	            $dscr_c         = round($dscr_c, 2);
	            $dscr_c         = 1.20;

	            $receivable_to  = (float)$fr_data['income_statements'][0]->Revenue / 365;

	            if (0 < $receivable_to) {
	                $receivable_to = (((float)$fr_data['balance_sheets'][0]->TradeAndOtherCurrentReceivables + (float)$fr_data['balance_sheets'][1]->TradeAndOtherCurrentReceivables) / 2) / $receivable_to;
	            }
	            else {
	                //$srv_resp['sts']    = STS_NG;
	                //$srv_resp['msg']    = 'Division by Zero: Receivables Turnover Divisor is 0';
	                $receivable_to      = 0;
	            }

	            $payable_to     = ((float)$fr_data['income_statements'][0]->CostOfSales + (float)$fr_data['balance_sheets'][0]->Inventories - (float)$fr_data['balance_sheets'][1]->Inventories) / 365;

	            if (0 < $payable_to) {
	                $payable_to = (((float)$fr_data['balance_sheets'][0]->TradeAndOtherCurrentPayables + (float)$fr_data['balance_sheets'][1]->TradeAndOtherCurrentPayables + (float)$fr_data['balance_sheets'][0]->CurrentProvisionsForEmployeeBenefits + (float)$fr_data['balance_sheets'][1]->CurrentProvisionsForEmployeeBenefits) / 2) / $payable_to;
	            }
	            else {
	                //$srv_resp['sts']    = STS_NG;
	                //$srv_resp['msg']    = 'Division by Zero: Payable Turnover Divisor is 0';
	                $payable_to         = 0;
	            }

	            $inventory_to_1   = (float)$fr_data['balance_sheets'][1]->Inventories / 2;
	            $inventory_to_2   = (float)$fr_data['income_statements'][0]->CostOfSales / 365;

	            if ((0 < $inventory_to_1) &&
	            (0 < $inventory_to_2)) {
	                $inventory_to   = ((float)$fr_data['balance_sheets'][0]->Inventories / $inventory_to_1) / $inventory_to_2;
	            }
	            else {
	                //$srv_resp['sts']    = STS_NG;
	                //$srv_resp['msg']    = 'Division by Zero: Inventory Turnover Divisor is 0';
	                $inventory_to       = 0;
	            }

	            $ccc    = $inventory_to + $receivable_to - $payable_to;

	            if (0 > $ccc) {
	                $ccc = $ccc * -1;
	            }

	            for ($idx = 0; $idx < @count($dscr_data); $idx++) {
	                $ir     = $dscr_data[$idx];
	                $air    = $ir / 100;
	                $mir    = $air / 12;

	                if ($idx < 2) {
	                    $period         = $ccc;
	                    $annualization  = 360;
	                }
	                else {
	                    $period         = 30;
	                    $annualization  = 365;
	                }

	                $noi        = $noi_static;
	                $payment    = $noi / $dscr_c;

	                $ie     = $noi_int_exp - $noi - $int_exp;
	                $la     = ($tenor / 12) * $payment;

	                if (0 == $idx) {
	                    $loan_amount    = $la;
	                }

	                $monthly_amort  = $payment > 0 ? ($la / $payment) : 0;
	                $payment_period = 0;

	                if ($idx < 2) {
	                    //$pp1            = $la * ($air*($ccc/365));
	                    if ($payment > 0) {
	                        // $pp1 = (($la + ((($la * $air) * $ccc) / 365)) / $payment);
	                        // $pp1 = ($la + ((($la * $air) * $ccc) / 365)) / $tenor;
							$pp1 = ($period * $la * $air)/ 365;
	                    } else {
	                        $pp1 = 0;
	                    }


	                    //$payment_period = $la * ($air*($ccc/360)) / (1 - pow(1 + ($air*($ccc/360)), -1));

	                    $dscr_result[]  = $ccc <= 0 ? 'N/A' : (int)$ccc.' Days';

	                    if (0 < $pp1) {
	                        $payment_period = $pp1;
	                        $dscr_result[]  = number_format($payment_period, 2);
	                    }
	                    else {
	                        $srv_resp['sts']    = STS_NG;
	                        $srv_resp['msg']    = 'Division by Zero: Payment Period Divisor is 0';
	                        $dscr_result[]      = 'N/A';
	                    }

	                }
	                else {
	                    if ($payment > 1) {
	                        $nump1 = -log(1 - ($air * ($la / $payment)));
	                    } else {
	                        $nump1 = 0;
	                    }

	                    $nump2          = log(1 + $air);
	                    $num_payments   = $nump1 / $nump2;

	                    if ($payment > 1) {
	                        $monthly_term = ($la + ((($la * $air) * 30) / 360)) / $payment;
	                    } else {
	                        $monthly_term = 0;
	                    }

	                    if (0 < $monthly_amort) {
	                        $mt_1           = 24;
	                        if ($monthly_term > 1) {
	                            $pp_3 = ($la + ((($la * $air) * 30) / 360)) / round($monthly_term);
	                        } else {
	                            $pp_3 = 0;
	                        }

	                        //$monthly_term   = -(log(1 - (($la * $mir) / $monthly_amort))) / log(1 + $mir);
	                        //$payment_period = $la * ($mir / (1 - pow(1 + $mir, -(round($monthly_term)))));

	                        if (0 < $mt_1) {
	                            $monthly_term   = $num_payments;
	                            $dscr_result[]  = '1 Month';
	                        }
	                        else {
	                            $srv_resp['sts']    = STS_NG;
	                            $srv_resp['msg']    = 'Division by Zero: Monthly Term Divisor is 0';
	                            $dscr_result[]      = 'N/A';
	                        }

	                        if (0 < $pp_3) {
	                            $payment_period = $pp_3;
	                            $dscr_result[]  = number_format($payment_period, 2);
	                        }
	                        else {
	                            $srv_resp['sts']    = STS_NG;
	                            $srv_resp['msg']    = 'Division by Zero: Payment Period Divisor is 0';
	                            $dscr_result[]      = 'N/A';
	                        }
	                    }
	                    else {
	                        $dscr_result[]  = 'N/A';
	                        $dscr_result[]  = 'N/A';
	                    }
	                }

	            }

	            //$srv_resp['fr_data']        = $fr_data;
	            $srv_resp['la']             = $loan_amount;
	            $srv_resp['noi']            = $fr_data['cash_flow'][0]->NetOperatingIncome;
	            $srv_resp['dscr_result']    = $dscr_result;
	            $srv_resp['dscr_data']      = $dscr_data;
	            $srv_resp['fr_data']        = $fr_data;
	        }
	        else {
	            $srv_resp['la']             = 0;
	            $srv_resp['noi']            = 0;
	            $srv_resp['dscr_result']    = array();
	            $srv_resp['dscr_data']      = $dscr_data;
	            $srv_resp['fr_data']        = $fr_data;
	        }

			$srv_resp['months_cntr'] = $tenor;

			// print_r($srv_resp['dscr_result']); die();
	        return $srv_resp;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

}
