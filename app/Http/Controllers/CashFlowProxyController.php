<?php
use CreditBPO\Handlers\UploadErrorHandler;
use CreditBPO\Models\UploadErrors;
//======================================================================
//  Class 11: CashFlowProxyController
//  Description: Contains functions regarding CashFlow Proxy
//======================================================================
class CashFlowProxyController extends BaseController
{

	//-----------------------------------------------------
    //  Function 11.1: getBankStatements
	//  Description: Displays the Add Bank Statements form
    //-----------------------------------------------------
    public function getBankStatements($entity_id)
	{
		return View::make('sme.upload-bank-statements',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 11.2: postBankStatements
	//  Description: Adds Bank Statements
    //-----------------------------------------------------
    public function postBankStatements($entity_id)
	{
		try{
			/*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
	        $input_files    = Input::file('bank_statements');		// input file
	        $input_dates    = Input::get('as_of');					// input date string
	        $messages       = array();								// vairable container
	        $rules          = array();								// variable container

	        $filenames      = array();								// variable container
	        $display_names  = array();								// variable container
	        $master_ids     = array();								// variable container

	        $security_lib   = new MaliciousAttemptLib;				// create instance of MaliciousAttemptLib
	        $sts            = STS_NG;

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#bank-statements-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-bank-statements';
	        }

	        /*--------------------------------------------------------------------
			/*	Checks if the User is allowed to update the company
			/*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        /*--------------------------------------------------------------------
			/*	Sets rules and messages
			/*------------------------------------------------------------------*/
			foreach(Input::file('bank_statements') as $key => $value){
				if($value){
					$rules['bank_statements.'.$key] = 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt';
					$rules['as_of.'.$key]           = 'required|date_format:m/d/Y|before:tomorrow';

					$messages['bank_statements.'.$key.'.required']  = trans('validation.required', array('attribute'=>'Bank Statement Document'));
					$messages['bank_statements.'.$key.'.mimes']     = "File ".($key+1)." should be a PDF, Excel, CSV, Zip or Rar file.";
					$messages['as_of.'.$key.'.required']            = trans('validation.required', array('attribute'=>'Date'));;
	                $messages['as_of.'.$key.'.date_format']         = trans('validation.date_format', array('attribute'=>'Date'));;
					$messages['as_of.'.$key.'.before']         = trans('validation.before', array('attribute'=>'Date'));;
				}
			}

			$validator = Validator::make(Input::all(), $rules, $messages);

	        /*--------------------------------------------------------------------
			/*	Some inputs are invalid
			/*------------------------------------------------------------------*/
			if ($validator->fails()) {
				$srv_resp['messages']	= $validator->messages()->all();

	            if (isset($_COOKIE['formdata_support'])) {
	                if (STS_NG == $_COOKIE['formdata_support']) {
	                    return Redirect::to('/sme/upload-bank-statements/'.$entity_id)->withErrors($validator)->withInput();
	                }
	            }
			}
	        /*--------------------------------------------------------------------
			/*	All inputs are valid
			/*------------------------------------------------------------------*/
			else {
	            /*---------------------------------------------------------------
	            /*	Saves the inputs
	            /*-------------------------------------------------------------*/

	            $destinationPath = public_path('documents/'.$entity_id);

	            if (!file_exists($destinationPath)) {
	                mkdir($destinationPath);
	                chmod($destinationPath, 0775);
	            }

	            foreach ($input_files as $key => $value) {
	                if ($value) {
	                    /** Sets the filename and destination dir    */

	                    $filenameTime = date('Y-m-d', strtotime($input_dates[$key]));
	                    $extension = $value->getClientOriginalExtension();
	                    $fileLabel = 'Bank Statement for report dated '.$filenameTime;
	                    $filename = $fileLabel.'.'.$extension;

	                    /** Saves information to the database    */
	                    $data = array(
	                        'entity_id'         => $entity_id,
	                        'document_type'     => 0,
	                        'document_group'    => 51,
	                        'document_orig'     => $fileLabel,
	                        'document_rename'   => $entity_id.'/'.$filename,
	                        'created_at'        => date('Y-m-d H:i:s'),
	                        'updated_at'        => date('Y-m-d', strtotime($input_dates[$key]))
	                    );
	                    $documentid     = DB::table('tbldocument')->insertGetId($data);

	                    $master_ids[]       = $documentid;
	                    $filenames[]        = $filename;
	                    $display_names[]    = $fileLabel;

						// background process check
						if(env("APP_ENV") == "Production"){
							exec('wget '.URL::to('/tools/ocr_check_bank_statement/'.$documentid).' -q -o /dev/null > /dev/null 2>/dev/null &');

	                        /** Upload file to the server    */
	                        $upload_success = $value->move($destinationPath, $filename);
						}
	                    else {
	                        /** Upload file to the server    */
	                        $upload_success = $value->move($destinationPath, $filename);
	                    }
	                }
	            }

	            $srv_resp['sts']        = STS_OK;
	            $srv_resp['messages']   = 'Successfully Added Record';

	            if (!isset(Auth::user()->loginid)) {
	                $srv_resp['fields']['filename']         = $filenames;
	                $srv_resp['fields']['display_names']    = $display_names;
	                $srv_resp['master_ids']                 = $master_ids;
	            }
	        }

	        if (isset($_COOKIE['formdata_support'])) {
	            if (STS_NG == $_COOKIE['formdata_support']) {
	                return Redirect::to('/summary/smesummary/'.$entity_id);
	            }
	        }

	        if($srv_resp['sts'] == STS_NG && $srv_resp['messages'] == STR_EMPTY){
				$srv_resp['messages'] = ['Select a file to upload'];
			}
	        ActivityLog::saveLog();
	        return json_encode($srv_resp);
	    }
	    catch(Exception $ex){
        	$uploadError = [
        		'user_type' => Auth::user()->role,
        		'loginid' => Auth::user()->loginid,
        		'entityid' => $entity_id,
        		'error_details' => $ex->getMessage(),
        		'date_uploading' => date('Y-m-d H:i:s')
        	];

        	UploadErrorHandler::uploadErrorsUpdate($uploadError);
        	
        }
    }


	//-----------------------------------------------------
    //  Function 11.3: getUtilityBills
	//  Description: Display the Add utility bill form
    //-----------------------------------------------------
    public function getUtilityBills($entity_id)
	{
		return View::make('sme.upload-utility-bills',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 11.4: postUtilityBills
	//  Description: Adds Utility Bills
    //-----------------------------------------------------
    public function postUtilityBills($entity_id)
	{

		try{
			/*--------------------------------------------------------------------
			/*	Variable Declaration
			/*------------------------------------------------------------------*/
			$input_files    = Input::file('utility_bills');
	        $input_dates    = Input::get('as_of');
	        $messages       = array();
	        $rules          = array();

	        $filenames      = array();
	        $display_names  = array();
	        $master_ids     = array();

	        $security_lib   = new MaliciousAttemptLib;
	        $sts            = STS_NG;

	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;
	        $srv_resp['messages']       = STR_EMPTY;

	        if (isset(Auth::user()->loginid)) {
	            $srv_resp['action']         = REFRESH_CNTR_ACT;
	            $srv_resp['refresh_cntr']   = '#utility-bills-list-cntr';
	            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
	            $srv_resp['refresh_elems']  = '.reload-utility-bills';
	        }

	        /*--------------------------------------------------------------------
			/*	Checks if the User is allowed to update the company
			/*------------------------------------------------------------------*/
	        $sts            = $security_lib->checkUpdatePermission($entity_id);

	        /** User not allowed to edit Company    */
	        if (STS_NG == $sts) {
	            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
	        }

	        /*--------------------------------------------------------------------
			/*	Sets rules and messages
			/*------------------------------------------------------------------*/
			foreach ($input_files as $key => $value) {
				if($value){
					$rules['utility_bills.'.$key]   = 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt';
					$rules['as_of.'.$key]           = 'required|date_format:m/d/Y|before:yesterday';

					$messages['utility_bills.'.$key.'.required']    = trans('validation.required', array('attribute'=>'Utility Bill Document'));
					$messages['utility_bills.'.$key.'.mimes']       = "File ".($key+1)." should be a PDF, Excel, CSV, Zip or Rar file.";
	                $messages['as_of.'.$key.'.required']            = trans('validation.required', array('attribute'=>'Date'));;
	                $messages['as_of.'.$key.'.date_format']         = trans('validation.date_format', array('attribute'=>'Date'));;
	                $messages['as_of.'.$key.'.before']         = trans('validation.before', array('attribute'=>'Date'));;
				}
	        }

			$validator = Validator::make(Input::all(), $rules, $messages);

	        /*--------------------------------------------------------------------
			/*	Some inputs are invalid
			/*------------------------------------------------------------------*/
			if ($validator->fails()) {
				$srv_resp['messages']	= $validator->messages()->all();

	            if (isset($_COOKIE['formdata_support'])) {
	                if (STS_NG == $_COOKIE['formdata_support']) {
	                    return Redirect::to('/sme/upload-utility-bills/'.$entity_id)->withErrors($validator->messages())->withInput();
	                }
	            }
			}
	        /*--------------------------------------------------------------------
			/*	All inputs are valid
			/*------------------------------------------------------------------*/
			else {
	            $destinationPath = public_path('documents/'.$entity_id);

	            if (!file_exists($destinationPath)) {
	                mkdir($destinationPath);
	                chmod($destinationPath, 0775);
	            }

	            /*---------------------------------------------------------------
	            /*	Saves the inputs
	            /*-------------------------------------------------------------*/
	            foreach ($input_files as $key => $value) {

	                if ($value) {
	                    /** Sets the filename and destination dir    */
	                    $filenameTime = date('Y-m-d', strtotime($input_dates[$key]));
	                    $extension = $value->getClientOriginalExtension();
	                    $fileLabel = 'Utility Bill for report dated '.$filenameTime;
	                    $filename = $fileLabel.'.'.$extension;

	                    /** Saves information to the database    */
	                    $data = array(
	                        'entity_id'         => $entity_id,
	                        'document_type'     => 0,
	                        'document_group'    => 61,
	                        'document_orig'     => $fileLabel,
	                        'document_rename'   => $entity_id.'/'.$filename,
	                        'created_at'        => date('Y-m-d H:i:s'),
	                        'updated_at'        => date('Y-m-d H:i:s')
	                    );
	                    $documentid         = DB::table('tbldocument')->insertGetId($data);

	                    $master_ids[]       = $documentid;
	                    $filenames[]        = $filename;
	                    $display_names[]    = $fileLabel;

						// background process check
						if(env("APP_ENV") == "Production"){
							exec('wget '.URL::to('/tools/ocr_check_utility_bill/'.$documentid).' -q -o /dev/null > /dev/null 2>/dev/null &');

	                        /** Upload file to the server    */
	                        $upload_success = $value->move($destinationPath, $filename);

						}
	                    else {
	                        /** Upload file to the server    */
	                        $upload_success = $value->move($destinationPath, $filename);
	                    }

	                    $srv_resp['sts']        = STS_OK;
	                    $srv_resp['messages']   = 'Successfully Added Record';

	                    if (!isset(Auth::user()->loginid)) {
	                        $srv_resp['fields']['filename']         = $filenames;
	                        $srv_resp['fields']['display_names']    = $display_names;
	                        $srv_resp['master_ids']                 = $master_ids;
	                    }
	                }
	            }
	        }

	        if (isset(Auth::user()->loginid)) {
	            if (STS_NG == $_COOKIE['formdata_support']) {
	                return Redirect::to('/summary/smesummary/'.$entity_id);
	            }
	        }

			if($srv_resp['sts'] == STS_NG && $srv_resp['messages'] == STR_EMPTY){
				$srv_resp['messages'] = ['Select a file to upload'];
			}
			ActivityLog::saveLog();
	        return json_encode($srv_resp);
	    }
	    catch(Exception $ex){
        	$uploadError = [
        		'user_type' => Auth::user()->role,
        		'loginid' => Auth::user()->loginid,
        		'entityid' => $entity_id,
        		'error_details' => $ex->getMessage(),
        		'date_uploading' => date('Y-m-d H:i:s')
        	];

        	UploadErrorHandler::uploadErrorsUpdate($uploadError);
        	
        }
    }

	//-----------------------------------------------------
    //  Function 11.5: getDeleteCashProxy
	//  Description: Delete Cash Proxy Documents
    //-----------------------------------------------------
    public function getDeleteCashProxy($doc_id)
	{
        $destinationPath    = './documents/';
		$document           = DB::table('tbldocument')->where('documentid', $doc_id)->where('entity_id', Session::get('entity')->entityid)->first();

        if ($document) {
            $entityFile = $destinationPath.$document->entity_id.'/'.$document->document_rename;

			if (file_exists($entityFile)) {
                $destinationPath = $destinationPath.$document->entity_id.'/';
            }

			File::delete($destinationPath.$document->document_rename);
			DB::table('tbldocument')->where('documentid', $doc_id)->where('entity_id', Session::get('entity')->entityid)->delete();
		}
        ActivityLog::saveLog();
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

	//-----------------------------------------------------
    //  Function 11.6: getTaxPayments
	//  Description: Displays the Add Tax Payments form
    //-----------------------------------------------------
    public function getTaxPayments($entity_id)
	{
		return View::make('sme.upload-tax-payments',
            array(
                'entity_id' => $entity_id
            )
        );
	}

	//-----------------------------------------------------
    //  Function 11.7: postTaxPayments
	//  Description: Adds Bank Statements
    //-----------------------------------------------------
    public function postTaxPayments($entity_id)
	{

		/*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
		$input_files = Input::file('tax_payments');
        $input_dates = Input::get('as_of');
        $messages   = array();
        $rules      = array();

        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;

        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $srv_resp['action']         = REFRESH_CNTR_ACT;
        $srv_resp['refresh_cntr']   = '#tax-payments-list-cntr';
        $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
        $srv_resp['refresh_elems']  = '.reload-tax-payments';

        /*--------------------------------------------------------------------
		/*	Checks if the User is allowed to update the company
		/*------------------------------------------------------------------*/
        $sts            = $security_lib->checkUpdatePermission($entity_id);

        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }

        /*--------------------------------------------------------------------
		/*	Sets rules and messages
		/*------------------------------------------------------------------*/
		foreach ($input_files as $key => $value) {
            $rules['tax_payments.'.$key]    = 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt';
			$rules['as_of.'.$key]           = 'required';

			$messages['tax_payments.'.$key.'.required']     = trans('validation.required', array('attribute'=>'Tax Payments Document'));
			$messages['tax_payments.'.$key.'.mimes']        = "File ".($key+1)." should be a PDF, Excel, CSV, Zip or Rar file.";
			$messages['as_of.'.$key.'.required']            = trans('validation.required', array('attribute'=>'Date'));;
		}

		$validator = Validator::make(Input::all(), $rules, $messages);

        /*--------------------------------------------------------------------
		/*	Some inputs are invalid
		/*------------------------------------------------------------------*/
		if ($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();

            if (isset($_COOKIE['formdata_support'])) {
                if (STS_NG == $_COOKIE['formdata_support']) {
                    return Redirect::to('/sme/upload-tax-payments/'.$entity_id)->withErrors($validator)->withInput();
                }
            }
		}
        /*--------------------------------------------------------------------
		/*	All inputs are valid
		/*------------------------------------------------------------------*/
		else {
            /*---------------------------------------------------------------
            /*	Saves the inputs
            /*-------------------------------------------------------------*/
            foreach ($input_files as $key => $value) {

                if ($value) {
                    /** Sets the filename and destination dir    */
                    $destinationPath = 'documents';
                    $extension = $value->getClientOriginalExtension();
                    $filename = 'tax-payment-'.rand(111,999).Auth::user()->loginid.time().'.'.$extension;

                    /** Saves information to the database    */
                    $data = array(
                        'entity_id'         => $entity_id,
                        'document_type'     => 0,
                        'document_group'    => 71,
                        'document_orig'     => Session::get('entity')->companyname.' - Tax Payment as of '.$input_dates[$key],
                        'document_rename'   => $filename,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d', strtotime($input_dates[$key]))
                    );
                    DB::table('tbldocument')->insert($data);

                    /** Upload file to the server    */
                    $upload_success = $value->move($destinationPath, $filename);

                    $srv_resp['sts']    = STS_OK;
                }
            }
        }

        if (isset($_COOKIE['formdata_support'])) {
            if (STS_NG == $_COOKIE['formdata_support']) {
                return Redirect::to('/summary/smesummary/'.$entity_id);
            }
        }

        ActivityLog::saveLog();
        return json_encode($srv_resp);
    }
}
