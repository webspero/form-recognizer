<?php
use \Exception as Exception;
//======================================================================
//  Class 14: ConverterController
//  Description: Contains functions regarding FS Template to VFA converter
//======================================================================
class ConverterController extends BaseController {
	
	//-----------------------------------------------------
    //  Function 14.1: resetHash
	//  Description: function for generating / resetting converter key
    //-----------------------------------------------------
	public function resetHash()
	{
		$characters = "abcdefghijklmnopqrstuvwxyz0123456789";								// valid characters
		$new_password = $this->get_random_string($characters, 10);							// generate ranndom 10 characters
		Keys::where('name','converter_key')->update(array('value'=>$new_password));			// update key

		// send email regarding updated converter key
		if (env("APP_ENV") == "Production") {
			try{
				Mail::send('emails.converter', array('converter_key'=>$new_password), function($message){
					$message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
						->subject('CreditBPO change in converter link');
				});
			}catch(Exception $e){
            //
            }
		}
		return "New converter key created...";
	}
	
	//-----------------------------------------------------
    //  Function 14.2: getConverter
	//  Description: get FS Template to VFA converter page
    //-----------------------------------------------------
	public function getConverter($hash)
	{
		$password = Keys::where('name','converter_key')->first()->value; 	// get hash saved on db
		if($password!=$hash) App::abort(403, 'Unauthorized action.');	 	// compare hash input and db hash, abort if not equal
		
		return View::make('converter', array(
			'hash' => $hash
		));
	}
	
	//-----------------------------------------------------
    //  Function 14.3: postConverter
	//  Description: function that converts upload FS Template to .vfa file
    //-----------------------------------------------------
	public function postConverter($hash)
	{
		$password = Keys::where('name','converter_key')->first()->value; 	// get hash saved on db
		if($password!=$hash) App::abort(403, 'Unauthorized action.');		// compare hash input and db hash, abort if not equal
		
		$v = Validator::make(Input::all(), array(		// validation message and rules
			'file' => 'required|mimes:xlsx'
		), array(
			'file.required' => 'Please select the Excel template to convert.',
			'file.mimes' => 'The file you were trying to upload does not have the correct format.'
		));
		if($v->fails()){	// redirect back if validation fails
			return Redirect::route('getConverter')->withErrors($v->errors());
		}
		
		$file = Input::file('file');								// get input file
		$destinationPath = 'temp'; 									// set temporary directory
		$extension = $file->getClientOriginalExtension();			// get file extension
		$filename = rand(11111,99999).time().'.'.$extension;		// generate random filename
		$upload_success = $file->move($destinationPath, $filename); // upload to server
		
		// init excel reader
		$excel = Excel::load('temp/'.$filename);
		$sheet = $excel->getActiveSheet();

		// init vfa array
		// set money factor
		switch((string)$sheet->getCell('C6')->getValue()){
			case 'Millions':
				$money_factor = '1000000'; break;
			case 'Thousands':
				$money_factor = '1000'; break;
			default:
				$money_factor = '1';				
		}
		$vfa = array(
			'bal' => array(),
			'prib' => array(),
			'prib_s' => array(),
			'ubal1' => array(),
			'ubal2' => array(),
			'ubal2_s' => array(),
			'report' => '57',
			'TSID' => '=AFCG8lRG5QBQF0DBlgAEgNQRMxAWdFBIQxA',
			'anstep' => '12',	// yearly
			'repern' => '12',
			'YEAR' => date('Y', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell('B10')->getValue())),
			'money_factor' => $money_factor,
			'valedin' => (string)$sheet->getCell('B6')->getCalculatedValue(),
			'Otrasli' => (string)$sheet->getCell('B4')->getCalculatedValue(),
			'OrgFoma' => '',
			'title' => (string)$sheet->getCell('B2')->getValue(),
			'GGrafQuality' => '2',
			'GColumnsNumber' => '4',
			'GReportStyle' => '0',
			'GCritica' => '0',
			'GFont' => 'Arial, Helvetica, sans-serif',
			'GFontSize' => '10',
			'GOpenReport' => '4',
			'finyear_start' => '0',
			'printOglavlen' => '1',
			'GUseDefaultFormuls' => '0',
			'GCriticaPresume' => '1',
			'GColored' => '1',
			'GBold' => '0',
			'GExplodeNumbers' => '1',
			'GUseGraph' => '1',
			'GOpenReportNewWindow' => '0',
			'GSaveRawData' => '0',
			'GAnaliticBalance' => '0',
			'GPrintUserIndicators' => '0',
			'GAnaliticPribUb' => '0',
			'GAmerican' => '1',
			'og' => array(
				'oglavlen_1' => 'on',
				'oglavlen_1_1' => 'on',
				'oglavlen_1_2' => 'on',
				'oglavlen_1_3' => 'on',
				'oglavlen_1_3_1' => 'on',
				'oglavlen_1_3_2' => 'on',
				'oglavlen_1_4' => 'on',
				'oglavlen_2' => 'on',
				'oglavlen_2_1' => 'on',
				'oglavlen_2_2' => 'on',
				'oglavlen_2_3' => 'on',
				'oglavlen_2_4' => 'on',
				'oglavlen_3' => 'on',
				'oglavlen_3_1' => 'on',
				'oglavlen_3_2' => 'on',
				'oglavlen_4' => 'on',
				'oglavlen_4_1' => 'on',
				'oglavlen_4_2' => 'on'
			)
		);
		
		$count_items = 0;
		// check last balance sheet
		$balance_sheet_column_array = array('B','C','D','E');
		foreach($balance_sheet_column_array as $col){
			// check exist
			if($sheet->getCell($col.'42')->getCalculatedValue() > 0){
				$count_items++;
				$vfa['bal'][] = array(
					
					'PropertyPlantAndEquipment' => ($sheet->getCell($col.'26')->getCalculatedValue()) ? $sheet->getCell($col.'26')->getCalculatedValue() : "0",
						'InvestmentProperty' => ($sheet->getCell($col.'27')->getCalculatedValue()) ? $sheet->getCell($col.'27')->getCalculatedValue() : "0",
						'Goodwill' => ($sheet->getCell($col.'28')->getCalculatedValue()) ? $sheet->getCell($col.'28')->getCalculatedValue() : "0",
						'IntangibleAssetsOtherThanGoodwill' => ($sheet->getCell($col.'29')->getCalculatedValue()) ? $sheet->getCell($col.'29')->getCalculatedValue() : "0",
						'InvestmentAccountedForUsingEquityMethod' => ($sheet->getCell($col.'30')->getCalculatedValue()) ? $sheet->getCell($col.'30')->getCalculatedValue() : "0",
						'InvestmentsInSubsidiariesJointVenturesAndAssociates' => ($sheet->getCell($col.'31')->getCalculatedValue()) ? $sheet->getCell($col.'31')->getCalculatedValue() : "0",
						'NoncurrentBiologicalAssets' => ($sheet->getCell($col.'32')->getCalculatedValue()) ? $sheet->getCell($col.'32')->getCalculatedValue() : "0",
						'NoncurrentReceivables' => ($sheet->getCell($col.'33')->getCalculatedValue()) ? $sheet->getCell($col.'33')->getCalculatedValue() : "0",
						'NoncurrentInventories' => ($sheet->getCell($col.'34')->getCalculatedValue()) ? $sheet->getCell($col.'34')->getCalculatedValue() : "0",
						'DeferredTaxAssets' => ($sheet->getCell($col.'35')->getCalculatedValue()) ? $sheet->getCell($col.'35')->getCalculatedValue() : "0",
						'CurrentTaxAssetsNoncurrent' => ($sheet->getCell($col.'36')->getCalculatedValue()) ? $sheet->getCell($col.'36')->getCalculatedValue() : "0",
						'OtherNoncurrentFinancialAssets' => ($sheet->getCell($col.'37')->getCalculatedValue()) ? $sheet->getCell($col.'37')->getCalculatedValue() : "0",
						'OtherNoncurrentNonfinancialAssets' => ($sheet->getCell($col.'38')->getCalculatedValue()) ? $sheet->getCell($col.'38')->getCalculatedValue() : "0",
						'NoncurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'39')->getCalculatedValue()) ? $sheet->getCell($col.'39')->getCalculatedValue() : "0",
						'NoncurrentAssets' => ($sheet->getCell($col.'40')->getCalculatedValue()) ? $sheet->getCell($col.'40')->getCalculatedValue() : "0",
						
						'Inventories' => ($sheet->getCell($col.'16')->getCalculatedValue()) ? $sheet->getCell($col.'16')->getCalculatedValue() : "0",
						'TradeAndOtherCurrentReceivables' => ($sheet->getCell($col.'15')->getCalculatedValue()) ? $sheet->getCell($col.'15')->getCalculatedValue() : "0",
						'CurrentTaxAssetsCurrent' => ($sheet->getCell($col.'17')->getCalculatedValue()) ? $sheet->getCell($col.'17')->getCalculatedValue() : "0",
						'CurrentBiologicalAssets' => ($sheet->getCell($col.'18')->getCalculatedValue()) ? $sheet->getCell($col.'18')->getCalculatedValue() : "0",
						'OtherCurrentFinancialAssets' => ($sheet->getCell($col.'19')->getCalculatedValue()) ? $sheet->getCell($col.'19')->getCalculatedValue() : "0",
						'OtherCurrentNonfinancialAssets' => ($sheet->getCell($col.'20')->getCalculatedValue()) ? $sheet->getCell($col.'20')->getCalculatedValue() : "0",
						'CashAndCashEquivalents' => ($sheet->getCell($col.'14')->getCalculatedValue()) ? $sheet->getCell($col.'14')->getCalculatedValue() : "0",
						'CurrentNoncashAssetsPledgedAsCollateral' => ($sheet->getCell($col.'21')->getCalculatedValue()) ? $sheet->getCell($col.'21')->getCalculatedValue() : "0",
						'NoncurrentAssetsOrDisposalGroups' => ($sheet->getCell($col.'22')->getCalculatedValue()) ? $sheet->getCell($col.'22')->getCalculatedValue() : "0",
						'CurrentAssets' => ($sheet->getCell($col.'23')->getCalculatedValue()) ? $sheet->getCell($col.'23')->getCalculatedValue() : "0",
						
						'Assets' => ($sheet->getCell($col.'42')->getCalculatedValue()) ? $sheet->getCell($col.'42')->getCalculatedValue() : "0",
						
						'IssuedCapital' => ($sheet->getCell($col.'67')->getCalculatedValue()) ? $sheet->getCell($col.'67')->getCalculatedValue() : "0",
						'SharePremium' => ($sheet->getCell($col.'68')->getCalculatedValue()) ? $sheet->getCell($col.'68')->getCalculatedValue() : "0",
						'TreasuryShares' => ($sheet->getCell($col.'69')->getCalculatedValue()) ? $sheet->getCell($col.'69')->getCalculatedValue() : "0",
						'OtherEquityInterest' => ($sheet->getCell($col.'70')->getCalculatedValue()) ? $sheet->getCell($col.'70')->getCalculatedValue() : "0",
						'OtherReserves' => ($sheet->getCell($col.'71')->getCalculatedValue()) ? $sheet->getCell($col.'71')->getCalculatedValue() : "0",
						'RetainedEarnings' => ($sheet->getCell($col.'72')->getCalculatedValue()) ? $sheet->getCell($col.'72')->getCalculatedValue() : "0",
						'NoncontrollingInterests' => ($sheet->getCell($col.'73')->getCalculatedValue()) ? $sheet->getCell($col.'73')->getCalculatedValue() : "0",
						'Equity' => ($sheet->getCell($col.'74')->getCalculatedValue()) ? $sheet->getCell($col.'74')->getCalculatedValue() : "0",
						
						'NoncurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'57')->getCalculatedValue()) ? $sheet->getCell($col.'57')->getCalculatedValue() : "0",
						'OtherLongtermProvisions' => ($sheet->getCell($col.'58')->getCalculatedValue()) ? $sheet->getCell($col.'58')->getCalculatedValue() : "0",
						'NoncurrentPayables' => ($sheet->getCell($col.'59')->getCalculatedValue()) ? $sheet->getCell($col.'59')->getCalculatedValue() : "0",
						'DeferredTaxLiabilities' => ($sheet->getCell($col.'60')->getCalculatedValue()) ? $sheet->getCell($col.'60')->getCalculatedValue() : "0",
						'CurrentTaxLiabilitiesNoncurrent' => ($sheet->getCell($col.'61')->getCalculatedValue()) ? $sheet->getCell($col.'61')->getCalculatedValue() : "0",
						'OtherNoncurrentFinancialLiabilities' => ($sheet->getCell($col.'62')->getCalculatedValue()) ? $sheet->getCell($col.'62')->getCalculatedValue() : "0",
						'OtherNoncurrentNonfinancialLiabilities' => ($sheet->getCell($col.'63')->getCalculatedValue()) ? $sheet->getCell($col.'63')->getCalculatedValue() : "0",
						'NoncurrentLiabilities' => ($sheet->getCell($col.'64')->getCalculatedValue()) ? $sheet->getCell($col.'64')->getCalculatedValue() : "0",
						
						'CurrentProvisionsForEmployeeBenefits' => ($sheet->getCell($col.'47')->getCalculatedValue()) ? $sheet->getCell($col.'47')->getCalculatedValue() : "0",
						'OtherShorttermProvisions' => ($sheet->getCell($col.'48')->getCalculatedValue()) ? $sheet->getCell($col.'48')->getCalculatedValue() : "0",
						'TradeAndOtherCurrentPayables' => ($sheet->getCell($col.'49')->getCalculatedValue()) ? $sheet->getCell($col.'49')->getCalculatedValue() : "0",
						'CurrentTaxLiabilitiesCurrent' => ($sheet->getCell($col.'50')->getCalculatedValue()) ? $sheet->getCell($col.'50')->getCalculatedValue() : "0",
						'OtherCurrentFinancialLiabilities' => ($sheet->getCell($col.'51')->getCalculatedValue()) ? $sheet->getCell($col.'51')->getCalculatedValue() : "0",
						'OtherCurrentNonfinancialLiabilities' => ($sheet->getCell($col.'52')->getCalculatedValue()) ? $sheet->getCell($col.'52')->getCalculatedValue() : "0",
						'LiabilitiesIncludedInDisposalGroups' => ($sheet->getCell($col.'53')->getCalculatedValue()) ? $sheet->getCell($col.'53')->getCalculatedValue() : "0",
						'CurrentLiabilities' => ($sheet->getCell($col.'54')->getCalculatedValue()) ? $sheet->getCell($col.'54')->getCalculatedValue() : "0",
						
						'Liabilities' => ($sheet->getCell($col.'76')->getCalculatedValue()) ? $sheet->getCell($col.'76')->getCalculatedValue() : "0",
						'EquityAndLiabilities' => ($sheet->getCell($col.'78')->getCalculatedValue()) ? $sheet->getCell($col.'78')->getCalculatedValue() : "0"
				);
			}
		}
		
		//income statement
		$income_statement_column_array = array('H','I','J','K');
		for($x=0;$x<($count_items-1);$x++){
			$vfa['prib'][] = array(
				
				'Revenue' => ($sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'12')->getCalculatedValue() : "0",
				'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'13')->getCalculatedValue() : "0",
				'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'14')->getCalculatedValue() : "0",
				
				'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'16')->getCalculatedValue() : "0",
				'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'17')->getCalculatedValue() : "0",
				'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'18')->getCalculatedValue() : "0",
				'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'19')->getCalculatedValue() : "0",
				'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'20')->getCalculatedValue() : "0",
				'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'21')->getCalculatedValue() : "0",
				
				'DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed' => ($sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'23')->getCalculatedValue() : "0",
				'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'24')->getCalculatedValue() : "0",
				'GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost' => ($sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'25')->getCalculatedValue() : "0",
				'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'26')->getCalculatedValue() : "0",
				'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'27')->getCalculatedValue() : "0",
				'ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod' => ($sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'28')->getCalculatedValue() : "0",
				'GainsLossesArisingFromDifference' => ($sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'29')->getCalculatedValue() : "0",
				'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'30')->getCalculatedValue() : "0",
				
				'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'32')->getCalculatedValue() : "0",
				'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'33')->getCalculatedValue() : "0",
				'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'34')->getCalculatedValue() : "0",
				'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x].'36')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'36')->getCalculatedValue() : "0",
				'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'38')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'38')->getCalculatedValue() : "0",
				'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x].'39')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x].'39')->getCalculatedValue() : "0"
			);
			
			$vfa['prib_s'][] = array(
				'Revenue' => ($sheet->getCell($income_statement_column_array[$x+1].'12')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'12')->getCalculatedValue() : "0",
				'CostOfSales' => ($sheet->getCell($income_statement_column_array[$x+1].'13')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'13')->getCalculatedValue() : "0",
				'GrossProfit' => ($sheet->getCell($income_statement_column_array[$x+1].'14')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'14')->getCalculatedValue() : "0",
				
				'OtherIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'16')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'16')->getCalculatedValue() : "0",
				'DistributionCosts' => ($sheet->getCell($income_statement_column_array[$x+1].'17')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'17')->getCalculatedValue() : "0",
				'AdministrativeExpense' => ($sheet->getCell($income_statement_column_array[$x+1].'18')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'18')->getCalculatedValue() : "0",
				'OtherExpenseByFunction' => ($sheet->getCell($income_statement_column_array[$x+1].'19')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'19')->getCalculatedValue() : "0",
				'OtherGainsLosses' => ($sheet->getCell($income_statement_column_array[$x+1].'20')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'20')->getCalculatedValue() : "0",
				'ProfitLossFromOperatingActivities' => ($sheet->getCell($income_statement_column_array[$x+1].'21')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'21')->getCalculatedValue() : "0",
				
				'DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed' => ($sheet->getCell($income_statement_column_array[$x+1].'23')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'23')->getCalculatedValue() : "0",
				'GainsLossesOnNetMonetaryPosition' => ($sheet->getCell($income_statement_column_array[$x+1].'24')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'24')->getCalculatedValue() : "0",
				'GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost' => ($sheet->getCell($income_statement_column_array[$x+1].'25')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'25')->getCalculatedValue() : "0",
				'FinanceIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'26')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'26')->getCalculatedValue() : "0",
				'FinanceCosts' => ($sheet->getCell($income_statement_column_array[$x+1].'27')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'27')->getCalculatedValue() : "0",
				'ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod' => ($sheet->getCell($income_statement_column_array[$x+1].'28')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'28')->getCalculatedValue() : "0",
				'GainsLossesArisingFromDifference' => ($sheet->getCell($income_statement_column_array[$x+1].'29')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'29')->getCalculatedValue() : "0",
				'ProfitLossBeforeTax' => ($sheet->getCell($income_statement_column_array[$x+1].'30')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'30')->getCalculatedValue() : "0",
				
				'IncomeTaxExpenseContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'32')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'32')->getCalculatedValue() : "0",
				'ProfitLossFromContinuingOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'33')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'33')->getCalculatedValue() : "0",
				'ProfitLossFromDiscontinuedOperations' => ($sheet->getCell($income_statement_column_array[$x+1].'35')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'35')->getCalculatedValue() : "0",
				'ProfitLoss' => ($sheet->getCell($income_statement_column_array[$x+1].'36')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'36')->getCalculatedValue() : "0",
				'OtherComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'38')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'38')->getCalculatedValue() : "0",
				'ComprehensiveIncome' => ($sheet->getCell($income_statement_column_array[$x+1].'39')->getCalculatedValue()) ? $sheet->getCell($income_statement_column_array[$x+1].'39')->getCalculatedValue() : "0"
			);
		}
		
		//additional
		for($x=0;$x<($count_items);$x++){
			$vfa['ubal1'][] = array(
				'staff' => '0'
			);
		}
		for($x=0;$x<($count_items-1);$x++){
			$vfa['ubal2'][] = array(
				'DepreciationAndAmortisationExpense' => '0'
			);
			$vfa['ubal2_s'][] = array(
				'DepreciationAndAmortisationExpense' => '0'
			);
		}
		
		unlink('temp/'.$filename);	// delete file
		file_put_contents('temp/temporary_vfa.vfa', json_encode($vfa));		// save file contents as json
		return Response::download('temp/temporary_vfa.vfa', 'creditbpo_converted.vfa');		// stream for download
		
	}
	
	//-----------------------------------------------------
    //  Function 14.4: get_random_string
	//  Description: function that generates random string from list of valid characters
    //-----------------------------------------------------
	private function get_random_string($valid_chars, $length)
	{
		// start with an empty random string
		$random_string = "";

		// count the number of chars in the valid chars string so we know how many choices we have
		$num_valid_chars = strlen($valid_chars);

		// repeat the steps until we've created a string of the right length
		for ($i = 0; $i < $length; $i++)
		{
			// pick a random number from 1 up to the number of valid chars
			$random_pick = mt_rand(1, $num_valid_chars);

			// take the random character out of the string of valid chars
			// subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
			$random_char = $valid_chars[$random_pick-1];

			// add the randomly-chosen char onto the end of our string so far
			$random_string .= $random_char;
		}

		// return our finished random string
		return $random_string;
	}
	
}