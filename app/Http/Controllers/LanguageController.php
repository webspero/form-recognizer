<?php
use \Exception as Exception;
use Illuminate\Support\Facades\App;
//======================================================================
//  Class 30: Language Controller
//      Functions related to Language translation
//      are routed to this class
//======================================================================
class LanguageController extends BaseController {
	
    //-----------------------------------------------------
    //  Function 30.1: getChangeLanguage
    //      Changes the language file referenced
    //-----------------------------------------------------
	public function getChangeLanguage($lang)
	{
        /*  Saves the chosen language in a Session variable */
		Session::put('language',$lang);
		
        /*  Language exists */
		try {
			return Redirect::back();
		}
        /*  The language chosen does not exist  */
        catch (Exception $e) {
			return Redirect::to('/');
		}
	}
	
}