<?php

use CreditBPO\Models\Bank;
use Illuminate\Support\Facades\Redirect;
use Devio\Pipedrive\Pipedrive;
use \Exception as Exception;
//======================================================================================
//  Class X: CmapController
//  Description: Contains all the functions regarding CMAP free trial account creation
//======================================================================================
class FreeTrialController extends BaseController {
	public function InitiatePipedrive() {
		$token = '4a87e91842ccc34d5e586bb9a184fa070a6eee95';
		$pipedrive = new Pipedrive($token);
		return $pipedrive;
	}
   
	//-----------------------------------------------------
    //  Function X.XX: getCreateFreeTrialAccount
	//  Description: get create free trial account page
    //-----------------------------------------------------
	public function getCreateFreeTrialAccount()
	{
		return View::make('create_free_trial_account');
   }

	//-----------------------------------------------------
    //  Function 51.13: getOrganizationTypeByName
    //      functinon to get organizations ordered by type
    //-----------------------------------------------------
	public function getOrganizationByName($name){
		$orgnanizationDb = new Bank;
		$orgnanization = $orgnanizationDb->getOrganizationByName($name);

		if ($orgnanization == null) {
			return $orgnanization = STS_NG;
		}

		return $orgnanization;
	}

	//-----------------------------------------------------
    //  Function X.XX: postCmapCreateTrialAccounts
	//  Description: post process to create cmap free trial accounts
    //-----------------------------------------------------
	public function postCreateFreeTrialAccount()
	{
		try{$messages = array(	// validation message
			'basic_email.unique' => 'The Email you are trying to register is already taken.',
			'supervisor_email.unique' => 'The Email you are trying to register is already taken.',
			'supervisor_email.in' => 'The same as basic emmail',
			'phone.regex' => 'The phone number should be a minimum of 7 and a maximum of 20 characters',
		);

		$rules = array(		// validation rules
			'basic_email'	=> 'unique:tbllogin,email|email|unique:supervisor_application,email',
			'supervisor_email'	=> 'unique:tbllogin,email|email|unique:supervisor_application,email',
			'phone' => 'regex:/^.{7,20}$/',
	    );

		$validator = Validator::make(Input::all(), $rules, $messages);  // run validation

		if($validator->fails()) {	// if error redirect back and show error
            return Redirect::route('getCreateFreeTrialAccount')->withErrors($validator)->withInput();
		}
		else
		{
			//get details from form
			$name = request('name');
			$phone = request('phone');
			$email = request('email');
			$basic_email = request('basic_email');
			$sup_email = request('supervisor_email');
			$bank_type = request('bank_type_hidden');
			$org_name = request('bank_name');
			
			/*Get bank details*/
			$bankDb = new Bank;
			$bank = $bankDb->getOrganizationByName($org_name);
			if ($bank != null) {
				$bank->update(array('bank_type' => $bank_type));
			} else {
				$bank = new Bank;
				$bank->bank_name = $org_name;
				$bank->bank_type = $bank_type;
				$bank->contact_person = $name;
				$bank->bank_phone = $phone;
				$bank->bank_email = $email;
				$bank->is_standalone = 1;
				$bank->save();
			}
         
         $sup_random_password = $this->generateRandomPassword();
         $sup_pass = Hash::make($sup_random_password);

         $supervisor                 = new User();
         $supervisor->email          = $sup_email;
         $supervisor->password       = $sup_pass;
         $supervisor->job_title      = "Supervisor";
         $supervisor->name      		 = request('supervisor_username');
         $supervisor->role           = 4;
         $supervisor->status         = 2;
         $supervisor->trial_flag     = 0;
         $supervisor->bank_id        = $bank->id;

         $supervisor->save();

         $payment                    = new Transaction();
         $payment->loginid           = $supervisor->loginid;
         $payment->amount            = 10000.00;
         $payment->ccy               = 'PHP';
         $payment->status            = 'S';
         $payment->description       = 'Bank User Subscription';
         $payment->created_at        = date('Y-m-d H:i:s');
         $payment->updated_at        = date('Y-m-d H:i:s');

         $payment->save();

         $subscribe                  = new BankSubscription();
         $subscribe->bank_id         = $supervisor->loginid;
         $subscribe->expiry_date     = date('Y-m-d H:i:s', strtotime('+1 month'));
         $subscribe->created_at      = date('Y-m-d H:i:s');
         $subscribe->updated_at      = date('Y-m-d H:i:s');

         $subscribe->save();

			// create the user, pre-populate some fields
         $basic_random_password = $this->generateRandomPassword();
			$basic_pass = Hash::make($basic_random_password);
			
			$user = new User();
			$user->email = $basic_email;
			$user->password = $basic_pass;
			$user->job_title = "Owner Title";
			$user->name = request('basic_username');
			$user->role = 1;
			$user->status = 2;
			$user->trial_flag = 1;
			$user->save();

			$loginid = $user->loginid;

			// create the entity/questionnaire with pre populated fields
			$entity = new Entity();
			$entity->loginid = $loginid;
			$entity->companyname = $bank->bank_name;
			$entity->entity_type = 0;
			$entity->website = '';
			$entity->is_agree = 1;
			$entity->is_paid = 1;
			$entity->status = 0;
			$entity->current_bank = $bank->id;
			$entity->is_independent = 1;
			$entity->save();

			// create 2nd entity as free questionnaire
			$entity = new Entity();
			$entity->loginid = $loginid;
			$entity->companyname = $bank->bank_name;
			$entity->entity_type = 0;
			$entity->website = '';
			$entity->is_agree = 1;
			$entity->is_paid = 1;
			$entity->status = 0;
			$entity->current_bank = $bank->id;
			$entity->is_independent = 1;
			$entity->save();

			// create organization/person/deal on pipedrive
			$pipedrive = $this->InitiatePipedrive();

			//check if 'type' org field is already created
			$org_fields = $pipedrive->organizationFields->all();
			$org_field_names = array();

			foreach ($org_fields->getData() as $org_field) {
				array_push($org_field_names, $org_field->name);
			}

			// create 'type' org field if 'type' field is not yet created
			if (!in_array('Type', $org_field_names)) {
				$pipedrive->organizationFields->add([
					'name' => 'Type',
					'options' => [
						'Bank',
						'Non-Bank Financing',
						'Corporate',
						'Government'
					],
					'field_type' => 'enum',
				]);
			}
			
			// get key of 'type' field (used to set organziation type when adding new organization)
			$org_fields = $pipedrive->organizationFields->all();
			foreach ($org_fields->getData() as $org_field) {
				if ($org_field->name == 'Type') {
					$type_field_key = $org_field->key;
				break;
				}
			}

			// Add new organization on pipedrive
			$new_org = $pipedrive->organizations->add([
				'name' => $org_name,
				$type_field_key => $bank_type,
			]);

			// Add new person on pipedrive
			$new_person = $pipedrive->persons->add([
				'name' => $name,
				'org_id' => $new_org->getData()->id,
				'phone' => $phone,
				'email' => $email,
			]);

			// create new deal
			$new_deal = $pipedrive->deals->add([
				'title' => $new_org->getData()->name.' deal',
				'person_id' => $new_person->getData()->id,
				'org_id' => $new_org->getData()->id,
			]);
			
			// initial note to the new deal
			$note_content = "<p>Created by CreditBPO free trial register page using the following information:<br><br>
									Name: {$name} <br>
									Organization: {$org_name} <br>
									Organization Type: {$bank_type} <br>
									Phone Number: {$phone} <br>
									Email: {$email}";
			
			// create a note to the new deal
			$pipedrive->notes->add([
				'content' => $note_content,
				'deal_id' => $new_deal->getData()->id,
			]);

			try{
				// Send access details email to organization representative
				Mail::send('emails.free_trial_representative', array('org_name' => $org_name,
																		'email' => $email,
																		'sup_email' => $sup_email,
																		'basic_email' => $basic_email,), function($message) use ($email){
					$message->to($email, $email)
						->subject('Your CreditBPO Free Trial is now ready!');
					$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
					$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
				});
			}catch(Exception $e){
            //
            }

            try{
				// Send access details email to basic user
				Mail::send('emails.free_trial_basic', array('org_name' => $org_name, 
																		'basic_email' => $basic_email,
																		'basic_password' => $basic_random_password), function($message) use ($basic_email){
					$message->to($basic_email, $basic_email)
						->subject('Your CreditBPO Free Trial is now ready!');
					$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
					$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
				});
			}catch(Exception $e){
            //
            }

            try{
				// Send access details email to supervisor
				Mail::send('emails.free_trial_supervisor', array('org_name' => $org_name,
																		'sup_email' => $sup_email,
																		'sup_pass' => $sup_random_password), function($message) use ($sup_email){
					$message->to($sup_email, $sup_email)
						->subject('Your CreditBPO Free Trial is now ready!');
					$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
					$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
				});
			}catch(Exception $e){
            //
            }

			if (env("APP_ENV") == "Production") {

				try{
					Mail::send('emails.free_trial-notice', array('name' => $name, 
																						'org_name' => $org_name,
																						'org_type' => $bank_type,
																						'phone' => $phone,
																						'email_add' => $email), function($message) {
					$message->to('notify.user@creditbpo.com')
							->subject('New Free Trial Account');
					});
				}catch(Exception $e){
	            //
	            }
			}

			//redirect to free trial confirmation
			return Redirect::route('getFreeTrialConfirmation');
		}
		} catch(\Exception $e) {
			$error = array("error" => "An error has occured, please try again. If problem persists, please contact site administrator. Error Code: 19f7b91");

            $logParams = array("activity" => "Free trial registration", "stackTrace" => $e, "errorCode" => "19f7b91");

			AuditLogs::saveActivityLog($logParams);
			
			\Log::info("19f7b91: " . $e);

			return Redirect::route('getCreateFreeTrialAccount')->withErrors($error)->withInput();
		}
   }

	//-----------------------------------------------------
	//  Function XX.XX: getCmapFreeTrialConfirmation
	//      Display free trial Signup Confirmation Page
	//-----------------------------------------------------
  public function getFreeTrialConfirmation()
  {
	  return View::make('free_trial-confirmation');
  }
   
   //-----------------------------------------------------
   //  Function XX.XX: generateRandomPassword
	//  Description: Generate random password for supervisor trial account
	//-----------------------------------------------------
	public function generateRandomPassword()
	{
		$password = "";
		$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*+=";
		 
		for($i = 0; $i < 10; $i++){
			$random_int = mt_rand();
			$password .= $charset[$random_int % strlen($charset)];
		}

		return $password;
	}
}
