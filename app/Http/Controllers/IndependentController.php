<?php

use Omnipay\Omnipay;
use \Exception as Exception;
//======================================================================
//  Class 25: IndependentController
//  Description: Contains functions for Independent Keys purchasing
//======================================================================
class IndependentController extends BaseController {

	/* variable declaration */
	private $url;								// variable container
	private $merchant_id = 'CREDITBPO';			// dragonpay merchant id
	private $merchant_key = 'N6f9rEW3';			// dragonpay merchant key

	//-----------------------------------------------------
    //  Function 25.1: __construct
	//  Description: constructor class for selecting dragon pay url
    //-----------------------------------------------------
	public function __construct()
	{
		if(env("APP_ENV") == "Production"){
			$this->url = 'https://gw.dragonpay.ph/Pay.aspx';
		} else {
			$this->url = 'http://test.dragonpay.ph/Pay.aspx';
		}
	}

	//-----------------------------------------------------
    //  Function 25.2: getPaymentOption
	//  Description: function to get payment page selection
    //-----------------------------------------------------
	public function getPaymentOption($code)
	{
		return View::make('purchase_keys', array('code'=>$code));
	}

	//-----------------------------------------------------
    //  Function 25.3: getPaymentOption2
	//  Description: function to process payment
	//  This function was removed because independent key should not be paid based on CDP-943 
    //-----------------------------------------------------
	public function getPaymentOption2($code, $payment_option)
	{
        $discount_db    = new Discount;					// icreate instance of Discount
		$email          = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));	// decode email from code
		$batch          = substr($code, 10, 1);			// get batch number from code
		$quantity       = IndependentKey::where('email', $email)->where('batch', (int)$batch)->count();

        $base_price         = 3500;									// base price
        $vat_val            = 0.12;									// tax 12%
        $amount             = $base_price * $quantity;

        $discount_data = $discount_db->getDiscountByEmail($email);

        if ($discount_data) {	// check if discount is present

			if ($discount_data->type == 1) {		// percentage discount
				$amount = $amount - ($amount * ($discount_data->amount / 100));
			}
			else if ($discount_data->type == 0) {	// fixed discount
				$amount = $amount - $discount_data->amount;
			}

		}
		
		$vatable_amt = $amount / (1 + $vat_val);
		$vat_output = $amount-$vatable_amt;

		$total_price = $vatable_amt + $vat_output;
		$amount = $total_price;

        if (0 >= $amount) {
            IndependentKey::where('email', $email)
                ->where('batch', (int)$batch)
                ->update(array('is_paid'=>1));

			// email link to buyer
			if (env("APP_ENV") != "local") {

				try{
					Mail::send('emails.buyer_keys_paid', array(
							'hash'=>$code
						), function($message){
							$message->to($email, $email)
								->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

							$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
							$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
					});

				}catch(Exception $e){
	            //
	            }
			}
			// redirect to certificate download
			return Redirect::to('/certificate/'.$code);
        }

        $receipt_db     = new PaymentReceipt;		// create instance

        $receipt_data['login_id']           = 0;
        $receipt_data['total_amount']       = $amount;
        $receipt_data['vatable_amount']     = $vatable_amt;
        $receipt_data['vat_amount']         = $vat_output;
        $receipt_data['item_quantity']      = $quantity;

        $receipt_data['item_type']          = 3;            /*  Bank Keys   */
        $receipt_data['sent_status']        = STS_NG;
        $receipt_data['date_sent']          = date('Y-m-d');

		if($payment_option=="paypal"){		/* using paypal */

            $receipt_data['reference_id']       = 0;
            $receipt_data['payment_method']     = 1;
            $receipt_sts                        = $receipt_db->addPaymentReceipt($receipt_data);

			$params = array(
				'cancelUrl' => URL::to('/').'/payment_cancel',
				'returnUrl' => URL::to('/').'/payment_success/'.$code,
				'name'	=> 'CreditBPO Services',
				'description' => 'Credit Risk Rating Basic Package Keys (x'.$quantity.')',
				'amount' => number_format($amount, 2, '.', ''),
				'currency' => 'PHP',
                'reference_id'  => $receipt_sts
			);

            $itm_price = $vatable_amt / $quantity;

            $items  = array(
                array('name' => 'CreditBPO Services', 'description' => 'Credit Risk Rating Basic Package Keys (x'.$quantity.')', 'price' => $itm_price, 'quantity' => $quantity),
            );

			Session::put('params', $params);
			Session::save();

			$gateway = Omnipay::create('PayPal_Express');

			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setTestMode(false);
			} else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setTestMode(true);
			}
			$response = $gateway->purchase($params)->send();

			/*  PayPal payment sucessful    */
			if ($response->isSuccessful()) {
				print_r($response);
			}
			/*  Redirect to PayPal form     */
			else if ($response->isRedirect()) {
				$response->redirect();
			}
			/*  Error occured during Payment    */
			else {
				echo $response->getMessage();
			}

			// $response->redirect();

		} elseif($payment_option=="dragonpay") {  /* using dragonpay */
			$transaction = new Transaction();
			$transaction->loginid = 0;
			$transaction->amount = $amount;
			$transaction->ccy = "PHP";
			$transaction->description = "Credit Risk Rating Basic Package Keys (x".$quantity.")";
			$transaction->email = $email;
			$transaction->batch = (int)$batch;
			$transaction->save();

            $receipt_data['reference_id']       = $transaction->id;
            $receipt_data['payment_method']     = 2;
            $receipt_sts                        = $receipt_db->addPaymentReceipt($receipt_data);

			$data = array(
				'merchant_id' => $this->merchant_id,
				'txn_id' => $transaction->id,
				'amount' => number_format($transaction->amount, 2, ".", ""),
				'currency' => $transaction->ccy,
				'description' => $transaction->description,
				'email' => $email,
				'key' => $this->merchant_key
			);

			$seed = implode(':', $data);
			$digest = Sha1($seed);

			return Redirect::to(
				$this->url.
				"?merchantid=".$data['merchant_id'].
				"&txnid=".$data['txn_id'].
				"&amount=".$data['amount'].
				"&ccy=".$data['currency'].
				"&description=".urlencode($data['description']).
				"&email=".urlencode($data['email']).
				"&digest=".$digest
			);
		}
	}

	//-----------------------------------------------------
    //  Function 25.4: getPaymentCancel
	//  Description: landing page for cancelled Paypal payment
    //-----------------------------------------------------
	public function getPaymentCancel()
	{
		return View::make('cancel_payment');
	}

	//-----------------------------------------------------
    //  Function 25.5: getPaymentSuccess
	//  Description: postback page for successful Paypal payment
    //-----------------------------------------------------
	public function getPaymentSuccess($code)
	{
		$email = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));
		$batch = substr($code, 10, 1);

		$gateway = Omnipay::create('PayPal_Express');

		if(env("APP_ENV") == "Production"){
			$gateway->setUsername('lia.francisco_api1.gmail.com');
			$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
			$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
			$gateway->setTestMode(false);
		} else {
			$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
			$gateway->setPassword('87DWVXH4UGHD46W3');
			$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
			$gateway->setTestMode(true);
		}
		$params = Session::get('params');
		$response = $gateway->completePurchase($params)->send();
		$paypalResponse = $response->getData(); // this is the raw response object

		if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
			// Response
			$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

			$payment = new Payment();
      		$payment->loginid = 0;
      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
			$payment->email = $email;
			$payment->batch = (int)$batch;
      		$payment->save();

			IndependentKey::where('email', $email)->where('batch', (int)$batch)
			->update(array('is_paid'=>1));

			// email link to buyer
			if (env("APP_ENV") != "local") {
				try{
					Mail::send('emails.buyer_keys_paid', array(
							'hash'=>$code
						), function($message){
							$message->to($email, $email)
								->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

							$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
							$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
					});
				}catch(Exception $e){
	            //
	            }
			}

            /*--------------------------------------------------------------------
            /*	Email Receipt to Client
            /*------------------------------------------------------------------*/
            $receipt    = PaymentReceipt::where('login_id', Auth::user()->loginid)
                ->where('receipt_id', $params['reference_id'])
                ->where('sent_status', STS_NG)
                ->first();

            $receipt->sent_status = STS_OK;
			$receipt->save();
			
			$base_price = 3500;

			if (env("APP_ENV") != "local") {
				$mailHandler = new MailHandler();
				$mailHandler->sendReceipt(
					Auth::user()->loginid,
					[
						'receipt' => $receipt,
						'subtotal' => $params['amount']
					],
					'CreditBPO Payment Receipt'
				);
			}

			// redirect to certificate download
			return Redirect::to('/certificate/'.$code);
		}
	}

	//-----------------------------------------------------
    //  Function 25.6: getCertificates
	//  Description: function to display certificates page
    //-----------------------------------------------------
	public function getCertificates($code)
	{
		$email = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));
		$batch = substr($code, 10, 1);
		$keys = IndependentKey::where('email', $email)->where('is_paid',1)->get();
		foreach($keys as $k=>$v){
			if($v->is_used == 1){
				$keys[$k]->status = 'YES';
			} else {
				$keys[$k]->status = 'NO';
			}
		}
		return View::make('certificates', array('keys'=>$keys, 'code'=>$code));
	}

	//-----------------------------------------------------
    //  Function 25.7: getDownloadCertificate
	//  Description: function to download certificate
    //-----------------------------------------------------
	public function getDownloadCertificate($code, $serial_key)
	{
		$email = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));
		$key = IndependentKey::where('email', $email)->where('is_paid',1)
				->where('serial_key', $serial_key)->first();	// get serial key

		if($key){
			$data = array(
				'key' => $key
			);
			$certificate = $serial_key . '.pdf';
			$pdf = PDF::loadView('pdf.certificate', $data);		// init pdf driver
			$pdf->setPaper('letter', 'landscape');			    // init paper setting
			return $pdf->download($certificate);				// create and stream for download

		} else {
			App::abort(403, 'Unauthorized action.');
		}
	}

	//-----------------------------------------------------
    //  Function 25.8: postUpdateCertificate
	//  Description: function to update certificate
    //-----------------------------------------------------
	public function postUpdateCertificate($code)
	{
		$email = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));
		$key = IndependentKey::where('email', $email)->where('id', Input::get('cert_id'))->where('is_paid',1)->first();

		if($key){
			$key->name = Input::get('name');
			$key->save();
			return URL::to('/') . '/download_certificate/' . $code . '/'. $key->serial_key;
		}
		else {
			return "error";
		}
	}
}