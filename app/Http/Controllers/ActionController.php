<?php

use CreditBPO\Handlers\AutomateSimplifiedRatingHandler;
use CreditBPO\Libraries\HighCharts;
use CreditBPO\Services\UpdateSmeReport;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use CreditBPO\Models\Director;
use CreditBPO\Models\Certification;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Insurance;
use CreditBPO\Models\Competitor;
use CreditBPO\Models\BusinessSegment;
use CreditBPO\Models\Location;
use CreditBPO\Services\IbakusService;
use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Services\KeyRatioService;
use CreditBPO\Libraries\StringHelper;
use CreditBPO\Handlers\SMEInternalHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use \Exception as Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use CreditBPO\Services\IndustryComparison;


//======================================================================
//  Class 1: ActionController
//  Description: Contains all the functions regarding switching SME Questionnaire status
//======================================================================
class ActionController extends BaseController {

	//-----------------------------------------------------
    //  Function 1.1: postCreditInvestigation
    //  Description: function to switch Questionnaire to Submitted
    //-----------------------------------------------------
    public function getFsInputTemplates($entityId)
    {
    	try{
	    	$fs_template = DB::table('tbldocument')
	            ->where('entity_id', '=', $entityId)
	            ->where('document_group', '=', 21)
	            ->where('is_deleted', '=', 0)
	            ->get()
	            ->toArray();

	       	if(empty($fs_template)){
	       		echo 'false';
	       		return;
	       	}

	       	echo 'true';
	       	return;

       	}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
       	
    }

    //-----------------------------------------------------
    //  Function 1.1: postCreditInvestigation
    //  Description: function to switch Questionnaire to Submitted
    //-----------------------------------------------------
    public function postCreditInvestigation($id)
    {
    	try{
	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;               // default status
	        $srv_resp['messages']       = STR_EMPTY;            // variable container

	        $status = 5; // SME submitted
	        if($id) {
	            $entity = Entity::where("entityid", $id)->first();
	            if(Auth::user()->getAssociatedBank($entity->current_bank)->bank_name == "Contractors Platform") {
	                $status  = 8; // SME submitted for Document Verification
	            }
	        }
	        $entityFields       = array('status'    => $status);      
	        $updateProcess      = Entity::where('entityid', '=', $id)->update($entityFields);   // update entity
	        $entity             = Entity::where('entityid', '=', $id)
	            ->leftJoin('refcitymun', 'tblentity.cityid', 'refcitymun.id')
	            ->first();      // get entity

	        $entityFields       = array('submitted_at' => new DateTime() ); //update submitted_at
	        Entity::where('entityid', '=', $id)->update($entityFields);

	        ActivityLog::saveSubmitLog();                       // log activity to db

	        $is_contractor = Auth::user()->is_contractor;
	        $company_name = $entity->companyname;

	        if ($is_contractor) {
	            $reviewers = User::where('role', 5)->get();
	            $basic_user_email = Auth::user()->email;

	            foreach ($reviewers as $reviewer) {

	                try{
	                    Mail::send('emails.notifications.contractor_new_report', array(
	                        'report_number' => $id,
	                        'company_name' => $company_name,
	                        'reviewer' => $reviewer,
	                        'basic_user_email' => $basic_user_email,
	                    ), function ($message) use ($reviewer, $company_name){
	                        $message->to($reviewer->email, $reviewer->email)
	                        ->subject('CreditBPO: Renewal Request for '.$company_name.'.');
	                    });

	                }catch(Exception $e){
	                    //
	                    sleep(10);
	                }

	            }
	        }

			/** Deprecated CDP-1735 */
	        // send email to notify.user@creditbpo.com that a questionnaire has been submitted by SME
	        // if (env("APP_ENV") == "Production"){

	        //     //for($i=0;$i<5;$i++){

	        //         try{
	        //             Mail::send('emails.startscoring', array(
	        //                 'company_name' => $entity->companyname,
	        //                 'company_email' => $entity->email,
	        //                 'phone' => $entity->phone,
	        //                 'address1' => $entity->address1,
	        //                 'address2' => $entity->address2,
	        //                 'city' => $entity->city,
	        //                 'province' => $entity->province,
	        //                 'zipcode' => $entity->zipcode
	        //             ), function($message){
	        //                 $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
	        //                     ->subject('Alert: New Basic User Report has been submitted!');
	        //             });

	        //         }catch(Exception $e){
	        //             //
	        //             sleep(60);
	        //         }
	        //     //}
	        // }

	        $ratingHandler = new AutomateStandaloneRatingHandler();
	        // $ratingHandler->registerReport($id); //orig

	        /** Register created report for processing: automate/manual */
	        $ratingHandler->registerInnodataReport($id);

	        // crossmatch open sanctions
	        if($entity->is_premium == PREMIUM_REPORT ){
	            // $ibakus = new IbakusService();
	            // $ibakus->getRelatedPersonalities($entity->entityid);
	            $this->crossmatchOpensanction($entity);

	        }

	        if ($updateProcess) {       // if success
	            
				/** Deprecated CDP-1735 */
	            /** Email developer for created new report */
	            // if (env("APP_ENV") != "local"){
	            //     $cc = [];

	            //     if(env("APP_ENV") == "Production"){
	            //         array_push($cc, "jose.francisco@creditbpo.com");
	            //     }

	            //     try{
	            //         Mail::send('emails.startscoring', array(
	            //             'company_name' => $entity->companyname,
	            //             'company_email' => $entity->email,
	            //             'phone' => $entity->phone,
	            //             'address1' => $entity->address1,
	            //             'address2' => $entity->address2,
	            //             'city' => $entity->city,
	            //             'province' => $entity->province,
	            //             'zipcode' => $entity->zipcode
	            //         ), function($message) use ($cc){
	            //             $message->to('leovielyn.aureus@creditbpo.com')
	            //                     ->cc($cc)
	            //                     ->subject('Alert: New Basic User Report has been submitted!');
	            //         });

	            //     }catch(Exception $e){
	            //         //
	            //         sleep(10);
	            //     }

	            // }
	            
	             if($entity->is_premium == SIMPLIFIED_REPORT) {
	                $simplifiedRatingHandler = new AutomateSimplifiedRatingHandler();
	                $simplifiedRatingHandler->processReport($id);
	             }else{
	                $keyRatio = new KeyRatioService();
	                $keyRatio->generateKeyRatioByEntityId($id);
	             }
	            
	            if ((!Input::has('action')) && (!Input::has('section'))) {
	                if($entity->is_premium == PREMIUM_REPORT) {
	                    return Redirect::to('/premium/confirmation/tab1')->with('success', '<p>Transaction Completed.</p><p><a href="../../logout">Click here to logout</a></p>');
	                } else {
	                    return Redirect::to('/sme/confirmation/tab1')->with('success', '<p>Transaction Completed.</p><p>Analysis will now begin. We will email you upon completion of your report in approximately '.($entity->fs_input_who == 1 ? '5 business days' : '1 to 2 business days').'.</p><p><a href="../../dashboard/index">Go back to Dashboard</a></p>');
	                }
	            }
	            else {
	                $srv_resp['sts']        = STS_OK;
	                $srv_resp['messages']   = 'Successfully Added Record';
	                return json_encode($srv_resp);
	            }
	        }
	        else {                      // if failed
	            if ((!Input::has('action')) && (!Input::has('section'))) {
	                if($entity->is_premium == PREMIUM_REPORT ) {
	                    return Redirect::to('/premium/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../logout">click here to close</a>');
	                } else {
	                    return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../dashboard/index">click here to Index Page</a>');
	                }
	            }
	            else {
	                $srv_resp['messages']   = 'An error occured while updating the user.';
	                return json_encode($srv_resp);
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.1: postFinancialAnalysis
    //  Description: function to switch Questionnaire to Submitted,
    //				copied over from postCreditInvestigation
    //-----------------------------------------------------
    public function postFinancialAnalysis($id)
    {

    	try{
	        /** Initialize Server Response  */
	        $srv_resp['sts']            = STS_NG;               // default status
	        $srv_resp['messages']       = STR_EMPTY;            // variable container

	        $status = 5; // SME submitted
	        if($id) {
	            $entity = Entity::where("entityid", $id)->first();
	            if(Auth::user()->getAssociatedBank($entity->current_bank)->bank_name == "Contractors Platform") {
	                $status  = 8; // SME submitted for Document Verification
	            }
	        }
	        $entityFields       = array('status'    => $status, 'updated_at' => DB::raw('NOW()'));      
	        $updateProcess      = Entity::where('entityid', '=', $id)->update($entityFields);   // update entity
	        $entity             = Entity::where('entityid', '=', $id)
	            ->leftJoin('refcitymun', 'tblentity.cityid', 'refcitymun.id')
	            ->first();      // get entity
			$transcription_reviewer = DB::table('tblexternal_transcriber_account')->where('status', 1)->get();
	        $entityFields       = array('submitted_at' => new DateTime() ); //update submitted_at
	        Entity::where('entityid', '=', $id)->update($entityFields);

	        ActivityLog::saveSubmitLog();                       // log activity to db

	        $is_contractor = Auth::user()->is_contractor;
	        $company_name = $entity->companyname;

	        if ($is_contractor) {
	            $reviewers = User::where('role', 5)->get();
	            $basic_user_email = Auth::user()->email;

	            foreach ($reviewers as $reviewer) {

	                try{
	                    Mail::send('emails.notifications.contractor_new_report', array(
	                        'report_number' => $id,
	                        'company_name' => $company_name,
	                        'reviewer' => $reviewer,
	                        'basic_user_email' => $basic_user_email,
	                    ), function ($message) use ($reviewer, $company_name){
	                        $message->to($reviewer->email, $reviewer->email)
	                        ->subject('CreditBPO: Renewal Request for '.$company_name.'.');
	                    });
	                }catch(Exception $e){
	                    //
	                }
	            }
	        }

			/** Deprecated CDP-1735 */
	        // send email to notify.user@creditbpo.com that a questionnaire has been submitted by SME
	        // if (env("APP_ENV") == "Production"){

	        //     try{
	        //         Mail::send('emails.startscoring', array(
	        //             'company_name' => $entity->companyname,
	        //             'company_email' => $entity->email,
	        //             'phone' => $entity->phone,
	        //             'address1' => $entity->address1,
	        //             'address2' => $entity->address2,
	        //             'city' => $entity->city,
	        //             'province' => $entity->province,
	        //             'zipcode' => $entity->zipcode
	        //         ), function($message){
	        //             $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
	        //                 ->subject('Alert: New Basic User Report has been submitted!');
	        //         });
	        //     }catch(Exception $e){
	        //         //
	        //     }
	        // }

	        $ratingHandler = new AutomateStandaloneRatingHandler();
	        //$ratingHandler->registerReport($id); //orig

	        /** Register created report for processing: automate/manual */
	        $ratingHandler->registerInnodataReport($id);

	        // crossmatch open sanctions
	        if($entity->is_premium == PREMIUM_REPORT ){
	            // $ibakus = new IbakusService();
	            // $ibakus->getRelatedPersonalities($entity->entityid);
	            $this->crossmatchOpensanction($entity);

	        }

	        /** Check if report has financial template */
	        $fs_template = DB::table('tbldocument')
	            ->where('entity_id', '=', $id)
	            ->where('document_group', '=', 21)
	            ->where('upload_type', '=', 5)
	            ->where('is_deleted', 0)
	            ->orderBy('document_type', 'ASC')
	            ->get()
	            ->toArray();

	        //print_r($fs_template); exit;

	        if(empty($fs_template)){

	        	$cc = array("ana.liza.bongat@creditbpo.com");

	            if(env("APP_ENV") == "Production"){
	                array_push($cc, "jose.francisco@creditbpo.com");
	            }

				/** Email developer for created new report -- DEPRECATED (CDP-1735) */ 
	        	// try{

	            //     Mail::send('emails.no-fs-input-template', array(
	            //         'company_name' => $entity->companyname,
	            //         'company_email' => $entity->email,
	            //         'phone' => $entity->phone,
	            //         'address1' => $entity->address1,
	            //         'address2' => $entity->address2,
	            //         'city' => $entity->city,
	            //         'province' => $entity->province,
	            //         'zipcode' => $entity->zipcode
	            //     ), function($message) use ($cc){
	            //         $message->to('leovielyn.aureus@creditbpo.com')
	            //                 ->cc($cc)
	            //                 ->subject('Alert: New Basic User Report has been submitted but without FS Input Template!');
	            //     });

	            // }catch(Exception $e){
	            //     //
	            // }

	        }

	        if ($updateProcess) {       // if success
	            
	            /** Email developer for created new report -- DEPRECATED (CDP-1735) */ 
	            // if (env("APP_ENV") != "local"){
	            //     $cc = array("ana.liza.bongat@creditbpo.com");

	            //     if(env("APP_ENV") == "Production"){
	            //         array_push($cc, "jose.francisco@creditbpo.com");
	            //     }

	            //     try{

	            //         Mail::send('emails.startscoring', array(
	            //             'company_name' => $entity->companyname,
	            //             'company_email' => $entity->email,
	            //             'phone' => $entity->phone,
	            //             'address1' => $entity->address1,
	            //             'address2' => $entity->address2,
	            //             'city' => $entity->city,
	            //             'province' => $entity->province,
	            //             'zipcode' => $entity->zipcode
	            //         ), function($message) use ($cc){
	            //             $message->to('leovielyn.aureus@creditbpo.com')
	            //                     ->cc($cc)
	            //                     ->subject('Alert: New Basic User Report has been submitted!');
	            //         });

	            //     }catch(Exception $e){
	            //         //
	            //     }
	            // }

	            $financial_report = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->first();
	            if($financial_report){
	                if ($entity->is_premium == SIMPLIFIED_REPORT) {
	                    $simplifiedRatingHandler = new AutomateSimplifiedRatingHandler();
	                    $simplifiedRatingHandler->processReport($id);
	                } else {
	                    $keyRatio = new KeyRatioService();
	                    $keyRatio->generateKeyRatioByEntityId($id);
	                }

	               
	                if ((!Input::has('action')) && (!Input::has('section'))) {
						

	                    $standaloneRatingHandler = new AutomateStandaloneRatingHandler();
	                    $standaloneRatingHandler->processSingleReport($id);

						Entity::where("entityid", $entity->entityid)->update(array("status" => 7));
						$financialAnalysisHandler = new FinancialAnalysisHandler();
	           	 		$financialAnalysisHandler->emailPostFinishScoring($id);

						/** Industry Comparison */
						$reportIndustryComparison = new IndustryComparison();
						$dataic = $reportIndustryComparison->getIndustryComparisonReport($id);

						if($entity->matching_optin == "Yes"){
							// Status set to 8 until report matching is signup
							return Redirect::to('report_matching/sme_signup/' . $entity->entityid);
						}else{
							return redirect('summary/'.$entity->entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname));
						}
	                }
	                else {
						/** Industry Comparison */
						$reportIndustryComparison = new IndustryComparison();
						$dataic = $reportIndustryComparison->getIndustryComparisonReport($id);
	                    $srv_resp['sts']        = STS_OK;
	                    $srv_resp['messages']   = 'Successfully Added Record';
	                    return json_encode($srv_resp);
	                }
	                return Redirect::to('/sme/confirmation/tab1')->with('success', '<p>Transaction Completed.</p><p><a href="../../dashboard/index">Go back to Dashboard</a></p>');
	            } 
	            else {
					
					

					// Skip innodata upload process, innodata process is deprecated (CDP-1616)
					DB::table('automate_standalone_rating')->where("entity_id", $entity->entityid)->update(['status' => 5]);

					if($entity->matching_optin == "Yes"){
						return Redirect::to('report_matching/sme_signup/' . $entity->entityid);
					}else{
						return redirect('summary/'.$entity->entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname));
					}

					

	                return Redirect::to('/sme/confirmation/tab1')->with('success', '<p>Transaction Completed. We will email you upon completion of your report</p><p><a href="../../dashboard/index">Go back to Dashboard</a></p>');
	            }
	        }
	        else {                      // if failed
	            if ((!Input::has('action')) && (!Input::has('section'))) {
	                if($entity->is_premium == PREMIUM_REPORT ) {
	                    return Redirect::to('/premium/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../logout">click here to close</a>');
	                } else {
	                    return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../dashboard/index">click here to Index Page</a>');
	                }
	            }
	            else {
	                $srv_resp['messages']   = 'An error occured while updating the user.';
	                return json_encode($srv_resp);
	            }
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

    }

    //-----------------------------------------------------
    //  Function 1.2: postStartScoring
    //  Description: function to switch Questionnaire to Taken by Analyst
    //-----------------------------------------------------
    public function postStartScoring($id)
    {
    	try{
	        $result = DB::table('tblscoring')
	        ->where('loginid', Auth::user()->loginid)
	        ->where('entityid', $id)
	        ->first();

	        if(@count($result) == 0){    // create scoring item in db
	            $scoring = new Scoring();
	            $scoring->loginid = Auth::user()->loginid;
	            $scoring->entityid = $id;
	            $scoring->start_date = date('Y-m-d');
	            $scoring->end_date = date('Y-m-d', strtotime("+7 days"));
	            $scoring->save();
	        }

	        $entityFields = array(
	            'status'    => 6
	        );

	        $updateProcess = Entity::where('entityid', '=', $id)->update($entityFields);    // update questionnaire status

	        if($updateProcess)
	        {
	            return Redirect::to('/summary/smesummary/'.$id);
	        }
	        else
	        {
	            return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../dashboard/index">click here to Index Page</a>');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.3: deleteReleaseScoring
    //  Description: function to switch Questionnaire back to submitted
    //-----------------------------------------------------
    public function deleteReleaseScoring($id)
    {
    	try{
	        $entityFields = array('status'  => 0);      // revert back to editable status
	        $updateProcess = Entity::where('entityid', '=', $id)->update($entityFields);

	        if($updateProcess)
	        {
	            return Redirect::to('/sme/confirmation/tab1')->with('success', 'RELEASE THIS JOB.<a href="../../dashboard/index">click here to Index Page</a>');
	        }
	        else
	        {
	            return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../dashboard/index">click here to Index Page</a>');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.4: postFinishScoring
    //  Description: function to switch and process Questionnaire for finished Scoring
    //-----------------------------------------------------
    public function postFinishScoring($id)
    {   
    	try{
	        /* create and populate scoring data in db */
	        $smescore = new Smescoring();                                       // create instance of Smescoring
	        $smescore->loginid = Auth::user()->loginid;                         // populate fields
	        $smescore->entityid = Input::get('entityid');
	        $smescore->score_rm = Input::get('bconsideration_for_save');
	        $smescore->score_cd = Input::get('cdependency_for_save');
	        $smescore->score_sd = Input::get('sdependency_for_save');
	        $smescore->score_bois = Input::get('boindex_for_save');
	        $smescore->score_boe = Input::get('ownerexp_for_save');
	        $smescore->score_mte = Input::get('mtdependency_for_save');
	        $smescore->score_sp = Input::get('successionplan_for_save');
	        $smescore->score_rfp = Input::get('ratingfiscore');
	        $smescore->score_rfpm = Input::get('ratingfinancialperformancescore');
	        $smescore->score_fars = Input::get('farawscore_for_save');
	        $smescore->score_facs = Input::get('faccscore_for_save');
	        $smescore->score_sf = Input::get('riskscoretotal_for_save');
	        $smescore->average_daily_balance = Input::get('average_daily_balance_save') ?: 0;
	        $smescore->operating_cashflow_margin = Input::get('operating_cashflow_margin_save') ?: 0;
	        $smescore->operating_cashflow_ratio = Input::get('operating_cashflow_ratio_save') ?: 0;
	        $smescore->score_bdms = Input::get('bdmaindependency_for_save') ?: 0;
	        $smescore->score_ppfi = Input::get('ppfuture_for_save') ?: 0;

	        /* create and populate ready ratios data in db */
	        $readyratiodata = new Readyratiodata();
	        $readyratiodata->loginid = Auth::user()->loginid;
	        $readyratiodata->entityid = Input::get('entityid');
	        $readyratiodata->year1 = Input::get('year1');
	        $readyratiodata->year2 = Input::get('year2');
	        $readyratiodata->gross_revenue_growth1 = Input::get('gross_revenue_growth1');
	        $readyratiodata->gross_revenue_growth2 = Input::get('gross_revenue_growth2');
	        $readyratiodata->net_income_growth1 = Input::get('net_income_growth1');
	        $readyratiodata->net_income_growth2 = Input::get('net_income_growth2');
	        $readyratiodata->gross_profit_margin1 = Input::get('gross_profit_margin1');
	        $readyratiodata->gross_profit_margin2 = Input::get('gross_profit_margin2');
	        $readyratiodata->net_profit_margin1 = Input::get('net_profit_margin1');
	        $readyratiodata->net_profit_margin2 = Input::get('net_profit_margin2');
	        $readyratiodata->net_cash_margin1 = Input::get('net_cash_margin1');
	        $readyratiodata->net_cash_margin2 = Input::get('net_cash_margin2');
	        $readyratiodata->current_ratio1 = Input::get('current_ratio1');
	        $readyratiodata->current_ratio2 = Input::get('current_ratio2');
	        $readyratiodata->debt_equity_ratio1 = Input::get('debt_equity_ratio1');
	        $readyratiodata->debt_equity_ratio2 = Input::get('debt_equity_ratio2');

	        $readyratiodata->cc_year1 = Input::get('cc_year1');
	        $readyratiodata->cc_year2 = Input::get('cc_year2');
	        $readyratiodata->cc_year3 = Input::get('cc_year3');
	        $readyratiodata->receivables_turnover1 = Input::get('receivables_turnover1');
	        $readyratiodata->receivables_turnover2 = Input::get('receivables_turnover2');
	        $readyratiodata->receivables_turnover3 = Input::get('receivables_turnover3');
	        $readyratiodata->inventory_turnover1 = Input::get('inventory_turnover1');
	        $readyratiodata->inventory_turnover2 = Input::get('inventory_turnover2');
	        $readyratiodata->inventory_turnover3 = Input::get('inventory_turnover3');
	        $readyratiodata->accounts_payable_turnover1 = Input::get('accounts_payable_turnover1');
	        $readyratiodata->accounts_payable_turnover2 = Input::get('accounts_payable_turnover2');
	        $readyratiodata->accounts_payable_turnover3 = Input::get('accounts_payable_turnover3');
	        $readyratiodata->cash_conversion_cycle1 = Input::get('cash_conversion_cycle1');
	        $readyratiodata->cash_conversion_cycle2 = Input::get('cash_conversion_cycle2');
	        $readyratiodata->cash_conversion_cycle3 = Input::get('cash_conversion_cycle3');

	        $readyratiodata->save();    // save

	        /* save key ratio summary data */
	        for ($i = 1; $i <= 3; $i++) {
	            $readyratioKeySummary = new ReadyratioKeySummary();                                       // create instance of Smescoring
	            $readyratioKeySummary->entityid = Input::get('entityid');
	            $readyratioKeySummary->Year = !empty(Input::get('cc_year' . $i)) ? Input::get('cc_year' . $i) : 0;
	            $readyratioKeySummary->IntangibleAsset = !empty(Input::get('intangible_asset' . $i)) ? Input::get('intangible_asset' . $i) : 0;
	            $readyratioKeySummary->NetTangibleAsset = !empty(Input::get('net_tangible_asset' . $i)) ? Input::get('net_tangible_asset' . $i) : 0;
	            $readyratioKeySummary->NetTangibleAsset = !empty(Input::get('net_tangible_asset' . $i)) ? Input::get('net_tangible_asset' . $i) : 0;
	            $readyratioKeySummary->NetAsset = !empty(Input::get('net_asset' . $i)) ? Input::get('net_asset' . $i) : 0;
	            $readyratioKeySummary->FinancialLeverage = !empty(Input::get('financial_leverage' . $i)) ? Input::get('financial_leverage' . $i) : 0;
	            $readyratioKeySummary->DebtRatio = !empty(Input::get('debt_ratio' . $i)) ? Input::get('debt_ratio' . $i) : 0;
	            $readyratioKeySummary->LTDtoE = !empty(Input::get('LTD_to_E' . $i)) ? Input::get('LTD_to_E' . $i) : 0;
	            $readyratioKeySummary->NCAtoNW = !empty(Input::get('NCA_to_NW' . $i)) ? Input::get('NCA_to_NW' . $i) : 0;
	            $readyratioKeySummary->FixedAsset = !empty(Input::get('fixed_asset' . $i)) ? Input::get('fixed_asset' . $i) : 0;
	            $readyratioKeySummary->CLiabilityRatio = !empty(Input::get('c_liability_ratio' . $i)) ? Input::get('c_liability_ratio' . $i) : 0;
	            $readyratioKeySummary->Capitalization = !empty(Input::get('capitalization' . $i)) ? Input::get('capitalization' . $i) : 0;
	            $readyratioKeySummary->NWC = !empty(Input::get('NWC' . $i)) ? Input::get('NWC' . $i) : 0;
	            $readyratioKeySummary->WCD = !empty(Input::get('WCD' . $i)) ? Input::get('WCD' . $i) : 0;
	            $readyratioKeySummary->InventoryNWC = !empty(Input::get('inventory_NWC' . $i)) ? Input::get('inventory_NWC' . $i) : 0;
	            $readyratioKeySummary->CurrentRatio = !empty(Input::get('current_ratio' . $i)) ? Input::get('current_ratio' . $i) : 0;
	            $readyratioKeySummary->QuickRatio = !empty(Input::get('quick_ratio' . $i)) ? Input::get('quick_ratio' . $i) : 0;
	            $readyratioKeySummary->CashRatio = !empty(Input::get('cash_ratio' . $i)) ? Input::get('cash_ratio' . $i) : 0;
	            $readyratioKeySummary->OIEeFC = !empty(Input::get('OIEeFC' . $i)) ? Input::get('OIEeFC' . $i) : 0;
	            $readyratioKeySummary->EBIT = !empty(Input::get('EBIT' . $i)) ? Input::get('EBIT' . $i) : 0;
	            $readyratioKeySummary->EBITDA = !empty(Input::get('EBITDA' . $i)) ? Input::get('EBITDA' . $i) : 0;
	            $readyratioKeySummary->GrossMargin = !empty(Input::get('gross_margin' . $i)) ? Input::get('gross_margin' . $i) : 0;
	            $readyratioKeySummary->ROS = !empty(Input::get('ROS' . $i)) ? Input::get('ROS' . $i) : 0;
	            $readyratioKeySummary->ProfitMargin = !empty(Input::get('profit_margin' . $i)) ? Input::get('profit_margin' . $i) : 0;
	            $readyratioKeySummary->ICR = !empty(Input::get('ICR' . $i)) ? Input::get('ICR' . $i) : 0;
	            $readyratioKeySummary->ROE = !empty(Input::get('ROE' . $i)) ? Input::get('ROE' . $i) : 0;
	            $readyratioKeySummary->ROE_CI = !empty(Input::get('ROE_CI' . $i)) ? Input::get('ROE_CI' . $i) : 0;
	            $readyratioKeySummary->ROA = !empty(Input::get('ROA' . $i)) ? Input::get('ROA' . $i) : 0;
	            $readyratioKeySummary->ROA_CI = !empty(Input::get('ROA_CI' . $i)) ? Input::get('ROA_CI' . $i) : 0;
	            $readyratioKeySummary->ROCE = !empty(Input::get('ROCE' . $i)) ? Input::get('ROCE' . $i) : 0;
	            $readyratioKeySummary->ReceivablesTurnover = !empty(Input::get('receivablesTurnover' . $i)) ? Input::get('receivablesTurnover' . $i) : 0;
	            $readyratioKeySummary->PayableTurnover = !empty(Input::get('payableTurnover' . $i)) ? Input::get('payableTurnover' . $i) : 0;
	            $readyratioKeySummary->InventoryTurnover = !empty(Input::get('inventoryTurnover' . $i)) ? Input::get('inventoryTurnover' . $i) : 0;
	            $readyratioKeySummary->AssetTurnover = !empty(Input::get('asset_turnover' . $i)) ? Input::get('asset_turnover' . $i) : 0;
	            $readyratioKeySummary->CAssetTurnover = !empty(Input::get('CAsset_turnover' . $i)) ? Input::get('CAsset_turnover' . $i) : 0;
	            $readyratioKeySummary->CapitalTurnover = !empty(Input::get('capital_turnover' . $i)) ? Input::get('capital_turnover' . $i) : 0;
	            $readyratioKeySummary->CCC = !empty(Input::get('CCC' . $i)) ? Input::get('CCC' . $i) : 0;
	            $readyratioKeySummary->T1 = !empty(Input::get('T1' . $i)) ? Input::get('T1' . $i) : 0;
	            $readyratioKeySummary->T2 = !empty(Input::get('T2' . $i)) ? Input::get('T2' . $i) : 0;
	            $readyratioKeySummary->T3 = !empty(Input::get('T3' . $i)) ? Input::get('T3' . $i) : 0;
	            $readyratioKeySummary->T4 = !empty(Input::get('T4' . $i)) ? Input::get('T4' . $i) : 0;
	            $readyratioKeySummary->T5 = !empty(Input::get('T5' . $i)) ? Input::get('T5' . $i) : 0;
	            $readyratioKeySummary->save();
	        }
	        
	        if(Input::has('admin-review')){ // check if it has admin review flag
	            $admin_review_flag = 1;
	        } else {
	            $admin_review_flag = 0;
	        }

	        $entityFields = array('status' => 7, 'admin_review_flag'=>$admin_review_flag);

	        if ($admin_review_flag === 0) {
	            $entityFields['completed_date'] = date('c');
	        }

	        $updateProcess = Entity::where('entityid', '=', $id)->update($entityFields); // update status to finish scoring
	        $entity = Entity::where('entityid', '=', $id)->first();
	        $value = Input::file('file');

	        /** Crossmatch checking for premium report */
	        if ($entity->is_premium == PREMIUM_REPORT) {
	            // $ibakus = new IbakusService();
	            // $ibakus->getRelatedPersonalities($pendingReport->entity_id);
	            $this->crossmatchOpensanction($entity);
	        }

	        if(Input::hasFile('file')){     // process uploaded Financial Analyst Report
	            $company_name       = rtrim($entity->companyname, '.');
	            $company_name       = trim($company_name);

	            $destinationPath = 'financial_analysis';
	            $extension = $value->getClientOriginalExtension();
	            $fileformat = 'CreditBPO Financial Analysis - %s as of %s.%s';

	            $company_name = $company_name;
	            if(strpos($company_name,"&") !== false){
	                $company_name  = preg_replace("/[&]/", "And", $company_name);
	            }
			            
	            $filename = sprintf($fileformat, $company_name, date('Y-m-d'), $extension);
	            $data = array(
	                'entity_id'=>Input::get('entityid'),
	                'document_type'=>9,
	                'document_group'=>99999,
	                'document_orig'=>$value->getClientOriginalName(),
	                'document_rename'=>$filename,
	                'created_at'=>date('Y-m-d H:i:s'),
	                'updated_at'=>date('Y-m-d H:i:s')
	            );
	            DB::table('tbldocument')->insert($data);

	            $upload_success = $value->move($destinationPath, $filename);
	            File::copy(public_path().'/financial_analysis/'.$filename, public_path().'/temp/fr_copy.pdf');

	            /**
	             * Extracts Key Ratios Summary from Ready Ratios PDF.
	             *
	             * The local instance must have an installed pdftotext command
	             * in order to utilize this feature. Check UpdateSmeReport class
	             * to see how it works.
	             *
	             * https://www.xpdfreader.com/pdftotext-man.html
	             */
	            $updateReport = new UpdateSmeReport();
	            $summary_points = $updateReport->getKeyRatiosSummary($entity);

	            $itm_idx            = 0;
	            $positive_points    = STR_EMPTY;
	            $negative_points    = STR_EMPTY;
	            $pos_count          = @count($summary_points['positive']);
	            $neg_count          = @count($summary_points['negative']);

	            foreach ($summary_points['positive'] as $pos_itm) {
	                $itm_idx++;

	                $positive_points    .= $pos_itm;

	                if ($pos_count > $itm_idx) {
	                    $positive_points    .= '|';
	                }
	            }

	            $itm_idx            = 0;

	            foreach ($summary_points['negative'] as $neg_itm) {
	                $itm_idx++;

	                $negative_points    .= $neg_itm;

	                if ($neg_count > $itm_idx) {
	                    $negative_points    .= '|';
	                }

	            }

	            $smescore->positive_points  = $positive_points;
	            $smescore->negative_points  = $negative_points;
	        }

	        $smescore->save();

	        if($updateProcess)
	        {
				/* Disable CMAP Function for Simplified and Standalone Report (CDP-1604) */
	            if($entity->is_premium == PREMIUM_REPORT){
					/** Call CMAPSearch function */
					$cmapSearch = new AutomateStandaloneRatingHandler();
					$cmapSearch->cmapSearch($entity->companyname);
					$this->createSMEReport($id);        // call function createSMEReport (creates the internal Financial Analyst Report)
				}

	            $entity = Entity::where('entityid', '=', $id)->first();

	            if (0 != $entity->current_bank) {
	                $user_bank  = User::where('bank_id', $entity->current_bank)
	                    ->where('role', 4)
	                    ->where('parent_user_id', 0)
	                    ->first();
	            } else {
	                $user_bank = null;
	            }

	            $fc_pdf = $destinationPath.'/'.$filename;

	            if (1 == $admin_review_flag && env("APP_ENV") == "Production") {
	                $email_rcp  = 'notify.user@creditbpo.com';
	                $email_cc   = '';
	            }
	            else {
	                $email_rcp  = Input::get('loginemail');
	                $email_cc   = !empty($user_bank) ? $user_bank->email : null;
	            }

	            // send alert emails regarding finished scoring questionnaire
	            $financialAnalysisHandler = new FinancialAnalysisHandler();
	            // $financialAnalysisHandler->emailPostFinishScoring($id);

	            return Redirect::to('/sme/confirmation/tab1')->with('success', 'RISK SCORING IS COMPLETE.<a href="../../dashboard/index">Click here to Index Page</a>');
	        }
	        else
	        {
	            return Redirect::to('/sme/confirmation/tab1')->with('fail', 'An error occured while updating the user. Please try again. <a href="../../dashboard/index">click here to Index Page</a>');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.5: createBccHighChart
    //  Description: Creates the Business Condition Pie Chart
    //-----------------------------------------------------
    public function createBccHighChart($entity_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 1;
	        $bcc_total  = 240;
	        $bcc_score  = 0;
	        $bcc_empty  = 0;

	        /* Chart Labels */
	        $rm_name    = trans('risk_rating.risk_management_chart');
	        $cd_name    = trans('risk_rating.customer_dependency_chart');
	        $sd_name    = trans('risk_rating.supplier_dependency_chart');
	        $boi_name   = trans('risk_rating.business_outlook_chart');

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data = DB::table('tblsmescore')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('smescoreid', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Compute for empty space on the PIE Chart
	        /*------------------------------------------------------------------*/
	        $bcc_score  = $score_data->score_rm + $score_data->score_cd + $score_data->score_sd +$score_data->score_bois;
	        $bcc_empty  = $bcc_total - $bcc_score;

	        /*--------------------------------------------------------------------
	        /*  Sets Risk Management display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_rm) {
	            /* High Rating  */
	            case 90:
	                $boe_score['rating_txt']     = trans('risk_rating.high');
	                $boe_score['rating_color']   = 'green';
	                $boe_score['rating_desc']    = trans('risk_rating.rm_high');
	                break;

	            /* Low Rating  */
	            case 30:
	            default:
	                $boe_score['rating_txt']     = trans('risk_rating.low');
	                $boe_score['rating_color']   = 'red';
	                $boe_score['rating_desc']    = trans('risk_rating.rm_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Customer Dependency display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_cd) {
	            /* High Rating  */
	            case 30:
	                $cd_score['rating_txt']     = trans('risk_rating.high');
	                $cd_score['rating_color']   = 'green';
	                $cd_score['rating_desc']    = trans('risk_rating.cd_high');
	                break;

	            /* Moderate Rating  */
	            case 20:
	                $cd_score['rating_txt']     = trans('risk_rating.moderate');
	                $cd_score['rating_color']   = 'orange';
	                $cd_score['rating_desc']    = trans('risk_rating.cd_moderate');
	                break;

	            /* Low Rating  */
	            case 10:
	            default:
	                $cd_score['rating_txt']     = trans('risk_rating.low');
	                $cd_score['rating_color']   = 'red';
	                $cd_score['rating_desc']    = trans('risk_rating.cd_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Supplier Dependency display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_sd) {
	            /* High Rating  */
	            case 30:
	                $sd_score['rating_txt']     = trans('risk_rating.high');
	                $sd_score['rating_color']   = 'green';
	                $sd_score['rating_desc']    = trans('risk_rating.sd_high');
	                break;

	            /* Moderate Rating  */
	            case 20:
	                $sd_score['rating_txt']     = trans('risk_rating.moderate');
	                $sd_score['rating_color']   = 'orange';
	                $sd_score['rating_desc']    = trans('risk_rating.sd_moderate');
	                break;

	            /* Low Rating  */
	            case 10:
	            default:
	                $sd_score['rating_txt']     = trans('risk_rating.low');
	                $sd_score['rating_color']   = 'red';
	                $sd_score['rating_desc']    = trans('risk_rating.sd_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Business Outlook display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_bois) {
	            /* High Rating  */
	            case 90:
	                $bois_score['rating_txt']     = trans('risk_rating.high');
	                $bois_score['rating_color']   = 'green';
	                $bois_score['rating_desc']    = trans('risk_rating.bo_high');
	                break;

	            /* Moderate Rating  */
	            case 60:
	                $bois_score['rating_txt']     = trans('risk_rating.moderate');
	                $bois_score['rating_color']   = 'orange';
	                $bois_score['rating_desc']    = trans('risk_rating.bo_moderate');
	                break;

	            /* Low Rating  */
	            default:
	                $bois_score['rating_txt']     = trans('risk_rating.low');
	                $bois_score['rating_color']   = 'red';
	                $bois_score['rating_desc']    = trans('risk_rating.bo_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $series_data = array(
	            array(
	                'name'      => $rm_name.' <span style="color:'.$boe_score['rating_color'].';"> '.$boe_score['rating_txt'].' </span>',
	                'color'     => "#FBD5C4",
	                'y'         => (float)$score_data->score_rm,
	            ),
	            array(
	                'name'      => $cd_name.' <span style="color:'.$cd_score['rating_color'].';"> '.$cd_score['rating_txt'].' </span>',
	                'color'     => "#E4D2E3",
	                'y'         => (float)$score_data->score_cd,
	            ),
	            array(
	                'name'      => $sd_name.' <span style="color:'.$sd_score['rating_color'].';"> '.$sd_score['rating_txt'].' </span>',
	                'color'     => "#CFEBEC",
	                'y'         => (float)$score_data->score_sd,
	            ),
	            array(
	                'name'      => $boi_name.' <span style="color:'.$bois_score['rating_color'].';"> '.$bois_score['rating_txt'].' </span>',
	                'color'     => "#BDCF8E",
	                'y'         => (float)$score_data->score_bois,
	            ),
	            array(
	                'name'      => STR_EMPTY,
	                'color'     => "#FFF",
	                'y'         => (float)$bcc_empty,
	            )
	        );

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->setBccMqPieOptions($series_data);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.6: createMqHighChart
    //  Description: Creates the Management Quality Pie Chart
    //-----------------------------------------------------
    public function createMqHighChart($entity_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 2;
	        $mq_total   = 150;
	        $mq_score   = 0;
	        $mq_empty   = 0;

	        /* Chart Labels */
	        $boe_name   = trans('risk_rating.mq_business_owner');
	        $mte_name   = trans('risk_rating.mq_management_team');
	        $bdms_name  = trans('risk_rating.mq_business_driver');
	        $sp_name    = trans('risk_rating.mq_succession_plan');
	        $ppfi_name  = trans('risk_rating.mq_past_project');

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data = DB::table('tblsmescore')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('smescoreid', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Compute for empty space on the PIE Chart
	        /*------------------------------------------------------------------*/
	        $mq_score   = $score_data->score_boe + $score_data->score_mte + $score_data->score_bdms + $score_data->score_sp + $score_data->score_ppfi;
	        $mq_empty   = (float)$mq_total - (float)$mq_score;

	        /*--------------------------------------------------------------------
	        /*  Sets Business Owner Experience display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_boe) {
	            /* High Rating  */
	            case 21:
	                $boe_score['rating_txt']     = trans('risk_rating.high');
	                $boe_score['rating_color']   = 'green';
	                break;

	            /* Moderate Rating  */
	            case 14:
	                $boe_score['rating_txt']     = trans('risk_rating.low');
	                $boe_score['rating_color']   = 'red';
	                break;

	            /* Low Rating  */
	            case 7:
	            default:
	                $boe_score['rating_txt']     = trans('risk_rating.low');
	                $boe_score['rating_color']   = 'red';
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Business Management Team Experience display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_mte) {
	            /* High Rating  */
	            case 21:
	                $mte_score['rating_txt']     = trans('risk_rating.high');
	                $mte_score['rating_color']   = 'green';
	                break;

	            /* Moderate Rating  */
	            case 14:
	                $mte_score['rating_txt']     = trans('risk_rating.low');
	                $mte_score['rating_color']   = 'red';
	                break;

	            /* Low Rating  */
	            case 7:
	            default:
	                $mte_score['rating_txt']     = trans('risk_rating.low');
	                $mte_score['rating_color']   = 'red';
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Business Driver and Main Segments display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_bdms) {
	            /* High Rating  */
	            case 36:
	                $bdms_score['rating_txt']     = trans('risk_rating.high');
	                $bdms_score['rating_color']   = 'green';
	                $bdms_score['rating_desc']    = trans('risk_rating.bd_high');
	                break;

	            /* Low Rating  */
	            case 12:
	            default:
	                $bdms_score['rating_txt']     = trans('risk_rating.low');
	                $bdms_score['rating_color']   = 'red';
	                $bdms_score['rating_desc']    = trans('risk_rating.bd_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Succession Plan display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_sp) {
	            /* High Rating  */
	            case 36:
	                $sp_score['rating_txt']     = trans('risk_rating.high');
	                $sp_score['rating_color']   = 'green';
	                $sp_score['rating_desc']    = trans('risk_rating.sp_high');
	                break;

	            /* Moderate Rating  */
	            case 24:
	                $sp_score['rating_txt']     = trans('risk_rating.moderate');
	                $sp_score['rating_color']   = 'orange';
	                $sp_score['rating_desc']    = trans('risk_rating.sp_moderate');
	                break;

	            /* Low Rating  */
	            case 12:
	            default:
	                $sp_score['rating_txt']     = trans('risk_rating.low');
	                $sp_score['rating_color']   = 'red';
	                $sp_score['rating_desc']    = trans('risk_rating.sp_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets Past Projects display data
	        /*------------------------------------------------------------------*/
	        switch ($score_data->score_ppfi) {
	            /* High Rating  */
	            case 36:
	                $ppfi_score['rating_txt']     = trans('risk_rating.high');
	                $ppfi_score['rating_color']   = 'green';
	                $ppfi_score['rating_desc']    = trans('risk_rating.pp_high');
	                break;

	            /* Low Rating  */
	            case 12:
	            default:
	                $ppfi_score['rating_txt']     = trans('risk_rating.low');
	                $ppfi_score['rating_color']   = 'red';
	                $ppfi_score['rating_desc']    = trans('risk_rating.pp_low');
	                break;
	        }

	        /*--------------------------------------------------------------------
	        /*  Sets series data of the Pie Graph
	        /*------------------------------------------------------------------*/
	        $series_data = array(
	            array(
	                'name'      => $boe_name.' <span style="color:'.$boe_score['rating_color'].';"> '.$boe_score['rating_txt'].' </span>',
	                'color'     => "#FBD5C4",
	                'y'         => (float)$score_data->score_boe
	            ),
	            array(
	                'name'      => $mte_name.' <span style="color:'.$mte_score['rating_color'].';"> '.$mte_score['rating_txt'].' </span>',
	                'color'     => "#E4D2E3",
	                'y'         => (float)$score_data->score_mte
	            ),
	            array(
	                'name'      => $bdms_name.' <span style="color:'.$bdms_score['rating_color'].';"> '.$bdms_score['rating_txt'].' </span>',
	                'color'     => "#CFEBEC",
	                'y'         => (float)$score_data->score_bdms
	            ),
	            array(
	                'name'      => $sp_name.' <span style="color:'.$sp_score['rating_color'].';"> '.$sp_score['rating_txt'].' </span>',
	                'color'     => "#BDCF8E",
	                'y'         => (float)$score_data->score_sp
	            ),
	            array(
	                'name'      => $ppfi_name.' <span style="color:'.$ppfi_score['rating_color'].';"> '.$ppfi_score['rating_txt'].' </span>',
	                'color'     => "#B4BDC3",
	                'y'         => (float)$score_data->score_ppfi
	            ),
	            array(
	                'name'      => STR_EMPTY,
	                'color'     => "#FFF",
	                'y'         => (float)$mq_empty
	            )
	        );

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->setBccMqPieOptions($series_data);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.7: setBccMqPieOptions
    //  Description: Sets the Business Condition and Management Quality Chart options
    //-----------------------------------------------------
    public function setBccMqPieOptions($series_data)
    {
    	try{
	        $chart_options = array(
	            'chart' => array(
	                'plotBackgroundColor'   => null,
	                'plotBorderWidth'       => null,
	                'plotShadow'            => false,
	                'type'                  => 'pie',
	                'height'                => '600',
	                'width'                 => '1200',
	            ),
	            'credits' => array(
	                'enabled'   => false
	            ),
	            'title' => array(
	                'text'  => STR_EMPTY
	            ),
	            'tooltip' => array(
	                'enabled'   => false
	            ),
	            'plotOptions' => array(
	                'pie' => array(
	                    'borderWidth'   => 0,
	                    'innerSize'     => '60%',
	                    'dataLabels'    => array(
	                        'enabled'   => false,
	                        'useHTML'   => true,
	                        'fontWeight' => 'normal',
	                        'allowOverlap' => true,
	                    )
	                )
	            ),
	            'legend' => array(
	                'enabled'       => true,
	                'layout'        => 'vertical',
	                'align'         => 'right',
	                'width'         => 370,
	                'verticalAlign' => 'middle',
	                'useHTML'       => true,
	                'x'             => 0,
	                'y'             => 0,
	                'itemStyle'     => array(
	                    'fontSize' => '25px'
	                )
	            ),
	            'series'    => array(array(
	                'data'          => $series_data,
	                'size'          => '100%',
	                'innerSize'     => '54%',
	                'showInLegend'  => true,
	                'dataLabels'    => array(
	                        'enabled'   => false,
	                        'useHTML'   => true,
	                    )
	                )
	            )
	        );

	        return $chart_options;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.8: createFcHighChart
    //  Description: Creates the Financial Condition Gauge Chart
    //-----------------------------------------------------
    public function createFcHighChart($entity_id, $total)
    {
    	try{

	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 3;

	        /* Chart Labels */
	        $boe_name   = trans('risk_rating.mq_business_owner');
	        $mte_name   = trans('risk_rating.mq_management_team');
	        $bdms_name  = trans('risk_rating.mq_business_driver');
	        $sp_name    = trans('risk_rating.mq_succession_plan');
	        $ppfi_name  = trans('risk_rating.mq_past_project');

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data = DB::table('tblsmescore')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('smescoreid', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart data to be displayed according to points
	        /*------------------------------------------------------------------*/

	        if($total != null) {
	            if ($total >= 1.6) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'AAA';
	            }
	            else if ($total >= 1.2 && $total < 1.6) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'AA';
	            }
	            else if ($total >= 0.8 && $total < 1.2) {
	                $margin_left    = '14x;';
	                $facs_rating    = 'A';
	            }
	            else if ($total >= 0.4 && $total < 0.8) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'BBB';
	            }
	            else if ($total >= 0 && $total < 0.4) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'BB';
	            }
	            else if ($total >= -0.4 && $total < 0) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'B';
	            }
	            else if ($total >= -0.8 && $total < -0.4) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'CCC';
	            }
	            else if ($total >= -1.2 && $total < -0.8) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'CC';
	            }
	            else if ($total >= -1.6 && $total < -1.2) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'C';
	            }
	            else if ($total < -1.6) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'D';
	            }
	        } else {
	            if ($score_data->score_facs >= 162) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'AAA';
	            }
	            else if ($score_data->score_facs >= 145) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'AA';
	            }
	            else if ($score_data->score_facs >= 127) {
	                $margin_left    = '14x;';
	                $facs_rating    = 'A';
	            }
	            else if ($score_data->score_facs >= 110) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'BBB';
	            }
	            else if ($score_data->score_facs >= 92) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'BB';
	            }
	            else if ($score_data->score_facs >= 75) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'B';
	            }
	            else if ($score_data->score_facs >= 57) {
	                $margin_left    = '-10px;';
	                $facs_rating    = 'CCC';
	            }
	            else if ($score_data->score_facs >= 40) {
	                $margin_left    = '0px;';
	                $facs_rating    = 'CC';
	            }
	            else if ($score_data->score_facs >= 22) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'C';
	            }
	            else if ($score_data->score_facs >= 4) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'D';
	            }
	            else if ($score_data->score_facs < 4) {
	                $margin_left    = '14px;';
	                $facs_rating    = 'E';
	            }
	        }
	        

	        /*--------------------------------------------------------------------
	        /*  Displayed Text on the middle of the Gauge Chart
	        /*------------------------------------------------------------------*/
	        $facs_score['rating_txt']   =  '<div> <span style="text-align: center; font-size: 50px;  margin-left: '.$margin_left.'; float:left; position: relative; top: -40px;"> '.$facs_rating.' </span> </div>';

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = array(
	            'chart' => array(
	                'backgroundColor'   => 'transparent',
	                'type'              => 'solidgauge',
	                'height'            => 700,
	                'width'             => 2000,
	            ),
	            'credits' => array(
	                'enabled'   => false
	            ),
	            'title' => array(
	                'text'  => STR_EMPTY
	            ),
	            'pane'  => array(
	                'size'          => '100%',
	                'startAngle'    => -90,
	                'endAngle'      => 90,
	                'background'    => array(
	                    'backgroundColor'   => '#FFFFFF',
	                    'innerRadius'       => '75%',
	                    'outerRadius'       => '100%',
	                    'shape'             => 'arc',
	                    'borderWidth'       => 2
	                )
	            ),
	            'tooltip' => array(
	                'enabled'   => false
	            ),
	            'yAxis' => array(
	                'min'       => 0,
	                'max'       => 200,
	                'stops'     => array(
	                    array(0.1, '#FCD5C3'),
	                    array(0.5, '#F1C40F'),
	                    array(0.9, '#2ECC71')
	                ),
	                'minorTickInterval' => null,
	                'tickPixelInterval' => 400,
	                'tickWidth'         => 0,
	                'gridLineWidth'     => 0,
	                'gridLineColor'     => 'transparent',
	                'lineWidth'         => 0,
	                'labels'            => array(
	                    'enabled'   => false
	                ),
	                'title' => array(
	                    'enabled'   => false
	                )
	            ),
	            'plotOptions'       => array(
	                'solidgauge'    => array(
	                    'innerRadius'   => '75%',
	                    'dataLabels'    => array(
	                        'y'             => -45,
	                        'borderWidth'   => 0,
	                        'useHTML'       => true,
	                        'fontWeight' => 'normal',
	                        'allowOverlap' => true,
	                    )
	                )
	            ),
	            'legend' => array(
	                'enabled'   => false
	            ),
	            'series'    => array(array(
	                'data' => array(
	                    (float)$score_data->score_facs
	                ),
	                'dataLabels'    => array(
	                        'format'    => $facs_score['rating_txt'],
	                        'style'     => array(
	                            'fontSize' => '50px'
	                        )
	                    )
	                )
	            )
	        );

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        $filesize = 0;
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        
	        /** 関数終了 */
	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.9: createGrgHighChart
    //  Description: Creates the Gross Revenue Growth line Chart
    //-----------------------------------------------------
    public function createGrgHighChart($entity_id, $booster)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ      = 4;
	        $grg_score      = 0;
	        $ind_grg_score  = 0;

	        /* Chart Labels */
	        $grg_name   = ' Gross Revenue<br/>Growth %';

	        /*--------------------------------------------------------------------
	        /*  Get Industry data from the database
	        /*------------------------------------------------------------------*/
	        $industry_data  = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->where('tblentity.entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data     = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('readyrationdataid', 'desc')
	            ->first();

	        if(empty($score_data)){

	            $score_data = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
	        }

	        /*-------------------------------------------------------------------
	        /*  Gross Revenue Growth Computation
	        /*------------------------------------------------------------------*/
	        if (0 != $score_data->gross_revenue_growth1) {
	            $grg_score  = (($score_data->gross_revenue_growth2 / $score_data->gross_revenue_growth1) - 1) * 100;
	        }
	        else {
	            $grg_score  = 0;
	        }

	        /*-------------------------------------------------------------------
	        /*  Sets the compared industry data
	        /*------------------------------------------------------------------*/
	        $ind_grg_score = round($booster,2);

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->createIndCmpLineChartOptions($grg_name, $ind_grg_score, $grg_score);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.10: createNigHighChart
    //  Description: Creates the Net Income Growth line Chart
    //-----------------------------------------------------
    public function createNigHighChart($entity_id, $booster)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 5;
	        $nig_score  = 0;

	        /* Chart Labels */
	        $nig_name   = ' Net Income<br> Growth %';

	        /*--------------------------------------------------------------------
	        /*  Get Industry data from the database
	        /*------------------------------------------------------------------*/
	        $industry_data  = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->where('tblentity.entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data     = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('readyrationdataid', 'desc')
	            ->first();

	        if(empty($score_data)){

	            $score_data = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
	        }

	        /*-------------------------------------------------------------------
	        /*  Net Income Growth Computation
	        /*------------------------------------------------------------------*/
	        if (0 != $score_data->net_income_growth1) {
	            $nig_score  = (($score_data->net_income_growth2 / $score_data->net_income_growth1) - 1) * 100;
	        }
	        else {
	            $nig_score  = $score_data->net_income_growth2;
	        }

	        /*-------------------------------------------------------------------
	        /*  Sets the compared industry data
	        /*------------------------------------------------------------------*/
	        $ind_nig_score = round($booster, 2);

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->createIndCmpLineChartOptions($nig_name, $ind_nig_score, $nig_score);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.11: createGpmHighChart
    //  Description: Creates the Gross Profit Margin line Chart
    //-----------------------------------------------------
    public function createGpmHighChart($entity_id, $booster)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 6;

	        /* Chart Labels */
	        $gpm_name   = ' Gross Profit<br> Margin %';

	        /*--------------------------------------------------------------------
	        /*  Get Industry data from the database
	        /*------------------------------------------------------------------*/
	        $industry_data  = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->where('tblentity.entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data     = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('readyrationdataid', 'desc')
	            ->first();

	        if(empty($score_data)){

	            $score_data = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
	        }

	        /*-------------------------------------------------------------------
	        /*  Gross Profit Margin Computation
	        /*------------------------------------------------------------------*/
	        $gpm_score  = (float)$score_data->gross_profit_margin2 * 100;

	        /*-------------------------------------------------------------------
	        /*  Sets the compared industry data
	        /*------------------------------------------------------------------*/
	        $ind_gpm_score = round($booster, 2);

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->createIndCmpLineChartOptions($gpm_name, $ind_gpm_score, $gpm_score);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        return $filename;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.12: createCrHighChart
    //  Description: Creates the Current Ratio line Chart
    //-----------------------------------------------------
    public function createCrHighChart($entity_id, $booster)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 7;

	        /* Chart Labels */
	        $cr_name    = ' Current Ratio';

	        /*--------------------------------------------------------------------
	        /*  Get Industry data from the database
	        /*------------------------------------------------------------------*/
	        $industry_data  = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->where('tblentity.entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data     = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('readyrationdataid', 'desc')
	            ->first();

	        if(empty($score_data)){

	            $score_data = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
	        }

	        /*-------------------------------------------------------------------
	        /*  Current Ratio Computation
	        /*------------------------------------------------------------------*/
	        $cr_score   = $score_data->current_ratio2;

	        /*-------------------------------------------------------------------
	        /*  Sets the compared industry data
	        /*------------------------------------------------------------------*/
	        $ind_cr_score = round($booster, 2);

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->createIndCmpLineChartOptions($cr_name, $ind_cr_score, $cr_score);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return $filename;

        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.13: createDerHighChart
    //  Description: Creates the Debt to Equity line Chart
    //-----------------------------------------------------
    public function createDerHighChart($entity_id, $booster)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 9;

	        /* Chart Labels */
	        $der_name   = ' Debt-to-Equity<br />Ratio';

	        /*--------------------------------------------------------------------
	        /*  Get Industry data from the database
	        /*------------------------------------------------------------------*/
	        $industry_data  = DB::table('tblentity')
	            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
	            ->where('tblentity.entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Get SME rating data from the database
	        /*------------------------------------------------------------------*/
	        $score_data     = DB::table('tblreadyrationdata')
	            ->where('entityid', '=', $entity_id)
	            ->orderBy('readyrationdataid', 'desc')
	            ->first();

	        if(empty($score_data)){

	            $score_data = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
	        }
	        
	        /*-------------------------------------------------------------------
	        /*  Debt to Equity Computation
	        /*------------------------------------------------------------------*/
	        $der_score = $score_data->debt_equity_ratio2;

	        /*-------------------------------------------------------------------
	        /*  Sets the compared industry data
	        /*------------------------------------------------------------------*/
	        $ind_der_score = round($booster, 2);

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->createIndCmpLineChartOptions($der_name, $ind_der_score, $der_score);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
        	return $filename;
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.14: createIndCmpLineChartOptions
    //  Description: Sets the Industry Comparison Line Chart Options
    //-----------------------------------------------------
    public function createIndCmpLineChartOptions($label, $ind_score, $sme_score)
    {
    	try{
	        $chart_options = array(
	            'chart' => array(
	                'type'      => 'bar',
	                'height'    => '200',
	                'width'     => '1200',
	            ),
	            'credits' => array(
	                'enabled'   => false
	            ),
	            'title' => array(
	                'text'  => STR_EMPTY
	            ),
	            'tooltip' => array(
	                'enabled'   => false
	            ),
	            'xAxis' => array(
	                'categories'    => array(
	                    $label
	                ),
	                'title'     => array(
	                    'text'  => null
	                ),
	                'labels'    => array(
	                    'useHTML'   => true,
	                    'style'     => array(
	                        'font-size' => '25px',
	                    )
	                )
	            ),
	            'yAxis' => array(
	                'tickPixelInterval' => 150,
	                'gridLineWidth'     => 2,
	                'title'     => array(
	                    'text'  => null
	                ),
	                'labels'    => array(
	                    'style'     => array(
	                        'font-size' => '25px'
	                    )
	                )
	            ),
	            'plotOptions'   => array(
	                'bar'   => array(
	                    'pointPadding'  => 0,
	                    'pointWidth'    => 20,
	                    'dataLabels'    => array(
	                        'align'     => 'center',
	                        'format'    => '{y:.2f}',
	                        'enabled'   => true,
	                        'inside'    => true,
	                        'allowOverlap' => true,
	                        'style'     => array(
	                            'fontSize' => '20px',
	                        )
	                    )
	                )
	            ),
	            'legend' => array(
	                'enabled'   => false
	            ),
	            'series'    => array(
	                array(
	                    'name'  => 'Company',
	                    'color' => '#BDCF8E',
	                    'data'  => array(
	                        (float)$sme_score
	                    ),
	                    'connectNulls' => true
	                ),
	                array(
	                    'name'  => 'Industry',
	                    'color' => '#F9967C',
	                    'data'  => array(
	                        (float)$ind_score
	                    )
	                ),
	                'connectNulls' => true
	            )
	        );

	        return $chart_options;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.15: generateChart
    //  Description: Generates the Chart using Highcharts Export Server
    //-----------------------------------------------------
    public function generateChart($chartOptions, $chartType, $entityId)
    {
    	try{
	        $chart = new HighCharts();
	        return $chart->generateChart($entityId, $chartType, $chartOptions);
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        // $url                = 'http://export.highcharts.com/';                   // highcharts url
	        // $destination_path   = public_path().'/images/rating-report-graphs/'; // directory location
	        // $img_url            = STR_EMPTY;                                     // container
	        // $extension          = '.png';                                            // file extension name
	        // $filename           = $chart_typ.'-'.$entity_id.'-'.time().$extension;   // generate filename

	        // $chart_object = array(
	        //     'options'   => json_encode($chart_options),
	        //     'type'      =>'image/png',
	        //     'async'     => true
	        // );

	        /*--------------------------------------------------------------------
	        /*  Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        // $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        // curl_setopt($curl,CURLOPT_URL, $url);
	        // curl_setopt($curl,CURLOPT_POST, sizeof($chart_object));
	        // curl_setopt($curl,CURLOPT_POSTFIELDS, http_build_query($chart_object));
	        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
	        // $result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        // curl_close($curl);

	        /*--------------------------------------------------------------------
	        /*  Saves the generated Chart to the Server
	        /*------------------------------------------------------------------*/
	        // $img_url    = $url.$result;
	        // file_put_contents($destination_path.$filename, file_get_contents($img_url));

	        // return $filename;
    }
    
    //-----------------------------------------------------
    //  Function 1.16: createSMEReport
    //  Description: Generates the CreditBPO PDF Report
    //-----------------------------------------------------
    public function createSMEReport($entity_id)
    {
    	return;
    	
        $forecast_data  = NULL;

        /*--------------------------------------------------------------------
        /*  Removes 30 secs Maximum Execution time limit
        /*------------------------------------------------------------------*/
        set_time_limit(0);

        /*--------------------------------------------------------------------
        /*  Gets the Entity Data from the Database
        /*------------------------------------------------------------------*/
        $entity = DB::table('tblentity')
            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
            ->where('tblentity.entityid', '=', $entity_id)
            ->first();
        $entity->province = $entity->province;

        switch($entity->employee_size) {
            case 0:
                $employeeSize = "Micro 1-9";
                break;
            case 1:
                $employeeSize = "Small 10-99";
                break;
            case 2:
                $employeeSize = "Medium 100-199";
                break;
            case 3:
                $employeeSize = "Large 200+";
                break;
                default:
                    $employeeSize = "";
                    break;

        }

        /*--------------------------------------------------------------------
        /*  Classify asset size
        /*------------------------------------------------------------------*/
        if ($entity->total_assets < 3000000) {
            $assetClassification = "Micro";
        } elseif ($entity->total_assets >= 3000000 && $entity->total_assets <= 15000000){
            $assetClassification = "Small";
        } elseif ($entity->total_assets > 15000000 && $entity->total_assets <= 100000000) {
            $assetClassification = "Medium";
        } else {
            $assetClassification = "Large";
        }

        /*--------------------------------------------------------------------
        /*  Gets the financial reports data from the database
        /*------------------------------------------------------------------*/
        $financialReport = DB::table('financial_reports')
                        ->where('entity_id', $entity_id)
                        ->where('is_deleted', 0)
                        ->first();
                        
        $startingYear = $financialReport->year;
        $endingYear =  $startingYear + 3;

        
        
        /*--------------------------------------------------------------------
        /*  Gets the Related Companies Data from the Database
        /*------------------------------------------------------------------*/

        $relatedCompanies = Relatedcompanies::where('is_deleted',0)
                            ->where('entity_id', $entity_id)
                            ->get();

         /*--------------------------------------------------------------------
        /*  Gets the board of directors Data from the Database
        /*------------------------------------------------------------------*/
        $boardOfDirectors = Director::where('is_deleted',0)
                            ->where('entity_id', $entity_id)
                            ->get();
        foreach($boardOfDirectors as $boardOfDirector) {
            $middleName = $boardOfDirector->middlename ? ' ' . $boardOfDirector->middlename : '';
            $boardOfDirector->name = $boardOfDirector->firstname . $middleName . ' ' . $boardOfDirector->lastname;
        }

        // /*--------------------------------------------------------------------
        // /*  Gets the Key Managers Data from the Database
        // /*------------------------------------------------------------------*/
        $keyManagers = KeyManager::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();

        // /*--------------------------------------------------------------------
        // /*  Gets the shareholders Data from the Database
        // /*------------------------------------------------------------------*/
        $shareHolders = Shareholder::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();
        foreach($shareHolders as $shareHolder) {
            $middleName = $shareHolder->middlename ? ' ' . $shareHolder->middlename : '';
            $shareHolder->name = $shareHolder->firstname . $middleName . ' ' . $shareHolder->lastname;
        }

        /*--------------------------------------------------------------------
        /*  Gets the Major Customers Data from the Database
        /*------------------------------------------------------------------*/
        $majorCustomers = Majorcustomer::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();

        /*--------------------------------------------------------------------
        /*  Gets the Major Suppliers Data from the Database
        /*------------------------------------------------------------------*/
        $majorSuppliers = Majorsupplier::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();

        /*--------------------------------------------------------------------
        /*  Gets the capital details Data from the Database
        /*------------------------------------------------------------------*/
        $capitalDetails = Capital::where('is_deleted', 0)
                        ->where('entity_id', $entity_id)
                        ->first();

        /*--------------------------------------------------------------------
        /*  Gets the certifications Data from the Database
        /*------------------------------------------------------------------*/
        $certifications = Certification::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();
        foreach ($certifications as $certification) {
            $certification->certification_reg_date = date('m/d/Y', strtotime($certification->certification_reg_date));
        }

        /*--------------------------------------------------------------------
        /*  Gets the insurance Data from the Database
        /*------------------------------------------------------------------*/
        $insurances = Insurance::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();

        /*--------------------------------------------------------------------
        /*  Gets the customer segment Data from the Database
        /*------------------------------------------------------------------*/
        $customerSegment = BusinessSegment::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();

        
        /*--------------------------------------------------------------------
        /*  Gets the Location Data from the Database
        /*------------------------------------------------------------------*/
        $location = DB::table('tbllocations')
                        ->where('entity_id', $entity_id)
                        ->where('is_deleted',0)
                        ->first();
        $location = Location::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->first();

        /*--------------------------------------------------------------------
        /*  Gets the Major Market Location Data from the Database
        /*------------------------------------------------------------------*/
        $majorLocations = Majorlocation::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->where('is_deleted',0)
                        ->leftJoin('refcitymun', 'tblmajorlocation.marketlocation', 'refcitymun.id')
                        ->get();

         /*--------------------------------------------------------------------
        /*  Gets the Competitors Data from the Database
        /*------------------------------------------------------------------*/
        $competitors = Competitor::where('is_deleted',0)
                        ->where('entity_id', $entity_id)
                        ->get();
                        
        /*--------------------------------------------------------------------
        /*  Gets the Entity DSCR Details from the Database
        /*------------------------------------------------------------------*/
        $dscrDetails = DB::table('debt_service_capacity_calculator')
                        ->where('entity_id', '=', $entity_id)
                        ->first();

        /*--------------------------------------------------------------------
        /*  Gets the Industry Data from the Database
        /*------------------------------------------------------------------*/
        $ent = DB::table('tblentity')->where('entityid', $entity_id)->first(); /** Get information of the report */
        $year = DB::table('financial_reports')->where('entity_id', $entity_id)->pluck('year')->first();
        $industry = null;
        $tier = ['class', 'sub', 'main'];

        foreach($tier as $t){

            /** Check industry class */
            if($t == "class"){
                $ind_id = $ent->industry_row_id;
                // Check if industry class code exist
                $c_class = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->get();
                if($c_class){
                    // Check year exist
                    $y_class = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $year)->first();
                    if(!$y_class){
                        $closest_year = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $industry = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $industry = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }

            /** Check industry division */
            if($t == "sub"){
                $ind_id = $ent->industry_sub_id;
                // Check if industry sub code exist
                $c_sub = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->get();
                if($c_sub){
                    // Check year exist
                    $y_sub = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $year)->first();
                    if(!$y_sub){
                        $closest_year = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $industry = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $industry = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }

            /** Check industry main */
            if($t == "main"){
                $ind_id = $ent->industry_main_id;
                // Check if industry main code exist
                $c_main = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->get();
                if($c_main){
                    // Check year exist
                    $y_main = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $year)->first();
                    if(!$y_main){
                        $closest_year = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $industry = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $industry = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }
            if($industry){
                if($industry->gross_revenue_growth == 0.0000 &&
                    $industry->net_income == 0.0000 &&
                    $industry->gross_profit_margin == 0.0000 &&
                    $industry->current_ratio == 0.0000 &&
                    $industry->debt_equity_ratio == 0.000){
                    continue;
                }else{
                    break;
                }
            }
        }

        /*--------------------------------------------------------------------
        /*  Gets the Industry Main from the Database
        /*------------------------------------------------------------------*/
        $ind_main = DB::table('tblindustry_main')
            ->where('main_code', $entity->industry_main_id)
            ->first();

        $industry->main_title = $ind_main->main_title;
        
        /*--------------------------------------------------------------------
        /*  Gets the Industry Data from the Database
        /*------------------------------------------------------------------*/
        $ind_sub = DB::table('tblindustry_sub')
            ->where('sub_code', $entity->industry_sub_id)
            ->first();

        /*--------------------------------------------------------------------
        /*  Gets the Industry Data from the Database
        /*------------------------------------------------------------------*/
        $ind_row = DB::table('tblindustry_class')
            ->where('class_code', $entity->industry_row_id)
            ->first();

        /*--------------------------------------------------------------------
        /*  Gets the SME Score Data from the Database
        /*------------------------------------------------------------------*/
        $finalscore = DB::table('tblsmescore')
            ->where('entityid', '=', $entity->entityid)
            ->orderBy('smescoreid', 'desc')
            ->first();

        /*--------------------------------------------------------------------
        /*  Gets the Industry Comparison SME Data from the Database
        /*------------------------------------------------------------------*/
        $readyratiodata = DB::table('tblreadyrationdata')
            ->where('entityid', '=', $entity->entityid)
            ->orderBy('readyrationdataid', 'desc')
            ->first();

        if(empty($readyratiodata)){

            $readyratiodata = SMEInternalHandler::getOverwriteReadyRatio($entity_id);
        }

        /*--------------------------------------------------------------------
        /*  Gets the SME's Business Driver Data from the Database
        /*------------------------------------------------------------------*/
        $businessdriver = DB::table('tblbusinessdriver')
            ->where('entity_id', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gets the SME's Revenue Potential Data from the Database
        /*------------------------------------------------------------------*/
        $revenuepotential = DB::table('tblrevenuegrowthpotential')
            ->where('entity_id', '=', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gets the SME's Business Segment Data from the Database
        /*------------------------------------------------------------------*/
        $bizsegment = DB::table('tblbusinesssegment')
            ->where('entity_id', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gets the SME's Past Project Completed Data from the Database
        /*------------------------------------------------------------------*/
        $past_projs = DB::table('tblprojectcompleted')
            ->where('entity_id', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gets the SME's Future Growth Initiative Data from the Database
        /*------------------------------------------------------------------*/
        $future_growths  = DB::table('tblfuturegrowth')
            ->where('entity_id', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gets the SME's Required Credit Line Data from the Database
        /*------------------------------------------------------------------*/
        $planfacilities = DB::table('tblplanfacilityrequested')
            ->where('entity_id', '=', $entity->entityid)
            ->get();

        /*--------------------------------------------------------------------
        /*  Gross Revenue Growth Calculation
        /*------------------------------------------------------------------*/
        if (0 != $readyratiodata->gross_revenue_growth1) {
            $ind_cmp['grg'] = (($readyratiodata->gross_revenue_growth2 / $readyratiodata->gross_revenue_growth1) - 1) * 100;
        }
        else {
            $ind_cmp['grg'] = $readyratiodata->gross_revenue_growth2;
        }

        /*--------------------------------------------------------------------
        /*  Net Income Growth Calculation
        /*------------------------------------------------------------------*/
        if (0 != $readyratiodata->net_income_growth1) {
            $ind_cmp['nig'] = (($readyratiodata->net_income_growth2 / $readyratiodata->net_income_growth1) - 1) * 100;
        }
        else {
            $ind_cmp['nig'] = $readyratiodata->net_income_growth2;
        }

        /*--------------------------------------------------------------------
        /*  Gross Profit Margin Calculation
        /*------------------------------------------------------------------*/
        $ind_cmp['gpm']     = $readyratiodata->gross_profit_margin2 * 100;

        /*--------------------------------------------------------------------
        /*  Current Ratio Calculation
        /*------------------------------------------------------------------*/
        $ind_cmp['cr_ind']  = $industry->current_ratio / 100;

        /*--------------------------------------------------------------------
        /*  Debt to Equity Calculation
        /*------------------------------------------------------------------*/
        $ind_cmp['der_ind'] = $industry->debt_equity_ratio / 100;

        /*--------------------------------------------------------------------
        /*  Financial Position Rating Calculation
        /*------------------------------------------------------------------*/

        $frHelper = new FinancialHandler();
        $finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
        $financialTotal = 0;
        $financialChecker = false;
        if($finalRatingScore){
            //Financial Position
            if($finalRatingScore[0] > 1.6) $financial['fin_position'] = "AAA";
            if($finalRatingScore[0] >= 1.2 && $finalRatingScore[0] < 1.6) $financial['fin_position'] = "AA";
            if($finalRatingScore[0] >= 0.8 && $finalRatingScore[0] < 1.2) $financial['fin_position'] = "A";
            if($finalRatingScore[0] >= 0.4 && $finalRatingScore[0] < 0.8) $financial['fin_position'] = "BBB";
            if($finalRatingScore[0] >= 0 && $finalRatingScore[0] < 0.4) $financial['fin_position'] = "BB";
            if($finalRatingScore[0] >= -0.4 && $finalRatingScore[0] < 0) $financial['fin_position'] = "B";
            if($finalRatingScore[0] >= -0.8 && $finalRatingScore[0] < -0.4) $financial['fin_position'] = "CCC";
            if($finalRatingScore[0] >= -1.2 && $finalRatingScore[0] < -0.8) $financial['fin_position'] = "CC";
            if($finalRatingScore[0] >= -1.6 && $finalRatingScore[0] < -1.2) $financial['fin_position'] = "C";
            if($finalRatingScore[0] < -1.6) $financial['fin_position'] = "D";

            if($finalRatingScore[1] >= 1.6) $financial['fin_performance'] = "AAA";
            if($finalRatingScore[1] >= 1.2 && $finalRatingScore[1] < 1.6) $financial['fin_performance'] = "AA";
            if($finalRatingScore[1] >= 0.8 && $finalRatingScore[1] < 1.2) $financial['fin_performance'] = "A";
            if($finalRatingScore[1] >= 0.4 && $finalRatingScore[1] < 0.8) $financial['fin_performance'] = "BBB";
            if($finalRatingScore[1] >= 0 && $finalRatingScore[1] < 0.4) $financial['fin_performance'] = "BB";
            if($finalRatingScore[1] >= -0.4 && $finalRatingScore[1] < 0) $financial['fin_performance'] = "B";
            if($finalRatingScore[1] >= -0.8 && $finalRatingScore[1] < -0.4) $financial['fin_performance'] = "CCC";
            if($finalRatingScore[1] >= -1.2 && $finalRatingScore[1] < -0.8) $financial['fin_performance'] = "CC";
            if($finalRatingScore[1] >= -1.6 && $finalRatingScore[1] < -1.2) $financial['fin_performance'] = "C";
            if($finalRatingScore[1] < -1.6) $financial['fin_performance'] = "D";

            //Calculate Total 
            $financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
            $financialChecker = true;
        } else {
            if ($finalscore->score_rfp >= 162) {
                $financial['fin_position'] = "AAA";
            }
            else if ($finalscore->score_rfp >= 145) {
                $financial['fin_position'] = "AA";
            }
            else if ($finalscore->score_rfp >= 127) {
                $financial['fin_position'] = "A";
            }
            else if ($finalscore->score_rfp >= 110) {
                $financial['fin_position'] = "BBB";
            }
            else if ($finalscore->score_rfp >= 92) {
                $financial['fin_position'] = "BB";
            }
            else if ($finalscore->score_rfp >= 75) {
                $financial['fin_position'] = "B";
            }
            else if ($finalscore->score_rfp >= 57) {
                $financial['fin_position'] = "CCC";
            }
            else if ($finalscore->score_rfp >= 40) {
                $financial['fin_position'] = "CC";
            }
            else if ($finalscore->score_rfp >= 22) {
                $financial['fin_position'] = "C";
            }
            else if ($finalscore->score_rfp >= 4) {
                $financial['fin_position'] = "D";
            }
            else if ($finalscore->score_rfp < 4) {
                $financial['fin_position'] = "E";
            }
    
            /*--------------------------------------------------------------------
            /*  Financial Performance Rating Calculation
            /*------------------------------------------------------------------*/
            if ($finalscore->score_rfpm >= 162) {
                $financial['fin_performance'] = "AAA";
            }
            else if ($finalscore->score_rfpm >= 145) {
                $financial['fin_performance'] = "AA";
            }
            else if ($finalscore->score_rfpm >= 127) {
                $financial['fin_performance'] = "A";
            }
            else if ($finalscore->score_rfpm >= 110) {
                $financial['fin_performance'] = "BBB";
            }
            else if ($finalscore->score_rfpm >= 92) {
                $financial['fin_performance'] = "BB";
            }
            else if ($finalscore->score_rfpm >= 75) {
                $financial['fin_performance'] = "B";
            }
            else if ($finalscore->score_rfpm >= 57) {
                $financial['fin_performance'] = "CCC";
            }
            else if ($finalscore->score_rfpm >= 40) {
                $financial['fin_performance'] = "CC";
            }
            else if ($finalscore->score_rfpm >= 22) {
                $financial['fin_performance'] = "C";
            }
            else if ($finalscore->score_rfpm >= 4) {
                $financial['fin_performance'] = "D";
            }
            else if ($finalscore->score_rfpm < 4) {
                $financial['fin_performance'] = "E";
            }
    
        }
        
        /*--------------------------------------------------------------------
        /*  CreditBPO Rating Calculation
        /*------------------------------------------------------------------*/
        $credit_exp = explode('-', $finalscore->score_sf);
        $gscore     = $credit_exp[0];
        $gtxt       = $credit_exp[1];
        if($financialChecker){
            if($financialTotal >= 1.6) {
                $cbpo_rate['rating_txt'] = "AAA";
                $cbpo_rate['rating_class']  = 'gcircle-container';
                $cbpo_rate['rating_color']  = 'green';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
                $cbpo_rate['rating_label'] = "Excellent";
            } elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
                $cbpo_rate['rating_txt'] = "AA";
                $cbpo_rate['rating_class']  = 'gcircle-container';
                $cbpo_rate['rating_color']  = 'green';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
                $cbpo_rate['rating_label'] = "Very good";
            } elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
                $cbpo_rate['rating_txt'] = "A";
                $cbpo_rate['rating_class']  = 'gcircle-container';
                $cbpo_rate['rating_color']  = 'green';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
                $cbpo_rate['rating_label'] = "Good";
            } elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
                 $cbpo_rate['rating_txt'] = "BBB";
                 $cbpo_rate['rating_class']  = 'ocircle-container';
                $cbpo_rate['rating_color']  = 'orange';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
                $cbpo_rate['rating_label'] = "Positive";
            } elseif ($financialTotal >= 0 && $financialTotal< 0.4){
                $cbpo_rate['rating_txt'] = "BB";
                $cbpo_rate['rating_class']  = 'ocircle-container';
                $cbpo_rate['rating_color']  = 'orange';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
                $cbpo_rate['rating_label'] = "Normal";
            } elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
                $cbpo_rate['rating_txt'] = "B";
                $cbpo_rate['rating_class']  = 'ocircle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
                $cbpo_rate['rating_label'] = "Satisfactory";
            } elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
                $cbpo_rate['rating_txt'] = "CCC";
                $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
                $cbpo_rate['rating_label'] = "Unsatisfactory";
            } elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
                 $cbpo_rate['rating_txt'] = "CC";
                 $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
                $cbpo_rate['rating_label'] = "Adverse";
            } elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
                $cbpo_rate['rating_txt'] = "C";
                $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
                $cbpo_rate['rating_label'] = "Bad";
            } elseif ($financialTotal< -1.6) {
                $cbpo_rate['rating_txt'] = "D";
                $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
                $cbpo_rate['rating_label'] = "Critical";
            }

        } else {
            if($gscore >= 177) {
                $cbpo_rate['rating_txt']    = "AA";
                $cbpo_rate['rating_class']  = 'gcircle-container';
                $cbpo_rate['rating_color']  = 'green';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
            }
            else if ($gscore >= 150 && $gscore <= 176) {
                $cbpo_rate['rating_txt']    = "A";
                $cbpo_rate['rating_class']  = 'gcircle-container';
                $cbpo_rate['rating_color']  = 'green';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_high');
            }
            else if ($gscore >= 123 && $gscore <= 149) {
                $cbpo_rate['rating_txt']    = "BBB";
                $cbpo_rate['rating_class']  = 'ocircle-container';
                $cbpo_rate['rating_color']  = 'orange';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
            }
            else if ($gscore >= 96 && $gscore <= 122) {
                $cbpo_rate['rating_txt']    = "BB";
                $cbpo_rate['rating_class']  = 'ocircle-container';
                $cbpo_rate['rating_color']  = 'orange';
                $cbpo_rate['rating_desc']   = trans('risk_rating.cbr_moderate');
            }
            else if ($gscore >= 69 && $gscore <= 95) {
                $cbpo_rate['rating_txt']    = "B";
                $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
            }
            else if($gscore < 69){
                $cbpo_rate['rating_txt']    = "CCC";
                $cbpo_rate['rating_class']  = 'circle-container';
                $cbpo_rate['rating_color']  = 'red';
                $cbpo_rate['rating_desc']   = trans('risk_rating.crb_low');
            }    
            $cbpo_rate['rating_label'] = $credit_exp[1];

        }

        
        $cbpo_rate['rating_score'] = $credit_exp[0];

        /*--------------------------------------------------------------------
        /*  BCC, MQ and FC Chart Generation
        /*------------------------------------------------------------------*/
        $graphs['bcc_graph']    = $this->createBccHighChart($entity_id);
        $graphs['mq_graph']     = $this->createMqHighChart($entity_id);

        if($financialChecker) {
            $graphs['fpos_graph']   = $this->createFcHighChart($entity_id, $financialTotal);
        } else {
            $graphs['fpos_graph']   = $this->createFcHighChart($entity_id, null);

        }

        /*--------------------------------------------------------------------
        /*  Generates Industry Comparison according to latest year
        /*------------------------------------------------------------------*/
        $year = GRDP::getLatestYear();

        //get city data to get regCode
        $city_data = Entity::where('entityid', $entity_id)
            ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->first();

        //get region to based from reCode on city data
        $region_data = Region::where('regCode', $city_data->regDesc)->first();

        $entity_data = Entity::where('entityid', $entity_id)
            ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->first();

        $region         = $region_data->regDesc;
        $regionCode     = $region_data->regCode;
        $grdp_data      = GRDP::getBoost($year, $regionCode);
        $reverse_boost  = 1 + (1 - $grdp_data);
        $zipcode        = $entity_data->zipcode;
        $city           = $region_data->city;
        $entityCity     = $entity_data->citymunDesc;

        /*--------------------------------------------------------------------
        /*  Sets the Boost percentage label
        /*------------------------------------------------------------------*/
        switch ($grdp_data) {
            case 0.8:
                $boost_percent = '-20%';
                break;

            case 0.9:
                $boost_percent = '-10%';
                break;

            case 1.1:
                $boost_percent = '+10%';
                break;

            case 1.2:
                $boost_percent = '+20%';
                break;
        }

        /*--------------------------------------------------------------------
        /*  Computes the Industry comparison variables
        /*------------------------------------------------------------------*/
        $boost['grg']   = ($industry) ? ($industry->gross_revenue_growth * 100) : 0;
        $boost['nig']   = ($industry) ? ($industry->net_income * 100) : 0;
        $boost['gpm']   = ($industry) ? ($industry->gross_profit_margin * 100) : 0;
        $boost['cr']    = ($industry) ? ($industry->current_ratio) : 0;
        $boost['der']   = ($industry) ? ($industry->debt_equity_ratio) : 0;

        /*--------------------------------------------------------------------
        /*  Generates Industry Comparison Graphs
        /*------------------------------------------------------------------*/
        $graphs['reg1_graph']   = $this->createGrgHighChart($entity_id, $boost['grg']);
        $graphs['reg2_graph']   = $this->createNigHighChart($entity_id, $boost['nig']);
        $graphs['reg3_graph']   = $this->createGpmHighChart($entity_id, $boost['gpm']);
        $graphs['reg4_graph']   = $this->createCrHighChart($entity_id, $boost['cr']);
        $graphs['reg5_graph']   = $this->createDerHighChart($entity_id, $boost['der']);

        if ($entity->current_bank != 0) {
            $bank_standalone = Bank::find($entity->current_bank)->is_standalone;
        }
        else {
            $bank_standalone = 0;
        }

        $fa_report      = FinancialReport::where(['entity_id' => $entity->entityid, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        $main_id = DB::table('tblentity')
                    ->join('tblindustry_main', 'tblindustry_main.main_code', '=', 'tblentity.industry_main_id')
                    ->where('entityid', $entity->entityid)
                    ->pluck('tblindustry_main.industry_main_id')->first();

        /*  Forecast Slope Calculation  */
        if (NULL != $fa_report) {
            $gf_lib                 = new GrowthForecastLib($entity->entityid, $main_id, $bank_standalone);
            $forecast_data          = $gf_lib->getGrowthForecastData();

            $forecast               = $this->createForecastHighChart($entity->entityid, $forecast_data);
           
            $graphs['forecast']     = $forecast['file'];
        }
        $procurementTypes = [
            AdminController::BANK_TYPE_CORPORATE,
            AdminController::BANK_TYPE_GOVERNMENT,
        ];
        $isFinancing = !in_array($entity->bank_type, $procurementTypes);

        /*--------------------------------------------------------------------
        /*  Generates CreditBPO PDF Report
        /*------------------------------------------------------------------*/
        $date_created = new DateTime();
        // $date_created->modify('+8 hours');
        
        $ff_filename    = 'CreditBPO Rating Report - ('.$entity_id.') '.$entity->companyname.' - '.$date_created->format('Y-m-d').'.pdf';

        $fname = $ff_filename;
        if(strpos($fname,"&") !== false){
            $fname  = preg_replace("/[&]/", "And", $fname);
        }
            
        $file_to_save   = public_path().'/documents/'.$fname;

        $sec_reg_date = date('m/d/Y', strtotime($entity->sec_reg_date));

        $returnValue = array(
            'date_created'      => $date_created->format('m/d/Y g:i A'),
            'entity'            => $entity,
            'industry'          => $industry,
            'ind_sub'           => $ind_sub,
            'ind_row'           => $ind_row,
            'finalscore'        => $finalscore,
            'readyratiodata'    => $readyratiodata,
            'relatedCompanies'  => $relatedCompanies,
            'boardOfDirectors'  => $boardOfDirectors,
            'keyManagers'       => $keyManagers,
            'shareHolders'      => $shareHolders,
            'majorCustomers'    => $majorCustomers,
            'majorSuppliers'    => $majorSuppliers,
            'location'          => $location,
            'assetClassification' => $assetClassification,
            'startingYear'      => $startingYear,
            'endingYear'        => $endingYear,
            'sec_reg_date'      => $sec_reg_date,
            'city'              => $city,
            'zipcode'           => $zipcode,
            'employeeSize'      => $employeeSize,
            'capitalDetails'    => $capitalDetails,
            'certifications'    => $certifications,
            'insurances'        => $insurances,
            'majorLocations'     => $majorLocations,
            'competitors'   	=> $competitors,
            'customerSegment'   => $customerSegment,
            'entityCity'        => $entityCity,

            'businessdriver'    => $businessdriver,
            'revenuepotential'  => $revenuepotential,
            'bizsegment'        => $bizsegment,
            'past_projs'        => $past_projs,
            'future_growths'    => $future_growths,
            'planfacilities'    => $planfacilities,

            'ind_cmp'           => $ind_cmp,
            'financial'         => $financial,
            'cbpo_rate'         => $cbpo_rate,
            'graphs'            => $graphs,
            'region'            => $region,
            'boost'             => $boost,
            'boost_percent'     => $boost_percent,
            'bank_standalone'   => $bank_standalone,
            'growth_desc'       => $forecast_data['description'],
            'dscr_details'      => $dscrDetails,
            'is_financing'      => $isFinancing
        );

		/* Disable CMAP Function for Simplified and Standalone Report (CDP-1604) */

		if($entity->is_premium == PREMIUM_REPORT){
			$cmapSearch = AutomateStandaloneRatingHandler::findInDatabase($entity->companyname);
        
			if ($entity->pse_company_id !== null) {
				$officers = PseCompanyOfficer::where('company_id', $entity->pse_company_id)->groupBy('name')->get(); //limit for testing only

				foreach ($officers as $officer) {
					$data = AutomateStandaloneRatingHandler::findInDatabaseOfficer($officer->name);

					foreach ($data as $datum) {
						$sameTableType = STS_NG;
						if ($cmapSearch !== null) {
							foreach ($cmapSearch as $tbl_data_key => $tbl_data) {
								if ($tbl_data[0] == $datum['tableType']) {
									array_push($tbl_data[2], $datum['companyRecords']);
									$cmapSearch[$tbl_data_key] = $tbl_data;
									$sameTableType = STS_OK;
								}
							}
						}

						// new table type push to table data array
						if ($sameTableType == STS_NG) {
							$cmapSearch[] = [$datum['tableType'], $datum['cmapTableHeader'], [$datum['companyRecords']]];
						}
					}
				}
			}
			$returnValue['cmapSearch'] = $cmapSearch;
		}

        $current_bank = Entity::where('entityid', $entity_id)->first();

        $organization = Bank::where('id', $current_bank->current_bank)->first();
        
        $deliverables = array(
            'financial_rating'          => 0,
            'growth_forecast'           => 0,
            'industry_forecasting'      => 0
        );
        $is_deliverables = json_encode($deliverables);

        if(!empty($organization->deliverables)){
            $returnValue['deliverables']    = json_decode($organization->deliverables);
            $returnValue['is_deliverables'] = 'false';
        }else{
            $returnValue['deliverables']    = json_decode($is_deliverables);
            $returnValue['is_deliverables'] = 'true';
        }
        
        $pdf = PDF::loadView('summary.print-rating-summary', $returnValue)->save($file_to_save);

        /*--------------------------------------------------------------------
        /*  Deletes generated graphs
        /*------------------------------------------------------------------*/
        File::delete(
            array(
                public_path().'/images/rating-report-graphs/'.$graphs['bcc_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['mq_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['fpos_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['reg1_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['reg2_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['reg3_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['reg4_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['reg5_graph'],
                public_path().'/images/rating-report-graphs/'.$graphs['forecast'],
            )
        );

        //merge dashboard report and financial analysis report

        if($entity->is_premium != SIMPLIFIED_REPORT){
            // $where = array( 
            //     "entity_id" => $entity_id,
            //     "document_type" => 9
            // );
            // $documentFS = DB::table('tbldocument')
            // ->where($where)
            // ->orderBy("documentid", "DESC")
            // ->first();

            
            $documentFS = DB::table('tblentity')
            ->where('entityid',$entity_id)
            ->first();
    
            $pdfMerge = new PDFMerger();
    
            if(!empty($documentFS->cbpo_pdf)){

            	if($documentFS->financial_analysis_pdf){

	                $fname = $documentFS->financial_analysis_pdf;
	                if(strpos($fname,"&") !== false){
	                    $fname  = preg_replace("/[&]/", "And", $fname);
	                }
	                $finAnFile = public_path().'/financial_analysis/'.$fname;
	        
	                $pdfMerge->addPDF($file_to_save, 'all');
	                $pdfMerge->addPDF($finAnFile);
	        
	                $pdfMerge->merge('file', $file_to_save);
            	}else{
            		$financialAnalysis = new FinancialAnalysisInternalHandler();
            		$financial_report_id = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])->pluck('id')[0];
			        $fr = FinancialReport::find($financial_report_id);
			        $file = $financialAnalysis->createFinancialAnalysisInternalReport($financial_report_id,'en');
			        $fname = basename($file);

			        $finAnFile = public_path().'/financial_analysis/'.$fname;
	        
	                $pdfMerge->addPDF($file_to_save, 'all');
	                $pdfMerge->addPDF($finAnFile);
	        
	                $pdfMerge->merge('file', $file_to_save);
            	}
            }
        }

        /*--------------------------------------------------------------------
        /*  Saves the path of the Generated report to the Database
        /*------------------------------------------------------------------*/
        DB::table('tblentity')
            ->where('entityid', $entity_id)
            ->update(array(
                'cbpo_pdf'  => $ff_filename,
                'pdf_grdp'  => $year,
            )
        );
        
        $error = 0;
        if (!$finalscore->positive_points && !$finalscore->negative_points) {
            if(env("APP_ENV") == "Production") {
                $where = array("is_deleted"=>0, "name" => "Good and Bad Report Rating");
                $to = "joel.espadilla@creditbpo.com";
                $info = CustomNotification::where($where)->first();
                $emails = NotificationEmails::where("is_deleted", 0)->get();
                $cc = array();
                foreach ($emails as $email) {
                    $cc[] = $email->email;
                }
                if($info && $info->count() > 0) {
                    $to = $info->is_running == 1 ? $info->receiver : $to;
                    if($info->is_running == 1) {

                        try{
                            Mail::send('emails.report_generation_error', array(
                                'company_name' => $entity->companyname,
                                'company_email' => $entity->email,
                                'phone' => $entity->phone,
                                'address1' => $entity->address1,
                                'address2' => $entity->address2,
                                'entity_id' => $entity->entityid
                            ), function($message){
                                if($emails->count() > 0) {
                                    $message->to(to)->cc($cc)->subject('Generating Report Error!');
                                } else {
                                    $message->to(to)->subject('Generating Report Error!');
                                }
                            });
                        }catch(Exception $e){
                        //
                        }
                    }
                }
            }
            $error = 1;
        }

        DB::table("generate_report_pdf_logs")->where("entity_id",$entity->entityid)->update( array("is_deleted" => 1));
        $insert = array(
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "is_deleted" => 0,
            "entity_id" => $entity->entityid,
            "status" => ($error > 0) ? 0 : 1,
            "error" => ($error > 0) ? "Missing Good and Bad": null
        );

        DB::table("generate_report_pdf_logs")->insert($insert);

    }

    //-----------------------------------------------------
    //  Function 1.17: checkOutdatedCbpoReport
    //  Description: Checks for outdated the CreditBPO report
    //-----------------------------------------------------
    public function checkOutdatedCbpoReport($entity_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $sts    = STS_NG;

	        /*--------------------------------------------------------------------
	        /*  Gets the Entity Data from the Database
	        /*------------------------------------------------------------------*/
	        $entity = DB::table('tblentity')
	            ->where('entityid', '=', $entity_id)
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Gets the latest Regional Modifier year
	        /*------------------------------------------------------------------*/
	        $year = GRDP::getLatestYear();

	        /*--------------------------------------------------------------------
	        /*  No PDF or PDF is outdated
	        /*------------------------------------------------------------------*/
	        if ((STR_EMPTY == $entity->cbpo_pdf)
	        || (NULL == $entity->cbpo_pdf)
	        || ($year > $entity->pdf_grdp)
	        || (STR_EMPTY == $entity->pdf_grdp)
	        || (NULL == $entity->pdf_grdp)) {
	            /* Create a PDF Report  */
	            $this->createSMEReport($entity_id);
	            $sts    = STS_OK;
	        }
	        else if(strtotime($entity->completed_date) < strtotime("06/25/2019")){
	            $this->createSMEReport($entity_id);
	            $sts    = STS_OK;
	        }
	        /*--------------------------------------------------------------------
	        /*  PDF exist and contains the latest GRDP Modifier
	        /*------------------------------------------------------------------*/
	        else {
	            $sts    = STS_OK;
	        }

	        return $sts;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.18: createForecastHighChart
    //  Description: Function to Compute for the Forecast Graph
    //-----------------------------------------------------
    public function createForecastHighChart($entity_id, $forecast_data)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*  Variable Declaration
	        /*------------------------------------------------------------------*/
	        $chart_typ  = 8;

	        /*--------------------------------------------------------------------
	        /*  Gets the Entity Data from the Database
	        /*------------------------------------------------------------------*/
	        $freport = DB::table('financial_reports')
	            ->where('entity_id', '=', $entity_id)
	            ->orderBy('id', 'desc')
	            ->first();

	        /*--------------------------------------------------------------------
	        /*  Convert from Percent to actual values
	        /*------------------------------------------------------------------*/
	        // if (isset($forecast_data['industry'])) {
	        //     if(count($forecast_data['industry']) > 4) {
	        //         $forecast_data['industry'][0]   = $forecast_data['industry'][0];
	        //         $forecast_data['industry'][1]   = $forecast_data['industry'][1];
	        //         $forecast_data['industry'][2]   = $forecast_data['industry'][2];
	        //         $forecast_data['industry'][3]   = $forecast_data['industry'][3];
	        //         $forecast_data['industry'][4]   = $forecast_data['industry'][4];
	        //         $forecast_data['industry'][5]   = $forecast_data['industry'][5];
	        //         $forecast_data['industry'][6]   = $forecast_data['industry'][6];
	        //         $forecast_data['industry'][7]   = $forecast_data['industry'][7];
	        //         $forecast_data['industry'][8]   = $forecast_data['industry'][8];
	        //         $forecast_data['industry'][9]   = $forecast_data['industry'][9];
	        //         $forecast_data['industry'][10]   = $forecast_data['industry'][10];
	        //         $forecast_data['industry'][11]   = $forecast_data['industry'][11];
	        //         $forecast_data['industry'][12]   = $forecast_data['industry'][12];

	        //     }else {
	        //         $forecast_data['industry'][0]   = $forecast_data['industry'][0];
	        //         $forecast_data['industry'][1]   = $forecast_data['industry'][1];
	        //         $forecast_data['industry'][2]   = $forecast_data['industry'][2];
	        //         $forecast_data['industry'][3]   = $forecast_data['industry'][3];
	        //     }
	            
	        // }

	        if (isset($forecast_data['company'])) {
	            if(count($forecast_data['industry']) >4) {
	                $forecast_data['company'][0][1]   = $forecast_data['company'][0];
	                $forecast_data['company'][1][1]   = NULL;
	                $forecast_data['company'][2][1]   = NULL;
	                $forecast_data['company'][3][1]   = NULL;
	                $forecast_data['company'][4][1]   = $forecast_data['company'][1];
	                $forecast_data['company'][5][1]   = NULL;
	                $forecast_data['company'][6][1]   = NULL;
	                $forecast_data['company'][7][1]   = NULL;
	                $forecast_data['company'][8][1]   = $forecast_data['company'][2];
	                $forecast_data['company'][9][1]   = NULL;
	                $forecast_data['company'][10][1]   = NULL;
	                $forecast_data['company'][11][1]   = NULL;
	                $forecast_data['company'][12][1]   = $forecast_data['company'][3];
	            } else {
	                $forecast_data['company'][0]   = $forecast_data['company'][0];
	                $forecast_data['company'][1]   = NULL;
	                $forecast_data['company'][2]   = NULL;
	                $forecast_data['company'][3]   = NULL;
	                $forecast_data['company'][4]   = $forecast_data['company'][1];
	                $forecast_data['company'][5]   = NULL;
	                $forecast_data['company'][6]   = NULL;
	                $forecast_data['company'][7]   = NULL;
	                $forecast_data['company'][8]   = $forecast_data['company'][2];
	                $forecast_data['company'][9]   = NULL;
	                $forecast_data['company'][10]   = NULL;
	                $forecast_data['company'][11]   = NULL;
	                $forecast_data['company'][12]   = $forecast_data['company'][3];
	            }
	            
	        }


	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $series_data = array(
	            array(
	                'name' => 'Industry',
	                'color' => "#aaaaaa",
	                'data' => isset($forecast_data['industry']) ? $forecast_data['industry'] : array()
	            ),
	            array(
	                'name' => 'Company',
	                'color' => "#B1D46B",
	                'data' => isset($forecast_data['company']) ? $forecast_data['company'] : array(),
	                'connectNulls'=>true
	            )
	        );

	        /*--------------------------------------------------------------------
	        /*  Sets the Chart Options to be passed to Highcharts
	        /*------------------------------------------------------------------*/
	        $chart_options = $this->setForecastLineOptions($series_data, $freport->year);

	        /*--------------------------------------------------------------------
	        /*  Generate the Chart image
	        /*------------------------------------------------------------------*/
	        // do {
	        //     $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);
	        //     $filesize = filesize(public_path("images/rating-report-graphs/" . $filename));
	        // } while($filesize < 3000 && $filename);

	        $filename = $this->generateChart($chart_options, $chart_typ, $entity_id);

	        /** 関数終了 */
	        return array(
	            'file'          => $filename,
	            'forecast_data' => $forecast_data
	        );
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.19: setForecastLineOptions
    //  Description: Generate Line Graph for Forecast Data
    //-----------------------------------------------------
    public function setForecastLineOptions($series_data, $base_year)
    {
    	try{
	        $chart_options = array(
	            'chart' => array(
	                'type'      => 'line',
	                'height'    => '250'
	            ),
	            'credits' => array(
	                'enabled'   => false
	            ),
	            'title' => array(
	                'text'  => STR_EMPTY
	            ),
	            'tooltip' => array(
	                'enabled'   => false
	            ),
	            'xAxis' => array(
	                'type' => 'category',                
	                'categories'    => ["Q1 " . $base_year, "Q2 " . $base_year, "Q3 " . $base_year, "Q4 " . $base_year,
	                                    "Q1 " . ($base_year+1), "Q2 " .($base_year+1), "Q3 " . ($base_year + 1), "Q4 " . ($base_year +1),
	                                    "Q1 " . ($base_year+2), "Q2 " .($base_year+2), "Q3 " . ($base_year + 2), "Q4 " . ($base_year +2),
	                                    "Q1 " . ($base_year+3)],
	                'title'     => array(
	                    'text'  => null
	                ),
	            ),
	            'yAxis' => array(
	                'gridLineWidth'     => 2,
	                'title'     => array(
	                    'text'  => null
	                ),
	            ),
	            'legend' => array(
	                'layout' => 'vertical',
	                'align' => 'right',
	                'verticalAlign' => 'top',
	                'x' => -420,
	                'y' => 30,
	                'floating' => true,
	                'borderWidth' => 1,
	            ),
	            'plotOptions'   => array(
	                'line'   => array(
	                    'dataLabels'    => array(
	                        'align'     => 'center',
	                        'format'    => '{y:.2f}%',
	                        'enabled'   => true,
	                        'color'     => 'black',
	                        'allowOverlap' => true,
	                        'style'     => array(
	                            'fontSize' => '10px',
	                            'fontWeight' => 'normal',
	                        )
	                    ),
	              		'connectNulls' => true
	                ),
	                'series' => [
		            	'connectNulls' => true
		            ]
	            ),
	            'series'    => $series_data
	        );

	        return $chart_options;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.20: extractKrSummary
    //  Description: Extracts Key Ratios Summary from FR Report
    //-----------------------------------------------------
    public function extractKrSummary()
    {
    	try{
	        exec('pdftotext -eol dos -layout temp/fr_copy.pdf temp/fr_converted.txt');

	        // variable declaration
	        $line_p         = STR_EMPTY;
	        $start_line     = 0;
	        $start          = false;        // used for flagging in while loop
	        $start_data     = false;        // used for flagging in while loop
	        $start_data_1   = false;        // used for flagging in while loop
	        $positive       = false;        // used for flagging in while loop
	        $negative       = false;        // used for flagging in while loop
	        $start_now      = false;        // used for flagging in while loop
	        $positive_arr   = array();      // used for flagging in while loop
	        $negative_arr   = array();      // used for flagging in while loop
	        $parsed_array   = array();
	        $start_cntr     = 0;
	        $handle         = fopen('temp/fr_converted.txt', "r");

	        $description['positive']    = array();
	        $description['negative']    = array();

	        if ($handle) {

	            while (($line = fgets($handle)) !== false) {

	                if (FALSE !== stripos($line, '3.1. Key Ratios Summary')) {
	                    $start_cntr++;
	                    if (2 <= $start_cntr) {
	                        $start = true;
	                    }
	                }

	                if (($start)
	                && (false == $start_data)) {

	                    if ((FALSE !== stripos($line, 'bad'))
	                    || (FALSE !== stripos($line, 'negative'))
	                    || (FALSE !== stripos($line, 'critical'))
	                    || (FALSE !== stripos($line, 'unacceptable'))
	                    || (FALSE !== stripos($line, 'fail'))
	                    || (FALSE !== stripos($line, 'abnormal'))
	                    || (FALSE !== stripos($line, 'unsatisfactory'))) {
	                        $positive = false;
	                        $negative = true;
	                    }
	                    else if ((FALSE !== stripos($line, 'good'))
	                    || (FALSE !== stripos($line, 'positive'))
	                    || (FALSE !== stripos($line, 'acceptable'))
	                    || (FALSE !== stripos($line, 'excellent'))
	                    || (FALSE !== stripos($line, 'high'))
	                    || (FALSE !== stripos($line, 'normal'))
	                    || (FALSE !== stripos($line, 'outstanding'))
	                    || (FALSE !== stripos($line, 'success'))
	                    || (FALSE !== stripos($line, 'satisfactory'))) {
	                        $positive = true;
	                        $negative = false;
	                    }

	                    if (FALSE !== stripos($line, ':')) {
	                        $start_data = true;
	                    }
	                }

	                if ($start_data) {

	                    if (FALSE !== stripos($line, '●')) {

	                        if (1 == $start_line) {

	                            $start_line = 2;
	                        }
	                        else {
	                            $start_line = 1;
	                        }

	                    }

	                    if (1 == $start_line) {
	                        $line_p .= trim(str_replace('<br />', '', nl2br($line))).' ';
	                    }
	                    else if (2 == $start_line) {
	                        if ($positive) {
	                            $positive_arr[]   = $line_p;
	                        }
	                        else {
	                            $negative_arr[]   = $line_p;
	                        }

	                        $line_p     = $line;
	                        $start_line = 1;
	                    }

	                    if (('<br />' == trim(nl2br($line)))
	                    && (1 == $start_line)) {

	                        if ($positive) {
	                            $positive_arr[]   = $line_p;
	                        }
	                        else {
	                            $negative_arr[]   = $line_p;
	                        }

	                        $line_p         = STR_EMPTY;
	                        $start_line     = 0;
	                        $start_data     = false;
	                    }
	                }

	                if ($start && stripos($line, '3.2.')!==false) {
	                    break;
	                }
	            }

	            fclose($handle); // close file
	        }
	        else {
	            // error opening the file.
	        }

	        foreach ($positive_arr as $parsed) {
	            $parsed_clean   = trim($parsed, "\x00..\x1F");
	            $parsed_clean   = trim($parsed_clean, '●');
	            $parsed_clean   = trim($parsed_clean);
	            $parsed_clean   = ucfirst($parsed_clean);
	            $description['positive'][]  = $parsed_clean;
	        }

	        foreach ($negative_arr as $parsed) {
	            $parsed_clean   = trim($parsed, "\x00..\x1F");
	            $parsed_clean   = trim($parsed_clean, '●');
	            $parsed_clean   = trim($parsed_clean);
	            $parsed_clean   = ucfirst($parsed_clean);
	            $description['negative'][]  = $parsed_clean;
	        }

	        return $description;
    	}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    
    //-----------------------------------------------------
    //  Function 1.26: crossmatchOpensanction
    //  Description: crossmatch function for opensanctions
    //-----------------------------------------------------
	public function crossmatchOpensanction($entity){

		try{
	        // Fetch personalities that need to be crossmatched.
	        $keyManagers = KeyManager::select("firstname" , "middlename", "lastname", "nationality")
	                        ->where('is_deleted',0)
	                        ->where('entity_id', $entity->entityid)
	                        ->get();

	        $shareHolders = Shareholder::select("name", "firstname", "middlename", "lastname", "nationality")
	                        ->where('is_deleted',0)
	                        ->where('entity_id', $entity->entityid)
	                        ->get();

	        $directors = Director::select("name", "firstname", "middlename", "lastname", "nationality")
	                        ->where('is_deleted',0)
	                        ->where('entity_id', $entity->entityid)
	                        ->get();

	        // start crossmatch
	        $lists = array();
	        foreach ($keyManagers as $key) {
	            // get nationality code
	            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $key->nationality)->first();
	            $nationality="";
	            $data = array(
	                "firstname" => strtolower($key->firstname), 
	                "lastname" => ($key->middlename) ? strtolower($key->middlename .  " " . $key->lastname) : strtolower($key->lastname)
	            );
	            if($ncode) {
	                $nationality = $ncode->country_code;
	            } 
	            $name = $data['firstname'] . " " . $data['lastname'];
	            if(!in_array(strtolower($name), $lists)) { 
	                $lists[] = strtolower($name);
	                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
	            }
	           
	        }

	        foreach ($shareHolders as $key => $share) {
	            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $share->nationality)->first();
	            $nationality="";
	            $data = array(
	                "firstname" => strtolower($share->firstname), 
	                "lastname" => ($share->middlename)? strtolower($share->middlename .  " " . $share->lastname): strtolower($share->lastname)
	            );
	            if($ncode) {
	                $nationality = $ncode->country_code;
	            } 
	            if($data['firstname']) {
	                $name = $data['firstname'] . " " . $data['lastname'];
	                if(!in_array(strtolower($name), $lists)) { 
	                    $lists[] = strtolower($name);
	                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
	                }
	            } else {
	                $name = strtolower($share['name']);
	                if(!in_array(strtolower($name), $lists)) { 
	                    $lists[] = strtolower($name);
	                    $charges = $this->findByName($name, $entity->entityid, $nationality);
	                }
	            }
	        }
	        
	        foreach ($directors as $key => $dir) {
	            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $dir->nationality)->first();
	            $nationality="";
	            $data = array(
	                "firstname" => strtolower($dir->firstname), 
	                "lastname" => ($dir->middlename)? strtolower($dir->middlename .  " " . $dir->lastname): strtolower($dir->lastname)
	            );
	            if($ncode) {
	                $nationality = $ncode->country_code;
	            } 
	            if($data['firstname']) {
	                $name = $data['firstname'] . " " . $data['lastname'];
	                if(!in_array(strtolower($name), $lists)) { 
	                    $lists[] = strtolower($name);
	                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
	                }
	            } else {
	                $name = strtolower($dir['name']);
	                if(!in_array(strtolower($name), $lists)) { 
	                    $lists[] = strtolower($name);
	                    $charges = $this->findByName($name, $entity->entityid, $nationality);
	                }
	            }
	        }
		    return;
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
    
     //-----------------------------------------------------
    //  Function 1.27: findByFirstAndLastname
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByFirstAndLastname($data,$entity_id, $nationality) {

    	try{
	        $entity = Entity::where("entityid", $entity_id)->first();
	        $records = DB::table("open_sanctions")->select("charges")->where($data);
	        if($nationality) {
	            $records->where("nationality", "like", "%{$nationality}%");
	        }
	        $records = $records->get();
	        if($records->count() > 0) {
	            $name = ucwords($data['firstname'] . " " . $data['lastname']);

	            // $charges = "<span><b> {$name}: </b> </span> <ul>";
	            // foreach ($records as $key => $value) {
	            //     if ($value->charges) {
	            //         $charges .= "<li> " . $value->charges . " </li>";
	            //     }
	            // }
	            // $charges .= "</ul>";

	            $charges = "<tr><td> {$name} </td><td><ul>";
	            if ($entity->negative_findings == "") {
	                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
	            } else {
	                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
	            }
	            foreach ($records as $key => $value) {
	                if ($value->charges) {
	                    $charges .= "<li> " . $value->charges . " </li>";
	                }
	            }
	            $charges .= "</ul></td></tr></tbody></table>";


	            // update negative findings
	            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
	        }
	        return;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.28: findByName
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByName($name,$entity_id, $nationality) {
    	try{
	        $entity = Entity::where("entityid", $entity_id)->first();
	        $name = strtolower($name);
	        if($nationality) {
	            $nationality = " AND nationality LIKE '%{$nationality}%'";
	        }
	        $records = DB::select("SELECT charges FROM open_sanctions WHERE CONCAT(firstname, ' ', lastname) = '{$name}' {$nationality}");
	        if(!empty($records)) {
	            $name = ucwords($name);
	            $charges = "<tr><td> {$name} </td><td><ul>";
	            if ($entity->negative_findings == "") {
	                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
	            } else {
	                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
	            }
	            foreach ($records as $key => $value) {
	                if ($value->charges) {
	                    $charges .= "<li> " . $value->charges . " </li>";
	                }
	            }
	            $charges .= "</ul></td></tr></tbody></table>";
	            // update negative findings
	            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
	        }
	        return;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.29: getKeySummaryByEntityId
    //  Description: Test function to show Key Ratio Summary good and bad values using interal functions
    //-----------------------------------------------------
    public function getKeySummaryByEntityId($id) {
    	try{
	        $getSummary = new KeyRatioService();
	        $response = $getSummary->getEntityKeySummary($id);
	        return $response;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 1.30: getNetAssetsByEntityId
    //  Description: Test function to show Key Ratio Summary good and bad values using interal functions
    //-----------------------------------------------------
    public function getNetAssetsByEntityId($id)
    {
    	try{
	        $financialReport = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->first();
	        if(!empty($financialReport)) {
	            $balanceSheets = BalanceSheet::where('financial_report_id', $financialReport->id)
	                ->orderBy('index_count', 'desc')
	                ->get();
	            if(!empty($balanceSheets)) {
	                foreach($balanceSheets as $key => $balanceSheet) {
	                    $incomeStatement = IncomeStatement::where('financial_report_id', $balanceSheet->financial_report_id)
	                    ->where('Year', $balanceSheet->Year)
	                    ->where('GrossProfit', '>', 0)
	                    ->where('ProfitLoss', '>', 0)
	                    ->where('ComprehensiveIncome', '>', 0)
	                    ->first();
	                    if($balanceSheets->count() != 4 && $incomeStatement == null) {
	                        $balanceSheets->pull($key);
	                    }
	                    if ($balanceSheets->count() == 4 && $key == 1 && $incomeStatement == null) {
	                        $balanceSheets->pull($key);
	                        $balanceSheets->pull($key-1);
	                    }
	                }
	                $balanceSheets = $balanceSheets->values();
	            } else {
	                $financialReport = null;   
	            }
	        } else {
	            $balanceSheets = [];
	        }
	        return view('ratios.net-assets', compact('financialReport', 'balanceSheets'));
    	}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
}