<?php

namespace CreditBPO\Controllers;

use CreditBPO\Models\DebtServiceCapacityCalculator;
use CreditBPO\Models\Entity;
use BaseController;
use Input;
use Validator;

class DebtServiceCapacityController extends BaseController {

    //-----------------------------------------------------
    //  Function 55.1: saveEntityDSCR
	//  Description: Function to save Entity's DSCR
    //-----------------------------------------------------
    public function saveEntityDSCR() 
    {
        $input = Input::all();

        $rules = [
            'entity_id' => 'required|numeric',
            'dscr' => 'required|numeric',
            'loan_duration' => 'required|numeric',
            'net_operating_income' => 'required|numeric',
            'loan_amount' => 'required|numeric',
            'working_interest_rate_local' => 'required|numeric',
            'working_interest_rate_foreign' => 'required|numeric',
            'term_interest_rate_local' => 'required|numeric',
            'term_interest_rate_foreign' => 'required|numeric',
            // 'period' => 'required|numeric',
            // 'payment_per_period_local' => 'required|numeric',
            // 'payment_per_period_foreign' => 'required|numeric',
            'is_custom' => 'required|numeric',
            'source' => 'required',
        ];

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return json_encode([
                'valid' => STS_NG,
            ]);
        }

        $entity = Entity::where('entityid', $input['entity_id'])->first();

        if(($entity->cbpo_pdf != "") && (file_exists(public_path('documents/' . $entity->cbpo_pdf)))){
            unlink(public_path('documents/' . $entity->cbpo_pdf));
        }
        
        Entity::where('entityid', $input['entity_id'])->update(['cbpo_pdf' => '']);

        $model = new DebtServiceCapacityCalculator();
        $model->replace($input);

        return json_encode([
            'valid' => STS_OK,
        ]);
    }
}
