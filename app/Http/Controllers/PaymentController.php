<?php
use \Exception as Exception;
/** Omnipay Library */
use Omnipay\Omnipay;

use CreditBPO\Models\Zipcode;
use CreditBPO\Models\Entity;
use CreditBPO\Services\MailHandler;
use CreditBPO\Libraries\PaymentCalculator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use CreditBPO\Libraries\StringHelper;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client as GuzzleClient;

class PaymentController extends BaseController
{
    /**  variable declaration */
    private $url;                           // DragonPay URL
	private $merchant_id = 'CREDITBPO';     // Merchant ID to accesss API
    private $merchant_key = 'N6f9rEW3';     // Merchat Key for API access
    private $bank_subscription;				// storage for bank_subscription
	private $paymentCalculator;
	private $accountDetails = [];
	private $statusUrl;

    //-----------------------------------------------------
    //  Function 55.1: __construct
    //      Initialization
    //-----------------------------------------------------
    public function __construct()
	{
        try{
	        /*  For Production Environment  */
			if (env("APP_ENV") == "Production") {
				$this->url = 'https://gw.dragonpay.ph/Pay.aspx';
				$this->statusUrl = 'https://gw.dragonpay.ph/api/collect/v1';
			}
	        /*  For Testing Environment     */
	        else {
				$this->url = 'http://test.dragonpay.ph/Pay.aspx';
				$this->statusUrl = 'https://test.dragonpay.ph/api/collect/v1';
	        }

	        $this->bank_subscription = Permissions::get_bank_subscription(); // check for bank subscription status
			$this->paymentCalculator = new PaymentCalculator;
			if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor') == false){
				$this->accountDetails = BankSessionHelper::createBankSession(Auth::user());
			} else if (Auth::user() && Auth::user()->role === 4 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			} else if (Auth::user() && Auth::user()->role === 0 && Session::has('supervisor')){
				$this->accountDetails = Session::get('supervisor');
			}
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}

    }
    
    //-----------------------------------------------------
    //  Function 55.2: postPayViaPaypal
    //      HTTP Post request to validate and save PayPal
    //      payments
    //-----------------------------------------------------
    public function postPayViaPaypal($id)
	{
		try{
	        /*  Fetch the specified report from the database    */
	        $entity = Entity::where('entityid', $id)
	                ->where('is_paid', 0)
	                ->first();
	            
	        /* Set price information */
	        $paymentCalculator = new PaymentCalculator;

	        $bank = Bank::where('id', '=', $entity->current_bank)->first();
	        $locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

			/* Set price information */
			$paymentCalculator = new PaymentCalculator;
			$tax = 0;

			$bank = Bank::where('id', '=', $entity->current_bank)->first();
			$locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

			/** Calculate Report Price */
			$reportPrice = $paymentCalculator->paymentComputeAllReportType($id);

			if($bank->is_custom != 0) {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $bank->custom_price;
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
					$price = $bank->custom_price;
				} else { // Standlone Report
					$price = $bank->custom_price;
				}
			} else {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $reportPrice['premium'];
					$report_type = 'Premium';
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
					$price = $reportPrice['simplified'];
					$report_type = 'Simplified';
				} else { // Standalone Report
					$price = $reportPrice['standalone'];
					$report_type = 'Standalone';
				}
			}

			if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
				$report_type = 'Premium';
			} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
				$report_type = 'Simplified';
			} else { // Standlone Report
				$report_type = 'Standalone';
			}

			/* Price and amount are same here since the quantity is 1 in purchasing report.
			But I'll still declare amount variable for using right terms during calculation */
			$amount = $price;
			$subTotal = $amount;
			$displayDiscount = null;
			$discount_value = 0;

			/*  Check Discount  */
			$discount = Discount::where('discount_code', $entity->discount_code)->first();

			/*  There is a discount */
			if ($discount){
				if ($discount->type == $paymentCalculator::DISCOUNT_TYPE_PERCENTAGE){
					$displayDiscount = '(less: '. $discount->amount .'%)';
					$discount_value = ($amount * ($discount->amount / 100));
				} else{
					$displayDiscount = '(less: '. number_format($discount->amount, 2, '.', ',').')';
				}
				/* Get subtotal*/
				$subTotal = $paymentCalculator->computeSubtotal($amount, $discount->amount, $discount->type);
				/*  Free Trial  */
				if (0 >= $subTotal) {
					$entity_upd  = DB::table('tblentity')
						->where('loginid', Auth::user()->loginid)
						->where('entityid',$entity->entityid)
						->update(
							array(
								'is_paid' => 1
							)
						);
					$account_upd    = DB::table('tbllogin')
						->where('loginid', Auth::user()->loginid)
						->update(
							array(
								'status'    => 2
							)
						);
					return Redirect::to('/dashboard/index');
				}
			}

			// $totalAmount = $paymentCalculator->computeTotalAmountBySubTotal($subTotal);
			$totalAmount = $amount - $discount_value;

			$report_details = [
				'report_type' => $report_type,
				'amount' => $amount,
				'discount' => $discount_value,
				'total_amount' => $totalAmount,
				'tax_rate' => '0%',
				'tax_amount' => ($tax * ($totalAmount))
			];

	        // if($bank->is_custom != 0) {
	        //     if($entity->is_premium == PREMIUM_REPORT ) { // Premium Report
	        //         $price = $bank->custom_price + $locPremiumReportPrice->premiumReportPrice;
	        //     } elseif ($entity->is_premium == SIMPLIFIED_REPORT) { // Simplified Report
	        //         $price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
	        //     } else { // Standalone Report
	        //         $price = $bank->custom_price;
	        //     }
	        // } else {
	        //     if($entity->is_premium == PREMIUM_REPORT) {
	        //         $price = $paymentCalculator::REPORT_PRICE + $locPremiumReportPrice->premiumReportPrice; //$paymentCalculator::PREMIUM_REPORT_PRICE;
	        //     } elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
	        //         $price = $paymentCalculator::SIMPLIFIED_REPORT_PRICE;
	        //     } else { // Standalone Report
	        //         $price = $paymentCalculator::REPORT_PRICE;
	        //     }
	        // }

	        // $discount = null;
			// $discountType = null;

	        // /*  When there is a discount code   */
			// if ($entity->discount_code!=null) {
	        //     $discount_data = Discount::where('discount_code', $entity->discount_code)->first();

	        //     if ($discount_data){
	        //         $discount = $discount_data->amount;
			// 		$discountType = $discount_data->type;
	        //     }
	        // }

	        /*  Create a payment receipt record on the database */
	        $paymentReceiptDb = new PaymentReceipt;
	       
	        $paymentReceipt['price'] = $amount;
	        $paymentReceipt['amount'] = $amount;
	        $paymentReceipt['discount'] = $discount_value;
	        $paymentReceipt['subtotal'] = $amount - $discount_value;
	        $paymentReceipt['vat_amount'] = ($tax * ($amount - $discount_value));
	        $paymentReceipt['total_amount'] = $totalAmount;
	        $paymentReceipt['item_quantity'] = 1;
	        $paymentReceipt['login_id'] = Auth::user()->loginid;
	        $paymentReceipt['reference_id'] = 0;
	        $paymentReceipt['payment_method'] = 1;
	        $paymentReceipt['item_type'] = 1;            /*  SME Account   */
	        $paymentReceipt['sent_status'] = STS_NG;
	        $paymentReceipt['date_sent'] = date('Y-m-d');
	        /* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
	        $paymentReceipt['vatable_amount'] = null;
	        $paymentReceiptSts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

	        /*  Configure the PayPal required parameters    */
			$params = array(
				'cancelUrl' => URL::to('/').'/payment_cancel',
				'returnUrl' => URL::to('/').'/payment/paymentsuccess',
				'name' => 'CreditBPO Services',
				'description' => 'CreditBPO Rating Report®',
				'amount' => number_format($totalAmount,2,'.',''),
				'currency'      => 'PHP',
	            'reference_id'  => $paymentReceiptSts // Store receipt id for reference after PayPal responds
			);

	        /*  Put parameters into Session for reference after submitting to PayPal    */
	        session()->put("params",$params);
	        session()->save();

	        /** Save transaction to db */
	        $newParams = array(
	            "created_at"  => date("Y-m-d H:i:s"),
	            "entity_id"  => $id,
	            "token"  => session()->get("_token"),
	            "cancel_url" => $params['cancelUrl'],
	            "return_url" => $params['returnUrl'],
	            "name"  => $params['name'],
	            "description"  => $params['description'],
	            "amount" => $params['amount'],
	            "currency" => $params['currency'],
	            "reference_id"  => $params['reference_id']
	        );

	        DB::table("paypal_transactions")->insert($newParams);
	        

	        /*  Create an Instance of OmniPay library used for PayPal   */
			$gateway = Omnipay::create('PayPal_Express');

	        /*  Configuration for Production Environment    */
			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setLogoImageUrl(URL::To('images/creditbpo-logo.png'));
				$gateway->setTestMode(false);
			}
	        /*  Configuration for Testing Environment       */
	        else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setLogoImageUrl(URL::To('images/creditbpo-logo.png'));
				$gateway->setTestMode(true);
			}

	        /*  Send data to PayPal     */
			$response = $gateway->purchase($params)->send();

	        /*  PayPal payment sucessful    */
			if ($response->isSuccessful()) {
				print_r($response);
			}
	        /*  Redirect to PayPal form     */
	        else if ($response->isRedirect()) {
				$response->redirect();
			}
	        /*  Error occured during Payment    */
	        else {
				echo $response->getMessage();
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.3: getCancelPayment
    //      Displays the Cancel Payment Page
    //-----------------------------------------------------
	public function getCancelPayment()
	{
		try{
			return View::make('payment.cancel_order');
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 55.4: getSuccessPayment
    //      PayPal Callback function, after PayPal receives
    //      and processes the parameters, it will call this
    //      function
    //-----------------------------------------------------
    public function getSuccessPayment()
	{
		try{
	        /*  Create an Instance of OmniPay library used for PayPal   */
			$gateway = Omnipay::create('PayPal_Express');

	        /*  Configuration for Production Environment    */
			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setTestMode(false);
			}
	        /*  Configuration for Testing Environment       */
	        else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setTestMode(true);
			}

	        /*  Get the session parameters set before passing data to PayPal    */
	        
	        $params = session()->get('params');
	        if(!$params) {
	            $token = session()->get("_token");
	            $entity = session()->get("entity");
	            if(!empty($entity)){
	                $where = array("token" => $token, "entity_id" => $entity->entityid);
	                $params = (array) DB::table("paypal_transactions")->select("cancel_url as CancelUrl, return_url as returnUrl, name, description, amount, currency, reference_id")->where($where)->orderBy("created_at", "desc")->first();
	            }
	        }

			$response = $gateway->completePurchase($params)->send();
	        $paypalResponse = $response->getData();
	        
	        /*  PayPal payment was succesfull   */
			if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
	            /*  Check if there is already a payment for this transaction    */
				$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

				$entity = session()->get("entity");
				$payment = new Payment();
	      		$payment->loginid = Auth::user()->loginid;
				$payment->entity_id = !empty($entity) ? $entity->entityid : '';
	      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
	      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
	      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
	      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
	      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
	      		$payment->save();
	            
	            /*  Get the receipt from the database using the reference_id passed to PayPal   */
	            $receipt = PaymentReceipt::where('login_id', Auth::user()->loginid)
	                ->where('receipt_id', $params['reference_id'])
	                ->where('sent_status', STS_NG)
	                ->first();
	               
	            /*  Update the receipt status to sent       */
	            DB::table('payment_receipts')
	                ->where('receipt_id', $receipt['receipt_id'])
	                ->update(array('sent_status' => STS_OK));
	                
	            /*  Send the receipt to Customer's email    */
	            if(session()->get("entity") && Auth::user()->role != 4 ) {
	                $entity = session()->get("entity");
	            } else {
	                $entity = Entity::where('loginid' , Auth::user()->loginid)
	                    ->where('is_paid', 0)
	                    ->orderBy('entityid', 'desc')
	                    ->first();
	            }

				try {
					$user = User::find(Auth::user()->loginid);

					if($user->parent_user_id != 0) {
						$user = User::find($user->parent_user_id);
					} 

					$mailHandler = new MailHandler();
					$mailHandler->sendReceipt(
						Auth::user()->loginid,
						[
							'receipt' => $receipt,
							'entity' => $entity,
							'user' => $user,
						],
						'CreditBPO Payment Receipt'
					);

					// Send email if purchased is basic user report - premium type
					if(!empty($entity)){
						if($entity->is_premium == PREMIUM_REPORT) {
							$mailHandler->sendPremiumReportNotification($entity);
						}
					}
				} catch (Exception $e) {
					
					if(env("APP_ENV") != "Production"){
						return $e;	
					}

					// Log::error($ex->getMessage());
					// return Redirect::to('dashboard/error');
				}
	            //}

	            /* User is a Supervisor */
				if (Auth::user()->role==4) {
	                /* Create Keys after Payment */
					if ($payment_check == 0) {
						BankSerialKeys::createKeys($payment->loginid, $receipt['item_quantity'], Session::get('keyType'));
					}

					return Redirect::to('/bank/keys');
				}
	            /* User is an SME */
	            else {
	                $isFirst = false;    // User Account is newly created Flag
	                $userReports = Entity::where('loginid', Auth::user()->loginid)->get();
					if (!$userReports->isEmpty()){
						if ($userReports->count() == 1){
	                        $isFirst = true;
	                        $user = User::where('loginid', '=', Auth::user()->loginid)->first();
	                        if ($user->status != 2) {
	                            $user->status = 2;
	                            $user->save();
	                        }
						}
	                }
	                
	                if(!empty($entity->entityid))
	                	$where = 'entityid = '.$entity->entityid.' AND is_paid = 0';
	                else
	                	$where = 'is_paid = 0';
	                
	                /*  Payment is successful and not yet recorded  */
					if ($payment_check == 0) {

						
	                    /*  Update Report to Paid Status    */
						$entity = Entity::where('loginid', Auth::user()->loginid)
	                            ->whereRaw($where)
	                            ->orderBy('entityid', 'desc')
	                            ->first();

	                    Entity::whereRaw($where)->update(['is_paid' => 1]);
	     
					}

	                /*  If account is newly created show the MOA */
					if ($isFirst && ($entity->is_premium == 2 || $entity->is_premium == 0 )) {
						return View::make('signup_confirmation');
	                } elseif ($entity->is_premium == 1) {
	                    $region = Zipcode::where('id', $entity->cityid)->first();

	                    $regionArea = $region->region;

	                    if($regionArea == "NCR"){
	                        return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 business days.');
	                    }else{
	                        return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 10 business days.');
	                    }
	                }
	                /*  When user account is not new show the Report List */
	                else {
						return Redirect::to('/dashboard/index');
					}
				}
			}
	        /* PayPal Payment Failed. Show the error */
	        else {
	            $pay_sts = $paypalResponse['L_SHORTMESSAGE0'];
	            $error = $paypalResponse['L_LONGMESSAGE0'];

	            return View::make('errors.payment_error', array('pay_sts' => $pay_sts, 'error' => $error));
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 55.5: paymentPostback
    //      Postback function for DragonPay after payment
    //      callback is called
    //-----------------------------------------------------
	public function paymentPostback()
	{
		try{
	        /*  Get the response from DragonPay */
			$response = array(
				'id'        => Input::get('txnid'),
				'refno'     => Input::get('refno'),
				'status'    => Input::get('status'),
				'message'   => Input::get('message'),
				'key'       => $this->merchant_key
			);

			$digest_check   = Input::get('digest');
			$seed           = implode(':', $response);
			$digest         = Sha1($seed);

			/*  Log DragonPay activity  */
			$file   = "dragonpay_logs/dragonpay_postback.log";
			$string = date('c') . " => " . $seed . "\r\n";

			if (file_exists($file)) {
				if (filesize($file) > 5242880) {
					rename($file, "dragonpay_logs/" . date('ymd') . "_dragonpay_postback.log");
				}
			}

			file_put_contents($file, $string , FILE_APPEND | LOCK_EX);

			if ($digest_check == $digest) {
				$transaction = Transaction::find($response['id']);
				$create_keys = false;

				if ($transaction->status == NULL OR $transaction->status == "P") {
					/*  Update Database Record  */
					$create_keys            = true;

					$transaction->refno     = $response['refno'];
					$transaction->status    = $response['status'];
					$transaction->message   = $response['message'];
					$transaction->save();

				}

	            /*  Get Account Information */
				$user = User::where('loginid', $transaction->loginid)
	                ->first();

	            /*  Account is for Independent key buyers   */
	            if ($transaction->loginid == 0) {
					if ($response['status'] == "S") {
						IndependentKey::where('email', $transaction->email)
	                        ->where('batch', $transaction->batch)
	                        ->update(array('is_paid'=>1));

						$oldhash    = base64_encode($transaction->email);
						$hash       = substr_replace($oldhash, $transaction->batch, 10, 0);

						/*  Email to Keys to the buyer  */
						if (env("APP_ENV") != "local") {

	                        try{
	    						Mail::send('emails.buyer_keys_paid', array(
	    								'hash'=>$hash
	    							), function($message){
	    								$message->to($transaction->email, $transaction->email)
	    									->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

	    								$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
	    								$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
	    						});
	                        }catch(Exception $e){
	                        //
	                        }
						}
					}
				}
	            /*  Account is Supervisor   */
	            else if ($user->role == 4) {
	                /*  Transaction Successful   */
					if ($response['status'] == "S") {

						if ($create_keys) {
							if ($transaction->description == "Bank User Subscription") {

								if ($create_keys) {
									$subs = BankSubscription::firstOrCreate(array(
										'bank_id' => $user->loginid
									));

									$date = new DateTime();
									$start_day = $date->format('j');

									$date->modify("+1 month");
									$end_day = $date->format('j');

									if ($start_day != $end_day)
										$date->modify('last day of last month');

									$subs->expiry_date = $date->format('Y-m-d H:i:s');
									$subs->save();
								}

	                            if (3 == $user->product_plan) {
	                                $user->product_plan    = 1;
	                                $user->save();
	                            }

							}
	                        else {
	                            /*--------------------------------------------------------------------
	                            /*	Email Receipt to Client
	                            /*------------------------------------------------------------------*/
	                            $receipt    = PaymentReceipt::where('login_id', $user->loginid)
	                                ->where('reference_id', $response['id'])
	                                ->where('sent_status', STS_NG)
	                                ->where('payment_method', 2)
	                                ->first();

	                            $receipt->sent_status = STS_OK;
	                            $receipt->save();

								$user = User::find(Auth::user()->loginid);

								if($user->parent_user_id != 0) {
									$user = User::find($user->parent_user_id);
								}

								if (env("APP_ENV") != "local") {
									try{
										$mailHandler = new MailHandler();
										$mailHandler->sendReceipt(
											Auth::user()->loginid,
											[
												'receipt' => $receipt,
												'user' => $user
											],
											'CreditBPO Payment Receipt'
										);
									} catch (Exception $e) {
										$creditBPOUser = Str::endsWith(Auth::user()->email, '@creditbpo.com');
										if($creditBPOUser) {
											return Redirect::to('/bank/purchase_keys')->withErrors(array('discount_code' => 'For CreditBPO Personnel: ' . $e->getMessage()))->withInput();
										} else {
											Log::info('Error in paymentPostback()');
											Log::warning($e->getMessage());
										}
									}
								}
								
	                            BankSerialKeys::createKeys($user->loginid, $receipt->item_quantity, Session::get('keyType'));
							}
						}
					}
				}
	            /*  Account is SME  */
	            else {
					if ($response['status'] == "S") {
						// update login status
						User::where('loginid', '=', $user->loginid)->update(array('status' => 2));

						if($create_keys){
							// set paid on first unpaid entity
							$entity = Entity::where('entityid', $transaction->entity_id)
											->where('is_paid', 0)->first();
							$entity->is_paid = 1;
							$entity->save();
						}
					}
				}
			}

			return Response::make("complete");
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 55.6: paymentReturn
    //      Callback function for DragonPay after payment
    //-----------------------------------------------------
	public function paymentReturn()
	{
		try{

			/*  Get the response from DragonPay */
			$ctxnId = substr(Input::get('txnid'),0,-10);
			$response = array(
				'id'        => Input::get('txnid'),
				'refno'     => Input::get('refno'),
				'status'    => Input::get('status'),
				'message'   => Input::get('message'),
				'key'       => $this->merchant_key,
			);
			
			$digest_check   = Input::get('digest');
			$seed           = implode(':', $response);
			$digest         = Sha1($seed);

			/*  Log DragonPay activity  */
			$file   = public_path()."/dragonpay_logs/dragonpay.log";
			$string = date('c') . " => " . Request::server('HTTP_HOST') . Request::server('REQUEST_URI') . "\r\n";

	        if (file_exists($file)) {
				if (filesize($file) > 5242880) {
					rename($file, "dragonpay_logs/" . date('ymd') . "_dragonpay.log");
				}
			}

			file_put_contents($file, $string , FILE_APPEND | LOCK_EX);

			/** API REQUEST */
			if(!Auth::check()){
				if ($digest_check == $digest) {
					// Check if the server has a record on the database, because some request came from the cbpo test server
					$isCheckTransaction = Transaction::where('id', Input::get('txnid'))->first();
					if($isCheckTransaction){
						Session::put('api_request', true);
						Session::put('txnid', Input::get('txnid'));
						Session::put('refno', Input::get('refno'));
						Session::put('status', Input::get('status'));
						Session::put('message', Input::get('message'));
						Session::put('digest', $digest);
						Session::save();
						return redirect('/api/v1/payments/dragonpay-response');
					}else{
						//
					}
				}else{
					return Response::make('Incorrect digest.', 401);
				}
			}

			$response['id'] = $ctxnId;
			if ($digest_check == $digest) {
				$transaction = Transaction::find($response['id']);
				$create_keys = false;

				if ($transaction->status == NULL || $transaction->status == "P") {
					/*  Update Database Record  */
					$create_keys            = true;

					$transaction->refno     = $response['refno'];
					$transaction->status    = $response['status'];
					$transaction->message   = $response['message'];
					$transaction->save();
				}

	            /*  Get Account Information */
				$user = User::where('loginid', $transaction->loginid)->first();

	            /*  Account is Supervisor   */
				if ($user->role==4) {
	                /*  Transaction Successful   */
					if ($response['status'] == "S") {

	                    /*  Bank Subscription   */
						if ($transaction->description == "Bank User Subscription") {

	                        /*  Create or extend bank Subscription  */
							if ($create_keys) {
								$subs = BankSubscription::firstOrCreate(array(
									'bank_id' => $user->loginid
								));

								$date = new DateTime();
								$start_day = $date->format('j');

								$date->modify("+1 month");
								$end_day = $date->format('j');

								if ($start_day != $end_day)
									$date->modify('last day of last month');

								$subs->expiry_date = $date->format('Y-m-d H:i:s');
								$subs->save();
							}

	                        /*  Create or extend bank Subscription  */
	                        if (3 == $user->product_plan) {
	                            $user->product_plan    = 1;
	                            $user->save();
							}

							/*  Email Receipt to Client */
	                        $receipt = PaymentReceipt::where('login_id', $user->loginid)
	                            ->where('reference_id', $response['id'])
	                            ->where('sent_status', STS_NG)
	                            ->where('payment_method', 2)
								->first();

	                        if ($receipt) {
	                            $receipt->sent_status = STS_OK;
	                            $receipt->save();
							
								if(env("APP_ENV") != "local"){
									$user = User::find(Auth::user()->loginid);

									if($user->parent_user_id != 0) {
										$user = User::find($user->parent_user_id);
									}

									$mailHandler = new MailHandler();
									$mailHandler->sendReceipt(
										Auth::user()->loginid,
										[
											'receipt' => $receipt,
											'user' => $user,
										],
										'CreditBPO Payment Receipt'
									);
								}
							}

							return Redirect::to('/bank/dashboard');
						}
	                    /*  Supervisor Keys Payment */
						else {
	                        /*  Email Receipt to Client */
	                        $receipt = PaymentReceipt::where('login_id', $user->loginid)
	                            ->where('reference_id', $response['id'])
	                            ->where('sent_status', STS_NG)
	                            ->where('payment_method', 2)
								->first();

	                        if ($receipt) {
	                            $receipt->sent_status = STS_OK;
	                            $receipt->save();

								if(env("APP_ENV") != "local"){
									$user = User::find(Auth::user()->loginid);

									if($user->parent_user_id != 0) {
										$user = User::find($user->parent_user_id);
									}

									$mailHandler = new MailHandler();
									$mailHandler->sendReceipt(
										Auth::user()->loginid,
										[
											'receipt' => $receipt,
											'user' => $user
										],
										'CreditBPO Payment Receipt'
									);
								}
	                            BankSerialKeys::createKeys($user->loginid, $receipt->item_quantity, Session::get('keyType'));
	                        }
							return Redirect::to('/bank/keys');
						}
					}
	                /*  Transaction Pending */
					else if ($response['status'] == "P") {
						return View::make('payment.pending', array('refno' => $response['refno']));
					}
	                /*  Transaction Error   */
					else {
						return View::make('payment.error');
					}
				}
	            /*  Account is SME  */
	            else {
	                /*  Transaction Successful  */
					if ($response['status'] == "S") {
						/* Check if this is the first report of user */
						$isFirst = false;
						$userReports = Entity::where('loginid', $user->loginid)->get();
						if (!$userReports->isEmpty()){
							if ($userReports->count() == 1){
								$isFirst = true;
								$user->status = 2;
								$user->save();
							}
						}
							
						$entity = Entity::where('entityid', $transaction->entity_id)
								->where('is_paid', 0)->first();

						if ($create_keys) {
							// set paid on first unpaid entity
							$entity->is_paid = 1;
							$entity->save();
						}

	                    /*--------------------------------------------------------------------
	                    /*	Email Receipt to Client
	                    /*------------------------------------------------------------------*/
						$receipt = PaymentReceipt::where('login_id', Auth::user()->loginid)
								 ->where('reference_id', $response['id'])
								 ->where('sent_status', STS_NG)
								 ->where('payment_method', 2)
								 ->first();
	                    $receipt->sent_status = STS_OK;
						$receipt->save();
						
						if (env("APP_ENV") != "local") {
							$user = User::find(Auth::user()->loginid);

							if($user->parent_user_id != 0) {
								$user = User::find($user->parent_user_id);
							}

							$mailHandler = new MailHandler();
	                    	$mailHandler->sendReceipt(
								Auth::user()->loginid,
								[
									'receipt' => $receipt,
									'entity' => $entity,
									'user' => $user
								],
								'CreditBPO Payment Receipt'
							);

							if($entity->is_premium == PREMIUM_REPORT){
								$mailHandler->sendPremiumReportNotification($entity);
							}

						}

						//mark the report as paid
						$entity->is_paid = 1;
						$entity->save();

						if ($isFirst && ($entity->is_premium == STANDALONE_REPORT || $entity->is_premium == SIMPLIFIED_REPORT)) {
							return View::make('signup_confirmation');
						} elseif ($entity->is_premium == PREMIUM_REPORT) {
							$region = Zipcode::where('id', $entity->cityid)->first();

							$regionArea = $region->region;
		
							if($regionArea == "NCR"){
								return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 5 business days.');
							}else{
								return Redirect::to('/sme/confirmation/tab1')->with('success', 'Transaction Completed. Premium Report processing will now begin. We will email you upon completion of your report in approximately 10 business days.');
							}
						}
	                    else {
							return Redirect::to('/dashboard/index');
						}
					}
	                /*  Transaction Pending */
					else if ($response['status'] == "P") {
						return View::make('payment.pending', array('refno' => $response['refno']));
					}
	                /*  Transaction Error   */
					else{
						return View::make('payment.error');
					}
				}
			}
			else {
				return Response::make('Incorrect digest.', 401);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.7: paymentIndex
    //      Displays the payment needed page
    //-----------------------------------------------------
	public function paymentIndex()
	{
		try{
	        /*  Not logged in   */
			if (Input::has('id')) {
				$entity_id = Input::get('id');
			}
	        /*  User is logged in   */
	        else {
				$entity     = Entity::where('loginid', Auth::user()->loginid)->where('is_paid', 0)->first();
				$entity_id  = $entity->entityid;
			}

			return View::make('payment.index', ['entity_id'=>$entity_id]);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.8: paymentCheckout
    //      Checkout via DragonPay
    //-----------------------------------------------------
	public function paymentCheckout($id)
	{
		try{
	        /*  Get report information  */
			$entity = Entity::where('entityid', $id)
	                ->where('is_paid', 0)
	                ->first();

			// /* Set price information */
			// $paymentCalculator = new PaymentCalculator;

			// $bank = Bank::where('id', '=', $entity->current_bank)->first();

			// if($bank->is_custom != 0){
			// 	$price = $bank->custom_price;	
			// } else {
			// 	$price = $paymentCalculator::REPORT_PRICE;
			// }
			
			// $discount = null;
			// $discountType = null;

	        // /*  When there is a discount code   */
			// if ($entity->discount_code!=null) {
	        //     $discount_data = Discount::where('discount_code', $entity->discount_code)->first();

	        //     if ($discount_data){
			// 		$discount = $discount_data->amount;
			// 		$discountType = $discount_data->type;
	        //     }
	        // }
	        // /* Compute Payment */
			// $result = $paymentCalculator->paymentCompute($price, 1, $discount, $discountType);

			$rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
			/* Set price information */
			$paymentCalculator = new PaymentCalculator;
			$tax = 0;

			$bank = Bank::where('id', '=', $entity->current_bank)->first();
			$locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

			/** Calculate Report Price */
			$reportPrice = $paymentCalculator->paymentComputeAllReportType($id);

			if($bank->is_custom != 0) {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $bank->custom_price;
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
					$price = $bank->custom_price;
				} else { // Standlone Report
					$price = $bank->custom_price;
				}
			} else {
				if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
					$price = $reportPrice['premium'];
					$report_type = 'Premium';
				} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
					$price = $reportPrice['simplified'];
					$report_type = 'Simplified';
				} else { // Standalone Report
					$price = $reportPrice['standalone'];
					$report_type = 'Standalone';
				}
			}

			if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
				$report_type = 'Premium';
			} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
				$report_type = 'Simplified';
			} else { // Standlone Report
				$report_type = 'Standalone';
			}

			/* Price and amount are same here since the quantity is 1 in purchasing report.
			But I'll still declare amount variable for using right terms during calculation */
			$amount = $price;
			$subTotal = $amount;
			$displayDiscount = null;
			$discount_value = 0;

			/*  Check Discount  */
			$discount = Discount::where('discount_code', $entity->discount_code)->first();

			/*  There is a discount */
			if ($discount){
				if ($discount->type == $paymentCalculator::DISCOUNT_TYPE_PERCENTAGE){
					$displayDiscount = '(less: '. $discount->amount .'%)';
					$discount_value = ($amount * ($discount->amount / 100));
				} else{
					$displayDiscount = '(less: '. number_format($discount->amount, 2, '.', ',').')';
				}
				/* Get subtotal*/
				$subTotal = $paymentCalculator->computeSubtotal($amount, $discount->amount, $discount->type);
				/*  Free Trial  */
				if (0 >= $subTotal) {
					$entity_upd  = DB::table('tblentity')
						->where('loginid', Auth::user()->loginid)
						->where('entityid',$entity->entityid)
						->update(
							array(
								'is_paid' => 1
							)
						);
					$account_upd    = DB::table('tbllogin')
						->where('loginid', Auth::user()->loginid)
						->update(
							array(
								'status'    => 2
							)
						);
					return Redirect::to('/dashboard/index');
				}
			}

			// $totalAmount = $paymentCalculator->computeTotalAmountBySubTotal($subTotal);
			$totalAmount = $amount - $discount_value;
			
	        /*  Save transaction record to the database */
			$transaction                = new Transaction();
			$transaction->loginid       = Auth::user()->loginid;
			$transaction->entity_id     = $id;
			$transaction->amount        = $totalAmount;
			$transaction->ccy           = "PHP";
			$transaction->description   = "CreditBPO Rating Report®";
			$transaction->type			= PD_BASIC_USER_REPORT;
			$transaction->txn 			= $rand;
			$transaction->save();

			$entity->transaction_id = $transaction->id;
			$entity->save();

	        /*  Sets parameters to be passed to DragonPay   */
			$data = array(
				'merchant_id'   => $this->merchant_id,
				'txn_id'        => $transaction->id . $rand,
				'amount'        => number_format($transaction->amount, 2, ".", ""),
				'currency'      => $transaction->ccy,
				'description'   => $transaction->description,
				'email'         => Auth::user()->email,
				'key'           => $this->merchant_key
			);

			$seed   = implode(':', $data);
			$digest = Sha1($seed);
			
			/*  Create a payment receipt record on the database */
	        $paymentReceiptDb = new PaymentReceipt;

			$report_details = [
				'report_type' => $report_type,
				'amount' => $amount,
				'discount' => $discount_value,
				'total_amount' => $totalAmount,
				'tax_rate' => '0%',
				'tax_amount' => ($tax * ($totalAmount))
			];

	        $paymentReceipt['price'] = $amount;
	        $paymentReceipt['amount'] = $amount;
	        $paymentReceipt['discount'] = $discount_value;
	        $paymentReceipt['subtotal'] = $amount - $discount_value;
	        $paymentReceipt['vat_amount'] = $tax * ($totalAmount);
	        $paymentReceipt['total_amount'] = $totalAmount;
	        $paymentReceipt['item_quantity'] = 1;
	        $paymentReceipt['login_id'] = Auth::user()->loginid;
	        $paymentReceipt['reference_id'] = $transaction->id;
	        $paymentReceipt['payment_method'] = 2; /*DragonPay*/
	        $paymentReceipt['item_type'] = 1;            /*  SME Account   */
	        $paymentReceipt['sent_status'] = STS_NG;
	        $paymentReceipt['date_sent'] = date('Y-m-d');
	        /* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
	        $paymentReceipt['vatable_amount'] = null;
	        $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

	        /*  Send data to DragonPay  */
			$apiUrl = $this->statusUrl . '/' . $data['txn_id'] . '/post';
			$client = new GuzzleClient([
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key),
					'Content-Type'	=> 'application/json'
				]
			]);

			$postInput = [
				'Amount'		=> $data['amount'],
				'Currency'		=> $data['currency'],
				'Description'	=> $data['description'],
				'Email'			=> $data['email']
			];

			$result = $client->request('POST', $apiUrl, [ 'body' => json_encode($postInput) ]);
			$data = json_decode($result->getBody()->getContents());

			return Redirect::to($data->Url);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 55.9: paymentSelect
    //      Displays the choose payment method page
    //-----------------------------------------------------
	public function paymentSelect()
	{
		try{
			$reportId = Input::get('id');

			$user = User::find(Auth::user()->loginid);

			if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name == "")) {
					
				$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
				
				if($user->role == 4) { //supervisor
					return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
				} else { //basic user
					return Redirect::to('/billingdetails')->withErrors($error)->withInput();
				}
				
			}

			/* URL doesn't have report id parameter */
			if (!$reportId){
				/* Get the first unpaid report */
				$entity = Entity::where('loginid', Auth::user()->loginid)->where('is_paid', 0)->first();
				$reportId = $entity->entityid;
			}
			
			$entity = Entity::where('entityid', $reportId);		
			$entity = $entity->first();

			// Check if report is paid already
			if($entity->is_paid == 1 && $entity->is_premium == PREMIUM_REPORT){
				$mailHandler = new MailHandler();
				$mailHandler->sendPremiumReportNotification($entity);				
				return Redirect::intended("/sme/premiumsuccess/" . $entity->entityid);
			}

	        /*  Report exists   */
			if ($entity) {
				/** Check if report has pending payment on dragonpay */
				if($entity->is_paid == 0){
					$checkDragonpayPendingPayment = DB::table('tbltransactions')->where('entity_id', $entity->entityid)->where('status',  'P')->first();
					if($checkDragonpayPendingPayment){
						$apiUrl = $this->statusUrl . '/refno/' . $checkDragonpayPendingPayment->refno;
						$client = new GuzzleClient([
							'headers' => [
								'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key)
							]
						]);

						$r = $client->request('GET', $apiUrl);
						$data = json_decode($r->getBody()->getContents());
						if($data->Status == 'P'){
							$settle_date = strtotime($data->SettleDate);
							$settle_date = date('d-m-Y', $settle_date);
							if ($settle_date < date('d-m-Y')){
								//
							}else{
								return View::make('payment.pending', array('refno' => $checkDragonpayPendingPayment->refno));
							}
						}elseif($data->Status == 'S'){ /** Check if report is paid, in case dragonpay over-the-counter payment updated immediately */
							/** Update report Status to paid */
							$entity->is_paid = 1;
							$entity->save();

							/** Update transaction status */
							$this->transactionDb = new Transaction;
							$this->transactionDb->updateTransactionStatus(PD_BASIC_USER_REPORT, $data->Status);

							if($entity->is_premium == PREMIUM_REPORT){
								$mailHandler = new MailHandler();
								$mailHandler->sendPremiumReportNotification($entity);
							}

							return Redirect::to('/dashboard/index');
						}
					}
				}

				/* Set price information */
				$paymentCalculator = new PaymentCalculator;
				$tax = 0;

				$bank = Bank::where('id', '=', $entity->current_bank)->first();
				$locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

				/** Calculate Report Price */
				$reportPrice = $paymentCalculator->paymentComputeAllReportType($reportId);

				if($bank->is_custom != 0) {
					if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
						$price = $bank->custom_price;
					} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
						$price = $bank->custom_price;
					} else { // Standlone Report
						$price = $bank->custom_price;
					}
				} else {
					if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
						$price = $reportPrice['premium'];
	                    $report_type = 'Premium';
					} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
						$price = $reportPrice['simplified'];
	                    $report_type = 'Simplified';
					} else { // Standalone Report
						$price = $reportPrice['standalone'];
	                    $report_type = 'Standalone';
					}
				}

	            if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
	                $report_type = 'Premium';
	            } elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
	                $report_type = 'Simplified';
	            } else { // Standlone Report
	                $report_type = 'Standalone';
	            }

				/* Price and amount are same here since the quantity is 1 in purchasing report.
				But I'll still declare amount variable for using right terms during calculation */
				$amount = $price;
				$subTotal = $amount;
				$displayDiscount = null;
				$discount_value = 0;

	            /*  Check Discount  */
				$discount = Discount::where('discount_code', $entity->discount_code)->first();

				/*  There is a discount */
				if ($discount){
					if ($discount->type == $paymentCalculator::DISCOUNT_TYPE_PERCENTAGE){
						$displayDiscount = '(less: '. $discount->amount .'%)';
						$discount_value = $amount * ($discount->amount / 100);
					} else{
						$displayDiscount = '(less: '. number_format($discount->amount, 2, '.', ',').')';
					}

					/* Get subtotal*/
					$subTotal = $paymentCalculator->computeSubtotal($amount, $discount->amount, $discount->type);

	                /*  Free Trial  */
	                if (0 >= $subTotal) {
	                    $entity_upd  = DB::table('tblentity')
	                        ->where('loginid', Auth::user()->loginid)
	                        ->where('entityid',$entity->entityid)
	                        ->update(
	                            array(
	                                'is_paid' => 1
	                            )
	                        );

	                    $account_upd    = DB::table('tbllogin')
	                        ->where('loginid', Auth::user()->loginid)
	                        ->update(
	                            array(
	                                'status'    => 2
	                            )
	                        );

	                    return Redirect::to('/dashboard/index');
	                }
				}

				// $totalAmount = $paymentCalculator->computeTotalAmountBySubTotal($subTotal);
				$totalAmount = $amount - $discount_value;
				
				/*set up display message*/
				$displayMessage = sprintf("Php %s %s (+ %s%% tax) = Php %s*", number_format($amount,2), ($displayDiscount != null ? $displayDiscount : ''), ($tax * 100), number_format($totalAmount,2));

	            $report_details = [
	                'report_type' => $report_type,
	                'amount' => $amount,
	                'discount' => $discount_value,
	                'total_amount' => $totalAmount,
	                'tax_rate' => '0%',
	                'tax_amount' => ($tax * ($totalAmount))
	                
	            ];

				return View::make('payment.payment_selection', array(
					'display_amount' => 'CreditBPO Rating Report®: ' . $displayMessage,
					'entity_id' => $reportId,
	                'report_details' => $report_details,
	                'user' => $user
				));

			}
	        /*  Report does not exist   */
	        else {
				return Redirect::to('/');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
    
    //-----------------------------------------------------
    //  Function 55.10: showRequirements
    //      function to display sign up confirmation
    //-----------------------------------------------------
    public function showRequirements()
    {
    	try{
        	return View::make('signup_confirmation');
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 55.11: getPaymentOption
	//  Description: function to get payment page selection
    //-----------------------------------------------------
    public function getPaymentOption($code)
	{
		try{
			return View::make('purchase_keys', array('code'=>$code));
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.12: getPaymentOption2
	//  Description: function to process payment
	//  This function was removed because independent key should not be paid based on CDP-943 
    //-----------------------------------------------------
    public function getPaymentOption2($code, $payment_option)
	{
		try{
	        $discount_db    = new Discount;					// icreate instance of Discount
			$email          = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));	// decode email from code
			$batch          = substr($code, 10, 1);			// get batch number from code
			$quantity       = IndependentKey::where('email', $email)->where('batch', (int)$batch)->count();

	        $base_price         = 3500;									// base price
	        $vat_val            = 0.12;									// tax 12%
	        $amount             = $base_price * $quantity;

	        $discount_data = $discount_db->getDiscountByEmail($email);

	        if ($discount_data) {	// check if discount is present

				if ($discount_data->type == 1) {		// percentage discount
					$amount = $amount - ($amount * ($discount_data->amount / 100));
				}
				else if ($discount_data->type == 0) {	// fixed discount
					$amount = $amount - $discount_data->amount;
				}

			}
			
			$vatable_amt = $amount / (1 + $vat_val);
			$vat_output = $amount-$vatable_amt;

			$total_price = $vatable_amt + $vat_output;
			$amount = $total_price;

	        if (0 >= $amount) {
	            IndependentKey::where('email', $email)
	                ->where('batch', (int)$batch)
	                ->update(array('is_paid'=>1));

				// email link to buyer
				if (env("APP_ENV") != "local") {
	                try{
	    				Mail::send('emails.buyer_keys_paid', array(
	    						'hash'=>$code
	    					), function($message){
	    						$message->to($email, $email)
	    							->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

	    						$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
	    						$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
	    				});
	                }catch(Exception $e){
	                //
	                }
				}
				// redirect to certificate download
				return Redirect::to('/certificate/'.$code);
	        }

	        $receipt_db     = new PaymentReceipt;		// create instance

	        $receipt_data['login_id']           = 0;
	        $receipt_data['total_amount']       = $amount;
	        $receipt_data['vatable_amount']     = $vatable_amt;
	        $receipt_data['vat_amount']         = $vat_output;
	        $receipt_data['item_quantity']      = $quantity;

	        $receipt_data['item_type']          = 3;            /*  Bank Keys   */
	        $receipt_data['sent_status']        = STS_NG;
	        $receipt_data['date_sent']          = date('Y-m-d');

			if($payment_option=="paypal"){		/* using paypal */

	            $receipt_data['reference_id']       = 0;
	            $receipt_data['payment_method']     = 1;
	            $receipt_sts                        = $receipt_db->addPaymentReceipt($receipt_data);

				$params = array(
					'cancelUrl' => URL::to('/').'/payment_cancel',
					'returnUrl' => URL::to('/').'/payment_success/'.$code,
					'name'	=> 'CreditBPO Services',
					'description' => 'Credit Risk Rating Basic Package Keys (x'.$quantity.')',
					'amount' => number_format($amount, 2, '.', ''),
					'currency' => 'PHP',
	                'reference_id'  => $receipt_sts
				);

	            $itm_price = $vatable_amt / $quantity;

	            $items  = array(
	                array('name' => 'CreditBPO Services', 'description' => 'Credit Risk Rating Basic Package Keys (x'.$quantity.')', 'price' => $itm_price, 'quantity' => $quantity),
	            );

				Session::put('params', $params);
				Session::save();

				$gateway = Omnipay::create('PayPal_Express');

				if(env("APP_ENV") == "Production"){
					$gateway->setUsername('lia.francisco_api1.gmail.com');
					$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
					$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
					$gateway->setTestMode(false);
				} else {
					$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
					$gateway->setPassword('87DWVXH4UGHD46W3');
					$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
					$gateway->setTestMode(true);
				}
				$response = $gateway->purchase($params)->send();

				/*  PayPal payment sucessful    */
				if ($response->isSuccessful()) {
					print_r($response);
				}
				/*  Redirect to PayPal form     */
				else if ($response->isRedirect()) {
					$response->redirect();
				}
				/*  Error occured during Payment    */
				else {
					echo $response->getMessage();
				}

				// $response->redirect();

			} elseif($payment_option=="dragonpay") {  /* using dragonpay */
				$transaction = new Transaction();
				$transaction->loginid = 0;
				$transaction->amount = $amount;
				$transaction->ccy = "PHP";
				$transaction->description = "Credit Risk Rating Basic Package Keys (x".$quantity.")";
				$transaction->email = $email;
				$transaction->batch = (int)$batch;
				$transaction->save();

	            $receipt_data['reference_id']       = $transaction->id;
	            $receipt_data['payment_method']     = 2;
	            $receipt_sts                        = $receipt_db->addPaymentReceipt($receipt_data);

				$data = array(
					'merchant_id' => $this->merchant_id,
					'txn_id' => $transaction->id,
					'amount' => number_format($transaction->amount, 2, ".", ""),
					'currency' => $transaction->ccy,
					'description' => $transaction->description,
					'email' => $email,
					'key' => $this->merchant_key
				);

				$seed = implode(':', $data);
				$digest = Sha1($seed);

				return Redirect::to(
					$this->url.
					"?merchantid=".$data['merchant_id'].
					"&txnid=".$data['txn_id'].
					"&amount=".$data['amount'].
					"&ccy=".$data['currency'].
					"&description=".urlencode($data['description']).
					"&email=".urlencode($data['email']).
					"&digest=".$digest
				);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}
    
    //-----------------------------------------------------
    //  Function 55.13: getPaymentSuccess
	//  Description: postback page for successful Paypal payment
    //-----------------------------------------------------
	public function getPaymentSuccess($code)
	{
		try{
			$email = base64_decode(substr($code, 0, 10) . substr($code, 11, strlen($code) - 10));
			$batch = substr($code, 10, 1);

			$gateway = Omnipay::create('PayPal_Express');

			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setTestMode(false);
			} else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setTestMode(true);
			}
			$params = Session::get('params');
			$response = $gateway->completePurchase($params)->send();
			$paypalResponse = $response->getData(); // this is the raw response object

			if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
				// Response
				$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

				$payment = new Payment();
	      		$payment->loginid = 0;
	      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
	      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
	      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
	      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
	      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
				$payment->email = $email;
				$payment->batch = (int)$batch;
	      		$payment->save();

				IndependentKey::where('email', $email)->where('batch', (int)$batch)
				->update(array('is_paid'=>1));

				// email link to buyer
				if (env("APP_ENV") != "local") {
	                try{
	    				Mail::send('emails.buyer_keys_paid', array(
	    						'hash'=>$code
	    					), function($message){
	    						$message->to($email, $email)
	    							->subject('Client Serial Keys Payment Confirmation from CreditBPO!');

	    						$message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
	    						$message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
	    				});
	                }catch(Exception $e){
	                //
	                }    
				}

	            /*--------------------------------------------------------------------
	            /*	Email Receipt to Client
	            /*------------------------------------------------------------------*/
	            $receipt    = PaymentReceipt::where('login_id', Auth::user()->loginid)
	                ->where('receipt_id', $params['reference_id'])
	                ->where('sent_status', STS_NG)
	                ->first();

	            $receipt->sent_status = STS_OK;
				$receipt->save();
				
				$base_price = 3500;

				if (env("APP_ENV") != "local") {
					$mailHandler = new MailHandler();
					$mailHandler->sendReceipt(
						Auth::user()->loginid,
						[
							'receipt' => $receipt,
							'subtotal' => $params['amount']
						],
						'CreditBPO Payment Receipt'
					);
				}

				// redirect to certificate download
				return Redirect::to('/certificate/'.$code);
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    public function getClientKeyPriceValue($bank_id,$keyType){
		$bank = Bank::where('id', '=', $bank_id)->first();

		$price = 0;
		if($keyType == "Simplified"){
			$price = $bank->simplified_price;
		}else if($keyType == "Standalone"){
			$price = $bank->standalone_price;
		}else if($keyType == "Premium Zone 1"){
			$price = $bank->premium_zone1_price;
		}else if($keyType == "Premium Zone 2"){
			$price = $bank->premium_zone2_price;
		}else{
			$price = $bank->premium_zone3_price;
		}

		return $price;
	}

    //-----------------------------------------------------
    //  Function 56.14: postPurchaseBankKeys
	//  Description: post process for purchase Client Keys form page
    //-----------------------------------------------------
	public function postPurchaseBankKeys()
	{
		try{
			$rules = array(							// validation rules
		        'quantity' => 'required|numeric|min:1',
				'payment_option' => 'required',
				'agree' => 'required'
			);

			$customMessages = array(
				'agree.required' => 'Please read and agree to the Terms of Use and Privacy Policy, and then check the box below'
			);

	        $quantity_boom = explode('.', Input::get('quantity'));

			$validator = Validator::make(Input::all(), $rules, $customMessages);		// run validation

			if ($validator->fails()) { // if fail
				return Redirect::to('/bank/purchase_keys')->withErrors($validator->errors())->withInput();
			}
	        else if (isset($quantity_boom[1])) { // check for decimal point and return error
	            return Redirect::to('/bank/purchase_keys')->withErrors(array('quantity' => 'Only integers are allowed'))->withInput();
	        }
			else
			{
				// get input items
				$quantity           = Input::get('quantity');
				$discount_code      = Input::get('discount_code');
				$payment_option     = Input::get('payment_option');
				$keyType     = Input::get('keyType');
				Session::put('keyType', $keyType);
				Session::save();

				/* Set price information */
				//JM - paymentcalculator class will be the key to updating the clientkey price or report price
				//I will query in tblbank using loginid and get the setting if the the price per report is activated.
				
				$calculator = $this->paymentCalculator;

				$user = DB::table('tbllogin')
	                    ->where('loginid', $this->accountDetails['loginid'])
						->first();
				
				// if($user->bank_id != 0) { //check if there is an assigned bank to the supervisor
				// 	$bank = Bank::where('id', '=', $user->bank_id)->first();

				// 	if($bank->is_custom != 0) { //check if custom price for reports is on
				// 		$price = $bank->custom_price;
				// 	} else {
				// 		$price = $calculator::CLIENTKEY_PRICE;
				// 	}
				// } else { //default price if there is no assigned bank
				// 	$price = $calculator::CLIENTKEY_PRICE;
				// }

				$price = 0;
				if($user->bank_id != 0) { //check if there is an assigned bank to the supervisor
					$price = Self::getClientKeyPriceValue($user->bank_id,$keyType);
				}
				
				$discount = null;
				$discountType = null;

				if ($discount_code){
					$discountDb = new Discount;
					$discount_data = $discountDb->getDiscountByCode($discount_code);

					if ($discount_data) {										// check if discounted
						$discount = $discount_data->amount;
						$discountType = $discount_data->type;
					} else{
						return Redirect::to('/bank/purchase_keys')->withErrors(array('discount_code' => 'Invalid discount code'))->withInput();
					}
				}
				/* Compute Payment */
				$result = $calculator->paymentCompute($price, $quantity, $discount, $discountType);
				if (0 >= $result['totalAmount']) {		// if amount totals to 0 or free
	                BankSerialKeys::createKeys($this->accountDetails['loginid'], $quantity, $keyType);
					return Redirect::to('/bank/keys');	// directly create keys
				}
				
				/* Create payment details */
				$paymentReceiptDb = new PaymentReceipt;

				$paymentReceipt['price'] = $price;
				$paymentReceipt['amount'] = $result['amount'];
				$paymentReceipt['discount'] = $result['discountAmount'];
				$paymentReceipt['subtotal'] = $result['subTotal'];
				$paymentReceipt['vat_amount'] = $result['taxAmount'];
				$paymentReceipt['total_amount'] = $result['totalAmount'];
				$paymentReceipt['item_quantity'] = $quantity;
				$paymentReceipt['login_id'] = $this->accountDetails['loginid'];
				$paymentReceipt['item_type'] = 3;            /* Client Keys */
				$paymentReceipt['sent_status'] = STS_NG;
				$paymentReceipt['date_sent'] = date('Y-m-d');
				/* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
				$paymentReceipt['vatable_amount'] = null;

				/** Payment process for Paypal */
				if ($payment_option=="paypal") {

	                $paymentReceipt['reference_id'] = 0;
	                $paymentReceipt['payment_method'] = 1; /*PayPal*/
	                $receipt_sts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

					$params = array(
						'cancelUrl' => URL::to('/').'/bank/keys',
						'returnUrl' => URL::to('/').'/payment/paymentsuccess',
						'name'	=> 'CreditBPO Services',
						'description' => 'Credit Risk Rating Basic Package (x'.$quantity.')',
						'amount' => number_format($result['totalAmount'], 2, '.', ''),
						'currency' => 'PHP',
	                    'reference_id'  => $receipt_sts
					);
					
					Session::put('params', $params);
					Session::save();

					$gateway = Omnipay::create('PayPal_Express');

					if(env("APP_ENV") == "Production"){
						$gateway->setUsername('lia.francisco_api1.gmail.com');
						$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
						$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
						$gateway->setTestMode(false);
					} else {
						$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
						$gateway->setPassword('87DWVXH4UGHD46W3');
						$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
						$gateway->setTestMode(true);
					}
					$response = $gateway->purchase($params)->send();
					$response->redirect();

				}
				/* Payment process for DragonPay */
	            else if ($payment_option=="dragonpay") {
					$rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);

					$transaction = new Transaction();
					$transaction->loginid = $this->accountDetails['loginid'];
					$transaction->amount = $result['totalAmount'];
					$transaction->ccy = "PHP";
					$transaction->description = "Credit Risk Rating Basic Package Keys (x".$quantity.")";
					$transaction->txn = $rand;
					$transaction->type = PD_SUPERVISOR_CLIENT_KEY;
					$transaction->save();

					$user = User::find($this->accountDetails['loginid']);
					$user->transactionid = $transaction->id;
					$user->save();

					$paymentReceipt['reference_id'] = $transaction->id;
	                $paymentReceipt['payment_method'] = 2; /*DragonPay*/

					/* Saves data into the database */
					$db_data = new PaymentReceipt;
        
					$db_data->price = $paymentReceipt['price'];
					$db_data->amount = $paymentReceipt['amount'];
					$db_data->discount = $paymentReceipt['discount'];
					$db_data->subtotal = $paymentReceipt['subtotal'];
					$db_data->vat_amount = $paymentReceipt['vat_amount'];
					$db_data->total_amount = $paymentReceipt['total_amount'];
					$db_data->item_quantity = $paymentReceipt['item_quantity'];
					$db_data->vatable_amount = $paymentReceipt['vatable_amount'];
					$db_data->login_id = $paymentReceipt['login_id'];
					$db_data->reference_id = $paymentReceipt['reference_id'];
					$db_data->payment_method = $paymentReceipt['payment_method'];
					$db_data->item_type = $paymentReceipt['item_type'];
					$db_data->sent_status = $paymentReceipt['sent_status'];
					$db_data->date_sent = $paymentReceipt['date_sent'];
					$db_data->keyType = Session::get('keyType');
					$db_data->save();

					$data = array(
						'merchant_id' => $this->merchant_id,
						'txn_id' => $transaction->id .$rand,
						'amount' => number_format($transaction->amount, 2, ".", ""),
						'currency' => $transaction->ccy,
						'description' => $transaction->description,
						'email' => $this->accountDetails['email'],
						'key' => $this->merchant_key
					);

					$seed = implode(':', $data);
					$digest = Sha1($seed);

					/*  Send data to DragonPay  */
					$apiUrl = $this->statusUrl . '/' . $data['txn_id'] . '/post';
					$client = new GuzzleClient([
						'headers' => [
							'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key),
							'Content-Type'	=> 'application/json'
						]
					]);

					$postInput = [
						'Amount'		=> $data['amount'],
						'Currency'		=> $data['currency'],
						'Description'	=> $data['description'],
						'Email'			=> $data['email']
					];

					$result = $client->request('POST', $apiUrl, [ 'body' => json_encode($postInput) ]);
					$data = json_decode($result->getBody()->getContents());

					return Redirect::to($data->Url);

					// return Redirect::to(
					// 	$this->url.
					// 	"?merchantid=".$data['merchant_id'].
					// 	"&txnid=".$data['txn_id'].
					// 	"&amount=".$data['amount'].
					// 	"&ccy=".$data['currency'].
					// 	"&description=".urlencode($data['description']).
					// 	"&email=".urlencode($data['email']).
					// 	"&digest=".$digest
					// );
				}
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
	}

    //-----------------------------------------------------
    //  Function 55.15: postBankPayViaPaypal
	//  Description: function to process subsciption payment via PayPal
    //-----------------------------------------------------
	public function postBankPayViaPaypal()
	{
		try{
	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']                         = 'SetExpressCheckout';
			$paypal_pdata['VERSION']                        = '86';
			$paypal_pdata['LANDINGPAGE']           			= 'Billing';
	        $paypal_pdata['L_BILLINGTYPE0']                 = 'RecurringPayments';
	        $paypal_pdata['L_BILLINGAGREEMENTDESCRIPTION0'] = 'Supervisor Account Subscription';
	        $paypal_pdata['cancelUrl']                      = URL::to('/').'/bank/dashboard';
	        $paypal_pdata['returnUrl']                      = URL::to('/').'/bank_subscribe/recurring_profile';

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl, CURLOPT_URL, $paypal_url);
	        curl_setopt($curl, CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
	        $result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);
	        $resp_exp   = explode('&', $result);
	        $resp_exp   = explode('=', $resp_exp[0]);

	        $response['token']   = $resp_exp[1];

	        if (env("APP_ENV") == "Production") {
	            return Redirect::To('https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response['token']);
	        }
	        else {
	            return Redirect::To('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response['token']);
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.16: postRecurringProfileViaPaypal
	//  Description: post process recurring subsciption payment via PayPal
    //-----------------------------------------------------
    public function postRecurringProfileViaPaypal()
	{
		try{
	        $calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;	// subscription cost
			$calculationResult = $calculator->paymentCompute($price, 1);

	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']                = 'GetExpressCheckoutDetails';
	        $paypal_pdata['VERSION']               = '86';
	        $paypal_pdata['TOKEN']                 = Input::get('paypal_token');

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl,CURLOPT_URL, $paypal_url);
	        curl_setopt($curl,CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl,CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
			$result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);

	        $resp_exp   = explode('&', $result);
	        $resp_exp   = explode('=', $resp_exp[9]);
			$response['payer_id']   = $resp_exp[1];

	        if(env("APP_ENV") == "Production"){
	            $paypal_url                                     = 'https://api-3t.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco_api1.gmail.com';
				$paypal_pdata['PWD']                            = 'Z8FEJ5G84FBQNKDJ';
				$paypal_pdata['SIGNATURE']                      = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
			} else {
	            $paypal_url                                     = 'https://api-3t.sandbox.paypal.com/nvp';
				$paypal_pdata['USER']                           = 'lia.francisco-facilitator_api1.gmail.com';
				$paypal_pdata['PWD']                            = '87DWVXH4UGHD46W3';
				$paypal_pdata['SIGNATURE']                      = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
			}
			$paypal_pdata['METHOD']             = 'CreateRecurringPaymentsProfile';
	        $paypal_pdata['VERSION']            = '86';
	        $paypal_pdata['TOKEN']              = Input::get('paypal_token');
	        $paypal_pdata['PAYERID']            = $response['payer_id'];
	        $paypal_pdata['PROFILESTARTDATE']   = date('Y-m-d', strtotime("+15 days")).'T'.date('H:i:s').'Z';
			$paypal_pdata['DESC']               = 'Supervisor Account Subscription';
			$paypal_pdata['LANDINGPAGE']        = 'Billing';
	        $paypal_pdata['BILLINGPERIOD']      = 'Month';
	        $paypal_pdata['BILLINGFREQUENCY']   = '1';
	        $paypal_pdata['AMT']                = '1';
	        $paypal_pdata['CURRENCYCODE']       = 'PHP';
	        $paypal_pdata['COUNTRYCODE']        = 'PH';
	        $paypal_pdata['MAXFAILEDPAYMENTS']  = '2';
	        $paypal_pdata['AUTOBILLOUTAMT']     = 'AddToNextBilling';
	        $paypal_pdata['AMT']                = $calculationResult['totalAmount'];
	        //$paypal_pdata['TAXAMT']             = $vat_output;

	        $subscr_exist   = BankSubscription::where('bank_id', Auth::user()->loginid)->first();
	        if ($subscr_exist) {
	            $paypal_pdata['INITAMT']  = $calculationResult['totalAmount'];
	        }

	        /*--------------------------------------------------------------------
	        /*	Send POST request to Highcharts export Server via CuRL
	        /*------------------------------------------------------------------*/
	        /* Initializes the CuRL Library */
	        $curl = curl_init();

	        /* Sets the CuRL POST Request configuration  */
	        curl_setopt($curl, CURLOPT_URL, $paypal_url);
	        curl_setopt($curl, CURLOPT_POST, sizeof($paypal_pdata));
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($paypal_pdata));
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        /* Execute CuRL Request */
			$profile_result = curl_exec($curl);

	        /*  Closes the CuRL Library */
	        curl_close($curl);

	        $resp_explode   = explode('&', $profile_result);

	        foreach ($resp_explode as $exp) {
				$exp_pair   = explode('=', $exp);
				if(@count($exp_pair) > 1)
	            $success_response[$exp_pair[0]]  = urldecode($exp_pair[1]);
			}

			/* succesful payment */
	        if ($success_response['ACK'] == 'Success') {
				$payment                    = new Payment();
	      		$payment->loginid           = Auth::user()->loginid;
	      		$payment->transactionid     = $success_response['PROFILEID'];
	      		$payment->transactiontype   = 'expresscheckout';
	      		$payment->paymenttype       = 'RecurringPayments';
	      		$payment->paymentamount     = $calculationResult['totalAmount'];
	      		$payment->currencycode      = 'PHP';
	      		$payment->save();

				$payment_id                 = $payment->id;
	            $subs = BankSubscription::firstOrCreate(array(
	                'bank_id' => Auth::user()->loginid
	            ));

	            $date = new DateTime();
	            $date->modify("+1 month");
				$subs->expiry_date = $date->format('Y-m-d 23:59:59');
				$subs->status = 1;
	            $subs->save();

	            $bank_user = User::find(Auth::user()->loginid);

	            if (3 == $bank_user->product_plan) {
	                $bank_user->product_plan    = 1;
	                $bank_user->save();
	            }

	            if ($subscr_exist) {
					/*  Create a payment receipt record on the database */
					$paymentReceiptDb = new PaymentReceipt;

					$paymentReceipt['price'] = $price;
					$paymentReceipt['amount'] = $calculationResult['amount'] + $calculationResult['taxAmount'];
					$paymentReceipt['discount'] = $calculationResult['discountAmount'];
					$paymentReceipt['subtotal'] = $calculationResult['subTotal'];
					$paymentReceipt['vat_amount'] = $calculationResult['taxAmount'];
					$paymentReceipt['total_amount'] = $calculationResult['totalAmount'];
					$paymentReceipt['item_quantity'] = 1;
					$paymentReceipt['login_id'] = Auth::user()->loginid;
					$paymentReceipt['reference_id'] = $payment_id;
					$paymentReceipt['payment_method'] = 1; /*PayPal*/
					$paymentReceipt['item_type'] = 2; /* Bank Subscription */
					$paymentReceipt['sent_status'] = STS_OK;
					$paymentReceipt['date_sent'] = date('Y-m-d');
					/* The current payment process/computation was changed but due to old datas, vatable amount field will not remove*/
					$paymentReceipt['vatable_amount'] = null;
					$paymentReceiptSts = $paymentReceiptDb->addPaymentReceipt($paymentReceipt);

	                $receipt = PaymentReceipt::where('receipt_id', $paymentReceiptSts)->first();

					$user = User::find(Auth::user()->loginid);

					if($user->parent_user_id != 0) {
						$user = User::find($user->parent_user_id);
					}

					if (env("APP_ENV") != "local") {
						$mailHandler = new MailHandler();
						$mailHandler->sendReceipt(
							Auth::user()->loginid,
							[
								'receipt' => $receipt,
								'user' => $user
							],
							'CreditBPO Payment Receipt'
						);
					}
	            }
	            return Redirect::to('/bank/dashboard');
	        }
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }

    //-----------------------------------------------------
    //  Function 55.17: postBankPayViaDragonPay
	//  Description: post process subsciption payment via DragonPay
    //-----------------------------------------------------
	public function postBankPayViaDragonPay()
	{
		try{
			$calculator = $this->paymentCalculator;
			$price = $calculator::BANKSUBSCRIPTION_PRICE;	// subscription cost
			$calculationResult = $calculator->paymentCompute($price, 1);

			$rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);

			$transaction = new Transaction();					// create instance of transaction and save
			$transaction->loginid = Auth::user()->loginid;
			$transaction->amount = $calculationResult['totalAmount'];
			$transaction->ccy = "PHP";
			$transaction->description = "Bank User Subscription";
			$transaction->txn = $rand;
			$transaction->type = PD_SUPERVISOR_SUBSCRIPTION;
			$transaction->save();

			$user = User::find(Auth::user()->loginid);
			$user->transactionid = $transaction->id;
			$user->save();

			$data = array(											// values passed as parameters to dragon pay
				'merchant_id'   => $this->merchant_id,
				'txn_id'        => $transaction->id . $rand,
				'amount'        => number_format($calculationResult['totalAmount'], 2, ".", ""),
				'currency'      => $transaction->ccy,
				'description'   => $transaction->description,
				'email'         => Auth::user()->email,
				'key'           => $this->merchant_key
			);

			$seed = implode(':', $data);
			$digest = Sha1($seed);

	        if(env("APP_ENV") == "Production"){
				$recur_url = 'https://gw.dragonpay.ph/RecurPay.aspx';
			} else {
				$recur_url = 'http://test.dragonpay.ph/RecurPay.aspx';
			}

			/*  Create a payment receipt record on the database */
			$paymentReceiptDb = new PaymentReceipt;

			$paymentReceipt['price'] = $price;
			$paymentReceipt['amount'] = $calculationResult['amount'] + $calculationResult['taxAmount'];
			$paymentReceipt['discount'] = $calculationResult['discountAmount'];
			$paymentReceipt['subtotal'] = $calculationResult['subTotal'];
			$paymentReceipt['vat_amount'] = $calculationResult['taxAmount'];
			$paymentReceipt['total_amount'] = $calculationResult['totalAmount'];
			$paymentReceipt['item_quantity'] = 1;
			$paymentReceipt['login_id'] = Auth::user()->loginid;
			$paymentReceipt['reference_id'] = $transaction->id;
			$paymentReceipt['payment_method'] = 2; /*DragonPay*/
			$paymentReceipt['item_type'] = 2; /* Bank Subscription */
			$paymentReceipt['sent_status'] = STS_NG;
			$paymentReceipt['date_sent'] = date('Y-m-d');
			$paymentReceipt['vatable_amount'] = null;
			$paymentReceiptDb->addPaymentReceipt($paymentReceipt);

			// return Redirect::to(
			// 	$this->url.
			// 	"?merchantid=".$data['merchant_id'].
			// 	"&txnid=".$data['txn_id'].
			// 	"&amount=".$data['amount'].
			// 	"&ccy=".$data['currency'].
			// 	"&description=".urlencode($data['description']).
			// 	"&email=". $data['email'] .
			// 	"&digest=".$digest
			// );

			/*  Send data to DragonPay  */
			$apiUrl = $this->statusUrl . '/' . $data['txn_id'] . '/post';
			$client = new GuzzleClient([
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key),
					'Content-Type'	=> 'application/json'
				]
			]);

			$postInput = [
				'Amount'		=> $data['amount'],
				'Currency'		=> $data['currency'],
				'Description'	=> $data['description'],
				'Email'			=> $data['email']
			];

			$result = $client->request('POST', $apiUrl, [ 'body' => json_encode($postInput) ]);
			$data = json_decode($result->getBody()->getContents());

			return Redirect::to($data->Url);
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
    
    //-----------------------------------------------------
    //  Function 55.18: getBankSubscriptionPaymentSuccess
	//  Description: function for Paypal success payment
    //-----------------------------------------------------
	public function getBankSubscriptionPaymentSuccess()
	{
		try{
			$gateway = Omnipay::create('PayPal_Express');

			// set variables
			if(env("APP_ENV") == "Production"){
				$gateway->setUsername('lia.francisco_api1.gmail.com');
				$gateway->setPassword('Z8FEJ5G84FBQNKDJ');
				$gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs');
				$gateway->setTestMode(false);
			} else {
				$gateway->setUsername('lia.francisco-facilitator_api1.gmail.com');
				$gateway->setPassword('87DWVXH4UGHD46W3');
				$gateway->setSignature('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR');
				$gateway->setTestMode(true);
			}
			$params = Session::get('params');
			$response = $gateway->completePurchase($params)->send();
			$paypalResponse = $response->getData(); // this is the raw response object

			if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
				// Response
				$payment_check = Payment::where('transactionid', $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'])->count();

				$payment = new Payment();
	      		$payment->loginid = Auth::user()->loginid;
	      		$payment->transactionid = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
	      		$payment->transactiontype = $paypalResponse['PAYMENTINFO_0_TRANSACTIONTYPE'];
	      		$payment->paymenttype = $paypalResponse['PAYMENTINFO_0_PAYMENTTYPE'];
	      		$payment->paymentamount = $paypalResponse['PAYMENTINFO_0_AMT'];
	      		$payment->currencycode = $paypalResponse['PAYMENTINFO_0_CURRENCYCODE'];
	      		$payment->save();

				//create subscription
				if ($payment_check == 0) {
					$subs = BankSubscription::firstOrCreate(array(
						'bank_id' => Auth::user()->loginid
					));

					$date = new DateTime();
					$start_day = $date->format('j');

					$date->modify("+1 month");
					$end_day = $date->format('j');

					if ($start_day != $end_day)
						$date->modify('last day of last month');

					$subs->expiry_date = $date->format('Y-m-d 23:59:59');
					$subs->save();

	                $bank_user = User::find(Auth::user()->loginid);

	                if (3 == $bank_user->product_plan) {
	                    $bank_user->product_plan    = 1;
	                    $bank_user->save();
	                }
				}
				return Redirect::to('/bank/dashboard');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}	
	}

}
