<?php

//======================================================================
//  Class 27: Machine and equipments Controller
//      Functions related to machine and equipments on the Questionnaire
//      are routed to this class
//======================================================================
class MachineEquipmentController extends BaseController
{    
    //-----------------------------------------------------
    //  Function 27.1: getMachineEquipment
    //      Displays the Machine and Equipment Form
    //-----------------------------------------------------
	public function getMachineEquipment($entity_id)
	{
		return View::make('sme.machineEquipment',
            array(
                'entity_id' => $entity_id
            )
        );
	}

    //-----------------------------------------------------
    //  Function 27.2: postMachineEquipment
    //  
    //  Validates and saves the data to the database
    //  when Machine and Equipments "Add" form is submitted
    //
    //  The same function is responsible for the API
    //  "Add / Edit Machine and equipment"
    //-----------------------------------------------------
	public function postMachineEquipment($entity_id)
	{
        /*  Variable Declaration    */
        $machineEquipName  = Input::get('machine_equip_name'); // Insurance Name on the Form
        $messages   = array();                      // Validation Messages according to rules
        $rules      = array();                      // Validation Rules
        
        $security_lib   = new MaliciousAttemptLib;  // Instance of the Malicious Attempt Library
        $sts            = STS_NG;

        $entity = Entity::find($entity_id);
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;       // Server Process Status for Output
        $srv_resp['messages']       = STR_EMPTY;    // Server Process Message for Output
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;
            $srv_resp['refresh_cntr']   = '#machine-equip-cntr';
            $srv_resp['refresh_elems']  = '.reload-machine-equip';

            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);
            
        }
        
        /*  Checks if the User is allowed to update the company */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /*  User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
        for ($idx = 0; $idx < @count($machineEquipName); $idx++) {
            
            /*  Validates input set with data on Insurance Type
            *   Also, always validate the first Form input set
            */
            if ((STR_EMPTY != $machineEquipName[$idx])
            || (0 == $idx)) {

                $messages['machine_equip_name.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details2.machine_equip_name')));
                $messages['machine_equip_serial.'.$idx.'.required']	= trans('validation.required', array('attribute'=>trans('business_details2.machine_equip_serial')));
                $messages['machine_equip_serial.'.$idx.'.unique']	= trans('validation.unique', array('attribute'=>trans('business_details2.machine_equip_serial')));
            
                $rules['machine_equip_name.'.$idx]	= 'required';
                $rules['machine_equip_serial.'.$idx]	= 'required|unique:machine_and_equipments,serial_key,NULL,id,is_deleted,0';
            }
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);

        /*  Input validation failed     */
		if($validator->fails()) {
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else
		{
            /*  Stores all inputs from the form to an array for saving and looping  */
			$arraydata = Input::only('machine_equip_name', 'machine_equip_serial');
            
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Stores data into an array for loop traversal    */
			$machine_equip_name = $arraydata['machine_equip_name'];
            $machine_equip_serial = $arraydata['machine_equip_serial'];

            /*  Saves Input to the database     */
			foreach ($machine_equip_name as $key => $value) {
				if($value){
					$data = array(
					    'entity_id'=>$entity_id, 
						'name'=>$machine_equip_name[$key],
						'serial_key'=>$machine_equip_serial[$key],
						'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s')
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('machine_and_equipments')
                            ->where('id', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('machine_and_equipments')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
			
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Logs the action to the database */
        ActivityLog::saveLog();
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 27.3: getMachineEquipmentDelete
    //      HTTP GET request to delete specified Machine and Equipment
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getMachineEquipmentDelete($id)
	{

        $insurance = MachineEquipment::where('id', '=', $id)
                                ->where('entity_id', '=', Session::get('entity')->entityid)
                                ->first();

        $insurance->is_deleted = 1;
        $insurance->save();

        
        /*  Logs the action to the database */
		ActivityLog::saveLog();
        
        /*  Adds a one-off session variable for redirection */
		Session::flash('redirect_tab', 'tab1');
        
        $entity = Entity::find(Session::get('entity')->entityid);

        return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
