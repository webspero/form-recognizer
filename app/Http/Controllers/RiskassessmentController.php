<?php

//======================================================================
//  Class 49: Risk Assessment Controller
//      Functions related to Risk Assessment on the Questionnaire
//      are routed to this class
//======================================================================
class RiskassessmentController extends BaseController {

	//-----------------------------------------------------
    //  Function 49.1: getRiskassessment
    //      Displays the Risk Assessment Form
    //-----------------------------------------------------
	public function getRiskassessment($entity_id)
	{
        /*  Count number of existing Risk Assessment for the specified Report   */
        $ra_count = DB::table('tblriskassessment')
			->where('entity_id', '=', $entity_id)
			->count();
        
        if ($ra_count >= 1) {
            $ra_field = 2 - ($ra_count - 1);
        }
        else {
            $ra_field = 3;
        }
        
		return View::make('sme.riskassessment',
            array(
                'ra_field'  => $ra_field,
                'entity_id' => $entity_id
            )
        );
	}
    
    //-----------------------------------------------------
    //  Function 49.2: postRiskassessment
    //      HTTP Post request to validate and save Risk
    //      Assessment data in the database
    //-----------------------------------------------------
	public function postRiskassessment($entity_id)
	{
        /*  Variable Declaration    */
        $security_lib   = new MaliciousAttemptLib;
        $sts            = STS_NG;
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;
        
        /*  Request did not come from API set the server response   */
        if ((!Input::has('action')) && (!Input::has('section'))) {
            $srv_resp['action']         = REFRESH_CNTR_ACT;                             // Container element where changed data are located
            $srv_resp['refresh_cntr']   = '#ra-list-cntr';                              // Refresh the specified container
            $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.$entity_id);    // URL to fetch data from
            $srv_resp['refresh_elems']  = '.reload-ra';                                 // Elements that will be refreshed
        }
        
        /*  Request did not come from API set the server response   */
        $sts            = $security_lib->checkUpdatePermission($entity_id);
        
        /** User not allowed to edit Company    */
        if (STS_NG == $sts) {
            $srv_resp['messages'][0]    = 'You are not allowed to edit this entity.';
        }
        
        /*  Fetch the inputs from the form  */
        $input_data             = Input::except('_token');
        
        $risk_assessment_name   = $input_data['risk_assessment_name'];
        $risk_assessment_sol    = $input_data['risk_assessment_solution'];
        
        $risk_assessment_count  = @count($risk_assessment_name);
        
        /*  Sets the validation rules and messages according to received Form Inputs    */
		$rules = array(
	        'risk_assessment_name.0'        => 'required',
	        'risk_assessment_solution.0'	=> 'required',
	   	);
        
        $messages    = array(
            'risk_assessment_name.0.required'       => 'Please choose a Threat',
            'risk_assessment_solution.0.required'   => 'Please Choose a Counter Measure'
        );
        
        /*  Sets validation rules and messages for 2nd and more sets    */
        for ($idx = 1; $idx < $risk_assessment_count; $idx++) {
            $rules['risk_assessment_solution.'.$idx]    = 'required_with:risk_assessment_name.'.$idx;
            
            $messages['risk_assessment_solution.'.$idx.'.required_with']    = 'Please Choose a Counter Measure';
        }
        
        /*  Run the Laravel Validation  */
		$validator = Validator::make(Input::all(), $rules, $messages);
        
        /*  Input validation failed     */
		if ($validator->fails())	{
			$srv_resp['messages']	= $validator->messages()->all();
		}
        /*  Input validation success    */
		else {
            /*  Check if there is a Master Key
            *   Master key input is used for API Editing
            */
            if (Input::has('master_key')) {
                $primary_id     = Input::get('master_key');
            }
            
            /*  Saves Input to the database     */
			foreach ($risk_assessment_name as $key => $value) {
				if ($value) {
					$data = array(
					    'entity_id'                 => $entity_id, 
					   	'risk_assessment_name'      => $risk_assessment_name[$key],
					   	'risk_assessment_solution'  => $risk_assessment_sol[$key],
					   	'created_at'                => date('Y-m-d H:i:s'),
				    	'updated_at'                => date('Y-m-d H:i:s')
					);
                    
                    /*  When an edit request comes from the API */
                    if ((Input::has('action')) && ('edit' == Input::get('action')) && (isset($primary_id[$key]))) {
                        
                        DB::table('tblriskassessment')
                            ->where('riskassessmentid', $primary_id[$key])
                            ->update($data);
                        
                        $master_ids[]           = $primary_id[$key];
                        $srv_resp['messages']   = 'Successfully Updated Record';
                    }
                    /*  Request came from Web Application via browser   */
                    else {
                        $master_ids[]           = DB::table('tblriskassessment')->insertGetId($data);
                        $srv_resp['messages']   = 'Successfully Added Record';
                    }
				}
			}
            
            /*  Sets request status to success  */
            $srv_resp['sts']        = STS_OK;
            
            /*  Sets the master id for output when request came from API    */
            if ((Input::has('action')) && (Input::has('section'))) {
                $srv_resp['master_ids'] = $master_ids;
            }
		}
        
        /*  Encode server response variable to JSON for output  */
        return json_encode($srv_resp);
	}
    
    //-----------------------------------------------------
    //  Function 49.3: getRiskassessmentDelete
    //      HTTP GET request to delete specified Risk Assessment
    //          (Currently not in used)
    //-----------------------------------------------------
    public function getRiskassessmentDelete($id)
	{
        /*  Queries the Database for the specified Risk Assessment and delete it  */
		// DB::table('tblriskassessment')
        //     ->where('riskassessmentid', $id)
        //     ->where('entity_id', Session::get('entity')->entityid)
        //     ->delete();
        
        $risk = Riskassessment::where('riskassessmentid', '=', $id)
                ->where('entity_id', '=', Session::get('entity')->entityid)
                ->first();
        $risk->is_deleted = 1;
        $risk->save();

        /*  Adds a one-off session variable for redirection */
		Session::flash('redirect_tab', 'tab5');
        
		return Redirect::to('/summary/smesummary/'.Session::get('entity')->entityid);
	}

}
