<?php

use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Http\Request;

class VisionController extends BaseController
{
    public function displayForm(){
        return view('annotate');
    }

    public function imagickc(Request $request) {
        header("Content-Type: image/jpg");
        // echo $convertedImage;
    }

    // public function boxText(Request $request)
    // {
    //     $imageName = time().'.'.$request->$images->getClientOriginalExtension();
    //     $request->image->move(public_path('/documents/vision'), $imageName);
    //     $image = public_path('/documents/vision/'.$imageName);
    //     $imagick = new Imagick($image);
    //     $imagick->scaleImage(1024, 0);
    //     $imagick->sharpenimage(20,10);
    //     $imagick->enhanceImage();
    //     $imagick->setImageType(2);
    //     $convertedImage = $imagick->getImageBlob();
    //     file_put_contents($image, $imagick);

    //     $cloud = new ServiceBuilder([
    //         'keyFilePath' => public_path('visionAPI.json'),
    //         'projectId' => 'sixth-edition-253508'
    //     ]);

    //     $vision = $cloud->vision();

    //     $output = imagecreatefromjpeg($image);
    //     $image = $vision->image(file_get_contents($image), ['text']);
    //     $results = $vision->annotate($image);
        
    //     foreach ($results->text() as $face) {
    //         $vertices = $face->boundingPoly()['vertices'];
                
    //             $x1 = $vertices[0]['x'];
    //             $y1 = $vertices[0]['y'];
    //             $x2 = $vertices[2]['x'];
    //             $y2 = $vertices[2]['y'];

    //             imagerectangle($output, $x1, $y1, $x2, $y2, 0x00ff00);
    //     }

    //     // return View::make('results', array('results' => $results));

    //     header('Content-Type: image/jpeg');

    //     imagejpeg($output);
    //     imagedestroy($output);
    // }

    public function detectText(Request $request)
    {
        $dateBalance = [];
        $datesCount = 0;
        $balanceCount = 0;
        foreach($request->images as $img){

        $imageName = time().'.'.$img->getClientOriginalExtension();
        $img->move(public_path('/documents/vision'), $imageName);
        $image = public_path('/documents/vision/'.$imageName);

        $imagick = new Imagick($image);
        $imagick->sharpenimage(20,10);
        $imagick->getImageBlob();

        file_put_contents($image, $imagick);

        $cloud = new ServiceBuilder([
            'keyFilePath' => public_path('visionAPI.json'),
            'projectId' => 'sixth-edition-253508'
        ]);

        $vision = $cloud->vision();

        $output = imagecreatefromjpeg($image);
        $image = $vision->image(file_get_contents($image), ['text']);
        $textAnnotations = $vision->annotate($image)->text();

        // Remove first row (full text)
        array_shift($textAnnotations);
        
        /* --------------------------
        ----get Dates Column start--
        ---------------------------*/
        $dates = [];
        foreach($textAnnotations as $key => $text) {
            if($text->info()['description'] === "DATE"){
                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 30;
                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 25;
            } else {
                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
            }
            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
            $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
            $value = ["x1" => $x1, 
                        "x2" => $x2, 
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()];
            $dates[] = $value;
        }
        $isDateColumn = STS_NG;
        $dateKey = 0;
        foreach($dates as $key => $date){
            $isRotatedDiff = abs($date['x2'] - $date['x1']);
            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;
            if($isDateColumn == STS_NG){
                if ($date['text'] == "DATE"){
                    $isDateColumn = STS_OK;
                    $dateKey = $key;
                } else {
                    unset($dates[$key]);
                    continue;
                }
            }
            // $numberDate = preg_replace('/([^0-9])/i', '', $dates[$dateKey]['text']);
            if (($date['x1'] < $dates[$dateKey]['x1'] || $date['x2'] > $dates[$dateKey]['x2'])){
                unset($dates[$key]);
                continue;
            } else if ( $date['y1'] > $dates[$dateKey]['y1'] && $date['x2'] > $dates[$dateKey]['x2']) {
                unset($dates[$key]);
                continue;
            } else if ($isRotated) {
                unset($dates[$key]);
                continue;
            } else if (strstr($date['text'], '/')) {
                $formatDate = str_replace("/", "", $date['text']);
                if(!is_numeric($formatDate)){
                    unset($dates[$key]);
                }
                continue;
            }

            if(!is_numeric($date['text'])) {
                if($date['text'] == "DATE") {
                    continue;
                }
                unset($dates[$key]);
            }
            
        }
        array_shift($dates);
        $merged = STS_NG;
        $diff = 0;
        foreach($dates as $key => $date){
            if($key != array_key_last($dates) && $merged == STS_NG){
                $diff = abs($dates[$key+1]['y1'] - $dates[$key]['y1']);
                if($diff <= 5){
                    $month = preg_replace('/([^0-9])/i', '', $dates[$key]['text']);
                    $day = preg_replace('/([^0-9])/i', '', $dates[$key+1]['text']);
                    $value = $dates[$key]['text'].$dates[$key+1]['text'];
                    $merged = STS_OK;
                } else {
                    $fullDate = str_split(preg_replace('/([^0-9])/i', '', $dates[$key]['text']), 2);
                    $month = $fullDate[0];
                    $day = $fullDate[1];
                    $value = $dates[$key]['text'];
                }
                $dateBalance[$datesCount]['date'] = $value;
                $dateBalance[$datesCount]['month'] = $month;
                $dateBalance[$datesCount]['day'] = $day;
                $datesCount++;
            } else {
                $merged = STS_NG;
            }
        }
        /* --------------------------
        ----get Dates Column end-----
        ---------------------------*/

        //RECTANGLE TEST
        // $isBalanceColumn = STS_NG;
        // foreach($textAnnotations as $key => $text) {
        //     if($text->info()['description'] === "BALANCE") {
        //         if($textAnnotations[$key+1]->description() != "LAST"){
        //             $isBalanceColumn = STS_OK;
        //         }
        //     }
        //     if($isBalanceColumn == STS_OK){
        //         $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 95;
        //         $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 180;
        //         $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
        //         $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
        //         break;
        //     }
        // }

        // imagerectangle($output, $x1, $y1, $x2, $y2, 0x00ff00);
        // header('Content-Type: image/jpeg');

        // imagejpeg($output);
        // imagedestroy($output);
        //RECTANGLE TEST

        
        /* --------------------------
        ---get banlance Column start--
        ---------------------------*/
        $isBalanceColumn = STS_NG;
        $isBalanceLastStatement = STS_NG;
        $balances = [];
        foreach($textAnnotations as $key => $text) {
            if($text->info()['description'] === "BALANCE" && $textAnnotations[$key+1]->description() == "LAST") {
                $isBalanceLastStatement = STS_OK;
            }
            if($text->info()['description'] === "BALANCE") {
                if($textAnnotations[$key+1]->description() != "LAST" || $textAnnotations[$key+1]->description() != "THIS"){
                    $isBalanceColumn = STS_OK;
                }
            }
            if($isBalanceColumn == STS_OK){
                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 95;
                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 150;
                $isBalanceColumn = STS_NG;
            } else {
                $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
            }
            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
            $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
            $value = ["x1" => $x1, 
                        "x2" => $x2, 
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()];
            $balances[] = $value;
        }
        $isBalanceColumn = STS_NG;
        $balanceKey = 0;

        foreach($balances as $key => $balance){
            $isRotatedDiff = abs($balance['x2'] - $balance['x1']);
            $isRotated = $isRotatedDiff <= 1 ? 1 : 0;
            if($isBalanceColumn == STS_NG){
                if ($balance['text'] == "BALANCE"){
                    $isBalanceColumn = STS_OK;
                    $balanceKey = $key;
                } else {
                    unset($balances[$key]);
                    continue;
                }
            }
            if (($balance['x1'] < $balances[$balanceKey]['x1'] || $balance['x2'] > $balances[$balanceKey]['x2'])){
                unset($balances[$key]);
                continue;
            } else if ( $balance['y1'] > $balances[$balanceKey]['y1'] && $balance['x2'] > $balances[$balanceKey]['x2']) {
                unset($balances[$key]);
                continue;
            } else if ($isRotated) {
                unset($balances[$key]);
                continue;
            } else if (strstr($balance['text'], ',') || strstr($balance['text'], '.')) {
                $formatBalance = str_replace(array(".",","), "", $balance['text']);
                if(!is_numeric($formatBalance)){
                    unset($balances[$key]);
                }
                continue;
            }

            if(!is_numeric($balance['text']) || is_numeric($balance['text'])) {
                if($balance['text'] == "BALANCE") {
                    continue;
                }
                unset($balances[$key]);
            }
        }

        array_shift($balances);
        $merged = STS_NG;
        foreach($balances as $key => $balance){
            if($key != array_key_last($balances) && $merged == STS_NG){
                $diff = abs($balances[$key+1]['y1'] - $balances[$key]['y1']);
                if($diff <= 5){
                    $value = $balances[$key]['text'].$balances[$key+1]['text'];
                    $merged = STS_OK;
                } else {
                    $value = $balances[$key]['text'];
                }

                if ($isBalanceLastStatement == STS_OK) {
                    $isBalanceLastStatement = STS_NG;
                } else {
                    $dateBalance[$balanceCount]['balance'] = $this->convertAmount($value);
                    $balanceCount++;
                }
            } else {
                $dateBalance[$balanceCount]['balance'] = $this->convertAmount($balances[$key]['text']);
                $merged = STS_NG;
                $balanceCount++;
            }
        }
        /* --------------------------
        ---get banlance Column end---
        ---------------------------*/
        }
        // remove if date is not detected correctly
        foreach($dateBalance as $key => $balance){
            if($balance['month'] > 12 || $balance['day'] > 31) {
                unset($dateBalance[$key]);
            }
        }

        $dateBalance = array_values($dateBalance);

        $dayCount = 0;
        $totalBalance = 0;
        foreach($dateBalance as $key => $balance){
            $date = preg_replace('/([^0-9])/i', '', $balance['date']);
            if($key != array_key_last($dateBalance)){
                $nextDate = preg_replace('/([^0-9])/i', '', $dateBalance[$key+1]['date']);
            } else {
                $totalBalance += $balance['balance'];
                $dayCount++;
            }
            if($date != $nextDate) {
                $totalBalance += $balance['balance'];
                $dayCount++;
            }
        }

        $averageBalance = $totalBalance / $dayCount;
        
        return View::make('results', array('dateBalances' => $dateBalance,
                                                'averageBalance' => $averageBalance));
    }

    public function convertAmount($money)
    {
    $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
    $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

    $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

    $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
    $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

    return (float) str_replace(',', '.', $removedThousandSeparator);
    }

    
}