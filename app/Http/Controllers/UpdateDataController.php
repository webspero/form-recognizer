
<?php

use App\Countries;
use SignupController as Signup;
use \Exception as Exception;
//======================================================================
//  Class 53: Update Data Controller
//      Server scripts for Addding and Editing SME Information
//======================================================================
class UpdateDataController extends BaseController {

    /*-------------------------------------------------------------------------
	|	Editable field tables
	|------------------------------------------------------------------------*/
    private static $tb_editable_field = array(

        /*--------------------------------------------------------------------
        |	Existing Credit Facilities
        |-------------------------------------------------------------------*/
        'ecf'   => array(
            'db_info'                   => array('tblplanfacility', 'planfacilityid'),
            'lending_institution'       => array(array(array(EDITABLE_NONE)), array('lending_institution' => 'required'), array('lending_institution.required' => 'Lending Institution is required'),
                array(
                    '' => 'messages.choose_here',
                    'ecf.banks' => 'ecf.banks',
                    'ecf.suppliers' => 'ecf.suppliers',
                    'ecf.guarantors' => 'ecf.guarantors',
                    'ecf.other_lenders' => 'ecf.other_lenders'
                )
            ),
            'plan_credit_availment'     => array(array(array(EDITABLE_MONEY)), array('plan_credit_availment' => 'required|numeric|min:0|max:9999999999999.99'),
                array(
                    'plan_credit_availment.required'    => 'Amount of Line is required',
                    'plan_credit_availment.numeric'     => 'Amount of Line must be a number - please do not use any signs ($, %, #, etc)',
                    'plan_credit_availment.max'         => 'Maximum value for Amount of Line is 9999999999999.99'
                )
            ),
            'purpose_credit_facility'   => array(array(array(EDITABLE_NONE)), array('purpose_credit_facility' => 'required'), array('purpose_credit_facility.required' => 'Purpose of Credit Facility is required'),
                array(
                    '' => 'messages.choose_here',
                    '1' => 'ecf.working_capital',
                    '2' => 'ecf.expansion',
                    '3' => 'ecf.takeout',
                    '5' => 'ecf.supplier_credit',
                    '6' => 'ecf.equipment_purchase',
                    '7' => 'ecf.guarantee',
                    '4' => 'ecf.other_specify'
                )
            ),
            'credit_terms'                      => array(array(array(EDITABLE_NONE)), array('credit_terms' => 'required'), array('credit_terms.required' => 'Credit Terms is required')),
            'outstanding_balance'               => array(array(array(EDITABLE_MONEY)), array('outstanding_balance' => 'numeric|min:0|max:9999999999999.99'), array()),
            'experience'   => array(array(array(EDITABLE_NONE)), array(), array(),
                array(
                    '' => 'messages.choose_here',
                    '1' => 'messages.poor',
                    '2' => 'messages.fair',
                    '3' => 'messages.prompt',
                )
            ),
            'other_remarks' => array(array(array(EDITABLE_NONE)), array(), array ()),
            'purpose_credit_facility_others'    => array(array(array(EDITABLE_NONE)), array('purpose_credit_facility_others' => 'required'), array('purpose_credit_facility_others.required' => 'Purpose of Credit Facility is required')),
            'cash_deposit_details'              => array(array(array(EDITABLE_NONE)), array(), array()),
            'cash_deposit_amount'               => array(array(array(EDITABLE_MONEY)), array('cash_deposit_amount' => 'numeric|min:0|max:9999999999999.99'), array()),
            'securities_details'                => array(array(array(EDITABLE_NONE)), array(), array()),
            'securities_estimated_value'        => array(array(array(EDITABLE_MONEY)), array('securities_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'property_details'                  => array(array(array(EDITABLE_NONE)), array(), array()),
            'property_estimated_value'          => array(array(array(EDITABLE_MONEY)), array('property_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'chattel_details'                   => array(array(array(EDITABLE_NONE)), array(), array()),
            'chattel_estimated_value'           => array(array(array(EDITABLE_MONEY)), array('chattel_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'others_details'                    => array(array(array(EDITABLE_NONE)), array(), array()),
            'others_estimated_value'            => array(array(array(EDITABLE_MONEY)), array('others_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array())
        ),
        /*--------------------------------------------------------------------
        |	Requested Credit Line
        |-------------------------------------------------------------------*/
        'rcl'   => array(
            'db_info'                   => array('tblplanfacilityrequested', 'id'),
            'plan_credit_availment'     => array(array(array(EDITABLE_MONEY)), array('plan_credit_availment'	=> 'required|numeric|min:0|max:9999999999999.99'), array(
                    'plan_credit_availment.required'    => 'Amount of Line is required',
                    'plan_credit_availment.numeric'     => 'Amount of Line must be a number - please do not use any signs ($, %, #, etc)',
                    'plan_credit_availment.max'         => 'Maximum value for Amount of Line is 9999999999999.99'
                )
            ),
            'purpose_credit_facility'   => array(array(array(EDITABLE_NONE)), array('purpose_credit_facility' => 'required'), array('purpose_credit_facility.required' => 'Purpose of Credit Facility is required'),
                array(
                    '' => 'messages.choose_here',
                    '1' => 'ecf.working_capital',
                    '2' => 'ecf.expansion',
                    '3' => 'ecf.takeout',
                    '5' => 'ecf.supplier_credit',
                    '6' => 'ecf.equipment_purchase',
                    '7' => 'ecf.guarantee',
                    '4' => 'ecf.other_specify'
                )
            ),
            'credit_terms'                      => array(array(array(EDITABLE_NONE)), array('credit_terms' => 'required'), array('credit_terms.required' => 'Credit Terms is required')),
            'other_remarks'                     => array(array(array(EDITABLE_NONE)), array(), array ()),
            'purpose_credit_facility_others'    => array(array(array(EDITABLE_NONE)), array('purpose_credit_facility_others' => 'required'), array('purpose_credit_facility_others.required' => 'Purpose of Credit Facility is required')),
            'cash_deposit_details'              => array(array(array(EDITABLE_NONE)), array(), array()),
            'cash_deposit_amount'               => array(array(array(EDITABLE_MONEY)), array('cash_deposit_amount' => 'numeric|min:0|max:9999999999999.99'), array()),
            'securities_details'                => array(array(array(EDITABLE_NONE)), array(), array()),
            'securities_estimated_value'        => array(array(array(EDITABLE_MONEY)), array('securities_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'property_details'                  => array(array(array(EDITABLE_NONE)), array(), array()),
            'property_estimated_value'          => array(array(array(EDITABLE_MONEY)), array('property_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'chattel_details'                   => array(array(array(EDITABLE_NONE)), array(), array()),
            'chattel_estimated_value'           => array(array(array(EDITABLE_MONEY)), array('chattel_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array()),
            'others_details'                    => array(array(array(EDITABLE_NONE)), array(), array()),
            'others_estimated_value'            => array(array(array(EDITABLE_MONEY)), array('others_estimated_value' => 'numeric|min:0|max:9999999999999.99'), array())
        ),
        /*--------------------------------------------------------------------
        |	Company Succession, Landscape, Economic Factors, Tax Payments
        |-------------------------------------------------------------------*/
        'bcs'   => array(
            'db_info'                   => array('tblsustainability', 'sustainabilityid'),
            'succession_plan'           => array(array(array(EDITABLE_NONE)), array('succession_plan' => 'required'), array(),
                array(
                    ''  => 'condition_sustainability.choose_plan',
                    '1' => 'condition_sustainability.mngmt_relative',
                    '2' => 'condition_sustainability.trusted_employee',
                    '3' => 'condition_sustainability.none_less_five',
                )
            ),
            'succession_timeframe'      => array(array(array(EDITABLE_NONE)), array('succession_timeframe' => 'required'), array(),
                array(
                    ''  => 'condition_sustainability.choose_succession_time',
                    '0' => 'condition_sustainability.succession_plan_none',
                    '1' => 'condition_sustainability.succession_within_five',
                    '2' => 'condition_sustainability.succession_over_five'
                )
            ),
            'competition_landscape'     => array(array(array(EDITABLE_NONE)), array('competition_landscape' => 'required'), array(),
                array(
                    ''  => 'condition_sustainability.choose_landscape',
                    '1' => 'condition_sustainability.market_pioneer',
                    '2' => 'condition_sustainability.limited_market_entry',
                    '3' => 'condition_sustainability.high_market_penetration'
                )
            ),
            'ev_susceptibility_economic_recession'     => array(array(array(EDITABLE_NONE)), array('ev_susceptibility_economic_recession' => 'required'), array('ev_susceptibility_economic_recession.required' => 'Economic Recession is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'messages.low',
                    '2' => 'messages.medium',
                    '3' => 'messages.high'
                )
            ),
            'ev_foreign_exchange_interest_sensitivity'  => array(array(array(EDITABLE_NONE)), array('ev_foreign_exchange_interest_sensitivity' => 'required'), array('ev_foreign_exchange_interest_sensitivity.required' => 'Foreign Exchange is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'messages.low',
                    '2' => 'messages.medium',
                    '3' => 'messages.high'
                )
            ),
            'ev_commodity_price_volatility'             => array(array(array(EDITABLE_NONE)), array('ev_commodity_price_volatility' => 'required'), array('ev_commodity_price_volatility.required' => 'Commodity Price is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'messages.low',
                    '2' => 'messages.medium',
                    '3' => 'messages.high'
                )
            ),
            'ev_governement_regulation'                 => array(array(array(EDITABLE_NONE)), array('ev_governement_regulation' => 'required'), array('ev_governement_regulation.required' => 'Government Regulation is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'messages.low',
                    '2' => 'messages.medium',
                    '3' => 'messages.high'
                )
            ),
            'tax_payments'              => array(array(array(EDITABLE_NONE)), array('tax_payments' => 'required'), array(),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'condition_sustainability.taxes_current',
                    '2' => 'condition_sustainability.taxes_arrears',
                    '3' => 'condition_sustainability.taxes_bad'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Risk Assessment
        |-------------------------------------------------------------------*/
        'risk'   => array(
            'db_info'                   => array('tblriskassessment', 'riskassessmentid'),
            'risk_assessment_name'      => array(array(array(EDITABLE_NONE)), array('risk_assessment_name' => 'required'), array('risk_assessment_name.required' => 'Please choose a Threat'),
                array(
                    ''                          => 'messages.choose_here',
                    'threat_of_new_entrants'    => 'condition_sustainability.threat_of_new_entrants',
                    'threat_of_substitute'      => 'condition_sustainability.threat_of_substitute',
                    'buyer_bargainer_pow'       => 'condition_sustainability.buyer_bargainer_pow',
                    'supplier_bargainer_pow'    => 'condition_sustainability.supplier_bargainer_pow',
                    'rivalry_intensity'         => 'condition_sustainability.rivalry_intensity'
                )
            ),
            'risk_assessment_solution'  => array(array(array(EDITABLE_NONE)), array('risk_assessment_solution' => 'required'), array('risk_assessment_solution.required' => 'Please choose a Counter Measure'),
                array(
                    ''                      => 'messages.choose_here',
                    'cost_leadership_sel'   => 'condition_sustainability.cost_leadership_sel',
                    'product_innovation'    => 'condition_sustainability.product_innovation',
                    'focus'                 => 'condition_sustainability.focus'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Revenue Growth Potential
        |-------------------------------------------------------------------*/
        'rev_growth'    => array(
            'db_info'           => array('tblrevenuegrowthpotential', 'growthpotentialid'),
            'current_market'    => array(array(array(EDITABLE_NONE)), array('current_market' => 'required'), array('current_market.required' => 'Under Current Market Conditions is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => '0%',
                    '2' => 'condition_sustainability.less_3_percent',
                    '3' => 'condition_sustainability.between_3to5_percent',
                    '4' => 'condition_sustainability.greater_5_percent'
                )
            ),
            'new_market'    => array(array(array(EDITABLE_NONE)), array('new_market' => 'required'), array('new_market.required' => 'Expected Future Market Conditions is required'),
                array(
                    ''  => 'messages.choose_here',
                    '1' => '0%',
                    '2' => 'condition_sustainability.less_3_percent',
                    '3' => 'condition_sustainability.between_3to5_percent',
                    '4' => 'condition_sustainability.greater_5_percent'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Capital Investment for Expansion
        |-------------------------------------------------------------------*/
        'cap_expand'    => array(
            'db_info'       => array('tblcapacityexpansion', 'capacityexpansionid'),
            'current_year'  => array(array(array(EDITABLE_MONEY)), array('current_year' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'current_year.required' => 'Within next 12 months is required',
                    'current_year.numeric'  => array('validation.numeric', 'The Within next 12 months'),
                    'current_year.max'      => array('validation.max.numeric', 'The Within next 12 months')
                ),
            ),
            'new_year'    => array(array(array(EDITABLE_MONEY)), array('new_year' => 'required|numeric|min:0|max:9999999999999.99'),
                array(
                    'new_year.required'     => 'Within next 2 years is required',
                    'new_year.numeric'      => array('validation.numeric', 'The Within next 2 years'),
                    'new_year.max'          => array('validation.max.numeric', 'The Within next 2 years'),
                ),
            )
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Major Customers
        |-------------------------------------------------------------------*/
        'major_cust'    => array(
            'db_info'       => array('tblmajorcustomer', 'majorcustomerrid'),
            'customer_name' => array(array(array(EDITABLE_NONE)), array('customer_name' => 'required'),
                array (
                    'customer_name.required'  => array('validation.required', 'Customer Name'),
                ),
            ),
            'customer_share_sales'  => array(array(array(EDITABLE_PERCENT)), array('customer_share_sales' => 'required|numeric|min:0|max:100'),
                array(
                    'customer_share_sales.required'     => array('validation.required', 'business_details2', 'mc_percent_share'),
                    'customer_share_sales.numeric'      => array('validation.numeric', 'business_details2', 'mc_percent_share'),
                    'customer_share_sales.min'          => array('validation.min.numeric', 'business_details2', 'mc_percent_share'),
                    'customer_share_sales.max'          => array('validation.max.numeric', 'business_details2', 'mc_percent_share'),
                ),
            ),
            'customer_address'      => array(array(array(EDITABLE_NONE)), array('customer_address' => 'required'),
                array (
                    'customer_address.required' => array('validation.required', 'Address'),
                ),
            ),
            'customer_contact_person' => array(array(array(EDITABLE_NONE)), array('customer_contact_person' => 'required'),
                array (
                    'customer_contact_person.required'  => array('validation.required', 'Contact Person'),
                ),
            ),
            'customer_phone'    => array(array(array(EDITABLE_NONE)), array('customer_phone' => array('required', 'regex:/^.{7,20}$/')),
                array (
                    'customer_phone.required'   => array('validation.required', 'Contact Phone Number'),
                    'customer_phone.regex'       => array('validation.phoneregex', 'Contact Phone Number')
                ),
            ),
            'customer_email'    => array(array(array(EDITABLE_NONE)), array('customer_email' => 'required|email'),
                array (
                    'customer_email.required'   => array('validation.required', 'Contact Email'),
                    'customer_email.email'      => array('validation.email', 'Contact Email'),
                ),
            ),
            'customer_started_years'    => array(array(array(EDITABLE_NONE)), array('customer_started_years' => 'required|date_format:"Y"|after:1969|before:next year'),
                array (
                    'customer_started_years.required'       => array('validation.required', 'business_details2', 'mc_year_started'),
                    'customer_started_years.date_format'    => 'Invalid Date Format',
                    'customer_started_years.after'          => 'Year cannot be lower than 1970',
                    'customer_started_years.before'         => 'Year cannot exceed the year now',
                ),
            ),
            'relationship_satisfaction'   => [
                [
                    [
                        EDITABLE_NONE,
                    ],
                ],
                [
                    'relationship_satisfaction' => 'required'
                ],
                [
                    'relationship_satisfaction.required' => [
                        'validation.required',
                        'business_details2',
                        'relationship_satisfaction',
                    ],
                ],
                [
                    ''  => 'messages.choose_here',
                    '1' => 'business_details2.mc_relation_good',
                    '2' => 'business_details2.mc_relation_satisfactory',
                    '3' => 'business_details2.mc_relation_unsatisfactory',
                    '4' => 'business_details2.mc_relation_bad',
                ],
            ],
            'customer_settlement'   => array(array(array(EDITABLE_NONE)), array('customer_settlement' => 'required'),
                array (
                    'customer_settlement.required'   => array('validation.required', 'business_details2', 'mc_payment_behavior'),
                ),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'business_details2.mc_ahead',
                    '2' => 'business_details2.mc_ondue',
                    '3' => 'business_details2.mc_portion',
                    '4' => 'business_details2.mc_beyond',
                    '5' => 'business_details2.mc_delinquent'
                )
            ),
            'customer_order_frequency'   => array(array(array(EDITABLE_NONE)), array('customer_order_frequency' => 'required'),
                array (
                    'customer_order_frequency.required'   => array('validation.required', 'business_details2', 'mc_order_frequency'),
                ),
                array(
                    ''                  => 'messages.choose_here',
                    'mcof_regularly'    => 'business_details2.mcof_regularly',
                    'mcof_often'        => 'business_details2.mcof_often',
                    'mcof_rarely'       => 'business_details2.mcof_rarely'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Major Suppliers
        |-------------------------------------------------------------------*/
        'major_supp'    => array(
            'db_info'       => array('tblmajorsupplier', 'majorsupplierid'),
            'supplier_name' => array(array(array(EDITABLE_NONE)), array('supplier_name' => 'required'),
                array (
                    'supplier_name.required'  => array('validation.required', 'Supplier Name'),
                ),
            ),
            'supplier_share_sales'  => array(array(array(EDITABLE_PERCENT)), array('supplier_share_sales' => 'required|numeric|min:0|max:100'),
                array(
                    'supplier_share_sales.required'     => array('validation.required', 'business_details2', 'ms_percentage_share'),
                    'supplier_share_sales.numeric'      => array('validation.numeric', 'business_details2', 'ms_percentage_share'),
                    'supplier_share_sales.min'          => array('validation.min.numeric', 'business_details2', 'ms_percentage_share'),
                    'supplier_share_sales.max'          => array('validation.max.numeric', 'business_details2', 'ms_percentage_share'),
                ),
            ),
            'supplier_address'      => array(array(array(EDITABLE_NONE)), array('supplier_address' => 'required'),
                array (
                    'supplier_address.required' => array('validation.required', 'Address'),
                ),
            ),
            'supplier_contact_person' => array(array(array(EDITABLE_NONE)), array('supplier_contact_person' => 'required'),
                array (
                    'supplier_contact_person.required'  => array('validation.required', 'Contact Person'),
                ),
            ),
            'supplier_phone'    => array(array(array(EDITABLE_NONE)), array('supplier_phone' => array('required', 'regex:/^.{7,20}$/')),
                array (
                    'supplier_phone.required'   => array('validation.required', 'Contact Phone Number'),
                    'supplier_phone.regex'       => array('validation.phoneregex', 'Contact Phone Number')
                ),
            ),
            'supplier_email'    => array(array(array(EDITABLE_NONE)), array('supplier_email' => 'required|email'),
                array (
                    'supplier_email.required'   => array('validation.required', 'Contact Email'),
                    'supplier_email.email'      => array('validation.email', 'Contact Email'),
                ),
            ),
            'supplier_started_years'    => array(array(array(EDITABLE_NONE)), array('supplier_started_years' => 'required|date_format:"Y"|after:1969|before:next year'),
                array (
                    'supplier_started_years.required'       => array('validation.required', 'business_details2', 'ms_year_started'),
                    'supplier_started_years.date_format'    => 'Invalid Date Format',
                    'supplier_started_years.after'          => 'Year cannot be lower than 1970',
                    'supplier_started_years.before'         => 'Year cannot exceed the year now',
                ),
            ),
            'supplier_settlement'   => array(array(array(EDITABLE_NONE)), array('supplier_settlement' => 'required'),
                array (
                    'supplier_settlement.required'   => array('validation.required', 'business_details2', 'ms_payment'),
                ),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'business_details2.ms_ahead',
                    '2' => 'business_details2.ms_ondue',
                    '3' => 'business_details2.ms_portion',
                    '4' => 'business_details2.ms_beyond',
                    '5' => 'business_details2.ms_delinquent'
                )
            ),
            'supplier_order_frequency'   => array(array(array(EDITABLE_NONE)), array('supplier_order_frequency' => 'required'),
                array (
                    'supplier_order_frequency.required'   => array('validation.required', 'business_details2', 'ms_frequency'),
                ),
                array(
                    ''              => 'messages.choose_here',
                    'ms_cod'        => 'business_details2.ms_cod',
                    'ms_monthly'    => 'business_details2.ms_monthly',
                    'ms_bimonthly'  => 'business_details2.ms_bimonthly',
                    'ms_weekly'     => 'business_details2.ms_weekly',
                    'ms_daily'      => 'business_details2.ms_daily'
                )
            ),
			'average_monthly_volume'    => array(array(array(EDITABLE_NONE)), array('average_monthly_volume' => 'required'),
                array (
                    'average_monthly_volume.required'   => array('validation.required', 'Average Monthly Volume'),
					// 'average_monthly_volume.numeric'      => array('validation.numeric', 'Average Monthly Volume'),
                ),
            ),
			'item_1'    => array(array(array(EDITABLE_NONE)), array('item_1' => 'required'),
                array (
                    'item_1.required'   => array('validation.required', 'Item Name'),
                ),
            ),
			'item_2'    => array(array(array(EDITABLE_NONE)), array('item_2' => 'required'),
                array (
                    'item_2.required'   => array('validation.required', 'Item Name'),
                ),
            ),
			'item_3'    => array(array(array(EDITABLE_NONE)), array('item_3' => 'required'),
                array (
                    'item_3.required'   => array('validation.required', 'Item Name'),
                ),
            ),
            'relationship_satisfaction'   => [
                [
                    [
                        EDITABLE_NONE,
                    ],
                ],
                [
                    'relationship_satisfaction' => 'required'
                ],
                [
                    'relationship_satisfaction.required' => [
                        'validation.required',
                        'business_details2',
                        'relationship_satisfaction',
                    ],
                ],
                [
                    ''  => 'messages.choose_here',
                    '1' => 'business_details2.ms_relation_good',
                    '2' => 'business_details2.ms_relation_satisfactory',
                    '3' => 'business_details2.ms_relation_unsatisfactory',
                    '4' => 'business_details2.ms_relation_bad',
                ],
            ],
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Major Locations
        |-------------------------------------------------------------------*/
        'major_loc'    => array(
            'db_info'           => array('tblmajorlocation', 'marketlocationid'),
            'marketlocation'    => array(array(array(EDITABLE_NONE)), array('marketlocation' => 'required'),
                array (
                    'marketlocation.required'  => array('validation.required', 'Location'),
                ),
                array('query' => 'city')
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Competitors
        |-------------------------------------------------------------------*/
        'competitor'    => array(
            'db_info'           => array('tblcompetitor', 'competitorid'),
            'competitor_name'   => array(array(array(EDITABLE_NONE)), array('competitor_name' => 'required'),
                array (
                    'competitor_name.required'  => array('validation.required', 'Name'),
                ),
            ),
            'competitor_address'=> array(array(array(EDITABLE_NONE)), array('competitor_address' => 'required'),
                array (
                    'competitor_address.required'  => array('validation.required', 'Address'),
                ),
            ),
            'competitor_phone'  => array(array(array(EDITABLE_NONE)), array('competitor_phone' => array('required', 'regex:/^.{7,20}$/')),
                array (
                    'competitor_phone.required'  => array('validation.required', 'Contact Phone'),
                    'competitor_phone.regex'       => array('validation.phoneregex', 'Contact Phone Number')
                ),
            ),
            'competitor_email'  => array(array(array(EDITABLE_NONE)), array('competitor_email' => 'required|email'),
                array (
                    'competitor_phone.required' => array('validation.required', 'Contact Email'),
                    'competitor_phone.email'    => array('validation.email', 'Contact Email'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Business Driver
        |-------------------------------------------------------------------*/
        'driver'    => array(
            'db_info'               => array('tblbusinessdriver', 'businessdriverid'),
            'businessdriver_name'   => array(array(array(EDITABLE_NONE)), array('businessdriver_name' => 'required'),
                array (
                    'businessdriver_name.required'  => array('validation.required', 'business_details2', 'bdriver_name'),
                ),
            ),
            'businessdriver_total_sales'    => array(array(array(EDITABLE_PERCENT)), array('businessdriver_total_sales' => 'required|numeric|min:0|max:100'),
                array (
                    'businessdriver_total_sales.required'   => array('validation.required', 'business_details2', 'bdriver_share'),
                    'businessdriver_total_sales.numeric'    => array('validation.numeric', 'business_details2', 'bdriver_share'),
                    'businessdriver_total_sales.min'        => array('validation.min.numeric', 'business_details2', 'bdriver_share'),
                    'businessdriver_total_sales.max'        => array('validation.max.numeric', 'business_details2', 'bdriver_share'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Business Segments
        |-------------------------------------------------------------------*/
        'bizsegment'    => array(
            'db_info'               => array('tblbusinesssegment', 'businesssegmentid'),
            'businesssegment_name'  => array(array(array(EDITABLE_NONE)), array('businesssegment_name' => 'required'),
                array (
                    'businesssegment_name.required'  => array('validation.required', 'Name'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Past Projects Completed
        |-------------------------------------------------------------------*/
        'ppc'   => array(
            'db_info'               => array('tblprojectcompleted', 'projectcompletedid'),
            'projectcompleted_name' => array(array(array(EDITABLE_NONE)), array('projectcompleted_name' => 'required'),
                array (
                    'projectcompleted_name.required'  => array('validation.required', 'business_details2', 'ppc_purpose'),
                ),
            ),
            'projectcompleted_cost' => array(array(array(EDITABLE_MONEY)), array('projectcompleted_cost' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'projectcompleted_cost.required'   => array('validation.required', 'business_details2', 'ppc_cost'),
                    'projectcompleted_cost.numeric'    => array('validation.numeric', 'business_details2', 'ppc_cost'),
                    'projectcompleted_cost.max'        => array('validation.max.numeric', 'business_details2', 'ppc_cost'),
                ),
            ),
            'projectcompleted_source_funding'   => array(array(array(EDITABLE_NONE)), array('projectcompleted_source_funding' => 'required'),
                array (
                    'projectcompleted_source_funding.required'   => array('validation.required', 'business_details2', 'ppc_source'),
                ),
                array(
                    ''                      => 'messages.choose_here',
                    'ppc_source_organic'    => 'business_details2.ppc_source_organic',
                    'ppc_source_family'     => 'business_details2.ppc_source_family',
                    'ppc_source_partners'   => 'business_details2.ppc_source_partners',
                    'ppc_source_venture'    => 'business_details2.ppc_source_venture',
                    'ppc_source_ipo'        => 'business_details2.ppc_source_ipo',
                    'ppc_source_bank_loan'  => 'business_details2.ppc_source_bank_loan',
                    'ppc_source_informal'   => 'business_details2.ppc_source_informal'
                )
            ),
            'projectcompleted_year_began'   => array(array(array(EDITABLE_NONE)), array('projectcompleted_year_began' => 'required|date_format:"Y"|after:1969'),
                array (
                    'projectcompleted_year_began.required'      => array('validation.required', 'business_details2', 'ppc_year'),
                    'projectcompleted_year_began.date_format'   => 'Invalid year format',
                    'projectcompleted_year_began.after'         => 'Year cannot be lower than 1970',
                ),
            ),
            'projectcompleted_result'   => array(array(array(EDITABLE_NONE)), array('projectcompleted_result' => 'required'),
                array (
                    'projectcompleted_result.required'   => array('validation.required', 'business_details2', 'ppc_result'),
                ),
                array(
                    ''              => 'messages.choose_here',
                    'ppc_result_vs' => 'business_details2.ppc_result_vs',
                    'ppc_result_s'  => 'business_details2.ppc_result_s',
                    'ppc_result_u'  => 'business_details2.ppc_result_u'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details II - Future Growth Initiative
        |-------------------------------------------------------------------*/
        'fgi'   => array(
            'db_info'               => array('tblfuturegrowth', 'futuregrowthid'),
            'futuregrowth_name' => array(array(array(EDITABLE_NONE)), array('futuregrowth_name' => 'required'),
                array (
                    'futuregrowth_name.required'  => array('validation.required', 'business_details2', 'fgi_purpose'),
                ),
            ),
            'futuregrowth_estimated_cost' => array(array(array(EDITABLE_MONEY)), array('futuregrowth_estimated_cost' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'futuregrowth_estimated_cost.required'   => array('validation.required', 'business_details2', 'fgi_cost'),
                    'futuregrowth_estimated_cost.numeric'    => array('validation.numeric', 'business_details2', 'fgi_cost'),
                    'futuregrowth_estimated_cost.max'        => array('validation.max.numeric', 'business_details2', 'fgi_cost'),
                ),
            ),
            'futuregrowth_implementation_date' => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('futuregrowth_implementation_date' => array('required', 'date_format:"Y-m-d"', 'after: today')),
                array (
                    'futuregrowth_implementation_date.required'     => array('validation.required', 'business_details2', 'fgi_date'),
                    'futuregrowth_implementation_date.date_format'  => array('validation.date_format', 'business_details2', 'fgi_date'),
                    'futuregrowth_implementation_date.after'        => array('validation.after', 'business_details2', 'fgi_date')
                ),
            ),
            'futuregrowth_source_capital' => array(array(array(EDITABLE_NONE)), array('futuregrowth_source_capital' => 'required'),
                array (
                    'futuregrowth_source_capital.required'  => array('validation.required', 'business_details2', 'fgi_source'),
                ),
                array(
                    ''                      => 'messages.choose_here',
                    'fgi_source_organic'    => 'business_details2.fgi_source_organic',
                    'fgi_source_family'     => 'business_details2.fgi_source_family',
                    'fgi_source_partners'   => 'business_details2.fgi_source_partners',
                    'fgi_source_venture'    => 'business_details2.fgi_source_venture',
                    'fgi_source_ipo'        => 'business_details2.fgi_source_ipo',
                    'fgi_source_bank'       => 'business_details2.fgi_source_bank'
                )
            ),
            'planned_proj_goal' => array(array(array(EDITABLE_NONE)), array('planned_proj_goal' => 'required'),
                array (
                    'planned_proj_goal.required'  => array('validation.required', 'business_details2', 'fgi_proj_goal'),
                ),
                array(
                    ''                      => 'messages.choose_here',
                    'fgi_increase_sales'    => 'business_details2.fgi_increase_sales',
                    'fgi_increase_profit'   => 'business_details2.fgi_increase_profit'
                )
            ),
            'planned_goal_increase' => array(array(array(EDITABLE_NONE)), array('planned_goal_increase' => 'required'),
                array (
                    'planned_goal_increase.required'  => array('validation.required', 'business_details2', 'fgi_goal_increase'),
                ),
                array(
                    ''          => 'messages.choose_here',
                    '0-5'       => '0 - 5 %',
                    '6-10'      => '6 - 10 %',
                    '10-15'     => '10 - 15 %',
                    '15-20'     => '15 - 20 %',
                    'Over 20'   => 'Over 20 %',
                )
            ),
            'proj_benefit_date' => array(array(array(EDITABLE_DATE, DATE_FORMAT_FY)), array('proj_benefit_date' => 'required|date_format:"Y-m"'),
                array (
                    'proj_benefit_date.required'    => array('validation.required', 'business_details2', 'fgi_benefit_date'),
                    'proj_benefit_date.date_format' => 'Invalid Date Format',
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Machines and equipment
        |-------------------------------------------------------------------*/
        'machineEquipment'    => array(
            'db_info'           => array('machine_and_equipments', 'id'),
            'name'    => array(array(array(EDITABLE_NONE)), array('name' => 'required'),
                array (
                    'name.required'   => array('validation.required', 'business_details2', 'machine_equip_name'),
                ),
            ),
            'serial_key'    => array(array(array(EDITABLE_NONE)), array('serial_key' => 'required|unique:machine_and_equipments,serial_key'),
                array (
                    'serial_key.required'   => array('validation.required', 'business_details2', 'machine_equip_serial'),
                    'machine_equip_serial.unique'        => array('validation.unique', 'business_details2', 'machine_equip_serial'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Owner Details
        |-------------------------------------------------------------------*/
        'biz_owner' => array(
            'db_info'       => array('tblowner', 'ownerid'),
            'firstname'     => array(array(array(EDITABLE_NONE)), array('firstname' => 'required'),
                array (
                    'firstname.required'  => array('validation.required', 'First Name'),
                ),
            ),
            'middlename'    => array(array(array(EDITABLE_NONE)), array('middlename' => 'required'),
                array (
                    'middlename.required'  => array('validation.required', 'Middle Name'),
                ),
            ),
            'lastname'      => array(array(array(EDITABLE_NONE)), array('lastname' => 'required'),
                array (
                    'lastname.required'  => array('validation.required', 'Last Name'),
                ),
            ),
            'birthdate'     => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('birthdate' => array('required', 'date_format:"Y-m-d"', 'before: today')),
                array (
                    'birthdate.required'    => array('validation.required', 'Birthdate'),
                    'birthdate.before'      => array('validation.before', 'Birthdate'),
                ),
            ),
            'nationality'   => array(array(array(EDITABLE_NONE)), array('nationality' => 'required'),
                array (
                    'nationality.required'    => array('validation.required', 'Nationality'),
                ),
            ),
            'civilstatus'   => array(array(array(EDITABLE_NONE)), array('civilstatus' => 'required'),
                array (
                    'civilstatus.required'    => array('validation.required', 'Civil Status'),
                ),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'owner_details.married',
                    '2' => 'owner_details.single',
                    '3' => 'owner_details.separated'
                )
            ),
            'profession'    => array(array(array(EDITABLE_NONE)), array('profession' => 'required'),
                array (
                    'profession.required'    => array('validation.required', 'Profession'),
                ),
            ),
            'email'         => array(array(array(EDITABLE_NONE)), array('email' => 'required|email'),
                array (
                    'profession.required'   => array('validation.required', 'Email'),
                    'profession.email'      => array('validation.email', 'Email'),
                ),
            ),
            'address1'      => array(array(array(EDITABLE_NONE)), array('address1' => 'required'),
                array (
                    'address1.required' => array('validation.required', 'Address 1'),
                ),
            ),
            'address2'      => array(array(array(EDITABLE_NONE)), array(), array()),
            'cityid'        => array(array(array(EDITABLE_NONE)), array('cityid' => 'required'),
                array (
                    'cityid.required'  => array('validation.required', 'City'),
                ),
                array('query' => 'city')
            ),
            'province'      => array(array(array(EDITABLE_NONE)), array('province' => 'required', 'cityid' => 'required'),
                array (
                    'province.required'  => array('validation.required', 'Province'),
                    'cityid.required'  => array('validation.required', 'City'),
                ),
                array('query' => 'owner_province')
            ),
            'zipcode'       => array(array(array(EDITABLE_NONE)), array('zipcode' => 'digits:4'), array ()),
            'no_yrs_present_address'    => array(array(array(EDITABLE_NONE)), array('no_yrs_present_address' => 'required|numeric|min:0|max:100'),
                array (
                    'no_yrs_present_address.required'   => array('validation.required', 'owner_details', 'no_years_present'),
                    'no_yrs_present_address.numeric'    => array('validation.numeric', 'owner_details', 'no_years_present'),
                    'no_yrs_present_address.min'        => array('validation.min.numeric', 'owner_details', 'no_years_present'),
                    'no_yrs_present_address.max'        => array('validation.max.numeric', 'owner_details', 'no_years_present')
                ),
            ),
            'phone'         => array(array(array(EDITABLE_NONE)), array('phone' => 'required'),
                array (
                    'phone.required' => array('validation.required', 'Phone')
                ),
            ),
            'tin_num'       => array(array(array(EDITABLE_NONE)), array('tin_num' => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i')),
                array (
                    'tin_num.required'  => array('validation.required', 'Tin Number'),
                    'tin_num.regex'     => array('validation.regex', 'Tin Number')
                ),
            ),
            'percent_of_ownership'      => array(array(array(EDITABLE_PERCENT)), array('percent_of_ownership' => 'required|numeric|min:0|max:100'),
                array (
                    'percent_of_ownership.required' => array('validation.required', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.numeric'  => array('validation.numeric', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.min'      => array('validation.min.numeric', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.max'      => array('validation.max.numeric', 'owner_details', 'percent_ownership')
                ),
            ),
            'number_of_year_engaged'    => array(array(array(EDITABLE_NONE)), array('number_of_year_engaged' => 'required|numeric|min:0|max:100'),
                array (
                    'number_of_year_engaged.required'   => array('validation.required', 'owner_details', 'years_exp'),
                    'number_of_year_engaged.numeric'    => array('validation.numeric', 'owner_details', 'years_exp'),
                    'number_of_year_engaged.min'        => array('validation.min.numeric', 'owner_details', 'years_exp'),
                    'number_of_year_engaged.max'        => array('validation.max.numeric', 'owner_details', 'years_exp')
                ),
            ),
            'position'         => array(array(array(EDITABLE_NONE)), array('position' => 'required'),
                array (
                    'position.required' => array('validation.required', 'owner_details', 'position')
                ),
            )
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Personal Loans
        |-------------------------------------------------------------------*/
        'owner_loans' => array(
            'db_info'   => array('tblpersonalloans', 'id'),
            'purpose'   => array(array(array(EDITABLE_NONE)), array('purpose' => 'required'),
                array (
                    'firstname.required'  => array('validation.required', 'Purpose'),
                ),
                array(
                    ''                                      => 'messages.choose_here',
                    'Home Renovation / Upgrades'            => 'Home Renovation / Upgrades',
                    'Tuition / Education'                   => 'Tuition / Education',
                    'Furniture'                             => 'Furniture',
                    'Appliances / Electronic Gadgets'       => 'Appliances / Electronic Gadgets',
                    'Vacation / Travel'                     => 'Vacation / Travel',
                    'Balance Transfer / Debt Consolidation' => 'Balance Transfer / Debt Consolidation',
                    'Special Events'                        => 'Special Events',
                    'Health and Wellness'                   => 'Health and Wellness',
                    'Medical Emergencies'                   => 'Medical Emergencies'
                )
            ),
            'balance'   => array(array(array(EDITABLE_MONEY)), array('balance' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'balance.required'  => array('validation.required', 'Outstanding Balance'),
                    'balance.numeric'   => array('validation.numeric', 'Outstanding Balance'),
                    'balance.max'       => array('validation.max.numeric', 'Outstanding Balance')
                ),
            ),
            'monthly_amortization'  => array(array(array(EDITABLE_MONEY)), array('monthly_amortization' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'monthly_amortization.required'  => array('validation.required', 'Monthly Amortization'),
                    'monthly_amortization.numeric'   => array('validation.numeric', 'Monthly Amortization'),
                    'monthly_amortization.max'       => array('validation.max.numeric', 'Monthly Amortization')
                ),
            ),
            'monthly_amortization'  => array(array(array(EDITABLE_MONEY)), array('monthly_amortization' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'monthly_amortization.required'  => array('validation.required', 'Monthly Amortization'),
                    'monthly_amortization.numeric'   => array('validation.numeric', 'Monthly Amortization'),
                    'monthly_amortization.max'       => array('validation.max.numeric', 'Monthly Amortization')
                ),
            ),
            'creditor'   => array(array(array(EDITABLE_NONE)), array('creditor' => 'required'),
                array (
                    'creditor.required'  => array('validation.required', 'Creditor'),
                ),
            ),
            'due_date'   => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('due_date' => 'required|date_format:"Y-m-d"'),
                array (
                    'due_date.required'     => array('validation.required', 'Due Date'),
                    'due_date.date_format'  => 'Invalid Date format',
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Key Managers
        |-------------------------------------------------------------------*/
        'key_managers' => array(
            'db_info'       => array('tblkeymanagers', 'keymanagerid'),
            'firstname'     => array(array(array(EDITABLE_NONE)), array('firstname' => 'required'),
                array (
                    'firstname.required'  => array('validation.required', 'First Name'),
                ),
            ),
            'middlename'    => array(array(array(EDITABLE_NONE)), array(),
                array (),
            ),
            'lastname'      => array(array(array(EDITABLE_NONE)), array('lastname' => 'required'),
                array (
                    'lastname.required'  => array('validation.required', 'Last Name'),
                ),
            ),
            'birthdate'     => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('birthdate' => array('required', 'date_format:"Y-m-d"', 'before: today')),
                array (
                    'birthdate.required'    => array('validation.required', 'Birthdate'),
                    'birthdate.before'      => array('validation.before', 'Birthdate'),
                ),
            ),
            'nationality'       => array(array(array(EDITABLE_NONE)), array('nationality' => 'required'),
                array (
                    'nationality.required' => array('validation.required', 'Nationality')
                ),
                array('query' => 'nationality_related')
            ),
            'civilstatus'   => array(array(array(EDITABLE_NONE)), array('civilstatus' => 'required'),
                array (
                    'civilstatus.required'    => array('validation.required', 'Civil Status'),
                ),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'owner_details.married',
                    '2' => 'owner_details.single',
                    '3' => 'owner_details.separated'
                )
            ),
            'profession'    => array(array(array(EDITABLE_NONE)), array('profession' => 'required'),
                array (
                    'profession.required'    => array('validation.required', 'Profession'),
                ),
            ),
            'email'         => array(array(array(EDITABLE_NONE)), array('email' => 'required|email'),
                array (
                    'email.required'   => array('validation.required', 'Email'),
                    'email.email'      => array('validation.email', 'Email'),
                ),
            ),
            'address1'      => array(array(array(EDITABLE_NONE)), array(),
                array (),
            ),
            'address2'      => array(array(array(EDITABLE_NONE)), array(), array()),
            'cityid'        => array(array(array(EDITABLE_NONE)), array('cityid' => 'required'),
                array (
                    'cityid.required'  => array('validation.required', 'City'),
                ),
                array('query' => 'city')
            ),
            'province'      => array(array(array(EDITABLE_NONE)), array('province' => 'required', 'cityid' => 'required'),
                array (
                    'province.required'  => array('validation.required', 'Province'),
                    'cityid.required'  => array('validation.required', 'City'),
                ),
                array('query' => 'keymanager_province')
            ),
            'zipcode'       => array(array(array(EDITABLE_NONE)), array('zipcode' => 'digits:4'), array ()),
            'no_yrs_present_address'    => array(array(array(EDITABLE_NONE)), array('no_yrs_present_address' => 'required|numeric|min:0|max:100'),
                array (
                    'no_yrs_present_address.required'   => array('validation.required', 'owner_details', 'no_years_present'),
                    'no_yrs_present_address.numeric'    => array('validation.numeric', 'owner_details', 'no_years_present'),
                ),
            ),
            'phone'         => array(array(array(EDITABLE_NONE)), array('phone' => 'required'),
                array (
                    'phone.required' => array('validation.required', 'Phone')
                ),
            ),
            'tin_num'       => array(array(array(EDITABLE_NONE)), array('tin_num' => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i')),
                array (
                    'tin_num.required'  => array('validation.required', 'Tin Number'),
                    'tin_num.regex'     => array('validation.regex', 'Tin Number')
                ),
            ),
            'percent_of_ownership'      => array(array(array(EDITABLE_PERCENT)), array('percent_of_ownership' => 'required|numeric|min:0|max:100'),
                array (
                    'percent_of_ownership.required' => array('validation.required', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.numeric'  => array('validation.numeric', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.min'      => array('validation.min.numeric', 'owner_details', 'percent_ownership'),
                    'percent_of_ownership.max'      => array('validation.max.numeric', 'owner_details', 'percent_ownership')
                ),
            ),
            'number_of_year_engaged'    => array(array(array(EDITABLE_NONE)), array('number_of_year_engaged' => 'required|numeric|min:0|max:100'),
                array (
                    'number_of_year_engaged.required' => array('validation.required', 'owner_details', 'years_exp'),
                    'number_of_year_engaged.numeric'  => array('validation.numeric', 'owner_details', 'years_exp')
                ),
            ),
            'position'         => array(array(array(EDITABLE_NONE)), array('position' => 'required'),
                array (
                    'position.required' => array('validation.required', 'owner_details', 'position')
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Other Business
        |-------------------------------------------------------------------*/
        'other_biz' => array(
            'db_info'       => array('tblmanagebus', 'managebusid'),
            'bus_name'      => array(array(array(EDITABLE_NONE)), array('bus_name' => 'required'),
                array (
                    'bus_name.required'  => array('validation.required', 'Business Name'),
                ),
            ),
            'bus_type'      => array(array(array(EDITABLE_NONE)), array('bus_type' => 'required'),
                array (
                    'bus_type.required'  => array('validation.required', 'Business Type'),
                ),
            ),
            'bus_location'  => array(array(array(EDITABLE_NONE)), array('bus_location' => 'required'),
                array (
                    'bus_location.required'  => array('validation.required', 'Business Location'),
                ),
                array('query' => 'city')
            ),
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Educational Background
        |-------------------------------------------------------------------*/
        'owner_educ' => array(
            'db_info'       => array('tbleducation', 'educationid'),
            'school_name'   => array(array(array(EDITABLE_NONE)), array('school_name' => 'required'),
                array (
                    'school_name.required'  => array('validation.required', 'School Name'),
                ),
            ),
            'educ_degree'   => array(array(array(EDITABLE_NONE)), array('educ_degree' => 'required'),
                array (
                    'educ_degree.required'  => array('validation.required', 'Level of Completion'),
                ),
                array(
                    ''          => 'messages.choose_here',
                    'Not Taken' => 'owner_details.lvl_not_taken',
                    'Graduated' => 'owner_details.lvl_graduated',
                    'Ongoing'   => 'owner_details.lvl_ongoing',
                    'Delayed'   => 'owner_details.lvl_delayed'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Owner Details - Spouse
        |-------------------------------------------------------------------*/
        'owner_spouse' => array(
            'db_info'       => array('tblspouse', 'spouseid'),
            'firstname'     => array(array(array(EDITABLE_NONE)), array('firstname' => 'required'),
                array (
                    'firstname.required'  => array('validation.required', 'First Name'),
                ),
            ),
            'middlename'    => array(array(array(EDITABLE_NONE)), array('middlename' => 'required'),
                array (
                    'middlename.required'  => array('validation.required', 'Middle Name'),
                ),
            ),
            'lastname'      => array(array(array(EDITABLE_NONE)), array('lastname' => 'required'),
                array (
                    'lastname.required'  => array('validation.required', 'Last Name'),
                ),
            ),
            'birthdate'     => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('birthdate' => array('required', 'date_format:"Y-m-d"', 'before: today')),
                array (
                    'birthdate.required'    => array('validation.required', 'Birthdate'),
                    'birthdate.before'      => array('validation.before', 'Birthdate'),
                ),
            ),
            'nationality'   => array(array(array(EDITABLE_NONE)), array('nationality' => 'required'),
                array (
                    'nationality.required'    => array('validation.required', 'Nationality'),
                ),
            ),
            'profession'    => array(array(array(EDITABLE_NONE)), array('profession' => 'required'),
                array (
                    'profession.required'    => array('validation.required', 'Profession'),
                ),
            ),
            'email'         => array(array(array(EDITABLE_NONE)), array('email' => 'required|email'),
                array (
                    'profession.required'   => array('validation.required', 'Email'),
                    'profession.email'      => array('validation.email', 'Email'),
                ),
            ),
            'phone'         => array(array(array(EDITABLE_NONE)), array('phone' => 'required'),
                array (
                    'phone.required' => array('validation.required', 'Phone')
                ),
            ),
            'tin_num'       => array(array(array(EDITABLE_NONE)), array('tin_num' => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3}[a-z])$/i')),
                array (
                    'tin_num.required'  => array('validation.required', 'Tin Number'),
                    'tin_num.regex'     => array('validation.regex', 'Tin Number')
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Business Details - Contractors
        |-------------------------------------------------------------------*/
        'biz_details_contractor' => array(
            'db_info'           => array('tblentity', 'entityid'),
            'entity_type'       => array(array(array(EDITABLE_NONE)), array('entity_type' => 'required'),
                array (
                    'entity_type.required' => array('validation.required', 'Entity Type')
                ),
                array(
                    ''  => 'messages.choose_here',
                    '0' => 'business_details1.corporationPartnership',
                    '1' => 'business_details1.sole_proprietorship'
                )
            ),
        ),

        /*--------------------------------------------------------------------
        |	Business Details I - Business Details
        |-------------------------------------------------------------------*/
        'biz_details' => array(
            'db_info'           => array('tblentity', 'entityid'),
            'companyname'         => array(array(array(EDITABLE_NONE)), array('companyname' => 'required'),
                array (
                    'companyname.required' => array('validation.required', 'Company Name')
                ),
            ),
            'company_tin'       => array(array(array(EDITABLE_NONE)), array('company_tin' => array('required', 'regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i')),
                array (
                    'company_tin.required' => array('validation.required', 'Company TIN'),
                    'company_tin.regex'     => array('validation.regex', 'Company TIN')
                ),
            ),
            'entity_type'       => array(array(array(EDITABLE_NONE)), array('entity_type' => 'required'),
                array (
                    'entity_type.required' => array('validation.required', 'Entity Type')
                ),
                array(
                    ''  => 'messages.choose_here',
                    '0' => 'business_details1.corporation',
                    '1' => 'business_details1.sole_proprietorship'
                )
            ),
            'tin_num'         => array(array(array(EDITABLE_NONE)), array('tin_num' => 'required'),
                array (
                    'tin_num.required' => array('validation.required', 'Business Registration Number')
                ),
            ),
            'sec_reg_date'     => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('sec_reg_date' => array('required', 'date_format:"Y-m-d"', 'before: today')),
                array (
                    'sec_reg_date.required'     => array('validation.required', 'Registration Date'),
                    'sec_reg_date.before'       => array('validation.before', 'Registration Date'),
                ),
            ),
            'industry_main_id'  => array(array(array(EDITABLE_NONE)), array('industry_main_id' => 'required', 'industry_sub_id' => 'required', 'industry_row_id' => 'required'),
                array (
                    'industry_main_id.required' => array('validation.required', 'Industry Main'),
                    'industry_sub_id.required'  => array('validation.required', 'Industry Sub'),
                    'industry_row_id.required'  => array('validation.required', 'Industry'),
                ),
                array('query' => 'industry')
            ),
            'province'          => array(array(array(EDITABLE_NONE)), array('province' => 'required', 'cityid' => 'required'),
                array (
                    'province.required' => array('validation.required', 'Province'),
                    'cityid.required'   => array('validation.required', 'City'),
                ),
                array('query' => 'province_related')
            ),
            
            'sec_cert'          => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Registration Document'),
                    'editable-file-uploader.mimes'      => 'Registration Document must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'incorporation-docs',
                    'prefix'    => 'inc-doc-',
                    'doc_hdr'   => 'Registration Document',
                )
            ),
			'sec_generation'          => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'SEC General Information Sheet'),
                    'editable-file-uploader.mimes'      => 'SEC General Information Sheet must be a an XLS template or PDF file',
                ),
                array(
                    'path'      => 'incorporation-docs',
                    'prefix'    => 'sec-generation-',
                    'doc_hdr'   => 'SEC General Information Sheet',
                )
            ),
            'permit_to_operate' => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Registration Document'),
                    'editable-file-uploader.mimes'      => 'Permit to Operate must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'documents',
                    'prefix'    => 'permit-operate-',
                    'doc_hdr'   => 'Permit to Operate',
                )
            ),
            'authority_to_borrow' => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Registration Document'),
                    'editable-file-uploader.mimes'      => 'Board Resolution must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'documents',
                    'prefix'    => 'board-resolution-',
                    'doc_hdr'   => 'Board Resolution',
                )
            ),
            'authorized_signatory' => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Registration Document'),
                    'editable-file-uploader.mimes'      => 'Authorized Signatory must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'documents',
                    'prefix'    => 'auth-sign-',
                    'doc_hdr'   => 'Authorized Signatory',
                )
            ),
            'tax_registration' => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Registration Document'),
                    'editable-file-uploader.mimes'      => 'Tax Registration must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'documents',
                    'prefix'    => 'tax-reg-',
                    'doc_hdr'   => 'BIR Registration',
                )
            ),
            'income_tax'    => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,doc,docx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'business_details1', 'income_tax_return'),
                    'editable-file-uploader.mimes'      => 'Income Tax Return must be a ZIP, RAR, JPG, PNG, PDF, Word Document',
                ),
                array(
                    'path'      => 'documents',
                    'prefix'    => 'income-tax-',
                    'doc_hdr'   => 'Income Tax Return',
                )
            ),
			'total_asset_grouping'       => array(array(array(EDITABLE_NONE)), array('total_asset_grouping' => 'required'),
                array (
                    'total_asset_grouping.required' => array('validation.required', 'Total Asset Grouping')
                ),
                array(
                    ''  => 'messages.choose_here',
                    '4' => 'business_details1.tag_micro',
                    '1' => 'business_details1.tag_small',
                    '2' => 'business_details1.tag_med',
                    '3' => 'business_details1.tag_large'
                )
            ),
            'total_assets'      => array(
                array(array(EDITABLE_MONEY)),
                array('total_assets' => 'numeric|min:0|max:9999999999999.99'),
                array (
                    'total_assets.max'      => array('validation.max.numeric', 'business_details1', 'total_assets')
                ),
            ),
            'value_of_outstanding_contracts' => [
                [
                    [EDITABLE_MONEY],
                ],
                [
                    'value_of_outstanding_contracts' => 'numeric|min:0|max:9999999999999.99',
                ],
                [
                    'value_of_outstanding_contracts.max' => [
                        'validation.max.numeric',
                        'business_details1',
                        'value_of_outstanding_contracts',
                    ],
                ],
            ],
            'website'           => array(array(array(EDITABLE_NONE)), array(), array()),
            'email'             => array(array(array(EDITABLE_NONE)), array('email' => 'required|email'),
                array (
                    'email.required'   => array('validation.required', 'Company Email'),
                    'email.email'      => array('validation.email', 'Company Email'),
                ),
            ),
            'address1'          => array(array(array(EDITABLE_NONE)), array('address1' => 'required'),
                array (
                    'address1.required'   => array('validation.required', 'business_details1', 'primary_business_address'),
                ),
            ),
            'phone'             => array(array(array(EDITABLE_NONE)), array('phone' => array('required', 'regex:/^.{7,20}$/')),
                array (
                    'phone.required'    => array('validation.required', 'business_details1', 'primary_business_telephone'),
                    'phone.regex'       => array('validation.phoneregex', 'business_details1', 'primary_business_telephone')
                ),
            ),
            'no_yrs_present_address'    => array(array(array(EDITABLE_NONE)), array('no_yrs_present_address' => 'integer|min:0|max:999'),
                array (
                    'no_yrs_present_address.integer'    => array('validation.integer', 'business_details1', 'no_yrs_present_address'),
                ),
            ),
            'date_established'  => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('date_established' => array('required', 'date_format:"Y-m-d"', 'before: today')),
                array (
                    'date_established.required'     => array('validation.required', 'business_details1', 'date_established'),
                    'date_established.date_format'  => 'Date Established Invalid Format',
                    'date_established.before'       => array('validation.before', 'business_details1', 'date_established'),
                ),
            ),
            'number_year'       => array(array(array(EDITABLE_NONE)), array('number_year' => 'required|integer|min:0|max:999'),
                array (
                    'number_year.required'  => array('validation.required', 'business_details1', 'no_yrs_in_business'),
                    'number_year.numeric'   => array('validation.integer', 'business_details1', 'no_yrs_in_business'),
                ),
            ),
            'description'       => array(array(array(EDITABLE_PARAGRAPH)), array('description' => 'required'),
                array (
                    'description.required'  => array('validation.required', 'business_details1', 'company_description'),
                ),
            ),
            'employee_size'     => array(array(array(EDITABLE_NONE)), array('employee_size' => 'required|numeric|min:0'),
                array (
                    'employee_size.required'  => array('validation.required', 'business_details1', 'employee_size'),
                ),
                array (
                    ''  => 'messages.choose_here',
                    '0' => 'business_details1.employee_size_0',
                    '1' => 'business_details1.employee_size_1',
                    '2' => 'business_details1.employee_size_2',
                    '3' => 'business_details1.employee_size_3',
                )
            ),
            'fs_input_who'     => array(array(array(EDITABLE_NONE)), array('fs_input_who' => 'required|numeric|min:0'),
                array (
                    'fs_input_who.required'  => array('validation.required', 'business_details1', 'fs_input_who'),
                ),
                array (
                    ''  => 'messages.choose_here',
                    '0' => 'business_details1.fs_input_who_0',
                    '1' => 'business_details1.fs_input_who_1',
                )
            ),
            'updated_date'  => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('updated_date' => array('date_format:"Y-m-d"', 'before:tomorrow')),
                array (
                    'updated_date.date_format'  => 'Updated Date Invalid Format',
                    'updated_date.before'       => 'Invalid Updated Date'
                ),
            ),
            'number_year_management_team'       => array(array(array(EDITABLE_NONE)), array('number_year_management_team' => 'required|integer|min:0|max:999'),
                array (
                    'number_year_management_team.required'  => array('validation.required', 'business_details1', 'number_year_management_team'),
                    'number_year_management_team.integer'   => array('validation.integer', 'business_details1', 'number_year_management_team'),
                    'number_year_management_team.min'   => array('validation.min.numeric', 'business_details1', 'number_year_management_team'),
                ),
            ),
            'is_independent'    => array(array(array(EDITABLE_NONE)), array('is_independent' => 'required'),
                array (
                    'is_independent.required'   => array('validation.required', 'business_details1', 'requesting_bank'),
                ),
                array('query' => 'bank_list')
            ),
            'negative_findings'             => array(array(array(EDITABLE_PARAGRAPH)), array('negative_findings' => 'required'),
                array (
                    'negative_findings.required'    => array('validation.required', 'business_details1', 'negative_findings'),
                ),
            ),'zipcode'             => array(array(array(EDITABLE_NONE)), array('zipcode' => 'required'),
                array (
                    'zipcode.required'    => array('validation.required', 'business_details1', 'zipcode'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Affiliates and Subsidiaries
        |-------------------------------------------------------------------*/
        'affiliates' => array(
            'db_info'                   => array('tblrelatedcompanies', 'relatedcompaniesid'),
            'related_company_name'  => array(array(array(EDITABLE_NONE)), array('related_company_name' => 'required'),
                array (
                    'related_company_name.required' => array('validation.required', 'messages', 'name'),
                ),
            ),
            'related_company_address1'  => array(array(array(EDITABLE_NONE)), array('related_company_address1' => 'required'),
                array (
                    'related_company_address1.required' => array('validation.required', 'messages', 'address_1'),
                ),
            ),
            'related_company_address2'  => array(array(array(EDITABLE_NONE)), array(), array()),
            'related_company_province'  => array(array(array(EDITABLE_NONE)), array('related_company_province' => 'required', 'related_company_cityid' => 'required'),
                array (
                    'related_company_province.required' => array('validation.required', 'Province'),
                    'related_company_cityid.required'   => array('validation.required', 'City'),
                ),
                array('query' => 'affiliates_province')
            ),
            'related_company_phone'  => array(array(array(EDITABLE_NONE)), array('related_company_phone' => array('required', 'regex:/^.{7,20}$/')),
                array (
                    'related_company_phone.required' => array('validation.required', 'messages', 'phone_number'),
                    'related_company_phone.regex'       => array('validation.phoneregex', 'Contact Phone Number')
                ),
            ),
            'related_company_email'  => array(array(array(EDITABLE_NONE)), array('related_company_email' => 'required|email'),
                array (
                    'related_company_phone.required' => array('validation.required', 'messages', 'email'),
                    'related_company_phone.email' => array('validation.email', 'messages', 'email'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Products and Services Offered
        |-------------------------------------------------------------------*/
        'offers'    => array(
            'db_info'                   => array('tblservicesoffer', 'servicesofferid'),
            'servicesoffer_name'  => array(array(array(EDITABLE_NONE)), array('servicesoffer_name' => 'required'),
                array (
                    'servicesoffer_name.required' => array('validation.required', 'business_details1', 'products_services_offered'),
                ),
            ),
            'servicesoffer_targetmarket'  => array(array(array(EDITABLE_NONE)), array('servicesoffer_targetmarket' => 'required'),
                array (
                    'servicesoffer_targetmarket.required' => array('validation.required', 'business_details1', 'target_market'),
                ),
            ),
            'servicesoffer_share_revenue'  => array(array(array(EDITABLE_PERCENT)), array('servicesoffer_share_revenue' => 'required|numeric|min:0|max:100'),
                array(
                    'servicesoffer_share_revenue.required'     => array('validation.required', 'business_details1', 'share_to_total_sales'),
                    'servicesoffer_share_revenue.numeric'      => array('validation.numeric', 'business_details1', 'share_to_total_sales'),
                    'servicesoffer_share_revenue.min'          => array('validation.min.numeric', 'business_details1', 'share_to_total_sales'),
                    'servicesoffer_share_revenue.max'          => array('validation.max.numeric', 'business_details1', 'share_to_total_sales'),
                ),
            ),
            'servicesoffer_seasonality'  => array(array(array(EDITABLE_NONE)), array('servicesoffer_seasonality' => 'required'),
                array (
                    'servicesoffer_seasonality.required' => array('validation.required', 'business_details1', 'seasonality'),
                ),
                array(
                    ''  => 'messages.choose_here',
                    '1' => 'business_details1.seasonality_year_round',
                    '2' => 'business_details1.seasonality_6_mo',
                    '3' => 'business_details1.seasonality_3_mo',
                    '4' => 'business_details1.seasonality_less_than_3_mo'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Capital Details
        |-------------------------------------------------------------------*/
        'capital_details'    => array(
            'db_info'                   => array('tblcapital', 'capitalid'),
            'capital_authorized'  => array(array(array(EDITABLE_MONEY)), array('capital_authorized' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'capital_authorized.required'   => array('validation.required', 'business_details1', 'authorized_capital'),
                    'capital_authorized.numeric'    => array('validation.numeric', 'business_details1', 'authorized_capital'),
                    'capital_authorized.max'        => array('validation.max.numeric', 'business_details1', 'authorized_capital'),
                ),
            ),
            'capital_issued'  => array(array(array(EDITABLE_MONEY)), array('capital_issued' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'capital_issued.required'   => array('validation.required', 'business_details1', 'issued_capital'),
                    'capital_issued.numeric'    => array('validation.numeric', 'business_details1', 'issued_capital'),
                    'capital_issued.max'        => array('validation.max.numeric', 'business_details1', 'issued_capital'),
                ),
            ),
            'capital_paid_up'  => array(array(array(EDITABLE_MONEY)), array('capital_paid_up' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'capital_paid_up.required'   => array('validation.required', 'business_details1', 'paid_up_capital'),
                    'capital_paid_up.numeric'    => array('validation.numeric', 'business_details1', 'paid_up_capital'),
                    'capital_paid_up.max'        => array('validation.max.numeric', 'business_details1', 'paid_up_capital'),
                ),
            ),
            'capital_ordinary_shares'  => array(array(array(EDITABLE_MONEY)), array('capital_ordinary_shares' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'capital_ordinary_shares.required'   => array('validation.required', 'business_details1', 'ordinary_shares'),
                    'capital_ordinary_shares.numeric'    => array('validation.numeric', 'business_details1', 'ordinary_shares'),
                    'capital_ordinary_shares.max'        => array('validation.max.numeric', 'business_details1', 'ordinary_shares'),
                ),
            ),
            'capital_par_value'  => array(array(array(EDITABLE_MONEY)), array('capital_par_value' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'capital_par_value.required'   => array('validation.required', 'business_details1', 'par_value'),
                    'capital_par_value.numeric'    => array('validation.numeric', 'business_details1', 'par_value'),
                    'capital_par_value.max'        => array('validation.max.numeric', 'business_details1', 'par_value'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Main Locations
        |-------------------------------------------------------------------*/
        'main_loc'    => array(
            'db_info'       => array('tbllocations', 'locationid'),
            'location_size' => array(array(array(EDITABLE_SUFFIX, ' sqm')), array('location_size' => 'required|numeric|min:0|max:2147483647'),
                array (
                    'location_size.required'    => array('validation.required', 'business_details1', 'size_of_premises'),
                    'location_size.numeric'     => array('validation.numeric', 'business_details1', 'size_of_premises'),
                    'location_size.min'         => array('validation.min.numeric', 'business_details1', 'size_of_premises'),
                    'location_size.max'         => array('validation.max.numeric', 'business_details1', 'size_of_premises'),
                ),
            ),
            'location_type' => array(array(array(EDITABLE_NONE)), array('location_type' => 'required'),
                array (
                    'location_type.required'    => array('validation.required', 'business_details1', 'premises_owned_leased'),
                ),
                array(
                    ''          => 'messages.choose_here',
                    'ml_owned'  => 'business_details1.ml_owned',
                    'ml_leased' => 'business_details1.ml_leased'
                )
            ),
            'location_map' => array(array(array(EDITABLE_NONE)), array('location_map' => 'required'),
                array (
                    'location_map.required'    => array('validation.required', 'business_details1', 'location'),
                ),
                array(
                    ''                      => 'messages.choose_here',
                    'ml_commercial_area'    => 'business_details1.ml_commercial_area',
                    'ml_residential_area'   => 'business_details1.ml_residential_area',
                    'ml_industrial_area'    => 'business_details1.ml_industrial_area'
                )
            ),
            'location_used' => array(array(array(EDITABLE_NONE)), array('location_used' => 'required'),
                array (
                    'location_used.required'    => array('validation.required', 'business_details1', 'premise_used_as'),
                ),
                array(
                    ''                          => 'messages.choose_here',
                    'ml_admin_office'           => 'business_details1.ml_admin_office',
                    'ml_sales_office'           => 'business_details1.ml_sales_office',
                    'ml_warehouse'              => 'business_details1.ml_warehouse',
                    'ml_retail_outlet'          => 'business_details1.ml_retail_outlet',
                    'ml_production_facility'    => 'business_details1.ml_production_facility'
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Branches
        |-------------------------------------------------------------------*/
        'branches'    => array(
            'db_info'                   => array('tblbranches', 'branchid'),
            'branch_address'  => array(array(array(EDITABLE_NONE)), array('branch_address' => 'required'),
                array (
                    'branch_address.required'   => array('validation.required', 'Address'),
                )
            ),
            'branch_owned_leased'  => array(array(array(EDITABLE_NONE)), array('branch_owned_leased' => 'required'),
                array (
                    'branch_owned_leased.required'   => array('validation.required', 'business_details1', 'branches_owned_leased_short'),
                ),
                array (
                    ''          => 'messages.choose_here',
                    'br_owned'  => 'business_details1.br_owned',
                    'br_leased' => 'business_details1.br_leased'
                )
            ),
            'branch_phone'  => array(array(array(EDITABLE_NONE)), array('branch_phone' => 'required'),
                array (
                    'branch_phone.required'   => array('validation.required', 'messages', 'phone_number'),
                ),
            ),
            'branch_email'  => array(array(array(EDITABLE_NONE)), array('branch_email' => 'required|email'),
                array (
                    'branch_email.required' => array('validation.required', 'messages', 'email'),
                    'branch_email.email'    => array('validation.email', 'messages', 'email'),
                ),
            ),
            'branch_contact_person'  => array(array(array(EDITABLE_NONE)), array('branch_contact_person' => 'required'),
                array (
                    'branch_contact_person.required' => array('validation.required', 'business_details1', 'b_contact_person'),
                ),
            ),
            'branch_job_title'  => array(array(array(EDITABLE_NONE)), array('branch_job_title' => 'required'),
                array (
                    'branch_job_title.required' => array('validation.required', 'business_details1', 'b_job_title'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Management Shareholders
        |-------------------------------------------------------------------*/
        'shareholders'    => array(
            'db_info'                   => array('tblshareholders', 'id'),
            'firstName'  => array(array(array(EDITABLE_NONE)), array('firstName' => 'required'),
                array (
                    'firstName.required'   => array('validation.required', 'messages', 'firstName'),
                ),
            ),
            'middleName'           => array(array(array(EDITABLE_NONE)), array(), array()
            ),
            'address'  => array(array(array(EDITABLE_NONE)), array(),
                array (),
            ),
            'amount'  => array(array(array(EDITABLE_MONEY)), array('amount' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'amount.required'   => array('validation.required', 'Amount of Shareholdings'),
                    'amount.numeric'    => array('validation.numeric', 'Amount of Shareholdings'),
                    'amount.max'        => array('validation.max.numeric', 'Amount of Shareholdings'),
                ),
            ),
            'percentage_share'  => array(array(array(EDITABLE_PERCENT)), array('percentage_share' => 'required|numeric|min:0|max:100'),
                array(
                    'percentage_share.required'     => array('validation.required', '% Share'),
                    'percentage_share.numeric'      => array('validation.numeric', '% Share'),
                    'percentage_share.min'          => array('validation.min.numeric', '% Share'),
                    'percentage_share.max'          => array('validation.max.numeric', '% Share'),
                ),
            ),
            'id_no'       => array(array(array(EDITABLE_NONE)), array('id_no' => array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i')),
                array (
                    'id_no.regex'     => array('validation.regex', 'Tin Number')
                ),
            ),
            'nationality'       => array(array(array(EDITABLE_NONE)), array('nationality' => 'required'),
                array (
                    'nationality.required' => array('validation.required', 'Nationality')
                ),
                array('query' => 'nationality_related')
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Management Directors
        |-------------------------------------------------------------------*/
        'directors'    => array(
            'db_info'                   => array('tbldirectors', 'id'),
            'firstName'  => array(array(array(EDITABLE_NONE)), array('firstName' => 'required'),
                array (
                    'firstName.required'   => array('validation.required', 'messages', 'firstName'),
                ),
            ),
            'middleName'  => array(array(array(EDITABLE_NONE)), array(),
                array (),
            ),
            'lastName'  => array(array(array(EDITABLE_NONE)), array('lastName' => 'required'),
                array (
                    'lastName.required'   => array('validation.required', 'messages', 'lastName'),
                ),
            ),
            'address'  => array(array(array(EDITABLE_NONE)), array(),
                array (),
            ),
            'percentage_share'  => array(array(array(EDITABLE_PERCENT)), array('percentage_share' => 'required|numeric|min:0|max:100'),
                array(
                    'percentage_share.required'     => array('validation.required', '% Share'),
                    'percentage_share.numeric'      => array('validation.numeric', '% Share'),
                    'percentage_share.min'          => array('validation.min.numeric', '% Share'),
                    'percentage_share.max'          => array('validation.max.numeric', '% Share'),
                ),
            ),
            'id_no'       => array(array(array(EDITABLE_NONE)), array('id_no' => array('regex:/^\d{3}-\d{3}-\d{3}($|-\d{3}|-\d{3})$/i')),
                array (
                    'id_no.regex'     => array('validation.regex', 'Tin Number')
                ),
            ),
            'nationality'       => array(array(array(EDITABLE_NONE)), array('nationality' => 'required'),
                array (
                    'nationality.required' => array('validation.required', 'Nationality')
                ),
                array('query' => 'nationality_related')
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Certifications
        |-------------------------------------------------------------------*/
        'certifications'    => array(
            'db_info'           => array('tblcertifications', 'certificationid'),
            'certification_details'    => array(array(array(EDITABLE_NONE)), array('certification_details' => 'required'),
                array (
                    'certification_details.required'   => array('validation.required', 'Certification Details'),
                ),
            ),
            'certification_reg_no'    => array(array(array(EDITABLE_NONE)), array('certification_reg_no' => 'required'),
                array (
                    'certification_reg_no.required'   => array('validation.required', 'messages', 'registration_num'),
                ),
            ),
            'certification_reg_date'    => array(array(array(EDITABLE_DATE, DATE_FORMAT_FDY)), array('certification_reg_date' => 'required|date_format:"Y-m-d"|before: today'),
                array (
                    'certification_reg_date.required'       => array('validation.required', 'messages', 'registration_date'),
                    'certification_reg_date.before'         => array('validation.before', 'messages', 'registration_date'),
                    'certification_reg_date.date_format'    => 'Invalid Date Format',
                ),
            ),
            'certification_doc' => array(array(array(EDITABLE_NONE)), array('editable-file-uploader' => 'required|mimes:pdf,xls,xlsx,zip,rar,jpeg,png,csv,tsv,txt'),
                array (
                    'editable-file-uploader.required'   => array('validation.required', 'Certification Document'),
                    'editable-file-uploader.mimes'      => 'Certification Document must be a ZIP, RAR, JPG, PNG, PDF, Word Document, Excel',
                ),
                array(
                    'path'      => 'certification-docs',
                    'prefix'    => 'certification-docs-',
                    'doc_hdr'   => 'Permit to Operate',
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Business Type
        |-------------------------------------------------------------------*/
        'biz_type'    => array(
            'db_info'           => array('tblbusinesstype', 'businesstypeid'),
            'is_import'    => array(array(array(EDITABLE_NONE)), array('is_import' => 'required'),
                array (
                    'is_import.required'   => array('validation.required', 'business_details1', 'ie_imports'),
                ),
                array (
                    ''  => 'messages.choose_here',
                    '1' => 'business_details1.ie_yes',
                    '2' => 'business_details1.ie_no',
                )
            ),
            'is_export'    => array(array(array(EDITABLE_NONE)), array('is_export' => 'required'),
                array (
                    'is_export.required'   => array('validation.required', 'business_details1', 'ie_exports'),
                ),
                array (
                    ''  => 'messages.choose_here',
                    '1' => 'business_details1.ie_yes',
                    '2' => 'business_details1.ie_no',
                )
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Insurance
        |-------------------------------------------------------------------*/
        'insurance'    => array(
            'db_info'           => array('tblinsurance', 'insuranceid'),
            'insurance_type'    => array(array(array(EDITABLE_NONE)), array('insurance_type' => 'required'),
                array (
                    'insurance_type.required'   => array('validation.required', 'business_details1', 'insurance_type'),
                ),
            ),
            'insured_amount'    => array(array(array(EDITABLE_MONEY)), array('insured_amount' => 'required|numeric|min:0|max:9999999999999.99'),
                array (
                    'insured_amount.required'   => array('validation.required', 'business_details1', 'insured_amount'),
                    'insured_amount.numeric'    => array('validation.numeric', 'business_details1', 'insured_amount'),
                    'insured_amount.max'        => array('validation.max.numeric', 'business_details1', 'insured_amount'),
                ),
            ),
        ),
        /*--------------------------------------------------------------------
        |	Business Details I - Financial Statements
        |-------------------------------------------------------------------*/
        'financials'    => array(
            'db_info'           => array('tbldocument', 'documentid'),
            'document_type'    => array(array(array(EDITABLE_NONE)), array('document_type' => 'required'),
                array (
                    'document_type.required'   => array('validation.required', 'Document Type'),
                ),
                array (
                    ''  => 'messages.choose_here',
                    '1' => 'business_details1.unaudited',
                    '2' => 'business_details1.interim',
                    '3' => 'business_details1.audited',
                    '4' => 'business_details1.recasted'
                )
            ),
        ),
    );

    //-----------------------------------------------------
    //  Function 53.1: getEditableField
    //      Display the form for editable fields
    //-----------------------------------------------------
    public function getEditableField($field_id, $field_name)
    {
        /*  Variable Declaration    */
        $conv_sts               = STS_NG;
        $prev_value             = STR_EMPTY;

        $conv_data              = array();
        $industry               = array();
        $province               = array();
        $affiliates_province    = array();
        $keymanager_province    = array();
        $owner_province    		= array();

        /*  Separate Field Name from Table to be accessed   */
        $exp_tb_field   = explode('.', $field_name);
        $arr_tb         = $exp_tb_field[0];
        $field_name     = $exp_tb_field[1];

        /*  Get Table Data  */
        $db_tbl     = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_DBNAME];
        $id_fld     = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_FLDNAME];

        $fld_typ    = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDTYP];

        if (isset(self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDFORMAT])) {
            $fld_format = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDFORMAT];
        }

        /*  Fetch the Database information of the edited field  */
        $db_data    = DB::table($db_tbl)->where($id_fld, $field_id)->select($field_name)->first();

        $field_value    = $db_data->$field_name;
        $prev_value     = $db_data->$field_name;

        $entity = DB::table('tblentity')
                  ->where('entityid', '=', Session::get('entity')->entityid)
                  ->first();

        $login = DB::table('tbllogin')
            ->where('status', '=', 2)
            ->where('loginid', '=', $entity->loginid)
            ->first();

        /*  For Drop down fields with value conversion  */
        if (isset(self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_DROPDOWN_DATA])) {

            $conv_sts       = STS_OK;
            $translations   = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_DROPDOWN_DATA];

            /*  If Drop Down data requires a Query  */
            if (isset($translations['query'])) {
                /*  List of city not related to province    */
                if ('city' == $translations['query']) {
                    $conv_data  = City::all()->pluck('city', 'cityid');
                }
                /*  List of provinces   */
                else if ('province' == $translations['query']) {
                    $conv_data  = Province::all()->pluck('province', 'provinceid');
                }
                else if ('nationality_related' == $translations['query']) {
                    $conv_data  = Countries::all()->pluck('enNationality', 'enNationality');
                }
                /*  List of Industries related to Main and Sub Industries   */
                else if ('industry' == $translations['query']) {

                    /*  Get current SME data regarding industries   */
                    $ind_data   = DB::table('tblentity')
                        ->where('entityid', Session::get('entity')->entityid)
                        ->select('industry_main_id', 'industry_sub_id', 'industry_row_id')
                        ->first();

                    $industry['main_val']   = $ind_data->industry_main_id;
                    $industry['sub_val']    = $ind_data->industry_sub_id;
                    $industry['row_val']    = $ind_data->industry_row_id;

                    if(empty($industry['main_val'])){
                        
                        $industry['main_val'] = 'A';
                        $industry['sub_val'] = '01';
                        $industry['row_val'] = '0111';
                    }

                    /*  Arranges the Drop down data to be displayed in relation to current industry data   */
                    $industry['main_lst']   = Industrymain::all()->pluck('main_title', 'main_code');
                    $industry['sub_lst']    = Industrysub::where('main_code', $industry['main_val'])->pluck('sub_title', 'sub_code');

                    $industry['row_lst'] = DB::table('tblindustry_group')->where('sub_code', $industry['sub_val'])->pluck('group_description','group_code');

                    // if(!empty($group_code)){
                    //     $industry['row_lst']    = DB::table('tblindustry_group')->whereIn('group_code', $group_code)
                    //                                     ->pluck('class_description', 'class_code');
                    // // }else{
                    //     $industry['row_lst']    = DB::table('tblindustry_class')
                    //                             ->pluck('class_description', 'class_code');
                    // }

                    /*  Sets the Previous value used in Edit cancellation   */
                    if (0 != $ind_data->industry_main_id) {
                        /*  Industry has been chosen before */
                        $conv_data['industry_txt']  = $industry['main_lst'][$ind_data->industry_main_id];
                    }
                    else {
                        /*  No initial value for industry   */
                        $conv_data['industry_txt']  = '';
                    }
                }
                /*  List of Cities in relation to their province    */
                else if ('province_related' == $translations['query']) {

                    /*  Get current SME data for province and city  */
                    $prov_data   = DB::table('tblentity')
                        ->where('entityid', Session::get('entity')->entityid)
                        ->select('province', 'cityid')
                        ->first();
                    $entity_prov = DB::table('refprovince')->where('provDesc', $prov_data->province)->first();
                    $city = City2::first();

                    $province['province_val']   = $entity_prov != null ? $entity_prov->provCode : 1;
                    $province['city_val']       = $prov_data->cityid != 0 ? $prov_data->cityid : $city->id;

                    /*  Arranges the Drop down data to be displayed */
                    $province['province_list'] = DB::table('refprovince')->select('provDesc', 'provCode')->orderBy('provDesc')->pluck('provDesc', 'provCode');
                    $province['city_list'] = DB::table('refcitymun')->where('provCode', $province['province_val'])->pluck('citymunDesc', 'id');
                    
                    /*  Sets the Previous value used in Edit cancellation   */
                    if (STR_EMPTY != $prov_data->province) {
                        /*  Province has already been chosen before */
                        $conv_data['province_txt']  = $province['province_list'][$province['province_val']];
                    }
                    else {
                        /*  No initial value for Province   */
                        $conv_data['province_txt']  = '';
                    }
                }
                /*  List of Cities in relation to their province for
                *   Business Details - Affiliates
                */
                else if ('affiliates_province' == $translations['query']) {

                    /*  Get current SME data for province and city  */
                    $prov_data   = DB::table('tblrelatedcompanies')
                        ->where('entity_id', Session::get('entity')->entityid)
                        ->select('related_company_province', 'related_company_cityid')
                        ->first();

                    $affiliates_province['province_val']   = $prov_data->related_company_province;
                    $affiliates_province['city_val']       = $prov_data->related_company_cityid;

                    /*  Arranges the Drop down data to be displayed */
                    $affiliates_province['province_list']  = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
                    $affiliates_province['city_list']      = Zipcode::select('city')->where('major_area', $affiliates_province['province_val'])->pluck('city','city');

                    /*  Sets the Previous value used in Edit cancellation   */
                    $conv_data['province_txt']  = $affiliates_province['province_list'][$affiliates_province['province_val']];
                }
				/*  List of Cities in relation to their province for Keymanager */
                else if ('keymanager_province' == $translations['query']) {

                    /*  Get current SME data for province and city  */
                    $prov_data   = DB::table('tblkeymanagers')
                        ->where('entity_id', Session::get('entity')->entityid)
                        ->where('keymanagerid', $field_id)
                        ->select('province', 'cityid')
                        ->first();

                    $keymanager_province['province_val']   = $prov_data->province;
                    $keymanager_province['city_val']       = $prov_data->cityid;

                    /*  Arranges the Drop down data to be displayed */
                    $keymanager_province['province_list']  = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
                    $keymanager_province['city_list']      = Zipcode::select('city', 'id')->where('major_area', $keymanager_province['province_val'])->pluck('city','id');

					/*  Sets the Previous value used in Edit cancellation   */
                    if (isset($keymanager_province['province_list'][$keymanager_province['province_val']])) {
                        /*  Province has already been chosen before */
                        $conv_data['province_txt']  = $keymanager_province['province_list'][$keymanager_province['province_val']];
                    }
                    else {
                        /*  No initial value for Province   */
                        $conv_data['province_txt']  = '';
                    }
				}
				/*  List of Cities in relation to their province for Owner  */
                else if ('owner_province' == $translations['query']) {

                    /*  Get current SME data for province and city  */
                    $prov_data   = DB::table('tblowner')
                        ->where('entity_id', Session::get('entity')->entityid)
                        ->where('ownerid', $field_id)
                        ->select('province', 'cityid')
                        ->first();

                    $owner_province['province_val']   = $prov_data->province;
                    $owner_province['city_val']       = $prov_data->cityid;

                    /*  Arranges the Drop down data to be displayed */
                    $owner_province['province_list']  = Zipcode::distinct()->select('major_area')->groupBy('major_area')->pluck('major_area','major_area');
                    $owner_province['city_list']      = Zipcode::select('city', 'id')->where('major_area', $owner_province['province_val'])->pluck('city','id');

					/*  Sets the Previous value used in Edit cancellation   */
                    if (isset($owner_province['province_list'][$owner_province['province_val']])) {
                        /*  Province has already been chosen before */
                        $conv_data['province_txt']  = $owner_province['province_list'][$owner_province['province_val']];
                    }
                    else {
                        /*  No initial value for Province   */
                        $conv_data['province_txt']  = '';
                    }
				}
				/*  List of Banks when SME is applying independently    */
                else if ('bank_list' == $translations['query']) {

                    /*  Get current SME data for Banks  */
                    $biz_data   = DB::table('tblentity')
                        ->where('entityid', Session::get('entity')->entityid)
                        ->select('current_bank')
                        ->first();

                    /*  Get current SME data for Banks  */
                    $bank_data  = Bank::find($biz_data->current_bank);

                    $conv_data['bank_list']     = Bank::pluck('bank_name', 'id');
                    $conv_data['current_bank']  = $biz_data->current_bank;

                    /*  Sets the Previous value used in Edit cancellation   */
                    if ($bank_data) {
                        $conv_data['bank_name']     = $bank_data->bank_name;
                    }
                }
            }
            /*  No Database query required for Drop Down data   */
            else {
                foreach ($translations as $key => $val) {
                    $conv_data[$key]   = trans($val);
                }
            }
        }

        /*  A value has been set before */
        if (STR_EMPTY != $prev_value) {
            /*  Formats the Previous value according to Field Type  */
            switch ($fld_typ) {
                /*  Monetary Value Field    */
                case EDITABLE_MONEY:
                    $prev_value = number_format($db_data->$field_name, 2);
                    break;

                /*  Percentage value field  */
                case EDITABLE_PERCENT:
                    $prev_value     = number_format($db_data->$field_name, 2).'%';
                    $field_value    = number_format($field_value, 2);
                    break;

                /*  Date value field        */
                case EDITABLE_DATE:
                    if ('0000-00-00' != $prev_value) {
                        $prev_value = date($fld_format, strtotime($db_data->$field_name));
                    }
                    else {
                        $prev_value = STR_EMPTY;
                    }
                    break;

                /*  Paragraph value field   */
                case EDITABLE_PARAGRAPH:
                    $prev_value = nl2br($db_data->$field_name);
                    break;

                /*  With suffix field   */
                case EDITABLE_SUFFIX:
                    $prev_value = $db_data->$field_name.$fld_format;
                    break;
            }
        }
        /*  No initial value for the field  */
        else {
            $prev_value = STR_EMPTY;
        }

        /*  Used for Business Details 1 - Description field */
        if (('biz_details' == $arr_tb)
        && ('description' == $field_name)) {
            /*  Displays the Template description when no initial value is set  */
            if (STR_EMPTY == $prev_value) {
                $prev_value     = nl2br(trans('business_details1.company_description_data'));
                $field_value    = trans('business_details1.company_description_data');
            }
        }
        /*  Used for Future Growth Initiative - Beggining field */
        if (('fgi' == $arr_tb)
        && ('proj_benefit_date' == $field_name)) {
            if (STR_EMPTY != $prev_value) {
                $field_value    = substr($field_value, 0, 7);
            }
        }

        /*  Interfaces with the View modules for displaying output  */
        return View::make('editable-field', array(
            'field_id'              => $field_id,
            'arr_table'             => $arr_tb,
            'field_name'            => $field_name,
            'field_value'           => $field_value,
            'prev_value'            => $prev_value,
            'fld_typ'               => $fld_typ,
            'conv_sts'              => $conv_sts,
            'conv_data'             => $conv_data,
            'industry'              => $industry,
            'province'              => $province,
            'affiliates_province'   => $affiliates_province,
            'keymanager_province'   => $keymanager_province,
            'owner_province'  		=> $owner_province,
            'login'                 => $login
        ));
    }

    //-----------------------------------------------------
    //  Function 53.2: postEditableField
    //      Validates and Saves data for form submission
    //-----------------------------------------------------
    public function postEditableField($field_id, $field_name, $arr_tb)
    {
        try {

            $entity = Entity::where('entityid', $field_id)->first();

            /*  Variable Declaration    */
            $field_value        = Input::get($field_name);
            $multi_input        = STS_NG;
            $extra_validation   = STS_NG;
            $extra_msg          = STR_EMPTY;

            /*  Initialize the Server Response  */
            $srv_resp['sts']            = STS_NG;
            $srv_resp['messages']       = STR_EMPTY;

            $srv_resp['action']         = OVERWRITE_INFO_ACT;
            $srv_resp['refresh_cntr']   = '.edit-input-cntr';

            $srv_resp['refresh_value']  = $field_value;

            /*  Get Table Data  */
            $db_tbl = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_DBNAME];
            $id_fld = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_FLDNAME];

            /*  Fetch the Database information of the edited field  */
            $db_data   = DB::table($db_tbl)->where($id_fld, $field_id)->first();

            /*  Gets the Field Format   */
            $fld_typ    = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDTYP];
            $fld_format = STR_EMPTY;

            if (isset(self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDFORMAT])) {
                $fld_format = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDINFO][DATA_TBL_TYPINFO][DATA_TBL_FLDFORMAT];
            }

            /*  Initialize validation data  */
            $rules      = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDRULES];
            $error_msgs = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_RULES_MSGS];
            $messages   = array();

            $data_val   = array(
                $field_name => $field_value
            );

            /*  Formats the Error message outputs according to the Editable table   */
            foreach ($error_msgs as $key => $val ) {

                /*  The Validation message has translation  */
                if (is_array($val)) {

                    /** The field name is also translated   */
                    if (isset($val[DATA_TBL_TRANS_LABEL])) {
                        $conv_attr  = trans($val[DATA_TBL_TRANS_FILE].'.'.$val[DATA_TBL_TRANS_LABEL]);
                    }
                    /** No translation for field name       */
                    else {
                        $conv_attr  = $val[DATA_TBL_NO_TRANS_LABEL];
                    }

                    $messages[$key] = trans($val[DATA_TBL_TRANS_RULE], array('attribute' => $conv_attr));
                }
                /*  No Translation is used  */
                else {
                    $messages[$key] = $val;
                }
            }

            /*  For Drop down fields    */
            if (isset(self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_DROPDOWN_DATA])) {

                /*  There is a chosen data from the drop down
                *   The data is required for queries and value selection
                */
                if (STR_EMPTY != $field_value) {

                    /*  If Drop Down data requires a Query  */
                    if (isset(self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_DROPDOWN_DATA]['query'])) {

                        /*  List of city not related to province    */
                        if ('city' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $city_data                  = City2::find($field_value);
                            $srv_resp['refresh_value']  = $city_data['city'];
                        }
                        /*  List of provinces       */
                        else if ('province' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $province_data              = Province2::find($field_value);
                            $srv_resp['refresh_value']  = $province_data['province'];
                        }
                        /*  List of Industries related to Main and Sub Industries   */
                        else if ('industry' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $industry_main  = Industrymain::where('main_code', $field_value)->pluck('main_title')->first();
                            $industry_sub   = Industrysub::where('sub_code', Input::get('industry_sub_id'))->pluck('sub_title')->first();
                            $industry_row   = DB::table('tblindustry_group')->where('group_code', Input::get('industry_row_id'))->pluck('group_description')->first();

                            /* Adds Server response data, validated in the JavaScript   */
                            $srv_resp['refresh_value']          = $industry_main;
                            $srv_resp['industry']['sub_title']  = $industry_sub;
                            $srv_resp['industry']['row_title']  = $industry_row;

                            /* Adds additional validation for multi-inputs  */
                            $data_val['industry_sub_id']    = Input::get('industry_sub_id');
                            $data_val['industry_row_id']    = Input::get('industry_row_id');

                            /* Checks if database value is same as the current value  */
                            /* Prevents database error for current values which are all same with database values  */
                            if (($field_value != $db_data->industry_main_id)
                            || (Input::get('industry_sub_id') != $db_data->industry_sub_id)
                            || (Input::get('industry_row_id') != $db_data->industry_row_id)) {
                                $multi_input    = STS_OK;
                            }
                        }
                        /*  List of Cities in relation to their province    */
                        else if ('province_related' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $zipcode_data   = City2::find(Input::get('cityid'));
                            $prov_data = Province2::find($zipcode_data['provCode']);

                            /* Adds Server response data, validated in the JavaScript   */
                            $srv_resp['refresh_value']          = $prov_data['provDesc'];
                            $srv_resp['province']['city_name']  = $zipcode_data['citymunDesc'];
                            $srv_resp['province']['zipcode']    = $zipcode_data['zipcode'];
                            $srv_resp['province']['city_cntr']  = '.city-name';
                            $srv_resp['province']['zip_cntr']   = '.biz-zipcode';

                            /* Adds additional validation for multi-inputs  */
                            $data_val['province']   = Input::get('province');
                            $data_val['cityid']     = Input::get('cityid');

                            /* Checks if database value is same as the current value  */
                            /* Prevents database error for current values which are all same with database values  */
                            if ((Input::get('province') != $db_data->province)
                            || (Input::get('cityid') != $db_data->cityid)) {
                                $multi_input    = STS_OK;
                            }
                        }
                        /*  List of Cities in relation to their province for
                        *   Business Details - Affiliates
                        */
                        else if ('affiliates_province' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $zipcode_data   = Zipcode::where('city', Input::get('related_company_cityid'))->first();

                            /* Adds Server response data, validated in the JavaScript   */
                            $srv_resp['affiliates_province']['city_name']  = $zipcode_data['city'];
                            $srv_resp['affiliates_province']['zipcode']    = $zipcode_data['zip_code'];
                            $srv_resp['affiliates_province']['city_cntr']  = '.affiliates-city-'.$field_id;
                            $srv_resp['affiliates_province']['zip_cntr']   = '.affiliates-zip-'.$field_id;

                            /* Adds additional validation for multi-inputs  */
                            $data_val['related_company_province']   = Input::get('related_company_province');
                            $data_val['related_company_cityid']     = Input::get('related_company_cityid');

                            /* Checks if database value is same as the current value  */
                            /* Prevents database error for current values which are all same with database values  */
                            if ((Input::get('related_company_province') != $db_data->related_company_province)
                            || (Input::get('related_company_cityid') != $db_data->related_company_cityid)) {
                                $multi_input    = STS_OK;
                            }
                        }
                        /*  List of Cities in relation to their province for Keymanagers    */
                        else if ('keymanager_province' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $zipcode_data   = Zipcode::where('id', Input::get('cityid'))->first();

                            /* Adds Server response data, validated in the JavaScript   */
                            $srv_resp['keymanager_province']['city_name']  = $zipcode_data['city'];
                            $srv_resp['keymanager_province']['zipcode']    = $zipcode_data['zip_code'];
                            $srv_resp['keymanager_province']['city_cntr']  = '.keymanager-city-'.$field_id;
                            $srv_resp['keymanager_province']['zip_cntr']   = '.keymanager-zipcode-'.$field_id;

                            /* Adds additional validation for multi-inputs  */
                            $data_val['province']   = Input::get('province');
                            $data_val['cityid']     = Input::get('cityid');

                            /* Checks if database value is same as the current value  */
                            /* Prevents database error for current values which are all same with database values  */
                            if ((Input::get('province') != $db_data->province)
                            || (Input::get('cityid') != $db_data->cityid)) {
                                $multi_input    = STS_OK;
                            }
                        }
                        /*  List of Cities in relation to their province for Owners */
                        else if ('owner_province' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $zipcode_data   = Zipcode::where('id', Input::get('cityid'))->first();

                            /* Adds Server response data, validated in the JavaScript   */
                            $srv_resp['owner_province']['city_name']  = $zipcode_data['city'];
                            $srv_resp['owner_province']['zipcode']    = $zipcode_data['zip_code'];
                            $srv_resp['owner_province']['city_cntr']  = '.biz_owner-city-'.$field_id;
                            $srv_resp['owner_province']['zip_cntr']   = '.biz_owner-zipcode-'.$field_id;

                            /* Adds additional validation for multi-inputs  */
                            $data_val['province']   = Input::get('province');
                            $data_val['cityid']     = Input::get('cityid');

                            /* Checks if database value is same as the current value  */
                            /* Prevents database error for current values which are all same with database values  */
                            if ((Input::get('province') != $db_data->province)
                            || (Input::get('cityid') != $db_data->cityid)) {
                                $multi_input    = STS_OK;
                            }
                        }
                        /*  List of Banks when SME is applying independently    */
                        else if ('bank_list' == self::$tb_editable_field[$arr_tb][$field_name][3]['query']) {
                            $srv_resp['refresh_value']      = 'Applying Independently';

                            /*  SME is not independent  */
                            if (1 == $field_value) {
                                $bank_data                      = Bank::find(Input::get('bank'));
                                $srv_resp['refresh_value']      = $bank_data['bank_name'];

                                /* Checks if database value is same as the current value  */
                                /* Prevents database error for current values which are all same with database values  */
                                if (($field_value != $db_data->is_independent)
                                || (Input::get('bank') != $db_data->current_bank)) {
                                    $multi_input    = STS_OK;
                                }
                            }
                        }
                    }
                    /*  No Database query required for Drop Down data   */
                    else {
                        $srv_resp['refresh_value']  = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_DROPDOWN_DATA][$field_value];
                        $srv_resp['refresh_value']  = trans($srv_resp['refresh_value']);
                    }
                }
            }
            /*  Validates Input via Laravel validation Engine   */
            $validator  = Validator::make(
                $data_val,
                $rules,
                $messages
            );

            /*  Required additional Validations Laravel cannot validate */
            /*  Business Details 1 - Company Name   */
            // commented out for future use
            // if (('biz_details' == $arr_tb) && ('companyname' == $field_name)) {

            //     /*  Checks name validity, only Name that does not exist or owned by an Account is valid   */
            //     $valid_name         = Entity::checkCompanyNameValidity(Auth::user()->loginid, $field_value);
            //     $extra_validation   = $valid_name;
                
            //     if (STS_NG == $valid_name) {
            //         $extra_msg          = 'Company Name already exist.';
            //     }
            // }

            /** Update company name  */
            if (('biz_details' == $arr_tb) && ('companyname' == $field_name)){
                // delete uploaded files for innodata and dropbox
                $report = Session::get('entity');
                if(strcmp($report->companyname, $field_value) != 0){
                    try{
                        DB::table('innodata_files')
                        ->where('entity_id', $report->entityid)
                        ->update(['is_deleted' => 1]);

                        DB::table('tbldropboxfspdf')
                            ->where('entity_id', $report->entityid)
                            ->update(['is_deleted' => 1]);
                    } catch(Exception $e) {

                    }
                }
                
                $pse_company = PseRegisteredCompany::where('name', 'like', '%' . $field_value)->first();

                /** Check if companyname present in pse registered company and previous*/
                if($pse_company) {
                    /** Update fields for pse on tblentity */
                    Entity::where('entityid', $report->entityid)->update(['pse_company_id' => $pse_company->id]);

                    if($pse_company->email_address){
                        Entity::where('entityid', $report->entityid)->update(['email' => $pse_company->email_address]);
                    }else{
                        Entity::where('entityid', $report->entityid)->update(['email' => '']);
                    }

                    if($pse_company->business_address){
                        Entity::where('entityid', $report->entityid)->update(['address1' => $pse_company->business_address]);
                    }else{
                        Entity::where('entityid', $report->entityid)->update(['address1' => '']);
                    }

                    if($pse_company->tel_num){
                        Entity::where('entityid', $report->entityid)->update(['phone' => $pse_company->tel_num]);
                    }else{
                        Entity::where('entityid', $report->entityid)->update(['phone' => '']);
                    }

                    if($pse_company->company_description){
                        Entity::where('entityid', $report->entityid)->update(['description' => $pse_company->company_description]);
                    }else{
                        Entity::where('entityid', $report->entityid)->update(['description' => '']);
                    }

                    if($pse_company->website){
                        Entity::where('entityid', $report->entityid)->update(['website' => $pse_company->website]);
                    }else{
                        Entity::where('entityid', $report->entityid)->update(['website' => '']);
                    }
                    /** Download Files from PSE Edge */
                    $downloadPse = new Signup();
                    $downloadPse->postUploadFspdfFilesOnCreateReport($report->entityid, $pse_company->id);

                } else {
                    /** Reset entity fields to empty */
                    Entity::where('entityid', $report->entityid)
                            ->update([
                                'pse_company_id' => '',
                                'email'     => '',
                                'address1'  => '',
                                'phone'     => '',
                                'description'   => '',
                                'website'   => ''
                            ]);
                }

                $srv_resp['action']         = REFRESH_PAGE_ACT;
                $srv_resp['refresh_cntr']   = '#biz-details-cntr';
                $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.Session::get('entity')->entityid);
                $srv_resp['refresh_elems']  = '.reload-biz_details';

                /*  Additional Server Response variable, validated in JavaScript    */
                $srv_resp['companyname']    = $field_value;
            }

            /*  Past Projects Completed - Year Began    */
            if (('ppc' == $arr_tb) && ('projectcompleted_year_began' == $field_name)) {
                $year_now           = (int)date('Y');
                $date_input         = (int)$field_value;

                if ($date_input > $year_now) {
                    $extra_msg          = 'Year began cannot be greater than the year now';
                }
                else {
                    $extra_validation   = STS_OK;
                }
            }
            /*  Future Growth Initiative - Project Benefit Date     */
            else if (('fgi' == $arr_tb) && ('proj_benefit_date' == $field_name)) {
                /*  Adds 01 to the Project Benefit Date             */
                $field_value   = $field_value.'-01';
                $extra_validation   = STS_OK;
            }
            /* Customer Segment no repeat */
            else if (('bizsegment' == $arr_tb) && ('businesssegment_name' == $field_name)){
                $existing = DB::table('tblbusinesssegment')->where($id_fld, $field_id)->first();
                $check = DB::table('tblbusinesssegment')->where($id_fld, '!=', $field_id)
                    ->where('entity_id', $existing->entity_id)
                    ->where('businesssegment_name', $field_value)
                    ->count();
                if($check > 0){
                    $extra_msg          = 'Duplicate entry.';
                } else {
                    $extra_validation   = STS_OK;
                }
            }
            /* No additional validations required   */
            else {
                $extra_validation   = STS_OK;
            }

            /*  Invalid Input   */
            if ($validator->fails() && ($entity['is_premium'] == PREMIUM_REPORT && ($field_name != "no_yrs_present_address" && $field_name != "date_established" 
                                        && $field_name != "number_year" && $field_name != "number_year_management_team" && $field_name != "negative_findings")))
            {
                $srv_resp['messages']	= $validator->messages()->all();
            }

            else if ($validator->fails())
            {
                $srv_resp['messages']	= $validator->messages()->all();
            }
            /*  Database value and current value is the same    */
            else if (($db_data->$field_name == $field_value)
            && (STS_NG == $multi_input)) {
                $srv_resp['refresh_value']  = self::convertDataByFldTyp($fld_typ, $fld_format, $srv_resp['refresh_value']);
                $srv_resp['sts']    = STS_OK;
            }
            else if ((EDITABLE_MONEY == $fld_typ)
            && (STR_EMPTY == $field_value)) {
                $srv_resp['refresh_value']  = STR_EMPTY;
                $srv_resp['sts']            = STS_OK;

                if (0 != $db_data->$field_name) {
                    $data_update = array(
                        $field_name => 0
                    );
                    $db_update  = DB::table($db_tbl)->where($id_fld, $field_id)->update($data_update);
                }
            }
            else if ((EDITABLE_PERCENT == $fld_typ)
            && (number_format($db_data->$field_name, 2) == number_format($field_value, 2))) {
                $srv_resp['refresh_value']  = self::convertDataByFldTyp($fld_typ, $fld_format, $srv_resp['refresh_value']);
                $srv_resp['sts']    = STS_OK;
            }
            /*  Additional validations failed   */
            else if (STS_NG == $extra_validation) {
                $srv_resp['messages'] = array($extra_msg);
            }
            /*  Valid Input */
            else
            {
                /*  Formats the Displayed value according to Field Type */
                $srv_resp['refresh_value']  = self::convertDataByFldTyp($fld_typ, $fld_format, $srv_resp['refresh_value']);

                /*  Initialize Data for Update  */
                $data_update = array(
                    $field_name => $field_value
                );


                /*  Additional Data for Update  */
                /*	Business Details 2 - Major Customers / Suppliers            */
                /*  Computes the years doing business with client or supplier   */
                if (('customer_started_years' == $field_name)
                || ('supplier_started_years' == $field_name)) {

                    $year_now           = date('Y');
                    $doing_business     = (int)$year_now - (int)$field_value;

                    $srv_resp['year_cntr']  = '#'.$field_id.'-auto-calc-years';
                    $srv_resp['year_value'] = $doing_business;

                    /*  Major Customer  */
                    if ('customer_started_years' == $field_name) {
                        /*  Adds data for database update   */
                        $data_update['customer_years_doing_business'] = $doing_business;
                    }
                    /*  Major Supplier  */
                    else if ('supplier_started_years' == $field_name) {
                        /*  Adds data for database update   */
                        $data_update['supplier_years_doing_business'] = $doing_business;
                    }
                    else {
                        /** No Processing   */
                    }
                }
                /*  Business Details 1 - Industry multi-inputs              */
                else if ('industry_main_id' == $field_name) {
                    /*  Adds data for database update   */
                    $data_update['industry_sub_id'] = Input::get('industry_sub_id');
                    $data_update['industry_row_id'] = Input::get('industry_row_id');
                }
                /*  Business Details 1 - date_established              */
                else if ('date_established' == $field_name) {
                    $dateNow = new DateTime();
                    $dateEstablished = new DateTime(request('date_established'));
                    $diff = $dateNow->diff($dateEstablished);
                    /*  Adds data for database update   */
                    $data_update['number_year'] = $diff->y;
                }
                /*  Business Details 1 - Provinces and Cities               */
                else if (isset($srv_resp['province'])) {
                    /*  Adds data for database update   */
                    $prov_data = Province2::find(Input::get('province'));

                    $data_update['province']    = $prov_data['provDesc'];
                    $data_update['cityid']      = Input::get('cityid');
                    $data_update['zipcode']     = $srv_resp['province']['zipcode'];
                }
                /*  Business Details 1 - Affiliates - Provinces and Cities  */
                else if (isset($srv_resp['affiliates_province'])) {
                    /*  Adds data for database update   */
                    $data_update['related_company_province']    = Input::get('related_company_province');
                    $data_update['related_company_cityid']      = Input::get('related_company_cityid');
                    $data_update['related_company_zipcode']     = $srv_resp['affiliates_province']['zipcode'];
                }
                /* Owners and Keymanagers */
                else if (isset($srv_resp['owner_province'])) {
                    /*  Adds data for database update   */
                    $data_update['province']    = Input::get('province');
                    $data_update['cityid']      = Input::get('cityid');
                    $data_update['zipcode']     = $srv_resp['owner_province']['zipcode'];
                }
                else if (isset($srv_resp['keymanager_province'])) {
                    /*  Adds data for database update   */
                    $data_update['province']    = Input::get('province');
                    $data_update['cityid']      = Input::get('cityid');
                    $data_update['zipcode']     = $srv_resp['keymanager_province']['zipcode'];
                }
                /*  Business Details 1 - Bank Application                   */
                else if ('is_independent' == $field_name) {
                    /*  Adds data for database update   */
                    $data_update['current_bank']    = 0;

                    if (1 == $field_value) {
                        $data_update['current_bank']    = Input::get('bank');
                    }

                    /*  Refreshes pages instead of overwriting existing data            */
                    /*  To refresh all document requirements according to chosen bank   */
                    $srv_resp['action']         = REFRESH_PAGE_ACT;
                }
                /*  Business Details 1 - Entity Type special condition      */
                else if ('entity_type' == $field_name) {
                    /*  Refreshes Container instead of overwriting                  */
                    /*  To display proper labels of fields and hide other fields    */
                    $srv_resp['action']         = REFRESH_PAGE_ACT;
                    $srv_resp['refresh_cntr']   = '#biz-details-cntr';
                    $srv_resp['refresh_url']    = URL::to('summary/smesummary/'.Session::get('entity')->entityid);
                    $srv_resp['refresh_elems']  = '.reload-biz_details';

                    /*  Additional Server Response variable, validated in JavaScript    */
                    $srv_resp['entity_type']    = $field_value;
                }
                else if ('purpose_credit_facility' == $field_name) {

                    if ('ecf' == $arr_tb) {
                        $srv_resp['purpose_others'] = '#ecf-other-purpose-'.$field_id;
                        $srv_resp['terms_select']   = '#ecf-credit-terms-'.$field_id;
                    }
                    else if ('rcl' == $arr_tb) {
                        $srv_resp['purpose_others'] = '#rcl-other-purpose-'.$field_id;
                        $srv_resp['terms_select']   = '#rcl-credit-terms-'.$field_id;
                    }
                    else {

                    }

                    $srv_resp['purpose_show']   = 0;
                    $srv_resp['terms_show']     = 0;

                    if ('4' == $field_value) {
                        $srv_resp['purpose_show']   = 1;
                    }
                    else if ('5' == $field_value) {
                        $srv_resp['terms_show']     = 1;
                    }
                    else {
                        /*  No Processing   */
                    }
                }
                /*  Business Details 1 - Total Asset Grouping update */
                else if ('total_assets' == $field_name) {
                    /*  Adds data for database update   */
                    if ($field_value >= 3000000 && $field_value <= 15000000) {
                        $groupingId = 1;
                    } else if ($field_value >= 15000000 && $field_value <= 100000000) {
                        $groupingId = 2;
                    } else if ($field_value > 100000000) {
                        $groupingId = 3;
                    } else {
                        $groupingId = 4;
                    }

                    $data_update['total_asset_grouping'] = $groupingId;
                    $label = self::$tb_editable_field[$arr_tb]['total_asset_grouping'][DATA_TBL_DROPDOWN_DATA];
                    $srv_resp['total_asset_grouping'] = array(
                        'id' => $data_update['total_asset_grouping'],
                        'label' => trans($label[$groupingId]),
                    );
                }

                /*  Updates the Database    */
                $db_update  = DB::table($db_tbl)->where($id_fld, $field_id)->update($data_update);

                /** Update Successful   */
                if ($db_update) {
                    $srv_resp['sts']    = STS_OK;

                    /** Updates Geocode data when address information changes   */
                    if ((('biz_details' == $arr_tb) && ('address1' == $field_name))
                    || (('biz_details' == $arr_tb) && ('province' == $field_name))) {
                        Geocoder::getGeocode($field_id);
                    }
                }
                /** Update Failed       */
                else {
                    $srv_resp['messages']    = array('An error occured while updating information. <a href="javascript:window.location.href=window.location.href">Please click here to refresh</a>.');
                }

                ActivityLog::saveLog();
            }

            /*	関数終亁E   */
            return json_encode($srv_resp);

        } catch (\Exception $e) {

            $logParams = array("activity" => "Questionnaire Update: " . $field_name, "stackTrace" => $e, "errorCode" => "d76f29b", "loginid" => Auth::user()->loginid);

            AuditLogs::saveActivityLog($logParams);

            \Log::info("d76f29b: " . $e);

            $srv_resp['messages'][0]    = 'An internal error occured while updating information. If problem persists, please contact site administrator. Error Code: d76f29b <a href="../../summary/smesummary/'.Session::get ('entity')->entityid.'">Please click here to refresh</a>.';

            return json_encode($srv_resp);
        }
        
    }

    //-----------------------------------------------------
    //  Function 53.3: postEditableFileUpload
    //      Validates and Saves data for Upload related fields
    //-----------------------------------------------------
    public function postEditableFileUpload($field_id, $field_name, $arr_tb)
    {
        /*  Variable Declaration    */
        $field_file                 = Input::file('editable-file-uploader');

        /*  Initialize the Server Response  */
        $srv_resp['sts']            = STS_NG;
        $srv_resp['messages']       = STR_EMPTY;

        $srv_resp['action']         = OVERWRITE_INFO_ACT;

        /*  Get Table Data  */
        $db_tbl     = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_DBNAME];
        $id_fld     = self::$tb_editable_field[$arr_tb][DATA_TBL_DBINFO][DATA_TBL_FLDNAME];
        $path       = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_UPLOAD_DATA]['path'];
        $prefix     = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_UPLOAD_DATA]['prefix'];
        $doc_name   = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_UPLOAD_DATA]['doc_hdr'];

        /*  Fetch the Database information of the edited field  */
        $db_data    = DB::table($db_tbl)->where($id_fld, $field_id)->first();

        /*  Validate Input Data */
        $rules      = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_FLDRULES];
        $error_msgs = self::$tb_editable_field[$arr_tb][$field_name][DATA_TBL_RULES_MSGS];
        $messages   = array();

        $data_val   = array(
            'editable-file-uploader' => $field_file
        );

        /*  Formats the Error message outputs according to the Editable table   */
        foreach ($error_msgs as $key => $val ) {

            /*  The Validation message has translation  */
            if (is_array($val)) {

                /** The field name is also translated   */
                if (isset($val[DATA_TBL_TRANS_LABEL])) {
                    $conv_attr  = trans($val[DATA_TBL_TRANS_FILE].'.'.$val[DATA_TBL_TRANS_LABEL]);
                }
                /** No translation for field name       */
                else {
                    $conv_attr  = $val[DATA_TBL_TRANS_FILE];
                }

                $messages[$key] = trans($val[DATA_TBL_TRANS_RULE], array('attribute' => $conv_attr));
            }
            /*  No Translation is used  */
            else {
                $messages[$key] = $val;
            }
        }

        /*  Validates Input via Laravel validation Engine   */
        $validator  = Validator::make(
            $data_val,
            $rules,
            $messages
        );

        /*  There are invalid inputs    */
        if ($validator->fails()) {
            /*	Gets the messages from the validation						*/
			$messages 				= $validator->messages();
			$srv_resp['messages']	= $messages->all();
        }
        /*  All inputs are valid    */
        else {
            /** Sets the filename and destination dir    */
            $destination_path           = $path;
            $extension                  = $field_file->getClientOriginalExtension();
            $filename                   = $prefix.$field_id.'-'.time().'.'.$extension;

            /** Upload the file    */
            $upload_success             = $field_file->move($destination_path, $filename);

            /** Data for Update     */
            $data_update = array(
				$field_name => $filename
            );

			// get details from excel
			if ('sec_generation' == $field_name) {
				if(pathinfo($filename, PATHINFO_EXTENSION) == 'xls'){
					$sec_array = [];
					$srv_resp['action'] = REFRESH_CNTR_ACT;
					$excel = Excel::load('incorporation-docs/'.$filename);
					$excel->setActiveSheetIndex(0);
					$sheet = $excel->getActiveSheet();
					if(trim((string)$sheet->getCell('A2')->getValue()) == 'GENERAL INFORMATION SHEET (GIS)') {
						if(trim((string)$sheet->getCell('A3')->getValue()) != 'NON-STOCK CORPORATION'){
							$sec_array['type'] = 'stock';
							if(trim((string)$sheet->getCell('A23')->getValue()) != '' &&
								trim((string)$sheet->getCell('N23')->getValue()) != '' &&
								trim((string)$sheet->getCell('O30')->getValue()) != '' &&
								trim((string)$sheet->getCell('A27')->getValue()) != '' &&
								trim((string)$sheet->getCell('O35')->getValue()) != '' &&
								trim((string)$sheet->getCell('A39')->getValue()) != ''
							){
								$sec_array['corporate_name'] = trim((string)$sheet->getCell('A23')->getValue());
								$sec_array['date_registered'] = date('Y-m-d', strtotime(trim((string)$sheet->getCell('N23')->getValue())));
								$sec_array['tin'] = trim((string)$sheet->getCell('O30')->getValue());
								$sec_array['sec_number'] = trim((string)$sheet->getCell('A27')->getValue());
								$sec_array['website_url'] = trim((string)$sheet->getCell('O32')->getValue());
								$sec_array['email'] = trim((string)$sheet->getCell('O35')->getValue());
								$sec_array['address'] = trim((string)$sheet->getCell('A34')->getValue());
								$sec_array['b_address'] = trim((string)$sheet->getCell('A39')->getValue());
							} else {
								$srv_resp['sts'] = STS_NG;
								$srv_resp['messages'][0] = 'Please fill out SEC General Information Sheet Template before uploading.';
								return json_encode($srv_resp);
							}

							//capital details
							$excel->setActiveSheetIndex(2);
							$sheet = $excel->getActiveSheet();
							$sec_array['authorized_capital'] = ($sheet->getCell('I17')->getCalculatedValue()) ? $sheet->getCell('I17')->getCalculatedValue() : 0;
							$sec_array['issued_capital'] = ($sheet->getCell('I32')->getCalculatedValue()) ? $sheet->getCell('I32')->getCalculatedValue() : 0;
							$sec_array['paidup_capital'] = ($sheet->getCell('I48')->getCalculatedValue()) ? $sheet->getCell('I48')->getCalculatedValue() : 0;

							//par value
							$total1 = ($sheet->getCell('M17')->getCalculatedValue()) ? $sheet->getCell('M17')->getCalculatedValue() : 0;
							$shares1 = ($sheet->getCell('O17')->getCalculatedValue()) ? $sheet->getCell('O17')->getCalculatedValue() : 0;
							$total2 = ($sheet->getCell('M32')->getCalculatedValue()) ? $sheet->getCell('M32')->getCalculatedValue() : 0;
							$shares2 = (($sheet->getCell('O24')->getCalculatedValue()) ? $sheet->getCell('O24')->getCalculatedValue() : 0) +
										(($sheet->getCell('O31')->getCalculatedValue()) ? $sheet->getCell('O31')->getCalculatedValue() : 0);
							$total3 = ($sheet->getCell('M48')->getCalculatedValue()) ? $sheet->getCell('M48')->getCalculatedValue() : 0;
							$shares3 = (($sheet->getCell('O40')->getCalculatedValue()) ? $sheet->getCell('O40')->getCalculatedValue() : 0) +
										(($sheet->getCell('O47')->getCalculatedValue()) ? $sheet->getCell('O47')->getCalculatedValue() : 0);

							$par1 = ($shares1 > 0) ? $total1 / $shares1 : 0;
							$par2 = ($shares2 > 0) ? $total2 / $shares2 : 0;
							$par3 = ($shares3 > 0) ? $total3 / $shares3 : 0;

							$sec_array['par_value'] = $par2 + $par3;

							//directors
							$excel->setActiveSheetIndex(3);
							$sheet = $excel->getActiveSheet();
							$sec_array['directors'] = [];

							for($x=13; $x<=41; $x=$x+2){
								$dir_name = trim((string)$sheet->getCell('C'.$x)->getValue());
								if($dir_name!=''){
									$sec_array['directors'][] = array(
										'name' => $dir_name,
										'id_no' => trim((string)$sheet->getCell('M'.$x)->getValue()),
										'address' => trim((string)$sheet->getCell('C'.($x+1))->getValue()),
										'nationality' => ''
									);
								}
							}

							//shareholders
							$excel->setActiveSheetIndex(4);
							$sheet = $excel->getActiveSheet();
							$sec_array['shareholders'] = [];

							for($x=13; $x<=37; $x=$x+4){
								$shareholders_name = trim((string)$sheet->getCell('C'.$x)->getValue());
								if($shareholders_name!=''){
									$sec_array['shareholders'][] = array(
										'name' => $shareholders_name,
										'address' => trim((string)$sheet->getCell('C'.($x+2))->getValue()),
										'amount' => trim((string)$sheet->getCell('H'.$x)->getValue()),
										'percentage_share' => trim((string)$sheet->getCell('G'.$x)->getValue()),
										'id_no' => trim((string)$sheet->getCell('I'.$x)->getValue()),
										'nationality' => trim((string)$sheet->getCell('C'.($x+1))->getValue())
									);
								}
							}

							$excel->setActiveSheetIndex(5);
							$sheet = $excel->getActiveSheet();

							for($x=13; $x<=37; $x=$x+4){
								$shareholders_name = trim((string)$sheet->getCell('B'.$x)->getValue());
								if($shareholders_name!=''){
									$sec_array['shareholders'][] = array(
										'name' => $shareholders_name,
										'address' => trim((string)$sheet->getCell('B'.($x+2))->getValue()),
										'amount' => trim((string)$sheet->getCell('G'.$x)->getValue()),
										'percentage_share' => trim((string)$sheet->getCell('F'.$x)->getValue()),
										'id_no' => trim((string)$sheet->getCell('H'.$x)->getValue()),
										'nationality' => trim((string)$sheet->getCell('B'.($x+1))->getValue())
									);
								}
							}

							$excel->setActiveSheetIndex(6);
							$sheet = $excel->getActiveSheet();

							for($x=14; $x<=34; $x=$x+4){
								$shareholders_name = trim((string)$sheet->getCell('C'.$x)->getValue());
								if($shareholders_name!=''){
									$sec_array['shareholders'][] = array(
										'name' => $shareholders_name,
										'address' => trim((string)$sheet->getCell('C'.($x+2))->getValue()),
										'amount' => trim((string)$sheet->getCell('H'.$x)->getValue()),
										'percentage_share' => trim((string)$sheet->getCell('G'.$x)->getValue()),
										'id_no' => trim((string)$sheet->getCell('I'.$x)->getValue()),
										'nationality' => trim((string)$sheet->getCell('C'.($x+1))->getValue())
									);
								}
							}


						} else {
							$sec_array['type'] = 'non-stock';
							if(trim((string)$sheet->getCell('E29')->getValue()) != '' &&
								trim((string)$sheet->getCell('L30')->getValue()) != '' &&
								trim((string)$sheet->getCell('L34')->getValue()) != '' &&
								trim((string)$sheet->getCell('E33')->getValue()) != '' &&
								trim((string)$sheet->getCell('L38')->getValue()) != '' &&
								trim((string)$sheet->getCell('E41')->getValue()) != ''
							){
								$sec_array['corporate_name'] = trim((string)$sheet->getCell('E29')->getValue());
								$sec_array['date_registered'] = date('Y-m-d', strtotime(trim((string)$sheet->getCell('L30')->getValue())));
								$sec_array['tin'] = trim((string)$sheet->getCell('L34')->getValue());
								$sec_array['sec_number'] = trim((string)$sheet->getCell('E33')->getValue());
								$sec_array['website_url'] = trim((string)$sheet->getCell('L36')->getValue());
								$sec_array['email'] = trim((string)$sheet->getCell('L38')->getValue());
								$sec_array['address'] = trim((string)$sheet->getCell('E39')->getValue());
								$sec_array['b_address'] = trim((string)$sheet->getCell('E41')->getValue());
							} else {
								$srv_resp['sts'] = STS_NG;
								$srv_resp['messages'][0] = 'Please fill out SEC General Information Sheet Template before uploading.';
								return json_encode($srv_resp);
							}

							//directors
							$excel->setActiveSheetIndex(2);
							$sheet = $excel->getActiveSheet();
							$sec_array['directors'] = [];

							for($x=8; $x<=50; $x=$x+3){
								$dir_name = trim((string)$sheet->getCell('B'.$x)->getValue());
								if($dir_name!=''){
									$sec_array['directors'][] = array(
										'name' => $dir_name,
										'id_no' => trim((string)$sheet->getCell('I'.$x)->getValue()),
										'address' => trim((string)$sheet->getCell('A'.($x+1))->getValue()),
										'nationality' => trim((string)$sheet->getCell('D'.$x)->getValue())
									);
								}
							}
						}
						$company_details = Entity::where('entityid', Session::get('entity')->entityid)->first();

						//check existing
						if(($company_details->companyname != $sec_array['corporate_name']) ||
							($company_details->sec_reg_date!='0000-00-00' && $company_details->sec_reg_date != $sec_array['date_registered']) ||
							($company_details->company_tin!='' && $company_details->company_tin != $sec_array['tin']) ||
							($company_details->tin_num!=null && $company_details->tin_num != $sec_array['sec_number']) ||
							($company_details->website!='' && $company_details->website != $sec_array['website_url'])
						) {
							//set to session temp
							$srv_resp['action'] = 3;
							Session::put('temp_sec_gen', json_encode($sec_array));
							$srv_resp['sts'] = STS_OK;
						} else {
							// if($company_details->companyname=='') $company_details->companyname = $sec_array['corporate_name'];
							if($company_details->sec_reg_date=='0000-00-00') $company_details->sec_reg_date = $sec_array['date_registered'];
							if($company_details->company_tin=='') $company_details->company_tin = $sec_array['tin'];
							if($company_details->tin_num==null) $company_details->tin_num = $sec_array['sec_number'];
							if($company_details->website=='') $company_details->website = $sec_array['website_url'];
							// if($company_details->email=='') $company_details->email = $sec_array['email'];
							// parse address
							if(ENVIRONMENT!=ENV_LOCAL){
								if(trim($sec_array['b_address'])!=""){
									$address_data = Geocoder::getAddressBreakdown($sec_array['b_address']);
									if(isset($address_data['city'])){
										$zipcode = DB::table('zipcodes')->whereOr('major_area', 'LIKE', '%'.$address_data['province'].'%')
													->whereOr('city', 'LIKE', '%'.$address_data['city'].'%')->first();

										if($zipcode){
											$address1 = str_ireplace($address_data['province'], '', $sec_array['b_address']);
											$address1 = str_ireplace($address_data['city'], '', $address1);
											$address1 = str_replace(',', '', $address1);
											if($company_details->address1=='') $company_details->address1 = $address1;
											if($company_details->cityid==0) $company_details->cityid = $zipcode->id;
											if($company_details->province=='') $company_details->province = $zipcode->major_area;
											if($company_details->zipcode==0) $company_details->zipcode = $zipcode->zip_code;
										}
									}
								} else {
									$srv_resp['sts'] = STS_NG;
									$srv_resp['messages'][0] = 'The address in SEC General Information Sheet Template is invalid';
									return json_encode($srv_resp);
								}
							}
							$company_details->save();

							//directors
							foreach($sec_array['directors'] as $dir){
								$check_director = Director::where('name', $dir['name'])->where('entity_id', Session::get('entity')->entityid)->first();
								if(!$check_director){
									DB::table('tbldirectors')->insert(array(
										'entity_id' => Session::get('entity')->entityid,
										'name' => $dir['name'],
										'id_no' => $dir['id_no'],
										'address' => $dir['address'],
										'nationality' => $dir['nationality']
									));
								}
							}

							//capital details
							if($sec_array['type'] == 'stock'){

								$capital = Capital::firstOrNew(
									array(
										'entity_id' => Session::get('entity')->entityid
									)
								);
								$capital->entity_id = Session::get('entity')->entityid;
								$capital->capital_authorized = $sec_array['authorized_capital'];
								$capital->capital_issued = $sec_array['issued_capital'];
								$capital->capital_paid_up = $sec_array['paidup_capital'];
								//$capital->capital_ordinary_shares = '';
								$capital->capital_par_value = $sec_array['par_value'];
								$capital->save();

								//shareholders
								foreach($sec_array['shareholders'] as $dir){
									$check_shareholder = Shareholder::where('name', $dir['name'])->where('entity_id', Session::get('entity')->entityid)->first();
									if(!$check_shareholder){
										DB::table('tblshareholders')->insert(array(
											'entity_id' => Session::get('entity')->entityid,
											'name' => $dir['name'],
											'address' => $dir['address'],
											'amount' => (float)$dir['amount'],
											'percentage_share' => (float)str_replace('%','',$dir['percentage_share']) * 100,
											'id_no' => $dir['id_no'],
											'nationality' => $dir['nationality']
										));
									}
								}
							}
						}
					} else {
						$srv_resp['sts'] = STS_NG;
						$srv_resp['messages'][0] = 'Please download a SEC General Information Sheet Template';
						return json_encode($srv_resp);
					}
				} elseif(pathinfo($filename, PATHINFO_EXTENSION) == 'pdf'){
				} else {
					$srv_resp['sts'] = STS_NG;
					$srv_resp['messages'][0] = 'SEC General Information Sheet must be a an XLS template or PDF file';
					return json_encode($srv_resp);
				}
			}

            /** Update Database     */
            $db_update  = DB::table($db_tbl)->where($id_fld, $field_id)->update($data_update);

            /** Update Successful   */
            if ($db_update) {
				$srv_resp['sts']            = STS_OK;
                $srv_resp['refresh_cntr']   = '#'.$prefix.$field_id;
                $srv_resp['refresh_value']  = '<a href="'.URL::to('/download/'.$path.'/'.$filename).'" target="_blank"> Download '.$doc_name.' </a>';
			}
            /** Update Failed       */
			else {
				$srv_resp['messages'][0]    = 'An error occured while adding Existing Credit Facilities. <a href="../../summary/smesummary/'.Session::get('entity')->entityid.'">Please click here to return</a>.';
			}

			ActivityLog::saveLog();
        }

        /*	関数終亁E   */
		return json_encode($srv_resp);
    }

    //-----------------------------------------------------
    //  Function 53.4: convertDataByFldTyp
    //      Converts the Data according to the field type
    //      indicated in the table
    //-----------------------------------------------------
    private static function convertDataByFldTyp($fld_typ, $fld_format, $fld_val)
    {
        $conv_value = NULL;

        /*  Formats the Displayed value according to Field Type */
        switch ($fld_typ) {

            /*  Monetary Value Field        */
            case EDITABLE_MONEY:
                if (STR_EMPTY != $fld_val) {
                    $conv_value = number_format($fld_val, 2);
                }
                else {
                    $conv_value = STR_EMPTY;
                }
                break;

            /*  Percentage value field  */
            case EDITABLE_PERCENT:
                $conv_value = number_format($fld_val, 2).'%';
                break;

            /*  Date value field        */
            case EDITABLE_DATE:
                $conv_value = date($fld_format, strtotime($fld_val));
                break;

            /*  Paragraph value field   */
            case EDITABLE_PARAGRAPH:
                $conv_value = nl2br($fld_val);
                break;

            /*  With suffix field       */
            case EDITABLE_SUFFIX:
                $conv_value = $fld_val.$fld_format;
                break;

            default:
                $conv_value = $fld_val;
        }

        return $conv_value;
    }

    //-----------------------------------------------------
    //  Function 53.5: getUpdateNewGis
    //      Gets the latest uploaded GIS
    //-----------------------------------------------------
	public function getUpdateNewGis()
	{
		if(Session::has('temp_sec_gen')){
			$company_details = Entity::where('entityid', Session::get('entity')->entityid)->first();
			$sec_array = json_decode(Session::get('temp_sec_gen'), true);
			$company_details->companyname = $sec_array['corporate_name'];
			$company_details->sec_reg_date = $sec_array['date_registered'];
			$company_details->company_tin = $sec_array['tin'];
			$company_details->tin_num = $sec_array['sec_number'];
			$company_details->website = $sec_array['website_url'];
			$company_details->email = $sec_array['email'];
			// parse address
			if(ENVIRONMENT!=ENV_LOCAL){
				if(trim($sec_array['b_address'])!=""){
					$address_data = Geocoder::getAddressBreakdown($sec_array['b_address']);
					if(isset($address_data['city'])){
						$zipcode = DB::table('zipcodes')->whereOr('major_area', 'LIKE', '%'.$address_data['province'].'%')
								->whereOr('city', 'LIKE', '%'.$address_data['city'].'%')->first();

						if($zipcode){
							$address1 = str_ireplace($address_data['province'], '', $sec_array['b_address']);
							$address1 = str_ireplace($address_data['city'], '', $address1);
							$address1 = str_replace(',', '', $address1);
							if($company_details->address1=='') $company_details->address1 = $address1;
							if($company_details->cityid==0) $company_details->cityid = $zipcode->id;
							if($company_details->province=='') $company_details->province = $zipcode->major_area;
							if($company_details->zipcode==0) $company_details->zipcode = $zipcode->zip_code;
						}
					}
				}
			}
			$company_details->save();

			if($sec_array['type'] == 'stock'){

				$capital = Capital::firstOrNew(
					array(
						'entity_id' => Session::get('entity')->entityid
					)
				);
				$capital->entity_id = Session::get('entity')->entityid;
				$capital->capital_authorized = $sec_array['authorized_capital'];
				$capital->capital_issued = $sec_array['issued_capital'];
				$capital->capital_paid_up = $sec_array['paidup_capital'];
				//$capital->capital_ordinary_shares = '';
				$capital->capital_par_value = $sec_array['par_value'];
				$capital->save();
			}
		}

		return "success";
    }
    
    //-----------------------------------------------------
    //  Function: get_string_between
    //      Get string between two string
    //-----------------------------------------------------
    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    //-----------------------------------------------------
    //  Function: downloadFromUrl
    //      Download Annual Report from URL and Save it in pse_fs_attachments folder
    //-----------------------------------------------------
    function downloadFromUrl($url, $dir, $file_name, $pse_id)
    {
        // Initialize the cURL session 
        $ch = curl_init($url);
          
        // Inintialize directory name where 
        // file will be save 
        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        // Use basename() function to return
        // the base name of file  
        $file_name = $file_name;
          
        // Save file into file location 
        $save_file_loc = $dir . $file_name; 
          
        // Open file  
        $fp = fopen($save_file_loc, 'wb'); 
          
        // It set an option for a cURL transfer 
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_HEADER, 0); 
          
        // Perform a cURL session 
        curl_exec($ch); 
          
        // Closes a cURL session and frees all resources 
        curl_close($ch); 
          
        // Close file 
		fclose($fp); 
    }
}
