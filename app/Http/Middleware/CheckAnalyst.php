<?php

namespace App\Http\Middleware;

use Closure;

class CheckAnalyst
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user() == null || Auth::user() == "" || Auth::user()->role != 3) {
            if(Request::ajax()) {
                return Response::make('Unauthorized', 401);
            } else {
                return Response::make('Unauthorized', 401);
            }
        }

        return $next($request);
    }
}
