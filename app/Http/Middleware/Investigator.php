<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Request;
use Response;
use Redirect;


class Investigator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            if (Request::ajax()) {
                return Response::make('Unauthorized', 401);
            } else {
                return Redirect::guest('login');
            }
        } elseif (Auth::user() && Auth::user()->role != 2) {
            return Response::make('Unauthorized', 401);
	    }

        return $next($request);
    }
}
