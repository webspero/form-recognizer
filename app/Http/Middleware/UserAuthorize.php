<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Request;
use Response;
use Redirect;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class UserAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()) {
            if(Request::ajax()) {
                return Response::make('Unauthorized', 401);
            } else {
                return Redirect::guest('login');
            }
        }
        App::setLocale(Session::get('language'));

        return $next($request);
    }
}
