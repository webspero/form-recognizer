<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class Paid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->role == 1) {
            if (Auth::user()->status == 1) {
                return Redirect::to('/payment/index');
            }
        }
        
        return $next($request);
    }
}
