<?php

namespace App\Http\Middleware;

use CreditBPO\Models\DeveloperApiUser;
use CreditBPO\Controllers\API\v1\ApiController;

use Closure;
use Request;

class ApiClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*Check header if contains client id and secret key value*/
        $clientId = Request::header('Client-Id');
        $clientSecret = Request::header('Client-Secret');

        $apiController = new ApiController;

        if (!is_null($clientId) && !is_null($clientSecret)){
            /* Check if client id and secret key exist */
            $apiUserDb = new DeveloperApiUser();
            $isExist = $apiUserDb->checkApiKey($clientId, $clientSecret);
            
            /* If not exist, throw a forbidden error response */
            if (!$isExist){
                return $apiController->error('Invalid client id and secret key.', HTTP_STS_UNAUTHORIZED);
            }

        }
        /* If the header doesn't contains client id and secret key value, throw a forbidden error response */
        else{
            return $apiController->error('Client id and secret key are required.', HTTP_STS_UNAUTHORIZED);

        }
        return $next($request);
    }
}
