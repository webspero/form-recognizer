<?php
//======================================================================
//  Class M13: Branches
//      Branches Table Database Class
//======================================================================
class Branches extends Eloquent {
	
	protected $table = 'tblbranches';
	protected $primaryKey = 'branchid';

}
