<?php
//======================================================================
//  Class M32: Director
//      Director Table Database Class
//======================================================================
class Director extends Eloquent {
	
	protected $table = 'tbldirectors';
	protected $guarded = 'id, created_at, updated_at';

}
