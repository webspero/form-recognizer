<?php

//======================================================================
//  Class M57: Key Manager
//      Key Manager Table Database Model
//======================================================================
class KeyManager extends Eloquent
{	
	protected $table        = 'tblkeymanagers';
	protected $primaryKey   = 'keymanagerid';
	protected $guarded      = array('keymanagerid', 'created_at', 'updated_at');
	
}