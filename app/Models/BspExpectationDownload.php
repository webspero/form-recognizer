<?php
//======================================================================
//  Class M15: BspExpectationDownload
//      BspExpectationDownload Table Database Class
//======================================================================
class BspExpectationDownload extends Eloquent {

	/*-------------------------------------------------------------------------
	|	Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database Table
	|	primaryKey	--- Primary key of the Projects Table
	|	timestamps	--- Prevents created_at and deleted_at fields from being required
	|------------------------------------------------------------------------*/
	protected 	$table 		= 'bsp_expectation_downloads';  /* The database table used by the model.    */
	protected 	$primaryKey = 'bsp_file_id';                /* The table's primary key                  */
	public		$timestamps = false;
    
	//-----------------------------------------------------
    //  Function M15.1: getBspFile
    //  	Retrieves the BSP Expectation report by Filename
    //-----------------------------------------------------
    public function getBspFile($filename)
    {
        $data   = self::where('status', ACTIVE)
            ->where('filename', $filename)
            ->orderBy('bsp_file_id', 'desc')
            ->first();
        
        return $data;
    }
    
	//-----------------------------------------------------
    //  Function M15.2: addBspFile
    //  	Adds a new downloaded BSP Business Expectation Report
    //-----------------------------------------------------
    public function addBspFile($data)
    {
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $sts        = STS_NG;
        
        /*--------------------------------------------------------------------
		/*	Instantiate Database Class
		/*------------------------------------------------------------------*/
        $biz_expectation    = new BspExpectationDownload;
        
        /*--------------------------------------------------------------------
		/*	Saves Data to the Database
		/*------------------------------------------------------------------*/
        $biz_expectation->filename          = $data['filename'];
        $biz_expectation->status            = ACTIVE;
        $biz_expectation->date_uploaded     = date('Y-m-d');
        
        /*--------------------------------------------------------------------
		/*	Save to Database
		/*------------------------------------------------------------------*/
        $sts    = $biz_expectation->save();
        
        return $sts;
    }
}
