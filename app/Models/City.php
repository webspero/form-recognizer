<?php
//======================================================================
//  Class M24: City
//      City Table Database Class
//======================================================================
class City extends Eloquent {
	
	protected $table        = 'tblcity';
    protected $primaryKey   = 'cityid';

}
