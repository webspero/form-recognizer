<?php

class Currency extends Eloquent {
    protected $table = 'currency'; //table
    protected $guarded = array('id'); // locked fields
    protected $primaryKey = 'id';
}
