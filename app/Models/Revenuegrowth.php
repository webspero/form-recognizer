<?php

//======================================================================
//  Class M86: Revenue Growth Potential
//      Revenue Growth Potential Table Database Model
//======================================================================
class Revenuegrowth extends Eloquent
{	
	protected $table = 'tblrevenuegrowthpotential';
	protected $primaryKey = 'growthpotentialid';
}
