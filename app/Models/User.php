<?php

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

//======================================================================
//  Class M97: User Account
//      User Account Database Model
//======================================================================
class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	
	protected $primaryKey = 'loginid';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tbllogin';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
    
    //-----------------------------------------------------
    //  Function M97.1: getUserByEmail
    //      Gets a User according to Email
    //-----------------------------------------------------
    function getUserByEmail($email)
    {
        $user_data      = self::where('email', $email)->first();

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.2: paginateUserCompanyByRole
    //      Gets a limited number of User
    //-----------------------------------------------------
    function paginateUserCompanyByRole($role, $limit)
    {
        $user_data = self::leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
            ->where('tbllogin.role', $role)
            ->where('tbllogin.status', 2)
            ->select(
                'tbllogin.loginid',
                'tbllogin.name',
                'tbllogin.firstname',
                'tbllogin.lastname', 
                'tbllogin.email', 
                
                'tblentity.companyname'
            )
            ->paginate($limit);

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.3: getUserCompanyByRole
    //      Gets the Accounts first Report
    //-----------------------------------------------------
    function getUserCompanyByRole($user_id)
    {
        $user_data = self::leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
            ->where('tbllogin.loginid', $user_id)
            ->select(
                'tbllogin.loginid',
                'tbllogin.firstname',
                'tbllogin.lastname', 
                'tbllogin.email', 
                
                'tblentity.entityid',
                'tblentity.companyname'
            )
            ->first();

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.4: updateUserPassword
    //      Change the User's Password
    //-----------------------------------------------------
    function updateUserPassword($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        
        /*  Gets User Information       */
        $request    = self::where('email', $data['email'])
            ->first();
        
        /*  Save changes to the Database    */
        $request['password']    = Hash::make($data['new_password']);
        $request['updated_at']  = $data['date_updated'];
        
        $request->save();
        
        return $sts;
    }

    public function checkIfOrganizationUsed($organizationId)
    {
        $supervisor = self::where('bank_id', $organizationId)
                          ->where('role', 4)->get();
        if ($supervisor->count() > 0){
            return true;
        } else{
            return false;
        }
    }

}
