<?php

//======================================================================
//  Class M68: Owner Details
//      Owner Details Table Database Model
//======================================================================
class Owner extends Eloquent
{	
	protected $table = 'tblowner';
	protected $primaryKey = 'ownerid';
}
