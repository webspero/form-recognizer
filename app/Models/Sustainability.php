<?php

//======================================================================
//  Class M94: Condition and Sustainability
//      Condition and Sustainability Database Model
//======================================================================
class Sustainability extends Eloquent
{	
	protected $table        = 'tblsustainability';
    protected $primaryKey   = 'sustainabilityid';        /* The table's primary key                  */
	protected $guarded      = array('sustainabilityid', 'created_at', 'updated_at');
}
