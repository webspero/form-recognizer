<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnalysisResult extends Model
{
    protected $guarded = array('id', 'created_at', 'updated_at');
}
