<?php

//======================================================================
//  Class M60: Major Customer
//      Major Customer Table Database Model
//======================================================================
class Majorcustomer extends Eloquent
{	
	protected $table = 'tblmajorcustomer';
	protected $primaryKey = 'majorcustomerrid';

}
