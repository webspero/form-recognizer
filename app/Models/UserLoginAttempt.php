<?php

//======================================================================
//  Class M99: User Login Attempt
//      User Login Attempt Table Database Model
//======================================================================
class UserLoginAttempt extends Eloquent
{
	/*-------------------------------------------------------------------------
	|	Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database Table
	|	primaryKey	--- Primary key of the Projects Table
	|	timestamps	--- Prevents created_at and deleted_at fields from being required
	|------------------------------------------------------------------------*/
	protected 	$table 		= 'user_login_attempts';      /* The database table used by the model.    */
	protected 	$primaryKey = 'login_attempt_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M99.1: getUserAttempt
    //      Fetches the User's Login Attempts from the Database
    //-----------------------------------------------------
    public function getUserAttempt($login_id)
    {
        $data   = self::where('login_id', $login_id)->first();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M99.2: addLoginAttempt
    //      Adds a new User Attempt Data
    //-----------------------------------------------------
    public function addLoginAttempt($data)
    {
        /*  Variable Declaration        */
        $sts        = STS_NG;
        $country    = STR_EMPTY;
        
        /*  Instantiate Database Class  */
        $user_attempt   = new UserLoginAttempt;
        $mal_attmpt_lib = new MaliciousAttemptLib;
        
        /*  Get country via IP Address  */
        $geo_data  = $mal_attmpt_lib->getClientCountry();
        
        /*  Saves Data to the Database  */
        $user_attempt->login_id         = $data['login_id'];
        $user_attempt->attempts         = 0;
        $user_attempt->country_code     = $geo_data['country_code'];
        $user_attempt->status           = STS_NG;
        $user_attempt->last_login       = date('Y-m-d H:i:s');
        
        $sts    = $user_attempt->save();
        
        return $user_attempt->login_attempt_id;
    }
    
    //-----------------------------------------------------
    //  Function M99.3: updateAttemptSts
    //      Resets or Increment the Login Attempts of a User
    //-----------------------------------------------------
    public function updateAttemptSts($data, $attempt_sts)
    {
        /*  Variable Declaration        */
        $sts            = STS_NG;
        $msg            = STR_EMPTY;
        $attempt_count  = STR_EMPTY;
        
        /*  Instantiate Database Class  */
        $mal_attmpt_lib = new MaliciousAttemptLib;
        
        /*  Get country via IP Address  */
        $geo_data       = $mal_attmpt_lib->getClientCountry();
        
        /*  Instantiate Database Class  */
        $user_attempt   = self::find($data['attempt_id']);
        
        /*  Resets attempt counter      */
        if (STS_OK == $attempt_sts) {
            $attempt_count  = 0;
        }
        /*  Increments attempt counter  */
        else if (STS_NG == $attempt_sts) {
            $attempt_count  = $user_attempt->attempts + 1;
        }
        
        /*  Saves Data to the Database  */
        $user_attempt->attempts         = $attempt_count;
        $user_attempt->country_code     = $geo_data['country_code'];
        $user_attempt->status           = $attempt_sts;
        $user_attempt->last_login       = date('Y-m-d H:i:s');
        
        $sts    = $user_attempt->save();
        
        return $msg;
    }
}