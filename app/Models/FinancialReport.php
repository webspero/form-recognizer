<?php
//======================================================================
//  Class M40: FinancialReport
//      FinancialReport Table Database Class
//======================================================================
class FinancialReport extends Eloquent {

	protected $table = 'financial_reports';
	protected $guarded = array('id', 'created_at', 'updated_at');

	//-----------------------------------------------------
    //  Function M40.1: balanceSheets
    //  	gets all the balance sheet connected to this financial report
    //-----------------------------------------------------
	public function balanceSheets()
    {
        //JM
        //return $this->hasMany('BalanceSheet', 'financial_report_id');
        return $this->hasMany(BalanceSheet::class);
    }

	//-----------------------------------------------------
    //  Function M40.2: incomeStatements
    //  	gets all the income statements connected to this financial report
    //-----------------------------------------------------
	public function incomeStatements()
    {
        //JM
        //return $this->hasMany('IncomeStatement', 'financial_report_id');
        return $this->hasMany(IncomeStatement::class);
    }

	//-----------------------------------------------------
    //  Function M40.3: cashFlow
    //  	gets all the cash flow connected to this financial report
    //-----------------------------------------------------
	public function cashFlow()
    {
        //JM
        //return $this->hasMany('CashFlow', 'financial_report_id');
        return $this->hasMany(CashFlow::class);
    }

    //-----------------------------------------------------
    //  Function M40.4: keyRatio
    //  	gets all the key ratio connected to this financial report
    //-----------------------------------------------------
    public function keyRatio()
    {
        return $this->hasMany(ReadyratioKeySummary::class);
    }

    public function deleteOldFSInput($entity_id) {
        $oldFS = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])->get();

        if ($oldFS) {
            DB::beginTransaction();

            foreach ($oldFS as $fs) {
                BalanceSheet::where('financial_report_id', $fs->id)->update(['is_deleted' => 1]);
                IncomeStatement::where('financial_report_id', $fs->id)->update(['is_deleted' => 1]);
                CashFlow::where('financial_report_id', $fs->id)->update(['is_deleted' => 1]);
                FinancialReport::find($fs->id)->update(['is_deleted' => 1]);
            }

            DB::commit();
        }

        return true;
    }
}
