<?php

//======================================================================
//  Class M101: Zipcode
//      Zipcode Table Database Model
//======================================================================
class Zipcode extends Eloquent
{	
	protected $table = 'zipcodes';
    
    //-----------------------------------------------------
    //  Function M101.1: getRegionAll
    //      Get all Regions
    //-----------------------------------------------------
	public static function getRegionAll()
	{
		return DB::table('zipcodes')->select(DB::raw('DISTINCT region'));
	}
}
