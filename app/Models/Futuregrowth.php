<?php
//======================================================================
//  Class M41: Futuregrowth
//      Futuregrowth Table Database Class
//======================================================================
class Futuregrowth extends Eloquent {
	
	protected $table = 'tblfuturegrowth';
	protected $primaryKey = 'futuregrowthid';

}
