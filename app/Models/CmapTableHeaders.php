<?php
//======================================================================
//  Class M25: CmapTableHeaders
//      refcitymun Table Database Class
//======================================================================
class CmapTableHeaders extends Eloquent {
	protected $table        = 'cmap_table_headers';
    protected $primaryKey   = 'id';

}
