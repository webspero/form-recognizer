<?php
//======================================================================
//  Class M24: City2
//      refcitymun Table Database Class
//======================================================================
class ChecklistType extends Eloquent {
	
	protected $table        = 'checklist_type';
    protected $primaryKey   = 'id';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
