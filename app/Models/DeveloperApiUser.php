<?php

//======================================================================
//  Class M31: Developer API User
//      Developer API User Table Database Model
//======================================================================
class DeveloperApiUser extends Eloquent
{
	protected 	$table 		= 'developer_api_users';    /* The database table used by the model.    */
	protected 	$primaryKey = 'developer_id';           /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M31.1: getActiveDevelopers
    //      Fetches all Active Developers
    //-----------------------------------------------------
    public function getActiveDevelopers()
    {
        $data   = self::orderBy('client_id', 'desc')
            ->get();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M31.2: addNewApiUser
    //      Adds a new Developer API User
    //-----------------------------------------------------
    public function addNewApiUser($data)
    {
        /*  Variable Declaration        */
        $sts        = STS_NG;
        $idx        = 0;
        $scare_crow = STR_EMPTY;
        $characters = array_merge(range('A','Z'), range('a','z'));
        $max        = @count($characters) - 1;
        
        /*  Instantiate Database Class  */
        $add_dev                    = new DeveloperApiUser;
        
        /*  Create an 8 character random key    */
        for ($ctr = 0; 8 > $ctr; $ctr++) {
            $rand       = mt_rand(0, $max);
            $scare_crow .= $characters[$rand];
        }
        
        /*  Saves Data to the Database          */
        $add_dev->firstname         = $data['fname'];
        $add_dev->lastname          = $data['lname'];
        $add_dev->email             = $data['email'];
        $add_dev->password          = sha1($data['pword']);
        $add_dev->status            = ACTIVE;
        $add_dev->date_registered   = date('Y-m-d');
        $add_dev->last_activity     = date('Y-m-d');
        $add_dev->secret_key        = $scare_crow;

        $sts                        = $add_dev->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M31.3: updateDeveloperStatus
    //      Activate or Deactivate a Developer's Account
    //-----------------------------------------------------
    public function updateDeveloperStatus($sts, $developer_id)
    {
        /*  Gets the developer information from the database    */
        $developer = self::find($developer_id);
        
        if ($developer) {
            /*  Activate developer when Inactive    */
            if (ACTIVE == $sts) {
                $developer->status = ACTIVE;
            }
            /*  Deactivate developer when Active    */
            else {
                $developer->status = DELETED;
            }
            
            $developer->save();
        }
        
    }
}