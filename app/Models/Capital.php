<?php
//======================================================================
//  Class M21: Capital
//      Capital Table Database Class
//======================================================================
class Capital extends Eloquent {
	
	protected $table        = 'tblcapital';    
    protected $primaryKey   = 'capitalid';
    protected $fillable     = ['entity_id','capital_authorized','capital_issued','capital_paid_up','capital_ordinary_shares','capital_par_value'];
	protected $guarded      = ['capitalid', 'created_at', 'updated_at'];


}
