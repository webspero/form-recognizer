<?php
//======================================================================
//  Class M105: ShuftiUserDocument
//      shufti_user_documents Table Database Class
//======================================================================
class ShuftiUserDocument extends Eloquent
{
    protected $table        = 'shufti_user_documents';
    protected $primaryKey   = 'id';
}
