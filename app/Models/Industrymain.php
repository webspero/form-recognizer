<?php
//======================================================================
//  Class M48: Industrymain
//      Industrymain Table Database Class
//======================================================================
class Industrymain extends Eloquent {
	
	protected $table = 'tblindustry_main';
	protected $primaryKey = 'industry_main_id';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
