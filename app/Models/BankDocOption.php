<?php
//======================================================================
//  Class M5: BankDocOption
//      BankDocOption Table Database Class
//======================================================================
class BankDocOption extends Eloquent {
	
	protected $table = 'bank_doc_options';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
