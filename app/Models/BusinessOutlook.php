<?php

//======================================================================
//  Class M17: Business Outlook
//      Business Outlook Table Database Model
//======================================================================
class BusinessOutlook extends Eloquent
{
	protected 	$table 		= 'business_outlooks';      /* The database table used by the model.    */
	protected 	$primaryKey = 'business_outlook_id';    /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M17.1: getBusinessOutlookByIndustry
    //      Gets the Business Outlook By Industry ID
    //-----------------------------------------------------
    public function getBusinessOutlookByIndustry($industry_id)
    {
        $biz_outlook_data   = self::where('industry_id', $industry_id)->first();
        
        return $biz_outlook_data;
    }
    
    //-----------------------------------------------------
    //  Function M17.2: addUpdateBusinessOutlook
    //      Add or update the Industry's Business Outlook
    //-----------------------------------------------------
    public function addUpdateBusinessOutlook($industry_id, $data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;           // Processing Status Flag
        
        /*  Instantiate Database Class  */
        $biz_outlook    = BusinessOutlook::where('industry_id', $industry_id)->first();
        
        /** No business outlook data exist for chosen industry  */
        if (0 >= @count($biz_outlook)) {
            $biz_outlook    = new BusinessOutlook;
            $biz_outlook->industry_id   = $industry_id;
            $biz_outlook->status        = ACTIVE;
        }
        
        /*  Saves Data to the Database  */
        if(array_key_exists("biz-outlook-q1", $data)) {
            $biz_outlook->outlook_idx_q1    = $data['biz-outlook-q1'];
            $biz_outlook->outlook_idx_q2    = $data['biz-outlook-q2'];
            $biz_outlook->outlook_idx_q3    = $data['biz-outlook-q3'];
            $biz_outlook->outlook_idx_q4    = $data['biz-outlook-q4'];
            $biz_outlook->outlook_idx_q5    = $data['biz-outlook-q5'];
            $biz_outlook->outlook_idx_q6    = $data['biz-outlook-q6'];
            $biz_outlook->outlook_idx_q7    = $data['biz-outlook-q7'];
            $biz_outlook->outlook_idx_q8    = $data['biz-outlook-q8'];
        }
        $biz_outlook->date_revised      = date('Y-m-d');
        
        $sts    = $biz_outlook->save();
        
        return $sts;
    }
}
