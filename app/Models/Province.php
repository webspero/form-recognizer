<?php

//======================================================================
//  Class M77: Provinces
//      Provinces Table Database Model
//======================================================================
class Province extends Eloquent
{	
	protected $table = 'tblprovince';
    protected $primaryKey   = 'provinceid';
    
}
