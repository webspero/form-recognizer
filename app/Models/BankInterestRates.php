<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BankInterestRates extends Eloquent {
    protected $table = 'bank_interest_rates';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getCurrentInterestRates() {
        $local = $this->getCurrentInterestRateByType('local');
        $foreign = $this->getCurrentInterestRateByType('foreign');

        return [
            'foreign' => [
                'low' => (float) $foreign->low,
                'mid' => (float) $foreign->mid,
                'high' => (float) $foreign->high,
            ],
            'local' => [
                'low' => (float) $local->low,
                'mid' => (float) $local->mid,
                'high' => (float) $local->high,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getPreviousInterestRate() {
        return self::orderBy('executed', 'desc')
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getCurrentInterestRateByType($type) {
        return self::where('type', $type)
            ->orderBy('executed', 'desc')
            ->orderBy('id', 'desc')
            ->first();
    }

    public function addInterestRate($low, $mid, $high, $type) {
        $model = new BankInterestRates();
        $model->low = $low;
        $model->mid = $mid;
        $model->high = $high;
        $model->type = $type;
        $model->executed = date('Y-m-d H:i:s');
        $model->save();
    }
}
