<?php

//======================================================================
//  Class : PSE Company Financial Report
//      PSE Company Financial Report Table Database Model
//======================================================================
class PseCompanyFinancialReport extends Eloquent
{
    protected 	$table 		= 'pse_company_financial_reports';  /* The database table used by the model.    */
	protected 	$primaryKey = 'id';         /* The table's primary key                  */
	public		$timestamps = false;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pse_company_id',
		'attachment_file_name',
		'attachment_file_id',
		'attachment_file_url',
		'edge_no',
		'is_deleted',
    ];
}
