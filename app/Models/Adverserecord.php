<?php
//======================================================================
//  Class M2: Adverserecord
//      Adverserecord Table Database Class
//======================================================================
class Adverserecord extends Eloquent {
	
	protected $table = 'tbladverserecord';				// table name
	protected $primaryKey = 'adverserecordid';			// primary key
	protected $guarded = array('adverserecordid', 'created_at', 'updated_at');	// locked fields
}
