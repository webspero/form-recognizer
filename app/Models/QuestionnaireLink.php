<?php

//======================================================================
//  Class M82: Questionnaire Link
//      Questionnaire Link Table Database Model
//======================================================================
class QuestionnaireLink extends Eloquent
{	
	protected $table = 'questionnaire_links';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
