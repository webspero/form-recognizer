<?php
//======================================================================
//  Class M8: BankIndustry
//      BankIndustry Table Database Class
//======================================================================
class BankIndustry extends Eloquent {
	
	protected $table = 'tblbankindustryweights';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
