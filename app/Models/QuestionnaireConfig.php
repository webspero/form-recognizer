<?php

//======================================================================
//  Class M81: Questionnaire Configuration
//      Questionnaire Configuration Table Database Model
//======================================================================
class QuestionnaireConfig extends Eloquent
{
	protected 	$table 		= 'questionnaire_config';   /* The database table used by the model.    */
	protected 	$primaryKey = 'questionnaire_id';       /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M81.1: getQuestionnaireConfigbyBankID
    //      Gets the Questionnaire Configuration of a Bank
    //-----------------------------------------------------
    public function getQuestionnaireConfigbyBankID($bank_id)
    {
        /*  Gets the Questionnaire Configuration of the Bank    */
        $questionnaire_data   = self::where('bank_id', $bank_id)
            ->first();
        
        /*  Sets the Configuration to default if no configuration exists    */
        if (0 >= @count($questionnaire_data)) {
            $questionnaire_data = new stdClass;
            
            $questionnaire_data->question_type          = QUESTION_TYPE_BANK;
            $questionnaire_data->affiliates             = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->products               = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->main_locations         = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->capital_details        = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->branches               = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->import_export          = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->insurance              = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->cash_conversion        = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->major_customer         = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->major_supplier         = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->major_market           = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->competitors            = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->business_drivers       = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->customer_segments      = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->past_projects          = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->future_growth          = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->owner_details          = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->directors              = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->shareholders           = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->ecf                    = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->company_succession     = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->landscape              = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->economic_factors       = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->tax_payments           = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->risk_assessment        = QUESTION_CNFG_REQUIRED;
            $questionnaire_data->growth_potential       = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->investment_expansion   = QUESTION_CNFG_OPTIONAL;
            $questionnaire_data->rcl                    = QUESTION_CNFG_OPTIONAL;
        }
        
        return $questionnaire_data;
    }
    
    //-----------------------------------------------------
    //  Function M81.2: addConfigByQuestionType
    //      Adds a preset configuration according to
    //      Questionnaire type chosen
    //-----------------------------------------------------
    public function addConfigByQuestionType($bank_id, $question_type)
    {
        /*  Variable Declaration        */
        $sts            = STS_NG;
        
        /*  Gets existing configuration of a Bank       */
        $question_db    = self::where('bank_id', $bank_id)
            ->first();
        
        /*  Questionnaire Configuration for a Bank does not exist   */
        if (0 >= @count($question_db)) {
            
            /*  Saves to Database       */
            $question_db                        = new QuestionnaireConfig;
            
            $question_db->bank_id               = $bank_id;
            $question_db->affiliates            = QUESTION_CNFG_OPTIONAL;
            $question_db->products              = QUESTION_CNFG_OPTIONAL;
            $question_db->main_locations        = QUESTION_CNFG_OPTIONAL;
            $question_db->capital_details       = QUESTION_CNFG_OPTIONAL;
            $question_db->branches              = QUESTION_CNFG_OPTIONAL;
            $question_db->import_export         = QUESTION_CNFG_OPTIONAL;
            $question_db->insurance             = QUESTION_CNFG_OPTIONAL;
            $question_db->cash_conversion       = QUESTION_CNFG_OPTIONAL;
            $question_db->major_customer        = QUESTION_CNFG_REQUIRED;
            $question_db->major_supplier        = QUESTION_CNFG_REQUIRED;
            $question_db->major_market          = QUESTION_CNFG_OPTIONAL;
            $question_db->competitors           = QUESTION_CNFG_OPTIONAL;
            $question_db->business_drivers      = QUESTION_CNFG_REQUIRED;
            $question_db->customer_segments     = QUESTION_CNFG_REQUIRED;
            $question_db->past_projects         = QUESTION_CNFG_OPTIONAL;
            $question_db->future_growth         = QUESTION_CNFG_REQUIRED;
            $question_db->owner_details         = QUESTION_CNFG_REQUIRED;
            $question_db->directors             = QUESTION_CNFG_OPTIONAL;
            $question_db->shareholders          = QUESTION_CNFG_OPTIONAL;
            $question_db->ecf                   = QUESTION_CNFG_OPTIONAL;
            $question_db->company_succession    = QUESTION_CNFG_REQUIRED;
            $question_db->landscape             = QUESTION_CNFG_OPTIONAL;
            $question_db->economic_factors      = QUESTION_CNFG_OPTIONAL;
            $question_db->tax_payments          = QUESTION_CNFG_OPTIONAL;
            $question_db->risk_assessment       = QUESTION_CNFG_REQUIRED;
            $question_db->growth_potential      = QUESTION_CNFG_OPTIONAL;
            $question_db->investment_expansion  = QUESTION_CNFG_OPTIONAL;
            $question_db->rcl                   = QUESTION_CNFG_OPTIONAL;
        }
        
        $question_db->question_type             = $question_type;
        
        $sts = $question_db->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M81.3: setQuestionnaireSectionConfig
    //      Updates a section's configuration
    //-----------------------------------------------------
    public function setQuestionnaireSectionConfig($bank_id, $section, $configuration)
    {
        $update_config  = self::where('bank_id', $bank_id)
            ->update(
                array(
                    $section    => $configuration
                )
            );
            
        return $update_config;
    }
    
    //-----------------------------------------------------
    //  Function M81.4: checkQuestionnaireConfigExist
    //      Checks if a Questionnaire Configuration already exists
    //-----------------------------------------------------
    public function checkQuestionnaireConfigExist()
    {
        /*  Variable Declaration        */
        $sts            = STS_NG;
        
        /*  Checks the Questionnaire Configuration of the logged in Supervisor  */
        $config_exist   = self::where('bank_id', Auth::user()->bank_id)
            ->first();
        
        /*  Configuration Exists    */
        if (0 < @count($config_exist)) {
            $sts = STS_OK;
        }
        
        return $sts;
    }
}
