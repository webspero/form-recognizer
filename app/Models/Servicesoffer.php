<?php

//======================================================================
//  Class M89: Products and Services Offered
//      Products and Services Offered Table Database Model
//======================================================================
class Servicesoffer extends Eloquent
{	
    protected $table = 'tblservicesoffer';
    protected $primaryKey = 'servicesofferid';
	protected $fillable = array('servicesofferid', 'loginid', 'servicesoffer_name', 'servicesoffer_share_revenue', 'servicesoffer_seasonality');

	//-----------------------------------------------------
    //  Function M89.1: entity
    //      Sets the Products table as child of a report
    //-----------------------------------------------------
    public function entity() {
        return $this->belongsTo('tblentity');
    }

}
