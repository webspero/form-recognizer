<?php
//======================================================================
//  Class M22: CashFlow
//      CashFlow Table Database Class
//======================================================================
class CashFlow extends Eloquent {
	
	protected $table = 'fin_cashflow';
	protected $guarded = array('id', 'created_at', 'updated_at');
	
    //-----------------------------------------------------
    //  Function M22.1: financialReport
    //      Gets the FS where this cashflow belongs to 
    //-----------------------------------------------------
	public function financialReport()
    {
        //JM
        //return $this->belongsTo('FinancialReport', 'financial_report_id');
        return $this->belongsTo(FinancialReport::class);
    }
	
}
