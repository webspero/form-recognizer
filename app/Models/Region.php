<?php
//======================================================================
//  Class M24: City2
//      refcitymun Table Database Class
//======================================================================
class Region extends Eloquent {
	
	protected $table        = 'refregion';
    protected $primaryKey   = 'id';

    public static function generate_array(){
    	$results = Region::all();

    	$array = [];
    	foreach ($results as $record) {
    		$array[$record->regCode] = $record->regDesc;
    	}

    	return $array;
    }
}
