<?php

class CurrencyRate extends Eloquent {
    protected $table = 'currency_rate'; //table
    protected $guarded = array('id'); // locked fields
    protected $primaryKey = 'id';
}
