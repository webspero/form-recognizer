<?php
//======================================================================
//  Class M4: Bank
//      Bank Table Database Class
//======================================================================
class Bank extends Eloquent {
	
	protected $table = 'tblbank'; //table
	protected $guarded = array('id', 'created_at', 'updated_at'); // locked fields
    
	//-----------------------------------------------------
    //  Function M4.1: getBankQuestionTypeByEntity
    //  	Gets the bank questiontype using entity id 
    //-----------------------------------------------------
    public function getBankQuestionTypeByEntity($entity_id)
    {
        $question_typ   = QUESTION_TYPE_BANK;
        $entity         = Entity::find($entity_id);
        
        if ($entity) {
            $bank_data  = self::where('id', $entity->current_bank)
                ->first();
            
            if ($bank_data) {
                $question_typ   = $bank_data->question_type;
            }
        }
        
        return $question_typ;
    }

    public function getOrganizationList()
    {
        $organizations = DB::table('tblbank')
                        ->leftJoin('tbllogin', 'tblbank.id','=','tbllogin.bank_id', 'tbllogin.role', '=', 4)
                        ->selectRaw('tblbank.*, count(tbllogin.loginid) as supervisor_count')
                        ->groupBy('tblbank.id')
                        ->orderBy('bank_name', 'asc')
                        ->get();
        return $organizations;
    }

    public function getOrganizationById($id)
    {
        $organization = self::where('id',$id)->first();
        return $organization;
    }

    public function getOrganizationByName($name)
    {
        $organization = self::where('bank_name',$name)->first();
        return $organization;
    }

    public function getOrganizationByType($type)
    {
        $organizations = self::where('bank_type',$type)->get();
        return $organizations;
    }

    public function getOrganizationQuestionType($bankId)
    {
        $organization = self::where('id',$bankId)->first();
        return $organization->questionnaire_type;
    }

    public function addOrganization($organizationDetails)
    {
        $organization = new Bank;
        $organization->bank_name = $organizationDetails['bank_name'];
        $organization->bank_type = $organizationDetails['bank_type'];
        $organization->is_standalone = $organizationDetails['is_standalone'];
        //$organization->questionnaire_type = $organizationDetails['questionnaire_type'];
        $organization->is_custom = $organizationDetails['is_custom'];
        $organization->custom_price = $organizationDetails['custom_price'];
        $organization->created_at = date('Y-m-d');
        $organization->updated_at = date('Y-m-d');
        $organization->deliverables = $organizationDetails['deliverables'];

        return $organization->save();
    }

    public function updateOrganization($id, $organizationDetails)
    {
        $organization = self::find($id);
        if($organization){
            $organization->bank_type = $organizationDetails['bank_type'];
            //$organization->questionnaire_type = $organizationDetails['questionnaire_type'];
            $organization->is_custom = $organizationDetails['is_custom'];
            $organization->custom_price = $organizationDetails['custom_price'];
            $organization->updated_at = date('Y-m-d');
            $organization->deliverables = $organizationDetails['deliverables'];
            
            return $organization->save();
        } else{
            return null;
        }
        
    }
}
