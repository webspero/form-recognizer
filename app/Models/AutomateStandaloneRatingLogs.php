<?php
//======================================================================
//      AutomateStandaloneRating logs Table Database Class
//======================================================================

class AutomateStandaloneRatingLogs extends Eloquent {
	
	protected $table = 'automate_standalone_rating_logs';			// table name
    protected $primary_key = 'id';
    

    public function searchByEntityId($entityid)
    {
        $data = self::select("id")
                    ->where("entity_id", $entityid)
                    ->where("is_deleted", 0)
                    ->get();

        return $data;
    }

    public function updateByEntityId($data, $id) 
    {
        self::update($data)->where("id", $id);
    }


    public function softDelete($id) {
        $delete = array("is_deleted"=>1);
        self::update($delete)->where("id", $id);
    }
	
}
