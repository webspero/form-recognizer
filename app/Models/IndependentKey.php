<?php
//======================================================================
//  Class M47: IndependentKey
//      IndependentKey Table Database Class
//======================================================================
class IndependentKey extends Eloquent {
	
	protected $table = 'independent_keys';
	protected $guarded = array('id', 'created_at', 'updated_at');
	
	//-----------------------------------------------------
    //  Function M47.1: createKeys
    //  	creates independent keys
    //-----------------------------------------------------
	public static function createKeys($email, $quantity)
	{
		$exist = IndependentKey::where('email', $email)->orderBy('created_at', 'desc')->first();
		$batch = 1;
		if($exist){
			$batch = $exist->batch + 1;
		}
		for($x=0; $x<$quantity; $x++){
			$key='';
			$return=1;
			while($return!=0){
				$key = IndependentKey::generateKey();
				$return = IndependentKey::where('serial_key', $key)->count();
			}
			IndependentKey::create(array(
				'email' => $email,
				'serial_key' => $key,
				'batch' => $batch,
				'is_paid' => 1
			));
		}
		return $batch;
	}
	
	//-----------------------------------------------------
    //  Function M47.1: createKeys
    //  	creates independent keys
    //-----------------------------------------------------
	public static function createAdminKeys($email, $quantity)
	{
		$exist = IndependentKey::where('email', $email)->orderBy('created_at', 'desc')->first();
		$batch = 1;
		if($exist){
			$batch = $exist->batch + 1;
		}

		$keys = [];
		for($x=0; $x<$quantity; $x++){
			$key='';
			$return=1;
			while($return!=0){
				$key = IndependentKey::generateKey();
				$return = IndependentKey::where('serial_key', $key)->count();
			}
			IndependentKey::create(array(
				'email' => $email,
				'serial_key' => $key,
				'batch' => $batch,
				'is_paid' => 1
			));

			$keys[] = $key;
		}
		return $keys;
	}

	//-----------------------------------------------------
    //  Function M47.2: generateKey
    //  	generate random code for independent keys
    //-----------------------------------------------------
	public static function generateKey() {
		
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 4; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$randomString .= '-';
		for ($i = 0; $i < 4; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$randomString .= '-';
		for ($i = 0; $i < 4; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}
