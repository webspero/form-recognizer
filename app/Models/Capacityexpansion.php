<?php
//======================================================================
//  Class M20: Capacityexpansion
//      Capacityexpansion Table Database Class
//======================================================================
class Capacityexpansion extends Eloquent {
	
	protected $table = 'tblcapacityexpansion';
	protected $primaryKey = 'capacityexpansionid';
}
