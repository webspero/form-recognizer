<?php
//======================================================================
//  Class M16: Businessdriver
//      Businessdriver Table Database Class
//======================================================================
class Businessdriver extends Eloquent {
	
	protected $table = 'tblbusinessdriver';
	protected $primaryKey = 'businessdriverid';

}
