<?php

//======================================================================
//  Class M63: Major Supplier
//      Major Supplier Table Database Model
//======================================================================
class Majorsupplier extends Eloquent
{	
	protected $table = 'tblmajorsupplier';
    protected $primaryKey = 'majorsupplierid';
}
