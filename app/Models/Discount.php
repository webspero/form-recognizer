<?php
//======================================================================
//  Class M33: Discount
//      Discount Table Database Class
//======================================================================
class Discount extends Eloquent
{
	
	protected $table = 'discounts';
	protected $guarded = array('id', 'created_at', 'updated_at');
    
	//-----------------------------------------------------
    //  Function M33.1: getDiscountByEmail
    //  	Gets the discount code by email address
    //-----------------------------------------------------
    public function getDiscountByEmail($email)
    {
        $today = date('Y-m-d H:i:s');
        
        $discount_data  = self::where('status', CMN_ON)
            ->where('email', $email)
            ->where('valid_from', '<=', $today)
            ->where('valid_to', '>=', $today)
            ->first();
            
        return $discount_data;
    }

	//-----------------------------------------------------
    //  Function M33.2: getDiscountByCode
    //  	Gets the discount by Discount Code
    //-----------------------------------------------------
    public function getDiscountByCode($discount_code)
    {
        $today = date('Y-m-d H:i:s');
        
        $discount_data  = self::where('status', CMN_ON)
            ->where('discount_code', $discount_code)
            ->where('valid_from', '<=', $today)
            ->where('valid_to', '>=', $today)
            ->first();
            
        return $discount_data;
    }
}
