<?php

//======================================================================
//  Class M93: Supervisor Application
//      Supervisor Application Database Model
//======================================================================
class SupervisorApplication extends Eloquent
{
	protected 	$table 		= 'supervisor_application'; /* The database table used by the model.    */
	protected 	$primaryKey = 'application_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M93.1: getSupervisorApplication
    //      Gets a Supervisor Application
    //-----------------------------------------------------
    public function getSupervisorApplication($application_id)
    {
        $data   = self::find($application_id);

        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M93.2: getAllSupervisorApplication
    //      Gets all Supervisor Application
    //-----------------------------------------------------
    public function getAllSupervisorApplication()
    {
        $data   = self::leftJoin('tblbank', 'tblbank.id', '=', 'supervisor_application.bank_id')
            ->orderBy('supervisor_application.email', 'desc')
            ->get();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M93.3: addSupervisorApplication
    //      Adds a Supervisor Application
    //-----------------------------------------------------
    public function addSupervisorApplication($data)
    {
        /*  Variable Declaration        */
        $sts            = STS_NG;
        $ecf_included   = STS_NG;
        $rcl_included   = STS_NG;
        
        /*  Instantiate Database Class  */
        $application_db = new SupervisorApplication;
        
        /*  Sets the value for the ECF and RCL inclusion from Checkboxes    */
        /** ECF Inclusion   */
        if (isset($data['ecf_included'])) {
            $ecf_included   = STS_OK;
        }
        
        /** RCL Inclusion   */
        if (isset($data['rcl_included'])) {
            $rcl_included   = STS_OK;
        }
        
        /*  Saves Data to the Database      */
        $application_db->email              = $data['email'];
        $application_db->firstname          = $data['firstname'];
        $application_db->lastname           = $data['lastname'];
        $application_db->role               = $data['role'];
        //$application_db->question_type      = $data['question_type'];;
        $application_db->password           = Hash::make($data['password']);
        $application_db->bank_id            = $data['bank_id'];
        $application_db->product_features   = 1;
        $application_db->valid_id_picture   = $data['valid_id_picture'];
        $application_db->status             = STS_NG;
        $application_db->application_date   = date('Y-m-d');
        
        $sts    = $application_db->save();
        
        return $sts;
    }

    public function checkIfOrganizationUsed($organizationId)
    {
        $supervisor = self::where('bank_id', $organizationId)->get();
        if ($supervisor->count() > 0){
            return true;
        } else{
            return false;
        }
    }

    public function updateOrganizationDetails($organizationId, $data)
    {
        $supervisors = self::where('bank_id', $organizationId)->get();
        foreach($supervisors as $supervisor){
            $supervisor->role = $data['bank_type'];
            //$supervisor->question_type = $data['questionnaire_type'];
            $supervisor->save();
        }
    }
}
