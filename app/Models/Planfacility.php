<?php

//======================================================================
//  Class M73: Existing Credit Facilities
//      Existing Credit Facilities Table Database Model
//======================================================================
class Planfacility extends Eloquent
{	
	protected $table        = 'tblplanfacility';
	protected $primaryKey   = 'planfacilityid';

}
