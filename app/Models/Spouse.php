<?php

//======================================================================
//  Class M92: Owner's Spouse
//      Owner's Spouse Database Model
//======================================================================
class Spouse extends Eloquent
{	
	protected $table = 'tblspouse';
	protected $primaryKey = 'spouseid';
}
