<?php
//======================================================================
//  Class M50: Industrysub
//      Industrysub Table Database Class
//======================================================================
class Industrysub extends Eloquent {
	
	protected $table = 'tblindustry_sub';
	protected $primaryKey = 'industry_sub_id';

}
