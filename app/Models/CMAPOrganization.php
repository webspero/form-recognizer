<?php

class CMAPOrganization extends Eloquent {
	
	protected $table = 'cmap_organizations'; //table
	protected $guarded = array('id', 'created_at', 'updated_at'); // locked fields
    
}
