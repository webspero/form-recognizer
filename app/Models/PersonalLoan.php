<?php

//======================================================================
//  Class M72: Personal Loans
//      Personal Loans Table Database Model
//======================================================================
class PersonalLoan extends Eloquent
{	
	protected $table = 'tblpersonalloans';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
