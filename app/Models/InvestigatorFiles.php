<?php

//======================================================================
//  Class M54: Investigator Files
//      Investigator Files Table Database Model
//======================================================================
class InvestigatorFiles extends Eloquent
{
	
	protected $table = 'investigator_files';
	protected $guarded = array('id', 'created_at', 'updated_at');
    
}
