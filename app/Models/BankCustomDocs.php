<?php

//======================================================================
//  Class M102: Bank Custom Documents
//      Bank Custom Documents Table Database Model
//======================================================================
class BankCustomDocs extends Eloquent
{
    protected 	$table 		= 'bank_custom_docs';  /* The database table used by the model.    */
	protected 	$primaryKey = 'document_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M102.1: getDocumentsByBank
    //      Acquires all Custom documents of a Bank
    //-----------------------------------------------------
    public static function getDocumentsByBank($bank_id) {
        $data   = self::where('bank_id', $bank_id)
            ->where('status', STS_OK)
            ->get();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M102.2: saveCustomDoc
    //      Add / Edit Custom Document
    //-----------------------------------------------------
	public static function saveCustomDoc($data)
	{
        /*  Variable Declaration    */
		$sts        = STS_NG;
		$doc_id     = $data['doc_id'];
		$doc_lbl    = $data['doc_lbl'];
        
        /*  Adding Documents    */
        if (0 == $doc_id) {
            $document   = new BankCustomDocs;
            
            $document->bank_id          = Auth::user()->bank_id;
            $document->bank_required    = STS_NG;
            $document->status           = STS_OK;
            $document->created_date     = date('Y-m-d H:i:s');
            
        }
        /*  Editing Documents   */
        else {
            $document   = self::find($doc_id);
        }
        
        $document->document_label   = $doc_lbl;
        $document->updated_date     = date('Y-m-d H:i:s');
        
        /*  Save changes to the database    */
        if ($document->save()) {
            $sts    = STS_OK;
        }
        
        return $sts;
	}
    
    //-----------------------------------------------------
    //  Function M102.3: deleteCustomDoc
    //      Delete Custom Document
    //-----------------------------------------------------
	public static function deleteCustomDoc($document_id)
	{
        /*  Variable Declaration    */
		$sts        = STS_NG;
        
        /*  Get Document details from the database  */
        $document   = self::find($document_id);
        
        $document->status = STS_NG;
        
        /*  Save changes to the database    */
        if ($document->save()) {
            $sts    = STS_OK;
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M102.4: setCustomDocConfig
    //      Sets the Custom Document to required or not
    //-----------------------------------------------------
	public static function setCustomDocConfig($data)
	{
        /*  Variable Declaration    */
		$sts        = STS_NG;
        
        /*  Get Document details from the database  */
        $document   = self::find($data['document_id']);
        
        $document->bank_required = $data['bank_required'];
        
        /*  Save changes to the database    */
        if ($document->save()) {
            $sts    = STS_OK;
        }
        
        return $sts;
    }
}