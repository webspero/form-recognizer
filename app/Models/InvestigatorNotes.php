<?php

//======================================================================
//  Class M55: Investigator Notes
//      Investigator Notes Table Database Model
//======================================================================
class InvestigatorNotes extends Eloquent
{
	
	protected $table = 'investigator_notes';
	protected $guarded = array('id', 'created_at', 'updated_at');
    
    //-----------------------------------------------------
    //  Function M55.1: getEntityNotes
    //      Gets all investigator notes of a Report
    //-----------------------------------------------------
    public function getEntityNotes($entity_id)
    {
        $invst_notes_data  = self::where('entity_id', $entity_id)->get();
        return $invst_notes_data;
    }
}
