<?php

//======================================================================
//  Class M62: Major Location
//      Major Location Table Database Model
//======================================================================
class Majorlocation extends Eloquent
{	
	protected $table = 'tblmajorlocation';
	protected $primaryKey = 'marketlocationid';

}
