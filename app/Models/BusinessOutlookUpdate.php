<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BusinessOutlookUpdate extends Eloquent {
    protected $table = 'business_outlook_update';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * @return array
     */
    public function getPreviousDownload() {
        return self::where('status', ACTIVE)
            ->orderBy('executed', 'desc')
            ->first();
    }

    public function addDownloadInfo($quarter, $year) {
        $model = new BusinessOutlookUpdate();
        $model->quarter = $quarter;
        $model->year = $year;
        $model->status = 1;
        $model->executed = date('Y-m-d H:i:s');
        $model->save();
    }

    public function reset() {
        BusinessOutlookUpdate::where('status', 1)->delete();
    }
}
