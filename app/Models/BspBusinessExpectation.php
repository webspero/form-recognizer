<?php
//======================================================================
//  Class M14: BspBusinessExpectation
//      BspBusinessExpectation Table Database Class
//======================================================================
class BspBusinessExpectation extends Eloquent {

	/*-------------------------------------------------------------------------
	|	Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database Table
	|	primaryKey	--- Primary key of the Projects Table
	|	timestamps	--- Prevents created_at and deleted_at fields from being required
	|------------------------------------------------------------------------*/
	protected 	$table 		= 'bsp_business_expectations';  /* The database table used by the model.    */
	protected 	$primaryKey = 'bsp_expectation_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
	//-----------------------------------------------------
    //  Function M14.1: getBusinessExpectation
    //  	Function to get latest business expectation record
    //-----------------------------------------------------
    public function getBusinessExpectation()
    {
        $data   = self::where('status', ACTIVE)
            ->orderBy('bsp_expectation_id', 'desc')
            ->first();
        
        return $data;
    }
    
	//-----------------------------------------------------
    //  Function M14.2: addBusinessExpectation
    //  	Function to Add or update the Industry's Business Outlook
    //-----------------------------------------------------
    public function addBusinessExpectation($data)
    {
        /*--------------------------------------------------------------------
		/*	Variable Declaration
		/*------------------------------------------------------------------*/
        $sts        = STS_NG;
        
        /*--------------------------------------------------------------------
		/*	Instantiate Database Class
		/*------------------------------------------------------------------*/
        $biz_expectation    = new BspBusinessExpectation;
        
        /*--------------------------------------------------------------------
		/*	Saves Data to the Database
		/*------------------------------------------------------------------*/
        $biz_expectation->filename          = $data['filename'];
        $biz_expectation->status            = ACTIVE;
        $biz_expectation->date_uploaded     = date('Y-m-d');
        
        /*--------------------------------------------------------------------
		/*	Save to Database
		/*------------------------------------------------------------------*/
        $sts    = $biz_expectation->save();
        
        return $sts;
    }
}
