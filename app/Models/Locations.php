<?php

//======================================================================
//  Class M59: Main Locations
//      Main Locations Table Database Model
//======================================================================
class Locations extends Eloquent
{	
	protected $table = 'tbllocations';
	protected $primaryKey = 'locationid';

}
