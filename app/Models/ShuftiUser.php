<?php
//======================================================================
//  Class M105: ShuftiUser
//      shufti_users Table Database Class
//======================================================================
class ShuftiUser extends Eloquent
{
    protected $table        = 'shufti_users';
    protected $primaryKey   = 'id';
}
