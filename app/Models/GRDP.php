<?php
//======================================================================
//  Class M43: GRDP
//      GRDP Table Database Class
//======================================================================
class GRDP extends Eloquent {
	
	protected $table = 'grdp';	// table
	protected $guarded = array('id', 'created_at', 'updated_at'); //locked fields
	
	//-----------------------------------------------------
    //  Function M43.1: getBoost
    //  	gets the boost values depending on year and region
    //-----------------------------------------------------
	public static function getBoost($year, $regCode)
	{
		$grdp = GRDP::select('grdp')->where('year', $year)->get();
		$grdp_array = array();
		foreach($grdp as $g){
			$grdp_array[] = $g->grdp;
		}
		
		$n = @count($grdp_array);
        $mean = array_sum($grdp_array) / $n;
        $carry = 0.0;
        foreach ($grdp_array as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };
        $sd = sqrt($carry / ($n - 1));
		
		$grdp_region = GRDP::where('year', $year)->where('regCode', $regCode)->first()->grdp;
		$sd_max = $mean + $sd;
		$sd_min = $mean - $sd;
		
		if($grdp_region < $mean){
			if($grdp_region < $sd_min){
				return 0.80;
			} else {
				return 0.90;
			}
		} else {
			if($grdp_region > $sd_max){
				return 1.20;
			} else {
				return 1.10;
			}
		}
	}
	
	//-----------------------------------------------------
    //  Function M43.2: getYear
    //  	gets all the years stored in db for grdp
    //-----------------------------------------------------
	public static function getYear()
	{
		//JM
		return GRDP::select(DB::raw('DISTINCT year'))->get()->pluck('year', 'year');
	}
    
	//-----------------------------------------------------
    //  Function M43.3: getLatestYear
    //  	gets the latest year stored in db for grdp
    //-----------------------------------------------------
    public static function getLatestYear()
	{
		$grdp_data = GRDP::select(DB::raw('DISTINCT year'))->orderBy('year', 'desc')->first();
        return $grdp_data->year;
	}
}
