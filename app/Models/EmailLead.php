<?php
//======================================================================
//  Class M35: EmailLead
//      EmailLead Table Database Class
//======================================================================
class EmailLead extends Eloquent {
	
	protected $table = 'email_leads';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
