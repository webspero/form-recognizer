<?php
//======================================================================
//  Class M19: Businesstype
//      Businesstype Table Database Class
//======================================================================
class Businesstype extends Eloquent {
	
    protected 	$table 		= 'tblbusinesstype';    /* The database table used by the model.    */
	protected 	$primaryKey = 'businesstypeid';     /* The table's primary key                  */

}
