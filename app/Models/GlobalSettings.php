<?php
//======================================================================
//  Class M6: BankFormula
//      GlobalSettings Table Database Class
//======================================================================
class GlobalSettings extends Eloquent {
	
	protected $table = 'global_settings';
	protected $guarded = array('id', 'created_at', 'updated_at');
    
    //-----------------------------------------------------
    //  Function M56.1: getGlobalSetting
    //      Gets all investigator tags of a Report
    //-----------------------------------------------------
    public static function getGlobalSetting($key)
    {
        $output  = self::where('key', $key)->get();
        if(!isset($output[0])){
            return "";
        }
        return $output[0]->value;
    }
    
    public static function setGlobalSetting($key,$value)
    {
        $output  = self::where('key', $key)->get();
        if(isset($output[0])){
            $output  = self::where('key', $key)->update(['value' => $value]);
        }else{
            $setting = new GlobalSettings;
            $setting->key = $key;
            $setting->value = $value;
            $setting->save();
        }
    }
}