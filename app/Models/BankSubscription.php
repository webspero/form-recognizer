<?php
//======================================================================
//  Class M12: BankSubscription
//      BankSubscription Table Database Class
//======================================================================
class BankSubscription extends Eloquent {
	
	protected $table = 'tblbanksubscriptions';
	protected $guarded = array('id', 'created_at', 'updated_at');
	protected $primaryKey = 'id';

	public function getBankSubscription($userId){
		$subscription = self::where('bank_id', $userId)->first();
		return $subscription;
	}

	public function updateSubscriptionDate($userId, $date){
		$subscription = self::where('bank_id', $userId)->update(['expiry_date' => $date]);
		return;
	}
}