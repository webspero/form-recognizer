<?php

//======================================================================
//  Class M80: PSE Income Statements
//      PSE Income Statements Table Database Model
//======================================================================
class PseIncomeStatement extends Eloquent
{
	protected 	$table 		= 'pse_income_statement';   /* The database table used by the model.    */
	protected 	$primaryKey = 'pse_income_id';          /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M80.1: addPseIncome
    //      Adds a new PSE Income Statement
    //-----------------------------------------------------
    public function addPseIncome($data)
    {
        /*  Variable Declaration        */
        $sts = STS_OK;
        
        /*  Saves to Database           */
        $pse_income                             = new PseIncomeStatement;
        
        $pse_income->pse_id                     = $data['pse_id'];
        $pse_income->year                       = $data['year'];
        $pse_income->period                     = $data['period'];
        $pse_income->gross_revenue              = $data['gross_revenue'];
        $pse_income->gross_expense              = $data['gross_expense'];
        $pse_income->net_income_before_tax      = $data['net_income_before_tax'];
        $pse_income->net_income_after_tax       = $data['net_income_after_tax'];
        $pse_income->net_income_parent_attrib   = $data['net_income_parent_attrib'];
        $pse_income->basic_earnings_per_share   = $data['basic_earnings_per_share'];
        $pse_income->diluted_earnings_per_share = $data['diluted_earnings_per_share'];
        $pse_income->status                     = STS_OK;
        $pse_income->date_updated               = date('Y-m-d');
        
        /*  Save Successful         */
        if ($pse_income->save()) {
            $primary_key = $pse_income->pse_id;
        }
        /*  Save Failed             */
        else {
            $sts = STS_NG;
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M80.2: getPseIncomeByYear
    //      Gets Income Statement according to year
    //-----------------------------------------------------
    public function getPseIncomeByYear($pse_id, $year)
    {
        $pse_company = self::where('pse_id', $pse_id)
            ->where('year', $year)
            ->where('status', STS_OK)
            ->first();
            
        return $pse_company;
    }

}