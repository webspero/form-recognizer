<?php

//======================================================================
//  Class M65: Manage Other Business
//      Manage Other Business Table Database Model
//======================================================================
class ManageBusiness extends Eloquent
{	
	protected $table        = 'tblmanagebus';
    protected $primaryKey   = 'managebusid';

}
