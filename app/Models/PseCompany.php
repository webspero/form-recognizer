<?php

//======================================================================
//  Class M79: PSE Companies
//      PSE Company Table Database Model
//======================================================================
class PseCompany extends Eloquent
{
	protected 	$table 		= 'pse_companies';  /* The database table used by the model.    */
	protected 	$primaryKey = 'pse_id';         /* The table's primary key                  */
    protected   $guarded = ['pse_id'];
	public		$timestamps = false;

    //-----------------------------------------------------
    //  Function M179.1: addPseCompany
    //      Adds a new PSE Company
    //-----------------------------------------------------
    public function addPseCompany($data)
    {
        /*  Variable Declaration        */
        $sts = STS_OK;
        
        /*  Check if company exists     */
        $company_exist = self::where('pse_id', $data['pse_id'])
            ->first();
        
        /*  Create a record of the company if it does not exists    */
        if (0 >= @count($company_exist)) {
            $pse_company = new PseCompany;
            
            /*  Saves to Database   */
            $pse_company->pse_id            = $data['pse_id'];
            $pse_company->company_name      = $data['company_name'];
            $pse_company->pse_code          = $data['pse_code'];
            $pse_company->date_updated      = date('Y-m-d');
            $pse_company->status            = STS_OK;
            
            if ($pse_company->save()) {
                $primary_key = $pse_company->pse_id;
            }
            else {
                $sts = STS_NG;
            }
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M179.2: getActivePseCompanies
    //      Fetches all Active PSE Companies
    //-----------------------------------------------------
    public function getActivePseCompanies()
    {
        $companies = self::where('status', ACTIVE)
            ->orderBy('company_name', 'asc')
            ->get();
            
        return $companies;
    }
    
    //-----------------------------------------------------
    //  Function M179.3: getPseCompany
    //      Get a specified Company by ID
    //-----------------------------------------------------
    public function getPseCompany($pse_id)
    {
        $company = self::find($pse_id);
        
        return $company;
    }

}