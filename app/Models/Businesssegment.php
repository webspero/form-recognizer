<?php
//======================================================================
//  Class M18: Businesssegment
//      Businesssegment Table Database Class
//======================================================================
class Businesssegment extends Eloquent {
	
	protected $table = 'tblbusinesssegment';
	protected $primaryKey = 'businesssegmentid';
}
