<?php
//======================================================================
//  Class M7: BankFormulaHistory
//      BankFormulaHistory Table Database Class
//======================================================================
class BankFormulaHistory extends Eloquent {
	
	protected $table = 'bank_formula_history';
	protected $guarded = array('id', 'created_at', 'updated_at');

}
