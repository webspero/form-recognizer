<?php

class AdminSetting extends Eloquent
{
    protected $table = 'admin_settings';			// table name
	protected $primary_key = 'id';
	protected $guarded = array('id'); 
}
