<?php

//======================================================================
//  Class M78: PSE Balance Sheet
//      PSE Balance Sheet Table Database Model
//======================================================================
class PseBalSheet extends Eloquent
{
	protected 	$table 		= 'pse_balance_sheets';     /* The database table used by the model.    */
	protected 	$primaryKey = 'pse_balsheet_id';        /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M78.1: addPseBalSheet
    //      Adds a PSE Balance Sheet
    //-----------------------------------------------------
    public function addPseBalSheet($data)
    {
        /*  Variable Declaration        */
        $sts = STS_OK;
        
        /*  Saves into the Database     */
        $pse_balsheet                               = new PseBalSheet;
        
        $pse_balsheet->pse_id                       = $data['pse_id'];
        $pse_balsheet->year                         = $data['year'];
        $pse_balsheet->period                       = $data['period'];
        $pse_balsheet->current_asset                = $data['current_asset'];
        $pse_balsheet->total_asset                  = $data['total_asset'];
        $pse_balsheet->current_liabilities          = $data['current_liabilities'];
        $pse_balsheet->total_liabilities            = $data['total_liabilities'];
        $pse_balsheet->retained_earnings            = $data['retained_earnings'];
        $pse_balsheet->stockholders_equity          = $data['stockholders_equity'];
        $pse_balsheet->parent_stockholders_equity   = $data['parent_stockholders_equity'];
        $pse_balsheet->book_value                   = $data['book_value'];
        $pse_balsheet->status                       = STS_OK;
        $pse_balsheet->date_updated                 = date('Y-m-d');
        
        /*  Save Successful */
        if ($pse_balsheet->save()) {
            $primary_key = $pse_balsheet->pse_id;
        }
        /*  Save Failed*/
        else {
            $sts = STS_NG;
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M78.2: getPseBalSheetByYear
    //      Gets PSE Balance Sheet according to year
    //-----------------------------------------------------
    public function getPseBalSheetByYear($pse_id, $year)
    {
        $pse_company = self::where('pse_id', $pse_id)
            ->where('year', $year)
            ->where('status', STS_OK)
            ->first();
            
        return $pse_company;
    }
    
    //-----------------------------------------------------
    //  Function M78.3: getPseBalSheetByPseID
    //      Gets PSE Balance Sheet by Company
    //-----------------------------------------------------
    public function getPseBalSheetByPseID($pse_id)
    {
        $pse_company = self::where('pse_id', $pse_id)
            ->where('status', STS_OK)
            ->orderBy('pse_balsheet_id', 'desc')
            ->get();
            
        return $pse_company;
    }

}