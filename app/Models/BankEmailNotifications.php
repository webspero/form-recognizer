<?php

class BankEmailNotifications extends Eloquent {
	
	protected $table = 'tblemailnotif'; //table
	public $timestamps = false;
	protected $primaryKey = 'email_notif_id';
	//protected $guarded = array('id', 'created_at', 'updated_at'); // locked fields, laravel fields that are not used.
}

?>