<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use DB;

class DebtServiceCapacityCalculator extends Eloquent {
    protected $table = 'debt_service_capacity_calculator';
    protected $primaryKey = 'entity_id';
    public $timestamps = false;

    public function replace($data = []) {
        $loan_amount = $data['source_amount'] < 1 ? 0.00 : (float)$data['loan_amount'];

        DB::statement('
            REPLACE INTO debt_service_capacity_calculator
            SET entity_id = :entity_id,
                dscr = :dscr,
                loan_duration = :loan_duration,
                net_operating_income = :net_operating_income,
                loan_amount = :loan_amount,
                working_interest_rate_local = :working_interest_rate_local,
                working_interest_rate_foreign = :working_interest_rate_foreign,
                term_interest_rate_local = :term_interest_rate_local,
                term_interest_rate_foreign = :term_interest_rate_foreign,
                -- period = :period,
                -- payment_per_period_local = :payment_per_period_local,
                -- payment_per_period_foreign = :payment_per_period_foreign,
                is_custom = :is_custom,
                source = :source',
            [
                'entity_id' => (int) $data['entity_id'],
                'dscr' => (float) $data['dscr'],
                'loan_duration' => (int) $data['loan_duration'],
                'net_operating_income' => (float) $data['net_operating_income'],
                'loan_amount' => $loan_amount,
                'working_interest_rate_local' => round(
                    (float) $data['working_interest_rate_local'],
                    4
                ),
                'working_interest_rate_foreign' => round(
                    (float) $data['working_interest_rate_foreign'],
                    4
                ),
                'term_interest_rate_local' => round(
                    (float) $data['term_interest_rate_local'],
                    4
                ),
                'term_interest_rate_foreign' => round(
                    (float) $data['term_interest_rate_foreign'],
                    4
                ),
                // 'period' => (int) $data['period'],
                // 'payment_per_period_local' => (int) $data['payment_per_period_local'],
                // 'payment_per_period_foreign' => (int) $data['payment_per_period_foreign'],
                'is_custom' => (bool) $data['is_custom'],
                'source' => $data['source'],
            ]
        );
    }
}
