<?php
//======================================================================
//  Class M11: BankSerialKeys
//      BankSerialKeys Table Database Class
//======================================================================
class BankSerialKeys extends Eloquent {
	
	protected $table = 'bank_keys';	// table
	protected $guarded = array('id', 'created_at', 'updated_at'); // locked keys
	
	//-----------------------------------------------------
    //  Function M11.1: createKeys
    //  	Function to create supervisor keys
    //-----------------------------------------------------
	public static function createKeys($login_id, $quantity, $keyType)
	{
		$user = User::where('loginid', $login_id)->first();
		for($x=0; $x<$quantity; $x++){
			$key='';
			$return=1;
			while($return!=0){
				$key = BankSerialKeys::generateKey();
				$return = BankSerialKeys::where('serial_key', $key)->count();
			}
			BankSerialKeys::create(array(
				'bank_id' => $user->bank_id,
				'serial_key' => $key,
				'type'	=> $keyType
			));
		}
	}
	
	//-----------------------------------------------------
    //  Function M11.2: generateKey
    //  	Function to generate random string following a format to be used as key
    //-----------------------------------------------------
	public static function generateKey() {
		
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 5; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$randomString .= '-';
		for ($i = 0; $i < 5; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$randomString .= '-';
		for ($i = 0; $i < 5; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}
