<?php

class CustomNotification extends Eloquent
{
    protected $table = 'custom_notifications'; //table
    protected $guarded = array('id'); // locked fields
    protected $primaryKey = 'id';

}
