<?php
//======================================================================
//  Class M25: CmapSearchData
//      refcitymun Table Database Class
//======================================================================
class CmapSearchData extends Eloquent {
	protected $table        = 'cmap_search_data';
    protected $primaryKey   = 'id';

}