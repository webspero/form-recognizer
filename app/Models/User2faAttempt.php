<?php

//======================================================================
//  Class M98: User 2fa Attempts
//      User 2fa Attempts Database Model
//======================================================================
class User2faAttempt extends Eloquent
{
	protected 	$table 		= 'user_2fa_attempts';      /* The database table used by the model.    */
	protected 	$primaryKey = 'user_attempt_id';        /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M98.1: getUserAttempt
    //      Gets a User's 2FA Attempts
    //-----------------------------------------------------
    public function getUserAttempt($login_id)
    {
        $data   = self::where('login_id', $login_id)
            ->orderBy('user_attempt_id', 'desc')
            ->first();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M98.2: add2faAttempt
    //      Adds a new 2FA Attempt
    //-----------------------------------------------------
    public function add2faAttempt($data)
    {
        /*  Variable Declaration        */
        $sts    = STS_NG;
        
        $data['passcode']   = STR_EMPTY;
        $shuf_img           = [];
        $images             = ['car', 'house', 'coffee', 'airplane'];
        
        /*  Instantiate Database Class  */
        $user_attempt   = new User2faAttempt;
        
        /*  Get a Random Image  */
        $shuf_img       = shuffle($images);
        $data['image']  = $images[0];
        
        /*  Get 8 random digits */
        for ($i = 0; 8 > $i; $i++) {
            $data['passcode']  .= rand(0, 9);
        }
        
        /*  Saves Data to the Database  */
        $user_attempt->login_id         = $data['loginid'];
        $user_attempt->image            = $data['image'];
        $user_attempt->passcode         = $data['passcode'];
        $user_attempt->attempts         = 0;
        $user_attempt->status           = STS_NG;
        $user_attempt->last_attempt     = date('Y-m-d H:i:s');
        
        $sts                            = $user_attempt->save();
        
        /*  Send email to User  */
        if (ENVIRONMENT != ENV_LOCAL){
            try{
                Mail::send('emails.user-2fa-challenge', array('data'=>$data), function($message) use ($data) {
                    $message->to($data['email'], $data['email'])
                    ->subject('CreditBPO User Authentication Challenge');
                });
            }catch(Exception $e){
            //
            }
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M98.3: updateAttemptSts
    //      Validates and Updates a User's 2FA attempt
    //-----------------------------------------------------
    public function updateAttemptSts($data)
    {
        /*  Variable Declaration        */
        $sts    = STS_NG;
        $msg    = CONT_NULL;
        
        /*  Instantiate Database Class  */
        $mal_attmpt_lib     = new MaliciousAttemptLib;
        $login_attempt_db   = new UserLoginAttempt;
        $user_attempt       = self::find($data['attempt_id']);
        
        /*  Input Validation            */
		$validator	= Validator::make(
			/*	Data to be validated										*/
			$data,
			
			/*	Validation Rules											*/
			array(
				'image'		        => 'required',
                'passcode'          => 'required',
			),
            
            array(
                'image.required'    => trans('validation.required', array('attribute'=>'Image')),
                'passcode.required' => trans('validation.required', array('attribute'=>'Passcode')),
            )
		);
        
        /*  There are invalid inputs    */
		if ($validator->fails()) {
			/*	Gets the messages from the validation						*/
			$messages 				= $validator->messages();
			$msg                    = $messages->all();
		}
        /*  All inputs are valid        */
        else {
            /*  Check for Account Suspension due to 2FA */
            $mal_attmpt_data    = $mal_attmpt_lib->check2faAttempts($user_attempt['login_id']);
            
            $msg[0] = $mal_attmpt_data['message'];
            $sts    = $mal_attmpt_data['sts'];
        }
        
        /*  Account is not suspended        */
        if ($sts != STS_LOCK) {
            /*  Image chosen is invalid     */
            if ($data['image'] != $user_attempt->image) {
                $msg[0] = 'The image you chose does not match our requirements. Please see your email.';
                $sts    = STS_NG;
            }
            /*  Passcode given is invalid   */
            else if ($data['passcode'] != $user_attempt->passcode) {
                $msg[0] = 'The passcode you gave does not match our requirements. Please see your email.';
                $sts    = STS_NG;
            }
            /*  Image and Passcode is correct   */
            else {
                $sts    = STS_OK;
                
                /*  Reset Login failure count       */
                $login_data             = $login_attempt_db->getUserAttempt($user_attempt['login_id']);
                $log_data['attempt_id'] = $login_data['login_attempt_id'];
                
                $login_attempt_db->updateAttemptSts($log_data, STS_OK);
            }
        }
        
        /*  Saves Data to the Database  */
        $user_attempt->attempts         = $user_attempt->attempts + 1;
        $user_attempt->status           = $sts;
        $user_attempt->last_attempt     = date('Y-m-d H:i:s');
        
        $user_attempt->save();
        
        $svr_resp['valid']      = $sts;
        $svr_resp['messages']   = $msg;
        
        return $svr_resp;
    }
    
    //-----------------------------------------------------
    //  Function M98.4: unlock2faAttempt
    //      Unlock the User's 2FA attempt
    //-----------------------------------------------------
    public function unlock2faAttempt($login_id)
    {
        /*  Gets a User's 2FA Attempt   */
        $attempt_data = self::getUserAttempt($login_id);
        
        /*  Sets the Status of the attempt to finish    */
        if (1 <= @count($attempt_data)) {
            $unlock         = self::find($attempt_data['user_attempt_id']);
            $unlock->status = STS_OK;
            
            $unlock->save();
        }
        
    }
}