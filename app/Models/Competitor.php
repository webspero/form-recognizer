<?php
//======================================================================
//  Class M26: Competitor
//      Competitor Table Database Class
//======================================================================
class Competitor extends Eloquent {
	
	protected $table = 'tblcompetitor';
	protected $primaryKey = 'competitorid';

}
