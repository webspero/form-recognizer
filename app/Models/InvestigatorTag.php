<?php

//======================================================================
//  Class M56: Investigator Tag
//      Investigator Tag Table Database Model
//======================================================================
class InvestigatorTag extends Eloquent
{
	
	protected $table = 'investigator_tag';
	protected $guarded = array('id', 'created_at', 'updated_at');
    
    //-----------------------------------------------------
    //  Function M56.1: getEntityTags
    //      Gets all investigator tags of a Report
    //-----------------------------------------------------
    public function getEntityTags($entity_id)
    {
        $invst_tag_data  = self::where('entity_id', $entity_id)->get();
        return $invst_tag_data;
    }
}

