<?php

//======================================================================
//  Class M96: DragonPay Transactions Record
//      DragonPay Transactions Database Model
//======================================================================
class Transaction extends Eloquent
{	
	protected $table = 'tbltransactions';


	//-----------------------------------------------------
    //  Function XX.X: getPendingTransactions
    //      Get Dragonpay Pending Transactions
    //-----------------------------------------------------
	public function getPendingTransactions(){
		$transactions = Transaction::where('txn', '!=', '')->where('type', '!=', '')->where('status', 'P')->get();
		return $transactions;
	}

	//-----------------------------------------------------
    //  Function XX.X: getPendingTransactions
    //      Update Transaction Payment Status
	//-----------------------------------------------------
	public function updateTransactionStatus($id, $status){
		self::where('id', $id)
			  ->update(['status' => $status]);
		return;
	}
}
