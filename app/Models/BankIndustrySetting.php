<?php
//======================================================================
//  Class M9: BankIndustrySetting
//      BankIndustrySetting Table Database Class
//======================================================================
class BankIndustrySetting extends Eloquent {
	
	protected $table = 'tblbankindustryweightshistory';
	protected $guarded = array('id', 'created_at', 'updated_at');

}