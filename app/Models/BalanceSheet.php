<?php
//======================================================================
//  Class M3: BalanceSheet
//      BalanceSheet Table Database Class
//======================================================================
class BalanceSheet extends Eloquent {
	
	protected $table = 'fin_balance_sheets';	// table name
	protected $guarded = array('id', 'created_at', 'updated_at');	// locked fields
	
	//-----------------------------------------------------
    //  Function M3.1: financialReport
    //  	Gets the FS where this balance sheet belongs to 
    //-----------------------------------------------------
	public function financialReport()
    {
        //JM
        //return $this->belongsTo('FinancialReport', 'financial_report_id');
        return $this->belongsTo(FinancialReport::class);
    }
	
}
