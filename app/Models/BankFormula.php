<?php
//======================================================================
//  Class M6: BankFormula
//      BankFormula Table Database Class
//======================================================================
class BankFormula extends Eloquent {
	
	protected $table = 'bank_formula';
	protected $guarded = array('id', 'created_at', 'updated_at');

}