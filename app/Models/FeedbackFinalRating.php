<?php

//======================================================================
//  Class M38: Feedback Final Rating
//      Final Feedback Table Database Model
//======================================================================
class FeedbackFinalRating extends Eloquent
{
	protected 	$table 		= 'feedback_final_ratings'; /* The database table used by the model.    */
	protected 	$primaryKey = 'final_rating_id';        /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M38.1: getAllAnsweredFinalFeedback
    //      Gets all the User Feedback
    //-----------------------------------------------------
    public function getAllAnsweredFinalFeedback()
    {
        $feedback_data  = self::where('status', FINAL_FEEDBACK_DONE)->get();
        return $feedback_data;
    }
    
    //-----------------------------------------------------
    //  Function M38.2: getFinalUserFeedback
    //      Gets the User feedback for a specified transaction
    //-----------------------------------------------------
    public function getFinalUserFeedback($transaction_id)
    {
        $feedback_data  = self::where('transaction_id', $transaction_id)
            ->where('status', FINAL_FEEDBACK_DONE)
            ->first();
            
        return $feedback_data;
    }
    
    //-----------------------------------------------------
    //  Function M38.3: addFinalFeedbackRating
    //      Adds a new Feedback
    //-----------------------------------------------------
    function addFinalFeedbackRating($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        $feedback   = new FeedbackFinalRating;
        
        /*  Saves data into the database    */
        $feedback['transaction_id']         = $data['transaction_id'];
        $feedback['ease_of_experience']     = $data['ease_of_experience'];
        $feedback['report_satisfaction']    = $data['report_satisfaction'];
        $feedback['our_understanding']      = $data['our_understanding'];
        $feedback['will_recommend']         = $data['will_recommend'];
        $feedback['status']                 = FINAL_FEEDBACK_SENT;
        $feedback['date_rated']             = date('Y-m-d');
        
        $sts    = $feedback->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M38.4: updateFinalFeedbackRating
    //      Update the User's Feedback
    //-----------------------------------------------------
    function updateFinalFeedbackRating($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        $feedback   = new FeedbackFinalRating;
        
        $feedback_data  = self::where('transaction_id', $data['transaction_id'])->first();
       
        /*  Saves data into the database    */
        $feedback_data->ease_of_experience     = $data['ease_of_experience'];
        $feedback_data->report_satisfaction    = $data['report_satisfaction'];
        $feedback_data->our_understanding      = $data['our_understanding'];
        $feedback_data->will_recommend         = $data['will_recommend'];
        $feedback_data->status                 = $data['status'];
        $feedback_data->date_rated             = date('Y-m-d');
        
        $sts    = $feedback_data->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M38.5: checkFinalFeedbackRating
    //      Checks if there is already a final Feedback for a report
    //-----------------------------------------------------
    function checkFinalFeedbackRating($entity_id)
    {
        $fback_data = self::where('transaction_id', $entity_id)
            ->where('status', FINAL_FEEDBACK_DONE)
            ->first();
            
        return $fback_data;
    }
    
    //-----------------------------------------------------
    //  Function M38.6: updateFinalFeedbackRating
    //      Checks for the follow-up requests of Final feedback
    //-----------------------------------------------------
    function checkFollowUpFeedback()
    {
        /*  Variable Declaration        */
        $date_today     = date('Y-m-d');                // Date today
        $today_exp      = explode('-', $date_today);    // Convert Date today into array
        
        /*  Subtract 1 week and 2 weeks from the converted date array   */
        $week_ago	    = date('Y-m-d', mktime(0, 0, 0, $today_exp[1], $today_exp[2]-7, $today_exp[0]));
        $week_offset	= date('Y-m-d', mktime(0, 0, 0, $today_exp[1], $today_exp[2]-14, $today_exp[0]));
        
        /*  Fetch accounts that needs follow up rating  */
        $fback_data = self::join('tblentity', 'tblentity.entityid', '=', 'feedback_final_ratings.transaction_id')
                ->join('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
                /*  Feedback has already been sent but no follow ups yet    */
                ->where(function($query) use ($date_today)
                {
                    $query->where('feedback_final_ratings.date_rated', '<', $date_today)
                        ->where('feedback_final_ratings.status', FINAL_FEEDBACK_SENT);
                })
                /*  A follow-up has been sent a 2 weeks ago but no result   */
                ->orWhere(function($query) use ($week_ago, $week_offset)
                {
                    $query->where('feedback_final_ratings.date_rated', '<', $week_ago)
                        ->where('feedback_final_ratings.date_rated', '>', $week_offset)
                        ->where('feedback_final_ratings.status', FINAL_RATING_FOLLOWED);
                })
                ->take(30)
                ->get(array(
                    'tbllogin.email',
                    
                    'feedback_final_ratings.transaction_id',
                    'feedback_final_ratings.ease_of_experience',
                    'feedback_final_ratings.report_satisfaction',
                    'feedback_final_ratings.our_understanding',
                    'feedback_final_ratings.will_recommend'
                    )
                );
                
        return $fback_data;
    }
    
}
