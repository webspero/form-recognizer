<?php
//======================================================================
//  Class M37: Entity
//      Entity Table Database Class
//======================================================================
class Entity extends Eloquent {
	
	protected $table = 'tblentity';	//table
	protected $primaryKey = 'entityid';	// primary key

    protected $casts = [
        'premium_report_sent_to' => 'json'
    ];

	//-----------------------------------------------------
    //  Function M37.1: servicesoffered
    //  	Gets the services offered
    //-----------------------------------------------------
    public function servicesoffered() {
        return $this->hasMany('tblservicesoffer');
    }
	
	//-----------------------------------------------------
    //  Function M37.2: getEntityId
    //  	Gets the entity id using login id
    //-----------------------------------------------------
	public static function getEntityId($login_id)
    {
        $entity = Entity::where('loginid', $login_id)->first();
		return ($entity!=null) ? $entity->entityid : null;
    }
	
	//-----------------------------------------------------
    //  Function M37.3: getCompanyName
    //  	Gets the company name using login id
    //-----------------------------------------------------
	public static function getCompanyName($login_id)
	{
		$entity = Entity::where('loginid', $login_id)->first();
		return ($entity!=null) ? $entity->companyname : null;
	}
    
	//-----------------------------------------------------
    //  Function M37.4: checkSupportEnabled
    //  	Check if entity is not yet finished
    //-----------------------------------------------------
    public static function checkSupportEnabled($login_id)
	{
        $reports_count  = 0;
        $sts            = STS_NG;
        
		$reports_count      = self::where('loginid', $login_id)
            ->where('status', '!=', 7)
            ->count();
        
        if (0 >= $reports_count) {
            $sts    = STS_NG;
        }
        else {
            $sts    = STS_OK;
        }
        
        return $sts;
	}
    
	//-----------------------------------------------------
    //  Function M37.5: checkCompanyNameValidity
    //  	Check if company name is not yet used
    //-----------------------------------------------------
    public static function checkCompanyNameValidity($login_id, $company_name)
	{
        $sts    = STS_NG;
        
        $company_data      = self::where('companyname', $company_name)->first();
        if ($company_data == null) {
            $sts = STS_OK;
        }
        else if ($login_id == $company_data['loginid']) {
            $sts    = STS_OK;
        }
        else {
            /*  No Processing   */
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function XXX.X: updateReportPaymentStatus
    //  	Update Payment Status of the Report
    //-----------------------------------------------------
    public function updateReportPaymentStatus($id){
        Entity::where('entityid', $id)->update(['is_paid' => 1]);
        return;
    }

}
