<?php

//======================================================================
//  Class M58: SME Keys
//      SME Keys Table Database Model
//======================================================================
class Keys extends Eloquent
{	
	protected $table = 'config';
	protected $guarded = 'created_at, updated_at';

}