<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentAnalysis extends Model
{
    	protected $table = 'document_analyses';
      protected $guarded = array('id', 'created_at', 'updated_at');


      public function AnalysisResult()
      {
          return $this->hasOne(AnalysisResult::class, 'document_id');
      }
}
