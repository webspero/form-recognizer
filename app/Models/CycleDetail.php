<?php
//======================================================================
//  Class M30: CycleDetail
//      CycleDetail Table Database Class
//======================================================================
class CycleDetail extends Eloquent {
	
	protected $table = 'tblcycledetails';
	protected $guarded = array('id', 'created_at', 'updated_at');

}
