<?php
//======================================================================
//  Class M1: AuditLogs
//      AuditLogs Table Database Class
//======================================================================
class AuditLogs extends Eloquent {
	
	protected $table = 'auditlogs';			// table name
	protected $primary_key = 'auditLogId';
	
    //-----------------------------------------------------
    //  Function M1.1: SaveActivityLog
    //  	Function to log activity of user
    //      Params: 'loginid' = currently logged in loginid; 'activity' = string param, what the user was doing;
    //              'errorCode' = error code based on error code list; 'stackTrace' = stack trace of error exception;
    //-----------------------------------------------------
    public static function saveActivityLog($params)
    {
        $loginid = 0;
        $activity = "";
        $entityid = 0;
        $errorCode = null;
        $stackTrace = null;

        if(isset(Session::get('entity')->entityid)) {
            $entityid = Session::get('entity')->entityid;
        }

        if(isset($params['loginid'])) {
            $loginid = $params['loginid'];
        }

        if(isset($params['activity'])) {
            $activity = $params['activity'];
        }

        if(isset($params['errorCode'])) {
            $errorCode = $params['errorCode'];
        }

        if(isset($params['stackTrace'])) {
            $stackTrace = $params['stackTrace'];
        }
        
        $log = new AuditLogs();
        $log->entity_id = $entityid;
        $log->login_id = $loginid;
        $log->activity = $activity;
        $log->error_code = $errorCode;
        $log->stack_trace = $stackTrace;
        $log->save();
    }
}
