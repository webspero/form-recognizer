<?php

class OcrPossibleFailure extends Eloquent {
    protected $table = 'ocr_possible_failure'; //table
    protected $primaryKey = 'id';
    protected $fillable = ['entity_id'];
    public $timestamps = false;
}
