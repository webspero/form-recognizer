<?php

//======================================================================
//  Class M100: Facebook Like
//      Facebook Like Table Database Model
//======================================================================
class WebCalculatorLike extends Eloquent
{
	protected 	$table 		= 'web_calculator_likes';      /* The database table used by the model.    */
	protected 	$primaryKey = 'web_calculator_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M100.1: getUserFbLike
    //      Fetches the User's FB Like
    //-----------------------------------------------------
    public function getUserFbLike($fb_user_id)
    {
        $data   = self::where('fb_user_id', $fb_user_id)->first();
        
        return $data;
    }
    
    //-----------------------------------------------------
    //  Function M100.1: addCalculatorFbLike
    //      Saves an FB User Like
    //-----------------------------------------------------
    public function addCalculatorFbLike($fb_user_id)
    {
        /*  Variable Declaration        */
        $sts        = STS_NG;
        $country    = STR_EMPTY;
        
        /*  Instantiate Database Class  */
        $fb_like_data   = new WebCalculatorLike;
        
        /*  Saves Data to the Database  */
        $fb_like_data->fb_user_id       = $fb_user_id;
        $fb_like_data->status           = STS_OK;
        $fb_like_data->date_liked       = date('Y-m-d H:i:s');
        
        $sts                            = $fb_like_data->save();
        
        return $sts;
    }
}