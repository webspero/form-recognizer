<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiSecret extends Model
{
    protected $guarded = array('id', 'created_at', 'updated_at');
}
