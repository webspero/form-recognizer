<?php

//======================================================================
//  Class M69: Password Change Request
//      Password Change Request Table Database Model
//======================================================================
class PasswordRequest extends Eloquent
{
	protected 	$table 		= 'password_requests';  /* The database table used by the model.    */
	protected 	$primaryKey = 'request_id';         /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M69.1: addPasswordRequest
    //      Adds a new Request
    //-----------------------------------------------------
    function addPasswordRequest($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        $request    = new PasswordRequest;
        
        /*  Save to Database            */
        $request['user_id']             = $data['user_id'];
        $request['notification_code']   = $data['notif_code'];
        $request['status']              = PASSWORD_REQ_PENDING;
        $request['date_requested']      = $data['date_requested'];
        $request['date_used']           = $data['date_requested'];
        
        $sts    = $request->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M69.2: checkNotificationCode
    //      Check Email and notification code match
    //-----------------------------------------------------
    function checkNotificationCode($email, $notif_code)
    {
        /*  Gets the User's information */
        $user_data  = User::where('email', $email)
            ->first();
        
        /*  Checks if the notification code matches the sent code to user's email   */
        $pass_data = self::where('user_id', $user_data['loginid'])
            ->where('notification_code', $notif_code)
            ->orderBy('request_id', 'desc')
            ->first();
            
        return $pass_data;
    }
    
    //-----------------------------------------------------
    //  Function M69.3: updateNotificationSts
    //      Update Notification Status
    //-----------------------------------------------------
    function updateNotificationSts($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        $request    = self::find($data['request_id']);
        
        /*  Saves to Databae            */
        $request['status']         = PASSWORD_REQ_COMPLETE;
        $request['date_used']      = $data['date_updated'];
        
        $sts    = $request->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M69.4: checkUserPasswordRequest
    //      Gets the user's existing password request
    //-----------------------------------------------------
    function checkUserPasswordRequest($email)
    {
        $pass_data = self::leftJoin('tbllogin', 'tbllogin.loginid', '=', 'password_requests.user_id')
            ->where('tbllogin.email', $email)
            ->orderBy('password_requests.request_id', 'desc')
            ->select(['request_id', 'notification_code', 'password_requests.status AS req_status'])
            ->first();
        
        return $pass_data;
    }

}
