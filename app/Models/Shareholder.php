<?php

//======================================================================
//  Class M90: Shareholder
//      Shareholder Table Database Model
//======================================================================
class Shareholder extends Eloquent
{	
	protected $table = 'tblshareholders';
	protected $guarded = 'id, created_at, updated_at';

}
