<?php

class NotificationEmails extends Eloquent
{
    //
    protected $table = 'notification_emails'; //table
    protected $guarded = array('id'); // locked fields
    protected $primaryKey = 'id';
}
