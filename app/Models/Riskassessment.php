<?php

//======================================================================
//  Class M87: Risk Assessment
//      Risk Assessment Table Database Model
//======================================================================
class Riskassessment extends Eloquent
{	
	protected $table = 'tblriskassessment';
	protected $primaryKey = 'riskassessmentid';
}
