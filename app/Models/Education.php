<?php
//======================================================================
//  Class M34: Education
//      Education Table Database Class
//======================================================================
class Education extends Eloquent {
	
	protected $table        = 'tbleducation';
    protected $primaryKey   = 'educationid';
    
}
