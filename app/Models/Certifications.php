<?php
//======================================================================
//  Class M23: Certifications
//      Certifications Table Database Class
//======================================================================
class Certifications extends Eloquent {
	
	protected $table        = 'tblcertifications';
    protected $primaryKey   = 'certificationid';

}
