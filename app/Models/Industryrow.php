<?php
//======================================================================
//  Class M49: Industryrow
//      Industryrow Table Database Class
//======================================================================
class Industryrow extends Eloquent {
	
	protected $table = 'tblindustry_row';
	protected $primaryKey = 'industry_row_id';

}
