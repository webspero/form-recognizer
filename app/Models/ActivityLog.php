<?php
//======================================================================
//  Class M1: ActivityLog
//      ActivityLog Table Database Class
//======================================================================
class ActivityLog extends Eloquent {
	
	protected $table = 'activity_logs';			// table name
	protected $guarded = array('id');			
	
	//-----------------------------------------------------
    //  Function M1.1: saveLog
    //  	Function to create a log and update last activity
    //-----------------------------------------------------
	public static function saveLog()
	{
        if (isset(Session::get('entity')->entityid)) {
            $entityid = Session::get('entity')->entityid;
            $activity_log = ActivityLog::firstOrCreate(array('entity_id' => $entityid));
            if($activity_log->first_activity){
                $activity_log->last_activity = date('c');
            } else {
                $activity_log->first_activity = date('c');
            }
            $activity_log->save();
        }
	}
	
	//-----------------------------------------------------
    //  Function M1.2: saveSubmitLog
    //  	Function to log the first submission of SME questionnaire
    //-----------------------------------------------------
	public static function saveSubmitLog()
	{
        if (isset(Session::get('entity')->entityid)) {
            $entityid = Session::get('entity')->entityid;
            $activity_log = ActivityLog::firstOrCreate(array('entity_id' => $entityid));
            if(!$activity_log->first_submission){
                $activity_log->first_submission = date('c');
            }
            $activity_log->save();
        }
	}
}
