<?php
//======================================================================
//  Class M51: IndustryWeightsHistory
//      IndustryWeightsHistory Table Database Class
//======================================================================
class IndustryWeightsHistory extends Eloquent {
	
	protected $table = 'tblbankindustryweightshistory';
	protected $guarded = array('id', 'created_at', 'updated_at');

}
