<?php

//======================================================================
//  Class M52: Insurance
//      Insurance Table Database Class
//======================================================================
class Insurance extends Eloquent
{
	
	protected $table = 'tblinsurance';
	protected $primaryKey = 'insuranceid';

}
