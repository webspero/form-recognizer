<?php

//======================================================================
//  Class M39: Feedback Tab Rating
//      Tab Feedback Table Database Model
//======================================================================
class FeedbackTabRating extends Eloquent
{
	protected 	$table 		= 'feedback_tab_ratings'; /* The database table used by the model.    */
	protected 	$primaryKey = 'tab_rating_id';        /* The table's primary key                  */
	public		$timestamps = false;
    
    //-----------------------------------------------------
    //  Function M39.1: getUserFeedbackByIgnorSts
    //      Adds a new ignored feedback record
    //-----------------------------------------------------
    public function getUserFeedbackByIgnorSts($ignored)
    {
        $feedback_data  = self::where('ignored', $ignored)
            ->orderBy('tab_id', 'asc')
            ->get();
            
        return $feedback_data;
    }
    
    //-----------------------------------------------------
    //  Function M39.2: getUserFeedback
    //      Acquires the User Feedback on a current report
    //-----------------------------------------------------
    public function getUserFeedback($transaction_id)
    {
        $feedback_data  = self::where('transaction_id', $transaction_id)->get();
        return $feedback_data;
    }
    
    //-----------------------------------------------------
    //  Function M39.3: addTabFeedbackRating
    //      Adds a new Feedback
    //-----------------------------------------------------
    public function addTabFeedbackRating($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        $feedback   = new FeedbackTabRating;
        
        /*  Save to Database            */
        $feedback['transaction_id']          = $data['transaction_id'];
        $feedback['tab_id']                  = $data['tab_id'];
        $feedback['ignored']                 = $data['ignored'];
        $feedback['user_difficulty']         = $data['user_difficulty'];
        $feedback['user_recommendation']     = $data['user_recommendation'];
        $feedback['user_rating']             = $data['user_rating'];
        $feedback['status']                  = ACTIVE;
        $feedback['date_rated']              = date('Y-m-d');
        
        $sts    = $feedback->save();
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function M39.4: checkTabFeedbackRating
    //      Checks if there is already a Feedback for a tab in a report
    //-----------------------------------------------------
    public function checkTabFeedbackRating($entity_id, $tab_id)
    {
        $fback_data = self::where('transaction_id', $entity_id)
                ->where('tab_id', $tab_id)
                ->first();
                
        return $fback_data;
    }
    
}
