<?php
//======================================================================
//  Class M46: IncomeStatement
//      IncomeStatement Table Database Class
//======================================================================
class IncomeStatement extends Eloquent {
	
	protected $table = 'fin_income_statements';
	protected $guarded = array('id', 'created_at', 'updated_at');
	
	//-----------------------------------------------------
    //  Function M46.1: financialReport
    //  	gets the financial report that this IS belongs to
    //-----------------------------------------------------
	public function financialReport()
    {
        //JM
        //return $this->belongsTo('FinancialReport', 'financial_report_id');
        return $this->belongsTo(FinancialReport::class);
    }
	
}
