<?php
//======================================================================
//  Class M102: InnodataRecipient
//      innodata_recipients Table Database Class
//======================================================================
class InnodataRecipient extends Eloquent
{
    public const INNODATA = 1;
    public const CREDITBPO = 2;

    protected $table        = 'innodata_recipients';
    protected $primaryKey   = 'id';
}
