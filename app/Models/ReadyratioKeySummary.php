<?php

//======================================================================
//  Class M106: ReadyratioKeySummary
//      readyratio_key_summary Table Database Class
//======================================================================

class ReadyratioKeySummary extends Eloquent
{
    protected $table        = 'readyratio_key_summary';
    protected $primaryKey   = 'id';
}
