<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Bank;
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'tbllogin';
    protected $primaryKey = 'loginid';

    //-----------------------------------------------------
    //  Function M97.1: getUserByEmail
    //      Gets a User according to Email
    //-----------------------------------------------------
    function getUserByEmail($email)
    {
        $user_data      = self::where('email', $email)->first();

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.2: paginateUserCompanyByRole
    //      Gets a limited number of User
    //-----------------------------------------------------
    function paginateUserCompanyByRole($role, $limit)
    {
        $user_data = self::leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
            ->where('tbllogin.role', $role)
            ->where('tbllogin.status', 2)
            ->select(
                'tbllogin.loginid',
                'tbllogin.firstname',
                'tbllogin.lastname', 
                'tbllogin.email', 
                
                'tblentity.companyname'
            )
            ->paginate($limit);

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.3: getUserCompanyByRole
    //      Gets the Accounts first Report
    //-----------------------------------------------------
    function getUserCompanyByRole($user_id)
    {
        $user_data = self::leftJoin('tblentity', 'tblentity.loginid', '=', 'tbllogin.loginid')
            ->where('tbllogin.loginid', $user_id)
            ->select(
                'tbllogin.loginid',
                'tbllogin.firstname',
                'tbllogin.lastname', 
                'tbllogin.email', 
                'tblentity.entityid',
                'tblentity.companyname'
            )
            ->first();

        return $user_data;
    }
    
    //-----------------------------------------------------
    //  Function M97.4: updateUserPassword
    //      Change the User's Password
    //-----------------------------------------------------
    function updateUserPassword($data)
    {
        /*  Variable Declaration        */
        $sts        = FAILED;
        
        /*  Gets User Information       */
        $request    = self::where('email', $data['email'])
            ->first();
        
        /*  Save changes to the Database    */
        $request['password']    = Hash::make($data['new_password']);
        $request['updated_at']  = $data['date_updated'];
        
        $request->save();
        
        return $sts;
    }

    public function checkIfOrganizationUsed($organizationId)
    {
        $supervisor = self::where('bank_id', $organizationId)
                          ->where('role', 4)->get();
        if ($supervisor->count() > 0){
            return true;
        } else{
            return false;
        }
    }

    public function getAssociatedBank($bank_id) 
    {
        $bank = Bank::select('bank_name')->where('id', $bank_id)->first();
        return $bank;
    }

}
