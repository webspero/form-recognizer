<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Shareholder extends Model
{	
    protected $table = 'tblshareholders';

    //-----------------------------------------------------
    //  Function #: Create Shareholders record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $shareholders = self::where('entity_id', $previousReportId)->get();

        foreach ($shareholders as $shareholder) {
            $newShareholder = new Shareholder;

            $newShareholder->entity_id = $reportDTO->getReportId();
            $newShareholder->name = $shareholder->name;
            $newShareholder->address = $shareholder->address;
            $newShareholder->amount = $shareholder->amount;
            $newShareholder->percentage_share = $shareholder->percentage_share;
            $newShareholder->id_no = $shareholder->id_no;
            $newShareholder->nationality = $shareholder->nationality;
            $newShareholder->created_at = date('Y-m-d H:i:s');
            $newShareholder->updated_at = date('Y-m-d H:i:s');

            $newShareholder->save();
        }
    }

    public function getShareHoldersByPage($reportId, $page, $limit)
    {
    	
        $page -= 1;
        $shareHolders = self::select(
                        'id',
                        'entity_id as reportid',
                        'firstname as firstname',
                        'middlename as middlename',
                        'lastname as lastname',
                        'birthdate as birthdate',
                        'address as address',
                        'amount as amountofshareholdings',
                        'percentage_share as percentageshare',    
                        'id_no as tinno',
                        'nationality as nationality'
                        )
                  ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                  ->orderBy('id', 'ASC')
                  ->skip($limit * ($page))
                  ->take($limit)
                  ->get();
        return $shareHolders;
    }

    public function getShareHoldersByReportId($reportId)
    {
        $shareHolders = self::select(
            'id',
            'entity_id as reportid',
            'firstname as firstname',
            'middlename as middlename',
            'lastname as lastname',
            'birthdate as birthdate',
            'address as address',
            'amount as amountofshareholdings',
            'percentage_share as percentageshare',    
            'id_no as tinno',
            'nationality as nationality'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $shareHolders;
    }

    public function getShareHoldersById($id)
    {
        $shareHolders = self::select(
                        'id',
                        'entity_id as reportid',
                        'firstname as firstname',
                        'middlename as middlename',
                        'lastname as lastname',
                        'birthdate as birthdate',
                        'address as address',
                        'amount as amountofshareholdings',
                        'percentage_share as percentageshare',    
                        'id_no as tinno',
                        'nationality as nationality'
                        )
                ->where(['id' => $id, 'is_deleted' => 0])
                ->first()
                ->toArray();

        return $shareHolders;
    }
}