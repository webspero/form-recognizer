<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class MajorLocation extends Model
{	
    protected $table = 'tblmajorlocation';

    //-----------------------------------------------------
    //  Function #: Create Insurance record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $majorLocations = self::where('entity_id', $previousReportId)->get();

        foreach ($majorLocations as $majorLocation) {
            $newMajorLocation = new MajorLocation;

            $newMajorLocation->entity_id = $reportDTO->getReportId();
            $newMajorLocation->marketlocation = $majorLocation->marketlocation;
            $newMajorLocation->created_at = date('Y-m-d H:i:s');
            $newMajorLocation->updated_at = date('Y-m-d H:i:s');

            $newMajorLocation->save();
        }
    }

    public function getMajorMarketLocationByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $majorMarketLocation = self::select(
                                    'marketlocationid as id',
                                    'entity_id as reportid',
                                    'marketlocationid as marketlocation'    
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('marketlocationid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $majorMarketLocation;
    }

    public function getMajorMarketLocationByReportId($reportId)
    {
        $majorMarketLocation = self::select(
            'marketlocationid as id',
            'entity_id as reportid',
            'marketlocationid as marketlocation'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $majorMarketLocation;
    }

    public function getMajorMarketLocationById($marketlocationid)
    {
        $majorMarketLocation = self::select(
                                    'marketlocationid as id',
                                    'entity_id as reportid',
                                    'marketlocationid as marketlocation'
                                    )
                            ->where(['marketlocationid' => $marketlocationid, 'is_deleted' => 0])
                            ->first()
                            ->toArray();

        return $majorMarketLocation;
    }
}