<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use CreditBPO\Models\Entity;

class BusinessOutlookIndex extends Model {
    protected $table = 'business_outlooks';
    protected $primaryKey = 'business_outlook_id';
    public $timestamps = false;

    public function getByEntityId($entityId) {
        $entity = new Entity();
        return $entity
            ->leftJoin(
                'tblindustry_main as t',
                't.industry_main_id',
                '=',
                'tblentity.industry_main_id')
            ->where('tblentity.entityid', '=', $entityId)
            ->first();
    }
}
