<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model {
    protected $primaryKey = 'id';
    protected $table = 'currency_rate';
}
