<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Dropbox extends Model
{
	protected $table        = 'tbldropboxentity';
	protected $primaryKey   = 'id';
	protected $guarded      = array('id', 'created_at', 'updated_at');
}
