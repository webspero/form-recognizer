<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Login extends Model {
    protected $primaryKey = 'loginid';
    protected $table = 'tbllogin';
    protected $guarded = ['loginid', 'created_at', 'updated_at'];
}
