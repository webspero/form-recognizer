<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordRequest extends Model
{
    protected $table = 'password_requests'; 
	protected $primaryKey = 'request_id';
    public $timestamps = false;
    
    function addPasswordRequest($passwordRequestDetails)
    {
        $passwordRequest = new PasswordRequest();
        $passwordRequest['user_id'] = $passwordRequestDetails['userid'];
        $passwordRequest['notification_code'] = $passwordRequestDetails['code'];
        $passwordRequest['status'] = PASSWORD_REQ_PENDING;
        $passwordRequest['date_requested'] = $passwordRequestDetails['daterequested'];
        $passwordRequest['date_used'] = $passwordRequestDetails['dateused'];
        return $passwordRequest->save();
    }

    function userPasswordRequest($loginId, $code)
    {
        $passwordRequest = self::where('user_id', $loginId)
                               ->where('notification_code', $code)
                               ->orderBy('request_id', 'desc')
                               ->first();
        return $passwordRequest;
    }

    function getPasswordRequestByEmail($email)
    {
        $passwordRequest = self::leftJoin('tbllogin', 'tbllogin.loginid', '=', 'password_requests.user_id')
                               ->where('tbllogin.email', $email)
                               ->orderBy('password_requests.request_id', 'desc')
                               ->select(['request_id', 'notification_code', 'password_requests.status AS req_status'])
                               ->first();
        
        return $passwordRequest;
    }

    function updateNotificationStatus($passwordRequestId)
    {
        $passwordRequest = self::find($passwordRequestId);
        $passwordRequest['status'] = PASSWORD_REQ_COMPLETE;
        $passwordRequest['date_used'] = date('Y-m-d');
        return $passwordRequest->save();
    }
}