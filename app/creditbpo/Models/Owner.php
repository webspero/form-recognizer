<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model {
    protected $primaryKey = 'ownerid';
    protected $table = 'tblowner';

    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)->get();
    }

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function getFromPreviousReport($previousReportId)
    {
        return self::where('entity_id', $previousReportId)->get();
    }

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportId, $oldRecord)
    {
        $newOwner = new Owner;

        $newOwner->entity_id = $reportId;
        $newOwner->firstname = $oldRecord->firstname;
        $newOwner->middlename = $oldRecord->middlename;
        $newOwner->lastname = $oldRecord->lastname;
        $newOwner->nationality = $oldRecord->nationality;
        $newOwner->birthdate = $oldRecord->birthdate;
        $newOwner->civilstatus = $oldRecord->civilstatus;
        $newOwner->profession = $oldRecord->profession;
        $newOwner->email = $oldRecord->email;
        $newOwner->address1 = $oldRecord->address1;
        $newOwner->address2 = $oldRecord->address2;
        $newOwner->cityid = $oldRecord->cityid;
        $newOwner->province = $oldRecord->province;
        $newOwner->zipcode = $oldRecord->zipcode;
        $newOwner->present_address_status = $oldRecord->present_address_status;
        $newOwner->no_yrs_present_address = $oldRecord->no_yrs_present_address;
        $newOwner->phone = $oldRecord->phone;
        $newOwner->tin_num = $oldRecord->tin_num;
        $newOwner->percent_of_ownership = $oldRecord->percent_of_ownership;
        $newOwner->number_of_year_engaged = $oldRecord->number_of_year_engaged;
        $newOwner->position = $oldRecord->position;
        $newOwner->created_at = date('Y-m-d H:i:s');
        $newOwner->updated_at = date('Y-m-d H:i:s');

        $newOwner->save();

        return $newOwner;
    }
}
