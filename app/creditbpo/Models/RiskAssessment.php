<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class RiskAssessment extends Model {
    protected $table = 'tblriskassessment';

    /**
     * @param int $entityId
     * @return Model
     */
    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)
            ->get();
    }

    /**
     * @param int $entityId
     * @return Model
     */
    public function getForCountByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)
            ->groupBy('risk_assessment_name', 'risk_assessment_solution')
            ->get();
    }

    public function createWithPreviousData($reportDTO, $riskAssessment)
    {
        $newRiskAssessment = new RiskAssessment();
        $newRiskAssessment->entity_id = $reportDTO->getReportId();
		$newRiskAssessment->risk_assessment_name = $riskAssessment->risk_assessment_name;
	    $newRiskAssessment->risk_assessment_solution = $riskAssessment->risk_assessment_solution;
		$newRiskAssessment->created_at = date('Y-m-d H:i:s');
        $newRiskAssessment->updated_at = date('Y-m-d H:i:s');
        $newRiskAssessment->save();
    }
}
