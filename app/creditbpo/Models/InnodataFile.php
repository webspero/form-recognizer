<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;


class InnodataFile extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'innodata_files';

    public function saveInnodataFile($innodatafiles){
    	$innodata = new InnodataFile();
    	$innodata->entity_id = $innodatafiles['reportid'];
    	$innodata->file_name = $innodatafiles['documentfilename'];
    	$innodata->is_uploaded = 0;
    	$innodata->is_deleted = 0;
    	$innodata->save();
    	return $innodata;
	}
	
	public function getInnodataFilesById($id){
		$innodata_file = self::where('entity_id', $id)->where('is_deleted', 0)->get();
		return $innodata_file;
	}

	public function deleteRawFile($id){
		// print_r($id); die();
		$innodata = self::where('id', $id)->update(array('is_deleted' => 1));
        return $innodata;
	}
}