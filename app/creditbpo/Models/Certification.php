<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{	
    protected $table = 'tblcertifications';

    //-----------------------------------------------------
    //  Function #: Create Certifications record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $certifications = self::where('entity_id', $previousReportId)->get();

        foreach ($certifications as $certification) {
            $newCertification = new Certification;

            $newCertification->entity_id = $reportDTO->getReportId();
            $newCertification->certification_details = $certification->certification_details;
            $newCertification->certification_reg_no = $certification->certification_reg_no;
            $newCertification->certification_reg_date = $certification->certification_reg_date;
            $newCertification->certification_doc = $certification->certification_doc;
            $newCertification->created_at = date('Y-m-d H:i:s');
            $newCertification->updated_at = date('Y-m-d H:i:s');

            $newCertification->save();
        }
    }

    public function getCertificationByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $certification = self::select(
                                    'certificationid as id',
                                    'entity_id as reportid',
                                    'certification_details as certification',
                                    'certification_reg_no as registrationno',
                                    'certification_reg_date as registrationdate',
                                    'certification_doc as uploadfile'    
                                    )
                              ->where(['entity_id'=>$reportId,'is_deleted' => 0])
                              ->orderBy('certificationid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $certification;
    }

    public function getCertificationByReportId($reportId)
    {
        $certification = self::select(
            'certificationid as id',
            'entity_id as reportid',
            'certification_details as certification',
            'certification_reg_no as registrationno',
            'certification_reg_date as registrationdate',
            'certification_doc as uploadfile'
        )->where(['entity_id'=>$reportId,'is_deleted' => 0])->get();
        
        return $certification;
    }

    public function getCertificationById($relatedcompaniesid)
    {
        $certification = self::select(
                                    'certificationid as id',
                                    'entity_id as reportid',
                                    'certification_details as certification',
                                    'certification_reg_no as registrationno',
                                    'certification_reg_date as registrationdate',
                                    'certification_doc as uploadfile'
                                    )
                            ->where(['certificationid'=>$relatedcompaniesid,'is_deleted'=>0])
                            ->first()
                            ->toArray();

        return $certification;
    }
}