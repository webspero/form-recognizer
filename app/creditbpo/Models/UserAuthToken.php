<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class UserAuthToken extends Model
{
	protected $table = 'user_auth_token';
    protected $primaryKey = 'id';
    
    public function getTokenDetails($accessToken, $refreshToken)
    {
        $tokenDetails = self::where('access_token', '=', $accessToken)
                            ->orWhere('refresh_token', '=', $refreshToken)
                            ->where('status', '=', INT_TRUE)
                            ->first();
        return $tokenDetails;
    }

    public function getAccessToken($accessToken)
    {
        $tokenDetails = self::where('access_token', $accessToken)
                            ->where('status', '=', INT_TRUE)
                            ->first();
        return $tokenDetails;
    }

    public function getAccessTokenDetails($accessToken, $clientId)
    {
        $tokenDetails = self::where('access_token', $accessToken)
                            ->where('client_id', $clientId)
                            ->where('status', '=', INT_TRUE)
                            ->first();
        return $tokenDetails;
    }

    public function getRefreshTokenDetails($refreshToken, $clientId)
    {
        $tokenDetails = self::where('refresh_token', $refreshToken)
                            ->where('client_id', $clientId)
                            ->where('status', '=', INT_TRUE)
                            ->first();
        return $tokenDetails;
    }

    public function addNewToken($tokenDetails)
    {
        $userAuthToken = new UserAuthToken();
        $userAuthToken->login_id = $tokenDetails->getUserId();
        $userAuthToken->client_id = $tokenDetails->getClientId();
        $userAuthToken->access_token = $tokenDetails->getAccessToken();
        $userAuthToken->refresh_token = $tokenDetails->getRefreshToken();
        $userAuthToken->expires_in = $tokenDetails->getExpiresIn();
        $userAuthToken->status = $tokenDetails->getStatus();
        $userAuthToken->save();
        return $userAuthToken;
    }

    public function updateAccessToken($userAuthTokenDto)
    {
        $userAuthToken = self::find($userAuthTokenDto->getId());
        $userAuthToken->access_token = $userAuthTokenDto->getAccessToken();
        $userAuthToken->expires_in = $userAuthTokenDto->getExpiresIn();
        $userAuthToken->save();
        return $userAuthToken;
    }

    public function removeAccessToken($userAuthToken)
    {
        $userAuthToken->status = INT_FALSE;
        $userAuthToken->save();
    }
}