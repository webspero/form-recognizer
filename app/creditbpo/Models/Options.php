<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = 'options';	

    public static function generate_array($name){
    	$results = Options::select('label','value')->where('name',$name)->get();

    	$array = [];
    	foreach ($results as $record) {
    		$array[$record->value] = $record->label;
    	}

    	return $array;
    }
}
