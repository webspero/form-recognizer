<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class AutomateStandaloneRating extends Model {
    /** Variable Initialization */
    const STATUS_PENDING = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_ERROR = 2;
    const STATUS_SUCCESS = 3;
    const STATUS_INNODATA_PENDING = 4;
    const STATUS_INNODATA_WAITING = 5;

    protected $table = 'automate_standalone_rating';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @return array
     */

    //------------------------------------------------------------------
    //  Function 57.0: getPendingReports
    //      Get pending Reports for Automation
    //------------------------------------------------------------------
    public function getPendingReports() {
        return $this->leftJoin('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')->select('automate_standalone_rating.id', 'automate_standalone_rating.entity_id', 'automate_standalone_rating.status', 'automate_standalone_rating.created_at', 'automate_standalone_rating.updated_at')->where('automate_standalone_rating.status', '=', self::STATUS_PENDING)->get();
    }

    //------------------------------------------------------------------
    //  Function 57.0: getPendingReports
    //      Get pending Reports for Automation
    //------------------------------------------------------------------
    public function getSinglePendingReport($entityId) {
        return $this->leftJoin('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')->select('automate_standalone_rating.id', 'automate_standalone_rating.entity_id', 'automate_standalone_rating.status', 'automate_standalone_rating.created_at', 'automate_standalone_rating.updated_at')->where('tblentity.entityid',$entityId)->get();
    }

    /**
     * @return array
     */

    //------------------------------------------------------------------
    //  Function 57.1: getInnodataPendingReports
    //      Get Pending Report for Innodata
    //------------------------------------------------------------------
    public function getInnodataPendingReports($allowPremiumReportProcess = 1) {
        $query = $this->leftJoin('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')->select('automate_standalone_rating.id', 'automate_standalone_rating.entity_id', 'automate_standalone_rating.status', 'automate_standalone_rating.created_at', 'automate_standalone_rating.updated_at');
        if($allowPremiumReportProcess == 2)
        {
            $query = $query->where('tblentity.is_premium', '!=', 1);
        }
        return $query->where('automate_standalone_rating.status', '=', self::STATUS_INNODATA_PENDING)->get();
    }

    /**
     * @return array
     */

    //------------------------------------------------------------------
    //  Function 57.2: getInnodataPendingReports
    //      Get Waiting Report for Innodata
    //------------------------------------------------------------------
    public function getInnodataWaitingReports() {
        return $this->leftJoin('tblentity', 'tblentity.entityid', '=', 'automate_standalone_rating.entity_id')->select('automate_standalone_rating.id', 'automate_standalone_rating.entity_id', 'automate_standalone_rating.status', 'automate_standalone_rating.created_at', 'automate_standalone_rating.updated_at')->where('automate_standalone_rating.status', '=', self::STATUS_INNODATA_WAITING)->get();

        //->where('tblentity.is_premium', '!=', 1)
    }

    /**
     * @return array
     */
    public function getFailedReports() {

    }

    /**
     * @param int $entityId
     * @param int $status
     * @return void
     */
    public function changeReportStatus($entityId, $status = self::STATUS_IN_PROGRESS) {

    }
}
