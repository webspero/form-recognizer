<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class SMEScore extends Model {
    protected $table = 'tblsmescore';
    protected $softDelete = true;

    public function getByReportId($reportId)
    {
        $smeScore = self::where('entityid', $reportId)->first();

        if($smeScore !== null ) {
            return $smeScore;
        } else {
            return null;
        }
    }
}
