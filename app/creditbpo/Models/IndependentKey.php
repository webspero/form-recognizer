<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: IndependentKey
//  IndependentKey Table Database Class
//======================================================================
class IndependentKey extends Model
{	
    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
	protected $table = 'independent_keys';
    protected $guarded = array('id', 'created_at', 'updated_at');

    //-----------------------------------------------------
    //  Function #.1: getIndependentKey
    //  Gets the independent key details
    //-----------------------------------------------------
    function getIndependentKey($key)
    {
        $independentKey = self::where('serial_key', $key)
                              ->where('is_used', 0)
                              ->first();
        return $independentKey;
    }

    //-----------------------------------------------------
    //  Function #: getIndependentKeyByReportId
    //  Gets the client key details based on report id
    //-----------------------------------------------------
    function getIndependentKeyByReportId($reportId)
    {
        $clientKey = self::where('entity_id', $reportId)->first();
        return $clientKey;
    }

    //-----------------------------------------------------
    //  Function #.1: updateIndependentKey
    //  Update independent key details
    //-----------------------------------------------------
    function updateIndependentKey($userReportDto, $clientKey)
    {
        $clientKey->is_used = $userReportDto->getIsUsed();
        $clientKey->login_id = $userReportDto->getUserId();
        $clientKey->entity_id = $userReportDto->getReportId();
        $clientKey->save();
    }

    //-----------------------------------------------------
    //  Function #.1: consumeKey
    //  Update key record such that it is tagged as used
    //-----------------------------------------------------
    function consumeKey($clientKey, $userId, $reportId)
    {
        $keyRecord = self::where('id', $clientKey['id'])->first();

        $keyRecord->is_used = INT_TRUE;
        $keyRecord->login_id = $userId;
        $keyRecord->entity_id = $reportId;
        $keyRecord->save();
    }
}