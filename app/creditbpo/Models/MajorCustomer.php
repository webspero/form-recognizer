<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class MajorCustomer extends Model
{
    protected $table = 'tblmajorcustomer';
    protected $primaryKey = 'majorcustomerrid';
    protected $fillable = ['customer_name',
                           'customer_share_sales', 
                           'customer_address', 
                           'customer_contact_person', 
                           'customer_email', 
                           'customer_phone', 
                           'customer_started_years', 
                           'customer_years_doing_business', 
                           'customer_settlement', 
                           'customer_order_frequency', 
                           'relationship_satisfaction'];

    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)->get();
    }

    /*
    * This will replace the funcion 'getByEntityId'
    *  will retain the getByEntityId function because there might be some functions that are using it.
    */
    public function getMajorCustomersByReportId($reportId)
    {
        $majorCustomers = self::select(
            'majorcustomerrid as id',
            'entity_id as reportid',
            'customer_name as name',
            'customer_address as address',
            'customer_contact_person as contactperson',
            'customer_phone as contactnumber',
            'customer_email as email',
            'customer_started_years as yearstarted',
            'customer_years_doing_business as yearsdoingbusiness',
            'relationship_satisfaction as relationshipsatisfaction',
            'customer_share_sales as salespercentage',
            'customer_order_frequency as orderfrequency',
            'customer_settlement as paymentbehavior'
        )->where('entity_id', $reportId)->get();
        
        return $majorCustomers;
    }

    public function getMajorCustomerById($id)
    {
        $majorCustomer = self::select(
                                    'majorcustomerrid as id',
                                    'entity_id as reportid',
                                    'customer_name as name',
                                    'customer_address as address',
                                    'customer_contact_person as contactperson',
                                    'customer_phone as contactnumber',
                                    'customer_email as email',
                                    'customer_started_years as yearstarted',
                                    'customer_years_doing_business as yearsdoingbusiness',
                                    'relationship_satisfaction as relationshipsatisfaction',
                                    'customer_share_sales as salespercentage',
                                    'customer_order_frequency as orderfrequency',
                                    'customer_settlement as paymentbehavior'
                                    )
                            ->where('majorcustomerrid', $id)
                            ->first();
        return $majorCustomer;
    }

    //-----------------------------------------------------
    //  Function #: Create MajorCustomer record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $majorCustomers = self::where('entity_id', $previousReportId)->get();

        foreach ($majorCustomers as $majorCustomer) {
            $newMajorCustomer = new MajorCustomer;

            $newMajorCustomer->entity_id = $reportDTO->getReportId();
            $newMajorCustomer->customer_name = $majorCustomer->customer_name;
            $newMajorCustomer->customer_share_sales = $majorCustomer->customer_share_sales;
            $newMajorCustomer->customer_address = $majorCustomer->customer_address;
            $newMajorCustomer->customer_contact_person = $majorCustomer->customer_contact_person;
            $newMajorCustomer->customer_contact_details = $majorCustomer->customer_contact_details;
            $newMajorCustomer->customer_email = $majorCustomer->customer_email;
            $newMajorCustomer->customer_phone = $majorCustomer->customer_phone;
            $newMajorCustomer->customer_experience = $majorCustomer->customer_experience;
            $newMajorCustomer->customer_started_years = $majorCustomer->customer_started_years;
            $newMajorCustomer->customer_years_doing_business = $majorCustomer->customer_years_doing_business;
            $newMajorCustomer->customer_settlement = $majorCustomer->customer_settlement;
            $newMajorCustomer->customer_order_frequency = $majorCustomer->customer_order_frequency;
            $newMajorCustomer->created_at = date('Y-m-d H:i:s');
            $newMajorCustomer->updated_at = date('Y-m-d H:i:s');

            $newMajorCustomer->save();
        }
    }

    public function getReportMajorCustomerByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $majorCustomers = self::select(
                                    'majorcustomerrid as id',
                                    'entity_id as reportid',
                                    'customer_name as name',
                                    'customer_address as address',
                                    'customer_contact_person as contactperson',
                                    'customer_phone as contactnumber',
                                    'customer_email as email',
                                    'customer_started_years as yearstarted',
                                    'customer_years_doing_business as yearsdoingbusiness',
                                    'relationship_satisfaction as relationshipsatisfaction',
                                    'customer_share_sales as salespercentage',
                                    'customer_order_frequency as orderfrequency',
                                    'customer_settlement as paymentbehavior'
                                    )
                              ->where('entity_id', $reportId)
                              ->orderBy('majorcustomerrid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $majorCustomers;
    }

    public function addNewMajorCustomer($majorCustomerDto)
    {
        $majorCustomer = new MajorCustomer();
        $majorCustomer->entity_id = $majorCustomerDto->getReportId();
        $majorCustomer->customer_name = $majorCustomerDto->getName();
        $majorCustomer->customer_share_sales = $majorCustomerDto->getSalesPercentage();
        $majorCustomer->customer_address = $majorCustomerDto->getAddress();
        $majorCustomer->customer_contact_person = $majorCustomerDto->getContactPerson();
        $majorCustomer->customer_phone = $majorCustomerDto->getContactNumber();
        $majorCustomer->customer_email = $majorCustomerDto->getEmail();
        $majorCustomer->customer_started_years = $majorCustomerDto->getYearStarted();
        $majorCustomer->customer_years_doing_business = $majorCustomerDto->getYearsDoingBusiness();
        $majorCustomer->customer_settlement = $majorCustomerDto->getPaymentBehavior();
        $majorCustomer->customer_order_frequency = $majorCustomerDto->getOrderFrequency();
        $majorCustomer->relationship_satisfaction = $majorCustomerDto->getRelationshipSatisfaction();
        $majorCustomer->save();
        return $majorCustomer;
    }

    public function updateMajorCustomer($id, $data)
    {
        $majorCustomer = self::find($id);
        if (!empty($majorCustomer)){
            $majorCustomer->update(array_filter($data));
        }
        return $majorCustomer;
    }

    public function deleteMajorCustomer($id)
    {
        $majorCustomer = self::find($id);
        if (!empty($majorCustomer)){
            return $majorCustomer->delete();
        } else{
            return null;
        }
    }
}
