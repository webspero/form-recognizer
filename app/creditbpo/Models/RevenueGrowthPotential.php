<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class RevenueGrowthPotential extends Model {
    protected $table = 'tblrevenuegrowthpotential';

    public function getByReportId($reportId)
    {
        return $this->where('entity_id', '=', $reportId)
            ->get();
    }

    public function createWithPreviousData($reportDTO, $revenueGrowth)
    {
        $newRevenueGrowth = new RevenueGrowthPotential();
        $newRevenueGrowth->entity_id = $reportDTO->getReportId();
		$newRevenueGrowth->current_market = $revenueGrowth->current_market;
	    $newRevenueGrowth->new_market = $revenueGrowth->new_market;
		$newRevenueGrowth->created_at = date('Y-m-d H:i:s');
        $newRevenueGrowth->updated_at = date('Y-m-d H:i:s');
        $newRevenueGrowth->save();
    }
}
