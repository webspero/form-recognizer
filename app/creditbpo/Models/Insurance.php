<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{	
    protected $table = 'tblinsurance';

    //-----------------------------------------------------
    //  Function #: Create Insurance record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $insurances = self::where('entity_id', $previousReportId)->get();

        foreach ($insurances as $insurance) {
            $newInsurance = new Insurance;

            $newInsurance->entity_id = $reportDTO->getReportId();
            $newInsurance->insurance_type = $insurance->insurance_type;
            $newInsurance->insured_amount = $insurance->insured_amount;
            $newInsurance->created_at = date('Y-m-d H:i:s');
            $newInsurance->updated_at = date('Y-m-d H:i:s');

            $newInsurance->save();
        }
    }

    public function getInsuranceByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $insurance = self::select(
                                    'insuranceid as id',
                                    'entity_id as reportid',
                                    'insurance_type as insurancetype',
                                    'insured_amount as insuredamount'    
                                    )
                              ->where('entity_id', $reportId)
                              ->orderBy('insuranceid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $insurance;
    }

    public function getInsuranceByReportId($reportId)
    {
        $insurance = self::select(
            'insuranceid as id',
            'entity_id as reportid',
            'insurance_type as insurancetype',
            'insured_amount as insuredamount'  
        )->where('entity_id', $reportId)->get();
        
        return $insurance;
    }

    public function getInsuranceById($insuranceid)
    {
        $insurance = self::select(
                            'insuranceid as id',
				            'entity_id as reportid',
				            'insurance_type as insurancetype',
				            'insured_amount as insuredamount'
                                    )
                            ->where('insuranceid', $insuranceid)
                            ->first()
                            ->toArray();

        return $insurance;
    }
}