<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{	
    protected $table = 'tbldirectors';

    //-----------------------------------------------------
    //  Function #: Create Directors record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $directors = self::where('entity_id', $previousReportId)->get();

        foreach ($directors as $director) {
            $newDirector = new Director;

            $newDirector->entity_id = $reportDTO->getReportId();
            $newDirector->name = $director->name;
            $newDirector->address = $director->address;
            $newDirector->id_no = $director->id_no;
            $newDirector->percentage_share = $director->percentage_share;
            $newDirector->nationality = $director->nationality;
            $newDirector->created_at = date('Y-m-d H:i:s');
            $newDirector->updated_at = date('Y-m-d H:i:s');

            $newDirector->save();
        }
    }

    public function getDirectorsByPage($reportId, $page, $limit)
    {

        $page -= 1;
        $directors = self::select(
                                    'id as id',
                                    'entity_id as reportid',
                                    'firstname as firstname',
                                    'middlename as middlename',
                                    'lastname as lastname',
                                    'birthdate as birthdate',
                                    'id_no as tinno',
                                    'address as address',
                                    'nationality as nationality',
                                    'percentage_share as percentageShare'
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('id', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $directors;
    }

    public function getDirectorsByReportId($reportId)
    {
        $directors = self::select(
            'id as id',
            'entity_id as reportid',
            'firstname as firstname',
            'middlename as middlename',
            'lastname as lastname',
            'birthdate as birthdate',
            'id_no as tinno',
            'address as address',
            'nationality as nationality',
            'percentage_share as percentageShare'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $directors;
    }

    public function getDirectorsById($id)
    {
        $directors = self::select(
                        'id as id',
                        'entity_id as reportid',
                        'firstname as firstname',
                        'middlename as middlename',
                        'lastname as lastname',
                        'birthdate as birthdate',
                        'id_no as tinno',
                        'address as address',
                        'nationality as nationality',
                        'percentage_share as percentageShare'
                        )
                ->where(['id' => $id, 'is_deleted' => 0])
                ->first()
                ->toArray();

        return $directors;
    }
}