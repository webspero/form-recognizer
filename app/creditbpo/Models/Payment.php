<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{	
	protected $table = 'tblpayment';

    //-----------------------------------------------------
    //  Function M#.1: Count number of successful PayPal transactions made
    //-----------------------------------------------------
    public function countPaymentsMade($transactionId)
    {
        return self::where('transactionid', $transactionId)->count();
    }

    //-----------------------------------------------------
    //  Function M#.1: Count number of successful PayPal transactions made
    //-----------------------------------------------------
    public function addPayment($newPayment)
    {
        $payment = new Payment();
        $payment->loginid = $newPayment['loginid'];
        $payment->transactionid = $newPayment['transactionid'];
        $payment->transactiontype = $newPayment['transactiontype'];
        $payment->paymenttype = $newPayment['paymenttype'];
        $payment->paymentamount = $newPayment['paymentamount'];
        $payment->currencycode = $newPayment['currencycode'];
        $payment->save();
    }
}
