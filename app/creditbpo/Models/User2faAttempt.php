<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: User 2FA (2 Factor Authentication) Attempts
//	User 2fa Attempts Database Model
//======================================================================
class User2faAttempt extends Model {
	
	/*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
    protected 	$table 		= 'user_2fa_attempts';      /* The database table used by the model.    */
	protected 	$primaryKey = 'user_attempt_id';        /* The table's primary key                  */
	public		$timestamps = false;

	//-----------------------------------------------------
    //  Function #: getUserAttempt
    //	Gets a User's 2FA Attempts
	//-----------------------------------------------------
    public function getUserAttempt($loginId)
    {
        $data = self::where('login_id', $loginId)
                            ->orderBy('user_attempt_id', 'desc')
                            ->first();
        return $data;
	}
	
	//-----------------------------------------------------
    //  Function #: add2faAttempt
    //	Adds a new 2FA Attempt
    //-----------------------------------------------------
    public function add2faAttempt($userId, $user2faDetails)
    {
        /* Instantiate Database Class */
        $userAttempt = new User2faAttempt;

        /* Saves Data to the Database */
        $userAttempt->login_id = $userId;
        $userAttempt->image = $user2faDetails['image'];
        $userAttempt->passcode = $user2faDetails['passcode'];
        $userAttempt->attempts = $user2faDetails['attempts'];
        $userAttempt->status = $user2faDetails['status'];
        $userAttempt->last_attempt = $user2faDetails['lastattempt'];
        $userAttempt->save();
    }
}
