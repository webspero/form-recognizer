<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class BankIndustry extends Model {
    protected $table = 'tblbankindustryweights';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
