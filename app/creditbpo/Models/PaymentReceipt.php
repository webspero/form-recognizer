<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: Payment Receipt
//      Payment Receipt Table Database Model
//======================================================================
class PaymentReceipt extends Model
{
	protected 	$table 		= 'payment_receipts';   /* The database table used by the model.    */
	protected 	$primaryKey = 'receipt_id';         /* The table's primary key                  */
    public		$timestamps = false;

    //-----------------------------------------------------
    //  Function M71.1: addPaymentReceipt
    //      Adds a new Receipt
    //-----------------------------------------------------
    public function addPaymentReceipt($data)
    {
        /*  Variable Declaration        */
        $sts = STS_OK;
        
        /*  Saves data into the database    */
        $db_data = new PaymentReceipt;
        $db_data->login_id = $data['login_id'];
        $db_data->reference_id = $data['reference_id'];
        $db_data->total_amount = $data['total_amount'];
        $db_data->vatable_amount = $data['vatable_amount'];
        $db_data->vat_amount = $data['vat_amount'];
        $db_data->item_quantity = $data['item_quantity'];
        $db_data->payment_method = $data['payment_method'];
        $db_data->item_type = $data['item_type'];
        $db_data->sent_status = $data['sent_status'];
        $db_data->date_sent = $data['date_sent'];
        $db_data->amount = $data['amount'];
        $db_data->price = $data['price'];
        $db_data->discount = $data['discount'];

        if(isset($data['api_match_account_details'])){
            $db_data->api_match_account_details = $data['api_match_account_details'];
        }
        
        /*  Save successful */
        if ($db_data->save()) {
            $sts = $db_data->receipt_id;
        }
        /*  Save failed     */
        else {
            $sts = STS_NG;
        }
        
        return $sts;
    }
}