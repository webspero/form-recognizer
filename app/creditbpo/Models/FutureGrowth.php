<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class FutureGrowth extends Model {
    protected $table = 'tblfuturegrowth';

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $futureGrowths = self::where('entity_id', $previousReportId)->get();

        foreach ($futureGrowths as $futureGrowth) {
            $newFutureGrowth = new FutureGrowth;

            $newFutureGrowth->entity_id = $reportDTO->getReportId();
            $newFutureGrowth->futuregrowth_name = $futureGrowth->futuregrowth_name;
            $newFutureGrowth->futuregrowth_estimated_cost = $futureGrowth->futuregrowth_estimated_cost;
            $newFutureGrowth->futuregrowth_implementation_date = $futureGrowth->futuregrowth_implementation_date;
            $newFutureGrowth->futuregrowth_source_capital = $futureGrowth->futuregrowth_source_capital;
            $newFutureGrowth->planned_proj_goal = $futureGrowth->planned_proj_goal;
            $newFutureGrowth->planned_goal_increase = $futureGrowth->planned_goal_increase;
            $newFutureGrowth->proj_benefit_date = $futureGrowth->proj_benefit_date;
            $newFutureGrowth->created_at = date('Y-m-d H:i:s');
            $newFutureGrowth->updated_at = date('Y-m-d H:i:s');

            $newFutureGrowth->save();
        }
    }
}
