<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalLoan extends Model {
    protected $table = 'tblpersonalloans';
    
    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportId, $oldOwner, $newOwner)
    {
        $personalLoans = self::where('owner_id', $oldOwner->ownerid)->get();

        foreach ($personalLoans as $personalLoan) {
            $newPersonalLoan = new PersonalLoan;

            $newPersonalLoan->owner_id = $newOwner->ownerid;
            $newPersonalLoan->entity_id = $reportId;
            $newPersonalLoan->purpose = $personalLoan->purpose;
            $newPersonalLoan->balance = $personalLoan->balance;
            $newPersonalLoan->as_of = $personalLoan->as_of;
            $newPersonalLoan->monthly_amortization = $personalLoan->monthly_amortization;
            $newPersonalLoan->creditor = $personalLoan->creditor;
            $newPersonalLoan->due_date = $personalLoan->due_date;
            $newPersonalLoan->created_at = date('Y-m-d H:i:s');
            $newPersonalLoan->updated_at = date('Y-m-d H:i:s');

            $newPersonalLoan->save();
        }
    }
}
