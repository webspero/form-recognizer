<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

//======================================================================
//  Class #: Report (Entity)
//  Report (Entity) Database Model
//======================================================================
class Entity extends Model {
    const STATUS_FOR_REVIEW = 5;

    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
    protected $primaryKey = 'entityid';
    protected $table = 'tblentity';
    protected $fillable = [
        'companyname',
        'company_tin',
        'entity_type',
        'tin_num',
        'sec_reg_date',
        'industry_main_id',
        'industry_sub_id',
        'industry_row_id',
        'total_assets',
        'total_assets_grouping',
        'website',
        'email',
        'address1',
        'province',
        'cityid',
        'zipcode',
        'phone',
        'no_yrs_present_address',
        'date_established',
        'number_year',
        'description',
        'employee_size',
        'updated_date',
        'number_year_management_team',
        'current_bank',
        'fs_input_who',
        'value_of_outstanding_contracts'
    ];

    //-----------------------------------------------------
    //  Function #: checkSupportEnabled
    //  Check if entity is not yet finished
    //-----------------------------------------------------
    public static function checkSupportEnabled($login_id)
	{
        $reports_count = 0;
        $sts = STS_NG;
        
        $reports_count = self::where('loginid', $login_id)
        ->where('status', '!=', 7)
        ->count();
        
        if (0 >= $reports_count) {
            $sts = STS_NG;
        } else {
            $sts = STS_OK;
        }
        
        return $sts;
    }
    
    //-----------------------------------------------------
    //  Function #: addNewReport
    //  Saves new report record
    //-----------------------------------------------------
    function addNewReport($reportDetails)
    {
        $report = new Entity();
        $report->loginid = $reportDetails->getUserId();
        $report->companyname = $reportDetails->getCompanyName();
        $report->entity_type = $reportDetails->getCompanyType();
        $report->website = $reportDetails->getWebsite();
        $report->is_agree = $reportDetails->getIsAgree();
        $report->is_paid = $reportDetails->getIsPaid();
        $report->status = $reportDetails->getReportStatus();
        $report->is_independent = $reportDetails->getIsIndependent();
        $report->current_bank = $reportDetails->getOrganizationId();
        $report->discount_code = $reportDetails->getDiscountCode();
        $report->save();

        return $report;
    }

    //-----------------------------------------------------
    //  Function #: getReportById
    //  Gets a report based on a Report ID
    //-----------------------------------------------------
    public function getReportsByUserIdByPage($userId, $page, $limit)
    {
        $page -= 1;

        $reports = DB::table('tblentity')
            ->select('tblentity.entityid', 'tblentity.companyname', 'tblentity.date_established', 'refcitymun.citymunDesc', 'tblindustry_class.class_description', 'tblentity.is_paid', 'tblentity.status', 'tblentity.completed_date')
            ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
            ->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
            ->orderBy('tblentity.entityid', 'desc')
            ->groupBy('tblentity.entityid')
            ->where('loginid', $userId)
            ->skip($limit * ($page))
            ->take($limit)
            ->get();
        return $reports;
    }
    
    //-----------------------------------------------------
    //  Function #: getReportById
    //  Gets a report based on a Report ID
    //-----------------------------------------------------
    public function getReportsByUserId($userId)
    {
        $reports = DB::table('tblentity')
            ->select('tblentity.entityid', 'tblentity.companyname', 'tblentity.date_established', 'refcitymun.citymunDesc', 'tblindustry_class.class_description', 'tblentity.is_paid', 'tblentity.status', 'tblentity.completed_date')
            ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
            ->leftJoin('tblindustry_class', 'tblentity.industry_row_id', '=', 'tblindustry_class.class_code')
            ->orderBy('tblentity.entityid', 'desc')
            ->groupBy('tblentity.entityid')
            ->where('loginid', $userId)
            ->get();
        return $reports;
    }

    //-----------------------------------------------------
    //  Function #: getReportById
    //  Gets a report based on a Report ID
    //-----------------------------------------------------
    public function getReportByReportId($reportId)
    {
        $report = self::where('entityid', $reportId)->first();
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: getFirstReportByUserId
    //  Get first report of a user
    //-----------------------------------------------------
    function getFirstReportByUserId($userId)
    {
        $report = self::where('loginid', $userId)->first();
        return $report;
    }
    
    //-----------------------------------------------------
    //  Function #: getUnpaidReportByUserId
    //  Gets a report of a user which is tagged as unpaid
    //-----------------------------------------------------
    public function getUnpaidReportByUserId($userId)
    {
        $report = self::where('loginid', $userId)->where('is_paid', 0)->first();
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: updateReportPaymentStatus
    //  Update report payment status to paid
    //-----------------------------------------------------
    function updateReportPaymentStatus($reportDetails)
    {
        $report = self::find($reportDetails->getReportId());
        $report->is_paid = $reportDetails->getIsPaid();
        $report->save();
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: togglePaymentFlags
    //  Update the payment flag when a payment has been made for a report
    //-----------------------------------------------------
    public function togglePaidFlag($userId, $reportId = null)
    {
        $report = self::where('loginid', $userId);
        
        if($reportId != null)
            $report->where("entityid", $reportId);

        $report->update(
            array(
                'is_paid' => STS_OK
            )
        );
    }

    //-----------------------------------------------------
    //  Function #: recordTransactionId
    //  Update the payment flag when a payment has been made for a report
    //-----------------------------------------------------
    public function recordTransactionId($transactionId, $reportId)
    {
        $report = self::where('entityid', $reportId)->update(
            array(
                'transaction_id' => $transactionId
            )
            );
    }

    //-----------------------------------------------------
    //  Function #: checkCompanyNameValidity
    //  	Check if company name is not yet used
    //-----------------------------------------------------
    public function checkCompanyNameValidity($userId, $companyName)
	{
        $isValid = false;
		$companyData = self::where('companyname', $companyName)->get();
        
        /* If Name is found in database, check if user owns these report records */
        if(@count($companyData) > 0) {
            foreach ($companyData as $company) {
                if($company["loginid"] == $userId) {
                    $isValid = true;
                    break;
                }
            }
        } else {
            $isValid = true;
        }

        return $isValid;
    }
    
    //-----------------------------------------------------
    //  Function #: Add Report and Return ID
    //-----------------------------------------------------
    public function addReportReturnId($newReport)
	{
        $report = new Entity();
        $report->loginid = $newReport['loginid'];
        $report->companyname = $newReport['companyname'];
        $report->entity_type = $newReport['entity_type'];
        $report->is_agree = $newReport['is_agree'];
        $report->status = $newReport['status'];
        $report->is_independent = $newReport['is_independent'];
        $report->current_bank = $newReport['current_bank'];
        $report->discount_code = $newReport['discount_code'];
        $report->is_paid = $newReport['is_paid'];
        $report->is_premium = $newReport['is_premium'];
        $report->cityid = $newReport['city'];
        $report->province = $newReport['province'];
        $report->zipcode = $newReport['zipcode'];
        $report->cityid = $newReport['city'];
        $report->province = $newReport['province'];
        $report->zipcode = $newReport['zipcode'];
        $report->industry_main_id = $newReport['industry_main'];
        $report->industry_sub_id = $newReport['industry_sub'];
        $report->industry_row_id = $newReport['industry_row'];
        $report->isIndustry = $newReport['isIndustry'];
        $report->save();

        return $report->entityid;
    }

    //-----------------------------------------------------
    //  Function #: Update New Report with data from an existing report, either Standalone or Full
    //-----------------------------------------------------
    public function updateWithPreviousData($reportDTO)
	{
        $previousReport = $reportDTO->getPreviousReport();
        $currentReport = self::getReportByReportId($reportDTO->getReportId());

        $currentReport->company_tin = $previousReport['company_tin'];
        $currentReport->industry_main_id = $previousReport['industry_main_id'];
        $currentReport->industry_sub_id = $previousReport['industry_sub_id'];
        $currentReport->industry_row_id = $previousReport['industry_row_id'];
        $currentReport->total_assets = $previousReport['total_assets'];
        $currentReport->address1 = $previousReport['address1'];
        $currentReport->address2 = $previousReport['address2'];
        $currentReport->cityid = $previousReport['cityid'];
        $currentReport->province = $previousReport['province'];
        $currentReport->zipcode = $previousReport['zipcode'];
        $currentReport->phone = $previousReport['phone'];
        $currentReport->former_address1 = $previousReport['former_address1'];
        $currentReport->former_address2 = $previousReport['former_address2'];
        $currentReport->former_cityid = $previousReport['former_cityid'];
        $currentReport->former_province = $previousReport['former_province'];
        $currentReport->former_zipcode = $previousReport['former_zipcode'];
        $currentReport->former_phone = $previousReport['former_phone'];
        $currentReport->no_yrs_present_address = $previousReport['no_yrs_present_address'];
        $currentReport->date_established = $previousReport['date_established'];
        $currentReport->number_year = $previousReport['number_year'];
        $currentReport->description = $previousReport['description'];
        $currentReport->employee_size = $previousReport['employee_size'];
        $currentReport->updated_date = $previousReport['updated_date'];
        $currentReport->number_year_management_team = $previousReport['number_year_management_team'];
        $currentReport->related_companies = $previousReport['related_companies'];
        $currentReport->tin_num = $previousReport['tin_num'];
        $currentReport->sec_num = $previousReport['sec_num'];
        $currentReport->is_subscribe = $previousReport['is_subscribe'];
        $currentReport->sec_reg_date = $previousReport['sec_reg_date'];
        $currentReport->sec_cert = $previousReport['sec_cert'];
        $currentReport->entity_type = $previousReport['entity_type'];
        $currentReport->permit_to_operate = $previousReport['permit_to_operate'];
        $currentReport->authority_to_borrow = $previousReport['authority_to_borrow'];
        $currentReport->authorized_signatory = $previousReport['authorized_signatory'];
        $currentReport->tax_registration = $previousReport['tax_registration'];
        $currentReport->income_tax = $previousReport['income_tax'];
        $currentReport->total_asset_grouping = $previousReport['total_asset_grouping'];
        $currentReport->save();
    }

    //-----------------------------------------------------
    //  Function #: Update New Report with data from an existing report, either Standalone or Full
    //-----------------------------------------------------
    public function getBusinessDetailsOfReport($reportId)
	{
        $report = DB::table('tblentity')
            ->select(
                'tblentity.entityid',
                'tblentity.companyname',
                'tblentity.company_tin',
                'tblentity.entity_type',
                'tblentity.tin_num',
                'tblentity.sec_reg_date',
                'tblentity.fs_input_who',
                'tblindustry_main.main_code',
                'tblindustry_main.main_title',
                'tblindustry_sub.sub_code',
                'tblindustry_sub.sub_title',
                'tblindustry_group.group_code',
                'tblindustry_group.group_description',
                'tblentity.total_assets',
                'tblentity.total_asset_grouping',
                'tblentity.website',
                'tblentity.email',
                'tblentity.address1',
                'tblentity.province',
                'zipcodes.id as cityid',
                'zipcodes.city',
                'tblentity.zipcode',
                'tblentity.phone',
                'tblentity.no_yrs_present_address',
                'tblentity.date_established',
                'tblentity.number_year',
                'tblentity.description',
                'tblentity.employee_size',
                'tblentity.updated_date',
                'tblentity.number_year_management_team',
                'tblbank.id as organizationid',
                'tblbank.bank_name as organizationname'
                )
            ->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.main_code')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.sub_code')
            ->leftJoin('tblindustry_group', 'tblentity.industry_row_id', '=', 'tblindustry_group.group_code')
            ->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
            ->where('tblentity.entityid', $reportId)
            ->first();
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: getReportByReportIdAndUserId
    //  Check fi user owns report
    //-----------------------------------------------------
    public function checkReportOwner($reportId, $userId)
    {
        $report = self::where('entityid', $reportId)->where('loginid', $userId)->first();
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: updateBusinessDetails
    //  Update a Report's Business Details
    //-----------------------------------------------------
    public function updateBusinessDetails($reportId, $updateData)
    {
        $report = self::find($reportId);
        if (!empty($report)){
            $report->update(array_filter($updateData));
        }
        return $report;
    }

    //-----------------------------------------------------
    //  Function #: updateProgress
    //-----------------------------------------------------
    public function updateProgress($reportId, $progress)
    {
        $report = self::where('entityid', $reportId)->update(
            array(
                'completion' => $progress
            )
            );
    }

    //-----------------------------------------------------
    //  Function #: updateStatus
    //-----------------------------------------------------
    public function updateStatus($reportId, $status)
    {
        $report = self::where('entityid', $reportId)->update(
            array(
                'status' => $status
            )
            );
    }

    public function getCompanyReportList($companyName){
        $report = self::where('companyname', $companyName)->get();
    }

    public function getAllUserReport($userId){
        $reports = self::where('loginid', $userId)->get();
        return $reports;
    }

    
}
