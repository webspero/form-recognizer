<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class BalanceSheet extends Model {
    protected $table = 'fin_balance_sheets';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getByFinancialReportId($financialReportId)
    {
        return self::where('financial_report_id', $financialReportId)->orderBy('index_count', 'asc')->get();
    }
}
