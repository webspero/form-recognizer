<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use Hash;

//======================================================================
//  Class #: User Account
//  User Account Database Model
//======================================================================
class User extends Model {

    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
    protected $primaryKey = 'loginid';
	protected $table = 'tbllogin';
    protected $hidden = array('password', 'remember_token');

    //-----------------------------------------------------
    //  Function #.1: getUserByEmail
    //  Gets a User according to Email
    //-----------------------------------------------------
    function getUserByEmail($email)
    {
        $userDetails = self::where('email', $email)->first();
        return $userDetails;
    }

    //-----------------------------------------------------
    //  Function #.3: getUserById
    //  Get a user based on his loginid
    //-----------------------------------------------------
    function getUserById($userId)
    {
        $userDetails = self::where('loginid', $userId)->first();
        return $userDetails;
    }

    //-----------------------------------------------------
    //  Function #.3: getUserByActivationCode
    //  Get a user based on activation code and email
    //-----------------------------------------------------
    function getUserByActivationCode($userAccountDto)
    {
        $userData = self::where('activation_code', $userAccountDto->getActivationCode())
                          ->where('email', $userAccountDto->getEmail())
                          ->first();
        return $userData;
    }
    
    //-----------------------------------------------------
    //  Function #: addNewUser
    //  Saves new user record
    //-----------------------------------------------------
    function addNewUser($userDetails)
    {
        $user = new User();
        $user->email = $userDetails->getEmail();
        $user->password = Hash::make($userDetails->getPassword());
        $user->firstname = $userDetails->getFirstName();
        $user->lastname = $userDetails->getLastName();
        $user->name = $userDetails->getFirstName() . $userDetails->getLastName();
        $user->activation_code = $userDetails->getActivationCode();
        $user->activation_date = $userDetails->getActivationDate();
        $user->role = $userDetails->getRole();
        $user->status = $userDetails->getAccountStatus();
        $user->save();
        return $user;
    }

    //-----------------------------------------------------
    //  Function #.1: updateAccountActivation
    //  Activates user account
    //-----------------------------------------------------
    function updateAccountActivation($userDetails)
    {
        $user = User::find($userDetails->getUserId());
        $user->status = $userDetails->getStatus();
        $user->activation_code = $userDetails->getActivationCode();
        $user->activation_date = $userDetails->getActivationDate();
        $user->save();

        return $user;
    }

    //-----------------------------------------------------
    //  Function #.1: updateUserStatus
    //  Update a user's status based on passed parameter
    //-----------------------------------------------------
    function updateUserStatus($userId, $status)
    {
        $user_data = self::where('loginid', $userId)->update(
            array(
                'status' => $status
            )
        );
    }

    function updateUserPassword($userDetails)
    {
        $user = self::where('email', $userDetails->getEmail())->first();
        $user['password'] = Hash::make($userDetails->getPassword());
        $user['updated_at'] = date('Y-m-d');
        return $user->save();
    }

    public function updateBillingDetails($billingDetails){
        $user = User::find($billingDetails['user_id']);
        $user->street_address = $billingDetails['street_address'];
        $user->city = $billingDetails['city'];
        $user->province = $billingDetails['province'];
        $user->zipcode = $billingDetails['zipcode'];
        $user->phone = $billingDetails['phoneNumber'];
        $user->save();
        return;
    }
}
