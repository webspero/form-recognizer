<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class IndustryMain extends Model {
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    protected $primaryKey = 'industry_main_id';
    protected $table = 'tblindustry_main';

    //-----------------------------------------------------
    //  Function #: getMainIndustries
    //  Gets all Main Industries
    //-----------------------------------------------------
    public function getMainIndustries()
    {
        return self::all();
    }

    //-----------------------------------------------------
    //  Function #: getMainIndustriesByPage
    //  Gets all Main Industries with pagination
    //-----------------------------------------------------
    public function getMainIndustriesByPage($page, $limit)
    {
        $page -= 1;

        $mainIndustries = self::skip($limit * ($page))
            ->take($limit)
            ->get();

        return $mainIndustries;
    }

    public function getIndustrySectionByPage($page, $limit){
        $page -= 1;
        $industry = DB::table('tblindustry_main')
                  ->select(DB::raw('tblindustry_main.main_code, tblindustry_main.main_title as industry_main, tblindustry_sub.sub_code, 
                  tblindustry_sub.sub_title as industry_division, tblindustry_class.class_code, tblindustry_class.class_description as industry_group'))
                  ->leftJoin('tblindustry_sub', 'tblindustry_sub.main_code', '=', 'tblindustry_main.main_code')
                  ->leftJoin('tblindustry_group', 'tblindustry_group.sub_code', '=', 'tblindustry_sub.sub_code')
                  ->leftJoin('tblindustry_class', 'tblindustry_class.group_code', '=', 'tblindustry_group.group_code')
                  ->skip($limit * ($page))
                  ->take($limit)
                  ->get();
        return $industry;
    }

    public function getIndustrySection(){
        $industry = DB::table('tblindustry_main')
                    ->select(DB::raw('tblindustry_main.main_code, tblindustry_main.main_title as industry_main, tblindustry_sub.sub_code, 
                    tblindustry_sub.sub_title as industry_division, tblindustry_class.class_code, tblindustry_class.class_description as industry_group'))
                    ->leftJoin('tblindustry_sub', 'tblindustry_sub.main_code', '=', 'tblindustry_main.main_code')
                    ->leftJoin('tblindustry_group', 'tblindustry_group.sub_code', '=', 'tblindustry_sub.sub_code')
                    ->leftJoin('tblindustry_class', 'tblindustry_class.group_code', '=', 'tblindustry_group.group_code')
                    ->get();
        return $industry;
    }
}
