<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model {
    const TYPE_FINANCIAL_REPORT = 9;
    const GROUP_PRIVATE_DOCUMENT = 99999;

    protected $primaryKey = 'documentid';
    protected $table = 'tbldocument';
    protected $guarded = ['documentid', 'created_at', 'updated_at'];
    
    function saveDocument($documentDetails)
    {
        $document = new Document();
        $document->entity_id = $documentDetails['reportid'];
        $document->document_type = $documentDetails['documenttype'];
        $document->upload_type = $documentDetails['uploadtype'];
        $document->document_group = $documentDetails['documentgroup'];
        $document->document_orig = $documentDetails['documentfilename'];
        $document->document_rename = $documentDetails['documentrename'];
        $document->save();
        return $document;
    }

    function getFsDocumentByReportId($reportId)
    {
        $document = self::where('entity_id', $reportId)
                        ->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)
                        ->where('upload_type', UPLOAD_TYPE_FS_TEMPLATE)
                        ->orderBy('documentid', 'ASC')
                        ->where('is_deleted', 0)
                        ->get();
        return $document;
    }

    function getBsDocumentByReportId($reportId)
    {
        $document = self::where('entity_id', $reportId)
                        ->where('document_group', DOCUMENT_GROUP_BANK_STATEMENT)
                        ->orderBy('documentid', 'ASC')
                        ->get();
        return $document;
    }

    function getUbDocumentByReportId($reportId)
    {
        $document = self::where('entity_id', $reportId)
                        ->where('document_group', DOCUMENT_GROUP_UTILITY_BILLS)
                        ->orderBy('documentid', 'ASC')
                        ->get();
        return $document;
    }

    function getDocumentById($id)
    {
        $document = self::where('documentid', $id)->first();
        return $document;
    }

    function deleteDocument($ids = [])
    {
        $document = self::whereIn('documentid', $ids)->delete();
        return $document;
    }

    function updateDocumentType($document, $documentType)
    {
        $document->document_type = $documentType;
        $document->save();
    }

    function getBalanceSheetByReportId($reportId)
    {
        $document = self::where('entity_id', $reportId)
                        ->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)
                        ->where('upload_type', UPLOAD_TYPE_BALANCE_SHEET)
                        ->orderBy('documentid', 'ASC')
                        ->get();
        return $document;
    }

    function getIncomeStatementDocumentByReportId($reportId)
    {
        $document = self::where('entity_id', $reportId)
                        ->where('document_group', DOCUMENT_GROUP_FS_TEMPLATE)
                        ->where('upload_type', UPLOAD_TYPE_INCOME_STATEMENT)
                        ->orderBy('documentid', 'ASC')
                        ->get();
        return $document;
    }
}
