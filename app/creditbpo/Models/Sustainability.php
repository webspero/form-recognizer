<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Sustainability extends Model {
    protected $guarded = [
        'sustainabilityid',
        'created_at',
        'updated_at',
    ];
    protected $primaryKey = 'sustainabilityid';
    protected $table = 'tblsustainability';

    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)->first();
    }

    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $sustainability = self::where('entity_id', $previousReportId)->first();
		if($sustainability){
			$newSustainability = new Sustainability();
			$newSustainability->entity_id = $reportDTO->getReportId();
			$newSustainability->succession_plan = $sustainability->succession_plan;
			$newSustainability->succession_timeframe = $sustainability->succession_timeframe;
			$newSustainability->competition_details = $sustainability->competition_details;
			$newSustainability->competition_landscape = $sustainability->competition_landscape;
			$newSustainability->competition_timeframe = $sustainability->competition_timeframe;
			$newSustainability->ev_susceptibility_economic_recession = $sustainability->ev_susceptibility_economic_recession;
			$newSustainability->ev_foreign_exchange_interest_sensitivity = $sustainability->ev_foreign_exchange_interest_sensitivity;
			$newSustainability->ev_commodity_price_volatility = $sustainability->ev_commodity_price_volatility;
			$newSustainability->ev_governement_regulation = $sustainability->ev_governement_regulation;
			$newSustainability->cg_structure = $sustainability->cg_structure;
			$newSustainability->mt_executive_team = $sustainability->mt_executive_team;
			$newSustainability->mt_middle_management = $sustainability->mt_middle_management;
			$newSustainability->mt_staff = $sustainability->mt_staff;
			$newSustainability->crm_Structure = $sustainability->crm_Structure;
			$newSustainability->bpsf_Regular_cycle_business = $sustainability->bpsf_Regular_cycle_business;
			$newSustainability->tax_payments = $sustainability->tax_payments;
			$newSustainability->save();
		}
    }
}
