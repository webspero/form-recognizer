<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;
use CreditBPO\Models\Entity;
use DB;

class GRDP extends Model {
    protected $table = 'grdp';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function getBoost($year, $region) {
        $grdpArray = [];

        $grdp = self::select('grdp')
            ->where('year', $year)
            ->get();

        foreach($grdp as $g){
            $grdpArray[] = $g->grdp;
        }

        $n = @count($grdpArray);
        $mean = array_sum($grdpArray) / $n;
        $carry = 0.0;

        foreach ($grdpArray as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };

        $sd = sqrt($carry / ($n - 1));

        $grdp_region = self::where('year', $year)
            //->where('region', $region)
            ->first()->grdp;

        $sd_max = $mean + $sd;
        $sd_min = $mean - $sd;

        if ($grdp_region < $mean) {
            if ($grdp_region < $sd_min) {
                return 0.80;
            }

            return 0.90;
        } else {
            if ($grdp_region > $sd_max) {
                return 1.20;
            }

            return 1.10;
        }
    }

    public static function getBoostCurrentYear($entityId) {
        $year = self::getLatestYear();

        $region = Entity::where('entityid', $entityId)
            ->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
            ->first();

        $boost = self::getBoost($year, $region->region);
        $reverse = 1 + (1 - $boost);

        return [
            'boost' => $boost,
            'percent' => self::getBoostPercent($boost),
            'reverse' => $reverse,
        ];
    }

    public static function getBoostPercent($boost) {
        switch ($boost) {
            case 0.8:
                return '-20%';
            case 0.9:
                return '-10%';
            case 1.1:
                return '+10%';
            case 1.2:
                return '+20%';
            default:
                return '0%';
        }
    }

    public static function getYear() {
        return self::select(DB::raw('DISTINCT year'))
            ->get()
            ->pluck('year', 'year');
    }

    public static function getLatestYear() {
        $grdp_data = self::select(DB::raw('DISTINCT year'))
            ->orderBy('year', 'desc')
            ->first();
        return $grdp_data->year;
    }
}
