<?php
namespace CreditBPO\Models;
use Illuminate\Database\Eloquent\Model;
USE DB;
//======================================================================
//  Class M35: EmailLead
//      EmailLead Table Database Class
//======================================================================
class EmailLead extends Model {
	
	protected $table = 'email_leads';
	protected $guarded = array('id', 'created_at', 'updated_at');
}
