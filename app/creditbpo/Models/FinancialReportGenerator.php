<?php

namespace CreditBPO\Models;

class FinancialReportGenerator {
    protected $table = 'financial_report_generator';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
