<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class MajorSupplierAccount extends Model
{	
    protected $table = 'tblmajorsupplieraccount';

    //-----------------------------------------------------
    //  Function #: Create MajorSupplierAccount record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousDataIfAgree($reportDTO, $previousReportId)
    {
        $majorSupplierAccounts = self::where('entity_id', $previousReportId)->get();

        foreach ($majorSupplierAccounts as $majorSupplierAccount) {
            if($majorSupplierAccount->is_agree == STS_OK) {
                $newMajorSupplierAccount = new MajorSupplierAccount;
    
                $newMajorSupplierAccount->entity_id = $reportDTO->getReportId();
                $newMajorSupplierAccount->is_agree = $majorSupplierAccount->is_agree;
                $newMajorSupplierAccount->created_at = date('Y-m-d H:i:s');
                $newMajorSupplierAccount->updated_at = date('Y-m-d H:i:s');
    
                $newMajorSupplierAccount->save();
            }
        }
    }
}