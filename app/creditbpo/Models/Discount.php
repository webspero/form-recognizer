<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: Dicount
//  Discount Table Database Class
//======================================================================
class Discount extends Model
{	
    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
	protected $table = 'discounts';
	protected $guarded = array('id', 'created_at', 'updated_at');

    //-----------------------------------------------------
    //  Function #.1: getDiscountByValidity
    //  Gets the discount details with the given status
    //-----------------------------------------------------
    function getDiscountByValidity($discountCode, $status)
    {
        $discount = self::where('discount_code', $discountCode)
                        ->where('status', $status)
                        ->where('valid_from', '<=', date('Y-m-d H:i:s'))
                        ->where('valid_to', '>=', date('Y-m-d H:i:s'))
                        ->first();
        return $discount;
    }

    //-----------------------------------------------------
    //  Function #: getDiscountByCode
    //  Gets a discount's details for total amount calculation
    //-----------------------------------------------------
    public function getDiscountByCode($discountCode)
    {
        $discountDetails = self::where('discount_code', $discountCode)->first();
        return $discountDetails;
    }
}