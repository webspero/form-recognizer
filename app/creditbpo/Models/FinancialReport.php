<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;
use DB;

class FinancialReport extends Model {
    protected $table = 'financial_reports';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function balanceSheets() {
        return $this->hasMany('BalanceSheet', 'financial_report_id');
    }

    public function incomeStatements() {
        return $this->hasMany('IncomeStatement', 'financial_report_id');
    }

    public function cashFlow() {
        return $this->hasMany('CashFlow', 'financial_report_id');
    }

    public function getFinancialReportByReportId($reportId)
    {
        $financialReport = self::where('entity_id', $reportId)->orderBy('id', 'desc')->first();

        if($financialReport !== null ) {
            return $financialReport;
        } else {
            return null;
        }
    }

    public function getFinancialReportById($id){
        $financialReport = self::where('entity_id',$id)->where('is_deleted', 0)->first();
        
        if($financialReport == null ) {
        	return $financialStatement = array(
	            'income_statements' => [], 
	            'balance_sheets'    => [], 
	            'cashflow'          => []
	        );
        }

        $income_statements = $financialReport->incomeStatements()
                            ->select('*',
                                DB::raw('(CASE WHEN ProfitLoss = 0 AND ComprehensiveIncome = 0 AND ProfitLossFromContinuingOperations = 0 AND ProfitLossBeforeTax = 0 AND ProfitLossFromOperatingActivities = 0 THEN 1 ELSE 0 END) AS is_income')
                                )
                            ->orderBy('index_count', 'asc')->get();

        $balance_sheets = $financialReport->balanceSheets()
                            ->select(
                                '*', 
                                DB::raw('(CASE WHEN NoncurrentAssets = 0 AND CurrentLiabilities = 0 AND NoncurrentLiabilities = 0 AND Equity = 0 AND Liabilities = 0 AND EquityAndLiabilities = 0 THEN 1 ELSE 0 END) AS is_balance_sheets')
                            )
                            ->orderBy('index_count', 'asc')
                            ->get();

        $cashflow = $financialReport->cashFlow()
                                    ->select(
                                        '*', 
                                        DB::raw('(CASE WHEN AmortisationExpense = 0 AND DepreciationExpense = 0 AND NetCashFlowFromOperations = 0 AND Amortization = 0 AND Depreciation = 0 AND InterestExpense = 0 AND NonCashExpenses = 0 AND PrincipalPayments = 0 AND LeasePayments AND InterestPayments = 0 AND NetCashByOperatingActivities = 0 THEN 1 ELSE 0 END) AS is_cashflow')
                                    )
                                    ->orderBy('index_count', 'asc')->get();

        $incomeStatements = [];
        $balanceSheets = [];
        $cashFlow = [];
                                    
        // Remove empty income statement
        foreach($income_statements as $key_income => $income){
            if($income->is_income == 1){
                unset($income_statements[$key_income]);
            }else{
                $incomeStatements[] = $income;
            }
        }

        // Remove empty balance statement
        foreach($balance_sheets as $key_balance => $balance){
            if($balance->is_balance_sheets == 1){
                unset($balance_sheets[$key_balance]);
            }else{
                $balanceSheets[] = $balance;
            }
        }

        // Remove empty cashflow
        foreach($cashflow as $key_cashflow => $cash){
            if($cash->is_cashflow == 1){
                unset($cashflow[$key_cashflow]);
            }else{
                $cashFlow[] = $cash;
            }
        }

        return $financialStatement = array(
            'income_statements' => $incomeStatements, 
            'balance_sheets'    => $balanceSheets, 
            'cashflow'          => $cashFlow
        );
    }
}
