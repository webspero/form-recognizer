<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class ReadyRatio extends Model {
    protected $table = 'tblreadyrationdata';
}
