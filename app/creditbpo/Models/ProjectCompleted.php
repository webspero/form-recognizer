<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class ProjectCompleted extends Model {
    protected $table = 'tblprojectcompleted';

    //-----------------------------------------------------
    //  Function #: Create MajorSupplier record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $completedProjects = self::where('entity_id', $previousReportId)->get();

        foreach ($completedProjects as $completedProject) {
            $newCompletedProject = new ProjectCompleted;

            $newCompletedProject->entity_id = $reportDTO->getReportId();
            $newCompletedProject->projectcompleted_name = $completedProject->projectcompleted_name;
            $newCompletedProject->projectcompleted_cost = $completedProject->projectcompleted_cost;
            $newCompletedProject->projectcompleted_source_funding = $completedProject->projectcompleted_source_funding;
            $newCompletedProject->projectcompleted_year_began = $completedProject->projectcompleted_year_began;
            $newCompletedProject->projectcompleted_result = $completedProject->projectcompleted_result;
            $newCompletedProject->created_at = date('Y-m-d H:i:s');
            $newCompletedProject->updated_at = date('Y-m-d H:i:s');

            $newCompletedProject->save();
        }
    }
}
