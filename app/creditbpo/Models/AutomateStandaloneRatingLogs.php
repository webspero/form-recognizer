<?php
//======================================================================
//      AutomateStandaloneRating logs Table Database Class
//======================================================================

namespace CreditBPO\Models;
use Illuminate\Database\Eloquent\Model;
USE DB;

class AutomateStandaloneRatingLogs extends Model {
	
	protected $table = 'automate_standalone_rating_logs';			// table name
    protected $primary_key = 'id';
    protected $guarded = array('id', 'created_at', 'updated_at'); 
    

    public function searchByEntityId($entityid)
    {
        $data = self::select("id")
                    ->where("entity_id", $entityid)
                    ->where("is_deleted", 0)
                    ->get();

        return $data;
    }

    public function updateByEntityId($data, $id) 
    {
        self::update($data)->where("id", $id);
    }


    public function softDelete($id) {
        $delete = array("is_deleted"=>1);
        // self::update($delete)->where("id", $id);
        self::where("id",$id)
            ->update($delete);
    }
    
    public function insertRecord($insert) {
        self::insert($insert);
        return true;
    }

    public function checkError($entityid) {
        $data = self::select("error")
                    ->where("entity_id", $entityid)
                    ->where("is_deleted", 0)
                    ->get();
        
        return $data;
    }
}
