<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{	
    protected $table = 'tblbranches';

    //-----------------------------------------------------
    //  Function #: Create Branches record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $branches = self::where('entity_id', $previousReportId)->get();

        foreach ($branches as $branch) {
            $newBranch = new Branch;

            $newBranch->entity_id = $reportDTO->getReportId();
            $newBranch->branch_address = $branch->branch_address;
            $newBranch->branch_owned_leased = $branch->branch_owned_leased;
            $newBranch->branch_contact_person = $branch->branch_contact_person;
            $newBranch->branch_phone = $branch->branch_phone;
            $newBranch->branch_email = $branch->branch_email;
            $newBranch->branch_job_title = $branch->branch_job_title;
            $newBranch->created_at = date('Y-m-d H:i:s');
            $newBranch->updated_at = date('Y-m-d H:i:s');

            $newBranch->save();
        }
    }
}