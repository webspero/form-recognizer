<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class IndustryRow extends Model {
    protected $table = 'tblindustry_row';

    //-----------------------------------------------------
    //  Function #: getSubIndustries
    //  Gets Sub Industries of a Main Industry
    //-----------------------------------------------------
    public function getRowIndustriesBySubId($industryMainId, $industrySubId)
    {
        $result = DB::table('tblindustry_row')
            ->select('tblindustry_row.industry_row_id',
                'tblindustry_row.row_title',
                'tblindustry_row.row_status',
                'tblindustry_sub.industry_sub_id',
                'tblindustry_sub.sub_title',
                'tblindustry_main.industry_main_id',
                'tblindustry_main.main_title')
            ->leftJoin('tblindustry_sub', 'tblindustry_row.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
            ->leftJoin('tblindustry_main', 'tblindustry_sub.industry_main_id', '=', 'tblindustry_main.industry_main_id')
            ->where('tblindustry_main.industry_main_id', $industryMainId)
            ->where('tblindustry_sub.industry_sub_id', $industrySubId)
            ->where('row_status', STS_OK)
            ->get();

        return $result;
    }

    public function getRowIndustriesByMainAndSubID($industryMainId, $industrySubId)
    {
        $result = DB::table('tblindustry_group')
            ->select('tblindustry_group.group_code',
                'tblindustry_group.group_description',
                'tblindustry_sub.sub_code',
                'tblindustry_sub.sub_title',
                'tblindustry_main.main_code',
                'tblindustry_main.main_title')
            ->leftJoin('tblindustry_sub', 'tblindustry_sub.sub_code', '=', 'tblindustry_group.sub_code')
            ->leftJoin('tblindustry_main', 'tblindustry_main.main_code', '=', 'tblindustry_sub.main_code')
            ->where('tblindustry_sub.sub_code', $industrySubId)
            ->where('tblindustry_sub.sub_status', STS_OK)
            ->get();

        return $result;
    }
}
