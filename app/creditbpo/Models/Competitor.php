<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Competitor extends Model
{	
    protected $table = 'tblcompetitor';

    //-----------------------------------------------------
    //  Function #: Create Insurance record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $competitors = self::where('entity_id', $previousReportId)->get();

        foreach ($competitors as $competitor) {
            $newCompetitor = new Competitor;

            $newCompetitor->entity_id = $reportDTO->getReportId();
            $newCompetitor->competitor_name = $competitor->competitor_name;
            $newCompetitor->competitor_address = $competitor->competitor_address;
            $newCompetitor->competitor_contact_details = $competitor->competitor_contact_details;
            $newCompetitor->competitor_email = $competitor->competitor_email;
            $newCompetitor->competitor_phone = $competitor->competitor_phone;
            $newCompetitor->created_at = date('Y-m-d H:i:s');
            $newCompetitor->updated_at = date('Y-m-d H:i:s');

            $newCompetitor->save();
        }
    }

    public function getCompetitorsByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $competitors = self::select(
                                    'competitorid as id',
                                    'entity_id as reportid',
                                    'competitor_name as name',
                                    'competitor_address as address',
                                    'competitor_phone as phone',
                                    'competitor_email as email'    
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('competitorid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $competitors;
    }

    public function getCompetitorsByReportId($reportId)
    {
        $competitors = self::select(
            'competitorid as id',
            'entity_id as reportid',
            'competitor_name as name',
            'competitor_address as address',
            'competitor_phone as phone',
            'competitor_email as email' 
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $competitors;
    }

    public function getCompetitorsById($competitorid)
    {
        $competitors = self::select(
                                    'competitorid as id',
                                    'entity_id as reportid',
                                    'competitor_name as name',
                                    'competitor_address as address',
                                    'competitor_phone as phone',
                                    'competitor_email as email' 
                                    )
                            ->where(['competitorid' => $competitorid, 'is_deleted' => 0])
                            ->first()
                            ->toArray();

        return $competitors;
    }
}