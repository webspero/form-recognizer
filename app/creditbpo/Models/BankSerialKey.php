<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

//======================================================================
//  Class #: BankSerialKey (BankClientKey)
//  BankSerialKey (BankClientKey) Table Database Class
//======================================================================
class BankSerialKey extends Model
{	
    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
	protected $table = 'bank_keys';
    protected $guarded = array('id', 'created_at', 'updated_at');

    //-----------------------------------------------------
    //  Function #: getBankClientKey
    //  Gets the client key details with the given key and bank id
    //-----------------------------------------------------
    function getBankClientKey($key, $organizationId)
    {
        $clientKey = self::where('serial_key', $key)
                         ->where('bank_id', $organizationId)
                         ->where('is_used', 0)->first();
        return $clientKey;
    }

    //-----------------------------------------------------
    //  Function #: getBankKeyByReportId
    //  Gets the client key details based on report id
    //-----------------------------------------------------
    function getBankKeyByReportId($reportId)
    {
        $clientKey = self::where('entity_id', $reportId)->first();
        return $clientKey;
    }

    //-----------------------------------------------------
    //  Function #: updateBankClientKey
    //  Update bank client key details
    //-----------------------------------------------------
    function updateBankClientKey($userReportDto, $clientKey)
    {
        $clientKey->is_used = $userReportDto->getIsUsed();
        $clientKey->login_id = $userReportDto->getUserId();
        $clientKey->entity_id = $userReportDto->getReportId();
        $clientKey->save();
    }

    //-----------------------------------------------------
    //  Function #.1: consumeKey
    //  Update key record such that it is tagged as used
    //-----------------------------------------------------
    function consumeKey($clientKey, $userId, $reportId)
    {
        $keyRecord = self::where('id', $clientKey['id'])->first();

        $keyRecord->is_used = INT_TRUE;
        $keyRecord->login_id = $userId;
        $keyRecord->entity_id = $reportId;
        $keyRecord->save();
    }

    static function getUnusedBankKeys($bank_id){
        $bank_keys = self::where(['bank_id' => $bank_id, 'is_used' => 0])->where('entity_id','=', '')
                            ->orderBy('bank_keys.id', 'desc')
                            ->get();
        return $bank_keys;
    }
}