<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class KeyManager extends Model {
    protected $guarded = [
        'keymanagerid',
        'created_at',
        'updated_at',
    ];
    protected $primaryKey = 'keymanagerid';
    protected $table = 'tblkeymanagers';

    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)->get();
    }

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportId, $oldReport, $newOwner = null)
    {
        $keyManagers = self::where('entity_id', $oldReport)->get();

        foreach ($keyManagers as $keyManager) {
            $newKeyManager = new KeyManager;

            $newKeyManager->ownerid = $newOwner == null ? 0 : $newOwner->ownerid;
            $newKeyManager->entity_id = $reportId;
            $newKeyManager->firstname = $keyManager->firstname;
            $newKeyManager->middlename = $keyManager->middlename;
            $newKeyManager->lastname = $keyManager->lastname;
            $newKeyManager->nationality = $keyManager->nationality;
            $newKeyManager->birthdate = $keyManager->birthdate;
            $newKeyManager->civilstatus = $keyManager->civilstatus;
            $newKeyManager->profession = $keyManager->profession;
            $newKeyManager->email = $keyManager->email;
            $newKeyManager->address1 = $keyManager->address1;
            $newKeyManager->address2 = $keyManager->address2;
            $newKeyManager->cityid = $keyManager->cityid;
            $newKeyManager->province = $keyManager->province;
            $newKeyManager->zipcode = $keyManager->zipcode;
            $newKeyManager->present_address_status = $keyManager->present_address_status;
            $newKeyManager->no_yrs_present_address = $keyManager->no_yrs_present_address;
            $newKeyManager->phone = $keyManager->phone;
            $newKeyManager->tin_num = $keyManager->tin_num;
            $newKeyManager->percent_of_ownership = $keyManager->percent_of_ownership;
            $newKeyManager->number_of_year_engaged = $keyManager->number_of_year_engaged;
            $newKeyManager->position = $keyManager->position;
            $newKeyManager->created_at = date('Y-m-d H:i:s');
            $newKeyManager->updated_at = date('Y-m-d H:i:s');

            $newKeyManager->save();
        }
    }

    public function getKeyManagerByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $affiliateSubsidiaries = self::select(
                                    'keymanagerid as id',
                                    'entity_id as reportid',
                                    'firstname as firstname',
                                    'middlename as middlename',
                                    'lastname as lastname',
                                    'nationality as nationality',
                                    'birthdate as birthdate',
                                    'civilstatus as civilstatus',
                                    'profession as profession',
                                    'email as email',
                                    'address1 as address1',
                                    'tin_num as tinnum',
                                    'percent_of_ownership as percentofownership',    
                                    'number_of_year_engaged as numberofyearengaged',
                                    'position as position'
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('keymanagerid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $affiliateSubsidiaries;
    }

    public function getKeyManagersByReportId($reportId)
    {
        $affiliateSubsidiaries = self::select(
            'keymanagerid as id',
            'entity_id as reportid',
            'firstname as firstname',
            'middlename as middlename',
            'lastname as lastname',
            'nationality as nationality',
            'birthdate as birthdate',
            'civilstatus as civilstatus',
            'profession as profession',
            'email as email',
            'address1 as address1',
            'tin_num as tinnum',
            'percent_of_ownership as percentofownership',    
            'number_of_year_engaged as numberofyearengaged',
            'position as position'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $affiliateSubsidiaries;
    }

    public function getKeyManagersById($keyManagerId)
    {
        $affiliateSubsidiaries = self::select(
                                    'keymanagerid as id',
						            'entity_id as reportid',
						            'firstname as firstname',
						            'middlename as middlename',
						            'lastname as lastname',
						            'nationality as nationality',
						            'birthdate as birthdate',
						            'civilstatus as civilstatus',
						            'profession as profession',
						            'email as email',
						            'address1 as address1',
						            'tin_num as tinnum',
						            'percent_of_ownership as percentofownership',    
						            'number_of_year_engaged as numberofyearengaged',
						            'position as position'
                                    )
                            ->where(['keymanagerid' => $keyManagerId, 'is_deleted' => 0])
                            ->first()
                            ->toArray();

        return $affiliateSubsidiaries;
    }
}
