<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class MajorCustomerAccount extends Model 
{
    protected $table = 'tblmajorcustomeraccount';
    protected $primaryKey = 'majorcustomeraccountid';

    public function getByEntityId($entityId) 
    {
        return $this->where('entity_id', '=', $entityId)->get();
    }

    public function getFirstCustomerAccountByReportId($reportId) 
    {
        return $this->where('entity_id', $reportId)->first();
    }

    public function updateMajorCustomerAccount($majorCustomerAccount, $isAgree)
    {
        $majorCustomerAccount->is_agree = $isAgree;
        return $majorCustomerAccount->save();
    }

    public function addMajorCustomerAccount($reportId, $isAgree)
    {
        $majorCustomerAccount = new MajorCustomerAccount();
        $majorCustomerAccount->entity_id = $reportId;
        $majorCustomerAccount->is_agree = $isAgree;
        return $majorCustomerAccount->save();
    }

    //-----------------------------------------------------
    //  Function #: Create MajorCustomerAccount record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousDataIfAgree($reportDTO, $previousReportId)
    {
        $majorCustomerAccounts = self::where('entity_id', $previousReportId)->get();

        foreach ($majorCustomerAccounts as $majorCustomerAccount) {
            if($majorCustomerAccount->is_agree == STS_OK) {
                $newMajorCustomerAccount = new MajorCustomerAccount;
    
                $newMajorCustomerAccount->entity_id = $reportDTO->getReportId();
                $newMajorCustomerAccount->is_agree = $majorCustomerAccount->is_agree;
                $newMajorCustomerAccount->created_at = date('Y-m-d H:i:s');
                $newMajorCustomerAccount->updated_at = date('Y-m-d H:i:s');
    
                $newMajorCustomerAccount->save();
            }
        }
    }
}
