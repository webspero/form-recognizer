<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class CashFlow extends Model {
    protected $table = 'fin_cashflow';
	protected $guarded = array('id', 'created_at', 'updated_at');
	
	public function financialReport()
    {
        return $this->belongsTo('FinancialReport', 'financial_report_id');
    }
    
    public function getByFinancialReportId($financialReportId)
    {
        return self::where('financial_report_id', $financialReportId)->orderBy('index_count', 'asc')->get();
    }
}