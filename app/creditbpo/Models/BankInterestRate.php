<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class BankInterestRate extends Model {
    protected $table = 'bank_interest_rates';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getCurrentInterestRates() {
        $local = $this->getCurrentInterestRateByType('local');
        $foreign = $this->getCurrentInterestRateByType('foreign');

        return [
            'foreign' => [
                'low' => (float) $foreign->low,
                'mid' => (float) $foreign->mid,
                'high' => (float) $foreign->high,
            ],
            'local' => [
                'low' => (float) $local->low,
                'mid' => (float) $local->mid,
                'high' => (float) $local->high,
            ],
        ];
    }

    public function getCurrentInterestRateByType($type) {
        return self::where('type', $type)
            ->orderBy('executed', 'desc')
            ->orderBy('id', 'desc')
            ->first();
    }
}