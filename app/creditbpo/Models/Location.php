<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{	
    protected $table = 'tbllocations';

    //-----------------------------------------------------
    //  Function #: Create Locations record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $locations = self::where('entity_id', $previousReportId)->get();

        foreach ($locations as $location) {
            $newLocation = new Location;

            $newLocation->entity_id = $reportDTO->getReportId();
            $newLocation->location_size = $location->location_size;
            $newLocation->location_type = $location->location_type;
            $newLocation->location_map = $location->location_map;
            $newLocation->location_used = $location->location_used;
            $newLocation->created_at = date('Y-m-d H:i:s');
            $newLocation->updated_at = date('Y-m-d H:i:s');

            $newLocation->save();
        }
    }

    public function getMainLocationByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $mainLocation = self::select(
                            'locationid as id',
                            'entity_id as reportid',
                            'location_size as sizeofpremises',
                            'location_type as premisesownedleased',
                            'location_map as location',
                            'location_used as premisesusedas',
                            'floor_count as nooffloors',
                            'address as locationaddress',
                            'material as material',
                            'color as color',
                            'map as map',
                            'location_photo as actuallocationphoto',
                            'other as note',
                            )
                      ->where(['entity_id'=>$reportId,'is_deleted' => 0])
                      ->orderBy('locationid', 'ASC')
                      ->skip($limit * ($page))
                      ->take($limit)
                      ->get();
        return $mainLocation;
    }

    public function getMainLocationByReportId($reportId)
    {
        $mainLocation = self::select(
            'locationid as id',
            'entity_id as reportid',
            'location_size as sizeofpremises',
            'location_type as premisesownedleased',
            'location_map as location',
            'location_used as premisesusedas',
            'floor_count as nooffloors',
            'address as locationaddress',
            'material as material',
            'color as color',
            'map as map',
            'location_photo as actuallocationphoto',
            'other as note',
        )->where(['entity_id'=>$reportId,'is_deleted' => 0])->get();
        
        return $mainLocation;
    }

    public function getMainLocationById($locationid)
    {
        $mainLocation = self::select(
                                    'locationid as id',
						            'entity_id as reportid',
		                            'location_size as sizeofpremises',
		                            'location_type as premisesownedleased',
		                            'location_map as location',
		                            'location_used as premisesusedas',
		                            'floor_count as nooffloors',
		                            'address as locationaddress',
		                            'material as material',
		                            'color as color',
		                            'map as map',
		                            'location_photo as actuallocationphoto',
		                            'other as note',
                                    )
                            ->where(['locationid'=>$locationid,'is_deleted'=>0])
                            ->first()
                            ->toArray();

        return $mainLocation;
    }
}