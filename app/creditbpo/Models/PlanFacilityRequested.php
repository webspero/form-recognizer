<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class PlanFacilityRequested extends Model {
	protected $table = 'tblplanfacilityrequested';
	protected $fillable = [
		'plan_credit_availment',
		'cash_deposit_details',
		'cash_deposit_amount',
		'securities_details',
		'securities_estimated_value',
		'property_details',
		'property_estimated_value',
		'chattel_details',
		'chattel_estimated_value',
		'others_details',
		'others_estimated_value',
		'purpose_credit_facility',
		'purpose_credit_facility_others',
		'credit_terms',
		'other_remarks'
	];

    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $planFacilityRequested = self::where('entity_id', $previousReportId)->first();
		if($planFacilityRequested){
            $newPlanFacilityRequested = new PlanfacilityRequested();
			$newPlanFacilityRequested->entity_id = $reportDTO->getReportId();
			$newPlanFacilityRequested->plan_credit_availment = $planFacilityRequested->plan_credit_availment;
			$newPlanFacilityRequested->cash_deposit_details = $planFacilityRequested->cash_deposit_details;
			$newPlanFacilityRequested->cash_deposit_amount = $planFacilityRequested->cash_deposit_amount;
			$newPlanFacilityRequested->securities_details = $planFacilityRequested->securities_details;
			$newPlanFacilityRequested->securities_estimated_value = $planFacilityRequested->securities_estimated_value;
			$newPlanFacilityRequested->property_details = $planFacilityRequested->property_details;
			$newPlanFacilityRequested->property_estimated_value = $planFacilityRequested->property_estimated_value;
			$newPlanFacilityRequested->chattel_details = $planFacilityRequested->chattel_details;
			$newPlanFacilityRequested->chattel_estimated_value = $planFacilityRequested->chattel_estimated_value;
			$newPlanFacilityRequested->others_details = $planFacilityRequested->others_details;
			$newPlanFacilityRequested->others_estimated_value = $planFacilityRequested->others_estimated_value;
			$newPlanFacilityRequested->purpose_credit_facility = $planFacilityRequested->purpose_credit_facility;
			$newPlanFacilityRequested->purpose_credit_facility_others = $planFacilityRequested->purpose_credit_facility_others;
			$newPlanFacilityRequested->save();
		}
	}
	
	public function getCreditLinesByReportId($reportId)
	{
		$creditLines = self::where('entity_id', $reportId)
            ->get();
        return $creditLines;
	}

	public function getCreditLinesByReportIdByPage($reportId, $page, $limit)
	{
		$page -=1;

		$creditLines = self::where('entity_id', $reportId)
			->skip($limit * ($page))
			->take($limit)
            ->get();
        return $creditLines;
	}

	public function checkReportCreditLine($reportId, $creditLineId)
	{
		$creditLine = self::where('entity_id', $reportId)->where('id', $creditLineId)->first();
        return $creditLine;
	}

	public function getCreditLineDetails($creditLineId)
	{
		$creditLine = self::where('id', $creditLineId)->where('is_deleted', 0)->first();
        return $creditLine;
	}

	public function addCreditLine($newCreditLine)
	{
		$creditLine = new PlanFacilityRequested();
		$creditLine->entity_id = $newCreditLine['entity_id'];
		$creditLine->plan_credit_availment = $newCreditLine['plan_credit_availment'];
		$creditLine->cash_deposit_details = $newCreditLine['cash_deposit_details'];
		$creditLine->cash_deposit_amount = $newCreditLine['cash_deposit_amount'];
		$creditLine->securities_details = $newCreditLine['securities_details'];
		$creditLine->securities_estimated_value = $newCreditLine['securities_estimated_value'];
		$creditLine->property_details = $newCreditLine['property_details'];
		$creditLine->property_estimated_value = $newCreditLine['property_estimated_value'];
		$creditLine->chattel_details = $newCreditLine['chattel_details'];
		$creditLine->chattel_estimated_value = $newCreditLine['chattel_estimated_value'];
		$creditLine->others_details = $newCreditLine['others_details'];
		$creditLine->others_estimated_value = $newCreditLine['others_estimated_value'];
		$creditLine->purpose_credit_facility = $newCreditLine['purpose_credit_facility'];
		$creditLine->purpose_credit_facility_others = $newCreditLine['purpose_credit_facility_others'];
		$creditLine->credit_terms = $newCreditLine['credit_terms'];
		$creditLine->other_remarks = $newCreditLine['other_remarks'];
		$creditLine->save();

		return $creditLine->id;
	}

	//-----------------------------------------------------
    //  Function #: updateCreditLine
    //  Update a Report's Credit Line
    //-----------------------------------------------------
    public function updateCreditLine($creditLineId, $updateData)
    {
		$creditLine = self::find($creditLineId);

        if (!empty($creditLine)){
            $creditLine->update(array_filter($updateData));
		}
		
        return $creditLine;
	}
	
	public function deleteCreditLine($creditLineId)
    {
        $creditLine = self::find($creditLineId);
        if (!empty($creditLine)){
			
			$creditLine->is_deleted = 1;
			$creditLine->save();

			return $creditLine;
        } else{
            return null;
        }
    }
}
