<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Zipcode extends Model {
    protected $table = 'zipcodes';

    public function getAllRegions() {
        return $this->distinct()->get(['region']);
    }

    //-----------------------------------------------------
    //  Function #: getProvinces
    //  Gets all Provinces
    //-----------------------------------------------------
    public function getProvinces() {
        return $this->distinct()->orderBy('major_area')->get(['major_area']);
    }

    //-----------------------------------------------------
    //  Function #: getProvinceDetailsByProvinceName
    //  Gets province details by province name.
    //-----------------------------------------------------
    public function getProvinceDetailsByProvinceName($provinceName){
        $province = self::where('major_area', $provinceName)->first();

        return $province;
    }

    //-----------------------------------------------------
    //  Function #: getProvincesByPage
    //  Gets all Provinces with pagination
    //-----------------------------------------------------
    public function getProvincesByPage($page, $limit)
    {
        $page -= 1;

        $provinces = self::distinct()->select(['major_area'])->skip($limit * ($page))
            ->take($limit)
            ->get();

        return $provinces;
    }

    //-----------------------------------------------------
    //  Function #: getCitiesByProvince
    //  Gets all Cities within a province
    //-----------------------------------------------------
    public function getCitiesByProvince($province)
    {
        $cities = self::where('major_area', $province)->get();

        return $cities;
    }

    //-----------------------------------------------------
    //  Function #: getCityById
    //  Gets a specific City record
    //-----------------------------------------------------
    public function getCityById($cityId)
    {
        $city= self::where('id', $cityId)->first();

        return $city;
    }

    //-----------------------------------------------------
    //  Function #: getProvince
    //  Gets a specific Province record
    //-----------------------------------------------------
    public function getProvince($province){
        $province = DB::table('refprovince')->where('provCode', $province)->first();
        return $province;
    }

    //-----------------------------------------------------
    //  Function #: getCity
    //  Gets a specific City record
    //-----------------------------------------------------
    public function getCity($city, $province){
        $city = DB::table('refcitymun')->where('provCode', $province)->where('citymunCode', $city)->first();
        return $city;
    }
}
