<?php

namespace CreditBPO\Models;
use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class XX: Questionnaire Link
//      Questionnaire Link Table Database Model
//======================================================================
class QuestionnaireLink extends Model
{	
	protected $table = 'questionnaire_links';
	protected $guarded = array('id', 'created_at', 'updated_at');

	//-----------------------------------------------------
    //  Function x.xx: getQuestionnaireReport
	//  Description: Get record of questionnaire link
    //-----------------------------------------------------	
	public function getQuestionnaireReport($randomcode){
		$questionnaire = self::where('url', $randomcode)->first();
		return $questionnaire;
	}
}
