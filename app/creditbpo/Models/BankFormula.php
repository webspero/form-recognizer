<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class BankFormula extends Model {
    protected $table = 'bank_formula';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
