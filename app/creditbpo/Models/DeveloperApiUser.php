<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use Hash;

class DeveloperApiUser extends Model
{
	protected $table = 'developer_api_users';
	protected $primaryKey = 'client_id';
    
    public function getApiDevelopers()
    {
        $developers = self::orderBy('client_id', 'desc')->get();
        return $developers;
    }
    
    public function addNewApiUser($data)
    {
        $clientSecret = uniqid(rand(), true);
        
        $apiDeveloper = new DeveloperApiUser;
        $apiDeveloper->firstname = $data['fname'];
        $apiDeveloper->lastname = $data['lname'];
        $apiDeveloper->email = $data['email'];
        $apiDeveloper->password = Hash::make($data['pword']);
        $apiDeveloper->status = ACTIVE;
        $apiDeveloper->secret_key = Hash::make($clientSecret);
        
        return $apiDeveloper->save();
    }

    public function updateDeveloperStatus($status, $client_id)
    {
        $apiDeveloper = self::find($client_id);
        
        if ($apiDeveloper) {
            /* Activate developer when Inactive */
            if (ACTIVE == $status) {
                $apiDeveloper->status = ACTIVE;
            }
            /* Deactivate developer when Active */
            else {
                $apiDeveloper->status = DELETED;
            }
            
            $apiDeveloper->save();
        }
    }

    public function checkApiKey($clientId, $clientSecret)
    {
        $user = self::where('client_id', $clientId)
                    ->where('secret_key', $clientSecret)
                    ->where('status', ACTIVE)
                    ->first();
       
        if (!empty($user->client_id)){
            return true;
        } else{
            return false;
        }
    }
}