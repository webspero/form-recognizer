<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class fileStorage extends Model
{	
    protected $table = 'tbldropboxuser';

    public function getfileStorageByPage($loginId, $page, $limit)
    {
        $page -= 1;
        $file = self::select(
                                    'id as id',
                                    'login_id as loginid',
                                    'file_name as uploadedfile'    
                                    )
                              ->where(['login_id'=>$loginId,'is_deleted' => 0])
                              ->orderBy('id', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $file;
    }

    public function getFileStoragesByLoginId($loginId)
    {
        $fileStorages = self::select(
            'id as id',
            'login_id as loginid',
            'file_name as uploadedfile'
        )->where(['login_id'=>$loginId,'is_deleted' => 0])->get();
        
        return $fileStorages;
    }

    public function getFileStorageById($id)
    {
        $fileStorage = self::select(
                        'id as id',
			            'login_id as loginid',
			            'file_name as uploadedfile'
                        )
                ->where(['id'=>$id,'is_deleted'=>0])
                ->first()
                ->toArray();

        return $fileStorage;
    }
}