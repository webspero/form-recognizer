<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class Organization extends Model {
    protected $table = 'tblbank';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    public function getStandaloneOrganizationByPage($page, $limit)
    {
        $page -= 1;
        $organizations = self::select('id', 'bank_name as bank')
                             ->where('is_standalone', INT_TRUE)
                             ->orderBy('bank_name', 'ASC')
                             ->skip($limit * ($page))
                             ->take($limit)
                             ->get();
        return $organizations;
    }

    public function getStandaloneOrganizations()
    {
        $organizations = self::select('id', 'bank_name as bank')
                             ->where('is_standalone', INT_TRUE)
                             ->orderBy('bank_name', 'ASC')
                             ->get();
        return $organizations;
    }

    public function getAssociatedOrganizationById($associatedOrganizationId){
        $associatedOrganization = self::select('id', 'bank_name as bank')
                             ->where('is_standalone', INT_TRUE)
                             ->where('id', $associatedOrganizationId)
                             ->first();
        return $associatedOrganization;
    }

    public function getOrganizationById($organizationId)
    {
        $organization = self::where('id', $organizationId)
                            ->where('is_standalone', INT_TRUE)
                            ->first();
        return $organization;
    }
}
