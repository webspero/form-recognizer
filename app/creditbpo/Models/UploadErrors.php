<?php
namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

//======================================================================
//  Class: Upload Errors Record
//      DragonPay Transactions Database Model
//======================================================================
class UploadErrors extends Model
{	
	protected $table = 'upload_errors';


	public static function insertNewUploadError($uploadError){

		return UploadErrors::insert($uploadError);     
	}

	public static function getUploadErrorCount(){
		$date = date('Y-m-d');
		return UploadErrors::whereDate('date_uploading',$date)->where('loginid',Auth::user()->loginid)->count();
	}
}
