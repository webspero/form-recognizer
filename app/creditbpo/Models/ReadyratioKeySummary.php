<?php
namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


//======================================================================
//  Class M106: ReadyratioKeySummary
//      readyratio_key_summary Table Database Class
//======================================================================

class ReadyratioKeySummary extends Model
{
    protected $table        = 'readyratio_key_summary';
    protected $primaryKey   = 'id';
}
