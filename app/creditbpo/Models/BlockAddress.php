<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class BlockAddress extends Model {
    protected $table = 'block_email_address';
    protected $guarded = ['id', 'created_at', 'updated_at'];

}