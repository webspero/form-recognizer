<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Businesstype extends Model
{	
    protected $table = 'tblbusinesstype';

    //-----------------------------------------------------
    //  Function #: Create Businesstype record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $businessTypes = self::where('entity_id', $previousReportId)->get();

        foreach ($businessTypes as $businessType) {
            $newBusinessType = new Businesstype;

            $newBusinessType->entity_id = $reportDTO->getReportId();
            $newBusinessType->is_import = $businessType->is_import;
            $newBusinessType->is_export = $businessType->is_export;
            $newBusinessType->created_at = date('Y-m-d H:i:s');
            $newBusinessType->updated_at = date('Y-m-d H:i:s');

            $newBusinessType->save();
        }
    }
}