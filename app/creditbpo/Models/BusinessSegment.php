<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class BusinessSegment extends Model {
    protected $table = 'tblbusinesssegment';

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $businessDrivers = self::where('entity_id', $previousReportId)->get();

        foreach ($businessDrivers as $businessDriver) {
            $newBusinessDriver = new BusinessSegment;

            $newBusinessDriver->entity_id = $reportDTO->getReportId();
            $newBusinessDriver->businesssegment_name = $businessDriver->businesssegment_name;
            $newBusinessDriver->created_at = date('Y-m-d H:i:s');
            $newBusinessDriver->updated_at = date('Y-m-d H:i:s');

            $newBusinessDriver->save();
        }
    }

    public function getCustomerSegmentByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $customerSegments = self::select(
                                    'businesssegmentid as id',
                                    'entity_id as reportid',
                                    'businesssegment_name as name'    
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('businesssegmentid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $customerSegments;
    }

    public function getCustomerSegmentByReportId($reportId)
    {
        $customerSegments = self::select(
            'businesssegmentid as id',
            'entity_id as reportid',
            'businesssegment_name as name'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $customerSegments;
    }

    public function getCustomerSegmentById($businessSegmentId)
    {
        $customerSegments = self::select(
                                    'businesssegmentid as id',
                                    'entity_id as reportid',
                                    'businesssegment_name as name'
                                    )
                            ->where(['businesssegmentid' => $businessSegmentId, 'is_deleted' => 0])
                            ->first()
                            ->toArray();

        return $customerSegments;
    }
}
