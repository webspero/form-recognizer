<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class ManagedBusiness extends Model {
    protected $table = 'tblmanagebus';
    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($oldOwner, $newOwner, $reportId)
    {
        $managedBusinesses = self::where('ownerid', $oldOwner)->get();

        foreach ($managedBusinesses as $managedBusiness) {
            $newManagedBusiness = new ManagedBusiness;

            $newManagedBusiness->ownerid = $newOwner;
            $newManagedBusiness->entity_id = $reportId;
            $newManagedBusiness->bus_name = $managedBusiness->bus_name;
            $newManagedBusiness->bus_type = $managedBusiness->bus_type;
            $newManagedBusiness->bus_location = $managedBusiness->bus_location;
            $newManagedBusiness->created_at = date('Y-m-d H:i:s');
            $newManagedBusiness->updated_at = date('Y-m-d H:i:s');

            $newManagedBusiness->save();
        }
    }
}
