<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

//======================================================================
//  Class #: Report (Entity)
//  Report (Entity) Database Model
//======================================================================
class TranscribeFS extends Model {
    protected $primaryKey = 'id';
    protected $table = 'tbl_trabscribe_fs';
}