<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class CapacityExpansion extends Model {
    protected $table = 'tblcapacityexpansion';

    public function getByReportId($reportId) {
        return $this->where('entity_id', '=', $reportId)->get();
    }

    public function createWithPreviousData($reportDTO, $capacityExpansion)
    {
        $newCapacityExpansion = new CapacityExpansion();
        $newCapacityExpansion->entity_id = $reportDTO->getReportId();
		$newCapacityExpansion->current_year = $capacityExpansion->current_year;
        $newCapacityExpansion->new_year = $capacityExpansion->new_year;
        $newCapacityExpansion->within_five_years = $capacityExpansion->within_five_years;
		$newCapacityExpansion->created_at = date('Y-m-d H:i:s');
        $newCapacityExpansion->updated_at = date('Y-m-d H:i:s');
        $newCapacityExpansion->save();
    }
}