<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;
use DB;

class Currency extends Model {
    protected $table = 'currency';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    public function getCurrenciesByPage($page, $limit)
    {
        $page -= 1;
        $currencies = self::select('iso_code as isocode', 'name as description')
                          ->orderBy('iso_code', 'ASC')
                          ->skip($limit * ($page))
                          ->take($limit)
                          ->get();
        return $currencies;
    }

    public function getCurrencies()
    {
        $currencies = self::select('iso_code as isocode', 'name as description')
                            ->orderBy('iso_code', 'ASC')
                            ->get();
        return $currencies;
    }

    public function getCurrencyByIso($isoCode)
    {
        $currency = self::where('iso_code', $isoCode)
                        ->first();
        return $currency;
    }
}
