<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class PlanFacility extends Model
{
    protected $table = 'tblplanfacility';
    protected $primaryKey = 'planfacilityid';
    
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $planfacility = self::where('entity_id', $previousReportId)->first();
		if($planfacility){
            $newPlanFacility = new Planfacility();
			$newPlanFacility->entity_id = $reportDTO->getReportId();
			$newPlanFacility->lending_institution = $planfacility->lending_institution;
			$newPlanFacility->plan_credit_availment = $planfacility->plan_credit_availment;
			$newPlanFacility->credit_terms = $planfacility->credit_terms;
			$newPlanFacility->experience = $planfacility->experience;
			$newPlanFacility->other_remarks = $planfacility->other_remarks;
			$newPlanFacility->outstanding_balance = $planfacility->outstanding_balance;

			$newPlanFacility->cash_deposit_details = $planfacility->cash_deposit_details;
			$newPlanFacility->cash_deposit_amount = $planfacility->cash_deposit_amount;
			$newPlanFacility->securities_details = $planfacility->securities_details;
			$newPlanFacility->securities_estimated_value = $planfacility->securities_estimated_value;
			$newPlanFacility->property_details = $planfacility->property_details;
			$newPlanFacility->property_estimated_value = $planfacility->property_estimated_value;
			$newPlanFacility->chattel_details = $planfacility->chattel_details;
			$newPlanFacility->chattel_estimated_value = $planfacility->chattel_estimated_value;
			$newPlanFacility->others_details = $planfacility->others_details;
			$newPlanFacility->others_estimated_value = $planfacility->others_estimated_value;
			$newPlanFacility->purpose_credit_facility = $planfacility->purpose_credit_facility;
			$newPlanFacility->purpose_credit_facility_others = $planfacility->purpose_credit_facility_others;
			$newPlanFacility->save();
		}
    }
}