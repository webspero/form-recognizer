<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: PayPal Transactions
//      PayPal Transactions Table Database Model
//======================================================================
class PaypalTransaction extends Model
{
	protected 	$table 		= 'paypal_transactions';   /* The database table used by the model.    */
	protected 	$primaryKey = 'id';         /* The table's primary key                  */
    public		$timestamps = false;

    //-----------------------------------------------------
    //  Function M#.1: add record specific for paypal transactions
    //-----------------------------------------------------
    public function addRecord($data, $clientReturn)
    {
        /*  Saves data into the database    */
        $db_data = new PaypalTransaction;
        $db_data->cancel_url = $data['cancelUrl'];
        $db_data->return_url = $data['returnUrl'];
        $db_data->name = $data['name'];
        $db_data->description = $data['description'];
        $db_data->amount = $data['amount'];
        $db_data->currency = $data['currency'];
        $db_data->reference_id = $data['reference_id'];
        $db_data->client_return = $clientReturn;
        $db_data->entity_id = $data['entity_id'];
        $db_data->save();
    }

    //-----------------------------------------------------
    //  Function M#.1: get record based on Reference ID passed to and from PayPal
    //-----------------------------------------------------
    public function getParametersByReferenceId($referenceId)
    {
        $parameters = self::where('reference_id', $referenceId)->first();
        return $parameters;
    }
}