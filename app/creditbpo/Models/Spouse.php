<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class Spouse extends Model {
    protected $table = 'tblspouse';

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function getFromPreviousReport($oldOwnerId)
    {
        return self::where('ownerid', $oldOwnerId)->first();
    }

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($ownerId, $oldSpouseRecord, $reportId)
    {
        $newSpouse = new Spouse;

        $newSpouse->ownerid = $ownerId;
        $newSpouse->entity_id = $reportId;
        $newSpouse->firstname = $oldSpouseRecord->firstname;
        $newSpouse->middlename = $oldSpouseRecord->middlename;
        $newSpouse->lastname = $oldSpouseRecord->lastname;
        $newSpouse->nationality = $oldSpouseRecord->nationality;
        $newSpouse->birthdate = $oldSpouseRecord->birthdate;
        $newSpouse->profession = $oldSpouseRecord->profession;
        $newSpouse->email = $oldSpouseRecord->email;
        $newSpouse->phone = $oldSpouseRecord->phone;
        $newSpouse->tin_num = $oldSpouseRecord->tin_num;
        $newSpouse->created_at = date('Y-m-d H:i:s');
        $newSpouse->updated_at = date('Y-m-d H:i:s');

        $newSpouse->save();

        return $newSpouse;
    }
}