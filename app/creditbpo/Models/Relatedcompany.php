<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedCompany extends Model
{	
    protected $table = 'tblrelatedcompanies';
    
    //-----------------------------------------------------
    //  Function #: Create Related Companies record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $relatedCompanies = self::where('entity_id', $previousReportId)->get();

        foreach ($relatedCompanies as $relatedCompany) {
            $newRelatedCompany = new RelatedCompany;

            $newRelatedCompany->entity_id = $reportDTO->getReportId();
            $newRelatedCompany->related_company_name = $relatedCompany->related_company_name;
            $newRelatedCompany->related_company_address = $relatedCompany->related_company_address;
            $newRelatedCompany->related_company_phone = $relatedCompany->related_company_phone;
            $newRelatedCompany->related_company_email = $relatedCompany->related_company_email;
            $newRelatedCompany->created_at = date('Y-m-d H:i:s');
            $newRelatedCompany->updated_at = date('Y-m-d H:i:s');

            $newRelatedCompany->save();
        }
    }

    public function getAffiliateSubsidiariesByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $affiliateSubsidiaries = self::select(
                                    'relatedcompaniesid as id',
                                    'entity_id as reportid',
                                    'related_company_name as companyname',
                                    'related_company_address1 as companyaddress',
                                    'related_company_phone as companyphone',
                                    'related_company_email as companyemail'    
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('relatedcompaniesid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $affiliateSubsidiaries;
    }

    public function getAffiliateSubsidiariesByReportId($reportId)
    {
        $affiliateSubsidiaries = self::select(
            'relatedcompaniesid as id',
            'entity_id as reportid',
            'related_company_name as companyname',
            'related_company_address1 as companyaddress',
            'related_company_phone as companyphone',
            'related_company_email as companyemail'
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $affiliateSubsidiaries;
    }

    public function getAffiliateSubsidiariesById($relatedcompaniesid)
    {
        $affiliateSubsidiaries = self::select(
                                    'relatedcompaniesid as id',
						            'entity_id as reportid',
						            'related_company_name as companyname',
						            'related_company_address1 as companyaddress',
						            'related_company_phone as companyphone',
						            'related_company_email as companyemail'
                                    )
                            ->where(['relatedcompaniesid' => $relatedcompaniesid, 'is_deleted' => 0])
                            ->first();

        return !empty($affiliateSubsidiaries) ? $affiliateSubsidiaries->toArray() : [];
        
    }
}
