<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use CreditBPO\Models\UserLoginAttempt;
use CreditBPO\Libraries\MaliciousAttemptLib;

//======================================================================
//	Class #: User Login Attempt
//	User Login Attempt Table Database Model
//======================================================================
class UserLoginAttempt extends Model 
{
    
    protected $table = 'user_login_attempts';
	protected $primaryKey = 'login_attempt_id';
    public	$timestamps = false;
    protected $fillable = 
    [
        'attempts'
    ];
    
    public function getUserAttempt($userId)
    {
        $data = self::where('login_id', $userId)
                    ->where('status', ACTIVE)
                    ->first();
        return $data;
    }

    public function updateUserAttempt($userAttemptId, $updateDetails)
    {
        $userAttempt = self::find($userAttemptId);
        return $userAttempt->update(array_filter($updateDetails));
    }

    public function updateUserAttemptStatus($userAttemptDto)
    {
        $userAttempt = self::find($userAttemptDto->getId());
        $userAttempt->status = $userAttemptDto->getStatus();
        $userAttempt->last_login = $userAttemptDto->getLastLogin();
        $userAttempt->save();
    }

    public function saveUserAttempt($userAttemptDetails)
    {
        $userAttempt = new UserLoginAttempt;
        $userAttempt->login_id = $userAttemptDetails->getUserId();
        $userAttempt->attempts = $userAttemptDetails->getAttempts();
        $userAttempt->country_code = $userAttemptDetails->getCountryCode();
        $userAttempt->status = $userAttemptDetails->getStatus();
        $userAttempt->last_login = $userAttemptDetails->getLastLogin();
        return $userAttempt->save();
    }
}
