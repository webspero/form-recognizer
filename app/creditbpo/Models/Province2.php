<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

//======================================================================
//  Class #: Province
//  
//======================================================================
class Province2 extends Model 
{
	protected $table = 'refprovince';
    protected $primaryKey   = 'provCode';

    public function getLocationByPage($page, $limit){
        $page -= 1;
        $location = DB::table('refcitymun')
                  ->select(DB::raw('refcitymun.citymunCode as cityid, refcitymun.citymunDesc as city_description, refcitymun.provCode as provinceid, refprovince.provDesc as province_description, refcitymun.zipcode'))
                  ->leftJoin('refprovince', 'refprovince.provCode', '=', 'refcitymun.provCode')
                  ->skip($limit * ($page))
                  ->take($limit)
                  ->get();
        return $location;
    }

    public function getLocation(){
        $location = DB::table('refcitymun')
                  ->select(DB::raw('refcitymun.citymunCode as cityid, refcitymun.citymunDesc as city_description, refcitymun.provCode as provinceid, refprovince.provDesc as province_description, refcitymun.zipcode'))
                  ->leftJoin('refprovince', 'refprovince.provCode', '=', 'refcitymun.provCode')
                  ->get();
        return $location;
    }
}