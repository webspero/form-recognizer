<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class IndustrySub extends Model {
    protected $table = 'tblindustry_sub';

    //-----------------------------------------------------
    //  Function #: getSubIndustries
    //  Gets Sub Industries of a Main Industry
    //-----------------------------------------------------
    public function getSubIndustriesByMainId($industryMainId)
    {
        return self::where('main_code', $industryMainId)->where('sub_status', STS_OK)->get();
    }
}
