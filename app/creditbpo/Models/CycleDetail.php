<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class CycleDetail extends Model
{	
    protected $table = 'tblcycledetails';

    //-----------------------------------------------------
    //  Function #: Create Insurance record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $cycleDetails = self::where('entity_id', $previousReportId)->get();

        foreach ($cycleDetails as $cycleDetail) {
            $newCycleDetail = new CycleDetail;

            $newCycleDetail->entity_id = $reportDTO->getReportId();
            $newCycleDetail->index = $cycleDetail->index;
            $newCycleDetail->credit_sales = $cycleDetail->credit_sales;
            $newCycleDetail->accounts_receivable = $cycleDetail->accounts_receivable;
            $newCycleDetail->inventory = $cycleDetail->inventory;
            $newCycleDetail->cost_of_sales = $cycleDetail->cost_of_sales;
            $newCycleDetail->accounts_payable = $cycleDetail->accounts_payable;
            $newCycleDetail->start_date = $cycleDetail->start_date;
            $newCycleDetail->created_at = date('Y-m-d H:i:s');
            $newCycleDetail->updated_at = date('Y-m-d H:i:s');

            $newCycleDetail->save();
        }
    }
}