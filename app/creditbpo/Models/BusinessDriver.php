<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class BusinessDriver extends Model {
    protected $table = 'tblbusinessdriver';

    //-----------------------------------------------------
    //  Function #: Create MajorSupplier record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $businessDrivers = self::where('entity_id', $previousReportId)->get();

        foreach ($businessDrivers as $businessDriver) {
            $newBusinessDriver = new BusinessDriver;

            $newBusinessDriver->entity_id = $reportDTO->getReportId();
            $newBusinessDriver->businessdriver_name = $businessDriver->businessdriver_name;
            $newBusinessDriver->businessdriver_total_sales = $businessDriver->businessdriver_total_sales;
            $newBusinessDriver->created_at = date('Y-m-d H:i:s');
            $newBusinessDriver->updated_at = date('Y-m-d H:i:s');

            $newBusinessDriver->save();
        }
    }
}
