<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: DragonPay Transactions Record
//      DragonPay Transactions Database Model
//======================================================================
class Transaction extends Model
{	
	protected $table = 'tbltransactions';

	//-----------------------------------------------------
    //  Function #: Add Transaction Record
    //  Create a Transaction record for the report to be paid
    //-----------------------------------------------------
	public function addTransaction($transactionData)
	{
		/*  Variable Declaration        */
		$sts = STS_OK;
		$transaction = new Transaction;
		
		/* Save Transaction Record */
		$transaction->loginid       = $transactionData['loginid'];
		$transaction->entity_id     = $transactionData['entity_id'];
		$transaction->amount        = $transactionData['amount'];
		$transaction->ccy           = $transactionData['ccy'];
		$transaction->description   = $transactionData['description'];
		$transaction->txn 			= $transactionData['txn'];
		$transaction->type 			= $transactionData['type'];
		$transaction->api_return_url = $transactionData['api_return_url'];

		/*  Save successful */
        if ($transaction->save()) {
            $sts = $transaction->id;
        } else {
        /*  Save failed     */
            $sts = STS_NG;
		}
		
		return $sts;
	}
}