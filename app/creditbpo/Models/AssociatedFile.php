<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class AssociatedFile extends Model
{	
    protected $table = 'tbldropboxentity';

    public function getAssociatedFileByPage($reportId, $page, $limit)
    {
        $associatedFile -= 1;
        $file = self::select(
                                    'id as id',
                                    'entity_id as reportid',
                                    'file_name as uploadedfile'    
                                    )
                              ->where(['entity_id'=>$reportId,'is_deleted' => 0])
                              ->orderBy('id', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $associatedFile;
    }

    public function getAssociatedFilesByReportId($reportId)
    {
        $associatedFiles = self::select(
            'id as id',
            'entity_id as reportid',
            'file_name as uploadedfile'
        )->where(['entity_id'=>$reportId,'is_deleted' => 0])->get();
        
        return $associatedFiles;
    }

    public function getAssociatedFileById($id)
    {
        $associatedFile = self::select(
                                    'id as id',
						            'entity_id as reportid',
						            'file_name as uploadedfile'
                                    )
                            ->where(['id'=>$id,'is_deleted'=>0])
                            ->first()
                            ->toArray();

        return $associatedFile;
    }
}