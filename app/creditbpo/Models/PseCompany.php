<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

//======================================================================
//  Class #: PSE Companies
//======================================================================
class PseCompany extends Model {
	protected $table        = 'pse_companies';
    protected $primaryKey   = 'pse_id';

    public function getPseListByPage($page, $limit){
        $page -= 1;
        $pse = self::select('pse_id as id', 'company_name as companyname', 'pse_code as code')
                        ->where('status', INT_TRUE)
                        ->orderBy('pse_id', 'ASC')
                        ->skip($limit * ($page))
                        ->take($limit)
                        ->get();
        return $pse;
    }

    public function getPseList(){
        $pse = self::select('pse_id as id', 'company_name as companyname', 'pse_code as code')
                        ->where('status', INT_TRUE)
                        ->orderBy('pse_id', 'ASC')
                        ->get();
        return $pse;
    }
}