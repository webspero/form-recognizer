<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class MajorSupplier extends Model {
    protected $table = 'tblmajorsupplier';
    protected $primaryKey = 'majorsupplierid';
    protected $fillable = 
        [
            'supplier_name',
            'supplier_share_sales', 
            'supplier_address', 
            'supplier_contact_person', 
            'supplier_email', 
            'supplier_phone', 
            'supplier_started_years', 
            'supplier_years_doing_business', 
            'supplier_settlement', 
            'supplier_order_frequency', 
            'relationship_satisfaction',
            'average_monthly_volume',
            'item_1',
            'item_2',
            'item_3'
        ];

    public function getByEntityId($entityId) {
        return $this->where('entity_id', '=', $entityId)->get();
    }

    /*
    * This will replace the funcion 'getByEntityId'
    *  will retain the getByEntityId function because there might be some functions that are using it.
    */
    public function getMajorSuppliersByReportId($reportId)
    {
        $majorSuppliers = self::select(
                        'majorsupplierid as id',
                        'entity_id as reportid',
                        'supplier_name as name',
                        'supplier_address as address',
                        'supplier_contact_person as contactperson',
                        'supplier_phone as contactnumber',
                        'supplier_email as email',
                        'supplier_started_years as yearstarted',
                        'supplier_years_doing_business as yearsdoingbusiness',
                        'relationship_satisfaction as relationshipsatisfaction',
                        'supplier_order_frequency as paymentfrequency',
                        'supplier_settlement as paymentbehavior',
                        'average_monthly_volume as averagemonthlyvolume',
                        'supplier_share_sales as purchasepercentage',
                        'item_1 as firstitem',
                        'item_2 as seconditem',
                        'item_3 as thirditem')->where('entity_id', $reportId)->get();
        return $majorSuppliers;
    }

    public function getReportMajorSupplierByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $majorSuppliers = self::select(
                                    'majorsupplierid as id',
                                    'entity_id as reportid',
                                    'supplier_name as name',
                                    'supplier_address as address',
                                    'supplier_contact_person as contactperson',
                                    'supplier_phone as contactnumber',
                                    'supplier_email as email',
                                    'supplier_started_years as yearstarted',
                                    'supplier_years_doing_business as yearsdoingbusiness',
                                    'relationship_satisfaction as relationshipsatisfaction',
                                    'supplier_order_frequency as paymentfrequency',
                                    'supplier_settlement as paymentbehavior',
                                    'average_monthly_volume as averagemonthlyvolume',
                                    'supplier_share_sales as purchasepercentage',
                                    'item_1 as firstitem',
                                    'item_2 as seconditem',
                                    'item_3 as thirditem'
                                    )
                              ->where('entity_id', $reportId)
                              ->orderBy('majorsupplierid', 'ASC')
                              ->skip($limit * ($page))
                              ->take($limit)
                              ->get();
        return $majorSuppliers;
    }

    public function getMajorSupplierById($id)
    {
        $majorSupplier = self::select(
                                    'majorsupplierid as id',
                                    'entity_id as reportid',
                                    'supplier_name as name',
                                    'supplier_address as address',
                                    'supplier_contact_person as contactperson',
                                    'supplier_phone as contactnumber',
                                    'supplier_email as email',
                                    'supplier_started_years as yearstarted',
                                    'supplier_years_doing_business as yearsdoingbusiness',
                                    'relationship_satisfaction as relationshipsatisfaction',
                                    'supplier_order_frequency as paymentfrequency',
                                    'supplier_settlement as paymentbehavior',
                                    'average_monthly_volume as averagemonthlyvolume',
                                    'supplier_share_sales as purchasepercentage',
                                    'item_1 as firstitem',
                                    'item_2 as seconditem',
                                    'item_3 as thirditem'
                                    )
                            ->where('majorsupplierid', $id)
                            ->first();
        return $majorSupplier;
    }

    public function addNewMajorSupplier($majorSupplierDto)
    {
        $majorSupplier = new MajorSupplier();
        $majorSupplier->entity_id = $majorSupplierDto->getReportId();
        $majorSupplier->supplier_name = $majorSupplierDto->getName();
        $majorSupplier->average_monthly_volume = $majorSupplierDto->getAverageMonthlyVolume();
        $majorSupplier->supplier_address = $majorSupplierDto->getAddress();
        $majorSupplier->supplier_contact_person = $majorSupplierDto->getContactPerson();
        $majorSupplier->supplier_phone = $majorSupplierDto->getContactNumber();
        $majorSupplier->supplier_email = $majorSupplierDto->getEmail();
        $majorSupplier->supplier_started_years = $majorSupplierDto->getYearStarted();
        $majorSupplier->supplier_years_doing_business = $majorSupplierDto->getYearsDoingBusiness();
        $majorSupplier->supplier_settlement = $majorSupplierDto->getPaymentBehavior();
        $majorSupplier->supplier_order_frequency = $majorSupplierDto->getPaymentFrequency();
        $majorSupplier->relationship_satisfaction = $majorSupplierDto->getRelationshipSatisfaction();
        $majorSupplier->supplier_share_sales = $majorSupplierDto->getPurchasePercentage();
        $majorSupplier->item_1 = $majorSupplierDto->getFirstItem();
        $majorSupplier->item_2 = $majorSupplierDto->getSecondItem();
        $majorSupplier->item_3 = $majorSupplierDto->getThirdItem();
        $majorSupplier->save();
        return $majorSupplier;
    }

    public function updateMajorSupplier($id, $data)
    {
        $majorSupplier = self::find($id);
        if (!empty($majorSupplier)){
            $majorSupplier->update(array_filter($data));
        }
        return $majorSupplier;
    }

    public function deleteMajorSupplier($id)
    {
        $majorSupplier = self::find($id);
        if (!empty($majorSupplier)){
            return $majorSupplier->delete();
        } else{
            return null;
        }
    }

    //-----------------------------------------------------
    //  Function #: Create MajorSupplier record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $majorSuppliers = self::where('entity_id', $previousReportId)->get();

        foreach ($majorSuppliers as $majorSupplier) {
            $newMajorSupplier = new MajorSupplier;

            $newMajorSupplier->entity_id = $reportDTO->getReportId();
            $newMajorSupplier->supplier_name = $majorSupplier->supplier_name;
            $newMajorSupplier->supplier_share_sales = $majorSupplier->supplier_share_sales;
            $newMajorSupplier->supplier_address = $majorSupplier->supplier_address;
            $newMajorSupplier->supplier_contact_person = $majorSupplier->supplier_contact_person;
            $newMajorSupplier->supplier_contact_details = $majorSupplier->supplier_contact_details;
            $newMajorSupplier->supplier_email = $majorSupplier->supplier_email;
            $newMajorSupplier->supplier_phone = $majorSupplier->supplier_phone;
            $newMajorSupplier->supplier_experience = $majorSupplier->supplier_experience;
            $newMajorSupplier->supplier_started_years = $majorSupplier->supplier_started_years;
            $newMajorSupplier->supplier_years_doing_business = $majorSupplier->supplier_years_doing_business;
            $newMajorSupplier->supplier_settlement = $majorSupplier->supplier_settlement;
            $newMajorSupplier->supplier_order_frequency = $majorSupplier->supplier_order_frequency;
            $newMajorSupplier->created_at = date('Y-m-d H:i:s');
            $newMajorSupplier->updated_at = date('Y-m-d H:i:s');

            $newMajorSupplier->save();
        }
    }
}
