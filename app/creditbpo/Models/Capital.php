<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class Capital extends Model
{	
    protected $table = 'tblcapital';
    
    //-----------------------------------------------------
    //  Function #: Create Capital record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $capitalRecords = self::where('entity_id', $previousReportId)->where('is_deleted',0)->get();

        foreach ($capitalRecords as $capitalRecord) {
            $newCapitalRecord = new Capital;

            $newCapitalRecord->entity_id = $reportDTO->getReportId();
            $newCapitalRecord->capital_authorized = $capitalRecord->capital_authorized;
            $newCapitalRecord->capital_issued = $capitalRecord->capital_issued;
            $newCapitalRecord->capital_paid_up = $capitalRecord->capital_paid_up;
            $newCapitalRecord->capital_ordinary_shares = $capitalRecord->capital_ordinary_shares;
            $newCapitalRecord->capital_par_value = $capitalRecord->capital_par_value;
            $newCapitalRecord->created_at = date('Y-m-d H:i:s');
            $newCapitalRecord->updated_at = date('Y-m-d H:i:s');

            $newCapitalRecord->save();
        }
    }

    public function getCapitalDetailsByPage($reportId, $page, $limit)
    {
        $page -= 1;
        $capitalDetails = self::select(
                                    'capitalid as id',
                                    'entity_id as reportid',
                                    'capital_authorized as authorizedcapital',
                                    'capital_issued as issuedcapital',
                                    'capital_paid_up as paidupcapital',
                                    'capital_ordinary_shares as ordinaryshares',
                                    'capital_par_value as parvalue'    
                                    )
                              ->where(['entity_id' => $reportId, 'is_deleted' => 0])
                              ->orderBy('capitalid', 'ASC')
                              ->skip($page)
                              ->take($limit)
                              ->get();
        return $capitalDetails;
    }

    public function getCapitalDetailsByReportId($reportId)
    {
        $capitalDetails = self::select(
            'capitalid as id',
            'entity_id as reportid',
            'capital_authorized as authorizedcapital',
            'capital_issued as issuedcapital',
            'capital_paid_up as paidupcapital',
            'capital_ordinary_shares as ordinaryshares',
            'capital_par_value as parvalue' 
        )->where(['entity_id' => $reportId, 'is_deleted' => 0])->get();
        
        return $capitalDetails;
    }

    public function getCapitalDetailsById($capitalid)
    {
        $capitalDetails = self::select(
                                    'capitalid as id',
						            'entity_id as reportid',
						            'capital_authorized as authorizedcapital',
						            'capital_issued as issuedcapital',
						            'capital_paid_up as paidupcapital',
						            'capital_ordinary_shares as ordinaryshares',
						            'capital_par_value as parvalue'
                                    )
                            ->where(['capitalid' => $capitalid, 'is_deleted' => 0])
                            ->first()
                            ->toArray();

        return $capitalDetails;
    }
}