<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class Bank extends Model {
    protected $table = 'tblbank';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    public function getStandaloneBanksByPage($page, $limit)
    {
        $page -= 1;
        $banks = self::select('id', 'bank_name as organizationname')
                     ->where('is_standalone', INT_TRUE)
                     ->orderBy('bank_name', 'ASC')
                     ->skip($limit * ($page))
                     ->take($limit)
                     ->get();
        return $banks;
    }

    public function getStandaloneBanks()
    {
        $banks = self::where('is_standalone', INT_TRUE)
                     ->get();
        return $banks;
    }

    public function getBankById($bankId)
    {
        $bank = self::where('id', $bankId)
                    ->first();
                    \Log::info($bank);
        return $bank;
    }

    public function getOrganizationById($bankId)
    {
        $bank = self::where('id', $bankId)
                    ->where('is_standalone', INT_TRUE)
                    ->first();
                    \Log::info($bank);
        return $bank;
    }

    public function getOrganizationByName($name)
    {
        $organization = self::where('bank_name',$name)->first();
        return $organization;
    }

    public function updateOrganization($organizationId, $data){
    	$organization = self::where('id',$organizationId)->update($data);
        return $organization;
    }

    public function addOrganization($data){
    	if(self::create($data));
    		return STS_OK;
    	return STS_NG;
    }
}
