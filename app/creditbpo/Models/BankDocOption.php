<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

//======================================================================
//  Class #: BankDocOptions
//  Bank Document Options Database Model
//======================================================================
class BankDocOption extends Model {

    /*-------------------------------------------------------------------------
	|   Class Attributes
	|--------------------------------------------------------------------------
	|	table		--- Database table name
    |	primaryKey	--- Primary key of the database table
    |   guarded     --- Fields that are not mass assignable
    |	timestamps	--- Prevents created_at and deleted_at fields from being required
    |   hidden      --- Hides fields on response or user view
    |------------------------------------------------------------------------*/
    protected $table = 'bank_doc_options';
	protected $guarded = array('id', 'created_at', 'updated_at');

    //-----------------------------------------------------
    //  Function #: getBankDocOptions
    //  Gets bank documents options
    //-----------------------------------------------------
    function getBankDocOptions($bankId)
    {
        $docRequirements = self::where('bank_id', $bankId)
                               ->where('is_active', 1)
                               ->first();
        return $docRequirements;
    }
}