<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReadyRatioScore extends Model {
    protected $casts = [
        'score_range' => 'double',
    ];
    protected $table = 'tblreadyrationscore';

    public function getByScoreRange($scoreRange) {
        $scoreRound = round($scoreRange, 1);

        return $this->whereRaw('CAST(score_range AS DECIMAL(5,1)) = '.$scoreRound)
            ->first();
    }

    public function getClosestByScoreRange($scoreRange) {
        return $this->whereRaw($scoreRange.' >= CAST(score_range AS DECIMAL(5,1))')
            ->first();
    }
}
