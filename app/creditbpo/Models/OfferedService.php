<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model;

class OfferedService extends Model
{	
    protected $table = 'tblservicesoffer';
    
    //-----------------------------------------------------
    //  Function #: Create Offered Services record for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($reportDTO, $previousReportId)
    {
        $offeredServices = self::where('entity_id', $previousReportId)->get();

        foreach ($offeredServices as $offeredService) {
            $newOfferedService = new OfferedService;

            $newOfferedService->entity_id = $reportDTO->getReportId();
            $newOfferedService->servicesoffer_name = $offeredService->servicesoffer_name;
            $newOfferedService->servicesoffer_targetmarket = $offeredService->servicesoffer_targetmarket;
            $newOfferedService->servicesoffer_share_revenue = $offeredService->servicesoffer_share_revenue;
            $newOfferedService->servicesoffer_seasonality = $offeredService->servicesoffer_seasonality;
            $newOfferedService->created_at = date('Y-m-d H:i:s');
            $newOfferedService->updated_at = date('Y-m-d H:i:s');

            $newOfferedService->save();
        }
    }
}