<?php

namespace CreditBPO\Models;

use Illuminate\Database\Eloquent\Model ;

class Education extends Model {
    protected $table = 'tbleducation';

    //-----------------------------------------------------
    //  Function #: Create records for new report with data from previous report
    //-----------------------------------------------------
    public function createWithPreviousData($oldRecord, $newRecord, $reportId)
    {
        $educationRecords = self::where('ownerid', $oldRecord)->get();

        foreach ($educationRecords as $education) {
            $newEducation = new Education;

            $newEducation->ownerid = $newRecord;
            $newEducation->entity_id = $reportId;
            $newEducation->educ_type = $education->educ_type;
            $newEducation->school_name = $education->school_name;
            $newEducation->educ_degree = $education->educ_degree;
            $newEducation->created_at = date('Y-m-d H:i:s');
            $newEducation->updated_at = date('Y-m-d H:i:s');
    
            $newEducation->save();
        }
    }
}