<?php

//namespace CreditBPO\Controllers\Admin;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;
use CreditBPO\Controllers\BaseController;
use CreditBPO\Handlers\ValidUserStatisticsHandler;
use CreditBPO\Handlers\IndustryMainHandler;
use CreditBPO\Handlers\EntityHandler;
use CreditBPO\Handlers\RegionsHandler;

class ValidUserStatisticsController extends BaseController {
    /**
     * @param ValidUserStatisticsHandler|null $handler
     */
    public function __construct(ValidUserStatisticsHandler $handler = null) {
        $this->handler = $handler ?: new ValidUserStatisticsHandler();
    }

    /**
     * @return string
     */
    public function main() {
        $entityHandler = new EntityHandler();
        $industryHandler = new IndustryMainHandler();
        $regionsHandler = new RegionsHandler();

        $entities = $entityHandler->getAllTrialAccountsLists();
        $mainIndustries = $industryHandler->getAllLists();
		$regions = Region::select('regDesc', 'regCode')->pluck('regDesc', 'regCode');

        return View::make('admin.valid-user-statistics.main', [
            'sme_list' => $entities->toArray(),
            'sme_selected' => array(),
            'main_industry_list' => $mainIndustries->toArray(),
            'main_industry_selected' => '',
            'sub_industry_list' => array(),
            'sub_industry_selected' => '',
            'industry_list' => array(),
            'industry_selected' => '',
            'bcc_from' => '',
            'bcc_to' => '',
            'mq_from' => '',
            'mq_to' => '',
            'fc_from' => '',
            'fc_to' => '',
            'rating_from' => '',
            'rating_to' => '',
            'brating_from' => '',
            'brating_to' => '',
            'type' => 1,
            'data' => null,
            'region_list' => $regions->toArray()
        ]);
    }

    /**
     * @return array|void
     */
    public function getStatistics() {
        $result = $this->handler->getStatistics();

        if ($result) {
            return $result;
        }

        App::abort(404);
    }
}
