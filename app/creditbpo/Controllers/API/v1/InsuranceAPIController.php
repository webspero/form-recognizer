<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\InsuranceCRUD;
use CreditBPO\DTO\InsuranceDTO;

class InsuranceAPIController extends ApiController
{
    public function __construct()
    {
        $this->insuranceCRUDService = new InsuranceCRUD();
    }

    public function getInsuranceList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->insuranceCRUDService->getInsuranceByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->insuranceCRUDService->getInsuranceByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getInsuranceDetails($insuranceid)
    {
        $insuranceDto = $this->insuranceCRUDService->getInsuranceById($insuranceid);

        if (!is_null($insuranceDto)){
            return $this->response($insuranceDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postInsurance($reportId)
    {
    	$data = request()->all();

        /* Check if report id exist */
        $reportExist = $this->insuranceCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $insuranceDto = new InsuranceDTO();
            $insuranceDto->setVars($data);
            
            /* Validate request */
            $result = $this->insuranceCRUDService->validateInsuranceDetails($insuranceDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedInsuranceDto = $this->insuranceCRUDService->insuranceInsert($insuranceDto);
                if (!is_null($savedInsuranceDto->getId())){
                    return $this->response($savedInsuranceDto->getVars(), "Affiliates/Subsidiaries saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error affiliate subsidiaries.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateInsurance($insuranceId)
    {
    	$data = request()->all();

        $insuranceDto = new InsuranceDTO();
        $insuranceDto->setVars($data);
        
        /* Validate request */
        $result = $this->insuranceCRUDService->validateInsuranceDetails($insuranceDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedInsuranceDto = $this->insuranceCRUDService->insuranceUpdate($insuranceDto);
            if (!is_null($udpatedInsuranceDto->getId())){
                return $this->response($udpatedInsuranceDto->getVars(), "Insurance updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error insurance.");
            }
        	
        }
        
    }

    public function deleteInsurance($insuranceId)
    {
    	$data = request()->all();
       
        $deletedInsuranceDto = $this->insuranceCRUDService->insuranceDelete($insuranceId);
        if (!is_null($deletedInsuranceDto)){
            return $this->response([], "Insurance deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error insurance.");
        }
        
    }
}