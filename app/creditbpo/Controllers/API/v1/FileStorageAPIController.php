<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\FileStorageCRUD;
use CreditBPO\DTO\FileStorageDTO;

class FileStorageAPIController extends ApiController
{
    public function __construct()
    {
        $this->fileStorageCRUDService = new FileStorageCRUD();
    }

    public function getFileList($id)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit'); 
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->fileStorageCRUDService->getFileStorageByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->fileStorageCRUDService->getFileStoragesByLoginId($id);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getFile($id)
    {

        $fileStorageDto = $this->fileStorageCRUDService->getFileStorageById($id);

        //print_r($fileStorageDto); exit;

        if (!is_null($fileStorageDto)){
            
            $file = public_path('user-files/').$fileStorageDto->loginid.'/'.$fileStorageDto->uploadedfile;
            
	        if (file_exists($file)){
	            
	            return response()->download($file);
	        } else {
	            return $this->respondInternalError('File cannot be found.');
	        }
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postFile($reportId)
    {
    	$data = request()->all();
    	
        /* Check if report id exist */
        $reportExist = $this->fileStorageCRUDService->checkReportIdIfExist($reportId);
        
        if ($reportExist){
            $FileStorageDto = new FileStorageDTO();
            $FileStorageDto->setVars($data);
            
            /* Validate request */
            $result = $this->fileStorageCRUDService->validateFileStorage($data);
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedFileStorageDto = $this->fileStorageCRUDService->FileStorageInsert($data);
                if (!is_null($savedFileStorageDto->getId())){
                    return $this->response($savedFileStorageDto->getVars(), "Associated Files saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error certification.");
                }
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function deleteFile($id)
    {
    	$data = request()->all();
       
        $deletedFileStorageDto = $this->fileStorageCRUDService->FileStorageDelete($id);
        if (!is_null($deletedFileStorageDto)){
            return $this->response([], "Associated Files deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error Associated Files.");
        }        
    }
}