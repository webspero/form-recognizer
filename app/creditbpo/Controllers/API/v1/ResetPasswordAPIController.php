<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use Request;
use CreditBPO\Services\ResetPassword;
use CreditBPO\DTO\ResetPasswordDTO;

class ResetPasswordAPIController extends ApiController
{
    public function __construct()
    {
        $this->resetPasswordService = new ResetPassword;
    }

    public function postRequestResetPassword() 
    {
        /*Validate first the email address */
        $resetPasswordDto = $this->resetPasswordService->validateEmail(Request::all());
        if (!is_null($resetPasswordDto)){
            /*Generate code and send to user */
            $isSuccess = $this->resetPasswordService->sendEmailRequestResetPassword($resetPasswordDto);
            if ($isSuccess){
                return $this->response(null, 'Please check your email for notification code', HTTP_STS_OK);
            } else{
                return $this->respondInternalError("Problem encountered while requesting reset password.");
            }
        } else{
            return $this->error('The email you entered is not a registered user.', HTTP_STS_BADREQUEST);
        }
    }

    public function postResetPassword() 
    {
        /* Validate request data */
        $resetPasswordDto = new ResetPasswordDTO;
        $resetPasswordDto->setVars(Request::all());
        $validator = $this->resetPasswordService->validateRequest($resetPasswordDto);
        if ($validator->fails()){
            return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
        } else{
            /*Validate email, code and password */
            $error = $this->resetPasswordService->validateResetPasswordDetails($resetPasswordDto);
            if (is_null($error)){
                /*Update user password and code status */
                $result = $this->resetPasswordService->changePassword($resetPasswordDto);
                if ($result){
                    return $this->response(
                        $resetPasswordDto->getEmail(), 
                        'Password successfully changed. Please login using your new password.', 
                        HTTP_STS_OK);
                } else{
                    return $this->respondInternalError('Problem encountered while updating user password.');
                }
            } else{
                return $this->error($error, HTTP_STS_BADREQUEST);
            }
        }
    }
}