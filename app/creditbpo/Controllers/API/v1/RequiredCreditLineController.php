<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use URL;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Services\Report\Api\ReportService;
use CreditBPO\Services\RequiredCreditLine\CreditLineListService;
use CreditBPO\Services\RequiredCreditLine\CreditLineAddService;
use CreditBPO\Services\RequiredCreditLine\CreditLineEditService;
use CreditBPO\Services\RequiredCreditLine\CreditLineDeleteService;

class RequiredCreditLineController extends ApiController
{
    public function __construct()
    {
        $this->reportService = new ReportService;
        $this->creditLineListService = new CreditLineListService;
        $this->creditLineAddService = new CreditLineAddService;
        $this->creditLineEditService = new CreditLineEditService;
        $this->creditLineDeleteService = new CreditLineDeleteService;
        $this->userTokenService = new UserTokenService;
    }

    public function getCreditLines($reportId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if user owns report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);
        
        if($checkReport == null) {

            $page = (Request::query('page') !== null)? Request::query('page') : null;
            $limit = (Request::query('limit') !== null)? Request::query('limit') : null;

            if($page && $limit) {
                $result = $this->creditLineListService->getCreditLinesByPage($reportId, $page, $limit);
                if (empty($result['items'])){
                    return $this->response(null, 'No results found.', HTTP_STS_OK);
                } else{
                    return $this->response($result, 'Requested Credit Lines generated.', HTTP_STS_OK);
                }
            } else {
                /* Get list of reports owned by the user currently logged in */
                $creditLines = $this->creditLineListService->getCreditLines($reportId);
        
                if(!empty($creditLines)) {
                    return $this->response(
                        [
                            "requestedcreditlines" => $creditLines
                        ],
                        "Requested Credit Lines generated.",
                        HTTP_STS_OK
                    );
                } else {
                    return $this->response(null, 'No Credit Lines found.', HTTP_STS_OK);
                }
            }
        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function getCreditLineDetails($reportId, $creditLineId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
        
        /* Check if user owns report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Check if Credit Line belongs to the report */
            $checkCreditLine = $this->creditLineListService->validateCreditLine($reportId, $creditLineId);

            if($checkCreditLine == null) {
                /* Get Credit Line Details */
                $creditLine = $this->creditLineListService->getCreditLineDetails($creditLineId);

                return $this->response(
                    [
                        "requestedcreditline" => $creditLine->getFormVars()
                    ],
                    "Requested Credit Line generated.",
                    HTTP_STS_OK
                );
            } else {
                return $this->error(
                    $checkCreditLine['errors'],
                    $checkCreditLine['code']
                );
            }
        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function postCreditLine($reportId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
        
        /* Check if user owns report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {

            /* Field Validation */
            $validateCreditLine = $this->creditLineAddService->validateRequest(Request::all());

            if($validateCreditLine->passes()) {
                /* Add Credit Line */
                $newCreditLineDTO = $this->creditLineAddService->addCreditLine(Request::all(), $reportId);

                if(!is_null($newCreditLineDTO->getCreditLineId())){
                    return $this->response($newCreditLineDTO->getFormVars(), "Requested Credit Line Added.", HTTP_STS_CREATED);
                }else{
                    return $this->respondInternalError("Error saving Credit Line details.");
                }
            } else {
                /*return bad request with validation messages */
                return $this->error($validateCreditLine->messages()->all(), HTTP_STS_BADREQUEST);
            }
        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function updateCreditLine($reportId, $creditLineId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
        
        /* Check if user owns report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Check if Credit Line belongs to the report */
            $checkCreditLine = $this->creditLineListService->validateCreditLine($reportId, $creditLineId);

            if($checkCreditLine == null) {

                /* Field Validation */
                $validateCreditLine = $this->creditLineAddService->validateRequest(Request::all());

                if($validateCreditLine->passes()) {
                    /* Get Credit Line Details */
                    $existingCreditLineDTO = $this->creditLineListService->getCreditLineDetails($creditLineId);
                    $updatedCreditLineDTO = $this->creditLineEditService->prepareCreditLineUpdate($existingCreditLineDTO, Request::all());
    
                    if($updatedCreditLineDTO->getErrors() == null) {
                        $this->creditLineEditService->updateCreditLine($updatedCreditLineDTO, $existingCreditLineDTO);
                        
                        if(!is_null($updatedCreditLineDTO->getCreditLineId())){
                            return $this->response($updatedCreditLineDTO->getFormVars(), "Requested Credit Line Successfully Edited.", HTTP_STS_CREATED);
                        }
                    } else {
                        return $this->error($updatedCreditLineDTO->getErrors(), HTTP_STS_BADREQUEST);
                    }
                } else {
                    /*return bad request with validation messages */
                    return $this->error($validateCreditLine->messages()->all(), HTTP_STS_BADREQUEST);
                }

            } else {
                return $this->error(
                    $checkCreditLine['errors'],
                    $checkCreditLine['code']
                );
            }
        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function deleteCreditLine($reportId, $creditLineId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
        
        /* Check if user owns report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Check if Credit Line belongs to the report */
            $checkCreditLine = $this->creditLineListService->validateCreditLine($reportId, $creditLineId);

            if($checkCreditLine == null) {

                /* Get Credit Line Details */
                $existingCreditLineDTO = $this->creditLineListService->getCreditLineDetails($creditLineId);

                $this->creditLineDeleteService->deleteCreditLine($existingCreditLineDTO);

                return $this->response(
                    null,
                    "Requested Credit Line Successfully Deleted.",
                    HTTP_STS_OK
                );

            } else {
                return $this->error(
                    $checkCreditLine['errors'],
                    $checkCreditLine['code']
                );
            }
        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }
}