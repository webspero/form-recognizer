<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\MajorCustomer\MajorCustomerList;
use CreditBPO\Services\MajorCustomer\MajorCustomerInsert;
use CreditBPO\Services\MajorCustomer\MajorCustomerUpdate;
use CreditBPO\Services\MajorCustomer\MajorCustomerDelete;
use CreditBPO\Services\MajorCustomerAccount\MajorCustomerAccountUpdate;
use CreditBPO\DTO\MajorCustomerDTO;

class MajorCustomerAPIController extends ApiController
{
    public function __construct()
    {
        $this->majorCustomerListService = new MajorCustomerList();
        $this->majorCustomerInsertService = new MajorCustomerInsert();
        $this->majorCustomerUpdateService = new MajorCustomerUpdate();
        $this->majorCustomerDeleteService = new MajorCustomerDelete();
        $this->majorCustomerAccountUpdateService = new MajorCustomerAccountUpdate();
    }

    public function getReportMajorCustomerList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->majorCustomerListService->getReportMajorCustomerByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }else{
            $result = $this->majorCustomerListService->getReportMajorCustomerList($reportId);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getMajorCustomerDetails($id)
    {
        $majorCustomerDto = $this->majorCustomerListService->getMajorCustomerById($id);
        if (!is_null($majorCustomerDto)){
            return $this->response($majorCustomerDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postMajorCustomer($reportId)
    {
        /* Check if report id exist */
        $reportExist = $this->majorCustomerAccountUpdateService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $majorCustomerDto = new MajorCustomerDTO();
            /* Populate Major Customer DTO of request values */
            $majorCustomerDto->setVars(Request::all());
            $majorCustomerDto->setReportId($reportId);
            $majorCustomerDto->setYearsDoingBusiness(date('Y') - $majorCustomerDto->getYearStarted());

            /* Validate request */
            $result = $this->majorCustomerInsertService->validateMajorCustomerDetails($majorCustomerDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {
                $savedMajorCustomerDto = $this->majorCustomerInsertService->postReportMajorCustomer($majorCustomerDto, $result['anonymous']);
                if (!is_null($savedMajorCustomerDto->getId())){
                    return $this->response($savedMajorCustomerDto->getVars(), "Major customer saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error saving major customer.");
                }
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function putMajorCustomer($id)
    {
        /* Check if major customer id exist, then get report id */
        $reportId = $this->majorCustomerUpdateService->getMajorSupplierReportId($id);
        if (!is_null($reportId)){
            $majorCustomerDto = new MajorCustomerDTO();
            /* Populate Major Customer DTO of database and request values */
            $majorCustomerDto->setVars(Request::all());
            /* Validate data request */
            $validator = $this->majorCustomerUpdateService->validateRequestDetails($majorCustomerDto, $reportId);
            
            if (!empty($validator)){
                return $this->error($validator, HTTP_STS_BADREQUEST);
            } 
            /* Validation success, update data to database */
            else{
                $savedMajorCustomerDto = $this->majorCustomerUpdateService->updateReportMajorCustomer($id, $majorCustomerDto);
                return $this->response($savedMajorCustomerDto->getVars(), 'Major customer updated successfully.', HTTP_STS_OK);
            }
        } 
        /* Major customer doesn't exist */
        else{
            return $this->error("Major customer doesn't exist!", HTTP_STS_BADREQUEST);
        }
    }
    
    public function deleteMajorCustomer($id)
    {
        $deleteResponse = $this->majorCustomerDeleteService->deleteMajorCustomer($id);
        if (!is_null($deleteResponse)){
            if ($deleteResponse){
                /* Used http-ok instead of http-nocontent to display deletion message */
                return $this->response(null, "Major customer deleted successfully.", HTTP_STS_OK);
            } else{
                return $this->respondInternalError("Error occured while deleting major customer.");
            }
        } else{
            return $this->error("Major customer doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function updateMajorCustomerAccount($reportId)
    {
        /* Check if report id exist */
        $reportExist = $this->majorCustomerAccountUpdateService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $isAgree = Request::input('agree');
            $error = $this->majorCustomerAccountUpdateService->validateRequestDetails($isAgree);
            
            if (!is_null($error)){
                return $this->error($error, HTTP_STS_BADREQUEST);
            } else{
                $updateResponse = $this->majorCustomerAccountUpdateService->updateMajorCustomerAccount($reportId, $isAgree);
                if ($updateResponse){
                    $isAgree = $isAgree ? "true" : "false";
                    return $this->response(null, 'No individual customer accounts for more than 1% of sales was set to '.$isAgree, HTTP_STS_OK);
                } else {
                    return $this->respondInternalError("Error updating major customer account.");
                }
            }
        } else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }
}