<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\CapitalDetailsCRUD;
use CreditBPO\DTO\CapitalDetailsDTO;

class CapitalDetailsAPIController extends ApiController
{
    public function __construct()
    {
        $this->capitalDetailsCRUDService = new CapitalDetailsCRUD();
    }

    public function getCapitalDetailsList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->capitalDetailsCRUDService->getCapitalDetailsByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->capitalDetailsCRUDService->getCapitalDetailsByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getCapitalDetails($capitalid)
    {
        $capitalDetailsDto = $this->capitalDetailsCRUDService->getCapitalDetailsById($capitalid);

        if (!empty($capitalDetailsDto)){
            return $this->response($capitalDetailsDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postCapitalDetails($reportId)
    {
    	$data = request()->all();
    	
        /* Check if report id exist */
        $reportExist = $this->capitalDetailsCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $capitalDetailsDto = new CapitalDetailsDTO();
            $capitalDetailsDto->setVars($data);
            
            /* Validate request */
            $result = $this->capitalDetailsCRUDService->validateCapitalDetails($capitalDetailsDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }

            $checkCapitalDetailsIfExist = $this->capitalDetailsCRUDService->checkCapitalDetailsIfExist($reportId);
            if(!empty($checkCapitalDetailsIfExist)){

            	return $this->error("Capital details already exists",HTTP_STS_BADREQUEST);
            }

            /* Validation success, save data to database */
            else {

                $savedCapitalDetailsDto = $this->capitalDetailsCRUDService->capitalDetailsInsert($capitalDetailsDto);
                if (!empty($savedCapitalDetailsDto->getId())){
                    return $this->response($savedCapitalDetailsDto->getVars(), "Capital Details saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error Capital Details.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateCapitalDetails($capitalid)
    {
    	$data = request()->all();

        $capitalDetailsDto = new CapitalDetailsDTO();
        $capitalDetailsDto->setVars($data);
        
        /* Validate request */
        $result = $this->capitalDetailsCRUDService->validateCapitalDetails($capitalDetailsDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedCapitalDetailsDto = $this->capitalDetailsCRUDService->capitalDetailsUpdate($capitalDetailsDto);
            if (!empty($udpatedCapitalDetailsDto->getId())){
                return $this->response($udpatedCapitalDetailsDto->getVars(), "Capital Details updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Capital Details.");
            }
        	
        }
        
    }

    public function deleteCapitalDetails($capitalid)
    {
    	$data = request()->all();
       
        $deletedCapitalDetailsDto = $this->capitalDetailsCRUDService->capitalDetailsDelete($capitalid);
        if (!empty($deletedCapitalDetailsDto)){
            return $this->response([], "Capital Details deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error capital details.");
        }
        
    }
}