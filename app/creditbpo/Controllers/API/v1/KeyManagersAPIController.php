<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\KeyManagersCRUD;
use CreditBPO\DTO\KeyManagersDTO;

class KeyManagersAPIController extends ApiController
{
    public function __construct()
    {
        $this->keyManagersCRUDService = new KeyManagersCRUD();
    }

    public function getKeyManagersList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->keyManagersCRUDService->getKeyManagersByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->keyManagersCRUDService->getKeyManagersByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getKeyManagersDetails($relatedCompaniesId)
    {
        $keyManagersDto = $this->keyManagersCRUDService->getKeyManagersById($relatedCompaniesId);

        if (!empty($keyManagersDto)){
            return $this->response($keyManagersDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postKeyManagers($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->keyManagersCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $keyManagersDto = new KeyManagersDTO();
            $keyManagersDto->setVars($data);
            
            /* Validate request */
            $result = $this->keyManagersCRUDService->validateKeyManagersDetails($keyManagersDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedKeyManagersDto = $this->keyManagersCRUDService->keyManagersInsert($keyManagersDto);
                if (!empty($savedKeyManagersDto->getId())){
                    return $this->response($savedKeyManagersDto->getVars(), "Key Managers saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error Key Managers.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateKeyManagers($relatedCompaniesId)
    {
    	$data = request()->all();

        $keyManagersDto = new KeyManagersDTO();
        $keyManagersDto->setVars($data);
        
        /* Validate request */
        $result = $this->keyManagersCRUDService->validateKeyManagersDetails($keyManagersDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedKeyManagersDto = $this->keyManagersCRUDService->keyManagersUpdate($keyManagersDto);
            if (!empty($udpatedKeyManagersDto->getId())){
                return $this->response($udpatedKeyManagersDto->getVars(), "Key Managers updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Key Managers.");
            }
        	
        }
        
    }

    public function deleteKeyManagers($keyManagerId)
    {
    	$data = request()->all();
       
        $deletedKeyManagersDto = $this->keyManagersCRUDService->keyManagersDelete($keyManagerId);
        if (!empty($deletedKeyManagersDto)){
            return $this->response([], "Key Managers deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error Key Managers.");
        }
        	
       
        
    }
}