<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\DocumentDelete;

class DocumentAPIController extends ApiController
{
    public function __construct()
    {
        $this->documentDeleteService = new DocumentDelete;
    }

    public function downloadDocument($path, $filename)
    {
        $download = $path.'/'.$filename;
        if(file_exists($download)){
            return $this->download($download, $filename);
        } else {
            return $this->error("File not found", HTTP_STS_NOTFOUND);
        }
    }

    public function downloadReportDocument($reportId, $path, $filename)
    {
        if((strpos($path, 'innodata') !== false)){
            $download = $path. '/' . $filename;
        }else{
            $download = $path.'/'.$reportId.'/'.$filename;
        }

        if(file_exists($download)){
            return $this->download($download, $filename);
        } else {
            return $this->error("File not found", HTTP_STS_NOTFOUND);
        }
    }

    public function deleteDocument($id)
    {
        /* Delete data */
        $deleteResult = $this->documentDeleteService->deleteDocument($id);
        if (!is_null($deleteResult)){
            if ($deleteResult){
                return $this->response(null, "Document deleted successfully.", HTTP_STS_OK);
            } else{
                return $this->respondInternalError("Error in deleting the document.");
            }
        } else{
            return $this->error("Document id doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }
}