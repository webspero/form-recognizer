<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\DirectorsCRUD;
use CreditBPO\DTO\DirectorsDTO;

class DirectorsAPIController extends ApiController
{
    public function __construct()
    {
        $this->directorsCRUDService = new DirectorsCRUD();
    }

    public function getDirectorsList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->directorsCRUDService->getDirectorsByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->directorsCRUDService->getDirectorsByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getDirectorsDetails($relatedCompaniesId)
    {
        $directorsDto = $this->directorsCRUDService->getDirectorsById($relatedCompaniesId);

        if (!empty($directorsDto)){
            return $this->response($directorsDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postDirectors($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->directorsCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $directorsDto = new DirectorsDTO();
            $directorsDto->setVars($data);
            
            /* Validate request */
            $result = $this->directorsCRUDService->validateDirectorsDetails($directorsDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedDirectorsDto = $this->directorsCRUDService->directorsInsert($directorsDto);
                if (!empty($savedDirectorsDto->getId())){
                    return $this->response($savedDirectorsDto->getVars(), "Directors saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error Directors.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateDirectors($id)
    {
    	$data = request()->all();

        $directorsDto = new DirectorsDTO();
        $directorsDto->setVars($data);
        
        /* Validate request */
        $result = $this->directorsCRUDService->validateDirectorsDetails($directorsDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedDirectorsDto = $this->directorsCRUDService->directorsUpdate($directorsDto);

            if (!empty($udpatedDirectorsDto->getId())){
            
                return $this->response($udpatedDirectorsDto->getVars(), "Directors updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Directors.");
            }
        	
        }
        
    }

    public function deleteDirectors($id)
    {
    	$data = request()->all();
       
        $deletedDirectorsDto = $this->directorsCRUDService->directorsDelete($id);
        if (!empty($deletedDirectorsDto)){
            return $this->response([], "Directors deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error directors.");
        }        
    }
}