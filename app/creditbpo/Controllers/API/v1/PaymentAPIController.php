<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Auth;
use Session;
use URL;
use View;
use Redirect;
use Omnipay\Omnipay;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Libraries\PaymentPrepLib;
use CreditBPO\Services\Payments\PaymentService;
use CreditBPO\Services\Payments\DragonpayService;
use CreditBPO\Services\Payments\PaypalService;
use CreditBPO\Libraries\PaymentCalculator;
use CreditBPO\Services\UserTokenService;
use CreditBPO\DTO\PaymentDTO;
use CreditBPO\DTO\DragonpayDTO;
use CreditBPO\Models\Discount;
use CreditBPO\DTO\PayPalDTO;
use CreditBPO\Services\MailHandler;
use DB;
use App\User;
use CreditBPO\Models\Entity;
use CreditBPO\Models\Zipcode;
use CreditBPO\Models\Bank;
use \Exception as Exception;
//======================================================================
//  Class #: API Report Payments Controller
//      Functions related to Reports Payment APIs are routed in this class
//======================================================================

class PaymentAPIController extends ApiController 
{

	/* Global Variables for Testing */
	private $userId;
	private $merchant_id = 'CREDITBPO';     // Merchant ID to accesss API
	private $merchant_key = 'N6f9rEW3';     // Merchat Key for API access
	//---------------------------------------------------------------
	// Function #.a. Initialization
	//---------------------------------------------------------------
	public function __construct()
	{
		$this->paymentDTO = new PaymentDTO;

		$this->paymentService = new PaymentService;
		$this->dragonpayService = new DragonpayService;
		$this->paypalService = new PaypalService;
		$this->userTokenService = new UserTokenService;

		/* For unit testing */
		$user = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'));
		if($user) {
			$this->userId  = $user->getUserId();
		} 
	}

	//---------------------------------------------------------------
	// Function #.b. Returns EULA
	// Gives EULA Statement and Report ID if authenticated user 
	// and if requested via URI, report is owned by said user
	//---------------------------------------------------------------
	public function getEULA($reportId = null)
	{
		/* Get Report based on passed Report ID (if any) and authenticated user */
		$reportRecord = $this->paymentService->getReport($reportId, $this->userId);

		/* Check passed Report ID if exists in the system */
		if($reportRecord != null) {

			$this->paymentDTO->setVars($reportRecord);

			/* Check if User owns the report */
			if ($this->paymentDTO->getUserId() == $this->userId){
				$eula = PaymentPrepLib::getEULA();

				/* Check if the payment is made for the report or not */
				if (!$this->paymentDTO->getIsPaid()) {
					return $this->response(
						[
							"eula" => $eula,
							"reportnumber" => $this->paymentDTO->getReportId()
						],
						"Success.",
						HTTP_STS_OK
					);
				} else {
					return $this->error(
						"Bad Request. Payment has been made for this report", 
						HTTP_STS_BADREQUEST
					);
				}
			} else {
				return $this->error(
					"Unauthorized Access. User is not owner of report", 
					HTTP_STS_FORBIDDEN
				);
			}

		} else {
			return $this->error(
				"Report does not exist.",
				HTTP_STS_BADREQUEST
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.c. Gives Face Amount and Report ID
	//---------------------------------------------------------------
	public function getAmount($reportId = null)
	{
		/* Get Report based on passed Report ID (if any) and authenticated user */
		$reportRecord = $this->paymentService->getReport($reportId, $this->userId);

		/* Check passed Report ID if exists in the system */
		if ($reportRecord != null) {
			$price = $this->paymentService->computeBaseAmount($reportId);
			$this->paymentDTO->setBaseAmount($price);
			
			$this->paymentDTO->setVars($reportRecord);

			/* Check if User owns the report */
			if ($this->paymentDTO->getUserId() == $this->userId){
				/* Check if the payment is made for the report or not */
				if (!$this->paymentDTO->getIsPaid()) {
					/* Check if a discount is applied */
					if($this->paymentDTO->getDiscountCode() != null) {
						/* Compute face amount and discount */
						$this->paymentService->getAmountAfterDiscount($this->paymentDTO);
						/* If face amount <= 0, user can proceed with completing the questionnaire */
						if ($this->paymentDTO->getTotalAmount() <= 0) {
							/* Run process for tagging Free Trial Accounts */
							$this->paymentService->togglePaymentFlags($this->userId, $reportId);
							/* Prompt user/developer can proceed to the dashboard */
							return $this->response(
								[
									null
								],
								"The user has a free trial account. Send a Reports Request to receive a list of reports under his account.",
								HTTP_STS_OK
							);
						} else {
							/* Return Report ID and Face Amount; Discount Amount and Base Amount will also be displayed so that user can be more informed about how his total amount came to be. */
							return $this->response(
								[
									"reportnumber" => $this->paymentDTO->getReportId(),
									"baseamount" => $this->paymentDTO->getBaseAmount(),
									"discountamount" => $this->paymentDTO->getDiscountAmount(),
									"totalamount" => $this->paymentDTO->getTotalAmount()
								],
								"Discount code has been applied.",
								HTTP_STS_OK
							);
						}
					} else {
					/* If there's no discount, return face amount and Report ID */
						return $this->response(
							[
								"reportnumber" => $this->paymentDTO->getReportId(),
								"totalamount" => $this->paymentDTO->getBaseAmount()
							],
							"No discount code applied.",
							HTTP_STS_OK
						);
					}
				} else {
					return $this->error(
						"Payment has been made for this report", 
						HTTP_STS_BADREQUEST
					);
				}
			} else {
				return $this->error(
					"User is not owner of report", 
					HTTP_STS_FORBIDDEN
				);
			}
		} else {
			return $this->error(
				"Report does not exist.",
				HTTP_STS_BADREQUEST
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.d. Upon selecting Dragonpay, this function will be called to provide the Dragonpay PS Request URL
	//---------------------------------------------------------------
	public function getDragonpayCheckout($reportId)
	{
		/* Get Report based on passed Report ID (if any) and authenticated user */
		$reportRecord = $this->paymentService->getReport($reportId, null);

		/** Check if has return URL */
		$returnUrl = Request::get('return_url');

		if($returnUrl == ''){
			return $this->error(
				"Return URL for the payment is required.",
				HTTP_STS_BADREQUEST
			);
		}

		/** Check if report organization is Matching Platform, additional field is required - email of the matching platform account to send the Invoice */
		$organization =  DB::table('tblbank')->where('id', $reportRecord->current_bank)->first();
		if($organization->bank_name == "Matching Platform"){

			// email
			$match_email_recipient = Request::get('match_email');
			if($match_email_recipient == ''){
				return $this->error(
					"Matching platform email account is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// street address
			$match_address = Request::get('match_street_address');
			if($match_address == ''){
				return $this->error(
					"Matching platform account address is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// city
			$match_city = Request::get('match_city');
			if($match_city == ''){
				return $this->error(
					"Matching platform account city is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// phone number
			$match_province = Request::get('match_province');
			if($match_province == ''){
				return $this->error(
					"Matching platform account province is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// zipcode
			$match_zipcode = Request::get('match_zipcode');
			if($match_zipcode == ''){
				return $this->error(
					"Matching platform account zipcode is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// phone number
			$match_phone_number = Request::get('match_phone_number');
			if($match_phone_number == ''){
				return $this->error(
					"Matching platform account phone number is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// full name
			$match_full_name = Request::get('match_full_name');
			if($match_full_name == ''){
				return $this->error(
					"Matching platform account full name is required.",
					HTTP_STS_BADREQUEST
				);
			}

			$api_match_account_details = json_encode(array(
				'email'	=> $match_email_recipient,
				'street'	=> $match_address,
				'city'	=> $match_city,
				'province'	=> $match_province,
				'zipcode'	=> $match_zipcode,
				'phonenumber'	=> $match_phone_number,
				'fullname'	=> $match_full_name
			));

			// set email of match email
			$this->paymentDTO->setAPIMatchAccountDetails($api_match_account_details);
		}

		
		/* IF Report Exists */
		/* Check passed Report ID if exists in the system */
		if($reportRecord != null) {

			$this->paymentDTO->setVars($reportRecord);

			/* Check IF User owns the report */
			if ($this->paymentDTO->getUserId() == $this->userId){

				$price = $this->paymentService->computeBaseAmount($reportId);
				$this->paymentDTO->setBaseAmount($price);

				/* Check if the payment is made for the report  */
				if (!$this->paymentDTO->getIsPaid()) {
					/* Calculate BASE vat output, vatable amount, total price, amount and then apply discount (adjusts amount, vat output, vatable amount), if any */
					$this->paymentService->getAmountAfterTaxAndDiscount($this->paymentDTO);

					/* After calculation, if total amount <= 0, tag user as free trial account and escape payment process */
					/* If Face Amount <= 0, user can proceed with completing the questionnaire */
					if ($this->paymentDTO->getTotalAmount() <= 0) {
						/* Run process for tagging Free Trial Accounts */
						$this->paymentService->togglePaymentFlags($this->userId, $reportId);
						/* Prompt user/developer can proceed to the dashboard */
						return $this->response(
							[
								null
							],
							"The user has a free trial account. Send a Reports Request to receive a list of reports under his account.",
							HTTP_STS_OK
						);
					} else {

						$clientReturn = $returnUrl;

						/* Save payment receipt and transaction based on data from transaction, Dragonpay parameters, and price calculation */
						$dragonpayDTO = $this->dragonpayService->recordDragonpayTransaction($this->paymentDTO, $clientReturn);

						/* Create Request URL */
						$requestURL = $this->dragonpayService->createDragonpayRequestUrl($dragonpayDTO);

						/* Return Dragonpay Request URL */
						return $this->response(
							[
								"requestUrl" => $requestURL
							],
							"Redirect user to the provided Request URL",
							HTTP_STS_OK
						);
					}

				} else {
					return $this->error(
						"Payment has been made for this report", 
						HTTP_STS_BADREQUEST
					);
				}
			} else {
				return $this->error(
					"User is not owner of report", 
					HTTP_STS_FORBIDDEN
				);
			}
		} else {
			return $this->error(
				"Report does not exist.",
				HTTP_STS_BADREQUEST
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.e. Upon completion of checkout via Dragonpay, this function will process the Response URL 
	// and will return corresponding response based on payment status
	//---------------------------------------------------------------
	function processDragonpayResponse()
	{
		/* Get URIs and  Check Digest*/
		$response = array(
			'txnid'		=> Session::get('txnid'),
			'refno'		=> Session::get('refno'),
			'status'	=> Session::get('status'),
			'message'	=> Session::get('message')
		);
		$responseDigest = Session::get('digest');

		//call service to set requests and return digest check
		$dragonpayDTO = new DragonpayDTO;
		$dragonpayDTO->setVars($response);
		$dragonpayDTO->setDigest($responseDigest);

		$response['returnUrl'] = Request::query('param1');

		/* Log Dragonpay Transaction */
		$this->dragonpayService->logDragonpay(Request::server('HTTP_HOST'), Request::server('REQUEST_URI'));

		if($this->dragonpayService->checkDigest($dragonpayDTO)) {
			/* Update Transaction Record */
			$isReportPurchase = $this->dragonpayService->updateTransaction($dragonpayDTO);

			$status = $dragonpayDTO->getStatus();

			/* Check Transaction Status */
			if($status == "S") {
				//Toggle Status & New User Flag, if necessary
				$isNew = $this->dragonpayService->toggleUserStatus($dragonpayDTO);

				if($isReportPurchase) {
					//Toggle Payment Flag for Report
					$this->paymentService->toggleReportPaidFlag($dragonpayDTO->getUser(), $dragonpayDTO->getReport());
				}

				//Get Payment Receipt Record and Send as Email
				$this->dragonpayService->sendEmailReceipt($dragonpayDTO);

				$response['returnUrl'] = $dragonpayDTO->getClientReturnUrl();

				$data = array(
					'status'	=> "S",
					'message'	=> "Payment Successfull",
					'reportid' => $dragonpayDTO->getReport()
				);

				$url = $response['returnUrl'] . '/' . json_encode($data);

				return Redirect::to($url);
				// return $this->response(
				// 	[
				// 		"clientReturn" => $response['returnUrl'],
				// 		"paymentstatus" => $response['status']
				// 	],
				// 	"Payment Transaction Successful.",
				// 	HTTP_STS_OK
				// );
			} else if ($status == "P") {
				/* Else If P, return pending prompt and reference number */
				// TODO: Redirect to Client Return URL, if provided
				// return $this->response(
				// 	[
				// 		"referencenumber" => $dragonpayDTO->getReferenceNumber(),
				// 		"clientReturn" => $dragonpayDTO->getClientReturnUrl(),
				// 		"paymentstatus" => $response['status']
				// 	],
				// 	"User's transaction has pending payment.",
				// 	HTTP_STS_OK
				// );

				$response['returnUrl'] = $dragonpayDTO->getClientReturnUrl();

				$data = array(
					'status'	=> "P",
					'message'	=> "Payment Transaction is pending.",
					'reportid' => $dragonpayDTO->getReport()
				);

				$url = $response['returnUrl'] . '/' . json_encode($data);

				return Redirect::to($url);
			} else {
				/* Else, return error prompt and previous request URL */
				return $this->error(
					"Error processing transaction", 
					HTTP_STS_BADREQUEST
				);
			}

		} else {
			return $this->error(
				"Incorrect digest", 
				HTTP_STS_FORBIDDEN
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.f. Upon selecting PayPal, this function will redirect to the PayPal Express Checkout via Omnipay
	//---------------------------------------------------------------
	function getPayPalCheckout($reportId)
	{
		/* Get Report based on passed Report ID (if any) and authenticated user */
		$reportRecord = $this->paymentService->getReport($reportId, null);

		/** Check if has return URL */
		$returnUrl = Request::get('return_url');

		if($returnUrl == ''){
			return $this->error(
				"Return URL for the payment is required.",
				HTTP_STS_BADREQUEST
			);
		}

		/** Check if report organization is Matching Platform, additional field is required - email of the matching platform account to send the Invoice */
		$organization =  DB::table('tblbank')->where('id', $reportRecord->current_bank)->first();
		if($organization->bank_name == "Matching Platform"){

			// email
			$match_email_recipient = Request::get('match_email');
			if($match_email_recipient == ''){
				return $this->error(
					"Matching platform email account is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// street address
			$match_address = Request::get('match_street_address');
			if($match_address == ''){
				return $this->error(
					"Matching platform account address is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// city
			$match_city = Request::get('match_city');
			if($match_city == ''){
				return $this->error(
					"Matching platform account city is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// phone number
			$match_province = Request::get('match_province');
			if($match_province == ''){
				return $this->error(
					"Matching platform account province is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// zipcode
			$match_zipcode = Request::get('match_zipcode');
			if($match_zipcode == ''){
				return $this->error(
					"Matching platform account zipcode is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// phone number
			$match_phone_number = Request::get('match_phone_number');
			if($match_phone_number == ''){
				return $this->error(
					"Matching platform account phone number is required.",
					HTTP_STS_BADREQUEST
				);
			}

			// full name
			$match_full_name = Request::get('match_full_name');
			if($match_full_name == ''){
				return $this->error(
					"Matching platform account full name is required.",
					HTTP_STS_BADREQUEST
				);
			}

			$api_match_account_details = json_encode(array(
				'email'	=> $match_email_recipient,
				'street'	=> $match_address,
				'city'	=> $match_city,
				'province'	=> $match_province,
				'zipcode'	=> $match_zipcode,
				'phonenumber'	=> $match_phone_number,
				'fullname'	=> $match_full_name
			));

			// set email of match email
			$this->paymentDTO->setAPIMatchAccountDetails($api_match_account_details);
		}
		
		/* IF Report Exists */
		/* Check passed Report ID if exists in the system */
		if($reportRecord != null) {

			$this->paymentDTO->setVars($reportRecord);

			/* Check IF User owns the report */
			if ($this->paymentDTO->getUserId() == $this->userId){

				// Set price
				$price = $this->paymentService->computeBaseAmount($reportId);
				$this->paymentDTO->setBaseAmount($price);

				/* Check if the payment is made for the report  */
				if (!$this->paymentDTO->getIsPaid()) {
					/* Calculate BASE vat output, vatable amount, total price, amount and then apply discount (adjusts amount, vat output, vatable amount), if any */
					$this->paymentService->getAmountAfterTaxAndDiscount($this->paymentDTO);

					/* After calculation, if total amount <= 0, tag user as free trial account and escape payment process */
					/* If Face Amount <= 0, user can proceed with completing the questionnaire */
					if ($this->paymentDTO->getTotalAmount() <= 0) {
						/* Run process for tagging Free Trial Accounts */
						$this->paymentService->togglePaymentFlags($this->userId, $reportId);
						/* Prompt user/developer can proceed to the dashboard */
						return $this->response(
							[
								null
							],
							"The user has a free trial account. Send a Reports Request to receive a list of reports under his account.",
							HTTP_STS_OK
						);
					} else {
						/* Save payment receipt based on data from transaction, Dragonpay parameters, and price calculation and then return Receipt ID
						*/
						$paypalDTO = $this->paypalService->recordPayPalTransaction($this->paymentDTO);

						/* Configure PayPal required parameters */
						$clientReturn = $returnUrl;
						$paypalDTO->setClientReturnUrl($clientReturn);
						$parameters = $this->paypalService->getPayPalParams($paypalDTO, $this->paymentDTO);
						
						/* Create an instance of OmniPay library used for PayPal Express */
						$gateway = Omnipay::create('PayPal_Express');
						
						/* Environment Configuration */
						$this->paypalService->environmentSetup($paypalDTO, $gateway);
						
						/* Send data to PayPal */
						$response = $gateway->purchase($parameters)->send();
						

						/* Get PayPal response and do action accordingly */
						if ($response->isRedirect()) {
							return $this->response(
								[
									"requestUrl" => $response->getRedirectUrl()
								],
								"Success. Redirect user to provided Request URL",
								HTTP_STS_OK
							);
						}
						else {
							return $this->response(
								[
									"error" => $response->getMessage()
								],
								"Error. Something went wrong with the PayPal Request.",
								HTTP_STS_INTERNALSERVERERROR
							);
						}
					}

				} else {
					return $this->error(
						"Payment has been made for this report", 
						HTTP_STS_BADREQUEST
					);
				}
			} else {
				return $this->error(
					"User is not owner of report", 
					HTTP_STS_FORBIDDEN
				);
			}
		} else {
			return $this->error(
				"Report does not exist.",
				HTTP_STS_BADREQUEST
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.g. User has completed a PayPal transaction
	//---------------------------------------------------------------
	function processPayPalTransaction($reference)
	{
		/* Create Omnipay and PaypalDTO instance */
		$gateway = Omnipay::create('PayPal_Express');
		$paypalDTO = new PaypalDTO;

		/* Setup Omnipay Config */
		$this->paypalService->environmentSetup($paypalDTO, $gateway);

		/* Get parameter data from database */
		$parameters = $this->paypalService->getParameterRecords($reference, $paypalDTO);

		/* Pass data to PayPal via Omnipay */
		$response = $gateway->completePurchase($parameters)->send();
		$paypalResponse = $response->getData();

		$paypalDTO->setResponseVars($paypalResponse);

		/* If PayPal was successful, */
		if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
			/* Add payment Record and note if transaction has been made before*/
			$isPaidBefore = $this->paypalService->getPaymentCount($paypalDTO, $this->userId);
			
			/* Update necessary records and send receipt via email */
			$isNewUser = $this->paypalService->sendReceipt($paypalDTO, $isPaidBefore);

			$data = array(
				'status'	=> "S",
				'message'	=> "Payment Successfull",
				'reportid' => $paypalDTO->getReport()
			);

			$url = $paypalDTO->getClientReturnUrl() . '/' . json_encode($data);

			return Redirect::to($url);

		} else {
			/* Else, show error message from PayPal */
            $error = $paypalResponse['L_LONGMESSAGE0'];

            return $this->respondInternalError(
				[
					"paypalerror" => $error
				]
			);
		}
	}

	//---------------------------------------------------------------
	// Function #.g. User cancelled PayPal transaction
	//---------------------------------------------------------------
	function cancelPayPalTransaction($reference)
	{
		/* Get Return URL sent by client */
		$paypalDTO = new PaypalDTO;
		$parameters = $this->paypalService->getParameterRecords($reference, $paypalDTO);

		return $this->response(
			[
				"clientReturn" => $paypalDTO->getClientReturnUrl()
			],
			"User has cancelled payment.",
			HTTP_STS_OK
		);
	} 
	public function paymentInvoice($reportId){
		
		try{

			$user = User::find(Auth::user()->loginid);
			if(($user->street_address == "" || $user->city == "" || $user->province == "" || $user->zipcode == "" || $user->phone == "" || $user->name == "")) {
					
				$error = array('Please complete your billing details before proceeding. These details will appear on your invoice after every purchase. Thank you.');
				
				if($user->role == 4) { //supervisor
					return Redirect::to('/bankbillingdetails')->withErrors($error)->withInput();
				} else { //basic user
					return Redirect::to('/billingdetails')->withErrors($error)->withInput();
				}
				
			}
			/* URL doesn't have report id parameter */
			if (!$reportId){
				/* Get the first unpaid report */
				$entity = Entity::where('entityid', $reportsId)->where('is_paid', 0)->first();
				$reportId = $entity->entityid;
			}
			$entity = Entity::where('entityid', $reportId);		
			$entity = $entity->first();
			// Check if report is paid already
			if($entity->is_paid == 1 && $entity->is_premium == PREMIUM_REPORT){
				$mailHandler = new MailHandler();
				$mailHandler->sendPremiumReportNotification($entity);				
				return Redirect::intended("/sme/premiumsuccess/" . $entity->entityid);
			}

	        /*  Report exists   */
			if ($entity) {
				/** Check if report has pending payment on dragonpay */
				//if($entity->is_paid == 0){
				//	$checkDragonpayPendingPayment = DB::table('tbltransactions')->where('entity_id', $entity->entityid)->where('status',  'P')->first();
					// if($checkDragonpayPendingPayment){
					// 	$apiUrl = $this->statusUrl . '/refno/' . $checkDragonpayPendingPayment->refno;
					// 	$client = new GuzzleClient([
					// 		'headers' => [
					// 			'Authorization' => 'Basic ' . base64_encode($this->merchant_id . ':' . $this->merchant_key)
					// 		]
					// 	]);

					// 	$r = $client->request('GET', $apiUrl);
					// 	$data = json_decode($r->getBody()->getContents());
					// 	if($data->Status == 'P'){
					// 	$data = array('refno' => $checkDragonpayPendingPayment->refno);
					// 		return $this->response(
					// 			$data,
					// 			"Payment Status Pending.",
					// 			HTTP_STS_OK
					// 		);
					// 	}elseif($data->Status == 'S'){ /** Check if report is paid, in case dragonpay over-the-counter payment updated immediately */
					// 		/** Update report Status to paid */
					// 		$entity->is_paid = 1;
					// 		$entity->save();

					// 		/** Update transaction status */
					// 		$this->transactionDb = new Transaction;
					// 		$this->transactionDb->updateTransactionStatus(PD_BASIC_USER_REPORT, $data->Status);

					// 		if($entity->is_premium == PREMIUM_REPORT){
					// 			$mailHandler = new MailHandler();
					// 			$mailHandler->sendPremiumReportNotification($entity);
					// 		}

					// 		return Redirect::to('/dashboard/index');
					// 	}
					// }
				//}

				/* Set price information */
				$paymentCalculator = new PaymentCalculator;
				$tax = 1;

				$bank = Bank::where('id', '=', $entity->current_bank)->first();
				$locPremiumReportPrice = Zipcode::where('id', $entity->cityid)->first();

				/** Calculate Report Price */
				$reportPrice = $paymentCalculator->paymentComputeAllReportType($reportId);

				// if($bank->is_custom != 0) {
				// 	if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
				// 		$price = $bank->custom_price;
				// 	} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
				// 		$price = $bank->custom_price;
				// 	} else { // Standlone Report
				// 		$price = $bank->custom_price;
				// 	}
				// } else {
				// 	if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
				// 		$price = $reportPrice['premium'];
	            //         $report_type = 'Premium';
				// 	} elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
				// 		$price = $reportPrice['simplified'];
	            //         $report_type = 'Simplified';
				// 	} else { // Standalone Report
						$price = 3500;
	                    $report_type = 'Standalone';
				//	}
			//	}

	            // if($entity->is_premium == PREMIUM_REPORT) { // Premium Report
	            //     $report_type = 'Premium';
	            // } elseif($entity->is_premium == SIMPLIFIED_REPORT){ // Simplified Report
	            //     $report_type = 'Simplified';
	            // } else { // Standlone Report
	            //     $report_type = 'Standalone';
	            // }
				/* Price and amount are same here since the quantity is 1 in purchasing report.
				But I'll still declare amount variable for using right terms during calculation */
				$amount = $price;
				$subTotal = $amount;
				$displayDiscount = null;
				$discount_value = 0;

	            /*  Check Discount  */
				$discount = Discount::where('discount_code', $entity->discount_code)->first();

				/*  There is a discount */
				if ($discount){
					if ($discount->type == $paymentCalculator::DISCOUNT_TYPE_PERCENTAGE){
						$displayDiscount = '(less: '. $discount->amount .'%)';
						$discount_value = $amount * ($discount->amount / 100);
					} else{
						$displayDiscount = '(less: '. number_format($discount->amount, 2, '.', ',').')';
					}
				}
				// $totalAmount = $paymentCalculator->computeTotalAmountBySubTotal($subTotal);
				$totalAmount = ($amount)*(1+1/100);
				
				/*set up display message*/
				$displayMessage = sprintf("Php %s %s (1 + %s%% tax) = Php %s*", number_format($amount,2), ($displayDiscount != null ? $displayDiscount : ''), ($tax * 1), number_format($totalAmount,2));
	            $report_details = [
	                'report_type' => $report_type,
	                'amount' => $amount,
	                'total_amount' => $totalAmount,
	                'tax_rate' => '1%',
	                'tax_amount' => ($tax * ($totalAmount)),
					'display_amount' => 'Match Rating Report®: ' . $displayMessage,
					'entity_id' => $reportId,
	                'user' => $user
	                
	            ];
				return $this->response(
					$report_details,
					"Payment Status.",
					HTTP_STS_OK
				);
				

			}
	        /*  Report does not exist   */
	        else {
				return Redirect::to('/');
			}
		}catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
		return $this->response(
			$reportsId,
			"Report has been successfully submitted and analysis will now begin. An email will be sent upon completion of the report in approximately 24 to 48 hours.",
			HTTP_STS_OK
		);
	}
}
