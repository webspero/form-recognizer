<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Services\FinancialRawFile\FinancialRawUpload;
use CreditBPO\Services\FinancialRawFile\FinancialRawList;
use CreditBPO\Services\FinancialRawFile\FinancialRawDelete;


use CreditBPO\Services\UserTokenService;
use CreditBPO\Controllers\API\v1\ApiController;
use Request;

class FinancialRawFileController extends ApiController
{
    public function __construct(){
        $this->rawUploadService = new FinancialRawUpload;
        $this->rawListService = new FinancialRawList;
        $this->userTokenService = new UserTokenService;
        $this->rawDeleteService = new FinancialRawDelete;
    }

    public function uploadFinancialRawFile($reportId){
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->rawUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /* Manage and validate request details */
        $fsUploadDto = $this->rawUploadService->manageRequestDetails(Request::all(), $reportId);

        if($fsUploadDto){
            $errors = $this->rawUploadService->validateRequestData($fsUploadDto, Request::all());

            if (!empty($errors)){
                return $this->error($errors, HTTP_STS_BADREQUEST);
            }
            /* If request doesn't have errors*/
            else{
                $rawFileUploadList = $this->rawUploadService->uploadFinancialRawFileList($fsUploadDto);
                return $this->response($rawFileUploadList, "Financial Raw File Successfully uploaded.", HTTP_STS_OK);
            }
        }

        /*Manage details to dto*/
        // uploadFinancialRawFile
        // $financialDtoList = $this->rawUploadService->manageRequestDetails(Request::all(), $reportId);

        // // return $this->error($financialDtoList, HTTP_STS_BADREQUEST);

        // if (!empty($financialDtoList)){
        //     /*Validate data request*/
        //     $errors = $this->rawUploadService->validateRequestData($financialDtoList);
        //     if (empty($errors)){
        //         $rawFileUploadList = $this->rawUploadService->uploadFinancialRawFileList($financialDtoList);
        //         return $this->response($rawFileUploadList, "Financial Raw File Successfully uploaded.", HTTP_STS_OK);
        //     }else{
        //         return $this->error($errors, HTTP_STS_BADREQUEST);
        //     }
        //     print_r("emee"); die();
        // } else{
        //     return $this->error("Financial Raw File is required.", HTTP_STS_BADREQUEST);
        // }
    }

    public function getFinancialRawFile($reportId){
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->rawUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }
        
        /*Validate report id if exist */
        $idExist = $this->rawListService->validateReportIfExist($reportId);

        if($idExist){
             /* Get Report Financial Raw File */
             $rawList = $this->rawListService->getRawFileList($reportId);
             if (!is_null($rawList)){
                 return $this->response($rawList, "Success", HTTP_STS_OK);
             } else{
                 return $this->response(null, "No Utility Bill found.", HTTP_STS_OK);
             }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function deleteRawFile($reportid,$id){
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
        /** Check valid report */
        $isReport = $this->rawDeleteService->validateReportOwner($userId, $reportid);
        if($isReport == null){
            /** Validate if innodata file exists */
            $validateInnoFile = $this->rawDeleteService->validateInnodataFile($id);
            if(empty($validateInnoFile)){
                /** Delete innodata file */
                $this->rawDeleteService->deleteInnoFile($id);
                return $this->response(null, "Document deleted successfully.", HTTP_STS_OK);
            }else{
                return $this->error($validateInnoFile, HTTP_STS_BADREQUEST);
            }
        }else{
            return $this->error(
                $isReport['errors'],
                $isReport['code']
            );
        }
    }
}