<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;

class UsersController extends ApiController {
    /**
     * @SWG\Get(
     *     path="/api/v1/users",
     *     @SWG\Response(response="200", description="A user resource"),
     *     @SWG\Response(response="400", description="A non-existing user resource")
     * )
     */
    public function get() {
        return [
            'Test' => 1,
        ];
    }
}
