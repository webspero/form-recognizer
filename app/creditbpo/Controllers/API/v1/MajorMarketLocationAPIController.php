<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\MajorMarketLocationCRUD;
use CreditBPO\DTO\MajorMarketLocationDTO;

class MajorMarketLocationAPIController extends ApiController
{
    public function __construct()
    {
        $this->majorMarketLocationCRUDService = new MajorMarketLocationCRUD();
    }

    public function getMajorMarketLocationList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->majorMarketLocationCRUDService->getMajorMarketLocationByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->majorMarketLocationCRUDService->getMajorMarketLocationByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getMajorMarketLocationDetails($marketLocationId)
    {
        $majorMarketLocationDto = $this->majorMarketLocationCRUDService->getMajorMarketLocationById($marketLocationId);

        if (!empty($majorMarketLocationDto)){
            return $this->response($majorMarketLocationDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postMajorMarketLocation($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->majorMarketLocationCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $majorMarketLocationDto = new MajorMarketLocationDTO();
            $majorMarketLocationDto->setVars($data);
            
            /* Validate request */
            $result = $this->majorMarketLocationCRUDService->validateMajorMarketLocationDetails($majorMarketLocationDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedMajorMarketLocationDto = $this->majorMarketLocationCRUDService->majorMarketLocationInsert($majorMarketLocationDto);
                if (!empty($savedMajorMarketLocationDto->getId())){
                    return $this->response($savedMajorMarketLocationDto->getVars(), "Major Market Location saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error Major Market Location.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateMajorMarketLocation($relatedCompaniesId)
    {
    	$data = request()->all();

        $majorMarketLocationDto = new MajorMarketLocationDTO();
        $majorMarketLocationDto->setVars($data);
        
        /* Validate request */
        $result = $this->majorMarketLocationCRUDService->validateMajorMarketLocationDetails($majorMarketLocationDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedMajorMarketLocationDto = $this->majorMarketLocationCRUDService->majorMarketLocationUpdate($majorMarketLocationDto);
            if (!empty($udpatedMajorMarketLocationDto->getId())){
                return $this->response($udpatedMajorMarketLocationDto->getVars(), "Major Market Location updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Major Market Location.");
            }
        	
        }
        
    }

    public function deleteMajorMarketLocation($marketLocationId)
    {
    	$data = request()->all();
       
        $deletedMajorMarketLocationDto = $this->majorMarketLocationCRUDService->majorMarketLocationDelete($marketLocationId);
        if (!empty($deletedMajorMarketLocationDto)){
            return $this->response([], "Major Market Location deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error Major Market Location.");
        }
        	
       
        
    }
}