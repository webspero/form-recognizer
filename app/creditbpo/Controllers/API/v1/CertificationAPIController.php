<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\CertificationCRUD;
use CreditBPO\DTO\CertificationDTO;

class CertificationAPIController extends ApiController
{
    public function __construct()
    {
        $this->certificationCRUDService = new CertificationCRUD();
    }

    public function getCertificationList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit'); 
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->certificationCRUDService->getCertificationByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->certificationCRUDService->getCertificationByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getCertificationDetails($certificationid)
    {

        $certificationDto = $this->certificationCRUDService->getCertificationById($certificationid);

        if (!empty($certificationDto)){
            return $this->response($certificationDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postCertification($reportId)
    {
    	$data = request()->all();
    	
        /* Check if report id exist */
        $reportExist = $this->certificationCRUDService->checkReportIdIfExist($reportId);
        
        if ($reportExist){
            $CertificationDto = new CertificationDTO();
            $CertificationDto->setVars($data);
            
            /* Validate request */
            $result = $this->certificationCRUDService->validateCertification($data);
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedCertificationDto = $this->certificationCRUDService->certificationInsert($data);
                if (!empty($savedCertificationDto->getId())){
                    return $this->response($savedCertificationDto->getVars(), "Certification saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error certification.");
                }
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateCertification($certificationid)
    {

    	$data = request()->all();

        $CertificationDto = new CertificationDTO();
        $CertificationDto->setVars($data);
        
        /* Validate request */
        $result = $this->certificationCRUDService->validateCertification($CertificationDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }

        /* Validation success, save data to database */
        else {

            $udpatedCertificationDto = $this->certificationCRUDService->certificationUpdate($CertificationDto);

            if (!empty($udpatedCertificationDto->getId())){
                return $this->response($udpatedCertificationDto->getVars(), "Certification updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error certification.");
            }
        	
        }
        
    }

    public function deleteCertification($certificationid)
    {
    	$data = request()->all();
       
        $deletedCertificationDto = $this->certificationCRUDService->certificationDelete($certificationid);
        if (!empty($deletedCertificationDto)){
            return $this->response([], "Certification deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error certification.");
        }
        	
       
        
    }
}