<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\MajorSupplier\MajorSupplierList;
use CreditBPO\Services\MajorSupplier\MajorSupplierInsert;
use CreditBPO\Services\MajorSupplier\MajorSupplierUpdate;
use CreditBPO\Services\MajorSupplier\MajorSupplierDelete;
use CreditBPO\DTO\MajorSupplierDTO;
use CreditBPO\Libraries\MajorSupplierValue;

class MajorSupplierAPIController extends ApiController
{
    public function __construct()
    {
        $this->majorSupplierListService = new MajorSupplierList();
        $this->majorSupplierInsertService = new MajorSupplierInsert();
        $this->majorSupplierUpdateService = new MajorSupplierUpdate();
        $this->majorSupplierDeleteService = new MajorSupplierDelete();
        $this->majorSupplierValueLibrary = new MajorSupplierValue();
    }

    public function getReportMajorSupplierList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->majorSupplierListService->getReportMajorSupplierByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {
            $result = $this->majorSupplierListService->getMajorSupplierList($reportId);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getMajorSupplierDetails($id)
    {
        $majorSupplierDto = $this->majorSupplierListService->getMajorSupplierById($id);
        if (!is_null($majorSupplierDto)){
            return $this->response($majorSupplierDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postMajorSupplier($reportId)
    {
        /* Check if report id exist */
        $reportExist = $this->majorSupplierInsertService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $majorSupplierDto = new MajorSupplierDTO();
            /* Populate Major Supplier DTO of request values */
            $majorSupplierDto->setVars(Request::all());
            $majorSupplierDto->setReportId($reportId);
            $majorSupplierDto->setYearsDoingBusiness(date('Y') - $majorSupplierDto->getYearStarted());

            /* Validate request */
            $result = $this->majorSupplierInsertService->validateMajorSupplierDetails($majorSupplierDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {
                $savedMajorSupplierDto = $this->majorSupplierInsertService->postReportMajorSupplier($majorSupplierDto, $result['anonymous']);
                if (!is_null($savedMajorSupplierDto->getId())){
                    return $this->response($savedMajorSupplierDto->getVars(), "Major supplier saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error saving major supplier.");
                }
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
        
    }

    public function putMajorSupplier($id)
    {
        /* Check if major supplier id exist then get report id*/
        $reportId = $this->majorSupplierUpdateService->getMajorSupplierReportId($id);
        if (!is_null($reportId)){
            $majorSupplierDto = new MajorSupplierDTO();
            /* Populate Major Supplier DTO of database and request values */
            $majorSupplierDto->setVars(Request::all());
            /* Validate data request */
            $validator = $this->majorSupplierUpdateService->validateRequestDetails($majorSupplierDto, $reportId);
            
            if (!empty($validator)){
                return $this->error($validator, HTTP_STS_BADREQUEST);
            } 
            /* Validation success, update data to database */
            else{
                $savedMajorSupplierDto = $this->majorSupplierUpdateService->updateReportMajorSupplier($id, $majorSupplierDto);
                return $this->response($savedMajorSupplierDto->getVars(), 'Major supplier updated successfully.', HTTP_STS_OK);
            }
        } 
        /* Major supplier doesn't exist */
        else{
            return $this->error("Major supplier doesn't exist!", HTTP_STS_BADREQUEST);
        }
    }
    
    public function deleteMajorSupplier($id)
    {
        $deleteResponse = $this->majorSupplierDeleteService->deleteMajorSupplier($id);
        if (!is_null($deleteResponse)){
            if ($deleteResponse){
                /* Used http-ok instead of http-nocontent to display deletion message */
                return $this->response(null, "Major supplier deleted successfully.", HTTP_STS_OK);
            } else{
                return $this->respondInternalError("Error occured while deleting major supplier.");
            }
        } else{
            return $this->error("Major supplier doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }
}