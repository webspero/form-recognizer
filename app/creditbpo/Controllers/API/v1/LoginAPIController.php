<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\AccountService;
use CreditBPO\Services\UserTokenService;
use CreditBPO\DTO\UserAccountDTO;
use CreditBPO\DTO\BillingDetailsDTO;
use Request;
use Auth;
use CreditBPO\Services\ResetPassword;

//======================================================================
//  Class #: API Login Controller
//  Functions related to API Logging in are routed to this class
//======================================================================
class LoginAPIController extends ApiController
{
    public function __construct()
    {
        $this->accountService = new AccountService();
        $this->resetPasswordService = new ResetPassword;
        $this->userTokenService = new UserTokenService();
        $this->userAccountDto = new UserAccountDTO();
        $this->billingDetailsDto = new BillingDetailsDTO();
    }

    //-----------------------------------------------------
    //  Function #: postLogin
    //  Validate account login credentials
    //-----------------------------------------------------
    public function postLogin() 
    {
        /** Check if user is valid to access CreditBPO API using header 'Client-Secret' and 'Client-Id' */
        $isAllowedRequest = $this->accountService->validateAPIUser(Request::header('Client-Id'), Request::header('Client-Secret'));

        if ($isAllowedRequest == false) {
            return $this->error("Please check your Client-ID and Secret Key to access CreditBPO API.", HTTP_STS_UNAUTHORIZED);
        } else {
            /* Get request header 'Authorization' */
            $authorization = Request::header('Authorization');

            if($authorization){
                /** Get encoded credentials */
                $removedBasicKeyword = str_replace(' ', '', trim($authorization, "Basic"));
                $decodedCredential = explode(":", base64_decode($removedBasicKeyword));

                if (@count($decodedCredential) == 2){
                    $userCredential = [
                        'email' => $decodedCredential[0],
                        'password' => $decodedCredential[1]
                    ];    
                }
                else{
                    return $this->error('Invalid authorization request.', HTTP_STS_BADREQUEST);
                }

                /* Populate User Account DTO of request values */
                $this->userAccountDto->setVars($userCredential);

                /** Request Data Validation */
                $validator = $this->accountService->validateLoginData($this->userAccountDto->getVars());
                if ($validator->fails()){
                    return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
                } else {
                    /*Check if there's existing reset password request */
                    $hasPasswordRequest = $this->resetPasswordService->checkPasswordRequest($this->userAccountDto->getEmail());
                    if ($hasPasswordRequest){
                        return $this->error('As per your previous request, you are required to change your password.', HTTP_STS_BADREQUEST);
                    }

                    $this->accountService->getUserByEmail($this->userAccountDto);

                    /** Check if account was locked */
                    if ($this->userAccountDto->getUserId() != null) {
                        $userAttemptDto = $this->accountService->getAttemptDetails($this->userAccountDto->getUserId());

                        if ((!is_null($userAttemptDto) && $userAttemptDto->getAttempts() < MAX_LOGIN_FAILED) || (is_null($userAttemptDto))){
                            /* Verify and login user credentials */
                            $loginSuccess = $this->accountService->verifyLoginCredentials($this->userAccountDto);
                            
                            $userAuthDto = null;
                            if ($loginSuccess){
                                /* Check first if activated */
                                if ($this->userAccountDto->getRole() == USER_BASIC_USER &&
                                    $this->userAccountDto->getStatus() == USER_STS_INACTIVE){
                                        Auth::logout();
                                        return $this->error(
                                            "Please activate your account.", 
                                            HTTP_STS_FORBIDDEN
                                        );
                                    }
    
                                /* Generate user auth tokens */
                                $tokens = $this->userTokenService->generateUserTokens();
                                $tokens->setClientId(Request::header('Client-Id'))
                                       ->setUserId($this->userAccountDto->getUserId());
                                $userAuthDto = $this->userTokenService->addUserTokens($tokens);
                            }
    
                            if ($loginSuccess && !is_null($userAuthDto)){
    
                                $userLoggedInDetails = [
                                    'userid' => $this->userAccountDto->getUserId(),
                                    'email' => $this->userAccountDto->getEmail(),
                                    'firstname' => $this->userAccountDto->getFirstName(),
                                    'lastname' => $this->userAccountDto->getLastName(),
                                    'accesstoken' => $userAuthDto->getAccessToken(),
                                    'refreshtoken' => $userAuthDto->getRefreshToken()
                                ];

                                /** Check if billing details is done */
                                $userBillingDetails = $this->accountService->getUserDetails($this->userAccountDto->getUserId());
                                if($userBillingDetails){
                                    if(($userBillingDetails->street_address == "" || $userBillingDetails->city == "" || $userBillingDetails->province == "" || $userBillingDetails->zipcode == "" || $userBillingDetails->phone == "" || $userBillingDetails->name == "")) {
                                        $userLoggedInDetails['billingdetailsLink'] = route('billingdetails.put');
                                        $success_message = "Successfully logged-in. Please fill-up first the Billing Details to create report.";
                                    }else{
                                        $success_message = "Successfully logged-in.";
                                    }
                                }
    
                                /* If Administrator */
                                /* For now, API should not be available to administrator */
                                if ($this->userAccountDto->getRole() == USER_ADMINISTRATOR){
                                    return $this->error(
                                        "User has an account type that is not allowed to use the service.",
                                        HTTP_STS_FORBIDDEN
                                    );
                                }
                                
                                /* If basic user successfully logged-in, delete user attempt status */
                                if (!is_null($userAttemptDto)){
                                    $this->accountService->resetLoginAttempt($userAttemptDto);
                                }
    
                                return $this->response(
                                    $userLoggedInDetails, 
                                    $success_message, 
                                    HTTP_STS_OK
                                );
                            } else if (!$loginSuccess){
                                /* Password is incorrect, add/update malicious attempt value */
                                $userAttemptDto = $this->accountService->recordAttemptStatus($this->userAccountDto, $userAttemptDto);
                                if ($userAttemptDto){
                                    if ($userAttemptDto->getAttempts() >= MAX_LOGIN_FAILED){
                                        return $this->error(
                                            "Your account was locked due suspected malicious attempts at log in. You may email CreditBPO at info@creditbpo.com to unlock.", 
                                            HTTP_STS_UNAUTHORIZED
                                        );
                                    } else{
                                        return $this->error(
                                            "Password is incorrect.", 
                                            HTTP_STS_UNAUTHORIZED
                                        );
                                    }
                                } else{
                                    /* Error occured while saving user attempts */
                                    return $this->respondInternalError("Error occured while saving user attempts.");
                                }
                            } else{
                                /* Error occured while saving user tokens */
                                return $this->respondInternalError("Error occured while saving user tokens.");
                            }
                        } else{
                            return $this->error(
                                "Your account has been locked due suspected malicious attempts at log in. You may email CreditBPO at info@creditbpo.com to unlock.", 
                                HTTP_STS_UNAUTHORIZED
                            );
                        }    
                    } else {
                        return $this->error(
                            "User does not exist.", 
                            HTTP_STS_UNAUTHORIZED
                        );
                    }
                }
            } else {
                return $this->error(
                    "Please check your authorization request.",
                    HTTP_STS_BADREQUEST 
                );
            }
        }
    }

    //-----------------------------------------------------
    //  Function #: postActivateAccount
    //  Activate user account with activation code
    //-----------------------------------------------------
    public function postActivateAccount($activationCode)
    {
        /* Get request header 'Authorization'*/
        $authorization = Request::header('Authorization');

        /* If authorization is not empty */
        if (!is_null($authorization)){
            /* get the encoded credentials */
            $removedBasicKeyword = str_replace(' ', '', trim($authorization, "Basic"));
            $decodedCredential = explode(":", base64_decode($removedBasicKeyword));
            if (@count($decodedCredential) == 2){
                $userCredential = [
                    'email' => $decodedCredential[0],
                    'password' => $decodedCredential[1]
                ];  
                
                if (!Auth::attempt($userCredential)){
                    return $this->error(
                        "Please check your authorization request.",
                        HTTP_STS_BADREQUEST 
                      );
                }
            }
            else{
                return $this->error('Invalid authorization request.', HTTP_STS_BADREQUEST);
            }

            /* Populate User Account DTO of request values */
            $this->userAccountDto->setVars($userCredential);
            $this->userAccountDto->setActivationCode($activationCode);

            /* Request Data Validation */
            $validator = $this->accountService->validateActivationData($this->userAccountDto->getVars());
            if ($validator->fails()){
                return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
            } else {
                $user = $this->accountService->verifyAccountActivation($this->userAccountDto);
                if (!is_null($user)){
                    /** Check if account is already activated */
                    $activateSucess = $this->accountService->updateAccountActivation($this->userAccountDto, USER_STS_PAID);
                    return $this->response([
                        'email' => $this->userAccountDto->getEmail(),
                        'link'  => route('login.post')
                    ], "Account activation is succesfull. Please login to continue.", HTTP_STS_OK);
                
                } else{
                    $user_details = $this->accountService->getUserDetails($this->userAccountDto);
                    if($user_details){
                        return $this->response([
                            'email' => $this->userAccountDto->getEmail(),
                            'link'  => route('login.post')
                        ], "Account already active. Please proceed to login", HTTP_STS_OK);
                    }
                    return $this->error("Activation failed. Please check your request details.", HTTP_STS_BADREQUEST);
                }
            }
        } else {
            return $this->error(
              "Please check your authorization request.",
              HTTP_STS_BADREQUEST 
            );
        }
    }
    
    //-----------------------------------------------------
    //  Function #: updateBillingDetails
    //  Create or Update Basic User Billing Details
    //-----------------------------------------------------
    public function updateBillingDetails(){
        $data = Request::all();

        /** Check Request Body */
        if ($data) {
            /** Validate request data */
            $validateRequest = $this->accountService->validateBillingDetails($data);
            if ($validateRequest->fails()){
                return $this->error($validateRequest->messages()->all(), HTTP_STS_BADREQUEST);
            }

            $this->billingDetailsDto->setUserId($this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId());

            /** Request Body */
            $userBillingDetails = $this->accountService->getUserDetails($this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId());

            if($userBillingDetails){
                if(($userBillingDetails->street_address == "" || $userBillingDetails->city == "" || $userBillingDetails->province == "" || $userBillingDetails->zipcode == "" || $userBillingDetails->phone == "" || $userBillingDetails->name == "")) {
                    $message = "Billing Details created successfully! You can now create reports.";
                }else{

                    $message = "Billing Details updated succesfully! You can now create reprorts.";
                }
            }

             /** Set Billing Details Value */
             $this->billingDetailsDto->setVars($data);

             /** Save Billing Details */
             $this->accountService->saveBillingDetails($this->billingDetailsDto->getVars());
 
             return $this->response([
                 'link'  => route('reports.post')
                ], $message, HTTP_STS_OK);
        } else {
            return $this->error(
                "Request body is empty.", 
                HTTP_STS_BADREQUEST);
        }
    }

}
