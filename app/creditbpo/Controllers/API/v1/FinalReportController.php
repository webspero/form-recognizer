<?php

namespace CreditBPO\Controllers\API\v1;
// namespace Http\Controllers\PrintSummaryController;


use Request;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Services\FinalReportService;
use CreditBPO\Services\IndustryComparison;
use CreditBPO\Services\Report\Charts\FinancialCondition;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Models\Province2;
use CreditBPO\Models\GRDP;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use DB;
class FinalReportController extends ApiController
{

	public function __construct(){
		$this->userTokenService = new UserTokenService;
		$this->finalReportService = new FinalReportService;
		$this->financialCondition = new FinancialCondition;
		$this->industryComparison = new IndustryComparison;
	}

	public function getCbpoPdf($reportId){
		//if(!empty(Request::header('Authorization'))){
			$userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

				/*Check if report is finish*/
				$validation = $this->finalReportService->validateReport($reportId, $userId);

				if($validation == null){
						/** Download Rating Report + Financial Analysis */
						$file = $this->finalReportService->downloadFinalReport($reportId);

						if($file){
								/*  Sets header to PDF  */
								$headers = [
									'Content-Type: application/pdf',
								];
								return $this->download($file, $reportId . ' - Rating Summary Report', $headers);
						}else{

								return $this->respondInternalError('Rating Report does not exist.');
						}
				}else{
						return $this->error(
						$validation['errors'],
						$validation['code']
					);
				}

		//}else{
		//  return $this->error("The user is not authorized to access report.", HTTP_STS_BADREQUEST);
		//}
	}
	public function getIndustryComparison($reportId){
		//if(!empty(Request::header('Authorization'))){
			$userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();
          
				/*Check if report is finish*/
				$validation = $this->finalReportService->validateReport($reportId, $userId);
				
				if($validation == null){
						/** get Industry Comparison data */
						$industryComparison = $this->getIndustryComparisons($reportId);
					
						if($industryComparison){
								/*  Attach report Id with Industry Comparison  data*/
								
								return $this->response(
								    $industryComparison,
									"Industry comparison data",
									HTTP_STS_OK
								);
						}else{

							return $this->error(
								"Industry comparison data not found", 
								HTTP_STS_BADREQUEST
							);
						}
				}else{
						return $this->error(
						$validation['errors'],
						$validation['code']
					);
				}

		//}else{
		//  return $this->error("The user is not authorized to access report.", HTTP_STS_BADREQUEST);
		//}
	}
	public function getIndustryComparisons($id){
			        /*  Get Report Information  */
					$entity = DB::table('tblentity')
					->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
					->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
					->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
					->leftJoin('tblbank', 'tblentity.current_bank', '=', 'tblbank.id')
					->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
					->leftJoin('refregion', 'refcitymun.regDesc', 'refregion.regCode')
					->where('tblentity.entityid', '=', $id)->first();
				
		  /*-------------------------------------------
			--------Industry Comparison Line Graph-------
	        ---------------------------------------------*/

			$industryComaprison = new IndustryComparison();
			$industryData = $industryComaprison->getIndustryComparison($id);

	        /** Set Value for Industry Line Graph */
	        $industry_gross_revenue_growth = $industryData['gross_revenue_growth'];
	        $industry_net_income_growth    = $industryData['net_income_growth'];
	        $industry_gross_profit_margin  = $industryData['gross_profit_margin'];
	        $industry_current_ratio        = $industryData['current_ratio'];
	        $industry_debt_equity_ratio    = $industryData['debt_equity_ratio'];
			$industry_year 				  = $industryData['year'];

			/** Get GRDP Values */
			$regionCode = Province2::where('provDesc', $entity->province)->pluck('regCode')->first();
			$industryGrdp = GRDP::where('regCode', $regionCode)->where('year', $industry_year)->first();

			if(empty($industryGrdp)){
				$GrdpYear = GRDP::where('regCode', $regionCode)->pluck('year');

				/** Get the closest year */
				$closest = null;
				foreach ($GrdpYear as $item) {
					if ($closest === null || abs((int)$industry_year - $closest) > abs($item - (int)$industry_year)) {
						$closest = $item;
					}
				}
				$industryGrdp = GRDP::where('regCode', $regionCode)->where('year', $closest)->first();
			}

			/*  Debt Ratio Calculator  */
			$fa_report = FinancialReport::where(['entity_id' => $entity->entityid, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
	        if($fa_report != null && $fa_report != '' && !empty($fa_report)){

	            $financial = new FinancialReport();
	            $financialStatement = $financial->getFinancialReportById($id);

		        $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
	            $fa_report->income_statements   = $financialStatement['income_statements'];
		        $fa_report->cashflow            = $financialStatement['cashflow'];

	            /*---------------------------------------------------------------
	            --------Computation for the Industry Comparison (Company) -------
	            -----------------------------------------------------------------*/
	            $company_industry = array();

	            $max_count = count($fa_report->income_statements)-1;

	            /** Company Gross Revenue Growth */
	            $company_industry['gross_revenue_growth'] = 0;
	            if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
					$PercentageChange = 0;
	                $company_industry['gross_revenue_growth'] = 0;
	                $dividend = $fa_report->income_statements[$max_count]->Revenue;

					if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
                        $PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
                    }

					if($PercentageChange > 200){
						$company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
					}else{
						$company_industry['gross_revenue_growth'] = round($PercentageChange,2);
					}
	            }

	            /** Company Net Income Growth */
	            $company_industry['net_income_growth'] = 0;
	            if( isset($fa_report->income_statements[$max_count]->ProfitLoss) && (($fa_report->income_statements[$max_count]->ProfitLoss) != 0)){
					$PercentageChange = 0;
	                $dividend = ($fa_report->income_statements[$max_count]->ProfitLoss);

					if($dividend != 0  && $fa_report->income_statements[$max_count]->ProfitLoss != 0){
                        $PercentageChange = (($fa_report->income_statements[0]->ProfitLoss - $fa_report->income_statements[$max_count]->ProfitLoss) /  $dividend) * 100;
                    }

					if($PercentageChange > 200){
						$company_industry['net_income_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
					}else{
						$company_industry['net_income_growth'] = round($PercentageChange,2);
					}
	            }

	            /** Company Gross Profit Margin */
	            $company_industry['gross_profit_margin'] = 0;
				if($max_count >= 0){
					for($x=$max_count; $x >= 0 ; $x--){
						if($fa_report->income_statements[$x]->Revenue != 0){
							$company_industry['gross_profit_margin'] = ($fa_report->income_statements[$x]->GrossProfit / $fa_report->income_statements[$x]->Revenue) * 100;

							if($company_industry['gross_profit_margin'] == 0){
								$company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
							}else{
								$company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
								$company_industry['gross_profit_margin'] = (int)$company_industry['gross_profit_margin'];
							}
							
						}
					}
				}

	            /** Company Current Ratio */    
	            $company_industry['current_ratio'] = 0;
	            $i=0;
	            for($x=@count($fa_report->balance_sheets) - 1; $x >= 0 ; $x--){
	                $currentAssets = $fa_report->balance_sheets[$x]->CurrentAssets;
	                $currentLiabilities = $fa_report->balance_sheets[$x]->CurrentLiabilities;

	                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
	                if($currentLiabilities > 0){
	                    $company_industry['current_ratio'] = $currentAssets / $currentLiabilities;
	                    $company_industry['current_ratio'] = round($company_industry['current_ratio'], 2, PHP_ROUND_HALF_DOWN);
	                }
	                $i++;
	            }

	            /** Company Debt-to-Equity Ratio */
	            $company_industry['debt_to_equity'] = 0;
	            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
	            $company_industry['debt_to_equity'] = $sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage;
				return array(
					'company_debt_to_equity' =>  $company_industry['debt_to_equity'],
					'company_current_ratio' =>  $company_industry['current_ratio'],
					'company_gross_profit_margin' =>  $company_industry['gross_profit_margin'],
					'company_net_income_growth' =>  $company_industry['net_income_growth'],
					'company_gross_revenue_growth' =>  $company_industry['gross_revenue_growth'],
					'industry_gross_revenue_growth' => $industryData['gross_revenue_growth'],
					'industry_net_income_growth'    => $industryData['net_income_growth'],
					'industry_gross_profit_margin'  => $industryData['gross_profit_margin'],
					'industry_current_ratio'        => $industryData['current_ratio'],
					'industry_debt_equity_ratio'    => $industryData['debt_equity_ratio'],
					'industry_year'				  => $industryData['year'],
					'reportsId'				  => $id,
					'industryGrdp'			=> $industryGrdp->grdp * 100
		   );
			}
  
	}
}
