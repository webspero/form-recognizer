<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use Request;
use CreditBPO\Services\BankStatement\BankStatementUpload;
use CreditBPO\Services\BankStatement\BankStatementList;
use CreditBPO\Services\UtilityBill\UtilityBillUpload;
use CreditBPO\Services\UtilityBill\UtilityBillList;
use CreditBPO\Services\UserTokenService;

class CashFlowProxyAPIController extends ApiController
{
    public function __construct()
    {
        $this->bsUploadService = new BankStatementUpload;
        $this->bsListService = new BankStatementList;
        $this->ubUploadService = new UtilityBillUpload;
        $this->ubListService = new UtilityBillList;
        $this->userTokenService = new UserTokenService;
    }

    #region [Bank Statements]
    public function uploadBankStatements($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->ubUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /*Manage details to dto*/
        $bsUploadDtoList = $this->bsUploadService->manageRequestDetails(Request::all(), $reportId);
        if (!empty($bsUploadDtoList)){
            /*Validate data request*/
            $errors = $this->bsUploadService->validateRequestData($bsUploadDtoList);
            if (empty($errors)){
                $bsUploadList = $this->bsUploadService->uploadBankStatements($bsUploadDtoList);
                return $this->response($bsUploadList, "Bank statements successfully saved.", HTTP_STS_OK);
            } else{
                return $this->error($errors, HTTP_STS_BADREQUEST);
            }
        } else{
            return $this->error("Bank statement is required.", HTTP_STS_BADREQUEST);
        }
    }

    public function getReportBankStatements($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->ubUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }
        
        /*Validate report id if exist */
        $idExist = $this->bsListService->validateReportIfExist($reportId);
        if ($idExist){
            /* Get Report Bank Statement documents */
            $bsList = $this->bsListService->getReportBSList($reportId);
            if (!is_null($bsList)){
                return $this->response($bsList, "Success", HTTP_STS_OK);
            } else{
                return $this->response(null, "No Bank Statement found.", HTTP_STS_OK);
            }
        } else {
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }
    #endregion

    #region [Utility Bills]
    public function uploadUtilityBills($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->ubUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /*Manage details to dto*/
        $ubUploadDtoList = $this->ubUploadService->manageRequestDetails(Request::all(), $reportId);
        if (!empty($ubUploadDtoList)){
            /*Validate data request*/
            $errors = $this->ubUploadService->validateRequestData($ubUploadDtoList);
            if (empty($errors)){
                $ubUploadList = $this->ubUploadService->uploadUtilityBills($ubUploadDtoList);
                return $this->response($ubUploadList, "Utility Bills successfully saved.", HTTP_STS_OK);
            } else{
                return $this->error($errors, HTTP_STS_BADREQUEST);
            }
        } else{
            return $this->error("Utility Bill is required.", HTTP_STS_BADREQUEST);
        }
    }

    public function getReportUtilityBills($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->ubUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /*Validate report id if exist */
        $idExist = $this->ubListService->validateReportIfExist($reportId);
        if ($idExist){
            /* Get Report Utility Bills documents */
            $ubList = $this->ubListService->getReportUBList($reportId);
            if (!is_null($ubList)){
                return $this->response($ubList, "Success", HTTP_STS_OK);
            } else{
                return $this->response(null, "No Utility Bill found.", HTTP_STS_OK);
            }
        } else {
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }
    #endregion
}