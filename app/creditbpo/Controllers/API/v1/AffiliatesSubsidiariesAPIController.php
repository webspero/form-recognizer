<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\AffiliatesSubsidiariesCRUD;
use CreditBPO\DTO\AffiliateSubsidiariesDTO;

class AffiliatesSubsidiariesAPIController extends ApiController
{
    public function __construct()
    {
        $this->affiliatesSubsidiariesCRUDService = new AffiliatesSubsidiariesCRUD();
    }

    public function getAffiliateSubsidiariesList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        $result = [];
        if ($page && $limit){
            $result = $this->affiliatesSubsidiariesCRUDService->getAffiliateSubsidiariesByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->affiliatesSubsidiariesCRUDService->getAffiliateSubsidiariesByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getAffiliateSubsidiariesDetails($relatedCompaniesId)
    {
        $affiliatesSubsidiariesDto = $this->affiliatesSubsidiariesCRUDService->getAffiliateSubsidiariesById($relatedCompaniesId);

        //print_r($affiliatesSubsidiariesDto); exit;

        if (!empty($affiliatesSubsidiariesDto)){
            return $this->response($affiliatesSubsidiariesDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postAffiliatesSubsidiaries($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->affiliatesSubsidiariesCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $affiliatesSubsidiariesDto = new AffiliateSubsidiariesDTO();
            $affiliatesSubsidiariesDto->setVars($data);
            
            /* Validate request */
            $result = $this->affiliatesSubsidiariesCRUDService->validateAffiliatesSubsidiariesDetails($affiliatesSubsidiariesDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedaffiliatesSubsidiariesDto = $this->affiliatesSubsidiariesCRUDService->affiliateSubsidiariesInsert($affiliatesSubsidiariesDto);
                if (!empty($savedaffiliatesSubsidiariesDto->getId())){
                    return $this->response($savedaffiliatesSubsidiariesDto->getVars(), "Affiliates/Subsidiaries saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error affiliate subsidiaries.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateAffiliatesSubsidiaries($relatedCompaniesId)
    {
    	$data = request()->all();

        $affiliatesSubsidiariesDto = new AffiliateSubsidiariesDTO();
        $affiliatesSubsidiariesDto->setVars($data);
        
        /* Validate request */
        $result = $this->affiliatesSubsidiariesCRUDService->validateAffiliatesSubsidiariesDetails($affiliatesSubsidiariesDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedAffiliatesSubsidiariesDto = $this->affiliatesSubsidiariesCRUDService->affiliateSubsidiariesUpdate($affiliatesSubsidiariesDto);
            if (!empty($udpatedAffiliatesSubsidiariesDto->getId())){
                return $this->response($udpatedAffiliatesSubsidiariesDto->getVars(), "Affiliates/Subsidiaries updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error affiliate subsidiaries.");
            }
        	
        }
        
    }

    public function deleteAffiliatesSubsidiaries($relatedCompaniesId)
    {
    	$data = request()->all();
       
        $deletedAffiliatesSubsidiariesDto = $this->affiliatesSubsidiariesCRUDService->affiliateSubsidiariesDelete($relatedCompaniesId);
        if (!empty($deletedAffiliatesSubsidiariesDto)){
            return $this->response([], "Affiliates/Subsidiaries deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error affiliate subsidiaries.");
        }
        	
       
        
    }
}