<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use Request;
use CreditBPO\Services\AccountService;
use CreditBPO\Libraries\UseAgreement;
use CreditBPO\DTO\UserAccountDTO;
use CreditBPO\DTO\UserReportDTO;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//======================================================================
//  Class #: API Signup Controller
//  Functions related to API signing in are routed to this class
//======================================================================
class SignupAPIController extends ApiController
{

    public function __construct()
    {
        $this->accountService = new AccountService();
        $this->userReportDto = new UserReportDTO();
    }

	//-----------------------------------------------------
    //  Function #: getPrivacyPolicy
    //  Returns the system Privacy Policy
    //-----------------------------------------------------
    public function getPrivacyPolicy() 
    {
        $privacyPolicy = UseAgreement::getPrivacyPolicy();
        return $this->response($privacyPolicy, 'Success', HTTP_STS_OK);
    }

    //-----------------------------------------------------
    //  Function #: getTermsOfUse
    //  Returns the system Terms of Use
    //-----------------------------------------------------
    public function getTermsOfUse() 
    {
        $termsOfUse = UseAgreement::getTermsOfUse();
        return $this->response($termsOfUse, 'Success', HTTP_STS_OK);
    }

    //-----------------------------------------------------
    //  Function #: getStandaloneOrganizationList
    //  Returns the list of organizations with standalone report
    //-----------------------------------------------------
    public function getStandaloneOrganizationList()
    {
        $page = Request::query('page');
        $limit = Request::query('limit');
        
        if ($page && $limit){
            $result = $this->accountService->getStandaloneOrganizationByPage($page, $limit);
        } else{
            $result = $this->accountService->getStandaloneOrganization();
        }

        if (empty($result['items'])){
            return $this->response(null, 'No results found.', HTTP_STS_OK);
        } else{
            return $this->response($result, 'Success', HTTP_STS_OK);
        }
    }
    
    //-----------------------------------------------------
    //  Function #: postSignup
    //  Saves new account details and create first report
    //-----------------------------------------------------
    public function postSignup()
    {
        /** Get request header 'Client-Secret' and 'Client-Id'*/
        $clientSecret = Request::header('Client-Secret');
        $clientId = Request::header('Client-Id');

        /** Check if user is valid to access CreditBPO API */
        $isAllowedRequest = $this->accountService->validateAPIUser($clientId, $clientSecret);

        if ($isAllowedRequest == false) {
            return $this->error("Incorrect CreditBPO API Credentials. Please check your Client-ID and Secret Key", HTTP_STS_UNAUTHORIZED);
        } else {
            /** Populate User Report DTO of request values */
            $this->userReportDto->setVars(Request::all());

            /** Request Data Validation */
            $validator = $this->accountService->validateSignupData($this->userReportDto->getVars());

            if ($validator->fails()) {
                /** Return bad request with validation messages */
                return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
            } else {
                /** Save user details */
                $this->accountService->createUser($this->userReportDto);

                /** Success Response */
                return $this->response(
                    $this->userReportDto->getUserReportResponse(),
                    "Successfully registered a new account.",
                    HTTP_STS_CREATED
                );
            }
        }
    }
}
