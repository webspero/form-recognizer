<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\AssociatedFilesCRUD;
use CreditBPO\DTO\AssociatedFilesDTO;

class AssociatedFilesAPIController extends ApiController
{
    public function __construct()
    {
        $this->associatedFilesCRUDService = new AssociatedFilesCRUD();
    }

    public function getAssociatedFilesList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit'); 
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->associatedFilesCRUDService->getAssociatedFilesByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->associatedFilesCRUDService->getAssociatedFilesByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getAssociatedFileDetails($id)
    {

        $associatedFilesDto = $this->associatedFilesCRUDService->getAssociatedFileById($id);

        if (!empty($associatedFilesDto)){
            return $this->response($associatedFilesDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postAssociatedFiles($reportId)
    {
    	$data = request()->all();
    	
        /* Check if report id exist */
        $reportExist = $this->associatedFilesCRUDService->checkReportIdIfExist($reportId);
        
        if ($reportExist){
            $associatedFilesDto = new AssociatedFilesDTO();
            $associatedFilesDto->setVars($data);
            
            /* Validate request */
            $result = $this->associatedFilesCRUDService->validateAssociatedFiles($data);
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedAssociatedFilesDto = $this->associatedFilesCRUDService->associatedFilesInsert($data);
                if (!empty($savedAssociatedFilesDto->getId())){
                    return $this->response($savedAssociatedFilesDto->getVars(), "Associated Files saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error certification.");
                }
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateAssociatedFile($associatedFilesid)
    {

    	$data = request()->all();

        $associatedFilesDto = new AssociatedFilesDTO();
        $associatedFilesDto->setVars($data);
        
        /* Validate request */
        $result = $this->associatedFilesCRUDService->validateAssociatedFiles($associatedFilesDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }

        /* Validation success, save data to database */
        else {

            $udpatedAssociatedFilesDto = $this->associatedFilesCRUDService->associatedFilesUpdate($associatedFilesDto);

            if (!empty($udpatedAssociatedFilesDto->getId())){
                return $this->response($udpatedAssociatedFilesDto->getVars(), "Associated Files updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Associated Files.");
            }
        	
        }
        
    }

    public function deleteAssociatedFile($id)
    {
    	$data = request()->all();
       
        $deletedAssociatedFilesDto = $this->associatedFilesCRUDService->associatedFilesDelete($id);
        if (!empty($deletedAssociatedFilesDto)){
            return $this->response([], "Associated Files deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error Associated Files.");
        }
        	
       
        
    }
}