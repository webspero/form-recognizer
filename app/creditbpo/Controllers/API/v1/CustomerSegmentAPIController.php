<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\CustomerSegmentCRUD;
use CreditBPO\DTO\CustomerSegmentDTO;

class CustomerSegmentAPIController extends ApiController
{
    public function __construct()
    {
        $this->customerSegmentCRUDService = new CustomerSegmentCRUD();
    }

    public function getCustomerSegmentList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->CustomerSegmentCRUDService->getAffiliateSubsidiariesByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->customerSegmentCRUDService->getCustomerSegmentByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getCustomerSegmentDetails($businessSegmentId)
    {
        $customerSegmentDto = $this->customerSegmentCRUDService->getCustomerSegmentById($businessSegmentId);

        if (!empty($customerSegmentDto)){
            return $this->response($customerSegmentDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postCustomerSegment($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->customerSegmentCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $customerSegmentDto = new CustomerSegmentDTO();
            $customerSegmentDto->setVars($data);
            
            /* Validate request */
            $result = $this->customerSegmentCRUDService->validateCustomerSegmentDetails($customerSegmentDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedCustomerSegmentDto = $this->customerSegmentCRUDService->customerSegmentInsert($customerSegmentDto);
                if (!empty($savedCustomerSegmentDto->getId())){
                    return $this->response($savedCustomerSegmentDto->getVars(), "CustomerSegment saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error CustomerSegment.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateCustomerSegment($businessSegmentId)
    {
    	$data = request()->all();

        $customerSegmentDto = new CustomerSegmentDTO();
        $customerSegmentDto->setVars($data);
        
        /* Validate request */
        $result = $this->customerSegmentCRUDService->validateCustomerSegmentDetails($customerSegmentDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedCustomerSegmentDto = $this->customerSegmentCRUDService->customerSegmentUpdate($customerSegmentDto);
            if (!empty($udpatedCustomerSegmentDto->getId())){
                return $this->response($udpatedCustomerSegmentDto->getVars(), "Customer Segment updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error Customer Segment.");
            }
        	
        }
        
    }

    public function deleteCustomerSegment($businessSegmentId)
    {
    	$data = request()->all();
       
        $deletedCustomerSegmentDto = $this->customerSegmentCRUDService->customerSegmentDelete($businessSegmentId);
        if (!empty($deletedCustomerSegmentDto)){
            return $this->response([], "Customer Segment deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error Customer Segment.");
        }
        	
       
        
    }
}