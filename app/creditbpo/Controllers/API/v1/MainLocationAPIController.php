<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\MainLocationCRUD;
use CreditBPO\DTO\MainLocationDTO;

class MainLocationAPIController extends ApiController
{
    public function __construct()
    {
        $this->mainLocationCRUDService = new MainLocationCRUD();
    }

    public function getMainLocationList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->mainLocationCRUDService->getMainLocationByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->mainLocationCRUDService->getMainLocationByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getMainLocationDetails($locationId)
    {
        $mainLocationDto = $this->mainLocationCRUDService->getMainLocationById($locationId);

        if (!empty($mainLocationDto)){
            return $this->response($mainLocationDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postMainLocation($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->mainLocationCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $mainLocationDto = new MainLocationDTO();
            $mainLocationDto->setVars($data);
            
            /* Validate request */
            $result = $this->mainLocationCRUDService->validateMainLocationDetails($mainLocationDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedMainLocationDto = $this->mainLocationCRUDService->mainLocationInsert($mainLocationDto);
                if (!empty($savedMainLocationDto->getId())){
                    return $this->response($savedMainLocationDto->getVars(), "Main Location saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error main location.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateMainLocation($locationId)
    {
    	$data = request()->all();

        $mainLocationDto = new MainLocationDTO();
        $mainLocationDto->setVars($data);
        
        /* Validate request */
        $result = $this->mainLocationCRUDService->validateMainLocationDetails($mainLocationDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedMainLocationDto = $this->mainLocationCRUDService->MainLocationUpdate($mainLocationDto);
            if (!empty($udpatedMainLocationDto->getId())){
                return $this->response($udpatedMainLocationDto->getVars(), "Main Location updated successfully.", HTTP_STS_CREATED);
            } else{
                return $this->respondInternalError("Error main location.");
            }
        	
        }
        
    }

    public function deleteMainLocation($locationId)
    {
    	$data = request()->all();
       
        $deletedMainLocationDto = $this->mainLocationCRUDService->mainLocationDelete($locationId);
        if (!empty($deletedMainLocationDto)){
            return $this->response([], "Main Location deleted successfully.", HTTP_STS_CREATED);
        } else{
            return $this->respondInternalError("Error affiliate subsidiaries.");
        }
    }
}