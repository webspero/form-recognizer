<?php

namespace CreditBPO\Controllers\API\v1;

use Response;
use CreditBPO\Controllers\BaseController;
use Request;

/**
 * @SWG\Info(title="CreditBPO Api", version="1.0")
 */
class ApiController extends BaseController
{
    public function response($data, $messages, $status)
    {
        return Response::json([
            'data'=> $data, 
            'messages'=> $messages],
            $status);
    }

    public function download($file, $fileName, $headers = [])
    {
        return Response::download(
            $file,
            $fileName,
            $headers
        );
    }

    /**
     * @param array $details
     * @param string $status 
     * @return Response
     */
    public function error($details, $status)
    {
        /* Check if $details is object and transforms it into an array */
        if (is_object($details)){ 
            $details = $details->toArray(); 
        }

        switch ($status) {
            case HTTP_STS_BADREQUEST:
                $message = "The server cannot or will not process the request due to an error in the request.";
                break;

            case HTTP_STS_UNAUTHORIZED:
                $message = "The request made lacks needed credentials.";
                break;
            
            case HTTP_STS_FORBIDDEN:
                $message = "The request was valid, but it is not authorized to receive the correct response.";
                break;

            default:
                $message = 'An error occured.';
                break;
        }

        $error = array(
                'message' => $message,
                'details' => $details
            );

        return Response::json([
            'error' => $error
            ], $status);
    }

    public function respondInternalError($details)
    {
        /* Check if $details is object and transforms it into an array */
        if (is_object($details)){ 
            $details = $details->toArray(); 
        }

        $error = array(
            'message' => "An unexpected error occured.",
            'details' => $details
        );
        
        return Response::json([
            'error' => $error
            ], 500);
    }

    public function respondNotFoundException()
    {
        $error = array(
            'message' => "The requested resource could not be found.",
            'details' => "Please check your request."
        );
        
        return Response::json([
            'error' => $error
            ], 404);
    }
}
