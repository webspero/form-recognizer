<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;

class SampleApiController extends ApiController {
    /**
     * @SWG\Get(
     *     path="/api/v1/sample-api",
     *     @SWG\Response(response="200", description="An example resource")
     * )
     */
    public function get() {
        return $this->json([
            'test' => 'Sample data',
        ]);
    }

    /**
     *
     */
    public function post() {

    }

    /**
     *
     */
    public function put() {

    }

    /**
     *
     */
    public function delete() {

    }
}
