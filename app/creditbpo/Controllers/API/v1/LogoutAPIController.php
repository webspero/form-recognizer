<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use Request;
use CreditBPO\Services\UserTokenService;
use Auth;

class LogoutAPIController extends ApiController
{
    public function __construct()
    {
        $this->userTokenService = new UserTokenService;
    }

    public function postLogout() 
    {
        /*Get request header access token*/
        $authorizationHeader = Request::header('Authorization');
        $accessToken = str_replace(' ', '', trim($authorizationHeader, "Bearer"));

        /*Logout current user/token*/
        $result = $this->userTokenService->removeToken($accessToken);
        if ($result){
            return $this->response(null, "You have successfully logged out.", HTTP_STS_OK);
        } else{
            return $this->error("Invalid token details.", HTTP_STS_BADREQUEST);
        }
    }
}