<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\CurrencyList;
use CreditBPO\Services\FinancialStatement\FinancialStatementUpload;
use CreditBPO\Services\FinancialStatement\FinancialStatementList;
use CreditBPO\Services\FinancialStatement\FinancialStatementDelete;
use CreditBPO\Services\FinancialStatement\FinancialStatementUpdate;
use CreditBPO\DTO\FSUpdateDTO;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Models\Entity;
use Maatwebsite\Excel\Facades\Excel; 

class FinancialStatementAPIController extends ApiController
{
    public function __construct()
    {
        $this->currencyListService = new CurrencyList;
        $this->fsUploadService = new FinancialStatementUpload;
        $this->fsListService = new FinancialStatementList;
        $this->fsDeleteService = new FinancialStatementDelete;
        $this->fsUpdateService = new FinancialStatementUpdate;
        $this->userTokenService = new UserTokenService;
    }

    public function getCurrencyList()
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->currencyListService->getCurrenciesByPage($page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else{
            $result = $this->currencyListService->getCurrenciesList();
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }

    public function deleteReportFSDocument($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->fsUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }
        
        /*Validate report id if exist */
        $idExist = $this->fsDeleteService->validateReportIfExist($reportId);
        if ($idExist){
            /* Delete data */
            $deleteResult = $this->fsDeleteService->deleteFinancialStatement($reportId);
            if (!is_null($deleteResult)){
                if ($deleteResult){
                    return $this->response(null, "Financial statement documents deleted successfully.", HTTP_STS_OK);
                } else{
                    return $this->respondInternalError("Error in deleting financial statement documents");
                }
            } else{
                return $this->error("Report doesn't have financial statement document/s.", HTTP_STS_BADREQUEST);
            }
        } else {
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function updateFinancialStatement($reportId)
    {
        /*Validate report id if exist */
        $idExist = $this->fsUpdateService->validateReportIfExist($reportId);
        if ($idExist){
            $fsUpdateDto = new FSUpdateDTO;
            $fsUpdateDto->setReportId($reportId);
            $fsUpdateDto->setDocumentType(Request::input('documenttype'));
            /*Validate request details */
            $validator = $this->fsUpdateService->validateRequest($fsUpdateDto->getDocumentType());
            if ($validator->fails()){
                return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
            } else{
                $fsUpdateDto = $this->fsUpdateService->updateFSDetails($fsUpdateDto);
                if (!is_null($fsUpdateDto)){
                    return $this->response($fsUpdateDto->getVars(), "Financial statement document type succesfully updated.", HTTP_STS_OK);
                } else{
                    return $this->error("Report doesn't have financial statement document/s.", HTTP_STS_BADREQUEST);
                }
            }
        } else {
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function downloadFSTemplate($id)
    {
        $file = public_path("documents/FSInputTemplate.xls");
        $headers = [
            'Content-Type: application/vnd.ms-excel',
        ];

        $user = Entity::select('companyname')->where('entityid', $id)->first();
        $company = $user->companyname;
        $companyname = preg_replace('/\s+/', '_', $company);

        if(strpos($company,"&") !== false){
            $company  = preg_replace("/[&]/", "And", $company);
        }

        $sourceFilePath=public_path("documents/FSInputTemplate.xls");
        $destinationPath=public_path("documents/fs/" . $companyname . ".xls");
        $success = \File::copy($sourceFilePath,$destinationPath);

        $file_path = public_path("documents/fs/" . $companyname . ".xls");
        Excel::load($file_path, function($doc) use ($company) {
            $sheet = $doc->getActiveSheet(0);
            $sheet->setCellValue('B2', $company);
        })->store('xlsx', public_path('documents/fs'));

        $file_name = public_path("documents/fs/{$companyname}.xlsx");

        if (file_exists($file)){
            return $this->download($file_name, 'Financial Statement Template.xls', $headers);
        } else {
            return $this->respondInternalError('Financial statement template file cannot be found.');
        }
    }

    public function getReportFinancialStatement($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->fsUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /*Validate report id if exist */
        $idExist = $this->fsListService->validateReportIfExist($reportId);
        if ($idExist){
            /* Get Report FS documents */
            $fsListDto = $this->fsListService->getReportFSList($reportId);
            if (!is_null($fsListDto)){
                return $this->response($fsListDto->getVars(), "Click the provided link to download Financial Statement Input Template", HTTP_STS_OK);
            } else{
                return $this->response(null, "No Financial Statement found.", HTTP_STS_OK);
            }
        } else {
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

    public function uploadFinancialStatement($reportId)
    {
        /** Validate owner of the report */
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $errorValidReport = $this->fsUploadService->validateReport($reportId, $userId);
        if($errorValidReport){
            return $this->error(
                $errorValidReport['errors'],
                $errorValidReport['code']
            );
        }

        /* Manage and validate request details */
        $fsUploadDto = $this->fsUploadService->manageRequestDetails(Request::all(), $reportId);

        if ($fsUploadDto){
            $errors = $this->fsUploadService->validateDetails($fsUploadDto, Request::all());
        
            if (!empty($errors)){
                return $this->error($errors, HTTP_STS_BADREQUEST);
            }
            /* If request doesn't have errors*/
            else{
                /*Check if report can upload fs file */
                $canUpload = $this->fsUploadService->validateReportFSUpload($reportId);
                if ($canUpload){
                    
                    /*Create zip file*/
                    $processFsTemplate = $this->fsUploadService->processFsTemplate($fsUploadDto, Request::all());
                    if($processFsTemplate){
                        return $this->respondInternalError("Error occured while processing complex financial statement details.");
                    }
                    return $this->response($fsUploadDto->getVars(), "Successfully uploaded Financial Statement.", HTTP_STS_OK);
                } else{
                    return $this->error("Report already have financial statements.", HTTP_STS_BADREQUEST);
                }
            }
        } else{
            return $this->error("Invalid request body.", HTTP_STS_BADREQUEST);
        }
    }
}