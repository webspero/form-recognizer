<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Mail;
use URL;
use CreditBPO\Models\Document;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\Report\Api\DashboardService;
use CreditBPO\Services\Report\Api\CreateReportService;
use CreditBPO\Services\Report\Api\SubmitReportService;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use Illuminate\Support\Facades\Input;
use CreditBPO\Handlers\FinancialHandler;

use DB;

//======================================================================
//  Class #: API Report Controller
//      Functions related to Reports APIs are routed in this class
//======================================================================

class ReportsAPIController extends ApiController 
{
    //---------------------------------------------------------------
	// Function #.a. Initialization
	//---------------------------------------------------------------
	public function __construct()
	{
        $this->dashboardService = new DashboardService;
        $this->userTokenService = new UserTokenService;
        $this->submitReportService = new SubmitReportService;
        $this->createReportService = new createReportService;
    }
    
    //---------------------------------------------------------------
	// Function #.b. Returns List of Reports
	//---------------------------------------------------------------
	public function getReports()
	{
        //echo Request::header('Authorization'); exit;
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        //echo $userId; exit;

        $page = Request::query('page');
        $limit = Request::query('limit');

        if($page && $limit) {
            $result = $this->dashboardService->getReportsByPage($userId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {
            /* Get list of reports owned by the user currently logged in */
            $reports = $this->dashboardService->getReports($userId);
    
            if(!empty($reports)) {
                return $this->response(
                    [
                        "reports" => $reports
                    ],
                    "List of Reports generated. Access the link per report for more details.",
                    HTTP_STS_OK
                );
            } else {
                return $this->response(null, 'No reports found.', HTTP_STS_OK);
            }
        }

	}

    //---------------------------------------------------------------
	// Function #.c. Create New Report
	//---------------------------------------------------------------
	public function createReport()
	{
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /** Check Billing Details */
        $isBillingDetails = $this->createReportService->checkBillingDetails($userId);
        if($isBillingDetails == false){
            return $this->error(
                "Please update billing details of your account to create reports. Go to " . route('billingdetails.put'),
                HTTP_STS_BADREQUEST);
        }
    
        /* Run necessary validations */
        $validator = $this->createReportService->validateCreateReportData(Request::all());

        /* If POST data passed validations, populate ReportDTO */
        if($validator->fails()) {
            /*return bad request with validation messages */
            return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
        } else {
            /* Validate POST data on database, and prepare DTO */
            $reportDTO = $this->createReportService->prepareReportDTO(Request::all(), $userId);
            
            if($reportDTO->getErrors() == null) {
                /* Save Entity */
                $newReport = $this->createReportService->saveNewReport($reportDTO);
                
                /* Check if it uses previous report data and reuse it, if any*/
                if($reportDTO->getUseOldData()) {
                    $this->createReportService->savePreviousReportData($reportDTO);
                }
    
                /* Service F: Send Email */
                $this->createReportService->sendCreatedReportEmail($reportDTO);
    
                 if($reportDTO->getIsPaid()) {
                    /* If paid, Return to list of reports or link of newly created report */
                    return $this->response(
                        [
                            "link" => URL::route('report.get', $reportDTO->getReportId()),
                            "reportId" => $reportDTO->getReportId()
                        ],
                        "New report successfully created!",
                        HTTP_STS_OK
                    );

                } else {
                    /* Else, provide payment links, faceamount and request URLs */

                    return $this->response(
                        [
                            "paymentlinks" => array(
                                "amount" => URL::route('amount.get', $reportDTO->getReportId()),
                                "dragonpay" => URL::route('dragonpay.get', $reportDTO->getReportId()),
                                "paypal" => URL::route('paypal.get', $reportDTO->getReportId()),
                            )
                        ],
                        "New report successfully created! Provide face amount and Dragonpay and PayPal links to user.",
                        HTTP_STS_OK
                    );
                }
            } else {
                return $this->error($reportDTO->getErrors(), HTTP_STS_BADREQUEST);
            }
        }
    }

    //---------------------------------------------------------------
    // Function #.c. Create New Report
    //---------------------------------------------------------------
    public function uploadLOA($reportId,$userId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        $qatest = request('test');
        if(!empty($qatest)){

            DB::table('tbldocument')->where('entity_id',$reportId)->update(['is_deleted' => 0]);

            $loa = [
                'entity_id' => $reportId,
                'is_origin' => 0,
                'document_type' => 0,
                'document_group' => 11,
                'document_orig' => 'LETTER OF AUTHORIZATION.pdf',
                'document_rename' => 'QA - LETTER OF AUTHORIZATION.pdf',
                'created_at' => date('Y-m-d H-i-s'),
                'updated_at' => date('Y-m-d H-i-s'),
                'upload_type' => 6,
                'is_valid' => 0,
                'is_deleted' => 0
            ];

            DB::table('tbldocument')->insert($loa);

            return $this->response(
                null,
                "LOA is submitted.",
                HTTP_STS_OK
            );
        }

        $user = DB::table('tbllogin')->where('loginid',$userId)->first();

        $letter_of_authorization = Input::file('letter_of_auth');
                
        /* move file to letter_authorization folder */
        if($letter_of_authorization && !empty($user->email)){

            $entity = DB::table('tblentity')->where('entityid',$reportId)->first();
            $name = $letter_of_authorization->getClientOriginalName();
            $letter_of_authorization->move('documents/letter_authorization',$userId . ' - ' .$name);
            $document = new Document;
            $document->entity_id = $entity->entityid;
            $document->is_origin = 0;
            $document->document_type = 0;
            $document->document_group = DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION;
            $document->document_orig = $name;
            $document->document_rename =  $entity->entityid . ' - ' .$name;
            $document->upload_type = UPLOAD_TYPE_LETTER_OF_AUTHORIZATION;
            $document->is_valid = 0;
            $document->is_deleted = 0;
            $document->save();
            $email = $user->email;

            $cbpoPdf = public_path('documents/letter_authorization/' .$entity->entityid . ' - ' .$name ) ;

            if(env("APP_ENV") != "local"){
                try{
                    Mail::send('emails.letter_of_authorization', 
                        array('entity' => $entity),
                        function($message) use ($email, $cbpoPdf) {
                            $message->to($email)
                                    ->subject('Letter of Authorization')
                                    ->attach($cbpoPdf);
                    }); 
                }catch(Exception $ex){
                    if(env("APP_ENV") != "Production"){
                        throw $ex;
                    }
                }
            }

            return $this->response(
                    null,
                    "Report has been successfully submitted and analysis will now begin. An email will be sent upon completion of the report in approximately 24 to 48 hours.",
                    HTTP_STS_OK
                );
        }
    }
    
    //---------------------------------------------------------------
	// Function #.b. Submit for Credit Investigation
	//---------------------------------------------------------------
	public function submitQuestionnaire($reportId)
	{
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if user owns the report */
        $checkReport = $this->submitReportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Update progress in tblentity by checking required data and their weights */
            $response = $this->submitReportService->checkProgress($reportId);
            
            if($response){
                return $this->response(
                    $response,
                    "Report is Incomplete. User needs to provide information for the required fields before the report can be submitted for analysis",
                    HTTP_STS_OK
                );
            }

            // Get financial rating results
            $financialResult = $this->submitReportService->getFinancialRatingResult($reportId);
            
            return $this->response(
                $financialResult,
                "Report has been successfully submitted and analysis will now begin. An email will be sent upon completion of the report in approximately 24 to 48 hours.",
                HTTP_STS_OK
            );

        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function generateQAFS($reportId,$referenceId,$userId){
        // $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        //fs input template        
        $tbldocuments = DB::table('tbldocument')->where('entity_id',$referenceId)->get()->toArray();
        
        foreach($tbldocuments as $fs){
            $fs = (array) $fs;
            unset($fs['documentid']);
            $fs['entity_id'] = $reportId;
            $fs['is_deleted'] = 0;

            DB::table('tbldocument')->insert($fs);
         }

        //financial report        
        $financial_reports = DB::table('financial_reports')->where('entity_id',$referenceId)->get()->toArray();
        
        foreach($financial_reports as $fs){
            $fs = (array) $fs;
            $fs['entity_id'] = $reportId;
            $fs['is_deleted'] = 0;
            $refFinancialReportId = $fs['id'];
            unset($fs['id']);

            $financialReportId = DB::table('financial_reports')->insertGetId($fs);
         }

         //balance sheet        
        $balance_sheets = DB::table('fin_balance_sheets')->where('financial_report_id',$refFinancialReportId)->get()->toArray();
        
        foreach($balance_sheets as $fs){
            $fs = (array) $fs;
            unset($fs['id']);
            $fs['financial_report_id'] = $financialReportId;
            $fs['is_deleted'] = 0;

            DB::table('fin_balance_sheets')->insert($fs);
         }

        //income statement
        $income_statement = DB::table('fin_income_statements')->where('financial_report_id',$refFinancialReportId)->get()->toArray();
        
        foreach($income_statement as $fs){
            $fs = (array) $fs;
            unset($fs['id']);
            $fs['financial_report_id'] = $financialReportId;
            $fs['is_deleted'] = 0;

            DB::table('fin_income_statements')->insert($fs);
         }

        //cash flow
        $cash_flow = DB::table('fin_cashflow')->where('financial_report_id',$refFinancialReportId)->get()->toArray();
        
        foreach($cash_flow as $fs){
            $fs = (array) $fs;
            unset($fs['id']);
            $fs['financial_report_id'] = $financialReportId;
            $fs['is_deleted'] = 0;

            DB::table('fin_cashflow')->insert($fs);
         }
        
        //tblsmescore
        $tblsmescore = DB::table('tblsmescore')->where('entityid',$referenceId)->get()->toArray();
        
        foreach($tblsmescore as $fs){
            $fs = (array) $fs;
            unset($fs['smescoreid']);
            $fs['entityid'] = $reportId;
            $fs['loginid'] = 1210;
            //$fs['is_deleted'] = 0;

            DB::table('tblsmescore')->insert($fs);
         }

         // innodata_files 
        $innodata_files = DB::table('innodata_files')->where('entity_id',$referenceId)->get()->toArray();
        
        foreach($innodata_files as $fs){
            $fs = (array) $fs;
            unset($fs['id']);
            $fs['entity_id'] = $reportId;
            //$fs['is_deleted'] = 0;

            DB::table('innodata_files')->insert($fs);
         }

         $entity = [
            'is_deleted' => 0,
            'isIndustry' => 1
         ];

         DB::table('innodata_files')->insert($fs);

         return $this->response(
            ['status' => 'success'],
            "Report updated",
            HTTP_STS_OK
        );
         
    }
}
