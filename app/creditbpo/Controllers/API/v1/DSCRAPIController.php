<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Services\Report\Api\ReportService;
use CreditBPO\Services\Report\Api\DscrService;

class DscrAPIController extends ApiController
{
    public function __construct()
    {
        $this->reportService = new ReportService;
        $this->userTokenService = new UserTokenService;
        $this->dscrService = new DscrService;
    }

    public function generateDSCR($reportId)
    {
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if user owns the report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Check if report has financial statements needed, and get needed data */
            $dscrDTO = $this->dscrService->getFinancialReports($reportId);

            /* Validate user inputs, if any */
            if(!empty(Request::all()))
                $dscrDTO = $this->dscrService->validateRequestInputs(Request::all(), $dscrDTO);
            
            if($dscrDTO->getErrors() == null) {
                /* Perform necessary DSCR Calculations */
                $this->dscrService->calculateDSCR($dscrDTO);

                /* Update database */
                if($this->dscrService->updateDSCRRecord($dscrDTO)) {
                    $isFinancing = $this->dscrService->isFinancingAssocOrg($dscrDTO);

                    $relevantDetails = $this->dscrService->getRelevantDetails($dscrDTO);

                    if($isFinancing) {
                        /* Return DSCR values: Inputs, WorkingCapital/TermLoan, FS Overview */
                        return $this->response(
                            [
                                "debtServiceCapacityCalculator" => $dscrDTO->getFinancingResponse(),
                                'relevantDetails' => $relevantDetails
                            ],
                            "Calculation Assumption: 365 days/year for Working Capital and 360 days/year for Term Loan.",
                            HTTP_STS_OK
                        );
                    } else {
                        /* Return DSCR values: Inputs, WorkingCapital/TermLoan, FS Overview */
                        return $this->response(
                            [
                                "debtServiceCapacityCalculator" => $dscrDTO->getProcurementResponse(),
                                'relevantDetails' => $relevantDetails
                            ],
                            "DSCR for Procurement has been generated.",
                            HTTP_STS_OK
                        );
                    }
                } else {
                    return $this->error(
                        $dscrDTO->getErrors(),
                        HTTP_STS_INTERNALSERVERERROR
                    );
                }
            } else {
                return $this->error(
                    $dscrDTO->getErrors(),
                    HTTP_STS_BADREQUEST
                );
            }

        } else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }

    public function getDSCRDetails($reportId){
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if user owns the report */
        $checkReport = $this->reportService->validateReport($reportId, $userId);

        if($checkReport == null) {
            /* Check if report has financial statements needed, and get needed data */
            $dscrDTO = $this->dscrService->getFinancialReports($reportId);

            $isFinancing = $this->dscrService->isFinancingAssocOrg($dscrDTO);

            $relevantDetails = $this->dscrService->getRelevantDetails($dscrDTO);

            if($isFinancing) {
                /* Return DSCR values: Inputs, WorkingCapital/TermLoan, FS Overview */
                return $this->response(
                    [
                        "debtServiceCapacityCalculator" => $dscrDTO->getFinancingResponse(),
                        'relevantDetails' => $relevantDetails
                    ],
                    "Calculation Assumption: 365 days/year for Working Capital and 360 days/year for Term Loan.",
                    HTTP_STS_OK
                );
            } else {
                /* Return DSCR values: Inputs, WorkingCapital/TermLoan, FS Overview */
                return $this->response(
                    [
                        "debtServiceCapacityCalculator" => $dscrDTO->getProcurementResponse(),
                        'relevantDetails' => $relevantDetails
                    ],
                    "DSCR for Procurement has been generated.",
                    HTTP_STS_OK
                );
            }
        }else {
            return $this->error(
                $checkReport['errors'],
                $checkReport['code']
            );
        }
    }
}