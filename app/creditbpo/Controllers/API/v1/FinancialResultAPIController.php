<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\FinalReportService;
use CreditBPO\Models\Entity;
use CreditBPO\Handlers\AutomateSimplifiedRatingHandler;
use CreditBPO\Libraries\HighCharts;
use CreditBPO\Services\UpdateSmeReport;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use CreditBPO\Models\Director;
use CreditBPO\Models\Certification;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Insurance;
use CreditBPO\Models\Competitor;
use CreditBPO\Models\BusinessSegment;
use CreditBPO\Models\Location;
use CreditBPO\Services\IbakusService;
use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Services\KeyRatioService;
use CreditBPO\Libraries\StringHelper;
use CreditBPO\Handlers\SMEInternalHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use Auth;
use DateTime;
use ActivityLog;
use KeyManager;
use Shareholder;
use Input;
use DB;

class FinancialResultAPIController extends ApiController
{
    public function __construct()
    {
        $this->finalReportService = new FinalReportService();
    }

    public function getFinancialResult($id)
    {

    	/* Validate request */
        $result = $this->finalReportService->validateFinancialResultDetails($id);
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        
        /** Initialize Server Response  */
        $srv_resp['sts']            = STS_NG;               // default status
        $srv_resp['messages']       = STR_EMPTY;            // variable container

        $status = 5; // SME submitted
        if($id) {
            $entity = Entity::where("entityid", $id)->first();
        }
        $entityFields       = array('status'    => $status);      
        $updateProcess      = Entity::where('entityid', '=', $id)->update($entityFields);   // update entity
        $entity             = Entity::where('entityid', '=', $id)
            ->leftJoin('refcitymun', 'tblentity.cityid', 'refcitymun.id')
            ->first();      // get entity

        $entityFields       = array('submitted_at' => new DateTime() ); //update submitted_at
        Entity::where('entityid', '=', $id)->update($entityFields);

        ActivityLog::saveSubmitLog();                       // log activity to db

        $company_name = $entity->companyname;

        /** Deperecate CDP- 1735*/
        // send email to notify.user@creditbpo.com that a questionnaire has been submitted by SME
        // if (env("APP_ENV") == "Production"){

        //     try{
        //         Mail::send('emails.startscoring', array(
        //             'company_name' => $entity->companyname,
        //             'company_email' => $entity->email,
        //             'phone' => $entity->phone,
        //             'address1' => $entity->address1,
        //             'address2' => $entity->address2,
        //             'city' => $entity->city,
        //             'province' => $entity->province,
        //             'zipcode' => $entity->zipcode
        //         ), function($message){
        //             $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
        //                 ->subject('Alert: New Basic User Report has been submitted!');
        //         });
        //     }catch(Exception $e){
        //         //
        //     }
        // }

        $ratingHandler = new AutomateStandaloneRatingHandler();
        //$ratingHandler->registerReport($id); //orig

        /** Register created report for processing: automate/manual */
        $ratingHandler->registerInnodataReport($id);

        // crossmatch open sanctions
        if($entity->is_premium == PREMIUM_REPORT ){
            // $ibakus = new IbakusService();
            // $ibakus->getRelatedPersonalities($entity->entityid);
            $this->crossmatchOpensanction($entity);

        }

        if ($updateProcess) {       // if success
            
            /** Email developer for created new report */
            if (env("APP_ENV") != "local"){
                $cc = array("ana.liza.bongat@creditbpo.com");

                if(env("APP_ENV") == "Production"){
                    array_push($cc, "jose.francisco@creditbpo.com");
                }

                try{

                    Mail::send('emails.startscoring', array(
                        'company_name' => $entity->companyname,
                        'company_email' => $entity->email,
                        'phone' => $entity->phone,
                        'address1' => $entity->address1,
                        'address2' => $entity->address2,
                        'city' => $entity->city,
                        'province' => $entity->province,
                        'zipcode' => $entity->zipcode
                    ), function($message) use ($cc){
                        $message->to('leovielyn.aureus@creditbpo.com')
                                ->cc($cc)
                                ->subject('Alert: New Basic User Report has been submitted!');
                    });

                }catch(Exception $e){
                	//
                }
            }

            $financial_report = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->first();
            if($financial_report){
                if ($entity->is_premium == SIMPLIFIED_REPORT) {
                    $simplifiedRatingHandler = new AutomateSimplifiedRatingHandler();
                    $simplifiedRatingHandler->processReport($id);
                } else {
                    $keyRatio = new KeyRatioService();
                    $keyRatio->generateKeyRatioByEntityId($id);
                }
                
                if ((!Input::has('action')) && (!Input::has('section'))) {

                    $standaloneRatingHandler = new AutomateStandaloneRatingHandler();
                    $standaloneRatingHandler->processSingleReport($id);

                    Entity::where("entityid", $entity->entityid)->update(array("status" => 7));

                    if($entity->is_premium != PREMIUM_REPORT){
	                    $result = [
	                    	'report_link' => url('summary/'.$entity->entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname)),
	                    	
	                    ];
                	}else{
                		$result = [
	                    	'report_link' => url('premium-link/'.$entity->entityid.'/'.$entity->loginid.'/'.urlencode($entity->companyname))
	                    ];
                	}
                   
		            return $this->response($result, 'Success', HTTP_STS_OK);

                }
                else {
                    $srv_resp['sts']        = STS_OK;
                    $srv_resp['messages']   = 'Successfully Added Record';
                    return $this->response($srv_resp, 'Successfully Added Record', HTTP_STS_OK);
                }

                return $this->response([], 'Transaction Completed.', HTTP_STS_OK);
            } 
            else {
                DB::table('automate_standalone_rating')->where("entity_id", $entity->entityid)->update(['status' => 5]);

                return $this->response([], 'Transaction Completed. We will email you upon completion of your report.', HTTP_STS_OK);
            }
            
        }
        else {// if failed
            if ((!Input::has('action')) && (!Input::has('section'))) {
                if($entity->is_premium == PREMIUM_REPORT ) {
                    
                    return $this->response($srv_resp, 'An error occured while updating the user. Please try again.', HTTP_STS_BADREQUEST);
                } else {
                    
                    return $this->response($srv_resp, 'An error occured while updating the user. Please try again.', HTTP_STS_BADREQUEST);
                }
            }
            else {
                $srv_resp['messages']   = 'An error occured while updating the user.';
                
                return $this->response($srv_resp, 'Successfully Added Record', HTTP_STS_BADREQUEST);
            }
        }
    }

    //-----------------------------------------------------
    //  Function 1.26: crossmatchOpensanction
    //  Description: crossmatch function for opensanctions
    //-----------------------------------------------------
	public function crossmatchOpensanction($entity){
        // Fetch personalities that need to be crossmatched.
        $keyManagers = KeyManager::select("firstname" , "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();

        $shareHolders = Shareholder::select("name", "firstname", "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();

        $directors = Director::select("name", "firstname", "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();

        // start crossmatch
        $lists = array();
        foreach ($keyManagers as $key) {
            // get nationality code
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $key->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($key->firstname), 
                "lastname" => ($key->middlename) ? strtolower($key->middlename .  " " . $key->lastname) : strtolower($key->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            $name = $data['firstname'] . " " . $data['lastname'];
            if(!in_array(strtolower($name), $lists)) { 
                $lists[] = strtolower($name);
                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
            }
           
        }

        foreach ($shareHolders as $key => $share) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $share->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($share->firstname), 
                "lastname" => ($share->middlename)? strtolower($share->middlename .  " " . $share->lastname): strtolower($share->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            if($data['firstname']) {
                $name = $data['firstname'] . " " . $data['lastname'];
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
                }
            } else {
                $name = strtolower($share['name']);
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByName($name, $entity->entityid, $nationality);
                }
            }
        }
        
        foreach ($directors as $key => $dir) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $dir->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($dir->firstname), 
                "lastname" => ($dir->middlename)? strtolower($dir->middlename .  " " . $dir->lastname): strtolower($dir->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            if($data['firstname']) {
                $name = $data['firstname'] . " " . $data['lastname'];
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
                }
            } else {
                $name = strtolower($dir['name']);
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByName($name, $entity->entityid, $nationality);
                }
            }
        }
        return;
    }

    private function findByFirstAndLastname($data,$entity_id, $nationality) {
        $entity = Entity::where("entityid", $entity_id)->first();
        $records = DB::table("open_sanctions")->select("charges")->where($data);
        if($nationality) {
            $records->where("nationality", "like", "%{$nationality}%");
        }
        $records = $records->get();
        if($records->count() > 0) {
            $name = ucwords($data['firstname'] . " " . $data['lastname']);

            $charges = "<tr><td> {$name} </td><td><ul>";
            if ($entity->negative_findings == "") {
                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
            } else {
                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
            }
            foreach ($records as $key => $value) {
                if ($value->charges) {
                    $charges .= "<li> " . $value->charges . " </li>";
                }
            }
            $charges .= "</ul></td></tr></tbody></table>";


            // update negative findings
            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
        }
        return;
    }
}