<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\BusinessDetailsII\ShareHoldersCRUD;
use CreditBPO\DTO\ShareHoldersDTO;

class ShareHoldersAPIController extends ApiController
{
    public function __construct()
    {
        $this->shareHoldersCRUDService = new ShareHoldersCRUD();
    }

    public function getShareHoldersList($reportId)
    {
        if(Request::query('page') && Request::query('limit')){
            $page = Request::query('page');
            $limit = Request::query('limit');    
        }else{
            $page = null;
            $limit = null;
        }
        
        if ($page && $limit){
            $result = $this->shareHoldersCRUDService->getShareHoldersByPage($reportId, $page, $limit);
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        } else {

        	$result = $this->shareHoldersCRUDService->getShareHoldersByReportId($reportId);
        	$items = (array) $result['items'];
        	$result['total'] = count($items);
        	$result['limit'] = count($items);
        	$result['page'] = 1;
        	
            if (empty($result['items'])){
                return $this->response(null, 'No results found.', HTTP_STS_OK);
            } else{
                return $this->response($result, 'Success', HTTP_STS_OK);
            }
        }
    }
    
    public function getShareHoldersDetails($relatedCompaniesId)
    {
        $shareHoldersDto = $this->shareHoldersCRUDService->getShareHoldersById($relatedCompaniesId);

        if (!empty($shareHoldersDto)){
            return $this->response($shareHoldersDto->getVars(), 'Success', HTTP_STS_OK);
        } else{
            return $this->response(null, 'No result found.', HTTP_STS_OK);
        }
    }

    public function postShareHolders($reportId)
    {
    	$data = request()->all();

    	//print_r($data); exit;
        /* Check if report id exist */
        $reportExist = $this->shareHoldersCRUDService->checkReportIdIfExist($reportId);
        if ($reportExist){
            $shareHoldersDto = new ShareHoldersDTO();
            $shareHoldersDto->setVars($data);
            
            /* Validate request */
            $result = $this->shareHoldersCRUDService->validateShareHoldersDetails($shareHoldersDto->getVars());
            if (!empty($result['errors'])){
                return $this->error($result['errors'], HTTP_STS_BADREQUEST);
            }
            /* Validation success, save data to database */
            else {

                $savedShareHoldersDto = $this->shareHoldersCRUDService->shareHoldersInsert($shareHoldersDto);
                if (!empty($savedShareHoldersDto->getId())){
                    return $this->response($savedShareHoldersDto->getVars(), "Share Holders saved successfully.", HTTP_STS_CREATED);
                } else{
                    return $this->respondInternalError("Error Share Holders.");
                }
            	
            }
        }else{
            return $this->error("Report doesn't exist.", HTTP_STS_BADREQUEST);
        }
    }

	public function postUpdateShareHolders($relatedCompaniesId)
    {
    	$data = request()->all();

        $shareHoldersDto = new ShareHoldersDTO();
        $shareHoldersDto->setVars($data);
        
        /* Validate request */
        $result = $this->shareHoldersCRUDService->validateShareHoldersDetails($shareHoldersDto->getVars());
        if (!empty($result['errors'])){
            return $this->error($result['errors'], HTTP_STS_BADREQUEST);
        }
        /* Validation success, save data to database */
        else {

            $udpatedShareHoldersDto = $this->shareHoldersCRUDService->shareHoldersUpdate($shareHoldersDto);
            if (!empty($udpatedShareHoldersDto->getId())){
                return $this->response($udpatedShareHoldersDto->getVars(), "Share Holders updated successfully.", HTTP_STS_OK);
            } else{
                return $this->respondInternalError("Error Share Holders.");
            }
        	
        }
        
    }

    public function deleteShareHolders($id)
    {
    	$data = request()->all();
       
        $deletedShareHoldersDto = $this->shareHoldersCRUDService->shareHoldersDelete($id);
        if (!empty($deletedShareHoldersDto)){
            return $this->response([], "Share Holders deleted successfully.", HTTP_STS_OK);
        } else{
            return $this->respondInternalError("Error Share Holders.");
        }
        
    }
}