<?php

namespace CreditBPO\Controllers\API\v1;

use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\UserTokenService;
use CreditBPO\DTO\UserAuthDTO;
use Request;

class UserTokenAPIController extends ApiController
{
    public function __construct()
    {
        $this->userAuthTokenDto = new UserAuthDTO();
        $this->userTokenService = new UserTokenService();
    }
    public function getNewAccessToken()
    {
        /*Get request header client id*/
        $clientId = Request::header('Client-Id');
        $refreshToken = Request::header('Refresh-Token');

        $this->userAuthTokenDto->setClientId($clientId)
                               ->setRefreshToken($refreshToken);

        if (!is_null($refreshToken)){
            /* Check if clientId and refresh token exist and valid */
            $userAuthTokenDto = $this->userTokenService->getRefreshTokenDetails($this->userAuthTokenDto);
            
            /* If not exist, throw an error response */
            if (is_null($userAuthTokenDto)){
                return $this->error('Invalid refresh token.', HTTP_STS_UNAUTHORIZED);
            }

            /* Refresh token is valid. Generate new access token */
            $tokenUpdate = $this->userTokenService->updateUserTokens($userAuthTokenDto);
            
            if (!is_null($tokenUpdate)){
                $userTokenDetails = [
                    'accesstoken' => $tokenUpdate->getAccessToken()
                ];
                return $this->response($userTokenDetails, 'Creating new access token success!', HTTP_STS_OK);
            } else{
                return $this->respondInternalError('Problem updating access token. Please try again.');
            }
        } else{
            return $this->error('Refresh token is required!', HTTP_STS_BADREQUEST);
        }
    }
}