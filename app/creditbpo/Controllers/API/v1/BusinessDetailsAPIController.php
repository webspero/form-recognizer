<?php

namespace CreditBPO\Controllers\API\v1;

use URL;
use Request;
use CreditBPO\Controllers\API\v1\ApiController;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Services\Report\Api\IndustriesListService;
use CreditBPO\Services\Report\Api\LocationListService;
use CreditBPO\Services\Report\Api\BusinessDetailsService;
use CreditBPO\Libraries\BusinessDetailsLib;

//======================================================================
//  Class #: API Business Details Controller
//      Functions related to Business Details API are routed in this class
//======================================================================

class BusinessDetailsAPIController extends ApiController 
{
    //---------------------------------------------------------------
	// Function #.a. Initialization
	//---------------------------------------------------------------
	public function __construct()
	{
        $this->userTokenService = new UserTokenService;
        $this->industriesService = new IndustriesListService;
        $this->locationListService = new LocationListService;
        $this->businessDetailsService = new BusinessDetailsService;
    }

    //---------------------------------------------------------------
    // Function #.b. Returns Lists for Industry Main
	//---------------------------------------------------------------
	public function getMainIndustries()
	{
        $page = (Request::query('page') !== null)? Request::query('page') : null;

        $limit = (Request::query('limit') !== null)? Request::query('limit') : null;

        $mainIndustries = $this->industriesService->getMainIndustries($page, $limit);

        if(!empty($mainIndustries)) {
            return $this->response(
                [
                    "industriesmain" => $mainIndustries
                ],
                "Success.",
                HTTP_STS_OK
            );
        } else {
            return $this->response(null, 'No industries found.', HTTP_STS_OK);
        }
    }

    //---------------------------------------------------------------
    // Function #.b. Returns Lists for Industry Sub
	//---------------------------------------------------------------
	public function getSubIndustries($mainIndustryId)
	{
        $subIndustries = $this->industriesService->getSubIndustries($mainIndustryId);

        if(!empty($subIndustries)) {
            return $this->response(
                [
                    "industriessub" => $subIndustries
                ],
                "Success.",
                HTTP_STS_OK
            );
        } else {
            return $this->error("Invalid Main Industry ID. Plase check your request.", HTTP_STS_BADREQUEST);
        }
    }

    //---------------------------------------------------------------
    // Function #.c. Returns Lists for Industry Row
	//---------------------------------------------------------------
	public function getRowIndustries($mainIndustryId, $subIndustryId)
	{
        $rowIndustries = $this->industriesService->getRowIndustries($mainIndustryId, $subIndustryId);

        if(!empty($rowIndustries)) {
            return $this->response(
                [
                    "industries" => $rowIndustries
                ],
                "Success.",
                HTTP_STS_OK
            );
        } else {
            return $this->error("Invalid Main Industry ID and Sub Industry ID. Plase check your request.", HTTP_STS_BADREQUEST);
        }
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Lists of Provinces
	//---------------------------------------------------------------
	public function getProvinces()
	{
        $page = (Request::query('page') !== null)? Request::query('page') : null;
        $limit = (Request::query('limit') !== null)? Request::query('limit') : null;

        $provinces = $this->locationListService->getProvinces($page, $limit);
        if(!empty($provinces) || $page <= 0) {
            return $this->response(
                [
                    "provinces" => $provinces
                ],
                "Success.",
                HTTP_STS_OK
            );
        } else {
            return $this->response(null, 'No provinces found.', HTTP_STS_OK);
        }
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Lists of Provinces
	//---------------------------------------------------------------
	public function getCitiesByProvince()
	{
        $province = (Request::query('province') !== null)? Request::query('province') : null;

        if($province != null) {
            /* Check if Province does exists in our records */
            $locationDTO = $this->locationListService->checkProvince($province);
    
            if($locationDTO->getProvince() != null) {
                $cities = $this->locationListService->getCities($locationDTO->getProvince());
        
                if(!empty($cities)) {
                    return $this->response(
                        [
                            "cities" => $cities
                        ],
                        "Success.",
                        HTTP_STS_OK
                    );
                } else {
                    return $this->response(null, 'No cities found.', HTTP_STS_OK);
                }
            } else {
                return $this->error("Invalid Province. Please check your request.", HTTP_STS_BADREQUEST);
            }
        } else {
            return $this->error("Missing Province. Please check your request.", HTTP_STS_BADREQUEST);
        }
    }

    //---------------------------------------------------------------
    // Function #.d. Gets a report's Business Details
	//---------------------------------------------------------------
	public function getBusinessDetails($reportId)
	{
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if passed Report ID is valid and/or User owns the report */
        $validation = $this->businessDetailsService->validateReport($reportId, $userId);

        if($validation == null) {
            /* If Report ID is valid, get its Business Details */
            $reportDTO = $this->businessDetailsService->getBusinessDetails($reportId);
    
            return $this->response(
                [
                    "reportid" => $reportId,
                    "businessdetails" => $reportDTO->getBusinessDetails()
                ],
                "Success.",
                HTTP_STS_OK
            );
        } else {
            return $this->error(
                $validation['errors'],
                $validation['code']
            );
        }
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Business Types
	//---------------------------------------------------------------
	public function getBusinessTypes()
	{
        return BusinessDetailsLib::getBusinessTypes();
    }

    //---------------------------------------------------------------
    // Function #.d. Update a report's Business Details
	//---------------------------------------------------------------
	public function updateBusinessDetails($reportId)
	{
        $userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

        /* Check if passed Report ID is valid and/or User owns the report */
        //$checkData = $this->businessDetailsService->validateReport($reportId, $userId);

        //if($checkData == null) {
            if(!empty(Request::all())) {
                /* If Report ID is valid, get its Business Details */
                $existingReportDTO = $this->businessDetailsService->getBusinessDetails($reportId);
    
                /* Field Validation */
                $validator = $this->businessDetailsService->validateRequestFields(Request::all());
    
                if(!($validator->fails())) {
                    /* Prepare data to be updated, get data-related errors if any */
                    $updatedReportDTO = $this->businessDetailsService->prepareReportUpdate($existingReportDTO, Request::all(), $userId);
                    if($updatedReportDTO->getErrors() == null) {
                        /* Proceed with updating necessary database records */
                        $this->businessDetailsService->updateBusinessDetails($updatedReportDTO, $existingReportDTO);

                        return $this->response(
                            [
                                "businessdetailslink" => URL::route('businessdetails.get',$updatedReportDTO->getReportId())
                            ],
                            "Report successfully updated. Access the link to see updated Business Details of the report.",
                            HTTP_STS_OK
                        );
                    } else {
                        return $this->error($updatedReportDTO->getErrors(), HTTP_STS_BADREQUEST);
                    }
                } else {
                    return $this->error($validator->messages()->all(), HTTP_STS_BADREQUEST);
                }
            } else {
                return $this->error(
                    "No Request Body.",
                    HTTP_STS_BADREQUEST
                );
            }
        // } else {
        //     return $this->error(
        //         $checkData['errors'],
        //         $checkData['code']
        //     );
        // }
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Business Types
	//---------------------------------------------------------------
	public function getReportTypes()
	{
        return BusinessDetailsLib::getReportTypes();
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Employee Size
	//---------------------------------------------------------------
	public function getEmployeeSize()
	{
        return BusinessDetailsLib::getEmployeeSizeRanges();
    }

    //---------------------------------------------------------------
    // Function #.d. Returns Unit
	//---------------------------------------------------------------
	public function getUnits()
	{
        return BusinessDetailsLib::getUnits();
    }

    //---------------------------------------------------------------
    // Function #.d. Return PSE Companies
	//---------------------------------------------------------------
    public function getPseCompanies()
    {
        $page = Request::query('page');
        $limit = Request::query('limit');

        if ($page && $limit){
            $result = $this->businessDetailsService->getPseCompaniesByPage($page, $limit);
        } else{
            $result = $this->businessDetailsService->getPseCompanies();
        }

        if (empty($result['list'])){
            return $this->response(null, 'No results found.', HTTP_STS_OK);
        } else{
            return $this->response($result, 'Success', HTTP_STS_OK);
        }
    }

    //---------------------------------------------------------------
    // Function #.d. Return City, Province, Zipcode
	//---------------------------------------------------------------
    public function getCityProvinceLocation()
    {
        $page = Request::query('page');
        $limit = Request::query('limit');

        if ($page && $limit){
            $result = $this->businessDetailsService->getCityProvinceLocationByPage($page, $limit);
        } else{
            $result = $this->businessDetailsService->getCityProvinceLocation();
        }

        if (empty($result['list'])){
            return $this->response(null, 'No results found.', HTTP_STS_OK);
        } else{
            return $this->response($result, 'Success', HTTP_STS_OK);
        }
    }

    public function getIndustrySection(){
        $page = Request::query('page');
        $limit = Request::query('limit');

        if ($page && $limit){
            $result = $this->businessDetailsService->getIndustrySectionByPage($page, $limit);
        } else{
            $result = $this->businessDetailsService->getIndustrySection();
        }

        if (empty($result['list'])){
            return $this->response(null, 'No results found.', HTTP_STS_OK);
        } else{
            return $this->response($result, 'Success', HTTP_STS_OK);
        }

    }

}