<?php

namespace CreditBPO\Controllers\API\v1;

use Request;
use CreditBPO\Services\UserTokenService;
use CreditBPO\Services\Report\Api\QuestionPassService;
use CreditBPO\Controllers\API\v1\ApiController;

class QuestionPassController extends ApiController
{

	public function __construct() {
		$this->userTokenService = new UserTokenService;
		$this->questionPassService = new questionPassService;
	}

	public function getQuestionPassLink($reportId) {
		/* Check authorized user*/
		$userId = $this->userTokenService->getUserDetailsByAccessToken(Request::header('Authorization'))->getUserId();

		/* Check if passed Report ID is valid and/or User owns the report and not yet submitted */
		$checkData = $this->questionPassService->validateReport($reportId, $userId, Request::all());

		if($checkData == null) {
			/*Get Questionnaire Pass Link for Business Details I and II, Required Credit Line*/
			$questionnaire = $this->questionPassService->getQuestionPassData($reportId);
			$link = $this->questionPassService->generateLink($questionnaire);

			return $this->response(
                [
                    "tab"	=> $questionnaire['tab_name'],
                    "questionnaire_link" => $link
                ],
                "Questionnaire Link generated succesfully. Access the link to answer the questionnaires",
                HTTP_STS_OK
            );
			
		} else {
			return $this->error(
				$checkData['errors'],
				$checkData['code']
			);
		}
	}

}