<?php

namespace CreditBPO\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller {
    protected $layout;

    /**
     * @return void
     */
    protected function setupLayout() {
        View::addExtension(
            'html',
            'php'
        );

        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }
}
