<?php

namespace CreditBPO\Controllers;

use Illuminate\Support\Facades\Redirect;
use CreditBPO\Controllers\BaseController;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Entity;
use File;
use Response;

class FinancialReportController extends BaseController {
    /**
     * @param int $financialReportId
     * @return void
     */
    public function getDownloadReport($financialReportId) {
        return Redirect::to('/fr/compare-report/'.$financialReportId);

        $fr = FinancialReport::find($financialReportId);
        $financialAnalysis = new FinancialAnalysisHandler();

        if ($financialAnalysis->createReport($financialReportId)) {
            return Redirect::to('/fr/compare-report/'.$fr->id);
        }

        Session::flash('fs_gen_err', "Failed to Generate Financial Report, it maybe due to slow connection or image generation failure. Please try again.");
        return Redirect::to('summary/smesummary/'.$fr->entity_id);
    }

    /**
     * @param int $financialReportId
     * @return void
     */
    public function getDownloadFinancialAnalysisInternalReport($entityId,$language='en') {
        
        session(['fa_report_lang' => $language]);
        $financialAnalysis = new FinancialAnalysisInternalHandler();

        if($language == 'tl'){
            $file = Entity::where('entityid',$entityId)->pluck('financial_analysis_pdf_tl')[0];
        }else{
            $file = Entity::where('entityid',$entityId)->pluck('financial_analysis_pdf')[0];
        }

        // if(!empty($file)){
        //     if(file_exists('financial_analysis/'.basename($file))){
                
        //         $newfile = File::get(public_path().'/financial_analysis/'.$file);

        //         $response = response()->make($newfile, 200);

        //         /*  Sets header to PDF  */
        //         $response->header('Content-Type', 'application/pdf');

        //         return $response;
        //     }
        // }
    
        $financialAnalysis = new FinancialAnalysisInternalHandler();

        $financial_report_id = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])->pluck('id')[0];
        $fr = FinancialReport::find($financial_report_id);
        $file = $financialAnalysis->createFinancialAnalysisInternalReport($financial_report_id,$language);
        if ($file) {

            if($language == 'en'){
                Entity::where('entityid',$entityId)->update(['financial_analysis_pdf'=>basename($file)]);
            }else{
                Entity::where('entityid',$entityId)->update(['financial_analysis_pdf_tl'=>basename($file)]);
            }  
            
            $newfile = File::get(public_path().'/financial_analysis/'.basename($file));

            $response = response()->make($newfile, 200);

            /*  Sets header to PDF  */
            $response->header('Content-Type', 'application/pdf');

            return $response;
        }

        Session::flash('fs_gen_err', "Failed to Generate Financial Report, it maybe due to slow connection or image generation failure. Please try again.");
        return Redirect::to('summary/smesummary/'.$fr->entity_id);
    }
}
