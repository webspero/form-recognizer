<?php

namespace CreditBPO\Libraries;

use CreditBPO\DTO\ReportDTO;

//======================================================================
//  Class #: Submit Report Library
//======================================================================
class SubmitReportLib 
{
    //---------------------------------------------------------------
	// Function #.a. Calculate Progress Completion
	//---------------------------------------------------------------
    public static function calculateProgress($weights)
    {
        $totalWeight = 0;
        $progressWeight = 0;

        foreach ($weights as $weightKey => $weight) {
            if($weightKey == "majorcustomers" || $weightKey == "majorsuppliers") {
                $totalWeight += PROGRESS_WEIGHT_HEAVY;
                $progressWeight += PROGRESS_WEIGHT_HEAVY;
            } else if($weightKey == "bankstatements" || $weightKey == "utilitybills") {
                $totalWeight += PROGRESS_WEIGHT_NORMAL;
                $progressWeight += PROGRESS_WEIGHT_NORMAL;
            } else if($weightKey == "registrationdate" || $weightKey == "dateestablished") {
                $totalWeight += PROGRESS_WEIGHT_NORMAL;
                if($weight != "0000-00-00") {
                    $progressWeight += PROGRESS_WEIGHT_NORMAL;
                }
            } else if($weightKey == "employeesize" || $weightKey == "zipcode" || $weightKey == "yearsinpresentaddress" || $weightKey == "yearsmanagementisengaged") {
                $totalWeight += PROGRESS_WEIGHT_NORMAL;
                if($weight >= 0) {
                    $progressWeight += PROGRESS_WEIGHT_NORMAL;
                }
            }else {
                $totalWeight += PROGRESS_WEIGHT_NORMAL;
                if($weight != null) {
                    $progressWeight += PROGRESS_WEIGHT_NORMAL;
                }
            }
        }

        $progress = ($progressWeight / $totalWeight) * 100;

        return $progress;
	}
}