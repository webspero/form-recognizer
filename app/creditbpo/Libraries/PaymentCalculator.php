<?php

namespace CreditBPO\Libraries;
use Entity;
use Bank;
use Province2;
use User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;

class PaymentCalculator
{
    /*Prices*/
    const REPORT_PRICE = 3500;
    const SIMPLIFIED_REPORT_PRICE = 1750;
    const PREMIUM_REPORT_PRICE = 5250;
    const CLIENTKEY_PRICE = 3535;
    const BANKSUBSCRIPTION_PRICE = 10100;

    /* Discount Type */
    const DISCOUNT_TYPE_PERCENTAGE = 1;
    const DISCOUNT_TYPE_FIXED = 0;

    /*Tax*/
    const TAX = 0.01;

    public static function computeAmount($price, $quantity)
    {
        $amount = $price * $quantity;
        return $amount;
    }
    
    public static function computeDiscount($amount, $discount, $type)
    {
        if ($type == self::DISCOUNT_TYPE_PERCENTAGE){
            $discountAmount = $amount * ($discount / 100);
        } else if ($type == self::DISCOUNT_TYPE_FIXED){
            $discountAmount = $discount;
        }
        return $discountAmount;
    }

	public static function computeSubtotal($amount, $discount, $type = null)
    {
        /* Discount amount was already computed */
        if ($type == null){
            $subTotal = $amount - $discount;
        } else{
            /* Discount amount was not yet computed */
            if ($type == self::DISCOUNT_TYPE_PERCENTAGE){
                $subTotal = $amount - ($amount * ($discount / 100));
            } else if ($type == self::DISCOUNT_TYPE_FIXED){
                $subTotal = $amount - $discount;
            }
        }
        return $subTotal;
    }

    public static function computeTax($subTotal)
    {
        $tax = $subTotal * self::TAX;
        return $tax;
    }

    public static function computeTotalAmount($subTotal, $tax)
    {
        $totalAmount = $subTotal + $tax;
        return $totalAmount;
    }

    public static function computeTotalAmountBySubTotal($subTotal)
    {
        $tax = self::computeTax($subTotal);
        $totalAmount = $subTotal + $tax;
        return $totalAmount;
    }

    public static function paymentCompute($price, $quantity, $discount = null, $type = null)
    {
        $discountAmount = 0;
        /* Compute Amount */
        $amount = self::computeAmount($price, $quantity);
        if ($discount){
            /* Compute Discount */
            $discountAmount = self::computeDiscount($amount, $discount, $type);
        }
        /* Compute Subtotal */
        $subTotal = self::computeSubtotal($amount, $discountAmount);
        /* Compute Tax */
        $tax = 0;
        /* Compute TotalAmount */
        $totalAmount = self::computeTotalAmount($subTotal, $tax);

        return array(
            'amount' => $amount,
            'discountAmount' => $discountAmount,
            'subTotal' => $subTotal,
            'taxAmount' => $tax,
            'totalAmount' => $totalAmount
        );
    }

    public static function paymentComputeAllReportType($id){
        try{
            $report = Entity::where('entityid', $id)->select('province', 'current_bank', 'countrycode', 'loginid')->first(); // get report
            $bank = Bank::where('id', $report->current_bank)->first(); // get bank details

            $simplifiedPrice = 0;
            $standalonePrice = 0;
            $premiumPrice = 0;

            if($report->countrycode == "PH"){
                $provCode = Province2::where('provDesc', $report->province)->pluck('regCode')->first(); //get province code for filters

                /** PREMIUM REPORT */
                if ($provCode == 13) {                                      // Metro Manila
                    $premiumPrice = $bank->premium_zone1_price;
                }elseif(in_array($provCode, array(04,03))){                 // Laguna, Rizal, Cavite and Bulacan
                    $premiumPrice = $bank->premium_zone2_price;
                }else{                                                      // Rest of the Regions
                    $premiumPrice = $bank->premium_zone3_price;
                }

                /** STANDALONE REPORT */
                $standalonePrice = $bank->standalone_price;

                /** SIMPLIFIED REPORT */
                $simplifiedPrice = $bank->simplified_price;
            }else{
                $simplifiedPrice = $bank->simplified_price;
                $standalonePrice = $bank->standalone_price;
                $premiumPrice = $bank->premium_zone1_price; // this will be deprecated
            }

            $user = User::where('loginid', $report->loginid)->first();

            // Custom Report Price
            if(($bank->is_custom == 1) && ($user->is_contractor != 1)){
                $simplifiedPrice = $bank->custom_price;
                $standalonePrice = $bank->custom_price;
                $premiumPrice = $bank->custom_price;
            }
            
            return array(
                'simplified'	=> $simplifiedPrice,
                'standalone'	=> $standalonePrice,
                'premium'		=> $premiumPrice,
                'is_custom'		=> $bank->is_custom,
                'custom_price'  => $bank->custom_price
            );
        }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
                Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
        }
    }
}