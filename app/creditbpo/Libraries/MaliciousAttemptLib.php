<?php

namespace CreditBPO\Libraries;

use Request;

use Illuminate\Http\RedirectResponse;
use CreditBPO\Models\UserLoginAttempt;
use CreditBPO\Models\User2faAttempt;

//======================================================================
//  Class #: Malicious Attempt Helper
//  Malicious Attempt Library Classes
//======================================================================
class MaliciousAttemptLib
{
    public static function getClientCountry()
    {
        /*Get IP Address*/
        $ip = $_SERVER['REMOTE_ADDR'];
        
        if (env("APP_ENV") != "local")
        {
            /*Get public ip address*/
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            $ip = $m[1];
        }
        
        /*Set default location if locators doesn't work*/
        $location = array(
            'countrycode' => 'PH',
            /*Add more fields here if necessary*/
        );

        /*Get location using ipstack*/
        $ipStackLocation = self::useIpStack($ip);
        /*If it doesn't work use ipinfo*/
        if (is_null($ipStackLocation)){
            $ipInfoLocation = self::useIpInfo($ip);
            if (!is_null($ipInfoLocation)){
                $location = $ipInfoLocation;
            }
        } else{
            $location = $ipStackLocation;
        }
        return $location;
    }

    public static function useIpStack($ip)
    {
        $api = "http://api.ipstack.com/";
        $accessKey = "f3a9feacf90a59fb293068afba8cd292";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $api.$ip.'?access_key='.$accessKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $geoData = json_decode($response, true);
        if (!is_null($geoData['country_code'])){
            $data = array(
                'countrycode' => $geoData['country_code']
                /*Add more fields here if necessary*/
            );
            return $data;
        } else{
            return null;
        }
    }

    public static function useIpInfo($ip)
    {
        $api = "http://ipinfo.io/";
        $accessKey = "d81632f9cff113";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $api.$ip.'?token='.$accessKey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $geoData = json_decode($response, true);
        if (!$geoData['bogon']){
            $data = array(
                'countrycode' => $geoData['country']
                /*Add more fields here if necessary*/
            );
            return $data;
        } else{
            return null;
        }
    }
}