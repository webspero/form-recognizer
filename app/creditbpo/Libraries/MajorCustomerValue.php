<?php

namespace CreditBPO\Libraries;

class MajorCustomerValue
{

    CONST MC_RELATIONSHIP_SATISFACTION_UNDECIDED = 0;
    CONST MC_RELATIONSHIP_SATISFACTION_GOOD = 1;
    CONST MC_RELATIONSHIP_SATISFACTION_SATISFACTORY = 2;
    CONST MC_RELATIONSHIP_SATISFACTION_UNSATISFACTORY = 3;
    CONST MC_RELATIONSHIP_SATISFACTION_BAD = 4;

    CONST MC_CUSTOMER_ORDER_FREQUENCY_REGULARLY = "mcof_regularly";
    CONST MC_CUSTOMER_ORDER_FREQUENCY_OFTEN = "mcof_often";
    CONST MC_CUSTOMER_ORDER_FREQUENCY_RARELY = "mcof_rarely";

    CONST MC_CUSTOMER_SETTLEMENT_AHEAD = 1;
    CONST MC_CUSTOMER_SETTLEMENT_ON_DUE = 2;
    CONST MC_CUSTOMER_SETTLEMENT_PORTION_SETTLED = 3;
    CONST MC_CUSTOMER_SETTLEMENT_SETTLED_PAST = 4;
    CONST MC_CUSTOMER_SETTLEMENT_DELINQUENT = 5;

    public $relationshipSatisfaction = array(
        'good' => self::MC_RELATIONSHIP_SATISFACTION_GOOD,
        'satisfactory' => self::MC_RELATIONSHIP_SATISFACTION_SATISFACTORY,
        'unsatisfactory' => self::MC_RELATIONSHIP_SATISFACTION_UNSATISFACTORY,
        'bad' => self::MC_RELATIONSHIP_SATISFACTION_BAD
    );

    public $customerOrderFrequency = array(
        'regularly' => self::MC_CUSTOMER_ORDER_FREQUENCY_REGULARLY,
        'often' => self::MC_CUSTOMER_ORDER_FREQUENCY_OFTEN,
        'rarely' => self::MC_CUSTOMER_ORDER_FREQUENCY_RARELY
    );

    public $customerSettlement = array(
        'ahead of due date' => self::MC_CUSTOMER_SETTLEMENT_AHEAD,
        'on due date' => self::MC_CUSTOMER_SETTLEMENT_ON_DUE,
        'portion settled on due date' => self::MC_CUSTOMER_SETTLEMENT_PORTION_SETTLED,
        'settled past due date' => self::MC_CUSTOMER_SETTLEMENT_SETTLED_PAST,
        'delinquent' => self::MC_CUSTOMER_SETTLEMENT_DELINQUENT
    );

    /*Convert relatinship satisfaction from database value to response value */
    public function relationshipSatisfactionToResponse($mcRelationshipSatisfaction)
    {
        return array_search($mcRelationshipSatisfaction, $this->relationshipSatisfaction);
    }

    /*Convert relationship satisfaction from request value to database value */
    public function relationshipSatisfactionToDatabase($mcRelationshipSatisfaction)
    {
        return $this->relationshipSatisfaction[$mcRelationshipSatisfaction];
    }

    /*Convert customer order frequency from database value to response value */
    public function customerOrderFrequencyToResponse($mcCustomerOrderFrequency)
    {
        return array_search($mcCustomerOrderFrequency, $this->customerOrderFrequency);
    }

    /*Convert customer order frequency from request value to database value */
    public function customerOrderFrequencyToDatabase($mcCustomerOrderFrequency)
    {
        return $this->customerOrderFrequency[$mcCustomerOrderFrequency];
    }

    /*Convert customer settlement from database value to response value */
    public function customerSettlementToResponse($mcCustomerSettlement)
    {
        return array_search($mcCustomerSettlement, $this->customerSettlement);
    }

    /*Convert customer settlement from request value to database value */
    public function customerSettlementToDatabase($mcCustomerSettlement)
    {
        return $this->customerSettlement[$mcCustomerSettlement];
    }
}