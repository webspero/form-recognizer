<?php

namespace CreditBPO\Libraries;

class MimeTypes
{
    /* It doesn't contains all the MIME Types.
    * Its main purpose is to check the file type sent in api */
    public $mimeTypes = array(
        /* Image */
        'image/jpeg' => 'jpeg',
        'image/png' => 'png',
        'image/svg+xml' => 'svg',

        /* Application */
        'application/vnd.ms-excel' => 'xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet	' => 'xlsx',
        'application/zip' => 'zip',
        'application/x-rar-compressed' => 'rar',
        'application/pdf' => 'pdf',

        /* Text */
        'text/csv' => 'csv'
    );
}