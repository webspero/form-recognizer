<?php

namespace CreditBPO\Libraries;

class StringManipulator
{
    //This function removes whitespaces and removes special characters.
    public static function cleanString($string){
        $string = trim($string); //str_replace(' ', '', $string); // Removes extra spaces.
        $string = preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
        return strtolower(preg_replace('/-+/', '-', $string)); // Replaces multiple hyphens with single one.
    }

}