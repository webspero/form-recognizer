<?php

namespace CreditBPO\Libraries;

//======================================================================
//  Class #: payment Preparation Library
//      EULA and Calculations for Preparing Payment URLs and Handling Response URLs
//======================================================================
class PaymentPrepLib {
	const VAT = 0.12;

    //---------------------------------------------------------------
	// Function #.a. Prepare EULA
	//---------------------------------------------------------------
    public static function getEULA()
    {
        $eula = array(
			"greeting" => "DEAR SUBSCRIBER:",
			"body" => array(
				"Your subscription to CreditBPO’s rating  service grants CreditBPO the express authority to conduct a confirmation and validation of the financial, management, and business  information supplied to it by your company  through our online data collection platform.",
				"Further, all the information collected through our website and related analyses and reports generated thereof in conformity with the aforementioned subscription will be treated as strictly CONFIDENTIAL and subject to a full NON-DISCLOSURE restriction to third parties.",
				"We trust that you have a full understanding of the intent of our company to fulfill our commitment to undertake the services as agreed upon using reliable and accurate data. The data you provide will be the basis for generating your CreditBPO Rating Report, a self risk assessment tool that meets best industry credit practices and regulatory standards."
			),
			"close" => "We're on your side,",
			"signature" => "The CreditBPO Team"
		);

        return $eula;
	}

	//---------------------------------------------------------------
	// Function #.b. Calculate Total Amount and Discount Amount
	//---------------------------------------------------------------
	public static function getAmountAfterDiscount($faceAmount, $discountType, $discountDetailAmount) 
	{
		$discountAmount = 0;
		$totalAmount = 3500;
		
		if($discountType === 1) {
			/* Percentage Type Discount */
			$discountAmount = $faceAmount * ($discountDetailAmount / 100);
		} else {
			/* Fixed Value Type Discount */
			$discountAmount = $discountDetailAmount;
		}

		$totalAmount = $faceAmount - $discountAmount;

		return [
			"discountAmount" => $discountAmount,
			"totalAmount" => $totalAmount
		];
	}

	//---------------------------------------------------------------
	// Function #.c. Calculate VAT Output, Vatable Amount, Total Price/Amount before Discount, if any 
	//---------------------------------------------------------------
	public static function getBaseAmounts($basePrice)
	{
		$amount = $basePrice;

		$vatableAmount = $amount / (1 + self::VAT);
		$vatOutput = $amount - $vatableAmount;

		// $vatOutput = $basePrice * self::VAT;
		// $vatableAmount = $basePrice;

		// $totalPrice = $amount + ($amount * self::VAT);
		$totalPrice = $vatOutput + $vatableAmount;
		$amount = $totalPrice;

		return array(
			"vatAmount" => $vatOutput,
			"vatableAmount" => $vatableAmount,
			"amount" => $amount
		);
	}

	//---------------------------------------------------------------
	// Function #.d. Calculate VAT Output, Vatable Amount, Total Price/Amount after Discount
	//---------------------------------------------------------------
	public static function getVatableAfterTax($baseAmounts, $discountDetails, $basePrice) 
	{
		$amount = $baseAmounts['amount'];
		$vatAmount = $baseAmounts['vatAmount'];
		$vatableAmount = $baseAmounts['vatableAmount'];

		$discountType = $discountDetails['type'];
		$discountAmount = $discountDetails['amount'];

		if($discountType == 1) {
			/* Percentage Type Discount */
			// $discountAmount = $discountAmount / 100;
			// $amount = $amount - ($amount * $discountAmount);

			// $vatAmount = $vatAmount - ($vatAmount * $discountAmount);
			// $vatableAmount = $basePrice - ($basePrice * $discountAmount);
			$discountAmount = $discountAmount / 100;
			$amount = $amount - ($amount * ($discountAmount));

		} else {
			/* Fixed Value Type Discount */
			// $amount = $amount - ($discountAmount + $vatAmount);

			// $vatAmount = $amount * self::VAT;
			// $vatableAmount = $amount;

			// $amount = $vatAmount + $vatableAmount;
			$amount = $amount - $discountAmount;
		}

		$vatableAmount = $amount / (1 + self::VAT);
		$vatAmount = $amount - $vatableAmount;

		$amount = $vatAmount + $vatableAmount;

		return array(
			"amount" => $amount,
			"vatAmount" => 0,
			"vatableAmount" => 0,
			"discountAmount" => $discountAmount
		);
	}

}