<?php

namespace CreditBPO\Libraries;

use DomDocument;
use DomNode;
use DOMXPath;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use \Exception as Exception;

class Scraper {
    private $client;
    private $clientConfig = [];

    public function __construct(array $clientConfig = [], Client $client = null) {
        $this->setClientConfig($clientConfig);
        $this->setClient($client);
    }

    /**
     * @return array
     */
    public function getClientConfig() {
        return $this->clientConfig;
    }

    /**
     * @param array $config
     * @return void
     */
    public function setClientConfig(array $config = []) {
        $this->clientConfig = array_merge(
            Config::get('scraper'),
            $config
        );
    }

    /**
     * @param Client $client
     * @return void
     */
    public function setClient(Client $client = null) {
        if (!$client) {
            $client = new Client($this->getClientConfig());
        }

        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @param string $url
     * @param array $options
     * @param string $method
     * @return Response
     */
    public function fetch($url, $options = [], $method = 'GET') {
        //sleep(rand(1, 5));
        $response = $this->request($method, $url, $options);

        if (!$response || !$response->getBody()) {
            return null;
        }

        return $response;
        //->getBody()->getContents();
    }

    /**
     * @param string $html
     * @return DomDocument
     */
    public function getDomDocument($html) {
        libxml_use_internal_errors(true);
        $document = new DomDocument();
        $document->loadHTML($html);
        return $document;
    }

    /**
     * @param DomDocument $document
     * @param string $selector
     * @param DomNode|null $parent
     * @return DomNode
     */
    public function getNode(DomDocument $document, $selector, DomNode $parent = null) {
        $nodes = $this->getNodes($document, $selector, $parent);

        if (!$nodes) {
            return null;
        }

        return $nodes->item(0);
    }

    /**
     * @param DomDocument $document
     * @param string $selector
     * @param DomNode|null $parent
     * @return DomNodeList
     */
    public function getNodes(DomDocument $document, $selector, DomNode $parent = null) {
        $xpath = new DOMXPath($document);
        $nodes = $xpath->query($selector, $parent);
        return $nodes;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return Response|null;
     */
    protected function request($method, $url, $options = []) {
        try {
            return $this->client->request($method, $url, $options);
        } catch(ConnectException $e) {
            $message = 'Connection exception';
        } catch(BadResponseException $e) {
            $message = 'Bad response';
        } catch (RequestException $e) {
            $message = 'Request exception';
        }

        $message .= ' for scrape request with URL '.$url;
        Log::error($message);
        return null;
    }
}
