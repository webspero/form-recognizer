<?php

namespace CreditBPO\Libraries;

class MajorSupplierValue
{

    CONST MS_RELATIONSHIP_SATISFACTION_GOOD = 1;
    CONST MS_RELATIONSHIP_SATISFACTION_SATUSFACTORY = 2;
    CONST MS_RELATIONSHIP_SATISFACTION_UNSATISFACTORY = 3;
    CONST MS_RELATIONSHIP_SATISFACTION_BAD = 4;

    CONST MS_SUPPLIER_PAYMENT_FREQUENCY_COD = "ms_cod";
    CONST MS_SUPPLIER_PAYMENT_FREQUENCY_MONTHLY = "ms_monthly";
    CONST MS_SUPPLIER_PAYMENT_FREQUENCY_BIMONTHLY = "ms_bimonthly";
    CONST MS_SUPPLIER_PAYMENT_FREQUENCY_WEEKLY = "ms_weekly";
    CONST MS_SUPPLIER_PAYMENT_FREQUENCY_DAILY = "ms_daily";

    CONST MS_SUPPLIER_SETTLEMENT_AHEAD = 1;
    CONST MS_SUPPLIER_SETTLEMENT_ON_DUE = 2;
    CONST MS_SUPPLIER_SETTLEMENT_PORTION_SETTLED = 3;
    CONST MS_SUPPLIER_SETTLEMENT_SETTLED_PAST = 4;
    CONST MS_SUPPLIER_SETTLEMENT_DELINQUENT = 5;

    public $relationshipSatisfaction = array(
        'good' => self::MS_RELATIONSHIP_SATISFACTION_GOOD,
        'satisfactory' => self::MS_RELATIONSHIP_SATISFACTION_SATUSFACTORY,
        'unsatisfactory' => self::MS_RELATIONSHIP_SATISFACTION_UNSATISFACTORY,
        'bad' => self::MS_RELATIONSHIP_SATISFACTION_BAD
    );

    public $supplierPaymentFrequency = array(
        'cod' => self::MS_SUPPLIER_PAYMENT_FREQUENCY_COD,
        'monthly' => self::MS_SUPPLIER_PAYMENT_FREQUENCY_MONTHLY,
        'bimonthly' => self::MS_SUPPLIER_PAYMENT_FREQUENCY_BIMONTHLY,
        'weekly' => self::MS_SUPPLIER_PAYMENT_FREQUENCY_WEEKLY,
        'daily' => self::MS_SUPPLIER_PAYMENT_FREQUENCY_DAILY
    );

    public $supplierSettlement = array(
        'ahead of due date' => self::MS_SUPPLIER_SETTLEMENT_AHEAD,
        'on due date' => self::MS_SUPPLIER_SETTLEMENT_ON_DUE,
        'portion settled on due date' => self::MS_SUPPLIER_SETTLEMENT_PORTION_SETTLED,
        'settled past due date' => self::MS_SUPPLIER_SETTLEMENT_SETTLED_PAST,
        'delinquent' => self::MS_SUPPLIER_SETTLEMENT_DELINQUENT
    );

    /*Convert relatinship satisfaction from database value to response value */
    public function relationshipSatisfactionToResponse($msRelationshipSatisfaction)
    {
        return array_search($msRelationshipSatisfaction, $this->relationshipSatisfaction);
    }

    /*Convert relationship satisfaction from request value to database value */
    public function relationshipSatisfactionToDatabase($msRelationshipSatisfaction)
    {
        return $this->relationshipSatisfaction[$msRelationshipSatisfaction];
    }

    /*Convert supplier payment frequency from database value to response value */
    public function supplierPaymentFrequencyToResponse($msSupplierPaymentFrequency)
    {
        return array_search($msSupplierPaymentFrequency, $this->supplierPaymentFrequency);
    }

    /*Convert supplier payment frequency from request value to database value */
    public function supplierPaymentFrequencyToDatabase($msSupplierPaymentFrequency)
    {
        return $this->supplierPaymentFrequency[$msSupplierPaymentFrequency];
    }

    /*Convert supplier settlement from database value to response value */
    public function supplierSettlementToResponse($msSupplierSettlement)
    {
        return array_search($msSupplierSettlement, $this->supplierSettlement);
    }

    /*Convert supplier settlement from request value to database value */
    public function supplierSettlementToDatabase($msSupplierSettlement)
    {
        return $this->supplierSettlement[$msSupplierSettlement];
    }

}