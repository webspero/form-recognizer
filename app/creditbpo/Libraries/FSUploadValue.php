<?php

namespace CreditBPO\Libraries;

class FSUploadValue
{
    /*FS Document Type*/
    CONST FS_DOCUMENT_TYPE_UNAUDITED = 1;
    CONST FS_DOCUMENT_TYPE_INTERIM = 2;
    CONST FS_DOCUMENT_TYPE_AUDITED = 3;
    CONST FS_DOCUMENT_TYPE_RECASTED = 4;
    
    /*FS Units*/
    CONST FS_UNITS_DEFAULT = 1;
    CONST FS_UNITS_THOUSANDS = 1000;
    CONST FS_UNITS_MILLIONS = 1000000;

    public $documentType = array(
        'un-audited' => self::FS_DOCUMENT_TYPE_UNAUDITED,
        'interim' => self::FS_DOCUMENT_TYPE_INTERIM,
        'audited' => self::FS_DOCUMENT_TYPE_AUDITED,
        'recasted' => self::FS_DOCUMENT_TYPE_RECASTED
    );

    public $unit = array(
        'default' => self::FS_UNITS_DEFAULT,
        'thousands' => self::FS_UNITS_THOUSANDS,
        'millions' => self::FS_UNITS_MILLIONS
    );

    /*Convert document type from database value to response value */
    public function documentTypeToResponse($fsDocumentType)
    {
        return array_search($fsDocumentType, $this->documentType);
    }

    /*Convert document type from request value to database value */
    public function documentTypeToDatabase($fsDocumentType)
    {
        return $this->documentType[$fsDocumentType];
    }

    /*Convert unit from database value to response value */
    public function unitToResponse($fsUnit)
    {
        return array_search($fsUnit, $this->unit);
    }

    /*Convert unit from request value to database value */
    public function unitToDatabase($fsUnit)
    {
        return $this->unit[$fsUnit];
    }
}