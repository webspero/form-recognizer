<?php

namespace CreditBPO\Libraries;

//======================================================================
//  Class #: Business Details Library
//      Lists for Dropdowns that are not in the database
//======================================================================
class BusinessDetailsLib {
    //---------------------------------------------------------------
	// Function #.a. Total Asset Grouping List
	//---------------------------------------------------------------
    public static function getTotalAssetGroupings()
    {
        $tags = array(
			'tag_micro' => array(
                'value' => 4,
                'text' => 'Micro - less than Php 3 million'
            ),
	        'tag_small' => array(
                'value' => 1,
                'text' => 'Small - Php 3 million to Php 15 million'
            ),
	        'tag_med' => array(
                'value' => 2,
                'text' => 'Medium - over Php 15 million to P100 million'
            ),
	        'tag_large' => array(
                'value' => 3,
                'text' => 'Large - over Php 100 million'
            )
		);

        return $tags;
    }

    public $assetGroupings = array(
        4 => 'Micro - less than Php 3 million',
        1 => 'Small - Php 3 million to Php 15 million',
        2 => 'Medium - over Php 15 million to P100 million',
        3=> 'Large - over Php 100 million'
    );
    
    //---------------------------------------------------------------
	// Function #.b. Employee Size List
	//---------------------------------------------------------------
    public static function getEmployeeSizeRanges()
    {
        $tags = array(
			'employee_size_0' => array(
                'value' => 0,
                'text' => 'Less than 10'
            ),
	        'employee_size_1' => array(
                'value' => 1,
                'text' => '11 to 20'
            ),
	        'employee_size_2' => array(
                'value' => 2,
                'text' => '21 to 50'
            ),
	        'employee_size_3' => array(
                'value' => 3,
                'text' => '51 and up'
            )
		);

        return $tags;
    }

    public $employeeSizeRanges = array(
        0 => 'Less than 10',
        1 => '11 to 20',
        2 => '21 to 50',
        3=> '51 and up'
    );
    
    //---------------------------------------------------------------
	// Function #.b. Business Types List
	//---------------------------------------------------------------
    public static function getBusinessTypes()
    {
        $tags = array(
			'corporation' => array(
                'value' => COMPANY_TYPE_CORPORATE,
                'text' => 'Corporate'
            ),
	        'sole_proprietorship' => array(
                'value' => COMPANY_TYPE_SOLE_PROP,
                'text' => 'Sole Proprietorship'
            )
		);

        return $tags;
    }

    public $businessTypes = array(
        COMPANY_TYPE_CORPORATE => 'corporation',
        COMPANY_TYPE_SOLE_PROP => 'sole_proprietorship'
    );

    //---------------------------------------------------------------
	// Function #.b. Get Report Types
	//---------------------------------------------------------------
    public static function getReportTypes()
    {
        $types = array(
            'standalone_report' => array(
                'value' => STANDALONE_REPORT,
                'test'  => 'Standalone Report'
            ),
            'premium_report' => array(
                'value' => PREMIUM_REPORT,
                'text'  => 'Premium Report'
            ),
            'simplified_report' => array(
                'value' => SIMPLIFIED_REPORT,
                'text'  => 'Simplified Report'
            )
        );
        return $types;
    }

    //---------------------------------------------------------------
	// Function #.b. Get Units
    //---------------------------------------------------------------
    public static function getUnits()
    {
        $units = array(
            'units' => array(
                'default',
                'thousands',
                'millions'
            )
        );
        return $units;
    }
}