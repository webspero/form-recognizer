<?php

namespace CreditBPO\DTO;

class CashFlowDTO 
{
    protected $financialReportId;
    protected $indexCount;
    protected $amortization;
    protected $depreciation;
    protected $interestExpense;
    protected $nonCashExpense;
    protected $netOperatingIncome;
    protected $principalPayments;
    protected $interestPayments;
    protected $leasePayments;
    protected $debtServiceCapacity;
    protected $debtServiceCapacityRatio;
    protected $netCashByOperatingActivities;
    protected $depreciationExpense;
    protected $amortisationExpense;
    protected $depreciationAndAmortisationExpense;
    protected $netCashFlowFromOperations;

    /**
     * Get the value of financialReportId
     */ 
    public function getFinancialReportId()
    {
        return $this->financialReportId;
    }

    /**
     * Set the value of financialReportId
     *
     * @return  self
     */ 
    public function setFinancialReportId($financialReportId)
    {
        $this->financialReportId = $financialReportId;

        return $this;
    }

    /**
     * Get the value of indexCount
     */ 
    public function getIndexCount()
    {
        return $this->indexCount;
    }

    /**
     * Set the value of indexCount
     *
     * @return  self
     */ 
    public function setIndexCount($indexCount)
    {
        $this->indexCount = $indexCount;

        return $this;
    }

    /**
     * Get the value of amortization
     */ 
    public function getAmortization()
    {
        return $this->amortization;
    }

    /**
     * Set the value of amortization
     *
     * @return  self
     */ 
    public function setAmortization($amortization)
    {
        $this->amortization = $amortization;

        return $this;
    }

    /**
     * Get the value of depreciation
     */ 
    public function getDepreciation()
    {
        return $this->depreciation;
    }

    /**
     * Set the value of depreciation
     *
     * @return  self
     */ 
    public function setDepreciation($depreciation)
    {
        $this->depreciation = $depreciation;

        return $this;
    }

    /**
     * Get the value of interestExpense
     */ 
    public function getInterestExpense()
    {
        return $this->interestExpense;
    }

    /**
     * Set the value of interestExpense
     *
     * @return  self
     */ 
    public function setInterestExpense($interestExpense)
    {
        $this->interestExpense = $interestExpense;

        return $this;
    }

    /**
     * Get the value of nonCashExpense
     */ 
    public function getNonCashExpense()
    {
        return $this->nonCashExpense;
    }

    /**
     * Set the value of nonCashExpense
     *
     * @return  self
     */ 
    public function setNonCashExpense($nonCashExpense)
    {
        $this->nonCashExpense = $nonCashExpense;

        return $this;
    }

    /**
     * Get the value of netOperatingIncome
     */ 
    public function getNetOperatingIncome()
    {
        return $this->netOperatingIncome;
    }

    /**
     * Set the value of netOperatingIncome
     *
     * @return  self
     */ 
    public function setNetOperatingIncome($netOperatingIncome)
    {
        $this->netOperatingIncome = $netOperatingIncome;

        return $this;
    }

    /**
     * Get the value of principalPayments
     */ 
    public function getPrincipalPayments()
    {
        return $this->principalPayments;
    }

    /**
     * Set the value of principalPayments
     *
     * @return  self
     */ 
    public function setPrincipalPayments($principalPayments)
    {
        $this->principalPayments = $principalPayments;

        return $this;
    }

    /**
     * Get the value of interestPayments
     */ 
    public function getInterestPayments()
    {
        return $this->interestPayments;
    }

    /**
     * Set the value of interestPayments
     *
     * @return  self
     */ 
    public function setInterestPayments($interestPayments)
    {
        $this->interestPayments = $interestPayments;

        return $this;
    }

    /**
     * Get the value of leasePayments
     */ 
    public function getLeasePayments()
    {
        return $this->leasePayments;
    }

    /**
     * Set the value of leasePayments
     *
     * @return  self
     */ 
    public function setLeasePayments($leasePayments)
    {
        $this->leasePayments = $leasePayments;

        return $this;
    }

    /**
     * Get the value of debtServiceCapacity
     */ 
    public function getDebtServiceCapacity()
    {
        return $this->debtServiceCapacity;
    }

    /**
     * Set the value of debtServiceCapacity
     *
     * @return  self
     */ 
    public function setDebtServiceCapacity($debtServiceCapacity)
    {
        $this->debtServiceCapacity = $debtServiceCapacity;

        return $this;
    }

    /**
     * Get the value of debtServiceCapacityRatio
     */ 
    public function getDebtServiceCapacityRatio()
    {
        return $this->debtServiceCapacityRatio;
    }

    /**
     * Set the value of debtServiceCapacityRatio
     *
     * @return  self
     */ 
    public function setDebtServiceCapacityRatio($debtServiceCapacityRatio)
    {
        $this->debtServiceCapacityRatio = $debtServiceCapacityRatio;

        return $this;
    }

    /**
     * Get the value of netCashByOperatingActivities
     */ 
    public function getNetCashByOperatingActivities()
    {
        return $this->netCashByOperatingActivities;
    }

    /**
     * Set the value of netCashByOperatingActivities
     *
     * @return  self
     */ 
    public function setNetCashByOperatingActivities($netCashByOperatingActivities)
    {
        $this->netCashByOperatingActivities = $netCashByOperatingActivities;

        return $this;
    }

    /**
     * Get the value of depreciationExpense
     */ 
    public function getDepreciationExpense()
    {
        return $this->depreciationExpense;
    }

    /**
     * Set the value of depreciationExpense
     *
     * @return  self
     */ 
    public function setDepreciationExpense($depreciationExpense)
    {
        $this->depreciationExpense = $depreciationExpense;

        return $this;
    }

    /**
     * Get the value of amortisationExpense
     */ 
    public function getAmortisationExpense()
    {
        return $this->amortisationExpense;
    }

    /**
     * Set the value of amortisationExpense
     *
     * @return  self
     */ 
    public function setAmortisationExpense($amortisationExpense)
    {
        $this->amortisationExpense = $amortisationExpense;

        return $this;
    }

    /**
     * Get the value of depreciationAndAmortisationExpense
     */ 
    public function getDepreciationAndAmortisationExpense()
    {
        return $this->depreciationAndAmortisationExpense;
    }

    /**
     * Set the value of depreciationAndAmortisationExpense
     *
     * @return  self
     */ 
    public function setDepreciationAndAmortisationExpense($depreciationAndAmortisationExpense)
    {
        $this->depreciationAndAmortisationExpense = $depreciationAndAmortisationExpense;

        return $this;
    }

    /**
     * Get the value of netCashFlowFromOperations
     */ 
    public function getNetCashFlowFromOperations()
    {
        return $this->netCashFlowFromOperations;
    }

    /**
     * Set the value of netCashFlowFromOperations
     *
     * @return  self
     */ 
    public function setNetCashFlowFromOperations($netCashFlowFromOperations)
    {
        $this->netCashFlowFromOperations = $netCashFlowFromOperations;

        return $this;
    }

    /**
     * @param array $vars
     * @return CashFlowDTO
     */
    public function setDbVars($vars) 
    {
        if (isset($vars->financial_report_id)) {
            $this->setFinancialReportId($vars->financial_report_id);
        }
        if (isset($vars->index_count)) {
            $this->setIndexCount($vars->index_count);
        }
        if (isset($vars->Amortization)) {
            $this->setAmortization($vars->Amortization);
        }
        if (isset($vars->Depreciation)) {
            $this->setDepreciation($vars->Depreciation);
        }
        if (isset($vars->InterestExpense)) {
            $this->setInterestExpense($vars->InterestExpense);
        }
        if (isset($vars->NonCashExpenses)) {
            $this->setNonCashExpense($vars->NonCashExpenses);
        }
        if (isset($vars->NetOperatingIncome)) {
            $this->setNetOperatingIncome($vars->NetOperatingIncome);
        }
        if (isset($vars->PrincipalPayments)) {
            $this->setPrincipalPayments($vars->PrincipalPayments);
        }
        if (isset($vars->InterestPayments)) {
            $this->setInterestPayments($vars->InterestPayments);
        }
        if (isset($vars->LeasePayments)) {
            $this->setLeasePayments($vars->LeasePayments);
        }
        if (isset($vars->DebtServiceCapacity)) {
            $this->setDebtServiceCapacity($vars->DebtServiceCapacity);
        }
        if (isset($vars->DebtServiceCapacityRatio)) {
            $this->setDebtServiceCapacityRatio($vars->DebtServiceCapacityRatio);
        }
        if (isset($vars->NetCashByOperatingActivities)) {
            $this->setNetCashByOperatingActivities($vars->NetCashByOperatingActivities);
        }
        if (isset($vars->DepreciationExpense)) {
            $this->setDepreciationExpense($vars->DepreciationExpense);
        }
        if (isset($vars->AmortisationExpense)) {
            $this->setAmortisationExpense($vars->AmortisationExpense);
        }
        if (isset($vars->DepreciationAndAmortisationExpense)) {
            $this->setDepreciationAndAmortisationExpense($vars->DepreciationAndAmortisationExpense);
        }
        if (isset($vars->NetCashFlowFromOperations)) {
            $this->setNetCashFlowFromOperations($vars->NetCashFlowFromOperations);
        }

        return $this;
    }
}