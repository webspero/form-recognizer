<?php

namespace CreditBPO\DTO;

class FSUploadDTO 
{
    protected $file;
    protected $fileExtension;
    protected $reportId;
    protected $industryMainId;
    protected $currency;
    protected $unit;
    protected $documentType;
    protected $totalAsset;
    protected $totalAssetGrouping;
    protected $documentGroup = DOCUMENT_GROUP_FS_TEMPLATE;
    protected $uploadType = UPLOAD_TYPE_FS_TEMPLATE;
    protected $zipFilename;
    protected $tempFilename;
    protected $complexDocumentFilename;
    protected $simpleDocumentFilename;
    protected $destinationPath;


    /**
     * Get the value of file
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @return  self
     */ 
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of industryMainId
     */ 
    public function getIndustryMainId()
    {
        return $this->industryMainId;
    }

    /**
     * Set the value of industryMainId
     *
     * @return  self
     */ 
    public function setIndustryMainId($industryMainId)
    {
        $this->industryMainId = $industryMainId;

        return $this;
    }

    /**
     * Get the value of currency
     */ 
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set the value of currency
     *
     * @return  self
     */ 
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get the value of unit
     */ 
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set the value of unit
     *
     * @return  self
     */ 
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get the value of documentType
     */ 
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Set the value of documentType
     *
     * @return  self
     */ 
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get the value of documentGroup
     */ 
    public function getDocumentGroup()
    {
        return $this->documentGroup;
    }

    /**
     * Set the value of documentGroup
     *
     * @return  self
     */ 
    public function setDocumentGroup($documentGroup)
    {
        $this->documentGroup = $documentGroup;

        return $this;
    }

    /**
     * Get the value of uploadType
     */ 
    public function getUploadType()
    {
        return $this->uploadType;
    }

    /**
     * Set the value of uploadType
     *
     * @return  self
     */ 
    public function setUploadType($uploadType)
    {
        $this->uploadType = $uploadType;

        return $this;
    }

    /**
     * Get the value of zipFilename
     */ 
    public function getZipFilename()
    {
        return $this->zipFilename;
    }

    /**
     * Set the value of zipFilename
     *
     * @return  self
     */ 
    public function setZipFilename($zipFilename)
    {
        $this->zipFilename = $zipFilename;

        return $this;
    }

    /**
     * Get the value of tempFilename
     */ 
    public function getTempFilename()
    {
        return $this->tempFilename;
    }

    /**
     * Set the value of tempFilename
     *
     * @return  self
     */ 
    public function setTempFilename($tempFilename)
    {
        $this->tempFilename = $tempFilename;

        return $this;
    }

    /**
     * Get the value of fileExtension
     */ 
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * Set the value of fileExtension
     *
     * @return  self
     */ 
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    /**
     * Get the value of totalAsset
     */ 
    public function getTotalAsset()
    {
        return $this->totalAsset;
    }

    /**
     * Set the value of totalAsset
     *
     * @return  self
     */ 
    public function setTotalAsset($totalAsset)
    {
        $this->totalAsset = $totalAsset;

        return $this;
    }

    /**
     * Get the value of totalAssetGrouping
     */ 
    public function getTotalAssetGrouping()
    {
        return $this->totalAssetGrouping;
    }

    /**
     * Set the value of totalAssetGrouping
     *
     * @return  self
     */ 
    public function setTotalAssetGrouping($totalAssetGrouping)
    {
        $this->totalAssetGrouping = $totalAssetGrouping;

        return $this;
    }

    /**
     * Get the value of complexDocumentFilename
     */ 
    public function getComplexDocumentFilename()
    {
        return $this->complexDocumentFilename;
    }

    /**
     * Set the value of complexDocumentFilename
     *
     * @return  self
     */ 
    public function setComplexDocumentFilename($complexDocumentFilename)
    {
        $this->complexDocumentFilename = $complexDocumentFilename;

        return $this;
    }

    /**
     * Get the value of simpleDocumentFilename
     */ 
    public function getSimpleDocumentFilename()
    {
        return $this->simpleDocumentFilename;
    }

    /**
     * Set the value of simpleDocumentFilename
     *
     * @return  self
     */ 
    public function setSimpleDocumentFilename($simpleDocumentFilename)
    {
        $this->simpleDocumentFilename = $simpleDocumentFilename;

        return $this;
    }

    /**
     * Get the value of destinationPath
     */ 
    public function getDestinationPath()
    {
        return $this->destinationPath;
    }

    /**
     * Set the value of destinationPath
     *
     * @return  self
     */ 
    public function setDestinationPath($destinationPath)
    {
        $this->destinationPath = $destinationPath;

        return $this;
    }

    #region [get array]

    /**
     * @param array $vars
     * @return FSUploadDTO
     */
    public function getVars()
    {
        return array(
            'reportid' => $this->getReportId(),
            'documenttype' => $this->getDocumentType(),
            'basicdocument' => $this->getSimpleDocumentFilename(),
            'fulldocument' => $this->getComplexDocumentFilename(),
            'downloadlink' => route('document.download', ['path' => $this->getDestinationPath(), 'filename' => $this->getZipFilename()])
        );
    }

    #endregion [get array]
}