<?php

namespace CreditBPO\DTO;

class DirectorsDTO {
    public $id;
    public $reportid;
    public $firstname;
    public $middlename;
    public $lastname;
    public $birthdate;
    public $tinno;
    public $address;
    public $nationality;
    public $percentageshare;

    /**
     * Get the value of relatedCompaniesId
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of relatedcompaniesid
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of entityId
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of entityId
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of firstname
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */ 
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of middlename
     */ 
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set the value of middlename
     *
     * @return  self
     */ 
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get the value of lastname
     */ 
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */ 
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of birthdate
     */ 
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set the value of birthdate
     *
     * @return  self
     */ 
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get the value of tinno
     */ 
    public function getTinno()
    {
        return $this->tinno;
    }

    /**
     * Set the value of tinno
     *
     * @return  self
     */ 
    public function setTinno($tinno)
    {
        $this->tinno = $tinno;

        return $this;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of nationality
     */ 
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set the value of nationality
     *
     * @return  self
     */ 
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get the value of percentageShare
     */ 
    public function getPercentageShare()
    {
        return $this->percentageshare;
    }

    /**
     * Set the value of percentageShare
     *
     * @return  self
     */ 
    public function setPercentageShare($percentageShare)
    {
        $this->percentageshare = $percentageShare;

        return $this;
    }

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['firstname'])) {
            $this->setFirstname($vars['firstname']);
        }
        if (isset($vars['middlename'])) {
            $this->setMiddlename($vars['middlename']);
       	}
        if (isset($vars['lastname'])) {
            $this->setLastname($vars['lastname']);
        }
        if (isset($vars['birthdate'])) {
            $this->setBirthdate($vars['birthdate']);
        }
        if (isset($vars['tinno'])) {
            $this->setTinno($vars['tinno']);
        }
        if (isset($vars['address'])) {
            $this->setAddress($vars['address']);
        }
        if (isset($vars['nationality'])) {
            $this->setNationality($vars['nationality']);
        }
        if (isset($vars['percentageshare'])) {
            $this->setPercentageShare($vars['percentageshare']);
        }

	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'firstname' => $this->getFirstname(),
            'middlename' => $this->getMiddlename(),
            'lastname' => $this->getLastname(),
            'birthdate' => $this->getBirthdate(),
            'tinno' => $this->getTinno(),
            'address' => $this->getAddress(),
            'nationality' => $this->getNationality(),
            'percentageshare' => $this->getPercentageShare(),
        );
    }
    
}