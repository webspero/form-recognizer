<?php

namespace CreditBPO\DTO;

class LocationDTO 
{
    protected $province;
    protected $cityId;
    protected $cityName;
    protected $zipCode;
    protected $citiesLink;
    protected $cities;

    /**
     * Get the value of province
     */ 
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set the value of province
     *
     * @return  self
     */ 
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get the value of citiesLink
     */ 
    public function getCitiesLink()
    {
        return $this->citiesLink;
    }

    /**
     * Set the value of citiesLink
     *
     * @return  self
     */ 
    public function setCitiesLink($citiesLink)
    {
        $this->citiesLink = $citiesLink;

        return $this;
    }

    /**
     * Get the value of cityId
     */ 
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set the value of cityId
     *
     * @return  self
     */ 
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get the value of cityName
     */ 
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set the value of cityName
     *
     * @return  self
     */ 
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get the value of zipCode
     */ 
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set the value of zipCode
     *
     * @return  self
     */ 
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @param array $vars
     * @return IndustryDTO
     */
    public function setDropdownVars($vars = []) {
        if (isset($vars['major_area'])) {
            $this->setProvince($vars['major_area']);
        }
        if (isset($vars['id'])) {
            $this->setCityId($vars['id']);
        }
        if (isset($vars['city'])) {
            $this->setCityName($vars['city']);
        }
        if (isset($vars['zip_code'])) {
            $this->setZipCode($vars['zip_code']);
        }
    }

    /**
     * Get values needed for a dropdown list
     * @return array
     */
    public function getProvincesDropdownVars()
    {
        return array(
            'provincename' => $this->getProvince(),
            'citieslink' => $this->getCitiesLink()
        );
    }

    /**
     * Get values needed for a dropdown list
     * @return array
     */
    public function getCitiesDropdownVars()
    {
        return array(
            'id' => $this->getCityId(),
            'city' => $this->getCityName(),
            'zipcode' => $this->getZipCode()
        );
    }

    /**
     * Get the value of cities
     */ 
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set the value of cities
     *
     * @return  self
     */ 
    public function setCities($cities)
    {
        $this->cities = $cities;

        return $this;
    }
}