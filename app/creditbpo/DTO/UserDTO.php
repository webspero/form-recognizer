<?php

namespace CreditBPO\DTO;

class UserDTO {
    protected $userId; /* login id */
    protected $email;
    protected $firstName;
    protected $lastName;
    protected $role;
    protected $status;

    #region [getter & setter]

    /**
     * Get the value of userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }
    
    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of firstName
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    #endregion [getter & setter]

    #region [set array]

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['loginid'])) {
            $this->setUserId($vars['loginid']);
        }

        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }

        if (isset($vars['firstname'])) {
            $this->setFirstName($vars['firstname']);
        }

        if (isset($vars['lastname'])) {
            $this->setLastName($vars['lastname']);
        }

        if (isset($vars['role'])) {
            $this->setRole($vars['role']);
        }

        if (isset($vars['status'])) {
            $this->setStatus($vars['status']);
        }
    }

    #endregion [set array]

    #region [get array]

    /**
     * @param array $vars
     * @return UserAccountDTO
     */
    public function getVars()
    {
        return array(
            'userid' => $this->getUserId(),
            'email' => $this->getEmail(),
            'firstname' => $this->getFirstName(),
            'lastname' => $this->getLastName(),
            'role' => $this->getRole(),
            'status' => $this->getStatus()
        );
    }

    #endregion [get array]
}