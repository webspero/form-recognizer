<?php

namespace CreditBPO\DTO;

class ReportDTO 
{
    protected $reportId;
    protected $userId;
    protected $companyName;
    protected $companyEmail;
    protected $businessType;
    protected $companyWebsite;
    protected $dateEstablished;
    protected $city;
    protected $industry;
    protected $status;
    protected $link = "/reports/";
    protected $clientKey;
    protected $keyType;
    protected $discountCode;
    protected $associatedOrganization;
    protected $useOldData;
    protected $previousReport;
    protected $isAgree;
    protected $isFreeTrial;
    protected $isPaid;
    protected $owners;
    protected $errors;
    protected $whoFillUpFS;
    protected $outstandingContracts;

    /* Business Details */
    protected $companyTIN;
    protected $secRegistrationNumber;
    protected $secRegistrationDate;
    protected $mainIndustry;
    protected $subIndustry;
    protected $totalAssets;
    protected $totalAssetsGrouping;
    protected $roomBldgSt;
    protected $province;
    protected $zipcode;
    protected $telephoneNumber;
    protected $numberOfYearsInPresentAddress;
    protected $numberOfYearsInBusiness;
    protected $companyDescription;
    protected $employeeSize;
    protected $updatedDate;
    protected $numberOfYearsManagementIsEngaged;

    /* Progress and Incomplete Summary */
    protected $progress;
    protected $incompleteSummary;
    protected $financialStatements;
    protected $bankStatementCount;
    protected $utilityBillCount;
    protected $majorCustomerCount;
    protected $majorSupplierCount;
    protected $balanceSheetPeriodCount;
    protected $incomeStatementPeriodCount;

    /**
     * @param array $vars
     */
    public function __construct() {
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of companyName
     */ 
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set the value of companyName
     *
     * @return  self
     */ 
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get the value of dateEstablished
     */ 
    public function getDateEstablished()
    {
        return $this->dateEstablished;
    }

    /**
     * Set the value of dateEstablished
     *
     * @return  self
     */ 
    public function setDateEstablished($dateEstablished)
    {
        $this->dateEstablished = $dateEstablished;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of industry
     */ 
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set the value of industry
     *
     * @return  self
     */ 
    public function setIndustry($industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
    
    /**
     * Get the value of link
     */ 
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set the value of link
     *
     * @return  self
     */ 
    public function setLink($route, $link)
    {
        $this->link = $route."/".$link;

        return $this;
    }

    /**
     * Get the value of companyEmail
     */ 
    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    /**
     * Set the value of companyEmail
     *
     * @return  self
     */ 
    public function setCompanyEmail($companyEmail)
    {
        $this->companyEmail = $companyEmail;

        return $this;
    }

    /**
     * Get the value of businessType
     */ 
    public function getBusinessType()
    {
        return $this->businessType;
    }

    /**
     * Set the value of businessType
     *
     * @return  self
     */ 
    public function setBusinessType($businessType)
    {
        if(is_array($businessType)) {
            $this->businessType = $businessType;
        } else {
            $this->businessType = strtolower($businessType);
        }


        return $this;
    }

    /**
     * Get the value of companyWebsite
     */ 
    public function getCompanyWebsite()
    {
        return $this->companyWebsite;
    }

    /**
     * Set the value of companyWebsite
     *
     * @return  self
     */ 
    public function setCompanyWebsite($companyWebsite)
    {
        $this->companyWebsite = $companyWebsite;

        return $this;
    }

    /**
     * Get the value of clientKey
     */ 
    public function getClientKey()
    {
        return $this->clientKey;
    }

    /**
     * Set the value of clientKey
     *
     * @return  self
     */ 
    public function setClientKey($clientKey)
    {
        $this->clientKey = $clientKey;

        return $this;
    }

    /**
     * Get the value of discountCode
     */ 
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * Set the value of discountCode
     *
     * @return  self
     */ 
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * Get the value of associatedOrganization
     */ 
    public function getAssociatedOrganization()
    {
        return $this->associatedOrganization;
    }

    /**
     * Set the value of associatedOrganization
     *
     * @return  self
     */ 
    public function setAssociatedOrganization($associatedOrganization)
    {
        $this->associatedOrganization = $associatedOrganization;

        return $this;
    }

    /**
     * Get the value of useOldData
     */ 
    public function getUseOldData()
    {
        return $this->useOldData;
    }

    /**
     * Set the value of useOldData
     *
     * @return  self
     */ 
    public function setUseOldData($useOldData)
    {
        $this->useOldData = $useOldData;

        return $this;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get the value of keyType
     */ 
    public function getKeyType()
    {
        return $this->keyType;
    }

    /**
     * Set the value of keyType
     *
     * @return  self
     */ 
    public function setKeyType($keyType)
    {
        $this->keyType = $keyType;

        return $this;
    }
    
    /**
     * Get the value of isAgree
     */ 
    public function getIsAgree()
    {
        return $this->isAgree;
    }

    /**
     * Get the value of reportType
     */ 
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * Set the value of isAgree
     *
     * @return  self
     */ 
    public function setIsAgree($isAgree)
    {
        $this->isAgree = $isAgree;
        
        return $this;
    }

    /**
     * Set the value of reportType
     *
     * @return  self
     */ 
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
        
        return $this;
    }
    
    /**
     * Get the value of isFreeTrial
     */ 
    public function getIsFreeTrial()
    {
        return $this->isFreeTrial;
    }
    
    /**
     * Set the value of isFreeTrial
     *
     * @return  self
     */ 
    public function setIsFreeTrial($isFreeTrial)
    {
        $this->isFreeTrial = $isFreeTrial;
        
        return $this;
    }
    
    /**
     * Get the value of isPaid
     */ 
    public function getIsPaid()
    {
        return $this->isPaid;
    }
    
    /**
     * Set the value of isPaid
     *
     * @return  self
     */ 
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        
        return $this;
    }
    
    /**
     * Get the value of previousReport
     */ 
    public function getPreviousReport()
    {
        return $this->previousReport;
    }
    
    /**
     * Set the value of previousReport
     *
     * @return  self
     */ 
    public function setPreviousReport($previousReport)
    {
        $this->previousReport = $previousReport;
        
        return $this;
    }
    
    /**
     * Get the value of owners
     */ 
    public function getOwners()
    {
        return $this->owners;
    }

    /**
     * Set the value of owners
     *
     * @return  self
     */ 
    public function setOwners($owners)
    {
        $this->owners = $owners;

        return $this;
    }
    
    /**
     * @param array $vars
     * @return ReportDTO
     */
    public function setVars($vars) {
        if (isset($vars->entityid)) {
            $this->setReportId($vars->entityid);
        }
        if (isset($vars->companyname)) {
            $this->setCompanyName($vars->companyname);
        }
        if (isset($vars->date_established)) {
            $this->setDateEstablished($vars->date_established);
        }
        if (isset($vars->citymunDesc)) {
            $this->setCity($vars->citymunDesc);
        }
        if (isset($vars->class_description)) {
            $this->setIndustry($vars->class_description);
        }

        return $this;
    }

    /**
     * Get Dashboard values
     * @return array
     */
    public function getDashboardVars()
    {
        return array(
            'reportnumber' => $this->getReportId(),
            'company' => $this->getCompanyName(),
            'dateestablished' => $this->getDateEstablished(),
            'city' => $this->getCity(),
            'industry' => $this->getIndustry(),
            'status' => $this->getStatus(),
            'link' => $this->getLink()
        );
    }

    /**
     * @param array $vars
     * @return ReportDTO
     */
    public function setRequestVars($vars = [], $userId) {
        if (isset($vars['companyname'])) {
            $this->setCompanyName($vars['companyname']);
        }
        if (isset($vars['businesstype'])) {
            $this->setBusinessType(studly_case($vars['businesstype']));
        }
        if (isset($vars['clientkey'])) {
            $this->setClientKey($vars['clientkey']);
        }
        if (isset($vars['discountcode'])) {
            $this->setDiscountCode($vars['discountcode']);
        }
        if (isset($vars['organizationid'])) {
            $this->setAssociatedOrganization($vars['organizationid']);
        }
        if (isset($vars['useprevious'])) {
            $this->setUseOldData($vars['useprevious']);
        }
        if (isset($vars['previousreportnumber'])) {
            $this->setPreviousReport($vars['previousreportnumber']);
        }
        if (isset($vars['isagree'])) {
            $this->setIsAgree($vars['isagree']);
        }
        if (isset($vars['reporttype'])) {
            $this->setReportType($vars['reporttype']);
        }
        if (isset($vars['province'])) {
            $this->setProvince($vars['province']);
        }
        if (isset($vars['city'])) {
            $this->setCity($vars['city']);
        }
        if (isset($vars['mainindustry'])) {
            $this->setMainIndustry($vars['mainindustry']);
        }
        if (isset($vars['subindustry'])) {
            $this->setSubIndustry($vars['subindustry']);
        }
        if (isset($vars['industry'])) {
            $this->setIndustry($vars['industry']);
        }
        
        $this->setUserId($userId);
        return $this;
    }

    /**
     * Get the value of companyTIN
     */ 
    public function getCompanyTIN()
    {
        return $this->companyTIN;
    }

    /**
     * Set the value of companyTIN
     *
     * @return  self
     */ 
    public function setCompanyTIN($companyTIN)
    {
        $this->companyTIN = $companyTIN;

        return $this;
    }

    /**
     * Get the value of secRegistrationNumber
     */ 
    public function getSecRegistrationNumber()
    {
        return $this->secRegistrationNumber;
    }

    /**
     * Set the value of secRegistrationNumber
     *
     * @return  self
     */ 
    public function setSecRegistrationNumber($secRegistrationNumber)
    {
        $this->secRegistrationNumber = $secRegistrationNumber;

        return $this;
    }

    /**
     * Get the value of secRegistrationDate
     */ 
    public function getSecRegistrationDate()
    {
        return $this->secRegistrationDate;
    }

    /**
     * Set the value of secRegistrationDate
     *
     * @return  self
     */ 
    public function setSecRegistrationDate($secRegistrationDate)
    {
        $this->secRegistrationDate = $secRegistrationDate;

        return $this;
    }

    /**
     * Get the value of mainIndustry
     */ 
    public function getMainIndustry()
    {
        return $this->mainIndustry;
    }

    /**
     * Set the value of mainIndustry
     *
     * @return  self
     */ 
    public function setMainIndustry($mainIndustry)
    {
        $this->mainIndustry = $mainIndustry;

        return $this;
    }

    /**
     * Get the value of subIndustry
     */ 
    public function getSubIndustry()
    {
        return $this->subIndustry;
    }

    /**
     * Set the value of subIndustry
     *
     * @return  self
     */ 
    public function setSubIndustry($subIndustry)
    {
        $this->subIndustry = $subIndustry;

        return $this;
    }

    /**
     * Get the value of totalAssets
     */ 
    public function getTotalAssets()
    {
        return $this->totalAssets;
    }

    /**
     * Set the value of totalAssets
     *
     * @return  self
     */ 
    public function setTotalAssets($totalAssets)
    {
        $this->totalAssets = $totalAssets;

        return $this;
    }

    /**
     * Get the value of totalAssetsGrouping
     */ 
    public function getTotalAssetsGrouping()
    {
        return $this->totalAssetsGrouping;
    }

    /**
     * Set the value of totalAssetsGrouping
     *
     * @return  self
     */ 
    public function setTotalAssetsGrouping($totalAssetsGrouping)
    {
        $this->totalAssetsGrouping = $totalAssetsGrouping;

        return $this;
    }

    /**
     * Get the value of roomBldgSt
     */ 
    public function getRoomBldgSt()
    {
        return $this->roomBldgSt;
    }

    /**
     * Set the value of roomBldgSt
     *
     * @return  self
     */ 
    public function setRoomBldgSt($roomBldgSt)
    {
        $this->roomBldgSt = $roomBldgSt;

        return $this;
    }

    /**
     * Get the value of province
     */ 
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set the value of province
     *
     * @return  self
     */ 
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get the value of zipcode
     */ 
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set the value of zipcode
     *
     * @return  self
     */ 
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get the value of telephoneNumber
     */ 
    public function getTelephoneNumber()
    {
        return $this->telephoneNumber;
    }

    /**
     * Set the value of telephoneNumber
     *
     * @return  self
     */ 
    public function setTelephoneNumber($telephoneNumber)
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * Get the value of numberOfYearsInPresentAddress
     */ 
    public function getNumberOfYearsInPresentAddress()
    {
        return $this->numberOfYearsInPresentAddress;
    }

    /**
     * Set the value of numberOfYearsInPresentAddress
     *
     * @return  self
     */ 
    public function setNumberOfYearsInPresentAddress($numberOfYearsInPresentAddress)
    {
        $this->numberOfYearsInPresentAddress = $numberOfYearsInPresentAddress;

        return $this;
    }

    /**
     * Get the value of numberOfYearsInBusiness
     */ 
    public function getNumberOfYearsInBusiness()
    {
        return $this->numberOfYearsInBusiness;
    }

    /**
     * Set the value of numberOfYearsInBusiness
     *
     * @return  self
     */ 
    public function setNumberOfYearsInBusiness($numberOfYearsInBusiness)
    {
        $this->numberOfYearsInBusiness = $numberOfYearsInBusiness;

        return $this;
    }

    /**
     * Get the value of companyDescription
     */ 
    public function getCompanyDescription()
    {
        return $this->companyDescription;
    }

    /**
     * Set the value of companyDescription
     *
     * @return  self
     */ 
    public function setCompanyDescription($companyDescription)
    {
        $this->companyDescription = $companyDescription;

        return $this;
    }

    /**
     * Get the value of employeeSize
     */ 
    public function getEmployeeSize()
    {
        return $this->employeeSize;
    }

    /**
     * Set the value of employeeSize
     *
     * @return  self
     */ 
    public function setEmployeeSize($employeeSize)
    {
        $this->employeeSize = $employeeSize;

        return $this;
    }

    /**
     * Get the value of updatedDate
     */ 
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set the value of updatedDate
     *
     * @return  self
     */ 
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get the value of numberOfYearsManagementIsEngaged
     */ 
    public function getNumberOfYearsManagementIsEngaged()
    {
        return $this->numberOfYearsManagementIsEngaged;
    }

    /**
     * Set the value of numberOfYearsManagementIsEngaged
     *
     * @return  self
     */ 
    public function setNumberOfYearsManagementIsEngaged($numberOfYearsManagementIsEngaged)
    {
        $this->numberOfYearsManagementIsEngaged = $numberOfYearsManagementIsEngaged;

        return $this;
    }

    /**
     * Set the value of who
     *
     * @return  self
     */ 
    public function setWhoFillUpFS($whoFillUpFS)
    {
        $this->whoFillUpFS = $whoFillUpFS;
        return $this;
    }

    /**
     * Get the value of who
     */ 
    public function getWhoFillUpFS()
    {
        return $this->whoFillUpFS;
    }

    /**
     * Set the value of outstanding contracts
     *
     * @return  self
     */ 
    public function setOutstandingContracts($outStandingContracts)
    {
        $this->outstandingContracts = $outStandingContracts;
        return $this;
    }

    /**
     * Get the value of outstandong Contracts
     */ 
    public function getOutstandingContracts()
    {
        return $this->outstandingContracts;
    }

    /**
     * @param array $vars
     * @return ReportDTO
     */
    public function setBusinessDetails($vars) {
        /* Business Details */
        if (isset($vars->entityid)) {
            $this->setReportId($vars->entityid);
        }
        if (isset($vars->companyname)) {
            $this->setCompanyName($vars->companyname);
        }
        if (isset($vars->company_tin)) {
            $this->setCompanyTIN($vars->company_tin);
        }
        if (isset($vars->entity_type)) {
            $this->setBusinessType($vars->entity_type);
        }
        if (isset($vars->tin_num)) {
            $this->setSecRegistrationNumber($vars->tin_num);
        }
        if (isset($vars->sec_reg_date)) {
            $this->setSecRegistrationDate($vars->sec_reg_date);
        }
        if (isset($vars->main_code) && isset($vars->main_title)) {
            $mainIndustry = array(
                "value" => $vars->main_code,
                "text" => $vars->main_title
            );
            $this->setMainIndustry($mainIndustry);
        }
        if (isset($vars->industry_sub_id) && isset($vars->sub_title)) {
            $subIndustry = array(
                "value" => $vars->industry_sub_id,
                "text" => $vars->sub_title
            );
            $this->setSubIndustry($subIndustry);
        }
        if (isset($vars->industry_row_id) && isset($vars->row_title)) {
            $industry = array(
                "value" => $vars->industry_row_id,
                "text" => $vars->row_title
            );
            $this->setIndustry($industry);
        }
        if (isset($vars->total_assets)) {
            $this->setTotalAssets($vars->total_assets);
        }
        if (isset($vars->total_asset_grouping)) {
            $this->setTotalAssetsGrouping($vars->total_asset_grouping);
        }
        if (isset($vars->website)) {
            $this->setCompanyWebsite($vars->website);
        }
        if (isset($vars->email)) {
            $this->setCompanyEmail($vars->email);
        }
        if (isset($vars->address1)) {
            $this->setRoomBldgSt($vars->address1);
        }
        if (isset($vars->province)) {
            $this->setProvince($vars->province);
        }
        if (isset($vars->city) && isset($vars->cityid)) {
            $city = array(
                "value" => $vars->cityid,
                "text" => $vars->city
            );
            $this->setCity($city);
        }
        if (isset($vars->zipcode)) {
            $this->setZipcode($vars->zipcode);
        }
        if (isset($vars->phone)) {
            $this->setTelephoneNumber($vars->phone);
        }
        if (isset($vars->no_yrs_present_address)) {
            $this->setNumberOfYearsInPresentAddress($vars->no_yrs_present_address);
        }
        if (isset($vars->date_established)) {
            $this->setDateEstablished($vars->date_established);
        }
        if (isset($vars->number_year)) {
            $this->setNumberOfYearsInBusiness($vars->number_year);
        }
        if (isset($vars->description)) {
            $this->setCompanyDescription($vars->description);
        }
        if (isset($vars->employee_size)) {
            $this->setEmployeeSize($vars->employee_size);
        }
        if (isset($vars->updated_date)) {
            $this->setUpdatedDate($vars->updated_date);
        }
        if (isset($vars->number_year_management_team)) {
            $this->setNumberOfYearsManagementIsEngaged($vars->number_year_management_team);
        }
        if (isset($vars->organizationid) && isset($vars->organizationname)) {
            $associatedOrganization = array(
                "value" => $vars->organizationid,
                "text" => $vars->organizationname
            );
            $this->setAssociatedOrganization($associatedOrganization);
        }

        if (isset($vars->fsInputWho) && isset($vars->fsInputWho)) {
            $this->setWhoFillUpFS($vars->fsInputWho);
        }

        return $this;
    }

    /**
     * Get Business Details
     * @return array
     */
    public function getBusinessDetails()
    {
        return array(
            'companyname' => $this->getCompanyName(),
            'companytin' => $this->getCompanyTIN(),
            'businesstype' => $this->getBusinessType(),
            'registrationnumber' => $this->getSecRegistrationNumber(),
            'registrationdate' => $this->getSecRegistrationDate(),
            'mainindustry' => $this->getMainIndustry(),
            'subindustry' => $this->getSubIndustry(),
            'industry' => $this->getIndustry(),
            'totalassets' => $this->getTotalAssets(),
            'totalassetsgrouping' => $this->getTotalAssetsGrouping(),
            'companywebsite' => $this->getCompanyWebsite(),
            'companyemail' => $this->getCompanyEmail(),
            'primarybusinessaddress' => array (
                'streetaddress' => $this->getRoomBldgSt(),
                'province' => $this->getProvince(),
                'city' => $this->getCity(),
                'zipcode' => $this->getZipcode(),
                'telephonenumber' => $this->getTelephoneNumber()
            ),
            'yearsinpresentaddress' => $this->getNumberOfYearsInPresentAddress(),
            'dateestablished' => $this->getDateEstablished(),
            'yearsinbusiness' => $this->getNumberOfYearsInBusiness(),
            'companydescription' => $this->getCompanyDescription(),
            'employeesize' => $this->getEmployeeSize(),
            'updateddate' => $this->getUpdatedDate(),
            'yearsmanagementisengaged' => $this->getNumberOfYearsManagementIsEngaged(),
            'associatedorganization' => $this->getAssociatedOrganization(),
            'whowillfillupfs'     => $this->getWhoFillUpFS()
        );
    }

    /**
     * @param array $vars
     * @return ReportDTO
     */
    public function setUpdateRequestVars($vars = [], $reportId, $userId) {
        if (isset($vars['companyname'])) {
            $this->setCompanyName($vars['companyname']);
        }
        if (isset($vars['companytin'])) {
            $this->setCompanyTIN($vars['companytin']);
        }
        if (isset($vars['businesstype'])) {
            $this->setBusinessType($vars['businesstype']);
        }
        if (isset($vars['registrationnumber'])) {
            $this->setSecRegistrationNumber($vars['registrationnumber']);
        }
        if (isset($vars['registrationdate'])) {
            $this->setSecRegistrationDate($vars['registrationdate']);
        }
        if (isset($vars['mainindustry'])) {
            $this->setMainIndustry($vars['mainindustry']);
        }
        if (isset($vars['subindustry'])) {
            $this->setSubIndustry($vars['subindustry']);
        }
        if (isset($vars['industry'])) {
            $this->setIndustry($vars['industry']);
        }
        if (isset($vars['totalassets'])) {
            $this->setTotalAssets($vars['totalassets']);
        }
        if (isset($vars['totalassetsgrouping'])) {
            $this->setTotalAssetsGrouping($vars['totalassetsgrouping']);
        }
        if (isset($vars['companywebsite'])) {
            $this->setCompanyWebsite($vars['companywebsite']);
        }
        if (isset($vars['companyemail'])) {
            $this->setCompanyEmail($vars['companyemail']);
        }
        if (isset($vars['streetaddress'])) {
            $this->setRoomBldgSt($vars['streetaddress']);
        }
        if (isset($vars['province'])) {
            $this->setProvince($vars['province']);
        }
        if (isset($vars['city'])) {
            $this->setCity($vars['city']);
        }
        if (isset($vars['zipcode'])) {
            $this->setZipcode($vars['zipcode']);
        }
        if (isset($vars['telephonenumber'])) {
            $this->setTelephoneNumber($vars['telephonenumber']);
        }
        if (isset($vars['yearsinpresentaddress'])) {
            $this->setNumberOfYearsInPresentAddress($vars['yearsinpresentaddress']);
        }
        if (isset($vars['dateestablished'])) {
            $this->setDateEstablished($vars['dateestablished']);
        }
        if (isset($vars['yearsinbusiness'])) {
            $this->setNumberOfYearsInBusiness($vars['yearsinbusiness']);
        }
        if (isset($vars['companydescription'])) {
            $this->setCompanyDescription($vars['companydescription']);
        }
        if (isset($vars['employeesize'])) {
            $this->setEmployeeSize($vars['employeesize']);
        }
        if (isset($vars['updateddate'])) {
            $this->setUpdatedDate($vars['updateddate']);
        }
        // if (isset($vars['yearsmanagementisengaged'])) {
        //     $this->setNumberOfYearsManagementIsEngaged($vars['yearsmanagementisengaged']);
        // }
        if (isset($vars['associatedorganization'])) {
            $this->setAssociatedOrganization($vars['associatedorganization']);
        }

        if (isset($vars['fsInputWho'])) {
            $this->setWhoFillUpFS($vars['fsInputWho']);
        }

        if(isset($vars['outstandingContracts'])){
            $this->setOutstandingContracts($vars['outstandingContracts']);
        }

        $this->setReportId($reportId);
        $this->setUserId($userId);

        return $this;
    }

    /**
     * Get the value of progress
     */ 
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set the value of progress
     *
     * @return  self
     */ 
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get the value of incompleteSummary
     */ 
    public function getIncompleteSummary()
    {
        return $this->incompleteSummary;
    }

    /**
     * Set the value of incompleteSummary
     *
     * @return  self
     */ 
    public function setIncompleteSummary($incompleteSummary)
    {
        $this->incompleteSummary = $incompleteSummary;

        return $this;
    }

    /**
     * Get the value of financialStatements
     */ 
    public function getFinancialStatements()
    {
        return $this->financialStatements;
    }

    /**
     * Set the value of financialStatements
     *
     * @return  self
     */ 
    public function setFinancialStatements($financialStatements)
    {
        $this->financialStatements = $financialStatements;

        return $this;
    }

    /**
     * Get the value of bankStatementCount
     */ 
    public function getBankStatementCount()
    {
        return $this->bankStatementCount;
    }

    /**
     * Set the value of bankStatementCount
     *
     * @return  self
     */ 
    public function setBankStatementCount($bankStatementCount)
    {
        $this->bankStatementCount = $bankStatementCount;

        return $this;
    }

    /**
     * Get the value of utilityBillCount
     */ 
    public function getUtilityBillCount()
    {
        return $this->utilityBillCount;
    }

    /**
     * Set the value of utilityBillCount
     *
     * @return  self
     */ 
    public function setUtilityBillCount($utilityBillCount)
    {
        $this->utilityBillCount = $utilityBillCount;

        return $this;
    }

    /**
     * Get the value of majorCustomerCount
     */ 
    public function getMajorCustomerCount()
    {
        return $this->majorCustomerCount;
    }

    /**
     * Set the value of majorCustomerCount
     *
     * @return  self
     */ 
    public function setMajorCustomerCount($majorCustomerCount)
    {
        $this->majorCustomerCount = $majorCustomerCount;

        return $this;
    }

    /**
     * Get the value of majorSupplierCount
     */ 
    public function getMajorSupplierCount()
    {
        return $this->majorSupplierCount;
    }

    /**
     * Set the value of majorSupplierCount
     *
     * @return  self
     */ 
    public function setMajorSupplierCount($majorSupplierCount)
    {
        $this->majorSupplierCount = $majorSupplierCount;

        return $this;
    }

    /**
     * Get Business Details
     * @return array
     */
    public function getProgressFields()
    {
        return array(
            'companyname' => $this->getCompanyName(),
            'companytin' => $this->getCompanyTIN(),
            'businesstype' => $this->getBusinessType(),
            'registrationnumber' => $this->getSecRegistrationNumber(),
            'registrationdate' => $this->getSecRegistrationDate(),
            //'mainindustry' => $this->getMainIndustry(),
            //'totalassetsgrouping' => $this->getTotalAssetsGrouping(),
            'companyemail' => $this->getCompanyEmail(),
            'streetaddress' => $this->getRoomBldgSt(),
            'province' => $this->getProvince(),
            'city' => $this->getCity(),
            'zipcode' => $this->getZipcode(),
            'telephonenumber' => $this->getTelephoneNumber(),
            //'yearsinpresentaddress' => $this->getNumberOfYearsInPresentAddress(),
            //'dateestablished' => $this->getDateEstablished(),
            'employeesize' => $this->getEmployeeSize(),
            //'yearsmanagementisengaged' => $this->getNumberOfYearsManagementIsEngaged(),
            'fstemplates' => $this->getFinancialStatements(),
            //'bankstatements' => $this->getBankStatementCount(),
            //'utilitybills' => $this->getUtilityBillCount(),
            'majorcustomers' => $this->getMajorCustomerCount(),
            'majorsuppliers' => $this->getMajorSupplierCount()
        );
    }

    /**
     * Get the value of balanceSheetPeriodCount
     */ 
    public function getBalanceSheetPeriodCount()
    {
        return $this->balanceSheetPeriodCount;
    }

    /**
     * Set the value of balanceSheetPeriodCount
     *
     * @return  self
     */ 
    public function setBalanceSheetPeriodCount($balanceSheetPeriodCount)
    {
        $this->balanceSheetPeriodCount = $balanceSheetPeriodCount;

        return $this;
    }

    /**
     * Get the value of incomeStatementPeriodCount
     */ 
    public function getIncomeStatementPeriodCount()
    {
        return $this->incomeStatementPeriodCount;
    }

    /**
     * Set the value of incomeStatementPeriodCount
     *
     * @return  self
     */ 
    public function setIncomeStatementPeriodCount($incomeStatementPeriodCount)
    {
        $this->incomeStatementPeriodCount = $incomeStatementPeriodCount;

        return $this;
    }
}