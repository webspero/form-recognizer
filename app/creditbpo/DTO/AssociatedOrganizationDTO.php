<?php

namespace CreditBPO\DTO;

class AssociatedOrganizationDTO 
{
    protected $bankId;
    protected $name;
    protected $type;
    protected $isStandalone;

    /**
     * Get the value of bankId
     */ 
    public function getBankId()
    {
        return $this->bankId;
    }

    /**
     * Set the value of bankId
     *
     * @return  self
     */ 
    public function setBankId($bankId)
    {
        $this->bankId = $bankId;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of isStandalone
     */ 
    public function getIsStandalone()
    {
        return $this->isStandalone;
    }

    /**
     * Set the value of isStandalone
     *
     * @return  self
     */ 
    public function setIsStandalone($isStandalone)
    {
        $this->isStandalone = $isStandalone;

        return $this;
    }

    /**
     * @param array $vars
     * @return DscrDTO
     */
    public function setDbVars($vars) 
    {
        if (isset($vars->id)) {
            $this->setBankId($vars->id);
        }
        if (isset($vars->bank_name)) {
            $this->setName($vars->bank_name);
        }
        if (isset($vars->bank_type)) {
            $this->setType($vars->bank_type);
        }
        if (isset($vars->is_standalone)) {
            $this->setIsStandalone($vars->is_standalone);
        }
    }
}