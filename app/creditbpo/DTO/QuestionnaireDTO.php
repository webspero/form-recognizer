<?php

namespace CreditBPO\DTO;

class QuestionnaireDTO
{
	/*Inititalization*/
	protected $reportId;
	protected $tab;

	public function __construct() {

	}

	/**
     * Set the value of reportId
     */ 
	public function setReportId($reportId) {
		$this->reportId = $reportId;
		return $this;
	}

	/**
     * Get the value of reportId
     */ 
	public function getReportId(){
		return $this->reportId;
	}

	/**
     * Set the value of tabname
     */ 
	public function setTab($tab) {
		$this->tab = $tab;

		return $this;
	}

	/**
     * Get the value of reportId
     */ 
	public function getTab() {
		return $this->tab;
	}

	/**
     * Get the tab name
     */ 
	public function getTabName(){
		if($this->tab == 'registration1'){
			$tabName = "Business Details I";
		}elseif($this->tab == "registration2"){
			$tabName = "Business Details II";
		}elseif($this->tab == "registration3"){
			$tabName = "Associated Files";
		}elseif($this->tab == "pfs"){
			$tabName = "Required Credit Line";
		}

		return $tabName;
	}

	/**
     * @param array $vars
     * @return QuestionnaireDTO
     */
    public function setQuestionnaireVars($vars, $reportId) {
        if (isset($vars['tab'])) {
            $this->setTab($vars['tab']);
        }
        $this->setReportId($reportId);

        return $this;

    }

    public function getQuestionnaireVars(){
    	return array(
    		'reportid' => $this->getReportId(),
    		'tab'	   => $this->getTab(),
    		'tab_name' => $this->getTabName()
    	);
    }


}