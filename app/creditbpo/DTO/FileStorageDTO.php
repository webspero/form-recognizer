<?php

namespace CreditBPO\DTO;

class FileStorageDTO {
    public $id;
    public $loginid;
    public $uploadedfile;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of loginid
     */ 
    public function getLoginId()
    {
        return $this->loginid;
    }

    /**
     * Set the value of loginid
     *
     * @return  self
     */ 
    public function setLoginId($loginid)
    {
        $this->loginid = $loginid;

        return $this;
    }

    /**
     * Get the value of uploadedfile
     */ 
    public function getUploadedFile()
    {
        return $this->uploadedfile;
    }

   
    /**
     * Set the value of uploadfile
     *
     * @return  self
     */ 
    public function setUploadedFile($uploadedFile)
    {
        $this->uploadedfile = $uploadedFile;

        return $this;
    }
    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['loginid'])) {
            $this->setLoginId($vars['loginid']);
        }
        if (isset($vars['uploadedfile'])) {
            $this->setUploadedFile($vars['uploadedfile']);
        }
       
	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'loginid' => $this->getLoginId(),
            'uploadedfile' => $this->getUploadedFile()
        );
    }
    
}