<?php

namespace CreditBPO\DTO;

class UserAccountDTO {
    protected $userId; /* login id */
    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $jobTitle;
    protected $activationCode;
    protected $activationDate;
    protected $role;
    protected $status;

    #region [getter & setter]

    /**
     * Get the value of userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }
    
    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of firstName
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of jobTitle
     */ 
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set the value of jobTitle
     *
     * @return  self
     */ 
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get the value of activationCode
     */ 
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * Set the value of activationCode
     *
     * @return  self
     */ 
    public function setActivationCode($activationCode)
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * Get the value of activationDate
     */ 
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set the value of activationDate
     *
     * @return  self
     */ 
    public function setActivationDate($activationDate)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    #endregion [getter & setter]

    #region [set array]

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }

        if (isset($vars['password'])) {
            $this->setPassword($vars['password']);
        }
    }

    #endregion [set array]

    #region [get array]

    /**
     * @param array $vars
     * @return UserAccountDTO
     */
    public function getVars()
    {
        return array(
            'email' => $this->getEmail(),
            'password' => $this->getPassword(),
            'activationcode' => $this->getActivationCode()
        );
    }

    #endregion [get array]
}