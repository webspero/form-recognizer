<?php

namespace CreditBPO\DTO;

class DscrDTO 
{
    protected $reportId;
    protected $financialReport;
    protected $associatedOrganization;
    protected $balanceSheets = array();
    protected $incomeStatements = array();
    protected $cashflow = array();
    protected $bankInterestRates;
    protected $sources = array();
    protected $errors;

    /* DSCR Record Properties */
    protected $dscr;
    protected $loanDuration;
    protected $sourceAmount;
    protected $source;
    protected $loanAmount;
    protected $isCustom;
    protected $wcIntRateLocal;
    protected $wcIntRateForeign;
    protected $tlIntRateLocal;
    protected $tlIntRateForeign;
    protected $period;
    protected $paymentPerPeriodLocal;
    protected $paymentPerPeriodForeign;

    /* Working Capital Additional Properties */
    protected $wcPeriodLocal;
    protected $wcPeriodForeign;
    protected $wcInterestIncomePerPeriodLocal;
    protected $wcInterestIncomePerPeriodForeign;
    protected $wcTotalInterestIncomeLocal;
    protected $wcTotalInterestIncomeForeign;

    /* Term Loan Additional PRoperties */
    protected $tlPeriodLocal;
    protected $tlPeriodForeign;
    protected $tlPaymentPerPeriodLocal;
    protected $tlPaymentPerPeriodForeign;
    protected $tlTotalLoanPaymentLocal;
    protected $tlTotalLoanPaymentForeign;

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of financialReport
     */ 
    public function getFinancialReport()
    {
        return $this->financialReport;
    }

    /**
     * Set the value of financialReport
     *
     * @return  self
     */ 
    public function setFinancialReport($financialReport)
    {
        $this->financialReport = $financialReport;

        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    public function addError($error)
    {
        array_push($this->errors, $error);

        return $this;
    }
    
    /**
     * Get the value of dscr
     */ 
    public function getDscr()
    {
        return $this->dscr;
    }
    
    /**
     * Set the value of dscr
     *
     * @return  self
     */ 
    public function setDscr($dscr)
    {
        $this->dscr = $dscr;
        
        return $this;
    }
    
    /**
     * Get the value of loanDuration
     */ 
    public function getLoanDuration()
    {
        return $this->loanDuration;
    }
    
    /**
     * Set the value of loanDuration
     *
     * @return  self
     */ 
    public function setLoanDuration($loanDuration)
    {
        $this->loanDuration = $loanDuration;
        
        return $this;
    }
    
    /**
     * Get the value of sourceAmount
     */ 
    public function getSourceAmount()
    {
        return $this->sourceAmount;
    }
    
    /**
     * Set the value of sourceAmount
     *
     * @return  self
     */ 
    public function setSourceAmount($sourceAmount)
    {
        $this->sourceAmount = $sourceAmount;
        
        return $this;
    }
    
    /**
     * Get the value of loanAmount
     */ 
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }
    
    /**
     * Set the value of loanAmount
     *
     * @return  self
     */ 
    public function setLoanAmount($loanAmount)
    {
        $this->loanAmount = $loanAmount;

        return $this;
    }
    
    /**
     * Get the value of wcIntRateLocal
     */ 
    public function getWcIntRateLocal()
    {
        return $this->wcIntRateLocal;
    }

    /**
     * Set the value of wcIntRateLocal
     *
     * @return  self
     */ 
    public function setWcIntRateLocal($wcIntRateLocal)
    {
        $this->wcIntRateLocal = $wcIntRateLocal;
        
        return $this;
    }
    
    /**
     * Get the value of wcIntRateForeign
     */ 
    public function getWcIntRateForeign()
    {
        return $this->wcIntRateForeign;
    }
    
    /**
     * Set the value of wcIntRateForeign
     *
     * @return  self
     */ 
    public function setWcIntRateForeign($wcIntRateForeign)
    {
        $this->wcIntRateForeign = $wcIntRateForeign;
        
        return $this;
    }
    
    /**
     * Get the value of tlIntRateLocal
     */ 
    public function getTlIntRateLocal()
    {
        return $this->tlIntRateLocal;
    }
    
    /**
     * Set the value of tlIntRateLocal
     *
     * @return  self
     */ 
    public function setTlIntRateLocal($tlIntRateLocal)
    {
        $this->tlIntRateLocal = $tlIntRateLocal;
        
        return $this;
    }
    
    /**
     * Get the value of tlIntRateForeign
     */ 
    public function getTlIntRateForeign()
    {
        return $this->tlIntRateForeign;
    }
    
    /**
     * Set the value of tlIntRateForeign
     *
     * @return  self
     */ 
    public function setTlIntRateForeign($tlIntRateForeign)
    {
        $this->tlIntRateForeign = $tlIntRateForeign;

        return $this;
    }
    
    /**
     * Get the value of isCustom
     */ 
    public function getIsCustom()
    {
        return $this->isCustom;
    }
    
    /**
     * Set the value of isCustom
     *
     * @return  self
     */ 
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
        
        return $this;
    }
    
    /**
     * Get the value of source
     */ 
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set the value of source
     *
     * @return  self
     */ 
    public function setSource($source)
    {
        $this->source = $source;
        
        return $this;
    }

    /**
     * Get the value of period
     */ 
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set the value of period
     *
     * @return  self
     */ 
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get the value of paymentPerPeriodLocal
     */ 
    public function getPaymentPerPeriodLocal()
    {
        return $this->paymentPerPeriodLocal;
    }

    /**
     * Set the value of paymentPerPeriodLocal
     *
     * @return  self
     */ 
    public function setPaymentPerPeriodLocal($paymentPerPeriodLocal)
    {
        $this->paymentPerPeriodLocal = $paymentPerPeriodLocal;

        return $this;
    }

    /**
     * Get the value of paymentPerPeriodForeign
     */ 
    public function getPaymentPerPeriodForeign()
    {
        return $this->paymentPerPeriodForeign;
    }

    /**
     * Set the value of paymentPerPeriodForeign
     *
     * @return  self
     */ 
    public function setPaymentPerPeriodForeign($paymentPerPeriodForeign)
    {
        $this->paymentPerPeriodForeign = $paymentPerPeriodForeign;

        return $this;
    }

    /**
     * @param array $vars
     * @return DscrDTO
     */
    public function setDscrVars($vars) 
    {
        if (isset($vars->dscr)) {
            $this->setDscr($vars->dscr);
        }
        if (isset($vars->loan_duration)) {
            $this->setLoanDuration($vars->loan_duration);
        }
        if (isset($vars->net_operating_income)) {
            $this->setSourceAmount($vars->net_operating_income);
        }
        if (isset($vars->loan_amount)) {
            $this->setLoanAmount($vars->loan_amount);
        }
        if (isset($vars->working_interest_rate_local)) {
            $this->setWcIntRateLocal($vars->working_interest_rate_local);
        }
        if (isset($vars->working_interest_rate_foreign)) {
            $this->setWcIntRateForeign($vars->working_interest_rate_foreign);
        }
        if (isset($vars->term_interest_rate_local)) {
            $this->setTlIntRateLocal($vars->term_interest_rate_local);
        }
        if (isset($vars->term_interest_rate_foreign)) {
            $this->setTlIntRateForeign($vars->term_interest_rate_foreign);
        }
        if (isset($vars->period)) {
            $this->setPeriod($vars->period);
        }
        if (isset($vars->payment_per_period_local)) {
            $this->setPaymentPerPeriodLocal($vars->payment_per_period_local);
        }
        if (isset($vars->payment_per_period_foreign)) {
            $this->setPaymentPerPeriodForeign($vars->payment_per_period_foreign);
        }
        if (isset($vars->is_custom)) {
            $this->setIsCustom($vars->is_custom);
        }
        if (isset($vars->source)) {
            $this->setSource($vars->source);
        }

        return $this;
    }

    /**
     * Get the value of bankInterestRates
     */ 
    public function getBankInterestRates()
    {
        return $this->bankInterestRates;
    }

    /**
     * Set the value of bankInterestRates
     *
     * @return  self
     */ 
    public function setBankInterestRates($bankInterestRates)
    {
        $this->bankInterestRates = $bankInterestRates;

        return $this;
    }

    /**
     * Get the value of sources
     */ 
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * Set the value of sources
     *
     * @return  self
     */ 
    public function setSources($key, $value)
    {
        $this->sources[$key] = $value;

        return $this;
    }

    /**
     * Get the value of cashflow
     */ 
    public function getCashflow()
    {
        return $this->cashflow;
    }

    /**
     * Set the value of cashflow
     *
     * @return  self
     */ 
    public function addCashFlow($cashflow)
    {
        array_push($this->cashflow, $cashflow);

        return $this;
    }

    /**
     * Get the value of balanceSheets
     */ 
    public function getBalanceSheets()
    {
        return $this->balanceSheets;
    }

    /**
     * Set the value of balanceSheets
     *
     * @return  self
     */ 
    public function addBalanceSheet($balanceSheets)
    {
        array_push($this->balanceSheets, $balanceSheets);

        return $this;
    }

    /**
     * Get the value of incomeStatements
     */ 
    public function getIncomeStatements()
    {
        return $this->incomeStatements;
    }

    /**
     * Set the value of incomeStatements
     *
     * @return  self
     */ 
    public function addIncomeStatement($incomeStatement)
    {
        array_push($this->incomeStatements, $incomeStatement);

        return $this;
    }

    /**
     * Get the value of wcPeriodLocal
     */ 
    public function getWcPeriodLocal()
    {
        return $this->wcPeriodLocal;
    }

    /**
     * Set the value of wcPeriodLocal
     *
     * @return  self
     */ 
    public function setWcPeriodLocal($wcPeriodLocal)
    {
        $this->wcPeriodLocal = $wcPeriodLocal;

        return $this;
    }

    /**
     * Get the value of wcPeriodForeign
     */ 
    public function getWcPeriodForeign()
    {
        return $this->wcPeriodForeign;
    }

    /**
     * Set the value of wcPeriodForeign
     *
     * @return  self
     */ 
    public function setWcPeriodForeign($wcPeriodForeign)
    {
        $this->wcPeriodForeign = $wcPeriodForeign;

        return $this;
    }

    /**
     * Get the value of wcInterestIncomePerPeriodLocal
     */ 
    public function getWcInterestIncomePerPeriodLocal()
    {
        return $this->wcInterestIncomePerPeriodLocal;
    }

    /**
     * Set the value of wcInterestIncomePerPeriodLocal
     *
     * @return  self
     */ 
    public function setWcInterestIncomePerPeriodLocal($wcInterestIncomePerPeriodLocal)
    {
        $this->wcInterestIncomePerPeriodLocal = $wcInterestIncomePerPeriodLocal;

        return $this;
    }

    /**
     * Get the value of wcInterestIncomePerPeriodForeign
     */ 
    public function getWcInterestIncomePerPeriodForeign()
    {
        return $this->wcInterestIncomePerPeriodForeign;
    }

    /**
     * Set the value of wcInterestIncomePerPeriodForeign
     *
     * @return  self
     */ 
    public function setWcInterestIncomePerPeriodForeign($wcInterestIncomePerPeriodForeign)
    {
        $this->wcInterestIncomePerPeriodForeign = $wcInterestIncomePerPeriodForeign;

        return $this;
    }

    /**
     * Get the value of wcTotalInterestIncomeLocal
     */ 
    public function getWcTotalInterestIncomeLocal()
    {
        return $this->wcTotalInterestIncomeLocal;
    }

    /**
     * Set the value of wcTotalInterestIncomeLocal
     *
     * @return  self
     */ 
    public function setWcTotalInterestIncomeLocal($wcTotalInterestIncomeLocal)
    {
        $this->wcTotalInterestIncomeLocal = $wcTotalInterestIncomeLocal;

        return $this;
    }

    /**
     * Get the value of wcTotalInterestIncomeForeign
     */ 
    public function getWcTotalInterestIncomeForeign()
    {
        return $this->wcTotalInterestIncomeForeign;
    }

    /**
     * Set the value of wcTotalInterestIncomeForeign
     *
     * @return  self
     */ 
    public function setWcTotalInterestIncomeForeign($wcTotalInterestIncomeForeign)
    {
        $this->wcTotalInterestIncomeForeign = $wcTotalInterestIncomeForeign;

        return $this;
    }

    /**
     * Get the value of tlPeriodLocal
     */ 
    public function getTlPeriodLocal()
    {
        return $this->tlPeriodLocal;
    }

    /**
     * Set the value of tlPeriodLocal
     *
     * @return  self
     */ 
    public function setTlPeriodLocal($tlPeriodLocal)
    {
        $this->tlPeriodLocal = $tlPeriodLocal;

        return $this;
    }

    /**
     * Get the value of tlPeriodForeign
     */ 
    public function getTlPeriodForeign()
    {
        return $this->tlPeriodForeign;
    }

    /**
     * Set the value of tlPeriodForeign
     *
     * @return  self
     */ 
    public function setTlPeriodForeign($tlPeriodForeign)
    {
        $this->tlPeriodForeign = $tlPeriodForeign;

        return $this;
    }

    /**
     * Get the value of tlPaymentPerPeriodLocal
     */ 
    public function getTlPaymentPerPeriodLocal()
    {
        return $this->tlPaymentPerPeriodLocal;
    }

    /**
     * Set the value of tlPaymentPerPeriodLocal
     *
     * @return  self
     */ 
    public function setTlPaymentPerPeriodLocal($tlPaymentPerPeriodLocal)
    {
        $this->tlPaymentPerPeriodLocal = $tlPaymentPerPeriodLocal;

        return $this;
    }

    /**
     * Get the value of tlPaymentPerPeriodForeign
     */ 
    public function getTlPaymentPerPeriodForeign()
    {
        return $this->tlPaymentPerPeriodForeign;
    }

    /**
     * Set the value of tlPaymentPerPeriodForeign
     *
     * @return  self
     */ 
    public function setTlPaymentPerPeriodForeign($tlPaymentPerPeriodForeign)
    {
        $this->tlPaymentPerPeriodForeign = $tlPaymentPerPeriodForeign;

        return $this;
    }

    /**
     * Get the value of tlTotalLoanPaymentLocal
     */ 
    public function getTlTotalLoanPaymentLocal()
    {
        return $this->tlTotalLoanPaymentLocal;
    }

    /**
     * Set the value of tlTotalLoanPaymentLocal
     *
     * @return  self
     */ 
    public function setTlTotalLoanPaymentLocal($tlTotalLoanPaymentLocal)
    {
        $this->tlTotalLoanPaymentLocal = $tlTotalLoanPaymentLocal;

        return $this;
    }

    /**
     * Get the value of tlTotalLoanPaymentForeign
     */ 
    public function getTlTotalLoanPaymentForeign()
    {
        return $this->tlTotalLoanPaymentForeign;
    }

    /**
     * Set the value of tlTotalLoanPaymentForeign
     *
     * @return  self
     */ 
    public function setTlTotalLoanPaymentForeign($tlTotalLoanPaymentForeign)
    {
        $this->tlTotalLoanPaymentForeign = $tlTotalLoanPaymentForeign;

        return $this;
    }

    /**
     * Set the value of incomeStatements
     *
     * @return  array
     */ 
    public function getFinancingResponse()
    {
        return array(
            'dscr' => $this->getDscr(),
            'source' => $this->getSource(),
            'sourceAmount' => number_format($this->getSourceAmount(), 2, '.', ','),
            'totalLoanDuration' => $this->getLoanDuration(),
            'loanAmount' => number_format($this->getLoanAmount(), 2, '.', ','),
            'isCustomInterestRates' => $this->getIsCustom(),
            'workingCapital' => array (
                'localBank' => array(
                    'interestRate' => number_format($this->getWcIntRateLocal(), 2, '.', ','),
                    'period' => $this->getWcPeriodLocal(),
                    'interestIncomePerPeriod' => is_string($this->getWcInterestIncomePerPeriodLocal()) ? $this->getWcInterestIncomePerPeriodLocal() : number_format($this->getWcInterestIncomePerPeriodLocal(), 2, '.', ','),
                    'totalInterestIncome' => is_string($this->getWcTotalInterestIncomeLocal()) ? $this->getWcTotalInterestIncomeLocal() : number_format($this->getWcTotalInterestIncomeLocal(), 2, '.', ',')
                ),
                'foreignBank' => array(
                    'interestRate' => number_format($this->getWcIntRateForeign(), 2, '.', ','),
                    'period' => $this->getWcPeriodForeign(),
                    'interestIncomePerPeriod' => is_string($this->getWcInterestIncomePerPeriodForeign()) ? $this->getWcInterestIncomePerPeriodForeign() : number_format($this->getWcInterestIncomePerPeriodForeign(), 2, '.', ','),
                    'totalInterestIncome' => is_string($this->getWcTotalInterestIncomeForeign()) ? $this->getWcTotalInterestIncomeForeign() : number_format($this->getWcTotalInterestIncomeForeign(), 2, '.', ',')
                )
            ),
            'termLoan' => array (
                'localBank' => array(
                    'interestRate' => number_format($this->getTlIntRateLocal(), 2, '.', ','),
                    'period' => $this->getTlPeriodLocal(),
                    'paymentPerPeriod' => is_string($this->getTlPaymentPerPeriodLocal()) ? $this->getTlPaymentPerPeriodLocal() : number_format($this->getTlPaymentPerPeriodLocal(), 2, '.', ','),
                    'totalLoanPayment' => is_string($this->getTlTotalLoanPaymentLocal()) ? $this->getTlTotalLoanPaymentLocal() : number_format($this->getTlTotalLoanPaymentLocal(), 2, '.', ',')
                ),
                'foreignBank' => array(
                    'interestRate' => number_format($this->getTlIntRateForeign(), 2, '.', ','),
                    'period' => $this->getTlPeriodForeign(),
                    'paymentPerPeriod' => is_string($this->getTlPaymentPerPeriodForeign()) ? $this->getTlPaymentPerPeriodForeign() : number_format($this->getTlPaymentPerPeriodForeign(), 2, '.', ','),
                    'totalLoanPayment' => is_string($this->getTlTotalLoanPaymentForeign()) ? $this->getTlTotalLoanPaymentForeign() : number_format($this->getTlTotalLoanPaymentForeign(), 2, '.', ',')
                )
            )
        );
    }

    /**
     * @param array $vars
     * @return DscrDTO
     */
    public function setRequestVars($vars) 
    {
        if (isset($vars['dscr'])) {
            $this->setDscr(number_format($vars['dscr'], 1, '.', ','));
        }
        if (isset($vars['source'])) {
            $this->setSource($vars['source']);
        }
        if (isset($vars['sourceAmount'])) {
            $this->setSourceAmount($vars['sourceAmount']);
        }
        if (isset($vars['totalLoanDuration'])) {
            $this->setLoanDuration($vars['totalLoanDuration']);
        }
        if (isset($vars['isCustom'])) {
            $this->setIsCustom($vars['isCustom']);
        }
        if (isset($vars['localBankInterestRateWorkingCapital'])) {
            $this->setWcIntRateLocal($vars['localBankInterestRateWorkingCapital']);
        }
        if (isset($vars['localBankInterestRateTermLoan'])) {
            $this->setTlIntRateLocal($vars['localBankInterestRateTermLoan']);
        }
        if (isset($vars['foreignBankInterestRateWorkingCapital'])) {
            $this->setWcIntRateForeign($vars['foreignBankInterestRateWorkingCapital']);
        }
        if (isset($vars['foreignBankInterestRateTermLoan'])) {
            $this->setTlIntRateForeign($vars['foreignBankInterestRateTermLoan']);
        }

        return $this;
    }

    /**
     * Set the value of incomeStatements
     *
     * @return  array
     */ 
    public function getProcurementResponse()
    {
        return array(
            'dscr' => $this->getDscr(),
            'source' => $this->getSource(),
            'sourceAmount' => number_format($this->getSourceAmount(), 2, '.', ','),
            'requiredDeliveryOrCompletion' => $this->getLoanDuration(),
            'procurementAwardCap' => number_format($this->getLoanAmount(), 2, '.', ',')
        );
    }

    /**
     * Set the value of incomeStatements
     *
     * @return  array
     */ 
    public function getUpdateVars()
    {
        return array(
            'entity_id' => $this->getReportId(),
            'dscr' => $this->getDscr(),
            'loan_duration' => $this->getLoanDuration(),
            'net_operating_income' => $this->getSourceAmount(),
            'loan_amount' => $this->getLoanAmount(),
            'working_interest_rate_local' => $this->getWcIntRateLocal(),
            'working_interest_rate_foreign' => $this->getWcIntRateForeign(),
            'term_interest_rate_local' => $this->getTlIntRateLocal(),
            'term_interest_rate_foreign' => $this->getTlIntRateForeign(),
            'is_custom' => $this->getIsCustom(),
            'source' => $this->getSource()
        );
    }

    /**
     * @param array $vars
     * @return DscrDTO
     */
    public function setTermLoanValuesNA() 
    {
        $this->setTlPeriodLocal('N/A');
        $this->setTlPeriodForeign('N/A');
        $this->setTlPaymentPerPeriodLocal('N/A');
        $this->setTlPaymentPerPeriodForeign('N/A');
        $this->setTlTotalLoanPaymentLocal('N/A');
        $this->setTlTotalLoanPaymentForeign('N/A');
    }

    /**
     * @param array $vars
     * @return DscrDTO
     */
    public function setWorkingCapitalValuesNA()
    {
        $this->setWcPeriodLocal('N/A');
        $this->setWcPeriodForeign('N/A');
        $this->setWcInterestIncomePerPeriodLocal('N/A');
        $this->setWcInterestIncomePerPeriodForeign('N/A');
        $this->setWcTotalInterestIncomeLocal('N/A');
        $this->setWcTotalInterestIncomeForeign('N/A');
    }

    /**
     * Get the value of associatedOrganization
     */ 
    public function getAssociatedOrganization()
    {
        return $this->associatedOrganization;
    }

    /**
     * Set the value of associatedOrganization
     *
     * @return  self
     */ 
    public function setAssociatedOrganization($associatedOrganization)
    {
        $this->associatedOrganization = $associatedOrganization;

        return $this;
    }
}