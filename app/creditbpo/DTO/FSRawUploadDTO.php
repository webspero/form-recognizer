<?php

namespace CreditBPO\DTO;

class FSRawUploadDTO
{
    protected $reportId;
    protected $documentId;
    protected $file;
    protected $fileType;
    protected $filename;
    protected $uploadType = 0;

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of file
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @return  self
     */ 
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get the value of fileType
     */ 
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * Set the value of fileType
     *
     * @return  self
     */ 
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }


    /**
     * Get the value of filename
     */ 
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of filename
     *
     * @return  self
     */ 
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }



    /**
     * Get the value of uploadType
     */ 
    public function getUploadType()
    {
        return $this->uploadType;
    }

    /**
     * Set the value of uploadType
     *
     * @return  self
     */ 
    public function setUploadType($uploadType)
    {
        $this->uploadType = $uploadType;

        return $this;
    }

    /**
     * Get the value of documentId
     */ 
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set the value of documentId
     *
     * @return  self
     */ 
    public function setDocumentId($documentId)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get the value of fileExtension
     */ 
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * Set the value of fileExtension
     *
     * @return  self
     */ 
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    #region [set array]

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }

        if (isset($vars['file'])) {
            $this->setFile($vars['file']);
        }

        if (isset($vars['filetype'])) {
            $this->setFileType($vars['filetype']);
        }

        if (isset($vars['filename'])) {
            $this->setFilename($vars['filename']);
        }
    }

    #endregion [set array]

    #region [get array]

    /**
     * @param array $vars
     * @return FSRawUploadDTO
     */
    public function getVars()
    {
        return array(
            'reportid' => $this->getReportId(),
            'file' => $this->getFile(),
            'filetype' => $this->getFileType(),
            'documentfilename' => $this->getFilename(),
            'uploadtype' => $this->getUploadType(),
            'documentid' => $this->getDocumentId()
        );
    }

    #endregion [get array]
}