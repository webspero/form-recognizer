<?php

namespace CreditBPO\DTO;

class IncomeStatementDTO 
{
    protected $financialReportId;
    protected $indexCount;
    protected $revenue;
    protected $costOfSales;
    protected $grossProfit;
    protected $otherIncome;
    protected $distributionCosts;
    protected $administrativeExpense;
    protected $otherExpenseByFunction;
    protected $otherGainsLosses;
    protected $profitLossFromOperatingActivities;
    protected $differenceBetweenCarryingAmountOfDividendsPayable;
    protected $gainsLossesOnNetMonetaryPosition;
    protected $gainLossArisingFromDerecognitionOfFinancialAssets;
    protected $financeIncome;
    protected $financeCosts;
    protected $shareOfProfitLossOfAssociates;
    protected $gainsLossesArisingFromDifference;
    protected $profitLossBeforeTax;
    protected $incomeTaxExpenseContinuingOperations;
    protected $profitLossFromContinuingOperations;
    protected $profitLossFromDiscontinuedOperations;
    protected $profitLoss;
    protected $otherComprehensiveIncome;
    protected $comprehensiveIncome;
    protected $impairmentLoss;
    protected $otherIncomeFromSubsidiaries;
    protected $cumulativeGainPreviouslyRecognized;
    protected $hedgingGainsForHedge;
    protected $depreciationExpense;
    protected $amortisationExpense;
    protected $depreciationAndAmortisationExpense;

    /**
     * Get the value of financialReportId
     */ 
    public function getFinancialReportId()
    {
        return $this->financialReportId;
    }

    /**
     * Set the value of financialReportId
     *
     * @return  self
     */ 
    public function setFinancialReportId($financialReportId)
    {
        $this->financialReportId = $financialReportId;

        return $this;
    }

    /**
     * Get the value of indexCount
     */ 
    public function getIndexCount()
    {
        return $this->indexCount;
    }

    /**
     * Set the value of indexCount
     *
     * @return  self
     */ 
    public function setIndexCount($indexCount)
    {
        $this->indexCount = $indexCount;

        return $this;
    }

    /**
     * Get the value of revenue
     */ 
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * Set the value of revenue
     *
     * @return  self
     */ 
    public function setRevenue($revenue)
    {
        $this->revenue = $revenue;

        return $this;
    }

    /**
     * Get the value of costOfSales
     */ 
    public function getCostOfSales()
    {
        return $this->costOfSales;
    }

    /**
     * Set the value of costOfSales
     *
     * @return  self
     */ 
    public function setCostOfSales($costOfSales)
    {
        $this->costOfSales = $costOfSales;

        return $this;
    }

    /**
     * Get the value of grossProfit
     */ 
    public function getGrossProfit()
    {
        return $this->grossProfit;
    }

    /**
     * Set the value of grossProfit
     *
     * @return  self
     */ 
    public function setGrossProfit($grossProfit)
    {
        $this->grossProfit = $grossProfit;

        return $this;
    }

    /**
     * Get the value of otherIncome
     */ 
    public function getOtherIncome()
    {
        return $this->otherIncome;
    }

    /**
     * Set the value of otherIncome
     *
     * @return  self
     */ 
    public function setOtherIncome($otherIncome)
    {
        $this->otherIncome = $otherIncome;

        return $this;
    }

    /**
     * Get the value of distributionCosts
     */ 
    public function getDistributionCosts()
    {
        return $this->distributionCosts;
    }

    /**
     * Set the value of distributionCosts
     *
     * @return  self
     */ 
    public function setDistributionCosts($distributionCosts)
    {
        $this->distributionCosts = $distributionCosts;

        return $this;
    }

    /**
     * Get the value of administrativeExpense
     */ 
    public function getAdministrativeExpense()
    {
        return $this->administrativeExpense;
    }

    /**
     * Set the value of administrativeExpense
     *
     * @return  self
     */ 
    public function setAdministrativeExpense($administrativeExpense)
    {
        $this->administrativeExpense = $administrativeExpense;

        return $this;
    }

    /**
     * Get the value of otherExpenseByFunction
     */ 
    public function getOtherExpenseByFunction()
    {
        return $this->otherExpenseByFunction;
    }

    /**
     * Set the value of otherExpenseByFunction
     *
     * @return  self
     */ 
    public function setOtherExpenseByFunction($otherExpenseByFunction)
    {
        $this->otherExpenseByFunction = $otherExpenseByFunction;

        return $this;
    }

    /**
     * Get the value of otherGainsLosses
     */ 
    public function getOtherGainsLosses()
    {
        return $this->otherGainsLosses;
    }

    /**
     * Set the value of otherGainsLosses
     *
     * @return  self
     */ 
    public function setOtherGainsLosses($otherGainsLosses)
    {
        $this->otherGainsLosses = $otherGainsLosses;

        return $this;
    }

    /**
     * Get the value of profitLossFromOperatingActivities
     */ 
    public function getProfitLossFromOperatingActivities()
    {
        return $this->profitLossFromOperatingActivities;
    }

    /**
     * Set the value of profitLossFromOperatingActivities
     *
     * @return  self
     */ 
    public function setProfitLossFromOperatingActivities($profitLossFromOperatingActivities)
    {
        $this->profitLossFromOperatingActivities = $profitLossFromOperatingActivities;

        return $this;
    }

    /**
     * Get the value of differenceBetweenCarryingAmountOfDividendsPayable
     */ 
    public function getDifferenceBetweenCarryingAmountOfDividendsPayable()
    {
        return $this->differenceBetweenCarryingAmountOfDividendsPayable;
    }

    /**
     * Set the value of differenceBetweenCarryingAmountOfDividendsPayable
     *
     * @return  self
     */ 
    public function setDifferenceBetweenCarryingAmountOfDividendsPayable($differenceBetweenCarryingAmountOfDividendsPayable)
    {
        $this->differenceBetweenCarryingAmountOfDividendsPayable = $differenceBetweenCarryingAmountOfDividendsPayable;

        return $this;
    }

    /**
     * Get the value of gainsLossesOnNetMonetaryPosition
     */ 
    public function getGainsLossesOnNetMonetaryPosition()
    {
        return $this->gainsLossesOnNetMonetaryPosition;
    }

    /**
     * Set the value of gainsLossesOnNetMonetaryPosition
     *
     * @return  self
     */ 
    public function setGainsLossesOnNetMonetaryPosition($gainsLossesOnNetMonetaryPosition)
    {
        $this->gainsLossesOnNetMonetaryPosition = $gainsLossesOnNetMonetaryPosition;

        return $this;
    }

    /**
     * Get the value of gainLossArisingFromDerecognitionOfFinancialAssets
     */ 
    public function getGainLossArisingFromDerecognitionOfFinancialAssets()
    {
        return $this->gainLossArisingFromDerecognitionOfFinancialAssets;
    }

    /**
     * Set the value of gainLossArisingFromDerecognitionOfFinancialAssets
     *
     * @return  self
     */ 
    public function setGainLossArisingFromDerecognitionOfFinancialAssets($gainLossArisingFromDerecognitionOfFinancialAssets)
    {
        $this->gainLossArisingFromDerecognitionOfFinancialAssets = $gainLossArisingFromDerecognitionOfFinancialAssets;

        return $this;
    }

    /**
     * Get the value of financeIncome
     */ 
    public function getFinanceIncome()
    {
        return $this->financeIncome;
    }

    /**
     * Set the value of financeIncome
     *
     * @return  self
     */ 
    public function setFinanceIncome($financeIncome)
    {
        $this->financeIncome = $financeIncome;

        return $this;
    }

    /**
     * Get the value of financeCosts
     */ 
    public function getFinanceCosts()
    {
        return $this->financeCosts;
    }

    /**
     * Set the value of financeCosts
     *
     * @return  self
     */ 
    public function setFinanceCosts($financeCosts)
    {
        $this->financeCosts = $financeCosts;

        return $this;
    }

    /**
     * Get the value of shareOfProfitLossOfAssociates
     */ 
    public function getShareOfProfitLossOfAssociates()
    {
        return $this->shareOfProfitLossOfAssociates;
    }

    /**
     * Set the value of shareOfProfitLossOfAssociates
     *
     * @return  self
     */ 
    public function setShareOfProfitLossOfAssociates($shareOfProfitLossOfAssociates)
    {
        $this->shareOfProfitLossOfAssociates = $shareOfProfitLossOfAssociates;

        return $this;
    }

    /**
     * Get the value of gainsLossesArisingFromDifference
     */ 
    public function getGainsLossesArisingFromDifference()
    {
        return $this->gainsLossesArisingFromDifference;
    }

    /**
     * Set the value of gainsLossesArisingFromDifference
     *
     * @return  self
     */ 
    public function setGainsLossesArisingFromDifference($gainsLossesArisingFromDifference)
    {
        $this->gainsLossesArisingFromDifference = $gainsLossesArisingFromDifference;

        return $this;
    }

    /**
     * Get the value of profitLossBeforeTax
     */ 
    public function getProfitLossBeforeTax()
    {
        return $this->profitLossBeforeTax;
    }

    /**
     * Set the value of profitLossBeforeTax
     *
     * @return  self
     */ 
    public function setProfitLossBeforeTax($profitLossBeforeTax)
    {
        $this->profitLossBeforeTax = $profitLossBeforeTax;

        return $this;
    }

    /**
     * Get the value of incomeTaxExpenseContinuingOperations
     */ 
    public function getIncomeTaxExpenseContinuingOperations()
    {
        return $this->incomeTaxExpenseContinuingOperations;
    }

    /**
     * Set the value of incomeTaxExpenseContinuingOperations
     *
     * @return  self
     */ 
    public function setIncomeTaxExpenseContinuingOperations($incomeTaxExpenseContinuingOperations)
    {
        $this->incomeTaxExpenseContinuingOperations = $incomeTaxExpenseContinuingOperations;

        return $this;
    }

    /**
     * Get the value of profitLossFromContinuingOperations
     */ 
    public function getProfitLossFromContinuingOperations()
    {
        return $this->profitLossFromContinuingOperations;
    }

    /**
     * Set the value of profitLossFromContinuingOperations
     *
     * @return  self
     */ 
    public function setProfitLossFromContinuingOperations($profitLossFromContinuingOperations)
    {
        $this->profitLossFromContinuingOperations = $profitLossFromContinuingOperations;

        return $this;
    }

    /**
     * Get the value of profitLossFromDiscontinuedOperations
     */ 
    public function getProfitLossFromDiscontinuedOperations()
    {
        return $this->profitLossFromDiscontinuedOperations;
    }

    /**
     * Set the value of profitLossFromDiscontinuedOperations
     *
     * @return  self
     */ 
    public function setProfitLossFromDiscontinuedOperations($profitLossFromDiscontinuedOperations)
    {
        $this->profitLossFromDiscontinuedOperations = $profitLossFromDiscontinuedOperations;

        return $this;
    }

    /**
     * Get the value of profitLoss
     */ 
    public function getProfitLoss()
    {
        return $this->profitLoss;
    }

    /**
     * Set the value of profitLoss
     *
     * @return  self
     */ 
    public function setProfitLoss($profitLoss)
    {
        $this->profitLoss = $profitLoss;

        return $this;
    }

    /**
     * Get the value of otherComprehensiveIncome
     */ 
    public function getOtherComprehensiveIncome()
    {
        return $this->otherComprehensiveIncome;
    }

    /**
     * Set the value of otherComprehensiveIncome
     *
     * @return  self
     */ 
    public function setOtherComprehensiveIncome($otherComprehensiveIncome)
    {
        $this->otherComprehensiveIncome = $otherComprehensiveIncome;

        return $this;
    }

    /**
     * Get the value of comprehensiveIncome
     */ 
    public function getComprehensiveIncome()
    {
        return $this->comprehensiveIncome;
    }

    /**
     * Set the value of comprehensiveIncome
     *
     * @return  self
     */ 
    public function setComprehensiveIncome($comprehensiveIncome)
    {
        $this->comprehensiveIncome = $comprehensiveIncome;

        return $this;
    }

    /**
     * Get the value of impairmentLoss
     */ 
    public function getImpairmentLoss()
    {
        return $this->impairmentLoss;
    }

    /**
     * Set the value of impairmentLoss
     *
     * @return  self
     */ 
    public function setImpairmentLoss($impairmentLoss)
    {
        $this->impairmentLoss = $impairmentLoss;

        return $this;
    }

    /**
     * Get the value of otherIncomeFromSubsidiaries
     */ 
    public function getOtherIncomeFromSubsidiaries()
    {
        return $this->otherIncomeFromSubsidiaries;
    }

    /**
     * Set the value of otherIncomeFromSubsidiaries
     *
     * @return  self
     */ 
    public function setOtherIncomeFromSubsidiaries($otherIncomeFromSubsidiaries)
    {
        $this->otherIncomeFromSubsidiaries = $otherIncomeFromSubsidiaries;

        return $this;
    }

    /**
     * Get the value of cumulativeGainPreviouslyRecognized
     */ 
    public function getCumulativeGainPreviouslyRecognized()
    {
        return $this->cumulativeGainPreviouslyRecognized;
    }

    /**
     * Set the value of cumulativeGainPreviouslyRecognized
     *
     * @return  self
     */ 
    public function setCumulativeGainPreviouslyRecognized($cumulativeGainPreviouslyRecognized)
    {
        $this->cumulativeGainPreviouslyRecognized = $cumulativeGainPreviouslyRecognized;

        return $this;
    }

    /**
     * Get the value of hedgingGainsForHedge
     */ 
    public function getHedgingGainsForHedge()
    {
        return $this->hedgingGainsForHedge;
    }

    /**
     * Set the value of hedgingGainsForHedge
     *
     * @return  self
     */ 
    public function setHedgingGainsForHedge($hedgingGainsForHedge)
    {
        $this->hedgingGainsForHedge = $hedgingGainsForHedge;

        return $this;
    }

    /**
     * Get the value of depreciationExpense
     */ 
    public function getDepreciationExpense()
    {
        return $this->depreciationExpense;
    }

    /**
     * Set the value of depreciationExpense
     *
     * @return  self
     */ 
    public function setDepreciationExpense($depreciationExpense)
    {
        $this->depreciationExpense = $depreciationExpense;

        return $this;
    }

    /**
     * Get the value of amortisationExpense
     */ 
    public function getAmortisationExpense()
    {
        return $this->amortisationExpense;
    }

    /**
     * Set the value of amortisationExpense
     *
     * @return  self
     */ 
    public function setAmortisationExpense($amortisationExpense)
    {
        $this->amortisationExpense = $amortisationExpense;

        return $this;
    }

    /**
     * Get the value of depreciationAndAmortisationExpense
     */ 
    public function getDepreciationAndAmortisationExpense()
    {
        return $this->depreciationAndAmortisationExpense;
    }

    /**
     * Set the value of depreciationAndAmortisationExpense
     *
     * @return  self
     */ 
    public function setDepreciationAndAmortisationExpense($depreciationAndAmortisationExpense)
    {
        $this->depreciationAndAmortisationExpense = $depreciationAndAmortisationExpense;

        return $this;
    }

    /**
     * @param array $vars
     * @return BalanceSheetDTO
     */
    public function setDbVars($vars) 
    {
        if (isset($vars->financial_report_id)) {
            $this->setFinancialReportId($vars->financial_report_id);
        }
        if (isset($vars->index_count)) {
            $this->setIndexCount($vars->index_count);
        }
        if (isset($vars->Revenue)) {
            $this->setRevenue($vars->Revenue);
        }
        if (isset($vars->CostOfSales)) {
            $this->setCostOfSales($vars->CostOfSales);
        }
        if (isset($vars->GrossProfit)) {
            $this->setGrossProfit($vars->GrossProfit);
        }
        if (isset($vars->OtherIncome)) {
            $this->setOtherIncome($vars->OtherIncome);
        }
        if (isset($vars->DistributionCosts)) {
            $this->setDistributionCosts($vars->DistributionCosts);
        }
        if (isset($vars->AdministrativeExpense)) {
            $this->setAdministrativeExpense($vars->AdministrativeExpense);
        }
        if (isset($vars->OtherExpenseByFunction)) {
            $this->setOtherExpenseByFunction($vars->OtherExpenseByFunction);
        }
        if (isset($vars->OtherGainsLosses)) {
            $this->setOtherGainsLosses($vars->OtherGainsLosses);
        }
        if (isset($vars->ProfitLossFromOperatingActivities)) {
            $this->setProfitLossFromOperatingActivities($vars->ProfitLossFromOperatingActivities);
        }
        if (isset($vars->DifferenceBetweenCarryingAmountOfDividendsPayable)) {
            $this->setDifferenceBetweenCarryingAmountOfDividendsPayable($vars->DifferenceBetweenCarryingAmountOfDividendsPayable);
        }
        if (isset($vars->GainsLossesOnNetMonetaryPosition)) {
            $this->setGainsLossesOnNetMonetaryPosition($vars->GainsLossesOnNetMonetaryPosition);
        }
        if (isset($vars->GainLossArisingFromDerecognitionOfFinancialAssets)) {
            $this->setGainLossArisingFromDerecognitionOfFinancialAssets($vars->GainLossArisingFromDerecognitionOfFinancialAssets);
        }
        if (isset($vars->FinanceIncome)) {
            $this->setFinanceIncome($vars->FinanceIncome);
        }
        if (isset($vars->FinanceCosts)) {
            $this->setFinanceCosts($vars->FinanceCosts);
        }
        if (isset($vars->ShareOfProfitLossOfAssociates)) {
            $this->setShareOfProfitLossOfAssociates($vars->ShareOfProfitLossOfAssociates);
        }
        if (isset($vars->GainsLossesArisingFromDifference)) {
            $this->setGainsLossesArisingFromDifference($vars->GainsLossesArisingFromDifference);
        }
        if (isset($vars->ProfitLossBeforeTax)) {
            $this->setProfitLossBeforeTax($vars->ProfitLossBeforeTax);
        }
        if (isset($vars->IncomeTaxExpenseContinuingOperations)) {
            $this->setIncomeTaxExpenseContinuingOperations($vars->IncomeTaxExpenseContinuingOperations);
        }
        if (isset($vars->ProfitLossFromContinuingOperations)) {
            $this->setProfitLossFromContinuingOperations($vars->ProfitLossFromContinuingOperations);
        }
        if (isset($vars->ProfitLossFromDiscontinuedOperations)) {
            $this->setProfitLossFromDiscontinuedOperations($vars->ProfitLossFromDiscontinuedOperations);
        }
        if (isset($vars->ProfitLoss)) {
            $this->setProfitLoss($vars->ProfitLoss);
        }
        if (isset($vars->OtherComprehensiveIncome)) {
            $this->setOtherComprehensiveIncome($vars->OtherComprehensiveIncome);
        }
        if (isset($vars->ComprehensiveIncome)) {
            $this->setComprehensiveIncome($vars->ComprehensiveIncome);
        }
        if (isset($vars->ImpairmentLoss)) {
            $this->setImpairmentLoss($vars->ImpairmentLoss);
        }
        if (isset($vars->OtherIncomeFromSubsidiaries)) {
            $this->setOtherIncomeFromSubsidiaries($vars->OtherIncomeFromSubsidiaries);
        }
        if (isset($vars->CumulativeGainPreviouslyRecognized)) {
            $this->setCumulativeGainPreviouslyRecognized($vars->CumulativeGainPreviouslyRecognized);
        }
        if (isset($vars->HedgingGainsForHedge)) {
            $this->setHedgingGainsForHedge($vars->HedgingGainsForHedge);
        }
        if (isset($vars->DepreciationExpense)) {
            $this->setDepreciationExpense($vars->DepreciationExpense);
        }
        if (isset($vars->AmortisationExpense)) {
            $this->setAmortisationExpense($vars->AmortisationExpense);
        }
        if (isset($vars->DepreciationAndAmortisationExpense)) {
            $this->setDepreciationAndAmortisationExpense($vars->DepreciationAndAmortisationExpense);
        }

        return $this;
    }
}