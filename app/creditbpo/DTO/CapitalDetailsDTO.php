<?php

namespace CreditBPO\DTO;

class CapitalDetailsDTO {
    public $id;
    public $reportid;
    public $authorizedcapital;
    public $issuedcapital;
    public $paidupcapital;
    public $ordinaryshares;
    public $parvalue;
    
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of reportid
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }


    /**
     * Set the value of reportid
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportid = $reportId;

        return $this;
    }

    /**
     * Get the value of authorizedcapital
     */ 
    public function getAuthorizedCapital()
    {
        return $this->authorizedcapital;
    }

    /**
     * Set the value of authorizedcapital
     *
     * @return  self
     */ 
    public function setAuthorizedCapital($authorizedCapital)
    {
        $this->authorizedcapital = $authorizedCapital;

        return $this;
    }

    /**
     * Get the value of issuedcapital
     */ 
    public function getIssuedCapital()
    {
        return $this->issuedcapital;
    }

    /**
     * Set the value of authorizedcapital
     *
     * @return  self
     */ 
    public function setIssuedCapital($issuedCapital)
    {
        $this->issuedcapital = $issuedCapital;

        return $this;
    }

    /**
     * Get the value of paidupcapital
     */ 
    public function getPaidUpCapital()
    {
        return $this->paidupcapital;
    }

    /**
     * Set the value of paidupcapital
     *
     * @return  self
     */ 
    public function setPaidUpCapital($paidUpCapital)
    {
        $this->paidupcapital = $paidUpCapital;

        return $this;
    }

    /**
     * Get the value of ordinaryshares
     */ 
    public function getOrdinaryShares()
    {
        return $this->ordinaryshares;
    }

    /**
     * Set the value of ordinaryshares
     *
     * @return  self
     */ 
    public function setOrdinaryShares($ordinaryShares)
    {
        $this->ordinaryshares = $ordinaryShares;

        return $this;
    }

    /**
     * Get the value of parvalue
     */ 
    public function getParValue()
    {
        return $this->parvalue;
    }

    /**
     * Set the value of parvalue
     *
     * @return  self
     */ 
    public function setParValue($parValue)
    {
        $this->parvalue = $parValue;

        return $this;
    }

    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['authorizedcapital'])) {
            $this->setAuthorizedCapital($vars['authorizedcapital']);
        }
        if (isset($vars['issuedcapital'])) {
            $this->setIssuedCapital($vars['issuedcapital']);
        }
        if (isset($vars['paidupcapital'])) {
            $this->setPaidUpCapital($vars['paidupcapital']);
        }
        if (isset($vars['ordinaryshares'])) {
            $this->setOrdinaryShares($vars['ordinaryshares']);
       	}
        if (isset($vars['parvalue'])) {
            $this->setParValue($vars['parvalue']);
        }

	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'authorizedcapital' => $this->getAuthorizedCapital(),
            'issuedcapital' => $this->getIssuedCapital(),
            'paidupcapital' => $this->getPaidUpCapital(),
            'ordinaryshares' => $this->getOrdinaryShares(),
            'parvalue' => $this->getParValue()
        );
    }
    
}