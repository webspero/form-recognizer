<?php

namespace CreditBPO\DTO;

class AssociatedFilesDTO {
    public $id;
    public $reportid;
    public $uploadedfile;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of reportid
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of reportid
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of uploadedfile
     */ 
    public function getUploadedFile()
    {
        return $this->uploadedfile;
    }

   
    /**
     * Set the value of uploadfile
     *
     * @return  self
     */ 
    public function setUploadedFile($uploadedFile)
    {
        $this->uploadedfile = $uploadedFile;

        return $this;
    }
    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['uploadedfile'])) {
            $this->setUploadedFile($vars['uploadedfile']);
        }
       
	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'uploadedfile' => $this->getUploadedFile()
        );
    }
    
}