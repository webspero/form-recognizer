<?php

namespace CreditBPO\DTO;

use CreditBPO\Models\Discount;

class PaymentDTO 
{
    protected $reportId;
    protected $userId;
    protected $isPaid;
    protected $discountCode;
    protected $discountDetails;
    protected $baseAmount = 3500;
    protected $discountAmount;      //Discount Amount Before Tax
    protected $totalAmount;         //Total Amount After Tax and Discount
    protected $vatAmount;           //VAT Amount After Discount
    protected $vatableAmount;       //VATable Amount After Discount
    protected $api_match_account_details;

    /**
     * @param array $vars
     */
    public function __construct($vars = []) {
        $this->setVars($vars);
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get the value of isPaid
     */ 
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set the value of isPaid
     *
     * @return  self
     */ 
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        return $this;
    }

    /**
     * Get the value of isExist
     */ 
    public function getIsExist()
    {
        return $this->isExist;
    }

    /**
     * Set the value of isExist
     *
     * @return  self
     */ 
    public function setIsExist($isExist)
    {
        $this->isExist = $isExist;

        return $this;
    }

    /**
     * Get the value of discount
     */ 
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * Set the value of discount
     *
     * @return  self
     */ 
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * Get the value of discountDetails
     */ 
    public function getDiscountDetails()
    {
        return $this->discountDetails;
    }

    /**
     * Set the value of discountDetails
     *
     * @return  self
     */ 
    public function setDiscountDetails(Discount $discountDetails)
    {
        $this->discountDetails = $discountDetails;

        return $this;
    }

    /**
     * Set the value of baseAmount
     */ 
    public function setBaseAmount($baseAmount)
    {
        $this->baseAmount = $baseAmount;
        return;
    }

    /**
     * Get the value of baseAmount
     */ 
    public function getBaseAmount()
    {
        return $this->baseAmount;
    }

    /**
     * Get the value of discount type
     */ 
    public function getDiscountType()
    {
        return $this->discountDetails->type;
    }

    /**
     * Get the value of discount amount
     */ 
    public function getDiscountDetailAmount()
    {
        return $this->discountDetails->amount;
    }

    /**
     * Get the value of discountAmount
     */ 
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Set the value of discountAmount
     *
     * @return  self
     */ 
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get the value of totalAmount
     */ 
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set the value of totalAmount
     *
     * @return  self
     */ 
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get the value of vatAmount
     */ 
    public function getVatAmount()
    {
        return $this->vatAmount;
    }

    /**
     * Set the value of vatAmount
     *
     * @return  self
     */ 
    public function setVatAmount($vatAmount)
    {
        $this->vatAmount = $vatAmount;

        return $this;
    }

    /**
     * Get the value of vatableAmount
     */ 
    public function getVatableAmount()
    {
        return $this->vatableAmount;
    }

    /**
     * Set the value of vatableAmount
     *
     * @return  self
     */ 
    public function setVatableAmount($vatableAmount)
    {
        $this->vatableAmount = $vatableAmount;

        return $this;
    }

    public function setAPIMatchAccountDetails($api_match_account_details){
        $this->api_match_account_details = $api_match_account_details;
        return $this;
    }

    public function getAPIMatchAccountDetails()
    {
        return $this->api_match_account_details;
    }
    
    /**
     * @param array $vars
     * @return PaymentDTO
     */
    public function setVars($vars = [])
    {
        if (isset($vars['entityid'])) {
            $this->setReportId($vars['entityid']);
        }
        if (isset($vars['loginid'])) {
            $this->setUserId($vars['loginid']);
        }
        if (isset($vars['is_paid'])) {
            $this->setIsPaid($vars['is_paid']);
        }
        if (isset($vars['discount_code'])) {
            $this->setDiscountCode($vars['discount_code']);
        }

        return $this;
    }

}