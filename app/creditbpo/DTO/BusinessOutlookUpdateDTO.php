<?php

namespace CreditBPO\DTO;

class BusinessOutlookUpdateDTO {
    protected $contentTypes;
    protected $destination;
    protected $pdfFilename;
    protected $quarter;
    protected $source;
    protected $markerStart;
    protected $markerStartParsing;
    protected $markerStop;
    protected $textFilename;
    protected $url;
    protected $year;

    /**
     * @param string $contentTypes
     * @return BusinessOutlookUpdateDTO
     */
    public function setContentTypes(array $contentTypes) {
        $this->contentTypes = $contentTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function getContentTypes() {
        return $this->contentTypes;
    }

    /**
     * @param string $destination
     * @return BusinessOutlookUpdateDTO
     */
    public function setDestination($destination) {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @return string
     */
    public function getDestination() {
        return $this->destination;
    }

    /**
     * @param int $quarter
     * @return BusinessOutlookUpdateDTO
     */
    public function setQuarter($quarter) {
        $this->quarter = $quarter;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuarter() {
        return $this->quarter;
    }

    /**
     * @param string $source
     * @return BusinessOutlookUpdateDTO
     */
    public function setSource($source) {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * @param string $source
     * @return BusinessOutlookUpdateDTO
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param int $year
     * @return BusinessOutlookUpdateDTO
     */
    public function setYear($year) {
        $this->year = $year;
        return $this;
    }

    /**
     * @return int
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * @param string $start
     * @return BusinessOutlookUpdateDTO
     */
    public function setMarkerStart($start) {
        $this->markerStart = $start;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarkerStart() {
        return $this->markerStart;
    }

    /**
     * @param string $start
     * @return BusinessOutlookUpdateDTO
     */
    public function setMarkerStartParsing($start) {
        $this->markerStartParsing = $start;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarkerStartParsing() {
        return $this->markerStartParsing;
    }

    /**
     * @param string $start
     * @return BusinessOutlookUpdateDTO
     */
    public function setMarkerStop($stop) {
        $this->markerStop = $stop;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarkerStop() {
        return $this->markerStop;
    }

    /**
     * @param string $name
     * @return BusinessOutlookUpdateDTO
     */
    public function setPdfFilename($name) {
        $this->pdfFilename = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPdfFilename() {
        return $this->pdfFilename;
    }

    /**
     * @param string $name
     * @return BusinessOutlookUpdateDTO
     */
    public function setTextFilename($name) {
        $this->textFilename = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextFilename() {
        return $this->textFilename;
    }
}
