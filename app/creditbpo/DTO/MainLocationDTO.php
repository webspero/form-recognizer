<?php

namespace CreditBPO\DTO;

class MainLocationDTO {
    public $id;
    public $reportid;
    public $sizeofpremises;
    public $premisesownedleased;
    public $location;
    public $premisesusedas;
    public $nooffloors;
    public $locationaddress;
    public $material;
    public $color;
    public $map;
    public $actuallocationphoto;
    public $note;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of reportid
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of reportid
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

     /**
     * Get the value of sizeofpremises
     */ 
    public function getSizeOfPremises()
    {
        return $this->sizeofpremises;
    }

    /**
     * Set the value of sizeofpremises
     *
     * @return  self
     */ 
    public function setSizeOfPremises($sizeOfPremises)
    {
        $this->sizeofpremises = $sizeOfPremises;

        return $this;
    }

     /**
     * Get the value of premisesownedleased
     */ 
    public function getPremisesOwnedLeased()
    {
        return $this->premisesownedleased;
    }

    /**
     * Set the value of premisesownedleased
     *
     * @return  self
     */ 
    public function setPremisesOwnedLeased($premisesOwnedLeased)
    {
        $this->premisesownedleased = $premisesOwnedLeased;

        return $this;
    }

    /**
     * Get the value of location
     */ 
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set the value of location
     *
     * @return  self
     */ 
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get the value of premisesusedas
     */ 
    public function getPremisesUsedAs()
    {
        return $this->premisesusedas;
    }

    /**
     * Set the value of premisesusedas
     *
     * @return  self
     */ 
    public function setPremisesUsedAs($premisesUsedAs)
    {
        $this->premisesusedas = $premisesUsedAs;

        return $this;
    }

    /**
     * Get the value of nooffloors
     */ 
    public function getNoOfFloors()
    {
        return $this->nooffloors;
    }

    /**
     * Set the value of nooffloors
     *
     * @return  self
     */ 
    public function setNoOfFloors($noOfFloors)
    {
        $this->nooffloors = $noOfFloors;

        return $this;
    }

    /**
     * Get the value of locationaddress
     */ 
    public function getLocationAddress()
    {
        return $this->locationaddress;
    }

    /**
     * Set the value of locationaddress
     *
     * @return  self
     */ 
    public function setLocationAddress($locationaddress)
    {
        $this->locationaddress = $locationaddress;

        return $this;
    }

    /**
     * Get the value of material
     */ 
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set the value of material
     *
     * @return  self
     */ 
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get the value of color
     */ 
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */ 
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of map
     */ 
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set the value of map
     *
     * @return  self
     */ 
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get the value of actuallocationphoto
     */ 
    public function getActualLocationPhoto()
    {
        return $this->actuallocationphoto;
    }

    /**
     * Set the value of actuallocationphoto
     *
     * @return  self
     */ 
    public function setActualLocationPhoto($actualLocationPhoto)
    {
        $this->actuallocationphoto = $actualLocationPhoto;

        return $this;
    }

    /**
     * Get the value of note
     */ 
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set the value of note
     *
     * @return  self
     */ 
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['sizeofpremises'])) {
            $this->setSizeOfPremises($vars['sizeofpremises']);
        }
        if (isset($vars['premisesownedleased'])) {
            $this->setPremisesOwnedLeased($vars['premisesownedleased']);
        }
        if (isset($vars['location'])) {
            $this->setLocation($vars['location']);
       	}
        if (isset($vars['premisesusedas'])) {
            $this->setPremisesUsedAs($vars['premisesusedas']);
        }
        if (isset($vars['nooffloors'])) {
            $this->setNoOfFloors($vars['nooffloors']);
        }
        if (isset($vars['locationaddress'])) {
            $this->setLocationAddress($vars['locationaddress']);
        }
        if (isset($vars['material'])) {
            $this->setMaterial($vars['material']);
        }
        if (isset($vars['color'])) {
            $this->setColor($vars['color']);
        }
        if (isset($vars['map'])) {
            $this->setMap($vars['map']);
        }
        if (isset($vars['actuallocationphoto'])) {
            $this->setActualLocationPhoto($vars['actuallocationphoto']);
        }
        if (isset($vars['note'])) {
            $this->setNote($vars['note']);
        }

	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'sizeofpremises' => $this->getSizeOfPremises(),
            'premisesownedleased' => $this->getPremisesOwnedLeased(),
            'location' => $this->getLocation(),
            'premisesusedas' => $this->getPremisesUsedAs(),
            'nooffloors' => $this->getNoOfFloors(),
            'locationaddress' => $this->getLocationAddress(),
            'material' => $this->getMaterial(),
            'color' => $this->getColor(),
            'map' => $this->getMap(),
            'actuallocationphoto' => $this->getActualLocationPhoto(),
            'note' => $this->getNote()
        );
    }
}