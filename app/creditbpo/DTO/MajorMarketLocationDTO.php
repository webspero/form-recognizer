<?php

namespace CreditBPO\DTO;

class MajorMarketLocationDTO {
    public $id;
    public $reportid;
    public $marketlocation;
    
    /**
     * Get the value of relatedCompaniesId
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of relatedcompaniesid
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of entityId
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of entityId
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of marketlocation
     */ 
    public function getMarketLocation()
    {
        return $this->marketlocation;
    }

    /**
     * Set the value of marketlocation
     *
     * @return  self
     */ 
    public function setMarketLocation($marketLocation)
    {
        $this->marketlocation = $marketLocation;

        return $this;
    }
    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['marketlocation'])) {
            $this->setMarketLocation($vars['marketlocation']);
        }
        
	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'marketlocation' => $this->getMarketLocation()
        );
    }
    
}