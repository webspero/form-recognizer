<?php

namespace CreditBPO\DTO;

class UserAttemptDTO {
    protected $id;
    protected $userId;
    protected $attempts;
    protected $countryCode;
    protected $status;
    protected $lastLogin;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of attempts
     */ 
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * Set the value of attempts
     *
     * @return  self
     */ 
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;

        return $this;
    }

    /**
     * Get the value of countryCode
     */ 
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set the value of countryCode
     *
     * @return  self
     */ 
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of lastLogin
     */ 
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set the value of lastLogin
     *
     * @return  self
     */ 
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    #region [set array]
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['userid'])) {
            $this->setUserId($vars['userid']);
        }
        if (isset($vars['attempts'])) {
            $this->setAttempts($vars['attempts']);
        }
        if (isset($vars['countrycode'])) {
            $this->setCountryCode($vars['countrycode']);
        }
        if (isset($vars['status'])) {
            $this->setStatus($vars['status']);
        }
        if (isset($vars['lastlogin'])) {
            $this->setLastLogin($vars['lastlogin']);
        }
    }
}