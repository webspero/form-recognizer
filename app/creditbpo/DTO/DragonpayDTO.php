<?php

namespace CreditBPO\DTO;
use GuzzleHttp\Client as GuzzleClient;

class DragonpayDTO 
{
    private static $merchantId = 'CREDITBPO';
    private static $merchantKey = 'N6f9rEW3';
    protected $rootUrl;
    protected $transactionId;
    protected $user;
    protected $report;
    protected $amount;
    protected $currency;
    protected $description;
    protected $email;
    protected $digest;
    protected $requestUrl;
    protected $referenceNumber;
    protected $status;
    protected $message;
    protected $paidStatus = 2;
    protected $clientReturnUrl;
    
    /**
     * @param array $vars
     */
    public function __construct() {
        /*  For Production Environment  */
		if (env("APP_ENV") == "Production") {
			$this->rootUrl = 'https://gw.dragonpay.ph/api/collect/v1';
		} else {
        /*  For Testing Environment     */
			$this->rootUrl = 'https://test.dragonpay.ph/api/collect/v1';
		}
    }

    /**
     * Get the value of transactionId
     */ 
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set the value of transactionId
     *
     * @return  self
     */ 
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get the value of amount
     */ 
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the value of amount
     *
     * @return  self
     */ 
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of currency
     */ 
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set the value of currency
     *
     * @return  self
     */ 
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of digest
     */ 
    public function getDigest()
    {
        return $this->digest;
    }

    /**
     * Set the value of digest
     *
     * @return  self
     */ 
    public function setDigest($digest)
    {
        $this->digest = $digest;

        return $this;
    }

    /**
     * Get the value of requestUrl
     */ 
    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * Set the value of requestUrl
     *
     * @return  self
     */ 
    public function setRequestUrl()
    {
        // $this->requestUrl = $this->rootUrl.
        // "?merchantid=".self::$merchantId.
        // "&txnid=".$this->transactionId.
        // "&amount=".$this->amount.
        // "&ccy=".$this->currency.
        // "&description=".urlencode($this->description).
        // "&email=".urlencode($this->email).
        // "&digest=".$this->digest
        // ;

        // if($this->clientReturnUrl != null) {
        //     $this->requestUrl .= "&param1=".$this->clientReturnUrl;
        // }

        // return $this;

        /*  Send data to DragonPay  */
        $apiUrl = $this->rootUrl . '/' . $this->transactionId . '/post';
        $client = new GuzzleClient([
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode(self::$merchantId . ':' . self::$merchantKey),
                'Content-Type'	=> 'application/json'
            ]
        ]);

        $postInput = [
            'Amount'		=> $this->amount,
            'Currency'		=> $this->currency,
            'Description'	=> $this->description,
            'Email'			=> $this->email
        ];

        $result = $client->request('POST', $apiUrl, [ 'body' => json_encode($postInput) ]);
        $data = json_decode($result->getBody()->getContents());
        $this->requestUrl = $data->Url;

        return $this;
    }
    
    /**
     * Get the value of referenceNumber
     */ 
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Set the value of referenceNumber
     *
     * @return  self
     */ 
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
    
    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the properties needed for the request in array format
     */ 
    public function getRequestContent()
    {
        return array(
			'merchant_id'   => self::$merchantId,
			'txn_id'        => $this->transactionId,
			'amount'        => number_format($this->amount, 2, ".", ""),
			'currency'      => $this->currency,
			'description'   => $this->description,
			'email'         => $this->email,
			'key'           => self::$merchantKey
		);
    }
    
    /**
     * Get the value of report
     */ 
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set the value of report
     *
     * @return  self
     */ 
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get the value of paidStatus
     */ 
    public function getPaidStatus()
    {
        return $this->paidStatus;
    }

    /**
     * Get the value of clientReturnUrl
     */ 
    public function getClientReturnUrl()
    {
        return $this->clientReturnUrl;
    }

    /**
     * Set the value of clientReturnUrl
     *
     * @return  self
     */ 
    public function setClientReturnUrl($clientReturnUrl)
    {
        $this->clientReturnUrl = $clientReturnUrl;

        return $this;
    }

    /**
     * Get the properties taken from the response in array format
     */ 
    public function getResponseContent()
    {
        return array(
            'id' => $this->transactionId,
            'refno' => $this->referenceNumber,
            'status' => $this->status,
            'message' => $this->message,
			'key' => self::$merchantKey
		);
    }

    /**
     * @param array $vars
     * @return DragonpayDTO
     * For Dragonpay Response Data
     */
    public function setVars($vars = [])
    {
        if (isset($vars['txnid'])) {
            $this->setTransactionId($vars['txnid']);
        }
        if (isset($vars['refno'])) {
            $this->setReferenceNumber($vars['refno']);
        }
        if (isset($vars['status'])) {
            $this->setStatus($vars['status']);
        }
        if (isset($vars['message'])) {
            $this->setMessage($vars['message']);
        }

        return $this;
    }


}