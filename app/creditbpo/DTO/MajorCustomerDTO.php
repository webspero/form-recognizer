<?php

namespace CreditBPO\DTO;

class MajorCustomerDTO {
    protected $id;
    protected $reportId;
    protected $name;
    protected $address;
    protected $contactPerson;
    protected $contactNumber;
    protected $email;
    protected $yearStarted;
    protected $yearsDoingBusiness;
    protected $relationshipSatisfaction;
    protected $salesPercentage;
    protected $orderFrequency;
    protected $paymentBehavior;

    #region [getter & setter]

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of contactPerson
     */ 
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set the value of contactPerson
     *
     * @return  self
     */ 
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get the value of contactNumber
     */ 
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Set the value of contactNumber
     *
     * @return  self
     */ 
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of yearStarted
     */ 
    public function getYearStarted()
    {
        return $this->yearStarted;
    }

    /**
     * Set the value of yearStarted
     *
     * @return  self
     */ 
    public function setYearStarted($yearStarted)
    {
        $this->yearStarted = $yearStarted;

        return $this;
    }

    /**
     * Get the value of yearsDoingBusiness
     */ 
    public function getYearsDoingBusiness()
    {
        return $this->yearsDoingBusiness;
    }

    /**
     * Set the value of yearsDoingBusiness
     *
     * @return  self
     */ 
    public function setYearsDoingBusiness($yearsDoingBusiness)
    {
        $this->yearsDoingBusiness = $yearsDoingBusiness;

        return $this;
    }

    /**
     * Get the value of relationshipSatisfaction
     */ 
    public function getRelationshipSatisfaction()
    {
        return $this->relationshipSatisfaction;
    }

    /**
     * Set the value of relationshipSatisfaction
     *
     * @return  self
     */ 
    public function setRelationshipSatisfaction($relationshipSatisfaction)
    {
        $this->relationshipSatisfaction = $relationshipSatisfaction;

        return $this;
    }

    /**
     * Get the value of salesPercentage
     */ 
    public function getSalesPercentage()
    {
        return $this->salesPercentage;
    }

    /**
     * Set the value of salesPercentage
     *
     * @return  self
     */ 
    public function setSalesPercentage($salesPercentage)
    {
        $this->salesPercentage = $salesPercentage;

        return $this;
    }

    /**
     * Get the value of orderFrequency
     */ 
    public function getOrderFrequency()
    {
        return $this->orderFrequency;
    }

    /**
     * Set the value of orderFrequency
     *
     * @return  self
     */ 
    public function setOrderFrequency($orderFrequency)
    {
        $this->orderFrequency = $orderFrequency;

        return $this;
    }

    /**
     * Get the value of paymentBehavior
     */ 
    public function getPaymentBehavior()
    {
        return $this->paymentBehavior;
    }

    /**
     * Set the value of paymentBehavior
     *
     * @return  self
     */ 
    public function setPaymentBehavior($paymentBehavior)
    {
        $this->paymentBehavior = $paymentBehavior;

        return $this;
    }
    
    #endregion [getter & setter]

    #region [set array]

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['name'])) {
            $this->setName($vars['name']);
        }
        if (isset($vars['address'])) {
            $this->setAddress($vars['address']);
        }
        if (isset($vars['contactperson'])) {
            $this->setContactPerson($vars['contactperson']);
        }
        if (isset($vars['contactnumber'])) {
            $this->setContactNumber($vars['contactnumber']);
        }
        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }
        if (isset($vars['yearstarted'])) {
            $this->setYearStarted($vars['yearstarted']);
        }
        if (isset($vars['yearsdoingbusiness'])) {
            $this->setYearsDoingBusiness($vars['yearsdoingbusiness']);
        }
        if (isset($vars['relationshipsatisfaction'])) {
            $this->setRelationshipSatisfaction($vars['relationshipsatisfaction']);
        }
        if (isset($vars['salespercentage'])) {
            $this->setSalesPercentage($vars['salespercentage']);
        }
        if (isset($vars['orderfrequency'])) {
            $this->setOrderFrequency($vars['orderfrequency']);
        }
        if (isset($vars['paymentbehavior'])) {
            $this->setPaymentBehavior($vars['paymentbehavior']);
        }
    }

    #endregion [set array]

    #region [get array]

    /**
     * @param array $vars
     * @return MajorCustomer
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'name' => $this->getName(),
            'address' => $this->getAddress(),
            'contactperson' => $this->getContactPerson(),
            'contactnumber' => $this->getContactNumber(),
            'email' => $this->getEmail(),
            'yearstarted' => $this->getYearStarted(),
            'yearsdoingbusiness' => $this->getYearsDoingBusiness(),
            'relationshipsatisfaction' => $this->getRelationshipSatisfaction(),
            'salespercentage' => $this->getSalesPercentage(),
            'orderfrequency' => $this->getOrderFrequency(),
            'paymentbehavior' => $this->getPaymentBehavior()
        );
    }

    #endregion [get array]
}