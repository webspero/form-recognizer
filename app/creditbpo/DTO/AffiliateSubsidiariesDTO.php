<?php

namespace CreditBPO\DTO;

class AffiliateSubsidiariesDTO {
    public $id;
    public $reportid;
    public $companyname;
    public $companyaddress;
    public $companyphone;
    public $companyemail;
    

    /**
     * Get the value of relatedCompaniesId
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of relatedcompaniesid
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of entityId
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of entityId
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of relatedCompanyName
     */ 
    public function getRelatedCompanyName()
    {
        return $this->companyname;
    }

    /**
     * Set the value of relatedCompanyName
     *
     * @return  self
     */ 
    public function setRelatedCompanyName($companyname)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Get the value of relatedCompanyName
     */ 
    public function getRelatedCompanyaddress()
    {
        return $this->companyaddress;
    }

    /**
     * Set the value of relatedCompanyName
     *
     * @return  self
     */ 
    public function setRelatedCompanyAddress($companyaddress)
    {
        $this->companyaddress = $companyaddress;

        return $this;
    }

    /**
     * Get the value of relatedCompanyPhone
     */ 
    public function getRelatedCompanyPhone()
    {
        return $this->companyphone;
    }

    /**
     * Set the value of relatedCompanyPhone
     *
     * @return  self
     */ 
    public function setRelatedCompanyPhone($companyphone)
    {
        $this->companyphone = $companyphone;

        return $this;
    }

    /**
     * Get the value of relatedCompanyEmail
     */ 
    public function getRelatedCompanyEmail()
    {
        return $this->companyemail;
    }

    /**
     * Set the value of relatedCompanyEmail
     *
     * @return  self
     */ 
    public function setRelatedCompanyEmail($companyemail)
    {
        $this->companyemail = $companyemail;

        return $this;
    }

    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['companyname'])) {
            $this->setRelatedCompanyName($vars['companyname']);
        }
        if (isset($vars['companyaddress'])) {
            $this->setRelatedCompanyAddress($vars['companyaddress']);
       	}
        if (isset($vars['companyphone'])) {
            $this->setRelatedCompanyPhone($vars['companyphone']);
        }
        if (isset($vars['companyemail'])) {
            $this->setRelatedCompanyEmail($vars['companyemail']);
        }

	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'companyname' => $this->getRelatedCompanyName(),
            'companyaddress' => $this->getRelatedCompanyAddress(),
            'companyphone' => $this->getRelatedCompanyPhone(),
            'companyemail' => $this->getRelatedCompanyEmail()
        );
    }
    
}