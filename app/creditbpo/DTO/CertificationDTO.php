<?php

namespace CreditBPO\DTO;

class CertificationDTO {
    public $id;
    public $reportid;
    public $certification;
    public $registrationno;
    public $registrationdate;
    public $uploadfile;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of reportid
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of reportid
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of certification
     */ 
    public function getCertification()
    {
        return $this->certification;
    }

    /**
     * Set the value of certification
     *
     * @return  self
     */ 
    public function setCertification($certification)
    {
        $this->certification = $certification;

        return $this;
    }

    /**
     * Get the value of registrationno
     */ 
    public function getRegistrationNo()
    {
        return $this->registrationno;
    }

    /**
     * Set the value of registrationno
     *
     * @return  self
     */ 
    public function setRegistrationNo($registrationNo)
    {
        $this->registrationno = $registrationNo;

        return $this;
    }

    /**
     * Get the value of registrationdate
     */ 
    public function getRegistrationDate()
    {
        return $this->registrationdate;
    }

     /**
     * Set the value of registrationdate
     *
     * @return  self
     */ 
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationdate = $registrationDate;

        return $this;
    }

    /**
     * Get the value of uploadfile
     */ 
    public function getUploadFile()
    {
        return $this->uploadfile;
    }

   
    /**
     * Set the value of uploadfile
     *
     * @return  self
     */ 
    public function setUploadFile($uploadfile)
    {
        $this->uploadfile = $uploadfile;

        return $this;
    }
    
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['certification'])) {
            $this->setCertification($vars['certification']);
        }
        if (isset($vars['registrationno'])) {
            $this->setRegistrationNo($vars['registrationno']);
       	}
        if (isset($vars['registrationdate'])) {
            $this->setRegistrationDate($vars['registrationdate']);
        }
        if (isset($vars['uploadfile'])) {
            $this->setUploadFile($vars['uploadfile']);
        }
       
	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'certification' => $this->getCertification(),
            'registrationno' => $this->getRegistrationNo(),
            'registrationdate' => $this->getRegistrationDate(),
            'uploadfile' => $this->getUploadFile()
        );
    }
    
}