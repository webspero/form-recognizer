<?php

namespace CreditBPO\DTO;

class SMEScoringDTO {
    private $averageDailyBalance;
    private $businessOutlookIndexScore;
    private $businessOwnerExperience;
    private $convertedScore;
    private $customerDependency;
    private $entityId;
    private $financialPerformance;
    private $financialPosition;
    private $loginId = 0;
    private $mainDependency = 0;
    private $managementTeamExperience;
    private $operatingCashFlowMargin;
    private $operatingCashFlowRatio;
    private $pastProjectFutureInitiatives = 0;
    private $rawScore;
    private $riskManagement;
    private $riskScore;
    private $successionPlan;
    private $supplierDependency;

    /**
     * @param array $vars
     */
    public function __construct($vars = []) {
        $this->setVars($vars);
    }

    /**
     * @param float $averageDailyBalance
     * @return FinancialAnalysisDTO
     */
    public function setAverageDailyBalance($averageDailyBalance) {
        $this->averageDailyBalance = $averageDailyBalance;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAverageDailyBalance() {
        return $this->averageDailyBalance;
    }

    /**
     * @param float $businessOutlookIndexScore
     * @return FinancialAnalysisDTO
     */
    public function setBusinessOutlookIndexScore($businessOutlookIndexScore) {
        $this->businessOutlookIndexScore = $businessOutlookIndexScore;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getBusinessOutlookIndexScore() {
        return $this->businessOutlookIndexScore;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setBusinessOwnerExperience($businessOwnerExperience) {
        $this->businessOwnerExperience = $businessOwnerExperience;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBusinessOwnerExperience() {
        return $this->businessOwnerExperience;
    }

    /**
     * @param int $convertedScore
     * @return FinancialAnalysisDTO
     */
    public function setConvertedScore($convertedScore) {
        $this->convertedScore = $convertedScore;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getConvertedScore() {
        return $this->convertedScore;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setCustomerDependency($customerDependency) {
        $this->customerDependency = $customerDependency;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCustomerDependency() {
        return $this->customerDependency;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setEntityId($entityId) {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getEntityId() {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setFinancialPerformance($financialPerformance) {
        $this->financialPerformance = $financialPerformance;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFinancialPerformance() {
        return $this->financialPerformance;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setFinancialPosition($financialPosition) {
        $this->financialPosition = $financialPosition;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFinancialPosition() {
        return $this->financialPosition;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setLoginId($loginId) {
        $this->loginId = $loginId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLoginId() {
        return $this->loginId;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setMainDependency($mainDependency) {
        $this->mainDependency = $mainDependency;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMainDependency() {
        return $this->mainDependency;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setManagementTeamExperience($managementTeamExperience) {
        $this->managementTeamExperience = $managementTeamExperience;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getManagementTeamExperience() {
        return $this->managementTeamExperience;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setOperatingCashFlowMargin($operatingCashFlowMargin) {
        $this->operatingCashFlowMargin = $operatingCashFlowMargin;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOperatingCashFlowMargin() {
        return $this->operatingCashFlowMargin;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setOperatingCashFlowRatio($operatingCashFlowRatio) {
        $this->operatingCashFlowRatio = $operatingCashFlowRatio;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOperatingCashFlowRatio() {
        return $this->operatingCashFlowRatio;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setPastProjectFutureInitiatives($pastProjectFutureInitiatives) {
        $this->pastProjectFutureInitiatives = $pastProjectFutureInitiatives;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPastProjectFutureInitiatives() {
        return $this->pastProjectFutureInitiatives;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setRawScore($rawScore) {
        $this->rawScore = $rawScore;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRawScore() {
        return $this->rawScore;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setRiskManagement($riskManagement) {
        $this->riskManagement = $riskManagement;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRiskManagement() {
        return $this->riskManagement;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setRiskScore($riskScore) {
        $this->riskScore = $riskScore;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRiskScore() {
        return $this->riskScore;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setSuccessionPlan($successionPlan) {
        $this->successionPlan = $successionPlan;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSuccessionPlan() {
        return $this->successionPlan;
    }

    /**
     * @param int $entityId
     * @return FinancialAnalysisDTO
     */
    public function setSupplierDependency($supplierDependency) {
        $this->supplierDependency = $supplierDependency;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSupplierDependency() {
        return $this->supplierDependency;
    }

    /**
     * @param array $vars
     * @return FinancialAnalysisDTO
     */
    public function setVars($vars = []) {
        if (isset($vars['average_daily_balance'])) {
            $this->setAverageDailyBalance($vars['average_daily_balance']);
        }

        if (isset($vars['business_owner_experience'])) {
            $this->setBusinessOwnerExperience($vars['business_owner_experience']);
        }

        if (isset($vars['business_outlook_index_score'])) {
            $this->setBusinessOutlookIndexScore($vars['business_outlook_index_score']);
        }

        if (isset($vars['converted_score'])) {
            $this->setConvertedScore($vars['converted_score']);
        }

        if (isset($vars['customer_dependency'])) {
            $this->setCustomerDependency($vars['customer_dependency']);
        }

        if (isset($vars['entity_id'])) {
            $this->setEntityId($vars['entity_id']);
        }

        if (isset($vars['financial_performance'])) {
            $this->setFinancialPerformance($vars['financial_performance']);
        }

        if (isset($vars['financial_position'])) {
            $this->setFinancialPosition($vars['financial_position']);
        }

        if (isset($vars['login_id'])) {
            $this->setLoginId($vars['login_id']);
        }

        if (isset($vars['main_dependency'])) {
            $this->setMainDependency($vars['main_dependency']);
        }

        if (isset($vars['management_team_experience'])) {
            $this->setManagementTeamExperience($vars['management_team_experience']);
        }

        if (isset($vars['operating_cash_flow_margin'])) {
            $this->setOperatingCashFlowMargin($vars['operating_cash_flow_margin']);
        }

        if (isset($vars['operating_cash_flow_ratio'])) {
            $this->setOperatingCashFlowRatio($vars['operating_cash_flow_ratio']);
        }

        if (isset($vars['past_project_future_initiatives'])) {
            $this->setPastProjectFutureInitiatives($vars['past_project_future_initiatives']);
        }

        if (isset($vars['raw_score'])) {
            $this->setRawScore($vars['raw_score']);
        }

        if (isset($vars['risk_management'])) {
            $this->setRiskManagement($vars['risk_management']);
        }

        if (isset($vars['risk_score'])) {
            $this->setRiskScore($vars['risk_score']);
        }

        if (isset($vars['succession_plan'])) {
            $this->setSuccessionPlan($vars['succession_plan']);
        }

        if (isset($vars['supplier_dependency'])) {
            $this->setSupplierDependency($vars['supplier_dependency']);
        }

        return $this;
    }
}
