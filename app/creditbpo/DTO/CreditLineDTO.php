<?php

namespace CreditBPO\DTO;

use URL;

class CreditLineDTO 
{
    protected $creditLineId;
    protected $reportId;
    protected $amountOfLine;
    protected $purposeCreditFacility;
    protected $supplierCreditTerms;
    protected $othersPurposeCreditFacility;
    protected $otherRemarks;
    protected $DepositAccountType;
    protected $depositAmount;
    protected $marketableSecurities;
    protected $marketableSecuritiesValue;
    protected $property;
    protected $propertyValue;
    protected $chattel;
    protected $chattelValue;
    protected $others;
    protected $othersValue;
    protected $errors;

    /**
     * Get the value of creditLineId
     */ 
    public function getCreditLineId()
    {
        return $this->creditLineId;
    }

    /**
     * Set the value of creditLineId
     *
     * @return  self
     */ 
    public function setCreditLineId($creditLineId)
    {
        $this->creditLineId = $creditLineId;

        return $this;
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of amountOfLine
     */ 
    public function getAmountOfLine()
    {
        return $this->amountOfLine;
    }

    /**
     * Set the value of amountOfLine
     *
     * @return  self
     */ 
    public function setAmountOfLine($amountOfLine)
    {
        $this->amountOfLine = $amountOfLine;

        return $this;
    }

    /**
     * Get the value of purposeCreditFacility
     */ 
    public function getPurposeCreditFacility()
    {
        return $this->purposeCreditFacility;
    }

    /**
     * Set the value of purposeCreditFacility
     *
     * @return  self
     */ 
    public function setPurposeCreditFacility($purposeCreditFacility)
    {
        $this->purposeCreditFacility = $purposeCreditFacility;

        return $this;
    }

    /**
     * Get the value of supplierCreditTerms
     */ 
    public function getSupplierCreditTerms()
    {
        return $this->supplierCreditTerms;
    }

    /**
     * Set the value of supplierCreditTerms
     *
     * @return  self
     */ 
    public function setSupplierCreditTerms($supplierCreditTerms)
    {
        $this->supplierCreditTerms = $supplierCreditTerms;

        return $this;
    }

    /**
     * Get the value of othersPurposeCreditFacility
     */ 
    public function getOthersPurposeCreditFacility()
    {
        return $this->othersPurposeCreditFacility;
    }

    /**
     * Set the value of othersPurposeCreditFacility
     *
     * @return  self
     */ 
    public function setOthersPurposeCreditFacility($othersPurposeCreditFacility)
    {
        $this->othersPurposeCreditFacility = $othersPurposeCreditFacility;

        return $this;
    }

    /**
     * Get the value of otherRemarks
     */ 
    public function getOtherRemarks()
    {
        return $this->otherRemarks;
    }

    /**
     * Set the value of otherRemarks
     *
     * @return  self
     */ 
    public function setOtherRemarks($otherRemarks)
    {
        $this->otherRemarks = $otherRemarks;

        return $this;
    }

    /**
     * Get the value of DepositAccountType
     */ 
    public function getDepositAccountType()
    {
        return $this->DepositAccountType;
    }

    /**
     * Set the value of DepositAccountType
     *
     * @return  self
     */ 
    public function setDepositAccountType($DepositAccountType)
    {
        $this->DepositAccountType = $DepositAccountType;

        return $this;
    }

    /**
     * Get the value of depositAmount
     */ 
    public function getDepositAmount()
    {
        return $this->depositAmount;
    }

    /**
     * Set the value of depositAmount
     *
     * @return  self
     */ 
    public function setDepositAmount($depositAmount)
    {
        $this->depositAmount = $depositAmount;

        return $this;
    }

    /**
     * Get the value of marketableSecurities
     */ 
    public function getMarketableSecurities()
    {
        return $this->marketableSecurities;
    }

    /**
     * Set the value of marketableSecurities
     *
     * @return  self
     */ 
    public function setMarketableSecurities($marketableSecurities)
    {
        $this->marketableSecurities = $marketableSecurities;

        return $this;
    }

    /**
     * Get the value of marketableSecuritiesValue
     */ 
    public function getMarketableSecuritiesValue()
    {
        return $this->marketableSecuritiesValue;
    }

    /**
     * Set the value of marketableSecuritiesValue
     *
     * @return  self
     */ 
    public function setMarketableSecuritiesValue($marketableSecuritiesValue)
    {
        $this->marketableSecuritiesValue = $marketableSecuritiesValue;

        return $this;
    }

    /**
     * Get the value of property
     */ 
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set the value of property
     *
     * @return  self
     */ 
    public function setProperty($property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get the value of propertyValue
     */ 
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * Set the value of propertyValue
     *
     * @return  self
     */ 
    public function setPropertyValue($propertyValue)
    {
        $this->propertyValue = $propertyValue;

        return $this;
    }

    /**
     * Get the value of chattel
     */ 
    public function getChattel()
    {
        return $this->chattel;
    }

    /**
     * Set the value of chattel
     *
     * @return  self
     */ 
    public function setChattel($chattel)
    {
        $this->chattel = $chattel;

        return $this;
    }

    /**
     * Get the value of chattelValue
     */ 
    public function getChattelValue()
    {
        return $this->chattelValue;
    }

    /**
     * Set the value of chattelValue
     *
     * @return  self
     */ 
    public function setChattelValue($chattelValue)
    {
        $this->chattelValue = $chattelValue;

        return $this;
    }

    /**
     * Get the value of others
     */ 
    public function getOthers()
    {
        return $this->others;
    }

    /**
     * Set the value of others
     *
     * @return  self
     */ 
    public function setOthers($others)
    {
        $this->others = $others;

        return $this;
    }

    /**
     * Get the value of othersValue
     */ 
    public function getOthersValue()
    {
        return $this->othersValue;
    }

    /**
     * Set the value of othersValue
     *
     * @return  self
     */ 
    public function setOthersValue($othersValue)
    {
        $this->othersValue = $othersValue;

        return $this;
    }

    /**
     * @param array $vars
     * @return CreditLineDTO
     */
    public function setVars($vars) {
        if (isset($vars->id)) {
            $this->setCreditLineId($vars->id);
        }
        if (isset($vars->entity_id)) {
            $this->setReportId($vars->entity_id);
        }
        if (isset($vars->plan_credit_availment)) {
            $this->setAmountOfLine($vars->plan_credit_availment);
        }
        if (isset($vars->purpose_credit_facility)) {
            $this->setPurposeCreditFacility($vars->purpose_credit_facility);
        }
        if (isset($vars->credit_terms)) {
            $this->setSupplierCreditTerms($vars->credit_terms);
        }
        if (isset($vars->purpose_credit_facility_others)) {
            $this->setOthersPurposeCreditFacility($vars->purpose_credit_facility_others);
        }
        if (isset($vars->other_remarks)) {
            $this->setOtherRemarks($vars->other_remarks);
        }
        if (isset($vars->cash_deposit_details)) {
            $this->setDepositAccountType($vars->cash_deposit_details);
        }
        if (isset($vars->cash_deposit_amount)) {
            $this->setDepositAmount($vars->cash_deposit_amount);
        }
        if (isset($vars->securities_details)) {
            $this->setMarketableSecurities($vars->securities_details);
        }
        if (isset($vars->securities_estimated_value)) {
            $this->setMarketableSecuritiesValue($vars->securities_estimated_value);
        }
        if (isset($vars->property_details)) {
            $this->setProperty($vars->property_details);
        }
        if (isset($vars->property_estimated_value)) {
            $this->setPropertyValue($vars->property_estimated_value);
        }
        if (isset($vars->chattel_details)) {
            $this->setChattel($vars->chattel_details);
        }
        if (isset($vars->chattel_estimated_value)) {
            $this->setChattelValue($vars->chattel_estimated_value);
        }
        if (isset($vars->others_details)) {
            $this->setOthers($vars->others_details);
        }
        if (isset($vars->others_estimated_value)) {
            $this->setOthersValue($vars->others_estimated_value);
        }

        return $this;
    }

    /**
     * Get View values
     * @return array
     */
    public function getFormVars()
    {
        return array(
            'creditlineid' => $this->getCreditLineId(),
            'amountofline' => $this->getAmountOfLine(),
            'purposeofcreditfacility' => $this->getPurposeCreditFacility(),
            'creditterms' => $this->getSupplierCreditTerms(),
            'purposeofcreditfacilityothers' => $this->getOthersPurposeCreditFacility(),
            'otherremarks' => $this->getOtherRemarks(),
            'typeofdepositaccount' => $this->getDepositAccountType(),
            'depositamount' => $this->getDepositAmount(),
            'marketablesecurities' => $this->getMarketableSecurities(),
            'marketablesecuritiesestimatedvalue' => $this->getMarketableSecuritiesValue(),
            'property' => $this->getProperty(),
            'propertyestimatedvalue' => $this->getPropertyValue(),
            'chattel' => $this->getChattel(),
            'chattelestimatedvalue' => $this->getChattelValue(),
            'others' => $this->getOthers(),
            'othersestimatedvalue' => $this->getOthersValue(),
            'link' => URL::route('creditline.get', array($this->getReportId(), $this->getCreditLineId()))
        );
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @param array $vars
     * @return CreditLineDTO
     */
    public function setRequestVars($vars = [], $reportId) {
        if (isset($reportId)) {
            $this->setReportId($reportId);
        }
        if (isset($vars['amountofline'])) {
            $this->setAmountOfLine($vars['amountofline']);
        }
        if (isset($vars['purposeofcreditfacility'])) {
            $this->setPurposeCreditFacility($vars['purposeofcreditfacility']);
        }
        if (isset($vars['creditterms'])) {
            $this->setSupplierCreditTerms($vars['creditterms']);
        }
        if (isset($vars['purposeofcreditfacilityothers'])) {
            $this->setOthersPurposeCreditFacility($vars['purposeofcreditfacilityothers']);
        }
        if (isset($vars['otherremarks'])) {
            $this->setOtherRemarks($vars['otherremarks']);
        }
        if (isset($vars['typeofdepositaccount'])) {
            $this->setDepositAccountType($vars['typeofdepositaccount']);
        }
        if (isset($vars['depositamount'])) {
            $this->setDepositAmount($vars['depositamount']);
        }
        if (isset($vars['marketablesecurities'])) {
            $this->setMarketableSecurities($vars['marketablesecurities']);
        }
        if (isset($vars['marketablesecuritiesestimatedvalue'])) {
            $this->setMarketableSecuritiesValue($vars['marketablesecuritiesestimatedvalue']);
        }
        if (isset($vars['property'])) {
            $this->setProperty($vars['property']);
        }
        if (isset($vars['propertyestimatedvalue'])) {
            $this->setPropertyValue($vars['propertyestimatedvalue']);
        }
        if (isset($vars['chattel'])) {
            $this->setChattel($vars['chattel']);
        }
        if (isset($vars['chattelestimatedvalue'])) {
            $this->setChattelValue($vars['chattelestimatedvalue']);
        }
        if (isset($vars['others'])) {
            $this->setOthers($vars['others']);
        }
        if (isset($vars['othersestimatedvalue'])) {
            $this->setOthersValue($vars['othersestimatedvalue']);
        }

        return $this;
    }
}