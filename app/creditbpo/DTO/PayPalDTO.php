<?php

namespace CreditBPO\DTO;

class PayPalDTO 
{
    protected $cancelUrl;
    protected $returnUrl;
    protected $productName = 'CreditBPO Services';
    protected $productDescription = 'CreditBPO Rating Report®';
    protected $amount;
    protected $currency = 'PHP';
    protected $referenceId;
    protected $username;
    protected $password;
    protected $signature;
    protected $logoImage = '/images/creditbpo-logo.png';
    protected $isTest;
    protected $acknowledgement;
    protected $transactionId;
    protected $transactionType;
    protected $paymentType;
    protected $user;
    protected $report;
    protected $receipt;
    protected $clientReturnUrl;

    /**
     * @param array $vars
     */
    public function __construct() {
        /*  For Production Environment  */
		if (env("APP_ENV") == "Production") {
            $this->username = 'lia.francisco_api1.gmail.com';
            $this->password = 'Z8FEJ5G84FBQNKDJ';
            $this->signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE8P-Gm.vMjDPg.1XEMBeQYnfCYs';
            $this->isTest = false;
		} else {
        /*  For Testing Environment     */
            $this->username = 'lia.francisco-facilitator_api1.gmail.com';
            $this->password = '87DWVXH4UGHD46W3';
            $this->signature = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-Ae4oJbzOKgtGxX4E57BGYAFOf9JR';
            $this->isTest = true;
		}
    }
    
    /**
     * Get the value of amount
     */ 
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set the value of amount
     *
     * @return  self
     */ 
    public function setAmount($amount)
    {
        $this->amount = $amount;
        
        return $this;
    }
    
    /**
     * Get the value of referenceId
     */ 
    public function getReferenceId()
    {
        return $this->referenceId;
    }
    
    /**
     * Set the value of referenceId
     *
     * @return  self
     */ 
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        
        return $this;
    }
    
    /**
     * Get the value of cancelUrl
     */ 
    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }
    
    /**
     * Get the value of returnUrl
     */ 
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }
    
    /**
     * Get the value of productName
     */ 
    public function getProductName()
    {
        return $this->productName;
    }
    
    /**
     * Get the value of productDescription
     */ 
    public function getProductDescription()
    {
        return $this->productDescription;
    }
    
    /**
     * Set the value of currency
     *
     * @return  self
     */ 
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * Get the value of currency
     */ 
    public function getCurrency()
    {
        return $this->currency;
    }
    
    /**
     * Get the value of username
     */ 
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Get the value of signature
     */ 
    public function getSignature()
    {
        return $this->signature;
    }
    
    /**
     * Get the value of logoImage
     */ 
    public function getLogoImage()
    {
        return $this->logoImage;
    }
    
    /**
     * Get the value of isTest
     */ 
    public function getIsTest()
    {
        return $this->isTest;
    }
    
    /**
     * Get the value of acknowledgement
     */ 
    public function getAcknowledgement()
    {
        return $this->acknowledgement;
    }

    /**
     * Set the value of acknowledgement
     *
     * @return  self
     */ 
    public function setAcknowledgement($acknowledgement)
    {
        $this->acknowledgement = $acknowledgement;

        return $this;
    }

    /**
     * Get the value of transactionId
     */ 
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set the value of transactionId
     *
     * @return  self
     */ 
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get the value of transactionType
     */ 
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * Set the value of transactionType
     *
     * @return  self
     */ 
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * Get the value of paymentType
     */ 
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set the value of paymentType
     *
     * @return  self
     */ 
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of report
     */ 
    public function getReport()
    {
        return $this->report;
    }
    
    /**
     * Set the value of report
     *
     * @return  self
     */ 
    public function setReport($report)
    {
        $this->report = $report;
        
        return $this;
    }
    
    /**
     * Get the value of receipt
     */ 
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * Set the value of receipt
     *
     * @return  self
     */ 
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;

        return $this;
    }

    /**
     * Get the value of clientReturnUrl
     */ 
    public function getClientReturnUrl()
    {
        return $this->clientReturnUrl;
    }

    /**
     * Set the value of clientReturnUrl
     *
     * @return  self
     */ 
    public function setClientReturnUrl($clientReturnUrl)
    {
        $this->clientReturnUrl = $clientReturnUrl;

        return $this;
    }

    /**
     * @param array $vars
     * @return PaypalDTO
     */
    public function setVars($vars = [])
    {
        if (isset($vars['total_amount'])) {
            $this->setAmount($vars['total_amount']);
        }
        if (isset($vars['reference_id'])) {
            $this->setReferenceId($vars['reference_id']);
        }

        return $this;
    }

    /**
     * @param array $vars
     * @return PaypalDTO
     */
    public function setResponseVars($vars = [])
    {
        if (isset($vars['PAYMENTINFO_0_ACK'])) {
            $this->setAcknowledgement($vars['PAYMENTINFO_0_ACK']);
        }
        if (isset($vars['PAYMENTINFO_0_TRANSACTIONID'])) {
            $this->setTransactionId($vars['PAYMENTINFO_0_TRANSACTIONID']);
        }
        if (isset($vars['PAYMENTINFO_0_TRANSACTIONTYPE'])) {
            $this->setTransactionType($vars['PAYMENTINFO_0_TRANSACTIONTYPE']);
        }
        if (isset($vars['PAYMENTINFO_0_PAYMENTTYPE'])) {
            $this->setPaymentType($vars['PAYMENTINFO_0_PAYMENTTYPE']);
        }
        if (isset($vars['PAYMENTINFO_0_AMT'])) {
            $this->setAmount($vars['PAYMENTINFO_0_AMT']);
        }
        if (isset($vars['PAYMENTINFO_0_CURRENCYCODE'])) {
            $this->setCurrency($vars['PAYMENTINFO_0_CURRENCYCODE']);
        }

        return $this;
    }
}