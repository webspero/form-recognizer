<?php

namespace CreditBPO\DTO;

class BillingDetailsDTO
{
    protected $streetAddress;
    protected $city;
    protected $province;
    protected $zipcode;
    protected $phoneNumber;
    protected $userId;
    protected $errors;

    /**
     * @param array $vars
     */
    public function __construct($vars = [])
    {
        $this->setVars($vars);
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get the value of streetAddress
     */ 
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set the value of streetAddress
     *
     * @return  self
     */ 
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get the value of userId
     */ 
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set the value of province
     *
     * @return  self
     */ 
    public function setProvince($province)
    {
        $this->province = $province;
        return $this;
    }

    /**
     * Get the value of zipcode
     */ 
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set the value of zipcode
     *
     * @return  self
     */ 
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

       /**
     * Get the value of phoneNumber
     */ 
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set the value of phoneNumber
     *
     * @return  self
     */ 
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of error
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['streetaddress'])) {
            $this->setStreetAddress($vars['streetaddress']);
        }
        if (isset($vars['city'])) {
            $this->setCity($vars['city']);
        }
        if (isset($vars['province'])) {
            $this->setProvince($vars['province']);
        }
        if (isset($vars['zipcode'])) {
            $this->setZipcode($vars['zipcode']);
        }
        if (isset($vars['phonenumber'])) {
            $this->setPhoneNumber($vars['phonenumber']);
        }
        if (isset($vars['userid'])) {
            $this->setUserId($vars['userid']);
        }

        return $this;
    }

    /**
     * @param array $vars
     * @return BillingDetailsDTO
     */
    public function getVars()
    {
        return array(
            'user_id'           => $this->getUserId(),
            'street_address'    => $this->getStreetAddress(),
            'city'              => $this->getCity(),
            'province'          => $this->getProvince(),
            'zipcode'           => $this->getZipcode(),
            'phoneNumber'       => $this->getPhoneNumber()
        );
    }
}