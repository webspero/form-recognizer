<?php

namespace CreditBPO\DTO;

class FinancialReportDTO 
{
    protected $financialReportId;
    protected $reportId;
    protected $title;
    protected $year;
    protected $industryId;
    protected $moneyFactor;
    protected $currency;
    protected $step;
    protected $fsTemplateFile;

    protected $balanceSheets;
    protected $incomeStatements;
    protected $cashflows;

    /**
     * Get the value of financialReportId
     */ 
    public function getFinancialReportId()
    {
        return $this->financialReportId;
    }

    /**
     * Set the value of financialReportId
     *
     * @return  self
     */ 
    public function setFinancialReportId($financialReportId)
    {
        $this->financialReportId = $financialReportId;

        return $this;
    }

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of year
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of industryId
     */ 
    public function getIndustryId()
    {
        return $this->industryId;
    }

    /**
     * Set the value of industryId
     *
     * @return  self
     */ 
    public function setIndustryId($industryId)
    {
        $this->industryId = $industryId;

        return $this;
    }

    /**
     * Get the value of moneyFactor
     */ 
    public function getMoneyFactor()
    {
        return $this->moneyFactor;
    }

    /**
     * Set the value of moneyFactor
     *
     * @return  self
     */ 
    public function setMoneyFactor($moneyFactor)
    {
        $this->moneyFactor = $moneyFactor;

        return $this;
    }

    /**
     * Get the value of currency
     */ 
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set the value of currency
     *
     * @return  self
     */ 
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get the value of step
     */ 
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set the value of step
     *
     * @return  self
     */ 
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get the value of fsTemplateFile
     */ 
    public function getFsTemplateFile()
    {
        return $this->fsTemplateFile;
    }

    /**
     * Set the value of fsTemplateFile
     *
     * @return  self
     */ 
    public function setFsTemplateFile($fsTemplateFile)
    {
        $this->fsTemplateFile = $fsTemplateFile;

        return $this;
    }

    /**
     * @param array $vars
     * @return FinancialReportDTO
     */
    public function setDbVars($vars) 
    {
        if (isset($vars->id)) {
            $this->setFinancialReportId($vars->id);
        }
        if (isset($vars->entity_id)) {
            $this->setReportId($vars->entity_id);
        }
        if (isset($vars->title)) {
            $this->setTitle($vars->title);
        }
        if (isset($vars->year)) {
            $this->setYear($vars->year);
        }
        if (isset($vars->industry_id)) {
            $this->setIndustryId($vars->industry_id);
        }
        if (isset($vars->money_factor)) {
            $this->setMoneyFactor($vars->money_factor);
        }
        if (isset($vars->currency)) {
            $this->setCurrency($vars->currency);
        }
        if (isset($vars->step)) {
            $this->setStep($vars->step);
        }
        if (isset($vars->fstemplate_file)) {
            $this->setFsTemplateFile($vars->fstemplate_file);
        }

        return $this;
    }

    /**
     * Get the value of balanceSheets
     */ 
    public function getBalanceSheets()
    {
        return $this->balanceSheets;
    }

    /**
     * Set the value of balanceSheets
     *
     * @return  self
     */ 
    public function setBalanceSheets($balanceSheets)
    {
        $this->balanceSheets = $balanceSheets;

        return $this;
    }

    /**
     * Get the value of incomeStatements
     */ 
    public function getIncomeStatements()
    {
        return $this->incomeStatements;
    }

    /**
     * Set the value of incomeStatements
     *
     * @return  self
     */ 
    public function setIncomeStatements($incomeStatements)
    {
        $this->incomeStatements = $incomeStatements;

        return $this;
    }

    /**
     * Get the value of cashflows
     */ 
    public function getCashflows()
    {
        return $this->cashflows;
    }

    /**
     * Set the value of cashflows
     *
     * @return  self
     */ 
    public function setCashflows($cashflows)
    {
        $this->cashflows = $cashflows;

        return $this;
    }
}