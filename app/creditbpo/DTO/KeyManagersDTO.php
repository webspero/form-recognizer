<?php

namespace CreditBPO\DTO;

class KeyManagersDTO {
    public $id;
    public $reportid;
    public $firstname;
    public $middlename;
    public $lastname;
    public $nationality;
    public $birthdate;
    public $civilstatus;
    public $profession;
    public $email;
    public $address1;
    public $tinnum;
    public $percentofownership;
    public $numberofyearengaged;
    public $position;

    /**
     * Get the value of relatedCompaniesId
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of relatedcompaniesid
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of entityId
     */ 
    public function getReportId()
    {
        return $this->reportid;
    }

    /**
     * Set the value of entityId
     *
     * @return  self
     */ 
    public function setReportId($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get the value of firstname
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */ 
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of middlename
     */ 
    public function getMiddleName()
    {
        return $this->middlename;
    }

    /**
     * Set the value of middlename
     *
     * @return  self
     */ 
    public function setMiddleName($middleName)
    {
        $this->middlename = $middleName;

        return $this;
    }

    /**
     * Get the value of lastname
     */ 
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastname = $lastName;

        return $this;
    }

    /**
     * Get the value of nationality
     */ 
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set the value of nationality
     *
     * @return  self
     */ 
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get the value of birthdate
     */ 
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set the value of birthdate
     *
     * @return  self
     */ 
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get the value of civilstatus
     */ 
    public function getCivilStatus()
    {
        return $this->civilstatus;
    }

    /**
     * Set the value of civilstatus
     *
     * @return  self
     */ 
    public function setCivilStatus($civilStatus)
    {
        $this->civilstatus = $civilStatus;

        return $this;
    }

    /**
     * Get the value of profession
     */ 
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set the value of profession
     *
     * @return  self
     */ 
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of address1
     */ 
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set the value of address1
     *
     * @return  self
     */ 
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get the value of tinnum
     */ 
    public function getTinnum()
    {
        return $this->tinnum;
    }

    /**
     * Set the value of tinnum
     *
     * @return  self
     */ 
    public function setTinnum($tinnum)
    {
        $this->tinnum = $tinnum;

        return $this;
    }

    /**
     * Get the value of percentofownership
     */ 
    public function getPercentOfOwnership()
    {
        return $this->percentofownership;
    }

    /**
     * Set the value of percentofownership
     *
     * @return  self
     */ 
    public function setPercentOfOwnership($percentOfOwnership)
    {
        $this->percentofownership = $percentOfOwnership;

        return $this;
    }

    /**
     * Get the value of numberofyearengaged
     */ 
    public function getNumberOfYearEngaged()
    {
        return $this->numberofyearengaged;
    }

    /**
     * Set the value of numberofyearengaged
     *
     * @return  self
     */ 
    public function setNumberOfYearEngaged($numberOfYearEngaged)
    {
        $this->numberofyearengaged = $numberOfYearEngaged;

        return $this;
    }

    /**
     * Get the value of position
     */ 
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of position
     *
     * @return  self
     */ 
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {

        if (isset($vars['id'])) {
            $this->setId($vars['id']);
        }
        if (isset($vars['reportid'])) {
            $this->setReportId($vars['reportid']);
        }
        if (isset($vars['firstname'])) {
            $this->setFirstname($vars['firstname']);
        }
        if (isset($vars['middlename'])) {
            $this->setMiddlename($vars['middlename']);
       	}
        if (isset($vars['lastname'])) {
            $this->setLastname($vars['lastname']);
        }
        if (isset($vars['nationality'])) {
            $this->setNationality($vars['nationality']);
        }
        if (isset($vars['birthdate'])) {
            $this->setBirthdate($vars['birthdate']);
        }
        if (isset($vars['civilstatus'])) {
            $this->setCivilStatus($vars['civilstatus']);
        }
        if (isset($vars['profession'])) {
            $this->setProfession($vars['profession']);
        }
        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }
        if (isset($vars['address1'])) {
            $this->setAddress1($vars['address1']);
        }
        if (isset($vars['tinnum'])) {
            $this->setTinNum($vars['tinnum']);
        }
        if (isset($vars['percentofownership'])) {
            $this->setPercentOfOwnership($vars['percentofownership']);
        }
        if (isset($vars['numberofyearengaged'])) {
            $this->setNumberOfYearEngaged($vars['numberofyearengaged']);
        }
        if (isset($vars['position'])) {
            $this->setPosition($vars['position']);
        }

	}

    /**
     * @param array $vars
     * @return affiliatesSubsidiaries
     */
    public function getVars()
    {
        return array(
            'id' => $this->getId(),
            'reportid' => $this->getReportId(),
            'firstname' => $this->getFirstname(),
            'middlename' => $this->getMiddlename(),
            'lastname' => $this->getLastname(),
            'nationality' => $this->getNationality(),
            'birthdate' => $this->getBirthdate(),
            'civilstatus' => $this->getCivilstatus(),
            'profession' => $this->getProfession(),
            'email' => $this->getEmail(),
            'address1' => $this->getAddress1(),
            'tinnum' => $this->getTinnum(),
            'percentofownership' => $this->getPercentOfOwnership(),
            'numberofyearengaged' => $this->getNumberOfYearEngaged(),
            'position' => $this->getPosition(),
        );
    }
    
}