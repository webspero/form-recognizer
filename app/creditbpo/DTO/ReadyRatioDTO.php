<?php

namespace CreditBPO\DTO;

class ReadyRatioDTO {
    private $accountsPayableTurnoverOne;
    private $accountsPayableTurnoverTwo;
    private $accountsPayableTurnoverThree;
    private $cashConversionCycleOne;
    private $cashConversionCycleTwo;
    private $cashConversionCycleThree;
    private $cashConversionYearOne;
    private $cashConversionYearTwo;
    private $cashConversionYearThree;
    private $currentDebtEquityRatio;
    private $currentGrossProfitMargin;
    private $currentGrossRevenueGrowth;
    private $currentNetCashMargin;
    private $currentNetIncomeGrowth;
    private $currentNetProfitMargin;
    private $currentRatio;
    private $currentYear;
    private $entityId;
    private $inventoryTurnoverOne;
    private $inventoryTurnoverTwo;
    private $inventoryTurnoverThree;
    private $loginId;
    private $previousDebtEquityRatio;
    private $previousGrossProfitMargin;
    private $previousGrossRevenueGrowth;
    private $previousNetCashMargin;
    private $previousNetIncomeGrowth;
    private $previousNetProfitMargin;
    private $previousRatio;
    private $previousYear;
    private $receivablesTurnoverOne;
    private $receivablesTurnoverTwo;
    private $receivablesTurnoverThree;

    /**
     * @param array $vars
     */
    public function __construct($vars = []) {
        $this->setVars($vars);
    }

    /**
     * @param array $vars
     * @return ReadyRatioDTO
     */
    public function setVars($vars = []) {
        if (isset($vars['accounts_payable_turnover_1'])) {
            $this->setAccountsPayableTurnoverOne($vars['accounts_payable_turnover_1']);
        }

        if (isset($vars['accounts_payable_turnover_2'])) {
            $this->setAccountsPayableTurnoverTwo($vars['accounts_payable_turnover_2']);
        }

        if (isset($vars['accounts_payable_turnover_3'])) {
            $this->setAccountsPayableTurnoverThree($vars['accounts_payable_turnover_3']);
        }

        if (isset($vars['cash_conversion_cycle_1'])) {
            $this->setCashConversionCycleOne($vars['cash_conversion_cycle_1']);
        }

        if (isset($vars['cash_conversion_cycle_2'])) {
            $this->setCashConversionCycleTwo($vars['cash_conversion_cycle_2']);
        }

        if (isset($vars['cash_conversion_cycle_3'])) {
            $this->setCashConversionCycleThree($vars['cash_conversion_cycle_3']);
        }

        if (isset($vars['cash_conversion_year_1'])) {
            $this->setCashConversionYearOne($vars['cash_conversion_year_1']);
        }

        if (isset($vars['cash_conversion_year_2'])) {
            $this->setCashConversionYearTwo($vars['cash_conversion_year_2']);
        }

        if (isset($vars['cash_conversion_year_3'])) {
            $this->setCashConversionYearThree($vars['cash_conversion_year_3']);
        }

        if (isset($vars['current_debt_equity_ratio'])) {
            $this->setCurrentDebtEquityRatio($vars['current_debt_equity_ratio']);
        }

        if (isset($vars['current_gross_profit_margin'])) {
            $this->setCurrentGrossProfitMargin($vars['current_gross_profit_margin']);
        }

        if (isset($vars['current_gross_revenue_growth'])) {
            $this->setCurrentGrossRevenueGrowth($vars['current_gross_revenue_growth']);
        }

        if (isset($vars['current_net_cash_margin'])) {
            $this->setCurrentNetCashMargin($vars['current_net_cash_margin']);
        }

        if (isset($vars['current_net_income_growth'])) {
            $this->setCurrentNetIncomeGrowth($vars['current_net_income_growth']);
        }

        if (isset($vars['current_net_profit_margin'])) {
            $this->setCurrentNetProfitMargin($vars['current_net_profit_margin']);
        }

        if (isset($vars['current_ratio'])) {
            $this->setCurrentRatio($vars['current_ratio']);
        }

        if (isset($vars['current_year'])) {
            $this->setCurrentYear($vars['current_year']);
        }

        if (isset($vars['entity_id'])) {
            $this->setEntityId($vars['entity_id']);
        }

        if (isset($vars['inventory_turnover_1'])) {
            $this->setInventoryTurnoverOne($vars['inventory_turnover_1']);
        }

        if (isset($vars['inventory_turnover_2'])) {
            $this->setInventoryTurnoverTwo($vars['inventory_turnover_2']);
        }

        if (isset($vars['inventory_turnover_3'])) {
            $this->setInventoryTurnoverThree($vars['inventory_turnover_3']);
        }

        if (isset($vars['login_id'])) {
            $this->setLoginId($vars['login_id']);
        }

        if (isset($vars['previous_debt_equity_ratio'])) {
            $this->setPreviousDebtEquityRatio($vars['previous_debt_equity_ratio']);
        }

        if (isset($vars['previous_gross_profit_margin'])) {
            $this->setPreviousGrossProfitMargin($vars['previous_gross_profit_margin']);
        }

        if (isset($vars['previous_gross_revenue_growth'])) {
            $this->setPreviousGrossRevenueGrowth($vars['previous_gross_revenue_growth']);
        }

        if (isset($vars['previous_net_cash_margin'])) {
            $this->setPreviousNetCashMargin($vars['previous_net_cash_margin']);
        }

        if (isset($vars['previous_net_income_growth'])) {
            $this->setPreviousNetIncomeGrowth($vars['previous_net_income_growth']);
        }

        if (isset($vars['previous_net_profit_margin'])) {
            $this->setPreviousNetProfitMargin($vars['previous_net_profit_margin']);
        }

        if (isset($vars['previous_ratio'])) {
            $this->setPreviousRatio($vars['previous_ratio']);
        }

        if (isset($vars['previous_year'])) {
            $this->setPreviousYear($vars['previous_year']);
        }

        if (isset($vars['receivables_turnover_1'])) {
            $this->setReceivablesTurnoverOne($vars['receivables_turnover_1']);
        }

        if (isset($vars['receivables_turnover_2'])) {
            $this->setReceivablesTurnoverTwo($vars['receivables_turnover_2']);
        }

        if (isset($vars['receivables_turnover_3'])) {
            $this->setReceivablesTurnoverThree($vars['receivables_turnover_3']);
        }
    }

    /**
     * @param
     */
    public function setAccountsPayableTurnoverOne($accounts) {
        $this->accountsPayableTurnoverOne = $accounts;
        return $this;
    }

    /**
     * @return
     */
    public function getAccountsPayableTurnoverOne() {
        return $this->accountsPayableTurnoverOne;
    }

    /**
     * @param
     */
    public function setAccountsPayableTurnoverTwo($accounts) {
        $this->accountsPayableTurnoverTwo = $accounts;
        return $this;
    }

    /**
     * @return
     */
    public function getAccountsPayableTurnoverTwo() {
        return $this->accountsPayableTurnoverTwo;
    }

    /**
     * @param
     */
    public function setAccountsPayableTurnoverThree($accounts) {
        $this->accountsPayableTurnoverThree = $accounts;
        return $this;
    }

    /**
     * @return
     */
    public function getAccountsPayableTurnoverThree() {
        return $this->accountsPayableTurnoverThree;
    }

    /**
     * @param
     */
    public function setCashConversionCycleOne($conversion) {
        $this->cashConversionCycleOne = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionCycleOne() {
        return $this->cashConversionCycleOne;
    }

    /**
     * @param
     */
    public function setCashConversionCycleTwo($conversion) {
        $this->cashConversionCycleTwo = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionCycleTwo() {
        return $this->cashConversionCycleTwo;
    }

    /**
     * @param
     */
    public function setCashConversionCycleThree($conversion) {
        $this->cashConversionCycleThree = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionCycleThree() {
        return $this->cashConversionCycleThree;
    }

    /**
     * @param
     */
    public function setCashConversionYearOne($conversion) {
        $this->cashConversionYearOne = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionYearOne() {
        return $this->cashConversionYearOne;
    }

    /**
     * @param
     */
    public function setCashConversionYearTwo($conversion) {
        $this->cashConversionYearTwo = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionYearTwo() {
        return $this->cashConversionYearTwo;
    }

    /**
     * @param
     */
    public function setCashConversionYearThree($conversion) {
        $this->cashConversionYearThree = $conversion;
        return $this;
    }

    /**
     * @return
     */
    public function getCashConversionYearThree() {
        return $this->cashConversionYearThree;
    }

    /**
     * @param
     */
    public function setCurrentDebtEquityRatio($debtRatio) {
        $this->currentDebtEquityRatio = $debtRatio;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentDebtEquityRatio() {
        return $this->currentDebtEquityRatio;
    }

    /**
     * @param
     */
    public function setCurrentGrossProfitMargin($gross) {
        $this->currentGrossProfitMargin = $gross;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentGrossProfitMargin() {
        return $this->currentGrossProfitMargin;
    }

    /**
     * @param
     */
    public function setCurrentGrossRevenueGrowth($revenue) {
        $this->currentGrossRevenueGrowth = $revenue;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentGrossRevenueGrowth() {
        return $this->currentGrossRevenueGrowth;
    }

    /**
     * @param
     */
    public function setCurrentNetCashMargin($netCash) {
        $this->currentNetCashMargin = $netCash;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentNetCashMargin() {
        return $this->currentNetCashMargin;
    }

    /**
     * @param
     */
    public function setCurrentNetIncomeGrowth($growth) {
        $this->currentNetIncomeGrowth = $growth;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentNetIncomeGrowth() {
        return $this->currentNetIncomeGrowth;
    }

    /**
     * @param
     */
    public function setCurrentNetProfitMargin($profit) {
        $this->currentNetProfitMargin = $profit;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentNetProfitMargin() {
        return $this->currentNetProfitMargin;
    }

    /**
     * @param
     */
    public function setCurrentRatio($ratio) {
        $this->currentRatio = $ratio;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentRatio() {
        return $this->currentRatio;
    }

    /**
     * @param
     */
    public function setCurrentYear($year) {
        $this->currentYear = $year;
        return $this;
    }

    /**
     * @return
     */
    public function getCurrentYear() {
        return $this->currentYear;
    }

    /**
     * @param
     */
    public function setEntityId($entityId) {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * @return
     */
    public function getEntityId() {
        return $this->entityId;
    }

    /**
     * @param
     */
    public function setInventoryTurnoverOne($inventory) {
        $this->inventoryTurnoverOne = $inventory;
        return $this;
    }

    /**
     * @return
     */
    public function getInventoryTurnoverOne() {
        return $this->inventoryTurnoverOne;
    }

    /**
     * @param
     */
    public function setInventoryTurnoverTwo($inventory) {
        $this->inventoryTurnoverTwo = $inventory;
        return $this;
    }

    /**
     * @return
     */
    public function getInventoryTurnoverTwo() {
        return $this->inventoryTurnoverTwo;
    }

    /**
     * @param
     */
    public function setInventoryTurnoverThree($inventory) {
        $this->inventoryTurnoverThree = $inventory;
        return $this;
    }

    /**
     * @return
     */
    public function getInventoryTurnoverThree() {
        return $this->inventoryTurnoverThree;
    }

    /**
     * @param
     */
    public function setLoginId($loginId) {
        $this->loginId = $loginId;
        return $this;
    }

    /**
     * @return
     */
    public function getLoginId() {
        return $this->loginId;
    }

    /**
     * @param
     */
    public function setPreviousDebtEquityRatio($ratio) {
        $this->previousDebtEquityRatio = $ratio;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousDebtEquityRatio() {
        return $this->previousDebtEquityRatio;
    }

    /**
     * @param
     */
    public function setPreviousGrossProfitMargin($margin) {
        $this->previousGrossProfitMargin = $margin;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousGrossProfitMargin() {
        return $this->previousGrossProfitMargin;
    }

    /**
     * @param
     */
    public function setPreviousGrossRevenueGrowth($growth) {
        $this->previousGrossRevenueGrowth = $growth;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousGrossRevenueGrowth() {
        return $this->previousGrossRevenueGrowth;
    }

    /**
     * @param
     */
    public function setPreviousNetCashMargin($margin) {
        $this->previousNetCashMargin = $margin;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousNetCashMargin() {
        return $this->previousNetCashMargin;
    }

    /**
     * @param
     */
    public function setPreviousNetIncomeGrowth($income) {
        $this->previousNetIncomeGrowth = $income;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousNetIncomeGrowth() {
        return $this->previousNetIncomeGrowth;
    }

    /**
     * @param
     */
    public function setPreviousNetProfitMargin($margin) {
        $this->previousNetProfitMargin = $margin;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousNetProfitMargin() {
        return $this->previousNetProfitMargin;
    }

    /**
     * @param
     */
    public function setPreviousRatio($ratio) {
        $this->previousRatio = $ratio;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousRatio() {
        return $this->previousRatio;
    }

    /**
     * @param
     */
    public function setPreviousYear($year) {
        $this->year = $year;
        return $this;
    }

    /**
     * @return
     */
    public function getPreviousYear() {
        return $this->year;
    }

    /**
     * @param
     */
    public function setReceivablesTurnoverOne($receivables) {
        $this->receivablesTurnoverOne = $receivables;
        return $this;
    }

    /**
     * @return
     */
    public function getReceivablesTurnoverOne() {
        return $this->receivablesTurnoverOne;
    }

    /**
     * @param
     */
    public function setReceivablesTurnoverTwo($receivables) {
        $this->receivablesTurnoverTwo = $receivables;
        return $this;
    }

    /**
     * @return
     */
    public function getReceivablesTurnoverTwo() {
        return $this->receivablesTurnoverTwo;
    }

    /**
     * @param
     */
    public function setReceivablesTurnoverThree($receivables) {
        $this->receivablesTurnoverThree = $receivables;
        return $this;
    }

    /**
     * @return
     */
    public function getReceivablesTurnoverThree() {
        return $this->receivablesTurnoverThree;
    }
}
