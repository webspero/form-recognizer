<?php

namespace CreditBPO\DTO;

class BalanceSheetDTO 
{
    protected $financialReportId;
    protected $indexCount;
    protected $propertyPlantAndEquipment;
    protected $investmentProperty;
    protected $goodwill;
    protected $intangibleAssetsOtherThanGoodwill;
    protected $investmentAccountedForUsingEquityMethod;
    protected $investmentsInSubsidiariesJointVenturesAndAssociates;
    protected $noncurrentBiologicalAssets;
    protected $noncurrentReceivables;
    protected $noncurrentInventories;
    protected $deferredTaxAssets;
    protected $currentTaxAssetsNoncurrent;
    protected $otherNoncurrentFinancialAssets;
    protected $otherNoncurrentNonfinancialAssets;
    protected $noncurrentNoncashAssetsPledgedAsCollateral;
    protected $noncurrentAssets;
    protected $inventories;
    protected $tradeAndOtherCurrentReceivables;
    protected $currentTaxAssetsCurrent;
    protected $currentBiologicalAssets;
    protected $otherCurrentFinancialAssets;
    protected $otherCurrentNonfinancialAssets;
    protected $cashAndCashEquivalents;
    protected $currentNoncashAssetsPledgedAsCollateral;
    protected $noncurrentAssetsOrDisposalGroups;
    protected $currentAssets;
    protected $assets;
    protected $issuedCapital;
    protected $sharePremium;
    protected $treasuryShares;
    protected $otherEquityInterest;
    protected $otherReserves;
    protected $retainedEarnings;
    protected $noncontrollingInterests;
    protected $equity;
    protected $noncurrentProvisionsForEmployeeBenefits;
    protected $otherLongtermProvisions;
    protected $noncurrentPayables;
    protected $deferredTaxLiabilities;
    protected $currentTaxLiabilitiesNoncurrent;
    protected $otherNoncurrentFinancialLiabilities;
    protected $otherNoncurrentNonfinancialLiabilities;
    protected $noncurrentLiabilities;
    protected $currentProvisionsForEmployeeBenefits;
    protected $otherShorttermProvisions;
    protected $tradeAndOtherCurrentPayables;
    protected $currentTaxLiabilitiesCurrent;
    protected $otherCurrentFinancialLiabilities;
    protected $otherCurrentNonfinancialLiabilities;
    protected $liabilitiesIncludedInDisposalGroups;
    protected $currentLiabilities;
    protected $liabilities;
    protected $equityAndLiabilities;

    /**
     * Get the value of financialReportId
     */ 
    public function getFinancialReportId()
    {
        return $this->financialReportId;
    }

    /**
     * Set the value of financialReportId
     *
     * @return  self
     */ 
    public function setFinancialReportId($financialReportId)
    {
        $this->financialReportId = $financialReportId;

        return $this;
    }

    /**
     * Get the value of indexCount
     */ 
    public function getIndexCount()
    {
        return $this->indexCount;
    }

    /**
     * Set the value of indexCount
     *
     * @return  self
     */ 
    public function setIndexCount($indexCount)
    {
        $this->indexCount = $indexCount;

        return $this;
    }

    /**
     * Get the value of propertyPlantAndEquipment
     */ 
    public function getPropertyPlantAndEquipment()
    {
        return $this->propertyPlantAndEquipment;
    }

    /**
     * Set the value of propertyPlantAndEquipment
     *
     * @return  self
     */ 
    public function setPropertyPlantAndEquipment($propertyPlantAndEquipment)
    {
        $this->propertyPlantAndEquipment = $propertyPlantAndEquipment;

        return $this;
    }

    /**
     * Get the value of investmentProperty
     */ 
    public function getInvestmentProperty()
    {
        return $this->investmentProperty;
    }

    /**
     * Set the value of investmentProperty
     *
     * @return  self
     */ 
    public function setInvestmentProperty($investmentProperty)
    {
        $this->investmentProperty = $investmentProperty;

        return $this;
    }

    /**
     * Get the value of goodwill
     */ 
    public function getGoodwill()
    {
        return $this->goodwill;
    }

    /**
     * Set the value of goodwill
     *
     * @return  self
     */ 
    public function setGoodwill($goodwill)
    {
        $this->goodwill = $goodwill;

        return $this;
    }

    /**
     * Get the value of intangibleAssetsOtherThanGoodwill
     */ 
    public function getIntangibleAssetsOtherThanGoodwill()
    {
        return $this->intangibleAssetsOtherThanGoodwill;
    }

    /**
     * Set the value of intangibleAssetsOtherThanGoodwill
     *
     * @return  self
     */ 
    public function setIntangibleAssetsOtherThanGoodwill($intangibleAssetsOtherThanGoodwill)
    {
        $this->intangibleAssetsOtherThanGoodwill = $intangibleAssetsOtherThanGoodwill;

        return $this;
    }

    /**
     * Get the value of investmentAccountedForUsingEquityMethod
     */ 
    public function getInvestmentAccountedForUsingEquityMethod()
    {
        return $this->investmentAccountedForUsingEquityMethod;
    }

    /**
     * Set the value of investmentAccountedForUsingEquityMethod
     *
     * @return  self
     */ 
    public function setInvestmentAccountedForUsingEquityMethod($investmentAccountedForUsingEquityMethod)
    {
        $this->investmentAccountedForUsingEquityMethod = $investmentAccountedForUsingEquityMethod;

        return $this;
    }

    /**
     * Get the value of investmentsInSubsidiariesJointVenturesAndAssociates
     */ 
    public function getInvestmentsInSubsidiariesJointVenturesAndAssociates()
    {
        return $this->investmentsInSubsidiariesJointVenturesAndAssociates;
    }

    /**
     * Set the value of investmentsInSubsidiariesJointVenturesAndAssociates
     *
     * @return  self
     */ 
    public function setInvestmentsInSubsidiariesJointVenturesAndAssociates($investmentsInSubsidiariesJointVenturesAndAssociates)
    {
        $this->investmentsInSubsidiariesJointVenturesAndAssociates = $investmentsInSubsidiariesJointVenturesAndAssociates;

        return $this;
    }

    /**
     * Get the value of noncurrentBiologicalAssets
     */ 
    public function getNoncurrentBiologicalAssets()
    {
        return $this->noncurrentBiologicalAssets;
    }

    /**
     * Set the value of noncurrentBiologicalAssets
     *
     * @return  self
     */ 
    public function setNoncurrentBiologicalAssets($noncurrentBiologicalAssets)
    {
        $this->noncurrentBiologicalAssets = $noncurrentBiologicalAssets;

        return $this;
    }

    /**
     * Get the value of noncurrentReceivables
     */ 
    public function getNoncurrentReceivables()
    {
        return $this->noncurrentReceivables;
    }

    /**
     * Set the value of noncurrentReceivables
     *
     * @return  self
     */ 
    public function setNoncurrentReceivables($noncurrentReceivables)
    {
        $this->noncurrentReceivables = $noncurrentReceivables;

        return $this;
    }

    /**
     * Get the value of noncurrentInventories
     */ 
    public function getNoncurrentInventories()
    {
        return $this->noncurrentInventories;
    }

    /**
     * Set the value of noncurrentInventories
     *
     * @return  self
     */ 
    public function setNoncurrentInventories($noncurrentInventories)
    {
        $this->noncurrentInventories = $noncurrentInventories;

        return $this;
    }

    /**
     * Get the value of deferredTaxAssets
     */ 
    public function getDeferredTaxAssets()
    {
        return $this->deferredTaxAssets;
    }

    /**
     * Set the value of deferredTaxAssets
     *
     * @return  self
     */ 
    public function setDeferredTaxAssets($deferredTaxAssets)
    {
        $this->deferredTaxAssets = $deferredTaxAssets;

        return $this;
    }

    /**
     * Get the value of currentTaxAssetsNoncurrent
     */ 
    public function getCurrentTaxAssetsNoncurrent()
    {
        return $this->currentTaxAssetsNoncurrent;
    }

    /**
     * Set the value of currentTaxAssetsNoncurrent
     *
     * @return  self
     */ 
    public function setCurrentTaxAssetsNoncurrent($currentTaxAssetsNoncurrent)
    {
        $this->currentTaxAssetsNoncurrent = $currentTaxAssetsNoncurrent;

        return $this;
    }

    /**
     * Get the value of otherNoncurrentFinancialAssets
     */ 
    public function getOtherNoncurrentFinancialAssets()
    {
        return $this->otherNoncurrentFinancialAssets;
    }

    /**
     * Set the value of otherNoncurrentFinancialAssets
     *
     * @return  self
     */ 
    public function setOtherNoncurrentFinancialAssets($otherNoncurrentFinancialAssets)
    {
        $this->otherNoncurrentFinancialAssets = $otherNoncurrentFinancialAssets;

        return $this;
    }

    /**
     * Get the value of otherNoncurrentNonfinancialAssets
     */ 
    public function getOtherNoncurrentNonfinancialAssets()
    {
        return $this->otherNoncurrentNonfinancialAssets;
    }

    /**
     * Set the value of otherNoncurrentNonfinancialAssets
     *
     * @return  self
     */ 
    public function setOtherNoncurrentNonfinancialAssets($otherNoncurrentNonfinancialAssets)
    {
        $this->otherNoncurrentNonfinancialAssets = $otherNoncurrentNonfinancialAssets;

        return $this;
    }

    /**
     * Get the value of noncurrentNoncashAssetsPledgedAsCollateral
     */ 
    public function getNoncurrentNoncashAssetsPledgedAsCollateral()
    {
        return $this->noncurrentNoncashAssetsPledgedAsCollateral;
    }

    /**
     * Set the value of noncurrentNoncashAssetsPledgedAsCollateral
     *
     * @return  self
     */ 
    public function setNoncurrentNoncashAssetsPledgedAsCollateral($noncurrentNoncashAssetsPledgedAsCollateral)
    {
        $this->noncurrentNoncashAssetsPledgedAsCollateral = $noncurrentNoncashAssetsPledgedAsCollateral;

        return $this;
    }

    /**
     * Get the value of noncurrentAssets
     */ 
    public function getNoncurrentAssets()
    {
        return $this->noncurrentAssets;
    }

    /**
     * Set the value of noncurrentAssets
     *
     * @return  self
     */ 
    public function setNoncurrentAssets($noncurrentAssets)
    {
        $this->noncurrentAssets = $noncurrentAssets;

        return $this;
    }

    /**
     * Get the value of inventories
     */ 
    public function getInventories()
    {
        return $this->inventories;
    }

    /**
     * Set the value of inventories
     *
     * @return  self
     */ 
    public function setInventories($inventories)
    {
        $this->inventories = $inventories;

        return $this;
    }

    /**
     * Get the value of tradeAndOtherCurrentReceivables
     */ 
    public function getTradeAndOtherCurrentReceivables()
    {
        return $this->tradeAndOtherCurrentReceivables;
    }

    /**
     * Set the value of tradeAndOtherCurrentReceivables
     *
     * @return  self
     */ 
    public function setTradeAndOtherCurrentReceivables($tradeAndOtherCurrentReceivables)
    {
        $this->tradeAndOtherCurrentReceivables = $tradeAndOtherCurrentReceivables;

        return $this;
    }

    /**
     * Get the value of currentTaxAssetsCurrent
     */ 
    public function getCurrentTaxAssetsCurrent()
    {
        return $this->currentTaxAssetsCurrent;
    }

    /**
     * Set the value of currentTaxAssetsCurrent
     *
     * @return  self
     */ 
    public function setCurrentTaxAssetsCurrent($currentTaxAssetsCurrent)
    {
        $this->currentTaxAssetsCurrent = $currentTaxAssetsCurrent;

        return $this;
    }

    /**
     * Get the value of currentBiologicalAssets
     */ 
    public function getCurrentBiologicalAssets()
    {
        return $this->currentBiologicalAssets;
    }

    /**
     * Set the value of currentBiologicalAssets
     *
     * @return  self
     */ 
    public function setCurrentBiologicalAssets($currentBiologicalAssets)
    {
        $this->currentBiologicalAssets = $currentBiologicalAssets;

        return $this;
    }

    /**
     * Get the value of otherCurrentFinancialAssets
     */ 
    public function getOtherCurrentFinancialAssets()
    {
        return $this->otherCurrentFinancialAssets;
    }

    /**
     * Set the value of otherCurrentFinancialAssets
     *
     * @return  self
     */ 
    public function setOtherCurrentFinancialAssets($otherCurrentFinancialAssets)
    {
        $this->otherCurrentFinancialAssets = $otherCurrentFinancialAssets;

        return $this;
    }

    /**
     * Get the value of otherCurrentNonfinancialAssets
     */ 
    public function getOtherCurrentNonfinancialAssets()
    {
        return $this->otherCurrentNonfinancialAssets;
    }

    /**
     * Set the value of otherCurrentNonfinancialAssets
     *
     * @return  self
     */ 
    public function setOtherCurrentNonfinancialAssets($otherCurrentNonfinancialAssets)
    {
        $this->otherCurrentNonfinancialAssets = $otherCurrentNonfinancialAssets;

        return $this;
    }

    /**
     * Get the value of cashAndCashEquivalents
     */ 
    public function getCashAndCashEquivalents()
    {
        return $this->cashAndCashEquivalents;
    }

    /**
     * Set the value of cashAndCashEquivalents
     *
     * @return  self
     */ 
    public function setCashAndCashEquivalents($cashAndCashEquivalents)
    {
        $this->cashAndCashEquivalents = $cashAndCashEquivalents;

        return $this;
    }
    
        /**
         * Get the value of currentNoncashAssetsPledgedAsCollateral
         */ 
        public function getCurrentNoncashAssetsPledgedAsCollateral()
        {
            return $this->currentNoncashAssetsPledgedAsCollateral;
        }
    
        /**
         * Set the value of currentNoncashAssetsPledgedAsCollateral
         *
         * @return  self
         */ 
        public function setCurrentNoncashAssetsPledgedAsCollateral($currentNoncashAssetsPledgedAsCollateral)
        {
            $this->currentNoncashAssetsPledgedAsCollateral = $currentNoncashAssetsPledgedAsCollateral;
    
            return $this;
        }

    /**
     * Get the value of noncurrentAssetsOrDisposalGroups
     */ 
    public function getNoncurrentAssetsOrDisposalGroups()
    {
        return $this->noncurrentAssetsOrDisposalGroups;
    }

    /**
     * Set the value of noncurrentAssetsOrDisposalGroups
     *
     * @return  self
     */ 
    public function setNoncurrentAssetsOrDisposalGroups($noncurrentAssetsOrDisposalGroups)
    {
        $this->noncurrentAssetsOrDisposalGroups = $noncurrentAssetsOrDisposalGroups;

        return $this;
    }

    /**
     * Get the value of currentAssets
     */ 
    public function getCurrentAssets()
    {
        return $this->currentAssets;
    }

    /**
     * Set the value of currentAssets
     *
     * @return  self
     */ 
    public function setCurrentAssets($currentAssets)
    {
        $this->currentAssets = $currentAssets;

        return $this;
    }

    /**
     * Get the value of assets
     */ 
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * Set the value of assets
     *
     * @return  self
     */ 
    public function setAssets($assets)
    {
        $this->assets = $assets;

        return $this;
    }

    /**
     * Get the value of issuedCapital
     */ 
    public function getIssuedCapital()
    {
        return $this->issuedCapital;
    }

    /**
     * Set the value of issuedCapital
     *
     * @return  self
     */ 
    public function setIssuedCapital($issuedCapital)
    {
        $this->issuedCapital = $issuedCapital;

        return $this;
    }

    /**
     * Get the value of sharePremium
     */ 
    public function getSharePremium()
    {
        return $this->sharePremium;
    }

    /**
     * Set the value of sharePremium
     *
     * @return  self
     */ 
    public function setSharePremium($sharePremium)
    {
        $this->sharePremium = $sharePremium;

        return $this;
    }

    /**
     * Get the value of treasuryShares
     */ 
    public function getTreasuryShares()
    {
        return $this->treasuryShares;
    }

    /**
     * Set the value of treasuryShares
     *
     * @return  self
     */ 
    public function setTreasuryShares($treasuryShares)
    {
        $this->treasuryShares = $treasuryShares;

        return $this;
    }

    /**
     * Get the value of otherEquityInterest
     */ 
    public function getOtherEquityInterest()
    {
        return $this->otherEquityInterest;
    }

    /**
     * Set the value of otherEquityInterest
     *
     * @return  self
     */ 
    public function setOtherEquityInterest($otherEquityInterest)
    {
        $this->otherEquityInterest = $otherEquityInterest;

        return $this;
    }

    /**
     * Get the value of otherReserves
     */ 
    public function getOtherReserves()
    {
        return $this->otherReserves;
    }

    /**
     * Set the value of otherReserves
     *
     * @return  self
     */ 
    public function setOtherReserves($otherReserves)
    {
        $this->otherReserves = $otherReserves;

        return $this;
    }

    /**
     * Get the value of retainedEarnings
     */ 
    public function getRetainedEarnings()
    {
        return $this->retainedEarnings;
    }

    /**
     * Set the value of retainedEarnings
     *
     * @return  self
     */ 
    public function setRetainedEarnings($retainedEarnings)
    {
        $this->retainedEarnings = $retainedEarnings;

        return $this;
    }

    /**
     * Get the value of noncontrollingInterests
     */ 
    public function getNoncontrollingInterests()
    {
        return $this->noncontrollingInterests;
    }

    /**
     * Set the value of noncontrollingInterests
     *
     * @return  self
     */ 
    public function setNoncontrollingInterests($noncontrollingInterests)
    {
        $this->noncontrollingInterests = $noncontrollingInterests;

        return $this;
    }

    /**
     * Get the value of equity
     */ 
    public function getEquity()
    {
        return $this->equity;
    }

    /**
     * Set the value of equity
     *
     * @return  self
     */ 
    public function setEquity($equity)
    {
        $this->equity = $equity;

        return $this;
    }

    /**
     * Get the value of noncurrentProvisionsForEmployeeBenefits
     */ 
    public function getNoncurrentProvisionsForEmployeeBenefits()
    {
        return $this->noncurrentProvisionsForEmployeeBenefits;
    }

    /**
     * Set the value of noncurrentProvisionsForEmployeeBenefits
     *
     * @return  self
     */ 
    public function setNoncurrentProvisionsForEmployeeBenefits($noncurrentProvisionsForEmployeeBenefits)
    {
        $this->noncurrentProvisionsForEmployeeBenefits = $noncurrentProvisionsForEmployeeBenefits;

        return $this;
    }

    /**
     * Get the value of otherLongtermProvisions
     */ 
    public function getOtherLongtermProvisions()
    {
        return $this->otherLongtermProvisions;
    }

    /**
     * Set the value of otherLongtermProvisions
     *
     * @return  self
     */ 
    public function setOtherLongtermProvisions($otherLongtermProvisions)
    {
        $this->otherLongtermProvisions = $otherLongtermProvisions;

        return $this;
    }

    /**
     * Get the value of noncurrentPayables
     */ 
    public function getNoncurrentPayables()
    {
        return $this->noncurrentPayables;
    }

    /**
     * Set the value of noncurrentPayables
     *
     * @return  self
     */ 
    public function setNoncurrentPayables($noncurrentPayables)
    {
        $this->noncurrentPayables = $noncurrentPayables;

        return $this;
    }

    /**
     * Get the value of deferredTaxLiabilities
     */ 
    public function getDeferredTaxLiabilities()
    {
        return $this->deferredTaxLiabilities;
    }

    /**
     * Set the value of deferredTaxLiabilities
     *
     * @return  self
     */ 
    public function setDeferredTaxLiabilities($deferredTaxLiabilities)
    {
        $this->deferredTaxLiabilities = $deferredTaxLiabilities;

        return $this;
    }

    /**
     * Get the value of currentTaxLiabilitiesNoncurrent
     */ 
    public function getCurrentTaxLiabilitiesNoncurrent()
    {
        return $this->currentTaxLiabilitiesNoncurrent;
    }

    /**
     * Set the value of currentTaxLiabilitiesNoncurrent
     *
     * @return  self
     */ 
    public function setCurrentTaxLiabilitiesNoncurrent($currentTaxLiabilitiesNoncurrent)
    {
        $this->currentTaxLiabilitiesNoncurrent = $currentTaxLiabilitiesNoncurrent;

        return $this;
    }

    /**
     * Get the value of otherNoncurrentFinancialLiabilities
     */ 
    public function getOtherNoncurrentFinancialLiabilities()
    {
        return $this->otherNoncurrentFinancialLiabilities;
    }

    /**
     * Set the value of otherNoncurrentFinancialLiabilities
     *
     * @return  self
     */ 
    public function setOtherNoncurrentFinancialLiabilities($otherNoncurrentFinancialLiabilities)
    {
        $this->otherNoncurrentFinancialLiabilities = $otherNoncurrentFinancialLiabilities;

        return $this;
    }

    /**
     * Get the value of otherNoncurrentNonfinancialLiabilities
     */ 
    public function getOtherNoncurrentNonfinancialLiabilities()
    {
        return $this->otherNoncurrentNonfinancialLiabilities;
    }

    /**
     * Set the value of otherNoncurrentNonfinancialLiabilities
     *
     * @return  self
     */ 
    public function setOtherNoncurrentNonfinancialLiabilities($otherNoncurrentNonfinancialLiabilities)
    {
        $this->otherNoncurrentNonfinancialLiabilities = $otherNoncurrentNonfinancialLiabilities;

        return $this;
    }

    /**
     * Get the value of noncurrentLiabilities
     */ 
    public function getNoncurrentLiabilities()
    {
        return $this->noncurrentLiabilities;
    }

    /**
     * Set the value of noncurrentLiabilities
     *
     * @return  self
     */ 
    public function setNoncurrentLiabilities($noncurrentLiabilities)
    {
        $this->noncurrentLiabilities = $noncurrentLiabilities;

        return $this;
    }

    /**
     * Get the value of currentProvisionsForEmployeeBenefits
     */ 
    public function getCurrentProvisionsForEmployeeBenefits()
    {
        return $this->currentProvisionsForEmployeeBenefits;
    }

    /**
     * Set the value of currentProvisionsForEmployeeBenefits
     *
     * @return  self
     */ 
    public function setCurrentProvisionsForEmployeeBenefits($currentProvisionsForEmployeeBenefits)
    {
        $this->currentProvisionsForEmployeeBenefits = $currentProvisionsForEmployeeBenefits;

        return $this;
    }

    /**
     * Get the value of otherShorttermProvisions
     */ 
    public function getOtherShorttermProvisions()
    {
        return $this->otherShorttermProvisions;
    }

    /**
     * Set the value of otherShorttermProvisions
     *
     * @return  self
     */ 
    public function setOtherShorttermProvisions($otherShorttermProvisions)
    {
        $this->otherShorttermProvisions = $otherShorttermProvisions;

        return $this;
    }

    /**
     * Get the value of tradeAndOtherCurrentPayables
     */ 
    public function getTradeAndOtherCurrentPayables()
    {
        return $this->tradeAndOtherCurrentPayables;
    }

    /**
     * Set the value of tradeAndOtherCurrentPayables
     *
     * @return  self
     */ 
    public function setTradeAndOtherCurrentPayables($tradeAndOtherCurrentPayables)
    {
        $this->tradeAndOtherCurrentPayables = $tradeAndOtherCurrentPayables;

        return $this;
    }

    /**
     * Get the value of currentTaxLiabilitiesCurrent
     */ 
    public function getCurrentTaxLiabilitiesCurrent()
    {
        return $this->currentTaxLiabilitiesCurrent;
    }

    /**
     * Set the value of currentTaxLiabilitiesCurrent
     *
     * @return  self
     */ 
    public function setCurrentTaxLiabilitiesCurrent($currentTaxLiabilitiesCurrent)
    {
        $this->currentTaxLiabilitiesCurrent = $currentTaxLiabilitiesCurrent;

        return $this;
    }

    /**
     * Get the value of otherCurrentFinancialLiabilities
     */ 
    public function getOtherCurrentFinancialLiabilities()
    {
        return $this->otherCurrentFinancialLiabilities;
    }

    /**
     * Set the value of otherCurrentFinancialLiabilities
     *
     * @return  self
     */ 
    public function setOtherCurrentFinancialLiabilities($otherCurrentFinancialLiabilities)
    {
        $this->otherCurrentFinancialLiabilities = $otherCurrentFinancialLiabilities;

        return $this;
    }

    /**
     * Get the value of otherCurrentNonfinancialLiabilities
     */ 
    public function getOtherCurrentNonfinancialLiabilities()
    {
        return $this->otherCurrentNonfinancialLiabilities;
    }

    /**
     * Set the value of otherCurrentNonfinancialLiabilities
     *
     * @return  self
     */ 
    public function setOtherCurrentNonfinancialLiabilities($otherCurrentNonfinancialLiabilities)
    {
        $this->otherCurrentNonfinancialLiabilities = $otherCurrentNonfinancialLiabilities;

        return $this;
    }

    /**
     * Get the value of liabilitiesIncludedInDisposalGroups
     */ 
    public function getLiabilitiesIncludedInDisposalGroups()
    {
        return $this->liabilitiesIncludedInDisposalGroups;
    }

    /**
     * Set the value of liabilitiesIncludedInDisposalGroups
     *
     * @return  self
     */ 
    public function setLiabilitiesIncludedInDisposalGroups($liabilitiesIncludedInDisposalGroups)
    {
        $this->liabilitiesIncludedInDisposalGroups = $liabilitiesIncludedInDisposalGroups;

        return $this;
    }

    /**
     * Get the value of currentLiabilities
     */ 
    public function getCurrentLiabilities()
    {
        return $this->currentLiabilities;
    }

    /**
     * Set the value of currentLiabilities
     *
     * @return  self
     */ 
    public function setCurrentLiabilities($currentLiabilities)
    {
        $this->currentLiabilities = $currentLiabilities;

        return $this;
    }

    /**
     * Get the value of liabilities
     */ 
    public function getLiabilities()
    {
        return $this->liabilities;
    }

    /**
     * Set the value of liabilities
     *
     * @return  self
     */ 
    public function setLiabilities($liabilities)
    {
        $this->liabilities = $liabilities;

        return $this;
    }

    /**
     * Get the value of equityAndLiabilities
     */ 
    public function getEquityAndLiabilities()
    {
        return $this->equityAndLiabilities;
    }

    /**
     * Set the value of equityAndLiabilities
     *
     * @return  self
     */ 
    public function setEquityAndLiabilities($equityAndLiabilities)
    {
        $this->equityAndLiabilities = $equityAndLiabilities;

        return $this;
    }

    /**
     * @param array $vars
     * @return BalanceSheetDTO
     */
    public function setDbVars($vars) 
    {
        if (isset($vars->financial_report_id)) {
            $this->setFinancialReportId($vars->financial_report_id);
        }
        if (isset($vars->index_count)) {
            $this->setIndexCount($vars->index_count);
        }
        if (isset($vars->PropertyPlantAndEquipment)) {
            $this->setPropertyPlantAndEquipment($vars->PropertyPlantAndEquipment);
        }
        if (isset($vars->InvestmentProperty)) {
            $this->setInvestmentProperty($vars->InvestmentProperty);
        }
        if (isset($vars->Goodwill)) {
            $this->setGoodwill($vars->Goodwill);
        }
        if (isset($vars->IntangibleAssetsOtherThanGoodwill)) {
            $this->setIntangibleAssetsOtherThanGoodwill($vars->IntangibleAssetsOtherThanGoodwill);
        }
        if (isset($vars->InvestmentAccountedForUsingEquityMethod)) {
            $this->setInvestmentAccountedForUsingEquityMethod($vars->InvestmentAccountedForUsingEquityMethod);
        }
        if (isset($vars->InvestmentsInSubsidiariesJointVenturesAndAssociates)) {
            $this->setInvestmentsInSubsidiariesJointVenturesAndAssociates($vars->InvestmentsInSubsidiariesJointVenturesAndAssociates);
        }
        if (isset($vars->NoncurrentBiologicalAssets)) {
            $this->setNoncurrentBiologicalAssets($vars->NoncurrentBiologicalAssets);
        }
        if (isset($vars->NoncurrentReceivables)) {
            $this->setNoncurrentReceivables($vars->NoncurrentReceivables);
        }
        if (isset($vars->NoncurrentInventories)) {
            $this->setNoncurrentInventories($vars->NoncurrentInventories);
        }
        if (isset($vars->DeferredTaxAssets)) {
            $this->setDeferredTaxAssets($vars->DeferredTaxAssets);
        }
        if (isset($vars->CurrentTaxAssetsNoncurrent)) {
            $this->setCurrentTaxAssetsNonCurrent($vars->CurrentTaxAssetsNoncurrent);
        }
        if (isset($vars->OtherNoncurrentFinancialAssets)) {
            $this->setOtherNoncurrentFinancialAssets($vars->OtherNoncurrentFinancialAssets);
        }
        if (isset($vars->OtherNoncurrentNonfinancialAssets)) {
            $this->setOtherNoncurrentNonfinancialAssets($vars->OtherNoncurrentNonfinancialAssets);
        }
        if (isset($vars->NoncurrentNoncashAssetsPledgedAsCollateral)) {
            $this->setNoncurrentNoncashAssetsPledgedAsCollateral($vars->NoncurrentNoncashAssetsPledgedAsCollateral);
        }
        if (isset($vars->NoncurrentAssets)) {
            $this->setNoncurrentAssets($vars->NoncurrentAssets);
        }
        if (isset($vars->Inventories)) {
            $this->setInventories($vars->Inventories);
        }
        if (isset($vars->TradeAndOtherCurrentReceivables)) {
            $this->setTradeAndOtherCurrentReceivables($vars->TradeAndOtherCurrentReceivables);
        }
        if (isset($vars->CurrentTaxAssetsCurrent)) {
            $this->setCurrentTaxAssetsCurrent($vars->CurrentTaxAssetsCurrent);
        }
        if (isset($vars->CurrentBiologicalAssets)) {
            $this->setCurrentBiologicalAssets($vars->CurrentBiologicalAssets);
        }
        if (isset($vars->OtherCurrentFinancialAssets)) {
            $this->setOtherCurrentFinancialAssets($vars->OtherCurrentFinancialAssets);
        }
        if (isset($vars->OtherCurrentNonfinancialAssets)) {
            $this->setOtherCurrentNonfinancialAssets($vars->OtherCurrentNonfinancialAssets);
        }
        if (isset($vars->CashAndCashEquivalents)) {
            $this->setCashAndCashEquivalents($vars->CashAndCashEquivalents);
        }
        if (isset($vars->CurrentNoncashAssetsPledgedAsCollateral)) {
            $this->setCurrentNoncashAssetsPledgedAsCollateral($vars->CurrentNoncashAssetsPledgedAsCollateral);
        }
        if (isset($vars->NoncurrentAssetsOrDisposalGroups)) {
            $this->setNoncurrentAssetsOrDisposalGroups($vars->NoncurrentAssetsOrDisposalGroups);
        }
        if (isset($vars->CurrentAssets)) {
            $this->setCurrentAssets($vars->CurrentAssets);
        }
        if (isset($vars->Assets)) {
            $this->setAssets($vars->Assets);
        }
        if (isset($vars->IssuedCapital)) {
            $this->setIssuedCapital($vars->IssuedCapital);
        }
        if (isset($vars->SharePremium)) {
            $this->setSharePremium($vars->SharePremium);
        }
        if (isset($vars->TreasuryShares)) {
            $this->setTreasuryShares($vars->TreasuryShares);
        }
        if (isset($vars->OtherEquityInterest)) {
            $this->setOtherEquityInterest($vars->OtherEquityInterest);
        }
        if (isset($vars->OtherReserves)) {
            $this->setOtherReserves($vars->OtherReserves);
        }
        if (isset($vars->RetainedEarnings)) {
            $this->setRetainedEarnings($vars->RetainedEarnings);
        }
        if (isset($vars->NoncontrollingInterests)) {
            $this->setNoncontrollingInterests($vars->NoncontrollingInterests);
        }
        if (isset($vars->Equity)) {
            $this->setEquity($vars->Equity);
        }
        if (isset($vars->NoncurrentProvisionsForEmployeeBenefits)) {
            $this->setNoncurrentProvisionsForEmployeeBenefits($vars->NoncurrentProvisionsForEmployeeBenefits);
        }
        if (isset($vars->OtherLongtermProvisions)) {
            $this->setOtherLongtermProvisions($vars->OtherLongtermProvisions);
        }
        if (isset($vars->NoncurrentPayables)) {
            $this->setNoncurrentPayables($vars->NoncurrentPayables);
        }
        if (isset($vars->DeferredTaxLiabilities)) {
            $this->setDeferredTaxLiabilities($vars->DeferredTaxLiabilities);
        }
        if (isset($vars->CurrentTaxLiabilitiesNoncurrent)) {
            $this->setCurrentTaxLiabilitiesNoncurrent($vars->CurrentTaxLiabilitiesNoncurrent);
        }
        if (isset($vars->OtherNoncurrentFinancialLiabilities)) {
            $this->setOtherNoncurrentFinancialLiabilities($vars->OtherNoncurrentFinancialLiabilities);
        }
        if (isset($vars->OtherNoncurrentNonfinancialLiabilities)) {
            $this->setOtherNoncurrentNonfinancialLiabilities($vars->OtherNoncurrentNonfinancialLiabilities);
        }
        if (isset($vars->NoncurrentLiabilities)) {
            $this->setNoncurrentLiabilities($vars->NoncurrentLiabilities);
        }
        if (isset($vars->CurrentProvisionsForEmployeeBenefits)) {
            $this->setCurrentProvisionsForEmployeeBenefits($vars->CurrentProvisionsForEmployeeBenefits);
        }
        if (isset($vars->OtherShorttermProvisions)) {
            $this->setOtherShorttermProvisions($vars->OtherShorttermProvisions);
        }
        if (isset($vars->TradeAndOtherCurrentPayables)) {
            $this->setTradeAndOtherCurrentPayables($vars->TradeAndOtherCurrentPayables);
        }
        if (isset($vars->CurrentTaxLiabilitiesCurrent)) {
            $this->setCurrentTaxLiabilitiesCurrent($vars->CurrentTaxLiabilitiesCurrent);
        }
        if (isset($vars->OtherCurrentFinancialLiabilities)) {
            $this->setOtherCurrentFinancialLiabilities($vars->OtherCurrentFinancialLiabilities);
        }
        if (isset($vars->OtherCurrentNonfinancialLiabilities)) {
            $this->setOtherCurrentNonfinancialLiabilities($vars->OtherCurrentNonfinancialLiabilities);
        }
        if (isset($vars->LiabilitiesIncludedInDisposalGroups)) {
            $this->setLiabilitiesIncludedInDisposalGroups($vars->LiabilitiesIncludedInDisposalGroups);
        }
        if (isset($vars->CurrentLiabilities)) {
            $this->setCurrentLiabilities($vars->CurrentLiabilities);
        }
        if (isset($vars->Liabilities)) {
            $this->setLiabilities($vars->Liabilities);
        }
        if (isset($vars->EquityAndLiabilities)) {
            $this->setEquityAndLiabilities($vars->EquityAndLiabilities);
        }

        return $this;
    }
}