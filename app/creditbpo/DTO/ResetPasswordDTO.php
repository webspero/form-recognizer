<?php

namespace CreditBPO\DTO;

class ResetPasswordDTO {
    protected $userId;
    protected $email;
    protected $code;
    protected $password;
    protected $confirmPassword;
    protected $dateUpdated;
    protected $passwordRequestId;

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of code
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of confirmPassword
     */ 
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    /**
     * Set the value of confirmPassword
     *
     * @return  self
     */ 
    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }

    /**
     * Get the value of dateUpdated
     */ 
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set the value of dateUpdated
     *
     * @return  self
     */ 
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }
    
    /**
     * Get the value of passwordRequestId
     */ 
    public function getPasswordRequestId()
    {
        return $this->passwordRequestId;
    }

    /**
     * Set the value of passwordRequestId
     *
     * @return  self
     */ 
    public function setPasswordRequestId($passwordRequestId)
    {
        $this->passwordRequestId = $passwordRequestId;

        return $this;
    }

    public function setVars($vars = [])
    {
        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }
        if (isset($vars['code'])) {
            $this->setCode($vars['code']);
        }
        if (isset($vars['password'])) {
            $this->setPassword($vars['password']);
        }
        if (isset($vars['confirmpassword'])) {
            $this->setConfirmPassword($vars['confirmpassword']);
        }
    }

    public function getVars()
    {
        return array(
            'userid' => $this->getUserId(),
            'email' => $this->getEmail(),
            'code' => $this->getCode(),
            'password' => $this->getPassword(),
            'confirmpassword' => $this->getConfirmPassword()
        );
    }
}