<?php

namespace CreditBPO\DTO;

class FSUpdateDTO 
{
    protected $reportId;
    protected $documentType;
    protected $basicDocument;
    protected $fullDocument;
    protected $downloadLink;
    protected $destinationPath;
    protected $zipFilename;

    /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of documentType
     */ 
    public function getDocumentType()
    {
        return $this->documentType;
    }

     /**
     * Set the value of documentType
     *
     * @return  self
     */ 
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get the value of basicDocument
     */ 
    public function getBasicDocument()
    {
        return $this->basicDocument;
    }

    /**
     * Set the value of basicDocument
     *
     * @return  self
     */ 
    public function setBasicDocument($basicDocument)
    {
        $this->basicDocument = $basicDocument;

        return $this;
    }

    /**
     * Get the value of fullDocument
     */ 
    public function getFullDocument()
    {
        return $this->fullDocument;
    }

    /**
     * Set the value of fullDocument
     *
     * @return  self
     */ 
    public function setFullDocument($fullDocument)
    {
        $this->fullDocument = $fullDocument;

        return $this;
    }

    /**
     * Get the value of downloadLink
     */ 
    public function getDownloadLink()
    {
        return $this->downloadLink;
    }

    /**
     * Set the value of downloadLink
     *
     * @return  self
     */ 
    public function setDownloadLink($downloadLink)
    {
        $this->downloadLink = $downloadLink;

        return $this;
    }

    /**
     * Get the value of destinationPath
     */ 
    public function getDestinationPath()
    {
        return $this->destinationPath;
    }

    /**
     * Set the value of destinationPath
     *
     * @return  self
     */ 
    public function setDestinationPath($destinationPath)
    {
        $this->destinationPath = $destinationPath;

        return $this;
    }

    /**
     * Get the value of zipFilename
     */ 
    public function getZipFilename()
    {
        return $this->zipFilename;
    }

    /**
     * Set the value of zipFilename
     *
     * @return  self
     */ 
    public function setZipFilename($zipFilename)
    {
        $this->zipFilename = $zipFilename;

        return $this;
    }

    #region [get array]

    /**
     * @param array $vars
     * @return FSUpdateDTO
     */
    public function getVars()
    {
        return array(
            'reportid' => $this->getReportId(),
            'documenttype' => $this->getDocumentType(),
            'basicdocument' => $this->getBasicDocument(),
            'fulldocument' => $this->getFullDocument(),
            'downloadlink' => route('document.download', ['path' => $this->getDestinationPath(), 'filename' => $this->getZipFilename()])
        );
    }

    #endregion [get array]
}