<?php

namespace CreditBPO\DTO;

class IndustryDTO 
{
    protected $industryId;
    protected $industryTitle;

    /**
     * Get the value of industryId
     */ 
    public function getIndustryId()
    {
        return $this->industryId;
    }

    /**
     * Set the value of industryId
     *
     * @return  self
     */ 
    public function setIndustryId($industryId)
    {
        $this->industryId = $industryId;

        return $this;
    }

    /**
     * Get the value of industryTitle
     */ 
    public function getIndustryTitle()
    {
        return $this->industryTitle;
    }

    /**
     * Set the value of industryTitle
     *
     * @return  self
     */ 
    public function setIndustryTitle($industryTitle)
    {
        $this->industryTitle = $industryTitle;

        return $this;
    }

    /**
     * @param array $vars
     * @return IndustryDTO
     */
    public function setDropdownVars($vars = []) {
        /* For Main Industry */
        if (isset($vars['main_code'])) {
            $this->setIndustryId($vars['main_code']);
        }
        if (isset($vars['main_title'])) {
            $this->setIndustryTitle($vars['main_title']);
        }

        /* For Sub Industry */
        if (isset($vars['sub_code'])) {
            $this->setIndustryId($vars['sub_code']);
        }
        if (isset($vars['sub_title'])) {
            $this->setIndustryTitle($vars['sub_title']);
        }

        /* For Row Industry */
        if (isset($vars['group_code'])) {
            $this->setIndustryId($vars['group_code']);
        }
        if (isset($vars['group_description'])) {
            $this->setIndustryTitle($vars['group_description']);
        }
    }

    /**
     * Get values needed for a dropdown list
     * @return array
     */
    public function getDropdownVars()
    {
        return array(
            'industryid' => $this->getIndustryId(),
            'title' => $this->getIndustryTitle()
        );
    }
}