<?php

namespace CreditBPO\DTO;

class UserReportDTO
{
    //User
    protected $userId; /* login id */
    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $jobTitle;
    protected $activationCode;
    protected $activationDate = '0000-00-00';
    protected $role;
    protected $accountStatus;

    //Report
    protected $reportId;
    protected $companyName;
    protected $companyType; /* entity type */ 
    protected $website;
    protected $isAgree;
    protected $isPaid;
    protected $reportStatus;
    protected $isIndependent;
    protected $organizationId;
    protected $discountCode;

    //Other variables
    protected $clientKey;
    protected $isUsed;
    protected $passwordConfirmation;

    /**
     * @param array $vars
     */
    public function __construct($vars = [])
    {
        $this->setVars($vars);
    }

    #region [getter & setter]

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of firstName
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of jobTitle
     */ 
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set the value of jobTitle
     *
     * @return  self
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get the value of activationCode
     */ 
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * Set the value of activationCode
     *
     * @return  self
     */ 
    public function setActivationCode($activationCode)
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * Get the value of activationDate
     */ 
    public function getActivationDate()
    {
        return $this->activationDate;
    }

    /**
     * Set the value of activationDate
     *
     * @return  self
     */ 
    public function setActivationDate($activationDate)
    {
        $this->activationDate = $activationDate;

        return $this;
    }

    /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of accountStatus
     */ 
    public function getAccountStatus()
    {
        return $this->accountStatus;
    }

    /**
     * Set the value of accountStatus
     *
     * @return  self
     */ 
    public function setAccountStatus($accountStatus)
    {
        $this->accountStatus = $accountStatus;

        return $this;
    }

     /**
     * Get the value of reportId
     */ 
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * Set the value of reportId
     *
     * @return  self
     */ 
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;

        return $this;
    }

    /**
     * Get the value of companyName
     */ 
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set the value of companyName
     *
     * @return  self
     */ 
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get the value of companyType
     */ 
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * Set the value of companyType
     *
     * @return  self
     */ 
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get the value of website
     */ 
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set the value of website
     *
     * @return  self
     */ 
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get the value of isAgree
     */ 
    public function getIsAgree()
    {
        return $this->isAgree;
    }

    /**
     * Set the value of isAgree
     *
     * @return  self
     */ 
    public function setIsAgree($isAgree)
    {
        $this->isAgree = $isAgree;

        return $this;
    }

    /**
     * Get the value of isPaid
     */ 
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set the value of isPaid
     *
     * @return  self
     */ 
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get the value of reportStatus
     */ 
    public function getReportStatus()
    {
        return $this->reportStatus;
    }

    /**
     * Set the value of reportStatus
     *
     * @return  self
     */ 
    public function setReportStatus($reportStatus)
    {
        $this->reportStatus = $reportStatus;

        return $this;
    }

    /**
     * Get the value of isIndependent
     */ 
    public function getIsIndependent()
    {
        return $this->isIndependent;
    }

    /**
     * Set the value of isIndependent
     *
     * @return  self
     */ 
    public function setIsIndependent($isIndependent)
    {
        $this->isIndependent = $isIndependent;

        return $this;
    }

    /**
     * Get the value of organizationId
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set the value of organizationId
     *
     * @return  self
     */ 
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * Get the value of discountCode
     */ 
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    /**
     * Set the value of discountCode
     *
     * @return  self
     */ 
    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;

        return $this;
    }

    /**
     * Get the value of clientKey
     */ 
    public function getClientKey()
    {
        return $this->clientKey;
    }

    /**
     * Set the value of clientKey
     *
     * @return  self
     */ 
    public function setClientKey($clientKey)
    {
        $this->clientKey = $clientKey;

        return $this;
    }

    /**
     * Get the value of passwordConfirmation
     */
    public function getPasswordConfirmation()
    {
        return $this->passwordConfirmation;
    }

    /**
     * Set the value of passwordConfirmation
     *
     * @return  self
     */ 
    public function setPasswordConfirmation($passwordConfirmation)
    {
        $this->passwordConfirmation = $passwordConfirmation;

        return $this;
    }

    /**
     * Get the value of client key isUsed
     */
    public function getIsUsed()
    {
        return $this->isUsed;
    }

    /**
     * Set the value of client key isUsed
     *
     * @return  self
     */
    public function setIsUsed($isUsed)
    {
        $this->isUsed = $isUsed;

        return $this;
    }

    #endregion [getter & setter]

    #region [set array]
    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (isset($vars['email'])) {
            $this->setEmail($vars['email']);
        }

        if (isset($vars['password'])) {
            $this->setPassword($vars['password']);
        }

        if (isset($vars['passwordconfirmation'])) {
            $this->setPasswordConfirmation($vars['passwordconfirmation']);
        }

        if (isset($vars['firstname'])) {
            $this->setFirstName($vars['firstname']);
        }

        if (isset($vars['lastname'])) {
            $this->setLastName($vars['lastname']);
        }

        if (isset($vars['agreetoterms'])) {
            $this->setIsAgree($vars['agreetoterms']);
        }
    }
    #endregion [set array]

    #region [get array]
    /**
     * @param array $vars
     * @return UserReportDTO
     */
    public function getVars()
    {
        return array(
            'email'                 => $this->getEmail(),
            'password'              => $this->getPassword(),
            'passwordconfirmation'  => $this->getPasswordConfirmation(),
            'firstname'             => $this->getFirstName(),
            'lastname'              => $this->getLastName(),
            'agreetoterms'          => $this->getIsAgree(),
        );
    }
    #endregion [get array]

    #region [get response for signup]
    /**
     * @return UserReportDTO
     */
    public function getUserReportResponse()
    {
        return array(
            'userid' => $this->userId,
            'activationcode' => $this->activationCode
        );
    }
    #endregion [set response for signup]
}