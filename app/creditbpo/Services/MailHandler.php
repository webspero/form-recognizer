<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use Mail;
use User;
use ExternalLinkController;
use QuestionnaireLink;
use URL;
use PremiumUser;
use Zipcode;
use CustomNotification;
use NotificationEmails;
use InnodataRecipient;
use Entity;
use Auth;
use \Exception as Exception;
use DB;


class MailHandler {
    public function __construct() {
        $this->config = Config::get('services.mail-sender');
    }

    protected function send(
        $recepient = [],
        $subject,
        $template,
        $templateVars = [],
        $headers = []
    ) {
       
            try{
                $result = Mail::send(
                    $template,
                    $templateVars,
                    function($message) use ($subject, $recepient, $headers) {
                        $message->subject($subject);

                        if (isset($recepient['to'])) {
                            $message->to($recepient['to']);
                        }

                        if (isset($recepient['bcc'])) {
                            foreach ($recepient['bcc'] as $bcc) {
                                $message->bcc($bcc, $bcc);
                            }
                        }

                        if (@count($headers) > 0) {
                            foreach ($headers as $key => $header) {
                                $message->getHeaders()->addTextHeader($key, $header);
                            }
                        }
                    }
                );

            }catch(Exception $e){
                $result = 0;
                sleep(60);
            }

        return $result;
    }

    public function sendReceipt($userId, $vars = [], $subject = '') {
        $user = User::find($userId);
        $template = $this->config['receipt']['template'];
        $recepients = array_merge([
            'to' => $user->email,
            'bcc' => $this->config['receipt']['recepient']['bcc'],
        ]);
        $headers = $this->config['receipt']['headers'];

        if (!$subject) {
            $subject = $this->config['receipt']['subject'];
        }

        return $this->send(
            $recepients,
            $subject,
            $template,
            array_merge(
                [
                    'subject' => $subject,
                    'user' => $user,
                ],
                $vars
            ),
            $headers
        );
    }

    public function sendReceiptToAPIUser($userId, $vars = [], $subject = '') {
        $template = $this->config['receipt']['template'];
        $headers = $this->config['receipt']['headers'];

        if (!$subject) {
            $subject = $this->config['receipt']['subject'];
        }

        $vars['subject'] = $subject;

        /** API Request details */
        $ard = json_decode($vars['receipt']->api_match_account_details);
        $user = (object)[
            'street_address'    => $ard->street,
            'city'              => $ard->city,
            'province'          => $ard->province,
            'zipcode'           => $ard->zipcode,
            'phone'             => $ard->phonenumber,
            'name'              => $ard->fullname
        ];
        $vars['user']->street_address = $ard->street;
        $vars['user']->city = $ard->city;
        $vars['user']->province = $ard->province;
        $vars['user']->zipcode = $ard->zipcode;
        $vars['user']->phone = $ard->phonenumber;
        $vars['user']->name = $ard->fullname;

        $recepients = array(
            'to'    => $ard->email
        );

        try{
            $result = Mail::send(
                $template,
                $vars,
                function($message) use ($subject, $recepients, $headers) {
                    $message->subject($subject);
    
                    if (isset($recepients['to'])) {
                        $message->to($recepients['to']);
                    }
    
                    if (@count($headers) > 0) {
                        foreach ($headers as $key => $header) {
                            $message->getHeaders()->addTextHeader($key, $header);
                        }
                    }
                }
            );
        }catch(Exception $ex){
            //
        }
        
    }

    public function sendAutomateStandaloneEmail($vars) {
        $config = $this->config['automate_standalone_rating'];

        return $this->send(
            $config['recepients'],
            $config['subject'],
            $config['template'],
            $vars,
            $config['headers']
        );
    }

    public function sendNumOfCmapSearchQueries($data) {

    	if(!empty($this->config['cmap_search_queries'])){
	        $config = $this->config['cmap_search_queries'];

	        return $this->send(
	            $config['recepients'],
	            $config['subject'],
	            $config['template'],
	            $data,
	            $config['headers']
	        );
    	}
    }

    public function sendAutomateError($data){
        $info = CustomNotification::where("is_deleted",0)->where("id", 2)->first();
        $emails = NotificationEmails::where("is_deleted",0)->get();
        $cc = array("leovielyn.aureus@creditbpo.com");
        foreach ($emails as $email) {
            $cc[] = $email->email;
        }

        $to = "notify.user@creditbpo.com";
        if($info->count() > 0) {
            $to  = $info->is_running == 1 ?  $info->receiver : $to;
            if($info->is_running == 1) {
                $config = array(
                    'headers' => [
                        'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                        'Disposition-Notification-To' => 'notify.user@creditbpo.com',
                    ],
                    'recepients' => [
                        'to' => $to
                    ],
                    'subject' => 'CreditBPO Rating Error',
                    'template' => 'cron.error_report',
                );
                if($emails->count() > 0) {
                    $config['recepients']['cc'] = $cc;
                }
                
                return $this->send(
                    $config['recepients'],
                    $config['subject'],
                    $config['template'],
                    $data,
                    $config['headers']
                );
            }
        }
        
    }

    public function sendPremiumReportNotification($entity) {
        $email = [];
        $pm = PremiumUser::where('is_deleted', 0)->get();

        $city = Zipcode::where('zip_code', $entity->zipcode)->first();
        $receiver = array();
        foreach ($pm as $value) {
            $receiver[]=$value->email;
        }

        $email['email'] = $receiver;

        if(count($receiver) == 0) {
            $email['email'] = "ana.liza.bongat@creditbpo.com";
        }

        /*  Update Report to add Agency emails     */
        $entity = Entity::where('loginid', Auth::user()->loginid)
                ->orderBy('entityid', 'desc')
                ->first();
        $entity->premium_report_sent_to = $email['email'];
        $entity->save();
        
        $save_array = array(
            'tab' => "registration1",
            'data' => array('biz-details-cntr')
        );

        $randomCode = ExternalLinkController::createRandomCode();
        $randomCode = $randomCode.time();

        // do {
        //     $randomCode = ExternalLinkController::createRandomCode();
        //     $ql = QuestionnaireLink::where('url', $randomCode)->first();
        // } while($ql != null);
        
        $ql = QuestionnaireLink::where("entity_id" , $entity->entityid)->get();
        
        if($ql->count() == 0) {
            QuestionnaireLink::create(array(
                'entity_id' => $entity->entityid,
                'url' => $randomCode,
                'panels' => json_encode($save_array)
            ));
    
            $url = URL::to('/') . '/premium-link/' . $randomCode;
            $loa = DB::table('tbldocument')->where('entity_id', $entity->entityid)->where('document_group', DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION)->first();

            try{
                Mail::send('emails.notifications.premium-report-notification',
                    array('url' => $url, 'entity' => $entity, 'city' => $city, 'loa' => $loa),
                    function($message) use ($email, $entity, $loa) {
                        $message->to($email['email'])
                                ->subject('CreditBPO Inquiry Requested:'.$entity->companyname);
                        // if($loa){
                        //     $message->attach(public_path('documents/letter_authorization/'.$loa->document_rename), array('as' => $loa->document_orig));
                        // }
                    }
                );
            }catch(Exception $e){
            //
            }
        }
    }

    public function sendReminderSupervisorSubcription($supervisor) {
        $email = $supervisor->email;
        
        $expiDate = date_create($supervisor->expiry_date);
        $dueDate = $expiDate->format('F d, Y');
        $supervisor->expiry_date = $dueDate;

        try{
            Mail::send('emails.notifications.supervisor-reminder-subscription', 
                  array('supervisor' => $supervisor),
                  function($message) use ($email) {
                    $message->to($email, $email)
                            ->subject('CreditBPO Supervisor Subscription Reminder');
                }
            );
        }catch(Exception $e){
        //
        }
    }

    function sendReminderNotificationAnalyst($entity, $mail){
        $currentDate = date('F d, Y');
        
        try{
            Mail::send('emails.notifications.analyst-reminder', 
                  array('entity' => $entity, 'currentDate' => $currentDate),
                    function($message) use ($mail) {
                        $message->to($mail, $mail)
                            ->cc($mail)
                            ->subject('CreditBPO List of Submitted Reports for Risk Rating');
                    }
            );

        }catch(Exception $e){
        //
        }
    }

    /*
    * Function: sendInnodataRecepientNotificationMail
    * Description: Send Notification Mail to Innodata Recepient/s
    */
    public function sendInnodataRecepientNotificationMail($files, $to_folder, $from_folder, $file_name)
    {
        $innodata = InnodataRecipient::where('is_deleted', 0)->where('type', InnodataRecipient::INNODATA)->pluck('email')->toArray();
        $creditbpo = InnodataRecipient::where('is_deleted', 0)->where('type', InnodataRecipient::CREDITBPO)->get();

        try{
            Mail::send(
                'emails.notifications.innodata-recipient',
                array(
                    'creditbpo' => $creditbpo, 
                    'files'=> $files, 
                    'to_folder' => $to_folder, 
                    'from_folder' => $from_folder, 
                    'file_name' => $file_name
                ),
                function ($message) use ($innodata) {
                    $message->to($innodata)
                        ->subject('CreditBPO File Upload Notification');
                }
            );

        }catch(Exception $e){
        //
        }
    }

    public function sendRegeneratedTranscribeKey($email, $key){
        try{
            Mail::send(
                'emails.notifications.regenerate-transcribe-key',
                array(
                    'key' => $key
                ),
                function ($message) use ($email, $key) {
                    $message->to($email)
                            ->subject('CreditBPO Public Transcribe Key');
                }
            );

        }catch(Exception $e){
            //
        }
    }
}
