<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Validator;
use Auth;
use Mail;
use Hash;
use CreditBPO\Libraries\MaliciousAttemptLib;
use CreditBPO\DTO\UserReportDTO;
use CreditBPO\DTO\UserAuthDTO;
use CreditBPO\Models\User;
use CreditBPO\Models\UserLoginAttempt;
use CreditBPO\Models\User2faAttempt;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\BankSerialKey as BankClientKey;
use CreditBPO\Models\IndependentKey;
use CreditBPO\Models\Discount;
use CreditBPO\Models\BankDocOption;
use CreditBPO\Models\Organization;
use CreditBPO\Models\UserAuthToken;
use CreditBPO\DTO\UserAttemptDTO;
use CreditBPO\Models\DeveloperApiUser;

//======================================================================
//  Class #: Account Service
//  Functions related to Accounts
//======================================================================
class AccountService
{
    //-----------------------------------------------------
    //  Function #: __construct
    //  Initialization
    //-----------------------------------------------------
    public function __construct()
    {
        //Models
        $this->userDb = new User();
        $this->userAttemptDb = new UserLoginAttempt();
        $this->user2faDb = new User2faAttempt();
        $this->reportDb = new Report();
        $this->bankClientKeyDb = new BankClientKey();
        $this->independentKeyDb = new IndependentKey();
        $this->discountDb = new Discount();
        $this->bankDocOptionDb = new BankDocOption();
        $this->organizationDb = new Organization();
        $this->userAuthTokenDb = new UserAuthToken();
        $this->developerApiUser = new DeveloperApiUser();

        //Libraries
        $this->malAttemptLib = new MaliciousAttemptLib();

        //DTOs
        $this->userReportDto = new UserReportDTO();
    }

    #region [Account Login]
    
    //-----------------------------------------------------
    //  Function #: validateLoginData
    //  Laravel validation for logging in
    //-----------------------------------------------------
    public function validateLoginData($loginData)
    {
        /* Sets Validation Messages */
        $messages = array(
            'email.required' => 'Email address is required.',
            'password.required' => 'Password is required.',
        );

        /* Sets Validation Rules */
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|min:6'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($loginData, $rules, $messages);
        return $validator;
    }

    //-----------------------------------------------------
    //  Function #: getUserByEmail
    //  Function that get details of email in database
    //-----------------------------------------------------
    public function getUserByEmail($userAccountDto)
    {
        $email = $userAccountDto->getEmail();
        $userData = $this->userDb->getUserByEmail($email);
        $userAccountDto->setUserId($userData['loginid']);
        $userAccountDto->setFirstName($userData['firstname']);
        $userAccountDto->setLastName($userData['lastname']);
    }

    //-----------------------------------------------------
    //  Function #: getAttemptDetails
    //  Function that get user attempt data
    //-----------------------------------------------------
    public function getAttemptDetails($userId)
    {
        /*Check if existing*/
        $attemptDetails = $this->userAttemptDb->getUserAttempt($userId);
        if (!is_null($attemptDetails)){
            $data = array(
                'id' => $attemptDetails['login_attempt_id'],
                'userid' => $attemptDetails['login_id'],
                'attempts' => $attemptDetails['attempts'],
                'countrycode' => $attemptDetails['country_code'],
                'status' => $attemptDetails['status'],
                'lastlogin' => $attemptDetails['last_login']
            );
            $userAttemptDto = new UserAttemptDTO;
            $userAttemptDto->setVars($data);
            return $userAttemptDto;
        }else{
            return null;
        }
    }

    //-----------------------------------------------------
    //  Function #: recordAttemptStatus
    //  Function that save user attempt data
    //-----------------------------------------------------
    public function recordAttemptStatus($userAccountDto, $userAttemptDto)
    {
        /*If user has already wrong attempt*/
        if (!empty($userAttemptDto) && !is_null($userAttemptDto->getId()))
        {
            $attempts = $userAttemptDto->getAttempts() + 1;
            /*Update data, increment attempts count*/
            $updateDetails = array(
                'attempts' => $attempts,
                'last_login' => date('Y-m-d H:i:s')
            );
            $update = $this->userAttemptDb->updateUserAttempt($userAttemptDto->getId(), $updateDetails);
            if ($update){
                $userAttemptDto->setVars($updateDetails);
                /*If max login failed meets, send email*/
                if ($userAttemptDto->getAttempts() >= MAX_LOGIN_FAILED){
                    $this->sendSecurityAlertEmail($userAccountDto, $userAttemptDto);
                }
                return $userAttemptDto;
            } else{
                return null;
            }
        } else{
            /*Get current location*/
            $location = $this->malAttemptLib->getClientCountry();

            $userAttemptDto = new UserAttemptDTO;
            /*Create new user attempt details*/
            $userAttemptDto->setUserId($userAccountDto->getUserId())
                           ->setAttempts(1)
                           ->setCountryCode($location['countrycode'])
                           ->setStatus(ACTIVE)
                           ->setLastLogin(date('Y-m-d H:i:s'));
            $insert = $this->userAttemptDb->saveUserAttempt($userAttemptDto);
            if ($insert){
                return $userAttemptDto;
            } else{
                return null;
            }
        }
    }

    //-----------------------------------------------------
    //  Function #: verifyLoginCredentials
    //  Function that check and login account credentials
    //-----------------------------------------------------
    public function verifyLoginCredentials($userAccountDto)
    {
        /* Account Credentials */
        $credentials = array(
            'email'	=> $userAccountDto->getEmail(),
            'password' => $userAccountDto->getPassword()
        );

        if (Auth::attempt($credentials)){
            $userAccountDto->setUserId(Auth::user()->loginid)
                           ->setFirstName(Auth::user()->firstname)
                           ->setLastName(Auth::user()->lastname)
                           ->setRole(Auth::user()->role)
                           ->setStatus(Auth::user()->status);
            return true;
        } else{
            return false;
        }
    }
    
    //-----------------------------------------------------
    //  Function #: generateUserTokens
    //  Function that will generate user auth tokens
    //-----------------------------------------------------
    public function generateUserTokens($userId, $clientId)
    {
        $tokenUsed = false;
        do {
            $accessToken = Hash::make(uniqid(rand(), true));
            $refreshToken = Hash::make(uniqid(rand(), true));
            $tokenDetails = $this->userAuthTokenDb->getTokenDetails($accessToken, $refreshToken);
            if(empty($tokenDetails)){
                $tokenUsed = true;
            }
        } while ($tokenUsed == false);
        
        $userAuthDto = new UserAuthDTO();
        $userAuthDto->setUserId($userId)
                    ->setClientId($clientId)
                    ->setAccessToken($accessToken)
                    ->setRefreshToken($refreshToken)
                    ->setExpiresIn(date("Y-m-d H:i:s", strtotime('+1 hours')))
                    ->setStatus(ACTIVE);
            
        $userAuthSaved = $this->userAuthTokenDb->addNewToken($userAuthDto);
        if ($userAuthSaved){
            return $userAuthDto;
        } else {
            return null;
        }
    }


    //-----------------------------------------------------
    //  Function #: resetLoginAttempt
    //  Function that reset login attempts data
    //-----------------------------------------------------
    public function resetLoginAttempt($userAttemptDto){
        $userAttemptDto->setStatus(DELETED);
        $userAttemptDto->setLastLogin(date("Y-m-d H:i:s"));
        $result = $this->userAttemptDb->updateUserAttemptStatus($userAttemptDto);
        return $result;
    }
    #endregion [Account Login]

    #region [Account Signup]

    //-----------------------------------------------------
    //  Function #: getStandaloneOrganizationByPage
    //  Returns the list of organizations with standalone report by page
    //-----------------------------------------------------
    public function getStandaloneOrganizationByPage($page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get standalone organizations in bank table */
        $organizationsList = $this->organizationDb->getStandaloneOrganizationByPage($page, $limit);
        $results['total'] = $this->organizationDb->getStandaloneOrganizations()->count();  
        $results['items'] = $organizationsList->all();

        return $results;
    }

    //-----------------------------------------------------
    //  Function #: getStandaloneOrganization
    //  Returns the list of organizations with standalone report by page
    //-----------------------------------------------------
    public function getStandaloneOrganization()
    {
        $results = array();
        $results['page'] = 1;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get standalone organizations in bank table */
        $organizationsList = $this->organizationDb->getStandaloneOrganizations();
        $results['total'] = $this->organizationDb->getStandaloneOrganizations()->count();  
        $results['items'] = $organizationsList->all();

        return $results;
    }

    //-----------------------------------------------------
    //  Function #: validateSignupData
    //  Laravel validation for sign up
    //-----------------------------------------------------
    public function validateSignupData($signupDetails)
    {
        /* Sets Validation Messages */
        $messages = array(
            'email.required'                => 'Email is required.',
            'email.unique'                  => 'The email you are trying to register is already taken.',
            'password.required'             => 'Password is required.',
            'passwordconfirmation.required' => 'Password confirmation is required.',
            'firstname.required'            => 'First name is required.',
            'lastname.required'             => 'Last name is required.',
            'agreetoterms.required'         => 'Agree to Privacy Policy and Terms of Use is required.',
            'agreetoterms.in'               => 'The Privacy Policy and Terms of Use need to be accepted.',
            'agreetoterms.boolean'          => "The Privacy Policy and Terms of Use only accepts boolean type values true or false.",
        );

        /* Sets Validation Rules */
        $rules = array(
            'email'	                => 'required|unique:tbllogin|unique:supervisor_application',
            'password'	            => 'required|min:6',
            'passwordconfirmation'	=> 'required|min:6|same:password',
            'firstname'	            => 'required',
			'lastname'	            => 'required',
            'agreetoterms'          => 'required|boolean|in:'.true,
        );

        /* Run Laravel Validation */
        $validator = Validator::make($signupDetails, $rules, $messages);
        return $validator;
    }

    //-----------------------------------------------------
    //  Function #: validateOrganizationId
    //  Validation of organization id if exist
    //-----------------------------------------------------
    public function validateOrganizationId($organizationId)
    {
        /* Check if the given id exists in bank table */
        $organization = $this->organizationDb->getOrganizationById($organizationId);
        return $organization;
    }
    
    //-----------------------------------------------------
    //  Function #: validateClientKey
    //  Validation of client code if exist
    //-----------------------------------------------------
    public function validateClientKey($userReportDto, $clientkey)
    {
        /* Check if the given key with organization id exists in bank client keys table and unused */
        $organizationId = $userReportDto->getOrganizationId();
        $clientKeyDetails = $this->bankClientKeyDb->getBankClientKey($clientkey, $organizationId);
        if ($clientKeyDetails == null){
            /* Check if the given key exists in independent keys table and unused */
            $clientKeyDetails = $this->independentKeyDb->getIndependentKey($clientkey);
        }
        if ($clientKeyDetails){
            $userReportDto->setClientKey($clientKeyDetails);
        }
        return $clientKeyDetails;
    }

    //-----------------------------------------------------
    //  Function #: validateDiscountCode
    //  Validation of discount code if exist, active and valid
    //-----------------------------------------------------
    public function validateDiscountCode($userReportDto, $discountCode)
    {
        /* Check if the given discount code exist, active and valid */
        $discountDetails = $this->discountDb->getDiscountByValidity($discountCode, ACTIVE);
        if ($discountDetails){
            $userReportDto->setDiscountCode($discountCode);
        }
        return $discountDetails;
    }

    //-----------------------------------------------------
    //  Function #: createUser
    //  Save new user details
    //-----------------------------------------------------
    public function createUser($userReportDto)
    {
        $activationCode = rand(00000,99999);
        $userReportDto->setActivationCode($activationCode)
                      ->setRole(USER_BASIC_USER)
                      ->setAccountStatus(USER_STS_INACTIVE);
        $user = $this->userDb->addNewUser($userReportDto);
        $userReportDto->setUserId($user->loginid);
    }

    //-----------------------------------------------------
    //  Function #: createReport
    //  Save new report details
    //-----------------------------------------------------
    public function createReport($userReportDto)
    {
        $companyType = $userReportDto->getCompanyType();
        if (strtolower($companyType) == 'sole proprietorship'){
            $userReportDto->setCompanyType(COMPANY_TYPE_SOLE_PROP);
        }
        $userReportDto->setCompanyType(COMPANY_TYPE_CORPORATE);
        $userReportDto->setIsAgree(INT_TRUE)
                      ->setIsPaid(INT_FALSE)
                      ->setReportStatus(REPORT_STATUS_PENDING)
                      ->setIsIndependent(APPLIED_WITH_COMPANY); /* Automatically applied with associated company */

        $report = $this->reportDb->addNewReport($userReportDto);
        $userReportDto->setReportId($report->entityid);
        return $report->entityid;
    }

    //-----------------------------------------------------
    //  Function #: sendAccountVerificationEmail
    //  Send email for user account verification
    //-----------------------------------------------------
    public function sendSuccessRegistrationEmail($userReportDto)
    {
        $docRequirements = $this->bankDocOptionDb->getBankDocOptions($userReportDto->getOrganizationId());

        $emailDetails = array(
            'email' => $userReportDto->getEmail(),
            'firstname' => $userReportDto->getFirstName(),
            'lastname' => $userReportDto->getLastName()
        );
        $activationCode = $userReportDto->getActivationCode();

        if (env("APP_ENV") != "local") {

            try{
                Mail::send(
                    'emails.signup',
                            array(
                                'activationcode'=> $activationCode,
                                'email' => $emailDetails['email']
                            ),
                    function ($message) use ($emailDetails) {
                        $message->to($emailDetails['email'], $emailDetails['firstname'] . ' ' . $emailDetails['lastname'])
                                ->subject('Welcome to CreditBPO!');

                        $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                        $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                    }
                );

            }catch(Exception $e){
                //
            }

            if (env("APP_ENV") == "Production") {

                try{
                    Mail::send(
                        'emails.signup-notice',
                                array(
                                    'name' => $emailDetails['firstname'] . ' ' . $emailDetails['lastname'],
                                    'email_add' => $emailDetails['email']
                                ),
                        function ($message) {
                            $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
                                    ->subject('New registered member');
                        }
                    );

                }catch(Exception $e){
                //
                }

            }
        }
    }

    //-----------------------------------------------------
    //  Function #: sendSecurityAlertEmail
    //  Send email for malicious attempt notification
    //-----------------------------------------------------
    public function sendSecurityAlertEmail($userAccountDto, $userAttemptDto)
    {
        $emailDetails = array(
            'email' => $userAccountDto->getEmail(),
            'firstname' => $userAccountDto->getFirstName(),
            'lastname' => $userAccountDto->getLastName(),
            'attempid' => $userAttemptDto->getId(),
            'attempts' => $userAttemptDto->getAttempts(),
            'countrycode' => $userAttemptDto->getCountryCode(),
            'status' => $userAttemptDto->getStatus(),
            'lastlogin' => $userAttemptDto->getLastLogin()
        );

        if (env("APP_ENV") != "local") {

            try{
                Mail::send(
                    'emails.malicious_attempt',
                            array(
                                'email'=> $emailDetails['email'],
                                'firstname'=> $emailDetails['firstname'],
                                'lastname' => $emailDetails['lastname']
                            ),
                    function ($message) use ($emailDetails) {
                        $message->to($emailDetails['email'], $emailDetails['firstname'] . ' ' . $emailDetails['lastname'])
                                ->subject('Security Alert!');

                        $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                        $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                    }
                );

            }catch(Exception $e){
            //
            }    

            if (env("APP_ENV") == "Production") {

                try{
                    Mail::send(
                        'emails.account_locked_notice',
                                array(
                                    'email' => $emailDetails['email'],
                                    'attemptid' => $emailDetails['attempid'],
                                    'attempts' => $emailDetails['attempts'],
                                    'countrycode' => $emailDetails['countrycode'],
                                    'status' => $emailDetails['status'],
                                    'lastlogin' => $emailDetails['lastlogin']
                                ),
                        function ($message){
                            $message->to('notify.user@creditbpo.com', 'notify.user@creditbpo.com')
                                    ->subject('Account Locked');
                        }
                    );

                }catch(Exception $e){
                //
                }
            }
        }
    }

    //-----------------------------------------------------
    //  Function #: updateClientKey
    //  Update client key details after user used it
    //-----------------------------------------------------
    public function updateClientKey($userReportDto)
    {
        $userReportDto->setIsUsed(INT_TRUE);
        $clientKey = $userReportDto->getClientKey();
        if (isset($clientkey->email)){
            /* Save to Independent Keys */
            $this->independentKeyDb->updateIndependentKey($userReportDto, $clientKey);
        } else{
            /* Save to Bank Keys */
            $this->bankClientKeyDb->updateBankClientKey($userReportDto, $clientKey);
        }
    }

    #endregion [Account Signup]

    #region [Account Activation]

    //-----------------------------------------------------
    //  Function #: validateActivationData
    //  Laravel validation for account activation
    //-----------------------------------------------------
    public function validateActivationData($userAccountDto)
    {
        /* Sets Validation Messages */
        $messages = array(
            'email.required' => 'Email address is required.',
            'password.required' => 'Password is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|min:6'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($userAccountDto, $rules, $messages);
        return $validator;
    }

    //-----------------------------------------------------
    //  Function #: verifyAccountActivation
    //  Function that check activation code and user credentials
    //-----------------------------------------------------
    public function verifyAccountActivation($userAccountDto)
    {
        /* Check if activation code and user exist */
        $user = $this->userDb->getUserByActivationCode($userAccountDto);
        if (!empty($user)){
            $userAccountDto->setUserId($user['loginid'])
                           ->setRole($user['role'])
                           ->setStatus($user['status'])
                           ->setEmail($user['email'])
                           ->setActivationCode($user['activation_code']);
            return $userAccountDto;
        }
        return null;
    }

    //-----------------------------------------------------
    //  Function #: getUserFirstReport
    //  Get user first report
    //-----------------------------------------------------
    public function getUserFirstReport($userAccountDto)
    {
        /* Get report id by user id */
        $report = $this->reportDb->getFirstReportByUserId($userAccountDto->getUserId());
        $reportId = $report->entityid;
        if ($reportId != null && $reportId != 0){
            $this->userReportDto->setReportId($reportId);
            /* Set discount code */
            $this->userReportDto->setDiscountCode($report->discount_code);

            /* Check if report use bank keys */
            $clientKey = $this->bankClientKeyDb->getBankKeyByReportId($reportId);

            /* Check if report used independent keys */
            if (!$clientKey){
                $clientKey = $this->independentKeyDb->getIndependentKeyByReportId($reportId);
            } else{
                $this->userReportDto->setClientKey($clientKey->serial_key);
            }
            return $this->userReportDto;
        }
        return null;
    }

    //-----------------------------------------------------
    //  Function #: updateAccountActivation
    //  Update account activation details
    //-----------------------------------------------------
    public function updateAccountActivation($userAccountDto, $status)
    {
        $userAccountDto->setStatus($status)
                       ->setActivationCode(0)
                       ->setActivationDate(date('Y-m-d H:i:s'));

        $activateSuccess = $this->userDb->updateAccountActivation($userAccountDto);
        return $activateSuccess;
    }

    //-----------------------------------------------------
    //  Function #: updateReportToPaid
    //  Update report payment status to paid
    //-----------------------------------------------------
    public function updateReportToPaid($userReportDto, $paymentStatus)
    {
        $userReportDto->setIsPaid($paymentStatus);
        $reportUpdate = $this->reportDb->updateReportPaymentStatus($userReportDto);
        return $reportUpdate;
    }

    //-----------------------------------------------------
    //  Function #: checkIfFreeTrial
    //  Check discount code if free trial
    //-----------------------------------------------------
    public function checkIfFreeTrial($discountCode)
    {
        $discount = $this->discountDb->getDiscountByCode($discountCode);
        /* Free Trial */
		if (($discount->type == 1) && ($discount->amount == 100)) {
            return true;
        } else {
            return false;
        }
    }

    #endregion [Account Activation]

    public function validateAPIUser($clientId, $secretKey){
        
        $validateUser = $this->developerApiUser->checkApiKey($clientId, $secretKey);
        return $validateUser;
    }

    public function getUserDetails($email){
        $user = $this->userDb->getUserById($email);
        return $user;
    }

    public function validateBillingDetails($billingdetails){
        /* Sets Validation Messages */
        $messages = array(
            'email.required'            => 'Email address is required.',
            'password.required'         => 'Password is required.',
            'streetaddress.required'    => 'Street Addres is required',
            'city.required'             => 'City is required.',
            'province.required'         => 'Province is required.',
            'zipcode'                   => 'Zipcode is required',
            'phonenumber'               => 'Phone Number is required'
        );

        /* Sets Validation Rules */
        $rules = array(
            'streetaddress' => 'required',
            'city'          => 'required',
            'province'      => 'required',
            'zipcode'       => 'required',
            'phonenumber'   => 'required'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($billingdetails, $rules, $messages);
        return $validator;
    }

    public function saveBillingDetails($billingdetailsDto){
        $data = $this->userDb->updateBillingDetails($billingdetailsDto);
        return $data;
    }
}