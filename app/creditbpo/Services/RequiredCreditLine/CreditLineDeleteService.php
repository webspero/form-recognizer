<?php

namespace CreditBPO\Services\RequiredCreditLine;

use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\PlanFacilityRequested as RequiredCreditLine;

class CreditLineDeleteService
{
    public function __construct()
    {
        $this->creditLineDb = new RequiredCreditLine;
    }

    public function deleteCreditLine($creditLineDTO) 
    {
        return $this->creditLineDb->deleteCreditLine($creditLineDTO->getCreditLineId());
    }
}