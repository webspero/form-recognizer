<?php

namespace CreditBPO\Services\RequiredCreditLine;

use CreditBPO\Models\PlanFacilityRequested as RequiredCreditLine;
use CreditBPO\DTO\CreditLineDTO;

class CreditLineEditService
{
    public function __construct()
    {
        $this->creditLineDb = new RequiredCreditLine;
    }

    public function prepareCreditLineUpdate($existingCreditLineDTO, $updateCreditLineData)
    {
        $updateCreditLineDTO = new CreditLineDTO;
        $errors = array();

        if($updateCreditLineData != null) {
            $updateCreditLineDTO->setRequestVars($updateCreditLineData, $existingCreditLineDTO->getReportId());
    
            $updateCreditLineDTO->setCreditLineId($existingCreditLineDTO->getCreditLineId());
        } else {
            array_push($errors, "No Credit Line field to be updated.");
        }

        $updateCreditLineDTO->setErrors($errors);

        return $updateCreditLineDTO;
    }

    public function updateCreditLine($updateCreditLineDTO, $existingCreditLineDTO)
    {
        $toBeUpdated = array();

        if($updateCreditLineDTO->getAmountOfLine() !== null) {
            $toBeUpdated['plan_credit_availment'] = $updateCreditLineDTO->getAmountOfLine();
        }

        if($updateCreditLineDTO->getDepositAccountType() !== null) {
            $toBeUpdated['cash_deposit_details'] = $updateCreditLineDTO->getDepositAccountType();
        }

        if($updateCreditLineDTO->getDepositAmount() !== null) {
            $toBeUpdated['cash_deposit_amount'] = $updateCreditLineDTO->getDepositAmount();
        }

        if($updateCreditLineDTO->getMarketableSecurities() !== null) {
            $toBeUpdated['securities_details'] = $updateCreditLineDTO->getMarketableSecurities();
        }

        if($updateCreditLineDTO->getMarketableSecuritiesValue() !== null) {
            $toBeUpdated['securities_estimated_value'] = $updateCreditLineDTO->getMarketableSecuritiesValue();
        }

        if($updateCreditLineDTO->getProperty() !== null) {
            $toBeUpdated['property_details'] = $updateCreditLineDTO->getProperty();
        }

        if($updateCreditLineDTO->getPropertyValue() !== null) {
            $toBeUpdated['property_estimated_value'] = $updateCreditLineDTO->getPropertyValue();
        }

        if($updateCreditLineDTO->getChattel() !== null) {
            $toBeUpdated['chattel_details'] = $updateCreditLineDTO->getChattel();
        }

        if($updateCreditLineDTO->getChattelValue() !== null) {
            $toBeUpdated['chattel_estimated_value'] = $updateCreditLineDTO->getChattelValue();
        }

        if($updateCreditLineDTO->getOthers() !== null) {
            $toBeUpdated['others_details'] = $updateCreditLineDTO->getOthers();
        }

        if($updateCreditLineDTO->getOthersValue() !== null) {
            $toBeUpdated['others_estimated_value'] = $updateCreditLineDTO->getOthersValue();
        }

        if($updateCreditLineDTO->getPurposeCreditFacility() !== null) {
            $toBeUpdated['purpose_credit_facility'] = $updateCreditLineDTO->getPurposeCreditFacility();
        }

        if($updateCreditLineDTO->getOthersPurposeCreditFacility() !== null) {
            $toBeUpdated['purpose_credit_facility_others'] = $updateCreditLineDTO->getOthersPurposeCreditFacility();
        }

        if($updateCreditLineDTO->getSupplierCreditTerms() !== null) {
            $toBeUpdated['credit_terms'] = $updateCreditLineDTO->getSupplierCreditTerms();
        }

        if($updateCreditLineDTO->getOtherRemarks() !== null) {
            $toBeUpdated['other_remarks'] = $updateCreditLineDTO->getOtherRemarks();
        }

        $this->creditLineDb->updateCreditLine($updateCreditLineDTO->getCreditLineId(), $toBeUpdated);
    }
}