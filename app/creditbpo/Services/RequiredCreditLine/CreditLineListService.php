<?php

namespace CreditBPO\Services\RequiredCreditLine;

use CreditBPO\Models\PlanFacilityRequested as RequiredCreditLine;
use CreditBPO\DTO\CreditLineDTO;

class CreditLineListService
{
    public function __construct()
    {
        $this->creditLineDb = new RequiredCreditLine;
    }

    function getCreditLines($reportId)
    {
        $creditLineRecords = $this->creditLineDb->getCreditLinesByReportId($reportId);

        $creditLines = array();

        foreach($creditLineRecords as $creditLineRecord) {
            $creditLineDTO = new CreditLineDTO;
            $creditLineDTO->setVars($creditLineRecord);

            array_push($creditLines, $creditLineDTO->getFormVars());
        }

        return $creditLines;
    }
    
    function getCreditLinesByPage($reportId, $page, $limit)
    {
        $creditLineRecords = $this->creditLineDb->getCreditLinesByReportIdByPage($reportId, $page, $limit);
        $totalCount = @count($this->creditLineDb->getCreditLinesByReportId($reportId));

        $creditLines = array();

        $creditLines['page'] = $page;
        $creditLines['limit'] = $limit;
        $creditLines['total'] = $totalCount;
        $creditLines['items'] = array();       

        foreach($creditLineRecords as $creditLineRecord) {
            $creditLineDTO = new CreditLineDTO;
            $creditLineDTO->setVars($creditLineRecord);

            array_push($creditLines['items'], $creditLineDTO->getFormVars());
        }

        return $creditLines;
    }

    function validateCreditLine($reportId, $creditLineId)
    {
        $errorMessages = array();
        $existingReportCreditLine = $this->creditLineDb->checkReportCreditLine($reportId, $creditLineId);

        if($existingReportCreditLine == null) {
            $errorMessages['errors'] = "Invalid Report and Requested Credit Line combination.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }

    function getCreditLineDetails($creditLineId)
    {
        $creditLineRecord = $this->creditLineDb->getCreditLineDetails($creditLineId);

        $creditLineDTO = new CreditLineDTO;
        $creditLineDTO->setVars($creditLineRecord);

        return $creditLineDTO;
    }
}