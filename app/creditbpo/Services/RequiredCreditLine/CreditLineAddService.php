<?php

namespace CreditBPO\Services\RequiredCreditLine;

use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\PlanFacilityRequested as RequiredCreditLine;
use CreditBPO\DTO\CreditLineDTO;

class CreditLineAddService
{
    public function __construct()
    {
        $this->creditLineDb = new RequiredCreditLine;
    }

    public function validateRequest($addCreditLineData) {
        /* Sets Validation Messages */
        $messages = array(
        );

        /* Sets Validation Rules */
        $rules = array(
            'amountofline' => 'required|numeric|max:9999999999999.99',
            'purposeofcreditfacility' => 'required|in:1,2,3,4,5,6,7',
            'creditterms' => 'required_if:purposeofcreditfacility,5',
            'purposeofcreditfacilityothers' => 'required_if:purposeofcreditfacility,4',
			'typeofdepositaccount' => 'sometimes|in:CA,SA',
			'depositamount'	=> 'required_with:typeofdepositaccount|numeric|max:9999999999999.99',
			'marketablesecurities' => 'sometimes',
			'marketablesecuritiesestimatedvalue' => 'required_with:marketablesecurities|numeric|max:9999999999999.99',
			'property' => 'sometimes',
			'propertyestimatedvalue' => 'required_with:property|numeric|max:9999999999999.99',
			'chattel' => 'sometimes',
			'chattelestimatedvalue'	=> 'required_with:chattel|numeric|max:9999999999999.99',
			'others' => 'sometimes',
			'othersestimatedvalue' => 'required_with:others|numeric|max:9999999999999.99',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($addCreditLineData, $rules, $messages);
        return $validator;
    }

    public function addCreditLine($addCreditLineData, $reportId) {
        $newCreditLineDTO = new CreditLineDTO;
        $newCreditLineDTO->setRequestVars($addCreditLineData, $reportId);

        $newCreditLine = array(
            'entity_id' => $newCreditLineDTO->getReportId(),
            'plan_credit_availment' => $newCreditLineDTO->getAmountOfLine(),
            'cash_deposit_details' => ($newCreditLineDTO->getDepositAccountType() !== null)? $newCreditLineDTO->getDepositAccountType() : "",
            'cash_deposit_amount' => ($newCreditLineDTO->getDepositAmount() !== null)? $newCreditLineDTO->getDepositAmount() : 0,
            'securities_details' => ($newCreditLineDTO->getMarketableSecurities() !== null)? $newCreditLineDTO->getMarketableSecurities() : "",
            'securities_estimated_value' => ($newCreditLineDTO->getMarketableSecuritiesValue() !== null)? $newCreditLineDTO->getMarketableSecuritiesValue() : 0,
            'property_details' => ($newCreditLineDTO->getProperty() !== null)? $newCreditLineDTO->getProperty() : "",
            'property_estimated_value' => ($newCreditLineDTO->getPropertyValue() !== null)? $newCreditLineDTO->getPropertyValue() : 0,
            'chattel_details' => ($newCreditLineDTO->getChattel() !== null)? $newCreditLineDTO->getChattel() : "",
            'chattel_estimated_value' => ($newCreditLineDTO->getChattelValue() !== null)? $newCreditLineDTO->getChattelValue() : 0,
            'others_details' => ($newCreditLineDTO->getOthers() !== null)? $newCreditLineDTO->getOthers() : "",
            'others_estimated_value' => ($newCreditLineDTO->getOthersValue() !== null)? $newCreditLineDTO->getOthersValue() : 0,
            'purpose_credit_facility' => $newCreditLineDTO->getPurposeCreditFacility(),
            'purpose_credit_facility_others' => ($newCreditLineDTO->getOthersPurposeCreditFacility() !== null)? $newCreditLineDTO->getOthersPurposeCreditFacility() : "",
            'credit_terms' => ($newCreditLineDTO->getSupplierCreditTerms() !== null)? $newCreditLineDTO->getSupplierCreditTerms() : "",
            'other_remarks' => ($newCreditLineDTO->getOtherRemarks() !== null)? $newCreditLineDTO->getOtherRemarks() : ""
        );

        $newCreditLineRecord = $this->creditLineDb->addCreditLine($newCreditLine);
        $newCreditLineDTO->setCreditLineId($newCreditLineRecord);

        return $newCreditLineDTO;
    }
}