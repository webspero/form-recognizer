<?php

namespace CreditBPO\Services\UtilityBill;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;

class UtilityBillList
{
    CONST UPLOAD_DESTINATION_PATH = 'documents';

    public function __construct()
    {
        $this->documentDb = new Document;
        $this->reportDb = new Report;
    }

    public function validateReportIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function getReportUBList($reportId)
    {
        $ubDocument = $this->documentDb->getUbDocumentByReportId($reportId);
        if (!$ubDocument->isEmpty()){
            $returnList = array(
                'reportid' => $reportId,
                'utilitybills' => array()
            );
            foreach($ubDocument as $document){
                /* Manage return value */
                $returnValue = array(
                    'id' => $document->documentid,
                    'document' => $document->document_orig,
                    'downloadlink' => route('reportdocument.download', ['reportid' => $reportId, 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $document->document_orig])
                );
                array_push($returnList['utilitybills'], $returnValue);
            }
            return $returnList;
        } else{
            return null;
        }
    }
}