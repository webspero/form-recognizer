<?php

namespace CreditBPO\Services\UtilityBill;

use Validator;
use DropboxHelper;
use CreditBPO\Models\Document;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Libraries\MimeTypes;
use CreditBPO\DTO\UBUploadDTO;

class UtilityBillUpload
{
    CONST UPLOAD_DESTINATION_PATH = 'documents';
    protected $ubDestinationPath;

    public function __construct()
    {
        $this->reportDb = new Report;
        $this->documentDb = new Document;
        $this->mimeTypesLibrary = new MimeTypes;
    }

    public function manageRequestDetails($request, $reportId)
    {
        /*Fill up array of dto*/
        $ubUploadDtoList = [];
        /* Get utility bills details */
        if (array_key_exists('utilitybills', $request)){
            /* Check if array */
            $utilityBills = $request['utilitybills'];
            if (is_array($utilityBills)){
                foreach($utilityBills as $key => $value){
                    if ($value){
                        $ubUploadDto = new UBUploadDTO;
                        $value['reportid'] = $reportId;
                        /* Check if mime/file type in is field file */
                        $parts = explode(";base64,", $value['file']);
                        if (@count($parts) > 1){
                            $value['file'] = $parts[1];
                            /* Get the file type */
                            $value['filetype'] = explode(":", $parts[0])[1];
                        }
                        // $value['file'] = base64_decode($value['file']);
                        $ubUploadDto->setVars($value);
                        array_push($ubUploadDtoList, $ubUploadDto);
                    }
                }
            }
        }
        return $ubUploadDtoList;
    }

    public function validateRequestData($ubUploadDtoList)
    {
        $errors = [];
        /* Check if request has valid report id */
        $report = $this->reportDb->getReportByReportId($ubUploadDtoList[0]->getReportId());
        if (is_null($report)){
            array_push($errors, "Report ID doesn't exist.");
        }
        
        $mimeTypes = array_keys($this->mimeTypesLibrary->mimeTypes);
        foreach($ubUploadDtoList as $key => $value){
            if ($value){
                $rules['file'] = 'required';
                $rules['filetype'] = 'required|in:'.implode(',',$mimeTypes);
                $rules['date'] = 'required|date_format:Y-m-d';

                $messages['file.required'] = "File for utility bill ".($key+1)." is required.";
                $messages['filetype.required'] = "File type for utility bill ".($key+1)." is required.";
                $messages['date.required'] = "Date for utility bill ".($key+1)." is required.";
                $messages['filetype.in'] = "File type for utility bill ".($key+1)." is not allowed. File should be a PDF, Excel, CSV, Zip or Rar.";
                $messages['date.date_format'] = "Date for utility bill ".($key+1)." should be yyyy-mm-dd.";
                $validator = Validator::make($value->getVars(), $rules, $messages);
            }
            if ($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }
        return $errors;
    }

    public function setUbDestinationPath($reportId)
    {
        $this->ubDestinationPath = self::UPLOAD_DESTINATION_PATH.'/'.$reportId;
    }

    public function uploadUtilityBills($ubUploadDtoList)
    {
        /*Setup upload destination path */
        $this->setubDestinationPath($ubUploadDtoList[0]->getReportId());

        /*Check if folder exist, if not, create folder
        * 0775 is a php code for "Everything for owner, read and execute for others" */
        if (!file_exists($this->ubDestinationPath)) {
            mkdir($this->ubDestinationPath, 0777, true);
            chmod($this->ubDestinationPath, 0775);
        }

        $returnList = array(
            'reportid' => $ubUploadDtoList[0]->getReportId(),
            'utilitybills' => array()
        );
        /* Set up bank statement information */
        foreach ($ubUploadDtoList as $key => $value) {
            if ($value) {
                $extension = $this->mimeTypesLibrary->mimeTypes[$value->getFileType()];
                $label = 'Utility Bill for report dated '.$value->getDate();
                $filename = $label.'.'.$extension;
                $value->setFilename($filename);
                $value->setFileRename($value->getReportId().'/'.$filename);

                /* Upload to database */
                $document = $this->documentDb->saveDocument($value->getVars());
                $value->setDocumentId($document['documentid']);
                /* Save to server folder */
                $value->getFile()->move($this->ubDestinationPath.'/',$filename);
                // file_put_contents($this->ubDestinationPath.'/'.$filename, $value->getFile());

                /* Save to dropbox */
                if(env("APP_ENV") == "Production"){
                    DropboxHelper::sendToDropbox($value->getReportId(), $this->ubDestinationPath, $filename);
                }
                
                /* Manage return value */
                $returnValue = array(
                    'id' => $value->getDocumentId(),
                    'document' => $filename,
                    'downloadlink' => route('reportdocument.download', ['reportid' => $value->getReportId(), 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $filename])
                );
                array_push($returnList['utilitybills'], $returnValue);
            }
        }
        return $returnList;
    }

    public function validateReport($reportId, $userId){
        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } elseif($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } elseif($checkReportOwner->status == 7){
            $errorMessages['errors'] = "Report is already done.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }
}