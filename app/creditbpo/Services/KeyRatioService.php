<?php

namespace CreditBPO\Services;

use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Models\Entity;
use ReadyratioKeySummary;

class KeyRatioService 
{
    private $positive, $negative;

    public function __construct() {
        $this->positive = [];
        $this->negative = [];   
    }

    public function getEntityKeySummary($entityid = null) {
        $latest_year = ReadyratioKeySummary::where('entityid', $entityid)->orderBy('id', 'desc')->first();
        if(!empty($latest_year)) {
            $year = $latest_year->Year;
            $this->getDebtRatio($latest_year->DebtRatio);
            $this->getNCAtoNW($latest_year->NCAtoNW);
            $this->getWorkigCapital($latest_year->NWC, $latest_year->InventoryNWC);
            $this->getCurrentRatio($latest_year->CurrentRatio);
            $this->getQuickRatio($latest_year->QuickRatio);
            $this->getCashRatio($latest_year->CashRatio);
            $previous_year = ReadyratioKeySummary::where('entityid', $entityid)->where('Year', ($latest_year->Year-1))->latest()->first();
            if (!empty($previous_year)) {
                $this->getEBIT($latest_year->EBIT, $previous_year->EBIT);
                $this->getComprehensiveIncome($entityid, $year);
            }
            $this->getROE($latest_year->ROE, $year);
            $this->getROA($latest_year->ROA, $year);
            $this->getNetWorth($entityid, $latest_year->NetAsset, $year);
            $this->getEquity($entityid);
        }
        return ['positive' => $this->positive, 'negative' => $this->negative];
    }

    public function getDebtRatio($value) {
        switch ($value) {
            case $value < 0.30:
                $this->positive[] = 'The debt-to-equity ratio and debt ratio demonstrate good values, but say about too cautious attitude of company to use of the borrowed capital, which is only '. ($value) * 100 . '% of the total balance of the company;';
                break;
            case $value >= 0.30 && $value <= 0.50:
                $this->positive[] = 'The debt ratio has an excellent value (' . round($value, 2) . ') caused by a low percentage of liability, (' . ($value) * 100 . '% of total capital of the company).';
                break;
            case $value > 0.50 && $value <= 0.60:
                $this->positive[] = 'The percentage of liabilities in the total balance of company is ' . ($value) * 100 . '% which is normal for stable activity.';
                break;
            case $value > 0.6 && $value <= 1:
                $this->negative[] = 'The debt ratio has an unsatisfactory value (' . round($value, 2) . ') caused by a high percentage of liability, (' . ($value) * 100 . '% of total capital of the company).';
                break;
            case $value > 1:
                $this->negative[] = 'The debt ratio has a critical value (' . round($value, 2) . ').';
                break;
        }
    }

    public function getEBIT($latest_EBIT, $previous_EBIT) {
        if($latest_EBIT > 0) {
            if($latest_EBIT > $previous_EBIT) {
                $this->positive[] = 'During the entire period reviewed, earnings before interest and taxes (EBIT) showed PHP ' . number_format($latest_EBIT) . ', at the same time, a positive dynamics compared with the previous value.';
            } else {
                $this->positive[] = 'Earnings before interest and taxes (EBIT) were PHP ' . number_format($latest_EBIT) . ' during the last year, but it compares negatively with the previous value.';
            }
        } else {
            $this->negative[] = 'During the entire period reviewed, earnings before interest and taxes (EBIT) showed PHP ' . number_format($latest_EBIT);
        }
    }

    public function getComprehensiveIncome($entityid, $latest_year) {
        $financial_report = FinancialReport::where(['entity_id' => $entityid, 'is_deleted' => 0])->first();
        if(!empty($financial_report)) {
            $latest_comprehensive_income = IncomeStatement::where('financial_report_id', $financial_report->id)->where('Year', $latest_year)->pluck('ComprehensiveIncome')->first();
            if ($latest_comprehensive_income > 0) {
                $this->positive[] = 'The income from financial and operational activities (comprehensive income) was PHP ' . number_format($latest_comprehensive_income) . ' during the year.';
            } else {
                $this->negative[] = 'The income from financial and operational activities (comprehensive income) was PHP ' . number_format($latest_comprehensive_income) . ' during the year.';
            }
        }
    }

    public function getROE($value, $latest_year) {
        switch ($value) {
            case $value < 0:
                $this->negative[] = 'Return on equity (ROE) has a critical value (' . round($value, 2) . ').';
                break;
            case $value >= 0 && $value < 0.12:
                $this->negative[] = 'Return on equity (ROE) was only ' . ($value) * 100 . '% per annum during the last year.';
                break;
            case $value >= 0.12 && $value <= 0.2:
                $this->positive[] = 'Return on equity (ROE) was ' . ($value) * 100 . '% per annum for the ' . $latest_year . ', which came as a result of a low percentage of own capital (equity).';
                break;
            case $value > 0.2:
                $this->positive[] = 'High return on equity (' . ($value) * 100 . '% per annum), which became mainly a result of low percentage of own capital(equity).';
                break;
        }
    }

    public function getCashRatio($value) {
        switch ($value) {
            case $value < 0.05:
                $this->negative[] = 'the cash ratio is ' . round($value, 2) . ' at the end of the period analyzed (a deficit of cash at hand required for current payments).';
                break;
            case $value >= 0.05 && $value < 0.20:
                $this->negative[] = 'The cash ratio is equal to ' . round($value, 2) . ' at the end of the period (a low cash at hand required for current payments).';
                break;
            case $value >= 0.20 && $value < 0.25:
                $this->positive[] = 'The cash ratio is ' . round($value, 2) . ' at the end of the period (a high cash at hand required for current payments).';
                break;
            case $value > 0.25:
                $this->positive[] = 'The cash ratio is ' . round($value, 2) . ' at the end of the period (a high cash at hand required for current payments).';
                break;
        }
    }

    public function getROA($value, $latest_year) {
        switch ($value) {
            case $value < 0:
                $this->negative[] = 'Critical return on assets, during the ' . $latest_year . '.';
                break;
            case $value >= 0 && $value < 0.06:
                $this->negative[] = 'Unsatisfactory return on assets, which was ' . ($value) * 100 . '% during the ' . $latest_year . '.';
                break;
            case $value >= 0.06 && $value < 0.1:
                $this->positive[] = 'Good return on assets, which was ' . ($value) * 100 . '% during the ' . $latest_year . '.';
                break;
            case $value > 0.1:
                $this->positive[] = 'Excellent return on assets, which was ' . ($value) * 100 . '% during the ' . $latest_year . '.';
                break;
        }
    }

    public function getNetWorth($entityid, $net_worth, $latest_year) {
        $financial_report = FinancialReport::where(['entity_id' => $entityid, 'is_deleted' => 0])->first();
        if (!empty($financial_report)) {
            $share_capital = BalanceSheet::where('financial_report_id', $financial_report->id)->where('Year', $latest_year)->pluck('IssuedCapital')->first();
            if (!empty($share_capital)) {
                if ($net_worth > $share_capital && $share_capital != 0) {
                    if(($net_worth / $share_capital) > 10) {
                        $this->positive[] = 'Net worth (net assets) of the company is much higher (by ' . round(($net_worth/$share_capital),2). ' times) than the share capital on 12/31/' . $latest_year . '.';
                    } else {
                        $this->positive[] = 'The net worth is higher than the share capital of the company.';
                    }

                } else {
                    $this->negative[] = 'Net worth is less than the share capital (the difference makes PHP ' . number_format(($share_capital - $net_worth)) . ' on the last day of the period analyzed (12/31/' . $latest_year . ')).';
                }
            }
        }
    }

    public function getCurrentRatio($value) {
        switch ($value) {
            case $value < 1:
                $this->negative[] = 'The current ratio (' . round($value, 2) . ') is significantly lower than the standard value (2).';
                break;
            case $value >= 1 && $value < 2:
                $this->negative[] = 'The current ratio (' . round($value, 2) . ') does not correspond to the normal criteria for this rate (2).';
                break;
            case $value >= 2 && $value < 2.1:
                $this->positive[] = 'The current ratio (' . round($value, 2) . ') is higher than the standard value (2).';
                break;
            case $value >= 2.1:
                $this->positive[] = 'The current ratio (' . round($value, 2) . ') completely corresponds to the standard criteria for this rate.';
                break;
        }
    }

    public function getQuickRatio($value) {
        switch ($value) {
            case $value < 0.5:
                $this->negative[] = 'Liquid assets (current assets minus inventories) are certainly not enough to meet current liabilities (quick ratio is equal to ' . round($value, 2) . ', while the acceptable value is 1).';
                break;
            case $value >= 0.5 && $value < 1:
                $this->negative[] = 'Liquid assets (current assets minus inventories) are not enough to meet current liabilities (quick ratio is equal to ' . round($value, 2) . ', while the acceptable value is 1).';
                break;
            case $value >= 1 && $value < 1.1:
                $this->positive[] = 'A good relationship between liquid assets (current assets minus inventories) and current liabilities (quick ratio is ' . round($value, 2) . ').';
                break;
            case $value >= 1.1:
                $this->positive[] = 'An outstanding relationship between liquid assets (current assets minus inventories) and current liabilities (quick ratio is ' . round($value, 2) . ').';
                break;
        }
    }

    public function getWorkigCapital($working_capital, $inventory_NWC) {
        if ($working_capital > 0) {
            if ($inventory_NWC >= 0 && $inventory_NWC <= 0.9) {
                $this->positive[] = 'Long-term resources of the financing of the company\'s activity are enough to form a normal amount of working capital which would cover the available inventories.';
            } else if($inventory_NWC > 0.9 && $inventory_NWC <= 1.0) {
                $this->positive[] = 'Working capital has a normal value in comparison with inventories owned by the company.';
            } else {
                $this->negative[] = 'available working capital does not cover inventories of the company in full.';
            }
        } else {
            $this->negative[] = 'Not enough long-term resources of financing company activity (no working capital).';
        }
    }

    public function getEquity($entityid) {
        $financial_report = FinancialReport::where(['entity_id' => $entityid, 'is_deleted' => 0])->first();
        if (!empty($financial_report)) {
            $first_year = BalanceSheet::where('financial_report_id', $financial_report->id)->orderBy('Year')->first();   
            $last_year = BalanceSheet::where('financial_report_id', $financial_report->id)->orderBy('Year', 'desc')->first();
            if(!empty($first_year) && !empty($last_year)) {
                if($last_year->Equity > $first_year->Equity) {
                    if ($last_year->Assets < $first_year->Assets) {
                        $this->positive[] = 'Equity value grew for the period reviewed (from 31 December, ' . $first_year->Year . ' to 31 December, ' . $last_year->Year . ') despite the total assets of the company reducing.';
                    } else {
                        $this->positive[] = 'The equity growth for the period analyzed (from 31 December, ' . $first_year->Year . ' to 31 December, ' . $last_year->Year . ') did not exceed the total rate of assets value growth.';
                    }
                }
            }
        }
    }

    public function getNCAtoNW($value)
    {
        switch ($value) {
            case $value < 0:
                // $this->negative[] = 'critical';
            break;
            case $value >= 0 && $value <= 1:
                $this->positive[] = 'The value of the non-current assets to net worth ratio equal to ' . round($value, 2) . ' is without doubt excellent.';
                break;
            case $value > 1 && $value <= 1.25:
                $this->positive[] = 'The value of the non-current assets to net worth ratio equal to ' . round($value, 2) . ' is good.';
                break;
            case $value > 1.25 && $value <= 2:
                // $this->negative[] = 'unsatisfactory';
                break;    
            case $value >2:
                // $this->negative[] = 'critical';
                break;
        }
    }

    public function generateKeyRatioByEntityId($entityid = null) {
        $fa_report = FinancialReport::where(['entity_id' => $entityid, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        if($fa_report == null || $fa_report == ''){
        	return;
        }

        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($entityid);

        $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
        $fa_report->income_statements   = $financialStatement['income_statements'];
        $fa_report->cashflow            = $financialStatement['cashflow'];

        // $fa_report->balance_sheets      = $fa_report
        // ->balanceSheets()
        // ->orderBy('index_count', 'asc')
        // ->get();

        // $fa_report->income_statements   = $fa_report
        // ->incomeStatements()
        // ->get();

        // $fa_report->cashflow            = $fa_report
        // ->cashFlow()
        // ->orderBy('index_count', 'asc')
        // ->get();

        for ($x = 2; $x >= 0; $x--) {
            $intangible_asset = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->Goodwill + $fa_report->balance_sheets[$x]->IntangibleAssetsOtherThanGoodwill) : 0;
            $equity = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Equity : 0;
            $net_tangible_asset = $equity - $intangible_asset;
            $noncurrent_liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentLiabilities : 0;
            $current_liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentLiabilities : 0;
            $liabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Liabilities : 0;
            $assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Assets : 0;
            $current_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentAssets : 0;
            $noncurrent_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentAssets : 0;
            $fixed_asset = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->PropertyPlantAndEquipment + $fa_report->balance_sheets[$x]->InvestmentProperty +  $fa_report->balance_sheets[$x]->NoncurrentBiologicalAssets) : 0;
            $NWC = $current_assets - $current_liabilities;
            $inventories = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Inventories : 0;
            $WCD = $NWC - $inventories;
            $cash_and_cash_equivalents = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CashAndCashEquivalents : 0;
            $other_current_financial_assets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->OtherCurrentFinancialAssets : 0;
            $trade_and_other_current_receivables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentReceivables : 0;
            $retained_earnings = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->RetainedEarnings : 0;

            $profit_loss_before_tax = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ProfitLossBeforeTax : 0;
            $gross_profit = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->GrossProfit : 0;
            $finance_costs = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->FinanceCosts : 0;
            $depreciation_and_amortisation_expense = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->DepreciationAndAmortisationExpense : 0;
            $revenue = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->Revenue : 0;
            $profit_loss = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ProfitLoss : 0;
            $finance_costs = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->FinanceCosts : 0;
            $comprehensive_income = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->ComprehensiveIncome : 0;
            $cost_of_sales = isset($fa_report->income_statements[$x]) ? $fa_report->income_statements[$x]->CostOfSales : 0;

            $OIEeFC = $profit_loss_before_tax - ($gross_profit + $finance_costs);
            $EBIT = $profit_loss_before_tax + $finance_costs;
            $EBITDA = $EBIT + $depreciation_and_amortisation_expense;

            if (0 != $equity
            ) {
                $financial_leverage = $liabilities / $equity;
                $LTD_to_E = $noncurrent_liabilities / $equity;
                $NCA_to_NW = $noncurrent_assets / $equity;
                $FA_to_NW = $fixed_asset / $equity;
            } else {
                $financial_leverage = 0;
                $LTD_to_E = 0;
                $NCA_to_NW = 0;
                $FA_to_NW = 0;
            }

            if (0 != $assets
            ) {
                $debt_ratio = $liabilities / $assets;
            } else {
                $debt_ratio = 0;
            }

            if (0 != $liabilities
            ) {
                $c_liability_ratio = $current_liabilities / $liabilities;
            } else {
                $c_liability_ratio = 0;
            }

            if (0 != $noncurrent_liabilities && 0 != $equity
            ) {
                $capitalization = $noncurrent_liabilities / ($noncurrent_liabilities + $equity);
            } else {
                $capitalization = 0;
            }

            if (0 != $NWC
            ) {
                $inventory_NWC = $inventories / $NWC;
            } else {
                $inventory_NWC = 0;
            }

            if (0 != $current_liabilities
            ) {
                $current_ratio = $current_assets / $current_liabilities;
                $quick_ratio = ($cash_and_cash_equivalents + $other_current_financial_assets + $trade_and_other_current_receivables) / $current_liabilities;
                $cash_ratio = $cash_and_cash_equivalents / $current_liabilities;
            } else {
                $current_ratio = 0;
                $quick_ratio = 0;
                $cash_ratio = 0;
            }

            if (0 != $revenue
            ) {
                $gross_margin = $gross_profit / $revenue;
                $ROS = $EBIT / $revenue;
                $profit_margin = $profit_loss / $revenue;
            } else {
                $gross_margin = 0;
                $ROS = 0;
                $profit_margin = 0;
            }

            if (0 != $finance_costs
            ) {
                $ICR = $EBIT / $finance_costs;
            } else {
                $ICR = 0;
            }

            $beginningEquity = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Equity : 0;
            $endEquity = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Equity : 0;
            if ((0 == $beginningEquity) && (0 == $endEquity)) {
                $ROE = 0;
                $ROE_CI = 0;
            } else {
                $ROE = $profit_loss / (($beginningEquity + $endEquity) / 2);
                $ROE_CI = $comprehensive_income / (($beginningEquity + $endEquity) / 2);
            }

            $beginningAssets = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Assets : 0;
            $endAssets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Assets : 0;
            if ((0 == $beginningAssets) && (0 == $endAssets)) {
                $ROA = 0;
                $ROA_CI = 0;
            } else {
                $ROA = $profit_loss / (($beginningAssets + $endAssets) / 2);
                $ROA_CI = $comprehensive_income / (($beginningAssets + $endAssets) / 2);
            }

            $beginningNoncurrentLiabilities = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->NoncurrentLiabilities : 0;
            $endNoncurrentLiabilities = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->NoncurrentLiabilities : 0;

            if ((0 != $beginningEquity) || (0 != $beginningNoncurrentLiabilities) || (0 != $endEquity) || (0 != $endNoncurrentLiabilities)) {
                $ROCE = $EBIT / (($beginningEquity + $beginningNoncurrentLiabilities + $endEquity + $endNoncurrentLiabilities) / 2);
            } else {
                $ROCE = 0;
            }

            $beginningTradeAndOtherCurrentReceivables = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->TradeAndOtherCurrentReceivables : 0;
            $endTradeAndOtherCurrentReceivables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentReceivables : 0;
            if ($revenue > 0) {
                $receivables_turnover = (($beginningTradeAndOtherCurrentReceivables + $endTradeAndOtherCurrentReceivables) / 2) / ($revenue / 365);
            } else {
                $receivables_turnover = 0;
            }

            $beginningTradeAndOtherCurrentPayables = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->TradeAndOtherCurrentPayables : 0;
            $endTradeAndOtherCurrentPayables = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->TradeAndOtherCurrentPayables : 0;
            $beginningCurrentProvisionsForEmployeeBenefits = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->CurrentProvisionsForEmployeeBenefits : 0;
            $endCurrentProvisionsForEmployeeBenefits = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentProvisionsForEmployeeBenefits : 0;
            $beginningInventories = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->Inventories : 0;
            $endInventories = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->Inventories : 0;

            $totalTAOCP = $beginningTradeAndOtherCurrentPayables + $endTradeAndOtherCurrentPayables;
            $totalCPFEB = $beginningCurrentProvisionsForEmployeeBenefits + $endCurrentProvisionsForEmployeeBenefits;
            $totalBelow = $cost_of_sales + $endInventories - $beginningInventories;
            if ($totalBelow > 0) {
                $payable_turnover = (($totalTAOCP + $totalCPFEB) / 2) / ($totalBelow / 365);
            } else {
                $payable_turnover = 0;
            }

            if ($cost_of_sales > 0) {
                $inventory_turnover = (($beginningInventories + $endInventories) / 2) / ($cost_of_sales / 365);
            } else {
                $inventory_turnover = 0;
            }

            if ($revenue > 0) {
                $asset_turnover = (($beginningAssets + $endAssets) / 2) / ($revenue / 365);
            } else {
                $asset_turnover = 0;
            }

            $beginningCurrentAssets = isset($fa_report->balance_sheets[$x + 1]) ? $fa_report->balance_sheets[$x + 1]->CurrentAssets : 0;
            $endCurrentAssets = isset($fa_report->balance_sheets[$x]) ? $fa_report->balance_sheets[$x]->CurrentAssets : 0;

            if ($revenue > 0) {
                $CAsset_turnover = (($beginningCurrentAssets + $endCurrentAssets) / 2) / ($revenue / 365);
            } else {
                $CAsset_turnover = 0;
            }

            if ($revenue > 0) {
                $capital_turnover = (($beginningEquity + $endEquity) / 2) / ($revenue / 365);
            } else {
                $capital_turnover = 0;
            }

            $CCC = ($inventory_turnover + $receivables_turnover) - $payable_turnover;

            if (0 != $assets
            ) {
                $T1 = ($current_assets - $current_liabilities) / $assets;
                $T2 = $retained_earnings / $assets;
                $T3 = $EBIT / $assets;
                $T5 = $revenue / $assets;
            } else {
                $T1 = 0;
                $T2 = 0;
                $T3 = 0;
                $T5 = 0;
            }
            if (0 != $liabilities
            ) {
                $T4 = $equity / $liabilities;
            } else {
                $T4 = 0;
            }

            $readyratioKeySummary = new ReadyratioKeySummary();
            $readyratioKeySummary->entityid = $entityid;
            $readyratioKeySummary->Year = isset($fa_report->balance_sheets[$x]) ? ($fa_report->balance_sheets[$x]->Year) : 0;
            $readyratioKeySummary->IntangibleAsset = !empty($intangible_asset) ? round($intangible_asset, 3) : 0;
            $readyratioKeySummary->NetTangibleAsset = !empty($net_tangible_asset) ? round($net_tangible_asset, 3) : 0;
            $readyratioKeySummary->NetAsset = !empty($equity) ? round($equity, 3) : 0;
            $readyratioKeySummary->FinancialLeverage = !empty($financial_leverage) ? round($financial_leverage, 3) : 0;
            $readyratioKeySummary->DebtRatio = !empty($debt_ratio) ? round($debt_ratio, 3) : 0;
            $readyratioKeySummary->LTDtoE = !empty($LTD_to_E) ? round($LTD_to_E, 3) : 0;
            $readyratioKeySummary->NCAtoNW = !empty($NCA_to_NW) ? round($NCA_to_NW, 3) : 0;
            $readyratioKeySummary->FixedAsset = !empty($fixed_asset) ? round($fixed_asset, 3) : 0;
            $readyratioKeySummary->FAtoNW = !empty($FA_to_NW) ? round($FA_to_NW, 3) : 0;
            $readyratioKeySummary->CLiabilityRatio = !empty($c_liability_ratio) ? round($c_liability_ratio, 3) : 0;
            $readyratioKeySummary->Capitalization = !empty($capitalization) ? round($capitalization, 3) : 0;
            $readyratioKeySummary->NWC = !empty($NWC) ? round($NWC, 3) : 0;
            $readyratioKeySummary->WCD = !empty($WCD) ? round($WCD, 3) : 0;
            $readyratioKeySummary->InventoryNWC = !empty($inventory_NWC) ? round($inventory_NWC, 3) : 0;
            $readyratioKeySummary->CurrentRatio = !empty($current_ratio) ? round($current_ratio, 3) : 0;
            $readyratioKeySummary->QuickRatio = !empty($quick_ratio) ? round($quick_ratio, 3) : 0;
            $readyratioKeySummary->CashRatio = !empty($cash_ratio) ? round($cash_ratio, 3) : 0;
            $readyratioKeySummary->OIEeFC = !empty($OIEeFC) ? round($OIEeFC, 3) : 0;
            $readyratioKeySummary->EBIT = !empty($EBIT) ? round($EBIT, 3) : 0;
            $readyratioKeySummary->EBITDA = !empty($EBITDA) ? round($EBITDA, 3) : 0;
            $readyratioKeySummary->GrossMargin = !empty($gross_margin) ? round($gross_margin, 3) : 0;
            $readyratioKeySummary->ROS = !empty($ROS) ? round($ROS, 3) : 0;
            $readyratioKeySummary->ProfitMargin = !empty($profit_margin) ? round($profit_margin, 3) : 0;
            $readyratioKeySummary->ICR = !empty($ICR) ? round($ICR, 3) : 0;
            $readyratioKeySummary->ROE = !empty($ROE) ? round($ROE, 3) : 0;
            $readyratioKeySummary->ROE_CI = !empty($ROE_CI) ? round($ROE_CI, 3) : 0;
            $readyratioKeySummary->ROA = !empty($ROA) ? round($ROA, 3) : 0;
            $readyratioKeySummary->ROA_CI = !empty($ROA_CI) ? round($ROA_CI, 3) : 0;
            $readyratioKeySummary->ROCE = !empty($ROCE) ? round($ROCE, 3) : 0;
            $readyratioKeySummary->ReceivablesTurnover = !empty($receivablesTurnover) ? round($receivablesTurnover, 3) : 0;
            $readyratioKeySummary->PayableTurnover = !empty($payableTurnover) ? round($payableTurnover, 3) : 0;
            $readyratioKeySummary->InventoryTurnover = !empty($inventoryTurnover) ? round($inventoryTurnover, 3) : 0;
            $readyratioKeySummary->AssetTurnover = !empty($asset_turnover) ? round($asset_turnover, 3) : 0;
            $readyratioKeySummary->CAssetTurnover = !empty($CAsset_turnover) ? round($CAsset_turnover, 3) : 0;
            $readyratioKeySummary->CapitalTurnover = !empty($capital_turnover) ? round($capital_turnover, 3) : 0;
            $readyratioKeySummary->CCC = !empty($CCC) ? round($CCC, 3) : 0;
            $readyratioKeySummary->T1 = !empty($T1) ? round($T1, 3) : 0;
            $readyratioKeySummary->T2 = !empty($T2) ? round($T2, 3) : 0;
            $readyratioKeySummary->T3 = !empty($T3) ? round($T3, 3) : 0;
            $readyratioKeySummary->T4 = !empty($T4) ? round($T4, 3) : 0;
            $readyratioKeySummary->T5 = !empty($T5) ? round($T5, 3) : 0;
            $readyratioKeySummary->save();
        }
        
        $user = Entity::where('entityid', $entityid)->first();
        if($user->is_premium != SIMPLIFIED_REPORT){
            return;
        }else{
            return $this->getEntityKeySummary($entityid);  
        }  
    }
}