<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Insurance;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\InsuranceDTO;

class InsuranceCRUD
{
    public function __construct()
    {
        $this->insuranceDb = new Insurance();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateInsuranceDetails($insurance)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'insurancetype.required' => 'Insurance Type is required.',
            'insuredamount.required' => 'Insurance Amount is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'insurancetype' => 'required',
            'insuredamount' => 'required'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($insurance, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function insuranceInsert($insuranceDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $insuranceDTO->reportid,
        	'insurance_type' => $insuranceDTO->insurancetype,
        	'insured_amount' => $insuranceDTO->insuredamount
        ];

        $insuranceDTOFinal = new InsuranceDTO();
        $insuranceDTOFinal->setVars((array) $insuranceDTO);
        $insertResponse = $this->insuranceDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$insuranceDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $insuranceDTOFinal;
    }

    public function insuranceUpdate($insuranceDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $insuranceDTO->reportid,
        	'insurance_type' => $insuranceDTO->insurancetype,
        	'insured_amount' => $insuranceDTO->insuredamount
        ];

        $insuranceDTOFinal = new InsuranceDTO();
        $insuranceDTOFinal->setVars((array) $insuranceDTO);
        $udpateResponse = $this->insuranceDb->where('insuranceid',$insuranceDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$insuranceDTOFinal->setId($insuranceDTO->id);
        }

        /* Convert database value to readable response */
        return $insuranceDTOFinal;
    }

    public function insuranceDelete($insuranceid)
    {   
       $deleteResponse = $this->insuranceDb->where('insuranceid',$insuranceid)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getInsuranceByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report affiliates/subsidiaries in tblrelatedcompanies table */
        $insuranceList = $this->insuranceDb->getInsuranceByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $insuranceDtoList = [];
        foreach($insuranceList as $key => $value){
            $insuranceDto = new InsuranceDTO();            
            $insuranceDto->setVars($insuranceList[$key]);
            array_push($insuranceDtoList, $insuranceDto->getVars());
        }
        
        $results['total'] = $this->insuranceDb->getInsuranceByReportId($reportId)->count();  
        
        $results['items'] = $insuranceDtoList;
        return $results;
    }

    public function getInsuranceByReportId($reportId)
    {
    	/* Get report Insurance in tblrelatedcompanies table */
        $insurances = $this->insuranceDb->getInsuranceByReportId($reportId);

        $results = [];
        if (!empty($insurances)){
        	$results['items'] = [];
        	foreach ($insurances as $insurance) {
        		$insuranceDto = new InsuranceDTO;
        		$insuranceDto->setVars($insurance);
        		$results['items'][] = $insuranceDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getInsuranceById($relatedCompaniesId)
    {
        /* Get report Insurance in tblmajorcustomer table */
        $insurance = $this->insuranceDb->getInsuranceById($relatedCompaniesId);
        if (!empty($insurance)){
            $insuranceDto = new InsuranceDTO;
            $insuranceDto->setVars($insurance);

            return $insuranceDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}