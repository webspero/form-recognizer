<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\KeyManager;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\KeyManagersDTO;

class KeyManagersCRUD
{
    public function __construct()
    {
        $this->keyManagersDb = new KeyManager();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateKeyManagersDetails($keyManagers)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'firstname.required' => 'Company Name is required.',
            'middlename.required' => 'Company Address is required.',
            'lastname.required' => 'Company Phone is required.',
            'nationality.required' => 'Company Email is required.',
            'birthdate.required' => 'Company Email must be a valid email address.',
            'civilstatus.required' => 'Company Email must be a valid email address.',
            'profession.required' => 'Company Email must be a valid email address.',
            'email.required' => 'Company Email must be a valid email address.',
            'address1.required' => 'Company Email must be a valid email address.',
            'tinnum.required' => 'Company Email must be a valid email address.',
            'percentofownership.required' => 'Company Email must be a valid email address.',
            'numberofyearengaged.required' => 'Company Email must be a valid email address.',
            'position.required' => 'Company Email must be a valid email address.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
            'nationality' => 'required',
            'birthdate' => 'required',
            'civilstatus' => 'required',
            'profession' => 'required',
            'email' => 'required|email',
            'address1' => 'required',
            'tinnum' => 'required',
            'percentofownership' => 'required',
            'numberofyearengaged' => 'required',
            'position' => 'required',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($keyManagers, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function keyManagersInsert($keyManagersDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $keyManagersDTO->reportid,
        	'firstname' => $keyManagersDTO->firstname,
        	'middlename' => $keyManagersDTO->middlename,
        	'lastname' => $keyManagersDTO->lastname,
        	'nationality' => $keyManagersDTO->nationality,
        	'birthdate' => $keyManagersDTO->birthdate,
        	'civilstatus' => $keyManagersDTO->civilstatus,
        	'profession' => $keyManagersDTO->profession,
        	'email' => $keyManagersDTO->email,
        	'address1' => $keyManagersDTO->address1,
        	'tin_num' => $keyManagersDTO->tinnum,
        	'percent_of_ownership' => $keyManagersDTO->percentofownership,
        	'number_of_year_engaged' => $keyManagersDTO->percentofownership,
        	'position' => $keyManagersDTO->position
        ];

        $keyManagersDTOFinal = new KeyManagersDTO();
        $keyManagersDTOFinal->setVars((array) $keyManagersDTO);
        $insertResponse = $this->keyManagersDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$keyManagersDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $keyManagersDTOFinal;
    }

    public function keyManagersUpdate($keyManagersDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $keyManagersDTO->reportid,
        	'firstname' => $keyManagersDTO->firstname,
        	'middlename' => $keyManagersDTO->middlename,
        	'lastname' => $keyManagersDTO->lastname,
        	'nationality' => $keyManagersDTO->nationality,
        	'birthdate' => $keyManagersDTO->birthdate,
        	'civilstatus' => $keyManagersDTO->civilstatus,
        	'profession' => $keyManagersDTO->profession,
        	'email' => $keyManagersDTO->email,
        	'address1' => $keyManagersDTO->address1,
        	'tin_num' => $keyManagersDTO->tinnum,
        	'percent_of_ownership' => $keyManagersDTO->percentofownership,
        	'number_of_year_engaged' => $keyManagersDTO->percentofownership,
        	'position' => $keyManagersDTO->position
        ];

        $keyManagersDTOFinal = new  KeyManagersDTO();
        $keyManagersDTOFinal->setVars((array) $keyManagersDTO);
        $udpateResponse = $this->keyManagersDb->where('keymanagerid',$keyManagersDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$keyManagersDTOFinal->setId($keyManagersDTO->id);
        }

        /* Convert database value to readable response */
        return $keyManagersDTOFinal;
    }

    public function keyManagersDelete($keyManagerId)
    {   
       $deleteResponse = $this->keyManagersDb->where('keymanagerid',$keyManagerId)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getKeyManagersByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report Key Managers in tblrelatedcompanies table */
        $keyManagersList = $this->keyManagersDb->getKeyManagersByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $keyManagersDtoList = [];
        foreach($keyManagersList as $key => $value){
            $keyManagersDto = new KeyManagersDTO();            
            $keyManagersDto->setVars($keyManagersList[$key]);
            array_push($keyManagersDtoList, $keyManagersDto->getVars());
        }
        
        $results['total'] = $this->keyManagersDb->getKeyManagersByReportId($reportId)->count();  
        
        $results['items'] = $keyManagersDtoList;
        return $results;
    }

    public function getKeyManagersByReportId($reportId)
    {
    	/* Get report Key Managers in tblrelatedcompanies table */
        $keyManagers = $this->keyManagersDb->getKeyManagersByReportId($reportId);

        $results = [];
        if (!empty($keyManagers)){

        	$results['items'] = [];
        	foreach ($keyManagers as $keyManager) {
        		$keyManagersDto = new KeyManagersDTO;
        		$keyManagersDto->setVars($keyManager);
        		$results['items'][] = $keyManagersDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getKeyManagersById($relatedCompaniesId)
    {
        /* Get report Key Managers in tblmajorcustomer table */
        $keyManagers = $this->keyManagersDb->getKeyManagersById($relatedCompaniesId);
        if (!empty($keyManagers)){
            $keyManagersDto = new KeyManagersDTO;
            $keyManagersDto->setVars($keyManagers);

            return $keyManagersDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}