<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Director;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\DirectorsDTO;

class DirectorsCRUD
{
    public function __construct()
    {
        $this->directorsDb = new Director();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateDirectorsDetails($directors)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'firstname.required' => 'Firstname is required.',
            'middlename.required' => 'Middlename is required.',
            'lastname.required' => 'Lastname is required.',
            'birthdate.required' => 'Birthdate is required.',
            'tinno.required' => 'Tin no is required.',
            'address.required' => 'Address is required.',
            'nationality.required' => 'Nationality is required.',
            'percentageshare.required' => 'Percentage Share is required.',
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
            'birthdate' => 'required',
            'tinno' => 'required',
            'address' => 'required',
            'nationality' => 'required',
            'percentageshare' => 'required'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($directors, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function directorsInsert($directorsDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $directorsDTO->reportid,
        	'firstname' => $directorsDTO->firstname,
        	'middlename' => $directorsDTO->middlename,
        	'lastname' => $directorsDTO->lastname,
        	'birthdate' => $directorsDTO->birthdate,
        	'id_no' => $directorsDTO->tinno,
        	'address' => $directorsDTO->address,
        	'nationality' => $directorsDTO->nationality,
        	'percentage_share' => $directorsDTO->percentageshare
        ];

        $directorsDTOFinal = new DirectorsDTO();
        $directorsDTOFinal->setVars((array) $directorsDTO);
        $insertResponse = $this->directorsDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$directorsDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $directorsDTOFinal;
    }

    public function directorsUpdate($directorsDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $directorsDTO->reportid,
        	'firstname' => $directorsDTO->firstname,
        	'middlename' => $directorsDTO->middlename,
        	'lastname' => $directorsDTO->lastname,
        	'birthdate' => $directorsDTO->birthdate,
        	'id_no' => $directorsDTO->tinno,
        	'address' => $directorsDTO->address,
        	'nationality' => $directorsDTO->nationality,
        	'percentage_share' => $directorsDTO->percentageshare
        ];

        $directorsDTOFinal = new DirectorsDTO();
        $directorsDTOFinal->setVars((array) $directorsDTO);
        $udpateResponse = $this->directorsDb->where('id',$directorsDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$directorsDTOFinal->setId($directorsDTO->id);
        }

        /* Convert database value to readable response */
        return $directorsDTOFinal;
    }

    public function directorsDelete($id)
    {   
       $deleteResponse = $this->directorsDb->where('id',$id)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getDirectorsByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report affiliates/subsidiaries in tblrelatedcompanies table */
        $directorsList = $this->directorsDb->getDirectorsByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $directorsDtoList = [];
        foreach($directorsList as $key => $value){
            $directorsDto = new directorsDTO();            
            $directorsDto->setVars($directorsList[$key]);
            array_push($directorsDtoList, $directorsDto->getVars());
        }
        
        $results['total'] = $this->directorsDb->getDirectorsByReportId($reportId)->count();  
        
        $results['items'] = $directorsDtoList;
        return $results;
    }

    public function getDirectorsByReportId($reportId)
    {
    	/* Get report Affiliate Subsidiaries in tblrelatedcompanies table */
        $directors = $this->directorsDb->getDirectorsByReportId($reportId);

        $results = [];
        if (!empty($directors)){
        	$results['items'] = [];
        	foreach ($directors as $director) {
        		$directorsDto = new directorsDTO;
        		$directorsDto->setVars($director);
        		$results['items'][] = $directorsDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getDirectorsById($relatedCompaniesId)
    {
        /* Get report directors in tblmajorcustomer table */
        $directors = $this->directorsDb->getDirectorsById($relatedCompaniesId);
        if (!empty($directors)){
            $directorsDto = new DirectorsDTO;
            $directorsDto->setVars($directors);

            return $directorsDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}