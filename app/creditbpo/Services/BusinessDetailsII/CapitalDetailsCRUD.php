<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Capital;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\CapitalDetailsDTO;

class CapitalDetailsCRUD
{
    public function __construct()
    {
        $this->capitalDetailsDb = new Capital();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateCapitalDetails($capitalDetails)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'authorizedcapital.required' => 'Authorized Capital is required.',
            'issuedcapital.required' => 'Issued Capital is required.',
            'paidupcapital.required' => 'Paid Up Capital is required.',
            'ordinaryshares.required' => 'Ordinary Shares is required.',
            'parvalue.required' => 'Par Value is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'authorizedcapital' => 'required',
            'issuedcapital' => 'required',
            'paidupcapital' => 'required',
            'ordinaryshares' => 'required',
            'parvalue' => 'required',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($capitalDetails, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }

        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function capitalDetailsInsert($capitalDetailsDTO)
    {   

        /* Save data to database */
        $cd_data = [
        	'entity_id' => $capitalDetailsDTO->reportid,
        	'capital_authorized' => $capitalDetailsDTO->authorizedcapital,
        	'capital_issued' => $capitalDetailsDTO->issuedcapital,
        	'capital_paid_up' => $capitalDetailsDTO->paidupcapital,
        	'capital_ordinary_shares' => $capitalDetailsDTO->ordinaryshares,
        	'capital_par_value' => $capitalDetailsDTO->parvalue,
        ];

        $capitalDetailsDTOFinal = new CapitalDetailsDTO();
        $capitalDetailsDTOFinal->setVars((array) $capitalDetailsDTO);
        $insertResponse = $this->capitalDetailsDb->insertGetId($cd_data);

        if (!empty($insertResponse)){
        	$capitalDetailsDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $capitalDetailsDTOFinal;
    }

    public function capitalDetailsUpdate($capitalDetailsDTO)
    {   
        /* Save data to database */
        $cd_data = [
        	'entity_id' => $capitalDetailsDTO->reportid,
        	'capital_authorized' => $capitalDetailsDTO->authorizedcapital,
        	'capital_issued' => $capitalDetailsDTO->issuedcapital,
        	'capital_paid_up' => $capitalDetailsDTO->paidupcapital,
        	'capital_ordinary_shares' => $capitalDetailsDTO->ordinaryshares,
        	'capital_par_value' => $capitalDetailsDTO->parvalue,
        ];

        $capitalDetailsDTOFinal = new CapitalDetailsDTO();
        $capitalDetailsDTOFinal->setVars((array) $capitalDetailsDTO);
        $udpateResponse = $this->capitalDetailsDb->where('capitalid',$capitalDetailsDTO->id)->update($cd_data);

        if (!empty($udpateResponse)){
        	$capitalDetailsDTOFinal->setId($capitalDetailsDTO->id);
        }

        /* Convert database value to readable response */
        return $capitalDetailsDTOFinal;
    }

    public function capitalDetailsDelete($capitalid)
    {   
       $deleteResponse = $this->capitalDetailsDb->where('capitalid',$capitalid)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getCapitalDetailsByPage($reportId, $page, $limit)
    {
    	
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report Capital Details in tblrelatedcompanies table */
        $affiliateSubsidiariesList = $this->capitalDetailsDb->getCapitalDetailsByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $capitalDetailsDtoList = [];
        foreach($capitalDetailsList as $key => $value){
            $capitalDetailsDto = new CapitalDetailsDTO();            
            $capitalDetailsDto->setVars($capitalDetailsList[$key]);
            array_push($capitalDetailsDtoList, $capitalDetailsDto->getVars());
        }
        
        $results['total'] = $this->capitalDetailsDb->getCapitalDetailsByReportId($reportId)->count();  
        
        $results['items'] = $capitalDetailsList;
        return $results;
    }

    public function getCapitalDetailsByReportId($reportId)
    {
    	/* Get report Affiliate Subsidiaries in tblcapital table */
        $capitalDetails = $this->capitalDetailsDb->getCapitalDetailsByReportId($reportId);

        $results = [];
        if (!empty($capitalDetails)){
        	$results['items'] = [];
        	foreach ($capitalDetails as $capitalDetail) {
        		$capitalDetailsDto = new CapitalDetailsDTO;
        		$capitalDetailsDto->setVars($capitalDetail);
        		$results['items'][] = $capitalDetailsDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getCapitalDetailsById($capitalid)
    {
        /* Get report capitalDetailsDto in tblmajorcustomer table */
        $capitalDetails = $this->capitalDetailsDb->getCapitalDetailsById($capitalid);
        if (!empty($capitalDetails)){
            $capitalDetailsDto = new CapitalDetailsDTO;
            $capitalDetailsDto->setVars($capitalDetails);

            return $capitalDetailsDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

    public function checkCapitalDetailsIfExist($reportId)
    {
        $report = $this->capitalDetailsDb->getCapitalDetailsByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}