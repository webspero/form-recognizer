<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\RelatedCompany;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\AffiliateSubsidiariesDTO;

class AffiliatesSubsidiariesCRUD
{
    public function __construct()
    {
        $this->AffiliateSubsidiariesDb = new RelatedCompany();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateAffiliatesSubsidiariesDetails($affialteSubsidiaries)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'companyname.required' => 'Company Name is required.',
            'companyaddress.required' => 'Company Address is required.',
            'companyphone.required' => 'Company Phone is required.',
            'companyemail.required' => 'Company Email is required.',
            'companyemail.email' => 'Company Email must be a valid email address.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'companyname' => 'required',
            'companyaddress' => 'required',
            'companyphone' => 'required',
            'companyemail' => 'required|email',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($affialteSubsidiaries, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function affiliateSubsidiariesInsert($affiliatesSubsidiariesDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $affiliatesSubsidiariesDTO->reportid,
        	'related_company_name' => $affiliatesSubsidiariesDTO->companyname,
        	'related_company_address1' => $affiliatesSubsidiariesDTO->companyaddress,
        	'related_company_phone' => $affiliatesSubsidiariesDTO->companyphone,
        	'related_company_email' => $affiliatesSubsidiariesDTO->companyemail
        ];

        $affiliatesSubsidiariesDTOFinal = new AffiliateSubsidiariesDTO();
        $affiliatesSubsidiariesDTOFinal->setVars((array) $affiliatesSubsidiariesDTO);
        $insertResponse = $this->AffiliateSubsidiariesDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$affiliatesSubsidiariesDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $affiliatesSubsidiariesDTOFinal;
    }

    public function affiliateSubsidiariesUpdate($affiliatesSubsidiariesDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $affiliatesSubsidiariesDTO->reportid,
        	'related_company_name' => $affiliatesSubsidiariesDTO->companyname,
        	'related_company_address1' => $affiliatesSubsidiariesDTO->companyaddress,
        	'related_company_phone' => $affiliatesSubsidiariesDTO->companyphone,
        	'related_company_email' => $affiliatesSubsidiariesDTO->companyemail
        ];

        $affiliatesSubsidiariesDTOFinal = new AffiliateSubsidiariesDTO();
        $affiliatesSubsidiariesDTOFinal->setVars((array) $affiliatesSubsidiariesDTO);
        $udpateResponse = $this->AffiliateSubsidiariesDb->where('relatedcompaniesid',$affiliatesSubsidiariesDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$affiliatesSubsidiariesDTOFinal->setId($affiliatesSubsidiariesDTO->id);
        }

        /* Convert database value to readable response */
        return $affiliatesSubsidiariesDTOFinal;
    }

    public function affiliateSubsidiariesDelete($relatedcompaniesid)
    {   
       $deleteResponse = $this->AffiliateSubsidiariesDb->where('relatedcompaniesid',$relatedcompaniesid)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getAffiliateSubsidiariesByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report affiliates/subsidiaries in tblrelatedcompanies table */
        $affiliateSubsidiariesList = $this->AffiliateSubsidiariesDb->getAffiliateSubsidiariesByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $affiliateSubsidiariesDtoList = [];
        foreach($AffiliateSubsidiariesList as $key => $value){
            $AffiliateSubsidiariesDto = new AffiliateSubsidiariesDTO();            
            $AffiliateSubsidiariesDto->setVars($affiliateSubsidiariesList[$key]);
            array_push($affiliateSubsidiariesDtoList, $AffiliateSubsidiariesDto->getVars());
        }
        
        $results['total'] = $this->AffiliateSubsidiariesDb->getAffiliateSubsidiariesByReportId($reportId)->count();  
        
        $results['items'] = $affiliateSubsidiariesDtoList;
        return $results;
    }

    public function getAffiliateSubsidiariesByReportId($reportId)
    {
    	/* Get report Affiliate Subsidiaries in tblrelatedcompanies table */
        $affiliateSubsidiaries = $this->AffiliateSubsidiariesDb->getAffiliateSubsidiariesByReportId($reportId);

        $results = [];
        if (!empty($affiliateSubsidiaries)){

        	$results['items'] = [];
        	foreach ($affiliateSubsidiaries as $affiliateSubsidiary) {
        		$affiliateSubsidiariesDto = new AffiliateSubsidiariesDTO;
        		$affiliateSubsidiariesDto->setVars($affiliateSubsidiary);
        		$results['items'][] = $affiliateSubsidiariesDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getAffiliateSubsidiariesById($relatedCompaniesId)
    {
        /* Get report Affiliate SubsidiariesDto in tblmajorcustomer table */
        $affiliateSubsidiaries = $this->AffiliateSubsidiariesDb->getAffiliateSubsidiariesById($relatedCompaniesId);
        //print_r($affiliateSubsidiaries); exit;
        if (!empty($affiliateSubsidiaries)){
            $affiliateSubsidiariesDto = new AffiliateSubsidiariesDTO;
            $affiliateSubsidiariesDto->setVars($affiliateSubsidiaries);

            return $affiliateSubsidiariesDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}