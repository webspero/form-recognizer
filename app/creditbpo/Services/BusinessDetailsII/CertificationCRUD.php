<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Certification;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\CertificationDTO;

class CertificationCRUD
{
    public function __construct()
    {
        $this->certificationDb = new Certification();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateCertification($certification)
    {

        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'certification.required' => 'Certification is required.',
            'registrationno.required' => 'Registration No is required.',
            'registrationdate.required' => 'Registration Date is required.',
            'uploadfile.mimes' => 'Upload File file type should be doc,docx,pdf,jpeg,png,xls,xlsx.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'certification' => 'required',
            'registrationno' => 'required',
            'registrationdate' => 'required',
            'uploadfile' => 'mimes:doc,docx,pdf,jpeg,png,xls,xlsx'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($certification, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function certificationInsert($certificationDTO)
    {   
        /* Save data to database */
        $cd_data = [
        	'entity_id' => $certificationDTO['reportid'],
        	'certification_details' => $certificationDTO['certification'],
        	'certification_reg_no' => $certificationDTO['registrationno'],
        	'certification_reg_date' => $certificationDTO['registrationdate'],
        	
        ];

        $uploadFile = request()->file('uploadfile');
        $extension = $uploadFile->extension();

        if($uploadFile){
	        $filename = 'cert-doc-'.$certificationDTO['reportid'].'-'.uniqid().'.'.$extension;
	        
	        if($uploadFile->move(public_path('certification-docs'), $filename)){
				
	        	$certificationDTO['uploadfile'] = $filename;
	        	$cd_data['certification_doc'] = $certificationDTO['uploadfile'];
	        }
    	}

        $certificationDTOFinal = new CertificationDTO();
        $certificationDTOFinal->setVars($certificationDTO);
        $insertResponse = $this->certificationDb->insertGetId($cd_data);

        if (!empty($insertResponse)){
        	$certificationDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $certificationDTOFinal;
    }

    public function certificationUpdate($certificationDTO)
    {   
    	// print_r($certificationDTO); exit;
        /* Save data to database */
        $cd_data = [
        	'entity_id' => $certificationDTO->reportid,
        	'certification_details' => $certificationDTO->certification,
        	'certification_reg_no' => $certificationDTO->registrationno,
        	'certification_reg_date' => $certificationDTO->registrationdate,
        ];

        $uploadFile = request()->file('uploadfile');
        $extension = $uploadFile->extension();

        if($uploadFile){
	        $filename = 'cert-doc-'.$certificationDTO->reportid.'-'.uniqid().'.'.$extension;
	        
	        if($uploadFile->move(public_path('certification-docs'), $filename)){
				
	        	$certificationDTO->uploadfile = $filename;
	        	$cd_data['certification_doc'] = $certificationDTO->uploadfile;
	        }
    	}

        $certificationDTOFinal = new CertificationDTO();
        $certificationDTOFinal->setVars((array) $certificationDTO);
        
        $udpateResponse = $this->certificationDb->where('certificationid',$certificationDTO->id)->update($cd_data);

        if (!empty($udpateResponse)){
        	$certificationDTOFinal->setId($udpateResponse);
        }

        /* Convert database value to readable response */
        return $certificationDTOFinal;
    }

    public function certificationDelete($certificationid)
    {   
       $deleteResponse = $this->certificationDb->where('certificationid',$certificationid)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getCertificationByPage($reportId, $page, $limit)
    {
    	
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report Certification in tblrelatedcompanies table */
        $certificationList = $this->certificationDb->getCertificationByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $certificationDtoList = [];
        foreach($certificationList as $key => $value){
            $certificationDto = new CertificationDTO();            
            $certificationDto->setVars($certificationList[$key]);
            array_push($certificationDtoList, $certificationDto->getVars());
        }
        
        $results['total'] = $this->certificationDb->getCertificationByReportId($reportId)->count();  
        
        $results['items'] = $certificationList;
        return $results;
    }

    public function getCertificationByReportId($reportId)
    {
    	/* Get report Certification in tblcapital table */
        $certification = $this->certificationDb->getCertificationByReportId($reportId);

        $results = [];
        if (!empty($certification)){
        	$results['items'] = [];
        	foreach ($certification as $certification) {
        		$certificationDto = new CertificationDTO;
        		$certificationDto->setVars($certification);
        		$results['items'][] = $certificationDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getCertificationById($certificationid)
    {
        /* Get report Certification in tblcertification table */
        $certification = $this->certificationDb->getCertificationById($certificationid);
        if (!empty($certification)){
            $certificationDto = new CertificationDTO;
            $certificationDto->setVars($certification);

            return $certificationDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}