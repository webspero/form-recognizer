<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Location;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\MainLocationDTO;

class MainLocationCRUD
{
    public function __construct()
    {
        $this->MainLocationDb = new Location();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateMainLocationDetails($mainLocation)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'sizeofpremises.required' => 'Size Of Premises is required.',
            'premisesownedleased.required' => 'Premises Owned Leased is required.',
            'location.required' => 'Location is required.',
            'premisesusedas.required' => 'Premises Used As is required.',
            'nooffloors.required' => 'No Of Floors is required.',
            'locationaddress.required' => 'Location Address is required.',
            'material.required' => 'Material is required.',
            'color.required' => 'Color is required.',
            'map.required' => 'Map is required.',
            'actuallocationphoto.required' => 'Actual Location Photo is required.',
            'note.required' => 'Actual Location Photo is required.',
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'sizeofpremises' => 'required',
            'premisesownedleased' => 'required',
            'location' => 'required',
            'premisesusedas' => 'required',
            'nooffloors' => 'required',
            'locationaddress' => 'required',
            'material' => 'required',
            'color' => 'required',
            'map' => 'required',
            'actuallocationphoto' => 'required',
            'note' => 'required',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($mainLocation, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function mainLocationInsert($mainLocationDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $mainLocationDTO->reportid,
            'location_size' => $mainLocationDTO->sizeofpremises,
            'location_type' => $mainLocationDTO->premisesownedleased,
            'location_map' => $mainLocationDTO->location,
            'location_used' => $mainLocationDTO->premisesusedas,
            'floor_count' => $mainLocationDTO->nooffloors,
            'address' => $mainLocationDTO->locationaddress,
            'material' => $mainLocationDTO->material,
            'color' => $mainLocationDTO->color,
            'map' => $mainLocationDTO->map,
            'location_photo' => $mainLocationDTO->actuallocationphoto,
            'other' => $mainLocationDTO->note
        ];

        $mainLocationDTOFinal = new MainLocationDTO();
        $mainLocationDTOFinal->setVars((array) $mainLocationDTO);
        $insertResponse = $this->MainLocationDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$mainLocationDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $mainLocationDTOFinal;
    }

    public function mainLocationUpdate($mainLocationDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $mainLocationDTO->reportid,
            'location_size' => $mainLocationDTO->sizeofpremises,
            'location_type' => $mainLocationDTO->premisesownedleased,
            'location_map' => $mainLocationDTO->location,
            'location_used' => $mainLocationDTO->premisesusedas,
            'floor_count' => $mainLocationDTO->nooffloors,
            'address' => $mainLocationDTO->locationaddress,
            'material' => $mainLocationDTO->material,
            'color' => $mainLocationDTO->color,
            'map' => $mainLocationDTO->map,
            'location_photo' => $mainLocationDTO->actuallocationphoto,
            'other' => $mainLocationDTO->note
        ];

        $mainLocationDTOFinal = new MainLocationDTO();
        $mainLocationDTOFinal->setVars((array) $mainLocationDTO);
        $udpateResponse = $this->MainLocationDb->where('locationid',$mainLocationDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$mainLocationDTOFinal->setId($mainLocationDTO->id);
        }

        /* Convert database value to readable response */
        return $mainLocationDTOFinal;
    }

    public function mainLocationDelete($locationId)
    {   
       $deleteResponse = $this->MainLocationDb->where('locationid',$locationId)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getMainLocationByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report Main Location in tbllocations table */
        $affiliateSubsidiariesList = $this->MainLocationDb->getMainLocationByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $mainLocationDtoList = [];
        foreach($mainLocationList as $key => $value){
            $mainLocationDto = new MainLocationDTO();            
            $mainLocationDto->setVars($mainLocationList[$key]);
            array_push($mainLocationDtoList, $mainLocationDto->getVars());
        }
        
        $results['total'] = $this->MainLocationDb->getMainLocationByReportId($reportId)->count();  
        
        $results['items'] = $mainLocationDtoList;
        return $results;
    }

    public function getMainLocationByReportId($reportId)
    {
    	/* Get report Main Location in tblrelatedcompanies table */
        $mainLocations = $this->MainLocationDb->getMainLocationByReportId($reportId);

        $results = [];
        if (!empty($mainLocations)){
        	$results['items'] = [];
        	foreach ($mainLocations as $mainLocation) {
        		$mainLocationDto = new MainLocationDTO;
        		$mainLocationDto->setVars($mainLocation);
        		$results['items'][] = $mainLocationDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getMainLocationById($locationid)
    {
        /* Get report MainLocationDto in tbllocations table */
        $mainLocation = $this->MainLocationDb->getMainLocationById($locationid);
        if (!empty($mainLocation)){
            $mainLocationDto = new MainLocationDTO;
            $mainLocationDto->setVars($mainLocation);

            return $mainLocationDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }
}