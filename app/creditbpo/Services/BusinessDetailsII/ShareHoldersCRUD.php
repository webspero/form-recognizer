<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Shareholder;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\ShareHoldersDTO;

class ShareHoldersCRUD
{
    public function __construct()
    {
        $this->shareHoldersDb = new Shareholder();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateShareHoldersDetails($shareHolders)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'firstname.required' => 'Firstname is required.',
            'middlename.required' => 'Middlename is required.',
            'lastname.required' => 'Lastname is required.',
            'birthdate.required' => 'Birthdate is required.',
            'address.required' => 'Address is required.',
            'amountofshareholdings.required' => 'Amount of shareholdings is required.',
            'percentageshare.required' => 'Percentage share is required.',
            'tinno.required' => 'Tin no is required.',
            'nationality.required' => 'Nationality is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'birthdate' => 'required',
            'address' => 'required',
            'amountofshareholdings' => 'required',
            'percentageshare' => 'required',
            'tinno' => 'required',
            'nationality' => 'required',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($shareHolders, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function shareHoldersInsert($shareHoldersDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $shareHoldersDTO->reportid,
        	'firstname' => $shareHoldersDTO->firstname,
        	'middlename' => $shareHoldersDTO->middlename,
        	'lastname' => $shareHoldersDTO->lastname,
        	'birthdate' => $shareHoldersDTO->birthdate,
        	'address' => $shareHoldersDTO->address,
        	'amount' => $shareHoldersDTO->amountofshareholdings,
        	'percentage_share' => $shareHoldersDTO->percentageshare,
        	'id_no' => $shareHoldersDTO->tinno,
        	'nationality' => $shareHoldersDTO->nationality,
        ];

        $shareHoldersDTOFinal = new ShareHoldersDTO();
        $shareHoldersDTOFinal->setVars((array) $shareHoldersDTO);
        $insertResponse = $this->shareHoldersDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$shareHoldersDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $shareHoldersDTOFinal;
    }

    public function shareHoldersUpdate($shareHoldersDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $shareHoldersDTO->reportid,
        	'firstname' => $shareHoldersDTO->firstname,
        	'middlename' => $shareHoldersDTO->middlename,
        	'lastname' => $shareHoldersDTO->lastname,
        	'birthdate' => $shareHoldersDTO->birthdate,
        	'address' => $shareHoldersDTO->address,
        	'amount' => $shareHoldersDTO->amountofshareholdings,
        	'percentage_share' => $shareHoldersDTO->percentageshare,
        	'id_no' => $shareHoldersDTO->tinno,
        	'nationality' => $shareHoldersDTO->nationality,
        ];

        $shareHoldersDTOFinal = new ShareHoldersDTO();
        $shareHoldersDTOFinal->setVars((array) $shareHoldersDTO);
        $udpateResponse = $this->shareHoldersDb->where('id',$shareHoldersDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$shareHoldersDTOFinal->setId($shareHoldersDTO->id);
        }

        /* Convert database value to readable response */
        return $shareHoldersDTOFinal;
    }

    public function shareHoldersDelete($id)
    {   
       $deleteResponse = $this->shareHoldersDb->where('id',$id)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getShareHoldersByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report Share Holders in tblrelatedcompanies table */
        $shareHoldersList = $this->shareHoldersDb->getShareHoldersByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $shareHoldersDtoList = [];
        foreach($shareHoldersList as $key => $value){
            $shareHoldersDto = new shareHoldersDTO();            
            $shareHoldersDto->setVars($shareHoldersList[$key]);
            array_push($shareHoldersDtoList, $shareHoldersDto->getVars());
        }
        
        $results['total'] = $this->shareHoldersDb->getShareHoldersByReportId($reportId)->count();  
        
        $results['items'] = $shareHoldersDtoList;
        return $results;
    }

    public function getShareHoldersByReportId($reportId)
    {
    	/* Get report Share Holders in tblrelatedcompanies table */
        $shareHolders = $this->shareHoldersDb->getShareHoldersByReportId($reportId);

        $results = [];
        if (!empty($shareHolders)){

        	$results['items'] = [];
        	foreach ($shareHolders as $shareHolder) {
        		$shareHoldersDto = new ShareHoldersDTO;
        		$shareHoldersDto->setVars($shareHolder);
        		$results['items'][] = $shareHoldersDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getShareHoldersById($id)
    {
        /* Get report Share Holders in tblmajorcustomer table */
        $shareHolders = $this->shareHoldersDb->getShareHoldersById($id);
        if (!empty($shareHolders)){
            $shareHoldersDto = new ShareHoldersDTO;
            $shareHoldersDto->setVars($shareHolders);

            return $shareHoldersDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}