<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\MajorLocation;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\MajorMarketLocationDTO;

class MajorMarketLocationCRUD
{
    public function __construct()
    {
        $this->MajorMarketLocationDb = new MajorLocation();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateMajorMarketLocationDetails($majorMarketLocation)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'marketlocation.required' => 'Market Location is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'marketlocation' => 'required'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($majorMarketLocation, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function majorMarketLocationInsert($majorMarketLocationDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $majorMarketLocationDTO->reportid,
        	'marketlocation' => $majorMarketLocationDTO->marketlocation
        ];

        $majorMarketLocationDTOFinal = new MajorMarketLocationDTO();
        $majorMarketLocationDTOFinal->setVars((array) $majorMarketLocationDTO);
        $insertResponse = $this->MajorMarketLocationDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$majorMarketLocationDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $majorMarketLocationDTOFinal;
    }

    public function majorMarketLocationUpdate($majorMarketLocationDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $majorMarketLocationDTO->reportid,
        	'marketlocation' => $majorMarketLocationDTO->marketlocation
        ];

        $majorMarketLocationDTOFinal = new MajorMarketLocationDTO();
        $majorMarketLocationDTOFinal->setVars((array) $majorMarketLocationDTO);
        $udpateResponse = $this->MajorMarketLocationDb->where('marketlocationid',$majorMarketLocationDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$majorMarketLocationDTOFinal->setId($majorMarketLocationDTO->id);
        }

        /* Convert database value to readable response */
        return $majorMarketLocationDTOFinal;
    }

    public function majorMarketLocationDelete($locationId)
    {   
       $deleteResponse = $this->MajorMarketLocationDb->where('marketlocationid',$locationId)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getMajorMarketLocationByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report MajorMarketLocation in tblmajorlocation table */
        $majorMarketLocationList = $this->MajorMarketLocationDb->getMajorMarketLocationByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $majorMarketLocationDtoList = [];
        foreach($majorMarketLocationList as $key => $value){
            $majorMarketLocationDto = new majorMarketLocationDTO();            
            $majorMarketLocationDto->setVars($majorMarketLocationList[$key]);
            array_push($majorMarketLocationDtoList, $majorMarketLocationDto->getVars());
        }
        
        $results['total'] = $this->majorMarketLocationDb->getMajorMarketLocationByReportId($reportId)->count();  
        
        $results['items'] = $majorMarketLocationDtoList;
        return $results;
    }

    public function getMajorMarketLocationByReportId($reportId)
    {
    	/* Get report Major Market Location in tblmajormarketlocation table */
        $majorMarketLocations = $this->MajorMarketLocationDb->getMajorMarketLocationByReportId($reportId);

        $results = [];
        if (!empty($majorMarketLocations)){
        	$results['items'] = [];
        	foreach ($majorMarketLocations as $majorMarketLocation) {
        		$majorMarketLocationDto = new MajorMarketLocationDTO;
        		$majorMarketLocationDto->setVars($majorMarketLocation);
        		$results['items'][] = $majorMarketLocationDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getMajorMarketLocationById($locationid)
    {
        /* Get report MainLocationDto in tbllocations table */
        $majorMarketLocation = $this->MajorMarketLocationDb->getMajorMarketLocationById($locationid);
        if (!empty($majorMarketLocation)){
            $majorMarketLocationDto = new MajorMarketLocationDTO;
            $majorMarketLocationDto->setVars($majorMarketLocation);

            return $majorMarketLocationDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }
}