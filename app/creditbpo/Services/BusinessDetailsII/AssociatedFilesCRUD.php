<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\AssociatedFile;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\AssociatedFilesDTO;

class AssociatedFilesCRUD
{
    public function __construct()
    {
        $this->associatedFilesDb = new AssociatedFile();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateAssociatedFiles($associatedFile)
    {

        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'uploadedfile.required' => 'Upload file is required.',
            'uploadedfile.mimes' => 'Upload File file type should be doc,docx,pdf,jpeg,png,xls,xlsx.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'uploadedfile' => 'required|mimes:doc,docx,pdf,jpeg,png,xls,xlsx'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($associatedFile, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function associatedFilesInsert($associatedFileDTO)
    {   
    	//print_r($associatedFileDTO); exit;
        /* Save data to database */
        $ad_data = [
        	'entity_id' => $associatedFileDTO['reportid'],
        	'file_name' => $associatedFileDTO['uploadedfile']
        ];

        $uploadFile = request()->file('uploadedfile');
        $extension = $uploadFile->extension();
        $filename = $uploadFile->getClientOriginalName().$extension;

        $destinationPath = public_path('associated_files/'.$associatedFileDTO['reportid'].'/');
        
        if (!file_exists(public_path('associated_files/'))) {
            mkdir(public_path('associated_files/'.$associatedFileDTO['reportid'].'/'));
        }

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath,0777);
        }

        if($uploadFile){
	        
	        if($uploadFile->move($destinationPath, $filename)){
				
	        	$associatedFileDTO['uploadedfile'] = $filename;
	        	$ad_data['file_name'] = $associatedFileDTO['uploadedfile'];
	        }
    	}

        $associatedFileDTOFinal = new AssociatedFilesDTO();
        $associatedFileDTOFinal->setVars($associatedFileDTO);
        $insertResponse = $this->associatedFilesDb->insertGetId($ad_data);

        if (!empty($insertResponse)){
        	$associatedFileDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $associatedFileDTOFinal;
    }

    public function associatedFilesUpdate($associatedFileDTO)
    {   
        /* Save data to database */
        $ad_data = [
        	'entity_id' => $associatedFileDTO->reportid,
        	'file_name' => $associatedFileDTO->uploadedfile
        ];

        $uploadFile = request()->file('uploadedfile');
        //$extension = $uploadFile->extension();
        $filename = $uploadFile->getClientOriginalName();

        $destinationPath = public_path('associated_files/'.$associatedFileDTO->reportid.'/');
        
        if (!file_exists(public_path('associated_files/'))) {
            mkdir(public_path('associated_files/'.$associatedFileDTO->reportid.'/'));
        }

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath,0777);
        }

        if($uploadFile){
	        
	        if($uploadFile->move($destinationPath, $filename)){
				$associatedFileDTO->uploadedfile = $filename;
	        	$ad_data['file_name'] = $associatedFileDTO->uploadedfile;
	        }
    	}

        $associatedFileDTOFinal = new AssociatedFilesDTO();
        $associatedFileDTOFinal->setVars((array) $associatedFileDTO);
        
        $udpateResponse = $this->associatedFilesDb->where('id',$associatedFileDTO->id)->update($ad_data);

        if (!empty($udpateResponse)){
        	$associatedFileDTOFinal->setId($associatedFileDTO->id);
        }

        /* Convert database value to readable response */
        return $associatedFileDTOFinal;
    }

    public function associatedFilesDelete($id)
    {   
       $deleteResponse = $this->associatedFilesDb->where('id',$id)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getAssociatedFilesByPage($reportId, $page, $limit)
    {
    	
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report AssociatedFiles in tblrelatedcompanies table */
        $associatedFilesList = $this->associatedFilesDb->getAssociatedFilesByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $associatedFilesDtoList = [];
        foreach($associatedFilesList as $key => $value){
            $associatedFilesDto = new AssociatedFilesDTO();            
            $associatedFilesDto->setVars($associatedFilesList[$key]);
            array_push($associatedFilesDtoList, $associatedFilesDto->getVars());
        }
        
        $results['total'] = $this->associatedFilesDb->getAssociatedFilesByReportId($reportId)->count();  
        
        $results['items'] = $associatedFilesList;
        return $results;
    }

    public function getAssociatedFilesByReportId($reportId)
    {
    	/* Get report associatedFiles in tblcapital table */
        $associatedFiles = $this->associatedFilesDb->getAssociatedFilesByReportId($reportId);

        $results = [];
        if (!empty($associatedFiles)){
        	
        	$results['items'] = [];
        	foreach ($associatedFiles as $associatedFile) {
        		$associatedFileDto = new AssociatedFilesDTO;
        		$associatedFileDto->setVars($associatedFile);
        		$results['items'][] = $associatedFileDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getAssociatedFileById($id)
    {
        /* Get report Associated Files in tbldropboxentity table */
        $associatedFile = $this->associatedFilesDb->getAssociatedFileById($id);
        if (!empty($associatedFile)){
            $associatedFileDto = new AssociatedFilesDTO;
            $associatedFileDto->setVars($associatedFile);

            return $associatedFileDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}