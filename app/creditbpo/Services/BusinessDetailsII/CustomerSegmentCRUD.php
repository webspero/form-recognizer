<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\BusinessSegment;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\CustomerSegmentDTO;

class CustomerSegmentCRUD
{
    public function __construct()
    {
        $this->customerSegmentDb = new BusinessSegment();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateCustomerSegmentDetails($customerSegment)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'name.required' => 'Name is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'name' => 'required',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($customerSegment, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function customerSegmentInsert($customerSegmentDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $customerSegmentDTO->reportid,
        	'businesssegment_name' => $customerSegmentDTO->name
        ];

        $customerSegmentDTOFinal = new CustomerSegmentDTO();
        $customerSegmentDTOFinal->setVars((array) $customerSegmentDTO);
        $insertResponse = $this->customerSegmentDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$customerSegmentDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $customerSegmentDTOFinal;
    }

    public function customerSegmentUpdate($customerSegmentDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $customerSegmentDTO->reportid,
        	'businesssegment_name' => $customerSegmentDTO->name
        ];

        $customerSegmentDTOFinal = new customerSegmentDTO();
        $customerSegmentDTOFinal->setVars((array) $customerSegmentDTO);
        $udpateResponse = $this->customerSegmentDb->where('businesssegmentid',$customerSegmentDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$customerSegmentDTOFinal->setId($customerSegmentDTO->id);
        }

        /* Convert database value to readable response */
        return $customerSegmentDTOFinal;
    }

    public function customerSegmentDelete($businessSegmentId)
    {   
       $deleteResponse = $this->customerSegmentDb->where('businesssegmentid',$businessSegmentId)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getCustomerSegmentByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report affiliates/subsidiaries in tblrelatedcompanies table */
        $customerSegmentList = $this->customerSegmentDb->getCustomerSegmentByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $customerSegmentDtoList = [];
        foreach($customerSegmentList as $key => $value){
            $customerSegmentDto = new customerSegmentDTO();            
            $customerSegmentDto->setVars($customerSegmentList[$key]);
            array_push($customerSegmentDtoList, $customerSegmentDto->getVars());
        }
        
        $results['total'] = $this->customerSegmentDb->getCustomerSegmentByReportId($reportId)->count();  
        
        $results['items'] = $customerSegmentDtoList;
        return $results;
    }

    public function getCustomerSegmentByReportId($reportId)
    {
    	/* Get report Customer Segment in tblbusinesssegment table */
        $customerSegments = $this->customerSegmentDb->getCustomerSegmentByReportId($reportId);

        $results = [];
        if (!empty($customerSegments)){

        	$results['items'] = [];
        	foreach ($customerSegments as $customerSegment) {
        		$customerSegmentDto = new CustomerSegmentDTO;
        		$customerSegmentDto->setVars($customerSegment);
        		$results['items'][] = $customerSegmentDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getCustomerSegmentById($businessSegmentId)
    {
        /* Get report Customer Segment in tblbusinesssegment table */
        $customerSegment = $this->customerSegmentDb->getCustomerSegmentById($businessSegmentId);
        if (!empty($customerSegment)){
            $customerSegmentDto = new CustomerSegmentDTO;
            $customerSegmentDto->setVars($customerSegment);

            return $customerSegmentDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}