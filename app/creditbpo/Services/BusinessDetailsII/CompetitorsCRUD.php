<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\Competitor;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\CompetitorsDTO;

class CompetitorsCRUD
{
    public function __construct()
    {
        $this->competitorsDb = new Competitor();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateCompetitorsDetails($competitors)
    {
        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'name.required' => 'Name is required.',
            'address.required' => 'Address is required.',
            'phone.required' => 'Phone is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Email must be a valid email address.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($competitors, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function competitorsInsert($competitorsDTO)
    {   

        /* Save data to database */
        $as_data = [
        	'entity_id' => $competitorsDTO->reportid,
        	'competitor_name' => $competitorsDTO->name,
        	'competitor_address' => $competitorsDTO->address,
        	'competitor_phone' => $competitorsDTO->phone,
        	'competitor_email' => $competitorsDTO->email
        ];

        $competitorsDTOFinal = new CompetitorsDTO();
        $competitorsDTOFinal->setVars((array) $competitorsDTO);
        $insertResponse = $this->competitorsDb->insertGetId($as_data);

        if (!empty($insertResponse)){
        	$competitorsDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $competitorsDTOFinal;
    }

    public function competitorsUpdate($competitorsDTO)
    {   
        /* Save data to database */
        $as_data = [
        	'entity_id' => $competitorsDTO->reportid,
        	'competitor_name' => $competitorsDTO->name,
        	'competitor_address' => $competitorsDTO->address,
        	'competitor_phone' => $competitorsDTO->phone,
        	'competitor_email' => $competitorsDTO->email
        ];

        $competitorsDTOFinal = new CompetitorsDTO();
        $competitorsDTOFinal->setVars((array) $competitorsDTO);
        $udpateResponse = $this->competitorsDb->where('competitorid',$competitorsDTO->id)->update($as_data);

        if (!empty($udpateResponse)){
        	$competitorsDTOFinal->setId($competitorsDTO->id);
        }

        /* Convert database value to readable response */
        return $competitorsDTOFinal;
    }

    public function competitorsDelete($competitorid)
    {   
       $deleteResponse = $this->competitorsDb->where('competitorid',$competitorid)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getCompetitorsByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report competitors in tblrelatedcompanies table */
        $competitorsList = $this->competitorsDb->getCompetitorsByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $competitorsDtoList = [];
        foreach($competitorsList as $key => $value){
            $competitorsDto = new competitorsDTO();            
            $competitorsDto->setVars($competitorsList[$key]);
            array_push($competitorsDtoList, $competitorsDto->getVars());
        }
        
        $results['total'] = $this->competitorsDb->getCompetitorsByReportId($reportId)->count();  
        
        $results['items'] = $competitorsDtoList;
        return $results;
    }

    public function getCompetitorsByReportId($reportId)
    {
    	/* Get report competitors in tblrelatedcompanies table */
        $competitors = $this->competitorsDb->getCompetitorsByReportId($reportId);

        $results = [];
        if (!empty($competitors)){

        	$results['items'] = [];
        	foreach ($competitors as $competitor) {
        		$competitorsDto = new CompetitorsDTO;
        		$competitorsDto->setVars($competitor);
        		$results['items'][] = $competitorsDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getCompetitorsById($relatedCompaniesId)
    {
        /* Get report competitors in tblcompetitor table */
        $competitors = $this->competitorsDb->getCompetitorsById($relatedCompaniesId);
        if (!empty($competitors)){
            $competitorsDto = new CompetitorsDTO;
            $competitorsDto->setVars($competitors);

            return $competitorsDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}