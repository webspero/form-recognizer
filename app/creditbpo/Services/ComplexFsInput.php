<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use CreditBPO\Services\FsInput;
use CreditBPO\Exceptions\FormInputException;
use Maatwebsite\Excel\Facades\Excel;
use FinancialReport;
use Entity;

class ComplexFsInput extends FsInput {
    const ACCOUNTING_FORMAT = '_(* #,##0.00_);_(* (#,##0.00);_(* -??_);_(@_)';
    const FILENAME = 'Full FS Input Template - %s as of %s';
    const NUMBER_FORMAT = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)';

    public function __construct($filePath, $data = []) {
        parent::__construct($filePath, $data);
        $this->setConfig(Config::get('services.complex-fs-input'));
    }

    public function write($cellValues = []) {
        return Excel::load($this->getFilePath(), function($doc) use ($cellValues) {
            $this->sheet = $doc->setActiveSheetIndex(0);

            $config = $this->getConfig();
            $balanceSheetConfig = $config['values']['balance_sheet']['reference'];
            $incomeStatementConfig = $config['values']['income_statement']['reference'];
            $cashFlowConfig = $config['values']['statement_of_cashflow']['reference'];

            $balanceSheet = $cellValues['balance_sheet'];
            $incomeStatement = $cellValues['income_statement'];
            $cashFlow = $cellValues['cash_flow'];

            foreach ($balanceSheet as $column => $values) {
                foreach ($values as $key => $cell) {
                    if (!$cell) {
                        continue;
                    }

                    if (!isset($balanceSheetConfig[$key])) {
                        continue;
                    }

                    $cellMarker = $column.$balanceSheetConfig[$key];

                    if (in_array($cellMarker, $config['accounting_cells'])) {
                        continue;
                    }

                    if ($key != 'Year'){
                        $this->sheet
                        ->getStyle($cellMarker)
                        ->getNumberFormat()
                        ->setFormatCode(self::NUMBER_FORMAT);
                    }

                    if($key == 'Year') {
                        if($cellMarker == "B10") {
                            $this->sheet
                                ->getCell($cellMarker)
                                ->setValue($cell);
                        }
                    } else {
                        $this->sheet
                            ->getCell($cellMarker)
                            ->setValue($cell);
                    }
                }
            }

            foreach ($incomeStatement as $column => $values) {
                foreach ($values as $key => $cell) {
                    if (!$cell) {
                        continue;
                    }

                    if (!isset($incomeStatementConfig[$key])) {
                        continue;
                    }

                    $cellMarker = $column.$incomeStatementConfig[$key];

                    if (in_array($cellMarker, $config['accounting_cells'])) {
                        continue;
                    }

                    if ($key != 'Year'){
                        $this->sheet
                            ->getStyle($cellMarker)
                            ->getNumberFormat()
                            ->setFormatCode(self::NUMBER_FORMAT);

                        $this->sheet
                            ->getCell($cellMarker)
                            ->setValue($cell);
                    }
                }
            }

            foreach ($cashFlow as $column => $values) {
                foreach ($values as $key => $cell) {
                    if (!$cell) {
                        continue;
                    }

                    // if (!isset($cashFlowConfig[$key])) {
                    //     continue;
                    // }

                    $cellMarker = $column.$cashFlowConfig[$key];

                    // if (in_array($cellMarker, $config['accounting_cells'])) {
                    //     continue;
                    // }
                    
                    if ($key != 'Year' && $key != 'NetCashByOperatingActivities') {
                        $this->sheet
                            ->getStyle($cellMarker)
                            ->getNumberFormat()
                            ->setFormatCode(self::NUMBER_FORMAT);

                        $this->sheet
                            ->getCell($cellMarker)
                            ->setValue($cell);
                    }
                    
                }
            }

            foreach ($config['accounting_cells'] as $cell) {
                $this->sheet
                    ->getStyle($cell)
                    ->getNumberFormat()
                    ->setFormatCode(self::ACCOUNTING_FORMAT);
            }

            $data = $this->getData();
            $fs = FinancialReport::where(['entity_id' => $data['entity_id'],'is_deleted' => 0])
                ->orderBy('id', 'desc')
                ->get()
                ->first();
            $industry_id = Entity::where('entityid', $data['entity_id'])->pluck('industry_main_id')->first();
            $companyname = Entity::where('entityid', $data['entity_id'])->pluck('companyname')->first();

            $companyCell = $config['values']['financial_analysis']['company'];
            $this->sheet->getCell($companyCell)->setValue($companyname);

            $industryCell = $config['values']['financial_analysis']['industry_name'];
            $industryName = $this->getIndustryNameFromCode($fs->industry_id);
            $this->sheet->getCell($industryCell)->setValue($industryName);

            $this->sheet->getCell('B4')->setValue($fs->industry_id);
            $this->sheet->getCell('D4')->setValue($fs->industry_id);

            $moneyFactor = $this->getMoneyFactorFromUnit($fs->money_factor);
            $moneyFactorCell = $config['values']['financial_analysis']['units'];
            $this->sheet->getCell($moneyFactorCell)->setValue($moneyFactor);

            $currency = $this->getCurrencyNameFromCode($fs->currency);
            $currencyCell = $config['values']['financial_analysis']['currency'];
            $this->sheet->getCell($currencyCell)->setValue($currency);

            $sheet = $config['sheet'];
            $additionalLines = $config['additional_lines']['values'];
            $additionalLineValues = $cellValues['additional_lines'];

            $this->sheet = $doc->setActiveSheetIndex(4);
            //$this->sheet = $this->excel->getActiveSheet();

            foreach ($additionalLineValues as $column => $list) {
                foreach ($list as $key => $value) {
                    if (!$value) {
                        continue;
                    }

                    $cellMarker = $column.$additionalLines[$key];

                    if ($key != 'Year'){
                        $this->sheet
                            ->getStyle($cellMarker)
                            ->getNumberFormat()
                            ->setFormatCode(self::NUMBER_FORMAT);
                    }
                    $this->sheet
                        ->getCell($cellMarker)
                        ->setValue($value);
                }
            }
            if($fs->currency != 'PHP'){
                $this->sheet->getCell('H1')->setValue('Please note that this sheet is not converted to ' . $fs->currency . ' currency.');
            }

            $additionalLinesAccounting = $config['additional_lines_accounting_cells'];

            foreach ($additionalLinesAccounting as $cellMarker) {
                $this->sheet
                    ->getStyle($cellMarker)
                    ->getNumberFormat()
                    ->setFormatCode(self::ACCOUNTING_FORMAT);
            }

            $this->sheet = $doc->setActiveSheetIndex(0);
        })->setFilename($this->getDocumentName())
        ->store('xlsx', public_path('temp'), true);
    }

    public function getDocumentFileName() {
        return $this->getDocumentName().'.xlsx';
    }
}
