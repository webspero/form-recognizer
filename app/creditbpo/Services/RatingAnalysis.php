<?php

namespace CreditBPO\Services;

use CreditBPO\Services\Report\PDF\RatingAnalysis as PDFHandler;
use Illuminate\Support\Facades\View;
use File;

class RatingAnalysis {
    /**
     * @param int $entityId
     * @return string|null
     */
    public function createReport($entityId) {
        $pdf = new PDFHandler();
        $contents = $pdf->create($entityId)->generate();
        if ($contents) {
            $contents->setPaper('letter');
            $contents->setOrientation('portrait');
            $pdf->save();
            return $pdf->getReportFilepath($entityId);
        }

        return null;

        $contents = $pdf->create($entityId);
        return View::make($pdf->getTemplate(), $pdf->getData());
    }

    /**
     * @param int $entityId
     * @return string|null
     */
    public function getReport($entityId) {
        $file = $this->getReportFilepath($entityId);

        if (!file_exists($file)) {
            $file = $this->createReport();
        }

        if ($file) {
            return File::get($file);
        }

        return null;
    }

    /**
     * @param int $entityId
     * @return string
     */
    public function getReportFilepath($entityId) {
        $pdf = new PDFHandler();
        return $pdf->getReportFilepath($entityId);
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function reportFileExists($entityId) {
        return file_exists($this->getReportFilepath($entityId));
    }
}
