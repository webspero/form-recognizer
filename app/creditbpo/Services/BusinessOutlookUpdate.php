<?php

namespace CreditBPO\Services;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Config;
use CreditBPO\Libraries\Scraper;
use CreditBPO\Models\BusinessOutlookUpdate as BusinessOutlookUpdateModel;
use CreditBPO\DTO\BusinessOutlookUpdateDTO;
use Industrymain;
use BusinessOutlook;
use RegressionCalcLib;

class BusinessOutlookUpdate {
    public function autoUpdate() {
        $settings = $this->getAutoUpdateSettings();
        $scraper = new Scraper();
        $response = $scraper->fetch($settings->getUrl());
        return $this->process($settings, $response);
    }

    /**
     * @param BusinessOutlookUpdateDTO $settings
     *
     */
    protected function process(BusinessOutlookUpdateDTO $settings, Response $response) {
        $contentTypes = $response->getHeader('Content-Type');

        if (
            $response->getStatusCode() !== 200 ||
            array_diff($settings->getContentTypes(), $contentTypes)
        ) {
            return false;
        }

        echo 'Found new file '.$settings->getPdfFilename()."\n";
        $result = $response->getBody()->getContents();
        file_put_contents($settings->getSource(), $result, LOCK_EX);

        echo 'Converting '.$settings->getPdfFilename().
            ' to '.$settings->getTextFilename()."\n";
        $command = 'pdftotext -eol dos -layout %s %s';
        $source = $settings->getSource();
        $destination = $settings->getDestination();
        exec(sprintf($command, $source, $destination));

        $reader = fopen($destination, "r");
        $start = false;
        $parseData = false;
        $data = [];
        $endLoop = false;

        if ($reader) {
            while (!$endLoop && ($line = fgets($reader)) !== false) {
                if (!$start && strpos($line, $settings->getMarkerStart()) !== false) {
                    $start = true;
                    continue;
                }

                if (
                    $start && !$parseData &&
                    strpos($line, $settings->getMarkerStartParsing()) !== false
                ) {
                    $parseData = true;
                }

                if ($parseData) {
                    $str = explode(' ', preg_replace('/\s+/', ' ', trim($line)));

                    if ($str[@count($str)-1] == '') {
                        unset($str[@count($str)-1]);
                    }

                    $str = array_map('floatval', array_slice($str, -8));
                    $data[] = $str;
                }

                $endLoop = $start && $parseData &&
                    strpos($line, $settings->getMarkerStop()) !== false;
            }

            $industry_count = 18;
            $businessOutlook = new BusinessOutlook();
            $regressionCalcLib = new RegressionCalcLib();

            for ($i = 1; $i <= $industry_count; $i++) {
                $industry = Industrymain::find($i);

                switch ($i) {
                    case INDUSTRY_AGRICULTURE:
                    case INDUSTRY_MINING:
                    case INDUSTRY_MANUFACTURING:
                    case INDUSTRY_ELECTRICITY:
                    case INDUSTRY_WATER:
                        $id = INDUSTRY_BSP_INDUSTRY;
                    break;

                    case INDUSTRY_CONSTRUCTION:
                        $id = INDUSTRY_BSP_CONSTRUCTION;
                    break;

                    case INDUSTRY_WHOLESALE:
                        $id = INDUSTRY_BSP_WHOLESALE;
                    break;

                    case INDUSTRY_TRANSPORTATION:
                        $id = INDUSTRY_BSP_TRANSPORTATION;
                    break;

                    case INDUSTRY_ACCOMODATION:
                        $id = INDUSTRY_BSP_ACCOMODATIONS;
                    break;

                    case INDUSTRY_COMMUNICATION:
                    case INDUSTRY_SCIENTIFIC:
                    case INDUSTRY_OTHER_SERVICES:
                        $id = INDUSTRY_BSP_SERVICES;
                    break;

                    case INDUSTRY_FINANCE:
                        $id = INDUSTRY_BSP_FINANCE;
                    break;

                    case INDUSTRY_REAL_ESTATE:
                        $id = INDUSTRY_BSP_REAL_ESTATE;
                    break;

                    case INDUSTRY_ADMINISTRATIVE:
                        $id = INDUSTRY_BSP_BUSINESS;
                    break;

                    case INDUSTRY_HEALTH:
                    case INDUSTRY_EDUCATION:
                    case INDUSTRY_ENTERTAINMENT:
                        $id = INDUSTRY_BSP_SOCIAL_SERVICE;
                    break;
                }
                
                if(!empty($data) && is_countable($data[$id]) && count($data[$id]) > 7){
                    $businessData = [
                        'biz-outlook-q1' => $data[$id][BSP_REPORT_QUARTER_1],
                        'biz-outlook-q2' => $data[$id][BSP_REPORT_QUARTER_2],
                        'biz-outlook-q3' => $data[$id][BSP_REPORT_QUARTER_3],
                        'biz-outlook-q4' => $data[$id][BSP_REPORT_QUARTER_4],
                        'biz-outlook-q5' => $data[$id][BSP_REPORT_QUARTER_5],
                        'biz-outlook-q6' => $data[$id][BSP_REPORT_QUARTER_6],
                        'biz-outlook-q7' => $data[$id][BSP_REPORT_QUARTER_7],
                        'biz-outlook-q8' => $data[$id][BSP_REPORT_QUARTER_8],
                    ];
                } else {
                    $businessData = []; 
                }
                

                $businessOutlook->addUpdateBusinessOutlook($id, $businessData);

                if(array_key_exists("biz-outlook-q1",$businessOutlook)){
                    $list = [
                        [$businessData['biz-outlook-q1'], 1],
                        [$businessData['biz-outlook-q2'], 2],
                        [$businessData['biz-outlook-q3'], 3],
                        [$businessData['biz-outlook-q4'], 4],
                        [$businessData['biz-outlook-q5'], 5],
                        [$businessData['biz-outlook-q6'], 6],
                        [$businessData['biz-outlook-q7'], 7],
                        [$businessData['biz-outlook-q8'], 8],
                    ];
                }else {
                    $list = [];
                }
                

                $regression = $regressionCalcLib->calculateRegression($list);
                $industry->industry_score = $regression[REGRESSION_PTS_ALLOC];

                switch($regression[REGRESSION_PTS_ALLOC]){
                    case BIZ_OUTLOOK_UP_POINT:
                        $industry->industry_trend = BIZ_OUTLOOK_UP_TREND;
                    break;

                    case BIZ_OUTLOOK_FLAT_POINT:
                        $industry->industry_trend = BIZ_OUTLOOK_FLAT_TREND;
                    break;

                    case BIZ_OUTLOOK_DOWN_POINT:
                        $industry->industry_trend = BIZ_OUTLOOK_DOWN_TREND;
                    break;
                }

                $industry->save();
            }
        }

        $model = new BusinessOutlookUpdateModel();
        $model->addDownloadInfo($settings->getQuarter(), $settings->getYear());

        echo "\n".'Done'."\n";

        return true;
    }

    /**
     * @param int $quarter
     * @param int $year
     * @return array
     */
    protected function getNextQuarterYear($quarter, $year) {
        if ((int)$quarter === 4) {
            $quarter = 0;
            $year++;
        }

        return [
            'quarter' => $quarter + 1,
            'year' => $year,
        ];
    }

    /**
     * @return BusinessOutlookUpdateDTO
     */
    protected function getAutoUpdateSettings() {
        $config = Config::get('cron.business-outlook-update');
        $model = new BusinessOutlookUpdateModel();
        $data = $model->getPreviousDownload();

        if (!$data) {
            $data = [
                'quarter' => $config['defaults']['quarter'],
                'year' => $config['defaults']['year'],
            ];
        } else {
            $data = $this->getNextQuarterYear(
                $data['quarter'],
                $data['year']);
        }

        $url = sprintf(
            $config['url'],
            $data['year'],
            $data['quarter'],
            $data['year']);
        $source = sprintf(
            $config['file']['source'],
            $data['quarter'],
            $data['year']);
        $destination = sprintf(
            $config['file']['destination'],
            $data['quarter'],
            $data['year']);
        $pdfFilename = sprintf(
            $config['file']['pdf'],
            $data['quarter'],
            $data['year']);
        $textFilename = sprintf(
            $config['file']['text'],
            $data['quarter'],
            $data['year']);
        $dto = new BusinessOutlookUpdateDTO();
        $dto->setDestination(public_path().$destination)
            ->setQuarter($data['quarter'])
            ->setSource(public_path().$source)
            ->setUrl($url)
            ->setYear($data['year'])
            ->setContentTypes($config['content_types'])
            ->setMarkerStart($config['markers']['start'])
            ->setMarkerStartParsing($config['markers']['startParsing'])
            ->setMarkerStop($config['markers']['stop'])
            ->setPdfFilename($pdfFilename)
            ->setTextFilename($textFilename);

        return $dto;
    }
}
