<?php

namespace CreditBPO\Services;

use CreditBPO\Models\Currency;

class CurrencyList
{
    public function __construct()
    {
        //Models
        $this->currencyDb = new Currency();
    }

    public function getCurrenciesByPage($page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get currencies in currency table */
        $currenciesList = $this->currencyDb->getCurrenciesByPage($page, $limit);
        $results['total'] = $this->currencyDb->getCurrencies()->count();  
        $results['items'] = $currenciesList->all();

        return $results;
    }

    public function getCurrenciesList(){
        $results = array();
        $results['page'] = 1;
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get currencies in currency table */
        $currenciesList = $this->currencyDb->getCurrencies();
        $results['total'] = $this->currencyDb->getCurrencies()->count();
        $results['limit'] = $this->currencyDb->getCurrencies()->count();
        $results['items'] = $currenciesList->all();

        return $results;

    }
}