<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model as Eloquent;
use CreditBPO\Libraries\StringHelper;
use CreditBPO\Models\SMEScore;
use Entity;
use ActionController;
use DB;

class UpdateSmeReport {
    /**
     * This class requires pdftotext to be installed locally in order to
     * extract Key Ratio summary from uploaded Ready Ratios file.
     *
     * https://www.xpdfreader.com/pdftotext-man.html
     */
    const CONVERT_PDF = 'pdftotext -eol dos -nopgbrk -layout "%s" "%s"';
    const PDF_FILENAME = 'CreditBPO Financial Analysis - %s as of %s.pdf';
    const TXT_FILENAME = 'CreditBPO Financial Analysis - %s as of %s.txt';
    const FINANCIAL_ANALYSIS_DIRECTORY = '/financial_analysis/';

    protected $config;

    public function __construct() {
        $this->config = Config::get('cron.update-sme-report');
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function updateExistingReport($entityId, $createSMEReport = false) {
        $entity = $this->getEntity($entityId);

        if (!$entity) {
            return;
        }

        $points = $this->getKeyRatiosSummary($entity);
        $data = [
            'positive_points' => implode('|', $points['positive']),
            'negative_points' => implode('|', $points['negative']),
        ];
        $sme = $this->getSMEScoringByEntityId($entityId);
        $this->updateSmeScoring($sme->smescoreid, $data);

        if ($createSMEReport === true) {
            $actionController = new ActionController();
            $actionController->createSMEReport($entityId);
        }

        return true;
    }

    /**
     * @param $entityId
     * @return Eloquent
     */
    public function getEntity($entityId) {
        return Entity::where('entityid', '=', $entityId)->first();
    }

    /**
     * @param $entityId
     * @return Eloquent
     */
    public function getSMEScoringByEntityId($entityId) {
        return SMEScore::where('entityid', '=', $entityId)
            ->orderBy('updated_at', 'desc')
            ->first();
    }

    public function updateSmeScoring($smescoreid, $data) {
        return DB::table('tblsmescore')
            ->where('smescoreid', $smescoreid)
            ->update($data);
    }

    /**
     * @param Eloquent $entity
     * @return array
     */
    public function getKeyRatiosSummary($entity) {
        $lines = $this->getKeyRatiosSummaryFromFile($entity);

        if (!$lines) {
            return [
                'positive' => [],
                'negative' => [],
            ];
        }

        $config = $this->config;
        $markers = $config['markers'];
        $identifiers = $config['identifiers'];
        $foundIndicator = false;
        $positive = [];
        $negative = [];
        $good = null;

        foreach ($lines as $line) {
            // Don't include main indicator title
            if (stripos($line, $markers['indicator']) !== false && !$foundIndicator) {
                $foundIndicator = true;
                continue;
            }

            // If line is a positive or negative indicator value
            if (stripos($line, $markers['bullet']) !== false) {
                $line = $this->cleanKeyRatioValue($line);

                if ($good === false) {
                    $negative[] = $line;
                } else {
                    $positive[] = $line;
                }

                continue;
            }

            // If line is an indicator section
            if (stripos($line, $markers['section']) !== false) {
                $good = $this->isIndicatorGood($line);
                continue;
            }

            // If line is a combination of both section and indicator value
            if (stripos($line, $markers['single_result']) !== false) {
                $compare = substr($line, 0, mb_strpos($line, $markers['single_result']) + 4);
                $good = $this->isIndicatorGood($compare);
                $line = substr($line, mb_strpos($line, $markers['single_result']) + 4);
                $line = $this->cleanKeyRatioValue($line);

                if ($good === false) {
                    $negative[] = $line;
                } else {
                    $positive[] = $line;
                }
            }
        }

        return [
            'positive' => $positive,
            'negative' => $negative,
        ];
    }

    /**
     * @param Eloquent $entity
     * @return array
     */
    public function getKeyRatiosSummaryFromFile($entity) {
        $companyName = trim(rtrim($entity->companyname, '.'));
        // $document = DB::table('tbldocument')
        //     ->where('entity_id', $entity->entityid)
        //     ->where('document_type', 9)
        //     ->orderBy('created_at', 'desc')
        //     ->first();
            
        $period = date('Y-m-d', strtotime($entity->created_at));

        $pdf = sprintf(self::PDF_FILENAME, $companyName, $period);
        $pdfPath = public_path().self::FINANCIAL_ANALYSIS_DIRECTORY.$pdf;

        if (!file_exists($pdfPath)) {
            return;
        }

        $txt = sprintf(self::TXT_FILENAME, $companyName, $period);
        $txtPath = public_path().self::FINANCIAL_ANALYSIS_DIRECTORY.$txt;
        exec(sprintf(self::CONVERT_PDF, $pdfPath, $txtPath));

        if (!file_exists($txtPath)) {
            return;
        }

        $config = $this->config;
        $markers = $config['markers'];
        $startCounter = 0;
        $lines = [];
        $handle = fopen($txtPath, "r");

        /**
         * Collect every key ratio to an array to remove line break problems. This
         * should group every positive/negative indicator statements into one line
         */
        while (($line = fgets($handle)) !== false) {
            $line = trim($line);

            if (
                stripos($line, $markers['stop']) !== false &&
                $startCounter >= $markers['startCount']
            ) {
                break;
            }

            if (
                stripos($line, $markers['start']) === false &&
                $startCounter < $markers['startCount']
            ) {
                continue;
            }

            if (
                stripos($line, $markers['start']) !== false &&
                $startCounter < $markers['startCount']
            ) {
                $startCounter++;

                if ($startCounter <= $markers['startCount']) {
                    continue;
                }
            }

            if (
                $line === '' &&
                (@count($lines) === 0 || $lines[@count($lines) - 1] === '')
            ) {
                continue;
            }

            if (
                @count($lines) === 0 ||
                stripos($line, $markers['bullet']) !== false ||
                $line === '' ||
                substr($lines[@count($lines) - 1], -1) === '.'
            ) {
                $lines[] = $line;
            } else {
                $lines[@count($lines) - 1] .= ' '.$line;
            }
        }

        fclose($handle);
        @unlink($txtPath);

        return $lines;
    }

    /**
     * @param string $value
     * @return bool|null
     */
    protected function isIndicatorGood($value) {
        $config = $this->config;
        $identifiers = $config['identifiers'];

        if (StringHelper::striposInArray($value, $identifiers['bad'])) {
            return false;
        }

        if (StringHelper::striposInArray($value, $identifiers['good'])) {
            return true;
        }

        return null;
    }

    /**
     * @param string $value
     * @return void
     */
    protected function cleanKeyRatioValue($value) {
        $config = $this->config;
        $markers = $config['markers'];
        $cleaned = trim(str_replace($markers['bullet'], '', $value));
        $cleaned = trim($cleaned, "\x00..\x1F");

        return ucfirst($cleaned);
    }
}
