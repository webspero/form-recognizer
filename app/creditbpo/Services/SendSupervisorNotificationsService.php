<?php

namespace CreditBPO\Services;

use User;
use Mail;
use Illuminate\Support\Carbon;


use DB;
use Entity;
use \Exception as Exception;

class SendSupervisorNotificationsService extends BaseCommand {
    protected $config;

    public function __construct($command) {
        parent::__construct($command);
    }

    public function sendNotification() {
        $now = Carbon::now();
        $date_today = $now->toDateString();
        $current_time = $now->format('H:i:s');
        
		$emailList_reportsStarted = DB::table('tblnotif')
                        ->join('tblemailnotif', 'tblemailnotif.bank_id', '=', DB::raw('tblnotif.bank_id'))
                        ->where('tblemailnotif.status', '=', 1)
                        ->where('tblnotif.notif_daily', '=', 1)
                        ->where('tblnotif.notif_reports_started', '=', 1)
                        ->where('tblemailnotif.notif_reports_started_sent', '=', 0)
                        ->get();
                        

        foreach($emailList_reportsStarted as $email){
            $bankDetails = DB::table('tblbank')
                            ->where('id', $email->bank_id)->first();

            $reportsStartedToday = Entity::whereDate('created_at', $date_today)->get();
            
            $user = new User();
            $user->firstname = "Supervisor";
            $user->lastname = $bankDetails->bank_name;

            if ($current_time >= $email->time_trigger) {

                try{
                    Mail::send('emails.notifications.reports_started_today_notification',
                        array(
                            'user' => $user,
                            'reportsStartedToday' => $reportsStartedToday
                        ),
                        function($message) use ($email) {
                            $message->to($email->email, $email->email)
                                ->subject('CreditBPO Notification: Reports Started Today');
                        }
                    );

                }catch(Exception $e){
                //
                }

                if (!Mail::failures()) {
                    DB::table('tblemailnotif')->where('email_notif_id', $email->email_notif_id)->update(['notif_reports_started_sent' => 1]);
                }
            }
        }

        $emailList_reportsCompleted = DB::table('tblnotif')
                        ->join('tblemailnotif', 'tblemailnotif.bank_id', '=', DB::raw('tblnotif.bank_id'))
                        ->where('tblemailnotif.status', '=', 1)
                        ->where('tblnotif.notif_daily', '=', 1)
                        ->where('tblnotif.notif_reports_completed', '=', 1)
                        ->where('tblemailnotif.notif_reports_completed_sent', '=', 0)
                        ->get();

        foreach($emailList_reportsCompleted as $email){
            $bankDetails = DB::table('tblbank')
                            ->where('id', $email->bank_id)->first();

            $reportsCompletedToday = Entity::whereDate('completed_date', $date_today)->get();
            
            $user = new User();
            $user->firstname = "Supervisor";
            $user->lastname = $bankDetails->bank_name;

            if ($current_time >= $email->time_trigger) {

                try{
                    Mail::send('emails.notifications.reports_completed_today_notification',
                        array(
                            'user' => $user,
                            'reportsCompletedToday' => $reportsCompletedToday
                        ),
                        function($message) use ($email) {
                            $message->to($email->email, $email->email)
                                ->subject('CreditBPO Notification: Reports Completed Today');
                        }
                    );

                }catch(Exception $e){
                //
                }

                if (!Mail::failures()) {
                    DB::table('tblemailnotif')->where('email_notif_id', $email->email_notif_id)->update(['notif_reports_completed_sent' => 1]);
                }
            }
        }
    }
}
