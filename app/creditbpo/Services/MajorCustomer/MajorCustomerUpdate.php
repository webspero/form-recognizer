<?php

namespace CreditBPO\Services\MajorCustomer;

use CreditBPO\Models\MajorCustomer;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\MajorCustomerValue;
use CreditBPO\Libraries\StringManipulator;

class MajorCustomerUpdate
{
    public function __construct()
    {
        $this->majorCustomerDb = new MajorCustomer();
        $this->reportDb = new Report();
        $this->majorCustomerValueLibrary = new MajorCustomerValue();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function getMajorSupplierReportId($id)
    {
        $majorCustomer = $this->majorCustomerDb->getMajorCustomerById($id);
        if (!is_null($majorCustomer)){
            return $majorCustomer['reportid'];
        } else {
            return false;
        }
    }

    public function checkReportIfAnonymous($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            /*Check if anonymous */
            if ($report->anonymous_report == INT_TRUE){
                return true;
            } else{
                return false;
            }
        } else{
            return null;
        }
    }

    public function validateRequestDetails($majorCustomerDto, $reportId)
    {
        /* If year started was updated, recompute years doing business */
        if (!is_null($majorCustomerDto->getYearStarted())){
            $majorCustomerDto->setYearsDoingBusiness(date('Y') - $majorCustomerDto->getYearStarted());
        }
        
        /* Check if customer name want to update, if report was set anonymous, name should not be updated */
        if (!is_null($majorCustomerDto->getName())){
            /* Major customer can be anonymous or not */
            $isAnonymous = $this->checkReportIfAnonymous($reportId);
            if ($isAnonymous){
                $validator = $this->validateMajorCustomerDetails($majorCustomerDto->getVars(), INT_TRUE);
            } else{
                $validator = $this->validateMajorCustomerDetails($majorCustomerDto->getVars(), INT_FALSE);
            }
        } else{
            $validator = $this->validateMajorCustomerDetails($majorCustomerDto->getVars());
        }

        return $validator;
    }

    public function validateMajorCustomerDetails($majorCustomerDto, $isAnonymous = INT_FALSE)
    {
        $errors = [];
        /* Sets Validation Messages */
        $messages = array(
            'yearstarted.date_format' => 'Invalid year format for year started.',
            'yearstarted.after' => 'Year cannot be lower than 1970.',
            'yearstarted.before' => 'Year cannot exceed the year now.',
            
            'salespercentage.numeric' => 'Sales Percentage must be a number.',
            
            'relationshipsatisfaction.in' => "Use only 'good', 'satisfactory', 'unsatisfactory' and 'bad' for Relationship Satisfaction.",
            'orderfrequency.in' => "Use only 'regularly', 'often' and 'rarely' for Order Frequency.",
            'paymentbehavior.in' => "Use only 'ahead of due date', 'on due date', 'portion settled on due date' and 'delinquent' for Payment Behavior."
        );

        $relationshipSatisfaction = array_keys($this->majorCustomerValueLibrary->relationshipSatisfaction);
        $customerOrderFrequency = array_keys($this->majorCustomerValueLibrary->customerOrderFrequency);
        $customerSettlement = array_keys($this->majorCustomerValueLibrary->customerSettlement);

        /* Sets Validation Rules */
        $rules = array(
            'email' => 'email',
            'yearstarted' => 'date_format:"Y"|after:1969|before:next year',
            'relationshipsatisfaction' => 'in:'. implode(',', $relationshipSatisfaction),
            'salespercentage' => 'numeric|min:0|max:100',
            'orderfrequency' => 'in:'. implode(',', $customerOrderFrequency),
            'paymentbehavior' => 'in:'. implode(',', $customerSettlement)
        );

        /*Convert strings to lower*/
        $details = $majorCustomerDto;
        $details['relationshipsatisfaction'] = $this->stringManipulatorLibrary->cleanString($details['relationshipsatisfaction']);
        $details['orderfrequency'] = $this->stringManipulatorLibrary->cleanString($details['orderfrequency']);
        $details['paymentbehavior'] = $this->stringManipulatorLibrary->cleanString($details['paymentbehavior']);

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }

        /* Check name validation if report is anonymous, name should not be editable */
        if ($isAnonymous === INT_TRUE && !is_null($majorCustomerDto['name'])){
            array_push($errors, 'Customer name cannot be updated because report was set to anonymous.');
        }

        return $errors;
    }

    public function updateReportMajorCustomer($id, $majorCustomerDto)
    {
        //a copy of the original request data, to be returned if process successfuly executes
        $majorCustomerRaw = $majorCustomerDto;

        /* Convert request value to database response */
        if (!is_null($majorCustomerDto->getOrderFrequency())){
            $majorCustomerDto->setOrderFrequency($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getOrderFrequency()));
        }
        if (!is_null($majorCustomerDto->getPaymentBehavior())){
            $majorCustomerDto->setPaymentBehavior($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getPaymentBehavior()));
        }
        if (!is_null($majorCustomerDto->getRelationshipSatisfaction())){
            $majorCustomerDto->setRelationshipSatisfaction($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getRelationshipSatisfaction()));
        }

        $data = [
            'customer_name' => $majorCustomerDto->getName(),
            'customer_share_sales' => $majorCustomerDto->getSalesPercentage(),
            'customer_address' => $majorCustomerDto->getAddress(),
            'customer_contact_person' => $majorCustomerDto->getContactPerson(),
            'customer_email' => $majorCustomerDto->getEmail(),
            'customer_phone' => $majorCustomerDto->getContactNumber(),
            'customer_started_years' => $majorCustomerDto->getYearStarted(),
            'customer_years_doing_business' => $majorCustomerDto->getYearsDoingBusiness(),
            'customer_settlement' => $majorCustomerDto->getPaymentBehavior(),
            'customer_order_frequency' => $majorCustomerDto->getOrderFrequency(),
            'relationship_satisfaction' => $majorCustomerDto->getRelationshipSatisfaction()
        ];
        
        $majorCustomerUpdateData = $this->majorCustomerDb->updateMajorCustomer($id, $data);
        /* Set DTO data */
        $updatedData = [
            'id' => $majorCustomerUpdateData['majorcustomerrid'],
            'reportid' => $majorCustomerUpdateData['entity_id'],
            'name' => $majorCustomerUpdateData['customer_name'],
            'address' => $majorCustomerUpdateData['customer_address'],
            'contactperson' => $majorCustomerUpdateData['customer_contact_person'],
            'contactnumber' => $majorCustomerUpdateData['customer_phone'],
            'email' => $majorCustomerUpdateData['customer_email'],
            'yearstarted' => $majorCustomerUpdateData['customer_started_years'],
            'yearsdoingbusiness' => $majorCustomerUpdateData['customer_years_doing_business'],
            'salespercentage' => $majorCustomerUpdateData['customer_share_sales'],
        ];
        $majorCustomerDto->setVars($updatedData);

        return $majorCustomerRaw;
    }
}