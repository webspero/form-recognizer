<?php

namespace CreditBPO\Services\MajorCustomer;

use CreditBPO\Models\MajorCustomer;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\MajorCustomerValue;
use CreditBPO\Libraries\StringManipulator;

class MajorCustomerInsert
{
    public function __construct()
    {
        $this->majorCustomerDb = new MajorCustomer();
        $this->reportDb = new Report();
        $this->majorCustomerValueLibrary = new MajorCustomerValue();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateMajorCustomerDetails($majorCustomerDto)
    {
        $errors = [];
        $isAnonymous = null;
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'address.required' => 'Address is required.',
            'contactperson.required' => 'Contact person is required.',
            'contactnumber.required' => 'Contact number is required.',
            'email.required' => 'Email is required.',
            'yearstarted.required' => 'Year Started Doing Business With customer is required.',
            'yearsdoingbusiness.required' => 'Years Doing Business With customer is required.',
            'relationshipsatisfaction.required' => 'Relationship Satisfaction is required.',
            'salespercentage.required' => 'Sales Percentage is required.',
            'orderfrequency.required' => 'Order Frequency is required.',
            'paymentbehavior.required' => 'Payment Behavior is required.',

            'yearstarted.date_format' => 'Invalid year format.',
            'yearstarted.after' => 'Year cannot be lower than 1970.',
            'yearstarted.before' => 'Year cannot exceed the year now.',
            
            'yearsdoingbusiness.numeric' => 'Year Started Doing Business must be a number.',
            'salespercentage.numeric' => 'Sales Percentage must be a number.',
            
            'relationshipsatisfaction.in' => "Use only 'Good', 'Satisfactory', 'Unsatisfactory' and 'Bad' for Relationship Satisfaction.",
            'orderfrequency.in' => "Use only 'Regularly', 'Often' and 'Rarely' for Order Frequency.",
            'paymentbehavior.in' => "Use only 'Ahead of Due Date', 'On Due Date', 'portion settled on Due Date', 'Settled past Due Date' and 'Delinquent' for Payment Behavior."
        );

        $relationshipSatisfaction = array_keys($this->majorCustomerValueLibrary->relationshipSatisfaction);
        $customerOrderFrequency = array_keys($this->majorCustomerValueLibrary->customerOrderFrequency);
        $customerSettlement = array_keys($this->majorCustomerValueLibrary->customerSettlement);

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'address' => 'required',
            'contactperson' => 'required',
            'contactnumber' => 'required',
            'email' => 'required|email',
            'yearstarted' => 'required|date_format:"Y"|after:1969|before:next year',
            'yearsdoingbusiness' => 'required|numeric',
            'relationshipsatisfaction' => 'required|in:'. implode(',', $relationshipSatisfaction),
            'salespercentage' => 'required|numeric|min:0|max:100',
            'orderfrequency' => 'required|in:'. implode(',', $customerOrderFrequency),
            'paymentbehavior' => 'required|in:'. implode(',', $customerSettlement)
        );

        /* Check first if report exist */
        /* Major customer can be anonymous or not */
        $isAnonymous = $this->checkReportIfAnonymous($majorCustomerDto['reportid']);
        if (!is_null($isAnonymous)){
            if ($isAnonymous === INT_FALSE){
                $messages['name.required'] = 'Customer name is required.';
                $rules['name'] = 'required';
            }
        } else{
            array_push($errors, "Report doesn't exist.");
        }
        
        /*Convert strings to lower and removes special characters*/
        $details = $majorCustomerDto;
        $details['relationshipsatisfaction'] = $this->stringManipulatorLibrary->cleanString($details['relationshipsatisfaction']);
        $details['orderfrequency'] = $this->stringManipulatorLibrary->cleanString($details['orderfrequency']);
        $details['paymentbehavior'] = $this->stringManipulatorLibrary->cleanString($details['paymentbehavior']);

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors,
            'anonymous' => $isAnonymous
        ];
        return $result;
    }

    public function checkReportIfAnonymous($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            /*Check if anonymous */
            return $report->anonymous_report;
        } else{
            return null;
        }
    }

    public function postReportMajorCustomer($majorCustomerDto, $isAnonymous)
    {
        /* If report is anonymous, set major customer name to 'MajorCustomer[customer count]' */
        if ($isAnonymous){
            $majorCustomers = $this->majorCustomerDb->getMajorCustomersByReportId($majorCustomerDto->getReportId());
            $customerCount = @count($majorCustomers) + 1;
            $majorCustomerDto->setName('Major Customer '.$customerCount);
        }

        
        //a copy of the original request data, to be returned if process successfuly executes
        $majorCustomerRaw = $majorCustomerDto;

        // convert user request data dropdowns to valid string index
        $majorCustomerDto->setRelationshipSatisfaction($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getRelationshipSatisfaction()));
        $majorCustomerDto->setOrderFrequency($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getOrderFrequency()));
        $majorCustomerDto->setPaymentBehavior($this->stringManipulatorLibrary->cleanString($majorCustomerDto->getPaymentBehavior()));

        /* Change user request data to database data */
        $relationshipSatisfaction = $this->majorCustomerValueLibrary->relationshipSatisfactionToDatabase($majorCustomerDto->getRelationshipSatisfaction()); 
        $orderFrequency = $this->majorCustomerValueLibrary->customerOrderFrequencyToDatabase($majorCustomerDto->getOrderFrequency());
        $customerSettlement = $this->majorCustomerValueLibrary->customerSettlementToDatabase($majorCustomerDto->getPaymentBehavior());

        // setting changed user request data using database data
        $majorCustomerDto->setRelationshipSatisfaction($relationshipSatisfaction);
        $majorCustomerDto->setOrderFrequency($orderFrequency);
        $majorCustomerDto->setPaymentBehavior($customerSettlement);

        /* Save data to database */
        $insertResponse = $this->majorCustomerDb->addNewMajorCustomer($majorCustomerDto);
        if (!is_null($insertResponse)){
            $majorCustomerDto->setId($insertResponse->majorcustomerrid);
        }

        /* Convert database value to readable response */
        return $majorCustomerRaw;
    }
}