<?php

namespace CreditBPO\Services\MajorCustomer;

use CreditBPO\Models\MajorCustomer;
use CreditBPO\Libraries\MajorCustomerValue;
use CreditBPO\DTO\MajorCustomerDTO;

class MajorCustomerList
{
    public function __construct()
    {
        $this->majorCustomerDb = new MajorCustomer();
        $this->majorCustomerValueLibrary = new MajorCustomerValue();
    }

    public function getReportMajorCustomerByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report major customers in tblmajorcustomer table */
        $majorCustomersList = $this->majorCustomerDb->getReportMajorCustomerByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $majorCustomerDtoList = [];
        foreach($majorCustomersList as $key => $value){
            $majorCustomerDto = new MajorCustomerDTO();
            $value['orderfrequency'] = $this->majorCustomerValueLibrary->customerOrderFrequencyToResponse($value['orderfrequency']);
            $value['relationshipsatisfaction'] = $this->majorCustomerValueLibrary->relationshipSatisfactionToResponse($value['relationshipsatisfaction']);
            $value['paymentbehavior'] = $this->majorCustomerValueLibrary->customerSettlementToResponse($value['paymentbehavior']);
            $majorCustomerDto->setVars($majorCustomersList[$key]);

            array_push($majorCustomerDtoList, $majorCustomerDto->getVars());
        }
        $results['total'] = $this->majorCustomerDb->getMajorCustomersByReportId($reportId)->count();  
        $results['items'] = $majorCustomerDtoList;

        return $results;
    }

    public function getReportMajorCustomerList($reportId){
        $results = array();
        $results['page'] = 0;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['items'] = array();

        $majorCustomersList = $this->majorCustomerDb->getMajorCustomersByReportId($reportId);

        $majorCustomerDtoList = [];

        foreach($majorCustomersList as $key => $value){
            $majorCustomerDto = new MajorCustomerDTO();
            
            $value['orderfrequency'] = $this->majorCustomerValueLibrary->customerOrderFrequencyToResponse($value['orderfrequency']);
            $value['relationshipsatisfaction'] = $this->majorCustomerValueLibrary->relationshipSatisfactionToResponse($value['relationshipsatisfaction']);
            $value['paymentbehavior'] = $this->majorCustomerValueLibrary->customerSettlementToResponse($value['paymentbehavior']);
            
            $majorCustomerDto->setVars($majorCustomersList[$key]);

            array_push($majorCustomerDtoList, $majorCustomerDto->getVars());
        }

        $results['total'] = $majorCustomersList->count();
        $results['items'] = $majorCustomerDtoList;
        $results['page'] = 1;
        $results['limit'] = $majorCustomersList->count();

        return $results;

    }

    public function getMajorCustomerById($id)
    {
        /* Get report major customer in tblmajorcustomer table */
        $majorCustomer = $this->majorCustomerDb->getMajorCustomerById($id);
        if (!is_null($majorCustomer)){
            $majorCustomerDto = new MajorCustomerDTO;
            $majorCustomerDto->setVars($majorCustomer);

            /* Change relationship satisfaction, order frequency and payment behavior to readable string */
            $majorCustomerDto->setOrderFrequency($this->majorCustomerValueLibrary->customerOrderFrequencyToResponse($majorCustomerDto->getOrderFrequency()));
            $majorCustomerDto->setRelationshipSatisfaction($this->majorCustomerValueLibrary->relationshipSatisfactionToResponse($majorCustomerDto->getRelationshipSatisfaction()));
            $majorCustomerDto->setPaymentBehavior($this->majorCustomerValueLibrary->customerSettlementToResponse($majorCustomerDto->getPaymentBehavior()));
            return $majorCustomerDto;
        }
        return null;
    }
}