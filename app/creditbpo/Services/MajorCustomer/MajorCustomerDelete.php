<?php

namespace CreditBPO\Services\MajorCustomer;

use CreditBPO\Models\MajorCustomer;

class MajorCustomerDelete
{
    public function __construct()
    {
        $this->majorCustomerDb = new MajorCustomer();
    }

    /* Current process, the data will be deleted */
    public function deleteMajorCustomer($id)
    {
        return $this->majorCustomerDb->deleteMajorCustomer($id);
    }

    /*
    * The right process, the data should not be deleted. It should only update database field is_deleted
    * See Eloquent Soft Delete or use custom update like below
    */

    /*
    * public function deleteMajorCustomer($id)
    * {
    *    $data = [
    *        'is_delete' = 1
    *    ];
    *    return $this->majorCustomerDb->updateMajorCustomer($id, $data);
    *}
    */

}