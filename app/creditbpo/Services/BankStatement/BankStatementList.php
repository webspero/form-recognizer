<?php

namespace CreditBPO\Services\BankStatement;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;

class BankStatementList
{
    CONST UPLOAD_DESTINATION_PATH = 'documents';

    public function __construct()
    {
        $this->documentDb = new Document;
        $this->reportDb = new Report;
    }

    public function validateReportIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function getReportBSList($reportId)
    {
        $bsDocument = $this->documentDb->getBsDocumentByReportId($reportId);
        if (!$bsDocument->isEmpty()){
            $returnList = array(
                'reportid' => $reportId,
                'bankstatements' => array()
            );
            foreach($bsDocument as $document){
                /* Manage return value */
                $returnValue = array(
                    'id' => $document->documentid,
                    'document' => $document->document_orig,
                    'downloadlink' => route('reportdocument.download', ['reportid' => $reportId, 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $document->document_orig])
                );
                array_push($returnList['bankstatements'], $returnValue);
            }
            return $returnList;
        } else{
            return null;
        }
    }
}