<?php

namespace CreditBPO\Services\BankStatement;

use Validator;
use CreditBPO\Models\Document;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Libraries\MimeTypes;
use CreditBPO\DTO\BSUploadDTO;

class BankStatementUpload
{
    CONST UPLOAD_DESTINATION_PATH = 'documents';
    protected $bsDestinationPath;

    public function __construct()
    {
        $this->reportDb = new Report;
        $this->documentDb = new Document;
        $this->mimeTypesLibrary = new MimeTypes;
    }

    public function manageRequestDetails($request, $reportId)
    {
        /*Fill up array of dto*/
        $bsUploadDtoList = [];
        /* Get bank statements details */
        if (array_key_exists('bankstatements', $request)){
            /* Check if array */
            $bankStatement = $request['bankstatements'];
            if (is_array($bankStatement)){
                foreach($bankStatement as $key => $value){
                    if ($value){
                        $bsUploadDto = new BSUploadDTO;
                        $value['reportid'] = $reportId;
                        /* Check if mime/file type in is field file */
                        $parts = explode(";base64,", $value['file']);
                        if (@count($parts) > 1){
                            $value['file'] = $parts[1];
                            /* Get the file type */
                            $value['filetype'] = explode(":", $parts[0])[1];
                        }
                        // $value['file'] = base64_decode($value['file']);
                        $bsUploadDto->setVars($value);
                        array_push($bsUploadDtoList, $bsUploadDto);
                    }
                }
            }
        }
        return $bsUploadDtoList;
    }

    public function validateRequestData($bsUploadDtoList)
    {
        $errors = [];
        /* Check if request has valid report id */
        $report = $this->reportDb->getReportByReportId($bsUploadDtoList[0]->getReportId());
        if (is_null($report)){
            array_push($errors, "Report ID doesn't exist.");
        }
        
        $mimeTypes = array_keys($this->mimeTypesLibrary->mimeTypes);
        foreach($bsUploadDtoList as $key => $value){
            if ($value){
                $rules['file'] = 'required';
                $rules['filetype'] = 'required|in:'.implode(',',$mimeTypes);
                $rules['date'] = 'required|date_format:Y-m-d|before:tomorrow';

                $messages['file.required'] = "File for bank statement ".($key+1)." is required.";
                $messages['filetype.required'] = "File type for bank statement ".($key+1)." is required.";
                $messages['date.required'] = "Date for bank statement ".($key+1)." is required.";
                $messages['filetype.in'] = "File type for bank statement ".($key+1)." is not allowed. File should be a PDF, Excel, CSV, Zip or Rar.";
                $messages['date.date_format'] = "Date for bank statement ".($key+1)." should be yyyy-mm-dd.";
                $messages['date.before'] = "Date for bank statement ".($key+1)." cannot be later than the date today.";
                $validator = Validator::make($value->getVars(), $rules, $messages);
            }
            if ($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }
        return $errors;
    }

    public function setBsDestinationPath($reportId)
    {
        $this->bsDestinationPath = self::UPLOAD_DESTINATION_PATH.'/'.$reportId;
    }

    public function uploadBankStatements($bsUploadDtoList)
    {
        /*Setup upload destination path */
        $this->setBsDestinationPath($bsUploadDtoList[0]->getReportId());

        /*Check if folder exist, if not, create folder
        * 0775 is a php code for "Everything for owner, read and execute for others" */
        if (!file_exists($this->bsDestinationPath)) {
            mkdir($this->bsDestinationPath, 0777, true);
            chmod($this->bsDestinationPath, 0775);
        }

        $returnList = array(
            'reportid' => $bsUploadDtoList[0]->getReportId(),
            'bankstatements' => array()
        );
        /* Set up bank statement information */
        foreach ($bsUploadDtoList as $key => $value) {
            if ($value) {
                $extension = $this->mimeTypesLibrary->mimeTypes[$value->getFileType()];
                $label = 'Bank Statement for report dated '.$value->getDate();
                $filename = $label.'.'.$extension;
                $value->setFilename($filename);
                $value->setFileRename($value->getReportId().'/'.$filename);

                /* Upload to database */
                $document = $this->documentDb->saveDocument($value->getVars());
                $value->setDocumentId($document['documentid']);
                /* Save to server folder */
                $value->getFile()->move($this->bsDestinationPath.'/',$filename);
                
                /* Manage return value */
                $returnValue = array(
                    'id' => $value->getDocumentId(),
                    'document' => $filename,
                    'downloadlink' => route('reportdocument.download', ['reportid' => '', 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $filename])
                );
                array_push($returnList['bankstatements'], $returnValue);
            }
        }
        return $returnList;
    }
}