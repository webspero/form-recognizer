<?php

namespace CreditBPO\Services\FinancialRawFile;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\InnodataFile;

class FinancialRawList
{
    CONST UPLOAD_DESTINATION_PATH = 'documents/innodata/to_innodata';

    public function __construct(){
        $this->reportDb = new Report;
        $this->innodataDb = new InnodataFile;
    }

    public function validateReportIfExist($reportId){
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function getRawFileList($reportId){
        $rawFile = $this->innodataDb->getInnodataFilesById($reportId);
        if (!$rawFile->isEmpty()){
            $returnList = array(
                'reportid' => $reportId,
                'financialrawfile' => array()
            );

            foreach($rawFile as $raw){
                /* Manage return value */
                $returnValue = array(
                    'id' => $raw->id,
                    'document' => $raw->file_name,
                    'downloadlink' => route('reportdocument.download', ['reportid' => $reportId, 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $raw->file_name])
                );
                array_push($returnList['financialrawfile'], $returnValue);
            }
            return $returnList;
        }
        return null;
    }


}