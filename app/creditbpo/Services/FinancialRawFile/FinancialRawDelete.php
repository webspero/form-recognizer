<?php

namespace CreditBPO\Services\FinancialRawFile;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\InnodataFile;
use File;

class FinancialRawDelete
{
    CONST UPLOAD_DESTINATION_PATH = 'documents/innodata/to_innodata';
    
    public function __construct(){
        $this->reportDb = new Report;
        $this->innodataDb = new InnodataFile;
    }

    public function validateReportOwner($userid, $reportid){
        $errorMessages = array();
        $report = $this->reportDb->getReportByReportId($reportid);
        if($report){
            if($report->loginid != $userid){
                $errorMessages['errors'] = "Report does not belong to the user.";
                $errorMessages['code'] = HTTP_STS_FORBIDDEN;
            }elseif($report->status == 7){
                $errorMessages['errors'] = "Report is already done.";
                $errorMessages['code'] = HTTP_STS_FORBIDDEN;
            }else{
                $errorMessages = null;
            }
        }else{
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        }
        return $errorMessages;
    }

    public function validateInnodataFile($fileid){
        $innodata = InnodataFile::where('id', $fileid)->first();
        $errors = array();

        /** Check if financial raw file exists */
        if($innodata){
            /** Check if file is deleted */
            if($innodata->is_deleted != 0){
                array_push($errors, 'Financial Raw File is already deleted!');
            }
        }else{
            array_push($errors, 'Financial Raw File doesnt exist!');
        }

        return $errors;
    }

    public function deleteInnoFile($id){
        $innodata = InnodataFile::where('id', $id)->first();
        $this->innodataDb->deleteRawFile($id);
        $file = self::UPLOAD_DESTINATION_PATH.'/'.$innodata->file_name;
        if(file_exists($file)){
            File::delete($file);
        }
        return;
    }
}