<?php

namespace CreditBPO\Services\FinancialRawFile;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\InnodataFile;
use CreditBPO\Libraries\MimeTypes;
use CreditBPO\DTO\FSRawUploadDTO;
use Validator;

class FinancialRawUpload
{
    CONST UPLOAD_DESTINATION_PATH = 'documents/innodata/to_innodata';
    protected $bsDestinationPath;

    public function __construct(){
        $this->reportDb = new Report;
        $this->innodataDb = new InnodataFile;
        $this->mimeTypesLibrary = new MimeTypes;
    }

    public function manageRequestDetails($request, $reportId){

        $frRawUploadDTO = new FSRawUploadDTO();

        /* Check and get request file */
        if (array_key_exists('file', $request) && array_key_exists('filetype', $request)){
            /** Set file and file extension*/
            $frRawUploadDTO->setFileExtension($this->mimeTypesLibrary->mimeTypes[$request['filetype']]);
            $frRawUploadDTO->setFile($request['file']);
            $frRawUploadDTO->setFileType($request['filetype']);
            $frRawUploadDTO->setReportId($reportId);
            return $frRawUploadDTO;
        }else{
            return null;
        }
        
        // print_r($request); die();
        /*Fill up array of dto*/
        // $rawFileUploadDtoList = [];

        // /** Get Financial Raw File details */
        // if (array_key_exists('rawfile', $request)){
        //     /* Check if array */
        //     $bankStatement = $request['rawfile'];
        //     if (is_array($bankStatement)){
        //         foreach($bankStatement as $key => $value){
        //             if ($value){
        //                 $rawUploadDto = new FSRawUploadDTO;
        //                 $value['reportid'] = $reportId;
        //                 /* Check if mime/file type in is field file */
        //                 $parts = explode(";base64,", $value['file']);
        //                 if (@count($parts) > 1){
        //                     $value['file'] = $parts[1];
        //                     /* Get the file type */
        //                     $value['filetype'] = explode(":", $parts[0])[1];
        //                 }
        //                 // $value['file'] = base64_decode($value['file']);
        //                 $rawUploadDto->setVars($value);
        //                 array_push($rawFileUploadDtoList, $rawUploadDto);
        //             }
        //         }
        //     }
        // }
        return $rawFileUploadDtoList;
    }

    public function validateRequestData($financialDtoList){

        $errors = [];

        /* Check if the uploaded has right file format */
        if (!in_array($financialDtoList->getFileExtension(), ['xlsx', 'xls', 'pdf' ])){
            array_push($errors, "The file you were trying to upload is not allowed extension.");
        }

        $data = array(
            'filetype'  => $financialDtoList->getFileType(),
            'file'  => $financialDtoList->getFile()
        );

        $mimeTypes = array_keys($this->mimeTypesLibrary->mimeTypes);
        $rules['file'] = 'required';
        $rules['filetype'] = 'required|in:'.implode(',',$mimeTypes);
        $messages['file.required'] = "File for financial raw file is required.";
        $messages['filetype.required'] = "File type for financial raw file is required.";

        // print_r($financialDtoList); die();
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }

        // $mimeTypes = array_keys($this->mimeTypesLibrary->mimeTypes);
        // foreach($financialDtoList as $key => $value){
        //     if ($value){
        //         $rules['file'] = 'required';
        //         $rules['filetype'] = 'required|in:'.implode(',',$mimeTypes);

        //         $messages['file.required'] = "File for bank statement ".($key+1)." is required.";
        //         $messages['filetype.required'] = "File type for bank statement ".($key+1)." is required.";
        //         $messages['filetype.in'] = "File type for bank statement ".($key+1)." is not allowed. File should be a PDF, Excel, CSV, Zip or Rar.";
        //         $validator = Validator::make($value->getVars(), $rules, $messages);
        //     }
        //     if ($validator->fails()){
        //         foreach($validator->messages()->all() as $error){
        //             array_push($errors, $error);
        //         }
        //     }
        // }
        return $errors;
    }

    public function setBsDestinationPath($reportId){
        $this->bsDestinationPath = self::UPLOAD_DESTINATION_PATH;
    }

    public function uploadFinancialRawFileList($financialDtoList){
        /*Setup upload destination path */
        $this->setBsDestinationPath($financialDtoList->getReportId());

        $extension = $this->mimeTypesLibrary->mimeTypes[$financialDtoList->getFileType()];
        $filename = $financialDtoList->getReportId(). '-'. pathinfo($financialDtoList->getFile()->getClientOriginalName(), PATHINFO_FILENAME).'.'.$extension;
        $financialDtoList->setFilename($filename);

        /* Upload to database */

        $data = array(
            'reportid'  => $financialDtoList->getReportId(),
            'file' => $financialDtoList->getFile(),
            'filetype' => $financialDtoList->getFileType(),
            'documentfilename' => $financialDtoList->getFilename(),
            'uploadtype' => $financialDtoList->getUploadType(),
            // 'documentid' => $financialDtoList->getDocumentId()
        );

        // print_r($data); die();
        $innodata = $this->innodataDb->saveInnodataFile($data);
        $financialDtoList->setDocumentId($innodata['id']);

        $returnValue = array(
            'id' => $financialDtoList->getDocumentId(),
            'document' => $filename,
            'downloadlink' => route('reportdocument.download', ['reportid' => $financialDtoList->getReportId(), 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $filename])
        );


        // $returnList = array(
        //     'reportid' => $financialDtoList[0]->getReportId(),
        //     'rawfile' => array()
        // );

        /* Set up financial raw file information */
        // foreach ($financialDtoList as $key => $value) {
        //     if ($value) {
        //         $extension = $this->mimeTypesLibrary->mimeTypes[$value->getFileType()];
        //         // $label = 'Bank Statement for report dated '.$value->getDate();
        //         $filename = $value->getReportId(). '-'. pathinfo($value->getFile()->getClientOriginalName(), PATHINFO_FILENAME).'.'.$extension;

        //         $value->setFilename($filename);

        //         /* Upload to database */
        //         $innodata = $this->innodataDb->saveInnodataFile($value->getVars());
        //         $value->setDocumentId($innodata['id']);

        //         // /* Save to server folder */
        //         $value->getFile()->move($this->bsDestinationPath.'/',$filename);
                
        //         // /* Manage return value */
        //         $returnValue = array(
        //             'id' => $value->getDocumentId(),
        //             'document' => $filename,
        //             'downloadlink' => route('reportdocument.download', ['reportid' => $value->getReportId(), 'path' => self::UPLOAD_DESTINATION_PATH, 'filename' => $filename])
        //         );
        //         array_push($returnList['rawfile'], $returnValue);
        //     }
        // }
        return $returnValue;
    }

    public function validateReport($reportId, $userId){
        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } elseif($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } elseif($checkReportOwner->status == 7){
            $errorMessages['errors'] = "Report is already done.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }
}
