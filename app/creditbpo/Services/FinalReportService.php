<?php

namespace CreditBPO\Services;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Handlers\SMEInternalHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use PDFMerger;
use Response;
use File;
use DB;

class FinalReportService
{
	public function __construct(){
		$this->reportDb = new Report;
		$this->smeInternalHandler = new SMEInternalHandler;
	}

	public function validateReport($reportId, $userId){
		$errorMessages = array();
		$report = $this->reportDb->getReportByReportId($reportId);
		if(!$report){
			$errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
		}elseif($report->loginid != $userId){
			$errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
		}elseif($report->status != 7){
			$errorMessages['errors'] = "Report is not yet done. Please complete the report first.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
		}else{
			$errorMessages = null;
		}
		
		return $errorMessages;
	}

	public function validateFinancialResultDetails($entityid){
		
		$validationErrors = [];

		$entity = $this->reportDb->getReportByReportId($entityid);

		if($entity->status == 7 && !empty($entity->cbpo_pdf) && (!empty($entity->financial_analysis_pdf))){
			$validationErrors[] = "There is already an existing financial report for entity ".$entityid;
		}
		
		//validate business details 1
		if(empty($entity->industry_main_id))
			$validationErrors[] = "Industry Section is required";

		if(empty($entity->industry_sub_id))
			$validationErrors[] = "Industry Division is required";

		if(empty($entity->industry_row_id))
			$validationErrors[] = "Industry Group is required";

		if(empty($entity->company_tin))
			$validationErrors[] = "Company TIN is required";

		if(empty($entity->tin_num))
			$validationErrors[] = "Business Registration No. is required";

		if(empty($entity->sec_reg_date))
			$validationErrors[] = "Business Registration Date is required";

		if(empty($entity->email))
			$validationErrors[] = "Company Email is required";

		if(empty($entity->address1))
			$validationErrors[] = "Company Address is required";

		if(empty($entity->phone))
			$validationErrors[] = "Primary Business Address, Telephone is required";

		// if(empty($entity->fs_input_who))
		// 	$validationErrors[] = "Who will be filling up the Financial Input Template? is required";

		//validate financial statement raw file
		$fsPdfFiles = DB::table('innodata_files')->where('is_deleted', 0)->where('entity_id', $entityid)->get()->toArray();
		if(empty($fsPdfFiles))
			$validationErrors[] = "Financial Statement Raw File is required";
		
		$result = [
            'errors' => $validationErrors
        ];

        return $result;
	}

	public function downloadFinalReport($reportId){
		/*  Fetch the report information from the database  */
		$lang = app()->getLocale();
		$report = $this->reportDb->getReportByReportId($reportId);
		$headers = [
            'Content-Type: application/pdf',
        ];

		$pdfMerge = new PDFMerger();

		$entity_data = Report::where('entityid', $reportId)->first();
		
		if(empty($report->cbpo_pdf)){

	        $SME = new SMEInternalHandler();
	        $file = $SME->createSMEInternalReport($reportId);

	        $pdfMerge = new PDFMerger();        

	        if ($file) {
				if($entity_data->is_premium != SIMPLIFIED_REPORT){
					if($report->financial_analysis_pdf){
						$fname = $report->financial_analysis_pdf;
						if(strpos($fname,"&") !== false){
							$fname  = preg_replace("/[&]/", "And", $fname);
						}
						$finAnFile = public_path().'/financial_analysis/'.$fname;
				
						$pdfMerge->addPDF($file, 'all');
						$pdfMerge->addPDF($finAnFile);
				
						$pdfMerge->merge('file', $file);

						Report::where('entityid',$reportId)->update(['cbpo_pdf'=>basename($file)]);
					}else{
						$financialAnalysis = new FinancialAnalysisInternalHandler();
						$financial_report_id = FinancialReport::where(['entity_id' => $reportId, 'is_deleted' => 0])->pluck('id')[0];
						$fr = FinancialReport::find($financial_report_id);
						$frfile = $financialAnalysis->createFinancialAnalysisInternalReport($financial_report_id,$lang);
						$fname = basename($frfile);

						if($lang == "fil"){
							Report::where('entityid',$reportId)->update(['financial_analysis_pdf_tl'=>basename($frfile)]);
						}else{
							Report::where('entityid',$reportId)->update(['financial_analysis_pdf'=>basename($frfile)]);
						}   

						$finAnFile = public_path().'/financial_analysis/'.$fname;
				
						$pdfMerge->addPDF($file, 'all');
						$pdfMerge->addPDF($finAnFile);
				
						$pdfMerge->merge('file', $file);

						Report::where('entityid',$reportId)->update(['cbpo_pdf'=>basename($file)]);
					}
				}

	            $filepath = public_path().'/documents/'.basename($file);
	        }
    	}else{
			$filepath = public_path().'/documents/'.basename($report->cbpo_pdf);
    	}

        $newfile = File::get($filepath);

        $response = Response::make($newfile, 200);

		return $filepath;	
	}

}