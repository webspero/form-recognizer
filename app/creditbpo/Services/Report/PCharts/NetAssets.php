<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\libraries\pChart;
use App\libraries\pData;
use CreditBPO\Models\IncomeStatement;

class NetAssets extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of net assets and issued capital';
    const CHART_TITLE_TL = 'Dynamics ng net assets at inisyu na kapital';
    const FILENAME_FORMAT = 'graph_1_2_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        // $sheet = BalanceSheet::where('financial_report_id', $id)
        //                     ->where('Assets', '!=', 0)
        //                     ->where('Liabilities', '!=', 0)
        //                     ->orderBy('index_count', 'asc')
        //                     ->get();
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);
        $sheet = $financialStatement['balance_sheets'];

        $count = @count($sheet) - 1;
        $netAssetsSeries = [];
        $issueCapitalSeries = [];
        $reportYearSeries = [];

        for ($id = $count; $id >= 0; $id--) {
            $intangible = $sheet[$id]->Goodwill + $sheet[$id]->IntangibleAssetsOtherThanGoodwill;
            $netAssetsSeries[] = $sheet[$id]->Equity - $intangible;
            $issueCapitalSeries[] = $sheet[$id]->IssuedCapital;
            $yearDisplayed = $report->year - $id;
            $reportYearSeries[] = '12/31/'.$yearDisplayed;
        }

        $yaxisdisplay = 'PHP';

        // Get Highest value
        $netAssetsSeriesMax = max($netAssetsSeries);
        $issueCapitalSeriesMax = max($issueCapitalSeries);
        $finalMax = max(array($netAssetsSeriesMax, $issueCapitalSeriesMax));

        //Get minimum value
        $netAssetsSeriesMin = min($netAssetsSeries);
        $issueCapitalSeriesMin = min($issueCapitalSeries);
        $finalMin = min(array($netAssetsSeriesMin, $issueCapitalSeriesMin));

        //get length of the highest amount
        if(round($finalMax,0) <= 0){
            $finLength = strlen((string)round($finalMin,0));
            $finLength -= 1;
        }else{
            $finLength = strlen((string)round($finalMax,0));
        }

        $yaxisdisplay = "One PHP";
        $units_value = 1;
        $units_length = 1;

        if($finLength == 1){
            $yaxisdisplay = "One PHP";
            $units_value = 1;
            $units_length = 1;
        }elseif($finLength == 2){
            $yaxisdisplay = "Ten PHP";
            $units_value = 10;
            $units_length = 2;
        }elseif($finLength == 3){
            $yaxisdisplay = "Hundred PHP";
            $units_value = 100;
            $units_length = 3;
        }else if($finLength >= 4 && $finLength <=6){
            $yaxisdisplay = "Thousand PHP";
            $units_value = 1000;
            $units_length = 4;
        }else if($finLength >= 7 && $finLength <=9){
            $yaxisdisplay = "Million PHP";
            $units_value = 1000000;
            $units_length = 6;
        }else if($finLength >= 10 && $finLength <= 12){
            $yaxisdisplay = "Billion PHP";
            $units_value = 1000000000;
            $units_length = 9;
        }else if($finLength >= 13 && $finLength <= 15){
            $units_value = 1000000000000;
            $units_length = 12;
            $yaxisdisplay = "Trillion PHP";
        }else if($finLength >= 16 && $finLength <= 18){
            $yaxisdisplay = "Quadrillion PHP";
            $units_value = 1000000000000000;
            $units_length = 15;
        }

        $data = array(
            'netAssetsSeries'       => $netAssetsSeries,
            'issueCapitalSeries'    => $issueCapitalSeries,
            'issuedCapitalBegin'    => round($sheet[0]->IssuedCapital,0),
            'units_value'           => $units_value,
            'units_length'          => $units_length
        );

        if($finLength >= 1 && $finLength <=3){
            $netAssetsSeries = $data['netAssetsSeries'];
            $issueCapitalSeries = $data['issueCapitalSeries'];
            $issuedCapitalBegin = $data['issuedCapitalBegin'];
        }else{
            $result = $this->convertGraphUnits($data);
            $netAssetsSeries = $result['netAssetsSeries'];
            $issueCapitalSeries = $result['issueCapitalSeries'];
            $issuedCapitalBegin = $result['issuedCapitalBegin'];
        }

        $dataSet = new pData();
        $dataSet->AddPoint($netAssetsSeries, 'Serie1');
        $dataSet->AddPoint($issueCapitalSeries, 'Serie2');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYearSeries, 'Serie3');
        $dataSet->SetAbsciseLabelSerie('Serie3');
        $dataSet->SetSerieName('Net worth (net assets)', 'Serie1');
        $dataSet->SetSerieName('Issued (share) capital', 'Serie2');
        $dataSet->SetYAxisName($yaxisdisplay);
        $dataSet->SetXAxisFormat('');
        $dataSet->SetXAxisUnit('');
        $dataSet->SetYAxisFormat('');
        $dataSet->SetYAxisUnit('');

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(190, 120, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_START0,
            150,
            150,
            150,
            true,
            0,
            0,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->drawBarGraph($dataSet->GetData(), $dataSet->GetDataDescription(), true);
        $chart->drawTreshold($issuedCapitalBegin, 143, 55, 72, true, true);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 540, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(50, 55, $chart_title_constant, 50, 50, 50, 485);
        $chart->Render($filepath);

        return $filepath;
    }

    public function convertGraphUnits($data) {
        $units_value = $data['units_value'];
        $netAssetsSeries = $data['netAssetsSeries'];
        $issueCapitalSeries = $data['issueCapitalSeries'];
        $issuedCapitalBegin = $data['issuedCapitalBegin'];

        // Net asset series
        foreach($netAssetsSeries as $key => $nA){
            $naLength = strlen((string)round($nA,0));
            $displayValue = 0;
            do{
                if($nA < 0){
                    $nA += $units_value;
                    $displayValue += ($units_value * -1);
                    $naLength = strlen((string)round($nA,0)) - 1; 
                }elseif($nA == 0){
                    $netAssetsSeries[$key] = 0;
                    continue;
                }else{
                    $nA -= $units_value;
                    $displayValue += $units_value;
                    $naLength = strlen((string)round($nA,0));
                }
            }while($naLength > $data['units_length']);
            $netAssetsSeries[$key] = $displayValue / $units_value;
        }

        // Issued Capital Series
        foreach($issueCapitalSeries as $key => $ic){
            $icLength = strlen((string)round($ic,0));
            $displayValue = 0;
            do{
                if($ic < 0){
                    $ic += $units_value;
                    $displayValue += ($units_value * -1);
                    $icLength = strlen((string)round($ic,0)) - 1; 
                }elseif($ic == 0){
                    $issueCapitalSeries[$key] = 0;
                    continue;
                }else{
                    $ic -= $units_value;
                    $displayValue += $units_value;
                    $icLength = strlen((string)round($ic,0));
                }
            }while($icLength > $data['units_length']);
            $issueCapitalSeries[$key] = $displayValue / $units_value;
        }

        // Start issued capital
        $issuedCapitalBegin = $issuedCapitalBegin / $units_value;

        $result = array(
            'netAssetsSeries' => $netAssetsSeries,
            'issueCapitalSeries' => $issueCapitalSeries,
            'issuedCapitalBegin' => round($issuedCapitalBegin,2)
        );
        return $result;
    }
}