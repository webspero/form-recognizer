<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Models\BalanceSheet;
use App\libraries\pChart;
use App\libraries\pData;

class RevenueNetProfit extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of revenue and net profit';
    const CHART_TITLE_TL = 'Dinamika ng kita at netong kita';
    const FILENAME_FORMAT = 'graph_2_1_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);

        $incomeData = $financialStatement['income_statements'];
        $balanceSheet = $financialStatement['balance_sheets'];

        $count = @count($incomeData) - 1;
        $revenue = [];
        $comprehensiveIncome = [];
        $reportYear = [];

        for ($id = $count; $id >= 0; $id--) {
            $revenue[] = $incomeData[$id]->Revenue;
            $comprehensiveIncome[] = $incomeData[$id]->ComprehensiveIncome;
            $yearDisplayed = $report->year - $id;
            $reportYear[] = $yearDisplayed;
        }

        $revenue_label = 'Revenue';
        $comprehensive_label = 'Comprehensive Income';
        $thousand_label = 'thousand';

        if(session('fa_report_lang') == 'tl'){
            $revenue_label = 'Kita';
            $comprehensive_label = 'Komprehensibong kita';
            $thousand_label = 'libo';
        }

        $yaxisdisplay = 'PHP';

        // Get Highest value
        $revenueMax = max($revenue);
        $comprehensiveIncomeMax = max($comprehensiveIncome);
        $finalMax = max(array($revenueMax, $comprehensiveIncomeMax));

        //Get minimum value
        $revenueMin = min($revenue);
        $comprehensiveIncomeMin = min($comprehensiveIncome);
        $finalMin = min(array($revenueMin, $comprehensiveIncomeMin));

        //get length of the highest amount
        if(round($finalMax,0) <= 0){
            $finLength = strlen((string)round($finalMin,0));
            $finLength -= 1;
        }else{
            $finLength = strlen((string)round($finalMax,0));
        }

        if($finLength == 1){
            $yaxisdisplay = "One PHP";
            $units_value = 1;
            $units_length = 1;
        }elseif($finLength == 2){
            $yaxisdisplay = "Ten PHP";
            $units_value = 10;
            $units_length = 2;
        }elseif($finLength == 3){
            $yaxisdisplay = "Hundred PHP";
            $units_value = 100;
            $units_length = 3;
        }else if($finLength >= 4 && $finLength <=6){
            $yaxisdisplay = "Thousand PHP";
            $units_value = 1000;
            $units_length = 4;
        }else if($finLength >= 7 && $finLength <=9){
            $yaxisdisplay = "Million PHP";
            $units_value = 1000000;
            $units_length = 6;
        }else if($finLength >= 10 && $finLength <= 12){
            $yaxisdisplay = "Billion PHP";
            $units_value = 1000000000;
            $units_length = 9;
        }else if($finLength >= 13 && $finLength <= 15){
            $units_value = 1000000000000;
            $units_length = 12;
            $yaxisdisplay = "Trillion PHP";
        }else if($finLength >= 16 && $finLength <= 18){
            $yaxisdisplay = "Quadrillion PHP";
            $units_value = 1000000000000000;
            $units_length = 15;
        }

        $data = array(
            'revenue' => $revenue,
            'comprehensiveIncome' => $comprehensiveIncome,
            'units_value'           => $units_value,
            'units_length'          => $units_length
        );

        if($finLength >= 1 && $finLength <=3){
            // default
        }else{
            $result = $this->convertGraphUnits($data);
            $revenue = $result['revenue'];
            $comprehensiveIncome = $result['comprehensiveIncome'];
        }

        $dataSet = new pData();
        $dataSet->AddPoint($revenue, 'Serie1');
        $dataSet->AddPoint($comprehensiveIncome, 'Serie2');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie3');
        $dataSet->SetAbsciseLabelSerie('Serie3');
        $dataSet->SetSerieName($revenue_label, 'Serie1');
        $dataSet->SetSerieName($comprehensive_label, 'Serie2');
        $dataSet->SetYAxisName($yaxisdisplay);

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(190, 120, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_START0,
            150,
            150,
            150,
            true,
            0,
            2,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->drawBarGraph($dataSet->GetData(), $dataSet->GetDataDescription(), true);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 540, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(50, 55, $chart_title_constant, 50, 50, 50, 485);
        $chart->Render($filepath);

        return $filepath;
    }

    public function convertGraphUnits($data){
        $units_value = $data['units_value'];
        $revenue = $data['revenue'];
        $comprehensiveIncome = $data['comprehensiveIncome'];

        // Revenue
        foreach($revenue as $key => $rv){
            $rvLength = strlen((string)round($rv,0));
            $displayValue = 0;
            do{
                if($rv < 0){
                    $rv += $units_value;
                    $displayValue += ($units_value * -1);
                    $rvLength = strlen((string)round($rv,0)) - 1; 
                }elseif($rv == 0){
                    $revenue[$key] = 0;
                    continue;
                }else{
                    $rv -= $units_value;
                    $displayValue += $units_value;
                    $rvLength = strlen((string)round($rv,0));
                }
            }while($rvLength > $data['units_length']);
            $revenue[$key] = $displayValue / $units_value;
        }

        // Comprehensive Income
        foreach($comprehensiveIncome as $key => $ci){
            $ciLength = strlen((string)round($ci,0));
            $displayValue = 0;
            do{
                if($ci < 0){
                    $ci += $units_value;
                    $displayValue += ($units_value * -1);
                    $ciLength = strlen((string)round($ci,0)) - 1; 
                }elseif($ci == 0){
                    $comprehensiveIncome[$key] = 0;
                    continue;
                }else{
                    $ci -= $units_value;
                    $displayValue += $units_value;
                    $ciLength = strlen((string)round($ci,0));
                }
            }while($ciLength > $data['units_length']);
            $comprehensiveIncome[$key] = $displayValue / $units_value;
        }

        $result = array(
            'revenue'   => $revenue,
            'comprehensiveIncome'   => $comprehensiveIncome
        );
        return $result;
    }
}
