<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Models\BalanceSheet;
use App\libraries\pChart;
use App\libraries\pData;

class ProfitRatio extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of the profit ratios';
    const CHART_TITLE_TL = 'Dynamics ng mga ratio ng kita';
    const FILENAME_FORMAT = 'graph_2_2_1_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);

        $incomeData = $financialStatement['income_statements'];

        $balanceSheet = $financialStatement['balance_sheets'];

        $count = count($balanceSheet);
        $grossMargin = [];
        $operatingMargin = [];
        $profitMargin = [];
        $reportYear = [];

        $cnt = $count-1;
        for ($id = $count; $id > 0; $id--) {
            $yearDisplayed = $report->year - $cnt;
            $cnt--;
            $reportYear[] = $yearDisplayed;

            if (!isset($incomeData[$id]) || $incomeData[$id]->Revenue == 0) {
                $grossMargin[] = 0;
                $operatingMargin[] = 0;
                $profitMargin[] = 0;
                continue;
            }

            $grossMargin[] = ($incomeData[$id]->GrossProfit / $incomeData[$id]->Revenue) * 100;
            $profitMargin[] = ($incomeData[$id]->ProfitLoss / $incomeData[$id]->Revenue) * 100;
            $operatingMargin[] = (
                (
                    $incomeData[$id]->ProfitLossBeforeTax +
                    $incomeData[$id]->FinanceCosts
                ) / $incomeData[$id]->Revenue
            ) * 100;
        }

        $dataSet = new pData();
        $dataSet->AddPoint($grossMargin, 'Serie1');
        $dataSet->AddPoint($operatingMargin, 'Serie2');
        $dataSet->AddPoint($profitMargin, 'Serie3');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie4');
        $dataSet->SetAbsciseLabelSerie('Serie4');
        $dataSet->SetSerieName('Gross margin', 'Serie1');
        $dataSet->SetSerieName('Return on sales', 'Serie2');
        $dataSet->SetSerieName('Profit margin', 'Serie3');
        $dataSet->SetYAxisName('%');

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;
        
        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(120, 80, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 255, 255, 255);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_NORMAL,
            150,
            150,
            150,
            true,
            50,
            0,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 225, 0, 0);
        $chart->drawLineGraph($dataSet->GetData(), $dataSet->GetDataDescription());
        $chart->drawPlotGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            3,
            2,
            255,
            255,
            255
        );
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 580, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(780, 55, $chart_title_constant, 50, 50, 50, 160);
        $chart->Render($filepath);

        return $filepath;
    }
}
