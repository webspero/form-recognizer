<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\libraries\pChart;
use App\libraries\pData;

class LiquidityRatio extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of liquidity ratios';
    const CHART_TITLE_TL = 'Dynamics ng mga ratio ng pagkatubig';
    const FILENAME_FORMAT = 'graph_1_4_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);

        $balanceSheet = $financialStatement['balance_sheets'];

        $count = @count($balanceSheet) - 1;
        $liquidityRatio = [];
        $quickRatio = [];
        $cashRatio = [];
        $reportYear = [];

        for ($id = $count; $id >= 0; $id--) {

            if ($balanceSheet[$id]->CurrentLiabilities == 0) {
                continue;
                $liquidityRatio[] = 0;
                $quickRatio[] = 0;
                $cashRatio[] = 0;
            }

            $yearDisplayed = $balanceSheet[$id]->Year;
            $reportYear[] = '12/31/'.substr($yearDisplayed, 2);

            $liquidityRatio[] = $balanceSheet[$id]->CurrentAssets /
                $balanceSheet[$id]->CurrentLiabilities;
            $cashRatio[] = $balanceSheet[$id]->CashAndCashEquivalents /
                $balanceSheet[$id]->CurrentLiabilities;
            $quickRatio[] = (
                    $balanceSheet[$id]->CashAndCashEquivalents +
                    $balanceSheet[$id]->OtherCurrentFinancialAssets +
                    $balanceSheet[$id]->TradeAndOtherCurrentReceivables
                ) / $balanceSheet[$id]->CurrentLiabilities;
        }

        $dataSet = new pData();
        $dataSet->AddPoint($liquidityRatio, 'Serie1');
        $dataSet->AddPoint($quickRatio, 'Serie2');
        $dataSet->AddPoint($cashRatio, 'Serie3');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie4');
        $dataSet->SetAbsciseLabelSerie("Serie4");
        $dataSet->SetSerieName('Current liquidity ratio', 'Serie1');
        $dataSet->SetSerieName('Quick liquidity ratio', 'Serie2');
        $dataSet->SetSerieName('Cash ratio', 'Serie3');
        $dataSet->SetYAxisName('Ratio Value');
        $dataSet->SetXAxisFormat('');
        $dataSet->SetXAxisUnit('');
        $dataSet->SetYAxisFormat('');
        $dataSet->SetYAxisUnit('');

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;
        
        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(120, 80, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_NORMAL,
            150,
            150,
            150,
            true,
            0,
            2,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 225, 0, 0);
        $chart->drawPlotGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            3,
            2,
            255,
            255,
            255
        );
        $chart->drawTreshold(2, 143, 55, 72, true, true);
        $chart->drawTreshold(1, 143, 55, 72, true, true);
        $chart->drawTreshold(0.2, 143, 55, 72, true, true);
        $chart->setFontProperties($font, 15);
        $this->drawLegend($chart, 120, 530, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(780, 55, $chart_title_constant, 50, 50, 50, 160);
        $chart->setLineStyle(3,0); // Make line thicker
        $chart->drawLineGraph($dataSet->GetData(), $dataSet->GetDataDescription());
        $chart->Render($filepath);

        return $filepath;
    }

    /* Draw the data legends */
   public function drawLegend($chart, $XPos,$YPos,$DataDescription,$R,$G,$B,$Rs=-1,$Gs=-1,$Bs=-1,$Rt=0,$Gt=0,$Bt=0,$Border=TRUE)
    {
     /* Validate the Data and DataDescription array */
     $chart->validateDataDescription("drawLegend",$DataDescription);

     if ( !isset($DataDescription["Description"]) )
      return(-1);

     $C_TextColor =$chart->AllocateColor($chart->Picture,$Rt,$Gt,$Bt);

     /* <-10->[8]<-4->Text<-10-> */
     $MaxWidth = 0; $MaxHeight = 8;
     foreach($DataDescription["Description"] as $Key => $Value)
      {
       $Position   = imageftbbox($chart->FontSize,0,$chart->FontName,$Value);
       $TextWidth  = $Position[2]-$Position[0];
       $TextHeight = $Position[1]-$Position[7];
       if ( $TextWidth > $MaxWidth) { $MaxWidth = $TextWidth; }
       $MaxHeight = $MaxHeight + $TextHeight + 4;
      }
     $MaxHeight = $MaxHeight - 5;
     $MaxWidth  = $MaxWidth + 32;

     if ( $Rs == -1 || $Gs == -1 || $Bs == -1 )
      { $Rs = $R-30; $Gs = $G-30; $Bs = $B-30; }

     if ( $Border )
      {
       $chart->drawFilledRoundedRectangle($XPos+1,$YPos+1,$XPos+$MaxWidth+1,$YPos+$MaxHeight+1,5,$Rs,$Gs,$Bs);
       $chart->drawFilledRoundedRectangle($XPos,$YPos,$XPos+$MaxWidth,$YPos+$MaxHeight,5,$R,$G,$B);
      }

     $YOffset = 4 + $chart->FontSize; $ID = 0;
     foreach($DataDescription["Description"] as $Key => $Value)
      {
       $chart->drawFilledRoundedRectangle($XPos+12,$YPos+$YOffset-4,$XPos+12,$YPos+$YOffset-4,7,$chart->Palette[$ID]["R"],$chart->Palette[$ID]["G"],$chart->Palette[$ID]["B"]);
       imagettftext($chart->Picture,$chart->FontSize,0,$XPos+24,$YPos+$YOffset+3,$C_TextColor,$chart->FontName,$Value);

       $Position   = imageftbbox($chart->FontSize,0,$chart->FontName,$Value);
       $TextHeight = $Position[1]-$Position[7];

       $YOffset = $YOffset + $TextHeight + 4;
       $ID++;
      }
    }
}
