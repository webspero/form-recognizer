<?php

namespace CreditBPO\Services\Report\PCharts;

abstract class PCharts {
    const DEFAULT_FONT = 'tahoma.ttf';

    protected $filepath;
    protected $filename;
    protected $fontDirectory;

    public function __construct() {
        $this->setFilePath(public_path('images').'/');
        $this->setFontDirectory(public_path('fonts').'/');
    }

    /**
     * @param string $path
     * @return PCharts
     */
    public function setFilePath($path) {
        $this->filepath = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath() {
        return $this->filepath;
    }

    /**
     * @param string $filename
     * @return PCharts
     */
    public function setFilename($filename) {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return PCharts
     */
    public function setFontDirectory($directory) {
        $this->fontDirectory = $directory;
        return $this;
    }

    /**
     * @return string
     */
    public function getFontDirectory() {
        return $this->fontDirectory;
    }

    /**
     * @param int $id
     * @return string
     */
    abstract public function generate($id);
}
