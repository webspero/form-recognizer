<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\Libraries\pChart;
use App\Libraries\pData;

class CapitalStructure extends PCharts {
    const CHART_TITLE_EN = 'Capital structure of the company on 31 December ';
    const CHART_TITLE_TL = 'Kapital istraktura ng kumpanya sa Disyembre 31 ';
    const FILENAME_FORMAT = 'graph_1_3_1_1_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);
        
        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);

        $balanceSheet = $financialStatement['balance_sheets'];

        $currentLiabilities = round($balanceSheet[0]->CurrentLiabilities, 1);
        $nonCurrentLiabilities = round($balanceSheet[0]->NoncurrentLiabilities, 1);
        $equity = round($balanceSheet[0]->Equity, 1);

        $dataSet = new pData();

        $finalData = [];
        if($nonCurrentLiabilities > 0)
        {
            $finalData['Non-current liabilities'] = $nonCurrentLiabilities;
        }
        if($currentLiabilities > 0)
        {
            $finalData['Current Liabilities'] = $currentLiabilities;
        }
        if($equity > 0)
        {
            $finalData['Equity'] = $equity;
        }

        $dataSet->AddPoint(
            [
                ($nonCurrentLiabilities > 0) ? $nonCurrentLiabilities : null,
                ($currentLiabilities > 0) ? $currentLiabilities: null,
                ($equity > 0) ? $equity : null,
            ],
            'Serie1'
        );
        $dataSet->AddPoint(
            [
                ($nonCurrentLiabilities > 0) ? 'Non-current liabilities' : null,
                ($currentLiabilities > 0) ? 'Current Liabilities' : null,
                ($equity > 0) ? 'Equity' : null,
            ],
            'Serie2'
        );
        $dataSet->AddAllSeries();
        $dataSet->SetAbsciseLabelSerie('Serie2');

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        $chart = new pChart(1020, 650);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->setFontProperties($font, 15);
        if($nonCurrentLiabilities <= 0)
        {
            // $chart->setColorPalette(0, 255, 0, 0); // non current : red
            $chart->setColorPalette(0, 0, 255, 0); // Current lia : green 
            $chart->setColorPalette(1, 0, 0, 255); // Equity blue
        }
        else if($currentLiabilities <= 0)
        {
            $chart->setColorPalette(0, 255, 0, 0); // non current : red
            $chart->setColorPalette(1, 0, 0, 255); // Equity blue
            // $chart->setColorPalette(1, 0, 255, 0); // Current lia : green 
        }
        else if($equity <= 0)
        {
            $chart->setColorPalette(0, 255, 0, 0); // non current : red
            $chart->setColorPalette(1, 0, 255, 0); // Current lia : green 
            // $chart->setColorPalette(1, 0, 0, 255); // Equity blue
        }
        else
        {
            $chart->setColorPalette(0, 255, 0, 0); // non current : red
            $chart->setColorPalette(1, 0, 255, 0); // Current lia : green 
            $chart->setColorPalette(2, 0, 0, 255); // Equity blue
        }
        $chart->drawPieGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            500,
            340,
            290,
            PIE_PERCENTAGE_LABEL,
            true,
            50,
            20,
            5,
            1 //Decimal
        );
        $chart->drawPieLegend(
            780,
            30,
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            250,
            250,
            250
        );
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(
            200,
            45,
            $chart_title_constant.$report->year,
            50,
            50,
            50,
            450
        );
        $chart->Render($filepath);

        return $filepath;
    }
}
