<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\libraries\pChart;
use App\Libraries\pData;

class FinancialStability extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of the ratios of the company\'s financial stability';
    const CHART_TITLE_TL = 'Dynamics ng mga ratios ng katatagan sa pananalapi ng kumpanya';
    const FILENAME_FORMAT = 'graph_1_3_1_2_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);
        
        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);
        $balanceSheet = $financialStatement['balance_sheets'];

        $count = @count($balanceSheet) - 1;
        $curLiabRatio = [];
        $nonCurRatio = [];
        $debtRatio = [];
        $reportYear = [];

        for ($id = $count; $id >= 0; $id--) {
            if($balanceSheet[$id]->Liabilities == 0 &&
               $balanceSheet[$id]->Equity == 0 && 
               $balanceSheet[$id]->Assets == 0
               ){
            }else{
                if ($balanceSheet[$id]->Liabilities != 0) {
                    $curLiabRatio[] = $balanceSheet[$id]->CurrentLiabilities /
                        $balanceSheet[$id]->Liabilities;
                } else {
                    $curLiabRatio[] = 0;
                }

                if ($balanceSheet[$id]->Equity != 0) {
                    $nonCurRatio[] = $balanceSheet[$id]->NoncurrentAssets /
                        $balanceSheet[$id]->Equity;
                } else {
                    $nonCurRatio[] = 0;
                }

                if ($balanceSheet[$id]->Assets != 0) {
                    $debtRatio[] = $balanceSheet[$id]->Liabilities / $balanceSheet[$id]->Assets;
                } else {
                    $debtRatio[] = 0;
                }

                $yearDisplayed = $report->year - $id;
                $reportYear[] = '12/31'.$yearDisplayed;
            }
        }

        $dataSet = new pData();
        $dataSet->AddPoint($curLiabRatio, 'Serie1');
        $dataSet->AddPoint($nonCurRatio, 'Serie2');
        $dataSet->AddPoint($debtRatio, 'Serie3');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie4');
        $dataSet->SetAbsciseLabelSerie('Serie4');
        $dataSet->SetSerieName('Current liability', 'Serie1');
        $dataSet->SetSerieName('Non-current assets to Net worth ratio', 'Serie2');
        $dataSet->SetSerieName('Debt Ratio', 'Serie3');
        $dataSet->SetYAxisName('Value');
        $dataSet->SetXAxisFormat('');
        $dataSet->SetXAxisUnit('');
        $dataSet->SetYAxisFormat('');
        $dataSet->SetYAxisUnit('');

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(120, 80, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 255, 255, 255);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_NORMAL,
            150,
            150,
            150,
            true,
            45,
            2,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 255, 0, 0);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 0, 0, 255);
        $chart->drawLineGraph($dataSet->GetData(), $dataSet->GetDataDescription());
        $chart->drawPlotGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            3,
            2,
            255,
            255,
            255
        );
        $chart->drawTreshold(0.6, 143, 55, 72, true, true);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 580, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(780, 55, $chart_title_constant, 50, 50, 50, 160);
        $chart->Render($filepath);

        return $filepath;
    }
}
