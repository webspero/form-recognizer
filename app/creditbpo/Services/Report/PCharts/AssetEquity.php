<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use App\libraries\pChart;
use App\libraries\pData;

class AssetEquity extends PCharts {
    const CHART_TITLE_EN = 'Dynamics of total assets and equity ratios';
    const CHART_TITLE_TL = 'Dynamics ng kabuuang mga assets at equity ratio';
    const FILENAME_FORMAT = 'graph_2_2_2_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);
        
        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);

        $incomeData = $financialStatement['income_statements'];
        $balanceSheetArray = $financialStatement['balance_sheets'];
        $balanceSheet = $financialStatement['balance_sheets'];

        $count = @count($balanceSheetArray) - 2;
        $roe = [];
        $roa = [];
        $roce = [];
        $reportYear = [];

        for ($id = $count; $id >= 0; $id--) {
            $yearDisplayed = $report->year - $id;
            $reportYear[] = $yearDisplayed;

            $equity = ($balanceSheet[$id]->Equity + $balanceSheet[$id + 1]->Equity) / 2;
            $assets = ($balanceSheet[$id]->Assets + $balanceSheet[$id + 1]->Assets) / 2;
            $equityNonCurrentLiability = (
                $balanceSheet[$id]->Equity +
                $balanceSheet[$id]->NoncurrentLiabilities +
                $balanceSheet[$id + 1]->Equity +
                $balanceSheet[$id + 1]->NoncurrentLiabilities
            ) / 2;

            if ($equity != 0) {
                $roe[] = ($incomeData[$id]->ProfitLoss / $equity) * 100;
            } else {
                $roe[] = 0;
            }

            if ($assets != 0) {
                $roa[] = ($incomeData[$id]->ProfitLoss / $assets) * 100;
            } else {
                $roa[] = 0;
            }

            if ($equityNonCurrentLiability) {
                $roce[] = (
                    (
                        $incomeData[$id]->ProfitLossBeforeTax +
                        $incomeData[$id]->FinanceCosts
                    ) / $equityNonCurrentLiability
                ) * 100;
            } else {
                $roce[] = 0;
            }
        }

        $yaxisdisplay = '%';

        $chart_title = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title = Self::CHART_TITLE_TL;

        $dataSet = new pData();
        $dataSet->AddPoint($roe, 'Serie1');
        $dataSet->AddPoint($roa, 'Serie2');
        $dataSet->AddPoint($roce, 'Serie3');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie4');
        $dataSet->SetAbsciseLabelSerie('Serie4');
        $dataSet->SetSerieName('Return on equity (ROE)', 'Serie1');
        $dataSet->SetSerieName('Return on assets (ROA)', 'Serie2');
        $dataSet->SetSerieName('Return on capital employed (ROCE)', 'Serie3');
        $dataSet->SetYAxisName($yaxisdisplay);

        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(120, 80, 920, 480);
        $chart->setGraphArea(190, 120, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_START0,
            150,
            150,
            150,
            true,
            0,
            2,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 255, 0, 0);
        $chart->drawBarGraph($dataSet->GetData(), $dataSet->GetDataDescription(), true);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 540, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(50, 55, $chart_title, 50, 50, 50, 485);
        $chart->Render($filepath);

        return $filepath;
    }
}