<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\libraries\pChart;
use App\libraries\pData;

class AssetStructure extends PCharts {
    const CHART_TITLE_EN = 'The company\'s assets structure';
    const CHART_TITLE_TL = 'Paggalaw ng kabuuang mga yaman at karampatang tumbas';
    const FILENAME_FORMAT = 'graph_1_1_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);
                
        $data = $financialStatement['balance_sheets'];
        
        $arr_data = $data;

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        if(!empty($arr_data[0]['Year']) && session('fa_report_lang') == 'en'){
            $chartTitle = $chart_title_constant.' on 31 December, '.$arr_data[0]['Year'];
            $leftTitle = 420; 
        }elseif(!empty($arr_data[0]['Year']) && session('fa_report_lang') == 'tl'){
            $chartTitle = $chart_title_constant.' sa 31 Disyembre, '.$arr_data[0]['Year'];
            $leftTitle = 720;
        }elseif(empty($arr_data[0]['Year']) && session('fa_report_lang') == 'en'){
            $chartTitle = $chart_title_constant;
            $leftTitle = 200;
        }else{
            $chartTitle = $chart_title_constant;
            $leftTitle = 160;
        }

        $inventories = round(($data[0]->Inventories / $data[0]->Assets) * 100, 1);     

        $ncAssets = round(($data[0]->NoncurrentAssets / $data[0]->Assets) * 100, 1);
        $cReceivables = round(
            (
                $data[0]->TradeAndOtherCurrentReceivables / $data[0]->Assets
            ) * 100,
            1
        );
        $ocAssets = 100 - ($ncAssets + $cReceivables + $inventories);

        $addPointValue = [];
        $addPointLabel = [];

        $ncAssets_label = 'Non-current assets';
        $cReceivables_label = 'Current receivables';
        $inventories_label = 'Inventories';
        $ocAssets_label = 'Other current assets';
        if(session('fa_report_lang') == 'tl'){
            $chart_title_constant = Self::CHART_TITLE_TL;

            $ncAssets_label = 'Hindi kasalukuyang yaman';
            $cReceivables_label = 'Natanggap kasalukuyan';
            $inventories_label = 'Imbentaryo';
            $ocAssets_label = 'Ibang kasalukuyang yaman';
        }

        if(!empty($ncAssets)){
            $addPointValue[] = $ncAssets;
            $addPointLabel[] = $ncAssets_label;
        }

        if(!empty($cReceivables)){
            $addPointValue[] = $cReceivables;
            $addPointLabel[] = $cReceivables_label;
        }

        if(!empty($inventories)){
            $addPointValue[] = $inventories;
            $addPointLabel[] = $inventories_label;
        }

        if(!empty($ocAssets)){
            $addPointValue[] = $ocAssets;
            $addPointLabel[] = $ocAssets_label;
        }

        $dataSet = new pData();
        $dataSet->AddPoint(
            $addPointValue,
            'Serie1'
        );
        $dataSet->AddPoint(
            $addPointLabel,
            'Serie2'
        );
        $dataSet->AddAllSeries();
        $dataSet->SetAbsciseLabelSerie('Serie2');

        $chart = new pChart(1020, 650);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 255, 0, 0);
        $chart->drawPieGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            500,
            340,
            290,
            PIE_PERCENTAGE_LABEL,
            TRUE,
            50,
            20,
            5,
            2
        );
        $chart->drawPieLegend(
            780,
            30,
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            250,
            250,
            250
        );
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(200, 45, $chartTitle, 50, 50, 50, $leftTitle);
        $chart->Render($filepath);

        return $filepath;
    }
}
