<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use App\libraries\pChart;
use App\Libraries\pData;

class WorkingCapital extends PCharts {
    const CHART_TITLE_EN = 'Working Capital';
    const CHART_TITLE_TL = 'Gumaganang Kapital';
    const FILENAME_FORMAT = 'graph_1_3_2_%d.png';

    /**
     * @param int $id
     * @return string
     */
    public function generate($id) {
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = $this->getFilePath().$filename;
        $this->setFilename($filename);

        $report = FinancialReport::find($id);
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($report->entity_id);
        
        $balanceSheet = $financialStatement['balance_sheets'];

        $count = @count($balanceSheet) - 1;
        $workingCapital = [];
        $inventories = [];
        $capitalSufficiency = [];
        $reportYear = [];
        $cntYear = 0;

        for ($id = $count; $id >= 0; $id--) {
            if( ($balanceSheet[$id]->CurrentAssets - $balanceSheet[$id]->CurrentLiabilities) == 0 &&
                 $balanceSheet[$id]->Inventories == 0 &&
                (( $balanceSheet[$id]->CurrentAssets - $balanceSheet[$id]->CurrentLiabilities ) - $balanceSheet[$id]->Inventories) == 0
                ){
            }else{
                $workingCapital[] = $balanceSheet[$id]->CurrentAssets - $balanceSheet[$id]->CurrentLiabilities;
                $inventories[] = $balanceSheet[$id]->Inventories;
                $capitalSufficiency[] = ( $balanceSheet[$id]->CurrentAssets - $balanceSheet[$id]->CurrentLiabilities ) - $balanceSheet[$id]->Inventories;

            }
            $yearDisplayed = $report->year - $id;
            $reportYear[] = '12/31'.$yearDisplayed;
            $cntYear += 1;
        }

        $yaxisdisplay = 'PHP';

        // Get Highest value
        $workingCapitalMax = (count($workingCapital) == 0) ? 0 :  max($workingCapital);
        $inventoriesMax = (count($inventories) == 0) ? 0 : max($inventories);
        $capitalSufficiencyMax = (count($capitalSufficiency) == 0) ? 0 : max($capitalSufficiency);

        if($workingCapitalMax == 0 && $inventoriesMax == 0 && $capitalSufficiencyMax == 0){
            $finalMax = 0;
        }else{
            $finalMax = max(array($workingCapitalMax, $inventoriesMax, $capitalSufficiencyMax));
        }

        //Get minimum value
        $workingCapitalMin = (count($workingCapital) == 0) ? 0 : min($workingCapital);
        $inventoriesMin = (count($inventories) == 0) ? 0 : min($inventories);
        $capitalSufficiencyMin = (count($capitalSufficiency) == 0) ? 0 : min($capitalSufficiency);

        if($workingCapitalMin == 0 && $inventoriesMin == 0 && $capitalSufficiencyMin == 0){
            $finalMin = 0;
        }else{
            $finalMin = min(array($workingCapitalMin, $inventoriesMin, $capitalSufficiencyMin));
        }


        //get length of the highest amount
        if(round($finalMax,0) <= 0){
            $finLength = strlen((string)round($finalMin,0));
            $finLength -= 1;
        }else{
            $finLength = strlen((string)round($finalMax,0));
        }

        if($finLength == 1){
            $yaxisdisplay = "One PHP";
            $units_value = 1;
            $units_length = 1;
        }elseif($finLength == 2){
            $yaxisdisplay = "Ten PHP";
            $units_value = 10;
            $units_length = 2;
        }elseif($finLength == 3){
            $yaxisdisplay = "Hundred PHP";
            $units_value = 100;
            $units_length = 3;
        }else if($finLength >= 4 && $finLength <=6){
            $yaxisdisplay = "Thousand PHP";
            $units_value = 1000;
            $units_length = 4;
        }else if($finLength >= 7 && $finLength <=9){
            $yaxisdisplay = "Million PHP";
            $units_value = 1000000;
            $units_length = 6;
        }else if($finLength >= 10 && $finLength <= 12){
            $yaxisdisplay = "Billion PHP";
            $units_value = 1000000000;
            $units_length = 9;
        }else if($finLength >= 13 && $finLength <= 15){
            $units_value = 1000000000000;
            $units_length = 12;
            $yaxisdisplay = "Trillion PHP";
        }else if($finLength >= 16 && $finLength <= 18){
            $yaxisdisplay = "Quadrillion PHP";
            $units_value = 1000000000000000;
            $units_length = 15;
        }

        if($finLength >= 1 && $finLength <=3){
            // default PHP
        }else{
            if($finalMin == 0 && $finalMin == 0){
                $arrayResult = array();
                for($x=0; $x<$cntYear; $x++){
                    array_push($arrayResult, 0);
                }
                $workingCapital = $arrayResult;
                $inventories = $arrayResult;
                $capitalSufficiency = $arrayResult;
            }else{
                $data = array(
                    'workingCapital' => $workingCapital,
                    'inventories'   => $inventories,
                    'capitalSufficiency' => $capitalSufficiency,
                    'units_value'   => $units_value,
                    'units_length'  => $units_length,
                    'finalMax'      => $finalMax
                );

                $result = $this->convertGraphUnits($data);
                $workingCapital = $result['workingCapital'];
                $inventories = $result['inventories'];
                $capitalSufficiency = $result['capitalSufficiency'];
            }
        }

        $dataSet = new pData();
        $dataSet->AddPoint($workingCapital, 'Serie1');
        $dataSet->AddPoint($inventories, 'Serie2');
        $dataSet->AddPoint($capitalSufficiency, 'Serie3');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($reportYear, 'Serie4');
        $dataSet->SetAbsciseLabelSerie('Serie4');
        $dataSet->SetSerieName('Working capital', 'Serie1');
        $dataSet->SetSerieName('Inventories', 'Serie2');
        $dataSet->SetSerieName('Working capital sufficiency', 'Serie3');
        $dataSet->SetYAxisName($yaxisdisplay);

        $chart_title_constant = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title_constant = Self::CHART_TITLE_TL;

        $chart = new pChart(1020, 650);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(190, 120, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 230, 230, 230);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_START0,
            150,
            150,
            150,
            true,
            0,
            2,
            true
        );
        $chart->drawGrid(4, true, 230, 230, 230, 50);
        $chart->setFontProperties($font, 15);
        $chart->setColorPalette(0, 0, 0, 255);
        $chart->setColorPalette(1, 0, 255, 0);
        $chart->setColorPalette(2, 255, 0, 0);
        $chart->drawBarGraph($dataSet->GetData(), $dataSet->GetDataDescription(), true);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(120, 540, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(50, 55, $chart_title_constant, 50, 50, 50, 485);
        $chart->Render($filepath);

        return $filepath;
    }

    public function convertGraphUnits($data){
        $units_value = $data['units_value'];
        $workingCapital = $data['workingCapital'];
        $inventories = $data['inventories'];
        $capitalSufficiency = $data['capitalSufficiency'];

        // Working Capital
        foreach($workingCapital as $key => $wc){
            $wcLength = strlen((string)round($wc,0));
            $displayValue = 0;
            do{
                if($wc < 0){
                    $wc += $units_value;
                    $displayValue += ($units_value * -1);
                    $wcLength = strlen((string)round($wc,0)) - 1; 
                }elseif($wc == 0){
                    $workingCapital[$key] = 0;
                    continue;
                }else{
                    $wc -= $units_value;
                    $displayValue += $units_value;
                    $wcLength = strlen((string)round($wc,0));
                }
            }while($wcLength > $data['units_length']);
            $workingCapital[$key] = $displayValue / $units_value;
        }

        // Invertories
        foreach($inventories as $key => $iv){
            $ivLength = strlen((string)round($iv,0));
            $displayValue = 0;
            do{
                if($iv < 0){
                    $iv += $units_value;
                    $displayValue += ($units_value * -1);
                    $ivLength = strlen((string)round($iv,0)) - 1; 
                }elseif($iv == 0){
                    $inventories[$key] = 0;
                    continue;
                }else{
                    $iv -= $units_value;
                    $displayValue += $units_value;
                    $ivLength = strlen((string)round($iv,0));
                }
            }while($ivLength > $data['units_length']);
            $inventories[$key] = $displayValue / $units_value;
        }

        // Capital Sufficiency
        foreach($capitalSufficiency as $key => $cf){
            $cfLength = strlen((string)round($cf,0));
            $displayValue = 0;
            do{
                if($cf < 0){
                    $cf += $units_value;
                    $displayValue += ($units_value * -1);
                    $cfLength = strlen((string)round($cf,0)) - 1;
                }elseif($cf == 0){
                    $capitalSufficiency[$key] = 0;
                    continue;
                }else{
                    $cf -= $units_value;
                    $displayValue += $units_value;
                    $cfLength = strlen((string)round($cf,0));
                }
            }while($cfLength > $data['units_length']);
            $capitalSufficiency[$key] = $displayValue / $units_value;
        }
        
        $result = array(
            'workingCapital' => $workingCapital,
            'inventories'   => $inventories,
            'capitalSufficiency' => $capitalSufficiency
        );
        return $result;
    }
}
