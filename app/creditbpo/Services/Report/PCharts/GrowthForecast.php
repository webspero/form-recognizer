<?php

namespace CreditBPO\Services\Report\PCharts;

use CreditBPO\Services\Report\PCharts\PCharts;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Models\Entity;
use CreditBPO\Models\Bank;
use App\libraries\pChart;
use App\libraries\pData;
use DB;
use GrowthForecastLib;


class GrowthForecast extends PCharts{
    const CHART_TITLE_EN = '';
    const CHART_TITLE_TL = '';
    const FILENAME_FORMAT = 'growth_forecast_%d.png';

    public function generate($id){
        $font = $this->getFontDirectory().self::DEFAULT_FONT;
        $filename = sprintf(self::FILENAME_FORMAT, $id);
        $filepath = public_path('images/rating-report-graphs').'/'.$filename;
        $this->setFilename($filename);

        $forecastData = $this->getForecastData($id);
        $financialReport = $this->getFinancialReport($id);

        $year = [];
        $industry = array();
        $count = 1;
        $quarter = array("Q1", "Q2", "Q3", "Q4");
        for($x=0; $x<3; $x++){
            for($y=0; $y<=3; $y++){
                $year[] = $quarter[$y] . " " . ((int)$financialReport->year + $x) . "  ";
            }
        }
        $year[] = $quarter[0] . " " . ((int)$financialReport->year + 3) . "  ";

        foreach($forecastData['industry'] as $fcompany){
            array_push($industry, round($fcompany,2));
        }

        unset($forecastData['industry'][13]);
        unset($forecastData['industry'][14]);
        unset($forecastData['industry'][15]);

        $dataSet = new pData();
        $dataSet->AddPoint($forecastData['company'], 'Serie1');
        $dataSet->AddPoint($industry, 'Serie2');
        $dataSet->AddAllSeries();
        $dataSet->AddPoint($year, 'Serie3');
        $dataSet->SetAbsciseLabelSerie('Serie3');
        $dataSet->SetSerieName('Company', 'Serie1');
        $dataSet->SetSerieName('Industry', 'Serie2');
        $dataSet->SetYAxisName('');
        $dataSet->SetXAxisFormat('');
        $dataSet->SetXAxisUnit('');
        $dataSet->SetYAxisFormat('');
        $dataSet->SetYAxisUnit('');

        
        $chart = new pChart(1100, 180);
        $chart->setFontProperties($font, 15);
        $chart->setGraphArea(120, 80, 920, 480);
        $chart->drawFilledRoundedRectangle(7, 7, 1000, 630, 5, 255, 255, 255);
        $chart->drawRoundedRectangle(5, 5, 1000, 630, 5, 255, 255, 255);
        $chart->drawGraphArea(255, 255, 255, true);
        $chart->setLineStyle(3);  
        $chart->drawScale(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            SCALE_NORMAL,
            150,
            150,
            150,
            true,
            45,
            2,
            true
        );
        $chart->writeValues( $dataSet->GetData(),$dataSet->GetDataDescription(),array("Serie1","Serie2")); 

        $chart->setFontProperties($font, 20);
        $chart->setColorPalette(167,245,66, 0);
        $chart->setColorPalette(156,166,144, 0);
        $chart->setColorPalette(1,105,105, 105);
        $chart->drawLineGraph($dataSet->GetData(), $dataSet->GetDataDescription());
        $chart->drawPlotGraph(
            $dataSet->GetData(),
            $dataSet->GetDataDescription(),
            3,
            2,
            255,
            255,
            255
        );
        // $chart->drawTreshold(0.6, 143, 55, 72, true, true,4);
        $chart->setFontProperties($font, 15);
        $chart->drawLegend(150, 580, $dataSet->GetDataDescription(), 255, 255, 255);
        $chart->setFontProperties($font, 18);
        $chart->drawTitle(780, 55, "", 50, 50, 50, 160);
        $chart->Render($filepath);
        return $filename;
    }

    public function getForecastData($entityId){
        $entity = Entity::where('entityid', $entityId)->first();

        $industryMainRow = DB::table('tblindustry_main')->where('main_code',$entity->industry_main_id)->first();

        if(empty($industryMainRow)){
			$industryMainRow = DB::table('tblindustry_main')->where('industry_main_id',$entity->industry_main_id)->first();        	
        }
        $industryMain = $industryMainRow->industry_main_id;

        if ($entity->current_bank != 0) {
            $bankStandalone = Bank::find($entity->current_bank)->is_standalone;
        } else {
            $bankStandalone = 0;
        }

        /**
         * Forecase slope calculation, GrowthForecast Lib needs to be refactored
         * Place it on CreditBPO\Services\GrowthForecast
         */
        $growthForecast = new GrowthForecastLib($entityId, $industryMain, $bankStandalone);
        
        return $growthForecast->getGrowthForecastData();
    }

    protected function getFinancialReport($entityId) {
        return FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
    }
}