<?php

namespace CreditBPO\Services\Report\Api;

use Request;
use DB;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\QuestionnaireLink as Questionnaire;
use CreditBPO\DTO\QuestionnaireDTO;

class QuestionPassService
{

	public function __construct() {
		$this->reportDb = new Report;
		$this->questionnaireDTO = new QuestionnaireDTO;
        $this->questionnaireDb = new Questionnaire;
	}

	public function validateReport($reportId, $userId, $data){

		$questionnaireDTO = new QuestionnaireDTO();
		$errorMessages = array();

		/*Check if report is existing*/
        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } 
        /* Check if the user is the owner of the report*/
        elseif($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } 
        /* Check if the report is not yet done or sumbitted */
        elseif($checkReportOwner->status == 7){
        	$errorMessages['errors'] = "Report is already done.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        }else {
            $errorMessages = null;
        }

        /*Check if input tabs is valid*/
        $questionnaireDTO->setQuestionnaireVars(Request::all(), $reportId);

        $availableTab = array('registration1', 'registration2', 'registration3', 'pfs');
        if(!in_array($questionnaireDTO->getTab(), $availableTab)){
        	$errorMessages['errors'] = "Tab doesnt exist. Please refer to CreditBPO Tab API Reference list for valid values.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        }

        return $errorMessages;
	}

	public function getQuestionPassData($reportId){
		$questionnaireDTO = new QuestionnaireDTO();
		$questionnaireDTO->setQuestionnaireVars(Request::all(), $reportId);
		return $questionnaireDTO->getQuestionnaireVars();
	}

	public function generateLink($questionnaire){  
        if($questionnaire['tab'] == "registration1"){
            $data = array("biz-details-cntr", "cash-proxy-cntr");

        }elseif($questionnaire['tab'] == "registration2"){
            $data = array("major-cust-list-cntr", "major-supp-list-cntr");
        }elseif($questionnaire['tab'] == "registration3"){
            $data = array("dropbox-upload-cntr");
        }elseif($questionnaire['tab'] == "pfs"){
            $data = array("pfs");
        }

        $save_array = array(
            'tab' => $questionnaire['tab'],
            'data' => $data
        );

        do {
            /* generate random code */
            $random_code = self::createRandomCode();     
            $ql = $this->questionnaireDb->getQuestionnaireReport($random_code);                   
        } while($ql != null);   
        
        /* Checks if code already exists if not, create the link*/  
        $link = new Questionnaire();
        $link->entity_id = $questionnaire['reportid'];
        $link->url = $random_code;
        $link->panels = json_encode($save_array);
        $link->save();

        /* Return external link */
        return env('APP_URL') .  '/external-link/'. ($random_code);
	}

    public static function createRandomCode(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 16; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}