<?php

namespace CreditBPO\Services\Report\Api;

use URL;
use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;
use CreditBPO\Models\InnodataFile;
use CreditBPO\Models\MajorCustomer;
use CreditBPO\Models\MajorSupplier;
use CreditBPO\Models\FinancialReport;
use CreditBPO\DTO\ReportDTO;
use CreditBPO\Models\ReadyratioKeySummary;
use CreditBPO\Libraries\SubmitReportLib;
use CreditBPO\Handlers\AutomateSimplifiedRatingHandler;
use CreditBPO\Services\KeyRatioService;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use Auth;
use \Exception as Exception;
use App;
class SubmitReportService 
{
    public function __construct()
	{
        $this->reportDb = new Report;
        $this->documentDb = new Document;
        $this->majorCustomerDb = new MajorCustomer;
        $this->majorSupplierDb = new MajorSupplier;
        $this->reportDTO = new ReportDTO;
        $this->financialRawFileDb = new InnodataFile;
    }

    //---------------------------------------------------------------
    // Function #.a. Check if report exists and if user owns the report	
    //---------------------------------------------------------------
    function validateReport($reportId, $userId)
    {
        $errorMessages = array();

        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } else if($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            if($existingReport->status == 7){
                $errorMessages['errors'] = "Report already submitted";
                $errorMessages['code'] = HTTP_STS_FORBIDDEN;
            }else{
                $errorMessages = null;
            }
        }
        return $errorMessages;
    }

    //---------------------------------------------------------------
    // Function #.b. Get and update a report's progress if necessary, provide progress weights for a financial standalone report
    //---------------------------------------------------------------
    function checkProgress($reportId)
    {

        $data = array(
            'business_details' => array('incomplete' => array()), 
            'financial_raw_file' => array('incomplete' => array())
        );

        /** Required section */
        $isSection = array(
            'business_details'  => $this->checkBusinessDetails($reportId),
            'financial_raw_file'    => $this->checkFinancialRawFile($reportId)
        );

        if($isSection['business_details']['isComplete'] == false){
            foreach($isSection['business_details'] as $key => $bd){
                if($key == 'isComplete'){
                    continue;
                }
                if($bd['status'] == false){
                    array_push($data['business_details']['incomplete'], $bd['message']);
                }
            }
        }

        if($isSection['financial_raw_file'] == false){
            $data['financial_raw_file']['incomplete'] = 'Upload Financial Raw file is required.';
        }

        if($isSection['business_details']['isComplete'] == true && $isSection['financial_raw_file'] == true ){
            $this->processReport($reportId);
        }else{
            return $data;
        }
        return;
    }

    public function checkBusinessDetails($reportid){
        $reportRecord = $this->reportDb->getBusinessDetailsOfReport($reportid);

        /** Check if all details page field is filled-up */
        $requiredBussinessDetailsField = array(
            'companyname' => array('status' => true, 'message'),
            'company_tin' => array('status' => true, 'message'),
            'entity_type' => array('status' => true, 'message'),
            'sec_reg_date' => array('status' => true, 'message'),
            'industry_section' => array('status' => true, 'message'),
            'email' => array('status' => true, 'message'),
            'province_city' => array('status' => true, 'message'),
            'phone' => array('status' => true, 'message'),
            'organizationid' => array('status' => true, 'message'),
            'tin_num' => array('status' => true, 'message'),
            'address1' => array('status' => true, 'message'),
            'isComplete' => true
        );

        /** Check company name */
        if($reportRecord->companyname == null || empty($reportRecord->companyname)){
            $requiredBussinessDetailsField['companyname'] = ['status' => false, 'message' => 'Company name is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check company tin */
        if($reportRecord->company_tin == null || empty($reportRecord->company_tin)){
            $requiredBussinessDetailsField['company_tin'] = ['status' => false, 'message' => 'Company TIN is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check entity type */
        if($reportRecord->entity_type !== 0 && $reportRecord->entity_type !== 1){
            $requiredBussinessDetailsField['entity_type'] = ['status' =>  false, 'message' => 'Entity type is required.' ];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check SEC Registration Date */
        if($reportRecord->sec_reg_date == null || empty($reportRecord->sec_reg_date)){
            $requiredBussinessDetailsField['sec_reg_date'] = ['status' => false, 'message' => 'SEC Registration Date is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check industry */
        if($reportRecord->main_code == null || $reportRecord->sub_code == null || $reportRecord->group_code == null || 
           empty($reportRecord->main_code) || empty($reportRecord->sub_code) || empty($reportRecord->group_code) ){
            $requiredBussinessDetailsField['industry_section'] = ['status' => false, 'message' => 'Industry section, division and group is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check company email */
        if($reportRecord->email == null || empty($reportRecord->email)){
            $requiredBussinessDetailsField['email'] = ['status' => false, 'message' => 'Company Email is required'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check province and city */
        if($reportRecord->province == null || $reportRecord->city == null || empty($reportRecord->province) || empty($reportRecord->city) ){
            $requiredBussinessDetailsField['province_city'] = ['status' => false, 'message' => 'Province and City is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check telephone number */
        if($reportRecord->phone == null || empty($reportRecord->phone)){
            $requiredBussinessDetailsField['phone'] = ['status' => false, 'message' => 'Company telephone  is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check organization */
        if($reportRecord->organizationid == null || empty($reportRecord->organizationid)){
            $requiredBussinessDetailsField['organizationid'] = ['status' => false, 'message' => 'Organization is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check SEC Registration number */
        if($reportRecord->tin_num == null || empty($reportRecord->tin_num)){
            $requiredBussinessDetailsField['tin_num'] = ['status' => false, 'message' => 'SEC Registration number is required.'];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        /** Check primary business address */
        if($reportRecord->address1 == null || empty($reportRecord->address1)){
            $requiredBussinessDetailsField['tin_num'] = ['status' => false, 'message' => 'Primary Business Address is required.' ];
            $requiredBussinessDetailsField['isComplete'] = false;
        }

        return $requiredBussinessDetailsField;
    }

    public function checkFinancialRawFile($reportid){
        $rawFile = $this->financialRawFileDb->getInnodataFilesById($reportid);

        if(count($rawFile) > 0){
            return true;
        }

        return false;
    }

    public function processReport($reportid){
        $report = Report::where('entityid', $reportid)->first();

        $fs = Document::where('entity_id', $reportid)->where('document_group', 21)->where('is_deleted', 0)->get();

        if(count($fs) > 0){
            /** Internal Rating Process */
            if ($report->is_premium == 2) {
                $simplifiedRatingHandler = new AutomateSimplifiedRatingHandler();
                $simplifiedRatingHandler->processReport($reportid);
            } else {
                $keyRatio = new KeyRatioService();
                $keyRatio->generateKeyRatioByEntityId($reportid);
            }

            $standaloneRatingHandler = new AutomateStandaloneRatingHandler();
            $standaloneRatingHandler->processSingleReport($reportid);

            // Send alert emails regarding finished scoring questionnaire
            $financialAnalysisHandler = new FinancialAnalysisHandler();
            $financialAnalysisHandler->emailPostFinishScoring($reportid);

            Report::where("entityid", $report->entityid)->update(array("status" => 7));
        }else{
            // Waiting for transcribe files
            DB::table('automate_standalone_rating')->where("entity_id", $report->entityid)->update(['status' => 5]);
        }

        return;
    }

    //---------------------------------------------------------------
    // Function #.c. Updates necessary records in the database when a user submits a questionnaire
    //---------------------------------------------------------------
    function processSubmission($reportDTO)
    {
        $this->reportDb->updateStatus($this->reportDTO->getReportId(), REPORT_STATUS_SUBMITTED);
    }

    //---------------------------------------------------------------
    // Function #.d. Create the Incomplete Summary for a report
    //---------------------------------------------------------------
    function getIncompleteSummary($reportDTO)
    {
        $list = array(
            'businessdetails' => array(
                'incomplete' => array(),
                'optional' => array()
            ),
            'financialstatement' => array(
                'incomplete' => array(),
                'optional' => array()
            ),
            'cashproxy' => array(
                'incomplete' => array(),
                'optional' => array()
            )
        );

        /* Business Details - Incomplete Summary */
        if($reportDTO->getCompanyName() === null || empty($reportDTO->getCompanyName())) {
            array_push($list['businessdetails']['incomplete'], "Company Name is required.");
        }
        if($reportDTO->getCompanyTIN() === null || empty($reportDTO->getCompanyTIN())) {
            array_push($list['businessdetails']['incomplete'], "Company TIN is required.");
        }
        if($reportDTO->getBusinessType() === null) {
            array_push($list['businessdetails']['incomplete'], "Business Type is required.");
        }
        if($reportDTO->getSecRegistrationNumber() === null || empty($reportDTO->getSecRegistrationNumber())) {
            array_push($list['businessdetails']['incomplete'], "Business Reg. No. is required.");
        }
        if($reportDTO->getSecRegistrationDate() === null || $reportDTO->getSecRegistrationDate() === '0000-00-00') {
            array_push($list['businessdetails']['incomplete'], "Business Reg. Date is required.");
        }
        
        // if($reportDTO->getMainIndustry() === null || $reportDTO->getSubIndustry() === null || $reportDTO->getIndustry() === null) {
        //     array_push($list['businessdetails']['incomplete'], "Industry is required.");
        // }
        // if($reportDTO->getTotalAssetsGrouping() === null || empty($reportDTO->getTotalAssetsGrouping())) {
        //     array_push($list['businessdetails']['incomplete'], "Total Assets Grouping is required.");
        // }

        if($reportDTO->getCompanyEmail() === null || empty($reportDTO->getCompanyEmail())) {
            array_push($list['businessdetails']['incomplete'], "Company Email is required.");
        }
        if($reportDTO->getRoomBldgSt() === null || empty($reportDTO->getRoomBldgSt())) {
            array_push($list['businessdetails']['incomplete'], "Primary Business Address is required.");
        }
        if($reportDTO->getProvince() === null || empty($reportDTO->getProvince())) {
            array_push($list['businessdetails']['incomplete'], "Province is required.");
        }
        if($reportDTO->getTelephoneNumber() === null || empty($reportDTO->getTelephoneNumber())) {
            array_push($list['businessdetails']['incomplete'], "Telephone Number is required.");
        }
        // if($reportDTO->getNumberOfYearsInPresentAddress() === null || empty($reportDTO->getNumberOfYearsInPresentAddress())) {
        //     array_push($list['businessdetails']['optional'], "Number of Years in Present Address is required.");
        // }
        // if($reportDTO->getDateEstablished() === null || $reportDTO->getDateEstablished() === '0000-00-00') {
        //     array_push($list['businessdetails']['incomplete'], "Date Established is required.");
        // }
        if($reportDTO->getEmployeeSize() === null){
            array_push($list['businessdetails']['optional'], "Employee Size is required.");
        }
        // if($reportDTO->getNumberOfYearsManagementIsEngaged() === null || empty($reportDTO->getNumberOfYearsManagementIsEngaged())) {
        //     array_push($list['businessdetails']['incomplete'], "Number of Years Management Team has been Engaged in this Business is required.");
        // }

        /* Financial Statements - Incomplete Summary */
        if($reportDTO->getFinancialStatements() === null || empty($reportDTO->getFinancialStatements())) {
            array_push($list['financialstatement']['incomplete'], trans('business_details1.fs_must_be_uploaded'));
        }

        /* Balance Sheet Periods - Incomplete Summary */
        $balanceSheetPeriod = @count($this->documentDb->getBalanceSheetByReportId($reportDTO->getReportId()));
        $reportDTO->setBalanceSheetPeriodCount($balanceSheetPeriod);

        // if($reportDTO->getBalanceSheetPeriodCount() < 4) {
        //     array_push($list['financialstatement']['optional'], (4 - $reportDTO->getBalanceSheetPeriodCount())." more Balance Sheet Period(s) is ideal.");
        // }

        /* Income Statement Periods - Incomplete Summary */
        $incomeStatementPeriod = @count($this->documentDb->getIncomeStatementDocumentByReportId($reportDTO->getReportId()));
        $reportDTO->setIncomeStatementPeriodCount($incomeStatementPeriod);

        // if($reportDTO->getIncomeStatementPeriodCount() < 3) {
        //     array_push($list['financialstatement']['optional'], (3 - $reportDTO->getIncomeStatementPeriodCount())." more Income Statement Period(s) is ideal.");
        // }

        /* Bank Statements - Incomplete Summary */
        // if($reportDTO->getBankStatementCount() < 3) {
        //     array_push($list['cashproxy']['optional'], (3 - $reportDTO->getBankStatementCount())." more Bank Statements is ideal.");
        // }

        /* Utility Bills - Incomplete Summary */
        // if($reportDTO->getUtilityBillCount() < 3) {
        //     array_push($list['cashproxy']['optional'], (3 - $reportDTO->getUtilityBillCount())." more Utility Bills is ideal");
        // }
        
        $reportDTO->setIncompleteSummary($list);
    }

    public function getFinancialRatingResult($id){
        $entity = Report::select(
                    'tblentity.entityid',
                    'tblsmescore.score_sf',
                    'tblsmescore.score_rm',
                    'tblsmescore.score_cd',
                    'tblsmescore.score_sd',
                    'tblsmescore.score_bois',
                    'tblsmescore.score_boe',
                    'tblsmescore.score_mte',
                    'tblsmescore.score_bdms',
                    'tblsmescore.score_sp',
                    'tblsmescore.score_ppfi',
                    'tblsmescore.score_rfp',
                    'tblsmescore.score_rfpm',
                    'tblsmescore.score_facs'
                )
                ->leftJoin('tblsmescore', 'tblsmescore.entityid', '=', 'tblentity.entityid')
                ->where('tblentity.entityid', $id)
                ->first();

        $financialReport = FinancialReport::where(['entity_id' => $id,'is_deleted' => 0])->first();

        $frHelper = new FinancialHandler();
        $financialTotal = 0;
        $finalRatingScore = $frHelper->financialRatingComputation($financialReport->id);
        if($finalRatingScore) {
            $financialTotal = ($finalRatingScore[0] * 0.6) + ($finalRatingScore[1] * 0.4);
            $financialChecker = true;
        }

        $rating = '';

        if($financialChecker){
            //Financial Position
            if($financialTotal >= 1.6) {
                $rating = "AAA";
                $risk   = '1%';
            } elseif ($financialTotal>= 1.2 && $financialTotal< 1.6){
                $rating = "AA";
                $color  = '2%';
            } elseif ($financialTotal>= 0.8 && $financialTotal< 1.2) {
                $rating = "A - Good";
                $risk   = '3% - Low Risk';
            } elseif ($financialTotal>= 0.4 && $financialTotal< 0.8){
                $rating = "BBB";
                $risk   = '4% - Moderate Risk';
            } elseif ($financialTotal>= 0 && $financialTotal< 0.4){ 
                $rating  = "BB";
                $risk    = '6% - Moderate Risk';
            } elseif ($financialTotal>= -0.4 && $financialTotal< 0) {
                $rating = "B";
                $risk   = '8% - Moderate Risk';
            } elseif ($financialTotal>= -0.8 && $financialTotal< -0.4) {
                $rating = "CCC";
                $risk   = '15% - High Risk';
            } elseif ($financialTotal>= -1.2 && $financialTotal< -0.8){
                $rating = "CC";
                $risk   = '20% - High Risk';
            } elseif ($financialTotal>= -1.6 && $financialTotal< -1.2) {
                $rating = "C";
                $risk   = '25% - Very High Risk';
            } else {
                $rating = "D";
                $risk   = '32% - Very High Risk';
            }
        }elseif($entity->scoresf){
            $cred_rating_exp    = explode('-', $entity->score_sf);
            $rate_val           = str_replace(' ', '', $cred_rating_exp[0]);
            $rate_val   = intval($rate_val);

            if($rate_val >= 177){
                $rating = 'AA';
                $risk   = '2% - Very Low Risk';
            }
            else if($rate_val >= 150 && $rate_val <= 176) {
                $rating = 'A';
                $risk   = '3% - Low Risk';
            }
            else if($rate_val >= 123 && $rate_val <= 149) {
                $rating = 'BBB';
                $risk   = '4% - Moderate Risk';
            }
            else if($rate_val >= 96 && $rate_val <= 122) {
                $rating = 'BB';
                $risk   = '6% - Moderate Risk';
            }
            else if($rate_val >= 68 && $rate_val <= 95) {
                $rating = 'B';
                $risk   = '8% - High Risk';
            }
            else if($rate_val < 68) {
                $rating = 'CCC';
                if($rate_val >= 42 && $rate_val <= 68) {
                    $risk = '15% - High Risk';
                } else {
                    $risk = '30% - Very High Risk';
                }
            }
        }
        $entity = Report::where('entityid', $id)->first();
        $m_score    = array();
        if (7 == $entity->status) {
          //  $m_score    = $this->computeBeneishScore($entity->entityid);
          $m_score    = App::make('ProfileSMEController')->computeBeneishScore($entity->entityid);
        }
        
        $earning_manipulator = "The Report are Not earnings Manipulated";    
        if($m_score >= -2.22){
            $earning_manipulator = "The Report are earnings Manipulated";
        }
        $data = array(
            'earning_manipulator' => $earning_manipulator,
            'financial_score'   => $financialTotal,
            'financial_rating'  => $rating
        );
        return $data;
    }
public function computeBeneishScore($entity_id)
    {
    	try{
	        /*--------------------------------------------------------------------
	        /*	Variable Declaration
	        /*------------------------------------------------------------------*/
	        $other_lt_assets    = array();
	        $sts                = STS_OK;
	        $dsri               = 0;
	        $gmi                = 0;
	        $aqi                = 0;
	        $sgi                = 0;
	        $depi               = 0;
	        $depi_1             = 0;
	        $depi_2             = 0;
	        $depi_3             = 0;
	        $sgai_inc_1         = 0;
	        $sgai_inc_2         = 0;
	        $tata_profit_1      = 0;
	        $tata_profit_2      = 0;
	        $lvgi_liab_1        = 0;
	        $lvgi_liab_2        = 0;
	        $sgai               = 0;
	        $tata               = 0;
	        $lvgi               = 0;
	        $v5_model           = 0;
	        $v8_model           = 0;
	        $m_score            = 0;

	        /*--------------------------------------------------------------------
	        /*	Get the Uploaded Financial Statement Template
	        /*------------------------------------------------------------------*/
	        $fa_report = FinancialReport::where(['entity_id' => $entity_id, 'is_deleted' => 0])
	            ->orderBy('id', 'desc')
	            ->first();

	        if (NULL != $fa_report) {

	            /*----------------------------------------------------------
	            /*	Get Financial Statement Data
	            /*--------------------------------------------------------*/
	            $fa_report->balance_sheets      = $fa_report
	                ->balanceSheets()
	                ->orderBy('index_count', 'asc')
	                ->get();

				$fa_report->income_statements   = $fa_report
	                ->incomeStatements()
	                ->orderBy('index_count', 'asc')
	                ->get();

	            $fa_report->cashflow            = $fa_report
	                ->cashFlow()
	                ->orderBy('index_count', 'asc')
	                ->get();

	            if ((!isset($fa_report->balance_sheets[1]))
	            || (!isset($fa_report->income_statements[1]))
	            || (!isset($fa_report->cashflow[1]))) {
	                $sts    = STS_NG;
	            }

	            if ((0 == $fa_report->income_statements[0]->Revenue)
	            || (0 == $fa_report->income_statements[1]->Revenue)) {
	                $sts    = STS_NG;
	            }

	            if ((0 == $fa_report->balance_sheets[0]->Assets)
	            || (0 == $fa_report->balance_sheets[1]->Assets)) {
	                $sts    = STS_NG;
	            }

	            if (STS_OK == $sts) {
	                /*----------------------------------------------------------
	                /*	Compute Other L/T Assets
	                /*--------------------------------------------------------*/
	                $other_lt_assets[0] = $fa_report->balance_sheets[0]->Assets - $fa_report->balance_sheets[0]->PropertyPlantAndEquipment;
	                $other_lt_assets[0] = round($other_lt_assets[0], 2) - round($fa_report->balance_sheets[0]->CurrentAssets, 2);

	                $other_lt_assets[1] = $fa_report->balance_sheets[1]->Assets - $fa_report->balance_sheets[1]->PropertyPlantAndEquipment;
	                $other_lt_assets[1] = round($other_lt_assets[1], 2) - round($fa_report->balance_sheets[1]->CurrentAssets, 2);

	                if (0 == $other_lt_assets[0]) {
	                    $other_lt_assets[0] = 1;
	                }

	                if (0 == $other_lt_assets[1]) {
	                    $other_lt_assets[1] = 1;
	                }

	                /*----------------------------------------------------------
	                /*	Compute Days Sales in Receivables Index
	                /*--------------------------------------------------------*/
	                $dsri       = $fa_report->balance_sheets[1]->TradeAndOtherCurrentReceivables / $fa_report->income_statements[1]->Revenue;

	                if (0 != $dsri) {
	                    $dsri   = ($fa_report->balance_sheets[0]->TradeAndOtherCurrentReceivables / $fa_report->income_statements[0]->Revenue) / $dsri;
	                }
	                else {
	                    $dsri    = STS_NG;
	                }
	               


	                /*----------------------------------------------------------
	                /*	Compute Gross Margin Index
	                /*--------------------------------------------------------*/
	                $gmi    = ($fa_report->income_statements[1]->Revenue - $fa_report->income_statements[1]->CostOfSales) / $fa_report->income_statements[1]->Revenue;

	                if (0 != $gmi) {

	                    $gmi    = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[0]->CostOfSales) / $fa_report->income_statements[0]->Revenue) / $gmi;
	                }
	                else {
	                    $gmi    = STS_NG;
	                }
	                
	                /*----------------------------------------------------------
	                /*	Compute Asset Quality Index
	                /*--------------------------------------------------------*/
	                $aqi        = $other_lt_assets[1] / $fa_report->balance_sheets[1]->Assets;

	                if (0 != $aqi) {

	                    $aqi    = ($other_lt_assets[0] / $fa_report->balance_sheets[0]->Assets) / $aqi;
	                }
	                else {
	                    $aqi    = STS_NG;
	                }
	                

	                /*----------------------------------------------------------
	                /*	Compute Sales Growth Index
	                /*--------------------------------------------------------*/
	                $sgi    = $fa_report->income_statements[0]->Revenue / $fa_report->income_statements[1]->Revenue;

	                /*----------------------------------------------------------
	                /*	Compute Depreciation Index
	                /*--------------------------------------------------------*/
	                $depi_1 = $fa_report->cashflow[0]->Depreciation + $fa_report->balance_sheets[0]->PropertyPlantAndEquipment;
	                $depi_2 = $fa_report->cashflow[1]->Depreciation + $fa_report->balance_sheets[1]->PropertyPlantAndEquipment;

	                if (0 != $depi_2) {
	                    $depi_3 = $fa_report->cashflow[1]->Depreciation / $depi_2;
	                }

	                if ((0 != $depi_3)
	                && (0 != $depi_1)) {

	                    $depi   = ($fa_report->cashflow[0]->Depreciation / $depi_1) / $depi_3;
	                }
	                else {
	                    $depi    = STS_NG;
	                }
	                

	                /*----------------------------------------------------------
	                /*	Compute M-Score 5 Model Variable
	                /*--------------------------------------------------------*/
	                if (STS_OK == $sts) {
	                    $v5_model   = -6.065 + (0.823 * $dsri) + (0.906 * $gmi) + (0.593 * $aqi) + (0.717 * $sgi) + (0.107 * $depi);
	                }
	               

	                $sgai_inc_1     = $fa_report->income_statements[0]->OtherIncome + $fa_report->income_statements[0]->DistributionCosts + $fa_report->income_statements[0]->AdministrativeExpense + $fa_report->income_statements[0]->OtherExpenseByFunction + $fa_report->income_statements[0]->OtherGainsLosses;
	                $sgai_inc_2     = $fa_report->income_statements[1]->OtherIncome + $fa_report->income_statements[1]->DistributionCosts + $fa_report->income_statements[1]->AdministrativeExpense + $fa_report->income_statements[1]->OtherExpenseByFunction + $fa_report->income_statements[1]->OtherGainsLosses;

	                $tata_profit_2  = $fa_report->income_statements[1]->ComprehensiveIncome - $fa_report->cashflow[1]->NetCashByOperatingActivities;

	                $lvgi_liab_1    = $fa_report->balance_sheets[0]->OtherNoncurrentFinancialLiabilities + $fa_report->balance_sheets[0]->CurrentLiabilities;
	                $lvgi_liab_2    = $fa_report->balance_sheets[1]->OtherNoncurrentFinancialLiabilities + $fa_report->balance_sheets[1]->CurrentLiabilities;

	                if ((STS_OK == $sts)
	                && (0 != $sgai_inc_1)
	                && (0 != $lvgi_liab_1)
	                && (0 != $sgai_inc_2)
	                && (0 != $tata_profit_2)
	                && (0 != $lvgi_liab_2)) {
	                    /*----------------------------------------------------------
	                    /*	Compute Sales, General and Administrative Expenses Index
	                    /*--------------------------------------------------------*/
	                    $sgai   = $sgai_inc_2 / $fa_report->income_statements[1]->Revenue;

	                    if (0 != $sgai) {
	                        $sgai   = ($sgai_inc_1 / $fa_report->income_statements[0]->Revenue) / $sgai;
	                    }
	                    else {
	                        $sgai    = STS_NG;
	                    }

	                    /*----------------------------------------------------------
	                    /*	Compute Total Accruals to Total Assets
	                    /*--------------------------------------------------------*/
	                    $tata   = $tata_profit_2 / $fa_report->balance_sheets[1]->Assets;

	                    /*----------------------------------------------------------
	                    /*	Compute Leverage Index
	                    /*--------------------------------------------------------*/
	                    $lvgi   = $lvgi_liab_2 / $fa_report->balance_sheets[1]->Assets;

	                    if (0 != $lvgi) {
	                        $lvgi   = ($lvgi_liab_1 / $fa_report->balance_sheets[0]->Assets) / $lvgi;
	                    }
	                    else {
	                        $lvgi    = STS_NG;
	                    }

	                    /*----------------------------------------------------------
	                    /*	Compute M-Score 8 Model Variable
	                    /*--------------------------------------------------------*/
	                    if (STS_OK == $sts) {
	                        $v8_model   = -4.84 + (0.92 * $dsri) + (0.528 * $gmi) + (0.404 * $aqi) + (0.892 * $sgi) + (0.115 * $depi) - (0.172 * $sgai) + (4.679 * $tata) - (0.327 * $lvgi);
	                    }
	                }
	            }
	        }
	        

	        if (0 != $v8_model) {
	            $m_score = $v8_model;
	        }
	        else {
	            $m_score = $v5_model;
	        }
	       
	        return $m_score;
	    }catch(Exception $ex){
			
			if(env("APP_ENV") == "Production"){
				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
		}
    }
}
