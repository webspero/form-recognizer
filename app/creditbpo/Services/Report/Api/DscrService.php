<?php

namespace CreditBPO\Services\Report\Api;

use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\CashFlow;
use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Models\BankInterestRate;
use CreditBPO\Models\SMEScore;
use CreditBPO\Models\Bank;
use CreditBPO\DTO\DscrDTO;
use CreditBPO\DTO\FinancialReportDTO;
use CreditBPO\DTO\CashFlowDTO;
use CreditBPO\DTO\BalanceSheetDTO;
use CreditBPO\DTO\IncomeStatementDTO;
use CreditBPO\DTO\AssociatedOrganizationDTO;
use CreditBPO\Handlers\DebtServiceCapacityCalculatorHandler;
use \Exception as Exception;

class DscrService
{
    CONST ANNUALIZATION_WORKING_CAPITAL = 365;
    CONST ANNUALIZATION_TERM_LOAN = 360;

    CONST BANK_TYPE_GOVERNMENT = 'Government';
    CONST BANK_TYPE_CORPORATE = 'Corporate';

    public function __construct()
	{
        $this->reportDb = new Report;
        $this->financialReportDb = new FinancialReport;
        $this->cashFlowDb = new CashFlow;
        $this->balanceSheetDb = new BalanceSheet;
        $this->incomeStatementDb = new IncomeStatement;
        $this->dscrHandler = new DebtServiceCapacityCalculatorHandler;
        $this->bankInterestRateDb = new BankInterestRate;
        $this->smeScoreDb = new SMEScore;
        $this->assocOrgDb = new Bank;
        
        $this->dscrDTO = new DscrDTO;
        $this->financialReportDTO = new FinancialReportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Check if financial reports exist for a report, if it does set the DTO object to be used
	//---------------------------------------------------------------
    function getFinancialReports($reportId)
    {
        $this->dscrDTO->setReportId($reportId);

        $errors = array();

        $financialReport = $this->financialReportDb->getFinancialReportByReportId($this->dscrDTO->getReportId());

        if($financialReport !== null) {
            /* Set Financial Report */
            $this->financialReportDTO->setDbVars($financialReport);
            $this->dscrDTO->setFinancialReport($this->financialReportDTO);

            /* Set Bank Statements */
            $balanceSheets = $this->balanceSheetDb->getByFinancialReportId($this->dscrDTO->getFinancialReport()->getFinancialReportId());
            foreach ($balanceSheets as $balanceSheet) {
                $balanceSheetDTO = new BalanceSheetDTO;
                $balanceSheetDTO->setDbVars($balanceSheet);
                $this->dscrDTO->addBalanceSheet($balanceSheetDTO);
            }

            /* Set Income Statements */
            $incomeStatements = $this->incomeStatementDb->getByFinancialReportId($this->dscrDTO->getFinancialReport()->getFinancialReportId());
            foreach ($incomeStatements as $incomeStatement) {
                $incomeStatementDTO = new IncomeStatementDTO;
                $incomeStatementDTO->setDbVars($incomeStatement);
                $this->dscrDTO->addIncomeStatement($incomeStatementDTO);
            }

            /* Set Statement of Cashflow and Sources (NOI, Net Cash Flow) */
            $cashFlowStatements = $this->cashFlowDb->getByFinancialReportId($this->dscrDTO->getFinancialReport()->getFinancialReportId());
            $netOperatingIncome = null;
            $netCashByOperatingActivities = null;

            foreach ($cashFlowStatements as $cashFlow ) {
                if(is_null($netOperatingIncome) && $cashFlow['NetOperatingIncome'] != 0) {
                    $netOperatingIncome = $cashFlow['NetOperatingIncome'];
                }
                if(is_null($netCashByOperatingActivities) && $cashFlow['NetCashByOperatingActivities'] != 0) {
                    $netCashByOperatingActivities = $cashFlow['NetCashByOperatingActivities'];
                }
                $cashflowDTO = new CashFlowDTO;
                $cashflowDTO->setDbVars($cashFlow);
                $this->dscrDTO->addCashFlow($cashflowDTO);
            }
            $this->dscrDTO->setSources("NetOperatingIncome", $netOperatingIncome);
            $this->dscrDTO->setSources("NetCashByOperatingActivities",  $netCashByOperatingActivities);

            /* Set DSCR Record if exists */
            $dscrRecord = $this->dscrHandler->getByEntityId($this->dscrDTO->getReportId());
            if($dscrRecord !== null) {
                $this->dscrDTO->setDscrVars($dscrRecord);

                if($dscrRecord->source === 'Custom') {
                    $sourceValue = $dscrRecord->net_operating_income;
                } else {
                    $sourceValue = 0.00;
                }
                
                $this->dscrDTO->setSources("Custom", $sourceValue);
            }

            /* Set Current Bank Iterest Rates */
            $bankInterestRatesRecord = $this->bankInterestRateDb->getCurrentInterestRates();
            $this->dscrDTO->setBankInterestRates($bankInterestRatesRecord);

            // If report has SME Score, set source as ADB
            $smeScoreRecord = $this->smeScoreDb->getByReportId($this->dscrDTO->getReportId());
            if($smeScoreRecord !== null) {
                $this->dscrDTO->setSources("ADB", $smeScoreRecord->average_daily_balance);
            }

        } else {
            array_push($errors, "User has to upload a Financial Statement for this report using the FS Input Template in order to use the DSCR.");
        }

        $this->dscrDTO->setErrors($errors);

        return $this->dscrDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Validate user's inputs and update DSCR DTO
    //---------------------------------------------------------------
    public function validateRequestInputs($userInput, $dscrDTO)
    {
        /* Sets Validation Messages */
        $messages = array(
            'dscr.numeric' => 'The DSCR must be a number.',
            'dscr.between' => 'The DSCR must be between 0 and 5',
            'dscr.digits_between' => 'The DSCR must have a step value of 0.1 only.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'dscr' => 'numeric|between:0,5|digits_between:1,3',
            'source' => 'string',
            'sourceAmount' => 'numeric|min:0',
            'totalLoanDuration' => 'integer|min:0',
            'isCustom' => 'boolean',
            'localBankInterestRateWorkingCapital' => 'numeric|min:0',
            'localBankInterestRateTermLoan' => 'numeric|min:0',
            'foreignBankInterestRateWorkingCapital' => 'numeric|min:0',
            'foreignBankInterestRateTermLoan' => 'numeric|min:0'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($userInput, $rules, $messages);

        /* Interest Rates Custom Validation */
        $customerrors = array();
        if((isset($userInput['localBankInterestRateWorkingCapital']) || isset($userInput['localBankInterestRateTermLoan']) || isset($userInput['foreignBankInterestRateWorkingCapital']) || isset($userInput['foreignBankInterestRateTerlLoan'])) && (!isset($userInput['isCustom']) || $userInput['isCustom'] === false)) {
            array_push($customerrors, "To set Custom Interest Rates, set isCustom to true.");
        }

        /* Source Custom Validation */
        if(isset($userInput['source'])) {
            if(studly_case($userInput['source']) === studly_case("NetOperatingIncome")) {
                $userInput['source'] = "NetOperatingIncome";
            } else if (studly_case($userInput['source']) === studly_case("NetCashByOperatingActivities")) {
                $userInput['source'] = "NetCashByOperatingActivities";
            } else if (studly_case($userInput['source']) === studly_case("Custom")) {
                if(isset($userInput['sourceAmount'])) {
                    $userInput['source'] = "Custom";
                } else {
                    array_push($customerrors, "Source Amount is required if Source is set to Custom.");
                }
            } else {
                array_push($customerrors, "Invalid Source.");
            }
        }

        if(isset($userInput['sourceAmount']) && (!isset($userInput['source']) || studly_case($userInput['source']) !== studly_case("Custom"))) {
            array_push($customerrors, "To set Custom Source Amount, set Source to Custom first.");
        }
        
        if($validator->passes() && empty($customerrors)) {
            $dscrDTO->setRequestVars($userInput);
        } else {
            $dscrDTO->setErrors(array_merge($validator->messages()->all(), $customerrors));
        }

        return $dscrDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. DSCR Calculation
    //---------------------------------------------------------------
    public function calculateDSCR($dscrDTO)
    {
        /* Interest Rates */
        if(!$dscrDTO->getIsCustom()) {
            /* Reset to actual current interest rates */
            /* Get latest Bank Interest Rates based on Loan Duration */
            if($dscrDTO->getLoanDuration() > 60) {
                /* Use high for Term Loan */
                $dscrDTO->setTlIntRateLocal($dscrDTO->getBankInterestRates()['local']['high']);
                $dscrDTO->setTlIntRateForeign($dscrDTO->getBankInterestRates()['foreign']['high']);
            } else {
                /* Use medium for Term Loan */
                $dscrDTO->setTlIntRateLocal($dscrDTO->getBankInterestRates()['local']['mid']);
                $dscrDTO->setTlIntRateForeign($dscrDTO->getBankInterestRates()['foreign']['mid']);
            }

            /* Use low for Working Capital */
            $dscrDTO->setWcIntRateLocal($dscrDTO->getBankInterestRates()['local']['low']);
            $dscrDTO->setWcIntRateForeign($dscrDTO->getBankInterestRates()['foreign']['low']);
            
        }

        /* Generic Calculations needed for both */
        $sourceAmount = (null !== $dscrDTO->getSourceAmount()) ? $dscrDTO->getSourceAmount() : $dscrDTO->getCashflow()[0]->getNetOperatingIncome();
        $payment = $sourceAmount / $dscrDTO->getDscr();
        $loanAmount = $dscrDTO->getLoanDuration() * $payment / 12;

        $dscrDTO->setLoanAmount($loanAmount);

        if($dscrDTO->getLoanDuration() > 12) {
            /* Term Loan Computation */
            $dscrDTO->setWorkingCapitalValuesNA();

            /* Period */
            $period = 30;
            $dscrDTO->setTlPeriodLocal('1 Month');
            $dscrDTO->setTlPeriodForeign('1 Month');

            /* Payment Per Period */
            $localAnnualInterestRate = $dscrDTO->getTlIntRateLocal() / 100;
            $totalPaymentLocal = ($loanAmount + ($loanAmount * $localAnnualInterestRate * $period / self::ANNUALIZATION_TERM_LOAN));
            $paymentPerPeriodLocal = $totalPaymentLocal / $dscrDTO->getLoanDuration();
            
            $foreignAnnualInterestRate = $dscrDTO->getTlIntRateForeign() / 100;
            $totalPaymentForeign = ($loanAmount + ($loanAmount * $foreignAnnualInterestRate * $period / self::ANNUALIZATION_TERM_LOAN));
            $paymentPerPeriodForeign = $totalPaymentForeign / $dscrDTO->getLoanDuration();

            $dscrDTO->setTlPaymentPerPeriodLocal($paymentPerPeriodLocal);
            $dscrDTO->setTlPaymentPerPeriodForeign($paymentPerPeriodForeign);
            
            /* Total Loan Payment */
            $dscrDTO->setTlTotalLoanPaymentLocal($totalPaymentLocal);
            $dscrDTO->setTlTotalLoanPaymentForeign($totalPaymentForeign);
        } else {
            /* Working Capital Computation */
            $dscrDTO->setTermLoanValuesNA();

            /* Period */
            $procInventoryTurnover = null;
            $inventoryTurnover = (($dscrDTO->getBalanceSheets()[0]->getInventories() + $dscrDTO->getBalanceSheets()[1]->getInventories()) / 2) / ($dscrDTO->getIncomeStatements()[0]->getCostOfSales() / self::ANNUALIZATION_WORKING_CAPITAL);
            
            //Inventory Turnover may be 0
            $inventoryTurnover = (isset($procInventoryTurnover)) ? 0 : $procInventoryTurnover;

            $receivablesTurnover = (($dscrDTO->getBalanceSheets()[0]->GetTradeAndOtherCurrentReceivables() + $dscrDTO->getBalanceSheets()[1]->GetTradeAndOtherCurrentReceivables()) / 2) / ($dscrDTO->getIncomeStatements()[0]->getRevenue() / 365);
            $receivablesTurnover = is_nan($receivablesTurnover) ? 0 : $receivablesTurnover;

            $payablesTurnover = (($dscrDTO->getBalanceSheets()[0]->GetTradeAndOtherCurrentPayables() + $dscrDTO->getBalanceSheets()[1]->GetTradeAndOtherCurrentPayables() + $dscrDTO->getBalanceSheets()[0]->getCurrentProvisionsForEmployeeBenefits() + $dscrDTO->getBalanceSheets()[1]->getCurrentProvisionsForEmployeeBenefits()) / 2) / (($dscrDTO->getIncomeStatements()[0]->getCostOfSales() + $dscrDTO->getBalanceSheets()[0]->getInventories() - $dscrDTO->getBalanceSheets()[1]->getInventories()) / 365);
            $payablesTurnover = is_nan($payablesTurnover) ? 0 : $payablesTurnover;

            $ccc = abs($inventoryTurnover + $receivablesTurnover - $payablesTurnover);

            $dscrDTO->setWCPeriodLocal(intVal($ccc).' Days');
            $dscrDTO->setWcPeriodForeign(intVal($ccc).' Days');

            /* Interest Income Per Period */

            $localAnnualInterestRate = $dscrDTO->getWcIntRateLocal() / 100;

            $interestIncomePerPeriodLocal = $loanAmount * $localAnnualInterestRate * $ccc / self::ANNUALIZATION_WORKING_CAPITAL;

            $foreignAnnualInterestRate = $dscrDTO->getWcIntRateForeign() / 100;

            $interestIncomePerPeriodForeign = $loanAmount * $foreignAnnualInterestRate * $ccc / self::ANNUALIZATION_WORKING_CAPITAL;

            $dscrDTO->setWcInterestIncomePerPeriodLocal($interestIncomePerPeriodLocal);
            $dscrDTO->setWcInterestIncomePerPeriodForeign($interestIncomePerPeriodForeign);

            /* Total Interest Income */
            $totalInterestIncomeLocal = $interestIncomePerPeriodLocal * $dscrDTO->getLoanDuration();

            $totalInterestIncomeForeign = $interestIncomePerPeriodForeign * $dscrDTO->getLoanDuration();

            $dscrDTO->setWcTotalInterestIncomeLocal($totalInterestIncomeLocal);
            $dscrDTO->setWcTotalInterestIncomeForeign($totalInterestIncomeForeign);

        }
    }

    //---------------------------------------------------------------
	// Function #.a. Update DSCR Details
    //---------------------------------------------------------------
    public function updateDSCRRecord($dscrDTO)
    {
        /* Prepare data array to update DB record */
        $updatedDscr = $dscrDTO->getUpdateVars();

        try {
            $this->dscrHandler->updateDscr($updatedDscr);
            
        } catch (\Exception $e) {
            $dscrDTO->setErrors("An error occured during updating the report's DSCR");
            return false;
        }
        return true;
    }

    public function isFinancingAssocOrg($dscrDTO)
    {
        /* Get Report */
        $report = $this->reportDb->getReportByReportId($dscrDTO->getReportId());

        /* Based on report, get Bank */
        $assocOrg = $this->assocOrgDb->getBankById($report->current_bank);

        $assocOrgDTO = new AssociatedOrganizationDTO;
        $assocOrgDTO->setDbVars($assocOrg);

        $dscrDTO->setAssociatedOrganization($assocOrgDTO);

        /* Based on bank type, return if financing or not */
        if(studly_case($dscrDTO->getAssociatedOrganization()->getType()) === studly_case(self::BANK_TYPE_GOVERNMENT) || studly_case($dscrDTO->getAssociatedOrganization()->getType()) === studly_case(self::BANK_TYPE_CORPORATE)){ 
            return false;
        } else {
            return true;
        }
    }

    public function getRelevantDetails($dscrDTO){
        $relevantDetails = [
            'tradeAndOtherCurrentReceivables' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getBalanceSheets()[0]->GetTradeAndOtherCurrentReceivables(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getBalanceSheets()[1]->GetTradeAndOtherCurrentReceivables()
            ],
            'tradeAndOtherCurrentPayables' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getBalanceSheets()[0]->GetTradeAndOtherCurrentPayables(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getBalanceSheets()[1]->GetTradeAndOtherCurrentPayables()
            ],
            'currentProvisionsForEmployeeBenefits' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getBalanceSheets()[0]->getCurrentProvisionsForEmployeeBenefits(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getBalanceSheets()[1]->getCurrentProvisionsForEmployeeBenefits()
            ],
            'costOfSales' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getIncomeStatements()[0]->getCostOfSales(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getIncomeStatements()[1]->getCostOfSales()
            ],
            'inventories' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getBalanceSheets()[0]->getInventories(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getBalanceSheets()[1]->getInventories()
            ],
            'revenue' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getIncomeStatements()[0]->getRevenue(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getIncomeStatements()[1]->getRevenue()
            ],
            'interestExpense' => [
                $dscrDTO->getFinancialReport()->getYear() => $dscrDTO->getCashflow()[0]->getInterestExpense(),
                ($dscrDTO->getFinancialReport()->getYear())-1 => $dscrDTO->getCashflow()[1]->getInterestExpense()
            ]
        ];
        
        return $relevantDetails;
    }
}