<?php

namespace CreditBPO\Services\Report\Api;

use CreditBPO\Models\IndustryMain as MainIndustry;
use CreditBPO\Models\IndustrySub as SubIndustry;
use CreditBPO\Models\IndustryRow as Industry;
use CreditBPO\DTO\IndustryDTO;
use URL;

class IndustriesListService 
{
    public function __construct()
	{
        $this->industriesMainDb = new MainIndustry;
        $this->industriesSubDb = new SubIndustry;
        $this->industriesRowDb = new Industry;
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Main Industries
	//---------------------------------------------------------------
    function getMainIndustries($page = null, $limit = null)
    {
        if($page == null && $limit == null) {
            $mainIndustries = array();
            $mainIndustryRecords = $this->industriesMainDb->getMainIndustries();
            
            foreach ($mainIndustryRecords as $mainIndustry) {
                $mainIndustryDTO = new IndustryDTO;
                $mainIndustryDTO->setDropdownVars($mainIndustry);
                
                array_push($mainIndustries, $mainIndustryDTO->getDropdownVars());
            }
            
            return $mainIndustries;
        } else {
            $mainIndustryRecords = $this->industriesMainDb->getMainIndustriesByPage($page, $limit);
            $totalCount = @count($this->industriesMainDb->getMainIndustries());

            $results = array();
            $results['page'] = $page;
            $results['limit'] = $limit;
            $results['total'] = $totalCount;
            $results['items'] = array();
            $results['links'] = array(
                "prev" => null,
                "next" => null
            );
            
            if(($page+1) <= ceil($totalCount/$limit)) {
                $results['links']['next'] = URL::route('industriesmain.get')."?page=".($page+1)."&limit=".$limit;
            }
            if(($page-1) > 0) {
                $results['links']['prev'] = URL::route('industriesmain.get')."?page=".($page-1)."&limit=".$limit;
            }

            foreach ($mainIndustryRecords as $mainIndustry) {
                $mainIndustryDTO = new IndustryDTO;
                $mainIndustryDTO->setDropdownVars($mainIndustry);

                array_push($results['items'], $mainIndustryDTO->getDropdownVars());
            }

            if(!empty($results['items']))
                return $results;
            else
                return null;
        }
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Sub Industries
	//---------------------------------------------------------------
    function getSubIndustries($mainIndustryId)
    {
        $subIndustryRecords = $this->industriesSubDb->getSubIndustriesByMainId($mainIndustryId);

        $subIndustries = array();

        foreach ($subIndustryRecords as $subIndustry) {
            $subIndustryDTO = new IndustryDTO;
            $subIndustryDTO->setDropdownVars($subIndustry);

            array_push($subIndustries, $subIndustryDTO->getDropdownVars());
        }

        return $subIndustries;
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Sub Industries
	//---------------------------------------------------------------
    function getRowIndustries($industryMainId, $industrySubId)
    {
        $rowIndustryRecords = json_decode(json_encode($this->industriesRowDb->getRowIndustriesByMainAndSubID($industryMainId, $industrySubId)), true);
        $rowIndustries = array();

        foreach ($rowIndustryRecords as $rowIndustry) {
            $rowIndustryDTO = new IndustryDTO;
            $rowIndustryDTO->setDropdownVars($rowIndustry);

            array_push($rowIndustries, $rowIndustryDTO->getDropdownVars());
        }

        return $rowIndustries;
    }
}