<?php

namespace CreditBPO\Services\Report\Api;

use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Organization;
use CreditBPO\Models\Zipcode as Location;
use CreditBPO\Models\PseCompany;
use CreditBPO\Models\Province2;
use CreditBPO\Models\IndustryMain;
use CreditBPO\DTO\ReportDTO;
use CreditBPO\Libraries\BusinessDetailsLib;
use CreditBPO\Services\Report\Api\IndustriesListService;
use CreditBPO\Services\Report\Api\LocationListService;
use CreditBPO\Services\Report\Api\ReportService;

class BusinessDetailsService 
{

    public function __construct()
	{
        $this->reportDb = new Report;
        $this->reportDTO = new ReportDTO;
        $this->organizationDb = new Organization;
        $this->locationDb = new Location;
        $this->industriesListService = new IndustriesListService;
        $this->locationListService = new LocationListService;
        $this->reportService = new ReportService;
        $this->businessDetailsLibrary = new BusinessDetailsLib;
        $this->pseDb = new PseCompany;
        $this->locationDb2 = new Province2;
        $this->industryMainDb = new IndustryMain;
    }

    //---------------------------------------------------------------
	// Function #.a. Gets a report's Business Details
	//---------------------------------------------------------------
    function getBusinessDetails($reportId)
    {
        $reportRecord = $this->reportDb->getBusinessDetailsOfReport($reportId);
        $this->reportDTO->setBusinessDetails($reportRecord);

        /* Set Business Type and SEC/DTI Reg. No. and Date */
        if($reportRecord->entity_type == COMPANY_TYPE_CORPORATE) {
            $businessType = array(
                "value" => $reportRecord->entity_type,
                "text" => "Corporate"
            );
            $registrationNumber = array(
                "label" => "SEC Registration Number",
                "value" => $reportRecord->tin_num
            );
            $registrationDate = array(
                "label" => "SEC Registration Date",
                "value" => $reportRecord->sec_reg_date
            );
        } else if($reportRecord->entity_type == COMPANY_TYPE_SOLE_PROP) {
            $businessType = array(
                "value" => $reportRecord->entity_type,
                "text" => "Sole Proprietorship"
            );
            $registrationNumber = array(
                "label" => "DTI Registration Number",
                "value" => $reportRecord->tin_num
            );
            $registrationDate = array(
                "label" => "DTI Registration Date",
                "value" => $reportRecord->sec_reg_date
            );
        } else {
            $businessType = null;
            $registrationNumber = null;
            $registrationDate = null;
        }
        $this->reportDTO->setBusinessType($businessType);
        $this->reportDTO->setSecRegistrationNumber($registrationNumber);
        $this->reportDTO->setSecRegistrationDate($registrationDate);

        /* Set Employee Size */
        $employeeSizeRanges = BusinessDetailsLib::getEmployeeSizeRanges();

        foreach ($employeeSizeRanges as $employeeSize) {
            if($employeeSize['value'] == $reportRecord->employee_size) {
                $this->reportDTO->setEmployeeSize($employeeSize);
                break;
            }
        }

        return $this->reportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Check if report exists and if user owns the report
	//---------------------------------------------------------------
    function validateReport($reportId, $userId)
    {
        $errorMessages = array();

        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } elseif($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } elseif($checkReportOwner->status == 7){
            $errorMessages['errors'] = "Report is already done.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }

    //---------------------------------------------------------------
	// Function #.a. Fill Up Business Details - Field Validation
	//---------------------------------------------------------------
    function validateRequestFields($updateBusinessDetailsData)
    {
        /* Sets Validation Messages */
        $messages = array(
            'companytin.numeric'                => 'Company TIN format is invalid.',
            'businesstype.integer'              => 'Business Type value is invalid. Please make a Business Types List API Request to get list of valid Business Type values.',
            //'registrationnumber.numeric'        => 'Registration Number format is invalid.',
            'registrationdate.date'             => 'Registration Date is not a valid date.',
            'registrationdate.date_format'      => 'Registration Date format is invalid. Please follow this format: YYYY-MM-DD',
            'registrationdate.before'           => 'Registration Date must be a date before today',
            'companyemail.email'                => 'Company Email must be a valid email address.',
            'zipcode.numeric'                   => 'Zipcode value is invalid. Please make a City List API Request to get list of valid City and their Zipcode values.',
            'dateestablished.date'              => 'Date Established is not a valid date',
            'dateestablished.date_format'       => 'Date Established format is invalid. Please follow this format: YYYY-MM-DD',
            'dateestablished.before'            => 'Date Established must be a date before today.',
            'employeesize.integer'              => 'Employee Size value is invalid. Please make a Employee Size List API Request to get list of valid Employee Size values.',
            'updateddate.date'                  => 'Updated Date is not a valid date',
            'updateddate.date_format'           => 'Updated Date format is invalid. Please follow this format: YYYY-MM-DD',
            'associatedorganization.integer'    => 'Associated Organization value is invalid. Please make a Associated Organization List API Request to get list of valid Associated Organization values.',
            'telephonenumber.min'               => 'The Primary Business Address, Telephone should be a minimum of 7 and a maximum of 20 characters.',
            'telephonenumber.max'               => 'The Primary Business Address, Telephone should be a minimum of 7 and a maximum of 20 characters.',
            'fsInputWho.integer'                => 'Who will be Filling Up Financial Input Template value is Invalid. Value shoule be 1(Us) and 1(CreditBPO)',
            'companytin.required'               => 'The company TIN field is required.',
            'registrationnumber.required'       => 'The Business Registration number is required.',
            'registrationdate.required'         => 'The Registration Date field is required.',
            'employeesize.required'             => 'The empployee size field is required.',
            'fsInputWho.required'               => 'Who will be filling up FS Input Template field is required.',
            'streetaddress.required'            => 'The Primary Business Address, Room Building Street field is required.',
            'telephonenumber.required'          => 'The Primary Business Address, Telephone field is required',
            'outstandingcontracts.numeric'      => 'Outstanding Contracts should be numeric value.',
            /** Comment out deprecated field for future use */
            'totalassets.numeric'               => 'Total Assets format is invalid.',
            'totalassets.min'                   => 'Total assets should be a number between 0 and 9999999999.99.',
            'totalassets.max'                   => 'Total assets should be a number between 0 and 9999999999.99.',
            'totalassetsgrouping.integer'       => 'Total Asset Grouping value is invalid. Please make a Total Asset Grouping List API Request to get list of valid Total Asset Grouping values.',
            // 'yearsinpresentaddress.integer'     => 'No. of Years in Present Address must be a whole number.',
            // 'yearsinpresentaddress.min'         => 'No. of Years in Present Address should be a number between 0 and 100.',
            // 'yearsinpresentaddress.max'         => 'No. Of Years in Present Address should be a number between 0 and 100.',
            // 'yearsinbusiness.integer'           => 'No. of Years In Business must be a whole number.',
            // 'yearsinbusiness.min'               => 'No. of Years In Business should be a number between 0 and 100.',
            // 'yearsinbusiness.max'               => 'No. of Years in Business should be a number between 0 and 100.',
            // 'yearsmanagementisengaged.integer'  => 'Number of Years Management Team has been Engaged in this Business must be a whole number.',
            // 'yearsmanagementisengaged.min'      => 'Number of Years Management Team has been Engaged in this Business should be a number between 0 and 100.',
            // 'yearsmanagementisengaged.max'      => 'Number of Years Management Team has been Engaged in this Business should be a number between 0 and 100.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'companytin'            => 'required',
            'businesstype'          => 'integer',
            'registrationnumber'    => '|required',
            'registrationdate'      => 'required|date|date_format:Y-m-d|before:today',
            'companyemail'          => 'email',
            'zipcode'               => 'numeric',
            'dateestablished'       => 'date|date_format:Y-m-d|before:today',
            // 'employeesize'          => 'required|integer',
            'updateddate'           => 'date|date_format:Y-m-d',
            'associatedorganization'=> 'integer', 
            'telephonenumber'       => 'min:7|max:20',
            'fsInputWho'            => 'required|integer',
            'streetaddress'         => 'required',
            'telephonenumber'       => 'required',
            'outstandingcontracts'  => 'numeric',
            /** Comment out deprecated field for future use */
            'totalassets'           => 'numeric|min:0|max:9999999999.99',
            'totalassetsgrouping'   => 'integer',
            // 'yearsinpresentaddress' => 'integer|min:0|max:100',
            // 'yearsinbusiness'       => 'integer|min:0|max:100',
            // 'yearsmanagementisengaged' => 'integer|min:0|max:100',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($updateBusinessDetailsData, $rules, $messages);
        return $validator;
    }

    //---------------------------------------------------------------
	// Function #.a. Data Validation for Business Details prior to saving changes in the database
	//---------------------------------------------------------------
    function prepareReportUpdate($existingReportDTO, $updateReportData, $userId)
    {
        $updateReportDTO = new ReportDTO;
        $updateReportDTO->setUpdateRequestVars($updateReportData, $existingReportDTO->getReportId(), $userId);
        /* Data Validation */
        $errors = array();

        /* Validate Business Type if within valid list */
        if($updateReportDTO->getBusinessType() !== null) {
            $validBusinessTypes = array_keys($this->businessDetailsLibrary->businessTypes);

            /* Sets Validation Messages */
            $messages = array('businesstype.in' => 'Business Type value is invalid. Please refer to the Business Types List API Request to get list of valid Business Type values.');
            $rules = array('businesstype' => 'required|in:' . implode(',', $validBusinessTypes));

            $details['businesstype']  = $updateReportDTO->getBusinessType();
            $validator = Validator::make($details, $rules, $messages);

            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }

        /* Validate Associated Entity if it exists and if it accepts standalone reports */
        if($updateReportDTO->getAssociatedOrganization() !== null) {
            $validAssociatedOrganization = $this->organizationDb->getAssociatedOrganizationById($updateReportDTO->getAssociatedOrganization());

            if(!$validAssociatedOrganization) {
                array_push($errors, "Associated Organization value is invalid. Please refer to the Organization List API Request to get list of valid Associated Organization values.");
            }
        }

        /** Check if all fields for industry is complete */
        if ($updateReportDTO->getMainIndustry() !== null && $updateReportDTO->getSubIndustry() !== null && $updateReportDTO->getIndustry() !== null ) {
            /** Validate Industry Main */
            $validMainIndustries = $this->industriesListService->getMainIndustries();

            $validMainIndustry = false;

            foreach ($validMainIndustries as $mainIndustry) {
                if($updateReportDTO->getMainIndustry() == $mainIndustry['industryid']){
                    $validMainIndustry = true;
                    break;
                }
            }

            $mainlist = '';

            $mainlist = $validMainIndustries[0]['industryid'];
            $mainlist = $mainlist . '-' . $validMainIndustries[count($validMainIndustries)-1]['industryid'];

            if(!$validMainIndustry) {
                array_push($errors, "Main Industry value is invalid. Value shoud be [" . $mainlist . "].");
            }

            /* Validate Sub Industry if its active and belongs under report's Main Industry */
            $mainIndustry = $updateReportDTO->getMainIndustry();

            $validSubIndustries = $this->industriesListService->getSubIndustries($mainIndustry);
            $subList = '';

            if(count($validSubIndustries) > 1) {
                $sublist = $validSubIndustries[0]['industryid'];
                $sublist = $sublist . '-' . $validSubIndustries[count($validSubIndustries)-1]['industryid'];
            }elseif(count($validSubIndustries) == 1){
                $sublist = $validSubIndustries[0]['industryid'];
            }
            
            $validSubIndustry = false;

            foreach ($validSubIndustries as $subIndustry) {
                if($updateReportDTO->getSubIndustry() == $subIndustry['industryid']){
                    $validSubIndustry = true;
                    break;
                }
            }       

            if(!$validSubIndustry) {
                if(!$validMainIndustry){
                    array_push($errors, "Sub Industry value is invalid." );
                }else{
                    array_push($errors, "Sub Industry value is invalid. Sub value must be [" . $sublist . "]." );
                }
            }

            /* Validate Class Industry if its active and belongs under the report's Sub Industry */
            $subIndustry = $updateReportDTO->getSubIndustry();

            $validIndustries = $this->industriesListService->getRowIndustries($mainIndustry, $subIndustry);
            $validIndustry = false;
            $rowlist = '';

            foreach ($validIndustries as $industry) {
                if($updateReportDTO->getIndustry() == $industry['industryid']){
                    $validIndustry = true;
                    break;
                }
            }

            if(count($validIndustries) > 1){
                $rowlist = $validIndustries[0]['industryid'];
                $rowlist = $rowlist . '-' . $validIndustries[count($validIndustries)-1]['industryid'];
            }elseif(count($validIndustries) == 1){
                $rowlist = $validIndustries[0]['industryid'];
            }

            if(!$validIndustry) {
                if (!$validMainIndustry || !$validSubIndustry){
                    array_push($errors, "Industry value is invalid.");
                } else{
                    array_push($errors, "Industry value is invalid. Industry vaue should be [". $rowlist ."].");
                }
            }

        } elseif($updateReportDTO->getMainIndustry() == null && $updateReportDTO->getSubIndustry() == null && $updateReportDTO->getIndustry() == null){
        } else {
            array_push($errors, "Industry field has incomplete value. Please fill-up all fields for Industry Section.");
        }


        /* Validate Total Asset Grouping if within valid list */
        if($updateReportDTO->getTotalAssetsGrouping() !== null) {
            $validTotalAssetGroupings = array_keys($this->businessDetailsLibrary->assetGroupings); //BusinessDetailsLib::getTotalAssetGroupings();

            /* Sets Validation Messages */
            $messages = array('assetgroup.in' => 'Total Asset Grouping value is invalid. Please refer to the Total Asset Groupings List API Request to get list of valid Total Asset Grouping values.');
            $rules = array('assetgroup' => 'required|in:' . implode(',', $validTotalAssetGroupings));

            $details['assetgroup'] = $updateReportDTO->getTotalAssetsGrouping();
            $validator = Validator::make($details, $rules, $messages);

            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }


        /* Validate Province, City, and Zipcode */
        if($updateReportDTO->getCity() !== null && $updateReportDTO->getProvince() !== null) {
            /** Get city and province value */
            $cityId = $updateReportDTO->getCity();
            $province = $updateReportDTO->getProvince();
            $location_err = false;
            /** Check if Province is existing */
            $validProvince = $this->locationDb->getProvince($province);
            if(!$validProvince){
                array_push($errors, "Province value is invalid. Please refer to Location List API.");
                $prov_city_err = true;
            }
            /** Check if City is existing */
            $validCity = $this->locationDb->getCity($cityId, $province);
            if($validProvince && !$validCity){
                array_push($errors, "City value is invalid. Please refer to Location List API.");
                $prov_city_err = true;
            }
            /** Check Zipcode */
            if($updateReportDTO->getZipcode() !== null){
                $zipcode = $updateReportDTO->getZipcode();
                if($validCity && $updateReportDTO->getZipcode() !== $validCity->zipcode){
                    array_push($errors, "Zipcode value is invalid. Please refer to Location List API.");
                    $prov_city_err = true;
                }
            }
            if($location_err == false){
                /** Set Zipcode Value */
                $updateReportDTO->setCity($validCity->id);
                $updateReportDTO->setProvince($validProvince->provDesc);
                $updateReportDTO->setZipcode($validCity->zipcode);
            }
        }elseif($updateReportDTO->getCity() == null && $updateReportDTO->getProvince() == null){
        }else{
            array_push($errors, "Location field has incomplete value. Please fill-up Province and City Field. Please refere to Location List API.");
        }

        /* Validate Employee Size if within valid list */
        if($updateReportDTO->getEmployeeSize() !== null) {
            $validEmployeeSizes = array_keys($this->businessDetailsLibrary->employeeSizeRanges);

            /* Sets Validation Messages */
            $messages = array('employeesize.in' => 'Employee Size value is invalid. Please refer to the Employee Sizes List API Request to get list of valid Employee Size values.');
            $rules = array('employeesize' => 'required|in:' . implode(',', $validEmployeeSizes));

            $details['employeesize'] = $updateReportDTO->getEmployeeSize();
            $validator = Validator::make($details, $rules, $messages);
            
            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }

        /** Validate Who Will be Filling Up Financial Input -- Template Us(0) CreditBPO(1) */
        if($updateReportDTO->getWhoFillUpFS() !== null){
            if(!in_array($updateReportDTO->getWhoFillUpFS(), array(0,1))){
                array_push($errors, 'Who Will Be Filling Up FS Template is invalid. Please refer to the Who Will Be Filling Up FS Template List API Request to get list of valid values.');
            }
        }

        $updateReportDTO->setErrors($errors);

        return $updateReportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Data Validation for Business Details prior to saving changes in the database
	//---------------------------------------------------------------
    function updateBusinessDetails($updateReportDTO, $existingReportDTO)
    {
        /* Prepare dataset to be updated in the database */
        $toBeUpdated = array();

        if($updateReportDTO->getCompanyName() !== null) {
            $toBeUpdated['companyname'] = $updateReportDTO->getCompanyName();
        }
        if($updateReportDTO->getCompanyTIN() !== null) {
            $toBeUpdated['company_tin'] = $updateReportDTO->getCompanyTIN();
        }
        if($updateReportDTO->getBusinessType() !== null) {
            $businessType = $updateReportDTO->getBusinessType();
            $toBeUpdated['entity_type'] = $businessType;
        }
        if($updateReportDTO->getSecRegistrationNumber() !== null) {
            $toBeUpdated['tin_num'] = $updateReportDTO->getSecRegistrationNumber();
        }
        if($updateReportDTO->getSecRegistrationDate() !== null) {
            $toBeUpdated['sec_reg_date'] = $updateReportDTO->getSecRegistrationDate();
        }
        if($updateReportDTO->getMainIndustry() !== null) {
            $toBeUpdated['industry_main_id'] = $updateReportDTO->getMainIndustry();
        }
        if($updateReportDTO->getSubIndustry() !== null) {
            $toBeUpdated['industry_sub_id'] = $updateReportDTO->getSubIndustry();
        }
        if($updateReportDTO->getIndustry() !== null) {
            $toBeUpdated['industry_row_id'] = $updateReportDTO->getIndustry();
        }
        if($updateReportDTO->getCompanyWebsite() !== null) {
            $toBeUpdated['website'] = $updateReportDTO->getCompanyWebsite();
        }
        if($updateReportDTO->getCompanyEmail() !== null) {
            $toBeUpdated['email'] = $updateReportDTO->getCompanyEmail();
        }
        if($updateReportDTO->getRoomBldgSt() !== null) {
            $toBeUpdated['address1'] = $updateReportDTO->getRoomBldgSt();
        }
        if($updateReportDTO->getProvince() !== null) {
            $toBeUpdated['province'] = $updateReportDTO->getProvince();
        }
        if($updateReportDTO->getCity() !== null) {
            $toBeUpdated['cityid'] = $updateReportDTO->getCity();
        }
        if($updateReportDTO->getZipcode() !== null) {
            $toBeUpdated['zipcode'] = $updateReportDTO->getZipcode();
        }
        if($updateReportDTO->getTelephoneNumber() !== null) {
            $toBeUpdated['phone'] = $updateReportDTO->getTelephoneNumber();
        }
        if($updateReportDTO->getCompanyDescription() !== null) {
            $toBeUpdated['description'] = $updateReportDTO->getCompanyDescription();
        }
        if($updateReportDTO->getEmployeeSize() !== null) {
            $toBeUpdated['employee_size'] = $updateReportDTO->getEmployeeSize();
        }
        if($updateReportDTO->getUpdatedDate() !== null) {
            $toBeUpdated['updated_date'] = $updateReportDTO->getUpdatedDate();
        }
        if($updateReportDTO->getAssociatedOrganization() !== null) {
            $toBeUpdated['current_bank'] = $updateReportDTO->getAssociatedOrganization();
        }
        if($updateReportDTO->getWhoFillUpFS() !== null) {
            $toBeUpdated['fs_input_who'] = $updateReportDTO->getWhoFillUpFS();
        }
        if($updateReportDTO->getOutstandingContracts() !== null) {
            $toBeUpdated['value_of_outstanding_contracts'] = $updateReportDTO->getOutstandingContracts();
        }
        
        /** Comment out for future use - deprecated */
        if($updateReportDTO->getTotalAssets() !== null) {
            $toBeUpdated['total_assets'] = $updateReportDTO->getTotalAssets();
        }
        if($updateReportDTO->getTotalAssetsGrouping() !== null) {
            $toBeUpdated['total_asset_grouping'] = $updateReportDTO->getTotalAssetsGrouping();
        }
        // if($updateReportDTO->getNumberOfYearsInPresentAddress() !== null) {
        //     $toBeUpdated['no_yrs_present_address'] = $updateReportDTO->getNumberOfYearsInPresentAddress();
        // }
        // if($updateReportDTO->getDateEstablished() !== null) {
        //     $toBeUpdated['date_established'] = $updateReportDTO->getDateEstablished();
        // }
        // if($updateReportDTO->getNumberOfYearsInBusiness() !== null) {
        //     $toBeUpdated['number_year'] = $updateReportDTO->getNumberOfYearsInBusiness();
        // }
        // if($updateReportDTO->getNumberOfYearsManagementIsEngaged() !== null) {
        //     $toBeUpdated['number_year_management_team'] = $updateReportDTO->getNumberOfYearsManagementIsEngaged();
        // }
        $this->reportDb->updateBusinessDetails($updateReportDTO->getReportId(), $toBeUpdated);
    }

    //-----------------------------------------------------
    //  Function #: getPseCompaniesByPage
    //  Returns the list of pse companies with report by page
    public function getPseCompaniesByPage($page, $limit){
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['list'] = array();

        $pseList = $this->pseDb->getPseListByPage($page,$limit);
        $results['total'] = $this->pseDb->getPseList()->count();
        $results['list'] = $pseList->all();
        
        return $results;
    }

    public function getPseCompanies(){
        $results = array();
        $results['page'] = 1;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['list'] = array();

        /* Get standalone organizations in bank table */
        $pseList = $this->pseDb->getPseList();
        $results['total'] = $this->pseDb->getPseList()->count();  
        $results['list'] = $pseList->all();

        return $results;
    }

    public function getCityProvinceLocationByPage($page, $limit){
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['list'] = array();

        $location = $this->locationDb->getLocationByPage($page,$limit);
        $results['total'] = $this->locationDb->getLocation()->count();
        $results['list'] = $location->all();
        
        return $results;
    }

    public function getCityProvinceLocation(){
        $results = array();
        $results['page'] = 1;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['list'] = array();

        $location = $this->locationDb->getLocation();
        $results['total'] = $this->locationDb->getLocation()->count();
        $results['list'] = $location->all();
        
        return $results;
    }

    public function getIndustrySectionByPage($page, $limit){
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['list'] = array();

        $location = $this->industryMainDb->getIndustrySectionByPage($page,$limit);
        $results['total'] = $this->industryMainDb->getIndustrySection()->count();
        $results['list'] = $location->all();
        
        return $results;
    }

    public function getIndustrySection(){
        $results = array();
        $results['page'] = 1;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['list'] = array();

        $location = $this->industryMainDb->getIndustrySection();
        $results['total'] = $this->industryMainDb->getIndustrySection()->count();
        $results['list'] = $location->all();
        
        return $results;
    }
}