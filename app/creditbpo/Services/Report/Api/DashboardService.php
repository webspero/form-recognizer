<?php

namespace CreditBPO\Services\Report\Api;

use DateTime;
use URL;
use CreditBPO\Models\Entity as Report;
use CreditBPO\DTO\ReportDTO;

class DashboardService 
{
    public function __construct()
	{
        $this->reportDb = new Report;
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Reports
	//---------------------------------------------------------------
    function getReports($userId)
    {
        $reportRecords = $this->reportDb->getReportsByUserId($userId);
        $reports = array();

        foreach ($reportRecords as $report) {
            $reportDTO = new ReportDTO;
            $reportDTO->setVars($report);
            $reportDTO->setLink(URL::route('reports.get'), $report->entityid);

            if(!$report->is_paid){
                $reportDTO->setStatus("New Report");
            } elseif ($report->status != REPORT_STATUS_COMPLETE) {
                $reportDTO->setStatus("Ongoing Report");
            } elseif($report->status == REPORT_STATUS_COMPLETE){
                $reportDTO->setStatus("Report Finished ");
            }
            $reports["Report ".$report->entityid] = $reportDTO->getDashboardVars();
        }

        return $reports;
    }

    //---------------------------------------------------------------
	// Function #.b. Returns List of Reports By Page
	//---------------------------------------------------------------
    function getReportsByPage($userId, $page, $limit)
    {
        $reportRecords = $this->reportDb->getReportsByUserIdByPage($userId, $page, $limit);
        $totalCount = @count($this->reportDb->getReportsByUserId($userId));
        $reports = array();

        $results = array();
        $results['page'] = $page;
        $results['limit'] = $limit;
        $results['total'] = $totalCount;
        $results['items'] = array();

        foreach ($reportRecords as $report) {
            $reportDTO = new ReportDTO;
            $reportDTO->setVars($report);
            $reportDTO->setLink(URL::route('reports.get'), $report->entityid);

            if(!$report->is_paid){
                $reportDTO->setStatus("New Report");
            } elseif ($report->status != REPORT_STATUS_COMPLETE) {
                $reportDTO->setStatus("Ongoing Report");
            } elseif($report->status == REPORT_STATUS_COMPLETE){
                $reportDTO->setStatus("Report Finished ");
            }
            array_push($results['items'], $reportDTO->getDashboardVars());
        }
        return $results;
    }
}