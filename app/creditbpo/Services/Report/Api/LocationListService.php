<?php

namespace CreditBPO\Services\Report\Api;

use CreditBPO\Models\Zipcode;
use CreditBPO\DTO\LocationDTO;
use URL;

class LocationListService 
{
    public function __construct()
	{
        $this->zipcodesDb = new Zipcode;
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Provinces
	//---------------------------------------------------------------
    function getProvinces($page = null, $limit = null)
    {
        if($page == null && $limit == null) {
            $provinces = array();
            $provinceRecords = $this->zipcodesDb->getProvinces();
            
            foreach ($provinceRecords as $province) {
                $locationDTO = new LocationDTO;
                $locationDTO->setDropdownVars($province);

                $citiesLink = URL::route('cities.get')."?province=".urlencode($province->major_area);
                $locationDTO->setCitiesLink($citiesLink);
                
                array_push($provinces, $locationDTO->getProvincesDropDownVars());
            }
            
            if(!empty($provinces))
                return $provinces;
            else
                return null;
        } else {
            $provinceRecords = $this->zipcodesDb->getProvincesByPage($page, $limit);
            $totalCount = @count($this->zipcodesDb->getProvinces());

            $results = array();
            $results['page'] = $page;
            $results['limit'] = $limit;
            $results['total'] = $totalCount;
            $results['items'] = array();
            $results['links'] = array(
                "prev" => null,
                "next" => null
            );
            
            if(($page+1) <= ceil($totalCount/$limit)) {
                $results['links']['next'] = URL::route('provinces.get')."?page=".($page+1)."&limit=".$limit;
            }
            if(($page-1) > 0) {
                $results['links']['prev'] = URL::route('provinces.get')."?page=".($page-1)."&limit=".$limit;
            }

            foreach ($provinceRecords as $province) {
                $locationDTO = new LocationDTO;
                $locationDTO->setDropdownVars($province);

                $citiesLink = URL::route('cities.get')."?province=".urlencode($province->major_area);
                $locationDTO->setCitiesLink($citiesLink);

                array_push($results['items'], $locationDTO->getProvincesDropDownVars());
            }

            if(!empty($results['items']))
                return $results;
            else
                return null;
        }
    }

    //---------------------------------------------------------------
	// Function #.a. checkProvince
	//---------------------------------------------------------------
    function checkProvince($provinceName)
    {
        $provinceRecord =  $this->zipcodesDb->getProvinceDetailsByProvinceName($provinceName);

        $locationDTO = new LocationDTO;
        
        if($provinceRecord){
            $locationDTO->setProvince($provinceRecord['major_area']);
        }

        // foreach ($provinceRecords as $provinceRecord) {
        //     if(studly_case($provinceName) == studly_case($provinceRecord->major_area)) {
        //         $locationDTO->setProvince($provinceRecord->major_area);
        //         break;
        //     }
        // }

        return $locationDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Returns List of Cities with Zipcodes
	//---------------------------------------------------------------
    function getCities($provinceName)
    {
        $cityRecords = $this->zipcodesDb->getCitiesByProvince($provinceName);

        $cities = array();

        foreach ($cityRecords as $city) {
            $locationDTO = new LocationDTO;
            $locationDTO->setDropdownVars($city);
            $locationDTO->setProvince($provinceName);

            array_push($cities, $locationDTO->getCitiesDropdownVars());
        }

        return $cities;
    }
}