<?php

namespace CreditBPO\Services\Report\Api;

use Mail;
use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\BankSerialKey;
use CreditBPO\Models\IndependentKey;
use CreditBPO\Models\Discount;
use CreditBPO\Models\RelatedCompany;
use CreditBPO\Models\OfferedService;
use CreditBPO\Models\Capital;
use CreditBPO\Models\Location;
use CreditBPO\Models\Branch;
use CreditBPO\Models\Shareholder;
use CreditBPO\Models\Director;
use CreditBPO\Models\Certification;
use CreditBPO\Models\Businesstype;
use CreditBPO\Models\Insurance;
use CreditBPO\Models\CycleDetail;
use CreditBPO\Models\MajorCustomer;
use CreditBPO\Models\MajorCustomerAccount;
use CreditBPO\Models\MajorSupplier;
use CreditBPO\Models\MajorSupplierAccount;
use CreditBPO\Models\MajorLocation;
use CreditBPO\Models\Competitor;
use CreditBPO\Models\BusinessDriver;
use CreditBPO\Models\BusinessSegment;
use CreditBPO\Models\ProjectCompleted as CompletedProject;
use CreditBPO\Models\FutureGrowth;
use CreditBPO\Models\Owner;
use CreditBPO\Models\Spouse;
use CreditBPO\Models\Education;
use CreditBPO\Models\ManagedBusiness;
use CreditBPO\Models\PersonalLoan;
use CreditBPO\Models\KeyManager;
use CreditBPO\Models\PlanFacility;
use CreditBPO\Models\PlanFacilityRequested;
use CreditBPO\Models\Sustainability;
use CreditBPO\Models\RiskAssessment;
use CreditBPO\Models\RevenueGrowthPotential;
use CreditBPO\Models\User;
use CreditBPO\DTO\ReportDTO;
use CreditBPO\Models\CapacityExpansion;
use CreditBPO\Services\Report\Api\ReportService;
use CreditBPO\Models\Zipcode;
use CreditBPO\Services\Report\Api\IndustriesListService;
use \Exception as Exception;

class CreateReportService 
{
    public function __construct()
	{
        $this->reportDb = new Report;
        $this->bankKeyDb = new BankSerialKey;
        $this->independentKeyDb = new IndependentKey;
        $this->discountDb = new Discount;
        $this->relatedCompanyDb = new Relatedcompany;
        $this->offeredServiceDb = new OfferedService;
        $this->capitalDb = new Capital;
        $this->locationDb = new Location;
        $this->branchDb = new Branch;
        $this->shareholderDb = new Shareholder;
        $this->directorDb = new Director;
        $this->certificationDb = new Certification;
        $this->businessTypeDb = new Businesstype;
        $this->insuranceDb = new Insurance;
        $this->cycleDetailDb = new CycleDetail;
        $this->majorCustomerDb = new MajorCustomer;
        $this->majorCustomerAccountDb = new MajorCustomerAccount;
        $this->majorSupplierDb = new MajorSupplier;
        $this->majorSupplierAccountDb = new MajorSupplierAccount;
        $this->majorLocationDb = new MajorLocation;
        $this->competitorDb = new Competitor;
        $this->businessDriverDb = new BusinessDriver;
        $this->businessSegmentDb = new BusinessSegment;
        $this->completedProjectDb = new CompletedProject;
        $this->futureGrowthDb = new FutureGrowth;
        $this->ownerDb = new Owner;
        $this->spouseDb = new Spouse;
        $this->educationDb = new Education;
        $this->managedBusinessDb = new ManagedBusiness;
        $this->personalLoanDb = new PersonalLoan;
        $this->keyManagersDb = new KeyManager;
        $this->planFacilityDb = new PlanFacility;
        $this->planFacilityRequestedDb = new PlanFacilityRequested;
        $this->sustainabilityDb = new Sustainability;
        $this->riskAssessmentDb = new RiskAssessment;
        $this->revenueGrowthPotentialDb = new RevenueGrowthPotential;
        $this->capacityExpansionDb = new CapacityExpansion;
        $this->reportDTO = new ReportDTO;
        $this->reportService = new ReportService;
        $this->zipcodeDb = new Zipcode;
        $this->industriesListService = new IndustriesListService;
        $this->userDb = new User;
    }

    //-----------------------------------------------------
    //  Function #: validateCreateReportData
    //-----------------------------------------------------
    public function validateCreateReportData($newReportData)
    {
        /* Sets Validation Messages */
        $messages = array(
            'companyname.required'      => 'Company Name is required.',
            'businesstype.required'     => 'Business Type is required.',
            'organizationid.required'   => 'Organization Id is required.',
            'isagree.accepted'          => 'Please read and agree to the Terms of Use and Privacy Policy',
            'isagree.boolean'           => 'Please enter a valid value for Terms od Use and Agree Policy. Value should be True(0), False(1)',
            'province.required'         => 'Province is required',
            'city.required'             => 'City is required',
            'useprevious.required'      => 'Use Previous is required',
            'useprevious.boolean'       => 'Please enter a valid value for Use Previous. Value should be True(0), False(1).',
            'reporttype.required'       => 'Report Type is required',
            'mainindustry.required'     => 'Main Industry is required.',
            'subindustry.required'      => 'Division Industry is required.',
            'industry.required'         => 'Class Industry is required.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'companyname'       => 'required',
            'businesstype'      => 'required',
            'organizationid'    => 'required',
            'isagree'           => 'accepted|boolean',
            'province'          => 'required',
            'city'              => 'required',
            'useprevious'       => 'required|boolean',
            'reporttype'        => 'required',
            'mainindustry'      => 'required',
            'subindustry'       => 'required',
            'industry'        => 'required'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($newReportData, $rules, $messages);
        return $validator;
    }

    //-----------------------------------------------------
    //  Function #: prepareReportDTO
    //-----------------------------------------------------
    public function prepareReportDTO($newReportData, $userId)
    {
        $this->reportDTO->setRequestVars($newReportData, $userId);

        $errors = array();

        /** Check if report type is standalone(0), premium(1) or simplified (2) */
        $reportType = array(0,1,2);

        if( !in_array($this->reportDTO->getReportType(), $reportType) ){
            array_push($errors, "Report type is not a valid value. [ Standalone(0), Premium(1), Simplified(2) ].");
        }

        /* Check if use previous is set to true, and if the previous report id is own by user */
        if ($this->reportDTO->getUseOldData()){
            $report = $this->reportDb->getReportByReportId($this->reportDTO->getUseOldData());

            /** Check if first report */
            $checkIsFirst = $this->reportDb->getAllUserReport($userId);
            if(count($checkIsFirst) == 0){
                array_push($errors, "The user has no previous reports.");
            }elseif($report){
                if($report->loginid != $userId){
                    array_push($errors, "User is not the owner of the report.");
                }elseif($report->status != 7) {
                    array_push($errors, "Previous Report not yet completed");
                }
            }else{
                array_push($errors, "Previous Report doesn't exist");
            }
        }

        /* Check if the Client Key is valid */
        if($this->reportDTO->getClientKey() != null) {
            $isValidClientKey = true;

            /* Check and get Client Key Record, if any */
            $clientKey = $this->bankKeyDb->getBankClientKey($this->reportDTO->getClientKey(), $this->reportDTO->getAssociatedOrganization());
            if($clientKey == null) {
                $independentKey = $this->independentKeyDb->getIndependentKey($this->reportDTO->getClientKey());
                if($independentKey == null) {
                    $isValidClientKey = false;
                } else {
                    $this->reportDTO->setClientKey($independentKey);
                    $this->reportDTO->setKeyType(KEY_INDEPENDENT_TYPE);
                }
            } else {
                $this->reportDTO->setClientKey($clientKey);
                $this->reportDTO->setKeyType(KEY_BANK_TYPE);
            }
            
            if(!$isValidClientKey) {
                array_push($errors, "Incorrect Client Key.");
            }
        }
        
        /* Check Discount Code */
        if($this->reportDTO->getDiscountCode() != null) {
            $isValidDiscount = true;

            /* Check and get active Discount Record, if any */
            $discountCode = $this->discountDb->getDiscountByValidity($this->reportDTO->getDiscountCode(), ACTIVE);
            if($discountCode != null) {
                $this->reportDTO->setDiscountCode($discountCode);

                if($discountCode['type'] == 1 && $discountCode['amount'] == 100) {
                    $this->reportDTO->setIsFreeTrial(INT_TRUE);
                } else {
                    $this->reportDTO->setIsFreeTrial(INT_FALSE);
                }
            } else {
                $isValidDiscount = false;
            }

            if(!$isValidDiscount) {
                array_push($errors, "Incorrect Discount Code.");
            }
        }

        /* Adjust Business Type for Saving */
        if($this->reportDTO->getBusinessType() == "soleproprietorship") {
            $this->reportDTO->setBusinessType(COMPANY_TYPE_SOLE_PROP);
        } else if ($this->reportDTO->getBusinessType() == "corporation") {
            $this->reportDTO->setBusinessType(COMPANY_TYPE_CORPORATE);
        } else {
            array_push($errors, "Incorrect Business Type. Please select either 'Sole Proprietorship' or 'Corporation'");
        }

        /* Validate Province, City, and Zipcode */
        if($this->reportDTO->getCity() !== null && $this->reportDTO->getProvince() !== null) {
            $prov_city_err = false;
            /** Get city and province value */
            $cityId = $this->reportDTO->getCity();
            $province = $this->reportDTO->getProvince();

            /** Check if Province is existing */
            $validProvince = $this->zipcodeDb->getProvince($province);
            if(!$validProvince){
                array_push($errors, "Province value is invalid. Please refer to Cities List API.");
                $prov_city_err = true;
            }

            /** Check if City is existing */
            $validCity = $this->zipcodeDb->getCity($cityId, $province);
            if($validProvince && !$validCity){
                array_push($errors, "City value is invalid. Please refer to Cities List API.");
                $prov_city_err = true;
            }

            if($prov_city_err == false){
                /** Set Zipcode Value */
                $this->reportDTO->setCity($validCity->id);
                $this->reportDTO->setProvince($validProvince->provDesc);
                $this->reportDTO->setZipcode($validCity->zipcode);
            }
            
        }

        /* Validate ndustry Main, Industry Division, Industry Class Section */
        if ($this->reportDTO->getMainIndustry() !== null && $this->reportDTO->getSubIndustry() !== null && $this->reportDTO->getIndustry() !== null ){
            /** Validate Industry Main */
            $validMainIndustries = $this->industriesListService->getMainIndustries();
            $validMainIndustry = false;

            foreach ($validMainIndustries as $mainIndustry) {
                if($this->reportDTO->getMainIndustry() == $mainIndustry['industryid']){
                    $validMainIndustry = true;
                    break;
                }
            }

            $mainlist = '';

            $mainlist = $validMainIndustries[0]['industryid'];
            $mainlist = $mainlist . '-' . $validMainIndustries[count($validMainIndustries)-1]['industryid'];

            if(!$validMainIndustry) {
                array_push($errors, "Main Industry value is invalid. Value shoud be [" . $mainlist . "].");
            }

            /* Validate Sub Industry if its active and belongs under report's Main Industry */
            $mainIndustry = $this->reportDTO->getMainIndustry();
            $validSubIndustries = $this->industriesListService->getSubIndustries($mainIndustry);
            $subList = '';

            if(count($validSubIndustries) > 1) {
                $sublist = $validSubIndustries[0]['industryid'];
                $sublist = $sublist . '-' . $validSubIndustries[count($validSubIndustries)-1]['industryid'];
            }elseif(count($validSubIndustries) == 1){
                $sublist = $validSubIndustries[0]['industryid'];
            }

            $validSubIndustry = false;

            foreach ($validSubIndustries as $subIndustry) {
                if($this->reportDTO->getSubIndustry() == $subIndustry['industryid']){
                    $validSubIndustry = true;
                    break;
                }
            }

            if(!$validSubIndustry) {
                if(!$validMainIndustry){
                    array_push($errors, "Sub Industry value is invalid." );
                }else{
                    array_push($errors, "Sub Industry value is invalid. Sub value must be [" . $sublist . "]." );
                }
            }

            /* Validate Class Industry if its active and belongs under the report's Sub Industry */
            $subIndustry = $this->reportDTO->getSubIndustry();

            $validIndustries = $this->industriesListService->getRowIndustries($mainIndustry, $subIndustry);
            $validIndustry = false;
            $rowlist = '';

            foreach ($validIndustries as $industry) {
                if($this->reportDTO->getIndustry() == $industry['industryid']){
                    $validIndustry = true;
                    break;
                }
            }

            if(count($validIndustries) > 1){
                $rowlist = $validIndustries[0]['industryid'];
                $rowlist = $rowlist . '-' . $validIndustries[count($validIndustries)-1]['industryid'];
            }elseif(count($validIndustries) == 1){
                $rowlist = $validIndustries[0]['industryid'];
            }

            if(!$validIndustry) {
                if (!$validMainIndustry || !$validSubIndustry){
                    array_push($errors, "Industry value is invalid.");
                } else{
                    array_push($errors, "Industry value is invalid. Industry vaue should be [". $rowlist ."].");
                }
            }

            
        }




        $this->reportDTO->setErrors($errors);

        return $this->reportDTO;
    }

    //-----------------------------------------------------
    //  Function #: saveNewReport
    //-----------------------------------------------------
    public function saveNewReport(ReportDTO $reportDTO)
    {
        /* Prepare data for report */
        $discount = $reportDTO->getDiscountCode();
        $newReport = array(
            "loginid"           => $reportDTO->getUserId(),
            "companyname"       => $reportDTO->getCompanyName(),
            "entity_type"       => $reportDTO->getBusinessType(),
            "is_agree"          => $reportDTO->getIsAgree(),
            "status"            => REPORT_STATUS_PENDING,
            "is_independent"    => APPLIED_WITH_COMPANY,
            "current_bank"      => $reportDTO->getAssociatedOrganization(),
            "discount_code"     => $discount['discount_code'],
            "is_premium"        => $reportDTO->getReportType(),
            'city'              => $reportDTO->getCity(),
            'province'          => $reportDTO->getProvince(),
            'zipcode'           => $reportDTO->getZipcode(),
            'industry_main'  => $reportDTO->getMainIndustry(),
            'industry_sub'   => $reportDTO->getSubIndustry(),
            'industry_row'   => $reportDTO->getIndustry(),
            'isIndustry'    => 1
        );

        if($reportDTO->getIsFreeTrial() == INT_TRUE || $reportDTO->getClientKey() != null) {
            $newReport['is_paid'] = INT_TRUE;
        } else {
            $newReport['is_paid'] = INT_FALSE;
        }

        $reportDTO->setIsPaid($newReport['is_paid']);

        /* Pass dataset to database */
        $newReportId = $this->reportDb->addReportReturnId($newReport);
        
        /* Update ReportDTO with needed data i.e. Report ID */
        $reportDTO->setReportId($newReportId);
        
        /* Update Client Key if needed */
        if($reportDTO->getClientKey() != null) {
            if($reportDTO->getKeyType() == KEY_INDEPENDENT_TYPE) {
                $this->independentKeyDb->consumeKey($reportDTO->getClientKey(), $reportDTO->getUserId(), $reportDTO->getReportId());
            } else if($reportDTO->getKeyType() == KEY_BANK_TYPE) {
                $this->bankKeyDb->consumeKey($reportDTO->getClientKey(), $reportDTO->getUserId(), $reportDTO->getReportId());
            } else {
                /* No Processing */
            }
        }

        return $newReport;
    }

    //-----------------------------------------------------
    //  Function #: savePreviousReportData
    //-----------------------------------------------------
    public function savePreviousReportData(ReportDTO $reportDTO)
    {
        /* Get Previous Report Data and update Report DTO */
        $previousReport = $this->reportDb->getReportByReportId($reportDTO->getUseOldData());
        $reportDTO->setPreviousReport($previousReport);

        /* Get Previous Entity data and update New Entity data */
        $this->reportDb->updateWithPreviousData($reportDTO);

        /* Get Previous Relatedcompany data and update New Relatedcompany data  */
        $this->relatedCompanyDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Servicesoffer data and update New Servicesoffer data */
        $this->offeredServiceDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Capital data and update New Capital data */
        $this->capitalDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Locations data and update New Locations data */
        $this->locationDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Branches data and update New Branches data */
        $this->branchDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Shareholders data and update New Shareholders data */
        $this->shareholderDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Directors data and update New Directors data */
        $this->directorDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Certifications data and update New Certifications data */
        $this->certificationDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Businesstype data and update New Businesstype data */
        $this->businessTypeDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Insurance data and update New Insurance data */
        $this->insuranceDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous CycleDetail data and update New CycleDetail data */
        $this->cycleDetailDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Majorcustomer data and update New Majorcustomer data */
        $this->majorCustomerDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Majorcustomeraccount data and update new Majorcustomeraccount data IF is_agree */
        $this->majorCustomerAccountDb->createWithPreviousDataIfAgree($reportDTO, $previousReport['entityid']);

        /* Get Previous Majorsupplier data and update New Majorsupplier data */
        $this->majorSupplierDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Majorsupplieraccount data and update new Majorsupplieraccount data IF is_agree */
        $this->majorSupplierAccountDb->createWithPreviousDataIfAgree($reportDTO, $previousReport['entityid']);

        /* Get Previous Majorlocation data and update New Majorlocation data */
        $this->majorLocationDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Competitor data and update New Competitor data */
        $this->competitorDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Business Driver data and update New Business Driver data */
        $this->businessDriverDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Business Segment data and update New Business Segment data */
        $this->businessSegmentDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Past Projects data and update New Past Projects data */
        $this->completedProjectDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Future Growth data and update New Future Growth data */
        $this->futureGrowthDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* If the Business Type is Sole Proprietorship, get its Owners, Owners' Spouse, Owners' Education, Owners' Managed Business, Owners' Personal Loan, and Key Manager data tagged to last added Owner  */
        if($reportDTO->getBusinessType() == COMPANY_TYPE_SOLE_PROP) {

            /* Get Owners from PRevious Report */
            $ownersFromOldReport = $this->ownerDb->getFromPreviousReport($previousReport['entityid']);

            $reportDTO->setOwners($ownersFromOldReport);
            
            $newOwnerRecords = array();
            foreach ($ownersFromOldReport as $owner) {
                /* Get Previous Owners data and update New Owners data */
                $newOwnerRecord = $this->ownerDb->createWithPreviousData($reportDTO->getReportId(), $owner);

                array_push($newOwnerRecords, $newOwnerRecord);

                /* For each Owner, Get Previous Education data and update New Education data */
                $this->educationDb->createWithPreviousData($owner->ownerid, $newOwnerRecord->ownerid, $reportDTO->getReportId());

                /* For each Owner, Get Previous Managed Business data and update New Managed Business data */
                $this->managedBusinessDb->createWithPreviousData($owner->ownerid, $newOwnerRecord->ownerid, $reportDTO->getReportId());
                
                /* For each Owner, Get Previous Spouse data and update New Spouse data */
                $spouse = $this->spouseDb->getFromPreviousReport($owner->ownerid);
                $newSpouseRecord = $this->spouseDb->createWithPreviousData($newOwnerRecord->ownerid, $spouse, $reportDTO->getReportId());
                
                if($spouse != null) {
                    $this->educationDb->createWithPreviousData($spouse->spouseid, $newSpouseRecord->id, $reportDTO->getReportId());
                    
                    $this->managedBusinessDb->createWithPreviousData($spouse->spouseid, $newSpouseRecord->id, $reportDTO->getReportId());
                }
                
                /* For each Owner, Get Previous Personal Loan data and update New Personal Loan data */
                $this->personalLoanDb->createWithPreviousData($reportDTO->getReportId(), $owner, $newOwnerRecord);
            }
            
            /* Get Previous Key Managers data and update New Key Managers data tagged to last added Owner */
            $this->keyManagersDb->createWithPreviousData($reportDTO->getReportId(), $previousReport['entityid'], $newOwnerRecord);
        } else {
            /* Get Previous Key Managers data and update New Key Managers data */
            $this->keyManagersDb->createWithPreviousData($reportDTO->getReportId(), $previousReport['entityid'], null);
        }

        /* Get Previous Plan Facility data and update New Plan Facility data */
        $this->planFacilityDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Plan Facility Requested data and update New Plan Facility Requested data */
        $this->planFacilityRequestedDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Sustainability data and update New Sustainability data */
        $this->sustainabilityDb->createWithPreviousData($reportDTO, $previousReport['entityid']);

        /* Get Previous Risk Assessment data and update New Risk Assessment data */
        $riskAssessments = $this->riskAssessmentDb->getByEntityId($previousReport['entityid']);
        foreach($riskAssessments as $riskAssessment){
            $this->riskAssessmentDb->createWithPreviousData($reportDTO, $riskAssessment);
		}

        /* Get Previous Revenue Growth data and update New Revenue Growth data */
        $revenueGrowths = $this->revenueGrowthPotentialDb->getByReportId($previousReport['entityid']);
        foreach($revenueGrowths as $revenueGrowth){
            $this->revenueGrowthPotentialDb->createWithPreviousData($reportDTO, $revenueGrowth);
		}

        /* Get Previous Capacity for Expansion data and update New Capacity for Expansion data */
        $capacityExpansions = $this->capacityExpansionDb->getByReportId($previousReport['entityid']);
        foreach($capacityExpansions as $capacityExpansion){
            $this->capacityExpansionDb->createWithPreviousData($reportDTO, $capacityExpansion);
		}
    }

    public function sendCreatedReportEmail(ReportDTO $reportDTO){
        if (env("APP_ENV") != "local") {
            
            try{
                Mail::send('emails.newreport-notice', array('company_name' => $reportDTO->getCompanyName(), 'email_add' => $reportDTO->getCompanyEmail()), function($message) use ($reportDTO) {
                    $message->to('notify.user@creditbpo.com', $reportDTO->getCompanyName()) //to be changed
                    ->subject('New Report Created: ' . $reportDTO->getCompanyName());
                });
            }catch(Exception $e){
            //
            }
        
        }
    }

    public function checkBillingDetails($userId){
        $userDetails = $this->userDb->getUserById($userId);
        if(($userDetails) && ($userDetails->street_address != "") && ($userDetails->city != "") && ($userDetails->province != "") && ($userDetails->zipcode != "") && ($userDetails->phone != "")){
            return true;
        }
        return false;
    }
}