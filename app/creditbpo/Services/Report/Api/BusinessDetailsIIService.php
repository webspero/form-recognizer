<?php

namespace CreditBPO\Services\Report\Api;

use Illuminate\Support\Facades\Validator;


class BusinessDetailsIIService 
{

    public function __construct()
	{
        $this->reportDb = new Report;
        $this->reportDTO = new ReportDTO;
        $this->organizationDb = new Organization;
        $this->locationDb = new Location;
        $this->industriesListService = new IndustriesListService;
        $this->locationListService = new LocationListService;
        $this->reportService = new ReportService;
        $this->businessDetailsLibrary = new BusinessDetailsLib;
    }

    //---------------------------------------------------------------
	// Function #.a. Gets a report's Business Details
	//---------------------------------------------------------------
    function getBusinessDetails($reportId)
    {
        $reportRecord = $this->reportDb->getBusinessDetailsOfReport($reportId);
        $this->reportDTO->setBusinessDetails($reportRecord);

        /* Set Business Type and SEC/DTI Reg. No. and Date */
        if($reportRecord->entity_type == COMPANY_TYPE_CORPORATE) {
            $businessType = array(
                "value" => $reportRecord->entity_type,
                "text" => "Corporate"
            );
            $registrationNumber = array(
                "label" => "SEC Registration Number",
                "value" => $reportRecord->tin_num
            );
            $registrationDate = array(
                "label" => "SEC Registration Date",
                "value" => $reportRecord->sec_reg_date
            );
        } else if($reportRecord->entity_type == COMPANY_TYPE_SOLE_PROP) {
            $businessType = array(
                "value" => $reportRecord->entity_type,
                "text" => "Sole Proprietorship"
            );
            $registrationNumber = array(
                "label" => "DTI Registration Number",
                "value" => $reportRecord->tin_num
            );
            $registrationDate = array(
                "label" => "DTI Registration Date",
                "value" => $reportRecord->sec_reg_date
            );
        } else {
            $businessType = null;
            $registrationNumber = null;
            $registrationDate = null;
        }
        $this->reportDTO->setBusinessType($businessType);
        $this->reportDTO->setSecRegistrationNumber($registrationNumber);
        $this->reportDTO->setSecRegistrationDate($registrationDate);

        /* Set Total Assets Grouping */
        $tags = BusinessDetailsLib::getTotalAssetGroupings();

        foreach ($tags as $tag) {
            if($tag['value'] == $reportRecord->total_asset_grouping) {
                $this->reportDTO->setTotalAssetsGrouping($tag);
                break;
            }
        }

        /* Set Employee Size */
        $employeeSizeRanges = BusinessDetailsLib::getEmployeeSizeRanges();

        foreach ($employeeSizeRanges as $employeeSize) {
            if($employeeSize['value'] == $reportRecord->employee_size) {
                $this->reportDTO->setEmployeeSize($employeeSize);
                break;
            }
        }

        return $this->reportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Check if report exists and if user owns the report
	//---------------------------------------------------------------
    function validateReport($reportId, $userId)
    {
        $errorMessages = array();

        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } else if($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }

    //---------------------------------------------------------------
	// Function #.a. Fill Up Business Details - Field Validation
	//---------------------------------------------------------------
    function validateRequestFields($updateBusinessDetailsData)
    {
        /* Sets Validation Messages */
        $messages = array(
            'companytin.numeric' => 'Company TIN format is invalid.',
            'businesstype.integer' => 'Business Type value is invalid. Please make a Business Types List API Request to get list of valid Business Type values.',
            'registrationnumber.numeric' => 'Registration Number format is invalid.',
            'registrationdate.date' => 'Registration Date is not a valid date.',
            'registrationdate.date_format' => 'Registration Date format is invalid. Please follow this format: YYYY-MM-DD',
            'registrationdate.before' => 'Registration Date must be a date before today',
            'mainindustry.integer' => 'Main Industry value is invalid. Please make a Main Industry List API Request to get list of valid Main Industry values.',
            'subindustry.integer' => 'Sub Industry value is invalid. Please make a Sub Industry List API Request to get list of valid Sub Industry values.',
            'industry.integer' => 'Industry value is invalid. Please make a Industry List API Request to get list of valid Industry values.',
            'totalassets.numeric' => 'Total Assets format is invalid.',
            'totalassets.min' => 'Total assets should be a number between 0 and 9999999999.99.',
            'totalassets.max' => 'Total assets should be a number between 0 and 9999999999.99.',
            'totalassetsgrouping.integer' => 'Total Asset Grouping value is invalid. Please make a Total Asset Grouping List API Request to get list of valid Total Asset Grouping values.',
            'companyemail.email' => 'Company Email must be a valid email address.',
            'city.integer' => 'City value is invalid. Please make a City List API Request to get list of valid City values.',
            'zipcode.numeric' => 'Zipcode value is invalid. Please make a City List API Request to get list of valid City and their Zipcode values.',
            'yearsinpresentaddress.integer' => 'No. of Years in Present Address must be a whole number.',
            'yearsinpresentaddress.min' => 'No. of Years in Present Address should be a number between 0 and 100.',
            'yearsinpresentaddress.max' => 'No. Of Years in Present Address should be a number between 0 and 100.',
            'dateestablished.date' => 'Date Established is not a valid date',
            'dateestablished.date_format' => 'Date Established format is invalid. Please follow this format: YYYY-MM-DD',
            'dateestablished.before' => 'Date Established must be a date before today.',
            'yearsinbusiness.integer' => 'No. of Years In Business must be a whole number.',
            'yearsinbusiness.min' => 'No. of Years In Business should be a number between 0 and 100.',
            'yearsinbusiness.max' => 'No. of Years in Business should be a number between 0 and 100.',
            'employeesize.integer' => 'Employee Size value is invalid. Please make a Employee Size List API Request to get list of valid Employee Size values.',
            'updateddate.date' => 'Updated Date is not a valid date',
            'updateddate.date_format' => 'Updated Date format is invalid. Please follow this format: YYYY-MM-DD',
            'yearsmanagementisengaged.integer' => 'Number of Years Management Team has been Engaged in this Business must be a whole number.',
            'yearsmanagementisengaged.min' => 'Number of Years Management Team has been Engaged in this Business should be a number between 0 and 100.',
            'yearsmanagementisengaged.max' => 'Number of Years Management Team has been Engaged in this Business should be a number between 0 and 100.',
            'associatedorganization.integer' => 'Associated Organization value is invalid. Please make a Associated Organization List API Request to get list of valid Associated Organization values.',
        );

        /* Sets Validation Rules */
        $rules = array(
            'companytin' => 'numeric',
            'businesstype' => 'integer',
            'registrationnumber' => 'numeric',
            'registrationdate' => 'date|date_format:Y-m-d|before:today',
            'mainindustry' => 'integer',
            'subindustry' => 'integer',
            'industry' => 'integer',
            'totalassets' => 'numeric|min:0|max:9999999999.99',
            'totalassetsgrouping' => 'integer',
            'companyemail' => 'email',
            'city' => 'integer',
            'zipcode' => 'numeric',
            'yearsinpresentaddress' => 'integer|min:0|max:100',
            'dateestablished' => 'date|date_format:Y-m-d|before:today',
            'yearsinbusiness' => 'integer|min:0|max:100',
            'employeesize' => 'integer',
            'updateddate' => 'date|date_format:Y-m-d',
            'yearsmanagementisengaged' => 'integer|min:0|max:100',
            'associatedorganization' => 'integer',
        );

        /* Run Laravel Validation */
        $validator = Validator::make($updateBusinessDetailsData, $rules, $messages);
        return $validator;
    }

    //---------------------------------------------------------------
	// Function #.a. Data Validation for Business Details prior to saving changes in the database
	//---------------------------------------------------------------
    function prepareReportUpdate($existingReportDTO, $updateReportData, $userId)
    {
        $updateReportDTO = new ReportDTO;
        $updateReportDTO->setUpdateRequestVars($updateReportData, $existingReportDTO->getReportId(), $userId);
        /* Data Validation */
        $errors = array();

        /* Validate if requested Company Name already exists and not owne dby the user */
        if($updateReportDTO->getCompanyName() !== null) {
            $isValidName = $this->reportService->checkCompanyNameValidity($updateReportDTO->getUserId(), $updateReportDTO->getCompanyName()); //$this->reportDb->checkCompanyNameValidity($updateReportDTO->getUserId(), $updateReportDTO->getCompanyName());
            if(!$isValidName) {
                array_push($errors, "Company Name already exists.");
            }
        }

        /* Validate Business Type if within valid list */
        if($updateReportDTO->getBusinessType() !== null) {
            $validBusinessTypes = array_keys($this->businessDetailsLibrary->businessTypes);

            /* Sets Validation Messages */
            $messages = array('businesstype.in' => 'Business Type value is invalid. Please refer to the Business Types List API Request to get list of valid Business Type values.');
            $rules = array('businesstype' => 'required|in:' . implode(',', $validBusinessTypes));

            $details['businesstype']  = $updateReportDTO->getBusinessType();
            $validator = Validator::make($details, $rules, $messages);

            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }

        /* Validate Associated Entity if it exists and if it accepts standalone reports */
        if($updateReportDTO->getAssociatedOrganization() !== null) {
            $validAssociatedOrganization = $this->organizationDb->getAssociatedOrganizationById($updateReportDTO->getAssociatedOrganization());

            if(!$validAssociatedOrganization) {
                array_push($errors, "Associated Organization value is invalid. Please refer to the Organization List API Request to get list of valid Associated Organization values.");
            }
        }

        /* Validate Main Industry */
        if($updateReportDTO->getMainIndustry() !== null) {
            $validMainIndustries = $this->industriesListService->getMainIndustries();

            $validMainIndustry = false;

            foreach ($validMainIndustries as $mainIndustry) {
                if($updateReportDTO->getMainIndustry() == $mainIndustry['industryid']){
                    $validMainIndustry = true;
                    break;
                }
            }

            if(!$validMainIndustry) {
                array_push($errors, "Main Industry value is invalid. Please refer to the Main Industry List API Request to get list of valid Main Industry values.");
            }
        }

        /* Validate Sub Industry if its active and belongs under report's Main Industry */
        if($updateReportDTO->getSubIndustry() !== null) {

            if($updateReportDTO->getMainIndustry() !== null) {
                $mainIndustry = $updateReportDTO->getMainIndustry();
            } else {
                $mainIndustry = $existingReportDTO->getMainIndustry();
                $mainIndustry = $mainIndustry['value'];
            }

            $validSubIndustries = $this->industriesListService->getSubIndustries($mainIndustry);

            $validSubIndustry = false;

            foreach ($validSubIndustries as $subIndustry) {
                if($updateReportDTO->getSubIndustry() == $subIndustry['industryid']){
                    $validSubIndustry = true;
                    break;
                }
            }

            if(!$validSubIndustry) {
                array_push($errors, "Sub Industry value is invalid. Please refer to the Sub Industry List API Request to get list of valid Sub Industry values.");
            }
        }

        /* Validate Row Industry if its active and belongs under the report's Sub Industry */
        if($updateReportDTO->getIndustry() !== null) {

            if($updateReportDTO->getMainIndustry() !== null) {
                $mainIndustry = $updateReportDTO->getMainIndustry();
            } else {
                $mainIndustry = $existingReportDTO->getMainIndustry();
                $mainIndustry = $mainIndustry['value'];
            }

            if($updateReportDTO->getSubIndustry() !== null) {
                $subIndustry = $updateReportDTO->getSubIndustry();
            } else {
                $subIndustry = $existingReportDTO->getSubIndustry();
                $subIndustry = $subIndustry['value'];
            }

            $validIndustries = $this->industriesListService->getRowIndustries($mainIndustry, $subIndustry);
            $validIndustry = false;

            foreach ($validIndustries as $industry) {
                if($updateReportDTO->getIndustry() == $industry['industryid']){
                    $validIndustry = true;
                    break;
                }
            }

            if(!$validIndustry) {
                array_push($errors, "Industry value is invalid. Please refer to the Industry List API Request to get list of valid Industry values.");
            }
        }

        /* Validate Total Asset Grouping if within valid list */
        if($updateReportDTO->getTotalAssetsGrouping() !== null) {
            $validTotalAssetGroupings = array_keys($this->businessDetailsLibrary->assetGroupings); //BusinessDetailsLib::getTotalAssetGroupings();

            /* Sets Validation Messages */
            $messages = array('assetgroup.in' => 'Total Asset Grouping value is invalid. Please refer to the Total Asset Groupings List API Request to get list of valid Total Asset Grouping values.');
            $rules = array('assetgroup' => 'required|in:' . implode(',', $validTotalAssetGroupings));

            $details['assetgroup'] = $updateReportDTO->getTotalAssetsGrouping();
            $validator = Validator::make($details, $rules, $messages);

            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }


        /* Validate Province, City, and Zipcode */
        if($updateReportDTO->getCity() !== null || $updateReportDTO->getProvince() !== null || $updateReportDTO->getZipcode() !==null) {

            if($updateReportDTO->getCity() !== null) {
                $cityId = $updateReportDTO->getCity();
            } else {
                $cityId = $existingReportDTO->getCity();
                $cityId = $cityId['value'];
            }

            if($updateReportDTO->getProvince() !== null) {
                $province = $updateReportDTO->getProvince();
            } else {
                $province = $existingReportDTO->getProvince();
            }
 
            if($updateReportDTO->getZipcode() !== null) {
                $zipcode = $updateReportDTO->getZipcode();
            } else {
                $zipcode = $existingReportDTO->getZipcode();
            }

            $validCity = $this->locationDb->getCityById($cityId);

            if(studly_case($province) != studly_case($validCity['major_area'])) {
                if($updateReportDTO->getProvince() !== null) {
                    array_push($errors, "When changing the Province, please make sure to change the City and its Zipcode based on the list of valid cities for that province (Refer to Cities List API).");
                } else if ($updateReportDTO->getCity() !== null) {
                    array_push($errors, "When changing the City, please make sure that it is in the list of cities for the report's province (Refer to Cities List Api).");
                }
            }

            if($cityId != $validCity['id']) {
                array_push($errors, "City value is invalid. Please refer to the City List API Request to get list of valid City values for the report's province.");
            }

            if($zipcode != $validCity['zip_code']) {
                if($updateReportDTO->getCity() !== null) {
                    array_push($errors, "When changing the City, please make sure that the zipcode is updated as well based on the list of cities for the report's province (Refer to Cities List Api).");
                } else if($updateReportDTO->getZipcode() !== null) {
                    array_push($errors, "When changing the Zipcode, please make sure that it is the correct zipcode for the report's city (Refer to Cities List API).");
                }
            }
        }

        /* Validate Employee Size if within valid list */
        if($updateReportDTO->getEmployeeSize() !== null) {
            $validEmployeeSizes = array_keys($this->businessDetailsLibrary->employeeSizeRanges);

            /* Sets Validation Messages */
            $messages = array('employeesize.in' => 'Employee Size value is invalid. Please refer to the Employee Sizes List API Request to get list of valid Employee Size values.');
            $rules = array('employeesize' => 'required|in:' . implode(',', $validEmployeeSizes));

            $details['employeesize'] = $updateReportDTO->getEmployeeSize();
            $validator = Validator::make($details, $rules, $messages);
            
            if($validator->fails()){
                foreach($validator->messages()->all() as $error){
                    array_push($errors, $error);
                }
            }
        }

        $updateReportDTO->setErrors($errors);

        return $updateReportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Data Validation for Business Details prior to saving changes in the database
	//---------------------------------------------------------------
    function updateBusinessDetails($updateReportDTO, $existingReportDTO)
    {
        /* Prepare dataset to be updated in the database */
        $toBeUpdated = array();

        if($updateReportDTO->getCompanyName() !== null) {
            $toBeUpdated['companyname'] = $updateReportDTO->getCompanyName();
        }
        if($updateReportDTO->getCompanyTIN() !== null) {
            $toBeUpdated['company_tin'] = $updateReportDTO->getCompanyTIN();
        }
        if($updateReportDTO->getBusinessType() !== null) {
            $businessType = $updateReportDTO->getBusinessType();
            $toBeUpdated['entity_type'] = $businessType;
        }
        if($updateReportDTO->getSecRegistrationNumber() !== null) {
            $toBeUpdated['tin_num'] = $updateReportDTO->getSecRegistrationNumber();
        }
        if($updateReportDTO->getSecRegistrationDate() !== null) {
            $toBeUpdated['sec_reg_date'] = $updateReportDTO->getSecRegistrationDate();
        }
        if($updateReportDTO->getMainIndustry() !== null) {
            $toBeUpdated['industry_main_id'] = $updateReportDTO->getMainIndustry();
        }
        if($updateReportDTO->getSubIndustry() !== null) {
            $toBeUpdated['industry_sub_id'] = $updateReportDTO->getSubIndustry();
        }
        if($updateReportDTO->getIndustry() !== null) {
            $toBeUpdated['industry_row_id'] = $updateReportDTO->getIndustry();
        }
        if($updateReportDTO->getTotalAssets() !== null) {
            $toBeUpdated['total_assets'] = $updateReportDTO->getTotalAssets();
        }
        if($updateReportDTO->getTotalAssetsGrouping() !== null) {
            $toBeUpdated['total_asset_grouping'] = $updateReportDTO->getTotalAssetsGrouping();
        }
        if($updateReportDTO->getCompanyWebsite() !== null) {
            $toBeUpdated['website'] = $updateReportDTO->getCompanyWebsite();
        }
        if($updateReportDTO->getCompanyEmail() !== null) {
            $toBeUpdated['email'] = $updateReportDTO->getCompanyEmail();
        }
        if($updateReportDTO->getRoomBldgSt() !== null) {
            $toBeUpdated['address1'] = $updateReportDTO->getRoomBldgSt();
        }
        if($updateReportDTO->getProvince() !== null) {
            $toBeUpdated['province'] = $updateReportDTO->getProvince();
        }
        if($updateReportDTO->getCity() !== null) {
            $toBeUpdated['cityid'] = $updateReportDTO->getCity();
        }
        if($updateReportDTO->getZipcode() !== null) {
            $toBeUpdated['zipcode'] = $updateReportDTO->getZipcode();
        }
        if($updateReportDTO->getTelephoneNumber() !== null) {
            $toBeUpdated['phone'] = $updateReportDTO->getTelephoneNumber();
        }
        if($updateReportDTO->getNumberOfYearsInPresentAddress() !== null) {
            $toBeUpdated['no_yrs_present_address'] = $updateReportDTO->getNumberOfYearsInPresentAddress();
        }
        if($updateReportDTO->getDateEstablished() !== null) {
            $toBeUpdated['date_established'] = $updateReportDTO->getDateEstablished();
        }
        if($updateReportDTO->getNumberOfYearsInBusiness() !== null) {
            $toBeUpdated['number_year'] = $updateReportDTO->getNumberOfYearsInBusiness();
        }
        if($updateReportDTO->getCompanyDescription() !== null) {
            $toBeUpdated['description'] = $updateReportDTO->getCompanyDescription();
        }
        if($updateReportDTO->getEmployeeSize() !== null) {
            $toBeUpdated['employee_size'] = $updateReportDTO->getEmployeeSize();
        }
        if($updateReportDTO->getUpdatedDate() !== null) {
            $toBeUpdated['updated_date'] = $updateReportDTO->getUpdatedDate();
        }
        if($updateReportDTO->getNumberOfYearsManagementIsEngaged() !== null) {
            $toBeUpdated['number_year_management_team'] = $updateReportDTO->getNumberOfYearsManagementIsEngaged();
        }
        if($updateReportDTO->getAssociatedOrganization() !== null) {
            $toBeUpdated['current_bank'] = $updateReportDTO->getAssociatedOrganization();
        }

        $this->reportDb->updateBusinessDetails($updateReportDTO->getReportId(), $toBeUpdated);
    }
}