<?php

namespace CreditBPO\Services\Report\Api;

use URL;
use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\DTO\ReportDTO;

class ReportService 
{
    public function __construct()
	{
        $this->reportDb = new Report;
        $this->reportDTO = new ReportDTO;
    }

    //---------------------------------------------------------------
	// Function #.a. Check if report exists and if user owns the report
	//---------------------------------------------------------------
    function validateReport($reportId, $userId)
    {
        $errorMessages = array();

        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } else if($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }

    //-----------------------------------------------------
    //  Function #: checkCompanyNameValidity
    //  	Check if company name is not yet used
    //-----------------------------------------------------
    public function checkCompanyNameValidity($userId, $companyName)
	{
        $isValid = false;
		$companyData = $this->reportDb->getCompanyReportList($companyName);
        
        /* If Name is found in database, check if user owns these report records */
        if(@count($companyData) > 0) {
            foreach ($companyData as $company) {
                if($company["loginid"] == $userId) {
                    $isValid = true;
                    break;
                }
            }
        } else {
            $isValid = true;
        }

        return $isValid;
    }

}