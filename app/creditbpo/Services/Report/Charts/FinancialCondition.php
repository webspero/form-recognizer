<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class FinancialCondition extends Chart {
    const CHART_TYPE = 3;

    /**
     * @param int $entityId
     * @return string
     */
    public function createPieChart($entityId) {
        $score = $this->getSMEScore($entityId);

        $boeName = trans('risk_rating.mq_business_owner');
        $mteName = trans('risk_rating.mq_management_team');
        $bdmsName = trans('risk_rating.mq_business_driver');
        $spName = trans('risk_rating.mq_succession_plan');
        $ppfiName = trans('risk_rating.mq_past_project');

        $ratingText = $this->getRatingText($score->score_facs);
        $chartOptions = $this->getChartOptions($score->score_facs, $ratingText);

        return $this->generateEntityChart($chartOptions, $entityId);
    }

    /**
     * @param float $score
     * @return string
     */
    protected function getRatingText($score) {
        $style = $this->getRatingTextStyle($score);
        // $ratingStr = '<div><span style="text-align: center; font-size: 50px;  margin-left: %spx; float:left; position: relative; top: -40px;">%s</span></div>';
        // return sprintf($ratingStr, $style['margin_left'], $style['rating']);
        return $style;
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getRatingTextStyle($score) {
        if ($score > 161) {
            return [
                'margin_left' => -10,
                'rating' => 'AAA',
                'R' => 48,
                'G' => 204,
                'B' => 116
            ];
        }

        if ($score > 144) {
            return [
                'margin_left' => 0,
                'rating' => 'AA',
                'R' => 48,
                'G' => 204,
                'B' => 100
            ];
        }

        if ($score > 126) {
            return [
                'margin_left' => 14,
                'rating' => 'A',
                'R' => 120,
                'G' => 204,
                'B' => 76
            ];
        }

        if ($score > 109) {
            return [
                'margin_left' => -10,
                'rating' => 'BBB',
                'R' => 168,
                'G' => 192,
                'B' => 52
            ];
        }

        if ($score > 91) {
            return [
                'margin_left' => 0,
                'rating' => 'BB',
                'R' => 208,
                'G' => 196,
                'B' => 36
            ];
        }

        if ($score > 74) {
            return [
                'margin_left' => 14,
                'rating' => 'B',
                'R' => 252,
                'G' => 196,
                'B' => 52
            ];
        }

        if ($score > 56) {
            return [
                'margin_left' => -10,
                'rating' => 'CCC',
                'R' => 248,
                'G' => 204,
                'B' => 76
            ];
        }

        if ($score > 39) {
            return [
                'margin_left' => 0,
                'rating' => 'CC',
                'R' => 248,
                'G' => 204,
                'B' => 108
            ];
        }

        if ($score > 21) {
            return [
                'margin_left' => 14,
                'rating' => 'C',
                'R' => 256,
                'G' => 228,
                'B' => 164
            ];
        }

        if ($score > 3) {
            return [
                'margin_left' => 14,
                'rating' => 'D',
                'R' => 256,
                'G' => 212,
                'B' => 196
            ];
        }

        return [
            'margin_left' => 14,
            'rating' => 'E',
            'R' => 236,
            'G' => 84,
            'B' => 76
        ];
    }

    /**
     * @param int $score
     * @param string $ratingText
     * @return array
     */
    protected function getChartOptions($score, $ratingText) {
        $chartOptions = [];

        $chartOptions['chart'] = [
            'backgroundColor' => 'transparent',
            'height' => 700,
            'type' => 'solidgauge',
            'width' => 2000,
        ];

        $chartOptions['credits'] = [
            'enabled' => false,
        ];

        $chartOptions['legend'] = [
            'enabled' => false,
        ];

        $chartOptions['pane'] = [
            'background' => [
                'backgroundColor' => '#FFFFFF',
                'borderWidth' => 2,
                'innerRadius' => '75%',
                'outerRadius' => '100%',
                'shape' => 'arc',
            ],
            'endAngle' => 90,
            'size' => '100%',
            'startAngle' => -90,
        ];

        $chartOptions['plotOptions'] = [
            'solidgauge' => [
                'dataLabels' => [
                    'allowOverlap' => true,
                    'borderWidth' => 0,
                    'fontWeight' => 'normal',
                    'useHTML' => true,
                    'y' => -45,
                ],
                'innerRadius' => '75%',
            ],
        ];

        $chartOptions['series'] = [
            [
                'data' => [
                    (float) $score,
                ],
                'dataLabels' => [
                    'format' => $ratingText,
                    'style' => [
                        'fontSize' => '50px',
                    ],
                ],
            ]
        ];

        $chartOptions['title'] = [
            'text' => self::STR_EMPTY,
        ];

        $chartOptions['tooltip'] = [
            'enabled' => false,
        ];

        $chartOptions['yAxis'] = [
            'gridLineColor' => 'transparent',
            'gridLineWidth' => 0,
            'labels' => [
                'enabled' => false,
            ],
            'lineWidth' => 0,
            'max' => 200,
            'min' => 0,
            'minorTickInterval' => null,
            'stops' => [
                [0.1, '#FCD5C3'],
                [0.5, '#F1C40F'],
                [0.9, '#2ECC71'],
            ],
            'tickPixelInterval' => 400,
            'tickWidth' => 0,
            'title' => [
                'enabled' => false,
            ],
        ];

        return $chartOptions;
    }

    public function createFCondition($entityId)
	{   
		$scoreDetails = $this->getSMEScore($entityId);
		$ratingText = $this->getRatingText($scoreDetails->score_facs);
		        
	    $score = ($scoreDetails->score_facs/180)*100;
	    $maxScore = 100;
	    $time = time();
	    $imagePath = '1-'.$entityId.'-'.$time.'.jpg';

	    // Create Image From Existing File
	    $im = imagecreatetruecolor(550, 340);// Create a 550x340 image

	    $backgroundColor = imagecolorallocatealpha($im, 255, 255, 255, 127);//blending colors
	    imagefill($im, 0, 0, $backgroundColor);//creating white background
	    // Switch antialiasing on for one image
	    $white = imagecolorallocate($im, 255,255,255);
	    $black = imagecolorallocate($im, 0, 0, 0);
	    // for making transparent bg of white bg
	    imagecolortransparent($im, $white);

	    $score1 = $maxScore*0.1;
	    $score2 = $maxScore*0.5;
	    $score3 = $maxScore*0.9;

	    $color = imagecolorallocate($im, $ratingText['R'], $ratingText['G'], $ratingText['B']);

	    imagesetthickness($im, 4);

	    //creation of thick arc
	    $thickness = 40;
	    for ($i = $thickness; $i > 0; $i--) {
	        //imagearc($im, 275, 273, (180 - $i)*3, (180 - $i)*3,  180, 0, $dark_blue);
	        imagearc($im, 275, 273, (180 - $i)*3, (180 - $i)*3, 180, 180+($score*1.8), $color);
	    }
		 
	    // Set Path to Font File
	    //$font_path = public_path('fonts\arial.ttf');
	    $font_path = __DIR__.'/arial.ttf';
	   
	    imagesetthickness($im, 1);

	    //$textWidth = imagettfbbox(40,0, $font_path,$text);
	    $text = $ratingText['rating'];
	    //$text = 'AAA';
	    $textcount = strlen($text);

	    if($textcount == 1)
	    	imagettftext($im,45,0,240,240,$black,$font_path,$text);
	    elseif($textcount == 2)
	    	imagettftext($im,45,0,220,240,$black,$font_path,$text);

	    elseif($textcount == 3)
	    	imagettftext($im,45,0,210,240,$black,$font_path,$text);
	    
	    imagesavealpha($im, true);
	    // Send Image to Browser
	    
	    $pathDirImage = public_path('images/rating-report-graphs/'.$imagePath); 
	    imagejpeg($im,$pathDirImage);
	    imagedestroy($im);
	    return $imagePath;
	}
}
