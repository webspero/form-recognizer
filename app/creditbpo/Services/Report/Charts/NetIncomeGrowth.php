<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class NetIncomeGrowth extends Chart {
    const CHART_LABEL = ' Net Income<br> Growth %';
    const CHART_TYPE = 5;

    /**
     * @param int $entityId
     * @param int $boost
     * @return string
     */
    public function createLineChart($entityId, $companyVal, $industryVal) {
        // $industry = $this->getIndustryMain($entityId);
        // $score = $this->getReadyRatio($entityId);

        // if ($score->net_income_growth1 !== 0) {
        //     $nig = (($score->net_income_growth2 / $score->net_income_growth1) - 1) * 100;
        // } else {
        //     $nig = $score->net_income_growth2;
        // }

        // $boost = $boost ? $boost : $score->net_income_growth;
        $chartOptions = $this->getIndustryComparisonLineChartOptions($companyVal, $industryVal);

        return $this->generateEntityChart($chartOptions, $entityId);
    }
}
