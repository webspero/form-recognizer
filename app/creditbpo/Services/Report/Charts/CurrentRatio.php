<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class CurrentRatio extends Chart {
    const CHART_LABEL = ' Current Ratio';
    const CHART_TYPE = 7;

    /**
     * @param int $entityId
     * @param int $boost
     * @return string
     */
    public function createLineChart($entityId, $companyVal, $industryVal) {
        // $industry = $this->getIndustryMain($entityId);
        // $score = $this->getReadyRatio($entityId);
        // $cr = $score->current_ratio2;
        // $boost = $boost ? $boost : $industry->current_ratio / 100;   
        // print_r($company); die();
        $chartOptions = $this->getIndustryComparisonLineChartOptions($companyVal, $industryVal);

        return $this->generateEntityChart($chartOptions, $entityId);
    }
}
