<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class DebtToEquity extends Chart {
    const CHART_LABEL = ' Debt-to-Equity<br />Ratio';
    const CHART_TYPE = 9;

    /**
     * @param int $entityId
     * @param int $boost
     * @return string
     */
    public function createLineChart($entityId, $companyVal, $industryVal) {
        // $industry = $this->getIndustryMain($entityId);
        // $score = $this->getReadyRatio($entityId);
        // $dte = $score->debt_equity_ratio2;
        // $boost = $boost ? $boost : $industry->debt_equity_ratio / 100;
        $chartOptions = $this->getIndustryComparisonLineChartOptions($companyVal, $industryVal);

        return $this->generateEntityChart($chartOptions, $entityId);
    }
}
