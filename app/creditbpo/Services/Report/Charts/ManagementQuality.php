<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class ManagementQuality extends Chart {
    const CHART_TYPE = 2;
    const MQ_TOTAL = 150;
    const NAME_FORMAT = '%s <span style="color: %s";>%s</span>';

    /**
     * @param int $entityId
     * @return string
     */
    public function createPieChart($entityId) {
        $score = $this->getSMEScore($entityId);
        $mqTotal = self::MQ_TOTAL;

        $boeName = trans('risk_rating.mq_business_owner');
        $mteName = trans('risk_rating.mq_management_team');
        $bdmsName = trans('risk_rating.mq_business_driver');
        $spName = trans('risk_rating.mq_succession_plan');
        $ppfiName = trans('risk_rating.mq_past_project');

        $mqScore = $score->score_boe +
            $score->score_mte +
            $score->score_bdms +
            $score->score_sp +
            $score->score_ppfi;
        $mqEmpty = (float) $mqTotal - (float) $mqScore;

        $boeScore = $this->getBusinessOwnerDisplay($score->score_boe);
        $mteScore = $this->getManagementTeamDisplay($score->score_mte);
        $bdmsScore = $this->getBusinessDriverDisplay($score->score_bdms);
        $spScore = $this->getSuccessionPlanDisplay($score->score_sp);
        $ppfiScore = $this->getPastProjectDisplay($score->score_ppfi);

        $series = [
            [
                'color' => '#FBD5C4',
                'name' => $this->getSeriesName($boeName, $boeScore),
                'y' => (float) $score->score_boe,
            ],
            [
                'color' => '#E4D2E3',
                'name' => $this->getSeriesName($mteName, $mteScore),
                'y' => (float) $score->score_mte,
            ],
            [
                'color' => '#CFEBEC',
                'name' => $this->getSeriesName($bdmsName, $bdmsScore),
                'y' => (float) $score->score_bdms,
            ],
            [
                'color' => '#BDCF8E',
                'name' => $this->getSeriesName($spName, $spScore),
                'y' => (float) $score->score_sp,
            ],
            [
                'color' => '#B4BDC3',
                'name' => $this->getSeriesName($ppfiName, $ppfiScore),
                'y' => (float) $score->score_ppfi,
            ],
            [
                'color' => '#FFF',
                'name' => self::STR_EMPTY,
                'y' => (float) $mqEmpty,
            ],
        ];

        $chartOptions = $this->getBccMqPieOptions($series);

        return $this->generateEntityChart($chartOptions, $entityId);
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getBusinessOwnerDisplay($score) {
        switch ($score) {
            case 21:
                return [
                    'rating_color' => 'green',
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 14:
            case 7:
            default:
                return [
                    'rating_color' => 'red',
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getManagementTeamDisplay($score) {
        switch ($score) {
            case 21:
                return [
                    'rating_color' => 'green',
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 14:
            case 7:
            default:
                return [
                    'rating_color' => 'red',
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getBusinessDriverDisplay($score) {
        switch ($score) {
            case 36:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.bd_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 12:
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.bd_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getSuccessionPlanDisplay($score) {
        switch ($score) {
            case 36:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.sp_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 24:
                return [
                    'rating_color' => 'orange',
                    'rating_desc' => trans('risk_rating.sp_moderate'),
                    'rating_txt' => trans('risk_rating.moderate'),
                ];
            case 12:
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.sp_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getPastProjectDisplay($score) {
        switch ($score) {
            case 36:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.pp_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 12:
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.pp_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param string $name
     * @param string $property
     * @return string
     */
    protected function getSeriesName($name, $property) {
        return sprintf(
            self::NAME_FORMAT,
            $name,
            $property['rating_color'],
            $property['rating_txt']
        );
    }
}
