<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class GrossProfitMargin extends Chart {
    const CHART_LABEL = ' Gross Profit<br> Margin %';
    const CHART_TYPE = 6;

    /**
     * @param int $entityId
     * @param int $boost
     * @return string
     */
    public function createLineChart($entityId, $companyVal, $industryVal) {
        // $industry = $this->getIndustryMain($entityId);
        // $score = $this->getReadyRatio($entityId);
        // $gpm = (float) $score->gross_profit_margin2 * 100;
        // $boost = $boost ? $boost : $industry->gross_profit_margin;
        $chartOptions = $this->getIndustryComparisonLineChartOptions($companyVal, $industryVal);

        return $this->generateEntityChart($chartOptions, $entityId);
    }
}
