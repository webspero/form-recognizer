<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class BusinessCondition extends Chart {
    const BCC_TOTAL = 240;
    const CHART_TYPE = 1;
    const NAME_FORMAT = '%s <span style="color: %s";>%s</span>';

    /**
     * @param int $entityId
     * @return string
     */
    public function createPieChart($entityId) {
        $score = $this->getSMEScore($entityId);
        $bccTotal = self::BCC_TOTAL;

        $rmLabel = trans('risk_rating.risk_management_chart');
        $cdLabel = trans('risk_rating.customer_dependency_chart');
        $sdLabel = trans('risk_rating.supplier_dependency_chart');
        $boiLabel = trans('risk_rating.business_outlook_chart');

        $bccScore = $score->score_rm + $score->score_cd + $score->score_sd +$score->score_bois;
        $bccEmpty = $bccTotal - $bccScore;

        $boeScore = $this->getRiskManagementData($score->score_rm);
        $cdScore = $this->getCustomerDependencyData($score->score_cd);
        $sdScore = $this->getSupplierDependencyData($score->score_sd);
        $boiScore = $this->getBusinessOutlookData($score->score_bois);

        $series = [
            [
                'color' => '#FBD5C4',
                'name' => $this->getSeriesName($rmLabel, $boeScore),
                'y' => (float) $score->score_rm,
            ],
            [
                'color' => '#E4D2E3',
                'name' => $this->getSeriesName($cdLabel, $cdScore),
                'y' => (float) $score->score_cd,
            ],
            [
                'color' => '#CFEBEC',
                'name' => $this->getSeriesName($sdLabel, $sdScore),
                'y' => (float) $score->score_sd,
            ],
            [
                'color' => '#BDCF8E',
                'name' => $this->getSeriesName($boiLabel, $boiScore),
                'y' => (float) $score->score_bois,
            ],
            [
                'color' => '#FFF',
                'name' => self::STR_EMPTY,
                'y' => (float) $bccEmpty,
            ],
        ];

        $chartOptions = $this->getBccMqPieOptions($series);

        return $this->generateEntityChart($chartOptions, $entityId);
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getRiskManagementData($score) {
        switch ($score) {
            case 90:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.rm_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.rm_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getCustomerDependencyData($score) {
        switch ($score) {
            case 30:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.cd_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 20:
                return [
                    'rating_color' => 'orange',
                    'rating_desc' => trans('risk_rating.cd_moderate'),
                    'rating_txt' => trans('risk_rating.moderate'),
                ];
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.cd_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getSupplierDependencyData($score) {
        switch ($score) {
            case 30:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.sd_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 20:
                return [
                    'rating_color' => 'orange',
                    'rating_desc' => trans('risk_rating.sd_moderate'),
                    'rating_txt' => trans('risk_rating.moderate'),
                ];
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.sd_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param float $score
     * @return array
     */
    protected function getBusinessOutlookData($score) {
        switch ($score) {
            case 90:
                return [
                    'rating_color' => 'green',
                    'rating_desc' => trans('risk_rating.bo_high'),
                    'rating_txt' => trans('risk_rating.high'),
                ];
            case 60:
                return [
                    'rating_color' => 'orange',
                    'rating_desc' => trans('risk_rating.bo_moderate'),
                    'rating_txt' => trans('risk_rating.moderate'),
                ];
            default:
                return [
                    'rating_color' => 'red',
                    'rating_desc' => trans('risk_rating.bo_low'),
                    'rating_txt' => trans('risk_rating.low'),
                ];
        }
    }

    /**
     * @param string $name
     * @param string $property
     * @return string
     */
    protected function getSeriesName($name, $property) {
        return sprintf(
            self::NAME_FORMAT,
            $name,
            $property['rating_color'],
            $property['rating_txt']
        );
    }
}
