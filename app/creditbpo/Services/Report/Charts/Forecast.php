<?php

namespace CreditBPO\Services\Report\Charts;
use CreditBPO\Services\Report\PCharts\GrowthForecast;

use CreditBPO\Services\Report\Charts\Chart;

class Forecast extends Chart {
    const CHART_TYPE = 8;

    /**
     * @param int $entityId
     * @return string
     */
    public function createLineChart($entityId) {
        $forecastData = $this->getForecastData($entityId);
        $financialReport = $this->getFinancialReport($entityId);

        $growthForecast = new GrowthForecast;
        $file = $growthForecast->generate($entityId);
        
        unset($forecastData['industry'][13]);
        unset($forecastData['industry'][14]);
        unset($forecastData['industry'][15]);
        $series = [
            [
                'name' => 'Industry',
                'color' => "#aaaaaa",
                'data' => isset($forecastData['industry']) ? $forecastData['industry'] : [],
                'connectNulls' => true
            ],
            [
                'name' => 'Company',
                'color' => "#B1D46B",
                'data' => isset($forecastData['company']) ? $forecastData['company'] : [],
                'connectNulls' => true
            ]
            
        ];

        // $chartOptions = $this->getForecastLineOptions($series, $financialReport->year);

        return [
            'file' => $file,
            'forecastData' => $forecastData,
        ];
    }

    /**
     * @param array $series
     * @param int $year
     * @return array
     */
    protected function getForecastLineOptions($series, $year) {
        $chartOptions = [];

        $chartOptions['chart'] = [
            'height' => '250',
            'type' => 'line',
        ];

        $chartOptions['credits'] = [
            'enabled' => false,
        ];

        $chartOptions['legend'] = [
            'align' => 'right',
            'borderWidth' => 1,
            'floating' => true,
            'layout' => 'vertical',
            'verticalAlign' => 'top',
            'x' => -420,
            'y' => 30,
        ];

        $chartOptions['plotOptions'] = [
            'line' => [
                'dataLabels' => [
                    'align' => 'center',
                    'allowOverlap' => true,
                    'color' => 'black',
                    'enabled' => true,
                    'format' => '{y:.2f}%',
                    'style' => [
                        'fontWeight' => 'normal',
                        'fontSize' => '10px',
                    ],
                ],
                'connectNulls' => true
            ],
            'series' => [
            	'connectNulls' => true
            ]
        ];

        $chartOptions['series'] = $series;

        $chartOptions['title'] = [
            'text' => static::STR_EMPTY,
        ];

        $chartOptions['tooltip'] = [
            'enabled' => false,
        ];

        $options = [];

        for($x=1;$x<=4;$x++){

        	for($i=1;$i<=4;$i++){

	        	$options[] = 'Q'.$i.' '.$year;

	        	if($x == 4 && $i > 1) continue;
	        }
	        $year++;
	       
    	}

    	//print_r($options); exit;
        $chartOptions['xAxis'] = [
            'categories' => $options,
            'title' => [
                'text' => null,
            ],
        ];

        $chartOptions['yAxis'] = [
            'gridLineWidth' => 2,
            'title' => [
                'text' => null,
            ],
        ];

        return $chartOptions;
    }
}
