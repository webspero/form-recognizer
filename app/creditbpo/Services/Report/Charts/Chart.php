<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Libraries\HighCharts;
use CreditBPO\Models\Entity;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\ReadyRatio;
use CreditBPO\Models\SMEScore;
use CreditBPO\Models\Bank;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use GrowthForecastLib;
use CreditBPO\Handlers\SMEInternalHandler;
use DB;

abstract class Chart {
    const STR_EMPTY = '';

    protected $chartLabels = [];

    /**
     * @param array $labels
     * @return Chart
     */
    public function setChartLabels($labels = []) {
        $this->chartLabels = $labels;
        return $this;
    }

    /**
     * @return array
     */
    public function getChartLabels() {
        return $this->chartLabels;
    }

    /**
     * @param array $chartOptions
     * @param int $entityId
     * @return string
     */
    public function generateEntityChart($chartOptions, $entityId) {
        $chart = new HighCharts();
        return $chart->generateChart(
            $entityId,
            self::getChartType(),
            $chartOptions
        );
    }

    /**
     * @return string
     */
    public function getChartLabel() {
        return static::CHART_LABEL;
    }

    /**
     * @return string
     */
    public function getChartType() {
        return static::CHART_TYPE;
    }

    /**
     * @param int $entityId
     * @return Eloquent
     */
    protected function getFinancialReport($entityId) {
        return FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @param int $entityId
     * @return array
     */
    protected function getForecastData($entityId) {
        $entity = Entity::where('entityid', $entityId)->first();
        //print_r($entity); exit;
        $industryMainRow = DB::table('tblindustry_main')->where('main_code',$entity->industry_main_id)->first();

        if(empty($industryMainRow)){
			$industryMainRow = DB::table('tblindustry_main')->where('industry_main_id',$entity->industry_main_id)->first();        	
        }
        $industryMain = $industryMainRow->industry_main_id;

        if ($entity->current_bank != 0) {
            $bankStandalone = Bank::find($entity->current_bank)->is_standalone;
        } else {
            $bankStandalone = 0;
        }

        /**
         * Forecase slope calculation, GrowthForecast Lib needs to be refactored
         * Place it on CreditBPO\Services\GrowthForecast
         */
        $growthForecast = new GrowthForecastLib($entityId, $industryMain, $bankStandalone);

        //print_r($growthForecast->getGrowthForecastData()); exit;
        
        return $growthForecast->getGrowthForecastData();
    }

    /**
     * @param int $entityId
     * @return Eloquent
     */
    protected function getIndustryMain($entityId) {
        return Entity::leftJoin(
                'tblindustry_main',
                'tblentity.industry_main_id',
                '=',
                'tblindustry_main.main_code'
            )
            ->where('tblentity.entityid', $entityId)
            ->first();
    }

    /**
     * @param int $entityId
     * @return Eloquent
     */
    protected function getReadyRatio($entityId) {
        
        $readyratio = ReadyRatio::where('entityid', $entityId)
            ->orderBy('readyrationdataid', 'desc')
            ->first();

        $financialReport = $this->getFinancialReport($entityId);
        if(empty($readyratio)){

            $readyratio = SMEInternalHandler::getOverwriteReadyRatio($entityId);

        }

        return $readyratio;
    }

    /**
     * @param int $entityId
     * @return Eloquent
     */
    protected function getSMEScore($entityId) {
        return SMEScore::where('entityid', $entityId)
            ->orderBy('smescoreid', 'desc')
            ->first();
    }

    /**
     * @param float $score
     * @param string $boost
     * @return array
     */
    public function getIndustryComparisonLineChartOptions($score, $boost) {
        $chartOptions = [];

        $chartOptions['chart'] = [
            'height' => '200',
            'width' => '1200',
            'type' => 'bar',
        ];

        $chartOptions['credits'] = [
            'enabled' => false,
        ];

        $chartOptions['legend'] = [
            'enabled' => false,
        ];

        $chartOptions['plotOptions'] = [
            'bar' => [
                'dataLabels' => [
                    'align' => 'center',
                    'allowOverlap' => true,
                    'enabled' => true,
                    'format' => '{y:.2f}',
                    'inside' => true,
                    'style' => [
                        'fontSize' => '20px',
                    ],
                ],
                'pointPadding' => 0,
                'pointWidth' => 20,
            ],
            'series' => [
            	'connectNulls' => true
            ]
        ];

        $chartOptions['series'] = [
            [
                'color' => '#BDCF8E',
                'data' => [(float) $score],
                'name' => 'Company'
            ],
            [
                'color' => '#F9967C',
                'data' => [(float) $boost],
                'name' => 'Industry'
            ]
        ];

        $chartOptions['title'] = [
            'text' => self::STR_EMPTY,
        ];

        $chartOptions['tooltip'] = [
            'enabled' => false,
        ];

        $chartOptions['xAxis'] = [
            'categories' => " ",
            'labels' => [
                'useHTML' => true,
                'style' => [
                    'font-size' => '25px',
                ],
            ],
            'title' => [
                'text' => null,
            ],
        ];

        $chartOptions['yAxis'] = [
            'gridLineWidth' => 2,
            'labels' => [
                'style' => [
                    'font-size' => '25px',
                ],
            ],
            'tickPixelInterval' => 150,
            'title' => [
                'text' => null,
            ],
        ];

        return $chartOptions;
    }

    /**
     * @param array $series
     * @return array
     */
    protected function getBccMqPieOptions($series) {
        $chartOptions = [];

        $chartOptions['chart'] = [
            'height' => '600',
            'plotBackgroundColor' => null,
            'plotBorderWidth' => null,
            'plotShadow' => false,
            'type' => 'pie',
            'width' => '1200',
        ];

        $chartOptions['credits'] = [
            'enabled' => false,
        ];

        $chartOptions['legend'] = [
            'align' => 'right',
            'enabled' => true,
            'itemStyle' => [
                'fontSize' => '25px'
            ],
            'layout' => 'vertical',
            'useHTML' => true,
            'verticalAlign' => 'middle',
            'width' => 370,
            'x' => 0,
            'y' => 0,
        ];

        $chartOptions['plotOptions'] = [
            'pie' => [
                'borderWidth' => 0,
                'dataLabels' => [
                    'allowOverlap' => true,
                    'enabled'   => false,
                    'fontWeight' => 'normal',
                    'useHTML'   => true,
                ],
                'innerSize' => '60%',
            ],
            'series' => [
            	'connectNulls' => true
            ]
        ];

        $chartOptions['series'] = [
            [
                'data' => $series,
                'dataLabels' => [
                    'enabled'   => false,
                    'useHTML'   => true,
                ],
                'innerSize' => '54%',
                'showInLegend' => true,
                'size' => '100%',
                'connectNulls' => true
            ],
        ];

        $chartOptions['title'] = [
            'text' => self::STR_EMPTY,
        ];

        $chartOptions['tooltip'] = [
            'enabled' => false,
        ];

        return $chartOptions;
    }
}
