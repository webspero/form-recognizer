<?php

namespace CreditBPO\Services\Report\Charts;

use CreditBPO\Services\Report\Charts\Chart;

class GrossRevenueGrowth extends Chart {
    const CHART_LABEL = ' Gross Revenue<br/>Growth %';
    const CHART_TYPE = 4;

    /**
     * @param int $entityId
     * @param int $boost
     * @return string
     */
    public function createLineChart($entityId, $companyVal, $industryVal) {
        // $score = $this->getReadyRatio($entityId);

        // if ($score->gross_revenue_growth1 !== 0) {
        //     $grg = (($score->gross_revenue_growth2 / $score->gross_revenue_growth1) - 1) * 100;
        // } else {
        //     $grg = 0;
        // }

        // $boost = $boost ? $boost : 0;
        $chartOptions = $this->getIndustryComparisonLineChartOptions($companyVal, $industryVal);

        return $this->generateEntityChart($chartOptions, $entityId);
    }
}
