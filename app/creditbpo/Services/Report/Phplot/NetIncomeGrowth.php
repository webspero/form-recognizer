<?php

namespace CreditBPO\Services\Report\Phplot;

use App\libraries\PHPlot;

class NetIncomeGrowth{
    const CHART_TITLE_EN = 'Net Income Growth';
    const CHART_TITLE_TL = 'Net Income Growth';
    const FILENAME_FORMAT = '6-%d';
    const CHART_TYPE = 7;
    const SCALE_POS_TOPBOTTOM =  690102;
    const LABEL_POS_INSIDE = 690606;
    const LEGEND_NOBORDER = 690800;
    const LEGEND_HORIZONTAL = 690902;
    const DEFAULT_FONT = 'tahoma.ttf';
    const DIRECTION_HORIZONTAL = 690002;
    const DIRECTION_VERTICAL = 690001;
    

    public function __construct(){
        $this->filePath = public_path('images').'/';
        $this->fontDirectory = public_path('fonts').'/';
    }
    /**
     * @param int $id
     * @return string
     */
    public function generate($entityId, $companyVal, $industryVal) {
        
        $time = strval(time()); 
        
        $font = $this->fontDirectory.Self::DEFAULT_FONT;
        $filename = sprintf(Self::FILENAME_FORMAT, $entityId).'-'.$time.'.png';
        $filepath = $this->filePath.'rating-report-graphs/'.$filename;
        
        $chart_title = Self::CHART_TITLE_EN;
        if(session('fa_report_lang') == 'tl')
            $chart_title = Self::CHART_TITLE_TL;
    	
    	$industryVal = str_replace(',','',$industryVal);
    	$companyVal = str_replace(',','',$companyVal);
        
        $data = array(
            array(' ',$industryVal),
            array(' ',$companyVal)
		);

		$plot = new PHPlot(730,100);
		//$plot->SetTitle($chart_title);
		$plot->SetBackgroundColor('lavender');
		#  Force the X axis range to start at 0:
		// $plot->SetPlotAreaWorld(0);
		#  No ticks along Y axis, just bar labels:
		$plot->SetYTickPos('none');
		#  No ticks along X axis:
		$plot->SetXTickPos('plotdown');
		// #  No X axis labels. The data values labels are sufficient.
		// $plot->SetXTickLabelPos('none');
        #  Turn on the data value labels:
        $plot->SetXDataLabelPos('plotin');
        $plot->SetXTickLabelPos('plotdown');
        //Set font style
        $plot->SetFont('x_label', 3,30, 1);
		//$plot->SetXDataLabelPos('plotin');
		#  No grid lines are needed:
		$plot->SetDrawXGrid(true);
		$plot->SetCallback('data_color', 'Self::pickcolor', $data);
		#  Set the bar fill color:
        $plot->SetDataColors(['#aaaaaa','YellowGreen']);
		#  Use less 3D shading on the bars:
		$plot->SetShading(5);
		$plot->SetDataValues($data);
		$plot->SetDataType('text-data-yx');
        $plot->SetPlotType('bars');
        $plot->SetIsInline(true);
        $plot->SetOutputFile($filepath);
        $plot->DrawGraph();
        
        return $filename;

    }
}