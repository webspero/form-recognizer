<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Models\Entity;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\PDF;
use CreditBPO\Services\Report\PCharts\AssetStructure;

//======================================================================
//  Class: SAL
//  Description: Contains functions for creating SAL PDF and it's getting
//    it's path
//======================================================================
class FinancialReport extends PDF {
    const TEMPLATE = 'financial.SAL';
    const FILENAME = 'Structure of the Assets and Liabilities - %s as of %s.pdf';

    public function __construct() {
        $this->setFilePath(public_path('financial_analysis').'/');
    }

    //======================================================================
    //  Function: create
    //  Description: Create SAL PDF File
    //======================================================================
    public function create($id) {
        set_time_limit(0);

        $financialReport = FinancialReport::find($id);

        $assetStructure = new AssetStructure();

        $pcharts = [
            'assetStructure' => $assetStructure->generate($id)
        ];

        $this->setData([
            'financial_report' => $financialReport,
            'balance_sheets' => $financialReport->balanceSheets()
                ->where('Assets', '!=', 0)
                ->where('Liabilities', '!=', 0)
                ->orderBy('index_count', 'asc')->get(),
            'income_statements' => $financialReport->incomeStatements()
                ->where('Revenue', '!=', 0)
                ->orderBy('index_count', 'asc')->get(),
            'pcharts' => $pcharts,
        ]);

        $filename = sprintf(
            'Structure of the Assets and Liabilities %u %s',
            $financialReport->title,
            date('Y-m-d'));

        $fname = $filename.'.pdf';
        if(strpos($fname,"&") !== false) {
           $fname = preg_replace("/[&]/", "And", $fname);
        }
        
        $this->setFilename($fname);
        $this->setPCharts($pcharts);

        return $this;
    }

    //======================================================================
    //  Function: getTemplate
    //  Description: Get PDF Template
    //======================================================================
    public function getTemplate() {
        return self::TEMPLATE;
    }

    //======================================================================
    //  Function: getFilenamePath
    //  Description: Get PDF filename path
    //======================================================================
    public function getFilenamePath($frId) {
        
        $financialReport = FinancialReport::find($frId);

        if (@count($financialReport) <= 0) {
            return null;
        }

        $filename = sprintf(
            'Structure of the Assets and Liabilities %u %s',
            $financialReport->title,
            date('Y-m-d'));
        
        $fname = $filename.'.pdf';
        if(strpos($fname,"&") !== false) {
           $fname = preg_replace("/[&]/", "And", $fname);
        }
        
        return $this->getFilePath().$fname;
    }

}
