<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Services\Report\PDF\PDF;
use CreditBPO\Libraries\Calculation;
use CreditBPO\Models\Bank;
use CreditBPO\Models\Entity;
use CreditBPO\Models\GRDP;
use CreditBPO\Services\FinancialRating;
use CreditBPO\Services\IndustryComparison;
use CreditBPO\Services\SME as SMEHandler;
use CreditBPO\Services\Report\Charts\BusinessCondition;
use CreditBPO\Services\Report\Phplot\CurrentRatio;
use CreditBPO\Services\Report\Phplot\DebtToEquity;
use CreditBPO\Services\Report\Charts\FinancialCondition;
use CreditBPO\Services\Report\Charts\Forecast;
use CreditBPO\Services\Report\Phplot\GrossProfitMargin;
use CreditBPO\Services\Report\Phplot\GrossRevenueGrowth;
use CreditBPO\Services\Report\Charts\ManagementQuality;
use CreditBPO\Services\Report\Phplot\NetIncomeGrowth;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use CreditBPO\Handlers\SMEInternalHandler;
use DateTime;
use CreditBPO\Models\ReadyratioKeySummary;
use Relatedcompanies;
use Director;
use KeyManager;
use Shareholder;
use Majorcustomer;
use Majorsupplier;
use Capital;
use Certifications;
use Insurance;
use Businesssegment;
use DB;
use Majorlocation;
use Competitor;
use Region;
use PseCompanyOfficer;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use \Exception as Exception;
use CreditBPO\Services\MailHandler;
use App;

class SME extends PDF {
    const FILENAME = 'CreditBPO Rating Report - (%s) %s %s.pdf';
    const TEMPLATE = 'summary.print-rating-summary-internal';
    const BANK_TYPE_GOVERNMENT = 'Government';
    const BANK_TYPE_CORPORATE = 'Corporate';

    /**
     * @param int $entityId
     * @return SME
     */
    public function create($entityId) {
        
        try{
            set_time_limit(0);
            //echo __DIR__; exit;
            //echo public_path('images'); exit;
            // $font_path = public_path('fonts\arial.ttf');
            // echo $font_path; exit;
            $forecastData = [];
            $graphs = [];

            $handler = new SMEHandler();
            $entityData = $handler->getReportEntityData($entityId);
            $bankStandalone = $entityData['bankStandalone'];
            $entity = $entityData['entity'];
            $financialReport = $entityData['financialReport'];
            $industryMain = $entityData['industryMain'];
            $readyRatio = $entityData['readyRatio'];
            $region = $entityData['region'];
            $smeScore = $entityData['smeScore'];


            if(empty($entityData['industryMain'])) $industryMain = (object) [];

            $financialReport = FinancialReport::find($financialReport->id);

            $balance_sheets = $financialReport->balanceSheets()->get();
            $income_statements = $financialReport->IncomeStatements()->get();
            $cash_flows = $financialReport->cashFlow()->get();
            
            $cf_net_profit_margin = 0;
            if($income_statements[0]->Revenue != 0){
                $cf_net_profit_margin = $income_statements[0]->ProfitLossFromOperatingActivities/$income_statements[0]->Revenue;
            }

            $cf_net_profit_margin_percentage = number_format($cf_net_profit_margin*100,2,".","");
            
            $sumAssets = 0;
            foreach($balance_sheets as $bs){
                $sumAssets += floatval($bs->Assets);
            }

            $aveAssets = $sumAssets/3;
            
            $sumEquity = 0;
            foreach($balance_sheets as $bs){
                $sumEquity += floatval($bs->Equity);
            }

            $aveEquity = $sumEquity/3;
            
            $cf_asset_turnover = 0;
            $cf_asset_turnover_percentage = 0;

            $fa_report = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])->orderBy('id', 'desc')->first();
            $fa_report->balance_sheets = $fa_report->balanceSheets()->orderBy('index_count', 'asc')->get();
            $fa_report->income_statements = $fa_report->incomeStatements()->orderBy('index_count', 'asc')->get();
            $fa_report->cash_flows = $fa_report->cashFlow()->orderBy('index_count', 'asc')->get();

            if(!empty($aveAssets)){
                $cf_asset_turnover = $fa_report->income_statements[0]->Revenue/$aveAssets;
                $cf_asset_turnover_percentage = number_format($cf_asset_turnover*100,2,".",""); 
            }

            $cf_equity_multiplier = 0;
            $cf_equity_multiplier_percentage = 0;
            if(!empty($aveEquity)){
                $cf_equity_multiplier = $aveAssets/$aveEquity;
                $cf_equity_multiplier_percentage = number_format($cf_equity_multiplier*100,2,".","");   
            }

            $cf_dupont_analysis = $cf_net_profit_margin*$cf_asset_turnover*$cf_asset_turnover*$cf_equity_multiplier;

            $cf_dupont_analysis_percentage = number_format($cf_dupont_analysis*100,2,".","");

            //Rating Summary calculations
            if(empty($readyRatio)){

                $readyRatio = SMEInternalHandler::getOverwriteReadyRatio($entityId);
            }

            $financial = [
                'fin_position' => FinancialRating::getPositionScore($smeScore->score_rfp),
                'fin_performance' => FinancialRating::getPositionScore($smeScore->score_rfpm),
            ];

            $grossProfitMargin = $readyRatio->gross_profit_margin2 * 100;

            // if(empty($industryMain->current_ratio)) $industryMain->current_ratio = 0;
            // $currentRatio = $industryMain->current_ratio / 100;
            
            // if(empty($industryMain->debt_equity_ratio)) $industryMain->debt_equity_ratio = 0;
            // $debtToEquityRatio = $industryMain->debt_equity_ratio / 100;

            // $grossRevenueGrowth = $readyRatio->gross_revenue_growth2 - $readyRatio->gross_revenue_growth1;

            // $netIncomeGrowth = $readyRatio->net_income_growth2 - $readyRatio->net_income_growth1;

            $creditExp = explode('-', $smeScore->score_sf);
            $cbpoRating = FinancialRating::getRating($creditExp[0]);
            $cbpoRate = [
                'rating_txt' => $cbpoRating['score'],
                'rating_class' => $cbpoRating['class'],
                'rating_color' => $cbpoRating['color'],
                'rating_desc' => $cbpoRating['description'],
                'rating_score' => $creditExp[0],
                'rating_label' => $creditExp[1],
            ];

            $bcc = new BusinessCondition();
            $mq = new ManagementQuality();
            $fpos = new FinancialCondition();

            // $graphs['bcc_graph'] = $bcc->createPieChart($entityId);
            // $graphs['mq_graph'] = $mq->createPieChart($entityId);
            $graphs['bcc_graph'] = '';
            $graphs['mq_graph'] = '';
            $graphs['fpos_graph'] = $fpos->createFCondition($entityId);

            if(empty($industryMain->current_ratio)) $industryMain->current_ratio = 0;
            if(empty($industryMain->gross_revenue_growth)) $industryMain->gross_revenue_growth = 0;
            if(empty($industryMain->net_income_growth)) $industryMain->net_income_growth = 0;
            if(empty($industryMain->gross_profit_margin)) $industryMain->gross_profit_margin = 0;


            $boost = GRDP::getBoostCurrentYear($entityId);
            $industryBoost = [
                'currentRatio' => ($industryMain->current_ratio * $boost['boost']) / 100,
                'debtEquity' => ($industryMain->debt_equity_ratio * $boost['reverse']) / 100,
                'grossProfit' => $industryMain->gross_profit_margin * $boost['boost'],
                'grossRevenue' => $industryMain->gross_revenue_growth * $boost['boost'],
                'netIncome' => $industryMain->net_income_growth * $boost['boost'],
            ];

            $company_industry = $this->getCompanyIndustry($entityId);

            $industryComparison = [
                'grg' => $company_industry['gross_revenue_growth'],
                'nig' => $company_industry['net_income_growth'],
                'gpm' => $company_industry['gross_profit_margin'],
                'cr_ind' => $company_industry['current_ratio'],
                'der_ind' => $company_industry['debt_to_equity'],
            ];

            $industry = new IndustryComparison();
            $industry_comparison = $industry->getIndustryComparison($entityId);

            $boostValues = [
                'grg' => $industry_comparison['gross_revenue_growth'],
                'nig' => $industry_comparison['net_income_growth'],
                'gpm' => $industry_comparison['gross_profit_margin'],
                'cr' => $industry_comparison['current_ratio'],
                'der' => $industry_comparison['debt_equity_ratio'],
            ];

            // echo 'industryComparison:<br/>';
            // print_r($industryComparison);
            // echo '<br/>boostValues:<br/>';
            // print_r($boostValues);
            // exit;

            $charts = $this->getIndustryComparisonGraphs($entityId, $industryComparison, $boostValues);
            $graphs['reg1_graph'] = $charts['grossRevenue'];
            $graphs['reg2_graph'] = $charts['netIncome'];
            $graphs['reg3_graph'] = $charts['grossProfit'];
            $graphs['reg4_graph'] = $charts['currentRatio'];
            $graphs['reg5_graph'] = $charts['debtEquity'];
            
            if ($financialReport) {
                $chart = new Forecast();
                $forecastChart = $chart->createLineChart($entityId);
                $forecastData = $forecastChart['forecastData'];
                $graphs['forecast'] = $forecastChart['file'];
            }

            // foreach ($graphs as $key => $value) {
            // 	$file = public_path('images/rating-report-graphs/'.$value);
            // 	if(file_exists($file)){
            // 		chmod($file, 0777);
            // 	}
            // }
            
            $dateCreated = new DateTime();
            //$dateCreated->modify('+8 hours');
            $dateFormat = $dateCreated->format('Y-m-d');

            $companyname = $entity->companyname;
            if(strpos($companyname,"&") !== false){
                $companyname  = preg_replace("/[&]/", "And", $companyname);
            }

            $filename = sprintf(SELF::FILENAME, $entityId, $companyname, date('Y-m-d'));
            $this->setFilename($filename);

            $keys = ReadyratioKeySummary::where('entityid',$entityId)->orderBy('id', 'desc')->get();

            /** Check KeyRatios Value for old report*/
            if(count($keys) == 0){
                $keyRatio = new KeyRatioService();
                $keyRatio->generateKeyRatioByEntityId($entityId);
                $keySummary = ReadyratioKeySummary::where('entityid',$entityId)->orderBy('id', 'desc')->get();
            }else{
                $keySummary = ReadyratioKeySummary::where('entityid',$entityId)->orderBy('id', 'desc')->get();
            }

            $fa_report = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])->orderBy('id', 'desc')->first();

            /*--------------------------------------------------------------------
            /*  Generates Industry Comparison according to latest year
            /*------------------------------------------------------------------*/
            $year = GRDP::getLatestYear();

            $entity_data = Entity::where('entityid', $entityId)
                ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
                ->first();

            //get city data to get regCode
            $city_data = Entity::where('entityid', $entityId)
                ->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
                ->first();

            if(!empty($city_data->regDesc)){
            //get region to based from reCode on city data
                $region_data = Region::where('regCode', $city_data->regDesc)->first();

                $region         = $region_data->regDesc;
                $regionCode     = $region_data->regCode;
                // $grdp_data      = GRDP::getBoost($year, $regionCode);
                // $reverse_boost  = 1 + (1 - $grdp_data);
                $zipcode        = $entity_data->zipcode;
                $city           = $region_data->city;
                $entityCity     = $entity_data->citymunDesc;
            }

            /*--------------------------------------------------------------------
            /*  Classify asset size
            /*------------------------------------------------------------------*/
            if ($entity->total_assets < 3000000) {
                $assetClassification = "Micro";
            } elseif ($entity->total_assets >= 3000000 && $entity->total_assets <= 15000000){
                $assetClassification = "Small";
            } elseif ($entity->total_assets > 15000000 && $entity->total_assets <= 100000000) {
                $assetClassification = "Medium";
            } else {
                $assetClassification = "Large";
            }

            /*--------------------------------------------------------------------
            /*  Gets the Related Companies Data from the Database
            /*------------------------------------------------------------------*/

            $relatedCompanies = Relatedcompanies::where('is_deleted',0)
                                ->where('entity_id', $entityId)
                                ->get();

            /*--------------------------------------------------------------------
            /*  Gets the board of directors Data from the Database
            /*------------------------------------------------------------------*/
            $boardOfDirectors = Director::where('is_deleted',0)
                                ->where('entity_id', $entityId)
                                ->get();
            foreach($boardOfDirectors as $boardOfDirector) {
                $middleName = $boardOfDirector->middlename ? ' ' . $boardOfDirector->middlename : '';
                $boardOfDirector->name = $boardOfDirector->firstname . $middleName . ' ' . $boardOfDirector->lastname;
            }

            // /*--------------------------------------------------------------------
            // /*  Gets the Key Managers Data from the Database
            // /*------------------------------------------------------------------*/
            $keyManagers = KeyManager::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();

            // /*--------------------------------------------------------------------
            // /*  Gets the shareholders Data from the Database
            // /*------------------------------------------------------------------*/
            $shareHolders = Shareholder::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();
            foreach($shareHolders as $shareHolder) {
                $middleName = $shareHolder->middlename ? ' ' . $shareHolder->middlename : '';
                $shareHolder->name = $shareHolder->firstname . $middleName . ' ' . $shareHolder->lastname;
            }

            /*--------------------------------------------------------------------
            /*  Gets the Major Customers Data from the Database
            /*------------------------------------------------------------------*/
            $majorCustomers = Majorcustomer::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();

            /*--------------------------------------------------------------------
            /*  Gets the Major Suppliers Data from the Database
            /*------------------------------------------------------------------*/
            $majorSuppliers = Majorsupplier::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();

            /*--------------------------------------------------------------------
            /*  Gets the capital details Data from the Database
            /*------------------------------------------------------------------*/
            $capitalDetails = Capital::where('is_deleted', 0)
                            ->where('entity_id', $entityId)
                            ->first();

            /*--------------------------------------------------------------------
            /*  Gets the certifications Data from the Database
            /*------------------------------------------------------------------*/
            $certifications = Certifications::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();
            foreach ($certifications as $certification) {
                $certification->certification_reg_date = date('m/d/Y', strtotime($certification->certification_reg_date));
            }

            /*--------------------------------------------------------------------
            /*  Gets the insurance Data from the Database
            /*------------------------------------------------------------------*/
            $insurances = Insurance::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();

            /*--------------------------------------------------------------------
            /*  Gets the customer segment Data from the Database
            /*------------------------------------------------------------------*/
            $customerSegment = Businesssegment::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();

            
            /*--------------------------------------------------------------------
            /*  Gets the Location Data from the Database
            /*------------------------------------------------------------------*/
            $location = DB::table('tbllocations')
                            ->where('entity_id', $entityId)
                            ->where('is_deleted',0)
                            ->first();

            //print_r($location); exit;

            // $location = Locations::where('is_deleted',0)
            //                 ->where('entity_id', $entityId)
            //                 ->first();

            /*--------------------------------------------------------------------
            /*  Gets the Major Market Location Data from the Database
            /*------------------------------------------------------------------*/
            $majorLocations = Majorlocation::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->where('is_deleted',0)
                            ->leftJoin('refcitymun', 'tblmajorlocation.marketlocation', 'refcitymun.id')
                            ->get();

            /*--------------------------------------------------------------------
            /*  Gets the Competitors Data from the Database
            /*------------------------------------------------------------------*/
            $competitors = Competitor::where('is_deleted',0)
                            ->where('entity_id', $entityId)
                            ->get();
                            
            /*--------------------------------------------------------------------
            /*  Gets the Entity DSCR Details from the Database
            /*------------------------------------------------------------------*/
            $dscrDetails = DB::table('debt_service_capacity_calculator')
                            ->where('entity_id', '=', $entityId)
                            ->first();

            $procurementTypes = [
                Self::BANK_TYPE_CORPORATE,
                Self::BANK_TYPE_GOVERNMENT,
            ];

            $isFinancing = !in_array($entity->bank_type, $procurementTypes);

            /*--------------------------------------------------------------------
            /*  Gets the Entity DSCR Details from the Database
            /*------------------------------------------------------------------*/
            $dscrDetails = DB::table('debt_service_capacity_calculator')
                            ->where('entity_id', '=', $entity->entityid)
                            ->first();

            switch($entity->employee_size) {
                case 0:
                    $employeeSize = "Micro 1-9";
                    break;
                case 1:
                    $employeeSize = "Small 10-99";
                    break;
                case 2:
                    $employeeSize = "Medium 100-199";
                    break;
                case 3:
                    $employeeSize = "Large 200+";
                    break;
                    default:
                        $employeeSize = "";
                        break;

            }

            /* Disable CMAP Function for Simplified and Standalone Report (CDP-1604) */
            if($entity->is_premium == PREMIUM_REPORT){
                $cmapSearch = AutomateStandaloneRatingHandler::findInDatabase($entity->companyname);

                if ($entity->pse_company_id !== null) {
                    $officers = PseCompanyOfficer::where('company_id', $entity->pse_company_id)->groupBy('name')->get(); //limit for testing only

                    foreach ($officers as $officer) {
                        $data = AutomateStandaloneRatingHandler::findInDatabaseOfficer($officer->name);

                        foreach ($data as $datum) {
                            $sameTableType = STS_NG;
                            if ($cmapSearch !== null) {
                                foreach ($cmapSearch as $tbl_data_key => $tbl_data) {
                                    if ($tbl_data[0] == $datum['tableType']) {
                                        array_push($tbl_data[2], $datum['companyRecords']);
                                        $cmapSearch[$tbl_data_key] = $tbl_data;
                                        $sameTableType = STS_OK;
                                    }
                                }
                            }

                            // new table type push to table data array
                            if ($sameTableType == STS_NG) {
                                $cmapSearch[] = [$datum['tableType'], $datum['cmapTableHeader'], [$datum['companyRecords']]];
                            }
                        }
                    }
                }
            }  

            $current_bank = Entity::where('entityid', $financialReport->entity_id)->pluck('current_bank')->first();
            $bank = Bank::where('id', $current_bank)->first();
            $ROEDrivers = $this->getROEDrivers($fa_report);
            $turnoverRatios = FinancialAnalysisInternal::turnoverRatios($fa_report);

            $m_score = App::make('ProfileSMEController')->computeBeneishScore($financialReport->entity_id);

            $this->setData([
                'date_created' => $dateCreated->format('Y\'m\'d g:i A'),
                'entity' => $entity,
                'industry' => $industryMain,
                'ind_sub' => $entityData['industrySub'],
                'ind_row' => $entityData['industryRow'],
                'finalscore' => $smeScore,
                'readyratiodata' => $readyRatio,
                'businessdriver' => $entityData['businessDriver'],
                'revenuepotential' => $entityData['revenueGrowthPotential'],
                'bizsegment' => $entityData['businessSegment'],
                'past_projs' => $entityData['pastProjects'],
                'future_growths' => $entityData['futureGrowth'],
                'planfacilities' => $entityData['planFacilities'],
                'ind_cmp' => $industryComparison,
                'financial' => $financial,
                'cbpo_rate' => $cbpoRate,
                'graphs' => $graphs,
                'region' => !empty($region) ? $region:'',
                'zipcode' => !empty($entity->zipcode) ? $entity->zipcode : '',
                'boost' => $boostValues,
                'boost_percent' => $boost['percent'],
                'bank_standalone' => $bankStandalone,
                'growth_desc' => $forecastData['description'],
                'keySummary' => $keySummary,
                'financial_report' => $financialReport,
                'balance_sheets' => $balance_sheets,
                'income_statements' => $income_statements,
                'fa_report' => $fa_report,
                'relatedCompanies'  => $relatedCompanies,
                'boardOfDirectors'  => $boardOfDirectors,
                'keyManagers'       => $keyManagers,
                'shareHolders'      => $shareHolders,
                'majorCustomers'    => $majorCustomers,
                'majorSuppliers'    => $majorSuppliers,
                'location'          => $location,
                'assetClassification' => $assetClassification,
                'capitalDetails'    => $capitalDetails,
                'certifications'    => $certifications,
                'insurances'        => $insurances,
                'majorLocations'     => $majorLocations,
                'competitors'   	=> $competitors,
                'customerSegment'   => $customerSegment,
                'entityCity'        => !empty($entityCity) ? $entityCity:'',
                'is_financing'		=> $isFinancing,
                'dscr_details'		=> $dscrDetails,
                'employee_size'		=> $employeeSize,
                'cmapSearch'        => ($entity->is_premium == PREMIUM_REPORT) ? $cmapSearch : [],
                'bank'              => $bank,
                'cash_flows'        => $cash_flows,
                'cf_net_profit_margin_percentage' => $cf_net_profit_margin_percentage,
                'cf_asset_turnover_percentage'=> $cf_asset_turnover_percentage,
                'cf_equity_multiplier_percentage' => $cf_equity_multiplier_percentage,
                'cf_dupont_analysis_percentage' => $cf_dupont_analysis_percentage,
                'cf_net_profit_margin' => $cf_net_profit_margin,
                'cf_asset_turnover'=> $cf_asset_turnover,
                'cf_equity_multiplier' => $cf_equity_multiplier,
                'cf_dupont_analysis' => $cf_dupont_analysis,
                'turnoverRatios' => $turnoverRatios,
                'ROEDrivers'    => $ROEDrivers,
                'm_score' => $m_score
            ]);


            $this->setGraphs($graphs);

            return $this;
        }catch(Exception $ex){
            $data = array(
				"entity_id" => $entityId,
				"data" => '',
				"error" => $ex->getMessage(),
				"status" => 1,
				"is_deleted" => 0,
				"created_at" => date('Y-m-d H:i:s'),
				"updated_at" => date('Y-m-d H:i:s')
			);

			$mail = new MailHandler;
			$mail->sendAutomateError($data);

            if(env("APP_ENV") == "Production"){

				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
        }
    }

    /**
     * @param int $entityId
     * @param array $boostArray
     * @return array
     */
    public function getIndustryComparisonGraphs($entityId, $company = [], $industry = []) {
        $cr = new CurrentRatio();
        $dte = new DebtToEquity();
        $nig = new NetIncomeGrowth();
        $gpf = new GrossProfitMargin();
        $grg = new GrossRevenueGrowth();

        $graphs =  [
            'currentRatio' => $cr->generate($entityId, number_format($company['cr_ind'],2), number_format($industry['cr'],2)),
            'debtEquity' => $dte->generate($entityId, number_format($company['der_ind'],2), number_format($industry['der'],2)),
            'grossProfit' => $gpf->generate($entityId, number_format($company['gpm'],2), number_format($industry['gpm'],2)),
            'grossRevenue' => $grg->generate($entityId, number_format($company['grg'],2), number_format($industry['grg'],2)),
            'netIncome' => $nig->generate($entityId, number_format($company['nig'],2), number_format($industry['nig'],2)),
        ];

        // echo '<pre>';
        // print_r($graphs);
        // echo '</pre>';
        // exit;
        return $graphs;
    }

    /**
     * @return string
     */
    public function getTemplate() {
        return self::TEMPLATE;
    }

    //======================================================================
    //  Function: getFilenamePath
    //  Description: Get PDF filename path
    //======================================================================
    public function getFilenamePath($entityId) {
        
        $entity = Entity::find($entityId);

        $companyname = $entity->companyname;

        $filename_constant = SELF::FILENAME;
        if(strpos($companyname,"&") !== false) {
           $companyname = preg_replace("/[&]/", "And", $companyname);
        }

        $fname = sprintf(
            $filename_constant,
            $entityId,
            $companyname,date('Y-m-d'));
        
        
        
        return $this->getFilePath().$fname;
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function saveFileToDB($entityId) {
        $year = GRDP::getLatestYear();
        Entity::where('entityid', $entityId)
            ->update([
                'cbpo_pdf' => $this->getFilename(),
                'pdf_grdp' => $year,
            ]);
        $this->deleteGraphs();
        return true;
    }

    //======================================================================
    //  Function: getCompanyIndustry
    //  Description: Computation for the Industry Comparison (Company)
    //======================================================================
    public function getCompanyIndustry($id){
        $fa_report = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->orderBy('id', 'desc')->first();

        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($id);

        $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
        $fa_report->income_statements   = $financialStatement['income_statements'];
        $fa_report->cashflow            = $financialStatement['cashflow'];

        /*----------------------------------------------------------------
        --------Computation for the Industry Comparison (Company) --------
        -----------------------------------------------------------------*/
        $company_industry = array();

        $max_count = count($fa_report->income_statements)-1;

        /** Company Gross Revenue Growth */
        $company_industry['gross_revenue_growth'] = 0;
        if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
            $PercentageChange = 0;
            $company_industry['gross_revenue_growth'] = 0;
            $dividend = $fa_report->income_statements[$max_count]->Revenue;

            if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
                $PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
            }

            if($PercentageChange > 200){
                $company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
            }else{
                $company_industry['gross_revenue_growth'] = round($PercentageChange,2);
            }
        }

        /** Company Net Income Growth */
        $company_industry['net_income_growth'] = 0;
        if( isset($fa_report->income_statements[$max_count]->ProfitLoss) && (($fa_report->income_statements[$max_count]->ProfitLoss) != 0)){
            $PercentageChange = 0;
            $dividend = ($fa_report->income_statements[$max_count]->ProfitLoss);

            if($dividend != 0  && $fa_report->income_statements[$max_count]->ProfitLoss != 0){
                $PercentageChange = (($fa_report->income_statements[0]->ProfitLoss - $fa_report->income_statements[$max_count]->ProfitLoss) /  $dividend) * 100;
            }

            if($PercentageChange > 200){
                $company_industry['net_income_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
            }else{
                $company_industry['net_income_growth'] = round($PercentageChange,2);
            }
        }

        /** Company Gross Profit Margin */
        $company_industry['gross_profit_margin'] = 0;
        if($max_count >= 0){
            for($x=$max_count; $x >= 0 ; $x--){
                if($fa_report->income_statements[$x]->Revenue != 0){
                    $company_industry['gross_profit_margin'] = ($fa_report->income_statements[$x]->GrossProfit / $fa_report->income_statements[$x]->Revenue) * 100;

                    if($company_industry['gross_profit_margin'] == 0){
                        $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                    }else{
                        $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                        $company_industry['gross_profit_margin'] = (int)$company_industry['gross_profit_margin'];
                    }
                    
                }
            }
        }

            /** Company Current Ratio */    
            $company_industry['current_ratio'] = 0;
            $i=0;
            for($x=@count($fa_report->balance_sheets) - 1; $x >= 0 ; $x--){
                $currentAssets = $fa_report->balance_sheets[$x]->CurrentAssets;
                $currentLiabilities = $fa_report->balance_sheets[$x]->CurrentLiabilities;

                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                if($currentLiabilities > 0){
                    $company_industry['current_ratio'] = $currentAssets / $currentLiabilities;
                    $company_industry['current_ratio'] = round($company_industry['current_ratio'], 2, PHP_ROUND_HALF_DOWN);
                }
                $i++;
            }

            /** Company Debt-to-Equity Ratio */
            $company_industry['debt_to_equity'] = 0;
            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
            if(count($sustainability['financial_table']) > 0){
            	$company_industry['debt_to_equity'] = $sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage;	
            }

        return $company_industry;
    }

    public static function getCompanyIndustryStatic($id){
        $fa_report = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->orderBy('id', 'desc')->first();

        if(empty($fa_report->entity_id)){
        	return [];
        }
        
        $financial = new FinancialReport();
        $financialStatement = $financial->getFinancialReportById($id);

        $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
        $fa_report->income_statements   = $financialStatement['income_statements'];
        $fa_report->cashflow            = $financialStatement['cashflow'];

        /*----------------------------------------------------------------
        --------Computation for the Industry Comparison (Company) --------
        -----------------------------------------------------------------*/
        $company_industry = array();

        $max_count = count($fa_report->balance_sheets)-2;

        /** Company Gross Revenue Growth */
        $company_industry['gross_revenue_growth'] = 0;
        if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
            $PercentageChange = 0;
            $company_industry['gross_revenue_growth'] = 0;
            $dividend = $fa_report->income_statements[$max_count]->Revenue;

            if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
                $PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
            }

            if($PercentageChange > 200){
                $company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
            }else{
                $company_industry['gross_revenue_growth'] = round($PercentageChange,2);
            }
        }

        /** Company Net Income Growth */
        $company_industry['net_income_growth'] = 0;
        if( isset($fa_report->income_statements[$max_count]->ProfitLoss) && (($fa_report->income_statements[$max_count]->ProfitLoss) != 0)){
            $PercentageChange = 0;
            $dividend = ($fa_report->income_statements[$max_count]->ProfitLoss);

            if($dividend != 0  && $fa_report->income_statements[$max_count]->ProfitLoss != 0){
                $PercentageChange = (($fa_report->income_statements[0]->ProfitLoss - $fa_report->income_statements[$max_count]->ProfitLoss) /  $dividend) * 100;
            }

            if($PercentageChange > 200){
                $company_industry['net_income_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
            }else{
                $company_industry['net_income_growth'] = round($PercentageChange,2);
            }
        }

        /** Company Gross Profit Margin */
        $company_industry['gross_profit_margin'] = 0;
        if($max_count >= 0){
            for($x=$max_count; $x >= 0 ; $x--){
                if($fa_report->income_statements[$x]->Revenue != 0){
                    $company_industry['gross_profit_margin'] = ($fa_report->income_statements[$x]->GrossProfit / $fa_report->income_statements[$x]->Revenue) * 100;

                    if($company_industry['gross_profit_margin'] == 0){
                        $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                    }else{
                        $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                        $company_industry['gross_profit_margin'] = (int)$company_industry['gross_profit_margin'];
                    }
                    
                }
            }
        }

            /** Company Current Ratio */    
            $company_industry['current_ratio'] = 0;
            $i=0;
            for($x=@count($fa_report->balance_sheets) - 1; $x >= 0 ; $x--){
                $currentAssets = $fa_report->balance_sheets[$x]->CurrentAssets;
                $currentLiabilities = $fa_report->balance_sheets[$x]->CurrentLiabilities;

                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                if($currentLiabilities > 0){
                    $company_industry['current_ratio'] = $currentAssets / $currentLiabilities;
                    $company_industry['current_ratio'] = round($company_industry['current_ratio'], 2, PHP_ROUND_HALF_DOWN);
                }
                $i++;
            }

            /** Company Debt-to-Equity Ratio */
            $company_industry['debt_to_equity'] = 0;
            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
            $company_industry['debt_to_equity'] = $sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage;

        return $company_industry;
    }

    public function getIndustryComparisonTreammean($entityid){
		
		$entities = Entity::where('is_deleted',0)->get();

		$industries = Entity::select('industry_row_id')->where('is_deleted',0)->groupBy('industry_row_id')->get()->toArray();
		
		foreach ($industries as $k => $ind) {
			$entities = Entity::where(['status' => 7,'is_deleted' => 0,'industry_row_id' => $ind['industry_row_id']])->get();
			$countEntities = count($entities);
			$gross_revenue_growth = [];
			$net_income_growth = [];
			$gross_profit_margin = [];
			$current_ratio = [];
			$debt_equity_ratio = [];

			if($countEntities > 0){
				foreach ($entities as $k => $entity) {
			
					$financial = new FinancialReport();
			        $financialStatement = $financial->getFinancialReportById($entity->entityid);

			        $data = [
			        	'companyname' => $entity->companyname,
			        	'industry_code' => $entity->industry_row_id,
			        	
			        ];

			        $data['industry_code_name'] = DB::table('tblindustry_class')->where('class_code',$data['industry_code'])->pluck('class_description');	 	  
			       
			        $balance_sheets      = $financialStatement['balance_sheets'];
			        $income_statements   = $financialStatement['income_statements'];
			        $cashflow            = $financialStatement['cashflow'];	

			        $industryComparison = $this->getCompanyIndustry($entity->entityid);

			        $NetOperatingIncome = $cashflow[0]['NetOperatingIncome'];
			      	
			      	if($industryComparison){

			        	$gross_revenue_growth[] = $industryComparison['gross_revenue_growth'];
						$net_income_growth[] = $industryComparison['net_income_growth'];
						$gross_profit_margin[] = $industryComparison['gross_profit_margin'];
						$current_ratio[] = $industryComparison['current_ratio'];
						$debt_equity_ratio[] = $industryComparison['debt_to_equity'];
		        	}

				}

				$gross_revenue_growth = array_filter($gross_revenue_growth);

	   		 	$gross_revenue_growth_average = 0;
	   		 	if(count($gross_revenue_growth) > 0)
	   		 		$gross_revenue_growth_average = array_sum($gross_revenue_growth)/count($gross_revenue_growth);

				$net_income_growth = array_filter($net_income_growth);

				$net_income_growth_average = 0;
				if(count($net_income_growth) > 0)
					$net_income_growth_average = array_sum($net_income_growth)/count($net_income_growth);

				$gross_profit_margin = array_filter($gross_profit_margin);

				$gross_profit_margin_average = 0;
				if(count($gross_profit_margin) > 0)
					$gross_profit_margin_average = array_sum($gross_profit_margin)/count($gross_profit_margin);

				$current_ratio = array_filter($current_ratio);
				$current_ratio_average = 0;
				if(count($current_ratio) > 0)
					$current_ratio_average = array_sum($current_ratio)/count($current_ratio);

				$debt_equity_ratio = array_filter($debt_equity_ratio);

				$debt_equity_ratio_average = 0;
				if(count($debt_equity_ratio) > 0)
					$debt_equity_ratio_average = array_sum($debt_equity_ratio)/count($debt_equity_ratio);

				$cutoff = intval($countEntities*0.25/2);

				if($cutoff > 0){
					$percentage = number_format(1/(($countEntities-2)*$cutoff),2);
			        $countOff = intval($percentage*($countEntities));
			        
			        sort($gross_revenue_growth);
			        sort($net_income_growth);
			        sort($gross_profit_margin);
			        sort($current_ratio);
			        sort($debt_equity_ratio);


		       		$totalCount1 = $countEntities - $countOff; 

		       		$gross_revenue_growth =  array_slice($gross_revenue_growth, $countOff, count($gross_revenue_growth));
		       		$net_income_growth =  array_slice($net_income_growth, $countOff, count($net_income_growth));
		       		$gross_profit_margin =  array_slice($gross_profit_margin, $countOff, count($gross_profit_margin));
		       		$current_ratio =  array_slice($current_ratio, $countOff, count($current_ratio));
		       		$debt_equity_ratio =  array_slice($debt_equity_ratio, $countOff, count($debt_equity_ratio));

		       		$totalCount2 = $totalCount1 - $countOff;

		       		$gross_revenue_growth = array_slice($gross_revenue_growth, 0, $totalCount2);
		       		$net_income_growth = array_slice($net_income_growth, 0, $totalCount2);
		       		$gross_profit_margin = array_slice($gross_profit_margin, 0, $totalCount2);
		       		$current_ratio = array_slice($current_ratio, 0, $totalCount2);
		       		$debt_equity_ratio = array_slice($debt_equity_ratio, 0, $totalCount2);
		     

		   		 	$gross_revenue_growth = array_filter($gross_revenue_growth);

		   		 	$gross_revenue_growth_average = 0;
		   		 	if(count($gross_revenue_growth) > 0)
		   		 		$gross_revenue_growth_average = array_sum($gross_revenue_growth)/count($gross_revenue_growth);

					$net_income_growth = array_filter($net_income_growth);

					$net_income_growth_average = 0;
					if(count($net_income_growth) > 0)
						$net_income_growth_average = array_sum($net_income_growth)/count($net_income_growth);

					$gross_profit_margin = array_filter($gross_profit_margin);

					$gross_profit_margin_average = 0;
					if(count($gross_profit_margin) > 0)
						$gross_profit_margin_average = array_sum($gross_profit_margin)/count($gross_profit_margin);

					$current_ratio = array_filter($current_ratio);
					$current_ratio_average = 0;
					if(count($current_ratio) > 0)
						$current_ratio_average = array_sum($current_ratio)/count($current_ratio);

					$debt_equity_ratio = array_filter($debt_equity_ratio);

					$debt_equity_ratio_average = 0;
					if(count($debt_equity_ratio) > 0)
						$debt_equity_ratio_average = array_sum($debt_equity_ratio)/count($debt_equity_ratio);
			       
			    }

			    $ind_data = [
			       		'industry_code' => $entity->industry_row_id,
			       		'gross_revenue_growth' => $gross_revenue_growth_average,
			       		'net_income_growth' => $net_income_growth_average,
			       		'gross_profit_margin' => $gross_profit_margin_average,
			       		'current_ratio' => $current_ratio_average,
			       		'debt_equity_ratio' => $debt_equity_ratio_average,

			       ];

			       $industry = DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->get()->toArray();

			       if(!empty($industry)){
			       		DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->update($ind_data);
			       }else{
			       		DB::table('tbl_industry_comparisons_computed')->insert($ind_data);
			       }
				}
			}
	

		$entity = Entity::where('entityid',$entityid)->first();
		$ind_data = DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->first();
		
		return (array) $ind_data;
	}



	public static function getIndustryComparisonTreammeanStatic($entityid){
		$entities = Entity::where('is_deleted',0)->get();

		$industries = Entity::select('industry_row_id')->where('is_deleted',0)->groupBy('industry_row_id')->get()->toArray();
		
		foreach ($industries as $k => $ind) {
			$entities = Entity::where(['status' => 7,'is_deleted' => 0,'industry_row_id' => $ind['industry_row_id']])->get();
			$countEntities = count($entities);
			$gross_revenue_growth = [];
			$net_income_growth = [];
			$gross_profit_margin = [];
			$current_ratio = [];
			$debt_equity_ratio = [];

			if($countEntities > 0){
				foreach ($entities as $k => $entity) {
			
					$financial = new FinancialReport();
			        $financialStatement = $financial->getFinancialReportById($entity->entityid);

			        $data = [
			        	'companyname' => $entity->companyname,
			        	'industry_code' => $entity->industry_row_id,
			        	
			        ];

			        $data['industry_code_name'] = DB::table('tblindustry_class')->where('class_code',$data['industry_code'])->pluck('class_description');	 	  
			       
			        $balance_sheets      = $financialStatement['balance_sheets'];
			        $income_statements   = $financialStatement['income_statements'];
			        $cashflow            = $financialStatement['cashflow'];	

			        $industryComparison = self::getCompanyIndustryStatic($entity->entityid);

			        $NetOperatingIncome = $cashflow[0]['NetOperatingIncome'];
			      	
			      	if($industryComparison){

			        	$gross_revenue_growth[] = $industryComparison['gross_revenue_growth'];
						$net_income_growth[] = $industryComparison['net_income_growth'];
						$gross_profit_margin[] = $industryComparison['gross_profit_margin'];
						$current_ratio[] = $industryComparison['current_ratio'];
						$debt_equity_ratio[] = $industryComparison['debt_to_equity'];
		        	}

				}

				$gross_revenue_growth = array_filter($gross_revenue_growth);

	   		 	$gross_revenue_growth_average = 0;
	   		 	if(count($gross_revenue_growth) > 0)
	   		 		$gross_revenue_growth_average = array_sum($gross_revenue_growth)/count($gross_revenue_growth);

				$net_income_growth = array_filter($net_income_growth);

				$net_income_growth_average = 0;
				if(count($net_income_growth) > 0)
					$net_income_growth_average = array_sum($net_income_growth)/count($net_income_growth);

				$gross_profit_margin = array_filter($gross_profit_margin);

				$gross_profit_margin_average = 0;
				if(count($gross_profit_margin) > 0)
					$gross_profit_margin_average = array_sum($gross_profit_margin)/count($gross_profit_margin);

				$current_ratio = array_filter($current_ratio);
				$current_ratio_average = 0;
				if(count($current_ratio) > 0)
					$current_ratio_average = array_sum($current_ratio)/count($current_ratio);

				$debt_equity_ratio = array_filter($debt_equity_ratio);

				$debt_equity_ratio_average = 0;
				if(count($debt_equity_ratio) > 0)
					$debt_equity_ratio_average = array_sum($debt_equity_ratio)/count($debt_equity_ratio);

				$cutoff = intval($countEntities*0.25/2);

				if($cutoff > 0){
					$percentage = number_format(1/(($countEntities-2)*$cutoff),2);
			        $countOff = intval($percentage*($countEntities));
			        
			        sort($gross_revenue_growth);
			        sort($net_income_growth);
			        sort($gross_profit_margin);
			        sort($current_ratio);
			        sort($debt_equity_ratio);


		       		$totalCount1 = $countEntities - $countOff; 

		       		$gross_revenue_growth =  array_slice($gross_revenue_growth, $countOff, count($gross_revenue_growth));
		       		$net_income_growth =  array_slice($net_income_growth, $countOff, count($net_income_growth));
		       		$gross_profit_margin =  array_slice($gross_profit_margin, $countOff, count($gross_profit_margin));
		       		$current_ratio =  array_slice($current_ratio, $countOff, count($current_ratio));
		       		$debt_equity_ratio =  array_slice($debt_equity_ratio, $countOff, count($debt_equity_ratio));

		       		$totalCount2 = $totalCount1 - $countOff;

		       		$gross_revenue_growth = array_slice($gross_revenue_growth, 0, $totalCount2);
		       		$net_income_growth = array_slice($net_income_growth, 0, $totalCount2);
		       		$gross_profit_margin = array_slice($gross_profit_margin, 0, $totalCount2);
		       		$current_ratio = array_slice($current_ratio, 0, $totalCount2);
		       		$debt_equity_ratio = array_slice($debt_equity_ratio, 0, $totalCount2);
		     

		   		 	$gross_revenue_growth = array_filter($gross_revenue_growth);

		   		 	$gross_revenue_growth_average = 0;
		   		 	if(count($gross_revenue_growth) > 0)
		   		 		$gross_revenue_growth_average = array_sum($gross_revenue_growth)/count($gross_revenue_growth);

					$net_income_growth = array_filter($net_income_growth);

					$net_income_growth_average = 0;
					if(count($net_income_growth) > 0)
						$net_income_growth_average = array_sum($net_income_growth)/count($net_income_growth);

					$gross_profit_margin = array_filter($gross_profit_margin);

					$gross_profit_margin_average = 0;
					if(count($gross_profit_margin) > 0)
						$gross_profit_margin_average = array_sum($gross_profit_margin)/count($gross_profit_margin);

					$current_ratio = array_filter($current_ratio);
					$current_ratio_average = 0;
					if(count($current_ratio) > 0)
						$current_ratio_average = array_sum($current_ratio)/count($current_ratio);

					$debt_equity_ratio = array_filter($debt_equity_ratio);

					$debt_equity_ratio_average = 0;
					if(count($debt_equity_ratio) > 0)
						$debt_equity_ratio_average = array_sum($debt_equity_ratio)/count($debt_equity_ratio);
			       
			    }

			    $ind_data = [
			       		'industry_code' => $entity->industry_row_id,
			       		'gross_revenue_growth' => $gross_revenue_growth_average,
			       		'net_income_growth' => $net_income_growth_average,
			       		'gross_profit_margin' => $gross_profit_margin_average,
			       		'current_ratio' => $current_ratio_average,
			       		'debt_equity_ratio' => $debt_equity_ratio_average,

			       ];

			       $industry = DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->get()->toArray();

			       if(!empty($industry)){
			       		DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->update($ind_data);
			       }else{
			       		DB::table('tbl_industry_comparisons_computed')->insert($ind_data);
			       }
				}
			}
	

		$entity = Entity::where('entityid',$entityid)->first();
		$ind_data = DB::table('tbl_industry_comparisons_computed')->where('industry_code',$entity->industry_row_id)->first();
		
		return (array) $ind_data;
	}

    public function getROEDrivers($fa_report){
        $turnoverRatios = FinancialAnalysisInternal::turnoverRatios($fa_report);

        //Return on equity (ROE)
        $ROE = [];
        $ROEDrivers = [];
        $fa_report->balance_sheets = $fa_report->balanceSheets()->orderBy('index_count', 'asc')->get();
        $fa_report->income_statements = $fa_report->incomeStatements()->orderBy('index_count', 'asc')->get();
        $fa_report->cash_flows = $fa_report->cashFlow()->orderBy('index_count', 'asc')->get();

        for ($x = count($fa_report->income_statements) - 1; $x >= 0 ; $x--) {
            /** EQUITY AVERAGE */
            isset($fa_report->balance_sheets[$x]) ? $balanceSheetEquity1 = $fa_report->balance_sheets[$x]->Equity : $balanceSheetEquity1 = 0;
            isset($fa_report->balance_sheets[$x + 1]) ? $balanceSheetEquity2 = $fa_report->balance_sheets[$x + 1]->Equity : $balanceSheetEquity2 = 0;

            $equityAverage = ($balanceSheetEquity1 + $balanceSheetEquity2) / 2;

            /** ASSETS AVERAGE */
            isset($fa_report->balance_sheets[$x]) ? $balanceSheetAssets1 = $fa_report->balance_sheets[$x]->Assets : $balanceSheetAssets1 = 0;
            isset($fa_report->balance_sheets[$x + 1]) ? $balanceSheetAssets2 = $fa_report->balance_sheets[$x + 1]->Assets : $balanceSheetAssets2 = 0;

            $assetsAverage = ($balanceSheetAssets1 + $balanceSheetAssets2) / 2;

            /** REVENUE */
            isset($fa_report->income_statements[$x]->Revenue) ? $revenue = $fa_report->income_statements[$x]->Revenue : $revenue = 0;

            /** ROE */
            if ($equityAverage != 0) {
                $roeTemp = $fa_report->income_statements[$x]->ProfitLoss / $equityAverage;
                $ROE[] = $roeTemp;
            } else {
                $ROE[] = 0;
                $roeTemp = 0;
            }

            /** Financia Leverage */
            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
            $financialLeverage = 0;
            foreach($sustainability['financial_table'] as $key => $value){
                if($value->year == $fa_report->income_statements[$x]->Year){
                    $financialLeverage = $value->FinancialLeverage;
                }
            }

            /** Net Income */
            isset($fa_report->income_statements[$x]->ProfitLossFromOperatingActivities) ? $net_income = $fa_report->income_statements[$x]->ProfitLossFromOperatingActivities : $net_income = 0;

            /** Profit Margin */
            $ProfitMargin = 0;
            if($fa_report->income_statements[$x]->Revenue != 0){
                $ProfitMargin = ($fa_report->income_statements[$x]->ProfitLoss / $fa_report->income_statements[$x]->Revenue) * 100;
            }

            /** Turnover Asset */	
            $AssetTurnover = 0.1;
            foreach($turnoverRatios as $t){
                if($t->year == $fa_report->income_statements[$x]->Year){
                    $AssetTurnover = $t->AssetTurnover;

                    if($AssetTurnover != 0 ){
                        if($t->num_days/$t->AssetTurnover >= 0.1) {
                            $AssetTurnover = $t->num_days/$t->AssetTurnover;
                        }
                    }
                }
            }

            $ROEDrivers[] = array(
                'year' => $fa_report->income_statements[$x]->Year,
                'roe' => $roeTemp,
                'equity_average'		=> $equityAverage,
                'assets_average'		=> $assetsAverage,
                'revenue'				=> $revenue,
                'financial_leverage'	=> $financialLeverage,
                'net_income'			=> $net_income,
                'profit_margin'			=> $ProfitMargin,
                'turnover_asset'		=> $AssetTurnover
            );
        }

        $remove_array = array();
        foreach($ROEDrivers as $key => $value){
            $isZero = false;
            foreach($value as $key_value => $a){
                if($key_value == 'year' || $key_value == 'turnover_asset'){
                    //
                }else{
                    if((int)$a !== 0){
                        $isZero = true;
                    }
                }
            }
            if($isZero == false){
                array_push($remove_array, $key);
            }
        }
        foreach($remove_array as $rmv){
            unset($ROEDrivers[$rmv]);
        }

        return $ROEDrivers;
    }
}
