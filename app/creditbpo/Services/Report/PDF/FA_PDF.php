<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Libraries\HighCharts;
use PDF as PDFLoader;
use File;

abstract class FA_PDF {
    protected $data = [];
    protected $filename;
    protected $filepath;
    protected $graphs = [];
    protected $pcharts = [];
    protected $pdfInstance;

    public function __construct() {
        $this->setFilePath(public_path('documents').'/');
    }

    /**
     * @param string $path
     * @return PDF
     */
    public function setFilePath($path) {
        $this->filepath = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath() {
        return $this->filepath;
    }

    /**
     * @param array $data
     * @return PDF
     */
    public function setData(array $data = []) {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param string $filename
     * @return PDF
     */
    public function setFilename($filename) {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * @param array $data
     * @return PDF
     */
    public function setGraphs(array $graphs = []) {
        $this->graphs = array_merge($this->graphs, $graphs);
        return $this;
    }

    /**
     * @return array
     */
    public function getGraphs() {
        return $this->graphs;
    }

    /**
     * @return bool
     */
    public function deleteGraphs() {
        $highCharts = new HighCharts();
        $directory = $highCharts->getDirectory() . '/';
        $graphs = $this->getGraphs();

        foreach ($graphs as $graph) {
            $file = $directory.$graph;

            if (!file_exists($file)) {
                continue;
            }

            File::delete($file);
        }

        return true;
    }

    /**
     * @param array $pcharts
     * @return PDF
     */
    public function setPCharts(array $pcharts = []) {
        $this->pcharts = array_merge($this->pcharts, $pcharts);
        return $this;
    }

    /**
     * @return array
     */
    public function getPCharts() {
        return $this->pcharts;
    }

    /**
     * @return bool
     */
    public function deletePCharts() {
        foreach ($this->pcharts as $chart) {
            if (file_exists($chart)) {
                File::delete($chart);
            }
        }

        return true;
    }

    /**
     * @return void
     */
    public function generate($language='en') {
        $template = $this->getTemplate($language);
        $vars = $this->getData();
        $this->pdfInstance = PDFLoader::loadView($template, $vars);
        return $this->pdfInstance;
    }

    /**
     * @param PDFLoader $pdf
     */
    public function save() {
        if (!$this->pdfInstance) {
            return false;
        }

        $filename = $this->getFilename();
        $filepath = $this->getFilePath();

        if (file_exists($filepath.$filename)) {
            unlink($filepath.$filename);
        }

        $this->pdfInstance->save($filepath.$filename);

        return $this;
    }

    /**
     * @return string
     */
    abstract function getTemplate($language);
}
