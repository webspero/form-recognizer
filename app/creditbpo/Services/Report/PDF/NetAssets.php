<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Models\BalanceSheet;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\IncomeStatement;
use CreditBPO\Services\Report\PDF\PDF;
use CreditBPO\Services\Report\PCharts\NetAssets as PChartsNetAssets;

//======================================================================
//  Class: Net Assets
//  Description: Net Assets PDF Genrator
//======================================================================
class NetAssets extends PDF
{
    const TEMPLATE = 'financial.net-assets';
    const FILENAME = 'Net Assets - %s as of %s.pdf';

    public function __construct()
    {
        $this->setFilePath(public_path('financial_analysis') . '/');
    }

    public function create($id)
    {
        set_time_limit(0);

        $financialReport = FinancialReport::find($id);

        $pChartsNetAssets = new PChartsNetAssets();

        $pcharts = [
            'pChartsNetAssets' => $pChartsNetAssets->generate($id)
        ];

        $balanceSheets = BalanceSheet::where('financial_report_id', $financialReport->id)
            ->orderBy('index_count', 'asc')
            ->get();
        if (!empty($balanceSheets)) {
            foreach ($balanceSheets as $key => $balanceSheet) {
                if($key == 3 && $balanceSheet->Assets == 0 && $balanceSheet->Liabilities) {
                    $balanceSheets->pull($key);
                    continue;
                }
                $incomeStatement = IncomeStatement::where('financial_report_id', $balanceSheet->financial_report_id)
                    ->where('Year', $balanceSheet->Year)
                    ->where('GrossProfit', '>', 0)
                    ->where('ProfitLoss', '>', 0)
                    ->where('ComprehensiveIncome', '>', 0)
                    ->first();

                if ($balanceSheets->count() != 4 && $incomeStatement == null) {
                    $balanceSheets->pull($key);
                }
                if ($balanceSheets->count() == 4 && $key == 2 && $incomeStatement == null) {
                    $balanceSheets->pull($key);
                    $balanceSheets->pull($key + 1);
                }
            }
            $balanceSheets = $balanceSheets->values();
        }
        
        $this->setData([
            'financial_report' => $financialReport,
            'balance_sheets' => $balanceSheets,
            'pcharts' => $pcharts,
        ]);

        $filename = sprintf(
            'Net Assets %s',
            $financialReport->title . '-' . date('Y-m-d')
        );

        $fname = $filename . '.pdf';
        if (strpos($fname, "&") !== false) {
            $fname = preg_replace("/[&]/", "And", $fname);
        }

        $this->setFilename($fname);
        $this->setPCharts($pcharts);

        return $this;
    }

    //======================================================================
    //  Function: getTemplate
    //  Description: Get PDF Template
    //======================================================================
    public function getTemplate()
    {
        return self::TEMPLATE;
    }

    //======================================================================
    //  Function: getFilenamePath
    //  Description: Get PDF filename path
    //======================================================================
    public function getFilenamePath($frId)
    {

        $financialReport = FinancialReport::find($frId);

        if (@count($financialReport) <= 0) {
            return null;
        }

        $filename = sprintf(
            'Net Assets %s',
            $financialReport->title . '-' . date('Y-m-d')
        );

        $fname = $filename . '.pdf';
        if (strpos($fname, "&") !== false) {
            $fname = preg_replace("/[&]/", "And", $fname);
        }

        return $this->getFilePath() . $fname;
    }
}
