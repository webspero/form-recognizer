<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Models\Entity;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\PDF;
use CreditBPO\Services\Report\PCharts\AssetEquity;
use CreditBPO\Services\Report\PCharts\AssetStructure;
use CreditBPO\Services\Report\PCharts\CapitalStructure;
use CreditBPO\Services\Report\PCharts\FinancialStability;
use CreditBPO\Services\Report\PCharts\LiquidityRatio;
use CreditBPO\Services\Report\PCharts\NetAssets;
use CreditBPO\Services\Report\PCharts\ProfitRatio;
use CreditBPO\Services\Report\PCharts\RevenueNetProfit;
use CreditBPO\Services\Report\PCharts\WorkingCapital;

class FinancialAnalysis extends PDF {
    const TEMPLATE = 'financial.report';
    const FILENAME = 'CreditBPO Financial Analysis - %s as of %s.pdf';

    public function __construct() {
        $this->setFilePath(public_path('financial_analysis').'/');
    }

    public function create($id) {
        set_time_limit(0);

        $financialReport = FinancialReport::find($id);

        $assetEquity = new AssetEquity();
        $assetStructure = new AssetStructure();
        $capitalStructure = new CapitalStructure();
        $financialStability = new FinancialStability();
        $liquidityRatio = new LiquidityRatio();
        $netAssets = new NetAssets();
        $profitRatio = new ProfitRatio();
        $revenueNetProfit = new RevenueNetProfit();
        $workingCapital = new WorkingCapital();

        $pcharts = [
            'assetEquity' => $assetEquity->generate($id),
            'assetStructure' => $assetStructure->generate($id),
            'capitalStructure' => $capitalStructure->generate($id),
            'financialStability' => $financialStability->generate($id),
            'liquidityRatio' => $liquidityRatio->generate($id),
            'netAssets' => $netAssets->generate($id),
            'profitRatio' => $profitRatio->generate($id),
            'revenueNetProfit' => $revenueNetProfit->generate($id),
            'workingCapital' => $workingCapital->generate($id),
        ];

        $this->setData([
            'financial_report' => $financialReport,
            'balance_sheets' => $financialReport->balanceSheets()
                ->where('Assets', '!=', 0)
                ->where('Liabilities', '!=', 0)
                ->orderBy('index_count', 'asc')->get(),
            'income_statements' => $financialReport->incomeStatements()
                ->where('Revenue', '!=', 0)
                ->orderBy('index_count', 'asc')->get(),
            'pcharts' => $pcharts,
        ]);
        $filename = sprintf(
            self::FILENAME,
            $financialReport->title,
            date('Y-m-d'),
            $financialReport->id);
        $fname = $filename;
        if(strpos($fname,"&") !== false) {
           $fname = preg_replace("/[&]/", "And", $fname);
        }
        
        $this->setFilename($fname);
        $this->setPCharts($pcharts);

        return $this;
    }

    public function getTemplate() {
        return self::TEMPLATE;
    }

    /**
     * @param int $id
     * @return string|null
     */
    public function getFilenamePath($entityId) {
        $entity = Entity::find($entityId);

        if (@count($entity) <= 0) {
            return null;
        }

        $filename = sprintf(
            self::FILENAME,
            trim(rtrim($entity->companyname, '.')),
            date('Y-m-d'));
        
        $fname = $filename;
        if(strpos($fname,"&") !== false) {
           $fname = preg_replace("/[&]/", "And", $fname);
        }
        
        return $this->getFilePath().$fname;
    }
}
