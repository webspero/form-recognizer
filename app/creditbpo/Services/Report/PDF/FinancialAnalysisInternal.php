<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Models\Entity;
use CreditBPO\Models\Bank;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\FA_PDF as PDF;
use CreditBPO\Services\Report\PCharts\AssetStructure;
use CreditBPO\Services\Report\PCharts\RevenueNetProfit;
use CreditBPO\Services\Report\PCharts\AssetEquity;
use CreditBPO\Services\Report\PCharts\LiquidityRatio;
use CreditBPO\Services\Report\PCharts\ProfitRatio;
use CreditBPO\Services\Report\PCharts\CapitalStructure;
use CreditBPO\Services\Report\PCharts\FinancialStability;
use CreditBPO\Services\Report\PCharts\NetAssets;
use CreditBPO\Services\Report\PCharts\WorkingCapital;
use ReadyratioKeySummary;
use CreditBPO\Services\KeyRatioService;
use DB;
use \Exception as Exception;
use CreditBPO\Services\MailHandler;

//======================================================================
//  Class: FinancialAnalysisInternal
//  Description: Contains functions for creating Financial Analysis Internal PDF and it's getting
//    it's path
//======================================================================
class FinancialAnalysisInternal extends PDF {

    const TEMPLATE_EN = 'financial.financial_analysis_en.financial_analysis';
    const TEMPLATE_TL = 'financial.financial_analysis_tl.financial_analysis';
    const FILENAME_EN = 'CreditBPO Financial Analysis %s';
    const FILENAME_TL = 'Pagsusuri sa Pananalapi ng CreditBPO %s';

    public function __construct() {
        $this->setFilePath(public_path('financial_analysis').'/');
    }

    //======================================================================
    //  Function: create
    //  Description: Create Financial Analysis Internal PDF File
    //======================================================================
    public function create($id,$language) {
        try{
            set_time_limit(0);

            $financialReport = FinancialReport::find($id);

            $assetStructure = new AssetStructure();
            $revenueNetProfit = new RevenueNetProfit();
            $assetEquity = new AssetEquity();
            $liquidityRatio = new LiquidityRatio();
            $profitRatio = new ProfitRatio();
            $capitalStructure = new CapitalStructure();
            $financialStability = new FinancialStability();
            $netAssets = new NetAssets();
            $workingCapital = new WorkingCapital();

            $pcharts = [
                'assetStructure' => $assetStructure->generate($id),
                'revenueNetProfit' => $revenueNetProfit->generate($id),
                'assetEquity' => $assetEquity->generate($id),
                'liquidityRatio' => $liquidityRatio->generate($id),
                'profitRatio' => $profitRatio->generate($id),
                'capitalStructure' => $capitalStructure->generate($id),
                'financialStability' => $financialStability->generate($id),
                'netAssets' => $netAssets->generate($id),
                'workingCapital'    => $workingCapital->generate($id)
            ];
            
            $financial_sustainability = $this->financialSustainability($financialReport);
            $turnoverRatios = $this->turnOverRatios($financialReport);
            $keys = ReadyratioKeySummary::where('entityid',$financialReport->entity_id)->orderBy('id', 'desc')->get();

            /** Check KeyRatios Value for old report*/
            if(count($keys) == 0){
                $keyRatio = new KeyRatioService();
                $keyRatio->generateKeyRatioByEntityId($financialReport->entity_id);
                $keySummary = ReadyratioKeySummary::where('entityid',$financialReport->entity_id)->orderBy('id', 'desc')->get();
            }else{
                $keySummary = ReadyratioKeySummary::where('entityid',$financialReport->entity_id)->orderBy('id', 'desc')->get();
            }

            $financial = new FinancialReport();
            $financialStatement = $financial->getFinancialReportById($financialReport->entity_id);

            // Use registered companyname, not companyname on uploaded fs template
            $companyname = Entity::where('entityid', $financialReport->entity_id)->pluck('companyname')->first();
            $financialReport->title = $companyname;
            $current_bank = Entity::where('entityid', $financialReport->entity_id)->pluck('current_bank')->first();
            $bank = Bank::where('id', $current_bank)->first();

            $this->setData([    
                'financial_report'  => $financialReport,
                'balance_sheets'    => $financialStatement['balance_sheets'],
                'income_statements' => $financialStatement['income_statements'],
                'cashflow'          => $financialStatement['cashflow'],
                'pcharts'           => $pcharts,
                'financial_table'   => $financial_sustainability['financial_table'],
                'financial_change'  => $financial_sustainability['financial_change'],
                'turnoverRatios'    => $turnoverRatios,
                'keySummary'        => $keySummary,
                'bank'              => $bank
            ]);

            $filename_constant = Self::FILENAME_EN;
            if($language == 'tl')
                $filename_constant = Self::FILENAME_TL;

            $filename = sprintf(
                $filename_constant,
                $companyname.'-'.date('Y-m-d'));

            $fname = $filename.'.pdf';
            if(strpos($fname,"&") !== false) {
            $fname = preg_replace("/[&]/", "And", $fname);
            }
            
            $this->setFilename($fname);
            $this->setPCharts($pcharts);

            return $this;
        }catch(Exception $ex){

            $financialReport = FinancialReport::find($id);

            $data = array(
				"entity_id" => $financialReport->entity_id,
				"data" => '',
				"error" => $ex->getMessage(),
				"status" => 1,
				"is_deleted" => 0,
				"created_at" => date('Y-m-d H:i:s'),
				"updated_at" => date('Y-m-d H:i:s')
			);

			$mail = new MailHandler;
			$mail->sendAutomateError($data);

            if(env("APP_ENV") == "Production"){

				Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
        }
    }

    //======================================================================
    //  Function: getTemplate
    //  Description: Get PDF Template
    //======================================================================
    public function getTemplate($language) {
        
        $language = session('fa_report_lang');
        $template_constant = Self::TEMPLATE_EN;
        if($language == 'tl')
            $template_constant = Self::TEMPLATE_TL;

        return $template_constant;
    }

    //======================================================================
    //  Function: getFilenamePath
    //  Description: Get PDF filename path
    //======================================================================
    public function getFilenamePath($frId,$language) {
        
        $financialReport = FinancialReport::find($frId);

        // Use registered companyname, not companyname on uploaded fs template
        $companyname = Entity::where('entityid', $financialReport->entity_id)->pluck('companyname')->first();

        if (@count($financialReport) <= 0) {
            return null;
        }

        $filename_constant = Self::FILENAME_EN;
        if($language == 'tl')
            $filename_constant = Self::FILENAME_TL;

        $filename = sprintf(
            $filename_constant,
            $companyname.'-'.date('Y-m-d'));
        
        $fname = $filename.'.pdf';
        if(strpos($fname,"&") !== false) {
           $fname = preg_replace("/[&]/", "And", $fname);
        }
        
        return $this->getFilePath().$fname;
    }

    public static function financialSustainability($financialReport){
        $balance_sheets = $financialReport->balanceSheets()->get();
        $financial_table = [];
        
        $i = 0;
        foreach($balance_sheets as  $sheets){
            if( $sheets->CurrentAssets == "0.00" &&
                $sheets->NoncurrentAssets == "0.00" &&
                $sheets->CurrentLiabilities == "0.00" &&
                $sheets->NoncurrentLiabilities == "0.00" &&
                $sheets->Equity == "0.00" &&
                $sheets->Liabilities == "0.00" &&
                $sheets->EquityAndLiabilities == "0.00"
               ){
            }
            else{
                $FinancialLeverage = ($sheets->Equity == 0) ? 0 : (($sheets->CurrentLiabilities + $sheets->NoncurrentLiabilities) / $sheets->Equity);
                $DebtRatio = (($sheets->CurrentLiabilities + $sheets->NoncurrentLiabilities) / ($sheets->CurrentAssets + $sheets->NoncurrentAssets));
                $LTDtoE = ($sheets->Equity == 0) ? 0 : ($sheets->NoncurrentLiabilities  / $sheets->Equity);
                $NCAtoNW = ($sheets->Equity == 0) ? 0 : ($sheets->NoncurrentAssets / $sheets->Equity);
                $Capitalization = (($sheets->NoncurrentLiabilities + $sheets->Equity) == 0) ? 0 : ($sheets->NoncurrentLiabilities / ($sheets->NoncurrentLiabilities + $sheets->Equity));
                $FAtoNW = ($sheets->Equity == 0) ? 0 : (($sheets->PropertyPlantAndEquipment + $sheets->InvestmentProperty +  $sheets->NoncurrentBiologicalAssets)  / $sheets->Equity);
                
                $CLiabilityRatio = ($sheets->Liabilities == 0) ? 0 : ($sheets->CurrentLiabilities / $sheets->Liabilities);
                $currentLiabilitiesPercentage = ($sheets->Liabilities == 0) ? 0 : (($sheets->CurrentLiabilities * 100) / $sheets->Liabilities);
                $nonCurrentLiabilitiesPercentage =  ($sheets->Liabilities == 0) ? 0 : (($sheets->NoncurrentLiabilities * 100) / $sheets->Liabilities);

                $NWC = $sheets->CurrentAssets - $sheets->CurrentLiabilities; 
                $Inventories = $sheets->Inventories; // + $sheets->NoncurrentInventories;
                $WCD = $NWC - $Inventories;
                $InventoryNWC = ($NWC == 0) ? 0 : $Inventories / $NWC;

                $financial_table[] = (object) [
                    'year'                  => $sheets->Year,
                    'FinancialLeverage'     => round($FinancialLeverage, 2),
                    'DebtRatio'             => round($DebtRatio, 2),
                    'DebtRatio_normalValue' => $DebtRatio,
                    'LTDtoE'                => round($LTDtoE, 2),
                    'NCAtoNW'               => round($NCAtoNW,2),
                    'Capitalization'        => round($Capitalization,2),
                    'FAtoNW'                => round($FAtoNW,2),
                    'CLiabilityRatio'       => round($CLiabilityRatio, 2),
                    'currentLiabilitiesPercentage'  => round($currentLiabilitiesPercentage, 1),
                    'nonCurrentLiabilitiesPercentage' => round($nonCurrentLiabilitiesPercentage, 1),
                    'debtRatioPercentage' => round($DebtRatio * 100, 1, PHP_ROUND_HALF_DOWN),
                    'NWC'                   => round($NWC, 2),
                    'Inventories'           => round($Inventories, 2),
                    'WCD'                   => round($WCD, 2),
                    'InventoryNWC'          => round($InventoryNWC,2)
                ];
            }
        }

        usort($financial_table, function ($a, $b) {
            $t1 = strtotime($a->year);
            $t2 = strtotime($b->year);
            return $t1 - $t2;
        });

        $highest_column = count($financial_table)-1;
        $FinancialLeverage_change = 0;
        $DebtRatio_change = 0;
        $LTDtoE_change = 0;
        $NCAtoNW_change = 0;
        $Capitalization_change = 0;
        $FAtoNW_change = 0;
        $CLiabilityRatio_change = 0;

        if($financial_table){
            $FinancialLeverage_change = $financial_table[$highest_column]->FinancialLeverage - $financial_table[0]->FinancialLeverage;
            $DebtRatio_change = $financial_table[$highest_column]->DebtRatio - $financial_table[0]->DebtRatio;
            $LTDtoE_change = $financial_table[$highest_column]->LTDtoE - $financial_table[0]->LTDtoE;
            $NCAtoNW_change = $financial_table[$highest_column]->NCAtoNW - $financial_table[0]->NCAtoNW;
            $Capitalization_change = $financial_table[$highest_column]->Capitalization - $financial_table[0]->Capitalization;
            $FAtoNW_change = $financial_table[$highest_column]->FAtoNW - $financial_table[0]->FAtoNW;
            $CLiabilityRatio_change = $financial_table[$highest_column]->CLiabilityRatio - $financial_table[0]->CLiabilityRatio;
        }

        $financial_change = array(
            'FinancialLeverage_change'     => round($FinancialLeverage_change, 2),
            'DebtRatio_change'             => round($DebtRatio_change, 2),
            'LTDtoE_change'                => round($LTDtoE_change, 2),
            'NCAtoNW_change'               => round($NCAtoNW_change,2),
            'Capitalization_change'        => round($Capitalization_change,2),
            'FAtoNW_change'                => round($FAtoNW_change,2),
            'CLiabilityRatio_change'       => round($CLiabilityRatio_change, 2),
        );

        $data = array(
            'financial_table'   => $financial_table,
            'financial_change'  => $financial_change
        );
        
        return $data;
    }

    
    public static function turnoverRatios($financialReport){
        $balance_sheets = $financialReport->balanceSheets()->get();
        $incomeStatements = $financialReport->incomeStatements()->get();

        $limit = 0;
        foreach($balance_sheets as $key => $sheets){
            if( $sheets->NoncurrentAssets == "0.00" &&
                $sheets->CurrentAssets == "0.00" &&
                $sheets->Equity == "0.00" &&
                $sheets->NoncurrentLiabilities == "0.00" &&
                $sheets->CurrentLiabilities == "0.00" &&
                $sheets->Liabilities == "0.00" &&
                $sheets->EquityAndLiabilities == "0.00"
            ){

            }
            else{
                $limit += 1;
            }
        }
        $turnoverRatios = [];
        foreach($balance_sheets as $key => $sheets){
            /** Get number of days */
            $income_cnt = 0;
            $num_days=0; 
            $year=$sheets->Year;
            for( $month=1; $month<=12; $month++){ 
                $num_days = $num_days + cal_days_in_month(CAL_GREGORIAN,$month,$year);
            }

            foreach($incomeStatements as $cnt => $income){

                if($income->Year === $sheets->Year){
                    $income_cnt = $cnt;
                    }
            }

            foreach($balance_sheets as $b){
                if($b->Year == ($sheets->Year -1)){
                    /** Caclculate Receivables turnover (days sales outstanding) (ReceivablesTurnover) */
                    $ReceivablesTurnover = 0;
                    if( ($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $ReceivablesTurnover = ((($sheets->TradeAndOtherCurrentReceivables + $b->TradeAndOtherCurrentReceivables)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Accounts payable turnover (days payable outstanding) (PayableTurnover) */
                    $PayableTurnover = 0;
                    if(( ($incomeStatements[$income_cnt]->CostOfSales  + $incomeStatements[$income_cnt]->CostOfSales) / $num_days ) != 0){
                        $PayableTurnover = (($sheets->TradeAndOtherCurrentPayables + $sheets->CurrentProvisionsForEmployeeBenefits + $b->TradeAndOtherCurrentPayables + $b->CurrentProvisionsForEmployeeBenefits)/2)
                                        / (( (($incomeStatements[$income_cnt]->CostOfSales  + $sheets->Inventories) - $b->Inventories) / $num_days ));
                    }

                    /** Inventory turnover (days inventory outstanding) (InventoryTurnover) */
                    $InventoryTurnover = 0;
                    if(($incomeStatements[$income_cnt]->CostOfSales / $num_days) != 0){
                        $InventoryTurnover = ((($sheets->Inventories + $b->Inventories)/2) / ($incomeStatements[$income_cnt]->CostOfSales / $num_days));
                    }

                    /** Calculate Asset turnover (days) (AssetTurnover) */
                    $AssetTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $AssetTurnover = ((($sheets->Assets + $b->Assets)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Calculate Current asset turnover (days) (CAssetTurnover) */
                    $CAssetTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $CAssetTurnover = ((($sheets->CurrentAssets + $b->CurrentAssets)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    } 

                    /** Calculate Capital turnover (days) (CapitalTurnover) */
                    $CapitalTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $CapitalTurnover = ((($sheets->Equity + $b->Equity)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Calculate Cash conversion cycle (CCC) */
                    $CCC = 0;
                    $CCC = (($InventoryTurnover + $ReceivablesTurnover) - $PayableTurnover);

                    $turnoverRatios[] = (object) [
                        'year'                  => round($sheets->Year),
                        'num_days'              => $num_days,
                        'ReceivablesTurnover'   => round($ReceivablesTurnover),
                        'PayableTurnover'       => round($PayableTurnover),
                        'InventoryTurnover'     => round($InventoryTurnover),
                        'AssetTurnover'         => round($AssetTurnover),
                        'CAssetTurnover'        => round($CAssetTurnover),
                        'CapitalTurnover'       => round($CapitalTurnover),
                        'CCC'                   => round($CCC),
                    ];
                }
            }

            if($key == $limit-2){
                break;
            }
        }
        usort($turnoverRatios, function ($a, $b) {
            $t1 = strtotime($a->year);
            $t2 = strtotime($b->year);
            return $t1 - $t2;
        });
        return $turnoverRatios;
    }

}