<?php

namespace CreditBPO\Services\Report\PDF;

use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\SME;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;

class RatingAnalysisInternal extends PDF {
    const FILENAME = 'CreditBPO Rating Report and Analysis - %d.pdf';
    const TEMPLATE = 'rating-report.analysis.main';

    public function create($entityId) {
        set_time_limit(0);

        $sme = new SME();
        $fin = new FinancialAnalysisInternal();

        $financialReport = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        $smeData = $sme->create($entityId)->getData();
        $this->setData($smeData);

        $finData = $fin->create($financialReport->id)->getData();
        $this->setData($finData);

        $this->setFilename(sprintf(self::FILENAME, $entityId));
        return $this;
    }

    public function getTemplate() {
        return self::TEMPLATE;
    }

    public function getReportFilepath($entityId) {
        $filename = sprintf(self::FILENAME, $entityId);
        return $this->getFilepath().$filename;
    }
}
