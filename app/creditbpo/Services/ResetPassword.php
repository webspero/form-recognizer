<?php

namespace CreditBPO\Services;

use CreditBPO\Models\User;
use CreditBPO\Models\PasswordRequest;
use CreditBPO\DTO\ResetPasswordDTO;
use Mail;
use Validator;

class ResetPassword
{
    public function __construct()
    {
        $this->userDb = new User();
        $this->passwordRequestDb = new PasswordRequest();
    }

    public function validateEmail($request)
    {
        /* Get user details in database*/
        $user = $this->userDb->getUserByEmail($request['email']);
        if(!is_null($user)){
            $resetPasswordDto = new ResetPasswordDTO;
            $resetPasswordDto->setUserId($user['loginid']);
            $resetPasswordDto->setEmail($user['email']);
            return $resetPasswordDto;
        } else{
            return null;
        }
    }

    public function sendEmailRequestResetPassword($resetPasswordDto)
    {
        $resetPassword = array();
        $resetPassword['code'] = STR_EMPTY;
        /*Create notification code */
        for ($i = 0; PWORD_MIN_LEN > $i; $i++) {
            $resetPassword['code'] .= chr(rand(97, 122));
        }

        /*Save password request to database */
        $resetPassword['userid'] = $resetPasswordDto->getUserId();
        $resetPassword['daterequested'] = date('Y-m-d');
        $resetPassword['dateused'] = date('Y-m-d');
        $saved = $this->passwordRequestDb->addPasswordRequest($resetPassword);
        
        if ($saved){
            /*Send email to user*/
            if (env("APP_ENV") != "local"){

                try{
                    Mail::send('emails.forgot-password',
                            array(
                                'notif_code'=>$resetPassword['code']
                            ), function($message) use ($resetPasswordDto) {
                                $message->to($resetPasswordDto->getEmail(), $resetPasswordDto->getEmail())
                                ->subject('CreditBPO reset password notification.');
                    });
                }catch(Exception $e){
                //
                }

            }
            return SUCCESS;
        } 
        return FAILED;
    }

    public function validateRequest($resetPasswordDto)
    {
        /* Sets Validation Messages */
        $messages = array(
            'email.required' => 'Email address is required.',
            'code.required' => 'Notification code is required.',
            'password.required' => 'Password is required.',
            'confirmpassword.required' => 'Confirm password is required.',
        );

        /* Sets Validation Rules */
        $rules = array(
            'email' => 'required|email',
            'code' => 'required',
            'password' => 'required',
            'confirmpassword' => 'required|min:6'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($resetPasswordDto->getVars(), $rules, $messages);
        return $validator;
    }

    public function validateResetPasswordDetails($resetPasswordDto)
    {
        /*Validate Email */
        $user = $this->userDb->getUserByEmail($resetPasswordDto->getEmail());
        if (is_null($user)){
            return 'The email you entered is not a registered user.';
        }
        $resetPasswordDto->setUserId($user->loginid);
        /* Validate Code */
        $code = $this->passwordRequestDb->userPasswordRequest($resetPasswordDto->getUserId(), $resetPasswordDto->getCode());

        if (!is_null($code)){
            $resetPasswordDto->setPasswordRequestId($code->request_id);
        }

        /*Return error message*/
        if (@count($code) <= 0) {
            return 'The notification code you entered was invalid.';
        } else{
            if ($code->status == PASSWORD_REQ_COMPLETE){
                return 'The notification code you entered was already used. Please check your email or request a new one.';
            }
        }

        /*Validate Password */
        if ($resetPasswordDto->getPassword() != $resetPasswordDto->getConfirmPassword()){
            return 'New Password and Confim password doesn\'t match';
        }

        return null;
    }

    public function changePassword($resetPasswordDto)
    {
        /*Update user password*/
        $userResult = $this->userDb->updateUserPassword($resetPasswordDto);
        if ($userResult){
            /*Update request reset password details */
            $codeResult = $this->passwordRequestDb->updateNotificationStatus($resetPasswordDto->getPasswordRequestId());
            if ($codeResult){
                return true;
            }
        }
        return false;
    }

    public function checkPasswordRequest($email)
    {
        /*Get password request data based on email address */
        $passwordRequest = $this->passwordRequestDb->getPasswordRequestByEmail($email);
        if (!empty($passwordRequest)){
            if ($passwordRequest['req_status'] == PASSWORD_REQ_PENDING){
                return true;
            }
        }
        return false;
    }
}