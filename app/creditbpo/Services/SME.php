<?php

namespace CreditBPO\Services;

use CreditBPO\Services\Report\PDF\SME as SMEPDF;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Entity;
use CreditBPO\Models\Bank;

use DB;

class SME {
    /**
     * @param int $entityId
     * @return bool
     */
    public function createReport($entityId) {
        $pdf = new SMEPDF();
        $contents = $pdf->create($entityId)->generate();

        if ($contents) {
            $pdf->save();
            return $pdf->saveFileToDB($entityId);
        }

        return false;
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function getReportVars($entityId) {
        $pdf = new SMEPDF();
        $pdf->create($entityId);

        return $pdf->getData();
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function getReportEntityData($entityId) {


        $businessDriver = DB::table('tblbusinessdriver')
            ->where('entity_id', $entityId)
            ->get();
        $businessSegment = DB::table('tblbusinesssegment')
            ->where('entity_id', $entityId)
            ->get();
        $entity = DB::table('tblentity')
        	->leftJoin('tblbank','tblbank.id','=','tblentity.current_bank')
            ->where('entityid', $entityId)
            ->first();
        $futureGrowth = DB::table('tblfuturegrowth')
            ->where('entity_id', '=', $entityId)
            ->get();
        $industryMain = DB::table('tblindustry_main')
            ->where('main_code', $entity->industry_main_id)
            ->first();

        if(empty($industryMain->industry_main_id)){
        	$industryMain = DB::table('tblindustry_main')
            ->where('industry_main_id', $entity->industry_main_id)
            ->first();
        }

        $industryRow = DB::table('tblindustry_group')
            ->where('group_code', $entity->industry_row_id)
            ->first();
        $industrySub = DB::table('tblindustry_sub')
            ->where('sub_code', $entity->industry_sub_id)
            ->first();
        $pastProjects = DB::table('tblprojectcompleted')
            ->where('entity_id', $entityId)
            ->get();
        $planFacilities = DB::table('tblplanfacilityrequested')
            ->where('entity_id', $entityId)
            ->get();
        $readyRatio = DB::table('tblreadyrationdata')
            ->where('entityid', $entityId)
            ->orderBy('readyrationdataid', 'desc')
            ->first();
        $revenueGrowthPotential = DB::table('tblrevenuegrowthpotential')
            ->where('entity_id', $entityId)
            ->get();
        $smeScore = DB::table('tblsmescore')
            ->where('entityid', $entityId)
            ->orderBy('smescoreid', 'desc')
            ->first();
        $financialReport = FinancialReport::where(['entity_id' => $entity->entityid, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();
        $region = Entity::where('entityid', $entityId)
            ->leftJoin('zipcodes', 'tblentity.cityid', '=', 'zipcodes.id')
            ->first();

        if ($entity->current_bank != 0) {
            $bankStandalone = Bank::find($entity->current_bank)->is_standalone;
        } else {
            $bankStandalone = 0;
        }

        return [
            'bankStandalone' => $bankStandalone,
            'businessDriver' => $businessDriver,
            'businessSegment' => $businessSegment,
            'entity' => $entity,
            'financialReport' => $financialReport,
            'futureGrowth' => $futureGrowth,
            'industryMain' => $industryMain,
            'industryRow' => $industryRow,
            'industrySub' => $industrySub,
            'pastProjects' => $pastProjects,
            'planFacilities' => $planFacilities,
            'smeScore' => $smeScore,
            'readyRatio' => $readyRatio,
            'region' => $region,
            'revenueGrowthPotential' => $revenueGrowthPotential,
        ];
    }
}
