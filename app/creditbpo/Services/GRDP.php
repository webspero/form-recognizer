<?php

namespace CreditBPO\Services;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;
use CreditBPO\Services\BaseCommand;
use PHPExcel_IOFactory;
use GRDP as Model;
use DB;

class GRDP extends BaseCommand {
    protected $config;

    public function __construct($command) {
        $this->config = Config::get('cron/grdp');
        parent::__construct($command);
    }

    public function updateGRDP($years = []) {
        if (@count($years) <= 0) {
            return false;
        }

        $latestYear = Model::getLatestYear();

        foreach ($years as $index => $regions) {
            if ($index <= $latestYear) {
                continue;
            }

            DB::beginTransaction();
            if (!$this->processRegionPerYear($index, $regions)) {
                DB::rollback();
            } else {
                DB::commit();
            }
        }
    }

    protected function processRegionPerYear($year, $regions) {
        $this->commandLog('Processing year '.$year."\n");

        foreach ($regions as $index => $grdp) {
            $model = new Model();
            $model->region = $this->getCorrectRegionName($index);
            $model->year = $year;
            $model->grdp = $grdp;
            $model->save();
        }

        return true;
    }

    protected function getCorrectRegionName($name) {
        $regions = $this->config['regions'];
        $name = trim($name);

        if (isset($regions[$name])) {
            return $regions[$name];
        }

        return $name;
    }

    public function scrape() {
        $grdpFile = $this->tempDirectory.'/'.$this->config['file'];

        // Re-attempt to use already downloaded file in case of previous errors
        if (file_exists($grdpFile)) {
            $this->commandLog('Reusing already downloaded file'."\n");
            return $this->processDownloadedFile($grdpFile);
        }

        $baseUrl = $this->config['base_url'];
        $page = $this->scraper->fetch($baseUrl);

        if (!$page) {
            $errorMessage = $this->getErrorMessage(BaseCommand::REASON_TIMED_OUT);
            $this->commandError($errorMessage);
            return false;
        }

        $attrTitle = $this->config['title'];
        $contents = $page->getBody()->getContents();
        $domDocument = $this->scraper->getDomDocument($contents);
        $anchorTag = $this->scraper->getNode(
            $domDocument,
            '//span[@class="file"]/a[@title="'.$attrTitle.'"]');

        if (!$anchorTag) {
            $errorMessage = $this->getErrorMessage(
                BaseCommand::REASON_DOWNLOAD_LINK_MISSING
            );
            $this->commandError($errorMessage);
            return false;
        }

        $href = $anchorTag->getAttribute('href');
        $download = $this->scraper->fetch($href, ['sink' => $grdpFile]);

        return $this->processDownloadedFile($grdpFile);
    }

    protected function getFilteredValue($value) {
        return trim(strtolower($value));
    }

    protected function getNumericValues($list) {
        $returnList = [];
        $newList = array_slice($list, 2);

        foreach ($newList as $item) {
            $returnList[] = trim($item);
        }

        return $returnList;
    }

    protected function processDownloadedFile($file) {
        $reader = PHPExcel_IOFactory::createReader('CSV');
        $reader = $reader->load($file);
        $worksheet = $reader->getActiveSheet();
        $markers = $this->config['markers'];
        $startReference = $start = $markers['start_reference'];
        $years = [];
        $regions = [];

        foreach ($worksheet->getRowIterator() as $index => $row) {
            $cellIterator = $row->getCellIterator();
            $current = $cellIterator->current()->getValue();
            $current = $this->getFilteredValue($current);

            if (is_array($start) && @count($start) > 0) {
                if ($current !== $start[0] && @count($start) > 0) {
                    $start = $startReference;
                    continue;
                }

                if ($current === $start[0] && @count($start) > 0) {
                    array_shift($start);

                    if ($current === $markers['year']) {
                        $range = 'A'.$index.':'.$worksheet->getHighestColumn().$index;
                        $years = $this->getNumericValues($worksheet->rangeToArray($range)[0]);
                    }
                    continue;
                }
            }

            if (
                !$worksheet->getCell('A'.$index)->getValue() &&
                !$worksheet->getCell('B'.$index)->getValue()
            ) {
                continue;
            }

            if ($current === $markers['stop']) {
                break;
            }

            if (in_array($current, $markers['skip'])) {
                continue;
            }

            $cellIndex = trim($worksheet->getCell('B'.$index)->getValue());
            $range = 'A'.$index.':'.$worksheet->getHighestColumn().$index;
            $regions[$cellIndex] = $this->getNumericValues($worksheet->rangeToArray($range)[0]);
        }

        $yearList = [];

        foreach ($years as $yearIndex => $year) {
            $regionList = [];

            foreach ($regions as $regionIndex => $region) {
                $regionList[$regionIndex] = $region[$yearIndex] / 100;
            }

            $yearList[$year] = $regionList;
        }

        @unlink($file);

        return $yearList;
    }
}