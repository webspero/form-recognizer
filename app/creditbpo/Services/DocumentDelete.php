<?php

namespace CreditBPO\Services;

use File;
use CreditBPO\Models\Document;

class DocumentDelete
{
    CONST UPLOAD_DESTINATION_PATH = 'documents';

    public function __construct()
    {
        $this->documentDb = new Document;
    }

    public function deleteDocument($id)
    {
        $document = $this->documentDb->getDocumentById($id);
        /* Delete document to folder */
        if (!is_null($document)){
            $file = self::UPLOAD_DESTINATION_PATH.'/'.$document->document_rename;
            if (file_exists($file)){
                File::delete($file);
            }
            /* Delete data in database */
            $deleteResult = $this->documentDb->deleteDocument([$document->documentid]);
            return $deleteResult;
            
        } else{
            return null;
        }
    }
}