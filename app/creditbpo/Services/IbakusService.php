<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use CreditBPO\Libraries\Scraper;
use App\Models\Entity;
use Shareholder;
use Director;
use KeyManager;
use App\Countries;
use OpenSanction;

//======================================================================
//  Class #: Ibakus Service
//  Functions related to Crossmatch of Personalities
//======================================================================
class IbakusService 
{
    private $config;
    private $scraper;
    private $status;

    public function __construct(array $config = []) {
        if (empty($config)) {
            $config = Config::get('scraper.ibakus');
        }

        $this->setConfig($config);
        $this->scraper = new Scraper($config['client']);
        $this->getLogin();
    }

    public function setConfig(array $config = []) {
        $this->config = $config;
        return $this;
    }

    public function getConfig() {
        return $this->config;
    }

    public function getLogin() {
        $config = $this->getConfig()['login'];
        $page = $this->scraper->fetch($config['url'], $config['params'], $config['method']);

        $this->status = $page->getStatusCode();
    }

    public function searchPersonalities($params){
        $config = $this->getConfig()['person'];

        if($this->status == 201 || $this->status == 200) {
            // parameter from shareholders, key managers, directors
            foreach ($params as $key => $param) {
                $resp = $this->scraper->fetch($config['url'],$param, $config['method']);
                if($resp->getStatusCode() == 200 || $resp->getStatusCode() == 201 ) {
                    $res = $resp->getBody()->getContents();
                    $code = json_decode($res);
                    if(in_array("success", (array) $code )) {
                        // save data in negative findings if risk level is not Low
                        if(strtolower($code->result->riskLevel) != "low" && strtolower($code->result->riskLevel) != "normal") {
                            $insert = array(
                                "name"        => $param['json']['firstName'] . " " . $param['json']['lastName'],
                                "firstname"   => $param['json']['firstName'],
                                "lastname"    => $param['json']['lastName'],
                                "sex"         => ($param['json']['gender'] == "Male")? "M": "F",
                                "birth_date"  => $param['json']['birthDate'],
                                "nationality" => "['" . $param['json']['nationality'] . "']", 
                                "source"      => "https://apps.ibakus.com/api/v2/fast-check/natural-person",
                                "charges"     => "Ibakus High Risk Level",
                                "is_deleted"  => 0,
                                "created_at"  => date("Y-m-d H:i:s"),
                                "updated_at"  => date("Y-m-d H:i:s")
                            );
                            OpenSanction::insert($insert);
                        }
                    } else {
                        // do error handling here
                    }
                }  else {
                    // do error handling here
                }
            }
        } 
    }

    public function getRelatedPersonalities($id) {
        // if(env("APP_ENV") == "local") {
            return;
        // }
        
        if($this->status != 200) {
            $this->getLogin();
        }

        $where = array(
            "entity_id" => $id,
            "is_deleted" => 0
        );

        $persons = array();
        $persons[] = Shareholder::where($where)->get()->toArray();
        $persons[] = KeyManager::where($where)->get()->toArray();
        $persons[] = Director::where($where)->get()->toArray();

        $list = array();
        $params = array();
        // loop and put in list 
        foreach ($persons as $key => $value) {
            # firstname, lastname, nationality convert to us code, gender, birthdate
            foreach ($value as $val) {
                $cont = (object) $val;
                $code = Countries::where("enNationality", $cont->nationality)->first();
                $name = (isset($cont->name))? $cont->name: $cont->firstname . " " . $cont->lastname;
                if(!in_array($name, $list)){
                    $list[] = $name;
                    $arrName = array();
                    if(!$cont->firstname && !$cont->lastname && isset($cont->name)) {
                        $tmp = str_word_count($name, 1);
                        $getFname = "";
                        for($i = 0; $i < count($tmp) - 1; $i++) {
                            $getFname .= $tmp[$i] . " ";
                        }
                        $arrName[] = trim($getFname);
                        $arrName[] = end($tmp);
                    }
                    $fname = (count($arrName) > 0)? $arrName[0]: $cont->firstname;
                    $lname = (count($arrName) > 0)? $arrName[1]: $cont->lastname;
                   
                    $openSanction = OpenSanction::where("lastname", "like", "%{$lname}%")->where("nationality", "Like", "%{$code->country_code}%")->get()->toArray();
                    if($openSanction) {
                        foreach ($openSanction as $os) {
                            $ps = (object) $os;
                            $cnt1 = str_word_count($ps->firstname);
                            $cnt2 = str_word_count($fname);
                            $check = 0;
                            if($cnt1 > $cnt2) {
                                $check = stripos($ps->firstname, $fname);
                            } else {
                                $check = stripos( $fname, $ps->firstname);
                            }
                            if(!$check && $check != 0) {
                                $params[] =  array(
                                    "json" => array(
                                        "folderId"       => 149,
                                        "firstName"      => ucwords($fname),
                                        "lastName"       => ucwords($lname),
                                        "addressCountry" => $code->country_code,
                                        "gender"         => isset($cont->gender)? $cont->gender: "Male", #no gender yet
                                        "birthDate"      => ($cont->birthdate != "0000-00-00")? date("Y-m-d", strtotime($cont->birthdate)): "2002-02-02", #not yet required
                                        "nationality"    => $code->country_code
                                    )
                                );
                            }
                        }
                    } else {
                        $params[] =  array(
                            "json" => array(
                                "folderId"       => 149,
                                "firstName"      => ucwords($fname),
                                "lastName"       => ucwords($lname),
                                "addressCountry" => $code->country_code,
                                "gender"         => isset($cont->gender)? $cont->gender: "Male", #no gender yet
                                "birthDate"      => ($cont->birthdate != "0000-00-00")? date("Y-m-d", strtotime($cont->birthdate)): "2002-02-02", #not yet required
                                "nationality"    => $code->country_code
                            )
                        );
                    }
                }
            }
        }
        if($params) {
            $this->searchPersonalities($params);
        }
        
        return;
    }
}