<?php

namespace CreditBPO\Services;

class AssetGrouping {
    public static function getAll() {
        return [
            [
                'id' => '4',
                'label' => trans('business_details1.tag_micro'),
            ],
            [
                'id' => '1',
                'label' => trans('business_details1.tag_small'),
            ],
            [
                'id' => '2',
                'label' => trans('business_details1.tag_med'),
            ],
            [
                'id' => '3',
                'label' => trans('business_details1.tag_large'),
            ],
        ];
    }

    public static function getByAmount($amount) {
        $list = self::getAll();

        if ($amount > 100000000) {
            return $list[3];
        }

        if ($amount > 15000000) {
            return $list[2];
        }

        if ($amount > 3000000) {
            return $list[1];
        }

        return $list[0];
    }
}