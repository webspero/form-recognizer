<?php

namespace CreditBPO\Services;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Config;
use CreditBPO\Libraries\Scraper;
use CreditBPO\Libraries\MathHelper;
use CreditBPO\Exceptions\MathException;
use CreditBPO\Commands\ForecastingDataUpdate as ForecastingDataUpdateCommand;
use PHPExcel_IOFactory;
use Industrymain;

class ForecastingDataUpdate {
    const REASON_NO_CONFIG = 1;
    const REASON_TIMED_OUT = 2;
    const REASON_DOWNLOAD_LINK_MISSING = 3;
    const REASON_DOWNLOAD_TIMED_OUT = 4;
    const REASON_FILE_NOT_DOWNLOADED = 5;
    const QUARTER_COUNT = 8;

    protected $scraper;
    protected $command;

    public function __construct(ForecastingDataUpdateCommand $command = null) {
        $this->tempDirectory = public_path().'/temp/';;
        $this->scraper = new Scraper();

        if ($command) {
            $this->command = $command;
        }
    }

    public function autoUpdate() {
        $industryValues = $this->getIndustryQuarterValues();
        return true;
    }

    public function commandLog($message) {
        if ($this->command) {
            $this->command->info($message);
        }
    }

    public function commandError($message) {
        if ($this->command) {
            $this->command->error($message);
        }
    }

    /**
     * @param array $industries
     * @return bool
     */
    public function updateIndustryQuarterValues(array $industries) {
        $allIndustry = Industrymain::all();

        foreach ($allIndustry as $key => $industry) {
            $index = $key + 1;
            if (
                !isset($industries[$index]) ||
                !isset($industries[$index]['values'])
            ) {
                continue;
            }

            $this->commandLog('ID: '. $index . ' Name: ' . $industries[$index]['name'] ."\n");

            $values = $industries[$index]['values'];
            $industry->q1 = $values[0];
            $industry->q2 = $values[1];
            $industry->q3 = $values[2];
            $industry->q4 = $values[3];
            $industry->q5 = $values[4];
            $industry->q6 = $values[5];
            $industry->q7 = $values[6];
            $industry->q8 = $values[7];

            try {
                $result = MathHelper::linearRegression(range(1, 8), $industries[$index]['values']);
                $industry->slope = $result['m'] / $industry->q8;
            } catch (MathException $e) {
                $industry->slope = 0;
            }

            $industry->save();
        }
    }

    /**
     * @param string $sectorPage
     * @param string $reason
     * @return string
     */
    protected function getErrorMessage($sectorPage, $reason) {
        $reasonMessage = '';

        switch ($reason) {
            case self::REASON_NO_CONFIG:
                $reasonMessage = 'No config present';
            break;
            case self::REASON_TIMED_OUT:
                $reasonMessage = 'Request timed out for sector page';
            break;
            case self::REASON_DOWNLOAD_LINK_MISSING:
                $reasonMessage = 'Download link not found';
            break;
            case self::REASON_DOWNLOAD_TIMED_OUT:
                $reasonMessage = 'Request timed out when downloading file';
            break;
            case self::REASON_FILE_NOT_DOWNLOADED:
                $reasonMessage = 'File not downloaded';
            break;
            default:
                $reasonMessage = 'Unknown error has occured';
            break;
        }

        return sprintf("Unable to set sector %s. Reason: %s.\n", $sectorPage, $reasonMessage);
    }

    protected function processDownloadedFile($file, $start, $end) {
        $fileType = PHPExcel_IOFactory::identify($file);
        $reader = PHPExcel_IOFactory::createReader($fileType);
        $reader = $reader->load($file);
        $worksheet = $reader->getActiveSheet();
        $arrayRow = [];
        $startProcess = false;

        foreach ($worksheet->getRowIterator() as $row) {
            $cellCounter = 0;
            $cellIterator = $row->getCellIterator();
            $current = $cellIterator->current()->getValue();
            $cellIterator->setIterateOnlyExistingCells(true);

            if (trim($current) !== $start && !$startProcess) {
                continue;
            }

            $startProcess = true;

            foreach ($cellIterator as $key => $cell) {
                if ($key === 'A') {
                    continue;
                }

                $value = trim($cell->getValue());
                $currentArrayValue = trim(end($arrayRow));

                // stop when two consecutive empty cells were met
                if (!$value && !$currentArrayValue) {
                    unset($arrayRow[@count($arrayRow) - 1]);
                    break;
                }

                if (!isset($arrayRow[$cellCounter])) {
                    $arrayRow[$cellCounter] = null;
                }

                if (is_numeric($value)) {
                    $arrayRow[$cellCounter] += (float)$value;
                } else {
                    $arrayRow[$cellCounter] = $value;
                }

                $cellCounter++;
            }

            if (trim($current) === $end && $startProcess) {
                break;
            }
        }

        $arrayRow = $this->getRequiredValues($arrayRow);
        @unlink($file);
        return $arrayRow;
    }

    public function scrape() {
        $config = Config::get('cron.forecasting-data-update');
        $baseUrl = $config['base_url'];
        $sectors = $config['sectors'];
        $industrySectors = [];

        foreach ($sectors as $key => $sector) {
            $this->commandLog(
                "\n".'Sector: '.$sector['name']. "\n".
                str_repeat('=', strlen($sector['name']))."\n\n");

            if (!isset($sector['start']) || !isset($sector['page'])) {
                $errorMessage = $this->getErrorMessage(
                    $sector['name'], self::REASON_NO_CONFIG
                );
                $this->commandError($errorMessage);
                continue;
            }

            $industrySectors[$key] = [
                'name' => $sector['name'],
                'quarters' => null,
                'intercept' => 0,
            ];

            $sectorFile = $this->tempDirectory.$sector['file'];
            $startMarker = $sector['start'];
            $endMarker = isset($sector['end']) ? $sector['end'] : $sector['start'];

            if (file_exists($sectorFile)) {
                $this->commandLog('Reusing already downloaded file'."\n");
                $industrySectors[$key]['values'] = $this->processDownloadedFile(
                    $sectorFile,
                    $startMarker,
                    $endMarker
                );
                continue;
            }

            $page = $this->scraper->fetch($baseUrl.$sector['page']);

            if (!$page) {
                $errorMessage = $this->getErrorMessage(
                    $sector['name'], self::REASON_TIMED_OUT
                );
                $this->commandError($errorMessage);
                continue;
            }

            $contents = $page->getBody()->getContents();
            $domDocument = $this->scraper->getDomDocument($contents);
            $anchorTag = $this->scraper->getNode(
                $domDocument,
                '//span[@class="file"]/a');

            if (!$anchorTag) {
                $errorMessage = $this->getErrorMessage(
                    $sector['name'], self::REASON_DOWNLOAD_LINK_MISSING
                );
                $this->commandError($errorMessage);
                continue;
            }

            $href = $anchorTag->getAttribute('href');
            $download = $this->scraper->fetch($href, ['sink' => $sectorFile]);

            if (!$download) {
                $errorMessage = $this->getErrorMessage(
                    $sector['name'], self::REASON_DOWNLOAD_TIMED_OUT
                );
                $this->commandError($errorMessage);
                continue;
            }

            if (!file_exists($sectorFile)) {
                $errorMessage = $this->getErrorMessage(
                    $sector['name'], self::REASON_FILE_NOT_DOWNLOADED
                );
                continue;
            }

            $industrySectors[$key]['values'] = $this->processDownloadedFile(
                $sectorFile,
                $startMarker,
                $endMarker
            );
        }

        return $industrySectors;
    }

    /**
     * Return index of array item that has a null or empty string value
     * @param string $needle
     * @param array $haystack
     * @return int|null
     */
    protected function getLastIndexByValue($needle, $haystack) {
        $index = null;

        foreach ($haystack as $key => $value) {
            if ($value === $needle) {
                $index = $key;
            }
        }

        return $index;
    }

    /**
     * Remove array items with null values supplied by end marker
     *
     * @param array $values
     */
    protected function getRequiredValues($values) {
        $index = $this->getLastIndexByValue('', $values);
        $removedAnnualValues = array_slice($values, 0, $index);
        $filtered = [];

        foreach ($removedAnnualValues as $item) {
            if ($item) {
                $filtered[] = (float)$item;
            }
        }

        return array_slice($filtered, -self::QUARTER_COUNT);
    }
}
