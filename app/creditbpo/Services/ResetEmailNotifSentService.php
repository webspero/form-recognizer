<?php

namespace CreditBPO\Services;

use User;
use Mail;


use DB;

class ResetEmailNotifSentService extends BaseCommand {
    protected $config;

    public function __construct($command) {
        parent::__construct($command);
    }

    public function resetNotifications() {
        $emailList = DB::table('tblemailnotif')->get();

        foreach($emailList as $email){
            DB::table('tblemailnotif')->where('email_notif_id', $email->email_notif_id)->update(['notif_reports_started_sent' => 0]);
            DB::table('tblemailnotif')->where('email_notif_id', $email->email_notif_id)->update(['notif_reports_completed_sent' => 0]);
        }
    }
}
