<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Exceptions\InvalidCellException;
use CreditBPO\Exceptions\FormInputException;
use BalanceSheet;
use CashFlow;
use Currency;
use CurrencyRate;
use DB;
use Entity;
use FinancialReport;
use IncomeStatement;
use Auth;
use \Exception as Exception;

class FsInput {
    const DEFAULT_STEP = 12;
    const MINIMUM_CURRENT_ASSETS = 2;
    const FILENAME = 'Basic FS Input Template - %s as of %s';

    protected $additionalLines = [];
    protected $balanceSheetValues = [];
    protected $cashFlowValues = [];
    protected $config;
    protected $data = [];
    protected $documentGenerated;
    protected $errorMessages = [];
    protected $excel;
    protected $filename;
    protected $filepath;
    protected $incomeStatementValues = [];
    protected $sheet;
    protected $totalAssets;
    protected $companyName;
    protected $currencyRate = 1;

    public function __construct($filePath, $data = []) {
        $this->excel = Excel::load($filePath);
        $this->excel->setActiveSheetIndex(0);
        $this->sheet = $this->excel->getActiveSheet();
        $this->setData($data);
        $this->setFilename($filePath);
        $this->setFilePath($filePath);
    }

    public function setFilename($path) {
        $this->filename = basename($path);
    }

    public function getFilename() {
        return $this->filename;
    }

    public function setFilePath($path) {
        $this->filepath = $path;
    }

    public function getFilePath() {
        return $this->filepath;
    }

    public function setConfig($config = []) {
        $this->config = $config;
        return $this;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setData($data) {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    public function getData() {
        return $this->data;
    }

    public function setErrorMessage($message) {
        $this->errorMessages[] = $message;
        return $this;
    }

    public function getErrorMessages() {
        return $this->errorMessages;
    }

    public function getTranslatedMessage($messageKey, $data = []) {
        return trans($messageKey, $data);
    }

    public function save() {
        DB::beginTransaction();

        try {
            $this->verifyTemplate();

            if (!$this->verifyInput()) {
                return false;
            }

            $names = $this->excel->getSheetNames();

            $config = $this->getConfig();
            $sheet = $config['sheet'];

            if(in_array($sheet['data_entry'], $names) === false) {
                throw new FormInputException(
                    $this->getTranslatedMessage('fs_input.data_entry_required')
                ); 
            }

            // Check currency rate
            $data = $this->getData();
            $iso_code = $data['currency'];
            $latestYear = $this->getCellStringValue('B6');
    
            // check id id there's available currency rate for the country and specific year and country
            if($iso_code == 'PHP' || $data['country_code'] === ""){
                $this->currencyRate = 1;
            }else{
                $currencyRate = CurrencyRate::where('iso_code', $iso_code)->where('year', $latestYear)->pluck('php_per_unit')->first();
                $this->currencyRate = $currencyRate;
    
                // If currency rate not found return
                if(!$currencyRate){
                    $this->setErrorMessage("No available currency rate");
                    return;
                }

                // Convert again sheet files
                if (!$this->verifyInput()) {
                    return false;
                }
            }

            if(in_array($sheet['additional_lines'], $names) === false) {
                $this->deleteOldFSInput();
                $this->createFinancialReport();
                $this->createBalanceSheet();
                $this->createIncomeStatement();
                $this->createCashFlow();
                $this->setTotalAssets();
                DB::commit();
    
                return true;
            }else{
                $this->deleteOldFSInput();
                $this->createFinancialReport();
                $this->createBalanceSheet();
                $this->createIncomeStatement();
                $this->createCashFlow();
                $this->createAdditionalLines();
                $this->setTotalAssets();
                DB::commit();
    
                return true;
            }

        } catch (InvalidCellException $e) {
            $message = $e->getMessage();
        } catch (FormInputException $e) {
            $message = $e->getMessage();
        }

        DB::rollback();
        Log::error($message);
        $this->setErrorMessage($message);
        return false;
    }

    public function getCellStringValue($cell, $caseSensitive = true) {
        $value = (string) $this->sheet->getCell($cell)->getValue();

        if (!$caseSensitive) {
            $value = strtolower($value);
        }

        return trim($value);
    }

    public function getCellCalculatedValue($cell, $setNullAsZero = false) {
        $value = $this->sheet->getCell($cell)->getCalculatedValue();

        /** Check special character on cell value */
        if( (!preg_match('/[^a-zA-Z0-9_+\-\/*%&]*$/', $value))){
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.special_character_found',
                ['column' => $cell])
            );
        }

        if ($setNullAsZero && !$value) {
            $value = 0;
        }

        if(in_array($cell, array('B6', 'C6' , 'D6', 'E6'), true)){
            //
        }else{
            if(!$value || !is_numeric($value)){
                $value = 0;
            }
            $value = round(($value * $this->currencyRate),2);
        }

        return $value;
    }

    protected function verifyTemplate() {
        $company_name = $this->getCellStringValue('B2');

        $config = $this->getConfig();
        $sheetLabels = $config['validation']['labels']['data_entry'];

        /* check input fields with special characters */
        $sheetFields = array ('B', 'C', 'D', 'E', 'H', 'I', 'J');
        foreach($sheetFields as $sheets){
            if($sheets == 'B' || $sheets == 'C' || $sheets == 'D' || $sheets == 'E'){
                for($count=10; $count<=75; $count++){
                    if(($count>19&&$count<22) || ($count>36&&$count<38)  || ($count>38&&$count<43) || ($count>49&&$count<50) || ($count>50&&$count<53) || ($count>60&&$count<63) ||  ($count>70&&$count<72) || ($count>72&&$count<74) || $count>74 ){
                        continue;
                    }
                    $field = $sheets.$count;

                    if(is_string($this->getCellCalculatedValue($field, false))){
                        throw new InvalidCellException(
                            $this->getTranslatedMessage('fs_input.input_numeric',
                            ['column' => $field])
                        );
                    }
                }
            }
            if($sheets == 'H' || $sheets == 'I' || $sheets == 'J'){
                for($count=8; $count<=55; $count++){
                    if( ($count<8&&$count<10) || ($count>10&&$count<12) || ($count>17&&$count<19) || ($count>30&&$count<32) || ($count>33&&$count<35) || ($count>36&&$count<38) || ($count>39&&$count<43) || $count>53 ){
                        continue;
                    }
                    $field = $sheets.$count;
                    if(is_string($this->getCellCalculatedValue($field, false))){
                        throw new InvalidCellException(
                            $this->getTranslatedMessage('fs_input.input_numeric',
                            ['column' => $field])
                        );
                    }
                }
            }
        }
    
        // check if fs input template is up to date
        foreach ($sheetLabels as $index => $label) {
            $cell = $this->getCellStringValue($index, false);

            if (strtolower($label) !== $cell) {
                Log::error('FS Input: Incorrect label for '.$index);
                throw new InvalidCellException(
                    $this->getTranslatedMessage('fs_input.template_outdated')
                );
            }
        }

        // check if company name is valid
        $sheetValues = $config['values'];
        $invalidCompanyNames = $config['validation']['invalid_company_names'];
        $companyCell = $sheetValues['financial_analysis']['company'];
        $companyName = $this->getCellStringValue($companyCell, false);

        if (empty($companyName) || in_array($companyName, $invalidCompanyNames)) {
            $entity = Entity::where('entityid', '=', $this->data['entity_id'])->first();
            $this->companyName = $entity->companyname;
        } else {
            $this->companyName = $this->getCellStringValue($companyCell);
        }

        // check if year labels are the same
        $financialYear = $sheetValues['financial_analysis']['latest_year'];
        $incomeStatementYear = $sheetValues['income_statements']['latest_year'];
        $cashFlowYear = $sheetValues['cash_flow']['latest_year'];
        $financialCell = $this->getCellCalculatedValue($financialYear);
        $incomeCell = $this->getCellCalculatedValue($incomeStatementYear);
        $cashFlowCell = $this->getCellCalculatedValue($cashFlowYear);

        if ($financialCell !== $incomeCell || $cashFlowCell !== $financialCell) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.invalid_year')
            );
        }

        // check if Current Assets is null or numeric
        $currentAssets = $sheetValues['financial_analysis']['current_assets'];
        $count = 0;
        foreach ($currentAssets as $asset) {
            if($count <= 1 && ($this->getCellCalculatedValue($asset, true) === 0 || is_string($this->getCellCalculatedValue($asset, false)) )){
                if($count == 0) {
                    throw new InvalidCellException(
                        $this->getTranslatedMessage('fs_input.current_assets_data_required',
                        ['column' => $asset])
                    );
                } else {
                    throw new InvalidCellException(
                        $this->getTranslatedMessage('fs_input.current_assets_data_required',
                        ['column' => $asset])
                    );
                }
            }
            $count++;
        }

        //check if specific balance sheet reference fields are null or numeric
        $balanceSheetReferences = $sheetValues['balance_sheet']['reference'];
        foreach ($balanceSheetReferences as $asset) {
            /** Remove validation logic as per ticket CDP-1545 */
            // if($asset == 38) { //total assets
            //     if($this->getCellCalculatedValue('B' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('B' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.total_assets_data_required',
            //             ['column' => 'B' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('C' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('C' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.total_assets_data_required',
            //             ['column' => 'C' . $asset])
            //         );
            //     } 
            // }

            // if($asset == 50) { //current liabilities
            //     if($this->getCellCalculatedValue('B' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('B' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.current_liabilities_data_required',
            //             ['column' => 'B' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('C' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('C' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.current_liabilities_data_required',
            //             ['column' => 'C' . $asset])
            //         );
            //     } 
            // }

            /** Remove validation logic as per ticket CDP-1545 */
            // if($asset == 70) { //equity
            //     if($this->getCellCalculatedValue('B' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('B' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.equity_data_required',
            //             ['column' => 'B' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('C' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('C' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.equity_data_required',
            //             ['column' => 'C' . $asset])
            //         );
            //     } 
            // }

            /** Remove validation logic as per ticket CDP-1545 */
            // if($asset == 72) { //liabilities
            //     if($this->getCellCalculatedValue('B' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('B' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.liabilities_data_required',
            //             ['column' => 'B' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('C' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('C' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.liabilities_data_required',
            //             ['column' => 'C' . $asset])
            //         );
            //     } 
            // }

            /** Remove validation logic as per ticket CDP-1545 */
            // if($asset == 74) { //equity and liabilities
            //     if($this->getCellCalculatedValue('B' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('B' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.equity_and_liabilities_data_required',
            //             ['column' => 'B' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('C' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('C' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.equity_and_liabilities_data_required',
            //             ['column' => 'C' . $asset])
            //         );
            //     } 
            // }
        }

        $balanceValue = 76;
        if( ((round($this->getCellCalculatedValue('B74'), 2) - round($this->getCellCalculatedValue('B38'), 2)) != 0) || is_string($this->getCellCalculatedValue('B' . $balanceValue, false))) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.data_zero',
                ['column' => 'B' . $balanceValue])
            );
        }
        if( ((round($this->getCellCalculatedValue('C74'), 2) - round($this->getCellCalculatedValue('C38'), 2)) != 0) || is_string($this->getCellCalculatedValue('C' . $balanceValue, false))) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.data_zero',
                ['column' => 'C' . $balanceValue])
            );
        }
        if( ((round($this->getCellCalculatedValue('D74'), 2) - round($this->getCellCalculatedValue('D38'), 2)) != 0) || is_string($this->getCellCalculatedValue('D' . $balanceValue, false))) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.data_zero',
                ['column' => 'D' . $balanceValue])
            );
        }
        if( ((round($this->getCellCalculatedValue('E74'), 2) - round($this->getCellCalculatedValue('E38'), 2)) != 0) || is_string($this->getCellCalculatedValue('E' . $balanceValue, false))) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.data_zero',
                ['column' => 'E' . $balanceValue])
            );
        }
        

        $balanceSheetReferences = $sheetValues['income_statement']['reference'];
        /** Remove validation logic as per ticket CDP-1545 */
        // foreach ($balanceSheetReferences as $asset) {
            // if($asset == 10) { //gross profit
            //     if($this->getCellCalculatedValue('H' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('H' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.gross_profit_data_required',
            //             ['column' => 'H' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('I' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('I' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.gross_profit_data_required',
            //             ['column' => 'I' . $asset])
            //         );
            //     } 
            // }

            // if($asset == 17) { //profit loss from operating activities
            //     if($this->getCellCalculatedValue('H' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('H' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.profit_loss_oa_data_required',
            //             ['column' => 'H' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('I' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('I' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.profit_loss_oa_data_required',
            //             ['column' => 'I' . $asset])
            //         );
            //     } 
            // }

            // if($asset == 39) { //comprehensive income
            //     if($this->getCellCalculatedValue('H' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('H' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.comprehensive_income_data_required',
            //             ['column' => 'H' . $asset])
            //         );
            //     }
            //     if($this->getCellCalculatedValue('I' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('I' . $asset, false))) {
            //         throw new InvalidCellException(
            //             $this->getTranslatedMessage('fs_input.comprehensive_income_data_required',
            //             ['column' => 'I' . $asset])
            //         );
            //     }
            // }
        // }

        // $balanceSheetReferences = $sheetValues['statement_of_cashflow']['reference'];
        // foreach ($balanceSheetReferences as $asset) {
        //     if($asset == 53) { //net cash provided by operating activities
        //         if($this->getCellCalculatedValue('H' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('H' . $asset, false))) {
        //             throw new InvalidCellException(
        //                 $this->getTranslatedMessage('fs_input.net_cash_oa_data_required',
        //                 ['column' => 'H' . $asset])
        //             );
        //         }
        //         if($this->getCellCalculatedValue('I' . $asset, true) === 0 || is_string($this->getCellCalculatedValue('I' . $asset, false))) {
        //             throw new InvalidCellException(
        //                 $this->getTranslatedMessage('fs_input.net_cash_oa_data_required',
        //                 ['column' => 'I' . $asset])
        //             );
        //         }
        //     }
        // }

    }

    protected function verifyInput() {
        $data = $this->getData();

        if (!isset($data['entity_id']) || empty($data['entity_id'])) {
            throw new FormInputException(
                $this->getTranslatedMessage('fs_input.entity_required')
            );
        }

        if ($this->getCurrentAssetsCount() < self::MINIMUM_CURRENT_ASSETS) {
            throw new InvalidCellException(
                $this->getTranslatedMessage('fs_input.financial_analysis_minumum')
            );
        }

        $balanceSheetResult = $this->checkBalanceSheetValues();
        $incomeStatementResult = $this->checkIncomeStatementValues();
        $cashFlowValues = $this->checkCashFlowValues();

        return $balanceSheetResult && $incomeStatementResult && $cashFlowValues;
    }

    protected function getCurrentAssetsCount() {
        $config = $this->getConfig();
        $currentAssets = $config['values']['financial_analysis']['current_assets'];
        $count = 0;

        foreach ($currentAssets as $asset) {
            if ($this->getCellCalculatedValue($asset, true) !== 0) {
                $count++;
            }
        }

        return $count;
    }

    protected function checkBalanceSheetValues() {
        $config = $this->getConfig();
        $columns = $config['values']['balance_sheet']['validator']['columns'];
        $reference = $config['values']['balance_sheet']['reference'];

        foreach ($columns as $column) {
            $this->setBalanceSheetValuesByColumn($column, $reference);

            if (!$this->isValidNonCurrentAssetsTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.non_current_assets_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidCurrentAssetsTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.current_assets_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidAssetsTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.assets_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidEquityTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.equity_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidNonCurrentLiabilitiesTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.non_current_liabilities_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidCurrentLiabilitiesTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.current_liabilities_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidLiabilitiesTotal($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.liabilities_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidEquityLiabilitiesTally($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.equity_liabilities_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidAssetsLiabilitiesTally($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.assets_liabilities_tally',
                        ['column' => $column]
                    )
                );
            }
        }

        if (@count($this->getErrorMessages()) > 0) {
            return false;
        }

        return true;
    }

    protected function isValidNonCurrentAssetsTotal($column) {
        $bs = $this->getBalanceSheetValues($column);

        $total = $bs['PropertyPlantAndEquipment'] +
            $bs['InvestmentProperty'] +
            $bs['Goodwill'] +
            $bs['IntangibleAssetsOtherThanGoodwill'] +
            $bs['InvestmentAccountedForUsingEquityMethod'] +
            $bs['InvestmentsInSubsidiariesJointVenturesAndAssociates'] +
            $bs['NoncurrentBiologicalAssets'] +
            $bs['NoncurrentReceivables'] +
            $bs['NoncurrentInventories'] +
            $bs['DeferredTaxAssets'] +
            $bs['CurrentTaxAssetsNoncurrent'] +
            $bs['OtherNoncurrentFinancialAssets'] +
            $bs['OtherNoncurrentNonfinancialAssets'] +
            $bs['NoncurrentNoncashAssetsPledgedAsCollateral'];

        return round($total) === round($bs['NoncurrentAssets']);
    }

    protected function isValidCurrentAssetsTotal($column) {
        $bs = $this->getBalanceSheetValues($column);

        $total = $bs['Inventories'] +
            $bs['TradeAndOtherCurrentReceivables'] +
            $bs['CurrentTaxAssetsCurrent'] +
            $bs['CurrentBiologicalAssets'] +
            $bs['OtherCurrentFinancialAssets'] +
            $bs['OtherCurrentNonfinancialAssets'] +
            $bs['CashAndCashEquivalents'] +
            $bs['CurrentNoncashAssetsPledgedAsCollateral'] +
            $bs['NoncurrentAssetsOrDisposalGroups'];

        return round($total) === round($bs['CurrentAssets']);
    }

    protected function isValidAssetsTotal($column) {
        $currentAssets = $this->getBalanceSheetValues($column, 'CurrentAssets');
        $nonCurrentAssets = $this->getBalanceSheetValues($column, 'NoncurrentAssets');
        $assets = $this->getBalanceSheetValues($column, 'Assets');

        return round($currentAssets + $nonCurrentAssets) === round($assets);
    }

    protected function isValidEquityTotal($column) {
        $bs = $this->getBalanceSheetValues($column);
        $total = $bs['IssuedCapital'] +
            $bs['SharePremium'] -
            $bs['TreasuryShares'] +
            $bs['OtherEquityInterest'] +
            $bs['OtherReserves'] +
            $bs['RetainedEarnings'] +
            $bs['NoncontrollingInterests'];

        return round($total) === round($bs['Equity']);
    }

    protected function isValidNonCurrentLiabilitiesTotal($column) {
        $bs = $this->getBalanceSheetValues($column);
        $total = $bs['NoncurrentProvisionsForEmployeeBenefits'] +
            $bs['OtherLongtermProvisions'] +
            $bs['NoncurrentPayables'] +
            $bs['DeferredTaxLiabilities'] +
            $bs['CurrentTaxLiabilitiesNoncurrent'] +
            $bs['OtherNoncurrentFinancialLiabilities'] +
            $bs['OtherNoncurrentNonfinancialLiabilities'];

        return round($total) === round($bs['NoncurrentLiabilities']);
    }

    protected function isValidCurrentLiabilitiesTotal($column) {
        $bs = $this->getBalanceSheetValues($column);
        $total = $bs['CurrentProvisionsForEmployeeBenefits'] +
            $bs['OtherShorttermProvisions'] +
            $bs['TradeAndOtherCurrentPayables'] +
            $bs['CurrentTaxLiabilitiesCurrent'] +
            $bs['OtherCurrentFinancialLiabilities'] +
            $bs['OtherCurrentNonfinancialLiabilities'] +
            $bs['LiabilitiesIncludedInDisposalGroups'];

        return round($total) === round($bs['CurrentLiabilities']);
    }

    protected function isValidLiabilitiesTotal($column) {
        $currentLiabilities = $this->getBalanceSheetValues($column, 'CurrentLiabilities');
        $nonCurrentLiabilities = $this->getBalanceSheetValues($column, 'NoncurrentLiabilities');
        $liabilities = $this->getBalanceSheetValues($column, 'Liabilities');
        $liabilitiesSum = round($currentLiabilities + $nonCurrentLiabilities);

        return $liabilitiesSum === round($liabilities);
    }

    protected function isValidEquityLiabilitiesTally($column) {
        $equity = $this->getBalanceSheetValues($column, 'Equity');
        $liabilities = $this->getBalanceSheetValues($column, 'Liabilities');
        $equityAndLiabilities = $this->getBalanceSheetValues($column, 'EquityAndLiabilities');

        return round($equity + $liabilities) === round($equityAndLiabilities);
    }

    protected function isValidAssetsLiabilitiesTally($column) {
        $assets = $this->getBalanceSheetValues($column, 'Assets');
        $equityAndLiabilities = $this->getBalanceSheetValues($column, 'EquityAndLiabilities');

        return round($assets) === round($equityAndLiabilities);
    }

    protected function setBalanceSheetValuesByColumn($column, $reference) {
        foreach ($reference as $key => $item) {
            $cell = $column.$item;
            $this->balanceSheetValues[$column][$key] = $this->getCellCalculatedValue($cell, true);
        }

        return $this;
    }

    protected function getBalanceSheetValues($column = null, $reference = null) {
        if (!$column) {
            return $this->balanceSheetValues;
        }

        if (!$reference) {
            return $this->balanceSheetValues[$column];
        }

        return $this->balanceSheetValues[$column][$reference];
    }

    protected function checkIncomeStatementValues() {
        $config = $this->getConfig();
        $columns = $config['values']['income_statement']['validator']['columns'];
        $reference = $config['values']['income_statement']['reference'];

        foreach ($columns as $column) {
            $this->setIncomeStatementValuesByColumn($column, $reference);

            if (!$this->isValidGrossProfit($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.gross_profit_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidProfitLossOperatingActivities($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.profit_loss_operating_activities_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidProfitLossBeforeTax($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.profit_loss_before_tax_tally',
                        ['column' => $column]
                    )
                );
            }

            if (!$this->isValidComprehensiveIncome($column)) {
                $this->setErrorMessage(
                    $this->getTranslatedMessage(
                        'fs_input.comprehensive_income_tally',
                        ['column' => $column]
                    )
                );
            }
        }

        if (@count($this->getErrorMessages()) > 0) {
            return false;
        }

        return true;
    }

    protected function isValidGrossProfit($column) {
        $revenue = $this->getIncomeStatementValues($column, 'Revenue');
        $costOfSales = $this->getIncomeStatementValues($column, 'CostOfSales');
        $grossProfit = $this->getIncomeStatementValues($column, 'GrossProfit');

        return round($revenue - $costOfSales) === round($grossProfit);
    }

    protected function isValidProfitLossOperatingActivities($column) {
        $is = $this->getIncomeStatementValues($column);
        $total = $is['GrossProfit'] +
            $is['OtherIncome'] -
            $is['AdministrativeExpense'] -
            $is['OtherExpenseByFunction'] -
            $is['DistributionCosts'] +
            $is['OtherGainsLosses'];

        return round($total) === round($is['ProfitLossFromOperatingActivities']);
    }

    protected function isValidProfitLossBeforeTax($column) {
        $is = $this->getIncomeStatementValues($column);
        $total = $is['ProfitLossFromOperatingActivities'] +
            $is['DifferenceBetweenCarryingAmountOfDividendsPayable'] +
            $is['GainsLossesOnNetMonetaryPosition'] +
            $is['GainLossArisingFromDerecognitionOfFinancialAssets'] +
            $is['FinanceIncome'] -
            $is['FinanceCosts'] -
            $is['ImpairmentLoss'] +
            $is['ShareOfProfitLossOfAssociates'] +
            $is['OtherIncomeFromSubsidiaries'] +
            $is['GainsLossesArisingFromDifference'] +
            $is['CumulativeGainPreviouslyRecognized'] +
            $is['HedgingGainsForHedge'];

        return round($total) === round($is['ProfitLossBeforeTax']);
    }

    protected function isValidComprehensiveIncome($column) {
        $profitLoss = $this->getIncomeStatementValues($column, 'ProfitLoss');
        $otherComprehensiveIncome = $this->getIncomeStatementValues($column, 'OtherComprehensiveIncome');
        $comprehensiveIncome = $this->getIncomeStatementValues($column, 'ComprehensiveIncome');

        return round($profitLoss + $otherComprehensiveIncome) === round($comprehensiveIncome);
    }

    protected function setIncomeStatementValuesByColumn($column, $reference) {
        foreach ($reference as $key => $item) {
            $cell = $column.$item;
            $this->incomeStatementValues[$column][$key] = $this->getCellCalculatedValue($cell, true);
        }

        return $this;
    }

    protected function getIncomeStatementValues($column = null, $reference = null) {
        if (!$column) {
            return $this->incomeStatementValues;
        }

        if (!$reference) {
            return $this->incomeStatementValues[$column];
        }

        return $this->incomeStatementValues[$column][$reference];
    }

    protected function checkCashFlowValues() {
        $config = $this->getConfig();
        $columns = $config['values']['statement_of_cashflow']['validator']['columns'];
        $reference = $config['values']['statement_of_cashflow']['reference'];

        foreach ($columns as $column) {
            $this->setCashFlowValuesByColumn($column, $reference);
        }

        return true;
    }

    protected function setCashFlowValuesByColumn($column, $reference) {
        foreach ($reference as $key => $item) {
            $cell = $column.$item;
            $this->cashFlowValues[$column][$key] = $this->getCellCalculatedValue($cell, true);
        }

        return $this;
    }

    protected function getCashFlowValues($column = null, $reference = null) {
        if (!$column) {
            return $this->cashFlowValues;
        }

        if (!$reference) {
            return $this->cashFlowValues[$column];
        }

        return $this->cashFlowValues[$column][$reference];
    }

    protected function createFinancialReport() {
        $data = $this->getData();
        $config = $this->getConfig();
        $financialAnalysis = $config['values']['financial_analysis'];
        $yearCell = $financialAnalysis['latest_year'];
        $year = $this->getCellStringValue($yearCell);
        $fs = FinancialReport::create(array(
            'entity_id' => $data['entity_id'],
            'title' => $this->companyName,
            'year' => $year,
            'industry_id' => $this->getIndustryCodeFromId($data['industry_id']),
            'money_factor' => $data['money_factor'],
            'currency' => $data['currency'],
            'step' => self::DEFAULT_STEP,
            'fstemplate_file' => $this->getFilename(),
        ));

        $this->setData(['fsid' => $fs->id]);
    }

    protected function deleteOldFSInput() {
        $data = $this->getData();
        $fr = new FinancialReport();
        return $fr->deleteOldFSInput($data['entity_id']);
    }

    protected function createBalanceSheet() {
        $data = $this->getData();
        $config = $this->getConfig();
        $columns = $config['values']['balance_sheet']['validator']['columns'];
        $balanceSheetCount = 0;

        foreach ($columns as $key => $value) {
            $balanceSheetValue = $this->getBalanceSheetValues($value);
            $balanceSheetValue['financial_report_id'] = $data['fsid'];
            $balanceSheetValue['index_count'] = $balanceSheetCount;
            BalanceSheet::create($balanceSheetValue);
            $balanceSheetCount++;
        }
    }

    public function createIncomeStatement() {
        $data = $this->getData();
        $config = $this->getConfig();
        $columns = $config['values']['income_statement']['validator']['columns'];
        $incomeStatementCount = 0;

        foreach ($columns as $key => $value) {
            $incomeStatementValue = $this->getIncomeStatementValues($value);
            // $incomeStatementValue['DepreciationAndAmortisationExpense'] =
            //     $incomeStatementValue['DepreciationExpense'] +
            //     $incomeStatementValue['AmortisationExpense'];
            $incomeStatementValue['financial_report_id'] = $data['fsid'];
            $incomeStatementValue['index_count'] = $incomeStatementCount;
            IncomeStatement::create($incomeStatementValue);
            $incomeStatementCount++;
        }
    }

    protected function createCashFlow() {
        $data = $this->getData();
        $config = $this->getConfig();
        $columns = $config['values']['statement_of_cashflow']['validator']['columns'];
        $cashFlowCount = 0;

        foreach ($columns as $key => $value) {
            $cashFlowValue = $this->getCashFlowValues($value);
            $incomeStatementValue = $this->getIncomeStatementValues($value);
            $cashFlowValue['financial_report_id'] = $data['fsid'];
            $cashFlowValue['index_count'] = $cashFlowCount;
            $cashFlowValue['DepreciationAndAmortisationExpense'] =
                $cashFlowValue['DepreciationExpense'] +
                $cashFlowValue['AmortisationExpense'];
            $cashFlowValue['NetOperatingIncome'] = $incomeStatementValue['ComprehensiveIncome'];
            $cashFlowValue['DebtServiceCapacity'] = abs($cashFlowValue['PrincipalPayments']) +
                abs($cashFlowValue['InterestPayments']) +
                abs($cashFlowValue['LeasePayments']);
            if ($cashFlowValue['DebtServiceCapacity'] != 0) {
                $cashFlowValue['DebtServiceCapacityRatio'] = $cashFlowValue['NetOperatingIncome'] /
                    $cashFlowValue['DebtServiceCapacity'];
            } else {
                $cashFlowValue['DebtServiceCapacityRatio'] = 0;
            }

            CashFlow::create($cashFlowValue);
            $cashFlowCount++;
        }
    }

    public function createAdditionalLines() {
        $data = $this->getData();
        $config = $this->getConfig();
        $sheet = $config['sheet'];
        $columns = $config['additional_lines']['columns'];
        $values = $config['additional_lines']['values'];

        $this->excel->setActiveSheetIndexByName($sheet['additional_lines']);
        $this->excel->setActiveSheetIndex(1);
        $this->sheet = $this->excel->getActiveSheet();

        foreach ($columns as $value) {
            foreach ($values as $key => $index) {
                $cell = $this->getCellCalculatedValue($value.$index);
                $this->additionalLines[$value][$key] = $cell;
            }
        }

        $this->excel->setActiveSheetIndexByName($sheet['data_entry']);
        $this->excel->setActiveSheetIndex(0);
        $this->sheet = $this->excel->getActiveSheet();
        return $this->additionalLines;
    }

    public function getAdditionalLines() {
        return $this->additionalLines;
    }

    protected function getDocumentPeriod() {
        $config = $this->getConfig();
        $financialAnalysis = $config['values']['financial_analysis'];

        $toPeriod = $this->getCellStringValue($financialAnalysis['latest_year']);

        foreach ($financialAnalysis['years'] as $year) {
            $fromPeriod = $this->getCellStringValue($year);

            if ($year) {
                break;
            }
        }

        if ($toPeriod === $fromPeriod) {
            return $fromPeriod.'-12-31';
        }

        return $fromPeriod.'-01-01 to '.$toPeriod.'-12-31';
    }

    public function setDocumentGenerated($documentGenerated) {
        $this->documentGenerated = $documentGenerated;
        return $this;
    }

    public function getDocumentGenerated() {
        if ($this->documentGenerated) {
            return $this->documentGenerated;
        }

        return date('Y-m-d');
    }

    public function getDocumentName() {
        $data = $this->getData();
        $entity = Entity::where('entityid', '=', $data['entity_id'])->first();

        return sprintf(
            static::FILENAME,
            $entity->companyname,
            $this->getDocumentGenerated()
        );
    }

    public function getCellValues() {
        return [
            'balance_sheet' => $this->getBalanceSheetValues(),
            'income_statement' => $this->getIncomeStatementValues(),
            'cash_flow' => $this->getCashFlowValues(),
            'additional_lines' => $this->getAdditionalLines(),
        ];
    }

    public function getIndustryCodeFromId($industryId) {
        switch ($industryId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 10;
            case 4:
                return 35;
            case 5:
                return 36;
            case 6:
                return 41;
            case 7:
                return 45;
            case 8:
                return 49;
            case 9:
                return 55;
            case 10:
                return 58;
            case 11:
                return 64;
            case 12:
                return 68;
            case 13:
                return 69;
            case 14:
                return 77;
            case 15:
                return 94;
            case 16:
                return 85;
            case 17:
                return 86;
            case 18:
                return 90;
            default:
                return 94;
        }
    }

    public static function getIndustryCodeFromIdStatic($industryId) {
        switch ($industryId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 10;
            case 4:
                return 35;
            case 5:
                return 36;
            case 6:
                return 41;
            case 7:
                return 45;
            case 8:
                return 49;
            case 9:
                return 55;
            case 10:
                return 58;
            case 11:
                return 64;
            case 12:
                return 68;
            case 13:
                return 69;
            case 14:
                return 77;
            case 15:
                return 94;
            case 16:
                return 85;
            case 17:
                return 86;
            case 18:
                return 90;
            default:
                return 94;
        }
    }

    public function getIndustryNameFromCode($industryCode) {
        switch ($industryCode) {
            case 1:
                return ' A. Agriculture, forestry and fishing ';
            case 5:
                return ' B. Mining and quarrying ';
            case 10:
                return ' C. Manufacturing ';
            case 35:
                return ' D. Electricity, gas, steam and air conditioning supply ';
            case 36:
                return ' E. Water supply; sewerage, waste management and remediation activities ';
            case 41:
                return ' F. Construction ';
            case 45:
                return ' G. Wholesale and retail trade; repair of motor vehicles and motorcycles ';
            case 49:
                return ' H. Transportation and storage ';
            case 55:
                return ' I. Accommodation and food service activities ';
            case 58:
                return ' J. Information and communication ';
            case 64:
                return ' K. Financial and insurance activities ';
            case 68:
                return ' L. Real estate activities ';
            case 69:
                return ' M. Professional, scientific and technical activities ';
            case 77:
                return ' N. Administrative and support service activities ';
            case 94:
                return ' S. Other service activities ';
            case 84:
                return ' O. Public administration and defence; compulsory social security ';
            case 85:
                return ' P. Education ';
            case 86:
                return ' Q. Human health and social work activities ';
            case 90:
                return ' R. Arts, entertainment and recreation ';
            default:
                return ' S. Other service activities ';
        }
    }

    public function getIndustryName($industryCode) {
        switch ($industryCode) {
            case "A":
                return 'Agriculture, Forestry, and Fishing';
            case "B":
                return 'Mining and Quarrying';
            case "C":
                return 'Manufacturing';
            case "D":
                return 'Electricity, Gas, Steam, and Air Conditioning Supply';
            case "E":
                return 'Water Supply; Sewerage, Waste Management and Remediation Activities';
            case "F":
                return 'Construction';
            case "G":
                return 'Wholesale and Retail Trade; Repair of Motor Vehicles and Motorcycles';
            case "H":
                return 'Transportation and Storage';
            case "I":
                return 'Accommodation and Food Service Activities';
            case "J":
                return 'Information and Communication';
            case "K":
                return 'Financial and Insurance Activities';
            case "L":
                return 'Real Estate Activities';
            case "M":
                return 'Professional, Scientific and Technical Activities';
            case "N":
                return 'Administrative and Support Service Activities';
            case "O":
                return 'Public Administration and Defense; Compulsory Social Security';
            case "P":
                return 'Education';
            case "Q":
                return 'Human Health and Social Work Activities';
            case "R":
                return 'Arts, Entertainment, and Recreation';
            case "S":
                return 'Other Service Activities';
            case "T":
                return 'Activities of households as employers; Undifferentiated goods-and services-producing activities of households for own use';
            case "U":
                return 'Activities of extra-territorial organizations and bodies';
            default:
                return ' S. Other service activities ';
        }
    }

    public static function getIndustryNameStatic($industryCode) {
        switch ($industryCode) {
            case "A":
                return 'Agriculture, Forestry, and Fishing';
            case "B":
                return 'Mining and Quarrying';
            case "C":
                return 'Manufacturing';
            case "D":
                return 'Electricity, Gas, Steam, and Air Conditioning Supply';
            case "E":
                return 'Water Supply; Sewerage, Waste Management and Remediation Activities';
            case "F":
                return 'Construction';
            case "G":
                return 'Wholesale and Retail Trade; Repair of Motor Vehicles and Motorcycles';
            case "H":
                return 'Transportation and Storage';
            case "I":
                return 'Accommodation and Food Service Activities';
            case "J":
                return 'Information and Communication';
            case "K":
                return 'Financial and Insurance Activities';
            case "L":
                return 'Real Estate Activities';
            case "M":
                return 'Professional, Scientific and Technical Activities';
            case "N":
                return 'Administrative and Support Service Activities';
            case "O":
                return 'Public Administration and Defense; Compulsory Social Security';
            case "P":
                return 'Education';
            case "Q":
                return 'Human Health and Social Work Activities';
            case "R":
                return 'Arts, Entertainment, and Recreation';
            case "S":
                return 'Other Service Activities';
            case "T":
                return 'Activities of households as employers; Undifferentiated goods-and services-producing activities of households for own use';
            case "U":
                return 'Activities of extra-territorial organizations and bodies';
            default:
                return ' S. Other service activities ';
        }
    }

    public function getMoneyFactorFromUnit($unit) {
        switch ($unit) {
            case 1:
                return 'Ones';
            case 1000:
                return 'Thousands';
            case 1000000:
                return 'Millions';
        }
    }

    public function getCurrencyNameFromCode($currencyCode) {
        $currency = Currency::where('iso_code', '=', $currencyCode)->first();
        return $currency->name;
    }

    public function setTotalAssets() {
        $config = $this->getConfig();
        $data = $this->getData();
        $balanceSheet = $config['values']['balance_sheet'];
        $columns = $balanceSheet['validator']['columns'];
        $cell = $balanceSheet['reference']['Assets'];

        foreach ($columns as $column) {
            $value = $this->getCellCalculatedValue($column.$cell);

            if ($value && !$this->totalAssets) {
                $this->totalAssets = $value * $data['money_factor'];
                break;
            }
        }

        if ($this->totalAssets) {
            $assetInfo = AssetGrouping::getByAmount($this->totalAssets);
            DB::table('tblentity')
                ->where('entityid', $data['entity_id'])
                ->update([
                    'total_assets' => $this->totalAssets,
                    'total_asset_grouping' => $assetInfo['id'],
                ]);

        }
    }

    public function getTotalAssets() {
        return $this->totalAssets;
    }

    public function verifyYearOfUploadedFS(){
        $fsData = $this->getCellValues();
        $atleast = 2;
        foreach($fsData as $fsData_key => $fs_value) {
            if($fsData_key == "additional_lines"){
                continue;
            }
            $count = 0;
            foreach($fs_value as $sheet_key =>  $sheet_value) {
                $isData = false;
                foreach($sheet_value as $a => $v) {
                    if($a !== 'Year') {
                        if((int)$v !== 0) {
                            $isData = true;
                        }
                    }
                }
                if($isData !== false) {
                    $count += 1;
                }
            }
            if($count < $atleast) {
                $message = "We need 2 years of Balance Sheet, Income Statement and Statement of Cashflow data at minimum. Please fill in the relevant sections on the Financial Statement Input Template and try uploading again.";
                $this->setErrorMessage($message);
                $this->deleteOldFSInput();
                return;
            }
            return;
        }
    }
}
