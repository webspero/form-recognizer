<?php

namespace CreditBPO\Services\FinancialStatement;

use Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;
use CreditBPO\Libraries\FSUploadValue;

class FinancialStatementUpdate
{
    CONST ZIP_DESTINATION_PATH = 'documents';
    
    public function __construct()
    {
        $this->documentDb = new Document;
        $this->reportDb = new Report;
        $this->fsUploadValueLibrary = new FSUploadValue;
    }

    public function validateReportIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function validateRequest($documentType)
    {
        $details = array(
            'documenttype' => $documentType
        );

        /* Sets Validation Messages */
        $messages = array(
            'documenttype.required' => 'Document type is required.',
            'documenttype.in' => "Use only 'un-audited', 'interim' and 'audited' for document type."
        );
        
        $documentType = array_keys($this->fsUploadValueLibrary->documentType);

        /* Sets Validation Rules */
        $rules = array(
            'documenttype' => 'required|in:'.implode(',',$documentType)
        );

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);
        return $validator;
    }

    public function updateFSDetails($fsUpdateDto)
    {
        $fsDocument = $this->documentDb->getFsDocumentByReportId($fsUpdateDto->getReportId());
        if (!$fsDocument->isEmpty()){
            $fsUpdateDto->setZipFilename($fsDocument[0]->document_rename)
                        ->setDestinationPath(self::ZIP_DESTINATION_PATH);

            foreach($fsDocument as $document){
                $this->documentDb->updateDocumentType($document, $this->fsUploadValueLibrary->documentTypeToDatabase($fsUpdateDto->getDocumentType()));
                $displayName = $document->document_orig;
                /*Check if full or basic*/
                $type = explode(' ', trim($displayName));
                if ($type[0] == 'Basic'){
                    $fsUpdateDto->setBasicDocument($displayName);
                } else{
                    $fsUpdateDto->setFullDocument($displayName);
                }
            }
            return $fsUpdateDto;
        } else{
            return null;
        }
        
    }
}