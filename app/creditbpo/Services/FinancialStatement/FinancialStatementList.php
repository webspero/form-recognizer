<?php

namespace CreditBPO\Services\FinancialStatement;

use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;
use CreditBPO\DTO\FSListDTO;
use CreditBPO\Libraries\FSUploadValue;

class FinancialStatementList
{
    CONST ZIP_DESTINATION_PATH = 'documents';

    public function __construct()
    {
        $this->documentDb = new Document;
        $this->reportDb = new Report;
        $this->fsUploadValueLibrary = new FSUploadValue;
    }

    public function validateReportIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function getReportFSList($reportId)
    {
        $fsDocument = $this->documentDb->getFsDocumentByReportId($reportId);
        if (!$fsDocument->isEmpty()){
            $fsListDto = new FSListDTO;
            $fsListDto->setReportId($reportId)
                      ->setDocumentType($this->fsUploadValueLibrary->documentTypeToResponse($fsDocument[0]->document_type))
                      ->setZipFilename($fsDocument[0]->document_rename)
                      ->setDestinationPath(self::ZIP_DESTINATION_PATH);
            
            foreach($fsDocument as $document){
                $displayName = $document['document_orig'];
                /*Check if full or basic*/
                $type = explode(' ', trim($displayName));
                if ($type[0] == 'Basic'){
                    $fsListDto->setBasicDocument($displayName);
                } else{
                    $fsListDto->setFullDocument($displayName);
                }
            }
            return $fsListDto;
        } else{
            return null;
        }
    }
}