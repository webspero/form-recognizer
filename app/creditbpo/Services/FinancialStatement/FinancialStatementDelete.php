<?php

namespace CreditBPO\Services\FinancialStatement;

use ZipArchive;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Document;

class FinancialStatementDelete
{
    CONST ZIP_DESTINATION_PATH = 'documents';

    public function __construct()
    {
        $this->documentDb = new Document;
        $this->reportDb = new Report;
    }

    public function validateReportIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (is_null($report)){
            return false;
        } else{
            return true;
        }
    }

    public function deleteFinancialStatement($reportId)
    {
        $fsDocument = $this->documentDb->getFsDocumentByReportId($reportId);
        if (!$fsDocument->isEmpty()){
            $ids = [];
            /* Loop data to delete files in zip one a time */
            foreach($fsDocument as $document){
                $this->deleteFileInZip($document->document_rename, $document->document_orig);
                array_push($ids, $document->documentid);
            }
            /* Delete fs documents in database */
            return $this->documentDb->deleteDocument($ids);
        } else{
            return null;
        }
    }

    public function deleteFileInZip($zipFolderName, $filename)
    {
        $zip = new ZipArchive();
        $zipFolder = self::ZIP_DESTINATION_PATH.'/'.$zipFolderName;

        if (file_exists($zipFolder)){
            /* Hang: 11 Means 'Can\'t open file' in ZipArchive Class ($open == TRUE && !$open == 11) */
            if ($zip->open($zipFolder) == TRUE) {
                /* Locatename function returns file index or false */
                if($zip->locateName($filename) !== FALSE){
                    /* If zip file contains only the remaining file to delete it would not close properly
                        So will delete the entire zipfolder */
                    if ($zip->numFiles == 1){
                        $zip->close();
                        unlink($zipFolder);
                    } else{
                        $zip->deleteName($filename);
                        $zip->close();
                    }
                }
            }
        }
    }
}