<?php

namespace CreditBPO\Services\FinancialStatement;

use ZipArchive;
use Validator;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Currency;
use CreditBPO\DTO\FSUploadDTO;
use CreditBPO\Services\SimpleFsInput;
use CreditBPO\Services\ComplexFsInput;
use CreditBPO\Models\Document;
use CreditBPO\Libraries\FSUploadValue;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Libraries\MimeTypes;
use CreditBPO\Models\FinancialReport;
use Session;
use DB;

class FinancialStatementUpload
{
    CONST ZIP_DESTINATION_PATH = 'documents';
    CONST TEMP_FOLDER_PATH = 'temp';
    const FS_INPUT_COMPLEX = 'files/FS Input Template - Complex Output.xlsx';

    public function __construct()
    {
        $this->reportDb = new Report;
        $this->currencyDb = new Currency;
        $this->documentDb = new Document;
        $this->assetGroupingService = new AssetGrouping;
        $this->fsUploadValueLibrary = new FSUploadValue;
        $this->mimeTypesLibrary = new MimeTypes;
    }

    public function validateReportFSUpload($reportId)
    {
        /*Check if report doesn't upload any financial statement */
        $fsDocument = $this->documentDb->getFsDocumentByReportId($reportId);
        if ($fsDocument->isEmpty()){
            return true;
        } else{
            return false;
        }
    }

    public function manageRequestDetails($data, $reportId)
    {
        $fsUploadDto = new FSUploadDTO();
        
        /* Check and get request file */
        if (array_key_exists('file', $data))
        {
            /** Set file and file extension*/
            $fsUploadDto->setFileExtension($this->mimeTypesLibrary->mimeTypes[$data['filetype']]);
            $fsUploadDto->setFile($data['file']);

            /* Check report id and get industry main id */
            $report = $this->reportDb->getReportByReportId($reportId);
            if (!empty($report)){
                $fsUploadDto->setReportId($reportId);
                if ($report->industry_main_id != 0 || $report->industry_main_id != null){
                    $fsUploadDto->setIndustryMainId($report->industry_main_id);
                }
            }

            $fsUploadDto->setCurrency($data['currency'])
                        ->setUnit(strtolower($data['unit']))
                        ->setDocumentType(strtolower($data['documenttype']));
            return $fsUploadDto;
        }
        else{
            return null;
        }
    }

    public function validateDetails($fsUploadDto, $data)
    {
        $errors = [];

        /* Check if request has file */
        if(empty($fsUploadDto->getFile())) {
            array_push($errors, "Financial statement document is required.");
        }

        $financialStatement = $data;
        
        /* Check if fs file uploaded has right file format */
        if (!in_array($fsUploadDto->getFileExtension(), ['xlsx', 'xls'])){
            array_push($errors, "The file you were trying to upload is not a Financial Statement Template.  Please download the file on ".route('fstemplate.download'));
        }

        /* Check report and industry id */
        if (!empty($fsUploadDto->getReportId())){
            if (empty($fsUploadDto->getIndustryMainId())){
                array_push($errors, "Please select industry details first.");
            }
        } else{
            array_push($errors, "Report doesn't exist.");
        }

        /* Check currency request data */
        if (!empty($fsUploadDto->getCurrency())){
            $currency = $this->currencyDb->getCurrencyByIso($fsUploadDto->getCurrency());
            if (is_null($currency)){
                array_push($errors, "Incorrect currency details. Please enter the ISO code.");
            }
        }

        /* If report and request have main details, proceed to other validation */
        $details = array(
            'currency' => $fsUploadDto->getCurrency(),
            'unit' => $fsUploadDto->getUnit(),
            'documenttype' => $fsUploadDto->getDocumentType()
        );

        /* Sets Validation Messages */
        $messages = array(
            'currency.required' => 'Currency is required.',
            'unit.required' => 'Unit is required.',
            'documenttype.required' => 'Document type is required.',
                
            'unit.in' => "Use only 'default', 'thousands', and 'millions' for unit.",
            'documenttype.in' => "Use only 'un-audited', 'interim' and 'audited' for document type."
        );

        $unit = array_keys($this->fsUploadValueLibrary->unit);
        $documentType = array_keys($this->fsUploadValueLibrary->documentType);

        /* Sets Validation Rules */
        $rules = array(
            'currency' => 'required',
            'unit' => 'required|in:'.implode(',', $unit),
            'documenttype' => 'required|in:'.implode(',',$documentType)
        );

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }

        /* Check Validation of uploaded FS File */
        $entity = Report::where('entityid', $fsUploadDto->getReportId())->first();
        $unit;

        // Create temporary file
        $filename = rand(11111,99999).time().'.'.$fsUploadDto->getFileExtension();
        $financialStatement['file']->move('temp',$filename);
        Session::put('filenameTemp', $filename);
        
        if($fsUploadDto->getUnit() == "default"){
            $unit = 1;
        }elseif($fsUploadDto->getUnit() == "thousands"){
            $unit = 1000;
        }elseif($fsUploadDto->getUnit() == "millions"){
            $unit = 1000000;
        }
        
        /** Initialize Data for Validation */
        $simpleFsData = [
            'entity_id' => $fsUploadDto->getReportId(),
            'money_factor' => $unit,
            'currency' => $fsUploadDto->getCurrency(),
            'industry_id' => $entity->industry_main_id,
        ];
        $simpleFsInput = new SimpleFsInput('temp/'.$filename, $simpleFsData);
        $simple_result = $simpleFsInput->save();

        /** Check erros from fs validation */
        if(!$simple_result){
            array_push($errors,$simpleFsInput->getErrorMessages() );
        }

        return $errors;
    }

    public function createZipFile($fsUploadDto)
    {
        $zip = new ZipArchive();
        $zipFilename = str_replace(' ', '_', $fsUploadDto->getReportId().' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip');
        if ($zip->open(self::ZIP_DESTINATION_PATH.'/'.$zipFilename, ZipArchive::CREATE) !== TRUE){
            return null;
        } else{
            $fsUploadDto->setDestinationPath(self::ZIP_DESTINATION_PATH);
            $fsUploadDto->setZipFilename($zipFilename);
            return $zip;
        }
    }

    public function fileUploadToTemp($fsUploadDto)
    {
        $filename = rand(11111,99999).time().'.'.$fsUploadDto->getFileExtension();
        file_put_contents(self::TEMP_FOLDER_PATH.'/'.$filename, $fsUploadDto->getFile());
        $fsUploadDto->setTempFilename($filename);
    }

    public function processSimpleFSDetails($fsUploadDto)
    {
        $simpleFsData = [
            'entity_id' => $fsUploadDto->getReportId(),
            'money_factor' => $this->fsUploadValueLibrary->unitToDatabase($fsUploadDto->getUnit()),
            'currency' => $fsUploadDto->getCurrency(),
            'industry_id' => $fsUploadDto->getIndustryMainId(),
        ];
        $simpleFsInput = new SimpleFsInput(
            self::TEMP_FOLDER_PATH.'/'.$fsUploadDto->getTempFilename(), 
            $simpleFsData
        );
        $simpleFsInput->save();
        return $simpleFsInput;
    }

    public function processComplexFSDetails($zip, $fsUploadDto, $simpleFsDetails)
    {
        $complexFsInput = new ComplexFsInput(
            public_path(self::FS_INPUT_COMPLEX),
            ['entity_id' => $fsUploadDto->getReportId()]
        );
        $complexFsInput->setDocumentGenerated(
            $simpleFsDetails->getDocumentGenerated()
        );
        $complexFsInput->write($simpleFsDetails->getCellValues());

        /* Update total asset and asset grouping */
        $assets = $simpleFsDetails->getTotalAssets();
        $fsUploadDto->setTotalAsset(number_format($assets, 2));
        $fsUploadDto->setTotalAssetGrouping(
            $this->assetGroupingService->getByAmount($assets)
        );

        $complexFilename = $complexFsInput->getDocumentFilename();
        /*Save Complex Financial Statement to database*/
        $dbResult = $this->saveFSFileToDatabase($complexFilename, $fsUploadDto);
        /*Save complex filename to dto */
        $fsUploadDto->setComplexDocumentFilename($complexFilename);
        /*Save Complex Financial Statement to zip folder*/
        $zipResult = $zip->addFile(public_path(self::TEMP_FOLDER_PATH.'/'.$complexFilename), $complexFilename);
        return $dbResult && $zipResult;
    }

    public function saveSimpleFSDetails($zip, $fsUploadDto, $simpleFsDetails)
    {
        $simpleFilename = $simpleFsDetails->getDocumentFilename();
        /*Save Simple Financial Statement to database*/
        $this->saveFSFileToDatabase($simpleFilename, $fsUploadDto);
        /*Save Simple filename to dto */
        $fsUploadDto->setSimpleDocumentFilename($simpleFilename);
        /*Save Simple Financial Statement to zip folder*/
        $zip->addFile(self::TEMP_FOLDER_PATH.'/'.$fsUploadDto->getTempFilename(), $simpleFilename);
        $zip->close();
    }

    public function saveFSFileToDatabase($filename, $fsUploadDto)
    {
        $data = [
            'reportid' => $fsUploadDto->getReportId(),
            'documenttype'=> $this->fsUploadValueLibrary->documentTypeToDatabase($fsUploadDto->getDocumentType()),
            'uploadtype'=> $fsUploadDto->getUploadType(),
            'documentgroup'=> $fsUploadDto->getDocumentGroup(),
            'documentfilename'=> $filename,
            'documentrename'=> $fsUploadDto->getZipFilename()
        ];

        /* Save data to tbldocument */
        return $this->documentDb->saveDocument($data);
    }

    public function processFsTemplate($fsUploadDto, $data){
        /** INIIALIZATION */
        $errors = [];
        $entity = Report::where('entityid', $fsUploadDto->getReportId())->first();
        $document_type = 1;
        $upload_type = 5;

        $financialStatement = $data;

        $reportFileFormat = 'Raw FS %s as of %s.%s';

        /** Set file directory */
        $destinationPath = public_path().'/documents/';

        // Create new instance of zip file
        $zip = new ZipArchive();

        // Set filename of the zip file
        $zip_filename = str_replace(' ', '_', $fsUploadDto->getReportId().' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip');
        $zip->open($destinationPath.$zip_filename, ZipArchive::CREATE);
        $document_group = 21;
        $document_orig =  $fsUploadDto->getReportId().' - FS Input Template for the period of ';

        // Assign Value for Unit Types
        if($fsUploadDto->getUnit() == "default"){
            $unit = 1;
        }elseif($fsUploadDto->getUnit() == "thousands"){
            $unit = 1000;
        }elseif($fsUploadDto->getUnit() == "millions"){
            $unit = 1000000;
        }

        /** Initialize Data for Validation */
        $simpleFsData = [
            'entity_id' => $fsUploadDto->getReportId(),
            'money_factor' => $unit,
            'currency' => $fsUploadDto->getCurrency(),
            'industry_id' => $entity->industry_main_id,
        ];

        $simpleFsInput = new SimpleFsInput('temp/'.Session::get('filenameTemp'), $simpleFsData);
        $result = $simpleFsInput->save();

        $complexFsInput = new ComplexFsInput(
            public_path(self::FS_INPUT_COMPLEX),
            ['entity_id' =>  $fsUploadDto->getReportId()]
        );

        $complexFsInput->setDocumentGenerated(
            $simpleFsInput->getDocumentGenerated()
        );

        $result = $complexFsInput->write($simpleFsInput->getCellValues());
        if($result) {
            $document_orig = $simpleFsInput->getDocumentFileName();
            $file = $complexFsInput->getDocumentFileName();
            $assets = $simpleFsInput->getTotalAssets();
            $total_asset = number_format($assets, 2);
            $total_asset_grouping = AssetGrouping::getByAmount($assets);
            if($document_group == 21) {
                $cond = array(
                    "is_deleted" => 0,
                    "entity_id" => $fsUploadDto->getReportId(),
                    "document_group" => $document_group
                );
                $curDocs = Document::where($cond)->get();
                foreach ($curDocs as $key => $value) {
                    $value->is_deleted = 1;
                    $value->save;
                    Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
                }
            }
            /* create entry on database */
            $data = [
                'entity_id' => $fsUploadDto->getReportId(),
                'document_type'=> $document_type,
                'upload_type'=> $upload_type,
                'document_group'=> $document_group,
                'document_orig'=> $file,
                'document_rename'=> $zip_filename,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s'),
            ];
            $documentid = DB::table('tbldocument')->insertGetId($data);


            $master_ids[]       = $documentid;
            $filenames[]        = $file;
            $display_names[]    = $file;
            $where = array(
                "entity_id" => $fsUploadDto->getReportId(),
                "is_deleted" => 0
            );
            $fs = FinancialReport::where($where)->first();
            $zip->addFile(public_path('temp/'.$file), $file);

            $data = array(
                'entity_id'=>$fsUploadDto->getReportId(),
                'document_type'=>$document_type,
                'upload_type'=>$upload_type,
                'document_group'=>$document_group,
                'document_orig'=>$document_orig,
                'document_rename'=>$zip_filename,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            );
            $documentid = DB::table('tbldocument')->insertGetId($data);

            $master_ids[]       = $documentid;
            $filenames[]        = $document_orig;
            $display_names[]    = $document_orig;
            $zip->addFile('temp/'.Session::get('filenameTemp'), $document_orig);
        }

        /** Close Zip file */
        $zip->close(); 
        return;
    }

    public function validateReport($reportId, $userId){
        $existingReport = $this->reportDb->getReportByReportId($reportId);
        $checkReportOwner = $this->reportDb->checkReportOwner($reportId, $userId);

        if($existingReport == null) {
            $errorMessages['errors'] = "Report does not exist.";
            $errorMessages['code'] = HTTP_STS_NOTFOUND;
        } elseif($checkReportOwner == null) {
            $errorMessages['errors'] = "Report does not belong to the user.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } elseif($checkReportOwner->status == 7){
            $errorMessages['errors'] = "Report is already done.";
            $errorMessages['code'] = HTTP_STS_FORBIDDEN;
        } else {
            $errorMessages = null;
        }
        return $errorMessages;
    }
}