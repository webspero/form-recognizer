<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Exceptions\InvalidCellException;
use CreditBPO\Exceptions\FormInputException;
use BalanceSheet;
use CashFlow;
use Currency;
use DB;
use Entity;
use FinancialReport;
use IncomeStatement;
use Auth;
use File;
use ZipArchive;
use \Exception as Exception;

class QuickbooksInput {
    const ATLEAST = 1;

    protected $quickbook;
    protected $simpledata;
    protected $incomeStatementValues = [];
    protected $balanceSheetValues = [];
    protected $cashFlowValues = [];
    protected $balanceSheet;
    protected $incomeStatement;
    protected $cashFlow;
    protected $quickBookFile;
    protected $balance_sheet_year = [];
    protected $income_statement_year = [];
    protected $cash_flow_year = [];
    protected $report_index;
    protected $basic_fs_data;
    protected $basic_file;

    public function __construct($filePath, $data = []) {
        $this->quickbook = $filePath;
        $this->simpledata = $data;
    }

    public function setBalanceSheetData($balanceSheet) {
        $this->balanceSheet = $balanceSheet;
    }

    public function getBalanceSheetData() {
        return $this->balanceSheet;
    }

    public function setIncomeStatementSheetData($incomeStatement) {
        $this->incomeStatement = $incomeStatement;
    }

    public function getIncomeStatementSheetData() {
        return $this->incomeStatement;
    }

    public function setCashFlowSheetData($cashFlow) {
        $this->cashFlow = $cashFlow;
    }

    public function getCashFlowSheetData() {
        return $this->cashFlow;
    }

    public function setquickBookFile($quickBookFile) {
        $this->quickBookFile = $quickBookFile;
    }

    public function getquickBookFile() {
        return $this->quickBookFile;
    }

    public function setErrorMessage($message) {
        $this->errorMessages[] = $message;
        return $this;
    }

    public function getErrorMessages() {
        return $this->errorMessages;
    }

    public function getTranslatedMessage($messageKey, $data = []) {
        return trans($messageKey, $data);
    }
    
    public function verifyQuickBookFile($data){
        // set file location
        $this->setquickBookFile($data);

        // Set data of balance sheet, income statement, cashflow
        $this->setBalanceSheetData(Excel::load($data['balance_sheet_path'])->noHeading()->all()->toArray());
        $this->setIncomeStatementSheetData(Excel::load($data['income_statement_path'])->noHeading()->all()->toArray());
        $this->setCashFlowSheetData(Excel::load($data['cashflow_path'])->noHeading()->all()->toArray());

        /** Validate  */
        try{
            $this->verifyBalanceSheet();
            $this->verifyIncomeSheet();
            $this->verifyCashFlowSheet();

            // Verify years if present on all sheets
            $index = $this->verifyYearAndData();

            // Match QuickBook Files to FS Input Template
            $this->matchQuickBookToFSInputTemplate();

            // Convert QuickBook to Basic FS
            $this->convertToBasicFSInputTemplate();

            return true;

        }catch (InvalidCellException $e) {
            $message = $e->getMessage();
        } catch (FormInputException $e) {
            $message = $e->getMessage();
        }

        $this->deleteOldQuickBook();
        $this->setErrorMessage($message);
        return false;
    }

    public function verifyBalanceSheet(){
        $file = $this->getquickBookFile();
        $balance_sheet =  Excel::load($file['balance_sheet_path']);
        $balance_sheet_data = $this->getBalanceSheetData();
        if(in_array('Balance Sheet', $balance_sheet->getSheetNames())){
            // Check years
            if(isset($balance_sheet_data[4]) && !empty($balance_sheet_data[4])){
                foreach($balance_sheet_data[4] as $key => $value){
                    if($key != 0){
                        // Check if column is per year data
                        if(strpos("Jan - Dec", $value) !== false){
                            throw new FormInputException(
                                $this->getTranslatedMessage('quickbooks.balance_sheet_required_year_entry')
                            ); 
						}else{
							$valRaw = explode(' ',$value);
							if($valRaw[count($valRaw) -1] == 'Total' || $valRaw[count($valRaw) -1] == 'total'){
								continue;
							}
                            if($valRaw[count($valRaw) -1] != ''){
                                $this->balance_sheet_year[] = $valRaw[count($valRaw) -1];
                            }
						}
                    }
                }
            }
            if(count($this->balance_sheet_year) < self::ATLEAST){
                throw new FormInputException(
                    $this->getTranslatedMessage('quickbooks.balance_sheet_atleast')
                ); 
            }
        }else{
            throw new FormInputException(
                $this->getTranslatedMessage('quickbooks.balance_sheet_required')
            ); 
        }
        return;
    }

    public function verifyIncomeSheet(){
        $file = $this->getquickBookFile();
        $income_sheet =  Excel::load($file['income_statement_path']);
        $income_sheet_data = $this->getIncomeStatementSheetData();
        if(in_array('Profit and Loss', $income_sheet->getSheetNames())){
            // Check years
            if(isset($income_sheet_data[4]) && !empty($income_sheet_data[4])){
                foreach($income_sheet_data[4] as $key => $value){
                    if($key != 0){
                        // Check if column is per year data
                        if(strpos("Jan - Dec", $value) !== false){
                            throw new FormInputException(
                                $this->getTranslatedMessage('quickbooks.income_sheet_required_year_entry')
                            ); 
						}else{
							$valRaw = explode(' ',$value);
							if($valRaw[count($valRaw) -1] == 'Total' || $valRaw[count($valRaw) -1] == 'total'){
								continue;
							}
                            if($valRaw[count($valRaw) -1] != ''){
                                $this->income_statement_year[] = $valRaw[count($valRaw) -1];
                            }
						}
                    }
                }
            }
            if(count($this->balance_sheet_year) < self::ATLEAST){
                throw new FormInputException(
                    $this->getTranslatedMessage('quickbooks.income_statement_atleast')
                ); 
            }
        }else{
            throw new FormInputException(
                $this->getTranslatedMessage('quickbooks.income_sheet_required')
            ); 
        }
        return;
    }

    public function verifyCashFlowSheet(){
        $file = $this->getquickBookFile();
        $cashflow_sheet =  Excel::load($file['cashflow_path']);
        $cashflow_data = $this->getCashFlowSheetData();
        if(in_array('Statement of Cash Flows', $cashflow_sheet->getSheetNames())){
            // Check years
            if(isset($cashflow_data[4]) && !empty($cashflow_data[4])){
                foreach($cashflow_data[4] as $key => $value){
                    if($key != 0){
                        // Check if column is per year data
                        if(strpos("Jan - Dec", $value) !== false){
                            throw new FormInputException(
                                $this->getTranslatedMessage('quickbooks.cashflow_sheet_required_year_entry')
                            ); 
						}else{
							$valRaw = explode(' ',$value);
							if($valRaw[count($valRaw) -1] == 'Total' || $valRaw[count($valRaw) -1] == 'total'){
								continue;
							}
                            if($valRaw[count($valRaw) -1] != ''){
                                $this->cash_flow_year[] = $valRaw[count($valRaw) -1];
                            }
						}
                    }
                }
            }
            if(count($this->balance_sheet_year) < self::ATLEAST){
                throw new FormInputException(
                    $this->getTranslatedMessage('quickbooks.cashflow_statement_atleast')
                ); 
            }
        }else{
            throw new FormInputException(
                $this->getTranslatedMessage('quickbooks.income_sheet_required')
            ); 
        }
        return;
    }

    public function verifyYearAndData(){
        $data_year = array(
            'balance_sheet' => $this->balance_sheet_year,
            'income_statement' => $this->income_statement_year,
            'cashflow'  => $this->cash_flow_year
        );

        $income = 0;
        $balance = 0;
        $cashflow = 0;

        // Get latest year index
        foreach($data_year as $key => $value){
            $index = count($data_year);

            if($key == 'balance_sheet'){
                $balance = $index;
            } else if($key == 'income_statement'){
                $income = $index;
            } else{
                $cashflow = $index;
            }
        }

        $this->report_index = array(
            'balance_start_index' => $balance,
            'income_start_index'    => $income,
            'cashflow_start_index'  => $cashflow 
        );
        return;
    }

    public function matchQuickBookToFSInputTemplate(){
        $report_index = $this->report_index;

        $sheet = array("E", "D", "C", "B");

        // Match Balance Sheet
        $balance_sheet = $this->getBalanceSheetData();
        $b_sheet = [];

        for($x=$this->report_index['balance_start_index']; $x>=1; $x--){
            // Initialized all cells to zero
            $b_sheet[$sheet[$x]]['CashAndCashEquivalents'] = 0;
            $b_sheet[$sheet[$x]]['TradeAndOtherCurrentReceivables'] = 0;
            $b_sheet[$sheet[$x]]['Inventories'] = 0;
            $b_sheet[$sheet[$x]]['OtherCurrentFinancialAssets'] = 0;
            $b_sheet[$sheet[$x]]['PropertyPlantAndEquipment'] = 0;
            $b_sheet[$sheet[$x]]['TradeAndOtherCurrentPayables'] = 0;
            $b_sheet[$sheet[$x]]['OtherCurrentFinancialLiabilities'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentPayables'] = 0;
            $b_sheet[$sheet[$x]]['CurrentTaxAssetsCurrent'] = 0;
            $b_sheet[$sheet[$x]]['CurrentBiologicalAssets'] = 0;
            $b_sheet[$sheet[$x]]['OtherCurrentNonfinancialAssets'] = 0;
            $b_sheet[$sheet[$x]]['CurrentNoncashAssetsPledgedAsCollateral'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentAssetsOrDisposalGroups'] = 0;
            $b_sheet[$sheet[$x]]['InvestmentProperty'] = 0;
            $b_sheet[$sheet[$x]]['Goodwill'] = 0;
            $b_sheet[$sheet[$x]]['IntangibleAssetsOtherThanGoodwill'] = 0;
            $b_sheet[$sheet[$x]]['InvestmentAccountedForUsingEquityMethod'] = 0;
            $b_sheet[$sheet[$x]]['InvestmentsInSubsidiariesJointVenturesAndAssociates'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentBiologicalAssets'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentReceivables'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentInventories'] = 0;
            $b_sheet[$sheet[$x]]['DeferredTaxAssets'] = 0;
            $b_sheet[$sheet[$x]]['CurrentTaxAssetsNoncurrent'] = 0;
            $b_sheet[$sheet[$x]]['OtherNoncurrentFinancialAssets'] = 0;
            $b_sheet[$sheet[$x]]['OtherNoncurrentNonfinancialAssets'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentNoncashAssetsPledgedAsCollateral'] = 0;
            $b_sheet[$sheet[$x]]['CurrentProvisionsForEmployeeBenefits'] = 0;
            $b_sheet[$sheet[$x]]['OtherShorttermProvisions'] = 0;
            $b_sheet[$sheet[$x]]['TradeAndOtherCurrentPayables'] = 0;
            $b_sheet[$sheet[$x]]['CurrentTaxLiabilitiesCurrent'] = 0;
            $b_sheet[$sheet[$x]]['OtherCurrentNonfinancialLiabilities'] = 0;
            $b_sheet[$sheet[$x]]['LiabilitiesIncludedInDisposalGroups'] = 0;
            $b_sheet[$sheet[$x]]['NoncurrentProvisionsForEmployeeBenefits'] = 0;
            $b_sheet[$sheet[$x]]['OtherLongtermProvisions'] = 0;
            $b_sheet[$sheet[$x]]['DeferredTaxLiabilities'] = 0;
            $b_sheet[$sheet[$x]]['CurrentTaxLiabilitiesNoncurrent'] = 0;
            $b_sheet[$sheet[$x]]['OtherNoncurrentFinancialLiabilities'] = 0;
            $b_sheet[$sheet[$x]]['OtherNoncurrentNonfinancialLiabilities'] = 0;
            $b_sheet[$sheet[$x]]['IssuedCapital'] = 0;
            $b_sheet[$sheet[$x]]['SharePremium'] = 0;
            $b_sheet[$sheet[$x]]['TreasuryShares'] = 0;
            $b_sheet[$sheet[$x]]['OtherEquityInterest'] = 0;
            $b_sheet[$sheet[$x]]['OtherReserves'] = 0;
            $b_sheet[$sheet[$x]]['RetainedEarnings'] = 0;
            $b_sheet[$sheet[$x]]['NoncontrollingInterests'] = 0;

            foreach($balance_sheet as $key => $bdata){
                if($key == 4){
                    $b_sheet[$sheet[$x]]['Year'] = isset($this->balance_sheet_year[$x-1]) ? $this->balance_sheet_year[$x-1] : 0;
                }

                if(count($bdata) > 0){
                    // Cash and Cash Equivalents
                    if(trim($bdata[0]) == "Total Bank Accounts"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['CashAndCashEquivalents'] = $bdata[$x];
                        }
                    }

                    // Trade and Other Current Receivables
                    if(trim($bdata[0]) == "Total Accounts Receivable"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['TradeAndOtherCurrentReceivables'] = $bdata[$x];
                        }
                    }

                    // Inventories
                    if(trim($bdata[0]) == "Inventory Asset"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['Inventories'] = $bdata[$x];
                        }
                    }

                    // Other Current Financial Assets
                    if(trim($bdata[0]) == "Undeposited Funds"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['OtherCurrentFinancialAssets'] = $bdata[$x];
                        }
                    }

                    // Property, Plant and Equipment
                    if(trim($bdata[0]) == "Total Fixed Assets"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['PropertyPlantAndEquipment'] = $bdata[$x];
                        }
                    }

                    // Trade and other current payables = Total Accounts Payable
                    if(trim($bdata[0]) == "Total Accounts Payable"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['TradeAndOtherCurrentPayables'] = $bdata[$x];
                        }
                    }

                    // Total Other Current Liabilities 
                    if(trim($bdata[0]) == "Total Other Current Liabilities"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['OtherCurrentFinancialLiabilities'] = $bdata[$x];
                        }
                    }

                    // Trade and other non-current payables
                    if(trim($bdata[0]) == "Notes Payable"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['NoncurrentPayables'] = $bdata[$x];
                        }
                    }

                    // Current provisions for employee 
                    if(trim($bdata[0]) == "Total Credit Cards"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['CurrentProvisionsForEmployeeBenefits'] = $bdata[$x];
                        }
                    }

                    // Retained Earnings
                    if(trim($bdata[0]) == "Opening Balance Equity" || trim($bdata[0]) == "Net Income"){
                        if(isset($bdata[$x])){
                            $b_sheet[$sheet[$x]]['RetainedEarnings'] += $bdata[$x];
                        }
                    }
                }
            }
        }
        if($this->report_index['balance_start_index'] == 3){
            $b_sheet[$sheet[0]]['Year'] = $this->balance_sheet_year[$x] - 1 ;
            $b_sheet[$sheet[0]]['CashAndCashEquivalents'] = 0;
            $b_sheet[$sheet[0]]['TradeAndOtherCurrentReceivables'] = 0;
            $b_sheet[$sheet[0]]['Inventories'] = 0;
            $b_sheet[$sheet[0]]['OtherCurrentFinancialAssets'] = 0;
            $b_sheet[$sheet[0]]['PropertyPlantAndEquipment'] = 0;
            $b_sheet[$sheet[0]]['TradeAndOtherCurrentPayables'] = 0;
            $b_sheet[$sheet[0]]['OtherCurrentFinancialLiabilities'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentPayables'] = 0;
            $b_sheet[$sheet[0]]['CurrentTaxAssetsCurrent'] = 0;
            $b_sheet[$sheet[0]]['CurrentBiologicalAssets'] = 0;
            $b_sheet[$sheet[0]]['OtherCurrentNonfinancialAssets'] = 0;
            $b_sheet[$sheet[0]]['CurrentNoncashAssetsPledgedAsCollateral'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentAssetsOrDisposalGroups'] = 0;
            $b_sheet[$sheet[0]]['InvestmentProperty'] = 0;
            $b_sheet[$sheet[0]]['Goodwill'] = 0;
            $b_sheet[$sheet[0]]['IntangibleAssetsOtherThanGoodwill'] = 0;
            $b_sheet[$sheet[0]]['InvestmentAccountedForUsingEquityMethod'] = 0;
            $b_sheet[$sheet[0]]['InvestmentsInSubsidiariesJointVenturesAndAssociates'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentBiologicalAssets'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentReceivables'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentInventories'] = 0;
            $b_sheet[$sheet[0]]['DeferredTaxAssets'] = 0;
            $b_sheet[$sheet[0]]['CurrentTaxAssetsNoncurrent'] = 0;
            $b_sheet[$sheet[0]]['OtherNoncurrentFinancialAssets'] = 0;
            $b_sheet[$sheet[0]]['OtherNoncurrentNonfinancialAssets'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentNoncashAssetsPledgedAsCollateral'] = 0;
            $b_sheet[$sheet[0]]['CurrentProvisionsForEmployeeBenefits'] = 0;
            $b_sheet[$sheet[0]]['OtherShorttermProvisions'] = 0;
            $b_sheet[$sheet[0]]['TradeAndOtherCurrentPayables'] = 0;
            $b_sheet[$sheet[0]]['CurrentTaxLiabilitiesCurrent'] = 0;
            $b_sheet[$sheet[0]]['OtherCurrentNonfinancialLiabilities'] = 0;
            $b_sheet[$sheet[0]]['LiabilitiesIncludedInDisposalGroups'] = 0;
            $b_sheet[$sheet[0]]['NoncurrentProvisionsForEmployeeBenefits'] = 0;
            $b_sheet[$sheet[0]]['OtherLongtermProvisions'] = 0;
            $b_sheet[$sheet[0]]['DeferredTaxLiabilities'] = 0;
            $b_sheet[$sheet[0]]['CurrentTaxLiabilitiesNoncurrent'] = 0;
            $b_sheet[$sheet[0]]['OtherNoncurrentFinancialLiabilities'] = 0;
            $b_sheet[$sheet[0]]['OtherNoncurrentNonfinancialLiabilities'] = 0;
            $b_sheet[$sheet[0]]['IssuedCapital'] = 0;
            $b_sheet[$sheet[0]]['SharePremium'] = 0;
            $b_sheet[$sheet[0]]['TreasuryShares'] = 0;
            $b_sheet[$sheet[0]]['OtherEquityInterest'] = 0;
            $b_sheet[$sheet[0]]['OtherReserves'] = 0;
            $b_sheet[$sheet[0]]['RetainedEarnings'] = 0;
            $b_sheet[$sheet[0]]['NoncontrollingInterests'] = 0;
        }

        // Match Income Statement Sheet
        $income_sheet = $this->getIncomeStatementSheetData();
        $i_sheet = [];
        $sheet = array("J", "I", "H");

        for($x=$this->report_index['income_start_index']; $x>=1; $x--){
            // Initialize income statement
            $i_sheet[$sheet[$x-1]]['OtherIncome'] = 0;
            $i_sheet[$sheet[$x-1]]['OtherGainsLosses'] = 0;
            // $i_sheet[$sheet[$x-1]]['ProfitLossFromOperatingActivities'] = 0;
            $i_sheet[$sheet[$x-1]]['DifferenceBetweenCarryingAmountOfDividendsPayable'] = 0;
            $i_sheet[$sheet[$x-1]]['GainsLossesOnNetMonetaryPosition'] = 0;
            $i_sheet[$sheet[$x-1]]['GainLossArisingFromDerecognitionOfFinancialAssets'] = 0;
            $i_sheet[$sheet[$x-1]]['FinanceCosts'] = 0;
            $i_sheet[$sheet[$x-1]]['ImpairmentLoss'] = 0;
            $i_sheet[$sheet[$x-1]]['ShareOfProfitLossOfAssociates'] = 0;
            $i_sheet[$sheet[$x-1]]['OtherIncomeFromSubsidiaries'] = 0;
            $i_sheet[$sheet[$x-1]]['GainsLossesArisingFromDifference'] = 0;
            $i_sheet[$sheet[$x-1]]['CumulativeGainPreviouslyRecognized'] = 0;
            $i_sheet[$sheet[$x-1]]['HedgingGainsForHedge'] = 0;
            $i_sheet[$sheet[$x-1]]['IncomeTaxExpenseContinuingOperations'] = 0;
            $i_sheet[$sheet[$x-1]]['ProfitLossFromDiscontinuedOperations'] = 0;
            $i_sheet[$sheet[$x-1]]['OtherComprehensiveIncome'] = 0;
            $i_sheet[$sheet[$x-1]]['Revenue'] = 0;
            $i_sheet[$sheet[$x-1]]['CostOfSales'] = 0;
            $i_sheet[$sheet[$x-1]]['AdministrativeExpense'] = 0;
            $i_sheet[$sheet[$x-1]]['OtherExpenseByFunction'] = 0;
            $i_sheet[$sheet[$x-1]]['FinanceIncome'] = 0;
            $i_sheet[$sheet[$x-1]]['OtherComprehensiveIncome'] = 0;

            foreach($income_sheet as $key => $idata){
                if($key == 4){
                    $i_sheet[$sheet[$x-1]]['Year'] = isset($this->income_statement_year[$x-1]) ? $this->income_statement_year[$x-1] : 0;
                }

                // Revenue
                if(trim($idata[0]) == "Total Income"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['Revenue'] = $idata[$x];
                    }
                }

                // Cost of Sa les
                if(trim($idata[0]) == "Total Cost of Goods Sold"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['CostOfSales'] = $idata[$x];
                    }
                }

                // Administrative expense 
                if(trim($idata[0]) == "Total Expenses"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['AdministrativeExpense'] = $idata[$x];
                    }
                }

                // Other expense 
                if(trim($idata[0]) == "Total Other Expenses"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['OtherExpenseByFunction'] = $idata[$x];
                    }
                }

                // Finance income
                if(trim($idata[0]) == "Net cash provided by financing activities"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['FinanceIncome'] = $idata[$x];
                    }
                }

                // Other comprehensive income
                if(trim($idata[0]) == "Net Other Income"){
                    if(isset($idata[$x])){
                        $i_sheet[$sheet[$x-1]]['OtherComprehensiveIncome'] = $idata[$x];
                    }
                }

            }
        }

        // Match Statement of Cash Flow
        $cashflow_sheet = $this->getCashFlowSheetData();
        $c_sheet = [];
        for($x=$this->report_index['cashflow_start_index']; $x>=1; $x--){
            // Initialize variable sheet
            $c_sheet[$sheet[$x-1]]['AmortisationExpense'] = 0;
            $c_sheet[$sheet[$x-1]]['DepreciationExpense'] = 0;
            $c_sheet[$sheet[$x-1]]['NetCashFlowFromOperations'] = 0;
            $c_sheet[$sheet[$x-1]]['Amortization'] = 0;
            $c_sheet[$sheet[$x-1]]['Depreciation'] = 0;
            $c_sheet[$sheet[$x-1]]['InterestExpense'] = 0;
            $c_sheet[$sheet[$x-1]]['NonCashExpenses'] = 0;
            $c_sheet[$sheet[$x-1]]['PrincipalPayments'] = 0;
            $c_sheet[$sheet[$x-1]]['InterestPayments'] = 0;
            $c_sheet[$sheet[$x-1]]['LeasePayments'] = 0;

            foreach($cashflow_sheet as $key => $cdata){
                if($key == 4){
                    $c_sheet[$sheet[$x-1]]['Year'] = isset($this->balance_sheet_year[$x-1]) ? $this->balance_sheet_year[$x-1] : 0;
                }

                // Net Cash provided by operating activities
                if(trim($cdata[0]) == "Net cash provided by operating activities"){
                    if(isset($cdata[$x])){
                        $c_sheet[$sheet[$x-1]]['NetCashByOperatingActivities'] = $cdata[$x];
                    }
                }
            }
        }

        $this->basic_fs_data =  array(
            "balance_sheet" => $b_sheet,
            "income_statement"  => $i_sheet,
            "cashflow"  => $c_sheet
        );

        return;
    }

    public function convertToBasicFSInputTemplate(){
        // Get report
        $data = $this->simpledata;
        $basic_data = $this->basic_fs_data;
        $quickbooksPath = public_path('documents/quickbooks');

        $balance_sheet_reference = array(
            'CashAndCashEquivalents' => 10,
            'TradeAndOtherCurrentReceivables' => 11,
            'Inventories' => 12,
            'OtherCurrentFinancialAssets' => 15,
            'PropertyPlantAndEquipment' => 22,
            'TradeAndOtherCurrentPayables' => 45,
            'OtherCurrentFinancialLiabilities' => 47,
            'NoncurrentPayables' => 55,
            'CurrentTaxAssetsCurrent' => 13,
            'CurrentBiologicalAssets' => 14,
            'OtherCurrentNonfinancialAssets' => 16,
            'CurrentNoncashAssetsPledgedAsCollateral' => 17,
            'NoncurrentAssetsOrDisposalGroups' => 18,
            'InvestmentProperty' => 23,
            'Goodwill' => 24,
            'IntangibleAssetsOtherThanGoodwill' => 25,
            'InvestmentAccountedForUsingEquityMethod' => 26,
            'InvestmentsInSubsidiariesJointVenturesAndAssociates' => 27,
            'NoncurrentBiologicalAssets' => 28,
            'NoncurrentReceivables' => 29,
            'NoncurrentInventories' => 30,
            'DeferredTaxAssets' => 31,
            'CurrentTaxAssetsNoncurrent' => 32,
            'OtherNoncurrentFinancialAssets' => 33,
            'OtherNoncurrentNonfinancialAssets' => 34,
            'NoncurrentNoncashAssetsPledgedAsCollateral' => 35,
            'CurrentProvisionsForEmployeeBenefits' => 43,
            'OtherShorttermProvisions' => 44,
            'TradeAndOtherCurrentPayables' => 45,
            'CurrentTaxLiabilitiesCurrent' => 46,
            'OtherCurrentNonfinancialLiabilities' => 48,
            'LiabilitiesIncludedInDisposalGroups' => 49,
            'NoncurrentProvisionsForEmployeeBenefits' => 53,
            'OtherLongtermProvisions' => 54,
            'DeferredTaxLiabilities' => 56,
            'CurrentTaxLiabilitiesNoncurrent' => 57,
            'OtherNoncurrentFinancialLiabilities' => 58,
            'OtherNoncurrentNonfinancialLiabilities' => 59,
            'IssuedCapital' => 63,
            'SharePremium' => 64,
            'TreasuryShares' => 65,
            'OtherEquityInterest' => 66,
            'OtherReserves' => 67,
            'RetainedEarnings' => 68,
            'NoncontrollingInterests' => 69,
        );

        $income_sheet_reference = array(
            'Revenue' => 8,
            'CostOfSales' => 9,
            'AdministrativeExpense' => 14,
            'OtherExpenseByFunction' => 15,
            'FinanceIncome' => 22,
            'OtherIncome' => 12,
            'OtherGainsLosses' => 16,
            'DifferenceBetweenCarryingAmountOfDividendsPayable' => 19,
            'GainsLossesOnNetMonetaryPosition' => 20,
            'GainLossArisingFromDerecognitionOfFinancialAssets' => 21,
            'FinanceCosts' => 23,
            'ImpairmentLoss' => 24,
            'ShareOfProfitLossOfAssociates' => 25,
            'OtherIncomeFromSubsidiaries' => 26,
            'GainsLossesArisingFromDifference' => 27,
            'CumulativeGainPreviouslyRecognized' => 28,
            'HedgingGainsForHedge' => 29,
            'IncomeTaxExpenseContinuingOperations' => 32,
            'ProfitLossFromDiscontinuedOperations' => 35,
            'OtherComprehensiveIncome' => 38
        );

        $cashflow_sheet_reference = array(
            'NetCashByOperatingActivities' => 53,
            'AmortisationExpense' => 43,
            'DepreciationExpense' => 44,
            'NetCashFlowFromOperations' => 45,
            'Amortization' => 46,
            'Depreciation' => 47,
            'InterestExpense' => 48,
            'NonCashExpenses' => 49,
            'PrincipalPayments' => 50,
            'InterestPayments' => 51,
            'LeasePayments' => 52,
        );

        // Copy the basic FS Input Template to quickbook zip file for temp
        $sourceFilePath=public_path("documents/FSInputTemplate.xls");
        $destinationPath = $quickbooksPath.'/'.$data['entity']['entityid'].'/'. $data['entity']['entityid'] . ' - Basic FS Input Template.xlsx';
        File::copy($sourceFilePath, $destinationPath);

        // get highest year
        $highest_year = $this->balance_sheet_year[count($this->balance_sheet_year)-1];

        // Fill up basic FS Input Template
        Excel::load($destinationPath, function($doc) use ($basic_data, $data, $highest_year, $balance_sheet_reference, $income_sheet_reference, $cashflow_sheet_reference) {
            $sheet = $doc->getActiveSheet(0);
            // Set Company name
            $sheet->setCellValue('B2', $data['entity']['companyname']);

            // Set FS Year
            $sheet->setCellValue('B6', $highest_year);

            // Set balance sheet
            foreach($basic_data['balance_sheet'] as $key => $balance_value){
                foreach($balance_value as $b_sheet => $b_value){
                    if($b_sheet == 'Year'){
                        continue;
                    }
                    $cell = $key . $balance_sheet_reference[$b_sheet]; // get cell
                    $sheet->setCellValue($cell, $b_value); // set cell value
                }
            }

            // Set income statement
            foreach($basic_data['income_statement'] as $key => $income_value){
                foreach($income_value as $i_sheet => $i_value){
                    if($i_sheet == 'Year'){
                        continue;
                    }
                    $cell = $key . $income_sheet_reference[$i_sheet]; // get cell
                    $sheet->setCellValue($cell, $i_value); // set cell value
                }
            }

            // Set cashflow
            foreach($basic_data['cashflow'] as $key => $cashflow_value){
                foreach($cashflow_value as $c_sheet => $c_value){
                    if($i_sheet == 'Year'){
                        continue;
                    } 
                    $cell = $key . $cashflow_sheet_reference[$c_sheet]; // get cell
                    $sheet->setCellValue($cell, $c_value); // set cell value
                }
            }
        })->store('xlsx', $quickbooksPath . '/' . $data['entity']['entityid']);

        $this->basic_file = $destinationPath;

        return;
    }

    public function deleteOldQuickBook(){
        $data = $this->simpledata;
        $entity_id = $data['entity']['entityid'];
        $quickbook = DB::table('tbldocument')
                            ->where('entity_id', $entity_id)
                            ->where('document_group', 45)
                            ->where('is_deleted', 0)
                            ->first();
        if($quickbook){
            // delete records and files
            DB::table('tbldocument')->where('entity_id', $data['entity']['entityid'])->where('document_group', 45)->update(['is_deleted' => 1]);
            $filepath = public_path('documents/quickbooks/'. $quickbook->document_orig);
            if(file_exists($filepath)){
                File::delete($filepath);
            }
        }
        return;   
    }
}
