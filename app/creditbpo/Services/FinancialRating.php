<?php

namespace CreditBPO\Services;

class FinancialRating {

    public static function getPositionRating($value) {
        $score = self::getPositionScore($value);

        return [
            'score' => $score,
            'color' => self::getPositionColor($score),
        ];
    }

    public static function getPositionScore($value) {
        $value = intval($value);

        if ($value > 161) {
            return 'AAA';
        }

        if ($value > 144) {
            return 'AA';
        }

        if ($value > 126) {
            return 'A';
        }

        if ($value > 109) {
            return 'BBB';
        }

        if ($value > 91) {
            return 'BB';
        }

        if ($value > 74) {
            return 'B';
        }

        if ($value > 56) {
            return 'CCC';
        }

        if ($value > 39) {
            return 'CC';
        }

        if ($value > 21) {
            return 'C';
        }

        if ($value > 3) {
            return 'D';
        }

        return 'E';
    }

    public static function getPositionColor($score) {
        if (in_array($score, ['AAA', 'AA', 'A'])) {
            return 'green';
        }

        if (in_array($score, ['BBB', 'BB', 'B'])) {
            return 'orange';
        }

        return 'red';
    }

    public static function getRiskScoreTotal($score) {
        $score = self::getRatingScore($score);

        return [
            'score' => $score.'-'.self::getRiskScoreTotalText($score),
            'color' => self::getRatingColor($score),
        ];
    }

    public static function getRiskScoreTotalText($score) {
        if ($score === 'AA') {
            return 'Excellent';
        }

        if ($score === 'A') {
            return 'Strong';
        }

        if ($score === 'BBB') {
            return 'Good';
        }

        if ($score === 'BB') {
            return 'Satisfactory';
        }

        if ($score === 'B') {
            return 'Some Vulnerability';
        }

        return 'Watchlist';
    }

    public static function getRating($score) {
        $score = self::getRatingScore($score);

        return [
            'score' => $score,
            'class' => self::getRatingClass($score),
            'color' => self::getRatingColor($score),
            'description' => self::getRatingDescription($score),
        ];
    }

    public static function getRatingScore($score) {
        $score = intval($score);

        if ($score > 176) {
            return 'AA';
        }

        if ($score > 149) {
            return 'A';
        }

        if ($score > 122) {
            return 'BBB';
        }

        if ($score > 95) {
            return 'BB';
        }

        if ($score > 68) {
            return 'B';
        }

        return 'CCC';
    }

    public static function getRatingColor($score) {
        // return like this for now since they share the same logic
        return self::getPositionColor($score);
    }

    public static function getRatingDescription($score) {
        if (in_array($score, ['AAA', 'AA', 'A'])) {
            return trans('risk_rating.cbr_high');
        }

        if (in_array($score, ['BBB', 'BB', 'B'])) {
            return trans('risk_rating.cbr_moderate');
        }

        return trans('risk_rating.crb_low');
    }

    public static function getRatingClass($score) {
        if (in_array($score, ['AAA', 'AA', 'A'])) {
            return 'gcircle-container';
        }

        if (in_array($score, ['BBB', 'BB', 'B'])) {
            return 'ocircle-container';
        }

        return 'circle-container';
    }
}
