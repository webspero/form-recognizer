<?php

namespace CreditBPO\Services;

use User;
use Mail;


use DB;

class ClientKeyAvailNotifService extends BaseCommand {
    protected $config;

    public function __construct($command) {
        parent::__construct($command);
    }

    public function sendNotification() {
        $emailList = DB::table('tblnotif')
                        ->join('tblemailnotif', 'tblemailnotif.bank_id', '=', DB::raw('tblnotif.bank_id'))
                        ->where('tblemailnotif.status', '=', 1)
                        ->where('tblnotif.notif_keys_available', '=', 1)
                        ->get();

        foreach($emailList as $email){
            $bankDetails = DB::table('tblbank')
                            ->where('id', $email->bank_id)->first();

            $clientKeyCount = DB::table('bank_keys')
                                ->where('bank_id', $email->bank_id)->where('is_used', 0)->count();
            
            $user = new User();
            $user->firstname = "Supervisor";
            $user->lastname = $bankDetails->bank_name;
            $user->clientKeyCount = $clientKeyCount;

            if (env("APP_ENV") == "Production") {

                try{
                    Mail::send('emails.notifications.client-key-avail-notification',
                        array(
                            'user' => $user
                        ),
                        function($message) use ($email) {
                            $message->to($email->email, $email->email)
                                ->subject('CreditBPO Notification: Client Keys Available');
                        }
                    );

                }catch(Exception $e){
                //
                }
            }
        }
    }
}
