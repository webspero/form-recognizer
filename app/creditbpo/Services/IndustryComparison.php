<?php

namespace CreditBPO\Services;
use Entity;
use FinancialReport;
use CreditBPO\Models\FinancialReport as FRModels;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use DB;

class IndustryComparison {

    protected $reportId;

    public function setReportId($reportId) {
        $this->reportId = $reportId;
    }

    public function getReportId() {
        return $this->reportId;
    }


    public function __construct() {
        //
    }
    

    public function getIndustryComparisonReport($id){
        $financial = new FRModels();
        $financialStatement = $financial->getFinancialReportById($id);

        $fa_report = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->orderBy('id', 'desc')->first();

        $company_industry = array(
            'gross_revenue_growth'  => 0,
            'net_income_growth'     => 0,
            'gross_profit_margin'   => 0,
            'current_ratio'         => 0,
            'debt_to_equity'        => 0
        );

        if($fa_report){
            $fa_report->balance_sheets      = $financialStatement['balance_sheets'];
            $fa_report->income_statements   = $financialStatement['income_statements'];
            $fa_report->cashflow            = $financialStatement['cashflow'];
            $company_industry = array();
    
            $max_count = count($fa_report->income_statements)-1;
    
            /** Company Gross Revenue Growth */
            $company_industry['gross_revenue_growth'] = 0;
            if( (isset($fa_report->income_statements[$max_count]->Revenue)) && ($fa_report->income_statements[$max_count]->Revenue != 0)){
                $PercentageChange = 0;
                $company_industry['gross_revenue_growth'] = 0;
                $dividend = $fa_report->income_statements[$max_count]->Revenue;
    
                if($dividend != 0  && $fa_report->income_statements[$max_count]->Revenue != 0){
                    $PercentageChange = (($fa_report->income_statements[0]->Revenue - $fa_report->income_statements[$max_count]->Revenue) /  $dividend) * 100;
                }
    
                if($PercentageChange > 200){
                    $company_industry['gross_revenue_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
                }else{
                    $company_industry['gross_revenue_growth'] = round($PercentageChange,2);
                }
            }
    
            /** Company Net Income Growth */
            $company_industry['net_income_growth'] = 0;
            if( isset($fa_report->income_statements[$max_count]->ProfitLoss) && (($fa_report->income_statements[$max_count]->ProfitLoss) != 0)){
                $PercentageChange = 0;
                $dividend = ($fa_report->income_statements[$max_count]->ProfitLoss);
    
                if($dividend != 0  && $fa_report->income_statements[$max_count]->ProfitLoss != 0){
                    $PercentageChange = (($fa_report->income_statements[0]->ProfitLoss - $fa_report->income_statements[$max_count]->ProfitLoss) /  $dividend) * 100;
                }
    
                if($PercentageChange > 200){
                    $company_industry['net_income_growth'] = (round((($PercentageChange / 100) + 1), 2)*100);
                }else{
                    $company_industry['net_income_growth'] = round($PercentageChange,2);
                }
            }
    
            /** Company Gross Profit Margin */
            $company_industry['gross_profit_margin'] = 0;
            if($max_count >= 0){
                for($x=$max_count; $x >= 0 ; $x--){
                    if($fa_report->income_statements[$x]->Revenue != 0){
                        $company_industry['gross_profit_margin'] = ($fa_report->income_statements[$x]->GrossProfit / $fa_report->income_statements[$x]->Revenue) * 100;
    
                        if($company_industry['gross_profit_margin'] == 0){
                            $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                        }else{
                            $company_industry['gross_profit_margin'] = round($company_industry['gross_profit_margin'], 2);
                            $company_industry['gross_profit_margin'] = (int)$company_industry['gross_profit_margin'];
                        }
                        
                    }
                }
            }
    
            /** Company Current Ratio */    
            $company_industry['current_ratio'] = 0;
            $i=0;
            for($x=@count($fa_report->balance_sheets) - 1; $x >= 0 ; $x--){
                $currentAssets = $fa_report->balance_sheets[$x]->CurrentAssets;
                $currentLiabilities = $fa_report->balance_sheets[$x]->CurrentLiabilities;
    
                    // Curretn ratio, ∞ < crit. < 1 ≤ unsat. < 2 ≤ good < 2.1 ≤ excel. < ∞
                if($currentLiabilities > 0){
                    $company_industry['current_ratio'] = $currentAssets / $currentLiabilities;
                    $company_industry['current_ratio'] = round($company_industry['current_ratio'], 2, PHP_ROUND_HALF_DOWN);
                }
                $i++;
            }
    
            /** Company Debt-to-Equity Ratio */
            $company_industry['debt_to_equity'] = 0;
            $sustainability = FinancialAnalysisInternal::financialSustainability($fa_report);
            if(isset($sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage)){
                $company_industry['debt_to_equity'] = $sustainability['financial_table'][count($sustainability['financial_table'])-1]->FinancialLeverage;
            }
            
    
            /** Update readatio data */
            DB::table('tblreadyrationdata')->where('entityid', $id)->update([
                'gross_revenue_growth2' => $company_industry['gross_revenue_growth'],
                'net_income_growth2'    => $company_industry['net_income_growth'],
                'gross_profit_margin2'  => $company_industry['gross_profit_margin'],
                'current_ratio2'        => $company_industry['current_ratio'],
                'debt_equity_ratio2'    => $company_industry['debt_to_equity'],
                'is_updated'            => 1
            ]);
        }
        return $company_industry;
    }

    public function getIndustryComparison($reportid){
        $this->setReportId($reportid);
        /** Initialization */

        $ent = Entity::where('entityid', $this->reportId)->first(); /** Get information of the report */
        $year = FinancialReport::where('entity_id', $this->reportId)->pluck('year')->first();
        $ind_comp = null;
        $tier = ['class', 'group', 'sub', 'main'];

        foreach($tier as $t){
            /** Check industry class */
            if($t == "class"){
                $ind_id = $ent->industry_row_id;
                // Check if industry class code exist
                $c_class = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->get();
                if($c_class){
                    // Check year exist
                    $y_class = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $year)->first();
                    if(!$y_class){
                        $closest_year = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $ind_comp = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $ind_comp = DB::table('tblindustryclass_forecast')->where('class_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }

            /** Check industry group */
            // if($t == "group"){
            //     $ind_id = $ent->industry_row_id;
            //     // Check if industry group code exist
            //     $c_group = DB::table('tblindustrygroup_forecast')->where('group_code', $ind_id)->get();
            //     if($c_group){
            //         // Check year exist
            //         $y_group = DB::table('tblindustrygroup_forecast')->where('group_code', $ind_id)->where('year', $year)->first();
            //         if(!$y_group){
            //             $closest_year = DB::table('tblindustrygroup_forecast')->where('group_code', $ind_id)->pluck('year');

            //             /** Get the closest year */
            //             $closest = null;
            //             foreach ($closest_year as $item) {
            //                 if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
            //                     $closest = $item;
            //                 }
            //             }
            //             $new_year = $closest;
            //             $ind_comp = DB::table('tblindustrygroup_forecast')->where('group_code', $ind_id)->where('year', $new_year)->first();
            //         }else{
            //             $ind_comp = DB::table('tblindustrygroup_forecast')->where('group_code', $ind_id)->where('year', $year)->first();
            //         }
            //     }
            // }

            /** Check industry division */
            if($t == "sub"){
                $ind_id = $ent->industry_sub_id;
                // Check if industry sub code exist
                $c_sub = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->get();
                if($c_sub){
                    // Check year exist
                    $y_sub = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $year)->first();
                    if(!$y_sub){
                        $closest_year = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $ind_comp = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $ind_comp = DB::table('tblindustrysub_forecast')->where('sub_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }

            if($t == "main"){
                $ind_id = $ent->industry_main_id;
                // Check if industry main code exist
                $c_main = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->get();
                if($c_main){
                    // Check year exist
                    $y_main = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $year)->first();
                    if(!$y_main){
                        $closest_year = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->pluck('year');

                        /** Get the closest year */
                        $closest = null;
                        foreach ($closest_year as $item) {
                            if ($closest === null || abs((int)$year - $closest) > abs($item - (int)$year)) {
                                $closest = $item;
                            }
                        }
                        $new_year = $closest;
                        $ind_comp = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $new_year)->first();
                    }else{
                        $ind_comp = DB::table('tblindustrymain_forecast')->where('main_code', $ind_id)->where('year', $year)->first();
                    }
                }
            }

            if($ind_comp){
                if($ind_comp->gross_revenue_growth == 0.0000 &&
                    $ind_comp->net_income == 0.0000 &&
                    $ind_comp->gross_profit_margin == 0.0000 &&
                    $ind_comp->current_ratio == 0.0000 &&
                    $ind_comp->debt_equity_ratio == 0.000){
                    continue;
                }else{
                    break;
                }
            }
        }

        $industry_comparison = array();

        /** Set Value for Industry Line Graph */
        $industry_comparison['gross_revenue_growth'] = ($ind_comp) ? ($ind_comp->gross_revenue_growth) : 0;
        $industry_comparison['net_income_growth'] = ($ind_comp) ? ($ind_comp->net_income) : 0;
        $industry_comparison['gross_profit_margin']  = ($ind_comp) ? ($ind_comp->gross_profit_margin) : 0;
        $industry_comparison['current_ratio'] = ($ind_comp) ? ($ind_comp->current_ratio) : 0;
        $industry_comparison['debt_equity_ratio']    = ($ind_comp) ? ($ind_comp->debt_equity_ratio) : 0;
        $industry_comparison['year']    = ($ind_comp) ? ($ind_comp->year) : 0;

        return $industry_comparison;
    }
}
