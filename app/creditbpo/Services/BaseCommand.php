<?php

namespace CreditBPO\Services;

use CreditBPO\Libraries\Scraper;

abstract class BaseCommand {
    const REASON_NO_CONFIG = 1;
    const REASON_TIMED_OUT = 2;
    const REASON_DOWNLOAD_LINK_MISSING = 3;
    const REASON_DOWNLOAD_TIMED_OUT = 4;
    const REASON_FILE_NOT_DOWNLOADED = 5;

    protected $command;
    protected $scraper;

    public function __construct($command) {
        $this->tempDirectory = public_path().'/temp/';

        $this->scraper = new Scraper();

        if ($command) {
            $this->command = $command;
        }
    }

    public function commandLog($message) {
        if ($this->command) {
            $this->command->info($message);
        }
    }

    public function commandError($message) {
        if ($this->command) {
            $this->command->error($message);
        }
    }

    /**
     * @param string $reason
     * @return string
     */
    protected function getErrorMessage($reason) {
        $reasonMessage = '';

        switch ($reason) {
            case self::REASON_NO_CONFIG:
                $reasonMessage = 'No config present';
            break;
            case self::REASON_TIMED_OUT:
                $reasonMessage = 'Request timed out for sector page';
            break;
            case self::REASON_DOWNLOAD_LINK_MISSING:
                $reasonMessage = 'Download link not found';
            break;
            case self::REASON_DOWNLOAD_TIMED_OUT:
                $reasonMessage = 'Request timed out when downloading file';
            break;
            case self::REASON_FILE_NOT_DOWNLOADED:
                $reasonMessage = 'File not downloaded';
            break;
            default:
                $reasonMessage = 'Unknown error has occured';
            break;
        }

        return sprintf("Error: %s.\n", $reasonMessage);
    }
}