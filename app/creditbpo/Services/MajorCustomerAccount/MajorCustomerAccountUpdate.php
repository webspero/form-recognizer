<?php

namespace CreditBPO\Services\MajorCustomerAccount;

use Illuminate\Support\Facades\Validator;
use CreditBPO\Models\MajorCustomerAccount;
use CreditBPO\Models\Entity as Report;

class MajorCustomerAccountUpdate
{
    public function __construct()
    {
        $this->majorCustomerAccountDb = new MajorCustomerAccount();
        $this->reportDb = new Report();
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            return true;
        } else{
            return false;
        }
    }

    public function validateRequestDetails($isAgree)
    {
        if (!empty($isAgree)){
            if (!is_bool($isAgree)){
                return "Use only boolean type values true or false to answer 'No individual customer account for more than 1% of sales.'";
            } else{
                return null;
            }
        } else{
            return 'Agree value is required.';
        }
    }

    public function updateMajorCustomerAccount($reportId, $isAgree)
    {
        /* Check if update or insert */
        $majorCustomer = $this->majorCustomerAccountDb->getFirstCustomerAccountByReportId($reportId);
        if (!is_null($majorCustomer)){
            /* update */
            $update = $this->majorCustomerAccountDb->updateMajorCustomerAccount($majorCustomer, $isAgree);
            return $update;
        } else{
            /* create */
            $create = $this->majorCustomerAccountDb->addMajorCustomerAccount($reportId, $isAgree);
            return $create;
        }
    }
}