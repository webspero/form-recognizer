<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use CreditBPO\Libraries\Scraper;
use CreditBPO\Libraries\StringHelper;
use DB;

class BankInterestRates {
    const BANK_LOCAL = 'local';
    const BANK_FOREIGN = 'foreign';

    protected $config;
    protected $scraper;

    public function __construct() {
        $this->config = Config::get('cron.update-bank-interest-rate');
        $this->scraper = new Scraper();
    }

    /**
     * @return array
     */
    public function getDefaultValues() {
        $config = Config::get('services.bank-interest-rate');
        return $config;
    }

    /**
     * Scrape site to get Bank Interest Rates
     */
    public function scrapeInterestRates() {
    	
        $scraper = $this->scraper;
        $response = $scraper->fetch('https://www.bsp.gov.ph/Statistics/Financial%20System%20Accounts/intrates.aspx');
        $contentTypes = $response->getHeader('Content-Type');
        
        if (
            $response->getStatusCode() !== 200
        ) {
            return false;
        }

        $htmlText = $response->getBody()->getContents();
        $document = $scraper->getDomDocument($htmlText);
        $elements = $scraper->getNodes($document, '*//tr');
        $start = false;
        $type = null;
        $local = [];
        $foreign = [];

        $foreign_banks = DB::table('foreign_banks')->get()->toArray();
        $banks = [];
        foreach($foreign_banks as $fbanksVal){
	    	$fbanks = str_replace(' - Manila Branch','',$fbanksVal->bank);
	        $fbanks = str_replace(', Manila Branch','',$fbanks);
	        $fbanks = str_replace('-Manila Branch','',$fbanks);
	        $fbanks = str_replace('Manila Branch','',$fbanks);
	        $compressedFbanks = str_replace(' ','',$fbanks);
	        $compressedFbanks = str_replace(',','',$compressedFbanks);
	        $compressedFbanks = str_replace('-','',$compressedFbanks);
	        $compressedFbanks = str_replace('.','',$compressedFbanks);
	        $compressedFbanks = str_replace('(','',$compressedFbanks);
	        $compressedFbanks = str_replace(')','',$compressedFbanks);
	        $compressedFbanks = strtolower($compressedFbanks);
	        $compressedFbanks = str_replace('manilabranch','',$compressedFbanks);

	        $banks[] = $compressedFbanks;
    	}

    	$bankGiven = [];
    	foreach ($elements as $element) {
            $columns = $element->getElementsByTagName('td');

            $column = $this->cleanTags($document, 'span|br', $columns->item(0));
            $label = $this->cleanLabel($column->textContent);

            if(strpos($label,'EFFECTIVELENDINGRATES') > -1){
            	$start = false;
            	continue;
            }

            if(strpos($label,'fortheweek') > -1){
            	$start = false;
            	continue;
            }

            if(strpos($label,'inpercent') > -1){
            	$start = false;
            	continue;
            }

            if(strpos($label,'No.') > -1){
            	$start = false;
            	continue;
            }

            if($label == ''){
            	$start = false;
            	continue;
            }

            $bankColumn = $this->cleanTags($document, 'span|br', $columns->item(1));
            
            if(isset($bankColumn->textContent)){
            	$bankLabel = $this->cleanLabel($bankColumn->textContent);
            	$bankLabel = str_replace(' - Manila Branch','',$bankLabel);
	            $bankLabel = str_replace(', Manila Branch','',$bankLabel);
	            $bankLabel = str_replace('-Manila Branch','',$bankLabel);
	            $bankLabel = str_replace('Manila Branch','',$bankLabel);
	            $compressedLabel = str_replace(' ','',$bankLabel);
	            $compressedLabel = str_replace(',','',$compressedLabel);
	            $compressedLabel = str_replace('-','',$compressedLabel);
	            $compressedLabel = str_replace('.','',$compressedLabel);
	            $compressedLabel = str_replace('(','',$compressedLabel);
		        $compressedLabel = str_replace(')','',$compressedLabel);
	            $compressedLabel = strtolower($compressedLabel);
	            $compressedLabel = str_replace('manilabranch','',$compressedLabel);
	            if($compressedLabel == '') continue;
            }else
            {
            	continue;
            }
                        
            $shortHighLabel = '';
            $shortHighColumn = $this->cleanTags($document, 'span|br', $columns->item(3));
            if(isset($shortHighColumn->textContent))
            	$shortHighLabel = $this->cleanLabel($shortHighColumn->textContent);	
            if($shortHighLabel == '-' || $shortHighLabel == '') $shortHighLabel = 0;

            $shortLowLabel = '';
            $shortLowColumn = $this->cleanTags($document, 'span|br', $columns->item(4));
            if(isset($shortLowColumn->textContent))
            	$shortLowLabel = $this->cleanLabel($shortLowColumn->textContent);
            if($shortLowLabel == '-' || $shortLowLabel == '') $shortLowLabel = 0;

            $medHighLabel = '';
            $medHighColumn = $this->cleanTags($document, 'span|br', $columns->item(6));
            if(isset($medHighColumn->textContent))
            	$medHighLabel = $this->cleanLabel($medHighColumn->textContent);
            if($medHighLabel == '-' || $medHighLabel == '') $medHighLabel = 0;

            $medLowLabel = '';
            $medLowColumn = $this->cleanTags($document, 'span|br', $columns->item(7));
            if(isset($medLowColumn->textContent))
            	$medLowLabel = $this->cleanLabel($medLowColumn->textContent);
            if($medLowLabel == '-' || $medLowLabel == '') $medLowLabel = 0;
            
            $longHighabel = '';
            $longHighColumn = $this->cleanTags($document, 'span|br', $columns->item(9));
            if(isset($longHighColumn->textContent))
            	$longHighabel = $this->cleanLabel($longHighColumn->textContent);
            if($longHighabel == '-' || $longHighabel == '') $longHighabel = 0;

            $longLowLabel = '';
            $longLowColumn = $this->cleanTags($document, 'span|br', $columns->item(10));
            if(isset($longLowColumn->textContent))
            	$longLowLabel = $this->cleanLabel($longLowColumn->textContent);
            if($longLowLabel == '-' || $longLowLabel == '') $longLowLabel = 0;

            $itemsRaw = [
            	$shortHighLabel,
            	$shortLowLabel,
            	$medHighLabel,
            	$medLowLabel,
            	$longHighabel,
            	$longLowLabel
            ];

            if(array_sum($itemsRaw) == 0) continue;

            $items = $itemsRaw;

            if(in_array($compressedLabel,$banks)){
	    		$foreign[] = $items;
	    		
	    	}else{
	    		$local[] = $items;
	    	}
        }

        return [
            'local' => $this->getLowMidHighValues($local),
            'foreign' => $this->getLowMidHighValues($foreign),
        ];

    }

    /**
     * Function that cleans DOM elements based on selectors
     *
     * @param DomDocument $document
     * @param DomNode $parent
     * @param string $selector
     * @return DomNode
     */
    protected function cleanTags($document, $selector, $parent) {
        $scraper = $this->scraper;
        $nodes = $scraper->getNodes($document, $selector, $parent);

        foreach ($nodes as $node) {
            $parent->removeChild($node);
        }

        return $parent;
    }

    /**
     * Remove new line, carriage returns and empty spaces from string
     *
     * @param string $label
     * @return string
     */
    protected function cleanLabel($label) {
        $cleaned = str_replace("\r\n", '', $label);
        $cleaned = str_replace(" ", '', $label);
        $cleaned = str_replace(' ', '', $cleaned);
        return trim($cleaned);
    }

    /**
     *
     */
    public function getLowMidHighValues($values) {
        $low = [];
        $mid = [];
        $high = [];

        foreach ($values as $key => $value) {
            $this->assignToArray($low, $value[0]);
            $this->assignToArray($low, $value[1]);
            $this->assignToArray($mid, $value[2]);
            $this->assignToArray($mid, $value[3]);
            $this->assignToArray($high, $value[4]);
            $this->assignToArray($high, $value[5]);
        }

        return [
            'low' => $this->getArrayAverage($low),
            'mid' => $this->getArrayAverage($mid),
            'high' => $this->getArrayAverage($high),
        ];
    }

    /**
     *
     */
    public function assignToArray(&$arr, $value) {
        if ((float) $value > 0) {
            $arr[] = $value;
        }
    }

    /**
     *
     */
    public function getArrayAverage($arr) {
        if (@count($arr) > 2) {
            asort($arr);
            array_pop($arr);
            array_shift($arr);
        }

        if(@count($arr) > 0) {
            return (float) (array_sum($arr) / @count($arr));
        } else {
            return 0;
        }
    }
}