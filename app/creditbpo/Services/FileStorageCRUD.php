<?php

namespace CreditBPO\Services\BusinessDetailsII;

use CreditBPO\Models\fileStorage;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\StringManipulator;
use CreditBPO\DTO\fileStoragesDTO;

class FileStoragesCRUD
{
    public function __construct()
    {
        $this->fileStoragesDb = new fileStorage();
        $this->reportDb = new Report();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validatefileStorages($fileStorage)
    {

        $errors = [];
        
        /* Sets Validation Messages */
        $messages = array(
            'loginid.required' => 'Login ID is required.',
            'uploadedfile.required' => 'Upload file is required.',
            'uploadedfile.mimes' => 'Upload File file type should be doc,docx,pdf,jpeg,png,xls,xlsx.'
        );

        /* Sets Validation Rules */
        $rules = array(
            'loginid' => 'required',
            'uploadedfile' => 'required|mimes:doc,docx,pdf,jpeg,png,xls,xlsx'
        );

        /* Run Laravel Validation */
        $validator = Validator::make($fileStorage, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors
        ];

        return $result;
    }

    public function fileStoragesInsert($fileStorageDTO)
    {   
    	//print_r($fileStorageDTO); exit;
        /* Save data to database */
        $ad_data = [
        	'entity_id' => $fileStorageDTO['reportid'],
        	'file_name' => $fileStorageDTO['uploadedfile']
        ];

        $uploadFile = request()->file('uploadedfile');
        $extension = $uploadFile->extension();
        $filename = $uploadFile->getClientOriginalName().$extension;

        $destinationPath = public_path('associated_files/'.$fileStorageDTO['reportid'].'/');
        
        if (!file_exists(public_path('associated_files/'))) {
            mkdir(public_path('associated_files/'.$fileStorageDTO['reportid'].'/'));
        }

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath,0777);
        }

        if($uploadFile){
	        
	        if($uploadFile->move($destinationPath, $filename)){
				
	        	$fileStorageDTO['uploadedfile'] = $filename;
	        	$ad_data['file_name'] = $fileStorageDTO['uploadedfile'];
	        }
    	}

        $fileStorageDTOFinal = new fileStoragesDTO();
        $fileStorageDTOFinal->setVars($fileStorageDTO);
        $insertResponse = $this->fileStoragesDb->insertGetId($ad_data);

        if (!empty($insertResponse)){
        	$fileStorageDTOFinal->setId($insertResponse);
        }

        /* Convert database value to readable response */
        return $fileStorageDTOFinal;
    }

    public function fileStoragesUpdate($fileStorageDTO)
    {   
        /* Save data to database */
        $ad_data = [
        	'entity_id' => $fileStorageDTO->reportid,
        	'file_name' => $fileStorageDTO->uploadedfile
        ];

        $uploadFile = request()->file('uploadedfile');
        //$extension = $uploadFile->extension();
        $filename = $uploadFile->getClientOriginalName();

        $destinationPath = public_path('associated_files/'.$fileStorageDTO->reportid.'/');
        
        if (!file_exists(public_path('associated_files/'))) {
            mkdir(public_path('associated_files/'.$fileStorageDTO->reportid.'/'));
        }

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath,0777);
        }

        if($uploadFile){
	        
	        if($uploadFile->move($destinationPath, $filename)){
				$fileStorageDTO->uploadedfile = $filename;
	        	$ad_data['file_name'] = $fileStorageDTO->uploadedfile;
	        }
    	}

        $fileStorageDTOFinal = new fileStoragesDTO();
        $fileStorageDTOFinal->setVars((array) $fileStorageDTO);
        
        $udpateResponse = $this->fileStoragesDb->where('id',$fileStorageDTO->id)->update($ad_data);

        if (!empty($udpateResponse)){
        	$fileStorageDTOFinal->setId($fileStorageDTO->id);
        }

        /* Convert database value to readable response */
        return $fileStorageDTOFinal;
    }

    public function fileStoragesDelete($id)
    {   
       $deleteResponse = $this->fileStoragesDb->where('id',$id)->update(['is_deleted' => 1]);

       /* Convert database value to readable response */
       return $deleteResponse;
    }

    public function getfileStoragesByPage($reportId, $page, $limit)
    {
    	
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;
        $results['items'] = array();

        /* Get report fileStorages in tblrelatedcompanies table */
        $fileStoragesList = $this->fileStoragesDb->getfileStoragesByPage($reportId, $page, $limit);

        /* Change relationship satisfaction, order frequency and payment behavior to readable string */
        $fileStoragesDtoList = [];
        foreach($fileStoragesList as $key => $value){
            $fileStoragesDto = new fileStoragesDTO();            
            $fileStoragesDto->setVars($fileStoragesList[$key]);
            array_push($fileStoragesDtoList, $fileStoragesDto->getVars());
        }
        
        $results['total'] = $this->fileStoragesDb->getfileStoragesByReportId($reportId)->count();  
        
        $results['items'] = $fileStoragesList;
        return $results;
    }

    public function getfileStoragesByReportId($reportId)
    {
    	/* Get report fileStorages in tblcapital table */
        $fileStorages = $this->fileStoragesDb->getfileStoragesByReportId($reportId);

        $results = [];
        if (!empty($fileStorages)){
        	$results['items'] = [];
        	foreach ($fileStorages as $fileStorage) {
        		$fileStorageDto = new fileStoragesDTO;
        		$fileStorageDto->setVars($fileStorage);
        		$results['items'][] = $fileStorageDto;
        	}
            
            return $results;
        }
        
        return null;
    }

    public function getfileStorageById($id)
    {
        /* Get report Associated Files in tbldropboxentity table */
        $fileStorage = $this->fileStoragesDb->getfileStorageById($id);
        if (!empty($fileStorage)){
            $fileStorageDto = new fileStoragesDTO;
            $fileStorageDto->setVars($fileStorage);

            return $fileStorageDto;
        }
        return null;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!empty($report)){
            return true;
        } else{
            return false;
        }
    }

}