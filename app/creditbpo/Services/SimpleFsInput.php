<?php

namespace CreditBPO\Services;

use Illuminate\Support\Facades\Config;
use CreditBPO\Services\FsInput;
use CreditBPO\Exceptions\FormInputException;
use \Exception as Exception;

class SimpleFsInput extends FsInput {
    public function __construct($filePath, $data = []) {
        parent::__construct($filePath, $data);
        $this->setConfig(Config::get('services.simple-fs-input'));
    }

    protected function verifyInput() {
        $data = $this->getData();

        if (!isset($data['money_factor']) || empty($data['money_factor'])) {
            throw new FormInputException(
                $this->getTranslatedMessage('fs_input.unit_required')
            );
        }

        if (!isset($data['currency']) || empty($data['currency'])) {
            throw new FormInputException(
                $this->getTranslatedMessage('fs_input.currency_required')
            );
        }

        if (!isset($data['industry_id']) || empty($data['industry_id'])) {
            throw new FormInputException(
                $this->getTranslatedMessage('fs_input.industry_required')
            );
        }

        return parent::verifyInput();
    }

    public function getDocumentFileName() {
        return $this->getDocumentName().'.xls';
    }
}