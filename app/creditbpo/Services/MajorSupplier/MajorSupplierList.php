<?php

namespace CreditBPO\Services\MajorSupplier;

use CreditBPO\Models\MajorSupplier;
use CreditBPO\Libraries\MajorSupplierValue;
use CreditBPO\DTO\MajorSupplierDTO;
use CreditBPO\Services\MajorCustomerAccount\MajorCustomerAccountUpdate;

class MajorSupplierList
{
    public function __construct()
    {
        $this->majorSupplierDb = new MajorSupplier();
        $this->majorSupplierValueLibrary = new MajorSupplierValue();
    }

    public function getReportMajorSupplierByPage($reportId, $page, $limit)
    {
        $results = array();
        $results['page'] = $page;  
        $results['limit'] = $limit;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get report major suppliers in tblmajorsupplier table */
        $majorSuppliersList = $this->majorSupplierDb->getReportMajorSupplierByPage($reportId, $page, $limit);

        $results['total'] = $this->majorSupplierDb->getMajorSuppliersByReportId($reportId)->count();  
        $results['items'] = $this->transformDtoDataToReadable($majorSuppliersList);

        return $results;
    }

    public function getMajorSupplierList($reportId){
        $results = array();
        $results['page'] = 1;  
        $results['limit'] = 0;  
        $results['total'] = 0;  
        $results['items'] = array();

        /* Get report major suppliers in tblmajorsupplier table */
        $majorSuppliersList = $this->majorSupplierDb->getMajorSuppliersByReportId($reportId);
        
        $results['total'] = $majorSuppliersList->count(); 
        $results['items'] = $this->transformDtoDataToReadable($majorSuppliersList);
        $results['limit'] = $majorSuppliersList->count();

        return $results;
    }

    public function transformDtoDataToReadable($majorSuppliersList){
        /* Change relationship satisfaction, payment frequency and payment behavior to readable string */
        $majorSupplierDtoList = [];
        foreach($majorSuppliersList as $key => $value){
            $majorSupplierDto = new MajorSupplierDTO();
            
            /*arrange array to dto*/
            $value['itemspurchased'] = array(
                $value['firstitem'],
                $value['seconditem'],
                $value['thirditem'],
            );
            $value['paymentfrequency'] = $this->majorSupplierValueLibrary->supplierPaymentFrequencyToResponse($value['paymentfrequency']);
            $value['relationshipsatisfaction'] = $this->majorSupplierValueLibrary->relationshipSatisfactionToResponse($value['relationshipsatisfaction']);
            $value['paymentbehavior'] = $this->majorSupplierValueLibrary->supplierSettlementToResponse($value['paymentbehavior']);
            $majorSuppliersList[$key] = $value;
            $majorSupplierDto->setVars($majorSuppliersList[$key]);

            array_push($majorSupplierDtoList, $majorSupplierDto->getVars());
        }

        return $majorSupplierDtoList;
    }

    public function getMajorSupplierById($id)
    {
        /* Get report major suppliers in tblmajorsupplier table */
        $majorSupplier = $this->majorSupplierDb->getMajorSupplierById($id);
        if (!is_null($majorSupplier)){
            $majorSupplierDto = new MajorSupplierDTO;

            /*arrange item details*/
            $majorSupplier['itemspurchased'] = array(
                $majorSupplier['firstitem'],
                $majorSupplier['seconditem'],
                $majorSupplier['thirditem'],
            );

            /* Change relationship satisfaction, payment frequency and payment behavior to readable string */
            $majorSupplier['paymentfrequency'] = $this->majorSupplierValueLibrary->supplierPaymentFrequencyToResponse($majorSupplier['paymentfrequency']);
            $majorSupplier['relationshipsatisfaction'] = $this->majorSupplierValueLibrary->relationshipSatisfactionToResponse($majorSupplier['relationshipsatisfaction']);
            $majorSupplier['paymentbehavior'] = $this->majorSupplierValueLibrary->supplierSettlementToResponse($majorSupplier['paymentbehavior']);
            $majorSupplierDto->setVars($majorSupplier);

            return $majorSupplierDto;
        }
        return null;
    }
}