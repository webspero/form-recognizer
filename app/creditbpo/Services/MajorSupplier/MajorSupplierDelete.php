<?php

namespace CreditBPO\Services\MajorSupplier;

use CreditBPO\Models\MajorSupplier;

class MajorSupplierDelete
{
    public function __construct()
    {
        $this->majorSupplierDb = new MajorSupplier();
    }

    /* Current process, the data will be deleted */
    public function deleteMajorSupplier($id)
    {
        return $this->majorSupplierDb->deleteMajorSupplier($id);
    }

    /*
    * The right process, the data should not be deleted. It should only update database field is_deleted
    * See Eloquent Soft Delete or use custom update like below
    */

    /*
    * public function deleteMajorSupplier($id)
    * {
    *    $data = [
    *        'is_delete' = 1
    *    ];
    *    return $this->majorSupplierDb->updateMajorSupplier($id, $data);
    *}
    */

}