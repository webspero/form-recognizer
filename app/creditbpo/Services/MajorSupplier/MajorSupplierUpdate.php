<?php

namespace CreditBPO\Services\MajorSupplier;

use CreditBPO\Models\MajorSupplier;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\MajorSupplierValue;
use CreditBPO\Libraries\StringManipulator;

class MajorSupplierUpdate
{
    public function __construct()
    {
        $this->majorSupplierDb = new MajorSupplier();
        $this->reportDb = new Report();
        $this->majorSupplierValueLibrary = new MajorSupplierValue();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function getMajorSupplierReportId($id)
    {
        $majorSupplier = $this->majorSupplierDb->getMajorSupplierById($id);
        if (!is_null($majorSupplier)){
            return $majorSupplier['reportid'];
        } else {
            return null;
        }
    }

    public function checkReportIfAnonymous($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            /*Check if anonymous */
            if ($report->anonymous_report == INT_TRUE){
                return true;
            } else{
                return false;
            }
        } else{
            return null;
        }
    }

    public function validateRequestDetails($majorSupplierDto, $reportId)
    {
        /* If year started was updated, recompute years doing business */
        if (!is_null($majorSupplierDto->getYearStarted())){
            $majorSupplierDto->setYearsDoingBusiness(date('Y') - $majorSupplierDto->getYearStarted());
        }
        
        /* Check if supplier name want to update, if report was set anonymous, name should not be updated */
        if (!is_null($majorSupplierDto->getName())){
            /* Major supplier can be anonymous or not */
            $isAnonymous = $this->checkReportIfAnonymous($reportId);
            if ($isAnonymous){
                $validator = $this->validateMajorSupplierDetails($majorSupplierDto->getVars(), INT_TRUE);
            } else{
                $validator = $this->validateMajorSupplierDetails($majorSupplierDto->getVars(), INT_FALSE);
            }
        } else{
            $validator = $this->validateMajorSupplierDetails($majorSupplierDto->getVars());
        }

        return $validator;
    }

    public function validateMajorSupplierDetails($majorSupplierDto, $isAnonymous = INT_FALSE)
    {
        $errors = [];
        /* Sets Validation Messages */
        $messages = array(
            'yearstarted.date_format' => 'Invalid year format for year started.',
            'yearstarted.after' => 'Year cannot be lower than 1970.',
            'yearstarted.before' => 'Year cannot exceed the year now.',
            
            'averagemonthlyvolume.numeric' => 'Average Monthly Volume must be a number.',
            'purchasepercentage.numeric' => 'Purchase Percentage must be a number.',
            
            'relationshipsatisfaction.in' => "Use only 'good', 'satisfactory', 'unsatisfactory' and 'bad' for Relationship Satisfaction.",
            'paymentfrequency.in' => "Use only 'cod', 'monthly', 'bimonthly', 'weekly', 'daily' for Payment Frequency.",
            'paymentbehavior.in' => "Use only 'ahead of due date', 'on due date', 'portion settled on due date', 'settled past due date' and 'delinquent' for Payment Behavior."
        );

        $relationshipSatisfaction = array_keys($this->majorSupplierValueLibrary->relationshipSatisfaction);
        $supplierPaymentFrequency = array_keys($this->majorSupplierValueLibrary->supplierPaymentFrequency);
        $supplierSettlement = array_keys($this->majorSupplierValueLibrary->supplierSettlement);

        /* Sets Validation Rules */
        $rules = array(
            'email' => 'email',
            'yearstarted' => 'date_format:"Y"|after:1969|before:next year',
            'relationshipsatisfaction' => 'in:' . implode(',', $relationshipSatisfaction),
            'salespercentage' => 'numeric|min:0|max:100',
            'paymentfrequency' => 'in:' . implode(',', $supplierPaymentFrequency),
            'paymentbehavior' => 'in:' . implode(',', $supplierSettlement),
            'averagemonthlyvolume' => 'numeric'
        );

        /*Convert strings to lower and removes special characters*/
        $details = $majorSupplierDto;
        $details['relationshipsatisfaction'] = $this->stringManipulatorLibrary->cleanString($details['relationshipsatisfaction']);
        $details['paymentfrequency'] = $this->stringManipulatorLibrary->cleanString($details['paymentfrequency']);
        $details['paymentbehavior'] = $this->stringManipulatorLibrary->cleanString($details['paymentbehavior']);

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }

        /* Check name validation if report is anonymous, name should not be editable */
        if ($isAnonymous === INT_TRUE && !is_null($majorSupplierDto['name'])){
            array_push($errors, 'Supplier name cannot be updated because report was set to anonymous.');
        }

        return $errors;
    }

    public function updateReportMajorSupplier($id, $majorSupplierDto)
    {
        $majorSupplierDtoRaw = $majorSupplierDto;

        /* Convert request value to database response */
        if (!is_null($majorSupplierDto->getPaymentFrequency())){
            $majorSupplierDto->setPaymentFrequency($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getPaymentFrequency()));
        }
        if (!is_null($majorSupplierDto->getPaymentBehavior())){
            $majorSupplierDto->setPaymentBehavior($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getPaymentBehavior()));
        }
        if (!is_null($majorSupplierDto->getRelationshipSatisfaction())){
            $majorSupplierDto->setRelationshipSatisfaction($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getRelationshipSatisfaction()));
        }

        $data = [
            'supplier_name' => $majorSupplierDto->getName(),
            'supplier_share_sales' => $majorSupplierDto->getPurchasePercentage(),
            'supplier_address' => $majorSupplierDto->getAddress(),
            'supplier_contact_person' => $majorSupplierDto->getContactPerson(),
            'supplier_email' => $majorSupplierDto->getEmail(),
            'supplier_phone' => $majorSupplierDto->getContactNumber(),
            'supplier_started_years' => $majorSupplierDto->getYearStarted(),
            'supplier_years_doing_business' => $majorSupplierDto->getYearsDoingBusiness(),
            'supplier_settlement' => $majorSupplierDto->getPaymentBehavior(),
            'supplier_order_frequency' => $majorSupplierDto->getPaymentFrequency(),
            'relationship_satisfaction' => $majorSupplierDto->getRelationshipSatisfaction(),
            'average_monthly_volume' => $majorSupplierDto->getAverageMonthlyVolume(),
            'item_1' => $majorSupplierDto->getFirstItem(),
            'item_2' => $majorSupplierDto->getSecondItem(),
            'item_3' => $majorSupplierDto->getThirdItem()
        ];
        
        $majorSupplierUpdateData = $this->majorSupplierDb->updateMajorSupplier($id, $data);
        /* Set DTO data */
        $updatedData = [
            'id' => $majorSupplierUpdateData['majorsupplierid'],
            'reportid' => $majorSupplierUpdateData['entity_id'],
            'name' => $majorSupplierUpdateData['supplier_name'],
            'address' => $majorSupplierUpdateData['supplier_address'],
            'contactperson' => $majorSupplierUpdateData['supplier_contact_person'],
            'contactnumber' => $majorSupplierUpdateData['supplier_phone'],
            'email' => $majorSupplierUpdateData['supplier_email'],
            'yearstarted' => $majorSupplierUpdateData['supplier_started_years'],
            'yearsdoingbusiness' => $majorSupplierUpdateData['supplier_years_doing_business'],
            'averagemonthlyvolume' => $majorSupplierUpdateData['average_monthly_volume'],
            'purchasepercentage' => $majorSupplierUpdateData['supplier_share_sales'],
            'itemspurchased' => array(
                $majorSupplierUpdateData['item_1'],
                $majorSupplierUpdateData['item_2'],
                $majorSupplierUpdateData['item_3']
            )
        ];
        $majorSupplierDto->setVars($updatedData);

        /* Convert database value to readable response */
        // $majorSupplierDto->setPaymentFrequency($this->majorSupplierValueLibrary->paymentFrequencyToResponse($majorSupplierUpdateData['supplier_order_frequency']));
        // $majorSupplierDto->setPaymentBehavior($this->majorSupplierValueLibrary->paymentBehaviorToResponse($majorSupplierUpdateData['supplier_settlement']));
        // $majorSupplierDto->setRelationshipSatisfaction($this->majorSupplierValueLibrary->relationshipSatisfactionToResponse($majorSupplierUpdateData['relationship_satisfaction']));
        
        return $majorSupplierDtoRaw;
    }
}