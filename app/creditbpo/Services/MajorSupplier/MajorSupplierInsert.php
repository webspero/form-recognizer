<?php

namespace CreditBPO\Services\MajorSupplier;

use CreditBPO\Models\MajorSupplier;
use CreditBPO\Models\Entity as Report;
use Validator;
use CreditBPO\Libraries\MajorSupplierValue;
use CreditBPO\Libraries\StringManipulator;

class MajorSupplierInsert
{
    public function __construct()
    {
        $this->majorSupplierDb = new MajorSupplier();
        $this->reportDb = new Report();
        $this->majorSupplierValueLibrary = new MajorSupplierValue();
        $this->stringManipulatorLibrary = new StringManipulator();
    }

    public function validateMajorSupplierDetails($majorSupplierDto)
    {
        $errors = [];
        $isAnonymous = null;
        
        /* Sets Validation Messages */
        $messages = array(
            'reportid.required' => 'Report ID is required.',
            'address.required' => 'Address is required.',
            'contactperson.required' => 'Contact Person is required.',
            'contactnumber.required' => 'Contact Number is required.',
            'email.required' => 'Email is required.',
            'yearstarted.required' => 'Year started doing business with supplier is required.',
            'yearsdoingbusiness.required' => 'Years doing business with supplier is required.',
            'relationshipsatisfaction.required' => 'Relationship Satisfaction is required.',
            'averagemonthlyvolume.required' => 'Average Monthly Volume is required.',
            'paymentfrequency.required' => 'Payment Frequency is required.',
            'paymentbehavior.required' => 'Payment Behavior is required.',
            'purchasepercentage.required' => 'Purchase Percentage is required.',

            'yearstarted.date_format' => 'Invalid year format.',
            'yearstarted.after' => 'Year cannot be lower than 1970.',
            'yearstarted.before' => 'Year cannot exceed the year now.',
            
            'yearsdoingbusiness.numeric' => 'Year Started Doing Business must be a number.',
            'averagemonthlyvolume.numeric' => 'Average Monthly Volume must be a number.',
            'purchasepercentage.numeric' => 'Purchase Percentage must be a number.',
            
            'relationshipsatisfaction.in' => "Use only 'good', 'satisfactory', 'unsatisfactory' and 'bad' for Relationship Satisfaction.",
            'paymentfrequency.in' => "Use only 'cod', 'monthly', 'bimonthly', 'weekly', 'daily' for Payment Frequency.",
            'paymentbehavior.in' => "Use only 'ahead of due date', 'on due date', 'portion settled on due date', 'settled past due date' and 'delinquent' for Payment Behavior."
        );

        $relationshipSatisfaction = array_keys($this->majorSupplierValueLibrary->relationshipSatisfaction);
        $supplierPaymentFrequency = array_keys($this->majorSupplierValueLibrary->supplierPaymentFrequency);
        $supplierSettlement = array_keys($this->majorSupplierValueLibrary->supplierSettlement);

        /* Sets Validation Rules */
        $rules = array(
            'reportid' => 'required',
            'address' => 'required',
            'contactperson' => 'required',
            'contactnumber' => 'required',
            'email' => 'required|email',
            'yearstarted' => 'required|date_format:"Y"|after:1969|before:next year',
            'yearsdoingbusiness' => 'required|numeric',
            'relationshipsatisfaction' => 'required|in:' . implode(',', $relationshipSatisfaction),
            'averagemonthlyvolume' => 'required|numeric',
            'paymentfrequency' => 'required|in:' . implode(',', $supplierPaymentFrequency),
            'paymentbehavior' => 'required|in:' . implode(',', $supplierSettlement),
            'purchasepercentage' => 'required|numeric|min:0|max:100',
        );

        /* Check first if report exist */
        /* Major supplier can be anonymous or not */
        $isAnonymous = $this->checkReportIfAnonymous($majorSupplierDto['reportid']);
        if (!is_null($isAnonymous)){
            if ($isAnonymous === INT_FALSE){
                $messages['name.required'] = 'Supplier name is required.';
                $rules['name'] = 'required';
            }
        } else {
            array_push($errors, "Report doesn't exist.");
        }
        
        /*Convert strings to lower and removes special characters*/
        $details = $majorSupplierDto;
        $details['relationshipsatisfaction'] = $this->stringManipulatorLibrary->cleanString($details['relationshipsatisfaction']);
        $details['paymentfrequency'] = $this->stringManipulatorLibrary->cleanString($details['paymentfrequency']);
        $details['paymentbehavior'] = $this->stringManipulatorLibrary->cleanString($details['paymentbehavior']);

        /* Run Laravel Validation */
        $validator = Validator::make($details, $rules, $messages);

        if ($validator->fails()){
            foreach($validator->messages()->all() as $error){
                array_push($errors, $error);
            }
        }
        
        $result = [
            'errors' => $errors,
            'anonymous' => $isAnonymous
        ];
        return $result;
    }

    public function checkReportIdIfExist($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            return true;
        } else{
            return false;
        }
    }

    public function checkReportIfAnonymous($reportId)
    {
        $report = $this->reportDb->getReportByReportId($reportId);
        if (!is_null($report)){
            /*Check if anonymous */
            return $report->anonymous_report;
        } else{
            return null;
        }
    }

    public function postReportMajorSupplier($majorSupplierDto, $isAnonymous)
    {
        /* If report is anonymous, set major supplier name to 'MajorSupplier[supplier count]' */
        if ($isAnonymous){
            $majorSuppliers = $this->majorSupplierDb->getMajorSuppliersByReportId($majorSupplierDto->getReportId());
            $supplierCount = @count($majorSuppliers) + 1;
            $majorSupplierDto->setName('Major Supplier '.$supplierCount);
        }

        /* Change user request data to database data */
        //$this->majorSupplierValueLibrary->requestToDatabase($majorSupplierDto);

        //a copy of the original request data, to be returned if process successfuly executes
        $majorSupplierRaw = $majorSupplierDto;

        // convert user request data dropdowns to valid string index
        $majorSupplierDto->setRelationshipSatisfaction($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getRelationshipSatisfaction()));
        $majorSupplierDto->setPaymentFrequency($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getPaymentFrequency()));
        $majorSupplierDto->setPaymentBehavior($this->stringManipulatorLibrary->cleanString($majorSupplierDto->getPaymentBehavior()));

        /* Change user request data to database data */
        $relationshipSatisfaction = $this->majorSupplierValueLibrary->relationshipSatisfactionToDatabase($majorSupplierDto->getRelationshipSatisfaction());
        $paymentFrequency = $this->majorSupplierValueLibrary->supplierPaymentFrequencyToDatabase($majorSupplierDto->getPaymentFrequency());
        $paymentBehavior = $this->majorSupplierValueLibrary->supplierSettlementToDatabase($majorSupplierDto->getPaymentBehavior());


        
        /* Save data to database */
        $insertResponse = $this->majorSupplierDb->addNewMajorSupplier($majorSupplierDto);
        if (!is_null($insertResponse)){
            $majorSupplierDto->setId($insertResponse->majorsupplierid);
        }
        return $majorSupplierRaw;
    }
}