<?php

namespace CreditBPO\Services\Payments;

use CreditBPO\Services\MailHandler;
use CreditBPO\DTO\PaymentDTO;
use CreditBPO\DTO\DragonpayDTO;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\User;
use CreditBPO\Models\Bank;
use CreditBPO\Models\Transaction;
use CreditBPO\Models\PaymentReceipt;

//======================================================================
//  Class #: Dragonpay PS Service
//      Functions related to generating Request URLs and processing Responses from Dragonpay
//======================================================================

class DragonpayService
{
    public function __construct()
	{
        $this->reportDb = new Report;
        $this->userDb = new User;
        $this->transactionDb = new Transaction;
        $this->paymentReceiptDb = new PaymentReceipt;
    }
    
    //---------------------------------------------------------------
	// Function #.f. Save Transaction and PaymentReceipt records in the database
	//---------------------------------------------------------------
    public function recordDragonpayTransaction(PaymentDTO $paymentDTO, $clientReturn = null)
    {
        /* Dummy user email, should be replaced by email of authenticated user */
        $email = "duane.gavino+trial@creditbpo.com";
        $rand = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);

        /* Save Transaction Record 
        *  TODO: Get User ID from Authentication
        */
        $transactionData = array(
            'loginid' => $paymentDTO->getUserId(),     //User ID
            'entity_id' => $paymentDTO->getReportId(),  //Report ID
            'amount' => $paymentDTO->getTotalAmount(),
            'ccy' => "PHP",
            "description" => "CreditBPO Rating Report®",
            'ccy'   => "PHP",
            'txn'   => $rand,
            'type'  => "BUP",
            "api_return_url"    => $clientReturn
        );

        $transactionId = $this->transactionDb->addTransaction($transactionData);

        $transactionData['transactionId'] = $transactionId;

        /* Update Report Record's Transaction ID */
        $this->reportDb->recordTransactionId($transactionId, $paymentDTO->getReportId());

        /* Prepare Dragonpay Parameters */
        $dragonpayDTO = new DragonpayDTO;

        $dragonpayDTO->setTransactionId($transactionData['transactionId']);
        $dragonpayDTO->setAmount($transactionData['amount']);
        $dragonpayDTO->setCurrency($transactionData['ccy']);
        $dragonpayDTO->setDescription($transactionData['description']);
        $dragonpayDTO->setEmail($email);

        if($clientReturn != null)
            $dragonpayDTO->setClientReturnUrl($clientReturn);

        /* Save Payment Receipt Record
        *  TODO: Get User ID from Authentication
        */
        $receiptData = array(
            'login_id' => $paymentDTO->getUserId(),
            'reference_id' => $dragonpayDTO->getTransactionId(),
            'total_amount' => $dragonpayDTO->getAmount(),
            'vatable_amount' => $paymentDTO->getvatableAmount(),
            'vat_amount' => $paymentDTO->getVatAmount(),
            'item_quantity' => 1,
            'payment_method' => PAYMENT_METHOD_DRAGONPAY,
            'item_type' => ITEM_TYPE_FINANCIAL_STANDALONE,
            'sent_status' => STS_NG,
            'date_sent' => date('Y-m-d'),
            'api_match_account_details'    => $paymentDTO->getAPIMatchAccountDetails(),
            'amount'    => $paymentDTO->getTotalAmount(),
            'price' => $paymentDTO->getTotalAmount(),
            'discount' => $paymentDTO->getDiscountAmount()
        );

        $this->paymentReceiptDb->addPaymentReceipt($receiptData);
        
        /* Return array containing parameters needed for the Dragonpay Request URL */
        return $dragonpayDTO;
    }

    //---------------------------------------------------------------
	// Function #.g. Save Transaction and PaymentReceipt records in the database
    //---------------------------------------------------------------
    public function createDragonpayRequestUrl(DragonpayDTO $dragonpayDTO)
    {
        /* Create digest based on parameters*/
        $seed = implode(":",$dragonpayDTO->getRequestContent());
        $digest = Sha1($seed);
    
        $dragonpayDTO->setDigest($digest);
        $dragonpayDTO->setRequestURL();

        return $dragonpayDTO->getRequestUrl();
    }

    //---------------------------------------------------------------
	// Function #.h. Check if the digest passed by Dragonpay is valid
    //---------------------------------------------------------------
    public function checkDigest($dragonpayDTO)
    {
        $seed = implode(":",$dragonpayDTO->getResponseContent());
        $digest = Sha1($seed);

        $responseDigest = $dragonpayDTO->getDigest();

        if($digest == $responseDigest)
            return true;
        else {
            return false;
        }
    }

    //---------------------------------------------------------------
	// Function #.i. Record Dragonpay Response in Log File
    //---------------------------------------------------------------
    public function logDragonpay($host, $uri)
    {
        $file   = public_path()."/dragonpay_logs/dragonpay.log";
		$string = date('c') . " => " . $host . $uri . "\r\n";

        if (file_exists($file)) {
			if (filesize($file) > 5242880) {
				rename($file, "./public/dragonpay_logs/" . date('ymd') . "_dragonpay.log");
			}
		}

		file_put_contents($file, $string , FILE_APPEND | LOCK_EX);
    }

    //---------------------------------------------------------------
    // Function #.j. Update Transaction Record associated with the report
    //---------------------------------------------------------------
    public function updateTransaction($dragonpayDTO)
    {
        $transaction = Transaction::find($dragonpayDTO->getTransactionId());

        $dragonpayDTO->setClientReturnUrl($transaction->api_return_url);

        if($transaction->status == null || $transaction->status == "P") {
            $transaction->refno = $dragonpayDTO->getReferenceNumber();
            $transaction->status = $dragonpayDTO->getStatus();
            $transaction->message = $dragonpayDTO->getMessage();
            $transaction->save();

            $dragonpayDTO->setUser(User::find($transaction->loginid));
            $dragonpayDTO->setReport(Report::find($transaction->entity_id));

            return true;
        } else {
            return false;
        }
    }

    //---------------------------------------------------------------
	// Function #.e. Toggles User Status to 'áctive and paid'
    //---------------------------------------------------------------
    public function toggleUserStatus($dragonpayDTO)
    {
        
        /* Get Report By Transaction ID */
        $transaction = Transaction::find($dragonpayDTO->getTransactionId());
        $dragonpayDTO->setReport($transaction['entity_id']);

        /* Get User by Report */
        $report = $this->reportDb->getReportByReportId($transaction['entity_id']);
        $dragonpayDTO->setUser($this->userDb->getUserById($report['loginid']));

        $user = $dragonpayDTO->getUser();

        if($user['status'] != $dragonpayDTO->getPaidStatus()) {
            $this->userDb->updateUserStatus($user['loginid'], $dragonpayDTO->getPaidStatus());

            return true;
        } else {
            return false;
        }
    }

    //---------------------------------------------------------------
    // Function #.k. Send receipt via email    //---------------------------------------------------------------
    public function sendEmailReceipt(DragonpayDTO $dragonpayDTO) {
        $user = $dragonpayDTO->getUser();

        $receipt = PaymentReceipt::where('login_id', $user['loginid'])
            ->where('reference_id', $dragonpayDTO->getTransactionId())
            ->where('payment_method', PAYMENT_METHOD_DRAGONPAY)
            ->first();

        if($receipt != null) {
            $receipt->sent_status = STS_OK;
            $receipt->save();
        }

        $basePrice = $receipt->total_amount;

        $mailHandler = new MailHandler();
        $mailHandler->sendReceipt(
            $user['loginid'],
            [
                'receipt' => $receipt,
                'subtotal' => $basePrice
            ],
            'CreditBPO Payment Receipt'
        );

        $dragonpayTransaction = Transaction::where('id', $dragonpayDTO->getTransactionId())->first();
        $report = Report::where('entityid', $dragonpayTransaction->entity_id)->first();
        $organization_name = Bank::where('id', $report->current_bank)->pluck('bank_name')->first();

        if($organization_name == "Matching Platform"){
            // sent email using api recepient details
            $mailHandler->sendReceiptToAPIUser(
                $receipt['loginid'],
                [
                    'receipt' => $receipt,
                    'user' => $user
                ],
                'CreditBPO Payment Receipt'
            );
        }
    }
}