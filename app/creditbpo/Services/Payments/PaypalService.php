<?php

namespace CreditBPO\Services\Payments;

use URL;
use CreditBPO\DTO\PaymentDTO;
use CreditBPO\DTO\PayPalDTO;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Bank;
use CreditBPO\Models\Discount;
use CreditBPO\Models\User;
use CreditBPO\Models\Payment;
use CreditBPO\Models\PaymentReceipt;
use CreditBPO\Models\PaypalTransaction;
use CreditBPO\Libraries\PaymentPrepLib;
use CreditBPO\Services\MailHandler;

//======================================================================
//  Class #: Payment Poral Service
//      Generic functions related to generating Request URLs and processing Responses from both Paypal and Dragonpay
//======================================================================

class PaypalService
{
    public function __construct()
	{
        $this->receiptDb = new PaymentReceipt;
        $this->paypalTransactionDb = new PayPalTransaction;
    }

    //---------------------------------------------------------------
	// Function #.h. Prepare PayPal required parameters
    //---------------------------------------------------------------
    public function recordPayPalTransaction(PaymentDTO $paymentDTO)
    {
        /* Prepare data of receipt to be stored */
        $receiptData = array(
            'login_id' => $paymentDTO->getUserId(),
            'reference_id' => 0,
            'total_amount' => $paymentDTO->getTotalAmount(),
            'vatable_amount' => $paymentDTO->getVatableAmount(),
            'vat_amount' => $paymentDTO->getVatAmount(),
            'item_quantity' => 1,
            'payment_method' => PAYMENT_METHOD_PAYPAL,
            'item_type' => 1,
            'sent_status' => STS_NG,
            'date_sent' => date('Y-m-d'),
            'api_match_account_details'    => $paymentDTO->getAPIMatchAccountDetails(),
            'amount'    => $paymentDTO->getTotalAmount(),
            'price' => $paymentDTO->getTotalAmount(),
            'discount' => $paymentDTO->getDiscountAmount()
        );
        
        /* Save receipt */
        $receipt = $this->receiptDb->addPaymentReceipt($receiptData);
        $receiptData['reference_id'] = $receipt;

        /* Get Receipt ID and assign to PayPalDTO */
        $paypalDTO = new PaypalDTO;
        $paypalDTO->setVars($receiptData);

        /* return PayPalDTO with Receipt ID */
        return $paypalDTO;
    }

    //---------------------------------------------------------------
	// Function #.i. Prepare PayPal required parameters
    //---------------------------------------------------------------
    public function getPayPalParams(PayPalDTO $paypalDTO, PaymentDTO $paymentDTO)
    {
        /* Prepare parameter array */
        $parameters = array(
            'cancelUrl' => URL::route('paypal.cancel', ["reference" => $paypalDTO->getReferenceID()]),
			'returnUrl' => URL::route('paypal.success', ["reference" => $paypalDTO->getReferenceID()]),
			'name' => $paypalDTO->getProductName(),
			'description' => $paypalDTO->getProductDescription(),
			'amount' => number_format($paypalDTO->getAmount(),2,'.',''),
			'currency' => $paypalDTO->getCurrency(),
            'reference_id' => $paypalDTO->getReferenceId(),
            'entity_id'  => $paymentDTO->getReportId()
        );

        $transaction = $this->paypalTransactionDb->addRecord($parameters, $paypalDTO->getClientReturnUrl());

        return $parameters;
    }

    //---------------------------------------------------------------
	// Function #.k. Prepare Omnipay gateway configuration
    //---------------------------------------------------------------
    public function environmentSetup(PayPalDTO $paypalDTO, $gateway)
    {
        $gateway->setUsername($paypalDTO->getUsername());
        $gateway->setPassword($paypalDTO->getPassword());
        $gateway->setSignature($paypalDTO->getSignature());
        $gateway->setLogoImageUrl(URL::To($paypalDTO->getLogoImage()));
        $gateway->setTestMode($paypalDTO->getIsTest());
    }

    //---------------------------------------------------------------
	// Function #.l. Get parameters recorded in the database
    //---------------------------------------------------------------
    public function getParameterRecords($referenceId, $paypalDTO)
    {
        $parameterRecords = $this->paypalTransactionDb->getParametersByReferenceId($referenceId);

        /* Prepare parameter array */
        $parameters = array(
            'cancelUrl' => $parameterRecords['cancel_url'],
			'returnUrl' => $parameterRecords['return_url'],
			'name' => $parameterRecords['name'],
			'description' => $parameterRecords['description'],
			'amount' => number_format($parameterRecords['amount'],2,'.',''),
			'currency' => $parameterRecords['currency'],
            'reference_id' => $parameterRecords['reference_id']
        );

        $paypalDTO->setVars($parameters);
        $paypalDTO->setClientReturnUrl($parameterRecords['client_return']);

        return $parameters;
    }

    //---------------------------------------------------------------
	// Function #.m. Get number of times a PayPal transaction has been made
    //---------------------------------------------------------------
    public function getPaymentCount($paypalDTO, $userId)
    {
        $paymentDb = new Payment;
        $paymentCount = $paymentDb->countPaymentsMade($paypalDTO->getTransactionId());

        if($paymentCount == 0) {
            $newPayment = array(
                'loginid' => $userId,
                'transactionid' => $paypalDTO->getTransactionId(),
                'transactiontype' => $paypalDTO->getTransactionType(),
                'paymenttype' => $paypalDTO->getPaymentType(),
                'paymentamount' => $paypalDTO->getAmount(),
                'currencycode' => $paypalDTO->getCurrency()
            );
    
            $paymentDb->addPayment($newPayment);
            return false;
        } else {
            return true;
        }
    }

    //---------------------------------------------------------------
	// Function #.m. Get the receipt, user, and report records for a certain transaction
    //---------------------------------------------------------------
    public function sendReceipt($paypalDTO, $isPaidBefore)
    {
        $receipt = PaymentReceipt::where('receipt_id', $paypalDTO->getReferenceId())
            ->where('payment_method', PAYMENT_METHOD_PAYPAL)
            ->first();
        
        $isSent = $receipt->sent_status;

        $userDb = new User;
        $user = $userDb->getUserById($receipt['login_id']);
        $isNew = false;

        if($user->status != USER_STS_PAID) {
            $isNew = true;
            $userDb->updateUserStatus($user->loginid, USER_STS_PAID);
            $user->status = USER_STS_PAID;
        }


        $paypalTransaction = PaypalTransaction::where('reference_id', $paypalDTO->getReferenceId())->first();
        $reportDb = new Report;
        $report = Report::where('entityid', $paypalTransaction->entity_id)->first();

        \Log::info($user);
        \Log::info($report);
        $reportDb->togglePaidFlag($user->loginid, $report->entityid);
        $report->is_paid = STS_OK;
        $report->save();
        
        /* Send receipt if not yet sent before */
        if($receipt != null && !$isSent) {
            $mailHandler = new MailHandler();


            $mailHandler = new MailHandler();
            $mailHandler->sendReceipt(
                $receipt['login_id'],
                [
                    'receipt' => $receipt,
                    'entity' => $report,
                    'user' => $user,
                ],
                'CreditBPO Payment Receipt'
            );

            $organization_name = Bank::where('id', $report->current_bank)->pluck('bank_name')->first();

            if($organization_name == "Matching Platform"){
                // sent email using api recepient details
                $mailHandler->sendReceiptToAPIUser(
                    $receipt['login_id'],
                    [
                        'receipt' => $receipt,
                        'user' => $user
                    ],
                    'CreditBPO Payment Receipt'
                );
            }
            
            $receipt->sent_status = STS_OK;
            $receipt->save();
        }
        
        $paypalDTO->setReceipt($receipt['receipt_id']);
        $paypalDTO->setUser($user['loginid']);
        $paypalDTO->setReport($report['entityid']);

        return $isNew;
    }
}