<?php

namespace CreditBPO\Services\Payments;

use CreditBPO\DTO\PaymentDTO;
use CreditBPO\DTO\DragonpayDTO;
use CreditBPO\Models\Entity as Report;
use CreditBPO\Models\Discount;
use CreditBPO\Models\User;
use CreditBPO\Models\Bank;
use CreditBPO\Models\Transaction;
use CreditBPO\Models\PaymentReceipt;
use CreditBPO\Libraries\PaymentPrepLib;
use DB;

//======================================================================
//  Class #: Payment Poral Service
//      Generic functions related to generating Request URLs and processing Responses from both Paypal and Dragonpay
//======================================================================

class PaymentService
{
    public function __construct()
	{
		$this->reportDb = new Report;
        $this->discountDb = new Discount;
        $this->transactionDb = new Transaction;
        $this->paymentReceiptDb = new PaymentReceipt;
        $this->userDb = new User;
    }
    
    //---------------------------------------------------------------
	// Function #.a. Get a Report based on the Report ID and User ID of owner
	//---------------------------------------------------------------
	public function getReport($reportId, $userId)
	{
		$report = null;

		if($reportId == null){
			$report = $this->reportDb->getUnpaidReportByUserId($userId);
		}
		else{
			$report = $this->reportDb->getReportByReportId($reportId);
		}
		return $report;
	}

	//---------------------------------------------------------------
	// Function #.b. Get a Report's Base Amount, Discounted Amount, Total Amount
	//---------------------------------------------------------------
	public function getAmountAfterDiscount(PaymentDTO $paymentDTO)
	{
		/* Get Discount Details */
		$discountDetails = $this->discountDb->getDiscountByCode($paymentDTO->getDiscountCode());
		$paymentDTO->setDiscountDetails($discountDetails);

		$faceAmount = $paymentDTO->getBaseAmount();
		$discountType = $paymentDTO->getDiscountType();
		$discountDetailAmount = $paymentDTO->getDiscountDetailAmount();

		// /* Run Total Amount Calculation */
		$amounts = PaymentPrepLib::getAmountAfterDiscount($faceAmount, $discountType, $discountDetailAmount);

		$paymentDTO->setDiscountAmount($amounts['discountAmount']);
		$paymentDTO->setTotalAmount($amounts['totalAmount']);
	}

	//---------------------------------------------------------------
	// Function #.c. Toggle isPaid Flag and User Status for Free Trial Accounts
	//---------------------------------------------------------------
	public function togglePaymentFlags($userId, $reportId = null)
	{

		$this->reportDb->togglePaidFlag($userId, $reportId);
		$this->userDb->updateUserStatus($userId, USER_STS_PAID);
	}

    //---------------------------------------------------------------
	// Function #.d. Calculate the amount/s after tax and discount, if any
	//---------------------------------------------------------------
	public function getAmountAfterTaxAndDiscount(PaymentDTO $paymentDTO)
	{
        /* Calculate Amounts After Tax*/
        $amount = PaymentPrepLib::getBaseAmounts($paymentDTO->getBaseAmount());
        
        /* If discount is applied, adjust base amounts based on discount type */
        if($paymentDTO->getDiscountCode() != null) {
            /* Get Discount Details */
            $discountDetails = $this->discountDb->getDiscountByCode($paymentDTO->getDiscountCode());
            $paymentDTO->setDiscountDetails($discountDetails);

            /* Adjust amounts after discount and after tax */
            $amount = PaymentPrepLib::getVatableAfterTax($amount, $discountDetails, $paymentDTO->getBaseAmount());
            $paymentDTO->setDiscountAmount($amount['discountAmount']);
        }else{
			$paymentDTO->setDiscountAmount(0);
		}

        $paymentDTO->setTotalAmount($amount['amount']);
        $paymentDTO->setVatAmount($amount['vatAmount']);
        $paymentDTO->setVatableAmount($amount['vatableAmount']);
    }

    //---------------------------------------------------------------
	// Function #.f. Toggles User Status to 'áctive and paid'
    //---------------------------------------------------------------
    public function toggleReportPaidFlag($user, $report)
    {
        $userId = $user['loginid'];
        $reportId = $report;

        $this->reportDb->togglePaidFlag($userId, $reportId);

        return true;
    }

	public function computeBaseAmount($reportid){
		$report = Report::where('entityid', $reportid)->first();
		$reportOrganization = Bank::where('id', $report->current_bank)->first();

		$price = 0;

		if($report->is_premium == SIMPLIFIED_REPORT){
			$price = $reportOrganization->simplified_price;
		}elseif($report->is_premium == STANDALONE_REPORT){
			$price = $reportOrganization->standalone_price;
		}else{
			$provCode = DB::table('refprovince')->where('provDesc', $report->province)->pluck('regCode')->first();

			// Filter prizes according to premium zone
			if($provCode == 13){ // Metro Manila
				$premiumPrice = $organization->premium_zone1_price;
			}elseif(in_array($provCode, array(04,03))){ // Laguna, Rizal, Cavite and Bulacan
				$premiumPrice = $organization->premium_zone2_price;
			}else{ // Rest of the regions
				// Filter prizes according to premium zone
				if($provCode == 13){ // Metro Manila
					$price = $reportOrganization->premium_zone1_price;
				}elseif(in_array($provCode, array(04,03))){ // Laguna, Rizal, Cavite and Bulacan
					$price = $reportOrganization->premium_zone2_price;
				}else{ // Rest of the regions
					$price = $reportOrganization->premium_zone3_price;
				}
			}
		}

		return $price;
	}

}