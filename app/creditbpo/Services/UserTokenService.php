<?php

namespace CreditBPO\Services;

use CreditBPO\Models\UserAuthToken;
use CreditBPO\Models\User;
use CreditBPO\DTO\UserAuthDTO;
use CreditBPO\DTO\UserDTO;
use Hash;
use Auth;

class UserTokenService
{

    public function __construct()
    {
        $this->userAuthTokenDb = new UserAuthToken();
        $this->userDb = new User();
    }

    public function getUserDetailsByAccessToken($authorizationHeader)
    {
        $accessToken = substr($authorizationHeader, 7);
        /* Check if access token exist and valid */
        $userAuthTokenDb = new UserAuthToken();
        $tokenDetails = $userAuthTokenDb->getAccessToken($accessToken);
        $userDetails = $this->userDb->getUserById($tokenDetails['login_id']);

        if (!is_null($userDetails)) {
            $userDto = new UserDTO();
            $userDto->setVars($userDetails);

            return $userDto;
        }
        return null;
    }
    
    public function generateUserTokens()
    {
        $tokenUsed = false;
        $userAuthTokenDto = new UserAuthDTO();

        do {
            $accessToken = Hash::make(uniqid(rand(), true));
            $refreshToken = Hash::make(uniqid(rand(), true));
            
            $tokenDetails = $this->userAuthTokenDb->getTokenDetails($accessToken, $refreshToken);
            if(!is_null($tokenDetails)) {
                $tokenUsed = true;
            } else {
                $tokenUsed = false;
            }
        } while ($tokenUsed == true);

        $userAuthTokenDto->setAccessToken($accessToken)
                         ->setRefreshToken($refreshToken);
        
        return $userAuthTokenDto;
    }

    public function addUserTokens($userTokenDetails)
    {
        $userTokenDetails->setExpiresIn(date("Y-m-d H:i:s", strtotime('+1 hours')))
                         ->setStatus(ACTIVE);
            
        $userAuthSaved = $this->userAuthTokenDb->addNewToken($userTokenDetails);
        if ($userAuthSaved) {
            return $userTokenDetails;
        } else {
            return null;
        }
    }

    public function updateUserTokens($userAuthTokenDto)
    {
        $tokenUsed = false;
        
        do {
            $accessToken = Hash::make(uniqid(rand(), true));
            
            $accessTokenDetails = $this->userAuthTokenDb->getAccessToken($accessToken);
            if(!is_null($accessTokenDetails)) {
                $tokenUsed = true;
            } else {
                $tokenUsed = false;
            }
        } while ($tokenUsed == true);
        
        $userAuthTokenDto->setAccessToken($accessToken)
                         ->setExpiresIn(date("Y-m-d H:i:s", strtotime('+1 hours')));
            
        $userTokenUpdate = $this->userAuthTokenDb->updateAccessToken($userAuthTokenDto);
        if ($userTokenUpdate) {
            return $userAuthTokenDto;
        } else {
            return null;
        }
    }

    public function getRefreshTokenDetails($userAuthTokenDto)
    {
        $tokenDetails = $this->userAuthTokenDb->getRefreshTokenDetails($userAuthTokenDto->getRefreshToken(), $userAuthTokenDto->getClientId());

        if (!is_null($tokenDetails)) {
            $userAuthTokenDto->setId($tokenDetails['id'])
                             ->setUserId($tokenDetails['login_id'])
                             ->setAccessToken($tokenDetails['access_token'])
                             ->setExpiresIn($tokenDetails['expires_in'])
                             ->setStatus($tokenDetails['status']);
            return $userAuthTokenDto;
        } else {
            return null;
        }
    }

    public function removeToken($accessToken)
    {
        $tokenDetails = $this->userAuthTokenDb->getAccessToken($accessToken);

        if (!is_null($tokenDetails)) {
            /*Remove token details by setting status to 0*/
            $this->userAuthTokenDb->removeAccessToken($tokenDetails);
            /*Logout user*/
            Auth::logout();
            return true;
        } else {
            return false;
        }
    }
}
