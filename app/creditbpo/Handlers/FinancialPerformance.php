<?php

namespace CreditBPO\Handlers;

use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\ReadyRatioScoreHandler;
use CreditBPO\Models\FinancialReport;

class FinancialPerformance extends FinancialHandler {
    protected $model;

    /**
     * @param FinancialReport $model
     */
    public function __construct(FinancialReport $model = null) {
        $this->model = $model ?: new FinancialReport();
    }

    /**
     * @param int $financialReportId
     * @return int
     */
    public function getScore($financialReportId) {
        $financialReport = $this->model->find($financialReportId);

        if (@count($financialReport) <= 0) {
            return 0;
        }

        $incomeStatements = $financialReport->incomeStatements()
            ->orderBy('index_count', 'asc')
            ->get();
        $balanceSheets = $financialReport->balanceSheets()
            ->orderBy('index_count', 'asc')
            ->get();
        $returnOfEquity = $this->getReturnOfEquity($incomeStatements, $balanceSheets);
        $returnOfAssets = $this->getReturnOfAssets($incomeStatements, $balanceSheets);
        $salesGrowth = $this->getSalesGrowth($incomeStatements);

        return ($returnOfEquity * 0.5) +
            ($returnOfAssets * 0.3) +
            ($salesGrowth * 0.2);
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getScoreByEntityId($entityId) {
        $financialReport = $this->model
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();

        if (@count($financialReport) <= 0) {
            return 0;
        }

        return $this->getScore($financialReport->id);
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getScoreRangeByEntityId($entityId) {
        $readyRatioScoreHandler = new ReadyRatioScoreHandler();
        return $readyRatioScoreHandler->getValueByRange(
            $this->getScoreByEntityId($entityId));
    }

    /**
     * @param array $incomeStatements
     * @param array $balanceSheets
     * @return int
     */
    protected function getReturnOfEquity($incomeStatements, $balanceSheets) {
        $data = [];
        for ($i = @count($incomeStatements) - 1; $i >= 0; $i--) {
            $equity1 = isset($balanceSheets[$i])
                ? $balanceSheets[$i]->Equity
                : 0;

            $equity2 = isset($balanceSheets[$i + 1])
                ? $balanceSheets[$i + 1]->Equity
                : 0;

            if (($equity1 + $equity2) === 0) {
                $data[] = 0;
                continue;
            }
            $totEquity = ($equity1 + $equity2) / 2;
            if($totEquity === 0.0 || $totEquity === 0){
                $data[] = 0;
            } else {
                $data[] = $incomeStatements[$i]->ProfitLoss / $totEquity;  
            }
        }

        $roeSlope = [
            self::calculateSP($data),
            $data[@count($incomeStatements) - 1],
            self::calculateSf($data),
        ];
        foreach ($roeSlope as &$value) {
            if ($value >= 0.2) {
                $value = 2;
                continue;
            }

            if ($value >= 0.128) {
                $value = 1;
                continue;
            }

            if ($value >= 0.112) {
                $value = 0;
                continue;
            }

            if ($value >= 0) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($roeSlope);
    }

    /**
     * @param array $incomeStatements
     * @param array $balanceSheets
     * @return int
     */
    protected function getReturnOfAssets($incomeStatements, $balanceSheets) {
        $data = [];
        for ($i = @count($incomeStatements) - 1; $i >= 0; $i--) {
            $asset1 = isset($balanceSheets[$i])
                ? $balanceSheets[$i]->Assets
                : 0;

            $asset2 = isset($balanceSheets[$i + 1])
                ? $balanceSheets[$i + 1]->Assets
                : 0;

            if (($asset1 + $asset2) === 0) {
                $data[] = 0;
                continue;
            }
            $totAssets = ($asset1 + $asset2) / 2;
            if($totAssets === 0.0 || $totAssets === 0){
                $data[] = 0;
            } else {
                $data[] = $incomeStatements[$i]->ProfitLoss / $totAssets; 
            }
        }

        $returnAssetSlope = [
            self::calculateSP($data),
            $data[@count($data) - 1],
            self::calculateSf($data),
        ];

        foreach ($returnAssetSlope as &$value) {
            if ($value >= 0.1) {
                $value = 2;
                continue;
            }

            if ($value >= 0.064) {
                $value = 1;
                continue;
            }

            if ($value >= 0.056) {
                $value = 0;
                continue;
            }

            if ($value >= 0) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($returnAssetSlope);
    }

    /**
     * @param array $incomeStatements
     * @param array $balanceSheets
     * @return int
     */
    public function getSalesGrowth($incomeStatements) {
        $data = [];
        $totalRevenue = 0;

        for ($i = @count($incomeStatements) - 1; $i >= 0; $i--) {
            $data[] = $incomeStatements[$i]->Revenue;
            $totalRevenue += $incomeStatements[$i]->Revenue;
        }

        if(@count($incomeStatements) > 0 ){
            $averageSales = $totalRevenue / @count($incomeStatements);
        } else {
            $averageSales = 0;
        }
        if ($averageSales != 0 || $averageSales != 0.0){
            $salesSlope = [
                self::calculateSlope($data, false) / $averageSales,
                self::calculateSlope($data, true) / $averageSales,
                self::calculateSf($data, true) / $averageSales,
            ];
        } else {
            $salesSlope = [0,0,0];
        }

        foreach ($salesSlope as &$value) {
            if ($value >= 0.3) {
                $value = 2;
                continue;
            }

            if ($value >= 0.04) {
                $value = 1;
                continue;
            }

            if ($value >= -0.04) {
                $value = 0;
                continue;
            }

            if ($value >= -0.3) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($salesSlope);
    }
}
