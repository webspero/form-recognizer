<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\Entity;


class GenerateInnoFilenamesHandler
{
   //------------------------------------------------------------------
    //  Function 60.1: __construct
    //       GenerateInnodataTextFilenamesHandler Initialization
    //------------------------------------------------------------------
    public function generateInnoFilenames()
    {
       $entity = Entity::select('entityid','companyname')->get()->toArray();
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") === "Production"){
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/TO_INNO/');
        }else{
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_To Innodata/');
        }

        if (!$fileExists) {
            return false;
        }

        if(env("APP_ENV") == "Production"){
            $dir = opendir('ssh2.sftp://' . $sftp . '/FROM_INNO/');
        }else{
            $dir = opendir('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/');
        }

        while (false !== ($folder = readdir($dir)))
        {
            if ($folder == "." || $folder == "..")
                continue;

            //echo $folder; exit;

            if(env("APP_ENV") === "Production"){
	            $folderPath = 'ssh2.sftp://' . $sftp . '/TO_INNO/'.$folder;
	        }else{
	            $folderPath = 'ssh2.sftp://' . $sftp . '/TEST_FOLDER/_To Innodata/'.$folder;
	        }

            if(is_dir($folderPath)){
            	
	            $entityId = trim($folder);

	            $entity = Entity::where('entityid',$entityId)->first();

	            if(!$entity)
	            	continue;

	            $txtFilename = $entityId.' - '.$entity->companyname.'.txt';
	            $xlsxFilename = $entityId.' - '.$entity->companyname.'.xlsx';

	            $filepath = $folder.'/'.$txtFilename;

	            if(env("APP_ENV") === "Production"){
		            $fileTxtFilename = file_exists('ssh2.sftp://' . $sftp . '/TO_INNO/'.$filepath);
		        }else{
		            $fileTxtFilename = file_exists('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_To Innodata/'.$filepath);
		        }

		        if(!$fileTxtFilename){
		        	$txfile = @fopen($fileTxtFilename,'w');
		        	@fwrite($txfile,"Instruction: Use this filename: ".$xlsxFilename);
		        	@fclose($txfile);

		        	if(env("APP_ENV") === "Production"){
			            $uploadedFileTxtFilename = file_exists('ssh2.sftp://' . $sftp . '/TO_INNO/'.$filepath);
			        }else{
			            $uploadedFileTxtFilename = file_exists('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_To Innodata/'.$filepath);
			        }

		        	if(!$uploadedFileTxtFilename){
		        		echo $folder." - Sucessfully uploaded text file to innodata"."\n";
		        	}else{
		        		echo $folder." - Error uploading filename text file to innodata"."\n";
		        	}
		        }else{
		        		echo $folder." - Error uploading filename text file to innodata"."\n";
		        	}
	
            }
            
        }

        $localPath = public_path('documents/innodata/to_innodata/');
        if (!file_exists(public_path('documents/innodata/'))) {
            mkdir(public_path('documents/innodata/',0777));
            $dirPath = 'documents/innodata/to_innodata/';
        }
     
        if (!file_exists($localPath)) {
            mkdir($localPath,0777);
        }

        $folders = scandir($localPath);

        foreach ($folders as $folder) {
         	
            if ($folder == "." || $folder == "..")
                continue;

            if(is_dir($folder)){

	            $entityId = trim($folder);

	            $entity = Entity::where('entityid',$entityId)->first();

	            if(!$entity)
	            	continue;

	            $txtFilename = $entityId.' - '.$entity->companyname.'.txt';
	            $xlsxFilename = $entityId.' - '.$entity->companyname.'.xlsx';

	            $filepath = public_path($dirPath.$folder.'/'.$txtFilename);

		        if(!file_exists($filepath)){
		        	$txfile = @fopen($fileTxtFilename,'w');
		        	@fwrite($txfile,"Instruction: Use this filename: ".$xlsxFilename);
		        	@fclose($txfile);

		        	if(file_exists($filepath)){
		        		echo $folder." - Sucessfully uploaded text file to innodata"."\n";
		        	}else{
		        		echo $folder." - Error uploading filename text file to innodata"."\n";
		        	}
		        }else{
	        		echo $folder." - Error uploading filename text file to innodata"."\n";
	        	}
	
            }
            
        }
    }
   
}
