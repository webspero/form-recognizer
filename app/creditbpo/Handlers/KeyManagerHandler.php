<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\KeyManager;

class KeyManagerHandler {
    const TWO_YEARS_OR_LESS_EXPERIENCE = 7;
    const THREE_TO_FOUR_YEARS_EXPERIENCE = 14;
    const MORE_THAN_FIVE_YEARS_EXPERIENCE = 21;

    protected $model;

    /**
     * @param KeyManager $model
     */
    public function __construct(KeyManager $model = null) {
        $this->model = $model ?: new KeyManager();
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getAssessmentScoreByEntityId($entityId) {
        $score = $this->getAssessmentScore($entityId);

        if ($score >= 5) {
            return self::MORE_THAN_FIVE_YEARS_EXPERIENCE;
        }

        if ($score >= 3) {
            return self::THREE_TO_FOUR_YEARS_EXPERIENCE;
        }

        return self::TWO_YEARS_OR_LESS_EXPERIENCE;
    }

    /**
     * @param int $entityId
     * @return int
     */
    protected function getAssessmentScore($entityId) {
        $keyManagers = $this->model->getByEntityId($entityId);

        if (@count($keyManagers) <= 0) {
            return 0;
        }

        $sum = 0;

        foreach ($keyManagers as $manager) {
            $sum += $manager->number_of_year_engaged;
        }

        return $sum;
    }
}
