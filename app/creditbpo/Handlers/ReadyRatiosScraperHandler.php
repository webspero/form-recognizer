<?php

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Config;
use CreditBPO\Libraries\Scraper;
use CreditBPO\Exceptions\ReadyRatiosScraperException;
use CreditBPO\Models\Entity;
use CreditBPO\Services\MailHandler;
use CreditBPO\Services\Report\PDF\FinancialAnalysis as FinancialAnalysisPDF;
use DB;
use \Exception as Exception;

class ReadyRatiosScraperHandler {
    private $config;
    private $scraper;

    public function __construct(array $config = []) {
        if (empty($config)) {
            $config = Config::get('scraper.ready-ratios');
        }

        $this->setConfig($config);
        $this->scraper = new Scraper($config['client']);
    }

    /**
     * @param array $config
     * @return ReadyRatiosScraperHandler
     */
    public function setConfig(array $config = []) {
        $this->config = $config;
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig() {
        return $this->config;
    }

    /* function to convert vfaContent to Vfa file */
    public function makeVfaFile($vfaContent, $entityid) {
        // create vfaFolder if it does not exist
        $vfaFolder = public_path('documents/vfaFolder/');
        if (!file_exists($vfaFolder)) {
            mkdir($vfaFolder);
        }

        $vfa = json_encode($vfaContent); // encode vfa content
        $vfaFile = $vfaFolder.$entityid.'.vfa'; // set file name
        file_put_contents($vfaFile, $vfa); // save file
        
        return $vfaFile;
    }

    /**
     * @param int $financialReportId
     * @return string|null
     */
    public function getFinancialReportPath($entityId) {
        $pdf = new FinancialAnalysisPDF();
        return $pdf->getFilenamePath($entityId);
    }

    /**
     * @param string $vfaContent
     * @return bool
     */
    public function getReport($financialReportId, $vfaContent, $isFullReport = false, $entityid) {
        // $this->getLoggedInUrl();
        // $this->processReport($vfaContent, $isFullReport);
        // $this->downloadReport($financialReportId, $entityid);
        return;
        echo 'Not using scraper anymore.';
        
        $this->makeVfaFile($vfaContent, $entityid); // convert vfacontent to vfa file
    
        $exec = exec("node /var/cbpo/nodejs/scraper.js x=$entityid" , $out);

        if(env("APP_ENV") != "local"){ 
             /** Copy ReadyRatios file to /var/cbpo/public/financial_analysis folder  */
            $companyname = Entity::where('entityid', $entityid)->pluck('companyname')->first();
            $date = date("Y-m-d");
            // $file_path = "/var/cbpo/public/financial_analysis/CreditBPO Financial Analysis - " . $companyname . " as of " . $date . ".pdf";
            $file_path = $this->getFinancialReportPath($entityid);
            $source = "/var/cbpo/document.pdf";

            /** Move document.pdf */
            exec("mv '$source' '$file_path'", $out);
            
            echo 'Scraper run successfully!';
        }else{
            /** Copy ReadyRatios file to local environment folder  */
            $source = '/opt/lampp/htdocs/business.creditbpo.com/document.pdf';
            if(!copy($source, $financialReportId)){
                echo "File not copied";
            }else{
                echo "File copied";
            }
        }
    }

    protected function getLoggedInUrl() {
        $config = $this->getConfig()['login'];
        $page = $this->scraper->fetch($config['url'], $config['params'], $config['method']);

        if (!$page) {
            throw new ReadyRatiosScraperException('Login URL does not exist!');
        }

        $contents = $page->getBody()->getContents();
        $domDocument = $this->scraper->getDomDocument($contents);
        $anchorTag = $this->scraper->getNode(
                $domDocument,
                '//div[@class="online"]/a');

        if (!$anchorTag) {
            throw new ReadyRatiosScraperException('Anchor Tag missing!');
        }

        // Check if we have properly logged in
        $href = $anchorTag->getAttribute('href');
        $loginUrl = parse_url($href);
        parse_str($loginUrl['query'], $queryParams);

        if (empty($queryParams) ||
            !isset($queryParams['ext_login']) ||
            $queryParams['ext_login'] !== $config['params']['form_params']['USER_LOGIN']) {
            throw new ReadyRatiosScraperException('Failed to login Ready Ratio account!');
        }

        $this->scraper->fetch($href);
    }

    protected function processReport($vfaContent, $fullReport = false) {
        $config = $this->getConfig();
        $uploadConfig = $config['upload'];
        $params = $uploadConfig['params'];
        $params['multipart'][] = [
            'name' => 'upload_fname[]',
            'contents' => json_encode($vfaContent),
            'filename' => 'temp.vfa',
            'headers' => [
                'Content-Type' => 'application/octet-stream'
            ],
        ];

        $page = $this->scraper->fetch(
            $uploadConfig['url'],
            $params,
            $uploadConfig['method']
        );

        if (!$page) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "Failed to upload VFA file!",
                "error" => "Failed to upload VFA file!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Failed to upload VFA file!');
        }
        $contents = $page->getBody()->getContents();
        $domDocument = $this->scraper->getDomDocument($contents);
        $titleNode = $this->scraper->getNode(
            $domDocument,
            '//input[@id="title"]');

        /*
         * Simple check if the page with uploaded content has the
         * same title as the VFA
         */
        if ($titleNode->getAttribute('value') !== $vfaContent['title']) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "ReadyRatio Uploaded VFA file doesn\'t match!",
                "error" => "ReadyRatio Uploaded VFA file doesn\'t match!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Uploaded VFA file doesn\'t match!');
        }


        /**
         * Use default VFA config as a more reliable form input for scraping.
         * Problem with depending on the DOM input itself is that the front-end
         * functionality might change as discussed in
         * https://creditbpo.atlassian.net/browse/CDP-1038
         */
        $defaultVFAConfig = Config::get('handlers.vfa')['defaults'];
        $formData = array_merge($defaultVFAConfig, [
            'report' => '',
            'OrgFoma' => '',
            'GUseDefaultFormuls' => 0,
            'GOpenReportNewWindow' => 0,
            'GSaveRawData' => 0,
            'GPrintUserIndicators' => 0,
        ]);

        $customFields = $this->getConfig()['custom_fields'];

        /*
         * This is ugly at the moment but it should simplify the loop
         * for several custom fields instead of using XPath to get
         * individual values. There are at least 300 values that this data
         * generates and using XPath would be resource expensive.
         */
        foreach ($customFields as $field) {
            foreach ($vfaContent[$field] as $index => $row) {
                foreach ($row as $key => $value) {
                    $formData[$field."[$index][$key]"] = $value;
                }
            }
        }
        

        $formNodes = $this->scraper->getNodes(
            $domDocument,
            '//form[@name="form3"]//input');
        foreach ($formNodes as $node) {             
            if (empty($node->getAttribute('name')) || $node->getAttribute('disabled') === 'disabled') {
                continue;
            }

            if ($node->getAttribute('value')) {
                    $value = $node->getAttribute('value');
            }

            if($node->getAttribute('name') != "title") {
               if($fullReport) {
                   $formData[$node->getAttribute('name')] = $value;
               }
            } else {
                if(strpos($value,"&") !== false){
                    $value = preg_replace("/[&]/", "And", $value);
                }
               $formData[$node->getAttribute('name')] = $value;
            }
            
        }
        


        $formNodes = $this->scraper->getNodes(
                $domDocument,
                '//form[@name="form3"]//select');

        foreach ($formNodes as $node) {
            if (!$node->getAttribute('name')) {
                continue;
            }

            foreach ($node->childNodes as $child) {
                if ($child->attributes && $child->getAttribute('selected')) {
                    if($node->getAttribute('name') != "GOpenReport"){
                        $formData[$node->getAttribute('name')] = $child->getAttribute('value');
                        break;
                    }
                }
            }
        }
        // Remove section 3.2 and 4 from Report
        if (!$fullReport) {
            unset($formData['og']['oglavlen_3_2']);
            unset($formData['og']['oglavlen_4']);
            unset($formData['og']['oglavlen_4_1']);
            unset($formData['og']['oglavlen_4_2']);
        }

        $sessionConfig = $config['session'];
        $page = $this->scraper->fetch(
            $sessionConfig['url'],
            [
                'form_params' => [
                    'form_data' => http_build_query($formData, 'flags_'),
                    'currentRepStandart' => 'ifrsforms',
                ],
            ],
            $sessionConfig['method']
        );

        if (!$page) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "ReadyRatio Parser page does not exist!",
                "error" => "ReadyRatio Parser page does not exist!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Parser page does not exist!');
        }

        $contents = $page->getBody()->getContents();
        $result = json_decode($contents, true);
        if ($result['ok'] !== 1) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "ReadyRatio Session was not properly saved!",
                "error" => "ReadyRatio Session was not properly saved!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Session was not properly saved!');
        }

        $parserConfig = $config['parser'];
        $page = $this->scraper->fetch(
            $parserConfig['url'],
            [
                'form_params' => [
                    'form_data' => http_build_query($formData, 'flags_'),
                    'currentRepStandart' => 'ifrsforms',
                ],
            ],
            $parserConfig['method']
        );

        if (!$page) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "ReadyRatio Parser was not properly loaded!",
                "error" => "ReadyRatio Parser was not properly loaded!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Parser was not properly loaded!');
        }

        $contents = $page->getBody()->getContents();
        $domDocument = $this->scraper->getDomDocument($contents);

        $processingNode = $this->scraper->getNode(
            $domDocument,
            '//p');

        if (
            strpos(
                $processingNode->nodeValue,
                'Comprehensive Financial Analysis (IFRS)'
            ) === false
        ) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "ReadyRatio Parser page has changed!",
                "error" => "ReadyRatio Parser page has changed!",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );

            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new ReadyRatiosScraperException('Parser page has changed!');
        }
    }

    protected function downloadReport($filename, $entityid) {
        $config = $this->getConfig()['download'];
        $page = $this->scraper->fetch(
            $config['url'],
            [
                'sink' => $filename,
            ],
            $config['method']
        );
        if(file_exists($filename)) {
            $file = file($filename);
            $endfile= trim($file[count($file) - 1]);
            $n="%%EOF";
            $repaired = "";
            if ($endfile !== $n) {
                // Clean PDF using mutool version 1.12.0 
                if(env('APP_ENV') == "local") {
                    // mutool for windows should have correct path.
                    // $split =explode("/", $filename);
                    // $path = str_replace("\\", "\\\\", $split[0]);
                    // $path = $path  . "\\\\";
                    // $input = $path . '"'. $split[1] . '"';
                    // $repaired = $path . '"repaired_' . $split[1] . '"'; 
                    // exec('C:\\Users\\kenneth.manongdo\\Documents\\mupdf2\\mutool clean '. $input . ' '  . $repaired);
                } else {
                    // mutool for ubuntu
                    $path = "" ;
                    $split = explode("/", $filename);
                    for ($i=0; $i < count($split); $i++) { 
                        if((count($split) - 1 ) == $i) {
                            $path .= "'" . $split[$i] . "'";
                        } else {
                            $path .= $split[$i] . "/";
                        }
                    }
                    exec("mutool clean " . $path . " " . $path);
                }
            } 
            $parser = new \Smalot\PdfParser\Parser();
            $pdf = $parser->parseFile($filename);
            $contents = $pdf->getText();
            $this->verifySections($contents, $entityid);
        }
        
        return $filename;
    }


    protected function verifySections($contents,$entityid){
        //supposed sections in the pdf
        $sections = array(
            "1. Table of Contents",
            "1.1. Structure of the Assets and Liabilities",
            "1.2. Net Assets (Net Worth)",
            "1.3. Financial Sustainability Analysis",
            "1.3.1. Key ratios of the company's financial sustainability",
            "1.3.2. Working capital analysis",
            "1.4. Liquidity Analysis",
            "2. Financial Performance",
            "2.1. Overview of the Financial Results",
            "2.2. Profitability Ratios",
            "2.3. Analysis of the Business Activity (Turnover Ratios)",
            "3.1. Key Ratios Summary"
        );
        $error = 0;
        $verifySection = array (
            "Table of Contents" => (substr_count($contents, "2.3. Analysis of the Business Activity (Turnover Ratios)") > 1) ? true : false,
            "Structure of the Assets and Liabilities" => (substr_count($contents, "1.1. Structure of the Assets and Liabilities") > 0) ? true : false,
            "Net Assets (Net Worth)" => (substr_count($contents, "1.2. Net Assets (Net Worth)") > 0) ? true : false,
            "Financial Sustainability Analysis" => (substr_count($contents, "1.3. Financial Sustainability Analysis") > 0) ? true : false,
            "Key ratios of the company's financial sustainability" => (substr_count($contents, "1.3.1. Key ratios of the company") > 0) ? true : false,
            "Working capital analysis" => (substr_count($contents, "1.3.2. Working capital analysis") > 0) ? true : false,
            "Liquidity Analysis" => (substr_count($contents, "1.4. Liquidity Analysis") > 0) ? true : false,
            "Financial Performance" => (substr_count($contents, "2. Financial Performance") > 0) ? true : false,
            "Overview of the Financial Results" => (substr_count($contents, "2.1. Overview of the Financial Results") > 0) ? true : false,
            "Profitability Ratios" => (substr_count($contents, "2.2. Profitability Ratios") > 0) ? true : false,
            "Analysis of the Business Activity (Turnover Ratios)" => (substr_count($contents, "2.3. Analysis of the Business Activity (Turnover Ratios)") > 0) ? true : false,
            "Key Ratios Summary" => (substr_count($contents, "3.1. Key Ratios Summary") > 0) ? true : false,
        );
        $matches = array();
        foreach( $verifySection as $key => $section){
            if (!$section) {
                echo $section;
                foreach($sections as $sect) {
                    if(strpos($sect, $key) !== false) {
                        $matches[] = $sect;
                    }
                }
                 $error = 1;
            }
        }
        
        //create logs for future reference
        $insert = array(
            "entity_id" => $entityid,
            "data" => json_encode($verifySection),
            "error" => ($error > 0)? json_encode($matches) : null,
            "status" => ($error > 0)? 0 : 1,
            "is_deleted" => 0,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        );

        //serach by entity id
        $ids = DB::table("automate_standalone_rating_logs")->select("id")
                ->where("entity_id", $entityid)
                ->where("is_deleted", 0)
                ->get();
        if($ids){
            foreach ($ids as $id) {
                //update to soft delete
                DB::table("automate_standalone_rating_logs")->where("id",$id->id)->update(array("is_deleted"=>1));
            }
        }
        
        //insert to table.
        DB::table("automate_standalone_rating_logs")->insert($insert);
        if($error) {
            //send email in case error.
            if (env("APP_ENV") == "Production") {
                $mail = new MailHandler();
                $entity = Entity::find($entityid);
                $insert['name'] = $entity->companyname;
                $insert['items'] = $matches;
                $mail->sendAutomateError($insert);
            }
        }
    }
}
