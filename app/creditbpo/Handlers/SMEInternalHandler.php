<?php

namespace CreditBPO\Handlers;


use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\SME as SMEPDF;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal;
use User;
use DB;
use Auth;
class SMEInternalHandler {

    const DEFAULT_LOGIN_ID = 76;
    
    /**
     * @param int $financialReportId
     * @return bool
     */
    public function createSMEInternalReport($entityId) {
        $pdf = new SMEPDF();

        $contents = $pdf->create($entityId)->generate();

        if ($contents) {
            $contents->setOptions(["isPhpEnabled"=> true, "isHtml5ParserEnabled"=> true, "isRemoteEnabled"=> true]); // To set page numbers
            $contents->setPaper('a4', 'portrait');

            $pdf->save();

            return $pdf->getFilenamePath($entityId);
        }
        
        return false;
    }

    public static function getOverwriteReadyRatio($entityId){

    	$exsReadyRatio = DB::table('tblreadyrationdata')
        				->where('entityid',$entityId)
        				->get()
        				->first();
        // echo 'test'; exit;
        // echo '<pre>';
        // print_r($exsReadyRatio);
        // echo '<pre/>';
        // exit;


        if(!empty($exsReadyRatio->entityid)){
        	return $exsReadyRatio;
        }
        
        $financialReportEntity = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])->first();

        if(!$financialReportEntity){
            
            $readyratiodata = new \stdClass();
            $readyratiodata->gross_revenue_growth1 = 0;
            $readyratiodata->gross_revenue_growth2 = 0;
            $readyratiodata->gross_profit_margin2 = 0;
            $readyratiodata->net_income_growth1 = 0;
            $readyratiodata->net_income_growth2 = 0;
            $readyratiodata->net_profit_margin2 = 0;
            $readyratiodata->net_cash_margin2 = 0;
            $readyratiodata->current_ratio2 = 0;
            $readyratiodata->debt_equity_ratio2 = 0;
            $readyratiodata->cc_year1 = 0;
            $readyratiodata->cc_year3 = 0;
            $readyratiodata->startingYear = 0;
            $readyratiodata->endingYear = 0;

            return $readyratiodata;
        }

        $readyratiodata = new \stdClass();
        $financialReport = FinancialReport::find($financialReportEntity->id);

        $gross_revenue_growth = [];
        $balance_sheets = $financialReport->balanceSheets()->get();
        $income_statements = $financialReport->IncomeStatements()->get();
        $max_count = @count($income_statements)-2;
        $cashRatioPositiveValues = [];
        $CIPercentageChange = [];
        $i = 0; 

        $years = [];
        for($x=count($balance_sheets); $x > 0 ; $x--){
            $years[] = (int)$financialReport->year - $x + 1;
        }
        
        $years_count = count($years);

        //print_r($years); exit;

        for($x=$max_count; $x >= 0 ; $x--){
            
            if(!empty($income_statements[$x+1])){
                if($income_statements[$x+1]->Revenue != 0){
                    
                    /*$dividend = $income_statements[$x+1]->Revenue;
                    if($dividend != 0 && $dividend != '0.00'){
                        $gross_revenue_growth[] = number_format((($income_statements[$x]->Revenue -
                            $income_statements[$x+1]->Revenue) / 
                            $dividend) * 100, 1, '.', ',');;
                    }*/
                    $dividend = $income_statements[$x+1]->Revenue;
                    if($dividend != 0 && $dividend != '0.00'){
                        $gross_revenue_growth[] = number_format(($income_statements[$x]->Revenue -
                            $income_statements[$x+1]->Revenue), 2, '.', '');
                    } 
                } else {
                    $gross_revenue_growth[] = 0;
                }
            }

            $currentAssets = $balance_sheets[$x]->CurrentAssets;
            $currentLiabilities = $balance_sheets[$x]->CurrentLiabilities;

            // Cash Ratio, ∞ < crit. < 0.05 ≤ unsat. < 0.2 ≤ good < 0.25 ≤ excel. < ∞
            $cashAndCashEquivalents = $balance_sheets[$x]->CashAndCashEquivalents;

            $cashRatioChangeValue = 0;

            if($currentLiabilities > 0){
            	$cashRatio = $cashAndCashEquivalents / $currentLiabilities;
            	$cashRatio = round($cashRatio, 2, PHP_ROUND_HALF_DOWN);
        	}else{
        		$cashRatio = 0;
        	}

            if ($i == 0) {
                $firstCashRatio = $cashRatio;
            }
            else if ($i == $max_count) {
                $lastCashRatio = $cashRatio;
                $cashRatioChangeValue = $lastCashRatio - $firstCashRatio;
                $cashRatioChangeColor = 'green';
                if($cashRatioChangeValue < 0)
                {
                    $cashRatioChangeColor = 'red';
                }
            }
            $i++;
            $cashRatioColor = 'red';
            if($cashRatio >= 0.2)
            {
                $cashRatioColor = 'green';
                array_push($cashRatioPositiveValues, $cashRatio);
            }

            if(!empty($income_statements[$x-1]) && !empty($income_statements[$x])){
                if(($income_statements[$x]->ComprehensiveIncome) != 0) {
                        
                    $CIPercentageChange = 0;
                    $dividend = ($income_statements[$x]->ComprehensiveIncome);
                    if($dividend != 0 && $dividend != '0.00'){
                        $CIPercentageChange = ((($income_statements[$x-1]->ComprehensiveIncome) -
                                ($income_statements[$x]->ComprehensiveIncome)) / $dividend) * 100;
                    }

                } else {
                    $CIPercentageChange = 0;
                }
            }

            if($x == $max_count){
                $readyratiodata->net_income_growth2 = $CIPercentageChange;
            }

            if($x == 0){
                $readyratiodata->net_income_growth1 = $CIPercentageChange;
            }

        }

        
        $grg_count = count($gross_revenue_growth);

        if(!empty($gross_revenue_growth[$grg_count-2]))
        $readyratiodata->gross_revenue_growth2 = $gross_revenue_growth[$grg_count-2];

        if(!empty($gross_revenue_growth[$grg_count-1]))
        $readyratiodata->gross_revenue_growth1 = $gross_revenue_growth[$grg_count-1];

    	$gross1 = 0;
    	$gross2 = 0;

    	if($income_statements[0]->Revenue > 0){
        	$gross1 = ($income_statements[0]->GrossProfit / 
                    $income_statements[0]->Revenue)*100;
    	}

        if(!empty($income_statements[$max_count]->Revenue)){
            if($income_statements[$max_count]->Revenue > 0){
                $gross2 = ($income_statements[$max_count]->GrossProfit / 
                        $income_statements[$max_count]->Revenue)*100;
            }    
        }
    	
    	
        $gross1 = number_format($gross1, 1, '.', ',');
        $gross2 = number_format($gross2, 1, '.', ',');

        $readyratiodata->gross_profit_margin2 = $gross1 - $gross2;

        $readyratiodata->net_cash_margin2 = number_format($cashRatioChangeValue, 2);
        $readyratiodata->current_ratio2 = number_format($cashRatioChangeValue, 2);

        $financial_sustainability = FinancialAnalysisInternal::financialSustainability($financialReport);

        $readyratiodata->debt_equity_ratio2 = $financial_sustainability['financial_change']['FinancialLeverage_change']; 

        if(empty($readyratiodata->gross_revenue_growth1))
            $readyratiodata->gross_revenue_growth1 = 0;

        if(empty($readyratiodata->gross_revenue_growth2))
            $readyratiodata->gross_revenue_growth2 = 0;

        if(empty($readyratiodata->gross_profit_margin2))
            $readyratiodata->gross_profit_margin2 = 0;

        if(empty($readyratiodata->net_income_growth1))
            $readyratiodata->net_income_growth1 = 0;

        if(empty($readyratiodata->net_income_growth2))
            $readyratiodata->net_income_growth2 = 0;

        if(empty($readyratiodata->net_profit_margin2))
            $readyratiodata->net_profit_margin2 = 0;

        if(empty($readyratiodata->net_cash_margin2))
            $readyratiodata->net_cash_margin2 = 0;

        if(empty($readyratiodata->current_ratio2))
            $readyratiodata->current_ratio2 = 0;

        if(empty($readyratiodata->debt_equity_ratio2))
            $readyratiodata->debt_equity_ratio2 = 0;

        $readyratiodata->cc_year1 = '';
        if($years_count >= 3)
            $readyratiodata->cc_year1 = $years[$years_count-3];
        
        $readyratiodata->cc_year2 = '';
        if($years_count >= 2)
            $readyratiodata->cc_year2 = $years[$years_count-2];
        
        $readyratiodata->cc_year3 = '';
        if($years_count >= 1)
            $readyratiodata->cc_year3 = $years[$years_count-1];

        $readyratiodata->year1 = $readyratiodata->cc_year2;
        $readyratiodata->year2 = $readyratiodata->cc_year3;

        $readyratiodata->entityid = $entityId;
        $readyratiodata->loginid = Auth::id();

        //compute turnover ratios
        $turnOverRatios = Self::turnOverRatios($financialReport);

        //get receivables turnover
        if(!empty($turnOverRatios[0]->ReceivablesTurnover))
        	$readyratiodata->receivables_turnover1 = $turnOverRatios[0]->ReceivablesTurnover;
        
        if(!empty($turnOverRatios[1]->ReceivablesTurnover))
        	$readyratiodata->receivables_turnover2 = $turnOverRatios[1]->ReceivablesTurnover;
        
        if(!empty($turnOverRatios[2]->ReceivablesTurnover))
        	$readyratiodata->receivables_turnover3 = $turnOverRatios[2]->ReceivablesTurnover;
        
        if(empty($readyratiodata->receivables_turnover1))
        	$readyratiodata->receivables_turnover1 = 0;
        if(empty($readyratiodata->receivables_turnover2))
        	$readyratiodata->receivables_turnover2 = 0;
        if(empty($readyratiodata->receivables_turnover3))
        	$readyratiodata->receivables_turnover3 = 0;

        if(!empty($turnOverRatios[0]->InventoryTurnover))
        	$readyratiodata->inventory_turnover1 = $turnOverRatios[0]->InventoryTurnover;

        if(!empty($turnOverRatios[1]->InventoryTurnover))
        	$readyratiodata->inventory_turnover2 = $turnOverRatios[1]->InventoryTurnover;

        if(!empty($turnOverRatios[2]->InventoryTurnover))
        	$readyratiodata->inventory_turnover3 = $turnOverRatios[2]->InventoryTurnover;
        
        if(empty($readyratiodata->inventory_turnover1))
        	$readyratiodata->inventory_turnover1 = 0;
        if(empty($readyratiodata->inventory_turnover2))
        	$readyratiodata->inventory_turnover2 = 0;
        if(empty($readyratiodata->inventory_turnover3))
        	$readyratiodata->inventory_turnover3 = 0;

        if(!empty($turnOverRatios[0]->PayableTurnover))
        	$readyratiodata->accounts_payable_turnover1 = $turnOverRatios[0]->PayableTurnover;
        if(!empty($turnOverRatios[1]->PayableTurnover))
        	$readyratiodata->accounts_payable_turnover2 = $turnOverRatios[1]->PayableTurnover;
        if(!empty($turnOverRatios[2]->PayableTurnover))
        	$readyratiodata->accounts_payable_turnover3 = $turnOverRatios[2]->PayableTurnover;
        if(empty($readyratiodata->accounts_payable_turnover1))
        	$readyratiodata->accounts_payable_turnover1 = 0;
        if(empty($readyratiodata->accounts_payable_turnover2))
        	$readyratiodata->accounts_payable_turnover2 = 0;
        if(empty($readyratiodata->accounts_payable_turnover3))
        	$readyratiodata->accounts_payable_turnover3 = 0;

        if(!empty($turnOverRatios[0]->CCC))
        	$readyratiodata->cash_conversion_cycle1 = $turnOverRatios[0]->CCC;
        if(!empty($turnOverRatios[1]->CCC))
        	$readyratiodata->cash_conversion_cycle2 = $turnOverRatios[1]->CCC;
        if(!empty($turnOverRatios[2]->CCC))
        	$readyratiodata->cash_conversion_cycle3 = $turnOverRatios[2]->CCC;

        // echo '<pre>';
        // print_r($readyratiodata);
        // echo '<pre/>';
        // exit;

        $arrReadyRatio = (array) $readyratiodata;

        DB::table('tblreadyrationdata')
        	->insert($arrReadyRatio);

        $readyratiodata->startingYear = $years[0];
        $readyratiodata->endingYear = $years[0] + 3;

        // echo '<pre>';
        // print_r($arrReadyRatio);
        // echo '<pre/>';
        // exit;
        
        return $readyratiodata;
    }

    public static function turnoverRatios($financialReport){
        $balance_sheets = $financialReport->balanceSheets()->get();
        $incomeStatements = $financialReport->incomeStatements()->get();

        $limit = 0;
        foreach($balance_sheets as $key => $sheets){
            if( $sheets->NoncurrentAssets == "0.00" &&
                $sheets->CurrentAssets == "0.00" &&
                $sheets->Equity == "0.00" &&
                $sheets->NoncurrentLiabilities == "0.00" &&
                $sheets->CurrentLiabilities == "0.00" &&
                $sheets->Liabilities == "0.00" &&
                $sheets->EquityAndLiabilities == "0.00"
            ){

            }
            else{
                $limit += 1;
            }
        }
        $turnoverRatios = [];
        foreach($balance_sheets as $key => $sheets){
            /** Get number of days */
            $income_cnt = 0;
            $num_days=0; 
            $year=$sheets->Year;
            for( $month=1; $month<=12; $month++){ 
                $num_days = $num_days + cal_days_in_month(CAL_GREGORIAN,$month,$year);
            }

            foreach($incomeStatements as $cnt => $income){

                if($income->Year === $sheets->Year){
                    $income_cnt = $cnt;
                    }
            }

            foreach($balance_sheets as $b){
                if($b->Year == ($sheets->Year -1)){
                    /** Caclculate Receivables turnover (days sales outstanding) (ReceivablesTurnover) */
                    $ReceivablesTurnover = 0;
                    if( ($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $ReceivablesTurnover = ((($sheets->TradeAndOtherCurrentReceivables + $b->TradeAndOtherCurrentReceivables)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Accounts payable turnover (days payable outstanding) (PayableTurnover) */
                    $PayableTurnover = 0;
                    if(( ($incomeStatements[$income_cnt]->CostOfSales  + $incomeStatements[$income_cnt]->CostOfSales) / $num_days ) != 0){
                        $PayableTurnover = ($sheets->TradeAndOtherCurrentPayables + $sheets->CurrentProvisionsForEmployeeBenefits + $b->TradeAndOtherCurrentPayables + $b->CurrentProvisionsForEmployeeBenefits)
                                        / (( ($incomeStatements[$income_cnt]->CostOfSales  + $incomeStatements[$income_cnt]->CostOfSales) / $num_days ));
                    }

                    /** Inventory turnover (days inventory outstanding) (InventoryTurnover) */
                    $InventoryTurnover = 0;
                    if(($incomeStatements[$income_cnt]->CostOfSales / $num_days) != 0){
                        $InventoryTurnover = ((($sheets->Inventories + $b->Inventories)/2) / ($incomeStatements[$income_cnt]->CostOfSales / $num_days));
                    }

                    /** Calculate Asset turnover (days) (AssetTurnover) */
                    $AssetTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $AssetTurnover = ((($sheets->Assets + $b->Assets)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Calculate Current asset turnover (days) (CAssetTurnover) */
                    $CAssetTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $CAssetTurnover = ((($sheets->CurrentAssets + $b->CurrentAssets)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    } 

                    /** Calculate Capital turnover (days) (CapitalTurnover) */
                    $CapitalTurnover = 0;
                    if(($incomeStatements[$income_cnt]->Revenue/$num_days) != 0){
                        $CapitalTurnover = ((($sheets->Equity + $b->Equity)/2) / ($incomeStatements[$income_cnt]->Revenue/$num_days));
                    }

                    /** Calculate Cash conversion cycle (CCC) */
                    $CCC = 0;
                    $CCC = (($InventoryTurnover + $ReceivablesTurnover) - $PayableTurnover);

                    $turnoverRatios[] = (object) [
                        'year'                  => round($sheets->Year),
                        'num_days'              => $num_days,
                        'ReceivablesTurnover'   => round($ReceivablesTurnover),
                        'PayableTurnover'       => round($PayableTurnover),
                        'InventoryTurnover'     => round($InventoryTurnover),
                        'AssetTurnover'         => round($AssetTurnover),
                        'CAssetTurnover'        => round($CAssetTurnover),
                        'CapitalTurnover'       => round($CapitalTurnover),
                        'CCC'                   => round($CCC),
                    ];
                }
            }

            // if($key == $limit-2){
            //     break;
            // }
        }

        usort($turnoverRatios, function ($a, $b) {
            $t1 = strtotime($a->year);
            $t2 = strtotime($b->year);
            return $t1 - $t2;
        });
        return $turnoverRatios;
    }
}
