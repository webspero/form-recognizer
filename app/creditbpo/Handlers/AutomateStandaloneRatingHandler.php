<?php

/**
 * Look for http://localhost/summary/smesummary/583/fs
 */

namespace CreditBPO\Handlers;

use Carbon\Carbon;
use CmapSearchData;
use CmapSearchQuery;
use CmapTableHeaders;
use Google\Cloud\Core\ServiceBuilder;
use Illuminate\Support\Facades\Log;
use CreditBPO\Exceptions\AutomateStandaloneRatingException;
use CreditBPO\Exceptions\ReadyRatiosScraperException;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Handlers\VfaHandler;
use CreditBPO\Models\AutomateStandaloneRating;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Entity;
use CreditBPO\Models\KeyManager;
use CreditBPO\Models\Shareholder;
use CreditBPO\Models\Director;
use CreditBPO\Models\SMEScore;
use CreditBPO\Services\IbakusService;
use \Exception as Exception;

use CreditBPO\Services\MailHandler;
use DateTime;
use DateTimeZone;
use DB;
use Imagick;
use PseCompanyOfficer;
use simple_html_dom;

class AutomateStandaloneRatingHandler
{
    protected $model;
    protected $financialAnalysis;
    protected $financialAnalysisHandler;
    protected $readyRatioScraperHandler;
    protected $vfaHandler;
    protected $success;
    protected $errors;

    /**
     * @param AutomateStandaloneRating $model
     * @param FinancialAnalysisHandler $financialAnalysis
     */

    //--------------------------------------------------------------------------
    //  Function 56.0: __construct
    //      Initialization and Declaration for AutomateStandaloneRating Handler
    //--------------------------------------------------------------------------
    public function __construct(
        AutomateStandaloneRating $model = null,
        FinancialAnalysisHandler $financialAnalysis = null
    ) {
        $this->model = $model ?: new AutomateStandaloneRating();
        $this->financialAnalysisHandler = $financialAnalysis ?: new FinancialAnalysisHandler();
    }

    /**
     * @param int $entityId
     * @param string $status
     * @return bool
     */

    //----------------------------------------------------------
    //  Function 56.1: registerReport
    //      Register Report on table automate_standalone_rating
    //----------------------------------------------------------
    public function registerReport(
        $entityId,
        $status = AutomateStandaloneRating::STATUS_PENDING
    ) {
        $entity = $this->model
            ->where('entity_id', '=', $entityId)
            ->where('status', '=', $status)
            ->pluck('entity_id');

        // don't add entity with same status
        if (@count($entity) > 0) {
            return false;
        }

        $this->model->create([
            'entity_id' => $entityId,
            'status' => AutomateStandaloneRating::STATUS_PENDING,
        ]);

        return true;
    }

    /**
     * @param int $entityId
     * @param string $status
     * @return bool
     */

    //-------------------------------------------------------------------
    //  Function 56.1: registerInnodataReport
    //      Register Innodata Report on table automate_standalone_rating
    //------------------------------------------------------------------
    public function registerInnodataReport(
        $entityId,
        $status = AutomateStandaloneRating::STATUS_INNODATA_PENDING
    ) {
    	
        /** Check if report has financial template */
        $fs_template = DB::table('tbldocument')
            ->where('entity_id', '=', $entityId)
            ->where('document_group', '=', 21)
            ->where('upload_type', '=', 5)
            ->where('is_deleted', 0)
            ->orderBy('document_type', 'ASC')
            ->get()
            ->toArray();

        $entity = $this->model
            ->where('entity_id', '=', $entityId)
            // ->where('status', '=', $status)
            ->pluck('entity_id');

        //echo 'test2'; exit;
        // don't add entity with same status
        // if (@count($entity) > 0) {
        //     return false;
        // }

        /** Skip process of innodata if user has financial template */
        if(!empty($fs_template)){
        	$status = AutomateStandaloneRating::STATUS_PENDING;
        }else{
        	
        	if(env("APP_ENV") != "Production"){
        		 $status = AutomateStandaloneRating::STATUS_INNODATA_WAITING;
        	}

        }

        $automate_standalone_rating = $this->model->where('entity_id',$entityId)->get()->toArray();

    	if(!empty($automate_standalone_rating)){
    		
    		$this->model->where('entity_id',$entityId)->update([
                'status' => $status
            ]);
    	}else{
    		
    		$this->model->create([
                'entity_id' => $entityId,
                'status' => $status
            ]);
    	}

        return true;
    }

    /**
     * @param int $entityId
     * @return bool
     */

    //------------------------------------------------------------------
    //  Function 56.1: processReports
    //      Process Report for Automation
    //------------------------------------------------------------------
    public function processReports()
    {
        $pendingReports = $this->model->getPendingReports();

        //Set retrieved reports to in progress immediately
        foreach ($pendingReports as $pendingReport) {
            $pendingReport->status = AutomateStandaloneRating::STATUS_IN_PROGRESS;
            $pendingReport->save();
        }

        // Process individually
        $result = false;
        $success = [];
        $failed = [];
        $error = 0;

        foreach ($pendingReports as $key => $pendingReport) {

            /** If report is more than 1, sleep 3 minutes before processing new reports.  */
            if($key != 0){
                /** Sleep for 3 minutes */
                sleep(180);
            }

            $result = null;

            $entityModel = new Entity();
            $entity = $entityModel->find($pendingReport->entity_id);

            try {
                echo 'Processing ' . $pendingReport->entity_id .
                    ' - ' . $entity->companyname . "\n";

                if ($entity->is_premium == PREMIUM_REPORT) {
                    // $ibakus = new IbakusService();
                    // $ibakus->getRelatedPersonalities($pendingReport->entity_id);
                    $this->crossmatchOpensanction($entity);
                }

                // $this->detectDueDateUtilityBill($entity);   #temporary
                $this->calculateAverageBalance($entity);

                /* Disable CMAP Function for Simplified and Standalone Report (CDP-1604) */
                if($entity->is_premium == PREMIUM_REPORT){
                    $this->cmapSearch($entity->companyname);

                    if ($entity->pse_company_id !== null) {
                        $this->cmapSearchOfficers($entity->companyname, $entity->pse_company_id);
                    }
                }

                $result = $this->processReport($pendingReport->entity_id);
            } catch (AutomateStandaloneRatingException $e) {
                $this->logError($e);
                $message = $e->getMessage();
            } catch (ReadyRatiosScraperException $e) {
                $this->logError($e);
                $message = $e->getMessage();
            } catch (Exception $e) {
                $this->logError($e);
                $message = $e->getMessage();
            }

            if ($result != null) {
                $pendingReport->status = AutomateStandaloneRating::STATUS_SUCCESS;
                $success[] = $pendingReport->entity_id . ' - ' . $entity->companyname;
                echo 'Result: Success' . "\n\n";
            } else {
                $pendingReport->status = AutomateStandaloneRating::STATUS_ERROR;
                $failed[] = $pendingReport->entity_id .
                    ' - ' . $entity->companyname . ': ' . $message;
                echo 'Result: Failed' . "\n\n";
            }

            $pendingReport->save();

            //check in automate logs if no error.
            $entityid = $pendingReport->entity_id;
            $cronData = DB::table("automate_standalone_rating_logs")->select("error")
                ->where("entity_id", $entityid)
                ->where("is_deleted", 0)
                ->get();
            foreach ($cronData as $data) {
                if ($data->error != null) {
                    //Return completed report to financial analyst
                    DB::table("tblsmescore")->where("entityid", $entityid)->delete();
                    DB::table("tblreadyrationdata")->where("entityid", $entityid)->delete();
                    DB::table("tbldocument")->where("entity_id", $entityid)->where("document_group", 99999)->delete();
                    // AutomateStandAloneRating::where("entity_id",$entityid)->update(array("status"=>0)); //set in case of automation
                    $entity->where("entityid", $entityid)->update(array("status" => 5, "pdf_grdp" => null, "cbpo_pdf" => null));
                    $error += 1;
                }
            }
        }

        if (@count($success) > 0 || @count($failed) > 0) {
            $dateConverted = new DateTime('NOW', new DateTimeZone('Asia/Manila'));
            $date = $dateConverted->format('m/d/Y h:i A');
            $vars = [
                'success' => $success,
                'failed' => $failed,
                'date_processed' => $date,
            ];

            if (env("APP_ENV") == "Production" && $error == 0) {
                $mailHandler = new MailHandler();
                $result = $mailHandler->sendAutomateStandaloneEmail($vars);

                if ($result) {
                    echo 'Successfully sent automated report result email.' . "\n\n";
                } else {
                    echo 'Failed to send automated report result email.' . "\n\n";
                }
            } else {
                if ($error > 0) {
                    echo 'Something went missing in the report.' . "\n\n";
                }
            }
        }
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function processReport($entityId)
    {
        $entityModel = new Entity();
        $entity = $entityModel->find($entityId);

        if (!$entity) {
            throw new AutomateStandaloneRatingException(
                'Entity not found. ID: ' . $entityId
            );
        }

        // if ((int) $entity->status != Entity::STATUS_FOR_REVIEW) {
        //     throw new AutomateStandaloneRatingException(
        //         'Entity not using "For Review" status. ID: ' . $entityId
        //     );
        // }

        $financialReportModel = new FinancialReport();
        $financialReport = $financialReportModel
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();
        $financialReportId = $financialReport->id;


        if (!$financialReportId) {
            throw new AutomateStandaloneRatingException(
                'No Financial Report ID for Entity ID: ' . $entityId
            );
        }
        
        return $this->financialAnalysisHandler->automatePostFinishScoring($entityId);
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function processSingleReport($entityId)
    {
        $entityModel = new Entity();
        $entity = $entityModel->find($entityId);

        if (!$entity) {
            throw new AutomateStandaloneRatingException(
                'Entity not found. ID: ' . $entityId
            );
        }

        // if ((int) $entity->status != Entity::STATUS_FOR_REVIEW) {
        //     throw new AutomateStandaloneRatingException(
        //         'Entity not using "For Review" status. ID: ' . $entityId
        //     );
        // }

        try {

            if ($entity->is_premium == PREMIUM_REPORT) {
                $this->crossmatchOpensanction($entity);
            }

            $this->calculateAverageBalance($entity);

            /* Disable CMAP Function for Simplified and Standalone Report (CDP-1604) */
            if($entity->is_premium == PREMIUM_REPORT){
                $this->cmapSearch($entity->companyname);

                if ($entity->pse_company_id !== null) {
                    $this->cmapSearchOfficers($entity->companyname, $entity->pse_company_id);
                }
            }

            $result = $this->processReport($entityId);
        } catch (AutomateStandaloneRatingException $e) {
            $this->logError($e);
            $message = $e->getMessage();
        } catch (ReadyRatiosScraperException $e) {
            $this->logError($e);
            $message = $e->getMessage();
        } catch (Exception $e) {
            $this->logError($e);
            $message = $e->getMessage();
        }

        if (!empty($result)) {
            $success[] = $entityId . ' - ' . $entity->companyname;
            //echo 'Result: Success' . "\n\n";
        } else {
            $failed[] = $entityId .
                ' - ' . $entity->companyname . ': ' . $message;
            //echo 'Result: Failed' . "\n\n";
        }

        //check in automate logs if no error.
        $entityid = $entityId;
        $cronData = DB::table("automate_standalone_rating_logs")->select("error")
            ->where("entity_id", $entityid)
            ->where("is_deleted", 0)
            ->get();
        foreach ($cronData as $data) {
            if ($data->error != null) {
                //Return completed report to financial analyst
                DB::table("tblsmescore")->where("entityid", $entityid)->delete();
                DB::table("tblreadyrationdata")->where("entityid", $entityid)->delete();
                DB::table("tbldocument")->where("entity_id", $entityid)->where("document_group", 99999)->delete();
                // AutomateStandAloneRating::where("entity_id",$entityid)->update(array("status"=>0)); //set in case of automation
                $entity->where("entityid", $entityid)->update(array("status" => 5, "pdf_grdp" => null, "cbpo_pdf" => null));
                $error += 1;
            }
        }

        $financialReportModel = new FinancialReport();
        $financialReport = $financialReportModel
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();
        $financialReportId = $financialReport->id;

        // if (!$financialReportId) {
        //     return 0;
        // }

        // return 1;
        $this->financialAnalysisHandler->automatePostFinishScoringInternal($entityId);
        return 1;
    }

    public function processScoring($entityId)
    {
        $smescore = SMEScore::where('entityid',$entityId);

        if(empty($smescore)){
            $this->financialAnalysisHandler->automatePostFinishScoringInternal($entityId);
        }
        return 1;
    }

    /**
     * @param Exception $error
     * @return void
     */
    protected function logError($error)
    {
        Log::error($error);
    }

    //-----------------------------------------------------
    //  Function 11.2: crossmatchOpensanction
    //  Description: crossmatch function for opensanctions
    //-----------------------------------------------------
    public function crossmatchOpensanction($entity)
    {
        $entity->where("entityid", $entity->entityid)->update(array("negative_findings" => ""));
        // Fetch personalities that need to be crossmatched.
        $keyManagers = KeyManager::select("firstname", "middlename", "lastname", "nationality")
            ->where('is_deleted', 0)
            ->where('entity_id', $entity->entityid)
            ->get();

        $shareHolders = Shareholder::select("name", "firstname", "middlename", "lastname",  "nationality")
            ->where('is_deleted', 0)
            ->where('entity_id', $entity->entityid)
            ->get();

        $directors = Director::select("name", "firstname", "middlename", "lastname",  "nationality")
            ->where('is_deleted', 0)
            ->where('entity_id', $entity->entityid)
            ->get();

        // start crossmatch
        $lists = array();
        foreach ($keyManagers as $key) {
            // get nationality code
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $key->nationality)->first();
            $nationality = "";
            $data = array(
                "firstname" => strtolower($key->firstname),
                "lastname" => ($key->middlename) ? strtolower($key->middlename .  " " . $key->lastname) : strtolower($key->lastname)
            );
            if ($ncode) {
                $nationality = $ncode->country_code;
            }
            $name = $data['firstname'] . " " . $data['lastname'];
            if (!in_array(strtolower($name), $lists)) {
                $lists[] = strtolower($name);
                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
            }
        }

        foreach ($shareHolders as $key => $share) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $share->nationality)->first();
            $nationality = "";
            $data = array(
                "firstname" => strtolower($share->firstname),
                "lastname" => ($share->middlename) ? strtolower($share->middlename .  " " . $share->lastname) : strtolower($share->lastname)
            );
            if ($ncode) {
                $nationality = $ncode->country_code;
            }
            $name = $data['firstname'] . " " . $data['lastname'];
            if (!in_array(strtolower($name), $lists)) {
                $lists[] = strtolower($name);
                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
            }
        }

        foreach ($directors as $key => $dir) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $dir->nationality)->first();
            $nationality = "";
            $data = array(
                "firstname" => strtolower($dir->firstname),
                "lastname" => ($dir->middlename) ? strtolower($dir->middlename .  " " . $dir->lastname) : strtolower($dir->lastname)
            );
            if ($ncode) {
                $nationality = $ncode->country_code;
            }
            $name = $data['firstname'] . " " . $data['lastname'];
            if (!in_array(strtolower($name), $lists)) {
                $lists[] = strtolower($name);
                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
            }
        }
        return;
    }

    //-----------------------------------------------------
    //  Function 11.3: findByFirstAndLastname
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByFirstAndLastname($data, $entity_id, $nationality)
    {
        $entity = Entity::where("entityid", $entity_id)->first();
        $records = DB::table("open_sanctions")->select("charges")->where($data);
        if ($nationality) {
            $records->where("nationality", "like", "%{$nationality}%");
        }

        $records = $records->get();

        if ($records->count() > 0) {
            $name = ucwords($data['firstname'] . " " . $data['lastname']);
            $charges = "<tr><td> {$name} </td><td><ul>";
            if ($entity->negative_findings == "") {
                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
            } else {
                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
            }
            foreach ($records as $key => $value) {
                if ($value->charges) {
                    $charges .= "<li> " . $value->charges . " </li>";
                }
            }
            $charges .= "</ul></td></tr></tbody></table>";
            
            // update negative findings
            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
        }
        return;
    }

    //-----------------------------------------------------
    //  Function 11.4: findByName
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByName($name, $entity_id, $nationality)
    {
        $entity = Entity::where("entityid", $entity_id)->first();
        $name = strtolower($name);
        if ($nationality) {
            $nationality = " AND nationality LIKE '%{$nationality}%'";
        }
        $records = DB::select("SELECT charges FROM open_sanctions WHERE CONCAT(firstname, ' ', lastname) = '{$name}' {$nationality}");

        if (!empty($records)) {
            $name = ucwords($name);
            $charges = "<tr><td> {$name} </td><td><ul>";
            if ($entity->negative_findings == "") {
                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
            } else {
                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
            }
            foreach ($records as $key => $value) {
                if ($value->charges) {
                    $charges .= "<li> " . $value->charges . " </li>";
                }
            }
            $charges .= "</ul></td></tr></tbody></table>";
            // update negative findings
            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
        }
        return;
    }

    private function detectDueDateUtilityBill($entity)
    {
        $ubillFiles = DB::table('tbldocument')->where("is_deleted", 0)->where("entity_id", $entity->entityid)->where("document_type", 0)->where("document_group", 61)->get();
        $destinationPath = public_path('documents/');
        $bills = [];

        foreach ($ubillFiles as $ubillFile) {
            $image = $destinationPath . $ubillFile->document_rename;
            $enhancedImage = $destinationPath . $entity->entityid . '/' . pathinfo($image, PATHINFO_FILENAME) . "(enhanced)." . pathinfo($image, PATHINFO_EXTENSION);
            $imageName = pathinfo($image, PATHINFO_FILENAME) . "." . pathinfo($image, PATHINFO_EXTENSION);
            copy($image, $enhancedImage);

            // $imagick = new Imagick($enhancedImage);
            // $imagick->sharpenimage(20, 10);
            // $imagick->enhanceImage();
            // $imagick->getImageBlob();

            // file_put_contents($enhancedImage, $imagick);

            $cloud = new ServiceBuilder([
                'keyFilePath' => public_path('visionAPI.json'),
                'projectId' => 'sixth-edition-253508'
            ]);

            $vision = $cloud->vision();
            $image = $vision->image(file_get_contents($enhancedImage), ['text']);
            $textAnnotations = $vision->annotate($image)->text();

            // Remove first row (full text)
            array_shift($textAnnotations);

            /* ----------------------------
            ----get due date Column Start--
            -----------------------------*/
            $isDueDate = STS_NG;
            $isBillDate = STS_NG;
            $texts = [];
            $textsBillDate = [];

            $billType = '';

            foreach ($textAnnotations as $key => $text) {
                if ($billType == '') {
                    if (strcasecmp("meralco", $text->info()['description']) == 0) {
                        $billType = "meralco";
                        break;
                    } else if (strcasecmp("manila", $text->info()['description']) == 0 and strcasecmp("water", $textAnnotations[$key + 1]->description()) == 0) {
                        $billType = "manila_water";
                        break;
                    } else if (strcasecmp("pldt", $text->info()['description']) == 0) {
                        $billType = "pldt";
                        break;
                    } else if (strcasecmp("maynilad", $text->info()['description']) == 0) {
                        $billType = "maynilad";
                        break;
                    } else if (strcasecmp("globe", $text->info()['description']) == 0) {
                        $billType = "globe";
                        break;
                    } else if (strcasecmp("smart", $text->info()['description']) == 0) {
                        $billType = "smart";
                        break;
                    } else if (strcasecmp("sun", $text->info()['description']) == 0 and strcasecmp("cellular", $textAnnotations[$key + 1]->description()) == 0) {
                        $billType = "sun";
                        break;
                    }
                }
            }

            if ($billType === "meralco") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Balance"
                        and $textAnnotations[$key + 1]->description() === "From"
                        and $textAnnotations[$key + 2]->description() === "Previous"
                        and $textAnnotations[$key + 3]->description() === "Billing"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"] - 50;
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 165;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 50;
                        $isDueDate = STS_NG;
                    } else {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] == "Balance"
                            and $texts[$key + 1]['text'] == "From"
                            and $texts[$key + 2]['text'] == "Previous"
                            and $texts[$key + 3]['text'] == "Billing"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "Balance" or $txt['text'] == "From" or $txt['text'] == "Previous" or $txt['text'] == "Billing") {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 1;

                    if ($txt['text'] == "Thank") {
                        $isOverDue = 0;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if ($text->info()['description'] === "Bill" and $textAnnotations[$key + 1]->description() === "Date") {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 230;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Bill" and $textsBillDate[$key + 1]['text'] == "Date") {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    } else if ($isRotated) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Bill" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $billDate = str_replace(':', '', $billDate);

                $isMeralco = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "meralco") {
                        $isMeralco = STS_OK;
                        $billKey = $key;
                    }
                }
                if ($isMeralco) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            } else if ($billType == "manila_water") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "PREVIOUS"
                        and $textAnnotations[$key + 1]->description() === "UNPAID"
                        and $textAnnotations[$key + 2]->description() === "AMOUNT"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 500;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x2']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] == "PREVIOUS"
                            and $texts[$key + 1]['text'] == "UNPAID"
                            and $texts[$key + 2]['text'] == "AMOUNT"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (
                        $txt['text'] == "PREVIOUS"
                        or $txt['text'] == "UNPAID"
                        or $txt['text'] == "AMOUNT"
                        or $txt['text'] == "P"
                    ) {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;
                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if ($text->info()['description'] === "Bill" and $textAnnotations[$key + 1]->description() === "Date") {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 500;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Bill" and $textsBillDate[$key + 1]['text'] == "Date") {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Bill" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $isMaynilad = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "manila_water") {
                        $isMaynilad = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isMaynilad) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            } else if ($billType == "pldt") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Remaining"
                        and $textAnnotations[$key + 1]->description() === "Balance"
                        and $textAnnotations[$key + 2]->description() === "from"
                        and $textAnnotations[$key + 3]->description() === "Previous"
                        and $textAnnotations[$key + 4]->description() === "Bill"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 470;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] === "Remaining"
                            and $texts[$key + 1]['text'] === "Balance"
                            and $texts[$key + 2]['text'] === "from"
                            and $texts[$key + 3]['text'] === "Previous"
                            and $texts[$key + 4]['text'] === "Bill"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (
                        $txt['text'] == "Remaining"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill"
                    ) {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Statement"
                        and $textAnnotations[$key + 1]->description() === "Date"
                    ) {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 230;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if (
                            $txt['text'] == "Statement"
                            and $textsBillDate[$key + 1]['text'] == "Date"
                        ) {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $billDate = str_replace(':', '', $billDate);

                $isPldt = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "pldt") {
                        $isPldt = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isPldt) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            } else if ($billType == "maynilad") {
                foreach ($textAnnotations as $key => $text) {
                    if ($text->info()['description'] === "DUE") {
                        if ($textAnnotations[$key + 1]->description() === "DATE") {
                            $isDueDate = STS_OK;
                        }
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 300;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 10;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG) {
                        if ((($txt['text'] == "DUE" or $txt['text'] == "OUE") and $texts[$key + 1]['text'] == "DATE")) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                        } else {
                            unset($texts[$key]);
                            continue;
                        }
                    }

                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "DUE" or $txt['text'] == "DATE" or $txt["text"] == "DUE DATE") {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (count($texts) > 1) {
                        $dueDate = "{$txt['text']} {$texts[$key + 1]['text']} {$texts[$key + 2]['text']}";
                    } else {
                        $dueDate = $txt['text'];
                    }
                    break;
                }
            } else if ($billType == "globe") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Due"
                        and $textAnnotations[$key + 1]->description() === "Immediately"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 170;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] === "Due"
                            and $texts[$key + 1]['text'] === "Immediately"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (
                        $txt['text'] == "Due"
                        or $txt['text'] == "Immediately"
                    ) {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Bill"
                        and $textAnnotations[$key + 1]->description() === "Period"
                    ) {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 180;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if (
                            $txt['text'] == "Bill"
                            and $textsBillDate[$key + 1]['text'] == "Period"
                        ) {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['text'] == "Bill"
                        or $txt['text'] == "Period"
                    ) {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $billDate = substr($billDate, 0, strpos($billDate, "-"));

                $isGlobe = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "globe") {
                        $isGlobe = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isGlobe) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            } else if ($billType == "smart") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Remaining"
                        and $textAnnotations[$key + 1]->description() === "Balance"
                        and $textAnnotations[$key + 2]->description() === "from"
                        and $textAnnotations[$key + 3]->description() === "Previous"
                        and $textAnnotations[$key + 4]->description() === "Bill"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 520;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;
                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] === "Remaining"
                            and $texts[$key + 1]['text'] === "Balance"
                            and $texts[$key + 2]['text'] === "from"
                            and $texts[$key + 3]['text'] === "Previous"
                            and $texts[$key + 4]['text'] === "Bill"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (
                        $txt['text'] == "Remaining"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill"
                    ) {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Statement"
                        and $textAnnotations[$key + 1]->description() === "Date:"
                    ) {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 220;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if (
                            $txt['text'] == "Statement"
                            and $textsBillDate[$key + 1]['text'] == "Date:"
                        ) {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date:") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $isSmart = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "smart") {
                        $isSmart = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isSmart) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            } else if ($billType == "sun") {
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Total"
                        and $textAnnotations[$key + 1]->description() === "Balance"
                        and $textAnnotations[$key + 2]->description() === "from"
                        and $textAnnotations[$key + 3]->description() === "Previous"
                        and $textAnnotations[$key + 4]->description() === "Bill"
                    ) {
                        $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 420;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if (
                            $txt['text'] === "Total"
                            and $texts[$key + 1]['text'] === "Balance"
                            and $texts[$key + 2]['text'] === "from"
                            and $texts[$key + 3]['text'] === "Previous"
                            and $texts[$key + 4]['text'] === "Bill"
                        ) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (
                        $txt['text'] == "Total"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill"
                    ) {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;
                    $txt['text'] = preg_replace("/[^0-9.,-]/", "", $txt['text']);

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text) {
                    if (
                        $text->info()['description'] === "Statement"
                        and ($textAnnotations[$key + 1]->description() === "Date"
                            or $textAnnotations[$key + 1]->description() === "Date:")
                    ) {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"] - 10;
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 35;
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"] + 20;
                        $isBillDate = STS_NG;
                    } else {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if (
                            $txt['text'] == "Statement"
                            and ($textsBillDate[$key + 1]['text'] == "Date"
                                or $textsBillDate[$key + 1]['text'] == "Date:")
                        ) {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if (
                        $txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']
                    ) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date" or $txt['text'] == "Date:") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate . $txt['text'] . ' ';
                    } else {
                        $billDate = $billDate . $txt['text'];
                    }
                }

                $isSun = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "sun") {
                        $isSun = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isSun) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = [
                        "billType" => $billType,
                        "isOverDue" => $isOverDue,
                        "billDate" => $billDate
                    ];
                }
            }
            // $dateNow = date('m/d/Y');

            // if (strtotime($dueDate) < strtotime($dateNow)) {
            //     $status = 1;
            // } else {
            //     $status = 0;
            // }

            // $ubillCheck = DB::table('utility_bills')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('utility_bill_file', $imageName)->first();

            // if ($ubillCheck) {
            //     DB::table('utility_bills')->where('id', $ubillCheck->id)->update(["status" => $status]);
            // } else {
            //     //create entry on database
            //     $data = [
            //         'entity_id' => $entity->entityid,
            //         'status' => $status,
            //         'utility_bill_file' => $imageName,
            //         'is_deleted' => 0,
            //         'created_at' => date('Y-m-d H:i:s'),
            //         'updated_at' => date('Y-m-d H:i:s'),
            //     ];

            //     DB::table('utility_bills')->insertGetId($data);
            // }
        }
        $isOverDueFinal = 1;
        foreach ($bills as $key => $bill) {
            if ($bill['isOverDue'] == 1) {
                $isOverDueFinal = 2;
                break;
            }
        }

        DB::table('tblentity')->where('entityid', $entity->entityid)->update(['utility_bill' => $isOverDueFinal]);

        return;
    }

    public function calculateAverageBalance($entity)
    {
        $bankStatements = DB::table('tbldocument')->where("is_deleted", 0)->where("entity_id", $entity->entityid)->where("document_type", 0)->where("document_group", 51)->get();
        $destinationPath = public_path('documents/');
        $dateBalance = [];
        $datesCount = 0;
        $balanceCount = 0;

        if (count($bankStatements)) {
            foreach ($bankStatements as $bankStatement) {
                $image = $destinationPath . $bankStatement->document_rename;
                $enhancedImage = $destinationPath . $entity->entityid . '/' . pathinfo($image, PATHINFO_FILENAME) . "(enhanced)." . pathinfo($image, PATHINFO_EXTENSION);
                $imageName = pathinfo($image, PATHINFO_FILENAME) . "." . pathinfo($image, PATHINFO_EXTENSION);
                copy($image, $enhancedImage);

                // $imagick = new Imagick($enhancedImage);
                // $imagick->sharpenimage(20, 10);
                // $imagick->getImageBlob();

                // file_put_contents($enhancedImage, $imagick);

                $cloud = new ServiceBuilder([
                    'keyFilePath' => public_path('visionAPI.json'),
                    'projectId' => 'sixth-edition-253508'
                ]);

                $vision = $cloud->vision();

                $output = imagecreatefromjpeg($image);
                $image = $vision->image(file_get_contents($enhancedImage), ['text']);
                $textAnnotations = $vision->annotate($image)->text();

                // Remove first row (full text)
                array_shift($textAnnotations);

                /* --------------------------
        ----get Dates Column start--
        ---------------------------*/
                $dates = [];
                foreach ($textAnnotations as $key => $text) {
                    if ($text->info()['description'] === "DATE") {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"] - 30;
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 25;
                    } else {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                    }
                    $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                    $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $dates[] = $value;
                }
                $isDateColumn = STS_NG;
                $dateKey = 0;
                foreach ($dates as $key => $date) {
                    $isRotatedDiff = abs($date['x2'] - $date['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;
                    if ($isDateColumn == STS_NG) {
                        if ($date['text'] == "DATE") {
                            $isDateColumn = STS_OK;
                            $dateKey = $key;
                        } else {
                            unset($dates[$key]);
                            continue;
                        }
                    }
                    if (($date['x1'] < $dates[$dateKey]['x1'] || $date['x2'] > $dates[$dateKey]['x2'])) {
                        unset($dates[$key]);
                        continue;
                    } else if ($date['y1'] > $dates[$dateKey]['y1'] && $date['x2'] > $dates[$dateKey]['x2']) {
                        unset($dates[$key]);
                        continue;
                    } else if ($isRotated) {
                        unset($dates[$key]);
                        continue;
                    } else if (strstr($date['text'], '/')) {
                        $formatDate = str_replace("/", "", $date['text']);
                        if (!is_numeric($formatDate)) {
                            unset($dates[$key]);
                        }
                        continue;
                    }

                    if (!is_numeric($date['text'])) {
                        if ($date['text'] == "DATE") {
                            continue;
                        }
                        unset($dates[$key]);
                    }
                }
                array_shift($dates);
                $merged = STS_NG;
                $diff = 0;
                foreach ($dates as $key => $date) {
                    if ($key != array_key_last($dates) && $merged == STS_NG) {
                        $diff = abs($dates[$key + 1]['y1'] - $dates[$key]['y1']);
                        if ($diff <= 5) {
                            $month = preg_replace('/([^0-9])/i', '', $dates[$key]['text']);
                            $day = preg_replace('/([^0-9])/i', '', $dates[$key + 1]['text']);
                            $value = $dates[$key]['text'] . $dates[$key + 1]['text'];
                            $merged = STS_OK;
                        } else {
                            $fullDate = str_split(preg_replace('/([^0-9])/i', '', $dates[$key]['text']), 2);
                            $month = $fullDate[0];
                            $day = $fullDate[1];
                            $value = $dates[$key]['text'];
                        }
                        $dateBalance[$datesCount]['date'] = $value;
                        $dateBalance[$datesCount]['month'] = $month;
                        $dateBalance[$datesCount]['day'] = $day;
                        $datesCount++;
                    } else {
                        $merged = STS_NG;
                    }
                }
                /* --------------------------
        ----get Dates Column end-----
        ---------------------------*/

                //RECTANGLE TEST
                // $isBalanceColumn = STS_NG;
                // foreach($textAnnotations as $key => $text) {
                //     if($text->info()['description'] === "BALANCE") {
                //         if($textAnnotations[$key+1]->description() != "LAST"){
                //             $isBalanceColumn = STS_OK;
                //         }
                //     }
                //     if($isBalanceColumn == STS_OK){
                //         $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 95;
                //         $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 180;
                //         $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                //         $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                //         break;
                //     }
                // }

                // imagerectangle($output, $x1, $y1, $x2, $y2, 0x00ff00);
                // header('Content-Type: image/jpeg');

                // imagejpeg($output);
                // imagedestroy($output);
                //RECTANGLE TEST


                /* --------------------------
        ---get banlance Column start--
        ---------------------------*/
                $isBalanceColumn = STS_NG;
                $isBalanceLastStatement = STS_NG;
                $balances = [];
                foreach ($textAnnotations as $key => $text) {
                    if ($text->info()['description'] === "BALANCE" && $textAnnotations[$key + 1]->description() == "LAST") {
                        $isBalanceLastStatement = STS_OK;
                    }
                    if ($text->info()['description'] === "BALANCE") {
                        if ($textAnnotations[$key + 1]->description() != "LAST" || $textAnnotations[$key + 1]->description() != "THIS") {
                            $isBalanceColumn = STS_OK;
                        }
                    }
                    if ($isBalanceColumn == STS_OK) {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"] - 95;
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"] + 150;
                        $isBalanceColumn = STS_NG;
                    } else {
                        $x1 = (int) $text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int) $text->info()["boundingPoly"]["vertices"][1]["x"];
                    }
                    $y1 = (int) $text->info()["boundingPoly"]["vertices"][0]["y"];
                    $y2 = (int) $text->info()["boundingPoly"]["vertices"][2]["y"];
                    $value = [
                        "x1" => $x1,
                        "x2" => $x2,
                        "y1" => $y1,
                        "y2" => $y2,
                        "text" => $text->description()
                    ];
                    $balances[] = $value;
                }
                $isBalanceColumn = STS_NG;
                $balanceKey = 0;

                foreach ($balances as $key => $balance) {
                    $isRotatedDiff = abs($balance['x2'] - $balance['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;
                    if ($isBalanceColumn == STS_NG) {
                        if ($balance['text'] == "BALANCE") {
                            $isBalanceColumn = STS_OK;
                            $balanceKey = $key;
                        } else {
                            unset($balances[$key]);
                            continue;
                        }
                    }
                    if (($balance['x1'] < $balances[$balanceKey]['x1'] || $balance['x2'] > $balances[$balanceKey]['x2'])) {
                        unset($balances[$key]);
                        continue;
                    } else if ($balance['y1'] > $balances[$balanceKey]['y1'] && $balance['x2'] > $balances[$balanceKey]['x2']) {
                        unset($balances[$key]);
                        continue;
                    } else if ($isRotated) {
                        unset($balances[$key]);
                        continue;
                    } else if (strstr($balance['text'], ',') || strstr($balance['text'], '.')) {
                        $formatBalance = str_replace(array(".", ","), "", $balance['text']);
                        if (!is_numeric($formatBalance)) {
                            unset($balances[$key]);
                        }
                        continue;
                    }

                    if (!is_numeric($balance['text']) || is_numeric($balance['text'])) {
                        if ($balance['text'] == "BALANCE") {
                            continue;
                        }
                        unset($balances[$key]);
                    }
                }

                array_shift($balances);
                $merged = STS_NG;
                foreach ($balances as $key => $balance) {
                    if ($key != array_key_last($balances) && $merged == STS_NG) {
                        $diff = abs($balances[$key + 1]['y1'] - $balances[$key]['y1']);
                        if ($diff <= 5) {
                            $value = $balances[$key]['text'] . $balances[$key + 1]['text'];
                            $merged = STS_OK;
                        } else {
                            $value = $balances[$key]['text'];
                        }

                        if ($isBalanceLastStatement == STS_OK) {
                            $isBalanceLastStatement = STS_NG;
                        } else {
                            $dateBalance[$balanceCount]['balance'] = $this->convertAmount($value);
                            $balanceCount++;
                        }
                    } else {
                        $dateBalance[$balanceCount]['balance'] = $this->convertAmount($balances[$key]['text']);
                        $merged = STS_NG;
                        $balanceCount++;
                    }
                }
                /* --------------------------
        ---get banlance Column end---
        ---------------------------*/
            }
            // remove if date is not detected correctly
            foreach ($dateBalance as $key => $balance) {
                if ($balance['month'] > 12 || $balance['day'] > 31) {
                    unset($dateBalance[$key]);
                }
            }

            $dateBalance = array_values($dateBalance);

            $dayCount = 0;
            $totalBalance = 0;
            foreach ($dateBalance as $key => $balance) {
                $date = preg_replace('/([^0-9])/i', '', $balance['date']);
                if ($key != array_key_last($dateBalance)) {
                    $nextDate = preg_replace('/([^0-9])/i', '', $dateBalance[$key + 1]['date']);
                } else {
                    $totalBalance += $balance['balance'];
                    $dayCount++;
                }
                if ($date != $nextDate) {
                    $totalBalance += $balance['balance'];
                    $dayCount++;
                }
            }

            $averageBalance = round(($totalBalance / $dayCount), 2);

            DB::table('tblentity')->where('entityid', $entity->entityid)->update(['average_daily_balance' => $averageBalance]);
        }
        return;
    }

    public function convertAmount($money)
    {
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousandSeparator);
    }

    public static function cmapSearch($company_name)
    {
        //check if company data from cmap search is already in the database
        $table_data = self::findInDatabase($company_name);

        if ($table_data) {
            return;
        }

        // stop function if company name is from jenkins automation or testing
        if (strpos($company_name, 'PRDTST') !== false or strpos($company_name, '[]') !== false) {
            return;
        }

        /*  Stop function if not Production Environment  */
        if (env("APP_ENV") != "Production") {
            return;
        }

        /*  For Production Environment  */
        //if company name has no record in the database, scrape cmap search website 

        //table ids of needed tables
        $tables_ids = [
            "court_cases" => "dnn_ctr446_all_gv_crtcse",
            "accounts_referred_to_lawyers" => "dnn_ctr446_all_gv_artl",
            "returned_cheques" => "dnn_ctr446_all_gv_retchk",
            "telecom_data" => "dnn_ctr446_all_gv_telco"
        ];

        $tables = array();

        //login to cmap
        $login = self::login("http://search.cmaphil.com/login.aspx?ReturnUrl=%2fiCMAPMo.aspx", "ScriptManager=dnn%24ctr%24dnn%24ctr%24Login_UPPanel%7Cdnn%24ctr%24Login%24Login_DNN%24cmdLogin&StylesheetManager_TSSM=%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen-US%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A45085116%3A27c5704c&ScriptManager_TSM=%3B%3BSystem.Web.Extensions%2C%20Version%3D4.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D31bf3856ad364e35%3Aen%3A92dc34f5-462f-43bd-99ec-66234f705cd1%3Aea597d4b%3Ab25378d2%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A16e4e7cd%3Af7645509%3Aed16cbdc&__EVENTTARGET=dnn%24ctr%24Login%24Login_DNN%24cmdLogin&__EVENTARGUMENT=&__VIEWSTATE=OjothFdRgLkhJb0moNNEvFnklid4X%2F7pho9KSuz3UAvju5LlJZ90czI886yEOosr6HTwAlJSg5f6XMr2suqPRt6wl8V5WrB%2FNUZO8eI1osmCIVkNJArAv24%2BY0jYy4v5fgSXTjVxZkS3pyGGZYOWQah0KWbarHa2ekUY%2FJsMY00GDPSVUTHtXQiZibz1IkAyQ%2BmvuoaSdmxOMYm8OESWL3SjxAuW6418TQiW8fWnKK%2F3CnbOtH74RQhmiGX9pIW0QQyUi1s6S7ayDDdDTfsQ56PhnKv82Pr%2FGyqUjrxUgg1ItNgzMxKHr1WLpop7rHXBIUukxUdIQ0vWnJk3NIhMtQS7GkxUBHKGDAr2UmaCzKcka4x%2BFN0Uk8AK00WNnZWJZyxGPjzb6BDIlaBtJGJT26FQ6wN12ZV%2F8t1ahh%2FO6Aic%2FMKrQEKu3OneTMhitcBwdlJrqSFK1NGAvhc1arXYiXFPvwomD090iVIyP2wu8Pl%2BfQvCPo4LTcAjRFZWRDQoNlSwHulwffQZcGzHltVLyI22DDCpnkejjyw3oid7Gx6hSZbryQ8vjoMaaVq0DR3vR0Sv1ed8TaQfXh%2FPms13Zw4Gup%2BKUtAiYPxmeX5b%2Bi06YWT9cmuKXgSI%2Bs3r4Hkz0NvocClIEY%2Ba70BylzzPQs%2B5aqkTJee1f5xks%2F09rr3aNxoHBXL0caY03APZ5MOQzk8v%2FhElA6I9YbRX2kIMJfV8xU6E2FyqUuZ6PeMPp%2FSqYXgQaCJ6ArzSewDgv6JO4bcyl0KP5iEffbfksYgjsaadwt6Ql%2FpeaVoIJjgHTfHr4%2FESgKjdsUzXlza1g8E7SbZ4bqwQXemFhAKYM%2BzL%2B%2Buz21IznnjWZe9%2Fd21J8NpJNFoZ3W324o2Cs4oxogsI8rTaH42U3zMAEn8t2zFgc9ECGPoxvcbHY8vY2vstk1ZaTlQobn1AFZvgAOJuMioWWBG0DJXbnDQzEDDiDKptAklj%2BOIlAk02KCOVBv3NvfKE5GqrceXxvE%2BErRnsWWjViA3ECfEcOrphwONa%2FxYuY71WfLeDYpImYorsjnyl0arx2Lh%2F4IdpVMXtDIELQeqRD3WuQOG8HvqJTxwRIclzNomKbdyvcC9%2BwxObx%2BdWxWZYY7U5L%2Bsw1vWiquZHbhMMJ%2Fogf1QXx6KbDlxi8kjp%2BbK1X64isu%2FI4Dv2yn6Kzy3wrJojKyCSDouNhA94L78k%2FJJELHbXlhtvW8riThRO7Yua54zOdMI%2FwvIVnm1zXhNRMtmzOxtPYvHJ9Ln7i63OVnfYxasfpjwW1FQZcy6k8JfatKuLBjJZPTfWdQxb0z7nVlpLmSDVVAK8OPCxrIqc0qga%2FEMGpIZ63F6Cbf4AMC5%2BNjGeqWxg3MBWsn3z24A1NOEDwxKP6%2BOVUelTWV3KYmUIZgIjA8Sp4KAx782mMxPzY9q%2FSzFIGZC9Qiun4W0x6ElhCJmYnMkFaBVzlsTznuKiWiLW8QFCjfljP8PmRgHMJQoSCF6z5UNl8IOAiR4Z4U73VdfPagYFCMArT0z%2Ba6o%2FtKsWxSCpF0Ylq%2FlSPHHPxcj2oi42Tq8qRnu7FOrUQS10KJkkKp%2F9OAnaDrNkOsuLt%2BLyZftY4h0s%2FWiVCEBfpvUD%2Fk2NBXUvB%2FbXLpKSy6bCjfh%2BwSLSI3%2BGTvp0XqZABDwqVpHw%2FRi2v7O3g7Kqqe9bMgZE9M9dkaeo5%2BiB8KlhO5NnU8LAikabmZFbuQ4kK4QlSxg%2Bf40RV5UOZK0Xhsqvy2N49Xt1a2SBViF2v8w0LoS8YxYx76KgN3ReqyzfR3LCURwBcmeytfkGGt2Dq7EjILjisj%2BKLgtISe2W8yYT8w835kRAfMuB1tynONd2H4pLpVzMNKD7f0AdyNm7Yr31kcmsx5dtWj7knhl4QyUc8%2FWP0xBSzEDWM7Mn0npGv2jiqcW8oeoHt3Y%2F8FRocEjRvMx7DE8VCqGDdlyZNTYdJLwPAFbYHsU8Z0Y7hqwxlnscnb1uCgzdYu1W9TYvygPau91%2Bv6tfc35ucvs8Ty4fK18kjIv46UqBS220wBBHcRWjVaXQiFd%2BJNDmWKHLzbh5L849bhgH1SYQVunGWRwmDMQXSe%2BWEJUKM8fCQSUld%2FTG%2Fhdh9IzxIX0v8T9wCMafov4Z8BwzCR48bnZlE%2FyG4mZguHIDCK7OBYg7cGcf%2FN%2B0rJUR%2FddB3FcjIxmpURfdbi3Va%2FSbQgouHkYRn6uAftKdhFMhzD4bR%2FcjVmxHxB%2FJUbn5Oz69ek45rfhvXlCZQU2i2TWsfWbtP%2FAMLmJccLxt%2B6Z0evxVLam1bpz%2BZfq6GneTm%2F1rx6yxTXjDNMldsmmEY7%2BL6f1KZICAL%2BTQKz2Y1n0vYmIK6BjS0gKu3WurpQI8YPEvQwoOmCmIQjcLl%2BUY%2BoqEHyR4qZNI0bSRMmlGyoN9XJB%2B5KEhYQs1VRunHOtWIOCfW4UFmh9QO7OsHHK0oW777OmFZJFGhxVz3kW2f%2BLAuK6o6Ii2YGLINGS1TdVsstPO5lQCxq0MkbgcwZ7o2Yw0B%2BD9nggCmEkt%2B8VIhKBrzayn7cmzeQl%2FH7dCjZ%2FjvZk4EnwbNOfU5n4DVOqiSiO%2BHCysaN3GiAezEP91DxXZ7TFZ8gt1iwUcsO%2By8HZmc1Jfbi%2FfpapcqduVXv1S1FukHcS4C1Qsr5nDlIQv87smP16slzkPKvOotuChJV%2BY5OhYMPsRfHrko%2BYIl9%2F2k1d2f%2BPiqY8XykX2RIzshe%2FOcqkl%2FuqRO%2FZJ71deNiI4MYivSsvHpgjr7zzSuWqupc4kiBIyJw3cQqP0Gf6j4Xb81mBieZFWGijDHh0OPgH%2BaiWrUbwLyz5CsaTChFV6fT086NrZLM3khBCasabBcSW%2B6PB2%2F35sAL3Bg6doGNaQai16BIWKISWg08Gvvg5a91Sy%2BdbAoTsKNqIuqCwz5VTag1BDd%2FbfCDMsoK5XTL9BnncCii27Pb8n5zQoDnmgq9%2F7W0eJZovu5SGXfhCFEaVpx2byClEwVF7nGs2dNgl20oKf23Ti4LrFjZcL%2FHxbFwU%2BNIqLDuVZG4C7RiWn5LTl3UqkKNToQZKCnwzO8oaqqSS3LbVSq2S4y4BiqdMneBszk4NnZ6BuJyMLEkt6JpAfr9dGmM8a6CdDPFE7Dli80v4trQi4l6mqZLYIlzNtPbwXDaxCq6cdnVcWtBWru2QvMBJnwPHV%2F45RIS21Ix4Jga1nqsyf8O8vqcdYcqBgluRFpuxUyBh9TXhEgIWxdLtq6N1snQL3V8wAKxw3KpMwtvsBUa1tXI69N3o7apwt0ZkGcZRxh1iiXoRsIj3vyeNStUhxCCtjtqH0zObAn0USYicpyqlbTt6443wcKCJLooT9jze8%2BA%2F19oukiMjnAsDtSXnxlSyoy4dPgG6xnq92ULgFhk4D0tG5cqBy%2BZe0%2B1hh4GTnPWSQSzgNxq0HTYnz5hRHWqhYCxJ%2BKueT1h8ppb8M&__VIEWSTATEGENERATOR=CA0B0334&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=za37uuunlJRQmTzTNzkT0qFZJC%2Fehn3mIfY4bMWMAtp4APN7n7CCxz7o25rJRSwQ8EBXqDN2k332qomn75pkuueLJH2ljkJGgqFPvRp0qwgDVTymH0v2a7CWulDkxkHmqRYX93xFWQho8y%2Fajw0TTRRPvOBjEi9vdsk6gM5C6zfrIxSEX79%2FjyDKVIVcJNGNq6kQGYjmvXuODTQr&dnn%24ctr%24Login%24Login_DNN%24txtUsername=Jose%20Franciso&dnn%24ctr%24Login%24Login_DNN%24txtPassword=jf73et4&ScrollTop=&__dnnVariable=%7B%22__scdoff%22%3A%221%22%2C%22__dnn_pageload%22%3A%22__dnn_SetInitialFocus(%5Cu0027dnn_ctr_Login_Login_DNN_txtUsername%5Cu0027)%3B%22%7D&__ASYNCPOST=true&RadAJAXControlID=dnn_ctr_Login_UP");
        // search for the company
        $search = self::search("http://search.cmaphil.com/iCMAPMo.aspx", "ScriptManager=dnn%24ctr446%24ViewCMAPSEARCH%24UpdatePanel1%7Cdnn%24ctr446%24ViewCMAPSEARCH%24IBSearch&StylesheetManager_TSSM=%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen-US%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A45085116%3A27c5704c&ScriptManager_TSM=%3B%3BSystem.Web.Extensions%2C%20Version%3D4.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D31bf3856ad364e35%3Aen%3A92dc34f5-462f-43bd-99ec-66234f705cd1%3Aea597d4b%3Ab25378d2%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A16e4e7cd%3Af7645509%3Aed16cbdc&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=QE%2BJIZ4xMCniIiyWgcGTbfouNmiSIClfAQBEHINwYITAWfmLyNiI0l%2BsIgF%2FHxurjmjXxAWvDyLe9jmZ8Qg6d817LsUzsFqJQwr02SqWcwbjUMUuxp5uplbepMF6i31oRJSebrZ%2FP%2BmFZJ%2BmXGKhKHw8JsiPnCiv8mcdGyAtzY%2FOGXwoOrrqQT%2F08LmCorj0pHd0sLQxIbpDSFg68hwM8d7oBro9Nws7sLY3ZAaUeruqQS8v6B5%2FY2xKzdpUu3TMQ3PxO2TDYEAqZPKWbhacPTKewc5vtkYMH9FTDJkQR3wTHpUns6cEtFfzExeBQA5kc6P0aFwbqdWkS6JlrMViXPbbkjDV%2BbFV126YjKjStLSJ%2BaAoA0CEjY4Dcy3bCKN%2BxZRMCCdgVmLdufJgUkafVHAXo5HzpRZdof9DaEBtjkpe2pZj2%2BzjipXYsqLfg%2BosYX5Eq0Vig2jxB%2Fm3xEqivau91KFvOnG2MNR5DcAmQ7bqxouSNtQGrK3tLi8Muye05VRXA3Vih0k4%2FMNIqU%2F3wPh8TQD7bXf7dfAh3iQ2ExUexP2xx8X8QMUarKRZoSoag%2Fsa5RTjZZtOZAQkpzcVCqjbqGq44VW3hijjMqi7fuye6rRDwNTW%2FKbfbOE%2BW%2F8hyWFnzcux1v3O0Q0Rout%2FkYuJS%2FRkePA4K936OLV0cSnURvxPlis4Cy%2Ff59lNxjI4fX1rFQNkZZgctIIWLiiRxae7tB4x9eSN0H8w9lB7Yb7OrHUVDj6gB0%2BrqzX4dYu%2Bfvymuh75LZ%2FbPWxRR0xLk2e4k4UeO2QTF%2BJZLpd4QwEE0u0oJ70IpUOE1wk5gS7oMh0Caax1eZekHZpQQ4hYryUvDHIQukSQTP6rZysh17dFVCx9dmsEVNaGSojwWxtBvig9sy8wJ6BiNVoPE8tZHQgTg1hOHIxksJq3sEtVkXIjKOFaBErLSalm5g0t7%2BgSsSQqFHTcHlPgCgTJ6yF47O1RkO1feBdfQ%2FavODmzqbNqc9S2vM20%2FYYycw9Va5womY1r9YcMV3Ve7a72mGgI6xnR0tKq%2BYDROUvFYC7aMZ3w2lRpLoB%2BQR9j3n9By6NaMeTXl95vqZmCFH8eVdUS0trOgHfu6rmNltOUBgbovdK0js6DsHJd6yG2BF0CtuC0rVm0vWQWn881UApromuIusQa4BhC9BmJCZbIEnu%2FS1JkDwdOD9OfmiRdwGE2mS9U70KdmHesQsa64iQ%2FBNu3HOXzhvsEclR28J6RYykTUi3CEQZzBEkcXwl56vAWlhzey1cR6GxCESNHAMolPjLUw40lKwZG0I%2FQIL5x9JQfZ6SYN0Rs2%2FsePHEHdXXizkE17Pj0z387M2Bh1MeXA%2F1PqBN4SAYiOWQMhWRqSditCr3AFWjkdE%2Fqi3yQ6rOw9gg2LvJ61cb%2BGZRheFlDocyUv3JUqwjgNztzRLx8vc6eklm%2FuiuoMuK5HpKlSnIRbGbYF1510hoV19xRoqfC3SCHxhsfMbGwMy8JY2V7caD6VZ4fN%2FBNzQZTZTuYfU0HXPy7XHNJ5%2Fio9w5KQ742h5H1tDDHPxIvypFMSoU8vwn45Wyyu8aqKMaSCpJqtT3Mis7VU7cxZwNjM27Jl0UquaRctHskVuAdpJpV%2FOv99%2BYNWKqbMeCyWQAtrxjchOz30xe22%2BeAr0ex2f9wktBDEJUNoZ50Bdv3IUOZhZ6uOqm2WgtlxV9QiGLYBn72brkrnmPZwH1kh1Bq71k%2FhiasOXlk3StEKhqQRA8BSP8N6Jh2NX4iX7SfXj%2BswBr%2FsRpySgJi0oL24yKNdOfSgVVxA2C6BrqAWaqJcxva6T6IQIdcM3em85Fj0cJ%2F1b6blnQrl5lENiesdh7TnzwOW%2FXd5ucKiyyPkI5QThsI2gP5T8DxA%2FECQdI615zobM%2B2h9dOXXKVzZS3eyKs53fndFSFszDD%2BW8CxmHlv%2FHVkJbs0UYjiGVP1BYrBvU0lUdTyRgsvhtXgEMkydFx0cgP8G4tU9840LC4j1mCgX9rDzN4KCFu5fhMtDYtQ6p9%2Fc94BDbzVdduipet9DYIx%2F8KBTLYvYeQPsaghCCkRGtTya%2BV20Qc2gXDkR8zN87fUG0eBW8T%2FL3gVYHlskeazKkOawkoCyTRpDMvh5Ss%2B6YF6QT%2F2EptXNx4mRv18HnD2L1trQf7FwerSVfNUO9Zsh4U7mnlnPmIX%2FFpEfl5IV%2FzG5UspJqch6Zkj6aNfpMMu8ekGvFHbXB5YNmZouhK1f%2FwmEaTd6MrGwiTu7e2xGknTXCKmtdhIpOmPdaZhmLAs5XXhRj2Vz32aY8Dj9HJTfwO%2FPdNe%2FxvKfuei9tr%2B2oYYhbQOXLGT5D%2BkC3q4IFIM%2FcV55dP3%2FzKAFgk37%2BRpg%2BoIldRsDl3x5lR0mlMo3KV%2BOVIRuuMzqZCdz6ITdOR%2BFihUMDCdkculV2b9PfWgt8DMNxlnTrmb20xe56sDHfLzwD0cWjRodzIkMq34zPBOS5rG%2B5VVgWUJhdtC%2BbMdTiU8YjYiAgX22%2Fmp%2BiFWnH9q%2FEWETgtEyoH3lURE9%2F7zIEvsIuSbLeR8LXufT2vTTAE7wZ0nG%2FMzXwbkve3VTZF3YcUc5VSymOMwaoTYzwXEsMi18YQ6GUeRhwtZ2%2FiDBfmteVmIBOVtX23ezrHkCG43AIfIR3r1ZAi4p9hLdg9qDwumYGuCCGNgfIU4AosggzstKKNAkDbk7ZVmFnQI4TdVVSZMXBlJlUGJUSftQZKbLN8UcGHeN7W2CU0Gq3mG%2FTTZqrFLkamGhl7TQlNg%2BZVLXyg9jdDVncJ3gvvTfptXYmwK49qGyXS4hnVQDxss8qe9RM1kNnHZvMxZ3cGC8tn3k5%2F8I3ujOymvjpY65kVOQcRxehbmBU0Xfpwph0CO9HVhyFuL44hEArEf5H8DYLkGXBYxOIrywKZFmuZyOAJEtd6sHEEVtMr6Ot9eP8JZFtiXMNqA5Hc1kjGwXIKzK45ukD0yj2h%2FqxfFjlYYvIMYDQ5vxlTo8vmPClpIZG2P8ivzTTdiuSY7%2B50mzPQzF9Mk%2BIHL7MkUX1wtKa50YdixLM8NoW0OMNw8NINHht2xNBj4eItKtwex1o4KJHjJQCt3xQpzsTHlc%2B0tXB90ZUM4QKLIBJlEVIFq%2BiiE%2Fu2NoQztbRRlE%2FT5Gdl8tn3nuC%2FvHDHObhNybcwUG8nj6pp22OXLBRSZ6OM%2FiYg1DqVLxgkvXDsdVgd9TulYnDrSArdTtBYunIR23rArVeUj3WOtXeeSDNx4EZ9v08u3E5%2FFvsSqwhvd3fqTA02nSqCb0NIBsUPrR0wIksd344RjtVz3x4VVgcoFDSeHFn%2ByJ%2F1UUdjOB70tr57fCPdlKSkECQ5TZyvs%2BU36e5UJR%2Bc6ElQ5bt7jp0lXRBBQbS%2BGOFecGTLQJSfk9q6o3cvdaHKDhHjvk%2Bw9UEAtXyVFEKgCAc%2FvZFwODjDInSh3M8uJHeJM5pPeZ59GKFK8x3Hx2dNHWbaBdFQHeBWSiYmvNhCigspqT0TxsGM3GqqmNj2Z4nd8c2P8mHg%2FYwtRMFl96cM8rpWD6bjoZnO48ARxBVFAHNucs%2BhNXlvFG6Ai7TLquLHjjk%2Bq6n6GvGxUiHf%2FH045Cc0CAlbWSO3RcY9x%2Fig0WfQorotQGXXfcrhyk%2B8oLvRE%2F8ccyrnJjCVINf%2FeHYhgu9uaCDd%2BIjRdY1Eu0RPycMWrMEWrHBOMsjsjS9AElkx07ugy9A2VoLqg5i8Bi%2BqaJQIUdilk%2FEIH9PwJzq6L4fJ08V1PuDAgzzzflupOnbMe4ahnoKwHmWqsgASiG%2FY8VwpyV%2BsltpkreNWIx5En9BzCityb3ORB5%2Fe9owajQSGOxfv%2B92Iz2qptdJrrSa4f%2FNqKwTbUv8lcyXsaiv8XwVynuFflG7tJnMfUVg7CBIZiIzlRGWthyJRKgjqGEz4Tqu28D7%2FIEICxOX9zYb5EOcyvJ6fNVp0gvW6VuF3LLBBJcFo%2BBCejlzHL4bBx1LjFK%2FtvSvNqTIhMFDGrmsH%2F%2F972%2Fp%2FRks0zQKDgDeyKPZCW68tgvdk6%2FauNsOoC8KdLp94MXkFUvkATgijM%2B8hdH2BufSV7kTWsEpHXa9qMg%3D%3D&__VIEWSTATEGENERATOR=CA0B0334&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=sse%2BJXEPVLV899AHbEtv7MKI7oCa05RvfDA08M0flNWYL01QGTa9DOIfZ53pCgd8s8znqsgwdFF%2FVcCfqkgC%2BUTK8BiyybgOi2KKjlzckcmXlRSAYFhbg%2B2tvHGYVCU9RBFZfD%2F67Tpyd1cNKPvNtj4azL0%2BBiJ9OSzCoZPuma0olVhezLWkIWZcUP9lt8fk9qMs8H5zbyakAHEal90Cbqsp%2FuJ7ukIWsAfylpdjKDgShIdce%2B3ZUdQoNOHuRCSL9OWBL9jCi7GdYBMqY%2BbusMOyHlaeTdnR0ozYhhIg14oygeQqYcd1yQbOam5DHVjjKS4EawLdR5Ngb47kcEqfYGJrSWI%3D&dnn%24ctr446%24ViewCMAPSEARCH%24txtNameSearch={$company_name}&ScrollTop=&__dnnVariable=%7B%22__scdoff%22%3A%221%22%7D&dnn%24ctr446%24ViewCMAPSEARCH%24txNameQueue=&dnn%24ctr446%24ViewCMAPSEARCH%24hfRoleID='Registered%20Users'%2C'CREDITBPO%20TECH%20INC'%2C'FINANCING%20LENDING'&__ASYNCPOST=true&dnn%24ctr446%24ViewCMAPSEARCH%24IBSearch.x=66&dnn%24ctr446%24ViewCMAPSEARCH%24IBSearch.y=16&RadAJAXControlID=dnn_ctr446_ViewCMAPSEARCH_UP");
        // grab all reports from the search
        $response = self::grab_page("http://search.cmaphil.com/iCMAPMo/tabid/88/ctl/all/mid/446/Default.aspx");

        self::addToCmapSearchQueryTable($company_name);

        // Create DOM from URL
        $html = new simple_html_dom();
        $html->load($response);

        foreach ($tables_ids as $key => $table_id) {
            $rowData = array();

            //find table where table id is equal to needed table id
            $table = $html->find("table[id^={$table_id}]", 0);

            if ($table) {
                //get results
                foreach ($table->find('tr') as $row) {
                    // initialize array to store the cell data from each row
                    $td = array();

                    foreach ($row->find('td, th') as $cell) {
                        //remove white space from text
                        $stripped = trim(preg_replace('/\s+/', ' ', $cell->plaintext));
                        // push the cell's text to td data
                        $td[] = $stripped;
                    }
                    // push td data to row data
                    $rowData[] = $td;
                }

                $tables[$key] = $rowData;
            }
        }

        if ($tables) {
            $data_ids_report = array();

            foreach ($tables as $tablekey => $table) {
                //check if table headers for each table exist in the database
                $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tablekey)->first();

                // if not, create new table headers in the database
                if ($cmapTableHeader == null) {
                    //iterate through the first entry in array which is the table headers
                    foreach ($table[0] as $headerKey => $headerName) {
                        //if no data in table
                        if ($headerName == "NO RELATED COLLECTED DATA FROM 3RD PARTY") {
                            continue;
                        }

                        //create header entries
                        $header = new CmapTableHeaders;
                        $header->cmap_table_name = $tablekey;
                        $header->cmap_table_header = $headerName;
                        $header->header_order = $headerKey;
                        $header->save();
                    }
                }

                //get headers of table type
                $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tablekey)->orderBy('header_order')->pluck('cmap_table_header');

                //create cmap search data entry
                foreach ($table as $dataKey => $data) {
                    //skip first entry in array which is the table headers
                    if ($dataKey == 0) {
                        continue;
                    }

                    $last_data_id = CmapSearchData::all()->last();
                    $data_id = 1;

                    if ($last_data_id != null) {
                        $data_id = $last_data_id->data_id + 1;
                    }

                    //iterate through the data and save to database
                    foreach ($data as $entryKey => $entryName) {
                        //create new search data
                        $entry = new CmapSearchData();
                        $entry->data_table_name = $tablekey;
                        $entry->data_name = $entryName;
                        $entry->data_type = $cmapTableHeader[$entryKey];
                        $entry->data_id = $data_id;
                        $entry->data_order = $entryKey;
                        $entry->save();
                    }
                    array_push($data_ids_report, $data_id);
                }
            }
            CmapSearchQuery::where('company_name', $company_name)->update(['data_ids' => json_decode($data_ids_report)]);
        }

        return;
    }

    public static function findInDatabase($company_name)
    {
        $searchCmap = CmapSearchQuery::where('company_name', $company_name)->pluck('data_ids')->first();

        // search database if company is already in cmap search data
        if(empty($searchCmap) || !$searchCmap){
            $companyIds = CmapSearchData::where('data_order', 0)->where('data_name', $company_name)->pluck('data_id');
        }else{
            $companyIds = json_decode($searchCmap);
        }

        $table_data = array();

        // if yes, get data
        if ($companyIds) {
            foreach ($companyIds as $companyId) {
                $sameTableType = STS_NG;
                $companyRecords = CmapSearchData::where('data_id', $companyId)->orderBy('data_order')->pluck('data_name', 'data_type');
                $tableType = CmapSearchData::where('data_id', $companyId)->first()->data_table_name;
                $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tableType)->orderBy('header_order')->pluck('cmap_table_header');

                //check if table data is of the same table type
                // if yes, push to the same array
                if ($table_data != null) {
                    foreach ($table_data as $tbl_data_key => $tbl_data) {
                        if ($tbl_data[0] == $tableType) {
                            array_push($tbl_data[2], $companyRecords);
                            $table_data[$tbl_data_key] = $tbl_data;
                            $sameTableType = STS_OK;
                        }
                    }
                }

                // new table type push to table data array
                if ($sameTableType == STS_NG) {
                    $table_data[] = [$tableType, $cmapTableHeader, [$companyRecords]];
                }
            }
        }

        return $table_data;
    }

    public static function cmapSearchOfficers($company_name, $pse_company_id)
    {
        //select all officers of company
        $officers = PseCompanyOfficer::where('company_id', $pse_company_id)->groupBy('name')->get(); //limit for testing only

        //iterate through officers of company
        foreach ($officers as $officer) {
            //check if officer data from cmap search is already in the database
            $table_data = self::findInDatabaseOfficer($officer->name);

            // continue if yes4
            if ($table_data) {
                continue;
            }

            // stop function if company name is from jenkins automation or testing
            if (strpos($company_name, 'PRDTST') !== false or strpos($company_name, '[]') !== false) {
                continue;
            }

            /*  Stop function if not Production Environment  */
            if (env("APP_ENV") != "Production") {
                continue;
            }

            /*  For Production Environment  */
            //if company name has no record in the database, scrape cmap search website 
            //table ids of needed tables
            $tables_ids = [
                "court_cases" => "dnn_ctr446_all_gv_crtcse",
                "accounts_referred_to_lawyers" => "dnn_ctr446_all_gv_artl",
                "returned_cheques" => "dnn_ctr446_all_gv_retchk",
                "telecom_data" => "dnn_ctr446_all_gv_telco"
            ];

            $tables = array();
        
            //login to cmap
            $login = self::login("http://search.cmaphil.com/login.aspx?ReturnUrl=%2fiCMAPMo.aspx", "ScriptManager=dnn%24ctr%24dnn%24ctr%24Login_UPPanel%7Cdnn%24ctr%24Login%24Login_DNN%24cmdLogin&StylesheetManager_TSSM=%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen-US%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A45085116%3A27c5704c&ScriptManager_TSM=%3B%3BSystem.Web.Extensions%2C%20Version%3D4.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D31bf3856ad364e35%3Aen%3A92dc34f5-462f-43bd-99ec-66234f705cd1%3Aea597d4b%3Ab25378d2%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A16e4e7cd%3Af7645509%3Aed16cbdc&__EVENTTARGET=dnn%24ctr%24Login%24Login_DNN%24cmdLogin&__EVENTARGUMENT=&__VIEWSTATE=OjothFdRgLkhJb0moNNEvFnklid4X%2F7pho9KSuz3UAvju5LlJZ90czI886yEOosr6HTwAlJSg5f6XMr2suqPRt6wl8V5WrB%2FNUZO8eI1osmCIVkNJArAv24%2BY0jYy4v5fgSXTjVxZkS3pyGGZYOWQah0KWbarHa2ekUY%2FJsMY00GDPSVUTHtXQiZibz1IkAyQ%2BmvuoaSdmxOMYm8OESWL3SjxAuW6418TQiW8fWnKK%2F3CnbOtH74RQhmiGX9pIW0QQyUi1s6S7ayDDdDTfsQ56PhnKv82Pr%2FGyqUjrxUgg1ItNgzMxKHr1WLpop7rHXBIUukxUdIQ0vWnJk3NIhMtQS7GkxUBHKGDAr2UmaCzKcka4x%2BFN0Uk8AK00WNnZWJZyxGPjzb6BDIlaBtJGJT26FQ6wN12ZV%2F8t1ahh%2FO6Aic%2FMKrQEKu3OneTMhitcBwdlJrqSFK1NGAvhc1arXYiXFPvwomD090iVIyP2wu8Pl%2BfQvCPo4LTcAjRFZWRDQoNlSwHulwffQZcGzHltVLyI22DDCpnkejjyw3oid7Gx6hSZbryQ8vjoMaaVq0DR3vR0Sv1ed8TaQfXh%2FPms13Zw4Gup%2BKUtAiYPxmeX5b%2Bi06YWT9cmuKXgSI%2Bs3r4Hkz0NvocClIEY%2Ba70BylzzPQs%2B5aqkTJee1f5xks%2F09rr3aNxoHBXL0caY03APZ5MOQzk8v%2FhElA6I9YbRX2kIMJfV8xU6E2FyqUuZ6PeMPp%2FSqYXgQaCJ6ArzSewDgv6JO4bcyl0KP5iEffbfksYgjsaadwt6Ql%2FpeaVoIJjgHTfHr4%2FESgKjdsUzXlza1g8E7SbZ4bqwQXemFhAKYM%2BzL%2B%2Buz21IznnjWZe9%2Fd21J8NpJNFoZ3W324o2Cs4oxogsI8rTaH42U3zMAEn8t2zFgc9ECGPoxvcbHY8vY2vstk1ZaTlQobn1AFZvgAOJuMioWWBG0DJXbnDQzEDDiDKptAklj%2BOIlAk02KCOVBv3NvfKE5GqrceXxvE%2BErRnsWWjViA3ECfEcOrphwONa%2FxYuY71WfLeDYpImYorsjnyl0arx2Lh%2F4IdpVMXtDIELQeqRD3WuQOG8HvqJTxwRIclzNomKbdyvcC9%2BwxObx%2BdWxWZYY7U5L%2Bsw1vWiquZHbhMMJ%2Fogf1QXx6KbDlxi8kjp%2BbK1X64isu%2FI4Dv2yn6Kzy3wrJojKyCSDouNhA94L78k%2FJJELHbXlhtvW8riThRO7Yua54zOdMI%2FwvIVnm1zXhNRMtmzOxtPYvHJ9Ln7i63OVnfYxasfpjwW1FQZcy6k8JfatKuLBjJZPTfWdQxb0z7nVlpLmSDVVAK8OPCxrIqc0qga%2FEMGpIZ63F6Cbf4AMC5%2BNjGeqWxg3MBWsn3z24A1NOEDwxKP6%2BOVUelTWV3KYmUIZgIjA8Sp4KAx782mMxPzY9q%2FSzFIGZC9Qiun4W0x6ElhCJmYnMkFaBVzlsTznuKiWiLW8QFCjfljP8PmRgHMJQoSCF6z5UNl8IOAiR4Z4U73VdfPagYFCMArT0z%2Ba6o%2FtKsWxSCpF0Ylq%2FlSPHHPxcj2oi42Tq8qRnu7FOrUQS10KJkkKp%2F9OAnaDrNkOsuLt%2BLyZftY4h0s%2FWiVCEBfpvUD%2Fk2NBXUvB%2FbXLpKSy6bCjfh%2BwSLSI3%2BGTvp0XqZABDwqVpHw%2FRi2v7O3g7Kqqe9bMgZE9M9dkaeo5%2BiB8KlhO5NnU8LAikabmZFbuQ4kK4QlSxg%2Bf40RV5UOZK0Xhsqvy2N49Xt1a2SBViF2v8w0LoS8YxYx76KgN3ReqyzfR3LCURwBcmeytfkGGt2Dq7EjILjisj%2BKLgtISe2W8yYT8w835kRAfMuB1tynONd2H4pLpVzMNKD7f0AdyNm7Yr31kcmsx5dtWj7knhl4QyUc8%2FWP0xBSzEDWM7Mn0npGv2jiqcW8oeoHt3Y%2F8FRocEjRvMx7DE8VCqGDdlyZNTYdJLwPAFbYHsU8Z0Y7hqwxlnscnb1uCgzdYu1W9TYvygPau91%2Bv6tfc35ucvs8Ty4fK18kjIv46UqBS220wBBHcRWjVaXQiFd%2BJNDmWKHLzbh5L849bhgH1SYQVunGWRwmDMQXSe%2BWEJUKM8fCQSUld%2FTG%2Fhdh9IzxIX0v8T9wCMafov4Z8BwzCR48bnZlE%2FyG4mZguHIDCK7OBYg7cGcf%2FN%2B0rJUR%2FddB3FcjIxmpURfdbi3Va%2FSbQgouHkYRn6uAftKdhFMhzD4bR%2FcjVmxHxB%2FJUbn5Oz69ek45rfhvXlCZQU2i2TWsfWbtP%2FAMLmJccLxt%2B6Z0evxVLam1bpz%2BZfq6GneTm%2F1rx6yxTXjDNMldsmmEY7%2BL6f1KZICAL%2BTQKz2Y1n0vYmIK6BjS0gKu3WurpQI8YPEvQwoOmCmIQjcLl%2BUY%2BoqEHyR4qZNI0bSRMmlGyoN9XJB%2B5KEhYQs1VRunHOtWIOCfW4UFmh9QO7OsHHK0oW777OmFZJFGhxVz3kW2f%2BLAuK6o6Ii2YGLINGS1TdVsstPO5lQCxq0MkbgcwZ7o2Yw0B%2BD9nggCmEkt%2B8VIhKBrzayn7cmzeQl%2FH7dCjZ%2FjvZk4EnwbNOfU5n4DVOqiSiO%2BHCysaN3GiAezEP91DxXZ7TFZ8gt1iwUcsO%2By8HZmc1Jfbi%2FfpapcqduVXv1S1FukHcS4C1Qsr5nDlIQv87smP16slzkPKvOotuChJV%2BY5OhYMPsRfHrko%2BYIl9%2F2k1d2f%2BPiqY8XykX2RIzshe%2FOcqkl%2FuqRO%2FZJ71deNiI4MYivSsvHpgjr7zzSuWqupc4kiBIyJw3cQqP0Gf6j4Xb81mBieZFWGijDHh0OPgH%2BaiWrUbwLyz5CsaTChFV6fT086NrZLM3khBCasabBcSW%2B6PB2%2F35sAL3Bg6doGNaQai16BIWKISWg08Gvvg5a91Sy%2BdbAoTsKNqIuqCwz5VTag1BDd%2FbfCDMsoK5XTL9BnncCii27Pb8n5zQoDnmgq9%2F7W0eJZovu5SGXfhCFEaVpx2byClEwVF7nGs2dNgl20oKf23Ti4LrFjZcL%2FHxbFwU%2BNIqLDuVZG4C7RiWn5LTl3UqkKNToQZKCnwzO8oaqqSS3LbVSq2S4y4BiqdMneBszk4NnZ6BuJyMLEkt6JpAfr9dGmM8a6CdDPFE7Dli80v4trQi4l6mqZLYIlzNtPbwXDaxCq6cdnVcWtBWru2QvMBJnwPHV%2F45RIS21Ix4Jga1nqsyf8O8vqcdYcqBgluRFpuxUyBh9TXhEgIWxdLtq6N1snQL3V8wAKxw3KpMwtvsBUa1tXI69N3o7apwt0ZkGcZRxh1iiXoRsIj3vyeNStUhxCCtjtqH0zObAn0USYicpyqlbTt6443wcKCJLooT9jze8%2BA%2F19oukiMjnAsDtSXnxlSyoy4dPgG6xnq92ULgFhk4D0tG5cqBy%2BZe0%2B1hh4GTnPWSQSzgNxq0HTYnz5hRHWqhYCxJ%2BKueT1h8ppb8M&__VIEWSTATEGENERATOR=CA0B0334&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=za37uuunlJRQmTzTNzkT0qFZJC%2Fehn3mIfY4bMWMAtp4APN7n7CCxz7o25rJRSwQ8EBXqDN2k332qomn75pkuueLJH2ljkJGgqFPvRp0qwgDVTymH0v2a7CWulDkxkHmqRYX93xFWQho8y%2Fajw0TTRRPvOBjEi9vdsk6gM5C6zfrIxSEX79%2FjyDKVIVcJNGNq6kQGYjmvXuODTQr&dnn%24ctr%24Login%24Login_DNN%24txtUsername=Jose%20Franciso&dnn%24ctr%24Login%24Login_DNN%24txtPassword=jf73et4&ScrollTop=&__dnnVariable=%7B%22__scdoff%22%3A%221%22%2C%22__dnn_pageload%22%3A%22__dnn_SetInitialFocus(%5Cu0027dnn_ctr_Login_Login_DNN_txtUsername%5Cu0027)%3B%22%7D&__ASYNCPOST=true&RadAJAXControlID=dnn_ctr_Login_UP");
            // search for the company
            $search = self::search("http://search.cmaphil.com/iCMAPMo.aspx", "ScriptManager=dnn%24ctr446%24ViewCMAPSEARCH%24UpdatePanel1%7Cdnn%24ctr446%24ViewCMAPSEARCH%24IBSearch&StylesheetManager_TSSM=%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen-US%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A45085116%3A27c5704c&ScriptManager_TSM=%3B%3BSystem.Web.Extensions%2C%20Version%3D4.0.0.0%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D31bf3856ad364e35%3Aen%3A92dc34f5-462f-43bd-99ec-66234f705cd1%3Aea597d4b%3Ab25378d2%3BTelerik.Web.UI%2C%20Version%3D2012.2.724.35%2C%20Culture%3Dneutral%2C%20PublicKeyToken%3D121fae78165ba3d4%3Aen%3A3fe22950-1961-4f26-b9d4-df0df7356bf6%3A16e4e7cd%3Af7645509%3Aed16cbdc&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=QE%2BJIZ4xMCniIiyWgcGTbfouNmiSIClfAQBEHINwYITAWfmLyNiI0l%2BsIgF%2FHxurjmjXxAWvDyLe9jmZ8Qg6d817LsUzsFqJQwr02SqWcwbjUMUuxp5uplbepMF6i31oRJSebrZ%2FP%2BmFZJ%2BmXGKhKHw8JsiPnCiv8mcdGyAtzY%2FOGXwoOrrqQT%2F08LmCorj0pHd0sLQxIbpDSFg68hwM8d7oBro9Nws7sLY3ZAaUeruqQS8v6B5%2FY2xKzdpUu3TMQ3PxO2TDYEAqZPKWbhacPTKewc5vtkYMH9FTDJkQR3wTHpUns6cEtFfzExeBQA5kc6P0aFwbqdWkS6JlrMViXPbbkjDV%2BbFV126YjKjStLSJ%2BaAoA0CEjY4Dcy3bCKN%2BxZRMCCdgVmLdufJgUkafVHAXo5HzpRZdof9DaEBtjkpe2pZj2%2BzjipXYsqLfg%2BosYX5Eq0Vig2jxB%2Fm3xEqivau91KFvOnG2MNR5DcAmQ7bqxouSNtQGrK3tLi8Muye05VRXA3Vih0k4%2FMNIqU%2F3wPh8TQD7bXf7dfAh3iQ2ExUexP2xx8X8QMUarKRZoSoag%2Fsa5RTjZZtOZAQkpzcVCqjbqGq44VW3hijjMqi7fuye6rRDwNTW%2FKbfbOE%2BW%2F8hyWFnzcux1v3O0Q0Rout%2FkYuJS%2FRkePA4K936OLV0cSnURvxPlis4Cy%2Ff59lNxjI4fX1rFQNkZZgctIIWLiiRxae7tB4x9eSN0H8w9lB7Yb7OrHUVDj6gB0%2BrqzX4dYu%2Bfvymuh75LZ%2FbPWxRR0xLk2e4k4UeO2QTF%2BJZLpd4QwEE0u0oJ70IpUOE1wk5gS7oMh0Caax1eZekHZpQQ4hYryUvDHIQukSQTP6rZysh17dFVCx9dmsEVNaGSojwWxtBvig9sy8wJ6BiNVoPE8tZHQgTg1hOHIxksJq3sEtVkXIjKOFaBErLSalm5g0t7%2BgSsSQqFHTcHlPgCgTJ6yF47O1RkO1feBdfQ%2FavODmzqbNqc9S2vM20%2FYYycw9Va5womY1r9YcMV3Ve7a72mGgI6xnR0tKq%2BYDROUvFYC7aMZ3w2lRpLoB%2BQR9j3n9By6NaMeTXl95vqZmCFH8eVdUS0trOgHfu6rmNltOUBgbovdK0js6DsHJd6yG2BF0CtuC0rVm0vWQWn881UApromuIusQa4BhC9BmJCZbIEnu%2FS1JkDwdOD9OfmiRdwGE2mS9U70KdmHesQsa64iQ%2FBNu3HOXzhvsEclR28J6RYykTUi3CEQZzBEkcXwl56vAWlhzey1cR6GxCESNHAMolPjLUw40lKwZG0I%2FQIL5x9JQfZ6SYN0Rs2%2FsePHEHdXXizkE17Pj0z387M2Bh1MeXA%2F1PqBN4SAYiOWQMhWRqSditCr3AFWjkdE%2Fqi3yQ6rOw9gg2LvJ61cb%2BGZRheFlDocyUv3JUqwjgNztzRLx8vc6eklm%2FuiuoMuK5HpKlSnIRbGbYF1510hoV19xRoqfC3SCHxhsfMbGwMy8JY2V7caD6VZ4fN%2FBNzQZTZTuYfU0HXPy7XHNJ5%2Fio9w5KQ742h5H1tDDHPxIvypFMSoU8vwn45Wyyu8aqKMaSCpJqtT3Mis7VU7cxZwNjM27Jl0UquaRctHskVuAdpJpV%2FOv99%2BYNWKqbMeCyWQAtrxjchOz30xe22%2BeAr0ex2f9wktBDEJUNoZ50Bdv3IUOZhZ6uOqm2WgtlxV9QiGLYBn72brkrnmPZwH1kh1Bq71k%2FhiasOXlk3StEKhqQRA8BSP8N6Jh2NX4iX7SfXj%2BswBr%2FsRpySgJi0oL24yKNdOfSgVVxA2C6BrqAWaqJcxva6T6IQIdcM3em85Fj0cJ%2F1b6blnQrl5lENiesdh7TnzwOW%2FXd5ucKiyyPkI5QThsI2gP5T8DxA%2FECQdI615zobM%2B2h9dOXXKVzZS3eyKs53fndFSFszDD%2BW8CxmHlv%2FHVkJbs0UYjiGVP1BYrBvU0lUdTyRgsvhtXgEMkydFx0cgP8G4tU9840LC4j1mCgX9rDzN4KCFu5fhMtDYtQ6p9%2Fc94BDbzVdduipet9DYIx%2F8KBTLYvYeQPsaghCCkRGtTya%2BV20Qc2gXDkR8zN87fUG0eBW8T%2FL3gVYHlskeazKkOawkoCyTRpDMvh5Ss%2B6YF6QT%2F2EptXNx4mRv18HnD2L1trQf7FwerSVfNUO9Zsh4U7mnlnPmIX%2FFpEfl5IV%2FzG5UspJqch6Zkj6aNfpMMu8ekGvFHbXB5YNmZouhK1f%2FwmEaTd6MrGwiTu7e2xGknTXCKmtdhIpOmPdaZhmLAs5XXhRj2Vz32aY8Dj9HJTfwO%2FPdNe%2FxvKfuei9tr%2B2oYYhbQOXLGT5D%2BkC3q4IFIM%2FcV55dP3%2FzKAFgk37%2BRpg%2BoIldRsDl3x5lR0mlMo3KV%2BOVIRuuMzqZCdz6ITdOR%2BFihUMDCdkculV2b9PfWgt8DMNxlnTrmb20xe56sDHfLzwD0cWjRodzIkMq34zPBOS5rG%2B5VVgWUJhdtC%2BbMdTiU8YjYiAgX22%2Fmp%2BiFWnH9q%2FEWETgtEyoH3lURE9%2F7zIEvsIuSbLeR8LXufT2vTTAE7wZ0nG%2FMzXwbkve3VTZF3YcUc5VSymOMwaoTYzwXEsMi18YQ6GUeRhwtZ2%2FiDBfmteVmIBOVtX23ezrHkCG43AIfIR3r1ZAi4p9hLdg9qDwumYGuCCGNgfIU4AosggzstKKNAkDbk7ZVmFnQI4TdVVSZMXBlJlUGJUSftQZKbLN8UcGHeN7W2CU0Gq3mG%2FTTZqrFLkamGhl7TQlNg%2BZVLXyg9jdDVncJ3gvvTfptXYmwK49qGyXS4hnVQDxss8qe9RM1kNnHZvMxZ3cGC8tn3k5%2F8I3ujOymvjpY65kVOQcRxehbmBU0Xfpwph0CO9HVhyFuL44hEArEf5H8DYLkGXBYxOIrywKZFmuZyOAJEtd6sHEEVtMr6Ot9eP8JZFtiXMNqA5Hc1kjGwXIKzK45ukD0yj2h%2FqxfFjlYYvIMYDQ5vxlTo8vmPClpIZG2P8ivzTTdiuSY7%2B50mzPQzF9Mk%2BIHL7MkUX1wtKa50YdixLM8NoW0OMNw8NINHht2xNBj4eItKtwex1o4KJHjJQCt3xQpzsTHlc%2B0tXB90ZUM4QKLIBJlEVIFq%2BiiE%2Fu2NoQztbRRlE%2FT5Gdl8tn3nuC%2FvHDHObhNybcwUG8nj6pp22OXLBRSZ6OM%2FiYg1DqVLxgkvXDsdVgd9TulYnDrSArdTtBYunIR23rArVeUj3WOtXeeSDNx4EZ9v08u3E5%2FFvsSqwhvd3fqTA02nSqCb0NIBsUPrR0wIksd344RjtVz3x4VVgcoFDSeHFn%2ByJ%2F1UUdjOB70tr57fCPdlKSkECQ5TZyvs%2BU36e5UJR%2Bc6ElQ5bt7jp0lXRBBQbS%2BGOFecGTLQJSfk9q6o3cvdaHKDhHjvk%2Bw9UEAtXyVFEKgCAc%2FvZFwODjDInSh3M8uJHeJM5pPeZ59GKFK8x3Hx2dNHWbaBdFQHeBWSiYmvNhCigspqT0TxsGM3GqqmNj2Z4nd8c2P8mHg%2FYwtRMFl96cM8rpWD6bjoZnO48ARxBVFAHNucs%2BhNXlvFG6Ai7TLquLHjjk%2Bq6n6GvGxUiHf%2FH045Cc0CAlbWSO3RcY9x%2Fig0WfQorotQGXXfcrhyk%2B8oLvRE%2F8ccyrnJjCVINf%2FeHYhgu9uaCDd%2BIjRdY1Eu0RPycMWrMEWrHBOMsjsjS9AElkx07ugy9A2VoLqg5i8Bi%2BqaJQIUdilk%2FEIH9PwJzq6L4fJ08V1PuDAgzzzflupOnbMe4ahnoKwHmWqsgASiG%2FY8VwpyV%2BsltpkreNWIx5En9BzCityb3ORB5%2Fe9owajQSGOxfv%2B92Iz2qptdJrrSa4f%2FNqKwTbUv8lcyXsaiv8XwVynuFflG7tJnMfUVg7CBIZiIzlRGWthyJRKgjqGEz4Tqu28D7%2FIEICxOX9zYb5EOcyvJ6fNVp0gvW6VuF3LLBBJcFo%2BBCejlzHL4bBx1LjFK%2FtvSvNqTIhMFDGrmsH%2F%2F972%2Fp%2FRks0zQKDgDeyKPZCW68tgvdk6%2FauNsOoC8KdLp94MXkFUvkATgijM%2B8hdH2BufSV7kTWsEpHXa9qMg%3D%3D&__VIEWSTATEGENERATOR=CA0B0334&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=sse%2BJXEPVLV899AHbEtv7MKI7oCa05RvfDA08M0flNWYL01QGTa9DOIfZ53pCgd8s8znqsgwdFF%2FVcCfqkgC%2BUTK8BiyybgOi2KKjlzckcmXlRSAYFhbg%2B2tvHGYVCU9RBFZfD%2F67Tpyd1cNKPvNtj4azL0%2BBiJ9OSzCoZPuma0olVhezLWkIWZcUP9lt8fk9qMs8H5zbyakAHEal90Cbqsp%2FuJ7ukIWsAfylpdjKDgShIdce%2B3ZUdQoNOHuRCSL9OWBL9jCi7GdYBMqY%2BbusMOyHlaeTdnR0ozYhhIg14oygeQqYcd1yQbOam5DHVjjKS4EawLdR5Ngb47kcEqfYGJrSWI%3D&dnn%24ctr446%24ViewCMAPSEARCH%24txtNameSearch={$officer->name}&ScrollTop=&__dnnVariable=%7B%22__scdoff%22%3A%221%22%7D&dnn%24ctr446%24ViewCMAPSEARCH%24txNameQueue=&dnn%24ctr446%24ViewCMAPSEARCH%24hfRoleID='Registered%20Users'%2C'CREDITBPO%20TECH%20INC'%2C'FINANCING%20LENDING'&__ASYNCPOST=true&dnn%24ctr446%24ViewCMAPSEARCH%24IBSearch.x=66&dnn%24ctr446%24ViewCMAPSEARCH%24IBSearch.y=16&RadAJAXControlID=dnn_ctr446_ViewCMAPSEARCH_UP");
            // grab all reports from the search
            $response = self::grab_page("http://search.cmaphil.com/iCMAPMo/tabid/88/ctl/all/mid/446/Default.aspx");

            self::addToCmapSearchQueryTable($officer->name);

            // Create DOM from URL
            $html = new simple_html_dom();
            $html->load($response);

            foreach ($tables_ids as $key => $table_id) {
                $rowData = array();
    
                //find table where table id is equal to needed table id
                $table = $html->find("table[id^={$table_id}]", 0);
    
                if ($table) {
                    //get results
                    foreach ($table->find('tr') as $row) {
                        // initialize array to store the cell data from each row
                        $td = array();
    
                        foreach ($row->find('td, th') as $cell) {
                            //remove white space from text
                            $stripped = trim(preg_replace('/\s+/', ' ', $cell->plaintext));
                            // push the cell's text to td data
                            $td[] = $stripped;
                        }
                        // push td data to row data
                        $rowData[] = $td;
                    }
    
                    $tables[$key] = $rowData;
                }
            }

            if ($tables) {
                foreach ($tables as $tablekey => $table) {
                    //check if table headers for each table exist in the database
                    $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tablekey)->first();
    
                    // if not, create new table headers in the database
                    if ($cmapTableHeader == null) {
                        //iterate through the first entry in array which is the table headers
                        foreach ($table[0] as $headerKey => $headerName) {
                            //if no data in table
                            if ($headerName == "NO RELATED COLLECTED DATA FROM 3RD PARTY") {
                                continue;
                            }
    
                            //create header entries
                            $header = new CmapTableHeaders;
                            $header->cmap_table_name = $tablekey;
                            $header->cmap_table_header = $headerName;
                            $header->header_order = $headerKey;
                            $header->save();
                        }
                    }
    
                    //get headers of table type
                    $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tablekey)->orderBy('header_order')->pluck('cmap_table_header');
    
                    //create cmap search data entry
                    foreach ($table as $dataKey => $data) {
                        //skip first entry in array which is the table headers
                        if ($dataKey == 0) {
                            continue;
                        }
    
                        $last_data_id = CmapSearchData::all()->last();
                        $data_id = 1;
    
                        if ($last_data_id != null) {
                            $data_id = $last_data_id->data_id + 1;
                        }
    
                        //iterate through the data and save to database
                        foreach ($data as $entryKey => $entryName) {
                            //create new search data
                            $entry = new CmapSearchData();
                            $entry->data_table_name = $tablekey;
                            $entry->data_name = $entryName;
                            $entry->data_type = $cmapTableHeader[$entryKey];
                            $entry->data_id = $data_id;
                            $entry->data_order = $entryKey;
                            $entry->save();
                        }
                    }
                }
            }
        }

        return;
    }

    public static function findInDatabaseOfficer($officerName)
    {
        //remove non alphanumeric chars in name - for accurate searching in db
        $officerName = preg_replace("/[^A-Za-z0-9 ]/", '', $officerName);
        $partialNames = explode(" ", $officerName);

        // search database if company is already in cmap search data
        $officerIds = CmapSearchData::where('data_order', '0');
        //iterate through partial name
        foreach($partialNames as $partialName) {
            $officerIds->where('data_name', 'like', "%{$partialName}%");
        }

        $officerIds = $officerIds->pluck('data_id');
        $data = array();

        // if yes, get data
        if ($officerIds) {
            foreach ($officerIds as $officerId) {
                $companyRecords = CmapSearchData::where('data_id', $officerId)->orderBy('data_order')->pluck('data_name', 'data_type');
                $tableType = CmapSearchData::where('data_id', $officerId)->first()->data_table_name;
                $cmapTableHeader = CmapTableHeaders::where('cmap_table_name', $tableType)->orderBy('header_order')->pluck('cmap_table_header');

                $data[] = ["tableType" => $tableType, 
                            "cmapTableHeader" => $cmapTableHeader, 
                            "companyRecords" => $companyRecords];
            }
        }

        return $data;
    }

    public static function login($url, $data)
    {
        $fp = fopen("cookie.txt", "w");
        fclose($fp);
        $login = curl_init();
        curl_setopt($login, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($login, CURLOPT_COOKIEFILE, "cookie.txt");
        curl_setopt($login, CURLOPT_TIMEOUT, 40000);
        curl_setopt($login, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($login, CURLOPT_URL, $url);
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($login, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        curl_setopt($login, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($login, CURLOPT_POST, TRUE);
        curl_setopt($login, CURLOPT_POSTFIELDS, $data);
        ob_start();
        return curl_exec($login);
        ob_end_clean();
        curl_close($login);
        unset($login);
    }

    public static function search($site, $data)
    {
        $datapost = curl_init();
        $headers = array("Expect:");
        curl_setopt($datapost, CURLOPT_URL, $site);
        curl_setopt($datapost, CURLOPT_TIMEOUT, 40000);
        curl_setopt($datapost, CURLOPT_HEADER, TRUE);
        curl_setopt($datapost, CURLOPT_HTTPHEADER, $headers);
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($datapost, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        curl_setopt($datapost, CURLOPT_POST, TRUE);
        curl_setopt($datapost, CURLOPT_POSTFIELDS, $data);
        curl_setopt($datapost, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($datapost, CURLOPT_COOKIEFILE, "cookie.txt");
        curl_setopt($datapost, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($datapost, CURLOPT_FOLLOWLOCATION, TRUE);
        ob_start();
        return curl_exec($datapost);
        ob_end_clean();
        curl_close($datapost);
        unset($datapost);
    }

    public static function grab_page($site)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");
        curl_setopt($ch, CURLOPT_URL, $site);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        ob_start();
        return $response = curl_exec($ch);
        ob_end_clean();
        curl_close($ch);
    }

    public static function addToCmapSearchQueryTable($company_name)
    {
        $query = new CmapSearchQuery;
        $query->company_name = $company_name;
        $query->save();

        // send email reminder
        $now = Carbon::now();
        $month = $now->month;
        $monthName = $now->format('F');
        $num_of_queries = count(CmapSearchQuery::whereMonth('created_at', $month)->get());

        if ($num_of_queries >= 40) {
            $mailHandler = new MailHandler();
            $mailHandler->sendNumOfCmapSearchQueries([
                'num_of_queries' => $num_of_queries,
                'month' => $monthName
            ]);
        }

        return;
    }
}
