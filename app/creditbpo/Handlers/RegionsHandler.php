<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\Zipcode;

class RegionsHandler {
    /**
     * @param Zipcode|null $model
     */
    public function __construct(Zipcode $model = null) {
        $this->model = $model ?: new Zipcode();
    }

    public function getAll() {
        return $this->model
            ->getAllRegions();
    }

    public function getAllLists() {
        return $this->getAll()->pluck('region', 'region');
    }
}
