<?php

namespace CreditBPO\Handlers;

use CreditBPO\Handlers\FinancialHandler;
use CreditBPO\Handlers\ReadyRatioScoreHandler;
use CreditBPO\Models\FinancialReport;

class FinancialPosition extends FinancialHandler {
    protected $model;

    /**
     * @param FinancialReport $model
     */
    public function __construct(FinancialReport $model = null) {
        $this->model = $model ?: new FinancialReport();
    }

    /**
     * @param int $financialReportId
     * @return int
     */
    public function getScore($financialReportId) {
        $financialReport = $this->model->find($financialReportId);

        if (@count($financialReport) <= 0) {
            return 0;
        }

        $balanceSheets = $financialReport->balanceSheets()
            ->orderBy('index_count', 'desc')
            ->get();

        if (@count($balanceSheets) <= 0) {
            return 0;
        }

        $debtRatio = $this->getDebtRatio($balanceSheets);
        $nonCurrentAssetToNetWorth = $this->getNonCurrentAssetToNetWorth($balanceSheets);
        $currentRatio = $this->getCurrentRatio($balanceSheets);
        $quickRatio = $this->getQuickRatio($balanceSheets);
        $cashRatio = $this->getCashRatio($balanceSheets);
        return ($debtRatio * 0.3) +
            ($nonCurrentAssetToNetWorth * 0.15) +
            ($currentRatio * 0.2) +
            ($quickRatio * 0.2) +
            ($cashRatio * 0.15);
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getScoreByEntityId($entityId) {
        $financialReport = $this->model
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();

        if (@count($financialReport) <= 0) {
            return 0;
        }

        return $this->getScore($financialReport->id);
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getScoreValueByEntityId($entityId) {
        $readyRatioScoreHandler = new ReadyRatioScoreHandler();
        return $readyRatioScoreHandler->getValueByRange(
            $this->getScoreByEntityId($entityId));
    }

    public function getScoreRangeByValue($value) {
        $readyRatioScoreHandler = new ReadyRatioScoreHandler();
        return $readyRatioScoreHandler->getValueByRange($value);
    }

    /**
     * @param array $balanceSheets
     * @return int
     */
    protected function getDebtRatio($balanceSheets) {
        $debtRatio = [];

        foreach ($balanceSheets as $balanceSheet) {
            if ($balanceSheet->Assets != 0 && $balanceSheet->Assets != 0.0) {
                $debtRatio[] = $balanceSheet->Liabilities / $balanceSheet->Assets;
                continue;
            }

            $debtRatio[] = 0;
        }

        $debtRatioSlope = [
            self::calculateSP($debtRatio),
            $debtRatio[@count($debtRatio) - 1],
            self::calculateSf($debtRatio),
        ];

        foreach ($debtRatioSlope as &$value) {
            if ($value > 1) {
                $value = -2;
                continue;
            }

            if ($value > 0.6) {
                $value = -1;
                continue;
            }

            if ($value > 0.5) {
                $value = 1;
                continue;
            }

            if ($value >= 0.3) {
                $value = 2;
                continue;
            }

            $value = 1;
        }

        return self::calculateAverageScore($debtRatioSlope);
    }

    /**
     * @param array $balanceSheets
     * @return int
     */
    protected function getNonCurrentAssetToNetWorth($balanceSheets) {
        $data = [];

        foreach ($balanceSheets as $balanceSheet) {
            if ($balanceSheet->Equity != 0 && $balanceSheet->Equity != 0) {
                $data[] = $balanceSheet->NoncurrentAssets / $balanceSheet->Equity;
                continue;
            }

            $data[] = 0;
        }

        $nonCurrentAssetSlope = [
            self::calculateSP($data),
            $data[@count($data) -1],
            self::calculateSf($data),
        ];

        foreach ($nonCurrentAssetSlope as &$value) {
            if ($value > 2) {
                $value = -2;
                continue;
            }

            if ($value > 1.25) {
                $value = -1;
                continue;
            }

            if ($value > 1) {
                $value = 1;
                continue;
            }

            if ($value >= 0) {
                $value = 2;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($nonCurrentAssetSlope);
    }

    /**
     * @param array $balanceSheets
     * @return int
     */
    protected function getCurrentRatio($balanceSheets) {
        $data = [];

        foreach ($balanceSheets as $balanceSheet) {
            if ($balanceSheet->CurrentLiabilities != 0 && $balanceSheet->CurrentLiabilities != 0.0) {
                $data[] = $balanceSheet->CurrentAssets / $balanceSheet->CurrentLiabilities;
                continue;
            }

            $data[] = 0;
        }

        $currentRatioSlope = [
            self::calculateSP($data),
            $data[@count($data) - 1],
            self::calculateSf($data),
        ];

        foreach ($currentRatioSlope as &$value) {
            if ($value >= 2.1) {
                $value = 2;
                continue;
            }

            if ($value >= 2) {
                $value = 1;
                continue;
            }

            if ($value >= 1) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($currentRatioSlope);
    }

    /**
     * @param array $balanceSheets
     * @return int
     */
    protected function getQuickRatio($balanceSheets) {
        $data = [];

        foreach ($balanceSheets as $balanceSheet) {
            if ($balanceSheet->CurrentLiabilities != 0 && $balanceSheet->CurrentLiabilities != 0.0) {
                $data[] = (
                        $balanceSheet->CashAndCashEquivalents +
                        $balanceSheet->OtherCurrentFinancialAssets +
                        $balanceSheet->TradeAndOtherCurrentReceivables
                    ) / $balanceSheet->CurrentLiabilities;
                continue;
            }

            $data[] = 0;
        }

        $quickRatioSlope = [
            self::calculateSP($data),
            $data[@count($data) - 1],
            self::calculateSf($data),
        ];

        foreach ($quickRatioSlope as &$value) {
            if ($value >= 1.1) {
                $value = 2;
                continue;
            }

            if ($value >= 1) {
                $value = 1;
                continue;
            }

            if ($value >= 0.5) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($quickRatioSlope);
    }

    public function getCashRatio($balanceSheets) {
        $data = [];

        foreach ($balanceSheets as $balanceSheet) {
            if ($balanceSheet->CurrentLiabilities != 0 && $balanceSheet->CurrentLiabilities != 0.0) {
                $data[] = $balanceSheet->CashAndCashEquivalents /
                    $balanceSheet->CurrentLiabilities;
                continue;
            }

            $data[] = 0;
        }

        $cashRatioSlope = [
            self::calculateSP($data),
            $data[@count($data) - 1],
            self::calculateSf($data),
        ];

        foreach ($cashRatioSlope as &$value) {
            if ($value >= 0.25) {
                $value = 2;
                continue;
            }

            if ($value >= 0.2) {
                $value = 1;
                continue;
            }

            if ($value >= 0.05) {
                $value = -1;
                continue;
            }

            $value = -2;
        }

        return self::calculateAverageScore($cashRatioSlope);
    }
}
