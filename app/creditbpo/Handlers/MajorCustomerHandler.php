<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\MajorCustomer;
use CreditBPO\Models\MajorCustomerAccount;

class MajorCustomerHandler {
    const RELATIONSHIP_SATISFACTION_GOOD = 1;
    const RELATIONSHIP_SATISFACTION_SATISFACTORY = 2;
    const RELATIONSHIP_SATISFACTION_UNSATISFACTORY = 3;
    const RELATIONSHIP_SATISFACTION_BAD = 4;
    const SCORE_LOW_DEPENDENCE = 30;
    const SCORE_MEDIUM_DEPENDENCE = 20;
    const SCORE_HIGH_DEPENDENCE = 10;

    protected $model;

    public function __construct(MajorCustomer $model = null) {
        $this->model = $model ?: new MajorCustomer();
    }

    public static function getAllRelationshipSatisfaction() {
        return [
            self::RELATIONSHIP_SATISFACTION_GOOD =>
                trans('business_details2.mc_relation_good'),
            self::RELATIONSHIP_SATISFACTION_SATISFACTORY =>
                trans('business_details2.mc_relation_satisfactory'),
            self::RELATIONSHIP_SATISFACTION_UNSATISFACTORY =>
                trans('business_details2.mc_relation_unsatisfactory'),
            self::RELATIONSHIP_SATISFACTION_BAD =>
                trans('business_details2.mc_relation_bad'),
        ];
    }

    public function getAssessmentScoreByEntityId($entityId) {
        $maxScore = $this->getMaxScoreByEntityId($entityId);

        if ($maxScore === 0) {
            $majorCustomerAccountHandler = new MajorCustomerAccount();
            $customer = $majorCustomerAccountHandler->getFirstCustomerAccountByReportId($entityId);

            if (@count($customer) > 0 && $customer->is_agree === 1) {
                return self::SCORE_LOW_DEPENDENCE;
            }
        }

        if ($maxScore >= 70) {
            return self::SCORE_HIGH_DEPENDENCE;
        }

        if ($maxScore > 30) {
            return self::SCORE_MEDIUM_DEPENDENCE;
        }

        return self::SCORE_LOW_DEPENDENCE;
    }

    protected function getMaxScoreByEntityId($entityId) {
        $majorCustomers = $this->model->getByEntityId($entityId);

        if (@count($majorCustomers) <= 0) {
            return 0;
        }

        $max = 0;

        foreach ($majorCustomers as $customer) {
            if ($customer->customer_share_sales > $max) {
                $max = $customer->customer_share_sales;
            }
        }

        return $max;
    }
}
