<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\Report\PDF\FinancialAnalysisInternal as FinancialAnalysisInternalPDF;
use User;
use DB;

class FinancialAnalysisInternalHandler {

    const DEFAULT_LOGIN_ID = 76;
    
    /**
     * @param int $financialReportId
     * @return bool
     */
    public function createFinancialAnalysisInternalReport($financialReportId,$language='en') {
        $pdf = new FinancialAnalysisInternalPDF();
        $contents = $pdf->create($financialReportId,$language)->generate();

        if ($contents) {
            $contents->setOptions(["isPhpEnabled"=> true, "isHtml5ParserEnabled"=> true, "isRemoteEnabled"=> true]); // To set page numbers
            $contents->setPaper('a4', 'portrait');
            $pdf->save();

            return $pdf->getFilenamePath($financialReportId,$language);
        }
        
        return false;
    }
}
