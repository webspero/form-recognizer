<?php

/**
 * This class should be refactored at some point since the methods were copied
 * from a legacy class.
 */

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Input;
use CreditBPO\Models\BankIndustry;
use CreditBPO\Models\BankFormula;
use stdClass;
use DB;
use ColorHelper;

class ValidUserStatisticsHandler {
    public function getStatistics() {
        $sme_selected = Input::get('sme');
        $main_industry_selected = Input::get('industry1');
        $sub_industry_selected = Input::get('industry2');
        $industry_selected = Input::get('industry3');
        $region_selected = Input::get('region');
        $pdefault_from = Input::get('pdefault_from');
        $pdefault_to = Input::get('pdefault_to');
        $pdfrom = 0;
        $pdto = 250;

        // set score ranges
        if($pdefault_from >= 0 && $pdefault_from <= 2){
            $pdto = 250;
        } elseif($pdefault_from > 2 && $pdefault_from <= 3){
            $pdto = 176;
        } elseif($pdefault_from > 3 && $pdefault_from <= 4){
            $pdto = 149;
        } elseif($pdefault_from > 4 && $pdefault_from <= 6){
            $pdto = 122;
        } elseif($pdefault_from > 6 && $pdefault_from <= 8){
            $pdto = 95;
        } elseif($pdefault_from > 8 && $pdefault_from <= 15){
            $pdto = 67;
        } elseif($pdefault_from > 15 && $pdefault_from <= 30){
            $pdto = 41;
        } elseif($pdefault_from > 30 && $pdefault_from <= 50){
            $pdto = 31;
        } elseif($pdefault_from > 50 && $pdefault_from <= 80){
            $pdto = 21;
        } elseif($pdefault_from > 80 && $pdefault_from <= 100){
            $pdto = 11;
        }

        // set score ranges
        if($pdefault_to >= 0 && $pdefault_to <= 2){
            $pdfrom = 177;
        } elseif($pdefault_to > 2 && $pdefault_to <= 3){
            $pdfrom = 150;
        } elseif($pdefault_to > 3 && $pdefault_to <= 4){
            $pdfrom = 123;
        } elseif($pdefault_to > 4 && $pdefault_to <= 6){
            $pdfrom = 96;
        } elseif($pdefault_to > 6 && $pdefault_to <= 8){
            $pdfrom = 68;
        } elseif($pdefault_to > 8 && $pdefault_to <= 15){
            $pdfrom = 42;
        } elseif($pdefault_to > 15 && $pdefault_to <= 30){
            $pdfrom = 32;
        } elseif($pdefault_to > 30 && $pdefault_to <= 50){
            $pdfrom = 22;
        } elseif($pdefault_to > 50 && $pdefault_to <= 80){
            $pdfrom = 12;
        } elseif($pdefault_to > 80 && $pdefault_to <= 100){
            $pdfrom = 0;
        }

        // Industry
        if(Input::get('type')==1){

            // get data
            $data = DB::table('tblentity')->select(DB::raw('
                tblindustry_main.main_title as industry_main,
                tblindustry_sub.sub_title as industry_sub,
                tblindustry_row.row_title as industry_row,
                count(tblentity.industry_row_id) as value,
                tblsmescore.*,
                (tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bgroup,
                (tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mgroup,
                tblsmescore.score_facs as fgroup,
                CAST(tblsmescore.score_sf AS DECIMAL) as creditbpo_rating,
                tblentity.industry_main_id
            '))
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
            ->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
            ->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
            ->groupBy('tblentity.industry_main_id')->groupBy('tblentity.industry_sub_id')->groupBy('tblentity.industry_row_id')
            ->orderBy('tblindustry_main.main_title')->orderBy('tblindustry_sub.sub_title')->orderBy('tblindustry_row.row_title');

            // apply filters
            if(@count($sme_selected)>0){
                $data->whereIn('tblentity.entityid', $sme_selected);
            }
            if($main_industry_selected!=""){
                $data->where('tblentity.industry_main_id', $main_industry_selected);
            }
            if($sub_industry_selected!=""){
                $data->where('tblentity.industry_sub_id', $sub_industry_selected);
            }
            if($industry_selected!=""){
                $data->where('tblentity.industry_row_id', $industry_selected);
            }
            if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
                $data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
            }
            if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
                $data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
            }
            if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
                $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
            }
            if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
                $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
            }
            $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
            if($region_selected!=""){
				$data->where('refcitymun.regDesc', $region_selected);
            }

            $data = $data->get();

            // bank rating computation
            if(Input::get('brating_from')!="" && Input::get('brating_to')!="") {
                foreach($data as $k=>$d){
                    $bank_group = BankIndustry::where('bank_id', $d->current_bank)
                            ->where('industry_id', $d->industry_main_id)->first();
                    $bank_formula = BankFormula::where('bank_id', $d->current_bank)->first();

                    if($bank_group){
                        if($bank_formula){
                            //bcc
                            $bgroup_array = explode(',', $bank_formula->bcc);
                            $bgroup_item = 0;
                            $bgroup_total = 0;
                            if(in_array('rm', $bgroup_array)) {
                                $bgroup_item += $d->score_rm;
                                $bgroup_total += MAX_RM;
                            }
                            if(in_array('cd', $bgroup_array)) {
                                $bgroup_item += $d->score_cd;
                                $bgroup_total += MAX_CD;
                            }
                            if(in_array('sd', $bgroup_array)) {
                                $bgroup_item += $d->score_sd;
                                $bgroup_total += MAX_SD;
                            }
                            if(in_array('boi', $bgroup_array)) {
                                $bgroup_item += $d->score_bois;
                                $bgroup_total += MAX_BOI;
                            }

                            if (0 < $bgroup_total) {
                                $bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;
                            }
                            else {
                                $bgroup = 0;
                            }

                            //mq
                            $mgroup_array = explode(',', $bank_formula->mq);
                            $mgroup_item = 0;
                            $mgroup_total = 0;
                            if(in_array('boe', $mgroup_array)) {
                                $mgroup_item += $d->score_boe;
                                $mgroup_total += MAX_BOE;
                            }
                            if(in_array('mte', $mgroup_array)) {
                                $mgroup_item += $d->score_mte;
                                $mgroup_total += MAX_MTE;
                            }
                            if(in_array('bd', $mgroup_array)) {
                                $mgroup_item += $d->score_bdms;
                                $mgroup_total += MAX_BD;
                            }
                            if(in_array('sp', $mgroup_array)) {
                                $mgroup_item += $d->score_sp;
                                $mgroup_total += MAX_SP;
                            }
                            if(in_array('ppfi', $mgroup_array)) {
                                $mgroup_item += $d->score_ppfi;
                                $mgroup_total += MAX_PPFI;
                            }

                            if (0 < $mgroup_total) {
                                $mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;
                            }
                            else {
                                $mgroup = 0;
                            }

                            //fa
                            $fgroup_array = explode(',', $bank_formula->fa);
                            if(@count($fgroup_array) == 2){
                                $fgroup = $d->score_facs;
                            } else {
                                if($bank_formula->fa == 'fp'){
                                    $fgroup = $d->score_rfp;
                                } elseif($bank_formula->fa == 'fr'){
                                    $fgroup = $d->score_rfpm;
                                } else {
                                    $fgroup = 0;
                                }
                            }
                        } else {
                            $bgroup = ( $d->score_rm + $d->score_cd + $d->score_sd + $d->score_bois );
                            $mgroup = ( $d->score_boe + $d->score_mte + $d->score_bdms + $d->score_sp + $d->score_ppfi );
                            $fgroup = $d->score_facs;
                        }
                        $gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

                        if($bank_formula && $bank_formula->boost == 0){
                            // no boost
                        } else {
                            //boost
                            if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
                            if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
                            if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
                        }
                    } else {
                        $gscore = $d->creditbpo_rating;
                    }
                    if(($gscore < (int)Input::get('brating_from')) || ($gscore > (int)Input::get('brating_to'))){
                        unset($data[$k]); //remove from list
                    }
                }
            }

            // series
            $color = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1", "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#D809CF", "#0AEE75", "#F8FA49",  "#F79730", "#F730B8", "#88FA69"];
            $total = 0;

            $series1 = array();
            $series1_temp = '';
            $series1_index = -1;
            $series2 = array();
            $series2_temp = '';
            $series2_index = -1;
            $series3 = array();
            $series3_temp = '';
            $series3_index = -1;

            $report_cntr    = 0;
            $report2_cntr   = 0;
            $report3_cntr   = 0;

            $loop_cntr      = 1;
            $data_cntr      = @count($data);

            foreach ($data as $d) {

                $credit_exp = explode('-', $d->score_sf);

                if ($series1_temp != $d->industry_main) {

                    if (0 <= $series1_index) {
                        $series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
                    }

                    $series1_index++;
                    $series1_temp                       = $d->industry_main;

                    $series1[$series1_index]            = new stdClass();
                    $series1[$series1_index]->name      = $d->industry_main;
                    $series1[$series1_index]->y         = $d->value;
                    $series1[$series1_index]->color     = ColorHelper::colourBrightness($color[$series1_index], -0.9);
                    $series1[$series1_index]->average   = (int)$credit_exp[0];

                    $report_cntr                        = 1;

                    if ($data_cntr == $loop_cntr) {
                        $series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
                    }
                }
                else {
                	$report_cntr++;
                    if(isset($series1[$series1_index])){
	                    
	                    $series1[$series1_index]->y         += $d->value;
	                    $series1[$series1_index]->average   += (int)$credit_exp[0];

	                    if ($data_cntr == $loop_cntr) {
	                        $series1[$series1_index]->average = $this->getScoreAverage($series1[$series1_index]->average, $report_cntr);
	                    }
                	}
                }

                if ($series2_temp != $d->industry_sub) {

                	if(isset($series2[$series2_index])){
	                    if (0 <= $series2_index) {
	                        $series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
	                    }

	                    $series2_index++;
	                    $series2_temp                       = $d->industry_sub;

                    
	                    $series2[$series2_index]            = new stdClass();
	                    $series2[$series2_index]->name      = $d->industry_sub;
	                    $series2[$series2_index]->y         = $d->value;
	                    $series2[$series2_index]->color     = ColorHelper::colourBrightness($color[$series1_index], 1);
	                    $series2[$series2_index]->average   = (int)$credit_exp[0];

	                    $report2_cntr                        = 1;

	                    if ($data_cntr == $loop_cntr) {
	                        $series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
	                    }
                	}
                }
                else {
                    $report2_cntr++;

                    if(isset($series2[$series2_index])){
	                    $series2[$series2_index]->y         += $d->value;
	                    $series2[$series2_index]->average   += (int)$credit_exp[0];

	                    if ($data_cntr == $loop_cntr) {
	                        $series2[$series2_index]->average = $this->getScoreAverage($series2[$series2_index]->average, $report2_cntr);
	                    }
                	}
                }

                if ($series3_temp != $d->industry_row) {

                	if(isset($series3[$series3_index])){
	                    if (0 <= $series3_index) {
	                        $series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
	                    }

	                    $series3_index++;
	                    $series3_temp                       = $d->industry_row;

	                    $series3[$series3_index]            = new stdClass();
	                    $series3[$series3_index]->name      = $d->industry_row;
	                    $series3[$series3_index]->y         = $d->value;
	                    $series3[$series3_index]->color     = ColorHelper::colourBrightness($color[$series1_index], .9);
	                    $series3[$series3_index]->average   = (int)$credit_exp[0];

	                    $report3_cntr                        = 1;

	                    if ($data_cntr == $loop_cntr) {
	                        $series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
	                    }
                	}
                }
                else {
                    $report3_cntr++;

                    if(isset($series3[$series3_index])){
	                    $series3[$series3_index]->y         += $d->value;
	                    $series3[$series3_index]->average   += (int)$credit_exp[0];

	                    if ($data_cntr == $loop_cntr) {
	                        $series3[$series3_index]->average = $this->getScoreAverage($series3[$series3_index]->average, $report3_cntr);
	                    }
                	}
                }

                $total += $d->value;
                $loop_cntr++;
            }

            // calculate value /100
            foreach($series1 as $key=>$value){
                $series1[$key]->y = round($value->y / $total * 100, 2);
            }
            foreach($series2 as $key=>$value){
                $series2[$key]->y = round($value->y / $total * 100, 2);
            }
            foreach($series3 as $key=>$value){
                $series3[$key]->y = round($value->y / $total * 100, 2);
            }

            return array(
                'type' => 1,
                'data' => array(
                    'series1' => $series1,
                    'series2' => $series2,
                    'series3' => $series3
                )
            );
        }

        // Composite Final Ratings
        if(Input::get('type')==2){
            $data = DB::table('tblentity')->select(DB::raw('
                tblentity.entityid,
                AVG(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bcc_score,
                AVG(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mq_score,
                AVG(tblsmescore.score_facs) as fa_score,
                AVG(CAST(tblsmescore.score_sf AS DECIMAL)) as creditbpo_rating
            '))
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
            ->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
            ->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
            ->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->leftJoin('tblbankindustryweights', function($join){
                $join->on('tblentity.industry_main_id', '=', 'tblbankindustryweights.industry_id');
                $join->on('tblentity.current_bank', '=', 'tblbankindustryweights.bank_id');
            })
            ->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7);

            //filters
            if(@count($sme_selected)>0){
                $data->whereIn('tblentity.entityid', $sme_selected);
            }
            if($main_industry_selected!=""){
                $data->where('tblentity.industry_main_id', $main_industry_selected);
            }
            if($sub_industry_selected!=""){
                $data->where('tblentity.industry_sub_id', $sub_industry_selected);
            }
            if($industry_selected!=""){
                $data->where('tblentity.industry_row_id', $industry_selected);
            }
            if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
                $data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
            }
            if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
                $data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
            }
            if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
                $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
            }
            if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
                $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
            }
            $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
            if($region_selected!=""){
				$data->where('refcitymun.regDesc', $region_selected);
            }
            if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
                $bank_formula = BankFormula::join('tblentity', 'tblentity.current_bank', '=', 'bank_formula.bank_id')
                    ->whereIn('tblentity.entityid', $sme_selected)->first();
                if($bank_formula){
                    //bcc
                    $bgroup_array = explode(',', $bank_formula->bcc);
                    $bgroup_item = [];
                    $bgroup_total = 0;
                    if(in_array('rm', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_rm';
                        $bgroup_total += MAX_RM;
                    }
                    if(in_array('cd', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_cd';
                        $bgroup_total += MAX_CD;
                    }
                    if(in_array('sd', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_sd';
                        $bgroup_total += MAX_SD;
                    }
                    if(in_array('boi', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_bois';
                        $bgroup_total += MAX_BOI;
                    }
                    $bgroup = '(' . implode(' + ', $bgroup_item) . ') * ' . (MAX_RM + MAX_CD + MAX_SD + MAX_BOI) . ' / ' . $bgroup_total;

                    //mq
                    $mgroup_array = explode(',', $bank_formula->mq);
                    $mgroup_item = [];
                    $mgroup_total = 0;
                    if(in_array('boe', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_boe';
                        $mgroup_total += MAX_BOE;
                    }
                    if(in_array('mte', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_mte';
                        $mgroup_total += MAX_MTE;
                    }
                    if(in_array('bd', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_bdms';
                        $mgroup_total += MAX_BD;
                    }
                    if(in_array('sp', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_sp';
                        $mgroup_total += MAX_SP;
                    }
                    if(in_array('ppfi', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_ppfi';
                        $mgroup_total += MAX_PPFI;
                    }
                    $mgroup = '(' . implode(' + ', $mgroup_item) . ') * ' . (MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI) . ' / ' . $mgroup_total;

                    //fa
                    $fgroup_array = explode(',', $bank_formula->fa);
                    if(@count($fgroup_array) == 2){
                        $fgroup = 'tblsmescore.score_facs';
                    } else {
                        if($bank_formula->fa == 'fp'){
                            $fgroup = 'tblsmescore.score_rfp';
                        } elseif($bank_formula->fa == 'fr'){
                            $fgroup = 'tblsmescore.score_rfpm';
                        } else {
                            $fgroup = '0';
                        }
                    }
                    $where_raw_string = '(
                        (('.$bgroup.') * (tblbankindustryweights.business_group / 100)) +
                        (('.$mgroup.') * (tblbankindustryweights.management_group / 100)) +
                        ('.$fgroup.' * (tblbankindustryweights.financial_group / 100))
                    )';
                } else {
                    $where_raw_string = '(
                        ((tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) * (tblbankindustryweights.business_group / 100)) +
                        ((tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) * (tblbankindustryweights.management_group / 100)) +
                        (tblsmescore.score_facs * (tblbankindustryweights.financial_group / 100))
                    )';
                }
                $data->whereRaw($where_raw_string.' between ' . Input::get('brating_from') . ' and ' . Input::get('brating_to'));
            }

            $data = $data->first();

            return array(
                'type' => 2,
                'data' => array(
                    'categories' => array(
                        'Business Considerations and Conditions',
                        'Management Quality',
                        'Financial Condition',
                        'CreditBPO Rating'
                    ),
                    'series' => array(
                        (float)$data->bcc_score,
                        (float)$data->mq_score,
                        (float)$data->fa_score,
                        (float)$data->creditbpo_rating
                    )
                )
            );
        }

        // Composite Final Ratings
        if(Input::get('type')==3){
            $data = DB::table('tblentity')->select(DB::raw('
                tblentity.entityid,
                AVG(tblsmescore.score_rm) as score_rm,
                AVG(tblsmescore.score_cd) as score_cd,
                AVG(tblsmescore.score_sd) as score_sd,
                AVG(tblsmescore.score_bois) as score_bois,

                AVG(tblsmescore.score_boe) as score_boe,
                AVG(tblsmescore.score_mte) as score_mte,
                AVG(tblsmescore.score_bdms) as score_bdms,
                AVG(tblsmescore.score_sp) as score_sp,
                AVG(tblsmescore.score_ppfi) as score_ppfi,

                AVG(tblsmescore.score_rfp) as score_rfp,
                AVG(tblsmescore.score_rfpm) as score_rfpm
            '))
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
            ->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
            ->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
            ->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->leftJoin('tblbankindustryweights', function($join){
                $join->on('tblentity.industry_main_id', '=', 'tblbankindustryweights.industry_id');
                $join->on('tblentity.current_bank', '=', 'tblbankindustryweights.bank_id');
            })
            ->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7);

            //filters
            if(@count($sme_selected)>0){
                $data->whereIn('tblentity.entityid', $sme_selected);
            }
            if($main_industry_selected!=""){
                $data->where('tblentity.industry_main_id', $main_industry_selected);
            }
            if($sub_industry_selected!=""){
                $data->where('tblentity.industry_sub_id', $sub_industry_selected);
            }
            if($industry_selected!=""){
                $data->where('tblentity.industry_row_id', $industry_selected);
            }
            if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
                $data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
            }
            if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
                $data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
            }
            if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
                $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
            }
            if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
                $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
            }
            $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
            if($region_selected!=""){
				$data->where('refcitymun.regDesc', $region_selected);
            }
            if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
                $bank_formula = BankFormula::join('tblentity', 'tblentity.current_bank', '=', 'bank_formula.bank_id')
                    ->whereIn('tblentity.entityid', $sme_selected)->first();
                if($bank_formula){
                    //bcc
                    $bgroup_array = explode(',', $bank_formula->bcc);
                    $bgroup_item = [];
                    $bgroup_total = 0;
                    if(in_array('rm', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_rm';
                        $bgroup_total += MAX_RM;
                    }
                    if(in_array('cd', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_cd';
                        $bgroup_total += MAX_CD;
                    }
                    if(in_array('sd', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_sd';
                        $bgroup_total += MAX_SD;
                    }
                    if(in_array('boi', $bgroup_array)) {
                        $bgroup_item[] = 'tblsmescore.score_bois';
                        $bgroup_total += MAX_BOI;
                    }
                    $bgroup = '(' . implode(' + ', $bgroup_item) . ') * ' . (MAX_RM + MAX_CD + MAX_SD + MAX_BOI) . ' / ' . $bgroup_total;

                    //mq
                    $mgroup_array = explode(',', $bank_formula->mq);
                    $mgroup_item = [];
                    $mgroup_total = 0;
                    if(in_array('boe', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_boe';
                        $mgroup_total += MAX_BOE;
                    }
                    if(in_array('mte', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_mte';
                        $mgroup_total += MAX_MTE;
                    }
                    if(in_array('bd', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_bdms';
                        $mgroup_total += MAX_BD;
                    }
                    if(in_array('sp', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_sp';
                        $mgroup_total += MAX_SP;
                    }
                    if(in_array('ppfi', $mgroup_array)) {
                        $mgroup_item[] = 'tblsmescore.score_ppfi';
                        $mgroup_total += MAX_PPFI;
                    }
                    $mgroup = '(' . implode(' + ', $mgroup_item) . ') * ' . (MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI) . ' / ' . $mgroup_total;

                    //fa
                    $fgroup_array = explode(',', $bank_formula->fa);
                    if(@count($fgroup_array) == 2){
                        $fgroup = 'tblsmescore.score_facs';
                    } else {
                        if($bank_formula->fa == 'fp'){
                            $fgroup = 'tblsmescore.score_rfp';
                        } elseif($bank_formula->fa == 'fr'){
                            $fgroup = 'tblsmescore.score_rfpm';
                        } else {
                            $fgroup = '0';
                        }
                    }
                    $where_raw_string = '(
                        (('.$bgroup.') * (tblbankindustryweights.business_group / 100)) +
                        (('.$mgroup.') * (tblbankindustryweights.management_group / 100)) +
                        ('.$fgroup.' * (tblbankindustryweights.financial_group / 100))
                    )';
                } else {
                    $where_raw_string = '(
                        ((tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) * (tblbankindustryweights.business_group / 100)) +
                        ((tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) * (tblbankindustryweights.management_group / 100)) +
                        (tblsmescore.score_facs * (tblbankindustryweights.financial_group / 100))
                    )';
                }
                $data->whereRaw($where_raw_string.' between ' . Input::get('brating_from') . ' and ' . Input::get('brating_to'));
            }

            $data = $data->first();

            return array(
                'type' => 3,
                'data' => array(
                    'category1' => array(
                        'Risk Management Rating',
                        'Customer Dependency',
                        'Supplier Dependency',
                        'Business Outlook Index Score'
                    ),
                    'series1' => array(
                        (float)$data->score_rm,
                        (float)$data->score_cd,
                        (float)$data->score_sd,
                        (float)$data->score_bois
                    ),
                    'category2' => array(
                        'Bus. Owner Exp.',
                        'Mgt. Team Exp.',
                        'Business Driver',
                        'Succession Plan',
                        'Future Growth'
                    ),
                    'series2' => array(
                        (float)$data->score_boe,
                        (float)$data->score_mte,
                        (float)$data->score_bdms,
                        (float)$data->score_sp,
                        (float)$data->score_ppfi
                    ),
                    'category3' => array(
                        'Financial Position',
                        'Financial Performance'
                    ),
                    'series3' => array(
                        (float)$data->score_rfp * 0.6,
                        (float)$data->score_rfpm * 0.4
                    )
                )
            );
        }

        // Industry VS SME
        if(Input::get('type')==4){
            $data = DB::table('tblentity')->select(DB::raw('
                tblentity.entityid,
                tblentity.companyname,
                tblreadyrationdata.gross_revenue_growth2,
                tblreadyrationdata.net_income_growth2,
                tblreadyrationdata.gross_profit_margin2,
                (tblreadyrationdata.current_ratio2 / 100) as current_ratio2,
                (tblreadyrationdata.debt_equity_ratio2 / 100) as debt_equity_ratio2,
                tblindustry_main.gross_revenue_growth,
                tblindustry_main.net_income_growth,
                tblindustry_main.gross_profit_margin,
                (tblindustry_main.current_ratio / 100) as current_ratio,
                (tblindustry_main.debt_equity_ratio / 100) as debt_equity_ratio,

                (tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) as bgroup,
                (tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) as mgroup,
                tblsmescore.score_facs as fgroup,
                CAST(tblsmescore.score_sf AS DECIMAL) as creditbpo_rating,
                tblentity.industry_main_id
            '))
            ->leftJoin('tblindustry_main', 'tblentity.industry_main_id', '=', 'tblindustry_main.industry_main_id')
            ->leftJoin('tblindustry_sub', 'tblentity.industry_sub_id', '=', 'tblindustry_sub.industry_sub_id')
            ->leftJoin('tblindustry_row', 'tblentity.industry_row_id', '=', 'tblindustry_row.industry_row_id')
            ->leftJoin('tblreadyrationdata', 'tblentity.entityid', '=', 'tblreadyrationdata.entityid')
            ->leftJoin('tblsmescore', 'tblentity.entityid', '=', 'tblsmescore.entityid')
            ->leftJoin('zipcodes', 'zipcodes.id', '=', 'tblentity.cityid')
			->leftJoin('refcitymun', 'tblentity.cityid', '=', 'refcitymun.id')
            ->where('tblentity.industry_row_id', '!=', 0)->where('tblentity.status', 7)
            ->where('tblreadyrationdata.gross_revenue_growth2', '!=', 'NULL');

            //filters
            if(@count($sme_selected)>0){
                $data->whereIn('tblentity.entityid', $sme_selected);
            }
            if($main_industry_selected!=""){
                $data->where('tblentity.industry_main_id', $main_industry_selected);
            }
            if($sub_industry_selected!=""){
                $data->where('tblentity.industry_sub_id', $sub_industry_selected);
            }
            if($industry_selected!=""){
                $data->where('tblentity.industry_row_id', $industry_selected);
            }
            if(Input::get('bcc_from')!="" && Input::get('bcc_to')!=""){
                $data->whereRaw('(tblsmescore.score_rm + tblsmescore.score_cd + tblsmescore.score_sd + tblsmescore.score_bois) between ' . Input::get('bcc_from') . ' and ' . Input::get('bcc_to'));
            }
            if(Input::get('mq_from')!="" && Input::get('mq_to')!=""){
                $data->whereRaw('(tblsmescore.score_boe + tblsmescore.score_mte + tblsmescore.score_bdms + tblsmescore.score_sp + tblsmescore.score_ppfi) between ' . Input::get('mq_from') . ' and ' . Input::get('mq_to'));
            }
            if(Input::get('fc_from')!="" && Input::get('fc_to')!=""){
                $data->whereRaw('tblsmescore.score_facs between ' . Input::get('fc_from') . ' and ' . Input::get('fc_to'));
            }
            if(Input::get('rating_from')!="" && Input::get('rating_to')!=""){
                $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . Input::get('rating_from') . ' and ' . Input::get('rating_to'));
            }
            $data->whereRaw('CAST(tblsmescore.score_sf AS DECIMAL) between ' . $pdfrom . ' and ' . $pdto);
            if($region_selected!=""){
				$data->where('refcitymun.regDesc', $region_selected);
            }

            $data = $data->get();

            // bank rating computation
            if(Input::get('brating_from')!="" && Input::get('brating_to')!=""){
                foreach($data as $k=>$d){
                    $bank_group = BankIndustry::where('bank_id', $d->current_bank)
                            ->where('industry_id', $d->industry_main_id)->first();
                    $bank_formula = BankFormula::where('bank_id', $d->current_bank)->first();

                    if($bank_group){
                        if($bank_formula){
                            //bcc
                            $bgroup_array = explode(',', $bank_formula->bcc);
                            $bgroup_item = 0;
                            $bgroup_total = 0;
                            if(in_array('rm', $bgroup_array)) {
                                $bgroup_item += $d->score_rm;
                                $bgroup_total += MAX_RM;
                            }
                            if(in_array('cd', $bgroup_array)) {
                                $bgroup_item += $d->score_cd;
                                $bgroup_total += MAX_CD;
                            }
                            if(in_array('sd', $bgroup_array)) {
                                $bgroup_item += $d->score_sd;
                                $bgroup_total += MAX_SD;
                            }
                            if(in_array('boi', $bgroup_array)) {
                                $bgroup_item += $d->score_bois;
                                $bgroup_total += MAX_BOI;
                            }
                            $bgroup = $bgroup_item * ( MAX_RM + MAX_CD + MAX_SD + MAX_BOI ) / $bgroup_total;

                            //mq
                            $mgroup_array = explode(',', $bank_formula->mq);
                            $mgroup_item = 0;
                            $mgroup_total = 0;
                            if(in_array('boe', $mgroup_array)) {
                                $mgroup_item += $d->score_boe;
                                $mgroup_total += MAX_BOE;
                            }
                            if(in_array('mte', $mgroup_array)) {
                                $mgroup_item += $d->score_mte;
                                $mgroup_total += MAX_MTE;
                            }
                            if(in_array('bd', $mgroup_array)) {
                                $mgroup_item += $d->score_bdms;
                                $mgroup_total += MAX_BD;
                            }
                            if(in_array('sp', $mgroup_array)) {
                                $mgroup_item += $d->score_sp;
                                $mgroup_total += MAX_SP;
                            }
                            if(in_array('ppfi', $mgroup_array)) {
                                $mgroup_item += $d->score_ppfi;
                                $mgroup_total += MAX_PPFI;
                            }
                            $mgroup = $mgroup_item * ( MAX_BOE + MAX_MTE + MAX_BD + MAX_SP + MAX_PPFI ) / $mgroup_total;

                            //fa
                            $fgroup_array = explode(',', $bank_formula->fa);
                            if(@count($fgroup_array) == 2){
                                $fgroup = $d->score_facs;
                            } else {
                                if($bank_formula->fa == 'fp'){
                                    $fgroup = $d->score_rfp;
                                } elseif($bank_formula->fa == 'fr'){
                                    $fgroup = $d->score_rfpm;
                                } else {
                                    $fgroup = 0;
                                }
                            }
                        } else {
                            $bgroup = ( $d->score_rm + $d->score_cd + $d->score_sd + $d->score_bois );
                            $mgroup = ( $d->score_boe + $d->score_mte + $d->score_bdms + $d->score_sp + $d->score_ppfi );
                            $fgroup = $d->score_facs;
                        }
                        $gscore = ($bgroup * ( $bank_group->business_group / 100 )) + ($mgroup * ( $bank_group->management_group / 100 )) + ($fgroup * ( $bank_group->financial_group / 100 ));

                        if($bank_formula && $bank_formula->boost == 0){
                            // no boost
                        } else {
                            //boost
                            if($bgroup >= 191 && $mgroup >= 125 && $fgroup >= 150) $gscore = $gscore * 1.05;
                            if($bgroup <= 190 && $bgroup >= 81 && $mgroup <= 124 && $mgroup >= 51 && $fgroup <= 149 && $fgroup >= 96) $gscore = $gscore * 1.05;
                            if($bgroup <= 80 && $mgroup <= 50 && $fgroup <= 95) $gscore = $gscore * 0.95;
                        }
                    } else {
                        $gscore = $d->creditbpo_rating;
                    }
                    if(($gscore < (int)Input::get('brating_from')) || ($gscore > (int)Input::get('brating_to'))){
                        unset($data[$k]); //remove from list
                    }
                }
            }

            $selector = Input::get('selector', 1);
            $chart_title = '';
            $chart_data = array();
            $chart_name = array();
            $lowest_standard = 0;
            $highest_standard = 0;

            // select tpye of chart
            switch($selector){
                case 1:
                    $chart_title = 'Gross Revenue Growth';
                    foreach($data as $d){
                        $chart_name[] = $d->companyname;
                        $chart_data[] = array((float)$d->gross_revenue_growth, (float)$d->gross_revenue_growth2);
                        $highest_standard = ((float)$d->gross_revenue_growth > $highest_standard) ? (float)$d->gross_revenue_growth : $highest_standard;
                        $lowest_standard = ((float)$d->gross_revenue_growth < $lowest_standard) ? (float)$d->gross_revenue_growth : $lowest_standard;
                    }
                    break;
                case 2:
                    $chart_title = 'Net Income Growth';
                    foreach($data as $d){
                        $chart_name[] = $d->companyname;
                        $chart_data[] = array((float)$d->net_income_growth, (float)$d->net_income_growth2);
                        $highest_standard = ((float)$d->net_income_growth > $highest_standard) ? (float)$d->net_income_growth : $highest_standard;
                        $lowest_standard = ((float)$d->net_income_growth < $lowest_standard) ? (float)$d->net_income_growth : $lowest_standard;
                    }
                    break;
                case 3:
                    $chart_title = 'Gross Profit Margin';
                    foreach($data as $d){
                        $chart_name[] = $d->companyname;
                        $chart_data[] = array((float)$d->gross_profit_margin, (float)$d->gross_profit_margin2);
                        $highest_standard = ((float)$d->gross_profit_margin > $highest_standard) ? (float)$d->gross_profit_margin : $highest_standard;
                        $lowest_standard = ((float)$d->gross_profit_margin < $lowest_standard) ? (float)$d->gross_profit_margin : $lowest_standard;
                    }
                    break;
                case 4:
                    $chart_title = 'Current Ratio';
                    foreach($data as $d){
                        $chart_name[] = $d->companyname;
                        $chart_data[] = array((float)$d->current_ratio, (float)$d->current_ratio2);
                        $highest_standard = ((float)$d->current_ratio > $highest_standard) ? (float)$d->current_ratio : $highest_standard;
                        $lowest_standard = ((float)$d->current_ratio < $lowest_standard) ? (float)$d->current_ratio : $lowest_standard;
                    }
                    break;
                case 5:
                    $chart_title = 'Debt-to-Equity Ratio';
                    foreach($data as $d){
                        $chart_name[] = $d->companyname;
                        $chart_data[] = array((float)$d->debt_equity_ratio, (float)$d->debt_equity_ratio2);
                        $highest_standard = ((float)$d->debt_equity_ratio > $highest_standard) ? (float)$d->debt_equity_ratio : $highest_standard;
                        $lowest_standard = ((float)$d->debt_equity_ratio < $lowest_standard) ? (float)$d->debt_equity_ratio : $lowest_standard;
                    }
                    break;
            }

            return array(
                'type' => 4,
                'selector' => $selector,
                'data' => array(
                    'title' => $chart_title,
                    'data' => $chart_data,
                    'name' => $chart_name,
                    'high' => array($highest_standard, $highest_standard),
                    'low' => array($lowest_standard, $lowest_standard)
                )
            );
        }

        return null;
    }

    public function getScoreAverage($score, $total_count) {
        $fscore_avg     = 0;
        $fscore_conv    = STR_EMPTY;

        if (0 != $total_count) {
            $fscore_avg   = $score / $total_count;

            if($fscore_avg >= 177) {
                $fscore_conv   = 'AA';
            }
            else if ($fscore_avg >= 150 && $fscore_avg <= 176) {
                $fscore_conv   = 'A';
            }
            else if ($fscore_avg >= 123 && $fscore_avg <= 149) {
                $fscore_conv   = 'BBB';
            }
            else if ($fscore_avg >= 96 && $fscore_avg <= 122) {
                $fscore_conv   = 'BB';
            }
            else if ($fscore_avg >= 69 && $fscore_avg <= 95) {
                $fscore_conv   = 'B';
            }
            else if ($fscore_avg < 69){
                $fscore_conv   = 'CCC';
            }
        }

        return $fscore_conv;
    }
}
