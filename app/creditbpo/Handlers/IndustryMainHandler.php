<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\IndustryMain;

class IndustryMainHandler {
    protected $model;

    /**
     * @param IndustryMain|null $model
     */
    public function __construct(IndustryMain $model = null) {
        $this->model = $model ?: new IndustryMain();
    }

    /**
     * @return array
     */
    public function getAll() {
        return $this->model;
    }

    public function getAllLists() {
        return $this->getAll()->pluck('main_title', 'main_code');
    }
}
