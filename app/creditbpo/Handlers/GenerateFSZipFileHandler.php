<?php

namespace CreditBPO\Handlers;

use CreditBPO\Services\ComplexFsInput;
use CreditBPO\Services\SimpleFsInput;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Models\Document;
use CreditBPO\Models\Entity;
use ZipArchive;	
use DB;
use \Exception as Exception;
use FinancialReport;

class GenerateFSZipFileHandler
{
	const FS_INPUT_COMPLEX = 'files/FS Input Template - Complex Output.xlsx';

	public static function generateFSZipFile($entityid)
    {
        $entity = Entity::where('entityid',$entityid)->first();
       
        $copyfile = $entity->entityid . ' - ' .$entity->companyname . '.xlsx';
        $copyfileupper = $entity->entityid . ' - ' .strtoupper($entity->companyname) . '.xlsx';
    	$basicFile = 'Basic FS Input Template - ' .$entity->companyname . ' as of '.date('Y-m-d').'.xlsx';
    	$fullFile = 'Full FS Input Template - ' .$entity->companyname . ' as of '.date('Y-m-d').'.xlsx';
    	$fsFile = public_path("documents/innodata/from_innodata/".$copyfile);
    	$fsFileUpper = public_path("documents/innodata/from_innodata/".$copyfileupper);
    	$basicFsFile = public_path("temp/".$basicFile);
    	$fullFsFile = public_path("temp/".$fullFile);
    	
    	
    	if(!file_exists($fsFile) && !file_exists($fsFileUpper)){
    	
    		$fsFile = public_path("documents/innodata/from_innodata/".$copyfile);

    		if(!file_exists($fsFile)){

    			$fsFile = public_path("documents/innodata/from_innodata/".$entity->entityid."/".$copyfile);
    		}

    	}

    	if(!file_exists($fsFile) && file_exists($fsFileUpper)){
    		$fsFile = $fsFileUpper;

    		if(!file_exists($fsFileUpper)){

    			$fsFile = public_path("documents/innodata/from_innodata/".$copyfileupper);
    		}

    		if(!file_exists($fsFileUpper)){

    			$fsFile = public_path("documents/innodata/from_innodata/".$entity->entityid."/".$copyfileupper);
    		}
    	}

    	$destinationPath = public_path('documents').'/';
				

    	if(file_exists($fsFile)){
    		@unlink($basicFsFile);
    		@copy($fsFile,$basicFsFile);

    		$zip = new ZipArchive();
        	$fsZip = $entity->entityid.'_-_Balance_Sheet_Income_Statement-'.date('Y-m-d').'.zip';
        	$fsZipFile = public_path("documents/".$fsZip);
        	$zip->open($fsZipFile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);

            $industryId = DB::table('tblindustry_main')->where('main_code', $entity->industry_main_id)->pluck('industry_main_id')->first();

            $simpleFsData = [
                'entity_id' => $entity->entityid,
                'money_factor' => 1,
                'currency' => 'PHP',
                'industry_id' => $industryId,
                'country_code'	=> $entity->country_code
            ];

            $simpleFsInput = new SimpleFsInput($basicFsFile, $simpleFsData);
            $result = $simpleFsInput->save();

        	$zip_filename = $entity->entityid.'_-_Balance_Sheet_Income_Statement-'.date('Y-m-d').'.zip';

            $complexFsInput = new ComplexFsInput(
                public_path(self::FS_INPUT_COMPLEX),
                ['entity_id' => $entity->entityid]
            );

            $complexFsInput->setDocumentGenerated(
                $simpleFsInput->getDocumentGenerated()
            );

            $result = $complexFsInput->write($simpleFsInput->getCellValues());

            if ($result) {

                $document_orig = $simpleFsInput->getDocumentFileName();
                $file = $complexFsInput->getDocumentFileName();
                $assets = $simpleFsInput->getTotalAssets();
                $srv_resp['total_asset'] = number_format($assets, 2);
                $srv_resp['total_asset_grouping'] = AssetGrouping::getByAmount($assets);
                
                $cond = array(
                    "is_deleted" => 0,
                    "entity_id" => $entity->entityid,
                    "document_group" => 21
                );
                $curDocs = Document::where($cond)->get();
                foreach ($curDocs as $key => $value) {
                    $value->is_deleted = 1;
                    $value->save;
                    Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
                }
               

                /* create entry on database */
                $data = [
                    'entity_id' => $entity->entityid,
                    'document_type'=> 3,
                    'upload_type'=> 5,
                    'document_group'=> 21,
                    'document_orig'=> $file,
                    'document_rename'=> $zip_filename,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                ];

                $documentid = DB::table('tbldocument')->insertGetId($data);

                $master_ids[]       = $documentid;
                $filenames[]        = $file;
                $display_names[]    = $file;
                $where = array(
                    "entity_id" => $entity->entityid,
                    "is_deleted" => 0
                );
                $fs = FinancialReport::where($where)->first();
                if($fs->count() >= 1) {
                    $srv_resp['fsid'] = $fs->id;
                }

                $zip->addFile(public_path('temp/'.$file), $file);


                // create entry on database
                $data = array(
                    'entity_id'=>$entity->entityid,
                    'document_type'=> 3,
                    'upload_type'=> 5,
                    'document_group'=> 21,
                    'document_orig'=>$basicFile,
                    'document_rename'=>$zip_filename,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                );
                
                $documentid = DB::table('tbldocument')->insertGetId($data);

                $zip->addFile($basicFsFile, $basicFile); // add the upload file to zip file

                $zip->close();	                    
            }else{
            	echo 'test'; exit;
            }
          
            
        	$fs_template = DB::table('tbldocument')
            ->where('entity_id', '=', $entity->entityid)
            ->where('document_group', '=', 21)
            ->where('upload_type', '=', 5)
            ->where('is_deleted', 0)
            ->orderBy('document_type', 'ASC')
            ->get();

            return $fs_template;
    	}

    	return [];
    }

   //------------------------------------------------------------------
    //  Function 60.1: __construct
    //       GenerateInnodataTextFilenamesHandler Initialization
    //------------------------------------------------------------------
    // public static function generateFSZipFile($entityid)
    // {
    //     $entity = Entity::where('entityid',$entityid)->first();
       
    //     $copyfile = $entity->entityid . ' - ' .$entity->companyname . '.xlsx';
    //     $copyfileupper = $entity->entityid . ' - ' .strtoupper($entity->companyname) . '.xlsx';
    // 	$basicFile = 'Basic FS Input Template - ' .$entity->companyname . ' as of '.date('Y-m-d').'.xlsx';
    // 	$fullFile = 'Full FS Input Template - ' .$entity->companyname . ' as of '.date('Y-m-d').'.xlsx';
    // 	$fsFile = public_path("documents/innodata/from_innodata/".$copyfile);
    // 	$fsFileUpper = public_path("documents/innodata/from_innodata/".$copyfileupper);
    // 	$basicFsFile = public_path("temp/".$basicFile);
    // 	$fullFsFile = public_path("temp/".$fullFile);
    	
    	
    // 	if(!file_exists($fsFile) && !file_exists($fsFileUpper)){
    	
    // 		$fsFile = public_path("documents/innodata/from_innodata/".$copyfile);

    // 		if(!file_exists($fsFile)){

    // 			$fsFile = public_path("documents/innodata/from_innodata/".$entity->entityid."/".$copyfile);
    // 		}

    // 	}

    // 	if(!file_exists($fsFile) && file_exists($fsFileUpper)){
    // 		$fsFile = $fsFileUpper;

    // 		if(!file_exists($fsFileUpper)){

    // 			$fsFile = public_path("documents/innodata/from_innodata/".$copyfileupper);
    // 		}

    // 		if(!file_exists($fsFileUpper)){

    // 			$fsFile = public_path("documents/innodata/from_innodata/".$entity->entityid."/".$copyfileupper);
    // 		}
    // 	}

    // 	if(file_exists($fsFile)){
    // 		@unlink($basicFsFile);
    // 		@copy($fsFile,$basicFsFile);
    		
    // 		@unlink($fullFsFile);
    // 		@copy($fsFile,$fullFsFile);
    		
    //     	$zip = new ZipArchive();
    //     	$fsZip = $entity->entityid.'_-_Balance_Sheet_Income_Statement-'.date('Y-m-d').'.zip';
    //     	$fsZipFile = public_path("documents/".$fsZip);

    //     	$basicFsFile = (string) $basicFsFile;
    //     	$fullFsFile = (string) $fullFsFile;
    //     	$zip->open($fsZipFile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
    //     	$zip->addFile($basicFsFile,$basicFile);
    //     	$zip->addFile($fullFsFile,$fullFile);
    //     	$zip->close();
    		
    // 		DB::table('tbldocument')->where(['entity_id'=>$entity->entityid,'document_group' => 21])->update(['is_deleted' => 1]);
    //     	$fsDocuments = [
    //     		[
	   //      		'entity_id' => $entity->entityid,
	   //      		'is_origin' => 0,
	   //      		'document_type' => 1,
	   //      		'document_group' => 21,
	   //      		'document_orig' => $basicFile,
	   //      		'document_rename' => $fsZip,
	   //      		'created_at' => date('Y-m-d H:i:s'),
	   //      		'updated_at' => date('Y-m-d H:i:s'),
	   //      		'upload_type' => 5,
	   //      		'is_valid' => 0,
	   //      		'is_deleted' => 0
    //     		],
    //     		[
	   //      		'entity_id' => $entity->entityid,
	   //      		'is_origin' => 0,
	   //      		'document_type' => 1,
	   //      		'document_group' => 21,
	   //      		'document_orig' => $fullFile,
	   //      		'document_rename' => $fsZip,
	   //      		'created_at' => date('Y-m-d H:i:s'),
	   //      		'updated_at' => date('Y-m-d H:i:s'),
	   //      		'upload_type' => 5,
	   //      		'is_valid' => 0,
	   //      		'is_deleted' => 0
    //     		]
    //     	];

    //     	DB::table('tbldocument')->insert($fsDocuments);

    //     	$fs_template = DB::table('tbldocument')
    //         ->where('entity_id', '=', $entity->entityid)
    //         ->where('document_group', '=', 21)
    //         ->where('upload_type', '=', 5)
    //         ->where('is_deleted', 0)
    //         ->orderBy('document_type', 'ASC')
    //         ->get();

    //         return $fs_template;

    // 	}

    // 	return [];
    // }

    public static function getFilesFromInno($entityid){
    	$entity = Entity::where('entityid', $entityid)->first();
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") === "Production"){
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$entityid);
        }else{
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'.$entityid);
        }

        if (!$fileExists) {
            return false;
        }

        $localInnodataPath = public_path('documents/innodata/from_innodata/');
        $localPath = public_path('documents/innodata/from_innodata/'.$entityid.'/');
        if (!file_exists(public_path('documents/innodata/'))) {
            mkdir(public_path('documents/innodata/',0777));
        }
        
        if (!file_exists($localInnodataPath)) {
            mkdir($localInnodataPath,0777);
        }

        if (!file_exists($localPath)) {
            mkdir($localPath,0777);
        }

        if(env("APP_ENV") == "Production"){
            $dir = opendir('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$entityid);
            $remoteDir = '/FROM_INNO/'.$entityid.'/';
        }else{
            $dir = opendir('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'.$entityid);
            $remoteDir = '/TEST_FOLDER/_From Innodata/'.$entityid.'/';
        }

        $files = array();
        while (false !== ($file = readdir($dir)))
        {
            if ($file == "." || $file == "..")
                continue;
            $files[] = $file;
        }

        try {
            $isInnoFile = false;
            foreach ($files as $file)
            {
                $filename_orig_upper = $entity->entityid . ' - ' .strtoupper($entity->companyname) . '.xlsx';
                $filename_orig_uc = $entity->entityid . ' - ' .ucwords($entity->companyname) . '.xlsx';
                $filename_orig_lower = $entity->entityid . ' - ' .strtolower($entity->companyname) . '.xlsx';
                $filename_orig_origin = $entity->entityid . ' - ' . $entity->companyname . '.xlsx';

                if(($filename_orig_origin == $file) || ($filename_orig_upper == $file) || ($filename_orig_lower == $file) || ($filename_orig_uc == $file) ){
                    $isInnoFile = true;
                    $copyfile = $file;
                    
                    if (!$remote = @fopen("ssh2.sftp://{$sftp}/{$remoteDir}{$file}", 'r'))
                    {
                        @fclose($remote);
                        return;
                    }
        
                    if (!$local = @fopen($localPath . $file, 'w'))
                    {
                        @fclose($local);
                        return;
                    }
        
                    $read = 0;
                    $filesize = filesize("ssh2.sftp://{$sftp}/{$remoteDir}{$file}");
                    while ($read < $filesize && ($buffer = fread($remote, $filesize - $read)))
                    {
                        $read += strlen($buffer);
                        if (fwrite($local, $buffer) === FALSE)
                        {
                            return;
                        }
                    }
                    fclose($local);
                    fclose($remote);
                }
            }

            /** Check inno folder if empty */
            if((count($files) == 0) && $isInnoFile == false){
                return;
            }

            /** Check if inno folder has file but file name format is wrong */
            if($files && $isInnoFile == false){
                $err_message = "File Format should be '" . $entity->entityid . " - " .$entity->companyname . ".xlsx'";
                return $err_message;
            }

        } catch (Exception $e) {
            error_log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }
   
}
