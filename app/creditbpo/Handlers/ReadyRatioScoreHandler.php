<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\ReadyRatioScore;

class ReadyRatioScoreHandler {
    const SCORE_LOWEST = 4;

    protected $model;

    /**
     * @param MajorCustomerAccount $model
     */
    public function __construct(ReadyRatioScore $model = null) {
        $this->model = $model ?: new ReadyRatioScore();
    }

    public function getValueByRange($scoreRange) {
        $result = $this->model->getByScoreRange($scoreRange);

        if (@count($result) > 0) {
            return $result->score_value;
        }

        return self::SCORE_LOWEST;
    }

    public function getClosestRatioByScoreRange($scoreRange) {
        $result = $this->model->getClosestByScoreRange($scoreRange);

        if (@count($result) > 0) {
            return $result;
        }

        return null;
    }
}
