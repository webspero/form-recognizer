<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\MajorCustomerAccount;

class MajorCustomerAccountHandler {
    protected $model;

    /**
     * @param MajorCustomerAccount $model
     */
    public function __construct(MajorCustomerAccount $model = null) {
        $this->model = $model ?: new MajorCustomerAccount();
    }

}
