<?php

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Config;
use CreditBPO\Models\RiskAssessment;

class RiskAssessmentHandler {
    const BUSINESS_CONSIDERATION_SAFE = 30;
    const BUSINESS_CONSIDERATION_THREAT = 90;

    protected $config;
    protected $model;

    /**
     * @param RiskAssessment $model
     */
    public function __construct(RiskAssessment $model = null) {
        $this->model = $model ?: new RiskAssessment();
        $this->config = Config::get('handlers/risk-assessment');
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getRiskManagementScoreByEntityId($entityId) {
        $totalRiskAssessment = @count($this->model->getByEntityId($entityId));

        if ($totalRiskAssessment <= 0) {
            return self::BUSINESS_CONSIDERATION_SAFE;
        }

        $assessments = $this->model->getForCountByEntityId($entityId);

        if (@count($assessments) <= 0) {
            return self::BUSINESS_CONSIDERATION_SAFE;
        }

        $score = 0;

        foreach ($assessments as $assessment) {
            $score += $this->getManagementScore($assessment);
        }

        if (round($score / $totalRiskAssessment * 100) >= 60) {
            return self::BUSINESS_CONSIDERATION_THREAT;
        }

        return self::BUSINESS_CONSIDERATION_SAFE;
    }

    protected function getManagementScore($assessment) {
        foreach ($this->config['management_scoring'] as $scoring) {
            if (
                $assessment->risk_assessment_name === $scoring['name'] &&
                $assessment->risk_assessment_solution === $scoring['solution']
            ) {
                return $scoring['score'];
            }
        }

        return 0;
    }
}
