<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\MajorSupplier;

class MajorSupplierHandler {
    const RELATIONSHIP_SATISFACTION_GOOD = 1;
    const RELATIONSHIP_SATISFACTION_SATISFACTORY = 2;
    const RELATIONSHIP_SATISFACTION_UNSATISFACTORY = 3;
    const RELATIONSHIP_SATISFACTION_BAD = 4;
    const SCORE_LOW_DEPENDENCE = 30;
    const SCORE_MEDIUM_DEPENDENCE = 20;
    const SCORE_HIGH_DEPENDENCE = 10;

    protected $model;

    /**
     * @param MajorSupplier $model
     */
    public function __construct(MajorSupplier $model = null) {
        $this->model = $model ?: new MajorSupplier();
    }

    public static function getAllRelationshipSatisfaction() {
        return [
            self::RELATIONSHIP_SATISFACTION_GOOD =>
                trans('business_details2.ms_relation_good'),
            self::RELATIONSHIP_SATISFACTION_SATISFACTORY =>
                trans('business_details2.ms_relation_satisfactory'),
            self::RELATIONSHIP_SATISFACTION_UNSATISFACTORY =>
                trans('business_details2.ms_relation_unsatisfactory'),
            self::RELATIONSHIP_SATISFACTION_BAD =>
                trans('business_details2.ms_relation_bad'),
        ];
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getAssessmentScoreByEntityId($entityId) {
        $score = $this->getMaxScoreByEntityId($entityId);

        if ($score >= 70) {
            return self::SCORE_HIGH_DEPENDENCE;
        }

        if ($score > 30) {
            return self::SCORE_MEDIUM_DEPENDENCE;
        }

        return self::SCORE_LOW_DEPENDENCE;
    }

    /**
     * @param int $entityId
     * @return int
     */
    protected function getMaxScoreByEntityId($entityId) {
        $majorSuppliers = $this->model->getByEntityId($entityId);

        if (@count($majorSuppliers) <= 0) {
            return 0;
        }

        $max = 0;

        foreach ($majorSuppliers as $supplier) {
            if ($supplier->supplier_share_sales > $max) {
                $max = $supplier->supplier_share_sales;
            }
        }

        return $max;
    }
}
