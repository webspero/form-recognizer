<?php

namespace CreditBPO\Handlers;

use CreditBPO\Services\Report\PDF\NetAssets as NetAssetsPDF;

class NetAssetsHandler {

    const DEFAULT_LOGIN_ID = 76;
    
    /**
     * @param int $financialReportId
     * @return bool
     */
    public function createNetAssetsReport($financialReportId) {
        $pdf = new NetAssetsPDF();
        $contents = $pdf->create($financialReportId)->generate();

        if ($contents) {
            $contents->setPaper('a4', 'portrait');
            $pdf->save();

            return $pdf->getFilenamePath($financialReportId);
        }
        
        return false;
    }

    /**
     * @param int $financialReportId
     * @return string|null
     */
    public function getSALReportPath($entityId) {
        $pdf = new SALPDF();
        return $pdf->getFilenamePath($entityId);
    }

}
