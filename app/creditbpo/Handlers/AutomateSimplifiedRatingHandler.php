<?php

namespace CreditBPO\Handlers;

use ActionController;
use CreditBPO\DTO\ReadyRatioDTO;
use CreditBPO\DTO\SMEScoringDTO;
use CreditBPO\Exceptions\FinancialAnalysisHandlerException;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Models\Entity;
use CreditBPO\Models\ReadyRatio;
use CreditBPO\Models\SMEScore;
use CreditBPO\Models\User;
use CreditBPO\Services\KeyRatioService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Scoring;
use \Exception as Exception;

class AutomateSimplifiedRatingHandler
{
    public function processReport($entityId) {
        $financialAnalysisHandler = new FinancialAnalysisHandler();
        $sme = $financialAnalysisHandler->getSMEScoringByEntityId($entityId);
        $readyRatio = $financialAnalysisHandler->getReadyRatioDataByEntityId($entityId);
        $this->simplifiedPostFinishScoring($entityId, $sme, $readyRatio);
        return true;    
    }

    public function simplifiedPostFinishScoring($entityId, SMEScoringDTO $sme, ReadyRatioDTO $readyRatio, $isAdminReview = false)
    {
        $smescore = new SMEScore();
        $smescore->loginid = $sme->getLoginId();
        $smescore->entityid = $sme->getEntityId();
        $smescore->score_rm = $sme->getRiskManagement();
        $smescore->score_cd = $sme->getCustomerDependency();
        $smescore->score_sd = $sme->getSupplierDependency();
        $smescore->score_bois = $sme->getBusinessOutlookIndexScore();
        $smescore->score_boe = $sme->getBusinessOwnerExperience();
        $smescore->score_mte = $sme->getManagementTeamExperience();
        $smescore->score_sp = $sme->getSuccessionPlan();
        $smescore->score_rfp = $sme->getFinancialPosition();
        $smescore->score_rfpm = $sme->getFinancialPerformance();
        $smescore->score_fars = $sme->getRawScore();
        $smescore->score_facs = $sme->getConvertedScore();
        $smescore->score_sf = $sme->getRiskScore();
        $smescore->average_daily_balance = $sme->getAverageDailyBalance();
        $smescore->operating_cashflow_margin = $sme->getOperatingCashFlowMargin();
        $smescore->operating_cashflow_ratio = $sme->getOperatingCashFlowRatio();
        $smescore->score_bdms = $sme->getMainDependency();
        $smescore->score_ppfi = $sme->getPastProjectFutureInitiatives();
        
        // Generate Good and Bad
        $keyRatioService    = new KeyRatioService();
        $summary_points     = $keyRatioService->generateKeyRatioByEntityId($entityId);
        $positive_points    = STR_EMPTY;
        $negative_points    = STR_EMPTY;
        $pos_count          = @count($summary_points['positive']);
        $neg_count          = @count($summary_points['negative']);
        $n_idx              = 0;
        $p_idx              = 0;
        foreach ($summary_points['positive'] as $pos_itm) {
            $p_idx++;
            $positive_points .= $pos_itm;
            if ($pos_count > $p_idx) {
                $positive_points .= '|';
            }
        }

        foreach ($summary_points['negative'] as $neg_itm) {
            $n_idx++;
            $negative_points .= $neg_itm;
            if ($neg_count > $n_idx) {
                $negative_points    .= '|';
            }
        }
        $smescore->positive_points  = $positive_points;
        $smescore->negative_points  = $negative_points;
        $smescore->save();

        $readyratiodata = new ReadyRatio();
        $readyratiodata->loginid = $readyRatio->getLoginId();
        $readyratiodata->entityid = $readyRatio->getEntityId();
        $readyratiodata->year1 = $readyRatio->getPreviousYear();
        $readyratiodata->year2 = $readyRatio->getCurrentYear();
        $readyratiodata->gross_revenue_growth1 = $readyRatio->getPreviousGrossRevenueGrowth();
        $readyratiodata->gross_revenue_growth2 = $readyRatio->getCurrentGrossRevenueGrowth();
        $readyratiodata->net_income_growth1 = $readyRatio->getPreviousNetIncomeGrowth();
        $readyratiodata->net_income_growth2 = $readyRatio->getCurrentNetIncomeGrowth();
        $readyratiodata->gross_profit_margin1 = $readyRatio->getPreviousGrossProfitMargin();
        $readyratiodata->gross_profit_margin2 = $readyRatio->getCurrentGrossProfitMargin();
        $readyratiodata->net_profit_margin1 = $readyRatio->getPreviousNetProfitMargin();
        $readyratiodata->net_profit_margin2 = $readyRatio->getCurrentNetProfitMargin();
        $readyratiodata->net_cash_margin1 = $readyRatio->getPreviousNetCashMargin();
        $readyratiodata->net_cash_margin2 = $readyRatio->getCurrentNetCashMargin();
        $readyratiodata->current_ratio1 = $readyRatio->getPreviousRatio();
        $readyratiodata->current_ratio2 = $readyRatio->getCurrentRatio();
        $readyratiodata->debt_equity_ratio1 = $readyRatio->getPreviousDebtEquityRatio();
        $readyratiodata->debt_equity_ratio2 = $readyRatio->getCurrentDebtEquityRatio();
        $readyratiodata->cc_year1 = $readyRatio->getCashConversionYearOne();
        $readyratiodata->cc_year2 = $readyRatio->getCashConversionYearTwo();
        $readyratiodata->cc_year3 = $readyRatio->getCashConversionYearThree();
        $readyratiodata->receivables_turnover1 = $readyRatio->getReceivablesTurnoverOne();
        $readyratiodata->receivables_turnover2 = $readyRatio->getReceivablesTurnoverTwo();
        $readyratiodata->receivables_turnover3 = $readyRatio->getReceivablesTurnoverThree();
        $readyratiodata->inventory_turnover1 = $readyRatio->getInventoryTurnoverOne();
        $readyratiodata->inventory_turnover2 = $readyRatio->getInventoryTurnoverTwo();
        $readyratiodata->inventory_turnover3 = $readyRatio->getInventoryTurnoverThree();
        $readyratiodata->accounts_payable_turnover1 = $readyRatio->getAccountsPayableTurnoverOne();
        $readyratiodata->accounts_payable_turnover2 = $readyRatio->getAccountsPayableTurnoverTwo();
        $readyratiodata->accounts_payable_turnover3 = $readyRatio->getAccountsPayableTurnoverThree();
        $readyratiodata->cash_conversion_cycle1 = $readyRatio->getCashConversionCycleOne();
        $readyratiodata->cash_conversion_cycle2 = $readyRatio->getCashConversionCycleTwo();
        $readyratiodata->cash_conversion_cycle3 = $readyRatio->getCashConversionCycleThree();
        $readyratiodata->save();

        $scoring = new Scoring();
        //$scoring->loginid = Auth::user()->loginid;
        $scoring->loginid = $sme->getLoginId();
        $scoring->entityid = $entityId;
        $scoring->start_date = date('Y-m-d');
        $scoring->end_date = date('Y-m-d');
        $scoring->save();

        Entity::where('entityid', '=', $entityId)->update([
            'status' => 7,
            'admin_review_flag' => $isAdminReview ? 1 : 0,
            'completed_date' => date('c'),
        ]);

        $actionController = new ActionController();
        $actionController->createSMEReport($entityId);
        // $this->simplifiedEmailPostFinishScoring($entityId);

        return true;
    }

    public function simplifiedEmailPostFinishScoring($entityId)
    {
        $entity = Entity::where('entityid', '=', $entityId)->first();
        $period = date('Y-m-d');
        $fname = $entity->companyname;
        if (strpos($fname, "&") !== false) {
            $fname  = preg_replace("/[&]/", "And", $fname);
        }
        $cbpoPdf = public_path('documents') . '/' . $entity->cbpo_pdf;

        if (!file_exists($cbpoPdf)) {
            $insert = array(
                "entity_id" => $entityId,
                "data" => "Rating report does not exist",
                "error" => "Rating report does not exist",
                "status" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            DB::table("automate_standalone_rating_logs")->insert($insert);
            throw new FinancialAnalysisHandlerException(
                'Rating report does not exist'
            );
        }
        $emailCc = array();
        $userBank = null;
        
        if ($entity->current_bank != 0) {
            $userBank  = User::where('tbllogin.bank_id', $entity->current_bank)
                ->leftJoin('tblbanksubscriptions as bs', "tbllogin.loginid", 'bs.bank_id')
                ->where('role', 4)
                ->where('parent_user_id', 0)
                ->whereRaw(" bs.expiry_date > NOW()")
                ->where('tbllogin.is_delete', 0)
                ->get()->toArray();
            if (empty($userBank)) {
                $userBank  = User::where('bank_id', $entity->current_bank)
                    ->where('role', 4)
                    ->where('parent_user_id', 0)
                    ->where('tbllogin.is_delete', 0)
                    ->get()->toArray();
            }
            foreach ($userBank as $key => $user) {
                $emailCc[] = $user['email'];
            }
        }

        $user = User::where('loginid', '=', $entity->loginid)->first();
        $emailTo = $user->email;
        if (env("APP_ENV") != "local") {

            try{
                // send email to supervisor
                Mail::send(
                    'emails.finishscoring',
                    [
                        'type' => '7'
                    ],
                    function ($message) use (
                        $entity,
                        $userBank,
                        $cbpoPdf,
                        $emailTo,
                        $emailCc
                    ) {
                        if ($userBank && !empty($emailCc)) {
                            $message->to($emailCc, $emailCc)
                                ->subject($entity->companyname . ' Rating Report Is Now Available!');
                            $message->attach($cbpoPdf);
                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                            return;
                        }

                        if ($emailCc) {
                            $message->to($emailCc, $emailCc)
                                ->subject('Your CreditBPO Rating Report Is Now Available!');
                            $message->attach($cbpoPdf);
                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                        }
                    }
                );
            }catch(Exception $e){
                //
            }

            try{
                // send notification to basic user
                Mail::send(
                    'emails.finish_scoring_user',
                    [
                        'type' => '7'
                    ],
                    function ($message) use (
                        $entity,
                        $userBank,
                        $emailTo,
                        $emailCc
                    ) {
                        if ($userBank) {
                            $message->to($emailTo, $emailTo)
                                ->subject($entity->companyname . ' Rating Report Is Now Available!');

                            if (!empty($emailCc)) {
                                $message->cc($emailCc);
                            }

                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                            return;
                        }

                        $message->to($emailTo, $emailTo)
                            ->subject('Your CreditBPO Rating Report Is Now Available!');
                        $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                        $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                    }
                );

            }catch(Exception $e){
                //
            }
        }
        return true;
    }
}