<?php

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Log;
use CreditBPO\Models\AutomateStandaloneRating;
use CreditBPO\Models\Entity;
use CreditBPO\Services\MailHandler;
use \Exception as Exception;

use InnodataFile;
use AdminSetting;

class InnodataUploadHandler
{
    protected $model;
    protected $success;
    protected $errors;

    /**
     * @param AutomateStandaloneRating $model
     */
    public function __construct(
        AutomateStandaloneRating $model = null
    ) {
        $this->model = $model ?: new AutomateStandaloneRating();
    }

    public function UploadFiles()
    {
        $admin_setting = AdminSetting::find(1);
        $allowPremiumReportProcess = (!empty($admin_setting)) ? $admin_setting->is_innodata_process_premium_reports : 2;
        $pendingReports = $this->model->getInnodataPendingReports($allowPremiumReportProcess);
        $reports = array();
        $jenkins_company = "XYZ [][][]";

        // Set retrieved reports to in progress immediately
        foreach ($pendingReports as $key => $pendingReport ) {
            /** Get company name of the report */
            $companyname = Entity::where('entityid', $pendingReport->entity_id)->pluck('companyname')->first();

            /** Halt process of report created from Jenkins */
            if($companyname == $jenkins_company){
                array_push($reports, $pendingReport->entity_id);
            }else{
                $pendingReport->status = AutomateStandaloneRating::STATUS_IN_PROGRESS;
                $pendingReport->save();
            }
        }

        // Process individually
        $result = false;
        $success = [];
        $failed = [];
        $error = 0;
        // Initalize MailHandleer
        $mailHandler = new MailHandler();
        foreach ($pendingReports as $pendingReport) {

            /** Stop process if jenkin files */
            if(in_array($pendingReport->entity_id, $reports)){
                continue;
            }
            $result = null;

            $entityModel = new Entity();
            $entity = $entityModel->find($pendingReport->entity_id);

            try {
                echo 'Processing ' . $pendingReport->entity_id .
                    ' - ' . $entity->companyname . "\n";

                $innodataFiles = InnodataFile::where('entity_id', $entity->entityid)->where('is_uploaded', 0)->where('is_deleted', 0)->get();
                
                if ($innodataFiles) {
                    $uploaded_files = [];
                    foreach($innodataFiles as $innodataFile) {
                        $result = null;
                        // $filepath = public_path('documents/innodata/to_innodata/'.$entity->entityid.'/'.$innodataFile->file_name);

                        $filenameTxt = $entity->entityid.' - '.$entity->companyname.'.txt';
                        
                        $filepath = public_path('documents/innodata/to_innodata/'.$entity->entityid.'/'.$innodataFile->file_name);

                        $filenameTxtPath = public_path('documents/innodata/to_innodata/'.$entity->entityid.'/'.$filenameTxt);

                        if(env("APP_ENV") ===  "Production"){
                            $remotePath = "/TO_INNO/".$entity->entityid."/".$innodataFile->file_name;
							$remoteFilenameTxtPath = "/TO_INNO/".$entity->entityid."/".$filenameTxt;


                            $ftpUpload = $this->uploadFTP($entity->entityid, $filepath, $remotePath);
                            $filenameFtpUpload = $this->uploadFTP($entity->entityid, $filenameTxtPath, $remoteFilenameTxtPath);
                        }
                        else{
                            $remotePath = "/TEST_FOLDER/_To Innodata/".$entity->entityid."/".$innodataFile->file_name;
                            $remoteFilenameTxtPath = "/TEST_FOLDER/_To Innodata/".$entity->entityid."/".$filenameTxt;
                            $ftpUpload = $this->uploadFTP($entity->entityid, $filepath, $remotePath);
                            $filenameFtpUpload = $this->uploadFTP($entity->entityid, $filenameTxtPath, $remoteFilenameTxtPath);
                        }

                        if (!empty($ftpUpload) && !empty($filenameFtpUpload)) {
                            $innodata_record = InnodataFile::where('id', $innodataFile->id)->first();
                            $innodata_record->is_uploaded = STS_OK;
                            $innodata_record->save();

                            $result = $ftpUpload;
                            // Add File In Temporary Array
                            $uploaded_files[] = $innodataFile->file_name;
                        } else {
                            $message = $ftpUpload;
                        break;
                        }
                    }
                    if (!empty($uploaded_files)) {
                        $remote_to_folder = "/TO_INNO/" . $entity->entityid;
                        $remote_from_folder = "/FROM_INNO/" . $entity->entityid;
                        $remote_file_name = $entity->entityid . " - " . $entity->companyname . ".xlsx";
                        // Send Notification Email To Innodata Recipient
                        $mailHandler->sendInnodataRecepientNotificationMail($uploaded_files, $remote_to_folder, $remote_from_folder, $remote_file_name);
                    }
                }
            } catch (Exception $e) {
                $this->logError($e);
                $message = $e->getMessage();
            }

            if ($result != null) {
                $pendingReport->status = AutomateStandaloneRating::STATUS_INNODATA_WAITING;
                echo 'Result: Success uploading to Innodata shared folder' . "\n\n";
            } else {
                $pendingReport->status = AutomateStandaloneRating::STATUS_ERROR;
                echo 'Result: Failed uploading to Innodata shared folder' . "\n\n";
            }

            $pendingReport->save();
        }
    }

    /**
     * @param Exception $error
     * @return void
     */
    protected function logError($error)
    {
        Log::error($error);
    }

    public function uploadFTP($entity_id, $local_file, $remote_file){
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);
        $fileExists = file_exists('ssh2.sftp://' . $sftp . '/TO_INNO/'.$entity_id);
        if (!$fileExists) {
            ssh2_sftp_mkdir($sftp, '/TO_INNO/'.$entity_id);
        }

        $sftpStream = @fopen('ssh2.sftp://'.$sftp.$remote_file, 'w');

        try {

            if (!$sftpStream) {
                throw new Exception("Could not open remote file: $remote_file");
            }
           
            $data_to_send = @file_get_contents($local_file);
           
            if ($data_to_send === false) {
                throw new Exception("Could not open local file: $local_file.");
            }
           
            if (@fwrite($sftpStream, $data_to_send) === false) {
                throw new Exception("Could not send data from file: $local_file.");
            }
           
            fclose($sftpStream);

            return 1;
                           
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            @fclose($sftpStream);
            return $e->getMessage();
        }
    }
}
