<?php

namespace CreditBPO\Handlers;

use Google\Cloud\Core\ServiceBuilder;
use CreditBPO\Models\Entity;
use CreditBPO\Models\KeyManager;
use CreditBPO\Models\Shareholder;
use CreditBPO\Models\Director;
use CreditBPO\Services\UpdateSmeReport;
use CreditBPO\Services\IbakusService;
use App;
use DB;
use Imagick;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;


class GenerateFinancialReportsHandler {
    protected $updateReport;
    
    public function __construct() {
    }

    public function checkFinancialReports($start = null, $end = null)
    {
        $entity = new Entity();
        if($start && $end) {
            $completedFR = $entity->whereBetween("entityid",[$start,$end])->where("cbpo_pdf", "!=", "")->orderBy("entityid","desc")->get();
        } elseif ($start) {
            $completedFR = $entity->where("entityid", ">=" ,$start)->where("cbpo_pdf", "!=", "")->orderBy("entityid","desc")->get();
        } elseif ($end) {
            $completedFR = $entity->where("entityid", $end)->where("cbpo_pdf", "!=", "")->orderBy("entityid","desc")->get();
            
        } else {
            $completedFR = $entity->where("cbpo_pdf", "!=", "")->orderBy("entityid","desc")->get();
        }
        $sme  = new UpdateSmeReport();
        foreach ($completedFR as $fr) {
            echo $fr->entityid;
            echo "\n";
            $record =  Entity::find($fr->entityid);
            $entity->where("entityid",$fr->entityid)->update(array("cbpo_pdf"=>"","pdf_grdp"=>"","negative_findings"=>""));
            if($record->is_premium == PREMIUM_REPORT) {
                // $ibakus = new IbakusService();
                // $ibakus->getRelatedPersonalities($fr->entityid);
                $this->crossmatchOpensanction($record);
            }

            $this->detectDueDateUtilityBill($record);
            if($entity->premium == PREMIUM_REPORT){
                AutomateStandaloneRatingHandler::cmapSearch($entity->companyname);
            }

            if ($entity->pse_company_id !== null) {
                AutomateStandaloneRatingHandler::cmapSearchOfficers($entity->companyname, $entity->pse_company_id);
            }

            $sme->updateExistingReport($fr->entityid,true);
            
            sleep(5);

            echo "\n";
            echo $fr->companyname . " is complete.";
            echo "\n";
            
        }
    }

     //-----------------------------------------------------
    //  Function 11.2: crossmatchOpensanction
    //  Description: crossmatch function for opensanctions
    //-----------------------------------------------------
	public function crossmatchOpensanction($entity){
        // Fetch personalities that need to be crossmatched.
        $keyManagers = KeyManager::select("firstname" , "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();

        $shareHolders = Shareholder::select("name", "firstname", "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();
        $directors = Director::select("name", "firstname", "middlename", "lastname", "nationality")
                        ->where('is_deleted',0)
                        ->where('entity_id', $entity->entityid)
                        ->get();

        // start crossmatch
        $lists = array();
        foreach ($keyManagers as $key) {
            // get nationality code
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $key->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($key->firstname), 
                "lastname" => ($key->middlename)? strtolower($key->middlename .  " " . $key->lastname): strtolower($key->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            $name = $data['firstname'] . " " . $data['lastname'];
            if(!in_array(strtolower($name), $lists)) { 
                $lists[] = strtolower($name);
                $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
            }
        }

        foreach ($shareHolders as $key => $share) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $share->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($share->firstname), 
                "lastname" => ($share->middlename)? strtolower($share->middlename .  " " . $share->lastname): strtolower($share->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            if($data['firstname']) {
                $name = $data['firstname'] . " " . $data['lastname'];
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
                }
            } else {
                $name = strtolower($share['name']);
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByName($name, $entity->entityid, $nationality);
                }
            }
        }
        foreach ($directors as $key => $dir) {
            $ncode = DB::table("countries")->select("country_code")->where("enNationality", $dir->nationality)->first();
            $nationality="";
            $data = array(
                "firstname" => strtolower($dir->firstname), 
                "lastname" => ($dir->middlename)? strtolower($dir->middlename .  " " . $dir->lastname): strtolower($dir->lastname)
            );
            if($ncode) {
                $nationality = $ncode->country_code;
            } 
            if($data['firstname']) {
                $name = $data['firstname'] . " " . $data['lastname'];
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByFirstAndLastname($data, $entity->entityid, $nationality);
                }
            } else {
                $name = strtolower($dir['name']);
                if(!in_array(strtolower($name), $lists)) { 
                    $lists[] = strtolower($name);
                    $charges = $this->findByName($name, $entity->entityid, $nationality);
                }
            }
        }
        return;
    }
    
     //-----------------------------------------------------
    //  Function 11.3: findByFirstAndLastname
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByFirstAndLastname($data,$entity_id, $nationality) {
        $entity = Entity::where("entityid", $entity_id)->first();
        $records = DB::table("open_sanctions")->select("charges")->where($data);

        if($nationality) {
            $records->where("nationality", "like", "%{$nationality}%");
        }

        $records = $records->get();
        if($records->count() > 0) {
            $name = ucwords($data['firstname'] . " " . $data['lastname']);
            $charges = "<tr><td> {$name} </td><td><ul>";
            if ($entity->negative_findings == "") {
                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
            } else {
                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
            }
            foreach ($records as $key => $value) {
                if ($value->charges) {
                    $charges .= "<li> " . $value->charges . " </li>";
                }
            }
            $charges .= "</ul></td></tr></tbody></table>";
            // update negative findings
            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
        }
        return;
    }

    //-----------------------------------------------------
    //  Function 11.4: findByName
    //  Description: start cross match using parameter
    //-----------------------------------------------------
    private function findByName($name,$entity_id, $nationality) {
        $entity = Entity::where("entityid", $entity_id)->first();
        $name = strtolower($name);
        if($nationality) {
            $nationality = " AND nationality LIKE '%{$nationality}%'";
        }
        $records = DB::select("SELECT charges FROM open_sanctions WHERE CONCAT(firstname, ' ', lastname) = '{$name}' {$nationality}");
        
        if(!empty($records)) {
            $name = ucwords($name);
            $charges = "<tr><td> {$name} </td><td><ul>";
            if ($entity->negative_findings == "") {
                $charges = "<table><thead><tr><th>Name</th><th>Findings</th></tr></thead><tbody><tr><td> {$name} </td><td><ul>";
            } else {
                $entity->negative_findings = str_replace("</tbody></table>", "", $entity->negative_findings);
            }
            foreach ($records as $key => $value) {
                if ($value->charges) {
                    $charges .= "<li> " . $value->charges . " </li>";
                }
            }
            $charges .= "</ul></td></tr></tbody></table>";
            // update negative findings
            Entity::where("entityid", $entity_id)->update(array("negative_findings" => $entity->negative_findings . $charges));
        }
        return;
    }

    private function detectDueDateUtilityBill($entity) {
        $ubillFiles = DB::table('tbldocument')->where("is_deleted", 0)->where("entity_id", $entity->entityid)->where("document_type", 0)->where("document_group", 61)->get();
        $destinationPath = public_path('documents/');
        $bills = [];

        foreach($ubillFiles as $ubillFile) {
            $image = $destinationPath.$ubillFile->document_rename;
            $enhancedImage = $destinationPath.$entity->entityid.'/'.pathinfo($image, PATHINFO_FILENAME)."(enhanced).".pathinfo($image, PATHINFO_EXTENSION);
            $imageName = pathinfo($image, PATHINFO_FILENAME).".".pathinfo($image, PATHINFO_EXTENSION);
            copy($image, $enhancedImage);
            
            $imagick = new Imagick($enhancedImage);
            $imagick->sharpenimage(20,10);
            $imagick->enhanceImage();
            $imagick->getImageBlob();

            file_put_contents($enhancedImage, $imagick);

            $cloud = new ServiceBuilder([
                'keyFilePath' => public_path('visionAPI.json'),
                'projectId' => 'sixth-edition-253508'
            ]);

            $vision = $cloud->vision();
            $image = $vision->image(file_get_contents($enhancedImage), ['text']);
            $textAnnotations = $vision->annotate($image)->text();

            // Remove first row (full text)
            array_shift($textAnnotations);

            /* ----------------------------
            ----get due date Column Start--
            -----------------------------*/
            $isDueDate = STS_NG;
            $isBillDate = STS_NG;
            $texts = [];
            $textsBillDate = [];

            $billType = '';

            foreach ($textAnnotations as $key => $text)
            {
                if ($billType == '') {
                    if (strcasecmp("meralco", $text->info()['description']) == 0) {
                        $billType = "meralco";
                    break;
                    } else if (strcasecmp("manila", $text->info()['description']) == 0 and strcasecmp("water", $textAnnotations[$key + 1]->description()) == 0) {
                        $billType = "manila_water";
                    break;
                    } else if (strcasecmp("pldt", $text->info()['description']) == 0) {
                        $billType = "pldt";
                    break;
                    } else if (strcasecmp("maynilad", $text->info()['description']) == 0) {
                        $billType = "maynilad";
                    break;
                    } else if (strcasecmp("globe", $text->info()['description']) == 0) {
                        $billType = "globe";
                    break;
                    } else if (strcasecmp("smart", $text->info()['description']) == 0) {
                        $billType = "smart";
                    break;
                    } else if (strcasecmp("sun", $text->info()['description']) == 0 and strcasecmp("cellular", $textAnnotations[$key + 1]->description()) == 0) {
                        $billType = "sun";
                    break;
                    }
                }
            }

            if ($billType === "meralco") {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Balance"
                        and $textAnnotations[$key + 1]->description() === "From"
                        and $textAnnotations[$key + 2]->description() === "Previous"
                        and $textAnnotations[$key + 3]->description() === "Billing") {
                            $isDueDate = STS_OK;
                    }

                    if($isDueDate == STS_OK) {
                        $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 50;
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 165;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 50;
                        $isDueDate = STS_NG;
                    } else {
                        $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG) {
                        if ($txt['text'] == "Balance"
                            and $texts[$key + 1]['text'] == "From"
                            and $texts[$key + 2]['text'] == "Previous"
                            and $texts[$key + 3]['text'] == "Billing") {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach($texts as $key => $txt) {
                    if ($txt['text'] == "Balance" or $txt['text'] == "From" or $txt['text'] == "Previous" or $txt['text'] == "Billing") {
                        unset($texts[$key]);
                    }
                }

                foreach($texts as $key => $txt) {
                    $isOverDue = 1;

                    if ($txt['text'] == "Thank") {
                        $isOverDue = 0;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Bill" and $textAnnotations[$key + 1]->description() === "Date") {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 230;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Bill" and $textsBillDate[$key + 1]['text'] == "Date") {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                        unset($textsBillDate[$key]);
                        continue;
                    } else if ($isRotated) {
                        unset($textsBillDate[$key]);
                        continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Bill" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';
                
                foreach ($textsBillDate as $key => $txt) {
                    if($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }

                $billDate = str_replace(':', '', $billDate);

                $isMeralco = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "meralco") {
                        $isMeralco = STS_OK;
                        $billKey = $key;
                    }
                }
                if ($isMeralco) {
                   if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                   }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }

            }
            else if ($billType == "manila_water")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "PREVIOUS"
                        and $textAnnotations[$key + 1]->description() === "UNPAID"
                        and $textAnnotations[$key + 2]->description() === "AMOUNT") {
                            $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 500;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x2']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG) {
                        if ($txt['text'] == "PREVIOUS"
                            and $texts[$key + 1]['text'] == "UNPAID"
                            and $texts[$key + 2]['text'] == "AMOUNT") {
                                $isDueDate = STS_OK;
                                $textKey = $key;
                                break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])){
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "PREVIOUS"
                        or $txt['text'] == "UNPAID"
                        or $txt['text'] == "AMOUNT"
                        or $txt['text'] == "P") {
                            unset($texts[$key]);
                        }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;
                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Bill" and $textAnnotations[$key + 1]->description() === "Date") {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 500;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 3;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Bill" and $textsBillDate[$key + 1]['text'] == "Date") {
                            $isBillDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                            unset($textsBillDate[$key]);
                            continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Bill" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';
                
                foreach ($textsBillDate as $key => $txt) {
                    if($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }

                $isMaynilad = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "manila_water") {
                        $isMaynilad = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isMaynilad) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }
            }
            else if ($billType == "pldt")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Remaining"
                        and $textAnnotations[$key + 1]->description() === "Balance"
                        and $textAnnotations[$key + 2]->description() === "from"
                        and $textAnnotations[$key + 3]->description() === "Previous"
                        and $textAnnotations[$key + 4]->description() === "Bill") {
                            $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 470;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG){
                        if ($txt['text'] === "Remaining"
                            and $texts[$key + 1]['text'] === "Balance"
                            and $texts[$key + 2]['text'] === "from"
                            and $texts[$key + 3]['text'] === "Previous"
                            and $texts[$key + 4]['text'] === "Bill") {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if($txt['text'] == "Remaining"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill") {
                            unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Statement"
                        and $textAnnotations[$key + 1]->description() === "Date") {
                            $isBillDate = STS_OK;
                        }

                        if ($isBillDate == STS_OK){
                            $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 230;
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                            $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                            $isBillDate = STS_NG;
                        } else {
                            $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                            $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                            $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                        }
                        
                        $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                        $value = ["x1" => $x1,
                                    "x2" => $x2,
                                    "y1" => $y1,
                                    "y2" => $y2,
                                    "text" => $text->description()];
                        $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Statement"
                            and $textsBillDate[$key + 1]['text'] == "Date") {
                                $isBillDate = STS_OK;
                                $textKey = $key;
                                break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                            unset($textsBillDate[$key]);
                            continue;
                    }
                }
                
                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';
                
                foreach ($textsBillDate as $key => $txt) {
                    if($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }

                $billDate = str_replace(':', '', $billDate);

                $isPldt = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "pldt") {
                        $isPldt = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isPldt) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }
            }
            else if ($billType == "maynilad")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "DUE") {
                        if ($textAnnotations[$key + 1]->description() === "DATE") {
                            $isDueDate = STS_OK;
                        }
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 300;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 10;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    $isRotatedDiff = abs($txt['x2'] - $txt['x1']);
                    $isRotated = $isRotatedDiff <= 1 ? 1 : 0;

                    if ($isDueDate == STS_NG){
                        if ((($txt['text'] == "DUE" or $txt['text'] == "OUE") and $texts[$key + 1]['text'] == "DATE")) {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                        } else {
                            unset($texts[$key]);
                            continue;
                        }
                    }

                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "DUE" or $txt['text'] == "DATE" or $txt["text"] == "DUE DATE") {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (count($texts) > 1) {
                        $dueDate = "{$txt['text']} {$texts[$key + 1]['text']} {$texts[$key + 2]['text']}";
                    } else {
                        $dueDate = $txt['text'];
                    }
                    break;
                }
            }
            else if ($billType == "globe")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Due"
                        and $textAnnotations[$key + 1]->description() === "Immediately") {
                            $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 170;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if ($txt['text'] === "Due"
                            and $texts[$key + 1]['text'] === "Immediately") {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "Due"
                        or $txt['text'] == "Immediately") {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Bill"
                        and $textAnnotations[$key + 1]->description() === "Period") {
                            $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 180;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 7;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Bill"
                            and $textsBillDate[$key + 1]['text'] == "Period") {
                                $isBillDate = STS_OK;
                                $textKey = $key;
                                break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                            unset($textsBillDate[$key]);
                            continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Bill"
                        or $txt['text'] == "Period"){
                            unset($textsBillDate[$key]);
                        }
                }

                $billDate = '';
                
                foreach ($textsBillDate as $key => $txt) {
                    if($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }
                
                $billDate = substr($billDate, 0, strpos($billDate, "-"));

                $isGlobe = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "globe") {
                        $isGlobe = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isGlobe) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }
            }
            else if ($billType == "smart")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Remaining"
                        and $textAnnotations[$key + 1]->description() === "Balance"
                        and $textAnnotations[$key + 2]->description() === "from"
                        and $textAnnotations[$key + 3]->description() === "Previous"
                        and $textAnnotations[$key + 4]->description() === "Bill") {
                            $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 520;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;
                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if ($txt['text'] === "Remaining"
                            and $texts[$key + 1]['text'] === "Balance"
                            and $texts[$key + 2]['text'] === "from"
                            and $texts[$key + 3]['text'] === "Previous"
                            and $texts[$key + 4]['text'] === "Bill") {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if ($txt['text'] == "Remaining"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill") {
                        unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                    break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Statement"
                        and $textAnnotations[$key + 1]->description() === "Date:") {
                            $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 220;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isBillDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Statement"
                            and $textsBillDate[$key + 1]['text'] == "Date:") {
                                $isBillDate = STS_OK;
                                $textKey = $key;
                            break;
                        }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                            unset($textsBillDate[$key]);
                            continue;
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date:") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }

                $isSmart = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "smart") {
                        $isSmart = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isSmart) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }
            }
            else if ($billType == "sun")
            {
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Total"
                    and $textAnnotations[$key + 1]->description() === "Balance"
                    and $textAnnotations[$key + 2]->description() === "from"
                    and $textAnnotations[$key + 3]->description() === "Previous"
                    and $textAnnotations[$key + 4]->description() === "Bill") {
                            $isDueDate = STS_OK;
                    }

                    if ($isDueDate == STS_OK) {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 420;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 5;
                        $isDueDate = STS_NG;
                    } else {
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }
                    $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $texts[] = $value;
                }

                $isDueDate = STS_NG;
                $textKey = 0;

                foreach ($texts as $key => $txt) {
                    if ($isDueDate == STS_NG) {
                        if ($txt['text'] === "Total"
                        and $texts[$key + 1]['text'] === "Balance"
                        and $texts[$key + 2]['text'] === "from"
                        and $texts[$key + 3]['text'] === "Previous"
                        and $texts[$key + 4]['text'] === "Bill") {
                            $isDueDate = STS_OK;
                            $textKey = $key;
                        }
                    }
                }

                foreach ($texts as $key => $txt) {
                    if (($txt['y1'] < $texts[$textKey]['y1'] || $txt['y2'] > $texts[$textKey]['y2'])) {
                        unset($texts[$key]);
                        continue;
                    } else if ($txt['x1'] < $texts[$textKey]['x1'] || $txt['x2'] > $texts[$textKey]['x2']) {
                        unset($texts[$key]);
                        continue;
                    }
                }

                foreach ($texts as $key => $txt) {
                    if($txt['text'] == "Total"
                        or $txt['text'] == "Balance"
                        or $txt['text'] == "from"
                        or $txt['text'] == "Previous"
                        or $txt['text'] == "Bill") {
                            unset($texts[$key]);
                    }
                }

                foreach ($texts as $key => $txt) {
                    $isOverDue = 0;
                    $txt['text'] = preg_replace("/[^0-9.,-]/", "", $txt['text']);

                    if ($txt['text'] > 0) {
                        $isOverDue = 1;
                        break;
                    }
                }

                //get bill date
                foreach ($textAnnotations as $key => $text)
                {
                    if ($text->info()['description'] === "Statement"
                    and ($textAnnotations[$key + 1]->description() === "Date"
                    or $textAnnotations[$key + 1]->description() === "Date:")) {
                        $isBillDate = STS_OK;
                    }

                    if ($isBillDate == STS_OK) {
                        $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"] - 10;
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"] + 35;
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"] - 5;
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"] + 20;
                        $isBillDate = STS_NG;
                    } else {
                        $x1 = (int)$text->info()["boundingPoly"]["vertices"][0]["x"];
                        $x2 = (int)$text->info()["boundingPoly"]["vertices"][1]["x"];
                        $y1 = (int)$text->info()["boundingPoly"]["vertices"][0]["y"];
                        $y2 = (int)$text->info()["boundingPoly"]["vertices"][2]["y"];
                    }

                    $value = ["x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2,
                                "text" => $text->description()];
                    $textsBillDate[] = $value;
                }

                $isBillDate = STS_NG;
                $textKey = 0;

                foreach ($textsBillDate as $key => $txt) {
                    if ($isBillDate == STS_NG) {
                        if ($txt['text'] == "Statement"
                            and ($textsBillDate[$key + 1]['text'] == "Date"
                            or $textsBillDate[$key + 1]['text'] == "Date:")) {
                                $isBillDate = STS_OK;
                                $textKey = $key;
                            break;
                            }
                    }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['x1'] < $textsBillDate[$textKey]['x1']
                        || $txt['x2'] > $textsBillDate[$textKey]['x2']
                        || $txt['y1'] < $textsBillDate[$textKey]['y1']
                        || $txt['y2'] > $textsBillDate[$textKey]['y2']) {
                            unset($textsBillDate[$key]);
                            continue;
                        }
                }

                foreach ($textsBillDate as $key => $txt) {
                    if ($txt['text'] == "Statement" or $txt['text'] == "Date" or $txt['text'] == "Date:") {
                        unset($textsBillDate[$key]);
                    }
                }

                $billDate = '';

                foreach ($textsBillDate as $key => $txt) {
                    if ($key != array_key_last($textsBillDate)) {
                        $billDate = $billDate.$txt['text'].' ';
                    } else {
                        $billDate = $billDate.$txt['text'];
                    }
                }

                $isSun = STS_NG;
                $billKey = STS_NG;

                foreach ($bills as $key => $bill) {
                    if ($bill['billType'] == "sun") {
                        $isSun = STS_OK;
                        $billKey = $key;
                    }
                }

                if ($isSun) {
                    if (strtotime($billDate) > strtotime($bills[$billKey]['billDate'])) {
                        $bills[$billKey]['isOverDue'] = $isOverDue;
                        $bills[$billKey]['billDate'] = $billDate;
                    }
                } else {
                    $bills[] = ["billType" => $billType,
                                "isOverDue" => $isOverDue,
                                "billDate" => $billDate];
                }
            }
            // $dateNow = date('m/d/Y');

            // if (strtotime($dueDate) < strtotime($dateNow)) {
            //     $status = 1;
            // } else {
            //     $status = 0;
            // }

            // $ubillCheck = DB::table('utility_bills')->where('is_deleted', 0)->where('entity_id', $entity->entityid)->where('utility_bill_file', $imageName)->first();
            
            // if ($ubillCheck) {
            //     DB::table('utility_bills')->where('id', $ubillCheck->id)->update(["status" => $status]);
            // } else {
            //     //create entry on database
            //     $data = [
            //         'entity_id' => $entity->entityid,
            //         'status' => $status,
            //         'utility_bill_file' => $imageName,
            //         'is_deleted' => 0,
            //         'created_at' => date('Y-m-d H:i:s'),
            //         'updated_at' => date('Y-m-d H:i:s'),
            //     ];
    
            //     DB::table('utility_bills')->insertGetId($data);
            // }
        }
        $isOverDueFinal = 1;
        foreach ($bills as $key => $bill) {
            if ($bill['isOverDue'] == 1) {
                $isOverDueFinal = 2;
            break;
            }
        }

        DB::table('tblentity')->where('entityid', $entity->entityid)->update(['utility_bill' => $isOverDueFinal]);

        return;
    }
}
