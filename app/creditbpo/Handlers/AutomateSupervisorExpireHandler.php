<?php

namespace CreditBPO\Handlers;

use CreditBPO\Services\MailHandler;
use DateTime;
use DateTimeZone;
use DB;

class AutomateSupervisorExpireHandler {

    public function __construct(){
    }

    public function processSupervisorAccounts(){
        //get all supervisor account
        $supervisorAccount = DB::table('tbllogin')
                            ->select(DB::raw("
                                tbllogin.loginid,
                                tbllogin.name,
                                tbllogin.email,
                                DATE(tbllogin.created_at) as date_registered,
                                tblbank.bank_type as organization_type,
                                tblbank.bank_name as organization,
                                tblbank.questionnaire_type as questionnaire_type,
                                supervisor_application.product_features as product,
                                supervisor_application.valid_id_picture as picture,
                                supervisor_application.application_id,
                                supervisor_application.status,
                                supervisor_application.application_date,
                                DATE(tblbanksubscriptions.expiry_date) as expiry_date,
                                tblbanksubscriptions.status as expiry_status,
                                tblbanksubscriptions.updated_at as sub_updated_at,
                                tblbanksubscriptions.created_at as expiry_start"))
                            ->leftJoin('tblbank', 'tbllogin.bank_id', '=', 'tblbank.id')
                            ->leftJoin('supervisor_application', 'tbllogin.email', '=', 'supervisor_application.email')
                            ->leftJoin('tblbanksubscriptions', 'tblbanksubscriptions.bank_id', '=', 'tbllogin.loginid')
                            ->where('tbllogin.role', 4)
                            ->whereRaw("DATEDIFF(tblbanksubscriptions.expiry_date,CURDATE()) = 10")
                            ->orderBy('supervisor_application.application_date', 'DESC')
                            ->get();
                     
        foreach($supervisorAccount as $supervisor){
            //sent email 10 days before supervisor subscription expired
            if(env('APP_ENV') != 'local') {
                $mailHandler = new MailHandler();
                $mailHandler->sendReminderSupervisorSubcription($supervisor);
                echo 'Send a reminder email to ' . $supervisor->name . '(' . $supervisor->email . ')' . "\n";
            }
        }
    }
}


?>