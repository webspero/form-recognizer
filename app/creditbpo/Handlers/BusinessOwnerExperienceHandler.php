<?php

namespace CreditBPO\Handlers;

use CreditBPO\Handlers\KeyManagerHandler;
use CreditBPO\Handlers\OwnerHandler;
use CreditBPO\Models\Entity;

class BusinessOwnerExperienceHandler {
    protected $entityModel;

    /**
     * @param Entity $entityModel
     */
    public function __construct(Entity $entityModel = null) {
        $this->entityModel = $entityModel ?: new Entity();
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getAssessmentScoreByEntityId($entityId) {
        $entity = $this->entityModel->find($entityId);

        if ($entity->entity_type === 0) {
            $handler = new KeyManagerHandler();
        } else {
            $handler = new OwnerHandler();
        }

        return $handler->getAssessmentScoreByEntityId($entityId);
    }
}
