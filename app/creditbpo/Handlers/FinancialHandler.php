<?php

namespace CreditBPO\Handlers;
use FinancialReport;

class FinancialHandler {
    /**
     * @param array $data
     * @return int
     */
    public static function calculateSP($data = []) {
        if (@count($data) <= 0) {
            return 0;
        }

        $sp = 0;
        for ($x = 0; $x < @count($data) - 1; $x++) {
            $sp += $data[$x];
        }

        return $sp / (@count($data) - 1);
    }

    /**
     * @param array $data
     * @param bool $slope
     * @return int
     */
    public static function calculateSf($data = [], $slope = false) {
        $n  = @count($data);
		$a = 0;
		foreach($data as $k=>$v){
				$a += ($k + 1) * $v;
		}
		$a = $a * $n;
		$ba = 0; $bb = 0;
		foreach($data as $k=>$v){
			$ba += ($k + 1);
			$bb += $v;
		}
		$b = $ba * $bb;
		$c = 0;
		for($x = 1; $x <= $n; $x++){
			$c += $x * $x;
		}
		$c = $c * $n;
		$d = 0;
		for($x = 1; $x <= $n; $x++){
			$d += $x;
		}
		$d = $d * $d;
		if(($c - $d) != 0)
			$m = ($a - $b) / ($c - $d);
		else
			$m = 0;
		if($slope){
			return $m;
		}
		$e = 0;
		foreach($data as $v){
			$e += $v;
		}
		$f = 0;
		for($x = 1; $x <= $n; $x++){
			$f += $x;
		}
		$f = $f * $m;
		if($n != 0)
			$y_intercept = ($e -$f) / $n;
		else
			$y_intercept = 0;

		return ($m * ($n + 1)) + $y_intercept;
    }

    /**
     * @param array $data
     * @return int
     */
    public static function calculateAverageScore($data = []) {
        return ($data[0] * 0.25) + ($data[1] * 0.6) + ($data[2] * 0.15);
    }

    /**
     * @param array $data
     * @param bool $present
     * @return int
     */
    public function calculateSlope($data = [], $present = false) {
        $dataCount = @count($data);

        if ($present) {
            return $data[$dataCount - 1] - $data[$dataCount - 2];
        }

        if ($dataCount > 2) {
            return ($data[$dataCount - 2] - $data[0]) / ($dataCount - 2);
        }

        return $data[$dataCount - 1] - $data[$dataCount - 2];
    }

    public function financialRatingComputation($id)
    {
        /** Financial Postion */
        $financialReport = FinancialReport::find($id);
        if($financialReport) {
            $balance_sheets = $financialReport->balanceSheets()
                ->orderBy('index_count', 'asc')->get();
            if(@count($balance_sheets) <= 3) {
                return false;
            }
        } else {
            return false;
        }
        
        $income_statements = $financialReport->incomeStatements()
                // ->where('Revenue', '!=', 0)
                ->orderBy('index_count', 'asc')->get();

        $debtRatio = array();
        for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
            if($balance_sheets[$x]->Assets != 0) $debtRatio[] = $balance_sheets[$x]->Liabilities / $balance_sheets[$x]->Assets;
            else $debtRatio[] = 0;
        }
        $debtRatioS = [
            $this->calculateSp($debtRatio), 
            $debtRatio[@count($balance_sheets)-1],
            $this->calculateSf($debtRatio)
        ];
        //∞ < good < 0.3 ≤ excel. ≤ 0.5 < good ≤ 0.6 < unsat. ≤ 1 < crit. < ∞
        foreach($debtRatioS as $k=>$v){
            if($v < 0.3) $debtRatioS[$k] = 1;
            elseif($v >= 0.3 && $v <= 0.5) $debtRatioS[$k] = 2;
            elseif($v > 0.5 && $v <= 0.6) $debtRatioS[$k] = 1;
            elseif($v > 0.6 && $v <= 1) $debtRatioS[$k] = -1;
            else $debtRatioS[$k] = -2;
        }
        $debtRatioAv = $this->calculateAverageScore($debtRatioS);
        
        //Non-current Assets to Net Worth
        $NCAtoNW = array();
        for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
            if($balance_sheets[$x]->Equity != 0 ) $NCAtoNW[] = $balance_sheets[$x]->NoncurrentAssets / $balance_sheets[$x]->Equity;
            else $NCAtoNW[] = 0;
        }
        $ncaToNw = $NCAtoNW[@count($balance_sheets)-1];
        $NCAtoNWS = [
            $this->calculateSp($NCAtoNW), 
            $ncaToNw,
            $this->calculateSf($NCAtoNW)
        ];
        
        foreach($NCAtoNWS as $k=>$v){
            if($v < 0) $NCAtoNWS[$k] = -2;
            elseif($v >= 0 && $v <= 1) $NCAtoNWS[$k] = 2;
            elseif($v > 1 && $v <= 1.25) $NCAtoNWS[$k] = 1;
            elseif($v > 1.25 && $v <= 2) $NCAtoNWS[$k] = -1;
            else $NCAtoNWS[$k] = -2;
        }
        $NCAtoNWAv = $this->calculateAverageScore($NCAtoNWS); 
        
        //Current Ratio
        $currentRatio = array();
        for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
            if($balance_sheets[$x]->CurrentLiabilities != 0) $currentRatio[] = $balance_sheets[$x]->CurrentAssets / $balance_sheets[$x]->CurrentLiabilities;
            else $currentRatio[] = 0;
        }
        $currentRatioS = [
            $this->calculateSp($currentRatio), 
            $currentRatio[@count($balance_sheets)-1],
            $this->calculateSf($currentRatio)
        ];
        
        foreach($currentRatioS as $k=>$v){
            if($v < 1) $currentRatioS[$k] = -2;
            elseif($v >= 1 && $v < 2) $currentRatioS[$k] = -1;
            elseif($v >= 2 && $v < 2.1) $currentRatioS[$k] = 1;
            else $currentRatioS[$k] = 2;
        }
        $currentRatioAv = $this->calculateAverageScore($currentRatioS);
        
        //quickRatio
        $quickRatio = array();
        for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
            if($balance_sheets[$x]->CurrentLiabilities != 0)
                $quickRatio[] = ($balance_sheets[$x]->CashAndCashEquivalents + 
                                $balance_sheets[$x]->OtherCurrentFinancialAssets + 
                                $balance_sheets[$x]->TradeAndOtherCurrentReceivables) / 
                                $balance_sheets[$x]->CurrentLiabilities;
            else
                $quickRatio[] = 0;
        }
        $quickRatioS = [
            $this->calculateSp($quickRatio), 
            $quickRatio[@count($balance_sheets)-1],
            $this->calculateSf($quickRatio)
        ];
        foreach($quickRatioS as $k=>$v){
            if($v < 0.5) $quickRatioS[$k] = -2;
            elseif($v >= 0.5 && $v < 1) $quickRatioS[$k] = -1;
            elseif($v >= 1 && $v < 1.1) $quickRatioS[$k] = 1;
            else $quickRatioS[$k] = 2;
        }
        $quickRatioAv = $this->calculateAverageScore($quickRatioS);
        
        //Cash Ratio
        $cashRatio = array();
        for($x=@count($balance_sheets)-1; $x >= 0 ; $x--){
            if($balance_sheets[$x]->CurrentLiabilities != 0)
                $cashRatio[] = $balance_sheets[$x]->CashAndCashEquivalents / 
                                $balance_sheets[$x]->CurrentLiabilities;
            else
                $cashRatio[] = 0;
        }
        $cashRatioS = [
            $this->calculateSp($cashRatio), 
            $cashRatio[@count($balance_sheets)-1],
            $this->calculateSf($cashRatio)
        ];
        
        foreach($cashRatioS as $k=>$v){
            if($v < 0.05) $cashRatioS[$k] = -2;
            elseif($v >= 0.05 && $v < 0.2) $cashRatioS[$k] = -1;
            elseif($v >= 0.2 && $v < 0.25) $cashRatioS[$k] = 1;
            else $cashRatioS[$k] = 2;
        }
        $cashRatioAv = $this->calculateAverageScore($cashRatioS);
        
        $financialPosition = ($debtRatioAv * 0.3) +
                                ($NCAtoNWAv * 0.15) +
                                ($currentRatioAv * 0.2) +
                                ($quickRatioAv * 0.2) +
                                ($cashRatioAv * 0.15);
        
        
        /** End for Financial Position */
        
        /** Financial Performance */
        //Return on equity (ROE)
        $ROE = array();

        for($x=@count($income_statements)-2; $x >= 0 ; $x--){
            $roeDiv = (($balance_sheets[$x]->Equity + $balance_sheets[$x+1]->Equity) / 2);
            if($roeDiv != 0) $ROE[] = $income_statements[$x]->ProfitLoss / $roeDiv;
            else $ROE[] = 0;
        }
        
        $ROES = [
            $this->calculateSp($ROE), 
            $ROE[@count($income_statements)-2],
            $this->calculateSf($ROE)
        ];
        //∞ < crit. < 0 ≤ unsat. < 0.12 ≤ good < 0.2 ≤ excel. < ∞
        foreach($ROES as $k=>$v){
            if($v < 0) $ROES[$k] = -2;
            elseif($v >= 0 && $v < 0.112) $ROES[$k] = -1;
            elseif($v >= 0.112 && $v < 0.128) $ROES[$k] = 0;
            elseif($v >= 0.128 && $v < 0.2) $ROES[$k] = 1;
            else $ROES[$k] = 2;
        }
        $ROEAv = $this->calculateAverageScore($ROES);
        
        //Return on assets (ROA)
        $ROA = array();
        for($x=@count($income_statements)-2; $x >= 0 ; $x--){
            $roeDiv =  (($balance_sheets[$x]->Assets + $balance_sheets[$x + 1]->Assets) / 2);
            if($roeDiv != 0 ) $ROA[] = $income_statements[$x]->ProfitLoss / $roeDiv;
            else $ROA[] = 0;
        }
        $ROAS = [
            $this->calculateSp($ROA), 
            $ROA[@count($income_statements)-2],
            $this->calculateSf($ROA)
        ];
        //∞ < crit. < 0 ≤ unsat. < 0.06 ≤ good < 0.1 ≤ excel. < ∞
        foreach($ROAS as $k=>$v){
            if($v < 0) $ROAS[$k] = -2;
            elseif($v >= 0 && $v < 0.056) $ROAS[$k] = -1;
            elseif($v >= 0.056 && $v < 0.064) $ROAS[$k] = 0;
            elseif($v >= 0.064 && $v < 0.1) $ROAS[$k] = 1;
            else $ROAS[$k] = 2;
        }
        $ROAAv = $this->calculateAverageScore($ROAS);
        
        //Sales Growth
        $salesGrowth = array();
        $SGtotal = 0;
        for($x=@count($income_statements)-1; $x >= 0 ; $x--){
            $salesGrowth[] = $income_statements[$x]->Revenue;
            $SGtotal += $income_statements[$x]->Revenue;
        }
        $SGaverage = $SGtotal / @count($income_statements);
        if($SGaverage != 0){
            $salesGrowthS = [
                $this->calculateSlope($salesGrowth, false) / $SGaverage, 
                $this->calculateSlope($salesGrowth, true) / $SGaverage,
                $this->calculateSf($salesGrowth, true) / $SGaverage
            ];
        } else {
            $salesGrowthS = [0 , 0 , 0];
        }
        
        /*
        <-0.3 – score "-2";
        -0.3 – 0.04 – score "-1";
        -0.04 – 0.04 – score "0";
        0.04 – 0.3 – score "+1";
        > 0.3 – score "+2"
        */
        foreach($salesGrowthS as $k=>$v){
            if($v < -0.3) $salesGrowthS[$k] = -2;
            elseif($v >= -0.3 && $v < -0.04) $salesGrowthS[$k] = -1;
            elseif($v >= -0.04 && $v < 0.04) $salesGrowthS[$k] = 0;
            elseif($v >= 0.04 && $v < 0.3) $salesGrowthS[$k] = 1;
            else $salesGrowthS[$k] = 2;
        }
        $salesGrowthAv = $this->calculateAverageScore($salesGrowthS);
        $financialPerformance = ($ROEAv * 0.5) +
                                ($ROAAv * 0.3) +
                                ($salesGrowthAv * 0.2);
                                
        /** End for Financial Performance */ 
        return array($financialPosition, $financialPerformance);
    }
}
