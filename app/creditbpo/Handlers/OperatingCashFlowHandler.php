<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\FinancialReport;

class OperatingCashFlowHandler {
    protected $model;

    /**
     * @param FinancialReport|null $model
     */
    public function __construct(FinancialReport $model = null) {
        $this->model = $model ?: new FinancialReport();
    }

    /**
     * @param int $financialReportId
     * @return array
     */
    public function getMarginAndRatio($financialReportId) {
        $financialReport = $this->model->find($financialReportId);
        $returnValue = [
            'margin' => 0,
            'ratio' => 0,
        ];

        if (@count($financialReport) <= 0) {
            return $returnValue;
        }

        $balanceReport = $financialReport->balanceSheets()
            ->orderBy('index_count', 'asc')
            ->first();
        $incomeStatement = $financialReport->incomeStatements()
            ->orderBy('index_count', 'asc')
            ->first();
        $cashFlow = $financialReport->cashFlow()
            ->where('NetCashByOperatingActivities', '>', 0)
            ->first();
        $netSales = (float)$incomeStatement->Revenue * $financialReport->money_factor / 2;
        $currentLiability = $balanceReport->CurrentLiabilities * $financialReport->money_factor;
        $adb = @count($cashFlow) > 0 ? $cashFlow->NetCashByOperatingActivities : 0;

        if ($netSales !== 0 && $netSales !== 0.0) {
            $returnValue['margin'] = round($adb / $netSales, 2);
        } else {
            $returnValue['margin'] = 0;
        }
        if($currentLiability !== 0 && $currentLiability !== 0.0){
            $returnValue['ratio'] = round($adb / $currentLiability, 2);
        } else {
            $returnValue['ratio'] = 0;
        }
        return $returnValue;
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function getMarginAndRatioByEntityId($entityId) {
        $financialReport = $this->model
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();

        if (@count($financialReport) <= 0) {
            return [
                'margin' => 0,
                'ratio' => 0,
            ];
        }

        return $this->getMarginAndRatio($financialReport->id);
    }
}
