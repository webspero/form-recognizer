<?php

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Config;
use CreditBPO\Exceptions\VfaException;
use CreditBPO\Models\Entity;
use CreditBPO\Models\FinancialReport;
use \Exception as Exception;

class VfaHandler {
    private $config;

    public function __construct() {
        $this->config = Config::get('handlers.vfa');
    }

    /**
     * @param int $entityId
     * @return string
     */
    public function getContentByEntityId($entityId) {
        $entityModel = new Entity();
        $entity = $entityModel->find($entityId);

        if (!$entity) {
            throw new VfaException('Entity not found. ID: '.$entityId);
        }

        $financialReportModel = new FinancialReport();
        $financialReport = $financialReportModel
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();
        $financialReportId = $financialReport->id;

        if (!$financialReportId) {
            throw new VfaException('No Financial Report ID for Entity ID: '.$entityId);
        }

        $vars = $this->config['defaults'];
        $vars['YEAR'] = $financialReport->year;
        $vars['money_factor'] = $financialReport->money_factor;
        $vars['valedin'] = $financialReport->currency;
        $vars['Otrasli'] = $financialReport->industry_id;
        $vars['title'] = $financialReport->title;

        $balanceSheets = $financialReport->balanceSheets()
            ->orderBy('index_count', 'asc')
            ->get();
        $count = 0;
        foreach ($balanceSheets as $row) {
            if($row->EquityAndLiabilities > 0) {
                $count++;
                $vars['bal'][] = [
                    'PropertyPlantAndEquipment' => $row->PropertyPlantAndEquipment,
                    'InvestmentProperty' => $row->InvestmentProperty,
                    'Goodwill' => $row->Goodwill,
                    'IntangibleAssetsOtherThanGoodwill' => $row->IntangibleAssetsOtherThanGoodwill,
                    'InvestmentAccountedForUsingEquityMethod' =>
                        $row->InvestmentAccountedForUsingEquityMethod,
                    'InvestmentsInSubsidiariesJointVenturesAndAssociates' =>
                        $row->InvestmentsInSubsidiariesJointVenturesAndAssociates,
                    'NoncurrentBiologicalAssets' =>
                        $row->NoncurrentBiologicalAssets,
                    'NoncurrentReceivables' => $row->NoncurrentReceivables,
                    'NoncurrentInventories' => $row->NoncurrentInventories,
                    'DeferredTaxAssets' => $row->DeferredTaxAssets,
                    'CurrentTaxAssetsNoncurrent' => $row->CurrentTaxAssetsNoncurrent,
                    'OtherNoncurrentFinancialAssets' => $row->OtherNoncurrentFinancialAssets,
                    'OtherNoncurrentNonfinancialAssets' => $row->OtherNoncurrentNonfinancialAssets,
                    'NoncurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral' =>
                        $row->NoncurrentNoncashAssetsPledgedAsCollateral,
                    'NoncurrentAssets' => $row->NoncurrentAssets,
                    'Inventories' => $row->Inventories,
                    'TradeAndOtherCurrentReceivables' => $row->TradeAndOtherCurrentReceivables,
                    'CurrentTaxAssetsCurrent' => $row->CurrentTaxAssetsCurrent,
                    'CurrentBiologicalAssets' => $row->CurrentBiologicalAssets,
                    'OtherCurrentFinancialAssets' => $row->OtherCurrentFinancialAssets,
                    'OtherCurrentNonfinancialAssets' => $row->OtherCurrentNonfinancialAssets,
                    'CashAndCashEquivalents' => $row->CashAndCashEquivalents,
                    'CurrentNoncashAssetsPledgedAsCollateralForWhichTransfereeHasRightByContractOrCustomToSellOrRepledgeCollateral' =>
                        $row->CurrentNoncashAssetsPledgedAsCollateral,
                    'NoncurrentAssetsOrDisposalGroupsClassifiedAsHeldForSaleOrAsHeldForDistributionToOwners' =>
                        $row->NoncurrentAssetsOrDisposalGroups,
                    'CurrentAssets' => $row->CurrentAssets,
                    'Assets' => $row->Assets,
                    'IssuedCapital' => $row->IssuedCapital,
                    'SharePremium' => $row->SharePremium,
                    'TreasuryShares' => $row->TreasuryShares,
                    'OtherEquityInterest' => $row->OtherEquityInterest,
                    'OtherReserves' => $row->OtherReserves,
                    'RetainedEarnings' => $row->RetainedEarnings,
                    'NoncontrollingInterests' => $row->NoncontrollingInterests,
                    'Equity' => $row->Equity,
                    'NoncurrentProvisionsForEmployeeBenefits' =>
                        $row->NoncurrentProvisionsForEmployeeBenefits,
                    'OtherLongtermProvisions' => $row->OtherLongtermProvisions,
                    'NoncurrentPayables' => $row->NoncurrentPayables,
                    'DeferredTaxLiabilities' => $row->DeferredTaxLiabilities,
                    'CurrentTaxLiabilitiesNoncurrent' => $row->CurrentTaxLiabilitiesNoncurrent,
                    'OtherNoncurrentFinancialLiabilities' =>
                        $row->OtherNoncurrentFinancialLiabilities,
                    'OtherNoncurrentNonfinancialLiabilities' =>
                        $row->OtherNoncurrentNonfinancialLiabilities,
                    'NoncurrentLiabilities' =>
                        $row->NoncurrentLiabilities,
                    'CurrentProvisionsForEmployeeBenefits' =>
                        $row->CurrentProvisionsForEmployeeBenefits,
                    'OtherShorttermProvisions' => $row->OtherShorttermProvisions,
                    'TradeAndOtherCurrentPayables' => $row->TradeAndOtherCurrentPayables,
                    'CurrentTaxLiabilitiesCurrent' => $row->CurrentTaxLiabilitiesCurrent,
                    'OtherCurrentFinancialLiabilities' => $row->OtherCurrentFinancialLiabilities,
                    'OtherCurrentNonfinancialLiabilities' => $row->OtherCurrentNonfinancialLiabilities,
                    'LiabilitiesIncludedInDisposalGroupsClassifiedAsHeldForSale' =>
                        $row->LiabilitiesIncludedInDisposalGroups,
                    'CurrentLiabilities' => $row->CurrentLiabilities,
                    'Liabilities' => $row->Liabilities,
                    'EquityAndLiabilities' => $row->EquityAndLiabilities,
                ];
                $vars['ubal1'][] = [
                    'staff' => 0,
                ];
            }
        }

        $incomeStatements = $financialReport->incomeStatements()
            ->orderBy('index_count', 'asc')
            ->get();

        foreach ($incomeStatements as $key => $row) {
            $values = [
                'Revenue' => $row->Revenue,
                'CostOfSales' => $row->CostOfSales,
                'GrossProfit' => $row->GrossProfit,
                'OtherIncome' => $row->OtherIncome,
                'DistributionCosts' => $row->DistributionCosts,
                'AdministrativeExpense' => $row->AdministrativeExpense,
                'OtherExpenseByFunction' => $row->OtherExpenseByFunction,
                'OtherGainsLosses' => $row->OtherGainsLosses,
                'ProfitLossFromOperatingActivities' => $row->ProfitLossFromOperatingActivities,
                'DifferenceBetweenCarryingAmountOfDividendsPayableAndCarryingAmountOfNoncashAssetsDistributed'
                    => $row->DifferenceBetweenCarryingAmountOfDividendsPayable,
                'GainsLossesOnNetMonetaryPosition' => $row->GainsLossesOnNetMonetaryPosition,
                'GainLossArisingFromDerecognitionOfFinancialAssetsMeasuredAtAmortisedCost'
                    => $row->GainLossArisingFromDerecognitionOfFinancialAssets,
                'FinanceIncome' => $row->FinanceIncome,
                'FinanceCosts' => $row->FinanceCosts,
                'ImpairmentLossImpairmentGainAndReversalOfImpairmentLossDeterminedInAccordanceWithIFRS9'
                    => $row->ImpairmentLoss,
                'ShareOfProfitLossOfAssociatesAndJointVenturesAccountedForUsingEquityMethod'
                    => $row->ShareOfProfitLossOfAssociates,
                'OtherIncomeExpenseFromSubsidiariesJointlyControlledEntitiesAndAssociates'
                    => $row->OtherIncomeFromSubsidiaries,
                'GainsLossesArisingFromDifferenceBetweenPreviousCarryingAmountAndFairValueOfFinancialAssetsReclassifiedAsMeasuredAtFairValue'
                    => $row->GainsLossesArisingFromDifference,
                'CumulativeGainLossPreviouslyRecognisedInOtherComprehensiveIncomeArisingFromReclassificationOfFinancialAssetsOutOfFairValueThroughOtherComprehensiveIncomeIntoFairValueThroughProfitOrLossMeasurementCategory'
                    => $row->CumulativeGainPreviouslyRecognized,
                'HedgingGainsLossesForHedgeOfGroupOfItemsWithOffsettingRiskPositions'
                    => $row->HedgingGainsForHedge,
                'ProfitLossBeforeTax' => $row->ProfitLossBeforeTax,
                'IncomeTaxExpenseContinuingOperations' =>
                    $row->IncomeTaxExpenseContinuingOperations,
                'ProfitLossFromContinuingOperations' =>
                    $row->ProfitLossFromContinuingOperations,
                'ProfitLossFromDiscontinuedOperations' =>
                    $row->ProfitLossFromDiscontinuedOperations,
                'ProfitLoss' => $row->ProfitLoss,
                'OtherComprehensiveIncome' => $row->OtherComprehensiveIncome,
                'ComprehensiveIncome' => $row->ComprehensiveIncome,
            ];

            $vars['prib'][] = $values;

            if ($key !== 0) {
                $vars['prib_s'][] = $values;
            }
            
        }

        $cashFlow = $financialReport->cashFlow()
            ->orderBy('index_count', 'asc')
            ->get();
        $x = 0;
        foreach ($cashFlow as $key => $row) {
            if($x  < $count) {
                $vars['ubal2'][] = [
                    'DepreciationAndAmortisationExpense' => $row->DepreciationAndAmortisationExpense,
                ];
    
                if ($key !== 0) {
                    $vars['ubal2_s'][] = [
                        'DepreciationAndAmortisationExpense' =>
                            $row->DepreciationAndAmortisationExpense,
                    ];
                }
                $x++;
            }
        }

        return $vars;
    }

    public function getFileByEntityId($entityId) {
        $contents = $this->getContentByEntityId($entityId);

        if (is_array($contents)) {
            $contents = json_encode($contents);
        }
    }
}
