<?php

namespace CreditBPO\Handlers;

use Illuminate\Auth\UserInterface;
use CreditBPO\Models\UploadErrors;
use Illuminate\Support\Facades\Auth;
use Mail;
use \Exception as Exception;

class UploadErrorHandler {
   
    /**
     * @param UploadErrors|null $model
     */
    public function __construct(Entity $model = null) {
        $this->model = $model ?: new UploadErrors();
    }

    public static function uploadErrorsUpdate($uploadError) {

    	UploadErrors::insertNewUploadError($uploadError);

        $errorCount = UploadErrors::getUploadErrorCount();

        if($errorCount > 3){
        	try{
            	if(env("APP_ENV") != "local"){
                    Mail::send('emails.uploaderror', array(
                        'username' => Auth::user()->email,
                        'report_number' => $uploadError['entityid'],
                        'time_of_report' => $uploadError['date_uploading'],
                        'error_details' => $uploadError['error_details']
                    ), function($message){
                        $emails = ['ana.liza.bongat@creditbpo.com'];
                        $message->to($emails)
                            ->subject('Alert: Basic User Upload Errors 3 times!');
                    });
                }
            }catch(Exception $e){
                //
            }
        }

    }

    
}
