<?php

namespace CreditBPO\Handlers;

use CreditBPO\Handlers\FinancialPerformance;
use CreditBPO\Handlers\FinancialPosition;
use CreditBPO\Handlers\ReadyRatioScoreHandler;
use CreditBPO\Models\FinancialReport;
use \Exception as Exception;

class FinancialConditionHandler {
    /**
     * @param FinancialReport $model
     */
    public function __construct(FinancialReport $model = null) {
        $this->model = $model ?: new FinancialReport();
    }

    /**
     * @param int $financialReportId
     * @return array
     */
    public function getFinancialCondition($financialReportId) {
        $financialPerformanceHandler = new FinancialPerformance();
        $financialPositionHandler = new FinancialPosition();
        $readyRatioScoreHandler = new ReadyRatioScoreHandler();
        $performanceRange = $financialPerformanceHandler->getScore($financialReportId);
        $performanceScore = $readyRatioScoreHandler->getValueByRange($performanceRange);
        $positionRange = $financialPositionHandler->getScore($financialReportId);
        $positionScore = $readyRatioScoreHandler->getValueByRange($positionRange);

        $financialCondition = $this->computeFinancialCondition(
            $positionRange,
            $performanceRange
        );
        $financialConditionRatio = $this->getRatioByScore($financialCondition);
        return [
            'condition_range' => $financialConditionRatio['range'],
            'condition_score' => $financialConditionRatio['value'],
            'performance_range' => $performanceRange,
            'performance_score' => $performanceScore,
            'position_range' => $positionRange,
            'position_score' => $positionScore,
        ];
    }

    /**
     * @param int $entityId
     * @return array|null
     */
    public function getFinancialConditionByEntityId($entityId) {
        $financialReport = $this->model
            ->where('entity_id', '=', $entityId)
            ->orderBy('id', 'desc')
            ->first();

        if (@count($financialReport) <= 0) {
            return null;
        }
        
        return $this->getFinancialCondition($financialReport->id);
    }

    public function computeFinancialCondition(
        $financialPosition, $financialPerformance
    ) {
        return ($financialPosition * 0.6) +
            ($financialPerformance * 0.4);
    }

    public function getRatioByScore($value) {
        $readyRatioScoreHandler = new ReadyRatioScoreHandler();
        $readyRatioScore = $readyRatioScoreHandler->getClosestRatioByScoreRange($value);

        if (@count($readyRatioScore) <= 0) {
            throw new Exception('Cant retrieve ratio by score');
        }

        return [
            'range' => $readyRatioScore->score_range,
            'value' => $readyRatioScore->score_value,
        ];
    }

}
