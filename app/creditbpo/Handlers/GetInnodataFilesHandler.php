<?php

namespace CreditBPO\Handlers;

use CreditBPO\Handlers\AutomateSimplifiedRatingHandler;
use CreditBPO\Handlers\AutomateStandaloneRatingHandler;
use CreditBPO\Handlers\FinancialAnalysisHandler;
use CreditBPO\Models\AutomateStandaloneRating;
use CreditBPO\Services\KeyRatioService;
use CreditBPO\Services\ComplexFsInput;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Services\AssetGrouping;
use CreditBPO\Services\SimpleFsInput;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use CreditBPO\Models\Document;
use CreditBPO\Models\Entity;
use CreditBPO\Models\TranscribeFS;
use InnodataFile;
use ZipArchive;
use Exception;
use File;
use CreditBPO\Services\IndustryComparison;

class GetInnodataFilesHandler
{
    /** Variable Declaration */
    const FS_INPUT_COMPLEX = 'files/FS Input Template - Complex Output.xlsx';

    protected $model;
    protected $success;
    protected $errors;

    /**
     * @param AutomateStandaloneRating $model
     */

    //------------------------------------------------------------------
    //  Function 59.0: __construct
    //       GetInnodataFilesHandler Initialization
    //------------------------------------------------------------------
    public function __construct(
        AutomateStandaloneRating $model = null
    ) {
        $this->model = $model ?: new AutomateStandaloneRating();
    }

    //------------------------------------------------------------------
    //  Function 59.1: __construct
    //       GetInnodataFilesHandler Initialization
    //------------------------------------------------------------------
    public function getInnoFiles()
    {
        $innodataWaitingReports = $this->model->getInnodataWaitingReports();

        // Set retrieved reports to in progress immediately
        // foreach ($innodataWaitingReports as $innodataWaitingReport) {
        //     $innodataWaitingReport->status = AutomateStandaloneRating::STATUS_IN_PROGRESS;
        //     $innodataWaitingReport->save();
        // }

        // Process individually
        $result = false;
        $success = [];
        $failed = [];
        $error = 0;
        foreach ($innodataWaitingReports as $waitingReport) {
            $result = null;

            $entityModel = new Entity();
            $entity = $entityModel->find($waitingReport->entity_id);
            if(empty($entity) || !$entity){
                continue;
            }

            try {
                echo 'Searching fs files for ' . $waitingReport->entity_id .
                    ' - ' . $entity->companyname . "\n";

                // Skip innodata upload process, innodata process is deprecated (CDP-1616)
                // $fs = $this->findFsFile($waitingReport->entity_id);]
                
                // Check CreditBPO Folder documents/innodata/from_innodata
                $fsFile = public_path('documents/innodata/from_innodata/' . $waitingReport->entity_id . '/' . $waitingReport->entity_id . ' - ' . $entity->companyname . '.xlsx');

                $dir = public_path('documents/innodata/from_innodata/' . $waitingReport->entity_id .'/');

                if(!file_exists($dir)){
                	echo 'FS input template directory does not exists for report ' . $waitingReport->entity_id . "\n\n";
                	continue;
                }

                $files_dir = File::files($dir);
                if(file_exists($fsFile)){
                    $fs = $this->associateFS($waitingReport->entity_id, $fsFile);
                }elseif(file_exists(public_path('documents/innodata/from_innodata/' . $waitingReport->entity_id)) && count($files_dir) != 0){
                    $fs = "Wrong File Name format. It should be ". $waitingReport->entity_id . ' - ' . $entity->companyname . '.xlsx' ;
                }

                if(!$fs) {
                    $result = null;
                }

                if ($fs == STS_OK) {
                    $result = STS_OK;
                }

                if ($fs && $fs != STS_OK) {
                    $result = $fs;
                }
                
            } catch (Exception $e) {
                $this->logError($e);
            }
            if ($result == STS_OK) {
                $waitingReport->status = AutomateStandaloneRating::STATUS_SUCCESS;
                
                /** Internal Rating Process */
                if ($entity->is_premium == SIMPLIFIED_REPORT) {
                    $simplifiedRatingHandler = new AutomateSimplifiedRatingHandler();
                    $simplifiedRatingHandler->processReport($waitingReport->entity_id);
                } else {
                    $keyRatio = new KeyRatioService();
                    $keyRatio->generateKeyRatioByEntityId($waitingReport->entity_id);
                }

                $standaloneRatingHandler = new AutomateStandaloneRatingHandler();
                $standaloneRatingHandler->processSingleReport($waitingReport->entity_id);

                /** Industry Comparison */
                $reportIndustryComparison = new IndustryComparison();
                $dataic = $reportIndustryComparison->getIndustryComparisonReport($waitingReport->entity_id);

                // send alert emails regarding finished scoring questionnaire
	            $financialAnalysisHandler = new FinancialAnalysisHandler();
	            $financialAnalysisHandler->emailPostFinishScoring($waitingReport->entity_id);

                Entity::where("entityid", $entity->entityid)->update(array("status" => 7));
                echo 'Result: Success getting and uploading FS file and processing the report.' . "\n\n";
            } 
            elseif(isset($result['error']) || $result){
                // $this->deleteOldErrorFile($waitingReport->entity_id);
                $filename_err = 'Error ' . preg_replace("/[^a-zA-Z]+/", " ", $entity->companyname).'.txt';
                $error_txt = '';

                if(isset($result['error'][0])){
                    $error_txt = $result['error'][0];
                }else{
                    $error_txt = $result;
                }

                if(file_exists(public_path('documents/innodata/from_innodata/'.$waitingReport->entity_id.'/'.$filename_err))){
                    if(!file_exists('documents/innodata/from_innodata/'.$waitingReport->entity_id.'/'.$filename_err)){
                        $file = fopen(public_path('documents/innodata/from_innodata/'.$waitingReport->entity_id.'/'.$filename_err), "w");
                    }else{
                        $file = fopen(public_path('documents/innodata/from_innodata/'.$waitingReport->entity_id.'/'.$filename_err), "a+");
                    }

                    $size = filesize(public_path('documents/innodata/from_innodata/'.$waitingReport->entity_id.'/'.$filename_err));
                    fwrite($file, $error_txt);
                    fclose($file);
                }else{
                    File::put(public_path('documents/innodata/from_innodata/' . $waitingReport->entity_id.'/'.$filename_err), $error_txt);
                }

                // $this->uploadErrorFile($filename_err, $waitingReport->entity_id);
                $waitingReport->status = AutomateStandaloneRating::STATUS_INNODATA_WAITING;
                echo 'Result: Error on file uploaded.' . "\n\n";
            } 
            else{
                $waitingReport->status = AutomateStandaloneRating::STATUS_INNODATA_WAITING;
                echo 'Result: No FS yet' . "\n\n";
            }

            $waitingReport->save();
        }
    }

    /**
     * @param Exception $error
     * @return void
     */
    protected function logError($error)
    {
        Log::error($error);
    }

    public function findFsFile($entity_id){
        $entity = Entity::where('entityid', $entity_id)->first();
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") === "Production"){
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$entity_id);
        }else{
            $fileExists = file_exists('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'.$entity_id);
        }

        if (!$fileExists) {
            return false;
        }

        $localInnodataPath = public_path('documents/innodata/from_innodata/');
        $localPath = public_path('documents/innodata/from_innodata/'.$entity_id.'/');
        if (!file_exists(public_path('documents/innodata/'))) {
            mkdir(public_path('documents/innodata/',0777));
        }
        
        if (!file_exists($localInnodataPath)) {
            mkdir($localInnodataPath,0777);
        }

        if (!file_exists($localPath)) {
            mkdir($localPath,0777);
        }

        if(env("APP_ENV") == "Production"){
            $dir = opendir('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$entity_id);
            $remoteDir = '/FROM_INNO/'.$entity_id.'/';
        }else{
            $dir = opendir('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'.$entity_id);
            $remoteDir = '/TEST_FOLDER/_From Innodata/'.$entity_id.'/';
        }

        $files = array();
        while (false !== ($file = readdir($dir)))
        {
            if ($file == "." || $file == "..")
                continue;
            $files[] = $file;
        }

        try {
            $isInnoFile = false;
            foreach ($files as $file)
            {
                $filename_orig_upper = $entity->entityid . ' - ' .strtoupper($entity->companyname) . '.xlsx';
                $filename_orig_uc = $entity->entityid . ' - ' .ucwords($entity->companyname) . '.xlsx';
                $filename_orig_lower = $entity->entityid . ' - ' .strtolower($entity->companyname) . '.xlsx';
                $filename_orig_origin = $entity->entityid . ' - ' . $entity->companyname . '.xlsx';

                if(($filename_orig_origin == $file) || ($filename_orig_upper == $file) || ($filename_orig_lower == $file) || ($filename_orig_uc == $file) ){
                    $isInnoFile = true;
                    $copyfile = $file;
                    echo "Copying file: $file\n";
                    if (!$remote = @fopen("ssh2.sftp://{$sftp}/{$remoteDir}{$file}", 'r'))
                    {
                        @fclose($remote);
                        throw new Exception("Unable to open remote file: $file\n");
                    }
        
                    if (!$local = @fopen($localPath . $file, 'w'))
                    {
                        @fclose($local);
                        throw new Exception("Unable to create local file: $file\n");
                    }
        
                    $read = 0;
                    $filesize = filesize("ssh2.sftp://{$sftp}/{$remoteDir}{$file}");
                    while ($read < $filesize && ($buffer = fread($remote, $filesize - $read)))
                    {
                        $read += strlen($buffer);
                        if (fwrite($local, $buffer) === FALSE)
                        {
                            throw new Exception("Unable to write to local file: $file\n");
                        }
                    }
                    fclose($local);
                    fclose($remote);
                }
            }

            /** Check inno folder if empty */
            if((count($files) == 0) && $isInnoFile == false){
                return;
            }

            /** Check if inno folder has file but file name format is wrong */
            if($files && $isInnoFile == false){
                $err_message = "File Format should be '" . $entity->entityid . " - " .$entity->companyname . ".xlsx'";
                return $err_message;
            }

        } catch (Exception $e) {
            error_log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }

        if(empty($copyfile))
        	$copyfile = $entity->entityid . ' - ' .$entity->companyname . '.xlsx';
        $fsFile = public_path(). "/documents/innodata/from_innodata/".$entity->entityid."/".$copyfile;
        $associateFS = $this->associateFS($entity_id, $fsFile);

        if (!$associateFS) {
            return false;
        }
        if ($associateFS && $associateFS != STS_OK){
            return $associateFS;
        }

        return STS_OK;
    }

    public function associateFS($entity_id, $fsFile) {
        echo "Associating file to report: ".basename($fsFile)."\n";
        echo $fsFile . "\n";
        
        $entity = Entity::where('entityid', $entity_id)->first();
        $extension = pathinfo($fsFile, PATHINFO_EXTENSION);

        // Get basic settings of associated file
        $transcribe_fs = TranscribeFS::where('entityid', $entity_id)->first();
        $fs_setting = json_decode($transcribe_fs->fs_setting);

        $currency = !empty($fs_setting->currency) ? $fs_setting->currency : "PHP";
        $unit = !empty($fs_setting->unit) ? $fs_setting->unit : 1;
        $document_type = !empty($fs_setting->document_type) ? $fs_setting->document_type : 1;
        $upload_type = 5;
        $industryId = $entity->industry_main_id;

        $reportFileFormat = 'Raw FS %s as of %s.%s';

        // zip file upload batch
        $destinationPath = public_path().'/documents/'; 			// get directory

        try {
            /** Create new instance of zip file */
            $zip = new ZipArchive();

            /** Set File Name */
            $zip_filename = str_replace(' ', '_', $entity_id.' - '.'Balance_Sheet_Income_Statement-' . date('Y-m-d') . '.zip'); 

            /** Attempt to open Zip File */
            if ($zip->open($destinationPath.$zip_filename, ZipArchive::CREATE)!==TRUE) {
                throw new Exception("cannot open <$zip_filename>\n");
            }
            
            $extension = pathinfo($fsFile, PATHINFO_EXTENSION);
            $document_group = 21;
            $document_orig = $entity_id.' - FS Input Template for the period of ';
    
            $filename = rand(11111,99999).time().'.'.$extension;
            \File::copy($fsFile, base_path('temp/'.$filename));
    
            // ---------------------------------------
            // Process if file uploaded is FS Template
            // ---------------------------------------
    
            if ($upload_type == 5 && in_array($extension, ['xlsx', 'xls'])) {
                $simpleFsData = [
                    'entity_id' => $entity_id,
                    'money_factor' => $unit,
                    'currency' => $currency,
                    'industry_id' => $industryId,
                    'country_code'	=> $entity->country_code
                ];
                $simpleFsInput = new SimpleFsInput('temp/'.$filename, $simpleFsData);
                $result = $simpleFsInput->save();
                if (!$result) {
                    $error = array(
                        'status' => STS_NG,
                        'error'  => $simpleFsInput->getErrorMessages()
                    );
                    return $error;
                } else {
                    $complexFsInput = new ComplexFsInput(
                        public_path(self::FS_INPUT_COMPLEX),
                        ['entity_id' => $entity_id]
                    );
                    $complexFsInput->setDocumentGenerated(
                        $simpleFsInput->getDocumentGenerated()
                    );
                    $result = $complexFsInput->write($simpleFsInput->getCellValues());
                    if ($result) {

                        $simpleFsInput->verifyYearOfUploadedFS();
                        if(!empty($simpleFsInput->getErrorMessages())){
                            $error = array(
                                'status' => STS_NG,
                                'error'  => $simpleFsInput->getErrorMessages()
                            );
                            return $error;
                        }

                        $document_orig = $simpleFsInput->getDocumentFileName();
                        $file = $complexFsInput->getDocumentFileName();
                        $assets = $simpleFsInput->getTotalAssets();
                        $srv_resp['total_asset'] = number_format($assets, 2);
                        $srv_resp['total_asset_grouping'] = AssetGrouping::getByAmount($assets);
                        if($document_group == 21) {
                            $cond = array(
                                "is_deleted" => 0,
                                "entity_id" => $entity_id,
                                "document_group" => $document_group
                            );
                            $curDocs = Document::where($cond)->get();
                            foreach ($curDocs as $key => $value) {
                                $value->is_deleted = 1;
                                $value->save;
                                Document::where("documentid", $value->documentid)->update(["is_deleted"=> 1]);
                            }
                        }
                        /* create entry on database */
                        $data = [
                            'entity_id' => $entity_id,
                            'document_type'=> $document_type,
                            'upload_type'=> $upload_type,
                            'document_group'=> $document_group,
                            'document_orig'=> $file,
                            'document_rename'=> $zip_filename,
                            'created_at'=> date('Y-m-d H:i:s'),
                            'updated_at'=> date('Y-m-d H:i:s'),
                        ];
                        $documentid = DB::table('tbldocument')->insertGetId($data);
    
                        $master_ids[]       = $documentid;
                        $filenames[]        = $file;
                        $display_names[]    = $file;
                        $where = array(
                            "entity_id" => $entity_id,
                            "is_deleted" => 0
                        );
                        $fs = FinancialReport::where($where)->first();
                        if($fs->count() >= 1) {
                            $srv_resp['fsid'] = $fs->id;
                        }
                        $zip->addFile(public_path('temp/'.$file), $file);
                        $srv_resp['sts'] = STS_OK;
                    }
                    return STS_OK;
                }
            } elseif($upload_type == 5 && $extension != 'xlsx'){
                throw new Exception("The file you were trying to upload is not a Financial Statement Template.  Please download the file on the link provided.\n");
            } else {
                $srv_resp['sts']    = STS_OK;
            }
    
            if($srv_resp['sts'] == STS_OK){
                /** create entry on database */
                $data = array(
                    'entity_id'=>$entity_id,
                    'document_type'=>$document_type,
                    'upload_type'=>$upload_type,
                    'document_group'=>$document_group,
                    'document_orig'=>$document_orig,
                    'document_rename'=>$zip_filename,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                );
                $documentid = DB::table('tbldocument')->insertGetId($data);
    
                $master_ids[]       = $documentid;
                $filenames[]        = $document_orig;
                $display_names[]    = $document_orig;
                $zip->addFile('temp/'.$filename, $document_orig); // add the upload file to zip file
            }
            
            /** Close the zip file */
            $zip->close();

        } catch (Exception $e) {
            error_log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }

        return STS_OK;

    }

    public function uploadErrorFile($local_file, $entity_id){

        //** Connect to the server */
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        /** Login Innodata Server Credentials */
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") == "Production"){
            $remote_path = "/FROM_INNO/" . $entity_id . '/' . $local_file;
        }else{
            $remote_path = "/TEST_FOLDER/_From Innodata/" . $entity_id . '/' . $local_file;
        }

        $sftpStream = @fopen('ssh2.sftp://'.(int)$sftp.$remote_path, 'w');

        try {

            if (!$sftpStream) {
                throw new Exception("Could not open remote file: $remote_path");
            }
           
            $data_to_send = @file_get_contents(public_path('temp/' . $local_file));
           
            if ($data_to_send === false) {
                throw new Exception("Could not open local file: $local_file.");
            }
           
            if (@fwrite($sftpStream, $data_to_send) === false) {
                throw new Exception("Could not send data from file: $local_file.");
            }
           
            fclose($sftpStream);

            /** Delete temporary error file */
            File::delete(public_path('temp/'. $local_file));

            return;
                           
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            @fclose($sftpStream);
            return $e->getMessage();
        }
    }

    public function deleteOldErrorFile($entityid){
        $entity = Entity::where('entityid', $entityid)->first();
        // connect to server
        $connection = ssh2_connect('ftpnda.innodata.com', 22);
    
        // login
        $login = ssh2_auth_password($connection, 'FTP-CreditBPO', 'BPO^%$#@!');
        $sftp = ssh2_sftp($connection);

        if(env("APP_ENV") == "Production"){
            $dir = opendir('ssh2.sftp://' . $sftp . '/FROM_INNO/'.$entityid);
            $remoteDir = '/FROM_INNO/'.$entityid.'/';
        }else{
            $dir = opendir('ssh2.sftp://' . $sftp . '/TEST_FOLDER/_From Innodata/'. $entityid);
            $remoteDir = '/TEST_FOLDER/_From Innodata/'.$entityid.'/';
        }

        $files = array();
        while (false !== ($file = readdir($dir)))
        {
            if ($file == "." || $file == "..")
                continue;
            $files[] = $file;
        }

        foreach($files as $err_file){
            /** Find the error file */
            if(str_contains($err_file, "Error ")){
                // Delete old error
                $file_directory = $remoteDir . $err_file;
                ssh2_sftp_unlink($sftp, $file_directory);
            }
        }
        return;
    }
}
