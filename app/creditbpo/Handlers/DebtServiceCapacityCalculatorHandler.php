<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\DebtServiceCapacityCalculator;

class DebtServiceCapacityCalculatorHandler {
    protected $model;

    /**
     * @param DebtServiceCapacityCalculator|null
     */
    public function __construct(DebtServiceCapacityCalculator $model = null)
    {
        $this->model = $model ?: new DebtServiceCapacityCalculator();
    }

    public function getByEntityId($entityId)
    {
        return $this->model->find($entityId);
    }

    public function updateDscr($updatedDscr)
    {
        $this->model->replace($updatedDscr);

        return true;
    }
}
