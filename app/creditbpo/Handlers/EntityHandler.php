<?php

namespace CreditBPO\Handlers;

use Illuminate\Auth\UserInterface;
use CreditBPO\Models\Entity;

class EntityHandler {
    const ENTITY_STATUS_IN_PROGRESS = 0;
    const ENTITY_STATUS_SUBMITTED = 5;
    const ENTITY_STATUS_REVIEWING = 6;
    const ENTITY_STATUS_COMPLETE = 7;
    const MANAGEMENT_TWO_YEARS_OR_LESS_EXPERIENCE = 7;
    const MANAGEMENT_THREE_TO_FOUR_YEARS_EXPERIENCE = 14;
    const MANAGEMENT_MORE_THAN_FIVE_YEARS_EXPERIENCE = 21;

    /**
     * @param Entity|null $model
     */
    public function __construct(Entity $model = null) {
        $this->model = $model ?: new Entity();
    }

    public function getBankIndustry($id) {
        return $this->model
            ->join(
                'tblbankindustryweights',
                'tblentity.industry_main_id',
                '=',
                'tblbankindustryweights.main_code')
            ->where('tblentity.entityid', '=', $id)
            ->first();
    }

    /**
     * @return array
     */
    public function getAllTrialAccounts() {
        return $this->model
                ->join('tbllogin', 'tbllogin.loginid', '=', 'tblentity.loginid')
                ->join('tblbank', 'tblbank.id', '=', 'tblentity.current_bank')
                ->where('tblentity.isIndustry', 1)
                ->where('tblentity.status', 7)
                ->where('tblbank.isTest', 0)
                ->where('tblentity.current_bank', '!=', '')
                ->orderBy('tblentity.companyname', 'asc')
                ->get();
    }

    /**
     * @return array
     */
    public function getAllTrialAccountsLists() {
        return $this->getAllTrialAccounts()->pluck('companyname', 'entityid');
    }

    /**
     * @param int $entityId
     * @return EntityHandler
     */
    public function setEntityStatus(
        $entityId,
        $status = self::ENTITY_STATUS_IN_PROGRESS
    ) {
        $entity = $this->model->find($entityId);

        if ($entity) {
            $entity->status = $status;
            $entity->save();
        }

        return $this;
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getManagementTeamExperienceScore($entityId) {
        $entity = $this->model->find($entityId);

        if ($entity->number_year_management_team >= 5) {
            return self::MANAGEMENT_MORE_THAN_FIVE_YEARS_EXPERIENCE;
        }

        if ($entity->number_year_management_team >= 3) {
            return self::MANAGEMENT_THREE_TO_FOUR_YEARS_EXPERIENCE;
        }

        return self::MANAGEMENT_TWO_YEARS_OR_LESS_EXPERIENCE;
    }
}
