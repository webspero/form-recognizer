<?php

namespace CreditBPO\Handlers;

use Illuminate\Support\Facades\Mail;
use CreditBPO\DTO\ReadyRatioDTO;
use CreditBPO\DTO\SMEScoringDTO;
use CreditBPO\Exceptions\FinancialAnalysisHandlerException;
use CreditBPO\Handlers\BusinessOutlookIndexHandler;
use CreditBPO\Handlers\BusinessOwnerExperienceHandler;
use CreditBPO\Handlers\FinancialAnalysisInternalHandler;
use CreditBPO\Handlers\DebtServiceCapacityCalculatorHandler;
use CreditBPO\Handlers\EntityHandler;
use CreditBPO\Handlers\FinancialConditionHandler;
use CreditBPO\Handlers\MajorCustomerHandler;
use CreditBPO\Handlers\MajorSupplierHandler;
use CreditBPO\Handlers\OCRHandler;
use CreditBPO\Handlers\OperatingCashFlowHandler;
use CreditBPO\Handlers\ReadyRatiosScraperHandler;
use CreditBPO\Handlers\RiskAssessmentHandler;
use CreditBPO\Handlers\SustainabilityHandler;
use CreditBPO\Handlers\VfaHandler;
use CreditBPO\Models\Bank;
use CreditBPO\Models\Entity;
use CreditBPO\Models\FinancialReport;
use CreditBPO\Models\Document;
use CreditBPO\Models\Login;
use CreditBPO\Models\ReadyRatio;
use CreditBPO\Models\SMEScore;
use CreditBPO\Services\Report\PDF\FinancialAnalysis as FinancialAnalysisPDF;
use CreditBPO\Services\Report\PDF\SAL as SALPDF;
use CreditBPO\Services\UpdateSmeReport;
use Illuminate\Support\Facades\Log;
use User;
use DB;
use PDFMerger;
use Illuminate\Support\Facades\Redirect;
use \Exception as Exception;

class FinancialAnalysisHandler {
    const TIER_LOW = 'low';
    const TIER_MID = 'mid';
    const TIER_TOP = 'top';

    const RISK_SCORE_EXCELLENT = 'Excellent';
    const RISK_SCORE_STRONG = 'Strong';
    const RISK_SCORE_GOOD = 'Good';
    const RISK_SCORE_SATISFACTORY = 'Satisfactory';
    const RISK_SCORE_ACCEPTABLE = 'Some Vulnerability'; 
    const RISK_SCORE_WATCHLIST = 'Watchlist';
    const RISK_SCORE_SPECIAL_MENTION = 'Special mention';
    const RISK_SCORE_SUBSTANDARD = 'Substandard';
    const RISK_SCORE_DOUBTFUL = 'Doubtful';
    const RISK_SCORE_LOSS = 'Loss';

    const DEFAULT_LOGIN_ID = 76;

    /**
     * @param int $financialReportId
     * @return bool
     */
    public function createReport($financialReportId) {
        $pdf = new FinancialAnalysisPDF();
        $contents = $pdf->create($financialReportId)->generate();

        if ($contents) {
            $contents->setPaper('letter');
            $contents->setOrientation('portrait');
            $pdf->save();
            $pdf->deletePCharts();

            return true;
        }

        return false;
    }

    /**
     * @param int $financialReportId
     * @return bool
     */
    public function createSALReport($financialReportId) {
        $pdf = new SALPDF();
        $contents = $pdf->create($financialReportId)->generate();

        if ($contents) {
            $contents->setPaper('a4', 'portrait');
            $pdf->save();

            return $pdf->getFilenamePath($financialReportId);
        }
        
        return false;
    }

    /**
     * @param int $entityId
     * @return bool
     */
    public function createReportByEntityId($entityId) {
        $financialReport = FinancialReport::where(['entity_id' => $entityId,'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        return $this->createReport($financialReport->id);
    }

    public function createReportFromReadyRatios($entityId) {
        $vfaHandler = new VfaHandler();
        $readyRatiosScraper = new ReadyRatiosScraperHandler();
        $vfaContent = $vfaHandler->getContentByEntityId($entityId);
        $filename = $this->getFinancialReportPath($entityId);

        $entity = Entity::find($entityId);
        //$isFullReport = Bank::find($entity->current_bank)->is_standalone;
        $isFullReport = false;

        for($i=1;$i<=5;$i++)
        {
            if(file_exists($filename))
            {
                break;
            }            
            sleep(20);
            try {
                $readyRatiosScraper->getReport(
                    $filename,
                    $vfaContent,
                    $isFullReport,
                    $entityId
                );
            }
            catch (\Exception $e) {
                $readyRatiosScraper->getReport(
                    $filename,
                    $vfaContent,
                    $isFullReport,
                    $entityId
                );
            }
        }

        if (!file_exists($filename)) {
            \Log::info('File does not exist: '.$filename.' of entity: '.$entityId);
            return null;
        }
        
        if(strpos($entity->companyname, "&") !== false) {
            $filename = preg_replace("/(And)/", "&", $filename);
        }

        Document::create([
            'entity_id' => $entityId,
            'document_type' => Document::TYPE_FINANCIAL_REPORT,
            'document_group' => Document::GROUP_PRIVATE_DOCUMENT,
            'document_orig' => basename($filename),
            'document_rename' => basename($filename),
        ]);

        return $filename;
    }

    /**
     * @param int $financialReportId
     * @return string|null
     */
    public function getFinancialReportPath($entityId) {
        $pdf = new FinancialAnalysisPDF();
        return $pdf->getFilenamePath($entityId);
    }

    public function automatePostFinishScoring($entityId) {
        
        $this->createReportFromReadyRatios($entityId);

        //echo 'test'; exit;
        
        $bankStatements = DB::table('tbldocument')
            ->where('entity_id', $entityId)
            ->where('document_group', 51)
            ->get();
        
        //update.  allow trial basic user to process report with bank statements and utility bills
        $basicUserTrial = DB::table("tblentity")
            ->select("tbllogin.trial_flag")
            ->leftjoin("tbllogin", "tbllogin.loginid", "=", "tblentity.loginid")
            ->where("tblentity.entityid",$entityId)
            ->first();

        // if (@count($bankStatements) > 0 && $basicUserTrial->trial_flag != 1) {
        //     // $insert = array(
        //     //     "entity_id" => $entityId,
        //     //     "data" => "Utility Bills and Balance Sheet Documents Exist",
        //     //     "error" => "Utility Bills and Balance Sheet Documents Exist",
        //     //     "status" => 0,
        //     //     "is_deleted" => 0,
        //     //     "created_at" => date('Y-m-d H:i:s'),
        //     //     "updated_at" => date('Y-m-d H:i:s')
        //     // );

        //     // DB::table("automate_standalone_rating_logs")->insert($insert);


        //     // return true;
        // }

        $sme = $this->getSMEScoringByEntityId($entityId);
        $readyRatios = $this->getReadyRatioDataByEntityId($entityId);
        return $this->postFinishScoring(
            $entityId,
            $sme,
            $readyRatios
        );
    }

    public function automatePostFinishScoringInternal($entityId) {
        
        $bankStatements = DB::table('tbldocument')
            ->where('entity_id', $entityId)
            ->where('document_group', 51)
            ->get();
        
        //update.  allow trial basic user to process report with bank statements and utility bills
        $basicUserTrial = DB::table("tblentity")
            ->select("tbllogin.trial_flag")
            ->leftjoin("tbllogin", "tbllogin.loginid", "=", "tblentity.loginid")
            ->where("tblentity.entityid",$entityId)
            ->first();

        $sme = $this->getSMEScoringByEntityId($entityId);


        return $this->postFinishScoringInternal(
            $entityId,
            $sme
        );
    }

    public function postFinishScoringInternal(
        $entityId,
        SMEScoringDTO $sme,
        $isAdminReview = false
    ) {
        $smescore = new SMEScore();
        $smescore->loginid = $sme->getLoginId();
        $smescore->entityid = $sme->getEntityId();
        $smescore->score_rm = $sme->getRiskManagement();
        $smescore->score_cd = $sme->getCustomerDependency();
        $smescore->score_sd = $sme->getSupplierDependency();
        $smescore->score_bois = $sme->getBusinessOutlookIndexScore();
        $smescore->score_boe = $sme->getBusinessOwnerExperience();
        $smescore->score_mte = $sme->getManagementTeamExperience();
        $smescore->score_sp = $sme->getSuccessionPlan();
        $smescore->score_rfp = $sme->getFinancialPosition();
        $smescore->score_rfpm = $sme->getFinancialPerformance();
        $smescore->score_fars = $sme->getRawScore();
        $smescore->score_facs = $sme->getConvertedScore();
        $smescore->score_sf = $sme->getRiskScore();
        $smescore->average_daily_balance = $sme->getAverageDailyBalance();
        $smescore->operating_cashflow_margin = $sme->getOperatingCashFlowMargin();
        $smescore->operating_cashflow_ratio = $sme->getOperatingCashFlowRatio();
        $smescore->score_bdms = $sme->getMainDependency();
        $smescore->score_ppfi = $sme->getPastProjectFutureInitiatives();
        $smescore->save();

        // $smeReport = new UpdateSmeReport();
        // $smeReport->updateExistingReport($entityId, true);

        Entity::where('entityid', '=', $entityId)->update([
            'status' => 7,
            'admin_review_flag' => $isAdminReview ? 1 : 0,
            'completed_date' => date('c'),
        ]);
        // $this->emailPostFinishScoring($entityId);

        return true;
    }

    public function postFinishScoring(
        $entityId,
        SMEScoringDTO $sme,
        ReadyRatioDTO $readyRatio,
        $isAdminReview = false
    ) {
        $smescore = new SMEScore();
        $smescore->loginid = $sme->getLoginId();
        $smescore->entityid = $sme->getEntityId();
        $smescore->score_rm = $sme->getRiskManagement();
        $smescore->score_cd = $sme->getCustomerDependency();
        $smescore->score_sd = $sme->getSupplierDependency();
        $smescore->score_bois = $sme->getBusinessOutlookIndexScore();
        $smescore->score_boe = $sme->getBusinessOwnerExperience();
        $smescore->score_mte = $sme->getManagementTeamExperience();
        $smescore->score_sp = $sme->getSuccessionPlan();
        $smescore->score_rfp = $sme->getFinancialPosition();
        $smescore->score_rfpm = $sme->getFinancialPerformance();
        $smescore->score_fars = $sme->getRawScore();
        $smescore->score_facs = $sme->getConvertedScore();
        $smescore->score_sf = $sme->getRiskScore();
        $smescore->average_daily_balance = $sme->getAverageDailyBalance();
        $smescore->operating_cashflow_margin = $sme->getOperatingCashFlowMargin();
        $smescore->operating_cashflow_ratio = $sme->getOperatingCashFlowRatio();
        $smescore->score_bdms = $sme->getMainDependency();
        $smescore->score_ppfi = $sme->getPastProjectFutureInitiatives();
        $smescore->save();

        $readyratiodata = new ReadyRatio();
        $readyratiodata->loginid = $readyRatio->getLoginId();
        $readyratiodata->entityid = $readyRatio->getEntityId();
        $readyratiodata->year1 = $readyRatio->getPreviousYear();
        $readyratiodata->year2 = $readyRatio->getCurrentYear();
        $readyratiodata->gross_revenue_growth1 = $readyRatio->getPreviousGrossRevenueGrowth();
        $readyratiodata->gross_revenue_growth2 = $readyRatio->getCurrentGrossRevenueGrowth();
        $readyratiodata->net_income_growth1 = $readyRatio->getPreviousNetIncomeGrowth();
        $readyratiodata->net_income_growth2 = $readyRatio->getCurrentNetIncomeGrowth();
        $readyratiodata->gross_profit_margin1 = $readyRatio->getPreviousGrossProfitMargin();
        $readyratiodata->gross_profit_margin2 = $readyRatio->getCurrentGrossProfitMargin();
        $readyratiodata->net_profit_margin1 = $readyRatio->getPreviousNetProfitMargin();
        $readyratiodata->net_profit_margin2 = $readyRatio->getCurrentNetProfitMargin();
        $readyratiodata->net_cash_margin1 = $readyRatio->getPreviousNetCashMargin();
        $readyratiodata->net_cash_margin2 = $readyRatio->getCurrentNetCashMargin();
        $readyratiodata->current_ratio1 = $readyRatio->getPreviousRatio();
        $readyratiodata->current_ratio2 = $readyRatio->getCurrentRatio();
        $readyratiodata->debt_equity_ratio1 = $readyRatio->getPreviousDebtEquityRatio();
        $readyratiodata->debt_equity_ratio2 = $readyRatio->getCurrentDebtEquityRatio();
        $readyratiodata->cc_year1 = $readyRatio->getCashConversionYearOne();
        $readyratiodata->cc_year2 = $readyRatio->getCashConversionYearTwo();
        $readyratiodata->cc_year3 = $readyRatio->getCashConversionYearThree();
        $readyratiodata->receivables_turnover1 = $readyRatio->getReceivablesTurnoverOne();
        $readyratiodata->receivables_turnover2 = $readyRatio->getReceivablesTurnoverTwo();
        $readyratiodata->receivables_turnover3 = $readyRatio->getReceivablesTurnoverThree();
        $readyratiodata->inventory_turnover1 = $readyRatio->getInventoryTurnoverOne();
        $readyratiodata->inventory_turnover2 = $readyRatio->getInventoryTurnoverTwo();
        $readyratiodata->inventory_turnover3 = $readyRatio->getInventoryTurnoverThree();
        $readyratiodata->accounts_payable_turnover1 = $readyRatio->getAccountsPayableTurnoverOne();
        $readyratiodata->accounts_payable_turnover2 = $readyRatio->getAccountsPayableTurnoverTwo();
        $readyratiodata->accounts_payable_turnover3 = $readyRatio->getAccountsPayableTurnoverThree();
        $readyratiodata->cash_conversion_cycle1 = $readyRatio->getCashConversionCycleOne();
        $readyratiodata->cash_conversion_cycle2 = $readyRatio->getCashConversionCycleTwo();
        $readyratiodata->cash_conversion_cycle3 = $readyRatio->getCashConversionCycleThree();
        $readyratiodata->save();
        $smeReport = new UpdateSmeReport();
        $smeReport->updateExistingReport($entityId, true);

        Entity::where('entityid', '=', $entityId)->update([
            'status' => 7,
            'admin_review_flag' => $isAdminReview ? 1 : 0,
            'completed_date' => date('c'),
        ]);
        // $this->emailPostFinishScoring($entityId);

        return true;
    }

    public function emailPostFinishScoring($entityId) {
        $entity = Entity::where('entityid', '=', $entityId)->first();
        
        $period = date('Y-m-d', strtotime($entity->submitted_at));

        $emailCc = array();
        $userBank = null;

        // Generate Internal Rating and Rating Summary PDF
        $this->generateRatingSummaryAndInternalRatingPDF($entityId);

        if ($entity->current_bank != 0) {
            $userBank  = User::where('tbllogin.bank_id', $entity->current_bank)
                ->leftJoin('tblbanksubscriptions as bs', "tbllogin.loginid", 'bs.bank_id')
                ->where('role', 4)
                ->where('parent_user_id', 0)
                ->where('tbllogin.is_delete', 0)
                ->whereRaw(" bs.expiry_date > NOW()")
                ->get()->toArray();
            if(empty($userBank)) {
                $userBank  = User::where('bank_id', $entity->current_bank)
                    ->where('role', 4)
                    ->where('parent_user_id', 0)
                    ->where('tbllogin.is_delete', 0)
                    ->get()->toArray();
            }
            foreach ($userBank as $key => $user) {
                $emailCc[]= $user['email'];
            }
        } 

        $user = User::where('loginid', '=', $entity->loginid)->first();
        $emailTo = $user->email;

        if (env("APP_ENV") != "local") {
            try{
                // send email to supervisor
                Mail::send('emails.finishscoring',['type'=>'7', 'entity' => $entity],
                    function($message) use ($entity, $userBank, $emailTo, $emailCc) {
                        if($emailCc) {
                            $message->to($emailCc)
                                ->subject('CreditBPO Report Available: ' . $entity->companyname);
                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                        }
                });
            }catch(Exception $e){
                //
            }

            try{
                // send notification to basic user
                Mail::send('emails.finish_scoring_user', ['type'=>'7', 'entity' => $entity],
                    function($message) use ($entity, $userBank, $emailTo, $emailCc) {
                        if ($userBank) {
                            $message->to($emailTo, $emailTo)
                                ->subject($entity->companyname.' Rating Report Is Now Available!');

                            // if (!empty($emailCc)) {
                            //     $message->cc($emailCc);
                            // }

                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                        }
                });
            }catch(Exception $e){
                //
            }
        }

        return true;
    }

    public function getSMEScoringByEntityId(
        $entityId, $loginId = self::DEFAULT_LOGIN_ID
    ) {
        $assessmentHandler = new RiskAssessmentHandler();
        $businessOutlookHandler = new BusinessOutlookIndexHandler();
        $businessOwnerHandler = new BusinessOwnerExperienceHandler();
        $customerHandler = new MajorCustomerHandler();
        $entityHandler = new EntityHandler();
        $financialConditionHandler = new FinancialConditionHandler();
        $ocrHandler = new OCRHandler();
        $operatingCashFlowHandler = new OperatingCashFlowHandler();
        $supplierHandler = new MajorSupplierHandler();
        $sustainabilityHandler = new SustainabilityHandler();

        $entityBank = $entityHandler->getBankIndustry($entityId); 
        $risk = $assessmentHandler->getRiskManagementScoreByEntityId($entityId);
        $customerDependency = $customerHandler->getAssessmentScoreByEntityId($entityId);
        $supplier = $supplierHandler->getAssessmentScoreByEntityId($entityId);
        $businessOutlook = $businessOutlookHandler->getAssessmentScoreByEntityId($entityId);
        $businessExperience = $businessOwnerHandler->getAssessmentScoreByEntityId($entityId);
        $managementExperience = $entityHandler->getManagementTeamExperienceScore($entityId);
        $successionPlan = $sustainabilityHandler->getAssessmentScoreByEntityId($entityId);
        $financialCondition = $financialConditionHandler->getFinancialConditionByEntityId(
            $entityId);
        
        $financialCreditScore = $this->getFinancialCreditScore(
            $entityBank,
            $risk,
            $customerDependency,
            $supplier,
            $businessOutlook,
            $businessExperience,
            $managementExperience,
            $successionPlan,
            $financialCondition['condition_score']
        );
        $operatingCashFlow = $operatingCashFlowHandler->getMarginAndRatioByEntityId( $entityId);

        $averageDailyBalance = $ocrHandler->getAverageDailyBalance($entityId);

        $sme = new SMEScoringDTO();
        $sme->setLoginId($loginId)
            ->setEntityId($entityId)
            ->setRiskManagement($risk)
            ->setCustomerDependency($customerDependency)
            ->setSupplierDependency($supplier)
            ->setBusinessOutlookIndexScore($businessOutlook)
            ->setBusinessOwnerExperience($businessExperience)
            ->setManagementTeamExperience($managementExperience)
            ->setSuccessionPlan($successionPlan)
            ->setFinancialPosition($financialCondition['position_score'])
            ->setFinancialPerformance($financialCondition['performance_score'])
            ->setRawScore($financialCondition['condition_range'])
            ->setConvertedScore($financialCondition['condition_score'])
            ->setRiskScore($financialCreditScore['group_score_with_word_value'])
            ->setAverageDailyBalance($averageDailyBalance)
            ->setOperatingCashFlowMargin($operatingCashFlow['margin'])
            ->setOperatingCashFlowRatio($operatingCashFlow['ratio'])
            ->setMainDependency(0)
            ->setPastProjectFutureInitiatives(0);

        return $sme;
    }

    /**
     * @param int $entityId
     * @param int $loginId
     * @return array|null
     */
    public function getReadyRatioDataByEntityId(
        $entityId, $loginId = self::DEFAULT_LOGIN_ID
    ) {
        $financialReport = FinancialReport::where(['entity_id' => $entityId, 'is_deleted' => 0])
            ->orderBy('id', 'desc')
            ->first();

        if (@count($financialReport) <= 0) {
            return null;
        }

        $incomeStatements = $financialReport->incomeStatements()
            ->orderBy('index_count', 'asc')
            ->get();
        $balanceSheets = $financialReport->balanceSheets()
            ->orderBy('index_count', 'asc')
            ->get();
        $cashFlow = $financialReport->cashFlow()
            ->orderBy('index_count', 'asc')
            ->get();
        $values = $this->getReadyRatioVars(
            $incomeStatements,
            $balanceSheets,
            $cashFlow
        );
        $receivablesTurnover = $this->getReceivablesTurnover(
            $incomeStatements,
            $balanceSheets);
        $inventoryTurnover = $this->getInventoryTurnover(
            $incomeStatements,
            $balanceSheets);

        $payablesTurnover = $this->getPayablesTurnover(
            $incomeStatements,
            $balanceSheets
        );

        $cashConversionCycle = $this->getCashConversionCyle(
            $receivablesTurnover,
            $inventoryTurnover,
            $payablesTurnover
        );

        $readyRatio = new ReadyRatioDTO();
        $readyRatio->setLoginId($loginId)
            ->setEntityId($entityId)
            ->setPreviousYear($financialReport->year - 1)
            ->setCurrentYear($financialReport->year)
            ->setPreviousGrossRevenueGrowth($values[1]['revenue'])
            ->setCurrentGrossRevenueGrowth($values[0]['revenue'])
            ->setPreviousNetIncomeGrowth($values[1]['net_income'])
            ->setCurrentNetIncomeGrowth($values[0]['net_income'])
            ->setPreviousGrossProfitMargin($values[0]['gross_profit_margin'])
            ->setCurrentGrossProfitMargin($values[1]['gross_profit_margin'])
            ->setPreviousNetProfitMargin($values[0]['net_profit_margin'])
            ->setCurrentNetProfitMargin($values[1]['net_profit_margin'])
            ->setPreviousNetCashMargin($values[0]['net_cash_margin'])
            ->setCurrentNetCashMargin($values[1]['net_cash_margin'])
            ->setPreviousRatio($values[0]['current_ratio'])
            ->setCurrentRatio($values[1]['current_ratio'])
            ->setPreviousDebtEquityRatio($values[0]['debt_equity_ratio'])
            ->setCurrentDebtEquityRatio($values[1]['debt_equity_ratio'])
            ->setCashConversionYearOne($financialReport->year - 2)
            ->setCashConversionYearTwo($financialReport->year - 1)
            ->setCashConversionYearThree($financialReport->year)
            ->setReceivablesTurnoverOne($receivablesTurnover[0])
            ->setReceivablesTurnoverTwo($receivablesTurnover[1])
            ->setReceivablesTurnoverThree($receivablesTurnover[2])
            ->setInventoryTurnoverOne($inventoryTurnover[0])
            ->setInventoryTurnoverTwo($inventoryTurnover[1])
            ->setInventoryTurnoverThree($inventoryTurnover[2])
            ->setAccountsPayableTurnoverOne($payablesTurnover[0])
            ->setAccountsPayableTurnoverTwo($payablesTurnover[1])
            ->setAccountsPayableTurnoverThree($payablesTurnover[2])
            ->setCashConversionCycleOne($cashConversionCycle[0])
            ->setCashConversionCycleTwo($cashConversionCycle[1])
            ->setCashConversionCycleThree($cashConversionCycle[2]);

        return $readyRatio;
    }

    protected function getReadyRatioVars(
        $incomeStatements,
        $balanceSheets,
        $cashFlow
    ) {
        $values = [];

        for ($i = 0; $i < 2; $i++) {
            if (!isset($incomeStatements[$i])) {
                $values[$i]['revenue'] = 0;
                $values[$i]['net_income'] = 0;
                $values[$i]['gross_profit_margin'] = 0;
                $values[$i]['net_profit_margin'] = 0;
            } else {
                $revenue = $incomeStatements[$i]->Revenue ?: 0;
                $netIncome = $incomeStatements[$i]->ComprehensiveIncome ?: 0;
                if ($revenue != 0 && $revenue != 0.0) {
                    $grossProfitMargin = $incomeStatements[$i]->GrossProfit / $revenue;
                    $netProfitMargin =  $incomeStatements[$i]->ProfitLoss / $revenue;
                } else {
                    $grossProfitMargin = 0;
                    $netProfitMargin = 0;
                }

                $values[$i]['revenue'] = $revenue;
                $values[$i]['net_income'] = $netIncome;
                $values[$i]['gross_profit_margin'] = round($grossProfitMargin, 2);
                $values[$i]['net_profit_margin'] = round($netProfitMargin, 2);
            }

            if (!isset($cashFlow[$i])) {
                $values[$i]['net_cash_margin'] = 0;
            } else {
                $values[$i]['net_cash_margin'] = $cashFlow[$i]['NetCashByOperatingActivities'];
            }

            if (!isset($balanceSheets[$i])) {
                $values[$i]['current_ratio'] = 0;
                $values[$i]['debt_equity_ratio'] = 0;
            } else {
                $currentLiabilities = $balanceSheets[$i]->CurrentLiabilities;
                if($currentLiabilities != 0 && $currentLiabilities != 0.0 ){
                 $currentRatio = $currentLiabilities ? $balanceSheets[$i]->CurrentAssets / $currentLiabilities : 0;
                } else {
                    $currentRatio = 0;
                }

                $equity = $balanceSheets[$i]->Equity;
                if($equity != 0 && $equity != 0.0) {
                    $liabilities = $equity ? $balanceSheets[$i]->Liabilities / $equity : 0;
                }else {
                    $liabilities = 0;
                }

                $values[$i]['current_ratio'] = round($currentRatio, 2);
                $values[$i]['debt_equity_ratio'] = round($liabilities, 2);
            }
        }
        return $values;
    }

    protected function getFinancialCreditScore(
        $entity, $risk, $customerDependency, $supplier, $businessOutlook,
        $ownerExperience, $managementExperience, $successionPlan, $financialValue
    ) {
        $totalGroupA = $risk + $customerDependency + $supplier + $businessOutlook;
        $totalGroupB = $ownerExperience + $managementExperience + $successionPlan;
        $totalGroupC = $financialValue;

        $groupAPercentage = $totalGroupA * $entity->business_group / 100;
        $groupBPercentage = $totalGroupB * $entity->management_group / 100;
        $groupCPercentage = $totalGroupC * $entity->financial_group / 100;

        $groupScore = ceil($groupAPercentage) +
            ceil($groupBPercentage) +
            ceil($groupCPercentage);

        $businessTier = $this->getBusinessConsiderationConditionTier($totalGroupA);
        $managementTier = $this->getManagementQualityTier($totalGroupB);
        $financialTier = $this->getFinancialAnalysisTier($totalGroupC);
        $groupScore = $this->boostFinancialCreditScore(
            $entity->entityid,
            $groupScore,
            $businessTier,
            $managementTier,
            $financialTier
        );
        $wordValue = $this->getRiskScoreWordValue($groupScore);

        return [
            'group_score' => $groupScore,
            'word_value' => $wordValue,
            'group_score_with_word_value' => $groupScore.' - '.$wordValue,
        ];
    }

    protected function getBusinessConsiderationConditionTier($groupTotal) {
        if ($groupTotal >= 191) {
            return self::TIER_TOP;
        }

        if ($groupTotal >= 81) {
            return self::TIER_MID;
        }

        return self:: TIER_LOW;
    }

    protected function getManagementQualityTier($groupTotal) {
        if ($groupTotal >= 125) {
            return self::TIER_TOP;
        }

        if ($groupTotal >= 51) {
            return self::TIER_MID;
        }

        return self::TIER_LOW;
    }

    protected function getFinancialAnalysisTier($groupTotal) {
        if ($groupTotal >= 150) {
            return self::TIER_TOP;
        }

        if ($groupTotal >= 96) {
            return self::TIER_MID;
        }

        return self::TIER_LOW;
    }

    protected function boostFinancialCreditScore(
        $entityId,
        $score,
        $businessConsiderationTier,
        $managementQualityTier,
        $financialAnalysisTier
    ) {
        $dscrScore = $this->getDscrBoost($entityId, $score);

        if (
            $businessConsiderationTier === self::TIER_TOP &&
            $managementQualityTier === self::TIER_TOP &&
            $financialAnalysisTier === self::TIER_TOP
        ) {
            return ceil($score * 1.05);
        }

        if (
            $businessConsiderationTier === self::TIER_MID &&
            $managementQualityTier === self::TIER_MID &&
            $financialAnalysisTier === self::TIER_MID
        ) {
            return ceil($score * 1.05);
        }

        if (
            $businessConsiderationTier === self::TIER_LOW &&
            $managementQualityTier === self::TIER_LOW &&
            $financialAnalysisTier === self::TIER_LOW
        ) {
            return ceil($score * 0.95);
        }

        return $score;
    }

    protected function getDscrBoost($entityId, $score) {
        return 0; // temporarily disabled
    }

    public static function getRiskScoreWordValue($score) {
        if ($score >= 177) {
            return self::RISK_SCORE_EXCELLENT;
        }

        if ($score >= 150) {
            return self::RISK_SCORE_STRONG;
        }

        if ($score >= 123) {
            return self::RISK_SCORE_GOOD;
        }

        if ($score >= 96) {
            return self::RISK_SCORE_SATISFACTORY;
        }

        if ($score >= 68) {
            return self::RISK_SCORE_ACCEPTABLE;
        }

        if ($score >= 50) {
            return self::RISK_SCORE_WATCHLIST;
        }

        if ($score >= 40) {
            return self::RISK_SCORE_SPECIAL_MENTION;
        }

        if ($score >= 30) {
            return self::RISK_SCORE_SUBSTANDARD;
        }

        return self::RISK_SCORE_DOUBTFUL;
    }

    public function getReceivablesTurnover($incomeStatements, $balanceSheeets) {
        $values = [];

        for ($i = 2; $i >= 0; $i--) {
            if (
                isset($incomeStatements[$i]) &&
                $incomeStatements[$i]->Revenue != 0 &&
                isset($balanceSheeets[$i + 1])
            ) {
                $rev = ($incomeStatements[$i]->Revenue / 365);
                if($rev != 0 && $rev != 0.0) {
                    $values[abs($i - 2)] =  ((
                        $balanceSheeets[$i]->TradeAndOtherCurrentReceivables + $balanceSheeets[$i + 1]->TradeAndOtherCurrentReceivables ) / 2) / $rev;
                }else{
                    $values[abs($i - 2)] = 0;
                }
               
            } else {
                $values[abs($i - 2)] = 0;
            }
        }

        return $values;
    }

    public function getInventoryTurnover($incomeStatements, $balanceSheets) {
        $values = [];

        for ($i = 2; $i >= 0; $i--) {
            if (
                isset($incomeStatements[$i]) &&
                $incomeStatements[$i]->CostOfSales != 0
            ) {
                $inventories = isset($balanceSheets[$i + 1]) ? $balanceSheets[$i + 1]->Inventories: 0;
                $inv =  ($incomeStatements[$i]->CostOfSales / 365);
                if($inv != 0 && $inv != 0.0){
                    $values[abs($i - 2)] = (($balanceSheets[$i]->Inventories +$inventories ) / 2) / $inv;
                }else{
                    $values[abs($i - 2)] = 0;
                }
                
            } else {
                $values[abs($i - 2)] = 0;
            }
        }

        return $values;
    }

    public function getPayablesTurnover($incomeStatements, $balanceSheets) {
        $values = [];

        for ($i = 2; $i >= 0; $i--) {
            $inventories = isset($balanceSheets[$i + 1])
                ? $balanceSheets[$i + 1]->Inventories
                : 0;

            if (
                isset($incomeStatements[$i]) &&
                isset($balanceSheets[$i]) &&
                (
                    $incomeStatements[$i]->CostOfSales +
                    $balanceSheets[$i]->Inventories - $inventories
                ) != 0
            ) {
                if (isset($balanceSheets[$i + 1])) {
                    $tcp = $balanceSheets[$i + 1]->TradeAndOtherCurrentPayables;
                    $cpe = $balanceSheets[$i + 1]->CurrentProvisionsForEmployeeBenefits;
                } else {
                    $tcp = 0;
                    $cpe = 0;
                }

                $costOfSalesDifference = $incomeStatements[$i]->CostOfSales + $incomeStatements[$i]->Inventories - $inventories;

                if ($costOfSalesDifference == 0) {
                    $values[abs($i - 2)] = 0;
                } else {
                    $cost = ($costOfSalesDifference / 365);
                    if($cost != 0 && $cost != 0.0){
                        $values[abs($i - 2)] = (( $balanceSheets[$i]->TradeAndOtherCurrentPayables + $balanceSheets[$i]->CurrentProvisionsForEmployeeBenefits + $tcp + $cpe ) / 2) / $cost;
                    }else{
                        $values[abs($i - 2)] = 0;
                    }
                   
                }
            } else {
                $values[abs($i - 2)] = 0;
            }
        }

        return $values;
    }

    public function getCashConversionCyle(
        $receivablesTurnover,
        $inventoryTurnover,
        $payablesTurnover
    ) {
        $values = [];

        for ($i = 0; $i <= 2; $i++) {
            if (
                isset($receivablesTurnover[$i]) &&
                isset($inventoryTurnover[$i]) &&
                isset($payablesTurnover[$i])
            ) {
                $values[$i] = round(
                    $receivablesTurnover[$i] +
                    $inventoryTurnover[$i] -
                    $payablesTurnover[$i],
                    1
                );
                continue;
            }

            $values[$i] = 0;
        }

        return $values;
    }

    public function generateRatingSummaryAndInternalRatingPDF($id){
        try{
            $entity = Entity::where('entityid', $id)->first();

            /*--------------------------------------------------------------------
            /* Generate Financial Analysis PDF AND Rating Summary PDF
            /*------------------------------------------------------------------*/
            $SME = new SMEInternalHandler();
            $rating_file = $SME->createSMEInternalReport($id);
            $pdfMerge = new PDFMerger();
            if($rating_file){
                if($entity->is_premium != SIMPLIFIED_REPORT){
                    $financialAnalysis = new FinancialAnalysisInternalHandler();
                    $financial_report_id = FinancialReport::where(['entity_id' => $id, 'is_deleted' => 0])->pluck('id')[0];
                    $fr = FinancialReport::find($financial_report_id);
                    $frfile = $financialAnalysis->createFinancialAnalysisInternalReport($financial_report_id,"en");
                    $fname = basename($frfile);

                    $lang = "en";

                    if($lang == "fil"){
                        Entity::where('entityid',$id)->update(['financial_analysis_pdf_tl'=>basename($frfile)]);
                    }else{
                        Entity::where('entityid',$id)->update(['financial_analysis_pdf'=>basename($frfile)]);
                    }   

                    $finAnFile = public_path().'/financial_analysis/'.$fname;
                    $pdfMerge->addPDF($rating_file, 'all');
                    $pdfMerge->addPDF($finAnFile);
                    $pdfMerge->merge('file', $rating_file);

                    Entity::where('entityid',$id)->update(['cbpo_pdf'=>basename($rating_file)]);
                }
            }
        }catch(Exception $ex){
            if(env("APP_ENV") == "Production"){
                Log::error($ex->getMessage());
	            return Redirect::to('dashboard/error');
	        }else{
	            throw $ex;
	        }
        }
    }
}
