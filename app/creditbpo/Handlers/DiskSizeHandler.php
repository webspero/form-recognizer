<?php 

namespace CreditBPO\Handlers;
use Illuminate\Support\Facades\Mail;
use CustomNotification;
use NotificationEmails;
use \Exception as Exception;

class DiskSizeHandler {

    public function monitorSize() {
        $df = disk_free_space("/");
        $to = "leovielyn.aureus@creditbpo.com";
        $where = array(
            "is_deleted" => 0,
            "name" => "Disk Size Checker"
        );
        $info = CustomNotification::where($where)->first();
        $emails = NotificationEmails::where("is_deleted",0)->get();
        $cc = array();
        foreach ($emails as $email) {
            $cc[] = $email->email;
        }
        if($info->count() > 0) {
            $to = ($info->is_running == 1) ? $info->receiver : $to;
            if( $df < 500000000 && $info->is_running == 1) {
                $emailTo = $to;
                //$bcc = array("leovielyn.aureus@creditbpo.com");

                try{
                    Mail::send(
                        'emails.disk_size_result',
                        [
                            'type'=>'7'
                        ],
                        function($message) use ( $emails, $emailTo, $cc) {
                            if($emails->count() > 0){
                                $message->to($emailTo, $emailTo)->cc($cc);
                            }else{
                                $message->to($emailTo, $emailTo);
                            }
                           
                            if(env("APP_ENV") == "Production") {
                                $message->subject('Warning Production Server Disk Free Space is LOW!!');
                            } elseif (env("APP_ENV") == "test") {
                                $message->subject("Warning Test Server Disk Free Space is LOW!!");
                            } else {
                                $message->subject("Warning Local Server Disk Free Space is LOW!!");
                            }
        
                            $message->getHeaders()->addTextHeader('X-Confirm-Reading-To', 'notify.user@creditbpo.com');
                            $message->getHeaders()->addTextHeader('Disposition-Notification-To', 'notify.user@creditbpo.com');
                            return;
                        }
                    );
                }catch(Exception $e){
                    //
                }
            }
        }
        return;
    }
}