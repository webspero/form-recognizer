<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\Sustainability;

class SustainabilityHandler {
    const SCORE_MANAGEMENT_RELATIVE = 36;
    const SCORE_TRUSTED_EMPLOYEE = 24;
    const SCORE_NONE_OF_THE_ABOVE = 12;

    protected $model;

    public function __construct(Sustainability $model = null) {
        $this->model = $model ?: new Sustainability();
    }

    public function getAssessmentScoreByEntityId($entityId) {
        $sustainability = $this->model->getByEntityId($entityId);

        if (@count($sustainability) <= 0) {
            return self::SCORE_NONE_OF_THE_ABOVE;
        }

        if ($sustainability->succession_plan === 1) {
            return self::SCORE_MANAGEMENT_RELATIVE;
        }

        if ($sustainability->succession_plan === 2) {
            return self::SCORE_TRUSTED_EMPLOYEE;
        }

        return self::SCORE_NONE_OF_THE_ABOVE;
    }
}
