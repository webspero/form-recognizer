<?php

namespace CreditBPO\Handlers;

use CreditBPO\Models\BusinessOutlookIndex;
use CreditBPO\Models\Entity;
use CreditBPO\Models\Industrymain;

class BusinessOutlookIndexHandler {
    const SCORE_UPWARD = 90;
    const SCORE_FLAT = 60;
    const SCORE_DOWNWARD = 30;

    protected $model;

    public function __construct(BusinessOutlookIndex $model = null) {
        $this->model = $model ?: new BusinessOutlookIndex();
    }

    /**
     * @param int $entityId
     * @return int
     */
    public function getAssessmentScoreByEntityId($entityId) {
        $entity = $this->model->getByEntityId($entityId);

        if (@count($entity) <= 0) {
            return self::SCORE_DOWNWARD;
        }

        if ($entity->industry_score === 80) {
            return self::SCORE_UPWARD;
        }

        if ($entity->industry_score === 53) {
            return self::SCORE_FLAT;
        }

        return self::SCORE_DOWNWARD;
    }
}
