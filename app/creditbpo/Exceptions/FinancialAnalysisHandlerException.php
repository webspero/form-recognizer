<?php

namespace CreditBPO\Exceptions;

use \Exception as Exception;

class FinancialAnalysisHandlerException extends Exception {};
