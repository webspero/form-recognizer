<?php

/*-------------------------------------------------------------------------
| System Constants
|--------------------------------------------------------------------------
|
|	Constants that are unique to a Server
|
|------------------------------------------------------------------------*/

define('ENV_LOCAL', 0);
define('ENV_TEST', 1);
define('ENV_PRODUCTION', 2);

// Current Environment
define('ENVIRONMENT', ENV_LOCAL); // change this