<?php

/*-------------------------------------------------------------------------
| Constants
|--------------------------------------------------------------------------
|
|	String literals, number constants and just about anything that is a
| 	commonly used constant are put here.
|
|------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
/*	Commonly used values
/*-----------------------------------------------------------------------*/
define('CONT_NULL',						NULL						);	/*	NULL Content		*/
define('STR_EMPTY',						''							);	/*	Empty String		*/
define('CHAR_SPACE',					' '							);	/*	Space Character		*/
define('THREE_DASHES',					'---'						);	/*	Three Dashes		*/
define('STR_TRUE',						'True'						);	/*	String type True	*/
define('STR_FALSE',						'False'						);	/*	String type False	*/
define('CMN_OFF',						0							);	/*	Integer 0			*/
define('CMN_ON',						1							);	/*	Integer 1			*/

/*-------------------------------------------------------------------------
/*	Commonly Status
/*-----------------------------------------------------------------------*/
define('STS_OK',						1						    );	/*	Okay				*/
define('STS_NG',						0						    );	/*	Not Good			*/
define('STS_LOCK',						2						    );	/*	Locked			    */
define('STS_ACT',						'active'					);	/*	Active				*/
define('STS_DEL',						'deleted'					);	/*	Deleted				*/

/*-------------------------------------------------------------------------
/*	Database Active Status
/*-----------------------------------------------------------------------*/
define('DELETED',						0							);	/*	Deleted				*/
define('ACTIVE',						1							);	/*	Active				*/

/*-------------------------------------------------------------------------
/*	User Roles
/*-----------------------------------------------------------------------*/
define('USER_ROLE_SME',					1							);	/*	SME Users   		    */
define('USER_ROLE_CI',					2							);	/*	Credit Investigators    */
define('USER_ROLE_FA',					3							);	/*	Financial Analyst	    */
define('USER_ROLE_BANK',				4							);	/*	Banks      		        */

/*-------------------------------------------------------------------------
/*	Change Password
/*-----------------------------------------------------------------------*/
define('FAILED',						0							);	/*	Failed				*/
define('SUCCESS',						1							);	/*	Success				*/

/*-------------------------------------------------------------------------
/*	Change Password
/*-----------------------------------------------------------------------*/
define('PWORD_MIN_LEN',					8							);	/*	Password Minimum Length		*/

define('PASSWORD_REQ_PENDING',			1							);	/*	Change Password Pending 	*/
define('PASSWORD_REQ_COMPLETE',			2							);	/*	Change Password Completed	*/

define('PASS_CHANGE_REQUEST_NONE',		0							);	/*	No existing request		    */
define('PASS_CHANGE_REQUEST_EXST',		1							);	/*	There is existing request	*/

/*-------------------------------------------------------------------------
/*	User Feedback
/*-----------------------------------------------------------------------*/
define('FEEDBACK_AGREED',				0							);	/*	Agreed to give feedback		*/
define('FEEDBACK_DECLINE',				1							);	/*	Decline to give feedback	*/

define('FINAL_FEEDBACK_SENT',			1							);	/*	Final Feedback Email sent   */
define('FINAL_RATING_FOLLOWED',			2							);	/*	Final Feedback Followed-up	*/
define('FINAL_FEEDBACK_DONE',			3							);	/*	Final Feedback completed    */

/*-------------------------------------------------------------------------
/*	Tab Feedback labels
/*-----------------------------------------------------------------------*/
define('FEEDBACK_BIZ_DETAILS_1',	    1							);	/*	Business Details 1              */
define('FEEDBACK_BIZ_DETAILS_2',	    2							);	/*	Business Details 2              */
define('FEEDBACK_OWNER_INFO',	        3							);	/*	Owner Information               */
define('FEEDBACK_ECF',	                4							);	/*	Existing Credit Facilities      */
define('FEEDBACK_COND_SUSTAIN',	        5							);	/*	Condition and Sustainability    */
define('FEEDBACK_RCL',	                6							);	/*	Required Credit Line            */

/*-------------------------------------------------------------------------
/*	Government Industry List
/*-----------------------------------------------------------------------*/
define('INDUSTRY_AGRICULTURE',	        1							);	/*	Agriculture, Forestry, and Fishing                                      */
define('INDUSTRY_MINING',	            2							);	/*	Mining and Quarrying                                                    */
define('INDUSTRY_MANUFACTURING',	    3							);	/*	Manufacturing                                                           */
define('INDUSTRY_ELECTRICITY',	        4							);	/*	Electricity, Gas, Steam, and Air Conditioning Supply                    */
define('INDUSTRY_WATER',	            5							);	/*	Water Supply; Sewerage, Waste Management and Remediation Activities     */
define('INDUSTRY_CONSTRUCTION',	        6							);	/*	Construction                                                            */
define('INDUSTRY_WHOLESALE',	        7							);	/*	Wholesale and Retail Trade; Repair of Motor Vehicles and Motorcycles    */
define('INDUSTRY_TRANSPORTATION',	    8							);	/*	Transport and Storage                                                   */
define('INDUSTRY_ACCOMODATION',	        9							);	/*	Accommodation and Food Service Activities                               */
define('INDUSTRY_COMMUNICATION',	    10							);	/*	Information and Communication                                           */
define('INDUSTRY_FINANCE',	            11							);	/*	Financial and Insurance Activities                                      */
define('INDUSTRY_REAL_ESTATE',	        12							);	/*	Real Estate Activities                                                  */
define('INDUSTRY_SCIENTIFIC',	        13							);	/*	Professional, Scientific and Technical Activities                       */
define('INDUSTRY_ADMINISTRATIVE',	    14							);	/*	Administrative and Support Service Activities                           */
define('INDUSTRY_EDUCATION',	        15							);	/*	Education                                                               */
define('INDUSTRY_HEALTH',	            16							);	/*	Human Health and Social Work Activities                                 */
define('INDUSTRY_ENTERTAINMENT',	    17							);	/*	Arts, Entertainment, and Recreation                                     */
define('INDUSTRY_OTHER_SERVICES',	    18							);	/*	Other Service Activities                                                */
define('INDUSTRY_HOUSEHOLDS', 19); /* Activities of households as employers; Undifferentiated goods-and services-producing activities of households for own use */
define('INDUSTRY_EXTRA_TERRITORIAL', 20); /* Activities of extra-territorial organizations and bodies */

/*-------------------------------------------------------------------------
/*	Banko Sentral Consolodated Industry List
/*-----------------------------------------------------------------------*/
define('INDUSTRY_BSP_INDUSTRY',	        0							);	/*	Indsutry                        */
define('INDUSTRY_BSP_CONSTRUCTION',	    1							);	/*	Construction                    */
define('INDUSTRY_BSP_WHOLESALE',	    2							);	/*	Wholesale                       */
define('INDUSTRY_BSP_SERVICES',	        3							);	/*	Services                        */
define('INDUSTRY_BSP_FINANCE',	        4							);	/*	Finance                         */
define('INDUSTRY_BSP_ACCOMODATIONS',	5							);	/*	Accomodations                   */
define('INDUSTRY_BSP_BUSINESS',	        6							);	/*	Business Activities             */
define('INDUSTRY_BSP_REAL_ESTATE',	    7							);	/*	Real Estate                     */
define('INDUSTRY_BSP_SOCIAL_SERVICE',	8							);	/*	Social Services                 */
define('INDUSTRY_BSP_TRANSPORTATION',	9							);	/*	Transportation                  */

/*-------------------------------------------------------------------------
/*	Business Outlook Semestral Quarters
/*-----------------------------------------------------------------------*/
define('BSP_REPORT_QUARTER_1',	        0							);	/*	Banko Sentral Report Quarter 1  */
define('BSP_REPORT_QUARTER_2',	        1							);	/*	Banko Sentral Report Quarter 2  */
define('BSP_REPORT_QUARTER_3',	        2							);	/*	Banko Sentral Report Quarter 3  */
define('BSP_REPORT_QUARTER_4',	        3							);	/*	Banko Sentral Report Quarter 4  */
define('BSP_REPORT_QUARTER_5',	        4							);	/*	Banko Sentral Report Quarter 5  */
define('BSP_REPORT_QUARTER_6',	        5							);	/*	Banko Sentral Report Quarter 6  */
define('BSP_REPORT_QUARTER_7',	        6							);	/*	Banko Sentral Report Quarter 7  */
define('BSP_REPORT_QUARTER_8',	        7							);	/*	Banko Sentral Report Quarter 8  */

/*-------------------------------------------------------------------------
/*	Business Outlook
/*-----------------------------------------------------------------------*/
define('BIZ_OUTLOOK_DOWN_POINT',		30							);	/*	Downward outlook points	    */
define('BIZ_OUTLOOK_FLAT_POINT',		53							);	/*	Flat outlook points	        */
define('BIZ_OUTLOOK_UP_POINT',		    80							);	/*	Upward outlook points	    */

define('BIZ_OUTLOOK_DOWN_TREND',		'Downward'					);	/*	Downward Industry Trend	    */
define('BIZ_OUTLOOK_FLAT_TREND',		'Flat'					    );	/*	Flat Industry Trend	        */
define('BIZ_OUTLOOK_UP_TREND',		    'Upward'					);	/*	Upward Industry Trend	    */

define('PEARSON_DEP_VAR_IDX',		    0							);	/*	Dependent Variable Index	            */
define('PEARSON_IND_VAR_IDX',		    1							);	/*	Independent Variable Index	            */
define('PEARSON_XY_VAR_IDX',		    2							);	/*	Dependent multiplied by Independent     */
define('PEARSON_DEP_SQR_IDX',		    3							);	/*	Dependent variable squared Index	    */
define('PEARSON_IND_SQR_IDX',		    4							);	/*	Independent variable squared Index      */

define('PEARSON_SUMM_DEP_IDX',		    0							);	/*	Summation of Dependent Variable Index               */
define('PEARSON_SUMM_IND_IDX',		    1							);	/*	Summation of Independent Variable Index             */
define('PEARSON_SUMM_XY_IDX',		    2							);	/*	Summation of Dependent multiplied by Independent    */
define('PEARSON_SUMM_DEP_SQR_IDX',		3							);	/*	Summation of Dependent variable squared Index       */
define('PEARSON_SUMM_IND_SQR_IDX',		4							);	/*	Summation of Independent variable squared Index     */

define('REGRESSION_Y_INTERCEPT',		0							);	/*	Regression result Index of Y-Intercept          */
define('REGRESSION_SLOPE',		        1							);	/*	Regression result Index of Slope                */
define('REGRESSION_EQUATION',		    2							);	/*	Regression result Index of Equation             */
define('REGRESSION_PTS_ALLOC',		    3							);	/*	Regression result Index of Points Allocation    */

/*-------------------------------------------------------------------------
/*	SME Summary View Types
/*-----------------------------------------------------------------------*/
define('SUMMARY_NORMAL',				'normal'					);	/*	Normal View                             */
define('SUMMARY_BANK_CI',				'ci_view'					);	/*	Credit Investigator view for Banks      */

/*-------------------------------------------------------------------------
/*	Month Numbers for date('m')
/*-----------------------------------------------------------------------*/
define('MONTH_JAN',				        '01'					);	/*	January                             */
define('MONTH_FEB',				        '02'					);	/*	February                            */
define('MONTH_MAR',				        '03'					);	/*	March                               */
define('MONTH_APR',				        '04'					);	/*	April                               */
define('MONTH_MAY',				        '05'					);	/*	May                                 */
define('MONTH_JUN',				        '06'					);	/*	June                                */
define('MONTH_JUL',				        '07'					);	/*	July                                */
define('MONTH_AUG',				        '08'					);	/*	August                              */
define('MONTH_SEP',				        '09'					);	/*	September                           */
define('MONTH_OCT',				        '10'					);	/*	October                             */
define('MONTH_NOV',				        '11'					);	/*	November                            */
define('MONTH_DEC',				        '12'					);	/*	December                            */

/*-------------------------------------------------------------------------
/*	2FA and Login Attempts
/*-----------------------------------------------------------------------*/
define('ACTIVE_ATTEMPT',				0					    );	/*	Current Attempt                         */
define('SUCCESS_ATTEMPT',				1					    );	/*	Successful Attempt                      */
define('FAILED_ATTEMPT',				2					    );	/*	Failed Attempt                          */
define('MAX_LOGIN_FAILED',				10					    );	/*	Maximum failed attempts at login        */

/*-------------------------------------------------------------------------
/*	Maximum score table
/*-----------------------------------------------------------------------*/
define('MAX_RM',						90						);  // Risk Management
define('MAX_CD',						30						);  // Customer Dependency
define('MAX_SD',						30						);  // Supplier Dependency
define('MAX_BOI',						90						);  // Business Outlook Index Score

define('MAX_BOE',						21						);  // Business Owner Experience
define('MAX_MTE',						21						);  // Management Team Experience
define('MAX_BD',						36						);  // Business Drivers and Main Business Segments
define('MAX_SP',						36						);  // Succession Plan
define('MAX_PPFI',						36						);  // Past Project and Future Initiatives

define('MAX_FP',						180						);  // Rating of Financial Position
define('MAX_FR',						180						);  // Rating of Financial Performance

/*-------------------------------------------------------------------------
/*	Bank Trial Account
/*-----------------------------------------------------------------------*/
define('BANK_SME_LIMIT',				7						);  // No of SMEs available to Test Bank
define('BANK_SUBSCRIPTION_AMOUNT',		10000					);  // Subscription amount in Php

/*-------------------------------------------------------------------------
/*	Geolocation webservice
/*-----------------------------------------------------------------------*/
define('GEOLOC_URL_API',				'http://ip-api.com/json/'					    );	/*	IP API Geolocation Web Service          */
define('GEOLOC_ALT_URL_API',            'http://freegeoip.net/json/'                    );	/*	Free GEOIP Geolocation Web Service      */

/*-------------------------------------------------------------------------
/*	Business Entity Type
/*-----------------------------------------------------------------------*/
define('BIZ_ENTITY_CORP',				0					    );	/*	Corporation                 */
define('BIZ_ENTITY_SOLE',				1					    );	/*	Sole Proprietorship         */


define('REFRESH_PAGE_ACT',				0					    );	/*	Reloads the whole page                          */
define('REFRESH_CNTR_ACT',				1					    );	/*	Refresh the Container via Load ajax method      */
define('OVERWRITE_INFO_ACT',            2					    );	/*	Writes a value to the Element                   */

/*-------------------------------------------------------------------------
/*	Google webservice
/*-----------------------------------------------------------------------*/
define('GOOGLE_SERVER_API_KEY', 'AIzaSyCuRcEw3g-ALR7VIHS9s9ZWdQcUQgPjtRw');
define('GOOGLE_BROWSER_API_KEY', 'AIzaSyCS_EC6ukOKOAg90kBr_mCdLqKkLs_VDq8');
define('GOOGLE_MAP_API_KEY', 'AIzaSyAnZdOlHN0WoLT_aES9KOSyZNdwI4heIXU');


/*-------------------------------------------------------------------------
/*	Editable Table Configuration Array labels
/*-----------------------------------------------------------------------*/
define('DATA_TBL_DBINFO',               'db_info'               );	/*	Database Information Array          */
define('DATA_TBL_DBNAME',               0					    );	/*	Database Name Index                 */
define('DATA_TBL_FLDNAME',				1					    );	/*	Database Table field name index     */

define('DATA_TBL_FLDINFO',				0					    );	/*	Field Information array             */
define('DATA_TBL_TYPINFO',				0					    );	/*	Field Type information array        */
define('DATA_TBL_FLDTYP',				0					    );	/*	Field type index                    */
define('DATA_TBL_FLDFORMAT',            1					    );	/*	Field type format index             */

define('DATA_TBL_FLDRULES',             1					    );	/*	Field Rules                         */
define('DATA_TBL_RULES_MSGS',           2					    );	/*	Field Rules Messages                */

define('DATA_TBL_DROPDOWN_DATA',        3					    );	/*	Drop down fields array              */

define('DATA_TBL_TRANS_RULE',           0					    );	/*	Drop down translation rule index    */
define('DATA_TBL_NO_TRANS_LABEL',       1					    );	/*	Drop down label index               */
define('DATA_TBL_TRANS_FILE',           1					    );	/*	Drop down translation file index    */
define('DATA_TBL_TRANS_LABEL',          2					    );	/*	Drop down translation label index   */

define('DATA_TBL_UPLOAD_DATA',          3					    );	/*	Upload fields array                 */
define('DATA_TBL_UPLOAD_PATH',          'path'                  );	/*	Upload path index                   */
define('DATA_TBL_UPLOAD_PREFIX',        'prefix'                );	/*	Upload file prefix index            */
define('DATA_TBL_UPLOAD_DOC_NAME',      'doc_hdr'               );	/*	Upload document name index          */

/*-------------------------------------------------------------------------
/*	Editable Output Format
/*-----------------------------------------------------------------------*/
define('EDITABLE_NONE',                 0					    );	/*	None                    */
define('EDITABLE_MONEY',				1					    );	/*	Monetary values         */
define('EDITABLE_PERCENT',				2					    );	/*	Percentage values       */
define('EDITABLE_DATE',                 3					    );	/*	Date Values             */
define('EDITABLE_PARAGRAPH',            5					    );	/*	Paragraph values        */
define('EDITABLE_SUFFIX',               6					    );	/*	With Suffix             */

/*-------------------------------------------------------------------------
/*	PHP Date Formats
/*-----------------------------------------------------------------------*/
define('DATE_FORMAT_FDY',               'F d, Y'    		    );	/*	Month day, Year         */
define('DATE_FORMAT_FY',				'F Y'					);	/*	Month Year              */

/*-------------------------------------------------------------------------
/*	Questionnaire Types
/*-----------------------------------------------------------------------*/
define('QUESTION_TYPE_BANK',            0    		            );	/*	Bank Type Questionnaire             */
define('QUESTION_TYPE_CORP',            1    		            );	/*	Corporate Type Questionnaire        */
define('QUESTION_TYPE_GOVT',            2    		            );	/*	Government Type Questionnaire       */

/*-------------------------------------------------------------------------
/*	Questionnaire Configuration
/*-----------------------------------------------------------------------*/
define('QUESTION_CNFG_OPTIONAL',        0    		            );	/*	Questionnaire Section is Optional           */
define('QUESTION_CNFG_REQUIRED',        1    		            );	/*	Questionnaire Section is Mandatory          */
define('QUESTION_CNFG_HIDDEN',          2    		            );	/*	Questionnaire Section is Hidden             */


define('APPLICATION_NAME', 'Google Sheets API PHP Quickstart');

/*-------------------------------------------------------------------------
/*	Beneish M-Score
/*-----------------------------------------------------------------------*/
//define('QUESTION_CNFG_OPTIONAL',        0    		            );	/*	Questionnaire Section is Optional           */

/*-------------------------------------------------------------------------
/*	User Login Days
/*-----------------------------------------------------------------------*/
define('USER_LOG_1DAY',                 0    		            );	/*	User Logged in 1 Day from ago               */
define('USER_LOG_3DAY',                 1    		            );	/*	User Logged in 3 Days from ago              */
define('USER_LOG_1WIK',                 2    		            );	/*	User Logged in a Week ago                   */
define('USER_LOG_2WIK',                 3    		            );	/*	User Logged in 2 Weeks ago                  */
define('USER_LOG_1MON',                 4    		            );	/*	User Logged in a Month ago                  */

/*-------------------------------------------------------------------------
/*	Loan Approval Status
/*-----------------------------------------------------------------------*/
define('APPROVAL_NONE_STS',             0    		            );	/*	Default Status                              */
define('LOAN_APPROVED_STS',             1    		            );	/*	Loan Approved                               */
define('PROJ_APPROVED_STS',             2    		            );	/*	Project Approved                            */
define('LOAN_DENIED_STS',               3    		            );	/*	Loan Denied                                 */
define('PROJ_DENIED_STS',               4    		            );	/*	Project Denied                              */
define('IN_PROCESS_STS',                5    		            );	/*	In Process                                  */

/**
 * Pipedrive API Token
 */
define('PIPEDRIVE_API_TOKEN', '4a87e91842ccc34d5e586bb9a184fa070a6eee95');
define('PIPEDRIVE_API_URL', 'https://creditbpo.pipedrive.com/v1/');
define('PIPEDRIVE_PERSON_SOURCE_FIELD', 'fcd7fe6884d2615fdd1a9d47f79be2421d0eb34a');
define('PIPEDRIVE_PERSON_SOURCE', 'SalesIQ');

/*-------------------------------------------------------------------------
/*	Commonly API Status
/*-----------------------------------------------------------------------*/
define('HTTP_STS_OK',						200						    );	/*	Okay - GET, PUT, POST	            */
define('HTTP_STS_CREATED',					201						    );	/*	Adding of new data			    	*/
define('HTTP_STS_NOCONTENT',				204						    );	/*	Deleting data   			    	*/

define('HTTP_STS_BADREQUEST',				400						    );	/*	Bad syntax      				    */
define('HTTP_STS_UNAUTHORIZED',				401						    );	/*	Wrong credentials			    	*/
define('HTTP_STS_FORBIDDEN',				403						    );	/*	Not authorized  				    */
define('HTTP_STS_NOTFOUND',				    404						    );	/*	No Request-URI matched			    */
define('HTTP_STS_INTERNALSERVERERROR',		500						    );	/*	Server cannot fulfll the request    */

/*-------------------------------------------------------------------------
/*	Discount Type
/*-----------------------------------------------------------------------*/
define('DISCOUNT_TYPE_FIXED',					0						    );	/*	Discount type is fixed	        */
define('DISCOUNT_TYPE_PERCENTAGE',				1						    );	/*	Discount type is percentage	    */

/*-------------------------------------------------------------------------
/*	User Roles
/*-----------------------------------------------------------------------*/
define('USER_ADMINISTRATOR',					0						 );	/*	User has administrator role	    */
define('USER_BASIC_USER',					    1						 );	/*	User has basic user role	    */
define('USER_ANALYST',					        2						 );	/*	User has analyst role   	    */
define('USER_INVESTIGATOR',					    3						 );	/*	User has investigator role	    */
define('USER_SUPERVISOR',					    4						 );	/*	User has supervisor role	    */

/*-------------------------------------------------------------------------
/*	User Status
/*-----------------------------------------------------------------------*/
define('USER_STS_INACTIVE',					0						 );	/*	User account is not yet activated	        */
define('USER_STS_ACTIVATED',				1						 );	/*	User account is activated but not yet paid	*/
define('USER_STS_PAID',					    2						 );	/*	User account is activated and paid   	    */

/*-------------------------------------------------------------------------
/*	Company Type
/*-----------------------------------------------------------------------*/
define('COMPANY_TYPE_CORPORATE',				0						 );	/*	Company type is corporate            */
define('COMPANY_TYPE_SOLE_PROP',			    1						 );	/*	Company type is sole proprietorship  */

/*-------------------------------------------------------------------------
/*	Association Options
/*-----------------------------------------------------------------------*/
define('APPLIED_INDEPENDENTLY',				0						 );	/*	Report created independently            */
define('APPLIED_WITH_COMPANY',			    1						 );	/*	Report created for a company            */

/*-------------------------------------------------------------------------
/*	Commonly used values
/*-----------------------------------------------------------------------*/
define('INT_TRUE',						1						    );	/*	The value in database is true	    */
define('INT_FALSE',						0							);	/*	The value in database is false		*/

/*-------------------------------------------------------------------------
/*	Report Status
/*-----------------------------------------------------------------------*/
define('REPORT_STATUS_PENDING',						0						    );	/*	Report is pending	                */
define('REPORT_STATUS_INPROGRESS',					1						    );	/*	Report is in progress	            */
/* Please add other report status here */
define('REPORT_STATUS_SUBMITTED',					5						    );	/*	Report is submitted or for review	*/
define('REPORT_STATUS_REVIEWING',					6						    );	/*	Report is in progress	            */
define('REPORT_STATUS_COMPLETE',					7						    );	/*	Report is in progress	            */

/*-------------------------------------------------------------------------
/*	Payment Method
/*-----------------------------------------------------------------------*/
define('PAYMENT_METHOD_PAYPAL',						1						    );	/*	User paid via PayPal                */
define('PAYMENT_METHOD_DRAGONPAY',					2						    );	/*	USer paid via Dragonpay	            */

/*-------------------------------------------------------------------------
/*	Item Type
/*-----------------------------------------------------------------------*/
define('ITEM_TYPE_FINANCIAL_STANDALONE',			1						    );	/*	Financial Standalone Report                */

/*-------------------------------------------------------------------------
/*	SME Key Type
/*-----------------------------------------------------------------------*/
define('KEY_INDEPENDENT_TYPE',			1						    );	    /*	Independent Key                */
define('KEY_BANK_TYPE',			        2						    );      /*	Bank Key                */

/*-------------------------------------------------------------------------
/*	Upload Types
/*-----------------------------------------------------------------------*/
define('UPLOAD_TYPE_BALANCE_SHEET', 1);             /* Balance Sheet Upload Type */
define('UPLOAD_TYPE_INCOME_STATEMENT', 2);          /* Income Statement Upload Type */
define('UPLOAD_TYPE_CASHFLOW_STATEMENT', 3);        /* Cashflow Statement Upload Type */
define('UPLOAD_TYPE_BS_IS_CS', 4);                  /* Balance Sheet, Income Statement and Cashflow Statement Upload Type */
define('UPLOAD_TYPE_FS_TEMPLATE', 5);               /* Financial Statment Upload Type */
define('UPLOAD_TYPE_LETTER_OF_AUTHORIZATION', 6);   /*Letter of Authorization Type*/

/*-------------------------------------------------------------------------
/*	Document Group
/*-----------------------------------------------------------------------*/
define('DOCUMENT_GROUP_FS_TEMPLATE', 21);               /* Financial Statment Document Group */
define('DOCUMENT_GROUP_BANK_STATEMENT', 51);            /* Bank Statement Document Group */
define('DOCUMENT_GROUP_UTILITY_BILLS', 61);             /* Utility Bills Document Group */
define('DOCUMENT_GROUP_LETTER_OF_AUTHORIZATION', 11);   /*Letter of Authorization Group*/

/*-------------------------------------------------------------------------
/*	Report Progress Weights
/*-----------------------------------------------------------------------*/
define('PROGRESS_WEIGHT_NORMAL', 1); /* Normal Progress Weight */
define('PROGRESS_WEIGHT_HEAVY', 8);  /* Heavy Progress Weight */
/*-------------------------------------------------------------------------
/*	Report Type
/*-----------------------------------------------------------------------*/
define('STANDALONE_REPORT', 0); /* Standalone Report */
define('PREMIUM_REPORT', 1);    /* Premium Report */
define('SIMPLIFIED_REPORT', 2); /* Simplified Report */

/*-------------------------------------------------------------------------
/*	Type of Dragonpay CreditBPO Transaction
/*-----------------------------------------------------------------------*/
define('PD_BASIC_USER_REPORT', 'BUP'); /* Purchase Basic User Report */
define('PD_SUPERVISOR_SUBSCRIPTION', 'SS');    /* Supervisor Subscription */
define('PD_SUPERVISOR_CLIENT_KEY', 'SSC'); /* Supervisor Client Keys */
