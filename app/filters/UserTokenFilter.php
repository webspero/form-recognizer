<?php

use CreditBPO\Models\UserAuthToken;
use CreditBPO\Controllers\API\v1\ApiController;

//======================================================================
//  Class #: UserTokenFilter
//  This class is for checking api user authentication tokens (access and refresh)
//  validity for logged-in users
//======================================================================
class UserTokenFilter
{
    public function filter()  
    {
        /*Get request header client id and access token value*/
        $clientId = Request::header('Client-Id');
        $authorizationHeader = Request::header('Authorization');
        $apiController = new ApiController();

        if (!is_null($authorizationHeader)){
            $accessToken = str_replace(' ', '', trim($authorizationHeader, "Bearer"));
            /* Check if access token exist and valid */
            $userAuthTokenDb = new UserAuthToken();
            $tokenDetails = $userAuthTokenDb->getAccessTokenDetails($accessToken, $clientId);
            
            /* If not exist, throw an error response */
            if (empty($tokenDetails)){
                return $apiController->error('Invalid access token.', HTTP_STS_UNAUTHORIZED);
            }

            /* Check token validity */
            if ($tokenDetails->expires_in < date("Y-m-d H:i:s")){
                return $apiController->error('Access token expired.', HTTP_STS_UNAUTHORIZED);
            }
        }
        /* If the header doesn't contains access token value, throw an error response */
        else{
            return $apiController->error('Access token is required.', HTTP_STS_BADREQUEST);
        }
    }
}