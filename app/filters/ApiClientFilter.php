<?php

use CreditBPO\Models\DeveloperApiUser;
use CreditBPO\Controllers\API\v1\ApiController;

//======================================================================
//  Class #: ApiClientFilter
//  This class is for checking api request client id and secret key
//  for both guest and logged-in users
//======================================================================
class ApiClientFilter
{
    public function filter()  
    {
        /*Check header if contains client id and secret key value*/
        $clientId = Request::header('Client-Id');
        $clientSecret = Request::header('Client-Secret');
        $apiController = new ApiController();

        if (!is_null($clientId) && !is_null($clientSecret)){
            /* Check if client id and secret key exist */
            $apiUserDb = new DeveloperApiUser();
            $isExist = $apiUserDb->checkApiKey($clientId, $clientSecret);
            
            /* If not exist, throw a forbidden error response */
            if (!$isExist){
                return $apiController->error('Invalid client id and secret key.', HTTP_STS_UNAUTHORIZED);
            }
        }
        /* If the header doesn't contains client id and secret key value, throw a forbidden error response */
        else{
            return $apiController->error('Client id and secret key are required.', HTTP_STS_UNAUTHORIZED);
        }
    }
}