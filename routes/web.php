<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Valid User Statistics
Route::group([
    'prefix' => 'financial-report',
], function() {
    Route::get(
        'download/{financialReportId}',
        [
            'uses' => 'CreditBPO\Controllers\FinancialReportController@getDownloadReport',
        ]
    );

    Route::get(
        'download/financial-analysis/{entityId}/{language}',
        [
            'uses' => 'CreditBPO\Controllers\FinancialReportController@getDownloadFinancialAnalysisInternalReport',
        ]
    );
});
