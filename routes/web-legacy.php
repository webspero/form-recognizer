<?php


// LOGIN
Route::get('/', array('uses' => 'LoginController@showLoginIndex', 'as' => 'showLoginIndex'));
Route::get('/errors', array('uses' => 'ProfileSMEController@showErrorPage', 'as' => 'showErrorPage'));
Route::get('/signup_basic_user', array('uses' => 'SignupController@getSignupUser', 'as' => 'getSignupUser'));
Route::post('/post_signup_basic_user', array('uses' => 'SignupController@postSignupUser', 'as' => 'postSignupUser'));

// UNIVERSAL LOGOUT
Route::get('/logout', array('uses' => 'LogoutController@getLogoutIndex', 'as' => 'getLogoutIndex'));
Route::get('/save-external-link', array('uses' => 'ExternalLinkController@getSaveAndExit', 'as' => 'getSaveAndExit'));
Route::post('/get/external-link-config/{bank_id}', array('uses' => 'ExternalLinkController@getQuestionnaireConfig', 'as' => 'getQuestionnaireConfig'));

//LANGUAGE
Route::get('/lang/{lang}', array('uses' => 'LanguageController@getChangeLanguage', 'as' => 'getChangeLanguage'));

Route::get('/cronjob/send-follow-up-rating', array('uses' => 'UserFeedbackController@sendFollowUpUserFeedback', 'as' => 'sendFollowUpUserFeedback'));
Route::get('/webservice/getdata', array('uses' => 'WebServiceController@getWebService', 'as' => 'getWebService'));
Route::get('/webservice/fbdata', array('uses' => 'WebServiceController@WebCalcFacebookLike', 'as' => 'WebCalcFacebookLike'));
Route::get('/webservice/test_gcalendar', array('uses' => 'GoogleApiClientController@executeLeadExtraction', 'as' => 'executeLeadExtraction'));
Route::get('/webservice/test_gquickie', array('uses' => 'WebServiceController@testGoogleQuickstart1', 'as' => 'testGoogleQuickstart'));
Route::get('/webservice/test_convert_fr', array('uses' => 'WebServiceController@convertFrPdftoText', 'as' => 'convertFrPdftoText'));
Route::get('/credit_score', array('uses' => 'WebServiceController@getCalculatorService', 'as' => 'getCalculatorService'));

Route::get('/beneish_test/{entity_id}', array('uses' => 'ProfileSMEController@computeBeneishScore', 'as' => 'computeBeneishScore'));

//Activation
Route::get('/activation/{activationcode}', array('uses' => 'LoginController@getActivationCode', 'as' => 'getActivationCode'));

// SUPERVISOR SIGNUP
Route::get('/supervisor-signup', array('uses' => 'SignupController@getSupervisorSignup', 'as' => 'getSupervisorSignup'));
Route::post('guest/supervisor-signup', array('uses' => 'SignupController@postSupervisorSignup', 'as' => 'postSupervisorSignup'));

//external link
Route::get('/external-link/{code}', array('uses' => 'ExternalLinkController@getExternalLink', 'as' => 'getExternalLink'));
Route::get('/premium-link/{code}', array('uses' => 'PremiumReportController@show', 'as' => 'showPremiumReport'));
Route::get('/premium-link/{id}/{user_id}/{company_name}', array('uses' => 'PremiumReportController@getSummaryLongUrl', 'as' => 'getSummaryLongUrl'));
Route::get('/premium-link/summary/{id}', array('uses' => 'PremiumReportController@getSMESummary', 'as' => 'getSMESummary'));

Route::post('/industry_search', array('uses' => 'IndustryController@postSearch', 'as' => 'postSearchIndustry'));
Route::get('/industry_search', array('uses' => 'IndustryController@postSearch', 'as' => 'postSearchIndustry'));
Route::post('/process_search', array('uses' => 'IndustryController@processSearch', 'as' => 'processSearchIndustry'));
Route::get('/process_search', array('uses' => 'IndustryController@processSearch', 'as' => 'processSearchIndustry'));

Route::post('/pse_search', array('uses' => 'SignupController@postSearchPseCompany', 'as' => 'postSearchPseCompany'));
Route::post('/process_pse_search', array('uses' => 'SignupController@processPseSearch', 'as' => 'processPseSearch'));

//External link for CreditBPO Transcriber
Route::post('/transcribe/login', array('uses' => 'AdminController@postTranscribeLogin', 'as' => 'postTranscribeLogin'));
Route::get('/transcribe_files', array('uses' => 'AdminController@getCreditBpoFTP', 'as' => 'getExternalCreditBpoFTP'));
Route::get('/upload-fs/{id}', array('uses' => 'AdminController@getUploadFTP', 'as' => 'getUploadFTP'));
Route::post('/upload/creditbpofs/{id}', array('uses' => 'AdminController@postCreditBpoFTP', 'as' => 'postCreditBpoFTP'));
Route::get('/fsRawFile', array('uses' => 'AdminController@getFsRawFile', 'as' => 'getFsRawFile'));
Route::get('/viewTranscribeFile', array('uses' => 'AdminController@getTranscribeFile', 'as' => 'getTranscribeFile'));
Route::get('/download_fspdfFile/{id}', array('uses' => 'FileController@getDownloadFspdfFiles', 'as' => 'getDownloadFspdfFiles'));
Route::get('/download/transcibe_fs/{id}', array('uses' => 'AdminController@downloadTranscribeFS', 'as' => 'downloadTranscribeFS'));
Route::get('/delete/transcribe_fs/{id}', array('uses' => 'AdminController@deleteTranscribeFs', 'as' => 'deleteTranscribeFs'));
Route::get('/getErrorFile/{id}', array('uses' => 'AdminController@getErrorFile', 'as' => 'getErrorFile'));

// GUEST
Route::group(['middleware' => ['guestold']], function()
{
    // SIGNIN
    Route::get('/login', array('uses' => 'LoginController@showLoginIndex', 'as' => 'showLoginIndex'));
    Route::post('/login', array('uses' => 'LoginController@postLoginIndex', 'as' => 'postLoginIndex'));

    // SIGNUP
    Route::get('/signup', array('uses' => 'SignupController@getSignupCreate', 'as' => 'getSignupCreate'));
    Route::post('/signup', array('uses' => 'SignupController@postSignupStore', 'as' => 'postSignupStore'));
    Route::get('/si-confirmation', array('uses' => 'SignupController@getSignupConfirmation', 'as' => 'getSignupConfirmation'));

    Route::get('/contractor', array('uses' => 'SignupController@getSignupContractor', 'as' => 'getSignupContractor'));
    Route::post('/contractor', array('uses' => 'SignupController@postSignupContractor', 'as' => 'postSignupContractor'));

    // Route::get('/eula', array('uses' => 'SignupController@getEula', 'as' => 'getEula'));

    // ACTIVATION CODE
    Route::post('/activation/{activationcode}', array('uses' => 'LoginController@postActivationCode', 'as' => 'postActivationCode'));

    // FORGOT PASSWORD
    Route::get('/rest/get_user_by_email', array('uses' => 'LoginController@validateEmail', 'as' => 'getUserByEmail'));
    Route::post('/rest/change_password', array('uses' => 'LoginController@changePassword', 'as' => 'changePassword'));
    Route::get('/rest/check_password_request', array('uses' => 'LoginController@checkPasswordRequest', 'as' => 'checkPasswordRequest'));

    // PUBLIC Converter
    Route::get('/converter/{hash}', array('uses' => 'ConverterController@getConverter', 'as' => 'getConverterPublic'));
    Route::post('/converter/{hash}', array('uses' => 'ConverterController@postConverter', 'as' => 'postConverterPublic'));
    Route::get('/converter_reset', array('uses' => 'ConverterController@resetHash', 'as' => 'resetHash'));

    /*  Public Trial List   */
    Route::get('/trial_list/{hash}', array('uses' => 'AdminController@getPublicTrialList', 'as' => 'getPublicTrialList'));
});

Route::get('api/industrysub', function(){
    $input = Input::get('industrymain');
    $projects = DB::table('tblindustry_sub')
        ->where('main_code', $input)
        ->pluck('sub_title','sub_code');

    return Response::json($projects);
});
Route::get('api/industryrow', function(){
    $input = Input::get('industrysub');
    $projects = DB::table('tblindustry_group')->where('sub_code', $input)->pluck('group_description','group_code');
    // $projects = DB::table('tblindustry_class')
    //             ->whereIn('group_code', $group_code)
    //             ->pluck('class_description','class_code');
    return Response::json($projects);
});
Route::get('api/cities', function(){
    $input = Input::get('province');
    $cities = DB::table('refcitymun')->select('id', 'citymunDesc', 'zipcode')->where('provCode', $input)->get();
    return Response::json($cities);
});
Route::get('api/validate_email', function(){
    $email = Input::get('email');
    $client = new QuickEmailVerification\Client('c1a7fed131f7359b8df03ce62419b57a1c8f8226c525e926f31119ddbe04');
    $quickemailverification = $client->quickemailverification();
    // If production write verify instead of sandbox
    return Response::json($quickemailverification->verify($email), 200, ['Access-Control-Allow-Origin' => 'http://creditbpo.com']);
});

Route::get('api/validate_blocked_email', function(){
    $email = Input::get('email');
    $is_blocked = DB::table('block_email_address')->where('email_address', $email)->first();
    if($is_blocked){
        return STS_NG;
    }
    return STS_OK;
});

Route::get('api/businessdriver_pl', function(){
    $input = Input::get('id');
    $result = DB::table('tblbusinessdriver')
    ->where('entity_id', $input)
    ->get();
    return $result;
    });
Route::get('api/businesssegment_pl', function(){
    $input = Input::get('id');
    $result = DB::table('tblbusinesssegment')
    ->where('entity_id', $input)
    ->get();
    return $result;
});
Route::get('api/pastprojectscompleted_pl', function(){
    $input = Input::get('id');
    $result = DB::table('tblprojectcompleted')
    ->where('entity_id', $input)
    ->get();
    return $result;
});
Route::get('api/futuregrowthinitiative_pl', function(){
    $input = Input::get('id');
    $result = DB::table('tblfuturegrowth')
    ->where('entity_id', $input)
    ->get();
    return $result;
});
Route::get('api/check_supervisor_key', function(){
    $key = Input::get('key');
    $province = Input::get('province');
    //$reportType = Input::get('reportType');
    $reportType = Input::get('who');

    //echo $reportType; exit;

    $result = DB::table('bank_keys')
    ->where('serial_key', $key)
    // ->where('is_used', 0)
    ->first();

	$provCode = [];
	$provCodeStr = '';
    $country = Input::get('country');

    if(!empty($result->type)){

        // Check if the input client key report type is available to the organization
        $organization = DB::table('tblbank')->where('id', $result->bank_id)->first();
        $organizationRT = json_decode($organization->report_type);

        // Check simplified
        if($result->type == "Simplified" && $organizationRT->simplified_type == 0){
            if($result->is_used == 0){
                //
            }else{
                $srv_resp['serial_key'] = $key;
                $srv_resp['report_type'] = $result->type;
                $srv_resp['prov_str'] = $provCodeStr;
                $srv_resp['messages'] = 'This Client Key report type is currently unavailable to the associated organization.';
                return json_encode($srv_resp);
            }
        }

        // Check standalone
        if($result->type == "Standalone" && $organizationRT->standalone_type == 0){
            if($result->is_used == 0){
                //
            }else{
                $srv_resp['serial_key'] = $key;
                $srv_resp['report_type'] = $result->type;
                $srv_resp['prov_str'] = $provCodeStr;
                $srv_resp['messages'] = 'This Client Key report type is currently unavailable to the associated organization.';
                return json_encode($srv_resp);
            }
        }

        // Check premium report
        if(($result->type == "Premium Zone 1" || $result->type == "Premium Zone 2" || $result->type == "Premium Zone 1") && $organizationRT->premium_type == 0){
            if($result->is_used == 0){
                //
            }else{
                $srv_resp['serial_key'] = $key;
                $srv_resp['report_type'] = $result->type;
                $srv_resp['prov_str'] = $provCodeStr;
                $srv_resp['messages'] = 'This Client Key report type is currently unavailable to the associated organization.';
                return json_encode($srv_resp);
            }
        }

        if($result->type == 'Premium Zone 1'){
            $provCode = ['1376','1374','1375'];
        }

        if($result->type == 'Premium Zone 2'){
            $provCode = ['0434','0458','0421'];
        }

        if($result->type == 'Premium Zone 3'){
            $provCode = ['1376','1374','1375','0434','0458','0421'];
        }

        $provCodeStr = implode('|',$provCode);

        // if(($result->type == 'Simplified/Standalone' || $result->type == 'Simplified' || $result->type == 'Standalone') && ($reportType != 2 && $reportType != 0)){
        //     $srv_resp['serial_key'] = $key;
        //     $srv_resp['report_type'] = $result->type;
        //     $srv_resp['prov_str'] = $provCodeStr;
        //     $srv_resp['messages'] = 'This Client Key is only valid for simplified or standalone reports.';
        //     return json_encode($srv_resp);
        // }

        // if($result->type == 'Standalone' && $reportType != 0){
        //     //echo 'test'; exit;
        //     $srv_resp['serial_key'] = $key;
        //     $srv_resp['report_type'] = $result->type;
        //     $srv_resp['messages'] = 'This Client Key is only valid for standalone reports.';
        //     return json_encode($srv_resp);
        // }

        // if($result->type == 'Simplified' && $reportType != 2){
        //     $srv_resp['serial_key'] = $key;
        //     $srv_resp['report_type'] = $result->type;
        //     $srv_resp['messages'] = 'This Client Key is only valid for simplified reports.';
        //     return json_encode($srv_resp);
        // }

        // if(($result->type == 'Premium Zone 1' || $result->type == 'Premium Zone 2' || $result->type == 'Premium Zone 3')  && ($reportType == 2 || $reportType == 0)){

        //     $srv_resp['serial_key'] = $key;
        //     $srv_resp['report_type'] = $result->type;
        //     $srv_resp['prov_str'] = $provCodeStr;
        //     $srv_resp['messages'] = 'This Client Key is only valid for premium reports';
        //     return json_encode($srv_resp);
        // }
    }

	if(!empty($result->type)){

        if($result->type == 'Premium Zone 1' || $result->type == 'Premium Zone 2'){
            if(!in_array($province,$provCode)){
                $srv_resp['serial_key'] = $key;
                $srv_resp['report_type'] = $result->type;
                $srv_resp['prov_str'] = $provCodeStr;
                $srv_resp['messages'] = 'That Client Key is out of zone.';
                return json_encode($srv_resp);
            }
        }elseif($result->type == 'Premium Zone 3'){
            if(in_array($province,$provCode)){
                $srv_resp['serial_key'] = $key;
                $srv_resp['report_type'] = $result->type;
                $srv_resp['prov_str'] = $provCodeStr;
                $srv_resp['messages'] = 'That Client Key is out of zone.';
                return json_encode($srv_resp);
            }
        }
	}

    if($result==null) {
		$srv_resp['serial_key'] = $key;
        $srv_resp['report_type'] = 'None';
        $srv_resp['prov_str'] = $provCodeStr;
		$srv_resp['messages'] = 'That Client Key is invalid.';
		return json_encode($srv_resp);

	}else{
		if($result->is_used == 1){
			$srv_resp['serial_key'] = $key;
            $srv_resp['report_type'] = $result->type;
            $srv_resp['prov_str'] = $provCodeStr;
			$srv_resp['messages'] = 'That Client Key has already been used.';
				return json_encode($srv_resp);

		}
	}

    // additional filter for client keys report type
    $bank = DB::table('tblbank')->where('id', $result->bank_id)->first();
    $bankRT = json_decode($bank->report_type);
    $result->activateReport = 3;
    if($bankRT->simplified_type == 0 && $result->type == "Simplified"){
        $result->activateReport = 2;
    }
    if($bankRT->standalone_type == 0 && $result->type == "Standalone"){
        $result->activateReport = 0;
    }
    if($bankRT->standalone_type == 0 && ($result->type == "Premium Zone 1"|| $result->type == "Premium Zone 2" || $result->type == "Premium Zone 3")){
        $result->activateReport = 1;
    }

    $result->report_type = $result->type;
    $result->prov_str = $provCodeStr;
    return Response::json($result);
});
Route::get('api/check_invalid_ub', function(){
    $id = Input::get('id');
    $result = DB::table('tbldocument')
    ->where('entity_id', $id)
    ->where('document_group', 61)
    ->where('is_valid', 2)
    ->get();
    return Response::json($result);
});
Route::post('api/update_invalid_ub', function(){
    $id = Input::get('id');
    foreach($id as $i){
        DB::table('tbldocument')->where('documentid', $i)->update(['is_valid'=>3]);
    }
    return 'ok';
});
Route::get('api/check_invalid_bs', function(){
    $id = Input::get('id');
    $result = DB::table('tbldocument')
    ->where('entity_id', $id)
    ->where('document_group', 51)
    ->where('is_valid', 2)
    ->get();
    return Response::json($result);
});
Route::post('api/update_invalid_bs', function(){
    $id = Input::get('id');
    foreach($id as $i){
        DB::table('tbldocument')->where('documentid', $i)->update(['is_valid'=>3]);
    }
    return 'ok';
});

Route::get('/termsofuse', array('uses' => 'SignupController@getTermsOfUse', 'as' => 'getTermsofUse'));
Route::get('/privacypolicy', array('uses' => 'SignupController@getPrivacyPolicy', 'as' => 'getPrivacyPolicy'));

Route::get('/vision', 'VisionController@displayForm');
Route::post('/vision', 'VisionController@detectText');

Route::get('/fs_ocr', 'FinancialStatementOcrController@displayForm');
Route::post('/fs_ocr', 'FinancialStatementOcrController@postFsOCR');
Route::get('/download/OCRgenerated_fs/{companyName}', array('uses' => 'FinancialStatementOcrController@getDownloadGeneratedFS', 'as' => 'getDownloadGeneratedFS'));

Route::get('/payment/paymentsuccess', 'PaymentController@getSuccessPayment');
Route::get('/payment/postback', 'PaymentController@paymentPostback');
Route::post('/payment/postback', 'PaymentController@paymentPostback');
Route::get('/payment/result', 'PaymentController@paymentReturn');
Route::get('/grdp/get_boost', array('uses' => 'ProfileSMEController@getGRDPBoost', 'as' => 'getGRDPBoost'));
Route::get('/api/check_fs', array('uses' => 'FinancialReportController@getFinancialReportRaw', 'as' => 'getFinancialReportRaw'));
Route::get('/refresh_csrf', function(){
    return csrf_token();
});

// SME
Route::group(['middleware' => 'authold'], function()
{
    // SUMARY API
    // Route::get('api/servicesoffer', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblservicesoffer')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/businessdriver', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblbusinessdriver')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/businesssegment', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblbusinesssegment')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/majorcustomer', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblmajorcustomer')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/majorsupplier', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblmajorsupplier')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/competitor', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblcompetitor')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/pastprojectscompleted', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblprojectcompleted')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/futuregrowthinitiative', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblfuturegrowth')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/riskassessment', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblriskassessment')
    //     ->where('entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    // Route::get('api/majorlocation', function(){
    //     $input = Input::get('id');
    //     $result = DB::table('tblmajorlocation')
    //     ->join('tblcity', 'tblmajorlocation.marketlocation', '=', 'tblcity.cityid')
    //     ->where('tblmajorlocation.entity_id', $input)
    //     ->get();
    //     return $result;
    // });
    Route::get('api/adverserecord', function(){
        $input = Input::get('id');
        $result = DB::table('tbladverserecord')
        ->where('entity_id', $input)
        ->get();
        return $result;
    });
    Route::get('api/spouseofowner', function(){
        $input = Input::get('id');
        $entity_id = Input::get('entity_id');
        $result = DB::table('tblspouse')
        ->where('ownerid', $input)
        ->where('entity_id', $entity_id)
        ->get();
        return $result;
    });
    Route::get('api/businessmanage', function(){
        $input = Input::get('id');
        $entity_id = Input::get('entity_id');
        $managebus = DB::table('tblmanagebus')
        ->join('tblcity', 'tblmanagebus.bus_location', '=', 'tblcity.cityid')
        ->where('tblmanagebus.ownerid', $input)
        ->where('tblmanagebus.entity_id', $entity_id)
        ->get();
        return $managebus;
    });
    Route::get('api/keymanage', function(){
        $input = Input::get('id');
        $entity_id = Input::get('entity_id');
        $keymanage = DB::table('tblkeymanagers')
        ->select(DB::raw('tblkeymanagers.*, tblcity.city as city_name, tblprovince.province as province_name'))
        ->leftJoin('tblcity', 'tblkeymanagers.cityid', '=', 'tblcity.cityid')
        ->leftJoin('tblprovince', 'tblkeymanagers.province', '=', 'tblprovince.provinceid')
        ->where('tblkeymanagers.ownerid', $input)
        ->where('tblkeymanagers.entity_id', $entity_id)
        ->get();
        return $keymanage;
    });
    Route::get('api/education', function(){
        $input = Input::get('id');
        $entity_id = Input::get('entity_id');
        $education = DB::table('tbleducation')
        ->where('ownerid', $input)
        ->where('entity_id', $entity_id)
        ->get();
        return $education;
    });
    Route::post('tutorial/update', function(){
        $flag = Input::get('status');
        $loginid = Auth::user()->loginid;
        User::where('loginid', $loginid)->update(array('tutorial_flag'=>$flag));
        return 'success';
    });

    Route::get('/download/{path}/{file}', array('uses' => 'DocumentController@downloadFile', 'as' => 'downloadFile'));
    Route::get('/download/{path}/{entity}/{file}', array('uses' => 'DocumentController@downloadEntityFile', 'as' => 'downloadEntityFile'));
    Route::get('/download/documents/checklist/{entity}/{file}', array('uses' => 'DocumentController@downloadChecklistFile', 'as' => 'downloadChecklistFile'));
    Route::get('/download/documents/quickbooks/{entity}/{file}', array('uses' => 'DocumentController@downloadQuickbooksFile', 'as' => 'downloadChecklistFile'));
    Route::get('/sme/download_ubill/{id}/{filename}', array('uses' => 'UtilityBillController@getDownloadUbill', 'as' => 'getDownloadUbill'));

    Route::group(['middleware' => 'authcheck'], function(){
        Route::get('/email-unlock', array('uses' => 'SignupController@getEmailUnlock', 'as' => 'getEmailUnlock'));
        Route::post('/email-unlock-post', array('uses' => 'SignupController@postEmailUnlock', 'as' => 'postEmailUnlock'));

        // Industry
        Route::get('signup/getIndustryMain', array('uses' => 'SignupController@getIndustryMain', 'as' => 'getIndustryMain'));
        Route::get('signup/getIndustrySub/{id}', array('uses' => 'SignupController@getIndustrySub', 'as' => 'getIndustrySub'));
        Route::get('signup/getIndustryRow/{id}', array('uses' => 'SignupController@getIndustryRow', 'as' => 'getIndustryRow'));

        Route::get('signup/getProvinceList', array('uses' => 'SignupController@getProvinceList', 'as' => 'getProvinceList'));
        Route::get('signup/getProvinceList2', array('uses' => 'SignupController@getProvinceList2', 'as' => 'getProvinceList2'));
        Route::get('signup/getCityList/{province}', array('uses' => 'SignupController@getCityList', 'as' => 'getCityList'));
        Route::get('signup/getCityList2/{provCode}', array('uses' => 'SignupController@getCityList2', 'as' => 'getCityList2'));

        Route::get('signup/getCustomReportPrice/{bankname}/{provinceCode}', array('uses' => 'SignupController@getCustomReportPrice', 'as' => 'getCustomReportPrice'));
        Route::get('signup/getReportPriceInternational/{bankname}/{serialkey}', array('uses' => 'SignupController@getReportPriceInternational', 'as' => 'getReportPriceInternational'));
        Route::get('signup/getProvincePremiumReportPrice/{province}', array('uses' => 'SignupController@getProvincePremiumReportPrice', 'as' => 'getProvincePremiumReportPrice'));
        Route::get('signup/getCityPremiumReportPrice/{city}', array('uses' => 'SignupController@getCityPremiumPrice', 'as' => 'getCityPremiumPrice'));
        //
        Route::get('signup/editreport/signup/getProvinceCityViaCityId/{cityid}', array('uses' => 'SignupController@getProvinceCityViaCityId', 'as' => 'getProvinceCityViaCityId'));
        //
        Route::get('signup/editreport/{entity_id}', array('uses' => 'SignupController@getPremiumReportDetails', 'as' => 'getPremiumReportDetails'));

        /** Autopopulate company name to fs template*/
        Route::get('/getFsTemplateFile/{entityid}', array('uses' => 'DocumentController@getFsTemplateFile', 'as' => 'getFsTemplateFile'));

        Route::get('/dashboard/index', array('uses' => 'DashboardController@getDashboardIndex', 'as' => 'getDashboardIndex'));
        Route::get('/dashboard/error', array('uses' => 'DashboardController@getDashboardError', 'as' => 'getDashboardError'));
        Route::get('/dashboard/smebucket', array('uses' => 'DashboardController@getDashboardSmeBucket', 'as' => 'getDashboardSmeBucket'));
        Route::get('/dashboard/smescored', array('uses' => 'DashboardController@getDashboardSmeScored', 'as' => 'getDashboardSmeScored'));
        Route::get('/summary/smesummary/{id}/', array('uses' => 'ProfileSMEController@getSMESummary', 'as' => 'getSMESummaryId'));
        Route::get('/summary/smesummary/{id}/{view_typ}', array('uses' => 'ProfileSMEController@getSMESummary', 'as' => 'getSMESummary'));
        Route::get('/summary/{id}/{user_id}/{company_name}', array('uses' => 'ProfileSMEController@getSummaryLongUrl', 'as' => 'getSummaryLongUrl'));
        Route::get('/summary/{id}/{user_id}/{company_name}/{view_typ}', array('uses' => 'ProfileSMEController@getSummaryLongUrl', 'as' => 'getSummaryLongUrl'));
        Route::get('/summary-contractor/{id}/{user_id}/{company_name}/{view_typ}', array('uses' => 'ProfileSMEController@getSummaryLongUrlContractor', 'as' => 'getSummaryLongUrlContractor'));
        Route::get('/sme/confirmation/{name}', array('uses' => 'FormSMEController@getConfirmation', 'as' => 'getConfirmation'));
        Route::get('/sme/premiumsuccess/{id}', array('uses' => 'FormSMEController@getSuccessPremiumReport', 'as' => 'getSuccessPremiumReport'));
        Route::get('/dashboard/approved', array('uses' => 'DashboardController@getDashboardApproved', 'as' => 'getDashboardApproved'));
        Route::get('/letterofauthorization/{id}', array('uses' => 'BankController@getLetterOfAuthorization', 'as' => 'getLetterOfAuthorization'));

        // upload files to dropbox
        Route::post('/assocFile_upload', array('uses' => 'DropboxController@postAssocFileUpload', 'as' => 'postAssocFileUpload'));
        Route::post('/assocFile_delete', array('uses' => 'DropboxController@postAssocFileDelete', 'as' => 'postAssocFileDelete'));

        // Export DSCR Details to Rating Summary
        Route::post('/exportdscr/{id}/', array('uses' => 'ProfileSMEController@exportToSummary', 'as' => 'exportToSummary'));

        // Export DSCR Details to Rating Summary
        Route::post('/exportdscrfinal/{id}/', array('uses' => 'ProfileSMEController@exportToSummaryFinal', 'as' => 'exportToSummary'));

        //CHECK FS INPUT TEMPLAGE
        Route::get('/summary/check_fsinput/{entityId}', array('uses' => 'ActionController@getFsInputTemplates', 'as' => 'postCreditInvestigation'));

        // PASS TO CREDIT INVESTIGATOR
       	Route::post('/summary/smesummary/{id}/pci', array('uses' => 'ActionController@postCreditInvestigation', 'as' => 'postCreditInvestigation'));

        Route::post('/summary/smesummary/{id}/pci/fa', array('uses' => 'ActionController@postFinancialAnalysis', 'as' => 'postFinancialAnalysis'));

        // START SCORING
        Route::post('/summary/smesummary/{id}/ss', array('uses' => 'ActionController@postStartScoring', 'as' => 'postStartScoring'));

        // RELEASE SCORING
        Route::delete('/summary/smesummary/{id}/rs', array('uses' => 'ActionController@deleteReleaseScoring', 'as' => 'postReleaseScoring'));

        // FINISH SCORING
        Route::post('/summary/smesummary/{id}/fs', array('uses' => 'ActionController@postFinishScoring', 'as' => 'postFinishScoring'));

        // URL to Verify Key Ratio Summary
        Route::get('/test-key-ratio-summary/{id}', array('uses' => 'ActionController@getKeySummaryByEntityId', 'as' => 'getKeySummaryByEntityId'));
        Route::get('/rating/net-assets/{id}', array('uses' => 'ActionController@getNetAssetsByEntityId', 'as' => 'getNetAssetsByEntityId'));

        // FINANCIAL ANALYSIS
        Route::post('/summary/smesummary/{id}/fdocument', array('uses' => 'ActionController@postFinancialAnalysisDocuments', 'as' => 'postFinancialAnalysisDocuments'));

        // CREATE CBPO RATING PDF
        Route::get('/show/cbpo_pdf/{entity_id}', array('uses' => 'PrintSummaryController@showCbpoPdf', 'as' => 'showCbpoPdf'));
        Route::get('/create/cbpo_pdf/{entity_id}', array('uses' => 'ActionController@createSMEReport', 'as' => 'createSMEReport'));
        Route::get('/check/cbpo_pdf/{entity_id}', array('uses' => 'ActionController@checkOutdatedCbpoReport', 'as' => 'checkOutdatedCbpoReport'));

        // CREATE DSCR PDF
        Route::get('/show/dscr_print/{entity_id}', array('uses' => 'PrintSummaryController@getSMEReport', 'as' => 'getSMEReport'));

        // USER FEEDBACK (TABS)
        Route::get('/feedback/tab_contents/{tab_type}', array('uses' => 'UserFeedbackController@showTabUserFeedback', 'as' => 'showTabUserFeedback'));
        Route::post('/feedback/tab_contents', array('uses' => 'UserFeedbackController@addTabUserFeedback', 'as' => 'addTabUserFeedback'));
        Route::post('/feedback/ignore_tab_rating', array('uses' => 'UserFeedbackController@ignoreTabUserFeedback', 'as' => 'ignoreTabUserFeedback'));

        Route::get('/rest/check_suspicious_attempts', array('uses' => 'LoginController@checkSuspiciousAttempts', 'as' => 'checkSuspiciousAttempts'));
        Route::post('/rest/check_suspicious_attempts', array('uses' => 'LoginController@post2faChallenge', 'as' => 'post2faChallenge'));

        //premium confirmation
        Route::get('/premium/confirmation/{name}', array('uses' => 'FormSMEController@getPremiumConfirmation', 'as' => 'getPremiumConfirmation'));

        Route::post('/sme/upload_documents/{entity_id}', array('uses' => 'DocumentController@postUploadDocuments', 'as' => 'postUploadDocuments'));

        Route::post('/sme/upload_assoc_files/{entity_id}', array('uses' => 'FileController@postUploadAssocFiles', 'as' => 'postUploadAssocFiles'));

        Route::post('/sme/upload_fsPdf_files/{entity_id}', array('uses' => 'FileController@postUploadFspdfFiles', 'as' => 'postUploadFspdfFiles'));

        Route::get('/sme/download', array('uses' => 'FileController@download', 'as' => 'download'));

        Route::get('/dropbox/cleanup_dropbox_local', array('uses' => 'DropboxController@cleanupDropboxLocal', 'as' => 'cleanupDropboxLocal'));

        Route::get('/dropbox/cleanup_dropbox_production', array('uses' => 'DropboxController@cleanupDropboxProduction', 'as' => 'cleanupDropboxProduction'));

        Route::post('/sme/upload_ubill_files/{entity_id}', array('uses' => 'UtilityBillController@postUploadUbill', 'as' => 'postUploadUbill'));

        // save files from checklist
        Route::post('/sme/save_checklist/{entity_id}', array('uses' => 'ChecklistController@postSaveChecklist', 'as' => 'postSaveChecklist'));
        Route::get('/sme/delete_checklist_file/{id}', array('uses' => 'ChecklistController@getDeleteChecklist', 'as' => 'getDeleteChecklist'));

        Route::group(['middleware' => 'checkbasic'], function()
        {
            Route::get('/billingdetails', 'DashboardController@getBillingDetails');
            Route::post('/billingdetails', array('uses' => 'DashboardController@postBillingDetails', 'as' => 'postBillingDetails'));

            Route::get('/payment/checkout_paypal/{id}', 'PaymentController@postPayViaPaypal');
            Route::get('/payment/index','PaymentController@paymentIndex');
            Route::get('/payment/checkout_dragonpay/{id}', 'PaymentController@paymentCheckout');
            Route::get('/payment/selection', array('uses'=>'PaymentController@paymentSelect', 'as'=>'paymentSelect'));
            Route::get('/payment/show_requirments', 'PaymentController@showRequirements');

            // create new report
            Route::get('/create_new_report/{counter?}', array('uses' => 'SignupController@getNewReport', 'as' => 'getNewReport'));
            Route::post('/create_new_report/{counter?}', array('uses' => 'SignupController@postNewReport', 'as' => 'postNewReport'));
            Route::post('/check_client_key_zone', array('uses' => 'SignupController@checkClientKeyZone', 'as' => 'checkClientKeyZone'));
            Route::get('/company_search', array('uses' => 'SignupController@companySearch', 'as' => 'companySearch'));
            Route::post('/get_discount_organization', array('uses' => 'SignupController@getDiscountOrganization', 'as' => 'getDiscountOrganization'));
            Route::post('/get_serial_key_organization', array('uses' => 'SignupController@getSerialKeyOrganization', 'as' => 'getSerialKeyOrganization'));

            // updating new report
            Route::post('/signup/editreport', array('uses' => 'SignupController@postUpdateNewReport', 'as' => 'postUpdateNewReport'));

            Route::get('/sme/busdetails/{id}', array('uses' => 'FormSMEController@getSMEBusDetails', 'as' => 'getSMEBusDetails'));
            Route::get('/sme/busdetails/{id}/edit', array('uses' => 'FormSMEController@getSMEBusDetails', 'as' => 'getSMEBusDetails'));
            Route::put('/sme/busdetails/{id}', array('uses' => 'FormSMEController@putSMEBusDetailsUpdate', 'as' => 'putSMEBusDetailsUpdate'));

            Route::get('/sme/servicesoffer/{entity_id}', array('uses' => 'FormSMEController@getSMEservicesoffer', 'as' => 'getSMEservicesoffer'));
            Route::post('/sme/servicesoffer/{entity_id}', array('uses' => 'FormSMEController@postSMEservicesoffer', 'as' => 'postSMEservicesoffer'));
            Route::get('/sme/servicesoffer/{id}/edit', array('uses' => 'FormSMEController@getSMEservicesofferedit', 'as' => 'getSMEservicesofferedit'));
            Route::put('/sme/servicesoffer/{id}', array('uses' => 'FormSMEController@putSMEservicesofferUpdate', 'as' => 'putSMEservicesofferUpdate'));
            Route::get('/sme/servicesoffer/{id}/delete', array('uses' => 'FormSMEController@getSMEservicesofferDelete', 'as' => 'getSMEservicesofferDelete'));

            // Business Driver
            Route::get('/sme/businessdriver/{entity_id}', array('uses' => 'BusinessdriverController@getBusinessdriver', 'as' => 'getBusinessdriver'));
            Route::post('/sme/businessdriver/{entity_id}', array('uses' => 'BusinessdriverController@postBusinessdriver', 'as' => 'postBusinessdriver'));
            Route::get('/sme/businessdriver/{id}/edit', array('uses' => 'BusinessdriverController@getBusinessdriverEdit', 'as' => 'getBusinessdriverEdit'));
            Route::put('/sme/businessdriver/{id}', array('uses' => 'BusinessdriverController@putBusinessdriverUpdate', 'as' => 'putBusinessdriverUpdate'));
            Route::get('/sme/businessdriver/{id}/delete', array('uses' => 'BusinessdriverController@getBusinessdriverDelete', 'as' => 'getBusinessdriverDelete'));

            // Business Segments
            Route::get('/sme/businesssegment/{entity_id}', array('uses' => 'BusinesssegmentController@getBusinesssegment', 'as' => 'getBusinesssegment'));
            Route::post('/sme/businesssegment/{entity_id}', array('uses' => 'BusinesssegmentController@postBusinesssegment', 'as' => 'postBusinesssegment'));
            Route::get('/sme/businesssegment/{id}/edit', array('uses' => 'BusinesssegmentController@getBusinesssegmentEdit', 'as' => 'getBusinesssegmentEdit'));
            Route::put('/sme/businesssegment/{id}', array('uses' => 'BusinesssegmentController@putBusinesssegmentUpdate', 'as' => 'putBusinesssegmentUpdate'));
            Route::get('/sme/businesssegment/{id}/delete', array('uses' => 'BusinesssegmentController@getBusinesssegmentDelete', 'as' => 'getBusinesssegmentDelete'));

            // Major Supplier
            Route::get('/sme/majorsupplier/{entity_id}', array('uses' => 'MajorsupplierController@getMajorsupplier', 'as' => 'getMajorsupplier'));
            Route::post('/sme/majorsupplier/{entity_id}', array('uses' => 'MajorsupplierController@postMajorsupplier', 'as' => 'postMajorsupplier'));
            Route::get('/sme/majorsupplier/{id}/edit', array('uses' => 'MajorsupplierController@getMajorsupplierEdit', 'as' => 'getMajorsupplierEdit'));
            Route::put('/sme/majorsupplier/{id}', array('uses' => 'MajorsupplierController@putMajorsupplierUpdate', 'as' => 'putMajorsupplierUpdate'));
            Route::get('/sme/majorsupplier/{id}/delete', array('uses' => 'MajorsupplierController@getMajorsupplierDelete', 'as' => 'getMajorsupplierDelete'));
            Route::post('/rest/update_major_account/supplier', array('uses' => 'MajorsupplierController@updateMajorAccount', 'as' => 'updateMajorAccountSupplier'));

            // Competitor
            Route::get('/sme/competitor/{entity_id}', array('uses' => 'CompetitorController@getCompetitor', 'as' => 'getCompetitor'));
            Route::post('/sme/competitor/{entity_id}', array('uses' => 'CompetitorController@postCompetitor', 'as' => 'postCompetitor'));
            Route::get('/sme/competitor/{id}/edit', array('uses' => 'CompetitorController@getCompetitorEdit', 'as' => 'getCompetitorEdit'));
            Route::put('/sme/competitor/{id}', array('uses' => 'CompetitorController@putCompetitorUpdate', 'as' => 'putCompetitorUpdate'));
            Route::get('/sme/competitor/{id}/delete', array('uses' => 'CompetitorController@getCompetitorDelete', 'as' => 'getCompetitorDelete'));

            // Past Projects Completed
            Route::get('/sme/pastprojectscompleted/{entity_id}', array('uses' => 'PastprojectController@getPastproject', 'as' => 'getPastproject'));
            Route::post('/sme/pastprojectscompleted/{entity_id}', array('uses' => 'PastprojectController@postPastproject', 'as' => 'postPastproject'));
            Route::get('/sme/pastprojectscompleted/{id}/edit', array('uses' => 'PastprojectController@getPastprojectEdit', 'as' => 'getPastprojectEdit'));
            Route::put('/sme/pastprojectscompleted/{id}', array('uses' => 'PastprojectController@putPastprojectUpdate', 'as' => 'putPastprojectUpdate'));
            Route::get('/sme/pastprojectscompleted/{id}/delete', array('uses' => 'PastprojectController@getPastprojectDelete', 'as' => 'getPastprojectDelete'));

            // Future Growth Initiative
            Route::get('/sme/futuregrowthinitiative/{entity_id}', array('uses' => 'FuturegrowthController@getFuturegrowth', 'as' => 'getFuturegrowth'));
            Route::post('/sme/futuregrowthinitiative/{entity_id}', array('uses' => 'FuturegrowthController@postFuturegrowth', 'as' => 'postFuturegrowth'));
            Route::get('/sme/futuregrowthinitiative/{id}/edit', array('uses' => 'FuturegrowthController@getFuturegrowthEdit', 'as' => 'getFuturegrowthEdit'));
            Route::put('/sme/futuregrowthinitiative/{id}', array('uses' => 'FuturegrowthController@putFuturegrowthUpdate', 'as' => 'putFuturegrowthUpdate'));
            Route::get('/sme/futuregrowthinitiative/{id}/delete', array('uses' => 'FuturegrowthController@getFuturegrowthDelete', 'as' => 'getFuturegrowthDelete'));

            // Risk Assessment
            Route::get('/sme/riskassessment/{entity_id}', array('uses' => 'RiskassessmentController@getRiskassessment', 'as' => 'getRiskassessment'));
            Route::post('/sme/riskassessment/{entity_id}', array('uses' => 'RiskassessmentController@postRiskassessment', 'as' => 'postRiskassessment'));
            Route::get('/sme/riskassessment/{id}/edit', array('uses' => 'RiskassessmentController@getRiskassessmentEdit', 'as' => 'getRiskassessmentEdit'));
            Route::put('/sme/riskassessment/{id}', array('uses' => 'RiskassessmentController@putRiskassessmentUpdate', 'as' => 'putRiskassessmentUpdate'));
            Route::get('/sme/riskassessment/{id}/delete', array('uses' => 'RiskassessmentController@getRiskassessmentDelete', 'as' => 'getRiskassessmentDelete'));

            // Major Market Location
            Route::get('/sme/majorlocation/{entity_id}', array('uses' => 'MajorlocationController@getMajorlocation', 'as' => 'getMajorlocation'));
            Route::post('/sme/majorlocation/{entity_id}', array('uses' => 'MajorlocationController@postMajorlocation', 'as' => 'postMajorlocation'));
            Route::get('/sme/majorlocation/{id}/edit', array('uses' => 'MajorlocationController@getMajorlocationEdit', 'as' => 'getMajorlocationEdit'));
            Route::put('/sme/majorlocation/{id}', array('uses' => 'MajorlocationController@putMajorlocationUpdate', 'as' => 'putMajorlocationUpdate'));
            Route::get('/sme/majorlocation/{id}/delete', array('uses' => 'MajorlocationController@getMajorlocationDelete', 'as' => 'getMajorlocationDelete'));

            // Branches
            Route::get('/sme/branches/{entity_id}', array('uses' => 'BranchesController@getBranches', 'as' => 'getBranches'));
            Route::post('/sme/branches/{entity_id}', array('uses' => 'BranchesController@postBranches', 'as' => 'postBranches'));
            Route::get('/sme/branches/{id}/edit', array('uses' => 'BranchesController@getBranchesEdit', 'as' => 'getBranchesEdit'));
            Route::put('/sme/branches/{id}', array('uses' => 'BranchesController@putBranchesUpdate', 'as' => 'putBranchesUpdate'));
            Route::get('/sme/branches/{id}/delete', array('uses' => 'BranchesController@getBranchesDelete', 'as' => 'getBranchesDelete'));

            // Locations
            Route::get('/sme/locations/{entity_id}', array('uses' => 'LocationsController@getLocations', 'as' => 'getLocations'));
            Route::post('/sme/locations/{entity_id}', array('uses' => 'LocationsController@postLocations', 'as' => 'postLocations'));
            Route::get('/sme/locations/{id}/edit', array('uses' => 'LocationsController@getLocationsEdit', 'as' => 'getLocationsEdit'));
            Route::put('/sme/locations/{id}', array('uses' => 'LocationsController@putLocationsUpdate', 'as' => 'putLocationsUpdate'));
            Route::get('/sme/locations/{id}/delete', array('uses' => 'LocationsController@getLocationsDelete', 'as' => 'getLocationsDelete'));

            // Location Premium
            Route::get('/sme/locationsPremium/{entity_id}', array('uses' => 'LocationsController@getLocationsPremium', 'as' => 'getLocationsPremium'));

            // Insurance Coverage
            Route::get('/sme/insurance/{entity_id}', array('uses' => 'InsuranceController@getInsurance', 'as' => 'getInsurance'));
            Route::post('/sme/insurance/{entity_id}', array('uses' => 'InsuranceController@postInsurance', 'as' => 'postInsurance'));
            Route::get('/sme/insurance/{id}/edit', array('uses' => 'InsuranceController@getInsuranceEdit', 'as' => 'getInsuranceEdit'));
            Route::put('/sme/insurance/{id}', array('uses' => 'InsuranceController@putInsuranceUpdate', 'as' => 'putInsuranceUpdate'));
            Route::get('/sme/insurance/{id}/delete', array('uses' => 'InsuranceController@getInsuranceDelete', 'as' => 'getInsuranceDelete'));

            // Machine and Equipment
            Route::get('/sme/machineEquipment/{entity_id}', array('uses' => 'MachineEquipmentController@getMachineEquipment', 'as' => 'getMachineEquipment'));
            Route::post('/sme/machineEquipment/{entity_id}', array('uses' => 'MachineEquipmentController@postMachineEquipment', 'as' => 'postMachineEquipment'));
            Route::get('/sme/machineEquipment/{id}/edit', array('uses' => 'MachineEquipmentController@getMachineEquipmentEdit', 'as' => 'getMachineEquipmentEdit'));
            Route::put('/sme/machineEquipment/{id}', array('uses' => 'MachineEquipmentController@putMachineEquipmentUpdate', 'as' => 'putMachineEquipmentUpdate'));
            Route::get('/sme/machineEquipment/{id}/delete', array('uses' => 'MachineEquipmentController@getMachineEquipmentDelete', 'as' => 'getMachineEquipmentDelete'));

            // Certifications, Accreditations, and Tax Exemptions
            Route::get('/sme/certification/{entity_id}', array('uses' => 'CertificationController@getCertification', 'as' => 'getCertification'));
            Route::post('/sme/certification/{entity_id}', array('uses' => 'CertificationController@postCertification', 'as' => 'postCertification'));
            Route::get('/sme/certification/{id}/edit', array('uses' => 'CertificationController@getCertificationEdit', 'as' => 'getCertificationEdit'));
            Route::put('/sme/certification/{id}', array('uses' => 'CertificationController@putCertificationUpdate', 'as' => 'putCertificationUpdate'));
            Route::get('/sme/certification/{id}/delete', array('uses' => 'CertificationController@getCertificationDelete', 'as' => 'getCertificationDelete'));

            // Capital Details
            Route::get('/sme/capital/{entity_id}', array('uses' => 'CapitalController@getCapital', 'as' => 'getCapital'));
            Route::post('/sme/capital/{entity_id}', array('uses' => 'CapitalController@postCapital', 'as' => 'postCapital'));
            Route::get('/sme/capital/{id}/edit', array('uses' => 'CapitalController@getCapitalEdit', 'as' => 'getCapitalEdit'));
            Route::put('/sme/capital/{id}', array('uses' => 'CapitalController@putCapitalUpdate', 'as' => 'putCapitalUpdate'));
            Route::get('/sme/capital/{id}/delete', array('uses' => 'CapitalController@getCapitalDelete', 'as' => 'getCapitalDelete'));

            // Premium Capital Details
            Route::get('/sme/capitalpremium/{entity_id}', array('uses' => 'CapitalController@getCapitalPremium', 'as' => 'getCapitalPremium'));

            // Related Companies
            Route::get('/sme/relatedcompanies/{entity_id}', array('uses' => 'RelatedcompaniesController@getRelatedcompanies', 'as' => 'getRelatedcompanies'));
            Route::post('/sme/relatedcompanies/{entity_id}', array('uses' => 'RelatedcompaniesController@postRelatedcompanies', 'as' => 'postRelatedcompanies'));
            Route::get('/sme/relatedcompanies/{id}/edit', array('uses' => 'RelatedcompaniesController@getRelatedcompaniesEdit', 'as' => 'getRelatedcompaniesEdit'));
            Route::put('/sme/relatedcompanies/{id}', array('uses' => 'RelatedcompaniesController@putRelatedcompaniesUpdate', 'as' => 'putRelatedcompaniesUpdate'));
            Route::get('/sme/relatedcompanies/{id}/delete', array('uses' => 'RelatedcompaniesController@getRelatedcompaniesDelete', 'as' => 'getRelatedcompaniesDelete'));

            // Related Companies Premium
            Route::get('/sme/relatedcompaniespremium/{entity_id}', array('uses' => 'RelatedcompaniesController@getPremiumRelatedCompanies', 'as' => 'getPremiumRelatedCompanies'));



            // Import/Export Business
            Route::get('/sme/businesstype/{entity_id}', array('uses' => 'BusinesstypeController@getBusinesstype', 'as' => 'getBusinesstype'));
            Route::post('/sme/businesstype/{entity_id}', array('uses' => 'BusinesstypeController@postBusinesstype', 'as' => 'postBusinesstype'));
            Route::get('/sme/businesstype/{id}/delete', array('uses' => 'BusinesstypeController@getBusinesstypeDelete', 'as' => 'getBusinesstypeDelete'));

            // Major Customer
            Route::get('/sme/majorcustomer/{entity_id}', array('uses' => 'MajorcustomerController@getMajorcustomer', 'as' => 'getMajorcustomer'));
            Route::post('/sme/majorcustomer/{entity_id}', array('uses' => 'MajorcustomerController@postMajorcustomer', 'as' => 'postMajorcustomer'));
            Route::get('/sme/majorcustomer/{id}/edit', array('uses' => 'MajorcustomerController@getMajorcustomerEdit', 'as' => 'getMajorcustomerEdit'));
            Route::put('/sme/majorcustomer/{id}', array('uses' => 'MajorcustomerController@putMajorcustomerUpdate', 'as' => 'putMajorcustomerUpdate'));
            Route::get('/sme/majorcustomer/{id}/delete', array('uses' => 'MajorcustomerController@getMajorcustomerDelete', 'as' => 'getMajorcustomerDelete'));
            Route::post('/rest/update_major_account/customer', array('uses' => 'MajorcustomerController@updateMajorAccount', 'as' => 'updateMajorAccount'));

            // Upload Documents
            Route::get('/sme/upload_fs_template/{entity_id}', array('uses' => 'DocumentController@getUploadFsTemplate', 'as' => 'getUploadFsTemplate'));
            Route::get('/sme/upload_fs_template_premium/{entity_id}', array('uses' => 'DocumentController@getUploadFsTemplatePremium', 'as' => 'getUploadFsTemplatePremium'));
            Route::get('/sme/upload_documents/{entity_id}', array('uses' => 'DocumentController@getUploadDocuments', 'as' => 'getUploadDocuments'));
            Route::get('/sme/upload_quickbooks/{entity_id}', array('uses' => 'DocumentController@getUploadQuickbooks', 'as' => 'getUploadQuickbooks'));
            Route::post('/sme/upload_quickbooks/{entity_id}', array('uses' => 'DocumentController@postUploadQuickbooks', 'as' => 'postUploadQuickbooks'));
            Route::get('/sme/upload-other-documents/{entity_id}', array('uses' => 'DocumentController@getOtherDocuments', 'as' => 'getOtherDocuments'));
            Route::post('/sme/upload-other-documents/{entity_id}', array('uses' => 'DocumentController@postOtherDocuments', 'as' => 'postOtherDocuments'));
            Route::get('/sme/delete_other_document/{entity_id}', array('uses' => 'DocumentController@getDeleteOtherDocs', 'as' => 'getDeleteOtherDocs'));
            Route::get('/sme/delete_document/{id}', array('uses' => 'DocumentController@getDeleteDocument', 'as' => 'getDeleteDocument'));
            Route::get('/sme/delete_quickbooks/{id}', array('uses' => 'DocumentController@getDeleteQuickbooks', 'as' => 'getDeleteQuickbooks'));
            Route::get('/sme/delete_cash_proxy/{doc_id}', array('uses' => 'CashFlowProxyController@getDeleteCashProxy', 'as' => 'getDeleteCashProxy'));

            // Dropbox upload
            Route::get('/sme/dropbox_upload_files/{entity_id}', array('uses' => 'DropboxController@getAssocFilesUpload', 'as' => 'getAssocFilesUpload'));
            Route::get('/sme/delete_assocfile/{id}', array('uses' => 'FileController@getDeleteAssocFiles', 'as' => 'getDeleteAssocFiles'));
            Route::get('/sme/fspdf_upload_files/{entity_id}', array('uses' => 'DropboxController@getFspdfUpload', 'as' => 'getFspdfUpload'));
            Route::get('/sme/delete_fspdfFile/{id}', array('uses' => 'FileController@getDeleteFspdfFiles', 'as' => 'getDeleteFspdfFiles'));
            Route::get('/sme/download_fspdfFile/{id}', array('uses' => 'FileController@getDownloadFspdfFiles', 'as' => 'getDownloadFspdfFiles'));

            // Utility Bill upload
            Route::get('/sme/ubill_upload_files/{entity_id}', array('uses' => 'UtilityBillController@getUbillUpload', 'as' => 'getUbillUpload'));
            Route::get('/sme/delete_ubill/{id}', array('uses' => 'UtilityBillController@getDeleteUbill', 'as' => 'getDeleteUbill'));
            Route::get('/sme/delete_ubill_premium/{id}', array('uses' => 'UtilityBillController@getDeleteUbillPremium', 'as' => 'getDeleteUbillPremium'));

            Route::get('/sme/upload-bank-statements/{entity_id}', array('uses' => 'CashFlowProxyController@getBankStatements', 'as' => 'getBankStatements'));
            Route::post('/sme/upload-bank-statements/{entity_id}', array('uses' => 'CashFlowProxyController@postBankStatements', 'as' => 'postBankStatements'));

            Route::get('/sme/upload-tax-payments/{entity_id}', array('uses' => 'CashFlowProxyController@getTaxPayments', 'as' => 'getTaxPayments'));
            Route::post('/sme/upload-tax-payments/{entity_id}', array('uses' => 'CashFlowProxyController@postTaxPayments', 'as' => 'postTaxPayments'));

            Route::get('/sme/upload-utility-bills/{entity_id}', array('uses' => 'CashFlowProxyController@getUtilityBills', 'as' => 'getUtilityBills'));
            Route::post('/sme/upload-utility-bills/{entity_id}', array('uses' => 'CashFlowProxyController@postUtilityBills', 'as' => 'postUtilityBills'));

            Route::get('/sme/busconandsus/{type}/{entity_id}', array('uses' => 'FormSMEController@getSMEBusConSus', 'as' => 'getSMEBusConSus'))->where('type', '(taxpayments|economicfactors|competitionlandscape|succession)');
            Route::post('/sme/busconandsus/{type}/{entity_id}', array('uses' => 'FormSMEController@postSMEBusConSus', 'as' => 'postSMEBusConSus'))->where('type', '(taxpayments|economicfactors|competitionlandscape|succession)');

            Route::get('/sme/planfacility/{entity_id}', array('uses' => 'FormSMEController@getSMEPlanFacility', 'as' => 'getSMEPlanFacility'));
            Route::post('/sme/planfacility/{entity_id}', array('uses' => 'FormSMEController@postSMEPlanFacilityCreate', 'as' => 'postSMEPlanFacilityCreate'));
            Route::get('/sme/planfacility/{id}/edit', array('uses' => 'FormSMEController@getSMEPlanFacilityEdit', 'as' => 'getSMEPlanFacilityEdit'));
            Route::put('/sme/planfacility/{id}', array('uses' => 'FormSMEController@putSMEPlanFacilityUpdate', 'as' => 'putSMEPlanFacilityUpdate'));
            Route::get('/sme/planfacility/{id}/delete', array('uses' => 'FormSMEController@getSMEPlanFacilityDelete', 'as' => 'getSMEPlanFacilityDelete'));

            Route::get('/sme/planfacilityrequested/{entity_id}', array('uses' => 'FormSMEController@getSMEPlanFacilityRequested', 'as' => 'getSMEPlanFacilityRequested'));
            Route::post('/sme/planfacilityrequested/{entity_id}', array('uses' => 'FormSMEController@postSMEPlanFacilityCreateRequested', 'as' => 'postSMEPlanFacilityCreateRequested'));
            Route::get('/sme/planfacilityrequested/{id}/edit', array('uses' => 'FormSMEController@getSMEPlanFacilityEditRequested', 'as' => 'getSMEPlanFacilityEditRequested'));
            Route::put('/sme/planfacilityrequested/{id}', array('uses' => 'FormSMEController@putSMEPlanFacilityUpdateRequested', 'as' => 'putSMEPlanFacilityUpdateRequested'));
            Route::get('/sme/planfacilityrequested/{id}/delete', array('uses' => 'FormSMEController@getSMEPlanFacilityDeleteRequested', 'as' => 'getSMEPlanFacilityDeleteRequested'));

            Route::get('/sme/futuresource', array('uses' => 'FormSMEController@getSMEFutureSource', 'as' => 'getSMEFutureSource'));
            Route::post('/sme/futuresource/{id}', array('uses' => 'FormSMEController@postSMEFutureSourceCreate', 'as' => 'postSMEFutureSourceCreate'));
            Route::get('/sme/futuresource/{id}/edit', array('uses' => 'FormSMEController@getSMEFutureSourceEdit', 'as' => 'getSMEFutureSourceEdit'));
            Route::put('/sme/futuresource/{id}', array('uses' => 'FormSMEController@putSMEFutureSourceUpdate', 'as' => 'putSMEFutureSourceUpdate'));

            Route::get('/sme/busowner/{entity_id}', array('uses' => 'FormSMEController@getSMEBusOwner', 'as' => 'getSMEBusOwner'));
            Route::post('/sme/busowner/{entity_id}', array('uses' => 'FormSMEController@postSMEBusOwnerStore', 'as' => 'postSMEBusOwnerStore'));
            Route::get('/sme/busowner/{id}/delete', array('uses' => 'FormSMEController@deleteSMEBusOwner', 'as' => 'deleteSMEBusOwner'));

            // CAPACITY FACTOR
            Route::get('/sme/revenuepotential/{entity_id}', array('uses' => 'CapacityController@getRevenuePotential', 'as' => 'getRevenuePotential'));
            Route::post('/sme/revenuepotential/{entity_id}', array('uses' => 'CapacityController@postRevenuePotential', 'as' => 'postRevenuePotential'));
            Route::get('/sme/revenuepotential/{id}/edit', array('uses' => 'CapacityController@getRevenuePotentialEdit', 'as' => 'getRevenuePotentialEdit'));
            Route::put('/sme/revenuepotential/{id}', array('uses' => 'CapacityController@putRevenuePotentialUpdate', 'as' => 'putRevenuePotentialUpdate'));
            Route::get('/sme/revenuepotential/{id}/delete', array('uses' => 'CapacityController@getRevenuePotentialDelete', 'as' => 'getRevenuePotentialDelete'));

            Route::get('/sme/phpm/{entity_id}', array('uses' => 'CapacityController@getPhpm', 'as' => 'getPhpm'));
            Route::post('/sme/phpm/{entity_id}', array('uses' => 'CapacityController@postPhpm', 'as' => 'postPhpm'));
            Route::get('/sme/phpm/{id}/edit', array('uses' => 'CapacityController@getPhpmEdit', 'as' => 'getPhpmEdit'));
            Route::put('/sme/phpm/{id}', array('uses' => 'CapacityController@putRevenuePhpmUpdate', 'as' => 'putRevenuePhpmUpdate'));
            Route::get('/sme/phpm/{id}/delete', array('uses' => 'CapacityController@getPhpmDelete', 'as' => 'getPhpmDelete'));

            // Owner Details
            Route::get('/sme/owner/{id}/edit', array('uses' => 'OwnerController@getOwneredit', 'as' => 'getOwneredit'));
            Route::put('/sme/owner/{id}', array('uses' => 'OwnerController@putOwnerUpdate', 'as' => 'putOwnerUpdate'));
            Route::get('/sme/owner/{id}/delete', array('uses' => 'OwnerController@getOwnerDelete', 'as' => 'getOwnerDelete'));

            Route::get('/sme/owner_loans/{entity_id}/{owner_id}', array('uses' => 'OwnerController@getPersonalLoans', 'as' => 'getPersonalLoans'));
            Route::post('/sme/owner_loans/{entity_id}/{owner_id}', array('uses' => 'OwnerController@postPersonalLoans', 'as' => 'postPersonalLoans'));
            Route::get('/sme/owner_loans/{id}/edit', array('uses' => 'OwnerController@getPersonalLoansEdit', 'as' => 'getPersonalLoansEdit'));
            Route::get('/sme/delete/owner_loans/{id}', array('uses' => 'OwnerController@deletePersonalLoans', 'as' => 'deletePersonalLoans'));
            Route::put('/sme/owner_loans/{id}', array('uses' => 'OwnerController@putPersonalLoans', 'as' => 'putPersonalLoans'));

            Route::get('/sme/spouse/{id}', array('uses' => 'OwnerController@getSpouse', 'as' => 'getSpouse'));
            Route::post('/sme/spouse/', array('uses' => 'OwnerController@postSpouse', 'as' => 'postSpouse'));
            Route::get('/sme/spouse/{id}/edit', array('uses' => 'OwnerController@getSpouseedit', 'as' => 'getSpouseedit'));
            Route::put('/sme/spouse/{id}', array('uses' => 'OwnerController@putSpouseUpdate', 'as' => 'putSpouseUpdate'));

            Route::get('/sme/keymanager/{entity_id}/{owner_id}', array('uses' => 'OwnerController@getKeyManager', 'as' => 'getKeyManager'));
            Route::post('/sme/keymanager/{entity_id}/{owner_id}', array('uses' => 'OwnerController@postKeyManager', 'as' => 'postKeyManager'));
            Route::get('/sme/keymanager/{entity_id}', array('uses' => 'OwnerController@getKeyManagerOnly', 'as' => 'getKeyManagerOnly'));
            Route::post('/sme/keymanager/{entity_id}', array('uses' => 'OwnerController@postKeyManagerOnly', 'as' => 'postKeyManagerOnly'));
            Route::get('/sme/keymanager/{id}/edit', array('uses' => 'OwnerController@getKeyManagerEdit', 'as' => 'getKeyManagerEdit'));
            Route::put('/sme/keymanager/{id}', array('uses' => 'OwnerController@putKeyManager', 'as' => 'putKeyManager'));
            Route::get('/sme/delete/keymanager/{id}', array('uses' => 'OwnerController@deleteKeyManager', 'as' => 'deleteKeyManager'));


            // Key manager Premium
            Route::get('/sme/keymanager2/{entity_id}', array('uses' => 'OwnerController@getKeyManager2', 'as' => 'getKeyManager2'));
            Route::post('/sme/keymanager2/{entity_id}', array('uses' => 'OwnerController@postKeymanager2', 'as' => 'postKeymanager2'));
            Route::get('/sme/keymanager2/{id}/delete', array('uses' => 'OwnerController@deleteKeyManager', 'as' => 'deleteKeyManager'));


            Route::get('/sme/managebus/{entity_id}/{owner_id}/{type}', array('uses' => 'OwnerController@getManageBusiness', 'as' => 'getManageBusiness'));
            Route::post('/sme/managebus/{entity_id}/{owner_id}/{type}', array('uses' => 'OwnerController@postManageBusiness', 'as' => 'postManageBusiness'));
            Route::get('/sme/delete/managebus/{id}', array('uses' => 'OwnerController@deleteManageBusiness', 'as' => 'deleteManageBusiness'));

            Route::get('/sme/education/{entity_id}/{owner_id}/{type}', array('uses' => 'OwnerController@getEducation', 'as' => 'getEducation'));
            Route::post('/sme/education/{entity_id}/{owner_id}/{type}', array('uses' => 'OwnerController@postEducation', 'as' => 'postEducation'));
            Route::get('/sme/delete/education/{id}', array('uses' => 'OwnerController@deleteEducationBg', 'as' => 'deleteEducationBg'));

            Route::get('/sme/cycledetails/{entity_id}', array('uses' => 'CycleDetailController@getCycleDetail', 'as' => 'getCycleDetail'));
            Route::post('/sme/cycledetails/{entity_id}', array('uses' => 'CycleDetailController@postCycleDetail', 'as' => 'postCycleDetail'));

            Route::get('/sme/shareholders/{entity_id}', array('uses' => 'ManagementController@getShareholders', 'as' => 'getShareholders'));
            Route::post('/sme/shareholders/{entity_id}', array('uses' => 'ManagementController@postShareholders', 'as' => 'postShareholders'));
            Route::get('/sme/shareholders/{id}/edit', array('uses' => 'ManagementController@getShareholdersEdit', 'as' => 'getShareholdersEdit'));
            Route::put('/sme/shareholders/{id}/edit', array('uses' => 'ManagementController@putShareholders', 'as' => 'putShareholders'));
            Route::get('/sme/shareholders/{id}/delete', array('uses' => 'ManagementController@getShareholdersDelete', 'as' => 'getShareholdersDelete'));

            Route::get('/sme/directors/{entity_id}', array('uses' => 'ManagementController@getDirectors', 'as' => 'getDirectors'));
            Route::post('/sme/directors/{entity_id}', array('uses' => 'ManagementController@postDirectors', 'as' => 'postDirectors'));
            Route::get('/sme/directors/{id}/edit', array('uses' => 'ManagementController@getDirectorsEdit', 'as' => 'getDirectorsEdit'));
            Route::put('/sme/directors/{id}/edit', array('uses' => 'ManagementController@putDirectors', 'as' => 'putDirectors'));
            Route::get('/sme/directors/{id}/delete', array('uses' => 'ManagementController@getDirectorsDelete', 'as' => 'getDirectorsDelete'));

            Route::get('/sme/upload_requested_documents', array('uses' => 'DocumentController@getUploadRequestedDocument', 'as' => 'getUploadRequestedDocument'));
            Route::post('/sme/upload_requested_documents', array('uses' => 'DocumentController@postUploadRequestedDocument', 'as' => 'postUploadRequestedDocument'));

            Route::get('/feedback/send_rating_email/{trans_id}', array('uses' => 'UserFeedbackController@sendFinalUserFeedback', 'as' => 'sendFinalUserFeedback'));

            Route::get('/rest/comp-sts-tb/{entity_id}', array('uses' => 'ProfileSMEController@getSmeSummaryTbl', 'as' => 'getSmeSummaryTbl'));
            Route::get('/rest/comp-sts-tb-premium/{entity_id}', array('uses' => 'ProfileSMEController@getSmeSummaryTblPremium', 'as' => 'getSmeSummaryTblPremium'));
            Route::get('/rest/get_unfilled_reminders/{entity_id}/{tab_name}', array('uses' => 'ProfileSMEController@getUnfilledReminder', 'as' => 'getUnfilledReminder'));

            Route::get('/rest/get_editable_field/{field_id}/{field_name}', array('uses' => 'UpdateDataController@getEditableField', 'as' => 'getEditableField'));
            Route::post('/rest/post_editable_field/{field_id}/{field_name}/{arr_tb}', array('uses' => 'UpdateDataController@postEditableField', 'as' => 'postEditableField'));

            Route::post('/rest/post_editable_upload/{field_id}/{field_name}/{arr_tb}', array('uses' => 'UpdateDataController@postEditableFileUpload', 'as' => 'postEditableFileUpload'));

            Route::post('/sme/update_pcqi', array('uses' => 'ProfileSMEController@postPotentialCreditQualityIssues', 'as' => 'postPotentialCreditQualityIssues'));
            Route::post('/sme/update_businesstype', array('uses' => 'ProfileSMEController@postUpdateBusinessType', 'as' => 'postUpdateBusinessType'));

            Route::post('/create-external-link', array('uses' => 'ExternalLinkController@postCreateExternalLink', 'as' => 'postCreateExternalLink'));
            Route::get('/update_with_new_gis', array('uses' => 'UpdateDataController@getUpdateNewGis', 'as' => 'getUpdateNewGis'));
            Route::get('/check_external_link', array('uses' => 'ExternalLinkController@getAnsweredLinks', 'as' => 'getAnsweredLinks'));

            Route::post('/update_progress_completion', array('uses' => 'ProfileSMEController@postUpdatePercentComplete', 'as' => 'postUpdatePercentComplete'));

            Route::get('/upgrade_to_full', array('uses' => 'DashboardController@getUpgradetoFull', 'as' => 'getUpgradetoFull'));

            // upload files to dropbox
            Route::get('/dropbox_upload', array('uses' => 'DropboxController@getDropboxUpload', 'as' => 'getDropboxUpload'));
            Route::post('/dropbox_upload', array('uses' => 'DropboxController@postDropboxUpload', 'as' => 'postDropboxUpload'));
            Route::post('/dropbox_delete', array('uses' => 'DropboxController@postDropboxDelete', 'as' => 'postDropboxDelete'));
            Route::get('/download_userfile/{fileDownload}', array('uses' => 'DropboxController@downloadUserFile', 'as' => 'downloadUserFile'));


        });
        Route::group(['middleware' => 'checkanalyst'], function() {

            // Adverse Record
            Route::get('/sme/adverserecord/{entity_id}', array('uses' => 'CreditlinesController@getAdverserecord', 'as' => 'getAdverserecord'));
            Route::post('/sme/adverserecord/{entity_id}', array('uses' => 'CreditlinesController@postAdverserecord', 'as' => 'postAdverserecord'));
            Route::get('/sme/adverserecord/{id}/edit', array('uses' => 'CreditlinesController@getAdverserecordEdit', 'as' => 'getAdverserecordEdit'));
            Route::put('/sme/adverserecord/{id}', array('uses' => 'CreditlinesController@putAdverserecordUpdate', 'as' => 'putAdverserecordUpdate'));
            Route::get('/sme/adverserecord/{id}/delete', array('uses' => 'CreditlinesController@getAdverserecordDelete', 'as' => 'getAdverserecordDelete'));
        });
        Route::group(['middleware' => 'checkinvana'], function() {
            Route::get('/sme/request-additional-info/{id}', array('uses' => 'ProfileSMEController@getRequestAdditionalInfo', 'as' => 'getRequestAdditionalInfo'));
            Route::post('/sme/request-additional-info', array('uses' => 'ProfileSMEController@postRequestAdditionalInfo', 'as' => 'postRequestAdditionalInfo'));
            Route::get('/tools/ocr', array('uses' => 'OCRController@getPDFtoCSV', 'as' => 'getPDFtoCSV'));
            Route::post('/tools/ocr', array('uses' => 'OCRController@postPDFtoCSV', 'as' => 'postPDFtoCSV'));
            Route::post('/ajax/tools/auto_cashflow/{entity_id}', array('uses' => 'OCRController@computeOperatingCashflow', 'as' => 'computeOperatingCashflow'));
        });
    });
});

Route::get('/payment_step/{code}', array('uses' => 'PaymentController@getPaymentOption', 'as' => 'getPaymentOption'));
Route::get('/payment_step2/{code}/{payment_option}', array('uses' => 'PaymentController@getPaymentOption2', 'as' => 'getPaymentOption2'));
Route::get('/payment_cancel', array('uses' => 'PaymentController@getCancelPayment', 'as' => 'getPaymentCancel'));
Route::get('/payment_success/{code}', array('uses' => 'PaymentController@getPaymentSuccess', 'as' => 'getPaymentSuccess'));
Route::get('/certificate/{code}', array('uses' => 'IndependentController@getCertificates', 'as' => 'getCertificates'));
Route::get('/download_certificate/{code}/{serial_key}', array('uses' => 'IndependentController@getDownloadCertificate', 'as' => 'getDownloadCertificate'));
Route::post('/update_certificate/{code}', array('uses' => 'IndependentController@postUpdateCertificate', 'as' => 'postUpdateCertificate'));

Route::get('/sendlogs', array('uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@sendLogs', 'as' => 'sendLogs'));

// ADMINISTRATOR
Route::group(['middleware' => 'authadminold'], function()
{
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/admin', array('uses' => 'AdminController@getAdminDashboard', 'as' => 'getAdminDashboard'));
    Route::get('/admin/add', array('uses' => 'AdminController@getAddUser', 'as' => 'getAddUser'));
    Route::post('/admin/add', array('uses' => 'AdminController@postAddUser', 'as' => 'postAddUser'));
    Route::get('/admin/edit/{loginid}', array('uses' => 'AdminController@getEditUser', 'as' => 'getEditUser'));
    Route::post('/admin/edit/{loginid}', array('uses' => 'AdminController@postEditUser', 'as' => 'postEditUser'));

    /** Report Section */
    Route::get('/admin/reports', array('uses' => 'AdminController@getReports', 'as' => 'getReports'));
    Route::post('/admin/reports/export', array('uses' => 'AdminController@exportReports', 'as' => 'exportReports'));
    Route::get('/admin/export/download', array('uses' => 'AdminController@downloadExportedReports', 'as' => 'downloadExportedReports'));
    Route::post('/admin/reports/receiver', array('uses' => 'AdminController@postReportsReceiver', 'as' => 'postReportsReceiver'));
    Route::get('/admin/reports/receiver/{id}', array('uses' => 'AdminController@deleteReportsReceiver', 'as' => 'deleteReportsReceiver'));

    /** Update industry table */
    Route::get('/admin/industry/comparison', array('uses' => 'AdminController@getIndustryUpload', 'as' => 'getIndustryUpload'));
    Route::post('/admin/industry/comparison', array('uses' => 'AdminController@postIndustryForecast', 'as' => 'postIndustryForecast'));

    //premium report
    Route::get('/admin/third_party', array('uses' => 'AdminController@getAdminThirdParty', 'as' => 'getAdminThirdParty'));
    Route::get('/admin/third_party/add', array('uses' => 'AdminController@getAddFormPremium', 'as' => 'getAddFormPremium'));
    Route::post('/admin/third_party/add', array('uses' => 'AdminController@postAddPremiumUser', 'as' => 'postAddPremiumUser'));
    Route::get('/admin/third_party/delete/{id}', array('uses' => 'AdminController@getPremiumUserDelete', 'as' => 'getPremiumUserDelete'));

    Route::get('/admin/cmap_search_count', array('uses' => 'AdminController@getCmapSearchCount', 'as' => 'getCmapSearchCount'));

    Route::get('/admin/analyst_report', array('uses' => 'AdminController@getAnalystThirdParty', 'as' => 'getAnalystThirdParty'));
    Route::get('/admin/analyst/delete/{id}', array('uses' => 'AdminController@getAnalystAccountDelete', 'as' => 'getAnalystAccountDelete'));
    Route::get('/admin/analyst_account/add', array('uses' => 'AdminController@getAddAccountAnalyst', 'as' => 'getAddAccountAnalyst'));
    Route::post('/admin/analyst_account/add', array('uses' => 'AdminController@postAddAccountAnalyst', 'as' => 'postAddAccountAnalyst'));

    Route::get('/admin/industry', array('uses' => 'AdminController@getIndustry', 'as' => 'getIndustry'));
    Route::get('/admin/industry/{id}', array('uses' => 'AdminController@getIndustry', 'as' => 'getIndustry'));
    Route::get('/admin/industry/edit/{industry_id}', array('uses' => 'AdminController@getEditIndustry', 'as' => 'getEditIndustry'));
    Route::post('/admin/industry/edit/{industry_id}', array('uses' => 'AdminController@postEditIndustry', 'as' => 'postEditIndustry'));
    Route::post('/admin/industry/reset', array('uses' => 'AdminController@postResetIndustry', 'as' => 'postResetIndustry'));

    Route::get('/admin/converter', array('uses' => 'AdminController@getConverter', 'as' => 'getConverter'));
    Route::post('/admin/converter', array('uses' => 'AdminController@postConverter', 'as' => 'postConverter'));
    Route::post('/admin/converter/update', array('uses' => 'AdminController@postUpdateConverter', 'as' => 'postUpdateConverter'));


    Route::post('/admin/upload-biz-outlook', array('uses' => 'AdminController@uploadBusinessOutlook', 'as' => 'uploadBusinessOutlook'));
    Route::post('/admin/bizoutlook/calculate', array('uses' => 'AdminController@calculateBusinessOutlooks', 'as' => 'calculateBusinessOutlooks'));
    Route::get('/admin/bizoutlook/bsphtm', array('uses' => 'AdminController@showBSPExpectation', 'as' => 'showBSPExpectation'));

    Route::get('/admin/bizoutlook/detect-bsp-pdf', array('uses' => 'AdminController@detectBspExpectation', 'as' => 'detectBspExpectation'));
    Route::get('/admin/bizoutlook/download-bsp-pdf/{filename}', array('uses' => 'AdminController@downloadBspExpectation', 'as' => 'downloadBspExpectation'));
    Route::post('/admin/bizoutlook/add-bsp-pdf', array('uses' => 'AdminController@addBspDownloadReport', 'as' => 'addBspDownloadReport'));

    Route::get('/admin/user-feedbacks', array('uses' => 'UserFeedbackController@getFeedbackDashboard', 'as' => 'getFeedbackDashboard'));
    Route::get('/admin/user-feedbacks/{user_id}', array('uses' => 'UserFeedbackController@getUserFeedback', 'as' => 'getUserFeedback'));

    Route::get('/admin/feedback-summary', array('uses' => 'UserFeedbackController@getFeedbackSummary', 'as' => 'getFeedbackSummary'));

    Route::get('/admin/clientkeys', array('uses' => 'AdminController@getClientKeys', 'as' => 'getClientKeys'));
    Route::get('/admin/supervisor_clientkeys', array('uses' => 'AdminController@getSupervisorClientKeys', 'as' => 'getSupervisorClientKeys'));
    Route::get('/admin/create_keys', array('uses' => 'AdminController@getCreateClientKeys', 'as' => 'getCreateClientKeys'));
    Route::post('/admin/create_keys', array('uses' => 'AdminController@postCreateClientKeys', 'as' => 'postCreateClientKeys'));
    Route::get('/rest/unlock-account/{login_id}', array('uses' => 'AdminController@unlockUserAccount', 'as' => 'unlockUserAccount'));

    Route::get('/admin/delete_key/{id}', array('uses' => 'AdminController@deleteKey', 'as' => 'deleteKey'));

    Route::get('/admin/discounts', array('uses' => 'AdminController@getDiscounts', 'as' => 'getDiscounts'));
    Route::get('/admin/add_discount', array('uses' => 'AdminController@getAddDiscount', 'as' => 'getAddDiscount'));
    Route::get('/admin/edit_discount/{id}', array('uses' => 'AdminController@getEditDiscount', 'as' => 'getEditDiscount'));
    Route::post('/admin/add_discount', array('uses' => 'AdminController@postAddDiscount', 'as' => 'postAddDiscount'));
    Route::post('/admin/edit_discount/{id}', array('uses' => 'AdminController@postEditDiscount', 'as' => 'postEditDiscount'));
    Route::get('/admin/discount_switch/{type}/{id}', array('uses' => 'AdminController@getSwitchDiscount', 'as' => 'getSwitchDiscount'));
    Route::get('/admin/delete_discount/{id}', array('uses' => 'AdminController@getDeleteDiscount', 'as' => 'getDeleteDiscount'));

    Route::get('/admin/autoupdate_download/{filename}', array('uses' => 'AdminController@getAutoUpdateDownload', 'as' => 'getAutoUpdateDownload'));
    Route::get('/admin/autoupdate_parse/{filename}', array('uses' => 'AdminController@getAutoUpdateParse', 'as' => 'getAutoUpdateParse'));


        Route::get('/admin/ocr_options', array('uses' => 'AdminController@getOCROptions', 'as' => 'getOCROptions'));
        Route::post('/admin/ocr_options', array('uses' => 'AdminController@postOCROptions', 'as' => 'postOCROptions'));

    Route::get('/admin/leads', array('uses' => 'AdminController@getLeads', 'as' => 'getLeads'));
    Route::get('/admin/leads_export', array('uses' => 'AdminController@getLeadsExport', 'as' => 'getLeadsExport'));
    Route::post('/admin/set_new_leads_email', array('uses' => 'AdminController@postSetLeadsEmail', 'as' => 'postSetLeadsEmail'));

    Route::get('/pse/extract_report/{edge_no}', array('uses' => 'PseController@testPseExtraction', 'as' => 'testPseExtraction'));
    Route::get('/pse/extract_doc/{doc_no}', array('uses' => 'PseController@testPseExtractDoc', 'as' => 'testPseExtractDoc'));
    Route::get('/pse/extraction_point', array('uses' => 'PseController@extractionPoint', 'as' => 'extractionPoint'));
    Route::get('/pse/company_list/{page}', array('uses' => 'PseController@testPseCompanyList', 'as' => 'testPseCompanyList'));
    Route::get('/pse/check_listing', array('uses' => 'PseController@checkPseCompanies', 'as' => 'checkPseCompanies'));
    Route::post('/pse/add_companies', array('uses' => 'PseController@addPseCompany', 'as' => 'addPseCompany'));
    Route::post('/pse/post_annual_report', array('uses' => 'PseController@addPseAnnualReport', 'as' => 'addPseAnnualReport'));
    Route::get('/pse/get_reports/{pse_id}', array('uses' => 'PseController@getPseCompanyReports', 'as' => 'getPseCompanyReports'));
    Route::post('/pse/view_annual', array('uses' => 'PseController@getPseCompanyAnnualReport', 'as' => 'getPseCompanyAnnualReport'));

    Route::get('/supervisor/list', array('uses' => 'AdminController@getSupervisorApplications', 'as' => 'getSupervisorApplications'));
    Route::get('/supervisor/approve_application/{status}/{application_id}', array('uses' => 'AdminController@approveSupervisorApplication', 'as' => 'approveSupervisorApplication'));
    Route::post('/supervisor/edit', array('uses' => 'AdminController@updateSubscriptionStatus', 'as' => 'updateSubscriptionStatus'));

    // Financial Report
    Route::get('/fr', array('uses' => 'FinancialReportController@getFinancialReportList', 'as' => 'getFinancialReportList'));
    Route::get('/fr/upload_vfa', array('uses' => 'FinancialReportController@getUploadVfa', 'as' => 'getUploadVfa'));
    Route::get('/fr/upload_excel', array('uses' => 'FinancialReportController@getUploadExcel', 'as' => 'getUploadExcel'));
    Route::get('/fr/clear', array('uses' => 'FinancialReportController@getClearFRTemplates', 'as' => 'getClearFRTemplates'));
    Route::get('/fr/view/{id}', array('uses' => 'FinancialReportController@getFinancialReport', 'as' => 'getFinancialReport'));
    Route::post('/fr/upload_vfa', array('uses' => 'FinancialReportController@postUploadVfa', 'as' => 'postUploadVfa'));
    Route::post('/fr/upload_excel', array('uses' => 'FinancialReportController@postUploadExcel', 'as' => 'postUploadExcel'));
    Route::get('/fr/upload_fs_input', array('uses' => 'FinancialReportController@getUploadFSInput', 'as' => 'getUploadFSInput'));
    Route::post('/fr/upload_fs_input', array('uses' => 'FinancialReportController@postUploadFSInput', 'as' => 'postUploadFSInput'));
    Route::get('/fr/key-ratio-view/{id}', array('uses' => 'FinancialReportController@getFSKeyRatio', 'as' => 'getFSKeyRatio'));

    Route::get('/admin/transactions', array('uses' => 'AdminController@getAdminTransactions', 'as' => 'getAdminTransactions'));
    Route::get('/admin/reviews', array('uses' => 'AdminController@getAdminReviews', 'as' => 'getAdminReviews'));
    Route::get('/admin/premium_reports', array('uses' => 'AdminController@getPremiumReports', 'as' => 'getPremiumReports'));
    Route::post("/add_third_party_email", array('uses' => 'AdminController@addThirdPartyEmails', 'as' => "addThirdPartyEmails"));
    Route::get("/delete_third_party_email_by_id/{id}", array('uses' => 'AdminController@deleteThirdPartyEmails', 'as' => "deleteThirdPartyEmails"));
    Route::get("/change_innodata_setting", array('uses' => 'AdminController@changeInnodataSetting', 'as' => "changeInnodataSetting"));
    Route::get('/admin/redact/{entity_id}', array('uses' => 'AdminController@redactEntity', 'as' => 'redactEntity'));
    Route::get('/admin/publish/{entity_id}', array('uses' => 'AdminController@publishEntity', 'as' => 'publishEntity'));
    Route::get('/admin/industry_comparison_treammean', array('uses' => 'AdminController@getIndustryComparisonTreammean', 'as' => 'getIndustryComparisonTreammean'));

    /** Developer API Users */
    Route::get('/developers/list', array('uses' => 'AdminController@getDevUserList', 'as' => 'getDevUserList'));
    Route::get('/register/developers', array('uses' => 'AdminController@getDevRegForm', 'as' => 'getDevRegForm'));
    Route::post('/register/developers', array('uses' => 'AdminController@postDevRegForm', 'as' => 'postDevRegForm'));
    Route::get('/admin/change_dev_sts/{sts}/{developer_id}', array('uses' => 'AdminController@changeDevSts', 'as' => 'changeDevSts'));

    Route::get('/admin/trials', array('uses' => 'AdminController@getTrialAccounts', 'as' => 'getTrialAccounts'));
    Route::get('/admin/create_trial_account', array('uses' => 'AdminController@getCreateTrialAccounts', 'as' => 'getCreateTrialAccounts'));
    Route::post('/admin/create_trial_account', array('uses' => 'AdminController@postCreateTrialAccounts', 'as' => 'postCreateTrialAccounts'));
    Route::post('/admin/send_trial_login/{entityid}', array('uses' => 'AdminController@postSendTrialLogin', 'as' => 'postSendTrialLogin'));

    /*Organizations*/
    Route::get('/admin/organization-type/{type}', array('uses' => 'SignupController@getOrganizationsByType', 'as' => 'getOrganizationsByType'));
    Route::get('/admin/questionnaire-type/{bankid}', array('uses' => 'SignupController@getOrganizationsQuestionType', 'as' => 'getOrganizationsQuestionType'));
    Route::get('/admin/organizations', array('uses' => 'AdminController@getOrganizations', 'as' => 'getOrganizations'));
    Route::get('/admin/create_organization', array('uses' => 'AdminController@getCreateOrganizations', 'as' => 'getCreateOrganizations'));
    Route::post('/admin/create_organization', array('uses' => 'AdminController@postOrganizations', 'as' => 'postOrganizations'));
    Route::get('/admin/update_organization/{id}', array('uses' => 'AdminController@getUpdateOrganizations', 'as' => 'getUpdateOrganizations'));
    Route::put('/admin/update_organization/{id}', array('uses' => 'AdminController@putOrganizations', 'as' => 'putOrganizations'));

    Route::get('/admin/scraper', array('uses' => 'AdminController@getAdminScrapper', 'as' => 'getAdminScrapper'));
    Route::get('/admin/run_scrapper', array('uses' => 'AdminController@getRunAdminScrapper', 'as' => 'getRunAdminScrapper'));

    Route::get('/admin/upload_psa_forecasting', array('uses' => 'AdminController@getPSAForecasting', 'as' => 'getPSAForecasting'));
    Route::post('/admin/upload_psa_forecasting', array('uses' => 'AdminController@postPSAForecasting', 'as' => 'postPSAForecasting'));
    Route::get('/admin/download_pse_forecasting_template', array('uses' => 'AdminController@downloadPSETemplate', 'as' => 'downloadPSETemplate'));

    Route::post('/admin/set_trial_receiver', array('uses' => 'AdminController@setTrialSummaryEmail', 'as' => 'setTrialSummaryEmail'));

    Route::get('/admin/valid-user-statistics', array('uses' => 'ValidUserStatisticsController@main', 'as', 'validUserStatistics'));
    Route::get('get-statistics',
        [
            'uses' => 'ValidUserStatisticsController@getStatistics',
            'as' => 'adminValidUserStatistics'
        ]
    );
    Route::get('/admin/notifications', array('uses' => 'AdminController@getCustomNotification', 'as', 'getCustomNotification'));
    Route::post("/update_custom", array('uses' => 'AdminController@updateCustomNotification', 'as' => "updateCustomNotification"));
    Route::get("/edit_custom/{custom_id}", array('uses' => 'AdminController@editCustomNotifications', 'as' => "editCustomNotifications"));
    Route::post("/update_custom_notification", array('uses' => 'AdminController@saveCustomNotifications', 'as' => "saveCustomNotifications"));
    Route::post("/add_receriver", array('uses' => 'AdminController@addNotificationEmails', 'as' => "addNotificationEmails"));
    Route::get("/delete_email_by_id/{id}", array('uses' => 'AdminController@deleteNotficationEmails', 'as' => "deleteNotficationEmails"));
    Route::get('/admin/innodata', array('uses' => 'AdminController@getInnodataRecipients', 'as' => 'getInnodataRecipients'));
    Route::get('/admin/innodata/add', array('uses' => 'AdminController@addInnodataRecipient', 'as' => 'addInnodataRecipient'));
    Route::post('/admin/innodata/add', array('uses' => 'AdminController@saveInnodataRecipient', 'as' => 'saveInnodataRecipient'));
    Route::get('/admin/innodata/delete/{id}', array('uses' => 'AdminController@deleteInnodataRecipient', 'as' => 'deleteInnodataRecipient'));

    Route::post('/admin/transcribe/update', array('uses' => 'AdminController@postUpdateTranscribeKey', 'as' => 'postUpdateTranscribeKey'));

    Route::get('/admin/transcribe_files', array('uses' => 'AdminController@getCreditBpoFTP', 'as' => 'getCreditBpoFTP'));
    Route::post('/admin/transcription/notification/status', array('uses' => 'AdminController@transcriptionNotificationstatus', 'as' => 'transcriptionNotificationstatus'));
    Route::get('/admin/upload-fs/{id}', array('uses' => 'AdminController@getUploadFTP', 'as' => 'getUploadFTP'));
    Route::post('/admin/upload/creditbpofs/{id}', array('uses' => 'AdminController@postCreditBpoFTP', 'as' => 'postCreditBpoFTP'));
    Route::get('/admin/fsRawFile', array('uses' => 'AdminController@getFsRawFile', 'as' => 'getFsRawFile'));
    Route::get('/admin/viewTranscribeFile', array('uses' => 'AdminController@getTranscribeFile', 'as' => 'getTranscribeFile'));
    Route::get('/admin/download/transcibe_fs/{id}', array('uses' => 'AdminController@downloadTranscribeFS', 'as' => 'downloadTranscribeFS'));
    Route::post("/admin/add_external_transciber_email", array('uses' => 'AdminController@addExternalTranscriberEmail', 'as' => "addExternalTranscriberEmail"));
    Route::get("/delete_transcriber_email/{id}", array('uses' => 'AdminController@deleteTranscribeReceiverEmail', 'as' => "deleteTranscribeReceiverEmail"));

    Route::get('/admin/security', array('uses' => 'AdminController@getBlockAddress', 'as' => 'getBlockAddress'));
    Route::post('/admin/security', array('uses' => 'AdminController@postBlockAddress', 'as' => 'postBlockAddress'));
    Route::post('/admin/security/update', array('uses' => 'AdminController@updateBlockAddress', 'as' => 'updateBlockAddress'));
    Route::post('/admin/security/delete', array('uses' => 'AdminController@deleteBlockAddress', 'as' => 'deleteBlockAddress'));
    Route::get('/admin/download/bsp', array('uses' => 'AdminController@downloadBspScrapedData', 'as' => 'downloadBspScrapedData'));

    /* form Recognizer */
    Route::get('/admin/form-recognizer', ['uses' => 'App\Http\Controllers\FormRecognizerController@index', 'as' => 'form.home']);
    Route::post('/admin/form-recognizer', ['uses' => 'App\Http\Controllers\FormRecognizerController@upload']);

    Route::get('/admin/check-results', ['uses' => 'App\Http\Controllers\FormRecognizerController@result']);
    Route::get('/admin/get-result/{documentAnalysis}', ['uses' => 'App\Http\Controllers\FormRecognizerController@results', 'as' => 'result']);
    Route::get('/admin/get-report/{documentAnalysis}', ['uses' => 'App\Http\Controllers\FormRecognizerController@getReport', 'as' => 'report']);
    Route::get('/admin/get-file/{documentAnalysis}', ['uses' => 'App\Http\Controllers\FormRecognizerController@getFile', 'as' => 'file.get']);

    Route::get('/admin/manage-resource', ['uses' => 'App\Http\Controllers\FormRecognizerController@resources', 'as' => 'resource.view']);
    Route::post('/admin/manage-resource', ['uses' => 'App\Http\Controllers\FormRecognizerController@updateResources', 'as' => 'resource.update']);
    /* form Recognizer */
});

// INVESTIGATOR
Route::get('/investigator/get_group', array('uses' => 'InvestigatorController@getGroup', 'as' => 'getGroup'));
Route::get('/investigator/get_all_failed', array('uses' => 'InvestigatorController@getAllFailed', 'as' => 'getAllFailed'));

Route::group(['middleware' => 'authinvestigatorold'], function()
{
    Route::post('/investigator/post_data', array('uses' => 'InvestigatorController@postInvestigatorData', 'as' => 'postInvestigatorData'));
    Route::post('/investigator/save_investigation', array('uses' => 'InvestigatorController@postSaveInvestigation', 'as' => 'postSaveInvestigation'));
});

Route::post('/contractor/save_document', array('uses' => 'DocumentController@postSaveDocument', 'as' => 'postSaveDocument'));
Route::post('/contractor/submit_document', array('uses' => 'DocumentController@postSubmitDocument', 'as' => 'postSubmitDocument'));
Route::get('/contractor/confirmation', array('uses' => 'DocumentController@getContractorConfirmation', 'as' => 'getContractorConfirmation'));
// BANK USERS
Route::group(['middleware' => 'authbankold'], function()
{

    Route::get('/bankbillingdetails', 'BankController@getBankBillingDetails');
    Route::post('/bankbillingdetails', array('uses' => 'BankController@postAdminSupBankBillingDetails', 'as' => 'postBankBillingDetails'));

    Route::get('/admin/supervisor/bankbillingdetails', 'BankController@getAdminSupBankBillingDetails');
    Route::post('/admin/supervisor/bankbillingdetails', array('uses' => 'BankController@postAdminSupBankBillingDetails', 'as' => 'postAdminSupBankBillingDetails'));

    Route::get('/bank/dashboard', array('uses' => 'BankController@getBankDashboard', 'as' => 'getBankDashboard'));
    Route::match(['get','post'],'/bank/reportlist/{tertiary?}/{id_industry?}', array('uses' => 'BankController@getBankReportList', 'as' => 'getBankReportList'));
    Route::post('/bank/email/reportlist', array('uses' => 'BankController@sendEmailReportlist', 'as' => 'sendEmailReportlist'));
    Route::get('/bank/industry', array('uses' => 'BankController@getIndustry', 'as' => 'getBankIndustry'));
    Route::get('/bank/industry/edit/{industry_id}', array('uses' => 'BankController@getEditIndustry', 'as' => 'getEditBankIndustry'));
    Route::post('/bank/industry/edit/{industry_id}', array('uses' => 'BankController@postEditIndustry', 'as' => 'postEditBankIndustry'));
    Route::post('/bank/industry/reset', array('uses' => 'BankController@postResetWeights', 'as' => 'postResetWeights'));
    Route::post('/bank/save_industry_weight_setting', array('uses' => 'BankController@postSaveWeights', 'as' => 'postSaveWeights'));
    Route::post('/bank/revert_industry_weight_setting', array('uses' => 'BankController@postRevertToSaveWeights', 'as' => 'postRevertToSaveWeights'));
    Route::post('/bank/standalone_option', array('uses' => 'BankController@postUpdateStandaloneOption', 'as' => 'postUpdateStandaloneOption'));
    Route::get('/bank/keys', array('uses' => 'BankController@getBankKeys', 'as' => 'getBankKeys'));
    Route::post('/bank/distribute_keys', array('uses' => 'BankController@postDistributeKeys', 'as' => 'postDistributeKeys'));
    Route::get('/bank/purchase_keys', array('uses' => 'BankController@getPurchaseBankKeys', 'as' => 'getPurchaseBankKeys'));
    Route::post('/bank/get_client_key_price', array('uses' => 'BankController@getClientKeyPrice', 'as' => 'getClientKeyPrice'));
    Route::post('/bank/purchase_keys', array('uses' => 'PaymentController@postPurchaseBankKeys', 'as' => 'postPurchaseBankKeys'));
    Route::get('/bank/export_keys', array('uses' => 'BankController@getExportBankKeys', 'as' => 'getExportBankKeys'));
    Route::get('/bank/statistics', array('uses' => 'BankController@getStatistics', 'as' => 'getStatistics'));
    Route::post('/bank/statistics', array('uses' => 'BankController@postStatistics', 'as' => 'postStatistics'));
    Route::get('/bank/ci_users', array('uses' => 'BankController@getCIUsers', 'as' => 'getCIUsers'));
    Route::get('/bank/add_ci_user', array('uses' => 'BankController@getAddCIUsers', 'as' => 'getAddCIUsers'));
    Route::get('/bank/edit_ci_user/{id}', array('uses' => 'BankController@getEditCIUsers', 'as' => 'getEditCIUsers'));
    Route::get('/bank/delete_ci_user/{id}', array('uses' => 'BankController@getDeleteCIUsers', 'as' => 'getDeleteCIUsers'));
    Route::post('/bank/add_ci_user', array('uses' => 'BankController@postAddCIUsers', 'as' => 'postAddCIUsers'));
    Route::put('/bank/edit_ci_user/{id}', array('uses' => 'BankController@putCIUsers', 'as' => 'putCIUsers'));
    Route::get('/bank/sub_users', array('uses' => 'BankController@getSubUsers', 'as' => 'getSubUsers'));

    Route::get('/bank/contractors', array('uses' => 'BankController@getContractors', 'as' => 'getContractors'));
    Route::get('/bank/add_contractors', array('uses' => 'BankController@getAddContractors', 'as' => 'getAddContractors'));
    Route::post('/bank/add_contractors', array('uses' => 'BankController@postAddContractors', 'as' => 'postAddContractors'));
    Route::get('/bank/edit_contractor/{id}', array('uses' => 'BankController@getEditContractor', 'as' => 'getEditContractor'));
    Route::get('/bank/delete_contractor/{id}', array('uses' => 'BankController@getDeleteContractor', 'as' => 'getDeleteContractor'));
    Route::post('/bank/edit_contractor/{id}', array('uses' => 'BankController@postEditContractor', 'as' => 'postEditContractor'));

    Route::get('/bank/add_sub_user', array('uses' => 'BankController@getAddSubUsers', 'as' => 'getAddSubUsers'));
    Route::get('/bank/edit_sub_user/{id}', array('uses' => 'BankController@getEditSubUsers', 'as' => 'getEditSubUsers'));
    Route::get('/bank/delete_sub_user/{id}', array('uses' => 'BankController@getDeleteSubUsers', 'as' => 'getDeleteSubUsers'));
    Route::post('/bank/add_sub_user', array('uses' => 'BankController@postSubUsers', 'as' => 'postSubUsers'));
    Route::put('/bank/edit_sub_user/{id}', array('uses' => 'BankController@putSubUsers', 'as' => 'putSubUsers'));
    Route::get('/bank/formula', array('uses' => 'BankController@getFormula', 'as' => 'getFormula'));
    Route::post('/bank/formula', array('uses' => 'BankController@postFormula', 'as' => 'postFormula'));
    Route::post('/bank/formula_set', array('uses' => 'BankController@postSetFormula', 'as' => 'postSetFormula'));
    Route::get('/bank/formula_reset', array('uses' => 'BankController@getResetFormula', 'as' => 'getResetFormula'));
    Route::post('/bank/check_email', array('uses' => 'BankController@checkEmail', 'as' => 'checkEmail'));

    Route::get('/bank/subscribe', array('uses' => 'BankController@getBankSubscribe', 'as' => 'getBankSubscribe'));
    Route::get('/bank_subscribe/checkout_paypal', 'PaymentController@postBankPayViaPaypal');
    Route::get('/bank_subscribe/recurring_profile', 'BankController@getRecurringProfileViaPaypal');
    Route::post('/bank_subscribe/recurring_profile', 'PaymentController@postRecurringProfileViaPaypal');
    Route::get('/bank_subscribe/checkout_dragonpay', 'PaymentController@postBankPayViaDragonPay');
    Route::get('/bank_subscribe/paymentsuccess', 'PaymentController@getBankSubscriptionPaymentSuccess');
    Route::get('/bank/sme-reports-list/{company_name}', 'BankController@getSmeReportsList');

    Route::get('/view_map', 'MapController@getViewMap');
    Route::post('/view_map', array('uses' => 'MapController@postViewMap', 'as' => 'postViewMap'));

    Route::get('/bank/basic', 'BankController@getBasicDashboard');
    Route::get('/bank/send-me-report/{id}', 'BankController@getSendSMEReportToSupervisor');

    Route::get('/bank/document_options', 'BankController@getDocumentOptions');
    Route::post('/bank/document_options', array('uses' => 'BankController@postDocumentOptions', 'as' => 'postDocumentOptions'));
    Route::post('/bank/document_options_load', array('uses' => 'BankController@postDocumentOptionsLoad', 'as' => 'postDocumentOptionsLoad'));
    Route::post('/bank/add/custom-doc', array('uses' => 'BankController@postCustomDocument', 'as' => 'postCustomDocument'));
    Route::get('/bank/delete/custom-doc/{doc_id}', array('uses' => 'BankController@deleteCustomDocument', 'as' => 'deleteCustomDocument'));
    Route::get('/bank/get/custom-doc/{doc_id}', array('uses' => 'BankController@getCustomDocument', 'as' => 'getCustomDocument'));
    Route::post('/bank/required/custom-doc', array('uses' => 'BankController@setCustomDocumentConfig', 'as' => 'setCustomDocumentConfig'));

    Route::get('/bank/notifications', 'BankController@getNotificationOptions');
    Route::post('/bank/notifications/addemail', array('uses' => 'BankController@postNewNotificationEmailSub', 'as' => 'postNewNotificationEmailSub'));
    Route::post('/bank/notifications/savesettings', array('uses' => 'BankController@postNotificationSettings', 'as' => 'postNotificationSettings'));
    Route::get('/bank/notifications/deleteemail/{emailnotifid}', array('uses' => 'BankController@deleteNotificationEmailSub', 'as' => 'deleteNotificationEmailSub'));
    Route::post('/bank/check-notification-email', array('uses' => 'BankController@checkNotificationEmail', 'as' => 'checkNotificationEmail'));

    Route::get('/bank/questionnaire_config', array('uses' => 'BankController@getQuestionnaireConfigPage', 'as' => 'getQuestionnaireConfigPage'));
    Route::post('/bank/set_questionnaire_config', array('uses' => 'BankController@postQuestionnaireConfigPage', 'as' => 'postQuestionnaireConfigPage'));

    Route::get('/bank/nfcc', array('uses' => 'BankController@getNFCC', 'as' => 'getNFCC'));
    Route::get('/bank/nfcc_get_fs', array('uses' => 'BankController@getNFCCFSdata', 'as' => 'getNFCCFSdata'));
    Route::post('/bank/upd-approval', array('uses' => 'BankController@updateApprovalSts', 'as' => 'updateApprovalSts'));
    Route::get('/bank/check-in-process', array('uses' => 'BankController@checkInProcessSts', 'as' => 'checkInProcessSts'));
    Route::get('/bank/reports_submitted', array('uses' => 'BankController@getReportsSubmitted', 'as' => 'getReportsSubmitted'));
    Route::get('/bank/reports_industry', array('uses' => 'BankController@getReportsbyRegion', 'as' => 'getReportsbyRegion'));

    Route::get('/admin/supervisor/bank/subscription', array('uses' => 'BankController@getAdminSupervisorSubscription', 'as' => 'getAdminSupervisorSubscription'));
    Route::get('/bank/subscription', array('uses' => 'BankController@getSupervisorSubscription', 'as' => 'getSupervisorSubscription'));
    Route::post('/bank/subscription', array('uses' => 'BankController@postSupervisorSubscription', 'as' => 'postSupervisorSubscription'));

    Route::get('/bank/financial_analysispdf/{id}', array('uses' => 'BankController@downloadFinancialAnalysisPDF', 'as' => 'downloadFinancialAnalysisPDF'));
    Route::get('/bank/rating_summarypdf/{id}', array('uses' => 'BankController@downloadRatingSummaryPDF', 'as' => 'downloadRatingSummaryPDF'));
});

Route::get('/view_result/{code}', array('uses' => 'PrivateLinksController@getPrivateLinkSummary', 'as' => 'getPrivateLinkSummary'));
Route::get('/cron/get_leads', array('uses' => 'CronController@getLeadsCron', 'as' => 'getLeadsCron'));
Route::get('/cron/send_new_leads', array('uses' => 'CronController@getSendLeadsCron', 'as' => 'getSendLeadsCron'));
Route::get('/cron/send_log_reminder', array('uses' => 'CronController@smeLoggedReminder', 'as' => 'smeLoggedReminder'));
Route::get('/cron/send_trial_summary', array('uses' => 'CronController@sendTrialSummary', 'as' => 'sendTrialSummary'));
Route::get('/geocode', array('uses' => 'MapController@getGeocode', 'as' => 'getGeocode'));
Route::get('/cron/bizoutlook/update_data', array('uses' => 'AdminController@updateBizOutlookCron', 'as' => 'updateBizOutlookCron'));

Route::get('/fr/compare-report/{fsid}', array('uses' => 'FinancialReportController@faReportVsTemplate', 'as' => 'faReportVsTemplate'));

Route::get('/fr_chart/create_asset_struct/{id}', array('uses' => 'FinancialReportController@makeAssetsStructGraph', 'as' => 'makeAssetsStructGraph'));
Route::get('/fr_chart/create_dyn_asset/{id}', array('uses' => 'FinancialReportController@makeDynAssetGraph', 'as' => 'makeDynAssetGraph'));
Route::get('/fr_chart/create_cap_struct/{id}', array('uses' => 'FinancialReportController@makeCapitalStructGraph', 'as' => 'makeCapitalStructGraph'));
Route::get('/fr_chart/create_fin_stability/{id}', array('uses' => 'FinancialReportController@makeFinStabilityGraph', 'as' => 'makeFinStabilityGraph'));
Route::get('/fr_chart/create_working_capital/{id}', array('uses' => 'FinancialReportController@makeWorkCapitalGraph', 'as' => 'makeWorkCapitalGraph'));
Route::get('/fr_chart/create_liquidity_ratio/{id}', array('uses' => 'FinancialReportController@makeLiquidRatioGraph', 'as' => 'makeLiquidRatioGraph'));
Route::get('/fr_chart/create_dyn_revenue/{id}', array('uses' => 'FinancialReportController@makeDynRevProfitGraph', 'as' => 'makeDynRevProfitGraph'));
Route::get('/fr_chart/create_dyn_profit/{id}', array('uses' => 'FinancialReportController@makeDynProfitRatio', 'as' => 'makeDynProfitRatio'));
Route::get('/fr_chart/create_asset_equity/{id}', array('uses' => 'FinancialReportController@makeDynAssetEquityGraph', 'as' => 'makeDynAssetEquityGraph'));

Route::get('/highcharts/create_bcc/{entity_id}', array('uses' => 'ActionController@createBccHighChart', 'as' => 'createBccHighChart'));
Route::get('/highcharts/create_mq/{entity_id}', array('uses' => 'ActionController@createMqHighChart', 'as' => 'createMqHighChart'));
Route::get('/highcharts/create_fc/{entity_id}', array('uses' => 'ActionController@createFcHighChart', 'as' => 'createFcHighChart'));

Route::get('/highcharts/create_grg/{entity_id}/{boost}', array('uses' => 'ActionController@createGrgHighChart', 'as' => 'createGrgHighChart'));
Route::get('/highcharts/create_nig/{entity_id}/{boost}', array('uses' => 'ActionController@createNigHighChart', 'as' => 'createNigHighChart'));
Route::get('/highcharts/create_gpm/{entity_id}/{boost}', array('uses' => 'ActionController@createGpmHighChart', 'as' => 'createGpmHighChart'));
Route::get('/highcharts/create_cr/{entity_id}/{boost}', array('uses' => 'ActionController@createCrHighChart', 'as' => 'createCrHighChart'));
Route::get('/highcharts/create_der/{entity_id}/{boost}', array('uses' => 'ActionController@createDerHighChart', 'as' => 'createDerHighChart'));

Route::get('/feedback/final_rating/{trans_id}', array('uses' => 'UserFeedbackController@showFinalUserFeedback', 'as' => 'showFinalUserFeedback'));
Route::post('/feedback/final_rating/{trans_id}', array('uses' => 'UserFeedbackController@putSMEOverallFeedback', 'as' => 'putSMEOverallFeedback'));

Route::get('/tools/auto_ocr/{entity_id}', array('uses' => 'OCRController@getAutoOCR', 'as' => 'getAutoOCR'));
Route::get('/tools/ocr_check_utility_bill/{id}', array('uses' => 'OCRController@getCheckUtilityBill', 'as' => 'getCheckUtilityBill'));
Route::get('/tools/ocr_check_bank_statement/{id}', array('uses' => 'OCRController@getCheckBankStement', 'as' => 'getCheckBankStement'));

Route::get('/cbpo_pub_rest/get_rating_result', array('uses' => 'PublicAPIController@getCreditRatingByCode', 'as' => 'getCreditRatingByCode'));
Route::get('/cbpo_pub_rest/get_company_rating', array('uses' => 'PublicAPIController@getCreditRatingByName', 'as' => 'getCreditRatingByName'));
Route::get('/cbpo_pub_rest/get_account_rating', array('uses' => 'PublicAPIController@getCreditRatingByLoginID', 'as' => 'getCreditRatingByLoginID'));
Route::get('/cbpo_pub_rest/test_login_api', array('uses' => 'PublicAPIController@loginAPITest', 'as' => 'loginAPITest'));
Route::get('/cbpo_pub_rest/test_rating_api', array('uses' => 'PublicAPIController@RatingAPITest', 'as' => 'RatingAPITest'));
Route::get('/cbpo_pub_rest/test_name_api', array('uses' => 'PublicAPIController@CompanyAPITest', 'as' => 'CompanyAPITest'));

Route::get('/bsp/bank-extraction', array('uses' => 'CronController@extractBankHtml', 'as' => 'extractBankHtml'));
Route::get('/bsp/bank-extraction-point', array('uses' => 'CronController@loadExtractionPoint', 'as' => 'loadExtractionPoint'));
Route::post('/bsp/update-bank-data', array('uses' => 'CronController@updateBankData', 'as' => 'updateBankData'));
Route::get('/bsp/test-bank-list', array('uses' => 'CronController@testDisplayBankList', 'as' => 'testDisplayBankList'));

Route::get('/test_pass_post', array('uses' => 'PublicAPIController@testPassToPost', 'as' => 'testPassToPost'));
Route::get('/test_login_post', array('uses' => 'PublicAPIController@testPassTologin', 'as' => 'testPassTologin'));
Route::get('/test_upload_post', array('uses' => 'PublicAPIController@testUploadToPost', 'as' => 'testUploadToPost'));
Route::get('/test_pass_get', array('uses' => 'PublicAPIController@testPassToGet', 'as' => 'testPassToGet'));
Route::get('/test_pass_delete', array('uses' => 'PublicAPIController@testPassToDelete', 'as' => 'testPassToDelete'));

Route::get('/api-test-post', array('uses' => 'DevApiController@testPostServiceMapper', 'as' => 'testPostServiceMapper'));
Route::get('/api-test-get', array('uses' => 'DevApiController@testGetServiceMapper', 'as' => 'testGetServiceMapper'));
Route::get('/api-test-form', array('uses' => 'DevApiController@showAPISuite', 'as' => 'showAPISuite'));

Route::post('/cbpo_rest_api/{action}', array('uses' => 'DevApiController@initUserAccntApiAccess', 'as' => 'initUserAccntApiAccess'));
Route::post('/cbpo_rest_api/misc/{action}', array('uses' => 'DevApiController@initMiscDataApiAccess', 'as' => 'initMiscDataApiAccess'));
Route::get('/cbpo_rest_api/get_data/{section}/{section_id}', array('uses' => 'DevApiController@initGetApiAccess', 'as' => 'initGetApiAccess'));
Route::get('/cbpo_rest_api/delete_data/{section}/{section_id}', array('uses' => 'DevApiController@initDeleteApiAccess', 'as' => 'initDeleteApiAccess'));
Route::post('/cbpo_rest_api/sme_data/{entity_id}', array('uses' => 'DevApiController@initPostApiAccess', 'as' => 'initPostApiAccess'));
Route::get('/selenium_test', array('uses' => 'SeleniumTestController@setSeleniumWebdrvr', 'as' => 'setSeleniumWebdrvr'));
Route::post('/selenium_test', array('uses' => 'SeleniumTestController@execSeleniumWebdrvr', 'as' => 'execSeleniumWebdrvr'));

Route::post('/ajax/save_jivo_leads', array('uses' => 'JivoController@addJivoLeads', 'as' => 'addJivoLeads'));
Route::post('/jivo/offline-message', array('uses' => 'JivoController@postJivoOfflineMessage', 'as' => 'postJivoOfflineMessage'));


// Used for testing
/* Route::get('/test_pdf', array('uses' => 'PrintSummaryController@getPDFTest', 'as' => 'getPDFTest'));
Route::get('/ocr', array('uses' => 'OCRController@getTest', 'as' => 'getTest'));
Route::post('/ocr', array('uses' => 'OCRController@postTest', 'as' => 'postTest'));
Route::post('/ocr_parsed', array('uses' => 'OCRController@postTestParse', 'as' => 'postTestParse'));
Route::get('/dropbox', array('uses' => 'OCRController@getTestDropbox', 'as' => 'getTestDropbox'));
Route::get('/test_static_dscr/{entity_id}', array('uses' => 'ProfileSMEController@calculateStaticDscr', 'as' => 'calculateStaticDscr'));
*/

Route::get('/address', array('uses' => 'OCRController@getTestAddressBreakdown', 'as' => 'getTestAddressBreakdown'));
Route::post('/address', array('uses' => 'OCRController@postTestAddressBreakdown', 'as' => 'postTestAddressBreakdown'));

Route::get('/ocr_sdk', array('uses' => 'OCRController@getOCRSDK', 'as' => 'getOCRSDK'));
Route::post('/ocr_sdk', array('uses' => 'OCRController@postOCRSDK', 'as' => 'postOCRSDK'));
Route::get('/ocr_sdk2', array('uses' => 'OCRController@getOCRSDK2', 'as' => 'getOCRSDK2'));
Route::post('/ocr_sdk2', array('uses' => 'OCRController@postOCRSDK2', 'as' => 'postOCRSDK2'));

Route::get('/test_imagick', array('uses' => 'OCRController@getTestImagick', 'as' => 'getTestImagick'));
Route::post('/test_imagick', array('uses' => 'OCRController@postTestImagick', 'as' => 'postTestImagick'));
Route::get('/test_rscript', array('uses' => 'RScriptController@testRscript', 'as' => 'testRscript'));
Route::get('/migrate_industries', array('uses' => 'UpdateIndustryController@addIndustries', 'as' => 'addIndustries'));
Route::get('/get/company_profile/{entity_id}', array('uses' => 'ProfileSMEController@getAnonymousProfile', 'as' => 'getAnonymousProfile'));
Route::get('/set_anonymous/{status}/{entity_id}', array('uses' => 'ProfileSMEController@setReportAnonymous', 'as' => 'setReportAnonymous'));
Route::get('/view_invoice', array('uses' => 'ProfileSMEController@viewReceipt', 'as' => 'viewReceipt'));
Route::post('/admin/filter_trial_accounts/{field}/{order}', array('uses' => 'AdminController@filterTrialAccounts', 'as' => 'filterTrialAccounts'));
Route::get('/search_companies', array('uses' => 'AdminController@searchCompanies', 'as' => 'searchCompanies'));
Route::get('/company_search', array('uses' => 'SignupController@companySearch', 'as' => 'companySearch'));

Route::get('/cron/update_ir', array('uses' => 'CronController@updateInterestRate', 'as' => 'updateInterestRate'));

Route::post('/pipedrive/add-person', array('uses' => 'PipeDriveController@addPerson','as' => 'addPerson'));

Route::post('/api/save_entity_dscr',
    [
        'uses' => 'CreditBPO\Controllers\DebtServiceCapacityController@saveEntityDSCR',
    ]
);

Route::get('/cmap_trial_account', array('uses' => 'CmapController@getCmapCreateTrialAccounts', 'as' => 'getCmapCreateTrialAccounts'));
Route::post('/cmap_trial_account', array('uses' => 'CmapController@postCmapCreateTrialAccounts', 'as' => 'postCmapCreateTrialAccounts'));
Route::get('/cmap_trial_account_confirmation', array('uses' => 'CmapController@getCmapFreeTrialConfirmation', 'as' => 'getCmapFreeTrialConfirmation'));

Route::get('/create_free_trial', array('uses' => 'FreeTrialController@getCreateFreeTrialAccount', 'as' => 'getCreateFreeTrialAccount'));
Route::post('/create_free_trial', array('uses' => 'FreeTrialController@postCreateFreeTrialAccount', 'as' => 'postCreateFreeTrialAccount'));
Route::get('/organization-type/{name}', array('uses' => 'FreeTrialController@getOrganizationByName', 'as' => 'getOrganizationByName'));
Route::get('/create_free_trial_confirmation', array('uses' => 'FreeTrialController@getFreeTrialConfirmation', 'as' => 'getFreeTrialConfirmation'));


Route::get('/files/download/fs-template', ['uses' => 'DocumentController@downloadFSTemplate', 'as' => 'downloadFSTemplate']);
Route::get('/checkExistingLocation', array('uses' => 'SignupController@checkExistingLocation', 'as' => 'checkExistingLocation'));

//Generate random password for trial accounts
Route::get('/generateRandomPassword', array('uses' => 'AdminController@generateRandomPassword', 'as' => '/generateRandomPassword'));
//Generate pdf
Route::get('/generatePcabPDF', array('uses' => 'SignupController@generatePcabPDF', 'as' => 'generatePcabPDF'));
// shuftipro
Route::get('getProvinceLists', array('uses' => 'SignupController@getProvinceList2', 'as' => 'getProvinceLists'));
Route::get('getCitiesLists/{provCode}', array('uses' => 'SignupController@getCityList2', 'as' => 'getCitiesLists'));
Route::get('/user-form', array('uses' => 'ShuftiController@getUserForm', 'as' => 'getUserForm'));
Route::post('/user-form', array('uses' => 'ShuftiController@postUserForm', 'as' => 'postUserForm'));
Route::get('/user-request/{id?}', array('uses' => 'ShuftiController@sendShuftiRequest', 'as' => 'sendShuftiRequest'));
Route::any('/user-response', array('uses' => 'ShuftiController@getShuftiResponse', 'as' => 'getShuftiResponse'));

//report matching
Route::get('/report_matching', array('uses' => 'ReportMatchingController@getLandingPage', 'as' => 'getLandingPage'));
Route::get('/report_matching/sme_signup/{entityid}', array('uses' => 'ReportMatchingController@getSMESignup', 'as' => 'getSMESignup'));
Route::post('/report_matching/post_sme_signup', array('uses' => 'ReportMatchingController@postReportMatchingSME', 'as' => 'postReportMatchingSME'));
Route::get('/report_matching/bank_signup/{loginid}', array('uses' => 'ReportMatchingController@getBankSignup', 'as' => 'getBankSignup'));
Route::post('/report_matching/post_bank_signup', array('uses' => 'ReportMatchingController@postReportMatchingBank', 'as' => 'postReportMatchingBank'));
Route::match(['get','post'],'/report_matching/sme_reports', array('uses' => 'ReportMatchingController@getSMEReports', 'as' => 'getSMEReports'));
Route::get('/report_matching/setRMNotify', array('uses' => 'ReportMatchingController@setRMNotify', 'as' => 'setRMNotify'));
Route::get('/report_matching/optin/{optin}/{entityid}', array('uses' => 'ReportMatchingController@setOptin', 'as' => 'setOptin'));
Route::get('/report_matching/sme_signup', array('uses' => 'ReportMatchingController@getSMESignup', 'as' => 'getSMESignup'));
Route::post('/industry/get_industry_division', array('uses' => 'ActionController@getIndustryDivision', 'as' => 'getIndustryDivision'));
Route::post('/industry/get_industry_group', array('uses' => 'ActionController@getIndustryGroup', 'as' => 'getIndustryGroup'));
