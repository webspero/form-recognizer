<?php

/**
 * All routes to admin page
 */

// Valid User Statistics
Route::group([
    'prefix' => 'valid-user-statistics',
], function() {
    Route::get(
        'get-statistics',
        [
            'uses' => 'ValidUserStatisticsController@getStatistics',
            'as' => 'adminValidUserStatistics'
        ]
    );
    Route::get('/', ['uses' => 'ValidUserStatisticsController@main']);
});
