<?php

// Sample API Routes

Route::get('/payments/success-paypal/{reference}', ['as' => 'paypal.success', 'uses' => 'PaymentAPIController@processPayPalTransaction']);
Route::get('/payments/dragonpay-response', ['as' => 'dragonpay.get', 'uses' => 'PaymentAPIController@processDragonpayResponse']);


Route::group([
    'prefix' => 'sample-api',
], function() {
    Route::get('/', ['uses' => 'SampleApiController@get']);
});

Route::group([
    'prefix' => 'users',
], function() {
    Route::get('/', ['uses' => 'UsersController@get']);
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the api routes for an application.
| Development Started: November 11, 2018
| RESTFUL API BEST PRACTICES: https://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/
|
*/

/**
 * Register here all the routes that needs
 * client id and api secret key validation
 * and should be available to guest users
 */

Route::group([
    'middleware'=> ['apiclient']
], function(){
    Route::group([
    ], function(){
        /* Routes before logging in */
        Route::post('/signup', ['as' => 'signup.post', 'uses' => 'SignupAPIController@postSignup']);
        Route::post('/login', ['as' => 'login.post', 'uses' => 'LoginAPIController@postLogin']);
        Route::get('/newaccesstoken', ['as' => 'newaccesstoken.get', 'uses' => 'UserTokenAPIController@getNewAccessToken']);
        Route::post('/activation/{activationcode}', ['as' => 'accountactivation.post', 'uses' => 'LoginAPIController@postActivateAccount']);
        Route::get('/privacypolicy', ['as' => 'privacypolicy.get', 'uses' => 'SignupAPIController@getPrivacyPolicy']);
        Route::get('/termsofuse', ['as' => 'termsofuse.get', 'uses' => 'SignupAPIController@getTermsOfUse']);
        Route::post('/request/resetpassword', ['as' => 'requestresetpassword.post', 'uses' => 'ResetPasswordAPIController@postRequestResetPassword']);
        Route::post('/resetpassword', ['as' => 'resetpassword.post', 'uses' => 'ResetPasswordAPIController@postResetPassword']);
        
        /* Reference List */
        Route::get('/organizations', ['as' => 'organizations.get', 'uses' => 'SignupAPIController@getStandaloneOrganizationList']);
        // Route::get('/provinces', ['as' => 'provinces.get', 'uses' => 'BusinessDetailsAPIController@getProvinces']);
        // Route::get('/cities', ['as' => 'cities.get', 'use   s' => 'BusinessDetailsAPIController@getCitiesByProvince']);
        Route::get('/currencies', ['as' => 'currencies.get', 'uses' => 'FinancialStatementAPIController@getCurrencyList']);
        Route::get('/businesstypes', ['as' => 'businesstypes.get', 'uses' => 'BusinessDetailsAPIController@getBusinessTypes']);
        Route::get('/reporttypes', ['as' => 'reporttypes.get', 'uses' => 'BusinessDetailsAPIController@getReportTypes']);
        Route::get('/employeesize', ['as' => 'employeesize.get', 'uses' => 'BusinessDetailsAPIController@getEmployeeSize']);
        Route::get('/units', ['as' => 'units.get', 'uses' => 'BusinessDetailsAPIController@getUnits']);
        Route::get('/psecompanies', ['as' => 'units.get', 'uses' => 'BusinessDetailsAPIController@getPseCompanies']);
        Route::get('/location', ['as' => 'location.get', 'uses' => 'BusinessDetailsAPIController@getCityProvinceLocation']);
        Route::get('/industry', ['as' => 'industry.get', 'uses' => 'BusinessDetailsAPIController@getIndustrySection']);
    });
});

/**
 * Register here all the routes that needs
 * client id, api secret key and access token validation
 * and should be available only to authorized user
 */



Route::group([
    'middleware'=> ['apiclient']
], function(){
    Route::put('/billingdetails', ['as' => 'billingdetails.put', 'uses' => 'LoginAPIController@updateBillingDetails']);
    Route::post('/logout', ['as' => 'logout.post', 'uses' => 'LogoutAPIController@postLogout']);
});


Route::group([
    'prefix' => 'payments',
    'middleware'=> ['apiclient']
], function(){
    
    $controller = 'PaymentAPIController';
    Route::get('/invoice/screen/{id?}', ['as' => 'payment.invoice.get', 'uses' => $controller.'@paymentInvoice']);
    Route::get('/amount/{id?}', ['as' => 'amount.get', 'uses' => $controller.'@getAmount']);
    Route::get('/dragonpay/{id}', ['as' => 'dragonpay.get', 'uses' => $controller.'@getDragonpayCheckout']);
    Route::get('/paypal/{id}', ['as' => 'paypal.get', 'uses' => $controller.'@getPayPalCheckout']);
    // Route::get('/dragonpay-response', ['uses' => $controller.'@processDragonpayResponse']);
    // Route::get('/success-paypal/{reference}', ['as' => 'paypal.success', 'uses' => $controller.'@processPayPalTransaction']);
    Route::get('/cancelled-paypal/{reference}', ['as' => 'paypal.cancel', 'uses' => $controller.'@cancelPayPalTransaction']);

    // Added missing routes
    Route::get('/eula', ['as' => 'eula.get', 'uses' => $controller.'@getEULA']);
    Route::get('/eula/{id}', ['as' => 'eula.get', 'uses' => $controller.'@getEULA']);
    
});

/* Route group prepared for all reports/questionnaire endpoints */
Route::group([
    'prefix' => 'reports',
    'middleware'=> ['apiclient']
], function(){
    /* Controllers variable */
    $reportController = 'ReportsAPIController';
    $majorCustomerController = 'MajorCustomerAPIController';
    $majorSupplierController = 'MajorSupplierAPIController';
    $businessDetailsController = 'BusinessDetailsAPIController';
    $affiliatesSubsidiariesController = 'AffiliatesSubsidiariesAPIController';
    $associatedFilesController = 'AssociatedFilesAPIController';
    $capitalDetailsController = 'CapitalDetailsAPIController';
    $certificationController = 'CertificationAPIController';
    $competitorsAPIController = 'CompetitorsAPIController';
    $keyManagersAPIController = 'KeyManagersAPIController';
    $customerSegmentAPIController = 'CustomerSegmentAPIController';
    $insuranceController = 'InsuranceAPIController';
    $shareHoldersController = 'ShareHoldersAPIController';
    $directorsController = 'DirectorsAPIController';
    $mainLocationAPIController = 'MainLocationAPIController';
    $majorMarketLocationAPIController = 'MajorMarketLocationAPIController';
    $documentController = 'DocumentAPIController';
    $financialStatementController = 'FinancialStatementAPIController';
    $cashFlowController = 'CashFlowProxyAPIController';
    $requiredCreditLineController = 'RequiredCreditLineController';
    $questionPassLink = 'QuestionPassController';
    $cboPdfController = 'FinalReportController';
    $financialResultController = 'FinancialResultAPIController';
    $rawFileController = 'FinancialRawFileController';

    Route::get('', ['as' => 'reports.get', 'uses' => $reportController.'@getReports']);

    Route::get('/{reportid}', ['as' => 'report.get', 'uses' => $reportController.'@getReports']);
    Route::post('', ['as' => 'reports.post', 'uses' => $reportController.'@createReport']);
    Route::post('/{reportId}/{userId}/upload_loa', ['as' => 'reports.upload_loa', 'uses' => $reportController.'@uploadLOA']);

    /* BUSINESS DETAILS I */

    /** Question Pass */
    Route::get('/{reportid}/questionpass', ['as' => 'questionpass.post', 'uses' => $questionPassLink.'@getQuestionPassLink']);

    /* Business Details */
    // Route::get('/industries/main', ['as' => 'industriesmain.get', 'uses' => $businessDetailsController.'@getMainIndustries']);
    // Route::get('/industries/main/{mainindustryid}/sub', ['as' => 'industriessub.get', 'uses' => $businessDetailsController.'@getSubIndustries']);
    // Route::get('/industries/main/{mainindustryid}/sub/{subindustryid}/row', ['as' => 'industriesrow.get', 'uses' => $businessDetailsController.'@getRowIndustries']);
    Route::get('/{reportid}/business_details', ['as' => 'businessdetails.get', 'uses' => $businessDetailsController.'@getBusinessDetails']);
    Route::put('/{reportid}/business_details', ['as' => 'businessdetails.put', 'uses' => $businessDetailsController.'@updateBusinessDetails']);
    
    /* Common document routes */
    Route::get('/download/{path}/{filename}', ['as' => 'document.download', 'uses' => $documentController.'@downloadDocument']);
    Route::get('/{reportid}/download/{path}/{filename}', ['as' => 'reportdocument.download', 'uses' => $documentController.'@downloadReportDocument'])->where('path', '(.*(?:%2F:)?.*)');
    
    /* Financial Statement */
    Route::get('/fs-template/download/{id}', ['as' => 'fstemplate.download', 'uses' => $financialStatementController.'@downloadFSTemplate']);
    Route::post('/{reportid}/financialstatements', ['as' => 'financialstatement.post', 'uses' => $financialStatementController.'@uploadFinancialStatement']);
    Route::get('/{reportid}/financialstatements', ['as' => 'financialstatement.get', 'uses' => $financialStatementController.'@getReportFinancialStatement']);
    Route::delete('/{reportid}/financialstatements', ['as' => 'financialstatement.delete', 'uses' => $financialStatementController.'@deleteReportFSDocument']);
    Route::put('/{reportid}/financialstatements', ['as' => 'financialstatement.put', 'uses' => $financialStatementController.'@updateFinancialStatement']);

    /* Bank Statement */
    Route::post('/{reportid}/bankstatements', ['as' => 'bankstatements.post', 'uses' => $cashFlowController.'@uploadBankStatements']);
    Route::get('/{reportid}/bankstatements', ['as' => 'bankstatements.get', 'uses' => $cashFlowController.'@getReportBankStatements']);
    Route::delete('/bankstatements/{id}', ['as' => 'bankstatements.delete', 'uses' => $documentController.'@deleteDocument']);

    /* Utility Bill */
    Route::post('/{reportid}/utilitybills', ['as' => 'utilitybills.post', 'uses' => $cashFlowController.'@uploadUtilityBills']);
    Route::get('/{reportid}/utilitybills', ['as' => 'utilitybills.get', 'uses' => $cashFlowController.'@getReportUtilityBills']);
    Route::delete('/utilitybills/{id}', ['as' => 'utilitybills.delete', 'uses' => $documentController.'@deleteDocument']);

    /** Financial Raw File */
    Route::post('/{reportid}/financialrawfile', ['as' => 'financialrawfile.post', 'uses' => $rawFileController.'@uploadFinancialRawFile']);
    Route::get('/{reportid}/financialrawfile', ['as' => 'financialrawfile.get', 'uses' => $rawFileController.'@getFinancialRawFile']);
    Route::delete('/financialrawfile/{reportid}/{fileid}', ['as' => 'financialrawfile.delete', 'uses' => $rawFileController.'@deleteRawFile']);

    /* BUSINESS DETAILS II */

    /* Affiliate Subsidiaries */
    Route::get('/{reportid}/affiliate_subsidiaries', ['as' => 'affilatesubsidiaries.get', 'uses' => $affiliatesSubsidiariesController.'@getAffiliateSubsidiariesList']);
    Route::get('/affiliate_subsidiary/{relatedCompaniesId}', ['as' => 'affiliatesubsidiaries.get.id', 'uses' => $affiliatesSubsidiariesController.'@getAffiliateSubsidiariesDetails']);
    Route::post('/{reportid}/affiliate_subsidiaries', ['as' => 'affiliatesubsidiaries.post', 'uses' => $affiliatesSubsidiariesController.'@postAffiliatesSubsidiaries']);
    Route::post('/affiliate_subsidiary/{relatedCompaniesId}', ['as' => 'affiliatesubsidiaries.post', 'uses' => $affiliatesSubsidiariesController.'@postUpdateAffiliatesSubsidiaries']);
    Route::delete('/affiliate_subsidiary/{relatedCompaniesId}', ['as' => 'affiliatesubsidiaries.delete', 'uses' => $affiliatesSubsidiariesController.'@deleteAffiliatesSubsidiaries']);

    /* Associated Files */
    Route::get('/{reportid}/associated_files', ['as' => 'associated_files.get', 'uses' => $associatedFilesController.'@getAssociatedFilesList']);
    Route::get('/associated_file/{id}', ['as' => 'associated_file.get.id', 'uses' => $associatedFilesController.'@getAssociatedFileDetails']);
    Route::post('/{reportid}/associated_files', ['as' => 'associated_files.post', 'uses' => $associatedFilesController.'@postAssociatedFiles']);
    Route::post('/associated_file/{id}', ['as' => 'associated_file.post', 'uses' => $associatedFilesController.'@postUpdateAssociatedFile']);
    Route::delete('/associated_file/{id}', ['as' => 'associated_file.delete', 'uses' => $associatedFilesController.'@deleteAssociatedFile']);

    /* Capital Details */
    Route::get('/{reportid}/capital_details', ['as' => 'capitaldetails.get', 'uses' => $capitalDetailsController.'@getCapitalDetailsList']);
    Route::get('/capital_details/{capitalid}', ['as' => 'capitaldetails.get.id', 'uses' => $capitalDetailsController.'@getCapitalDetails']);
    Route::post('/{reportid}/capital_details', ['as' => 'capitaldetails.post', 'uses' => $capitalDetailsController.'@postCapitalDetails']);
    Route::post('/capital_details/{capitalid}', ['as' => 'capitaldetails.post', 'uses' => $capitalDetailsController.'@postUpdateCapitalDetails']);
    Route::delete('/capital_details/{capitalid}', ['as' => 'capitaldetails.delete', 'uses' => $capitalDetailsController.'@deleteCapitalDetails']);

    /* Certification */
    Route::get('/{reportid}/certifications', ['as' => 'certifications.get', 'uses' => $certificationController.'@getCertificationList']);
    Route::get('/certification/{certificationid}', ['as' => 'certification.get.id', 'uses' => $certificationController.'@getCertificationDetails']);
    Route::post('/{reportid}/certifications', ['as' => 'certifications.post', 'uses' => $certificationController.'@postCertification']);
    Route::post('/certification/{certificationid}', ['as' => 'certification.post', 'uses' => $certificationController.'@postUpdateCertification']);
    Route::delete('/certification/{certificationid}', ['as' => 'certification.delete', 'uses' => $certificationController.'@deleteCertification']);

    /* Competitors */
    Route::get('/{reportid}/competitors', ['as' => 'competitors.get', 'uses' => $competitorsAPIController.'@getCompetitorsList']);
    Route::get('/competitor/{competitorid}', ['as' => 'competitor.get.id', 'uses' => $competitorsAPIController.'@getCompetitorsDetails']);
    Route::post('/{reportid}/competitors', ['as' => 'competitors.post', 'uses' => $competitorsAPIController.'@postCompetitors']);
    Route::post('/competitor/{competitorid}', ['as' => 'competitor.post', 'uses' => $competitorsAPIController.'@postUpdateCompetitors']);
    Route::delete('/competitor/{competitorid}', ['as' => 'competitor.delete', 'uses' => $competitorsAPIController.'@deletecompetitors']);

    /* Customer Segment */
    Route::get('/{reportid}/customer_segments', ['as' => 'customer_segments.get', 'uses' => $customerSegmentAPIController.'@getCustomerSegmentList']);
    Route::get('/customer_segment/{businesssegmentid}', ['as' => 'competitor.get.id', 'uses' => $customerSegmentAPIController.'@getCustomerSegmentDetails']);
    Route::post('/{reportid}/customer_segments', ['as' => 'customer_segments.post', 'uses' => $customerSegmentAPIController.'@postCustomerSegment']);
    Route::post('/customer_segment/{businesssegmentid}', ['as' => 'customer_segment.post', 'uses' => $customerSegmentAPIController.'@postUpdateCustomerSegment']);
    Route::delete('/customer_segment/{businesssegmentid}', ['as' => 'customer_segment.delete', 'uses' => $customerSegmentAPIController.'@deleteCustomerSegment']);

     /* Directors */
    Route::get('/{reportid}/directors', ['as' => 'directors.get', 'uses' => $directorsController.'@getDirectorsList']);
    Route::get('/director/{id}', ['as' => 'director.get.id', 'uses' => $directorsController.'@getDirectorsDetails']);
    Route::post('/{reportid}/directors', ['as' => 'directors.post', 'uses' => $directorsController.'@postDirectors']);
    Route::post('/director/{id}', ['as' => 'director.post', 'uses' => $directorsController.'@postUpdateDirectors']);
    Route::delete('/director/{id}', ['as' => 'director.delete', 'uses' => $directorsController.'@deleteDirectors']);

    /* Key Managers */
    Route::get('/{reportid}/key_managers', ['as' => 'key_managers.get', 'uses' => $keyManagersAPIController.'@getKeyManagersList']);
    Route::get('/key_manager/{keymanagerid}', ['as' => 'key_manager.get.id', 'uses' => $keyManagersAPIController.'@getKeyManagersDetails']);
    Route::post('/{reportid}/key_managers', ['as' => 'key_manager.post', 'uses' => $keyManagersAPIController.'@postKeyManagers']);
    Route::post('/key_manager/{keymanagerid}', ['as' => 'key_manager.post', 'uses' => $keyManagersAPIController.'@postUpdateKeyManagers']);
    Route::delete('/key_manager/{keymanagerid}', ['as' => 'key_manager.delete', 'uses' => $keyManagersAPIController.'@deleteKeyManagers']);

    /* Insurance */
    Route::get('/{reportid}/insurances', ['as' => 'insurances.get', 'uses' => $insuranceController.'@getInsuranceList']);
    Route::get('/insurance/{insuranceid}', ['as' => 'insurance.get.id', 'uses' => $insuranceController.'@getInsuranceDetails']);
    Route::post('/{reportid}/insurances', ['as' => 'insurances.post', 'uses' => $insuranceController.'@postInsurance']);
    Route::post('/insurance/{insuranceid}', ['as' => 'insurance.post', 'uses' => $insuranceController.'@postUpdateInsurance']);
    Route::delete('/insurance/{insuranceid}', ['as' => 'insurance.delete', 'uses' => $insuranceController.'@deleteInsurance']);

    /* Main Location */
    Route::get('/{reportid}/main_locations', ['as' => 'mainlocation.get', 'uses' => $mainLocationAPIController.'@getMainLocationList']);
    Route::get('/main_location/{locationid}', ['as' => 'mainlocation.get.id', 'uses' => $mainLocationAPIController.'@getMainLocationDetails']);
    Route::post('/{reportid}/main_locations', ['as' => 'mainlocation.post', 'uses' => $mainLocationAPIController.'@postMainLocation']);
    Route::post('/main_location/{locationid}', ['as' => 'mainlocation.post', 'uses' => $mainLocationAPIController.'@postUpdateMainLocation']);
    Route::delete('/main_location/{locationid}', ['as' => 'mainlocation.delete', 'uses' => $mainLocationAPIController.'@deleteMainLocation']);
    
    /* Major Customer */
    Route::get('/{reportid}/major_customers', ['as' => 'majorcustomer.get', 'uses' => $majorCustomerController.'@getReportMajorCustomerList']);
    Route::get('/major_customers/{id}', ['as' => 'majorcustomer.get.id', 'uses' => $majorCustomerController.'@getMajorCustomerDetails']);
    Route::post('/{reportid}/majorcustomers', ['as' => 'majorcustomer.post', 'uses' => $majorCustomerController.'@postMajorCustomer']);
    Route::put('/major_customers/{id}', ['as' => 'majorcustomer.put', 'uses' => $majorCustomerController.'@putMajorCustomer']);
    Route::delete('/major_customers/{id}', ['as' => 'majorcustomer.delete', 'uses' => $majorCustomerController.'@deleteMajorCustomer']);
    
    /* Major Customer Account */
    Route::put('/major_customer_accounts/{reportid}', ['as' => 'majorcustomer.put', 'uses' => $majorCustomerController.'@updateMajorCustomerAccount']);

    /* Major Supplier */
    Route::get('/{reportid}/major_suppliers', ['as' => 'majorsupplier.get', 'uses' => $majorSupplierController.'@getReportMajorSupplierList']);
    Route::get('/major_suppliers/{id}', ['as' => 'majorsupplier.get.id', 'uses' => $majorSupplierController.'@getMajorSupplierDetails']);
    Route::post('/{reportid}/major_suppliers', ['as' => 'majorsupplier.post', 'uses' => $majorSupplierController.'@postMajorSupplier']);
    Route::put('/major_suppliers/{id}', ['as' => 'majorsupplier.put', 'uses' => $majorSupplierController.'@putMajorSupplier']);
    Route::delete('/major_suppliers/{id}', ['as' => 'majorsupplier.delete', 'uses' => $majorSupplierController.'@deleteMajorSupplier']);

    /* Major Market Location */
    Route::get('/{reportid}/major_market_locations', ['as' => 'majormarketlocations.get', 'uses' => $majorMarketLocationAPIController.'@getMajorMarketLocationList']);
    Route::get('/major_market_location/{marketlocationid}', ['as' => 'majormarketlocation.get.id', 'uses' => $majorMarketLocationAPIController.'@getMajorMarketLocationDetails']);
    Route::post('/{reportid}/major_market_locations', ['as' => 'majormarketlocations.post', 'uses' => $majorMarketLocationAPIController.'@postMajorMarketLocation']);
    Route::post('/major_market_location/{marketlocationid}', ['as' => 'majormarketlocation.post', 'uses' => $majorMarketLocationAPIController.'@postUpdateMajorMarketLocation']);
    Route::delete('/major_market_location/{marketlocationid}', ['as' => 'majormarketlocation.post', 'uses' => $majorMarketLocationAPIController.'@deleteMajorMarketLocation']);

    /* Share Holders */
    Route::get('/{reportid}/share_holders', ['as' => 'share_holders.get', 'uses' => $shareHoldersController.'@getShareHoldersList']);
    Route::get('/share_holder/{id}', ['as' => 'share_holder.get.id', 'uses' => $shareHoldersController.'@getShareHoldersDetails']);
    Route::post('/{reportid}/share_holders', ['as' => 'share_holders.post', 'uses' => $shareHoldersController.'@postShareHolders']);
    Route::post('/share_holder/{id}', ['as' => 'share_holder.post', 'uses' => $shareHoldersController.'@postUpdateShareHolders']);
    Route::delete('/share_holder/{id}', ['as' => 'share_holder.delete', 'uses' => $shareHoldersController.'@deleteShareHolders']);

    /* REQUIRED CREDIT LINE */

    /*Required Credit Facilities*/
    Route::get('/{reportid}/creditlines', ['as' => 'creditlines.get', 'uses' => $requiredCreditLineController.'@getCreditLines']);
    Route::get('/{reportid}/creditlines/{creditlineid}', ['as' => 'creditline.get', 'uses' => $requiredCreditLineController.'@getCreditLineDetails']);
    Route::post('/{reportid}/creditlines', ['as' => 'creditlines.post', 'uses' => $requiredCreditLineController.'@postCreditLine']);
    Route::put('/{reportid}/creditlines/{creditlineid}', ['as' => 'creditline.put', 'uses' => $requiredCreditLineController.'@updateCreditLine']);
    Route::delete('/{reportid}/creditlines/{creditlineid}', ['as' => 'creditline.delete', 'uses' => $requiredCreditLineController.'@deleteCreditLine']);

    /* Debt Service Capacity Calculator */
    Route::get('/{reportid}/dscr', ['as' => 'dscr.get', 'uses' => 'DscrAPIController@getDSCRDetails']);
    Route::put('/{reportid}/dscr', ['as' => 'dscr.post', 'uses' => 'DscrAPIController@generateDSCR']);

    /* SUBMIT FOR CREDIT RISK RATING */
    Route::post('/{reportid}/qa_fs/{referenceId}/{userId}', ['as' => 'generatefs.post', 'uses' => $reportController.'@generateQAFS']);
    Route::post('/{reportid}/submit', ['as' => 'submitreport.post', 'uses' => $reportController.'@submitQuestionnaire']);

    /** Get Rating Report + Financial Analysis PDF */
    Route::get('/{reportid}/cbpo_pdf', ['as' => 'cbpo_pdf.get', 'uses' => $cboPdfController .'@getCbpoPdf']);
    Route::get('/{reportid}/get_industrycomparison', ['as' => 'get_industrycomparison.get', 'uses' => $cboPdfController .'@getIndustryComparison']);
    // Route::post('/financial_result/{reportid}', ['as' => 'financialresult.get', 'uses' => $financialResultController .'@getFinancialResult']);
        
});
	
	/* File Storage */
    // Route::get('/{loginid}/file_storages', ['as' => 'file_storages.get', 'uses' => 'FileStorageAPIController@getFileList']);
    // Route::get('/file_storage/download/{id}', ['as' => 'file_storage.download', 'uses' => 'FileStorageAPIController@getFile']);
    // Route::post('/{loginid}/file_storage/upload', ['as' => 'file_storage.post', 'uses' => 'FileStorageAPIController@postFile']);
    // Route::delete('/delete/{id}', ['as' => 'file_storage.delete', 'uses' => 'FileStorageAPIController@deleteFile']);

