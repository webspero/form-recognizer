const puppeteer = require('puppeteer');
const fs = require('fs');
let isFileDownloaded = false;
let browser;
let page;

(async () => {
  for(let i =1;i<=5;i++)
  {
    // await sleep(3000);
    appendInToFile('Value of i: '+i);
    if(isFileDownloaded) {
      break;
    }

    try {
      await downloadPdf();
    }
    catch (err) {
        appendInToFile('Error while downloading pdf');
        appendInToFile(err);
        await browser.close();
    }
  }
})();

async function downloadPdf(){
  // (async () => {
    // let vfaFile = '../public/documents/vfaFolder/PLDT.vfa';
    //if(!browser)
    //{
      browser = await puppeteer.launch({
          // headless: false,
          defaultViewport: null,
          ignoreDefaultArgs: ["--enable-automation"],
          args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--no-first-run',
            '--no-zygote',
            '--single-process', // <- this one doesn't works in Windows
            '--disable-gpu'
          ],
      });
      page = await browser.newPage();
    //}
    
    /** Pass vfa file */
    const querystring = require('querystring');
    const data = querystring.parse( process.argv[2] || '' );
    const x = data.x;
    let vfaFile = '/var/cbpo/public/documents/vfaFolder/' +  x + '.vfa';
    //let vfaFile = 'C:/xampp/htdocs/business.creditbpo.com/public/documents/vfaFolder/' +  x + '.vfa';
    appendInToFile('Entity: '+x);

    await page.setViewport({ width: 1400, height: 768 })
    await page.goto('https://www.readyratios.com/start/', {waitUntil: 'networkidle2'});
    await page.waitFor(10 * 1000);
    await page.click('a[title="Log in"]');
    await page.type('[name="USER_LOGIN"]', 'CreditBPOdataentry');
    await page.type('[name="USER_PASSWORD"]', 'uSp!7Bm*2Up$');
    await page.click('#wrap-auth-form input[name="Login"]');
    // await page.waitForSelector('.online a');
    await page.waitFor(10 * 1000);
    await page.click('.online a');
    await page.waitFor(10 * 1000);
    const pages = await browser.pages();
    const newPage = pages[pages.length - 1];
    await newPage._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: '/var/cbpo/'})
    await newPage.setViewport({ width: 1366, height: 768 })
    const frame = await newPage.frames().find(f => f.name() === 'fra');
    await page.waitFor(10 * 1000);
    await frame.waitForSelector('#load_nalogi');
    const button = await frame.$('#load_nalogi');
    button.click();
    await frame.waitForSelector('#upload_fname_1');
    const inputUpload = await frame.$('#upload_fname_1');
    inputUpload.uploadFile(vfaFile);

    appendInToFile('uploading...');
    const upload = await frame.$('#upload');
    await upload.click({waitUntil:'documentloaded'});
    appendInToFile('Upload Done');
    await newPage.waitFor(10 * 1000);
    appendInToFile('10 Secods Done');
    try {
      await newPage.waitFor(10 * 1000);
      // await page.waitForSelector('#element', { timeout: 1000 });
      // await newPage.waitForNavigation({
      //       waitUntil: 'load',
      //       timeout: 0
      //   });
      // do what you have to do here
    } catch (e) {
        appendInToFile(e);
    }

    /** Click content settings */
    await frame.waitForSelector('.tools-formul >tbody >tr.title-section >th >a.arrow');
    const openContent = await frame.$('.tools-formul >tbody >tr.title-section >th >a.arrow');
    openContent.click();
    appendInToFile('1');
   //  await newPage.waitForNavigation({
   //  waitUntil: 'load',
   //  timeout: 0
   // });
    await newPage.waitFor(10 * 1000);
  
    /** Uncheck Labor Productivity */
    //await frame.waitForSelector('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(2) >ul >li:nth-child(4) >span.checkbox-button');
    const laborProductivity = await frame.$('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(2) >ul >li:nth-child(4) >span.checkbox-button');
    await laborProductivity.click();
    appendInToFile('2');
  
    /** Uncheck Rating of the Financial Position and Financial Performance */
    await frame.waitForSelector('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(3) >ul >li:nth-child(2) >span.checkbox-button');
    const financialPosition = await frame.$('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(3) >ul >li:nth-child(2) >span.checkbox-button');
    await financialPosition.click();
  
    /** Uncheck Bankruptcy Test */
    await frame.waitForSelector('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(4) >ul >li >span.checkbox-button');
    const bankruptcy = await frame.$('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(4) >ul >li >span.checkbox-button');
    await bankruptcy.click();
  
    /** Uncheck Calculation of the Final Rating of the Financial Condition */
    await frame.waitForSelector('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(4) >ul >li:nth-child(2) >span.checkbox-button');
    const finalRating = await frame.$('.tools-formul >tbody >tr:nth-child(2) >td >ul.checkbox-list >li:nth-child(4) >ul >li:nth-child(2) >span.checkbox-button');
    await finalRating.click();
    
    //await frame.waitForSelector('.tools-formul .line .text');
    // await newPage.waitForNavigation({
    //     waitUntil: 'load',
    //     timeout: 0
    // });
    await newPage.waitFor(10 * 1000);
    appendInToFile('Waiting for report setting section');
    const openReport = await frame.$('.tools-formul .line .text');
    openReport.click();
    appendInToFile('Report Click!');
    await newPage.waitFor(1000);
    appendInToFile('Wait page!');
    const asPDF = await frame.$('.tools-formul .line li[val="4"]');
    asPDF.click();
    appendInToFile('Click as pdf');

    const saveButton = await frame.$('#save_button');
    saveButton.click();
    appendInToFile('Download PDF');
    isFileDownloaded = true;
    await newPage.waitFor(10 * 1000);
   

    await browser.close();
// })();
}

function appendInToFile(str)
{
    //fs.appendFile("C:/xampp/htdocs/business.creditbpo.com/node_log.txt", str+"\r\n", function() { console.log(str) });
  fs.appendFile("/var/cbpo/node_log.txt", str+"\r\n", function() { console.log(str) });
}
