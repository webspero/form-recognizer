<?php

return [
    'client' => [
        'cookies' => true,
        'timeout' => 360,
    ],

    'login' => [
        'url' => 'https://apps.ibakus.com/api/v2',
        'method' => 'GET',
        'params' => [ 
            'auth' => [
                    env("IBAKUS_USER"),
                    env("IBAKUS_PASSWORD")
                ]   
            ],
        ],
        
    'person' => [
        'url' => 'https://apps.ibakus.com/api/v2/fast-check/natural-person',
        'method' => 'POST',
        'params' =>  []
    ]
];

