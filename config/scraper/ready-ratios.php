<?php

return [
    'client' => [
        'cookies' => true,
        'timeout' => 360,
    ],
    'login' => [
        'url' => 'https://www.readyratios.com/start/?login=yes',
        'method' => 'POST',
        'params' => [
            'form_params' => [
                'backurl' => '/start/',
                'AUTH_FORM' => 'Y',
                'TYPE' => 'AUTH',
                'AUTH_FORM_SHOW' => 'Y',
                'USER_LOGIN' => 'CreditBPOdataentry',
                'USER_PASSWORD' => 'a77c0uNt!nG',
                // 'USER_LOGIN' => 'nylbrian',
                // 'USER_PASSWORD' => 'admin0353',
                'USER_REMEMBER' => 'Y',
                'Login' => null,
            ],
        ],
    ],
    'upload' => [
        'url' => 'https://www.readyratios.com/finan/forma/index.php',
        'method' => 'POST',
        'params' => [
            'multipart' => [
                [
                    'name' => 'uploadTaxFiles',
                    'contents' => 1,
                ],
            ],
        ],
    ],
    'session' => [
        'url' => 'https://www.readyratios.com/finan/forma/ajax.SaveToSession.php',
        'method' => 'POST',
        'params' => [],
    ],
    'parser' => [
        'url' => 'https://www.readyratios.com/finan/parser/parser_start.php',
        'method' => 'POST',
        'params' => [],
    ],
    'download' => [
        'url' => 'https://www.readyratios.com/finan/parser/parser.php',
        'method' => 'POST',
        'params' => [],
    ],
    'custom_fields' => [
        'bal',
        'prib',
        'prib_s',
        'ubal1',
        'ubal2',
        'ubal2_s',
    ],
];
