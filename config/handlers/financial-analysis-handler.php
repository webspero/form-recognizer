<?php

return [
    'email' => [
        'bcc' => [
            'notify.user@creditbpo.com',
        ],
    ],
];
