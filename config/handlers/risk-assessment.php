<?php

return [
    'management_scoring' => [
        [
            'name' => 'supplier_bargainer_pow',
            'solution' => 'cost_leadership_sel',
            'score' => 1,
        ],
        [
            'name' => 'buyer_bargainer_pow',
            'solution' => 'cost_leadership_sel',
            'score' => 1,
        ],
        [
            'name' => 'rivalry_intensity',
            'solution' => 'cost_leadership_sel',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_substitute',
            'solution' => 'cost_leadership_sel',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_new_entrants',
            'solution' => 'cost_leadership_sel',
            'score' => 1,
        ],
        [
            'name' => 'rivalry_intensity',
            'solution' => 'product_innovation',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_substitute',
            'solution' => 'product_innovation',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_new_entrants',
            'solution' => 'product_innovation',
            'score' => 1,
        ],
        [
            'name' => 'rivalry_intensity',
            'solution' => 'focus',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_substitute',
            'solution' => 'focus',
            'score' => 1,
        ],
        [
            'name' => 'threat_of_new_entrants',
            'solution' => 'focus',
            'score' => 1,
        ],
    ],
];
