<?php

return [
    'file' => [
        'type' => 'image/png',
        'extension' => 'png',
        'directory' => '/images/rating-report-graphs/',
    ],
    'server' => [
        'url' => 'http://export.highcharts.com/',
    ],
];