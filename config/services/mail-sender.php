<?php

if (env("APP_ENV") == "Production") {
    return [
        'receipt' => [
            'headers' => [
                'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                'Disposition-Notification-To' => 'notify.user@creditbpo.com',
            ],
            'recepient' => [
                'bcc' => [
                    'notify.user@creditbpo.com',
                    'info@creditbpo.com',
                ]
            ],
            'subject' => 'CreditBPO Payment Receipt',
            'template' => 'emails.new.payment_receipt',
        ],
        'automate_standalone_rating' => [
            'headers' => [
                'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                'Disposition-Notification-To' => 'notify.user@creditbpo.com',
            ],
            'recepients' => [
                'to' => [
                    'notify.user@creditbpo.com',
                ]
            ],
            'subject' => 'CreditBPO Automate Standalone Rating Result',
            'template' => 'cron.automate-report',
        ],
    ];
} else {
    return [
        'receipt' => [
            'headers' => [
                'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                'Disposition-Notification-To' => 'notify.user@creditbpo.com',
            ],
            'recepient' => [
                'bcc' => [
                ]
            ],
            'subject' => 'CreditBPO Payment Receipt',
            'template' => 'emails.new.payment_receipt',
        ],

        'automate_standalone_rating' => [
            'headers' => [
                'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                'Disposition-Notification-To' => 'notify.user@creditbpo.com',
            ],
            'recepients' => [
                'to' => [
                ]
            ],
            'subject' => 'CreditBPO Automate Standalone Rating Result',
            'template' => 'cron.automate-report',
        ],

        'cmap_search_queries' => [
            'headers' => [
                'X-Confirm-Reading-To' => 'notify.user@creditbpo.com',
                'Disposition-Notification-To' => 'notify.user@creditbpo.com',
            ],
            'recepients' => [
                'to' => 'notify.user@creditbpo.com'
            ],
            'subject' => 'CMAP Search Query Limit',
            'template' => 'cron.cmap_search_query_reminder',
        ],
    ];
}
