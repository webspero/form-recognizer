<?php

return [
    'markers' => [
        'start' => '3.1. Key Ratios Summary',
        'startCount' => 2,
        'stop' => '3.2.',
        'bullet' => '●',
        'section' => ':',
        'single_result' => '–',
        'indicator' => 'indicator',
    ],
    'identifiers' => [
        'good' => [
            'good',
            'positive',
            'acceptable',
            'excellent',
            'high',
            'normal',
            'outstanding',
            'success',
            'satisfactory',
            'neutral',
            'standard',
        ],
        'bad' => [
            'bad',
            'negative',
            'critical',
            'unacceptable',
            'fail',
            'abnormal',
            'unsatisfactory',
        ],
    ],
];
