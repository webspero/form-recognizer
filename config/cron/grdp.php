<?php

return [
    'base_url' => 'https://psa.gov.ph/regional-accounts/grdp/data-and-charts',
    'file' => 'grdp.xls',
    'file_type' => 'CSV',
    'regions' => [
        'AUTONOMOUS REGION IN MUSLIM MINDANAO' => 'ARMM',
        'BICOL' => 'Bicol',
        'CAGAYAN VALLEY' => 'Cagayan Valley',
        'CALABARZON' => 'CALABARZON',
        'CARAGA' => 'Caraga',
        'CENTRAL LUZON' => 'Central Luzon',
        'CENTRAL VISAYAS' => 'Cent. Visayas',
        'CORDILLERA ADMINISTRATIVE REGION' => 'CAR',
        'DAVAO REGION' => 'Davao',
        'EASTERN VISAYAS' => 'East. Visayas',
        'ILOCOS' => 'Ilocos',
        'MIMAROPA REGION' => 'MIMAROPA',
        'NATIONAL CAPITAL REGION' => 'NCR',
        'NORTHERN MINDANAO' => 'N. Mindanao',
        'SOCCSKSARGEN' => 'SOCCSKSARGEN',
        'WESTERN VISAYAS' => 'Wes. Visayas',
        'ZAMBOANGA PENINSULA' => 'Zamboanga',
    ],
    'title' => 'PHIL.csv',
    'markers' => [
        'start_reference' => [
            'at constant 2000 prices',
            'percent distribution',
            'region / year',
        ],
        'year' => 'region / year',
        'skip' => ['philippines'],
        'stop' => 'source: philippine statistics authority',
    ],
];
