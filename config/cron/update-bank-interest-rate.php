<?php

return [
    'content_types' => [
        'text/html',
    ],
    'markers' => [
        'none' => [
            '..',
            '...',
        ],
        'banks' => [
            'foreign' => 'FOREIGNBANKS',
            'local' => 'LOCALBANKS',
        ],
        'stop' => [
            'SUBSIDIARIESOFFOREIGNBANKS:',
            'UBS',
        ],
    ],
    'url' => 'http://www.bsp.gov.ph/statistics/keystat/intrates.htm',
];