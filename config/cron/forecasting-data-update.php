<?php

return [
    'base_url' => 'http://psa.gov.ph/nap-press-release/sector/',
    'sectors' => [
        'INDUSTRY_AGRICULTURE' => [
            'name' => 'Agriculture, Forestry, and Fishing',
            'page' => 'Agriculture, Hunting, Forestry and Fishing',
            'start' => 'GROSS VALUE ADDED IN AGRICULTURE, HUNTING, FORESTRY AND FISHING',
            'file' => 'agriculture.xls',
        ],
        'INDUSTRY_MINING' => [
            'name' => 'Mining and Quarrying',
            'page' => 'Mining and Quarrying',
            'start' => 'MINING AND QUARRYING',
            'file' => 'mining.xls'
        ],
        'INDUSTRY_MANUFACTURING' => [
            'name' => 'Manufacturing',
            'page' => 'Manufacturing',
            'start' => 'MANUFACTURING',
            'file' => 'manufacturing.xls',
        ],
        'INDUSTRY_ELECTRICITY' => [
            'name' => 'Electricity, Gas, Steam, and Air Conditioning Supply',
            'page' => 'Electricity,Gas and Water Supply',
            'start' => '1. ELECTRICITY',
            'stop' => '2. STEAM',
            'file' => 'electricity.xls',
        ],
        'INDUSTRY_WATER' => [
            'name' => 'Water Supply; Sewerage, Waste Management and Remediation Activities',
            'page' => 'Electricity,Gas and Water Supply',
            'start' => '3. WATER',
            'file' => 'electricity.xls',
        ],
        'INDUSTRY_CONSTRUCTION' => [
            'name' => 'Construction',
            'page' => 'Construction',
            'start' => 'GROSS VALUE IN CONSTRUCTION',
            'file' => 'construction.xls',
        ],
        'INDUSTRY_WHOLESALE' => [
            'name' => 'Wholesale and Retail Trade; Repair of Motor Vehicles and Motorcycles',
            'page' => 'Trade and Repair of Motor Vehicles, Motorcycles, Personal and Household Goods',
            'start' => 'GROSS VALUE ADDED IN TRADE AND REPAIR OF MOTOR VEHICLES, MOTORCYCLES, PERSONAL AND HOUSEHOLD GOODS',
            'file' => 'trade.xls',
        ],
        'INDUSTRY_TRANSPORTATION' => [
            'name' => 'Transport and Storage',
            'page' => 'Transportation, Storage and Communication',
            'start' => '1. TRANSPORT AND STORAGE',
            'file' => 'transportation.xls',
        ],
        'INDUSTRY_ACCOMODATION' => [
            'name' => 'Accommodation and Food Service Activities',
            'page' => 'Other Services',
            'start' => '3. Hotels and Restaurants',
            'file' => 'other.xls',
        ],
        'INDUSTRY_COMMUNICATION' => [
            'name' => 'Information and Communication',
            'page' => 'Transportation, Storage and Communication',
            'start' => '2. COMMUNICATION',
            'file' => 'transportation.xls',
        ],
        'INDUSTRY_FINANCE' => [
            'name' => 'Financial and Insurance Activities',
            'page' => 'Financial Intermediation',
            'start' => 'FINANCIAL INTERMEDIATION',
            'file' => 'financial.xls',
        ],
        'INDUSTRY_REAL_ESTATE' => [
            'name' => 'Real Estate Activities',
            'page' => 'Real Estate, Renting & Business Activity',
            'start' => 'REAL ESTATE, RENTING & BUSINESS ACTIVITIES',
            'file' => 'real.xls',
        ],
        'INDUSTRY_SCIENTIFIC' => [
            'name' => 'Professional, Scientific and Technical Activities',
            // missing
        ],
        'INDUSTRY_ADMINISTRATIVE' => [
            'name' => 'Administrative and Support Service Activities',
            // missing
        ],
        'INDUSTRY_EDUCATION' => [
            'name' => 'Education',
            'page' => 'Other Services',
            'start' => '1. Education',
            'file' => 'other.xls',
        ],
        'INDUSTRY_HEALTH' => [
            'name' => 'Human Health and Social Work Activities',
            'page' => 'Other Services',
            'start' => '2. Health and Social Work',
            'file' => 'other.xls',
        ],
        'INDUSTRY_ENTERTAINMENT' => [
            'name' => 'Arts, Entertainment, and Recreation',
            'page' => 'Other Services',
            'start' => '5.  Recreational, Cultural and Sporting Activities',
            'file' => 'other.xls',
        ],
        'INDUSTRY_OTHER_SERVICES' => [
            'name' => 'Other Service Activities',
            'page' => 'Other Services',
            'start' => '6. Other Service Activities',
            'file' => 'other.xls',
        ],
        'INDUSTRY_HOUSEHOLDS' => [
            'name' => 'Activities of households as employers; Undifferentiated goods-and services-producing activities of households for own use',
            // missing
        ],
        'INDUSTRY_EXTRA_TERRITORIAL' => [
            'name' => 'Activities of extra-territorial organizations and bodies',
            // missing
        ],
    ],
];
