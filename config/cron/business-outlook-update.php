<?php

return [
    'defaults' => [
        'year' => 2015,
        'quarter' => 1,
    ],
    'content_types' => [
        'application/pdf',
    ],
    'url' => 'http://www.bsp.gov.ph/downloads/Publications/%d/BES_%dqtr%d.pdf',
    'file' => [
        'source' => '/bsp-expectations/BES_%dqtr%d.pdf',
        'destination' => '/temp/BES_%dqtr%d.txt',
        'pdf' => 'BES_%dqtr%d.pdf',
        'text' => 'BES_%dqtr%d.txt',
    ],
    'markers' => [
        'start' => 'Business Outlook Index on Own Operations: Current Quarter',
        'startParsing' => 'Industry Sector',
        'stop' => 'Transportation',
    ],
];
