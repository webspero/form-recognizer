<?php

return [
    /*
    |----------------------------------------------------------------------------
    | Google application name
    |----------------------------------------------------------------------------
    */
    'application_name' => 'Google Sheets API Quickstart',

    /*
    |----------------------------------------------------------------------------
    | Google OAuth 2.0 access
    |----------------------------------------------------------------------------
    |
    | Keys for OAuth 2.0 access, see the API console at
    | https://developers.google.com/console
    |
    */
    /*
    'client_id'       => '705376027797-7lh240aqbkb1ajrq9cco0auhddh5h1pd.apps.googleusercontent.com',
    'client_secret'   => 'WRo5WE6FBWSlNRueKVnXv6NS',
    'redirect_uri'    => 'http://localhost',
    'scopes'          => ["https://www.googleapis.com/auth/drive.readonly", "https://www.googleapis.com/auth/spreadsheets.readonly", "https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/spreadsheets"],
    'access_type'     => 'offline',
    'approval_prompt' => 'auto',
    */
    'client_id'       => '16596885702-085f6cas0j8tentdtu81jgpnif3lo9pd.apps.googleusercontent.com',
    'client_secret'   => 'J7J_p1YISw7ZMqhxYbh_gwA4',
    'redirect_uri'    => 'http://localhost',
    'scopes'          => [
            "https://www.googleapis.com/auth/drive.readonly",
            "https://www.googleapis.com/auth/spreadsheets.readonly",
            "https://www.googleapis.com/auth/drive",
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/calendar",
            "https://www.googleapis.com/auth/calendar.readonly"
            ],
    'access_type'     => 'offline',
    'approval_prompt' => 'auto',

    /*
    |----------------------------------------------------------------------------
    | Google developer key
    |----------------------------------------------------------------------------
    |
    | Simple API access key, also from the API console. Ensure you get
    | a Server key, and not a Browser key.
    |
    */
    'developer_key' => '',

    /*
    |----------------------------------------------------------------------------
    | Google service account
    |----------------------------------------------------------------------------
    |
    | Set the information below to use assert credentials
    | Leave blank to use app engine or compute engine.
    |
    */
    'service' => [
        /*
        | Example xxx@developer.gserviceaccount.com
        */
        'account' => 'cbpo-fintech@appspot.gserviceaccount.com',

        /*
        | Example ['https://www.googleapis.com/auth/cloud-platform']
        */
        'scopes' => [
            "https://www.googleapis.com/auth/drive.readonly",
            "https://www.googleapis.com/auth/spreadsheets.readonly",
            "https://www.googleapis.com/auth/drive",
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/calendar",
            "https://www.googleapis.com/auth/calendar.readonly"
            ],

        /*
        | Path to key file
        | Example storage_path().'/key/google.p12'
        */
        'key' => public_path('gprivate_key3.p12'),
    ],
];
