<?php
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('tblbank')->truncate();

	    
		    Bank::create(array(
		        'bank_name' => 'HSBC',
		        'bank_type' => 'Commercial',
		        'contact_person' => 'Test Person',
		        'contact_person_designation' => 'Chairman',
		        'bank_street_address1' => '123 Main St.',
		        'bank_street_address2' => 'Suite # 121',
		        'bank_city' => 'test bank_city',
		        'bank_no_of_offices' => '1',
		        'bank_phone' => '123456789',
		        'bank_email' => 'email@bank1.com',
		        'bank_website' => 'bank1.com'
		    ));

		    Bank::create(array(
		        'bank_name' => 'Standard Chartered',
		        'bank_type' => 'Commercial',
		        'contact_person' => 'Test Person 1',
		        'contact_person_designation' => 'Chairman',
		        'bank_street_address1' => '123 Main St.',
		        'bank_street_address2' => 'Suite # 121',
		        'bank_city' => 'test bank_city',
		        'bank_no_of_offices' => '1',
		        'bank_phone' => '123456789',
		        'bank_email' => 'email@bank2.com',
		        'bank_website' => 'bank.2com'
		    )); 
	}

}
