<?php

use Illuminate\Database\Seeder;

class IndustryGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Table industry_group */
        DB::table('tblindustry_group')->insert([
            'group_code'        => '011',
            'sub_code'          => '01',
            'group_description' => 'Growing of Non-perrenial Crops'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '012',
            'sub_code'          => '01',
            'group_description' => 'Growing of Perennial Crops'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '013',
            'sub_code'          => '01',
            'group_description' => 'Plant Propagation'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '014',
            'sub_code'          => '01',
            'group_description' => 'Animal Production'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '015',
            'sub_code'          => '01',
            'group_description' => 'Support Activities to Agriculture and Post-harvest Crop Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '017',
            'sub_code'          => '01',
            'group_description' => 'Hunting, Trapping and Related Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '021',
            'sub_code'          => '02',
            'group_description' => 'Silviculture and Other Forestry Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '022',
            'sub_code'          => '02',
            'group_description' => 'Logging'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '023',
            'sub_code'          => '02',
            'group_description' => 'Gathering of Non-wood Forest Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '024',
            'sub_code'          => '02',
            'group_description' => 'Support Services to Forestry'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '031',
            'sub_code'          => '03',
            'group_description' => 'Fishing'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '032',
            'sub_code'          => '03',
            'group_description' => 'Aquaculture'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '051',
            'sub_code'          => '05',
            'group_description' => 'Mining of Hard Coal'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '052',
            'sub_code'          => '05',
            'group_description' => 'Mining of Lignite'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '061',
            'sub_code'          => '06',
            'group_description' => 'Extraction of Crude Petroleum'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '062',
            'sub_code'          => '06',
            'group_description' => 'Extraction of Natural Gas'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '071',
            'sub_code'          => '07',
            'group_description' => 'Mining of Iron Ores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '072',
            'sub_code'          => '07',
            'group_description' => 'Mining of Non-ferrous Metal Ores Except Precious Metals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '081',
            'sub_code'          => '08',
            'group_description' => 'Quarrying of Stone, Sand and Clay'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '089',
            'sub_code'          => '08',
            'group_description' => 'Mining and Quarrying, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '091',
            'sub_code'          => '09',
            'group_description' => 'Support Activities for Petroleum and Gas Extraction'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '099',
            'sub_code'          => '09',
            'group_description' => 'Support Activities for Other Mining and Quarrying'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '101',
            'sub_code'          => '10',
            'group_description' => 'Processing and Preserving of Meat'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '102',
            'sub_code'          => '10',
            'group_description' => 'Processing and Preserving of Fish, Crustaceans and Mollusks'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '103',
            'sub_code'          => '10',
            'group_description' => 'Processing and Preserving of Fruits and Vegetables'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '104',
            'sub_code'          => '10',
            'group_description' => 'Manufacture of Vegetable and Animal Oils and Fats'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '105',
            'sub_code'          => '10',
            'group_description' => 'Manufacture of Dairy Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '106',
            'sub_code'          => '10',
            'group_description' => 'Manufacture of Grain Mill Products, Starches and Starch Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '107',
            'sub_code'          => '10',
            'group_description' => 'Manufacture of Other Food Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '108',
            'sub_code'          => '10',
            'group_description' => 'Manufacture of Prepared Animal Feeds'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '110',
            'sub_code'          => '11',
            'group_description' => 'Manufacture of Beverages'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '120',
            'sub_code'          => '12',
            'group_description' => 'Manufacture of Tobacco Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '131',
            'sub_code'          => '13',
            'group_description' => 'Spinning, Weaving and Finishing of Textiles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '139',
            'sub_code'          => '13',
            'group_description' => 'Manufacture of Other Textiles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '141',
            'sub_code'          => '14',
            'group_description' => 'Manufacture of Wearing Apparel, Except Fur Apparel'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '142',
            'sub_code'          => '14',
            'group_description' => 'Custom Tailoring and Dressmaking'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '143',
            'sub_code'          => '14',
            'group_description' => 'Manufacture of Knitted and Crocheted Apparel'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '144',
            'sub_code'          => '14',
            'group_description' => 'Manufacture of Articles of Fur'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '151',
            'sub_code'          => '15',
            'group_description' => 'Tanning and Dressing of Leather; Manufacture of Luggage and Handbags'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '152',
            'sub_code'          => '15',
            'group_description' => 'Manufacture of Footwear'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '161',
            'sub_code'          => '16',
            'group_description' => 'Sawmilling and Planing of Wood'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '162',
            'sub_code'          => '16',
            'group_description' => 'Manufacture of Products of Wood, Cork, Straw and Plaiting Materials'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '170',
            'sub_code'          => '17',
            'group_description' => 'Manufacture of Paper and Paper Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '181',
            'sub_code'          => '18',
            'group_description' => 'Printing and Service Activities Related to Printing'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '182',
            'sub_code'          => '18',
            'group_description' => 'Reproduction of Recorded Media'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '191',
            'sub_code'          => '19',
            'group_description' => 'Manufacture of Coke Oven Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '192',
            'sub_code'          => '19',
            'group_description' => 'Manufacture of Refined Petroleum Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '199',
            'sub_code'          => '19',
            'group_description' => 'Manufacture of Other Fuel Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '201',
            'sub_code'          => '20',
            'group_description' => 'Manufacture of Basic Chemicals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '202',
            'sub_code'          => '20',
            'group_description' => 'Manufacture of Other Chemical Products, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '203',
            'sub_code'          => '20',
            'group_description' => 'Manufacture of Man-made Fibers'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '210',
            'sub_code'          => '21',
            'group_description' => 'Manufacture of Pharmaceuticals, Medicinal Chemical and Botanical Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '221',
            'sub_code'          => '22',
            'group_description' => 'Manufacture of Rubber Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '222',
            'sub_code'          => '22',
            'group_description' => 'Manufacture of Plastics Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '231',
            'sub_code'          => '23',
            'group_description' => 'Manufacture of Glass and Glass Products'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '239',
            'sub_code'          => '23',
            'group_description' => 'Manufacture of Non-metallic Mineral Products, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '241',
            'sub_code'          => '24',
            'group_description' => 'Manufacture of Basic Iron and Steel'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '242',
            'sub_code'          => '24',
            'group_description' => 'Manufacture of Basic Precious and Other Non-ferrous Metals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '243',
            'sub_code'          => '24',
            'group_description' => 'Casting of Metals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '251',
            'sub_code'          => '25',
            'group_description' => 'Manufacture of Structural Metal Products, Tanks, Reservoirs and Steam Generators'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '332',
            'sub_code'          => '33',
            'group_description' => 'Installation of Industrial Machinery and Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '331',
            'sub_code'          => '33',
            'group_description' => 'Repair of Fabricated Metal Products, Machinery and Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '351',
            'sub_code'          => '35',
            'group_description' => 'Electric Power Generation, Transmission and Distribution'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '352',
            'sub_code'          => '35',
            'group_description' => 'Manufacture of Gas; Distribution of Gaseous Fuels Through Mains'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '353',
            'sub_code'          => '35',
            'group_description' => 'Steam, Air Conditioning Supply and Production of Ice'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '360',
            'sub_code'          => '36',
            'group_description' => 'Water Collection, Treatment and Supply'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '370',
            'sub_code'          => '37',
            'group_description' => 'Sewerage'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '321',
            'sub_code'          => '32',
            'group_description' => 'Manufacture of Jewelry, Bijouterie and Related Articles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '324',
            'sub_code'          => '32',
            'group_description' => 'Manufacture of Games and Toys'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '325',
            'sub_code'          => '32',
            'group_description' => 'Manufacture of Medical and Dental Instruments and Supplies'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '329',
            'sub_code'          => '32',
            'group_description' => 'Other Manufacturing, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '293',
            'sub_code'          => '29',
            'group_description' => 'Manufacture of Parts and Accessories for Motor Vehicles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '301',
            'sub_code'          => '30',
            'group_description' => 'Building of Ships and Boats'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '302',
            'sub_code'          => '30',
            'group_description' => 'Manufacture of Railway Locomotive and Rolling Stock'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '303',
            'sub_code'          => '30',
            'group_description' => 'Manufacture of Air and Spacecraft and Related Machinery'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '304',
            'sub_code'          => '30',
            'group_description' => 'Manufacture of Military Fighting Vehicles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '309',
            'sub_code'          => '30',
            'group_description' => 'Manufacture of Transport Equipment, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '310',
            'sub_code'          => '31',
            'group_description' => 'Manufacture of Furniture'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '322',
            'sub_code'          => '32',
            'group_description' => 'Manufacture of Musical Instruments'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '323',
            'sub_code'          => '32',
            'group_description' => 'Manufacture of Sports Goods'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '271',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Electric Motors, Generators, Transformers and Electricity Distribution and Control Apparatus'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '273',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Wiring and Wiring Devices'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '274',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Electric Lighting Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '275',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Domestic Appliances'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '279',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Other Electrical Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '281',
            'sub_code'          => '28',
            'group_description' => 'Manufacture of General Purpose Machinery'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '282',
            'sub_code'          => '28',
            'group_description' => 'Manufacture of Special Purpose Machinery'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '291',
            'sub_code'          => '29',
            'group_description' => 'Manufacture of Motor Vehicles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '292',
            'sub_code'          => '29',
            'group_description' => 'Manufacture of Bodies (Coachwork) for Motor Vehicles; Manufacture of Trailers and Semi-trailers'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '969',
            'sub_code'          => '96',
            'group_description' => 'Other Personal Service Activities, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '970',
            'sub_code'          => '97',
            'group_description' => 'Activities of Households as Employers of Domestic Personnel'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '981',
            'sub_code'          => '98',
            'group_description' => 'Undifferentiated Goods-producing Activities of Private Households for Own Use'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '982',
            'sub_code'          => '98',
            'group_description' => 'Undifferentiated Services-producing Activities of Private Households for Own Use'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '990',
            'sub_code'          => '99',
            'group_description' => 'Activities of Extra-territorial Organizations and Bodies'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '252',
            'sub_code'          => '25',
            'group_description' => 'Manufacture of Weapons and Ammunition'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '259',
            'sub_code'          => '25',
            'group_description' => 'Manufacture of Other Fabricated Metal Products; Metal Working Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '261',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Electronic Components'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '262',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Computers and Peripheral Equipment and Accessories'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '263',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Communication Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '264',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Consumer Electronics'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '265',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Measuring, Testing, Navigating and Control Equipment; Watches and Clocks'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '266',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Irradiation, Electromedical and Electrotherapeutic Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '267',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Optical Instruments and Photographic Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '268',
            'sub_code'          => '26',
            'group_description' => 'Manufacture of Magnetic and Optical Media'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '272',
            'sub_code'          => '27',
            'group_description' => 'Manufacture of Batteries and Accumulators'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '381',
            'sub_code'          => '38',
            'group_description' => 'Waste Collection'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '382',
            'sub_code'          => '38',
            'group_description' => 'Waste Treatment and Disposal'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '383',
            'sub_code'          => '38',
            'group_description' => 'Materials Recovery'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '390',
            'sub_code'          => '39',
            'group_description' => 'Remediation Activities and Other Waste Management Services'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '410',
            'sub_code'          => '41',
            'group_description' => 'Construction of Buildings'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '421',
            'sub_code'          => '42',
            'group_description' => 'Construction of Roads and Railways'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '422',
            'sub_code'          => '42',
            'group_description' => 'Construction of Utility Projects'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '429',
            'sub_code'          => '42',
            'group_description' => 'Construction of Other Civil Engineering Projects'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '431',
            'sub_code'          => '43',
            'group_description' => 'Demolition and Site Preparation'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '432',
            'sub_code'          => '43',
            'group_description' => 'Electrical, Plumbing and Other Construction Installation Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '433',
            'sub_code'          => '43',
            'group_description' => 'Building Completion and Finishing'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '439',
            'sub_code'          => '43',
            'group_description' => 'Other Specialized Construction Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '451',
            'sub_code'          => '45',
            'group_description' => 'Sale of Motor Vehicles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '453',
            'sub_code'          => '45',
            'group_description' => 'Sale of Motor Vehicle Parts and Accessories'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '454',
            'sub_code'          => '45',
            'group_description' => 'Sale, Maintenance and Repair of Motorcycles and Related Parts and Accessories'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '461',
            'sub_code'          => '46',
            'group_description' => 'Wholesale on a Fee or Contract Basis'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '462',
            'sub_code'          => '46',
            'group_description' => 'Wholesale of Agricultural Raw Materials and Live Animals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '463',
            'sub_code'          => '46',
            'group_description' => 'Wholesale of Food, Beverages and Tobacco'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '464',
            'sub_code'          => '46',
            'group_description' => 'Wholesale of Household Goods'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '465',
            'sub_code'          => '46',
            'group_description' => 'Wholesale of Machinery, Equipment and Supplies'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '466',
            'sub_code'          => '46',
            'group_description' => 'Other Specialized Wholesale'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '469',
            'sub_code'          => '46',
            'group_description' => 'Non-specialized Wholesale Trade'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '471',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale in Non-specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '472',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Food, Beverages and Tobacco in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '473',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Automotive Fuel in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '474',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Information and Communications Equipment in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '475',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Other Household Equipment in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '476',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Cultural and Recreation Goods in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '477',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale of Other Goods in Specialized Stores'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '478',
            'sub_code'          => '47',
            'group_description' => 'Retail Sale Via Stalls and Markets'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '479',
            'sub_code'          => '47',
            'group_description' => 'Retail Trade Not in Stores, Stalls or Markets'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '491',
            'sub_code'          => '49',
            'group_description' => 'Transport Via Railways'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '492',
            'sub_code'          => '49',
            'group_description' => 'Transport Via Buses'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '493',
            'sub_code'          => '49',
            'group_description' => 'Other Land Transport'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '494',
            'sub_code'          => '49',
            'group_description' => 'Transport Via Pipeline'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '501',
            'sub_code'          => '50',
            'group_description' => 'Sea and Coastal Water Transport'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '502',
            'sub_code'          => '50',
            'group_description' => 'Inland Water Transport'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '511',
            'sub_code'          => '51',
            'group_description' => 'Passenger Air Transport'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '512',
            'sub_code'          => '51',
            'group_description' => 'Freight Air Transport'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '521',
            'sub_code'          => '52',
            'group_description' => 'Warehousing and Storage'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '522',
            'sub_code'          => '52',
            'group_description' => 'Support Activities for Transportation'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '531',
            'sub_code'          => '53',
            'group_description' => 'Postal Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '532',
            'sub_code'          => '53',
            'group_description' => 'Courier Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '551',
            'sub_code'          => '55',
            'group_description' => 'Short Term Acommodation Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '559',
            'sub_code'          => '55',
            'group_description' => 'Other Accommodation'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '561',
            'sub_code'          => '56',
            'group_description' => 'Restaurants and Mobile Food Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '562',
            'sub_code'          => '56',
            'group_description' => 'Event Catering and Other Food Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '563',
            'sub_code'          => '56',
            'group_description' => 'Beverage Serving Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '581',
            'sub_code'          => '58',
            'group_description' => 'Publishing of Books, Periodicals and Other Publishing Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '582',
            'sub_code'          => '58',
            'group_description' => 'Software Publishing'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '591',
            'sub_code'          => '59',
            'group_description' => 'Motion Picture, Video and Television Programme Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '592',
            'sub_code'          => '59',
            'group_description' => 'Sound Recording and Music Publishing Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '601',
            'sub_code'          => '60',
            'group_description' => 'Radio Broadcasting'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '602',
            'sub_code'          => '60',
            'group_description' => 'Television Programming and Broadcasting Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '611',
            'sub_code'          => '61',
            'group_description' => 'Wired Telecommunications Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '612',
            'sub_code'          => '61',
            'group_description' => 'Wireless Telecommunications Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '613',
            'sub_code'          => '61',
            'group_description' => 'Satellite Telecommunications Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '619',
            'sub_code'          => '61',
            'group_description' => 'Other Telecommunications Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '620',
            'sub_code'          => '62',
            'group_description' => 'Computer Programming, Consultancy and Related Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '631',
            'sub_code'          => '63',
            'group_description' => 'Data Processing, Hosting and Related Activities; Web Portals'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '639',
            'sub_code'          => '63',
            'group_description' => 'Other Information Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '641',
            'sub_code'          => '64',
            'group_description' => 'Monetary Intermediation'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '642',
            'sub_code'          => '64',
            'group_description' => 'Activities of Holding Companies'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '643',
            'sub_code'          => '64',
            'group_description' => 'Trusts, Funds and Other Financial Vehicles'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '649',
            'sub_code'          => '64',
            'group_description' => 'Other Financial Service Activities, Except Insurance and Pension Funding Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '651',
            'sub_code'          => '65',
            'group_description' => 'Insurance'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '652',
            'sub_code'          => '65',
            'group_description' => 'Reinsurance'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '653',
            'sub_code'          => '65',
            'group_description' => 'Pension Funding'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '661',
            'sub_code'          => '66',
            'group_description' => 'Activities Auxiliary to Financial Service, Except Insurance and Pension Funding'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '662',
            'sub_code'          => '66',
            'group_description' => 'Activities Auxillary to Insurance and Pension Funding'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '663',
            'sub_code'          => '66',
            'group_description' => 'Fund Management Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '681',
            'sub_code'          => '68',
            'group_description' => 'Real Estate Activities with Own or Leased Property'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '682',
            'sub_code'          => '68',
            'group_description' => 'Real Estate Activities on a Fee or Contract Basis'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '691',
            'sub_code'          => '69',
            'group_description' => 'Legal Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '692',
            'sub_code'          => '69',
            'group_description' => 'Accounting, Bookkeeping and Auditing Activities; Tax Consultancy'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '701',
            'sub_code'          => '70',
            'group_description' => 'Activities of Head Offices'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '702',
            'sub_code'          => '70',
            'group_description' => 'Management Consultancy Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '711',
            'sub_code'          => '71',
            'group_description' => 'Architectural and Engineering Activities and Related Technical Consultancy'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '712',
            'sub_code'          => '71',
            'group_description' => 'Technical Testing and Analysis'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '721',
            'sub_code'          => '72',
            'group_description' => 'Research and Experimental Development on Natural Sciences and Engineering'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '722',
            'sub_code'          => '72',
            'group_description' => 'Research and Experimental Development on Social Sciences and Humanities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '723',
            'sub_code'          => '72',
            'group_description' => 'Research and Experimental Development in Information Technology'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '731',
            'sub_code'          => '73',
            'group_description' => 'Advertising'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '732',
            'sub_code'          => '73',
            'group_description' => 'Market Research and Public Opinion Polling'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '741',
            'sub_code'          => '74',
            'group_description' => 'Specialized Design Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '742',
            'sub_code'          => '74',
            'group_description' => 'Photographic Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '749',
            'sub_code'          => '74',
            'group_description' => 'Other Professional, Scientific and Technical Activities, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '750',
            'sub_code'          => '75',
            'group_description' => 'Veterinary Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '771',
            'sub_code'          => '77',
            'group_description' => 'Renting and Leasing of Motor Vehicles (Except Motorcycle, Caravans, Campers)'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '772',
            'sub_code'          => '77',
            'group_description' => 'Renting and Leasing of Personal and Household Goods'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '773',
            'sub_code'          => '77',
            'group_description' => 'Renting and Leasing of Other Machinery, Equipment and Tangible Goods, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '774',
            'sub_code'          => '77',
            'group_description' => 'Leasing of Intellectual Property and Similar Products, Except Copyrighted Works'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '781',
            'sub_code'          => '78',
            'group_description' => 'Activities of Employment Placement Agencies'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '782',
            'sub_code'          => '78',
            'group_description' => 'Temporary Employment Agency Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '783',
            'sub_code'          => '78',
            'group_description' => 'Other Human Resources Provision'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '791',
            'sub_code'          => '79',
            'group_description' => 'Travel Agency and Tour Operator Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '799',
            'sub_code'          => '79',
            'group_description' => 'Other Reservation Service and Related Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '801',
            'sub_code'          => '80',
            'group_description' => 'Private Security Activites'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '802',
            'sub_code'          => '20',
            'group_description' => 'Security Systems Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '803',
            'sub_code'          => '80',
            'group_description' => 'Investigation Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '811',
            'sub_code'          => '81',
            'group_description' => 'Combined Facilities Support Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '812',
            'sub_code'          => '81',
            'group_description' => 'Cleaning Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '813',
            'sub_code'          => '81',
            'group_description' => 'Landscape Care and Maintenance Service Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '821',
            'sub_code'          => '82',
            'group_description' => 'Office Administrative and Support Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '822',
            'sub_code'          => '82',
            'group_description' => 'Call Centers and Other Related Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '823',
            'sub_code'          => '82',
            'group_description' => 'Organization of Conventions and Trade Shows'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '829',
            'sub_code'          => '82',
            'group_description' => 'Business Support Service Activities, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '831',
            'sub_code'          => '83',
            'group_description' => 'Administration of the State and the Economic and Social Policy of the Community'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '841',
            'sub_code'          => '84',
            'group_description' => 'Administration of the State and the Economic and Social Policy of the Community'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '842',
            'sub_code'          => '84',
            'group_description' => 'Provision of Services to the Community as a Whole'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '843',
            'sub_code'          => '84',
            'group_description' => 'Compulsory Social Security Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '851',
            'sub_code'          => '85',
            'group_description' => 'Pre-primary/pre-school Education'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '852',
            'sub_code'          => '85',
            'group_description' => 'Primary/elementary Education'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '853',
            'sub_code'          => '85',
            'group_description' => 'Secondary/high School Education'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '854',
            'sub_code'          => '85',
            'group_description' => 'Higher Education'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '855',
            'sub_code'          => '85',
            'group_description' => 'Other Education Services'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '856',
            'sub_code'          => '85',
            'group_description' => 'Educational Support Services'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '861',
            'sub_code'          => '86',
            'group_description' => 'Hospital Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '862',
            'sub_code'          => '86',
            'group_description' => 'Medical and Dental Practice Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '869',
            'sub_code'          => '86',
            'group_description' => 'Other Human Health Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '871',
            'sub_code'          => '87',
            'group_description' => 'Residential Nursing Care Facilities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '872',
            'sub_code'          => '87',
            'group_description' => 'Residential Care Activities for Mental Retardation, Mental Health and Substance Abuse'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '873',
            'sub_code'          => '87',
            'group_description' => 'Residential Care Activities for the Elderly and Disabled'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '879',
            'sub_code'          => '87',
            'group_description' => 'Other Residential Care Activities, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '881',
            'sub_code'          => '88',
            'group_description' => 'Social Work Activities without Accommodation for the Elderly and Disabled'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '889',
            'sub_code'          => '88',
            'group_description' => 'Other Social Work Activities without Accommodation, N.e.c.'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '900',
            'sub_code'          => '90',
            'group_description' => 'Creative, Arts and Entertainment Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '910',
            'sub_code'          => '91',
            'group_description' => 'Libraries, Archives, Museums and Other Cultural Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '920',
            'sub_code'          => '92',
            'group_description' => 'Gambling and Betting Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '931',
            'sub_code'          => '93',
            'group_description' => 'Sports Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '932',
            'sub_code'          => '93',
            'group_description' => 'Other Amusement and Recreation Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '941',
            'sub_code'          => '94',
            'group_description' => 'Activities of Business, Employers and Professional Membership Organizations'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '942',
            'sub_code'          => '94',
            'group_description' => 'Activities of Trade Unions'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '949',
            'sub_code'          => '94',
            'group_description' => 'Activities of Other Membership Organizations'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '951',
            'sub_code'          => '95',
            'group_description' => 'Repair of Computers and Communications Equipment'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '952',
            'sub_code'          => '95',
            'group_description' => 'Repair of Personal and Household Goods'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '961',
            'sub_code'          => '96',
            'group_description' => 'Personal Services for Wellness, Except Sports Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '962',
            'sub_code'          => '96',
            'group_description' => 'Laundry Services'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '963',
            'sub_code'          => '96',
            'group_description' => 'Funeral and Related Activities'
        ]);

        DB::table('tblindustry_group')->insert([
            'group_code'        => '964',
            'sub_code'          => '96',
            'group_description' => 'Domestic Services'
        ]);
    }
}
