<?php

use Illuminate\Database\Seeder;

class RolesAndStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_roles')->insert(['description' => 'Admin', 'id' => 0]);
        DB::table('user_roles')->insert(['description' => 'Basic User', 'id' => 1]);
        DB::table('user_roles')->insert(['description' => 'Credit Investigator', 'id' => 2]);
        DB::table('user_roles')->insert(['description' => 'Analyst', 'id' => 3]);
        DB::table('user_roles')->insert(['description' => 'Supervisror', 'id' => 4]);
        DB::table('user_roles')->insert(['description' => 'Contractor', 'id' => 5]);  //  new role for PCAB Document Verifier

        DB::table('report_status')->insert(['description' => 'Report is pending', 'id' => 0]);
        DB::table('report_status')->insert(['description' => 'Report is in-progress	', 'id' => 1]);
        DB::table('report_status')->insert(['description' => 'Report is submitted for analyst review', 'id' => 5]);
        DB::table('report_status')->insert(['description' => 'Report is in progress for admin review', 'id' => 6]);
        DB::table('report_status')->insert(['description' => 'Report is completed', 'id' => 7]);
        DB::table('report_status')->insert(['description' => 'Report is in progress for document review by PCAB', 'id' => 8]);
    }
}