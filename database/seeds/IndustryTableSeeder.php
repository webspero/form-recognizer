<?php

use Illuminate\Database\Seeder;

class IndustryTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('industry_class')->truncate();
        $array = array( 
            array("name"=>"Palay", "main_id" =>2006, "status" => 1, "is_deleted" => 0),
            array("name"=>"Corn", "main_id" =>2006, "status" => 1, "is_deleted" => 0),
            array("name"=>"Coconut including copra", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Sugarcane", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Banana", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Mango", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Pineapple", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Coffee", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Cassava", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Rubber", "main_id" =>2010, "status" => 1, "is_deleted" => 0),
            array("name"=>"Other crops", "main_id" =>2007, "status" => 1, "is_deleted" => 0),
            array("name"=>"Livestock", "main_id" =>2009, "status" => 1, "is_deleted" => 0),
            array("name"=>"Poultry", "main_id" =>2009, "status" => 1, "is_deleted" => 0),
            array("name"=>"Agricultural activities and services", "main_id" =>2010, "status" => 1, "is_deleted" => 0),
            array("name"=>"FORESTRY", "main_id" =>2013, "status" => 1, "is_deleted" => 0),
            array("name"=>"FISHING", "main_id" =>2016, "status" => 1, "is_deleted" => 0),
            array("name"=>"Copper mining", "main_id" =>2023, "status" => 1, "is_deleted" => 0),
            array("name"=>"Gold mining", "main_id" =>2023, "status" => 1, "is_deleted" => 0),
            array("name"=>"Chromium mining", "main_id" =>2023, "status" => 1, "is_deleted" => 0),
            array("name"=>"Nickel mining", "main_id" =>2023, "status" => 1, "is_deleted" => 0),
            array("name"=>"Other metallic mining", "main_id" =>2022, "status" => 1, "is_deleted" => 0),
            array("name"=>"Crude oil Natural Gas & Condensate", "main_id" =>2020, "status" => 1, "is_deleted" => 0),
            array("name"=>"Stone quarrying Clay and Sandpits", "main_id" =>2024, "status" => 1, "is_deleted" => 0),
            array("name"=>"Other non-metallic mining", "main_id" =>2025, "status" => 1, "is_deleted" => 0),
            array("name"=>"Food manufactures", "main_id" =>2030, "status" => 1, "is_deleted" => 0),
            array("name"=>"Beverage industries", "main_id" =>2036, "status" => 1, "is_deleted" => 0),
            array("name"=>"Tobacco manufactures", "main_id" =>2037, "status" => 1, "is_deleted" => 0),
            array("name"=>"Textile manufactures", "main_id" =>2039, "status" => 1, "is_deleted" => 0),
            array("name"=>"Wearing apparel", "main_id" =>2040, "status" => 1, "is_deleted" => 0),
            array("name"=>"Footwear and leather and leather products", "main_id" =>2045, "status" => 1, "is_deleted" => 0),
            array("name"=>"Wood, bamboo, cane and rattan articles", "main_id" =>2047, "status" => 1, "is_deleted" => 0),
            array("name"=>"Paper and paper products", "main_id" =>2048, "status" => 1, "is_deleted" => 0),
            array("name"=>"Publishing and printing", "main_id" =>2049, "status" => 1, "is_deleted" => 0),
            array("name"=>"Petroleum and other fuel products", "main_id" =>2052, "status" => 1, "is_deleted" => 0),
            array("name"=>"Chemical & chemical products", "main_id" =>2055, "status" => 1, "is_deleted" => 0),
            array("name"=>"Rubber and plastic products", "main_id" =>2058, "status" => 1, "is_deleted" => 0),
            array("name"=>"Non-metallic mineral products", "main_id" =>2061, "status" => 1, "is_deleted" => 0),
            array("name"=>"Basic metal industries", "main_id" =>2062, "status" => 1, "is_deleted" => 0),
            array("name"=>"Fabricated metal products", "main_id" =>2067, "status" => 1, "is_deleted" => 0),
            array("name"=>"Machinery and equipment except electrical", "main_id" =>2099, "status" => 1, "is_deleted" => 0),
            array("name"=>"Office, accounting and computing machinery", "main_id" =>2069, "status" => 1, "is_deleted" => 0),
            array("name"=>"Electrical machinery and apparatus", "main_id" =>2086, "status" => 1, "is_deleted" => 0),
            array("name"=>"Radio, television and communication equipment and apparatus", "main_id" =>2070, "status" => 1, "is_deleted" => 0),
            array("name"=>"Transport equipment", "main_id" =>2091, "status" => 1, "is_deleted" => 0),
            array("name"=>"Furniture and fixtures", "main_id" =>2092, "status" => 1, "is_deleted" => 0),
            array("name"=>"Miscellaneous manufactures", "main_id" =>2092, "status" => 1, "is_deleted" => 0),
            array("name"=>"PUBLIC", "main_id" =>2114, "status" => 1, "is_deleted" => 0),
            array("name"=>"PRIVATE", "main_id" =>2114, "status" => 1, "is_deleted" => 0),
            array("name"=>"ELECTRICITY", "main_id" =>2105, "status" => 1, "is_deleted" => 0),
            array("name"=>"STEAM", "main_id" =>2107, "status" => 1, "is_deleted" => 0),
            array("name"=>"WATER", "main_id" =>2108, "status" => 1, "is_deleted" => 0),
            array("name"=>"Land", "main_id" =>2144, "status" => 1, "is_deleted" => 0),
            array("name"=>"Water", "main_id" =>2146, "status" => 1, "is_deleted" => 0),
            array("name"=>"Air", "main_id" =>2149, "status" => 1, "is_deleted" => 0),
            array("name"=>"Storage and services incidental to transport", "main_id" =>2151, "status" => 1, "is_deleted" => 0),
            array("name"=>"COMMUNICATION", "main_id" =>2151, "status" => 1, "is_deleted" => 0),
            array("name"=>"Maintenance and Repair of Motor Vehicles Motorcycles Personal and Household Goods", "main_id" =>2123, "status" => 1, "is_deleted" => 0),
            array("name"=>"Wholesale Trade", "main_id" =>2124, "status" => 1, "is_deleted" => 0),
            array("name"=>"Retail Trade", "main_id" =>2141, "status" => 1, "is_deleted" => 0),
            array("name"=>"Banking Institutions", "main_id" =>2172, "status" => 1, "is_deleted" => 0),
            array("name"=>"Non-bank Financial Intermediation", "main_id" =>2174, "status" => 1, "is_deleted" => 0),
            array("name"=>"Insurance", "main_id" =>2176, "status" => 1, "is_deleted" => 0),
            array("name"=>"Activities Auxiliary to Financial Intermediation", "main_id" =>2179, "status" => 1, "is_deleted" => 0),
            array("name"=>"Real Estate", "main_id" =>2182, "status" => 1, "is_deleted" => 0),
            array("name"=>"Renting and other Business Activities", "main_id" =>2183, "status" => 1, "is_deleted" => 0),
            array("name"=>"Ownership of Dwellings", "main_id" =>2182, "status" => 1, "is_deleted" => 0),
            array("name"=>"Education", "main_id" =>2219, "status" => 1, "is_deleted" => 0),
            array("name"=>"Health and Social Work", "main_id" =>2222, "status" => 1, "is_deleted" => 0),
            array("name"=>"Hotels and Restaurants", "main_id" =>2155, "status" => 1, "is_deleted" => 0),
            array("name"=>"Sewage and Refuse Disposal Sanitation and Similar Activities", "main_id" =>2243, "status" => 1, "is_deleted" => 0),
            array("name"=>"Recreational Cultural and Sporting Activities", "main_id" =>2229, "status" => 1, "is_deleted" => 0),
            array("name"=>"Other Service Activities", "main_id" =>2239, "status" => 1, "is_deleted" => 0)
        );
        
        foreach ($array as $val ) {
            IndustryClass::create($val);
        }
	}

}
