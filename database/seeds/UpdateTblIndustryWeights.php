<?php

use Illuminate\Database\Seeder;

class UpdateTblIndustryWeights extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tblbankindustryweights')->where('industry_id', 1)->update(['main_code' => 'A']);
        DB::table('tblbankindustryweights')->where('industry_id', 2)->update(['main_code' => 'B']);
        DB::table('tblbankindustryweights')->where('industry_id', 3)->update(['main_code' => 'C']);
        DB::table('tblbankindustryweights')->where('industry_id', 4)->update(['main_code' => 'D']);
        DB::table('tblbankindustryweights')->where('industry_id', 5)->update(['main_code' => 'E']);
        DB::table('tblbankindustryweights')->where('industry_id', 6)->update(['main_code' => 'F']);
        DB::table('tblbankindustryweights')->where('industry_id', 7)->update(['main_code' => 'G']);
        DB::table('tblbankindustryweights')->where('industry_id', 8)->update(['main_code' => 'H']);
        DB::table('tblbankindustryweights')->where('industry_id', 9)->update(['main_code' => 'I']);
        DB::table('tblbankindustryweights')->where('industry_id', 10)->update(['main_code' => 'J']);
        DB::table('tblbankindustryweights')->where('industry_id', 11)->update(['main_code' => 'K']);
        DB::table('tblbankindustryweights')->where('industry_id', 12)->update(['main_code' => 'L']);
        DB::table('tblbankindustryweights')->where('industry_id', 13)->update(['main_code' => 'M']);
        DB::table('tblbankindustryweights')->where('industry_id', 14)->update(['main_code' => 'N']);
        DB::table('tblbankindustryweights')->where('industry_id', 15)->update(['main_code' => 'P']);
        DB::table('tblbankindustryweights')->where('industry_id', 16)->update(['main_code' => 'Q']);
        DB::table('tblbankindustryweights')->where('industry_id', 17)->update(['main_code' => 'R']);
        DB::table('tblbankindustryweights')->where('industry_id', 18)->update(['main_code' => 'S']);
        DB::table('tblbankindustryweights')->where('industry_id', 19)->update(['main_code' => 'T']);
        DB::table('tblbankindustryweights')->where('industry_id', 20)->update(['main_code' => 'U']);
        DB::table('tblbankindustryweights')->where('industry_id', 21)->update(['main_code' => 'O']);
    }
}
