<?php

use Illuminate\Database\Seeder;

class refcitymunPremiumPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('refcitymun')->update(['premiumReportPrice' => 3500]);
        DB::table('refcitymun')->where('regDesc', '13')->update(['premiumReportPrice' => 800]);
        DB::table('refcitymun')->whereIn('provCode', array('0434', '0421', '0458', '0314'))->update(['premiumReportPrice' => 1200]);
    }
}
