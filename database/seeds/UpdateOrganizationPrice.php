<?php

use Illuminate\Database\Seeder;

class UpdateOrganizationPrice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organization = DB::table('tblbank')->get();

        // Iterate Organizaton
        foreach($organization as $bank){

            // Update default price for simplified report
            if($bank->simplified_price == 3500){
                DB::table('tblbank')->where('id', $bank->id)->update(['simplified_price' => 1767.5]);
            }

            // Update default price for standalone report
            if($bank->standalone_price == 3500){
                DB::table('tblbank')->where('id', $bank->id)->update(['standalone_price' => 3535]);
            }

            // Update default price for premium zone 1 report
            if($bank->premium_zone1_price == 4300){
                DB::table('tblbank')->where('id', $bank->id)->update(['premium_zone1_price' => 4343]);
            }

            // Update default price for premium zone 2 report
            if($bank->premium_zone2_price == 5300){
                DB::table('tblbank')->where('id', $bank->id)->update(['premium_zone2_price' => 5353]);
            }

            // Update premium zone 3 report
            if($bank->premium_zone3_price == 6300){
                DB::table('tblbank')->where('id', $bank->id)->update(['premium_zone3_price' => 6363]);
            }
        }
    }
}
