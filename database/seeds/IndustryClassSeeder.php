<?php

use Illuminate\Database\Seeder;

class IndustryClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Table industry_class */
        DB::table('tblindustry_class')->insert([
            'class_code'            => '0111',
            'group_code'            => '011',
            'class_description'     => 'Growing of Cereals (Except Rice and Corn), Leguminous Crops and Oil Seeds',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0112',
            'group_code'            => '011',
            'class_description'     => 'Growing of Paddy Rice',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0113',
            'group_code'            => '011',
            'class_description'     => 'Growing of Corn, Except Young Corn (Vegetable)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0114',
            'group_code'            => '011',
            'class_description'     => 'Growing of Sugarcane Including Muscovado Sugar-making in the Farm',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0115',
            'group_code'            => '011',
            'class_description'     => 'Growing of Tobacco',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0116',
            'group_code'            => '011',
            'class_description'     => 'Growing of Fiber Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0117',
            'group_code'            => '011',
            'class_description'     => 'Growing of Leafy and Fruit Bearing Vegetables',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0118',
            'group_code'            => '011',
            'class_description'     => 'Growing of Other Vegetables, Melons, Roots and Tubers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0119',
            'group_code'            => '011',
            'class_description'     => 'Growing of Other Non-perennial Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0121',
            'group_code'            => '012',
            'class_description'     => 'Growing of Banana',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0122',
            'group_code'            => '012',
            'class_description'     => 'Growing of Pineapple',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0123',
            'group_code'            => '012',
            'class_description'     => 'Growing of Citrus Fruits',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0124',
            'group_code'            => '012',
            'class_description'     => 'Growing of Mango',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0125',
            'group_code'            => '012',
            'class_description'     => 'Growing of Papaya',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0126',
            'group_code'            => '012',
            'class_description'     => 'Growing of Coconut, Including Copra-making, Tuba Gathering and Coco-shell Charcoal Making in the Farm',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0127',
            'group_code'            => '012',
            'class_description'     => 'Growing of Beverage Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0128',
            'group_code'            => '012',
            'class_description'     => 'Growing of Spices, Aromatic, Drugs and Pharmaceutical Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0129',
            'group_code'            => '012',
            'class_description'     => 'Growing of Other Fruits and Perennial Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0130',
            'group_code'            => '013',
            'class_description'     => 'Plant Propagation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0141',
            'group_code'            => '014',
            'class_description'     => 'Raising of Cattle and Buffaloes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0142',
            'group_code'            => '014',
            'class_description'     => 'Raising of Horses and Other Equines',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0143',
            'group_code'            => '014',
            'class_description'     => 'Dairy Farming',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0144',
            'group_code'            => '014',
            'class_description'     => 'Raising of Sheeps and Goats',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0145',
            'group_code'            => '014',
            'class_description'     => 'Hog Farming',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0146',
            'group_code'            => '014',
            'class_description'     => 'Chicken Production (Including Operation Chicken Hatcheries)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0147',
            'group_code'            => '014',
            'class_description'     => 'Raising of Poultry (Except Chicken)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0148',
            'group_code'            => '014',
            'class_description'     => 'Egg Production',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0149',
            'group_code'            => '014',
            'class_description'     => 'Raising of Other Animals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0151',
            'group_code'            => '015',
            'class_description'     => 'Operation of Irrigation Systems Through Cooperatives and Non-cooperatives',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0152',
            'group_code'            => '015',
            'class_description'     => 'Planting, Transplanting and Other Related Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0153',
            'group_code'            => '015',
            'class_description'     => 'Services to Establish Crops, Promote Their Growth and Protect Them from Pests and Diseases',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0154',
            'group_code'            => '015',
            'class_description'     => 'Harvesting, Threshing, Grading, Bailing and Related Services',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0155',
            'group_code'            => '015',
            'class_description'     => 'Rental of Farm Machinery with Drivers and Crew',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0156',
            'group_code'            => '015',
            'class_description'     => 'Support Activities for Animal Production',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0157',
            'group_code'            => '015',
            'class_description'     => 'Post-harvest Crop Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0158',
            'group_code'            => '015',
            'class_description'     => 'Seed Processing for Propagation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0170',
            'group_code'            => '017',
            'class_description'     => 'Hunting, Trapping and Related Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0211',
            'group_code'            => '021',
            'class_description'     => 'Growing of Timber Forest Species (E.g. Gemelina, Eucalyptus, Etc.), Planting, Replanting, Transplanting, Thinning and Conserving of Forest and Timber Tracts',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0212',
            'group_code'            => '021',
            'class_description'     => 'Operation of Forest Tree Nurseries',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0220',
            'group_code'            => '022',
            'class_description'     => 'Logging',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0230',
            'group_code'            => '023',
            'class_description'     => 'Gathering of Non-wood Forest Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0240',
            'group_code'            => '024',
            'class_description'     => 'Support Services to Forestry',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0311',
            'group_code'            => '031',
            'class_description'     => 'Marine Fishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0312',
            'group_code'            => '031',
            'class_description'     => 'Freshwater Fishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0313',
            'group_code'            => '031',
            'class_description'     => 'Support Service Activities Incidental to Fishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0321',
            'group_code'            => '032',
            'class_description'     => 'Operation of Freshwater Fish Pond, Fish Pens, Cage and Hatcheries',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0322',
            'group_code'            => '032',
            'class_description'     => 'Operation of Marine or Sea Water Fish Tanks, Pens, Cage and Hatcheries',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0323',
            'group_code'            => '032',
            'class_description'     => 'Prawn Culture in Brackish Water',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0324',
            'group_code'            => '032',
            'class_description'     => 'Culture of Mollusks, Bivalves and Other Crustaceans (Except Prawn Culture)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0325',
            'group_code'            => '032',
            'class_description'     => 'Pearl Culture and Pearl Shell Gathering',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0326',
            'group_code'            => '032',
            'class_description'     => 'Seaweeds Farming and Gathering',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0329',
            'group_code'            => '032',
            'class_description'     => 'Other Aquaculture Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0510',
            'group_code'            => '051',
            'class_description'     => 'Mining of Hard Coal',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0520',
            'group_code'            => '052',
            'class_description'     => 'Mining of Lignite',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0610',
            'group_code'            => '061',
            'class_description'     => 'Extraction of Crude Petroleum',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0620',
            'group_code'            => '062',
            'class_description'     => 'Extraction of Natural Gas',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0710',
            'group_code'            => '071',
            'class_description'     => 'Mining of Iron Ores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0721',
            'group_code'            => '072',
            'class_description'     => 'Mining of Uranium and Thorium Ores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0722',
            'group_code'            => '072',
            'class_description'     => 'Mining of Precious Metals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0729',
            'group_code'            => '072',
            'class_description'     => 'Mining of Other Non-ferrous Metal Ores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0810',
            'group_code'            => '081',
            'class_description'     => 'Quarrying of Stone, Sand and Clay',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0891',
            'group_code'            => '089',
            'class_description'     => 'Mining of Chemical and Fertilizer Minerals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0892',
            'group_code'            => '089',
            'class_description'     => 'Extraction of Peat',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0893',
            'group_code'            => '089',
            'class_description'     => 'Extraction of Salt',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0899',
            'group_code'            => '089',
            'class_description'     => 'Other Mining and Quarrying, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0910',
            'group_code'            => '091',
            'class_description'     => 'Support Activities for Petroleum and Gas Extraction',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '0990',
            'group_code'            => '099',
            'class_description'     => 'Support Activities for Other Mining and Quarrying',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1011',
            'group_code'            => '101',
            'class_description'     => 'Slaughtering and Meat Packing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1012',
            'group_code'            => '101',
            'class_description'     => 'Production Processing and Preserving of Meat and Meat Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1020',
            'group_code'            => '102',
            'class_description'     => 'Processing and Preserving of Fish, Crustaceans and Mollusks',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1030',
            'group_code'            => '103',
            'class_description'     => 'Processing and Preserving of Fruits and Vegetables',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1041',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Virgin Coconut Oil',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1042',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Dessicated Coconut',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1043',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Nata De Coco',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1044',
            'group_code'            => '104',
            'class_description'     => 'Production of Crude Vegetable Oil, Cake and Meals, Other Than Virgin Coconut Oil',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1045',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Refined Coconut and Other Vegetable Oil (Including Corn Oil) and Margarine',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1046',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Fish Oil and Other Marine Animal Oils',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1047',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Unprepared Animal Feeds from Vegetable, Animal Oils and Fats',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1049',
            'group_code'            => '104',
            'class_description'     => 'Manufacture of Vegetable and Animal Oil and Fats, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1051',
            'group_code'            => '105',
            'class_description'     => 'Processing of Fresh Milk and Cream',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1052',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Powdered Milk (Except for Infants) and Condensed or Evaporated Milk (Filled, Combined or Reconstituted)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1053',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Infants Powdered Milk',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1054',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Butter, Cheese and Curd',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1055',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Ice Cream and Sherbet, Ice Drop, Ice Candy and Other Flavored Ices',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1056',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Milk-based Infants and Dietetic Foods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1057',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Yoghurt',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1058',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Whey',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1059',
            'group_code'            => '105',
            'class_description'     => 'Manufacture of Dairy Products, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1061',
            'group_code'            => '106',
            'class_description'     => 'Rice/corn Milling',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1062',
            'group_code'            => '106',
            'class_description'     => 'Manufacture of Grain and Vegetable Mill Products Except Rice and Corn',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1063',
            'group_code'            => '106',
            'class_description'     => 'Manufacture of Starches and Starch Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1071',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Bakery Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1072',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Sugar',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1073',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Cocoa, Chocolate and Sugar Confectionery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1074',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Macaroni, Noodles, Couscous and Similar Farinaceous Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1075',
            'group_code'            => '107',
            'class_description'     => 'Manufactured of Prepared Meals and Dishes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1076',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Food Supplements from Herbs and Other Plants',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1077',
            'group_code'            => '107',
            'class_description'     => 'Coffee Roasting and Processing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1079',
            'group_code'            => '107',
            'class_description'     => 'Manufacture of Other Food Products, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1080',
            'group_code'            => '108',
            'class_description'     => 'Manufacture of Prepared Animal Feeds',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1101',
            'group_code'            => '110',
            'class_description'     => 'Distilling, Rectifying and Blending of Spirits',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1102',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Wines',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1103',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Malt Liquors and Malt',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1104',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Softdrinks',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1105',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Drinking Water and Mineral Water',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1106',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Sports and Energy Drink',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1109',
            'group_code'            => '110',
            'class_description'     => 'Manufacture of Other Beverages, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1201',
            'group_code'            => '120',
            'class_description'     => 'Manufacture of Cigarettes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1202',
            'group_code'            => '120',
            'class_description'     => 'Manufacture of Cigars',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1203',
            'group_code'            => '120',
            'class_description'     => 'Manufacture of Chewing and Smoking Tobacco, Snuff',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1204',
            'group_code'            => '120',
            'class_description'     => 'Curing and Redrying Tobacco Leaves',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1209',
            'group_code'            => '120',
            'class_description'     => 'Tobacco Manufacturing, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1311',
            'group_code'            => '131',
            'class_description'     => 'Preparation and Spinning of Textile Fibers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1312',
            'group_code'            => '131',
            'class_description'     => 'Weaving of Textiles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1313',
            'group_code'            => '131',
            'class_description'     => 'Finishing of Textiles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1314',
            'group_code'            => '131',
            'class_description'     => 'Preparation and Finishing of Textiles (Integrated)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1391',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Knitted and Crocheted Fabrics',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1392',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Made-up Textile Articles, Except Wearing Apparel',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1393',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Carpet and Rugs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1394',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Cordage, Rope, Twine and Netting',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1395',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Embroidered Fabrics',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1399',
            'group_code'            => '139',
            'class_description'     => 'Manufacture of Other Textiles, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1411',
            'group_code'            => '141',
            'class_description'     => "Men's and Boys' Garment Manufacturing",
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1412',
            'group_code'            => '141',
            'class_description'     => "Women's and Girls' and Babies' Garment Manufacturing",
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1413',
            'group_code'            => '141',
            'class_description'     => 'Ready-made Embroidered Garments Manufacturing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1419',
            'group_code'            => '141',
            'class_description'     => 'Manufacture of Wearing Apparel, N.e.c',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1421',
            'group_code'            => '142',
            'class_description'     => 'Custom Tailoring',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1422',
            'group_code'            => '142',
            'class_description'     => 'Custom Dressmaking',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1430',
            'group_code'            => '143',
            'class_description'     => 'Manufacture of Knitted and Crocheted Apparel',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1440',
            'group_code'            => '144',
            'class_description'     => 'Manufacture of Articles of Fur',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1511',
            'group_code'            => '151',
            'class_description'     => 'Tanning and Dressing of Leather',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1512',
            'group_code'            => '151',
            'class_description'     => 'Manufacture of Products of Leather and Imitation Leather',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1521',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Leather Shoes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1522',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Rubber Shoes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1523',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Plastic Shoes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1524',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Shoes Made of Textile Materials with Applied Soles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1525',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Wooden Footwear and Accessories',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1529',
            'group_code'            => '152',
            'class_description'     => 'Manufacture of Footwear, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1610',
            'group_code'            => '161',
            'class_description'     => 'Sawmilling and Planing of Wood',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1621',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Veneer Sheets; Manufacture of Plywood, Laminboard, Particle Board and Other Panels and Board',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1622',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Wooden Window and Door Screens, Shades and Venetian Blinds',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1623',
            'group_code'            => '162',
            'class_description'     => "Manufacture of Other Builders' Carpentry and Joinery; Millworking",
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1624',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Wooden Containers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1625',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Wood Carvings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1626',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Charcoal Outside the Forest',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1627',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Wooden Wares',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1628',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Products of Bamboo, Cane, Rattan and the Like, and Plaiting Materials Except Furniture',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1629',
            'group_code'            => '162',
            'class_description'     => 'Manufacture of Other Products of Wood; Manufacture of Articles of Cork and Plaiting Materials, Except Furniture, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1701',
            'group_code'            => '170',
            'class_description'     => 'Manufacture of Pulp, Paper and Paperboard',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1702',
            'group_code'            => '170',
            'class_description'     => 'Manufacture of Corrugated Paper and Paperboard and of Containers of Paper and Paperboard',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1709',
            'group_code'            => '170',
            'class_description'     => 'Manufacture of Other Articles of Paper and Paperboard',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1811',
            'group_code'            => '181',
            'class_description'     => 'Printing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1812',
            'group_code'            => '181',
            'class_description'     => 'Service Activities Related to Printing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1820',
            'group_code'            => '182',
            'class_description'     => 'Reproduction of Recorded Media',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1910',
            'group_code'            => '191',
            'class_description'     => 'Manufacture of Coke Oven Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1920',
            'group_code'            => '192',
            'class_description'     => 'Manufacture of Refined Petroleum Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '1990',
            'group_code'            => '199',
            'class_description'     => 'Manufacture of Other Fuel Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2011',
            'group_code'            => '201',
            'class_description'     => 'Manufacture of Basic Chemicals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2012',
            'group_code'            => '201',
            'class_description'     => 'Manufacture of Fertilizers and Nitrogen Compounds',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2013',
            'group_code'            => '201',
            'class_description'     => 'Manufacture of Plastics and Synthetic Rubber in Primary Forms',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2021',
            'group_code'            => '202',
            'class_description'     => 'Manufacture of Pesticides and Other Agro-chemical Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2022',
            'group_code'            => '202',
            'class_description'     => 'Manufacture of Paints, Varnishes and Similar Coatings, Printing Ink and Mastics',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2023',
            'group_code'            => '202',
            'class_description'     => 'Manufacture of Soap and Detergents, Cleaning and Polishing Preparations, Perfumes and Toilet Preparations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2029',
            'group_code'            => '202',
            'class_description'     => 'Manufacture of Other Chemical Products, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2030',
            'group_code'            => '203',
            'class_description'     => 'Manufacture of Man-made Fibers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2100',
            'group_code'            => '210',
            'class_description'     => 'Manufacture of Pharmaceuticals, Medicinal Chemical and Botanical Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2211',
            'group_code'            => '221',
            'class_description'     => 'Manufacture of Rubber Tires and Tubes; Retreading and Rebuilding of Rubber Tires',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2219',
            'group_code'            => '221',
            'class_description'     => 'Manufacture of Other Rubber Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2220',
            'group_code'            => '222',
            'class_description'     => 'Manufacture of Plastics Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2310',
            'group_code'            => '231',
            'class_description'     => 'Manufacture of Glass and Glass Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2391',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Refractory Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2392',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Clay Building Materials',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2393',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Other Porcelain and Ceramic Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2394',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Cement',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2395',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Lime and Plaster',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2396',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Articles of Concrete, Cement and Plaster',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2397',
            'group_code'            => '239',
            'class_description'     => 'Cutting, Shaping and Finishing of Stone',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2399',
            'group_code'            => '239',
            'class_description'     => 'Manufacture of Other Non-metallic Mineral Products, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2411',
            'group_code'            => '241',
            'class_description'     => 'Operation of Blast Furnaces and Steel Making Furnaces',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2412',
            'group_code'            => '241',
            'class_description'     => 'Operation of Steel Works and Rolling Mills',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2421',
            'group_code'            => '242',
            'class_description'     => 'Gold and Other Precious Metal Refining',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2422',
            'group_code'            => '242',
            'class_description'     => 'Non-ferrous Smelting and Refining , Except Precious Metals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2423',
            'group_code'            => '242',
            'class_description'     => 'Non-ferrous Rolling, Drawing and Extrusion Mills',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2424',
            'group_code'            => '242',
            'class_description'     => 'Manufacture of Pipe Fittings of Non-ferrous Metals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2429',
            'group_code'            => '242',
            'class_description'     => 'Manufacture of Basic Precious and Other Non-ferrous Metals, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2431',
            'group_code'            => '243',
            'class_description'     => 'Casting of Iron and Steel',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2432',
            'group_code'            => '243',
            'class_description'     => 'Casting of Non-ferrous Metals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2511',
            'group_code'            => '251',
            'class_description'     => 'Manufacture of Structural Metal Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2512',
            'group_code'            => '251',
            'class_description'     => 'Manufacture of Tanks, Reservoirs and Containers of Metal',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2513',
            'group_code'            => '251',
            'class_description'     => 'Manufacture of Steam Generators, Except Central Heating Hot Water Boilers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2520',
            'group_code'            => '252',
            'class_description'     => 'Manufacture of Weapons and Ammunition',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2591',
            'group_code'            => '259',
            'class_description'     => 'Forging, Pressing, Stamping and Roll-forming of Metal; Powder Metallurgy',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2592',
            'group_code'            => '259',
            'class_description'     => 'Treatment and Coating of Metals; Machining',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2593',
            'group_code'            => '259',
            'class_description'     => 'Manufacture of Cutlery, Hand Tools and General Hardware',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2599',
            'group_code'            => '259',
            'class_description'     => 'Manufacture of Other Fabricated Metal Products, N.e.c.',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2611',
            'group_code'            => '261',
            'class_description'     => 'Manufacture of Electronic Valves and Tubes',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2612',
            'group_code'            => '261',
            'class_description'     => 'Manufacture of Semi-conductor Devices and Other Electronic Components',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2620',
            'group_code'            => '262',
            'class_description'     => 'Manufacture of Computers and Peripheral Equipment and Accessories',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2630',
            'group_code'            => '263',
            'class_description'     => 'Manufacture of Communication Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2640',
            'group_code'            => '264',
            'class_description'     => 'Manufacture of Consumer Electronics',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2651',
            'group_code'            => '265',
            'class_description'     => 'Manufacture of Measuring, Testing, Navigating and Control Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2652',
            'group_code'            => '265',
            'class_description'     => 'Manufacture of Watches and Clocks',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2660',
            'group_code'            => '266',
            'class_description'     => 'Manufacture of Irradiation, Electromedical and Electrotherapeutic Equipment',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2670',
            'group_code'            => '267',
            'class_description'     => 'Manufacture of Optical Instruments and Photographic Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2680',
            'group_code'            => '268',
            'class_description'     => 'Manufacture of Magnetic and Optical Media',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2711',
            'group_code'            => '271',
            'class_description'     => 'Manufacture of Electric Motors, Generators, Transformers and Electric Generating Sets',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2712',
            'group_code'            => '271',
            'class_description'     => 'Manufacture of Electricity Distribution and Control Apparatus',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2720',
            'group_code'            => '272',
            'class_description'     => 'Manufacture of Batteries and Accumulators',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2731',
            'group_code'            => '273',
            'class_description'     => 'Manufacture of Fiber Optic Cables',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2732',
            'group_code'            => '273',
            'class_description'     => 'Manufacture of Other Electronic and Electric Wires and Cables',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2733',
            'group_code'            => '273',
            'class_description'     => 'Manufacture of Wiring Devices',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2740',
            'group_code'            => '274',
            'class_description'     => 'Manufacture of Electric Lighting Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2750',
            'group_code'            => '275',
            'class_description'     => 'Manufacture of Domestic Appliances',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2790',
            'group_code'            => '279',
            'class_description'     => 'Manufacture of Other Electrical Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2811',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Engines and Turbines, Except Aircraft, Vehicle and Cycle Engines',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2812',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Fluid Power Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2813',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Other Pumps, Compressors, Taps and Valves',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2814',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Bearings, Gears and Driving Elements',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2815',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Ovens, Furnaces and Furnace Burners',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2816',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Lifting and Handling Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2817',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Office Machinery and Equipment (Except Computers and Peripheral Equipment)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2818',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Power-driven Hand Tools',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2819',
            'group_code'            => '281',
            'class_description'     => 'Manufacture of Other General-purpose Machinery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2821',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Agricultural and Forestry Machinery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2822',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Metal-forming Machinery and Machine Tools',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2823',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Machinery for Metallurgy',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2824',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Machinery for Mining, Quarrying and Construction',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2825',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Machinery for Food Beverage and Tobacco Processing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2826',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Machinery for Textile, Apparel and Leather Production',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2829',
            'group_code'            => '282',
            'class_description'     => 'Manufacture of Other Special-purpose Machinery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2910',
            'group_code'            => '291',
            'class_description'     => 'Manufacture of Motor Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2920',
            'group_code'            => '292',
            'class_description'     => 'Manufacture of Bodies (Coachwork) for Motor Vehicles; Manufacture of Trailers and Semi-trailers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '2930',
            'group_code'            => '293',
            'class_description'     => 'Manufacture of Parts and Accessories for Motor Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3011',
            'group_code'            => '301',
            'class_description'     => 'Building of Ships and Floating Structures',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3012',
            'group_code'            => '301',
            'class_description'     => 'Building of Pleasure and Sporting Boats',
        ]);
        

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3020',
            'group_code'            => '302',
            'class_description'     => 'Manufacture of Railway Locomotive and Rolling Stock',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3030',
            'group_code'            => '303',
            'class_description'     => 'Manufacture of Air and Spacecraft and Related Machinery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3040',
            'group_code'            => '304',
            'class_description'     => 'Manufacture of Military Fighting Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3091',
            'group_code'            => '309',
            'class_description'     => 'Manufacture of Motorcyles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3092',
            'group_code'            => '309',
            'class_description'     => 'Manufacture of Bicycles and Invalid Carriages',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3099',
            'group_code'            => '309',
            'class_description'     => 'Manufacture of Other Transport Equipment, N.e.c',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3101',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Wood Furniture',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3102',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Rattan Furniture (Reed, Wicker, and Cane)',
        ]);
        
        DB::table('tblindustry_class')->insert([
            'class_code'            => '3103',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Box Beds and Mattresses',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3104',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Partitions, Shelves, Lockers and Office and Store Fixtures',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3105',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Plastic Furniture',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3106',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Furniture and Fixtures of Metal',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3109',
            'group_code'            => '310',
            'class_description'     => 'Manufacture of Other Furniture and Fixtures, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3211',
            'group_code'            => '321',
            'class_description'     => 'Manufacture of Jewelry and Related Articles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3212',
            'group_code'            => '321',
            'class_description'     => 'Manufacture of Imitation of Jewelry and Related Articles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3220',
            'group_code'            => '322',
            'class_description'     => 'Manufacture of Musical Instruments',
        ]);
        
        DB::table('tblindustry_class')->insert([
            'class_code'            => '3230',
            'group_code'            => '323',
            'class_description'     => 'Manufacture of Sports Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3240',
            'group_code'            => '324',
            'class_description'     => 'Manufacture of Games and Toys',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3250',
            'group_code'            => '325',
            'class_description'     => 'Manufacture of Medical and Dental Instruments and Supplies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3291',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Pens and Pencils of All Kinds',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3292',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Umbrellas, Walking Sticks, Canes, Whips and Riding Crops',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3293',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Articles for Personal Use, E.g. Smoking Pipes, Combs, Slides and Similar Articles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3294',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Candles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3295',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Artificial Flowers, Fruits and Foliage',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3296',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Burial Coffin',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3299',
            'group_code'            => '329',
            'class_description'     => 'Manufacture of Other Miscellaneous Articles, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3311',
            'group_code'            => '331',
            'class_description'     => 'Repair of Fabricated Metal Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3312',
            'group_code'            => '331',
            'class_description'     => 'Repair of Machinery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3313',
            'group_code'            => '331',
            'class_description'     => 'Repair of Electronic and Optical Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3314',
            'group_code'            => '331',
            'class_description'     => 'Repair of Electrical Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3315',
            'group_code'            => '331',
            'class_description'     => 'Repair of Transport Equipment, Except Motor Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3319',
            'group_code'            => '331',
            'class_description'     => 'Repair of Other Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3320',
            'group_code'            => '332',
            'class_description'     => 'Installation of Industrial Machinery and Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3510',
            'group_code'            => '351',
            'class_description'     => 'Electric Power Generation, Transmission and Distribution',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3520',
            'group_code'            => '352',
            'class_description'     => 'Manufacture of Gas; Distribution of Gaseous Fuels Through Mains',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3530',
            'group_code'            => '353',
            'class_description'     => 'Steam, Air Conditioning Supply and Production of Ice',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3600',
            'group_code'            => '360',
            'class_description'     => 'Water Collection, Treatment and Supply',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3700',
            'group_code'            => '370',
            'class_description'     => 'Sewerage',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3811',
            'group_code'            => '381',
            'class_description'     => 'Collection of Non-hazardous Waste',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3812',
            'group_code'            => '381',
            'class_description'     => 'Collection of Hazardous Waste',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3821',
            'group_code'            => '382',
            'class_description'     => 'Treatment and Disposal of Non-hazardous Waste',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3822',
            'group_code'            => '382',
            'class_description'     => 'Treatment and Disposal of Hazardous Waste',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3830',
            'group_code'            => '383',
            'class_description'     => 'Materials Recovery',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '3900',
            'group_code'            => '390',
            'class_description'     => 'Remediation Activities and Other Waste Management Services',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4100',
            'group_code'            => '410',
            'class_description'     => 'Construction of Buildings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4210',
            'group_code'            => '421',
            'class_description'     => 'Construction of Roads and Railways',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4220',
            'group_code'            => '422',
            'class_description'     => 'Construction of Utility Projects',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4290',
            'group_code'            => '429',
            'class_description'     => 'Construction of Other Civil Engineering Projects',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4311',
            'group_code'            => '431',
            'class_description'     => 'Demolition',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4312',
            'group_code'            => '431',
            'class_description'     => 'Site Preparation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4321',
            'group_code'            => '432',
            'class_description'     => 'Electrical Installation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4322',
            'group_code'            => '432',
            'class_description'     => 'Plumbing, Heat and Air-conditioning Installation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4329',
            'group_code'            => '432',
            'class_description'     => 'Other Construction Installation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4330',
            'group_code'            => '433',
            'class_description'     => 'Building Completion and Finishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4390',
            'group_code'            => '439',
            'class_description'     => 'Other Specialized Construction Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4510',
            'group_code'            => '451',
            'class_description'     => 'Sale of Motor Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4520',
            'group_code'            => '452',
            'class_description'     => 'Maintenance and Repair of Motor Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4530',
            'group_code'            => '453',
            'class_description'     => 'Sale of Motor Vehicle Parts and Accessories',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4540',
            'group_code'            => '454',
            'class_description'     => 'Sale, Maintenance and Repair of Motorcycles and Related Parts and Accessories',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4610',
            'group_code'            => '461',
            'class_description'     => 'Wholesale on a Fee or Contract Basis',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4620',
            'group_code'            => '462',
            'class_description'     => 'Wholesale of Agricultural Raw Materials and Live Animals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4630',
            'group_code'            => '463',
            'class_description'     => 'Wholesale of Food, Beverages and Tobacco',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4641',
            'group_code'            => '464',
            'class_description'     => 'Wholesale of Textiles, Clothing and Footwear',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4642',
            'group_code'            => '464',
            'class_description'     => 'Wholesale of Miscellaneous Consumer Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4649',
            'group_code'            => '464',
            'class_description'     => 'Wholesale of Other Household Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4651',
            'group_code'            => '465',
            'class_description'     => 'Wholesale of Computers, Computer Peripheral Equipment and Software',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4652',
            'group_code'            => '465',
            'class_description'     => 'Wholesale of Electronic and Telecommunications Equipment and Parts',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4653',
            'group_code'            => '465',
            'class_description'     => 'Wholesale of Agricultural Machinery, Equipment and Supplies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4659',
            'group_code'            => '465',
            'class_description'     => 'Wholesale of Other Machinery and Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4661',
            'group_code'            => '466',
            'class_description'     => 'Wholesale of Solid, Liquid and Gaseous Fuels and Related Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4662',
            'group_code'            => '466',
            'class_description'     => 'Wholesale of Metals and Metal Ores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4663',
            'group_code'            => '466',
            'class_description'     => 'Wholesale of Construction Materials, Hardware, Plumbing and Heating Equipment and Supplies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4669',
            'group_code'            => '466',
            'class_description'     => 'Wholesale of Waste and Scrap and Other Products, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4690',
            'group_code'            => '469',
            'class_description'     => 'Non-specialized Wholesale Trade',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4711',
            'group_code'            => '471',
            'class_description'     => 'Retail Sale in Non-specialized Stores with Food, Beverages or Tobacco Predominating',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4719',
            'group_code'            => '471',
            'class_description'     => 'Other Retail Sale in Non-specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4721',
            'group_code'            => '472',
            'class_description'     => 'Retail Sale of Food in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4722',
            'group_code'            => '472',
            'class_description'     => 'Retail Sale of Beverages in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4723',
            'group_code'            => '472',
            'class_description'     => 'Retail Sale of Tobacco Products in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4730',
            'group_code'            => '473',
            'class_description'     => 'Retail Sale of Automotive Fuel in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4741',
            'group_code'            => '474',
            'class_description'     => 'Retail Sale of Computers, Peripheral Units, Software and Telecommunications Equipment in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4742',
            'group_code'            => '474',
            'class_description'     => 'Retail Sale of Audio and Video Equipment in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4751',
            'group_code'            => '475',
            'class_description'     => 'Retail Sale of Textiles in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4752',
            'group_code'            => '475',
            'class_description'     => 'Retail Sale of Hardware, Paints and Glass in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4753',
            'group_code'            => '475',
            'class_description'     => 'Retail Sale of Carpets, Rugs, Wall and Floor Coverings in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4759',
            'group_code'            => '475',
            'class_description'     => 'Retail Sale of Electrical Household Appliances, Furniture, Lighting Equipment and Other Household Articles in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4761',
            'group_code'            => '476',
            'class_description'     => 'Retail Sale of Books, Newspapers and Stationery in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4762',
            'group_code'            => '476',
            'class_description'     => 'Retail Sale of Music and Video Recordings in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4763',
            'group_code'            => '476',
            'class_description'     => 'Retail Sale of Sporting Equipment in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4764',
            'group_code'            => '476',
            'class_description'     => 'Retail Sale of Games and Toys in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4771',
            'group_code'            => '477',
            'class_description'     => 'Retail Sale of Clothing, Footwear and Leather Articles in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4772',
            'group_code'            => '477',
            'class_description'     => 'Retail Sale of Pharmaceutical and Medical Goods, Cosmetic and Toilet Articles in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4773',
            'group_code'            => '477',
            'class_description'     => 'Other Retail Sale of New Goods in Specialized Stores',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4774',
            'group_code'            => '477',
            'class_description'     => 'Retail Sale of Second-hand Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4775',
            'group_code'            => '477',
            'class_description'     => 'Retail Sale of Liquefied Petroleum Gas and Other Fuel Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4781',
            'group_code'            => '478',
            'class_description'     => 'Retail Sale Via Stalls and Markets of Food, Beverages and Tobacco Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4782',
            'group_code'            => '478',
            'class_description'     => 'Retail Sale Via Stalls and Markets of Textiles, Clothing and Footwear',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4789',
            'group_code'            => '478',
            'class_description'     => 'Retail Sale Via Stalls and Markets of Other Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4791',
            'group_code'            => '479',
            'class_description'     => 'Retail Sale Via Mail/telephone Order Houses or Via Internet',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4799',
            'group_code'            => '479',
            'class_description'     => 'Other Retail Sale Not in Stores, Stalls or Markets',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4911',
            'group_code'            => '491',
            'class_description'     => 'Passenger Rail Transport, Inter-urban',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4912',
            'group_code'            => '491',
            'class_description'     => 'Freight Rail Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4920',
            'group_code'            => '492',
            'class_description'     => 'Transport Via Buses',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4931',
            'group_code'            => '493',
            'class_description'     => 'Urban or Suburban Passenger Land Transport, Except Railway or Bus',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4932',
            'group_code'            => '493',
            'class_description'     => 'Other Passenger Land Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4933',
            'group_code'            => '493',
            'class_description'     => 'Freight Transport by Road',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '4940',
            'group_code'            => '494',
            'class_description'     => 'Transport Via Pipeline',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5011',
            'group_code'            => '501',
            'class_description'     => 'Sea and Coastal Passenger Water Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5012',
            'group_code'            => '501',
            'class_description'     => 'Sea and Coastal Freight Water Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5021',
            'group_code'            => '502',
            'class_description'     => 'Inland Passenger Water Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5022',
            'group_code'            => '502',
            'class_description'     => 'Inland Freight Water Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5110',
            'group_code'            => '511',
            'class_description'     => 'Passenger Air Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5120',
            'group_code'            => '512',
            'class_description'     => 'Freight Air Transport',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '521',
            'group_code'            => '521',
            'class_description'     => 'Warehousing and Storage',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5221',
            'group_code'            => '522',
            'class_description'     => 'Service Activities Incidental to Land Transportation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5222',
            'group_code'            => '522',
            'class_description'     => 'Service Activities Incidental to Water Transportation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5223',
            'group_code'            => '522',
            'class_description'     => 'Service Activities Incidental to Air Transportation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5224',
            'group_code'            => '522',
            'class_description'     => 'Cargo Handling',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5229',
            'group_code'            => '522',
            'class_description'     => 'Other Transportation Support Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5310',
            'group_code'            => '531',
            'class_description'     => 'Postal Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5320',
            'group_code'            => '532',
            'class_description'     => 'Courier Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5510',
            'group_code'            => '551',
            'class_description'     => 'Short Term Acommodation Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5590',
            'group_code'            => '559',
            'class_description'     => 'Other Accommodation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5610',
            'group_code'            => '561',
            'class_description'     => 'Restaurants and Mobile Food Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5621',
            'group_code'            => '562',
            'class_description'     => 'Event Catering',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5629',
            'group_code'            => '562',
            'class_description'     => 'Other Food Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5630',
            'group_code'            => '563',
            'class_description'     => 'Beverage Serving Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5811',
            'group_code'            => '581',
            'class_description'     => 'Book Publishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5812',
            'group_code'            => '581',
            'class_description'     => 'Publishing of Directories and Mailing Lists',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5813',
            'group_code'            => '581',
            'class_description'     => 'Publishing of Newspapers, Journals and Periodicals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5819',
            'group_code'            => '581',
            'class_description'     => 'Other Publishing Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5820',
            'group_code'            => '582',
            'class_description'     => 'Software Publishing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5911',
            'group_code'            => '591',
            'class_description'     => 'Motion Picture, Video and Television Programme Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5912',
            'group_code'            => '591',
            'class_description'     => 'Motion Picture, Video and Television Programme Post-production Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5913',
            'group_code'            => '591',
            'class_description'     => 'Motion Picture, Video and Television Programme Distribution Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5914',
            'group_code'            => '591',
            'class_description'     => 'Motion Picture Projection Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '5920',
            'group_code'            => '592',
            'class_description'     => 'Sound Recording and Music Publishing Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6010',
            'group_code'            => '601',
            'class_description'     => 'Radio Broadcasting',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6020',
            'group_code'            => '602',
            'class_description'     => 'Television Programming and Broadcasting Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6110',
            'group_code'            => '611',
            'class_description'     => 'Wired Telecommunications Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6120',
            'group_code'            => '612',
            'class_description'     => 'Wireless Telecommunications Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6130',
            'group_code'            => '613',
            'class_description'     => 'Satellite Telecommunications Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6190',
            'group_code'            => '619',
            'class_description'     => 'Other Telecommunications Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6201',
            'group_code'            => '620',
            'class_description'     => 'Computer Programming Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6202',
            'group_code'            => '620',
            'class_description'     => 'Computer Consultancy and Computer Facilities Management Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6209',
            'group_code'            => '620',
            'class_description'     => 'Other Information Technology and Computer Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6311',
            'group_code'            => '631',
            'class_description'     => 'Data Processing, Hosting and Related Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6312',
            'group_code'            => '631',
            'class_description'     => 'Web Portals',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6391',
            'group_code'            => '639',
            'class_description'     => 'News Agency Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6399',
            'group_code'            => '639',
            'class_description'     => 'Other Information Service Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6411',
            'group_code'            => '641',
            'class_description'     => 'Central Banking',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6419',
            'group_code'            => '641',
            'class_description'     => 'Other Monetary Intermediation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6420',
            'group_code'            => '642',
            'class_description'     => 'Activities of Holding Companies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6430',
            'group_code'            => '643',
            'class_description'     => 'Trusts, Funds and Other Financial Vehicles',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6491',
            'group_code'            => '649',
            'class_description'     => 'Financial Leasing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6492',
            'group_code'            => '649',
            'class_description'     => 'Other Credit Granting',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6493',
            'group_code'            => '649',
            'class_description'     => 'Pawnshop Operations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6499',
            'group_code'            => '649',
            'class_description'     => 'Other Financial Service Activities, Except Insurance and Pension Funding Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6511',
            'group_code'            => '651',
            'class_description'     => 'Life Insurance',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6512',
            'group_code'            => '651',
            'class_description'     => 'Non-life Insurance',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6520',
            'group_code'            => '652',
            'class_description'     => 'Reinsurance',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6530',
            'group_code'            => '653',
            'class_description'     => 'Pension Funding',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6611',
            'group_code'            => '661',
            'class_description'     => 'Administration of Financial Markets',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6612',
            'group_code'            => '661',
            'class_description'     => 'Security and Commodity Contracts Brokerage',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6613',
            'group_code'            => '661',
            'class_description'     => 'Foreign Exchange Dealing',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6619',
            'group_code'            => '661',
            'class_description'     => 'Other Activities Auxiliary to Financial Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6621',
            'group_code'            => '662',
            'class_description'     => 'Risk and Damage Evaluation',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6622',
            'group_code'            => '662',
            'class_description'     => 'Activities of Insurance Agents and Brokers',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6623',
            'group_code'            => '662',
            'class_description'     => 'Pre-need Plan Acitivities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6629',
            'group_code'            => '662',
            'class_description'     => 'Other Activities Auxilary to Insurance and Pension Funding',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6630',
            'group_code'            => '663',
            'class_description'     => 'Fund Management Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6811',
            'group_code'            => '681',
            'class_description'     => 'Real Estate Buying, Selling, Renting, Leasing and Operating of Self-owned/leased Apartment Buildings, Non-residential and Dwellings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6812',
            'group_code'            => '681',
            'class_description'     => 'Real Estate Buying, Developing, Subdividing and Selling',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6813',
            'group_code'            => '681',
            'class_description'     => 'Cemetery and Columbarium Development, Selling, Renting, Leasing and Operating of Self-owned Cemetery/columbarium (Including Burial Crypt)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6814',
            'group_code'            => '681',
            'class_description'     => 'Renting or Leasing Services of Residential Properties',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6819',
            'group_code'            => '681',
            'class_description'     => 'Other Real Estate Activities with Own or Leased Property',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6820',
            'group_code'            => '682',
            'class_description'     => 'Real Estate Activities on a Fee or Contract Basis',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6910',
            'group_code'            => '691',
            'class_description'     => 'Legal Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '6920',
            'group_code'            => '692',
            'class_description'     => 'Accounting, Bookkeeping and Auditing Activities; Tax Consultancy',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7010',
            'group_code'            => '701',
            'class_description'     => 'Activities of Head Offices',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7020',
            'group_code'            => '702',
            'class_description'     => 'Management Consultancy Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7110',
            'group_code'            => '711',
            'class_description'     => 'Architectural and Engineering Activities and Related Technical Consultancy',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7120',
            'group_code'            => '712',
            'class_description'     => 'Technical Testing and Analysis',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7210',
            'group_code'            => '721',
            'class_description'     => 'Research and Experimental Development on Natural Sciences and Engineering',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7220',
            'group_code'            => '722',
            'class_description'     => 'Research and Experimental Development on Social Sciences and Humanities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7230',
            'group_code'            => '723',
            'class_description'     => 'Research and Experimental Development in Information Technology',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7310',
            'group_code'            => '731',
            'class_description'     => 'Advertising',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7320',
            'group_code'            => '732',
            'class_description'     => 'Market Research and Public Opinion Polling',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7410',
            'group_code'            => '741',
            'class_description'     => 'Specialized Design Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7420',
            'group_code'            => '742',
            'class_description'     => 'Photographic Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7490',
            'group_code'            => '749',
            'class_description'     => 'Other Professional, Scientific and Technical Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7500',
            'group_code'            => '750',
            'class_description'     => 'Veterinary Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7710',
            'group_code'            => '771',
            'class_description'     => 'Renting and Leasing of Motor Vehicles (Except Motorcycle, Caravans, Campers)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7721',
            'group_code'            => '772',
            'class_description'     => 'Renting and Leasing of Recreational and Sports Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7722',
            'group_code'            => '772',
            'class_description'     => 'Renting of Video Tapes and Disks',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7729',
            'group_code'            => '772',
            'class_description'     => 'Renting and Leasing of Other Personal and Household Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7730',
            'group_code'            => '773',
            'class_description'     => 'Renting and Leasing of Other Machinery, Equipment and Tangible Goods, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7740',
            'group_code'            => '774',
            'class_description'     => 'Leasing of Intellectual Property and Similar Products, Except Copyrighted Works',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7810',
            'group_code'            => '781',
            'class_description'     => 'Activities of Employment Placement Agencies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7820',
            'group_code'            => '782',
            'class_description'     => 'Temporary Employment Agency Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7830',
            'group_code'            => '783',
            'class_description'     => 'Other Human Resources Provision',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7911',
            'group_code'            => '791',
            'class_description'     => 'Travel Agency Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7912',
            'group_code'            => '791',
            'class_description'     => 'Tour Operator Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '7990',
            'group_code'            => '799',
            'class_description'     => 'Other Reservation Service and Related Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8010',
            'group_code'            => '801',
            'class_description'     => 'Private Security Activites',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8020',
            'group_code'            => '802',
            'class_description'     => 'Security Systems Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8030',
            'group_code'            => '803',
            'class_description'     => 'Investigation Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8110',
            'group_code'            => '811',
            'class_description'     => 'Combined Facilities Support Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8121',
            'group_code'            => '812',
            'class_description'     => 'General Cleaning of Buildings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8129',
            'group_code'            => '812',
            'class_description'     => 'Other Building and Industrial Cleaning Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8130',
            'group_code'            => '813',
            'class_description'     => 'Landscape Care and Maintenance Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8211',
            'group_code'            => '821',
            'class_description'     => 'Combined Office Administrative Service Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8219',
            'group_code'            => '821',
            'class_description'     => 'Photocopying, Document Preparation and Other Specialized Office Support Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8221',
            'group_code'            => '822',
            'class_description'     => 'Call Centers Activities (Voice)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8222',
            'group_code'            => '822',
            'class_description'     => 'Back-office Operations Activities (Non-voice)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8229',
            'group_code'            => '822',
            'class_description'     => 'Other Non-voice Related Activities',
        ]);
        DB::table('tblindustry_class')->insert([
            'class_code'            => '8230',
            'group_code'            => '823',
            'class_description'     => 'Organization of Conventions and Trade Shows',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8291',
            'group_code'            => '829',
            'class_description'     => 'Activities of Collection Agencies and Credit Bureaus',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8292',
            'group_code'            => '829',
            'class_description'     => 'Packaging Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8299',
            'group_code'            => '829',
            'class_description'     => 'Other Business Support Service Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8411',
            'group_code'            => '841',
            'class_description'     => 'General Public Administration Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8412',
            'group_code'            => '841',
            'class_description'     => 'Regulation of the Activities of Providing Health Care, Education, Cultural Services and Other Social Services, Excluding Social Security',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8413',
            'group_code'            => '841',
            'class_description'     => 'Regulation of and Contribution to More Efficient Operation of Businesses',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8421',
            'group_code'            => '842',
            'class_description'     => 'Foreign Affairs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8422',
            'group_code'            => '842',
            'class_description'     => 'Defense Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8423',
            'group_code'            => '842',
            'class_description'     => 'Public Order and Safety Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8430',
            'group_code'            => '843',
            'class_description'     => 'Compulsory Social Security Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8511',
            'group_code'            => '851',
            'class_description'     => 'Pre-primary/pre-school Education (For Children without Special Needs)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8512',
            'group_code'            => '851',
            'class_description'     => 'Pre-primary Education for Children with Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8521',
            'group_code'            => '852',
            'class_description'     => 'Primary/elementary Education (For Children without Special Needs)',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8522',
            'group_code'            => '852',
            'class_description'     => 'Primary/elementary Education for Children with Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8531',
            'group_code'            => '853',
            'class_description'     => 'General Secondary Education for Children without Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8532',
            'group_code'            => '853',
            'class_description'     => 'General Secondary Education for Children with Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8533',
            'group_code'            => '853',
            'class_description'     => 'Technical and Vocational Secondary Education for Children without Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8534',
            'group_code'            => '853',
            'class_description'     => 'Technical and Vocational Secondary Education for Children with Special Needs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8540',
            'group_code'            => '854',
            'class_description'     => 'Higher Education',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8551',
            'group_code'            => '855',
            'class_description'     => 'Sports and Recreation Education',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8552',
            'group_code'            => '855',
            'class_description'     => 'Cultural Education',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8559',
            'group_code'            => '855',
            'class_description'     => 'Other Education N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8560',
            'group_code'            => '856',
            'class_description'     => 'Educational Support Services',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8611',
            'group_code'            => '861',
            'class_description'     => 'Public Hospitals, Sanitaria and Other Similar Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8612',
            'group_code'            => '861',
            'class_description'     => 'Private Hospitals, Sanitaria and Other Similar Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8621',
            'group_code'            => '862',
            'class_description'     => 'Public Medical, Dental and Other Health Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8622',
            'group_code'            => '862',
            'class_description'     => 'Private Medical, Dental and Other Health Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8690',
            'group_code'            => '869',
            'class_description'     => 'Other Human Health Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8710',
            'group_code'            => '871',
            'class_description'     => 'Residential Nursing Care Facilities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8720',
            'group_code'            => '872',
            'class_description'     => 'Residential Care Activities for Mental Retardation, Mental Health and Substance Abuse',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8730',
            'group_code'            => '873',
            'class_description'     => 'Residential Care Activities for the Elderly and Disabled',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8790',
            'group_code'            => '879',
            'class_description'     => 'Other Residential Care Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8810',
            'group_code'            => '881',
            'class_description'     => 'Social Work Activities without Accommodation for the Elderly and Disabled',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '8890',
            'group_code'            => '889',
            'class_description'     => 'Other Social Work Activities without Accommodation, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9000',
            'group_code'            => '900',
            'class_description'     => 'Creative, Arts and Entertainment Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9101',
            'group_code'            => '910',
            'class_description'     => 'Library and Archives Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9102',
            'group_code'            => '910',
            'class_description'     => 'Museums Activities and Preservation of Historical Sites and Buildings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9103',
            'group_code'            => '910',
            'class_description'     => 'Botanical and Zoological Gardens and Nature Reserves Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9200',
            'group_code'            => '920',
            'class_description'     => 'Gambling and Betting Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9311',
            'group_code'            => '931',
            'class_description'     => 'Operation of Sports Facilities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9312',
            'group_code'            => '931',
            'class_description'     => 'Activities of Sports Clubs',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9319',
            'group_code'            => '931',
            'class_description'     => 'Other Sports Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9321',
            'group_code'            => '932',
            'class_description'     => 'Activities of Amusement Parks and Theme Parks',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9329',
            'group_code'            => '932',
            'class_description'     => 'Other Amusement and Recreation Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9411',
            'group_code'            => '941',
            'class_description'     => 'Activities of Business and Employers Membership Organizations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9412',
            'group_code'            => '941',
            'class_description'     => 'Activities of Professional Membership Organizations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9420',
            'group_code'            => '942',
            'class_description'     => 'Activities of Trade Unions',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9491',
            'group_code'            => '949',
            'class_description'     => 'Activities of Religious Organizations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9492',
            'group_code'            => '949',
            'class_description'     => 'Activities of Political Organizations',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9499',
            'group_code'            => '949',
            'class_description'     => 'Activities of Other Membership Organizations, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9511',
            'group_code'            => '951',
            'class_description'     => 'Repair of Computers and Peripheral Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9512',
            'group_code'            => '951',
            'class_description'     => 'Repair of Communications Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9521',
            'group_code'            => '952',
            'class_description'     => 'Repair of Consumer Electronics',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9522',
            'group_code'            => '952',
            'class_description'     => 'Repair of Household Appliances and Home and Garden Equipment',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9523',
            'group_code'            => '952',
            'class_description'     => 'Repair of Footwear and Leather Goods',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9524',
            'group_code'            => '952',
            'class_description'     => 'Repair of Furniture and Home Furnishings',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9529',
            'group_code'            => '952',
            'class_description'     => 'Repair of Personal and Household Goods, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9610',
            'group_code'            => '961',
            'class_description'     => 'Personal Services for Wellness, Except Sports Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9621',
            'group_code'            => '962',
            'class_description'     => 'Washing and Dry Cleaning of Textile and Fur Products',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9630',
            'group_code'            => '963',
            'class_description'     => 'Funeral and Related Activities',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9640',
            'group_code'            => '964',
            'class_description'     => 'Domestic Services',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9690',
            'group_code'            => '969',
            'class_description'     => 'Other Personal Service Activities, N.e.c.',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9700',
            'group_code'            => '970',
            'class_description'     => 'Activities of Households as Employers of Domestic Personnel',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9810',
            'group_code'            => '981',
            'class_description'     => 'Undifferentiated Goods-producing Activities of Private Households for Own Use',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9820',
            'group_code'            => '982',
            'class_description'     => 'Undifferentiated Services-producing Activities of Private Households for Own Use',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9901',
            'group_code'            => '990',
            'class_description'     => 'Activities of Extra-territorial Organizations and Bodies',
        ]);

        DB::table('tblindustry_class')->insert([
            'class_code'            => '9909',
            'group_code'            => '990',
            'class_description'     => 'Activities of Other International Organizations',
        ]);
    }
}
