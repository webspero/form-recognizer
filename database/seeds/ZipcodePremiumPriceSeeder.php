<?php

use Illuminate\Database\Seeder;

class ZipcodePremiumPriceSeeder extends Seeder
{
	//php artisan db:seed --class=ZipcodePremiumPriceSeeder
	
	public function run()
	{ 
		DB::table('zipcodes')->where('region', 'NCR')->update(['premiumReportPrice' => 750]);
		
		DB::table('zipcodes')->where('major_area', 'Cavite')
							->whereIn('city', array('Imus', 'Carmona', 'Gen. Mariano Alvarez', 'Bacoor'))
							->update(['premiumReportPrice' => 1250]);
		
		DB::table('zipcodes')->where('major_area', 'Bulacan')
							->whereIn('city', array('San Jose del Monte', 'Santa Maria', 'Marilao', 'Bocaue', 'Maycauayan'))
							->update(['premiumReportPrice' => 1250]);

		DB::table('zipcodes')->where('major_area', 'Rizal')
							->whereIn('city', array('Tanay', 'Taytay', 'Binangonan', 'Angono', 'Cainta'))
							->update(['premiumReportPrice' => 1250]);
		
		DB::table('zipcodes')->where('major_area', 'Laguna')
							->whereIn('city', array('Santa Rosa', 'San Pedro', 'Binan'))
							->update(['premiumReportPrice' => 1250]);

		DB::table('zipcodes')->where('major_area', 'Ilocos Norte')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Ilocos Sur')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'La Union')->update(['premiumReportPrice' => 3500]);

		DB::table('zipcodes')->where('major_area', 'Pangasinan')->update(['premiumReportPrice' => 3000]);

		DB::table('zipcodes')->where('major_area', 'Isabela')->update(['premiumReportPrice' => 4000]);
		
		DB::table('zipcodes')->where('major_area', 'Nueva Vizcaya')->update(['premiumReportPrice' => 3500]);

		DB::table('zipcodes')->where('major_area', 'Quirino')->update(['premiumReportPrice' => 3500]);

		DB::table('zipcodes')->where('major_area', 'Benguet')->update(['premiumReportPrice' => 4000]);
		
		DB::table('zipcodes')->where('major_area', 'Aurora')->update(['premiumReportPrice' => 3000]);

		DB::table('zipcodes')->where('major_area', 'Bataan')->update(['premiumReportPrice' => 2500]);

		DB::table('zipcodes')->where('major_area', 'Bulacan')
							->whereNotIn('city', array('San Jose del Monte', 'Santa Maria', 'Marilao', 'Bocaue', 'Maycauayan'))
							->update(['premiumReportPrice' => 1500]);

		DB::table('zipcodes')->where('major_area', 'Nueva Ecija')->update(['premiumReportPrice' => 2500]);

		DB::table('zipcodes')->where('major_area', 'Pampanga')->update(['premiumReportPrice' => 2000]);

		DB::table('zipcodes')->where('major_area', 'Tarlac')->update(['premiumReportPrice' => 2500]);
		
		DB::table('zipcodes')->where('major_area', 'Zambales')->update(['premiumReportPrice' => 3000]);

		DB::table('zipcodes')->where('major_area', 'Cavite')
							->whereNotIn('city', array('Imus', 'Carmona', 'Gen. Mariano Alvarez', 'Bacoor'))
							->update(['premiumReportPrice' => 1800]);

		DB::table('zipcodes')->where('major_area', 'Laguna')
							->whereNotIn('city', array('Santa Rosa', 'San Pedro', 'Binan'))
							->update(['premiumReportPrice' => 1800]);
		
		DB::table('zipcodes')->where('major_area', 'Batangas')->update(['premiumReportPrice' => 2500]);

		DB::table('zipcodes')->where('major_area', 'Rizal')
							->whereIn('city', array('Tanay', 'Taytay', 'Binangonan', 'Angono', 'Cainta'))
							->update(['premiumReportPrice' => 1500]);
		
		DB::table('zipcodes')->where('major_area', 'Quezon')->update(['premiumReportPrice' => 2500]);

		DB::table('zipcodes')->where('major_area', 'Albay')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Camarines Norte')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Camarines Sur')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Catanduanes')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Masbate')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Sorsogon')->update(['premiumReportPrice' => 4000]);

		DB::table('zipcodes')->where('major_area', 'Marinduque')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Mindoro Occidental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Mindoro Oriental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Palawan')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Palawan')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Romblon')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Bohol')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Cebu Province')
							->whereNotIn('city', array('Cordova', 'Liloan', 'Talisay'))
							->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Cebu Province')
							->whereIn('city', array('Cordova', 'Liloan', 'Talisay'))
							->update(['premiumReportPrice' => 3500]);

		DB::table('zipcodes')->where('major_area', 'Siquijor')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Aklan')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Antique')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Capiz')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Guimaras Sub-Province')->update(['premiumReportPrice' => 5000]);

		$guimarasList = DB::table('zipcodes')->where('major_area', 'Guimaras Sub-Province')->whereIn('city', array('San Lorenzo', 'Sibunag'))->get();

		if(count($guimarasList) == 0) {
			DB::table('zipcodes')->insert(
				['country' => 'PH', 'major_area' => 'Guimaras Sub-Province', 'zip_code' => 5047, 'city' => 'San Lorenzo', 'region' => 'Wes. Visayas', 'premiumReportPrice' => 5000, 'isPremiumAvailable' => 1]
			);

			DB::table('zipcodes')->insert(
				['country' => 'PH', 'major_area' => 'Guimaras Sub-Province', 'zip_code' => 5048, 'city' => 'Sibunag', 'region' => 'Wes. Visayas', 'premiumReportPrice' => 5000, 'isPremiumAvailable' => 1]
			);
		}

		DB::table('zipcodes')->where('major_area', 'Iloilo Province')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Biliran Sub-Province')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Eastern Samar')->update(['premiumReportPrice' => 5000]);
		
		DB::table('zipcodes')->where('major_area', 'Leyte')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Northern Samar')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Western Samar')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Southern Leyte')->update(['premiumReportPrice' => 5000]);

		$southLeyteList = DB::table('zipcodes')->where('major_area', 'Southern Leyte')->where('city', 'Limasawa')->get();

		if(count($southLeyteList) == 0) {
			DB::table('zipcodes')->insert(
				['country' => 'PH', 'major_area' => 'Southern Leyte', 'zip_code' => 6618, 'city' => 'Limasawa', 'region' => 'East. Visayas', 'premiumReportPrice' => 5000, 'isPremiumAvailable' => 1]
			);
		}

		DB::table('zipcodes')->where('major_area', 'Negros Occidental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Negros Oriental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Misamis Oriental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Bukidnon')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Davao del Norte')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Davao del Sur')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->where('major_area', 'Davao Oriental')->update(['premiumReportPrice' => 5000]);

		DB::table('zipcodes')->whereNotIn('city', array('Maripipi', 'Arteche', 'Balangkayan', 'Can-Avid', 'Jipapad', 'Maslog', 'Maydulog', 'San Policarpio', 'Almagro',
														'Daram', 'Matuguinao', 'San Jose de Bauan', 'Macrohon', 'Pintuyan', 'San Juan (Cabalian)', 'Tomas Oppus'))
							->update(['isPremiumAvailable' => 1]);

		DB::table('zipcodes')->whereIn('major_area', array('Surigao del Sur', 'Surigao del Norte', 'Agusan del Sur', 'Agusan del Norte', 'Mountain Province',
															'Kalinga-Apayao', 'Ifugao', 'Abra', 'Tawi-Tawi', 'Sulu', 'Maguindanao', 'Lanao del Sur',
															'Basilan', 'Sultan Kudarat', 'South Cotabato', 'Sarangani', 'North Cotabato', 'Compostela Valley',
															'Misamis Occidental', 'Lanao del Norte', 'Camiguin', 'Zamboanga del Sur', 'Zamboanga del Norte')
															)->update(['isPremiumAvailable' => 0]);

		// DB::table('zipcodes')->where('region', 'Ilocos')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'Cagayan Valley')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'Central Luzon')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'CALABARZON')->update(['premiumReportPrice' => 1250]);
		// DB::table('zipcodes')->where('region', 'MIMAROPA')->update(['premiumReportPrice' => 2550]);
		// DB::table('zipcodes')->where('region', 'Bicol')->update(['premiumReportPrice' => 2550]);
		// DB::table('zipcodes')->where('region', 'Wes. Visayas')->update(['premiumReportPrice' => 2550]);
		// DB::table('zipcodes')->where('region', 'Cent. Visayas')->update(['premiumReportPrice' => 2550]);
		// DB::table('zipcodes')->where('region', 'East. Visayas')->update(['premiumReportPrice' => 2550]);
		// DB::table('zipcodes')->where('region', 'Zamboanga')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'N. Mindanao')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'Davao')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'SOCCSKSARGEN')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'ARMM')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'CAR')->update(['premiumReportPrice' => 3050]);
		// DB::table('zipcodes')->where('region', 'Caraga')->update(['premiumReportPrice' => 3050]);
		
	}

}
