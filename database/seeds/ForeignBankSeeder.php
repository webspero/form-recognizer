<?php

use Illuminate\Database\Seeder;

class ForeignBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
    		[
	            'id' => 1,
	            'bank' => 'Bangkok Bank Public Co. Ltd.',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 2,
	            'bank' => 'Bank of America NA',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 3,
	            'bank' => 'Bank of China Ltd. - Manila Branch',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 4,
	            'bank' => 'Cathay United Bank Co., Ltd.- Manila Branch',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 5,
	            'bank' => 'Chang Hwa Commercial Bank, Ltd.- Manila Branch',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 6,
	            'bank' => 'CIMB Bank Philippines Inc.',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 7,
	            'bank' => 'Citibank, N.A.',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
	            'id' => 8,
	            'bank' => 'First Commercial Bank, Ltd.- Manila Branch',
	            'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
        	],
        	[
				'id' => 9,
				'bank' => 'Hua Nan Commercial Bank, Ltd.- Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 10,
				'bank' => 'Industrial and Commercial Bank of China Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 11,
				'bank' => 'Industrial Bank of Korea- Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 12,
				'bank' => 'JP Morgan Chase Bank NA',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 13,
				'bank' => 'KEB Hana Bank- Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 14,
				'bank' => 'Korea Exchange Bank',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 15,
				'bank' => 'Mega International Commercial Bank Co. Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 16,
				'bank' => 'MUFG Bank, Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 17,
				'bank' => 'The Bank of Tokyo-Mitsubishi UFJ, Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 18,
				'bank' => 'Shinhan Bank- Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 19,
				'bank' => 'Sumitomo Mitsui Banking Corp.- Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 20,
				'bank' => 'United Overseas Bank Limited, Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 21,
				'bank' => 'CTBC Bank (Philippines) Corporation',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 22,
				'bank' => 'Maybank Philippines, Incorporated',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 23,
				'bank' => 'Bangkok Bank Public Co. Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 24,
				'bank' => 'Bank of America, N.A.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 25,
				'bank' => 'Bank of China (Hongkong) Limited-Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 26,
				'bank' => 'Cathay United Bank Co., LTD. - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 27,
				'bank' => 'Chang Hwa Commercial Bank, Ltd. - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 28,
				'bank' => 'CIMB Bank Philippines Inc.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 29,
				'bank' => 'Citibank, N.A.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 30,
				'bank' => 'First Commercial Bank, Ltd., Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 31,
				'bank' => 'Hua Nan Commercial Bank, Ltd. - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 32,
				'bank' => 'Industrial and Commercial Bank of China Limited - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 33,
				'bank' => 'Industrial Bank of Korea Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 34,
				'bank' => 'JP Morgan Chase Bank, N.A.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 35,
				'bank' => 'KEB Hana Bank - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 36,
				'bank' => 'Mega International Commercial Bank Co., Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 37,
				'bank' => 'MUFG Bank, Ltd.',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 38,
				'bank' => 'Shinhan Bank - Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 39,
				'bank' => 'Sumitomo Mitsui Banking Corporation-Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			],
			[
				'id' => 40,
				'bank' => 'United Overseas Bank Limited, Manila Branch',
				'created_at' => '2020-05-31 00:00',
	            'updated_at' => '2020-05-31 00:00'
			]
    	];

        DB::table('foreign_banks')->insert($banks);
    }
}
