<?php

use Illuminate\Database\Seeder;

class grdpRegcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grdp')->where('region', 'ARMM')->update(['regCode' => "15"]);

        DB::table('grdp')->where('region', 'Bicol')->update(['regCode' => "05"]);

        DB::table('grdp')->where('region', 'Cagayan Valley')->update(['regCode' => "02"]);

        DB::table('grdp')->where('region', 'CALABARZON')->update(['regCode' => "04"]);

        DB::table('grdp')->where('region', 'CAR')->update(['regCode' => "14"]);

        DB::table('grdp')->where('region', 'Caraga')->update(['regCode' => "16"]);

        DB::table('grdp')->where('region', 'Cent. Visayas')->update(['regCode' => "07"]);

        DB::table('grdp')->where('region', 'Central Luzon')->update(['regCode' => "03"]);

        DB::table('grdp')->where('region', 'Davao')->update(['regCode' => "11"]);

        DB::table('grdp')->where('region', 'East. Visayas')->update(['regCode' => "08"]);

        DB::table('grdp')->where('region', 'Ilocos')->update(['regCode' => "01"]);

        DB::table('grdp')->where('region', 'MIMAROPA')->update(['regCode' => "17"]);

        DB::table('grdp')->where('region', 'N. Mindanao')->update(['regCode' => "10"]);

        DB::table('grdp')->where('region', 'NCR')->update(['regCode' => "13"]);

        DB::table('grdp')->where('region', 'SOCCSKSARGEN')->update(['regCode' => "12"]);

        DB::table('grdp')->where('region', 'Wes. Visayas')->update(['regCode' => "06"]);

        DB::table('grdp')->where('region', 'Zamboanga')->update(['regCode' => "09"]);


    }
}
