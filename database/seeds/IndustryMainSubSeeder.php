<?php

use Illuminate\Database\Seeder;

class IndustryMainSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Table Industry Main */

        DB::table('tblindustry_main')
            ->where('industry_main_id', 1)
            ->update(['main_code' => 'A']);

        DB::table('tblindustry_main')
            ->where('industry_main_id', 2)
            ->update(['main_code' => 'B']);

        DB::table('tblindustry_main')->where('industry_main_id', 3)
            ->update(['main_code' => 'C']);

        DB::table('tblindustry_main')->where('industry_main_id', 4)
            ->update(['main_code' => 'D']);

        DB::table('tblindustry_main')->where('industry_main_id', 5)
            ->update(['main_code' => 'E']);

        DB::table('tblindustry_main')->where('industry_main_id', 6)
            ->update(['main_code' => 'F']);

        DB::table('tblindustry_main')->where('industry_main_id', 7)
            ->update(['main_code' => 'G']);

        DB::table('tblindustry_main')->where('industry_main_id', 8)
            ->update(['main_code' => 'H']);

        DB::table('tblindustry_main')->where('industry_main_id', 9)
            ->update(['main_code' => 'I']);

        DB::table('tblindustry_main')->where('industry_main_id', 10)
            ->update(['main_code' => 'J']);

        DB::table('tblindustry_main')->where('industry_main_id', 11)
            ->update(['main_code' => 'K']);

        DB::table('tblindustry_main')->where('industry_main_id', 12)
            ->update(['main_code' => 'L']);

        DB::table('tblindustry_main')->where('industry_main_id', 13)
            ->update(['main_code' => 'M']);

        DB::table('tblindustry_main')->where('industry_main_id', 14)
            ->update(['main_code' => 'N']);
        
        
        $o = DB::table('tblindustry_main')->where('main_code', 'O')->first();
        if(!$o){
            DB::table('tblindustry_main')->insert([
                'main_code' => 'O',
                'main_title' => 'Public Administration and Defense; Compulsory Social Security',
                'business_group' => 35,
                'management_group' => 35,
                'financial_group' => 35,
                'industry_trend' => 'Flat',
                'industry_score' => 53,
                'main_status'   => 0
            ]);
        }

        DB::table('tblindustry_main')->where('industry_main_id', 15)
            ->update(['main_code' => 'P']);

        DB::table('tblindustry_main')->where('industry_main_id', 16)
            ->update(['main_code' => 'Q']);

        DB::table('tblindustry_main')->where('industry_main_id', 17)
            ->update(['main_code' => 'R']);

        DB::table('tblindustry_main')->where('industry_main_id', 18)
            ->update(['main_code' => 'S']);

        DB::table('tblindustry_main')->where('industry_main_id', 19)
            ->update(['main_code' => 'T']);

        DB::table('tblindustry_main')->where('industry_main_id', 20)
            ->update(['main_code' => 'U']);

        /** Table industry_sub */
        DB::table('tblindustry_sub')
            ->where('sub_title', 'Crop and Animal Production, Hunting and Related Service Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'A',
                'sub_code' => '01',
            ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Forestry and Logging')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'A',
                'sub_code' => '02',
            ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Fishing and Aquaculture')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'A',
                'sub_code' => '03',
            ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Mining of Coal and Lignite')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'B',
                'sub_code' => '05',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Extraction of Crude Petroleum and Natural Gas')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'B',
                'sub_code' => '06',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Mining of Metal Ores')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'B',
                'sub_code' => '07',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Other Mining and Quarrying')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'B',
                'sub_code' => '08',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Mining Support Service Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'B',
                'sub_code' => '09',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Food Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '10',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Beverages')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '11',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Tobacco Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '12',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Textiles')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '13',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Wearing Apparel')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '14',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Leather and Related Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '15',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture Of Wood, Wood Products & Cork, Except Furniture; Manufacture Of Articles Bamboo, Cane Rattan & The Like Manufacturing Of Plating Materials')
            ->where('sub_status', 0)
            ->update([
                'main_code' => 'C',
                'sub_code' => '16',
                'sub_status' => '1'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Paper and Paper Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '17',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Printing and Reproduction of Recorded Media')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '18',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Coke and Refined Petroleum Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '19',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Chemicals and Chemical Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '20',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of rubber and plastic products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '22',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Other Non-metallic Mineral Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '23',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Basic Metals')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '24',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of frabricated metal products, except machinery and equipment')
            ->where('sub_status', 0)
            ->update([
                'main_code' => 'C',
                'sub_code' => '25',
                'sub_status' => 1
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Computer, Electronic and Optical Products')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '26',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Electrical Equipment')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '27',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture Of Machinery & Equipment N.E.C.')
            ->where('sub_status', 0)
            ->update([
                'main_code' => 'C',
                'sub_code' => '28',
                'sub_status' => 1
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Motor Vehicles, Trailers and Semi-trailers')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '29',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Other Transport Equipment')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '30',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of Furniture')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '31',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Other Manufacturing')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '32',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Repair and Installation of Machinery and Equipment')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '33',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Manufacture of pharmaceutical products and pharmaceutical preparations')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'C',
                'sub_code' => '21',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Electricity, Gas, Steam and Air Conditioning Supply')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'D',
                'sub_code' => '35',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Water Collection, Treatment and Supply')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'E',
                'sub_code' => '36',
        ]);

        $sewerage = DB::table('tblindustry_sub')->where('sub_code', '37')->first();
        if(!$sewerage){
            DB::table('tblindustry_sub')->insert([
                'industry_main_id' => 5, 
                'main_code' => 'E', 
                'sub_code' => '37', 
                'sub_title' => 'Sewerage', 
                'sub_status' => 1
            ]);
        }

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Waste Collection, Treatment and Disposal Activities; Materials Recovery')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'E',
                'sub_code' => '38',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Remediation Activities and Other Waste Management Services')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'E',
                'sub_code' => '39',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Construction of Buildings')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'F',
                'sub_code' => '41',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Civil Engineering')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'F',
                'sub_code' => '42',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Specialized Construction Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'F',
                'sub_code' => '43',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Wholesale and retails trade and repair of motor vehicles and motorcycles')
            ->orWhere('sub_title', 'Wholesale and Retail Trade and Repair of Motor Vehicles and Motorcycles')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'G',
                'sub_code' => '45',
                'sub_title' => 'Wholesale and Retail Trade and Repair of Motor Vehicles and Motorcycles'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Wholesale trade,  except of motor vehicles and motorcycles')
            ->orWhere('sub_title', 'Wholesale Trade, Except of Motor Vehicles and Motorcycles')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'G',
                'sub_code' => '46',
                'sub_title' => 'Wholesale Trade, Except of Motor Vehicles and Motorcycles'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Retail Trade, Except of Motor Vehicles and Motorcycles')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'G',
                'sub_code' => '47',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Land Transport and Transport Via Pipelines')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'H',
                'sub_code' => '49',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Water Transport')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'H',
                'sub_code' => '50',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Air Transport')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'H',
                'sub_code' => '51',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Warehousing and Support Activities for Transportation')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'H',
                'sub_code' => '52',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Postal and Courier Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'H',
                'sub_code' => '53',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Accomodation')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'I',
                'sub_code' => '55',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Food and Beverage Service Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'I',
                'sub_code' => '56',
                'sub_title' => 'Food and Beverage Service Activities'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Publishing activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '58',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Motion Picture, Video and Television Programme Production, Sound Recording and Music Publishing Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '59',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Programming and Broadcasting Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '60',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Telecommunications')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '61',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Computer Programming, Consultancy and Related Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '62',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Information Service Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'J',
                'sub_code' => '63',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Financial Service Activities, Except Insurance and Pension Funding')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'K',
                'sub_code' => '64',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Insurance, Reinsurance and Pension Funding, Except Compulsory Social Security')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'K',
                'sub_code' => '65',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Activities Auxiliary to Financial Service and Insurance Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'K',
                'sub_code' => '66',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Real estate activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'L',
                'sub_code' => '68',
                'sub_title' => 'Real Estate Activites'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Legal and Accounting Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'M',
                'sub_code' => '69',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Activities of Head Offices; Management Consultancy Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'M',
                'sub_code' => '70',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Architecture and Engineering Activities; Technical Testing and Analysis')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'M',
                'sub_code' => '71',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Scientific Research and Development')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'M',
                'sub_code' => '72',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Advertising and Market Research')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'M',
                'sub_code' => '73',
        ]);

        $m_professional = DB::table('tblindustry_sub')->where('sub_code', '74')->first();
        if(!$m_professional){
            DB::table('tblindustry_sub')->insert([
                'industry_main_id' => 13, 
                'main_code' => 'M', 
                'sub_code' => '74', 
                'sub_title' => 'Other Professional, Scientific and Technical Activities', 
                'sub_status' => 1
            ]);
        }

        $veterinary_activities = DB::table('tblindustry_sub')->where('sub_code', '75')->first();
        if(!$veterinary_activities){
            DB::table('tblindustry_sub')->insert([
                'industry_main_id' => 13, 
                'main_code' => 'M', 
                'sub_code' => '75', 
                'sub_title' => 'Veterinary Activities', 
                'sub_status' => 1
            ]);
        }

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Rental and leasing activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '77',
                'sub_title' => 'Rental and Leasing Activities'
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Employment Activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '78',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Travel agency, tour operator, reservation service and related activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '79',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Security and Investigation activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '80',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Services to buildings and landscape activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '81',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Office administrative, office support and other business support activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'N',
                'sub_code' => '82',
        ]);
        

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Education')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'P',
                'sub_code' => '85',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Human health activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'Q',
                'sub_code' => '86',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Residential care activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'Q',
                'sub_code' => '87',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Social work activities without accommodation')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'Q',
                'sub_code' => '88',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Creative, arts and entertainment activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'R',
                'sub_code' => '90',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Libraries, archives, museums and other cultural activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'R',
                'sub_code' => '91',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Gambling and betting activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'R',
                'sub_code' => '92',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Sports activities and amusement and recreation activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'R',
                'sub_code' => '93',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Activities of membership organizations')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'S',
                'sub_code' => '94',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Repair of computers and personal and household goods')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'S',
                'sub_code' => '95',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Other personal service activities')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'S',
                'sub_code' => '96',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Activities of households as employers of domestic personnel')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'T',
                'sub_code' => '97',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Undifferentiated goods-and-services-producing activities of private households for own use')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'T',
                'sub_code' => '98',
        ]);

        DB::table('tblindustry_sub')
            ->where('sub_title', 'Activities of extra-territorial organizations and bodies')
            ->where('sub_status', 1)
            ->update([
                'main_code' => 'U',
                'sub_code' => '',
        ]);

        $public_ad = DB::table('tblindustry_sub')->where('sub_code', '84')->first();
        if(!$public_ad){
            DB::table('tblindustry_sub')->insert([
                'industry_main_id' => 21, 
                'main_code' => 'O', 
                'sub_code' => '84', 
                'sub_title' => 'Public Administration and Defense; Compulsory Social Security', 
                'sub_status' => 1
            ]);
        }

    }
}
