<?php

use Illuminate\Database\Seeder;

class GrdpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grdp = [
            /** GRDP YEAR 2017 */
            [ 'region' => 'NCR',            'regCode' => '13', 'year' => '2017', 'grdp' => '0.322'],
            [ 'region' => 'CAR',            'regCode' => '14', 'year' => '2017', 'grdp' => '0.017'],
            [ 'region' => 'Ilocos',         'regCode' => '01', 'year' => '2017', 'grdp' => '0.032'],
            [ 'region' => 'Cagayan Valley', 'regCode' => '02', 'year' => '2017', 'grdp' => '0.022'],
            [ 'region' => 'Central Luzon',  'regCode' => '03', 'year' => '2017', 'grdp' => '0.112'],
            [ 'region' => 'CALABARZON',     'regCode' => '04', 'year' => '2017', 'grdp' => '0.146'],
            [ 'region' => 'MIMAROPA',       'regCode' => '17', 'year' => '2017', 'grdp' => '0.019'],
            [ 'region' => 'Bicol',          'regCode' => '05', 'year' => '2017', 'grdp' => '0.028'],
            [ 'region' => 'Wes. Visayas',   'regCode' => '06', 'year' => '2017', 'grdp' => '0.048'],
            [ 'region' => 'Cent. Visayas',  'regCode' => '07', 'year' => '2017', 'grdp' => '0.064'],
            [ 'region' => 'East. Visayas',  'regCode' => '08', 'year' => '2017', 'grdp' => '0.024'],
            [ 'region' => 'Zamboanga',      'regCode' => '09', 'year' => '2017', 'grdp' => '0.021'],
            [ 'region' => 'N. Mindanao',    'regCode' => '10', 'year' => '2017', 'grdp' => '0.045'],
            [ 'region' => 'Davao',          'regCode' => '11', 'year' => '2017', 'grdp' => '0.045'],
            [ 'region' => 'SOCCSKSARGEN',   'regCode' => '12', 'year' => '2017', 'grdp' => '0.025'],
            [ 'region' => 'Caraga',         'regCode' => '16', 'year' => '2017', 'grdp' => '0.016'],
            [ 'region' => 'ARMM',           'regCode' => '15', 'year' => '2017', 'grdp' => '0.013'],

            /** GRDP YEAR 2018 */
            [ 'region' => 'NCR',            'regCode' => '13', 'year' => '2018', 'grdp' => '0.318'],
            [ 'region' => 'CAR',            'regCode' => '14', 'year' => '2018', 'grdp' => '0.017'],
            [ 'region' => 'Ilocos',         'regCode' => '01', 'year' => '2018', 'grdp' => '0.032'],
            [ 'region' => 'Cagayan Valley', 'regCode' => '02', 'year' => '2018', 'grdp' => '0.022'],
            [ 'region' => 'Central Luzon',  'regCode' => '03', 'year' => '2018', 'grdp' => '0.112'],
            [ 'region' => 'CALABARZON',     'regCode' => '04', 'year' => '2018', 'grdp' => '0.148'],
            [ 'region' => 'MIMAROPA',       'regCode' => '17', 'year' => '2018', 'grdp' => '0.02'],
            [ 'region' => 'Bicol',          'regCode' => '05', 'year' => '2018', 'grdp' => '0.029'],
            [ 'region' => 'Wes. Visayas',   'regCode' => '06', 'year' => '2018', 'grdp' => '0.047'],
            [ 'region' => 'Cent. Visayas',  'regCode' => '07', 'year' => '2018', 'grdp' => '0.065'],
            [ 'region' => 'East. Visayas',  'regCode' => '08', 'year' => '2018', 'grdp' => '0.024'],
            [ 'region' => 'Zamboanga',      'regCode' => '09', 'year' => '2018', 'grdp' => '0.021'],
            [ 'region' => 'N. Mindanao',    'regCode' => '10', 'year' => '2018', 'grdp' => '0.045'],
            [ 'region' => 'Davao',          'regCode' => '11', 'year' => '2018', 'grdp' => '0.046'],
            [ 'region' => 'SOCCSKSARGEN',   'regCode' => '12', 'year' => '2018', 'grdp' => '0.025'],
            [ 'region' => 'Caraga',         'regCode' => '16', 'year' => '2018', 'grdp' => '0.016'],
            [ 'region' => 'ARMM',           'regCode' => '15', 'year' => '2018', 'grdp' => '0.013'],

            /** GRDP YEAR 2019 */
            [ 'region' => 'NCR',            'regCode' => '13', 'year' => '2019', 'grdp' => '0.323'],
            [ 'region' => 'CAR',            'regCode' => '14', 'year' => '2019', 'grdp' => '0.017'],
            [ 'region' => 'Ilocos',         'regCode' => '01', 'year' => '2019', 'grdp' => '0.032'],
            [ 'region' => 'Cagayan Valley', 'regCode' => '02', 'year' => '2019', 'grdp' => '0.022'],
            [ 'region' => 'Central Luzon',  'regCode' => '03', 'year' => '2019', 'grdp' => '0.112'],
            [ 'region' => 'CALABARZON',     'regCode' => '04', 'year' => '2019', 'grdp' => '0.147'],
            [ 'region' => 'MIMAROPA',       'regCode' => '17', 'year' => '2019', 'grdp' => '0.019'],
            [ 'region' => 'Bicol',          'regCode' => '05', 'year' => '2019', 'grdp' => '0.029'],
            [ 'region' => 'Wes. Visayas',   'regCode' => '06', 'year' => '2019', 'grdp' => '0.047'],
            [ 'region' => 'Cent. Visayas',  'regCode' => '07', 'year' => '2019', 'grdp' => '0.065'],
            [ 'region' => 'East. Visayas',  'regCode' => '08', 'year' => '2019', 'grdp' => '0.024'],
            [ 'region' => 'Zamboanga',      'regCode' => '09', 'year' => '2019', 'grdp' => '0.02'],
            [ 'region' => 'N. Mindanao',    'regCode' => '10', 'year' => '2019', 'grdp' => '0.045'],
            [ 'region' => 'Davao',          'regCode' => '11', 'year' => '2019', 'grdp' => '0.046'],
            [ 'region' => 'SOCCSKSARGEN',   'regCode' => '12', 'year' => '2019', 'grdp' => '0.024'],
            [ 'region' => 'Caraga',         'regCode' => '16', 'year' => '2019', 'grdp' => '0.016'],
            [ 'region' => 'ARMM',           'regCode' => '15', 'year' => '2019', 'grdp' => '0.013'],
        ];
        
        DB::table('grdp')->insert($grdp);
    }
}
