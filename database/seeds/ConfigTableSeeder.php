<?php

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('config')->truncate();

	    $keys = new Keys;
		$keys->name = 'converter_key';
		$keys->value = 'password';
		$keys->save();
	}

}
