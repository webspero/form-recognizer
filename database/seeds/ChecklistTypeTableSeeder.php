<?php

use Illuminate\Database\Seeder;

class ChecklistTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = array(
            array("code" => "a1", "description" => "(a) Affidavit of Attestation, (b) Integrity Pledge and (c) Contractor’s General Information (pages 5, 6 and 7);", "category" => 0, "group" => 1),
            array("code" => "a1_1", "description" => "Certified copy of the firm’s latest SEC General Information Sheet (GIS) showing the updated list of directors, officers, stockholders and their shareholdings & nationalities;", "category" => 0, "group" => 1),
            array("code" => "a2", "description" => "Authority to verify documents with (a) Depository Bank, (b) BIR and (c) Government Agencies (pages 8, 9 and 10);", "category" => 0, "group" => 1),
            array("code" => "b1", "description" => "Statement of Annual Value of Work Accomplished/On-going as of the Balance Sheet Date for the year immediately preceding the filing of application (page 12);", "category" => 0, "group" => 2),
            array("code" => "c1_1", "description" => "Valid Working Visa;", "category" => 0, "group" => 3),
            array("code" => "c1_2", "description" => "Valid Alien Certificate of Registration;", "category" => 0, "group" => 3),
            array("code" => "c2_1_1", "description" => "STE/s Affidavit of Undertaking with copy of valid PRC ID/s (pages 16 and 17);", "category" => 0, "group" => 3),
            array("code" => "c2_1_2", "description" => "Original NBI clearance/s;", "category" => 0, "group" => 3),
            array("code" => "c2_1_3", "description" => "STE/s Affidavit of Construction Experience (page 18);", "category" => 0, "group" => 3),
            array("code" => "c2_1_4", "description" => "STE/s Personal Appearance (page 19);", "category" => 0, "group" => 3),
            array("code" => "c2_1_5", "description" => "Certificate of Completion of 40-hour Construction Safety and Health Seminar (COSH), if applicable;", "category" => 0, "group" => 3),
            array("code" => "c2_2_1", "description" => "STE/s Affidavit of Undertaking with copy of valid PRC ID/s (pages 16 and 17);", "category" => 0, "group" => 3),
            array("code" => "c2_2_2", "description" => "For STE/s below 60 years old: Copy of the pertinent page of latest SSS Collection List Details reflecting the name/s of the nominated STE/s for the three (3) months immediately preceding the filing of application;", "category" => 0, "group" => 3),
            array("code" => "c2_2_3", "description" => "For STE/s 60 years old and above: BIR 1604 CF / Alphabetical List of Employees/Payees from Whom Taxes Were Withheld filed with the BIR;", "category" => 0, "group" => 3),
            array("code" => "d1", "description" => "Certified  copy of the Annual Income Tax Return filed with the BIR and proof of payment for the taxable year immediately preceding the filing of renewal application;", "category" => 0, "group" => 4),
            array("code" => "d2", "description" => "Audited Financial Statements (AFS) with accompanying Auditor’s Opinion Report, Statement of Changes in Equity, Cash Flow and Auditor’s Notes for the preceding taxable year duly stamped-received by the BIR (duly audited and signed on each & every page by an Independent CPA with valid PRC-BOA accreditation);", "category" => 0, "group" => 4),
            array("code" => "d3", "description" => "Schedule of Receivables, if applicable (page 21);", "category" => 0, "group" => 4),
            array("code" => "e1", "description" => "Authorized Representatives Affidavit (page 23);", "category" => 0, "group" => 5),
            array("code" => "e2", "description" => "Original signature (preferably with blue ink) of AMO on each and every page of the application forms including supporting documents.", "category" => 0, "group" => 5),
            array("code" => "a1", "description" => "(a) Affidavit of Attestation, (b) Integrity Pledge and (c) Contractor’s General Information (pages 5, 6 and 7);", "category" => 1, "group" => 1),
            array("code" => "a2", "description" => "Authority to verify documents with (a) Depository Bank, (b) BIR and (c) Government Agencies (pages 8, 9 and 10);", "category" => 1, "group" => 1),
            array("code" => "b1", "description" => "Statement of Annual Value of Work Accomplished/On-going as of the Balance Sheet Date for the year immediately preceding the filing of application (page 12);", "category" => 1, "group" => 2),
            array("code" => "c1_1_1", "description" => "STE/s Affidavit of Undertaking with copy of valid PRC ID/s (pages 16 and 17);", "category" => 1, "group" => 3),
            array("code" => "c1_1_2", "description" => "Original NBI clearance/s;", "category" => 1, "group" => 3),
            array("code" => "c1_1_3", "description" => "STE/s Affidavit of Construction Experience (page 18);", "category" => 1, "group" => 3),
            array("code" => "c1_1_4", "description" => "STE/s Personal Appearance (page 19);", "category" => 1, "group" => 3),
            array("code" => "c1_1_5", "description" => "Certificate of Completion of 40-hour Construction Safety and Health Seminar (COSH), if applicable;", "category" => 1, "group" => 3),
            array("code" => "c1_2_1", "description" => "STE/s Affidavit of Undertaking with copy of valid PRC ID/s (pages 16 and 17);", "category" => 1, "group" => 3),
            array("code" => "c1_2_2", "description" => "For STE/s below 60 years old: Copy of the pertinent page of latest SSS Collection List Details reflecting the name/s of the nominated STE/s for the three (3) months immediately preceding the filing of application;", "category" => 1, "group" => 3),
            array("code" => "c1_2_3", "description" => "For STE/s 60 years old and above: BIR 1604 CF / Alphabetical List of Employees/Payees from Whom Taxes Were Withheld filed with the BIR;", "category" => 1, "group" => 3),
            array("code" => "d1", "description" => "Certified  copy of the Annual Income Tax Return filed with the BIR and proof of payment for the taxable year immediately preceding the filing of renewal application;", "category" => 1, "group" => 4),
            array("code" => "d2", "description" => "udited Financial Statements (AFS) with accompanying Auditor’s Opinion Report, Statement of Changes in Equity, Cash Flow and Auditor’s Notes for the preceding taxable year duly stamped-received by the BIR (duly audited and signed on each & every page by an Independent CPA with valid PRC-BOA accreditation);", "category" => 1, "group" => 4),
            array("code" => "d3", "description" => "Schedule of Receivables, if applicable (page 21);", "category" => 1, "group" => 4),
            array("code" => "e1", "description" => "Authorized Representatives Affidavit (page 23);", "category" => 1, "group" => 5),
            array("code" => "e2", "description" => "Original signature (preferably with blue ink) of AMO on each and every page of the application forms including supporting documents.", "category" => 1, "group" => 5),
        );

        foreach ($array as $val) {
            ChecklistType::create($val);
        }
    }
}
