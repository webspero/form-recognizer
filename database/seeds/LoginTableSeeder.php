<?php

use Illuminate\Database\Seeder;
class LoginTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('tbllogin')->delete();
		    User::create(array(
		        'email' => 'administrator@creditbpo.com',
		        'password' => Hash::make('password'),
		        'firstname' => 'Dennis',
		        'lastname' => 'Jabonete',
		        'activation_code' => '09234',
		        'activation_date' => '2015-02-16',
		        'role' => '0',
		        'status' => '1'
		    ));
			User::create(array(
		        'email' => 'investigator@creditbpo.com',
		        'password' => Hash::make('password'),
		        'firstname' => 'Dennis',
		        'lastname' => 'Investigator',
		        'activation_code' => '03234',
		        'activation_date' => '2015-02-16',
		        'role' => '2',
		        'status' => '1'
		    ));
			User::create(array(
		        'email' => 'master@creditbpo.com',
		        'password' => Hash::make('master'),
		        'firstname' => 'Dennis',
		        'lastname' => 'Jabonete',
		        'activation_code' => '00000',
		        'activation_date' => '2015-02-16',
		        'role' => '100',
		        'status' => '1'
		    ));
	}

}
