<?php

use Illuminate\Database\Seeder;
use App\Models\ApiSecret;

class ApiSecretSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $secrets = new ApiSecret;
        	$secrets->api_key = 'fd1b9df6b24e44d48225e15f6476bb57';
        	$secrets->end_point = 'https://fmsea01.cognitiveservices.azure.com';
        	$secrets->model_id = '1f8a4a74-ab1f-40ee-997e-6d010d4dff2f';
        	$secrets->container_name = 'uploadedform';
        	$secrets->container_url = 'https://storageinstaceone.blob.core.windows.net';
        	$secrets->sas_token = 'sp=racwdli&st=2022-06-15T06:09:02Z&se=2022-06-21T14:09:02Z&sv=2020-08-04&sr=c&sig=9v3Id33i5092OqOE%2FMFqyUtdbmgPhlmbT8L2DauaJ2M%3D';
        	$secrets->save();

    }
}
