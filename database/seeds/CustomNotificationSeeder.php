<?php

use Illuminate\Database\Seeder;

class CustomNotificationSeeder extends Seeder
{

	public function run()
	{
	    DB::table('custom_notifications')->truncate();
		DB::table('custom_notifications')->insert(array(
			'name' => 'Good and Bad Report Rating',
			'receiver' => 'joel.espadilla@creditbpo.com',
			'is_running' => 1,
			'is_deleted' => 0,
		));

		DB::table('custom_notifications')->insert(array(
			'name' => 'Financial Analysis',
			'receiver' => 'joel.espadilla@creditbpo.com',
			'is_running' => 1,
			'is_deleted' => 0,
		));

		DB::table('custom_notifications')->insert(array(
			'name' => 'Disk Size Checker',
			'receiver' => 'joel.espadilla@creditbpo.com',
			'is_running' => 1,
			'is_deleted' => 0,
		));
	}

}
