<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFbLike extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('web_calculator_likes', function(Blueprint $table)
        {
            $table->increments('web_calculator_id');
            $table->string('fb_user_id', 50);
            $table->tinyInteger('status');
            $table->datetime('date_liked');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('web_calculator_likes');
	}

}
