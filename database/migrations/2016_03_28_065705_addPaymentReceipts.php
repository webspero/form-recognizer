<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentReceipts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_receipts', function(Blueprint $table)
		{
			$table->increments('receipt_id');
			$table->integer('login_id');
			$table->float('total_amount');
			$table->float('vatable_amount');
			$table->float('vat_amount');
			$table->tinyInteger('payment_method');
			$table->tinyInteger('item_type');
			$table->tinyInteger('sent_status');
			$table->date('date_sent');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_receipts');
	}

}
