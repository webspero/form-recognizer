<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblentityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_main_id` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_sub_id` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_row_id` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `total_assets` decimal(20,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `cityid` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `province` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `no_yrs_present_address` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `number_year` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `number_year_management_team` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_agree` int(1) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_subscribe` int(1) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `status` int(3) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_main_id` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_sub_id` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `industry_row_id` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `total_assets` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `cityid` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `province` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `no_yrs_present_address` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `number_year` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `number_year_management_team` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_agree` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_subscribe` int(11) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `status` int(11) NOT NULL;');
		});
	}

}
