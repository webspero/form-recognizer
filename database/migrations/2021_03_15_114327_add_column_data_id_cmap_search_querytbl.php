<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDataIdCmapSearchQuerytbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CmapSearchQuery', function (Blueprint $table) {
            $table->string('data_ids', 255)->after('company_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CmapSearchQuery', function (Blueprint $table) {
            $table->dropColumn('data_ids');
        });
    }
}
