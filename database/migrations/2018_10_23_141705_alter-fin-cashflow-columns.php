<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFinCashflowColumns extends Migration {

	/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fin_cashflow', function(Blueprint $table) {
            $table->decimal('DepreciationExpense', 15, 2)->default(0);
            $table->decimal('AmortisationExpense', 15, 2)->default(0);
            $table->decimal('DepreciationAndAmortisationExpense', 15, 2)->default(0);
            $table->decimal('NetCashFlowFromOperations', 15, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fin_cashflow', function(Blueprint $table) {
            $table->dropColumn('DepreciationExpense');
            $table->dropColumn('AmortisationExpense');
            $table->dropColumn('DepreciationAndAmortisationExpense');
            $table->dropColumn('NetCashFlowFromOperations');
        });
    }

}
