<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTableName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->string('name', 100)->nullable()->after('lastname');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('name');
		});
	}

}
