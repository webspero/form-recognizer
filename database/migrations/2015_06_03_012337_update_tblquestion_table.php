<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblquestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblquestion', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblquestion` MODIFY `groupby` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblquestion', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblquestion` MODIFY `groupby` int(11) NOT NULL;');
		});
	}

}
