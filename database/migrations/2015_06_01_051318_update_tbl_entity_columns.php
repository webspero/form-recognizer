<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblEntityColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_address1` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_address2` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_cityid` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_province` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_zipcode` integer(11) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_phone` varchar(100) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `related_companies` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `tin_num` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `sec_num` varchar(200) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `current_bank` integer(11) NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_subscribe` integer(11) NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_address1` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_address2` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_cityid` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_province` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_zipcode` integer(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `former_phone` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `related_companies` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `tin_num` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `sec_num` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `current_bank` integer(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblentity` MODIFY `is_subscribe` integer(11) NOT NULL;');
		});
	}

}
