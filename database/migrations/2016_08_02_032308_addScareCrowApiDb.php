<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScareCrowApiDb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('developer_api_users', function(Blueprint $table)
		{
			$table->string('scare_crow', 250);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('developer_api_users', function(Blueprint $table)
		{
			$table->dropColumn('scare_crow');
		});
	}

}
