<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShuftiUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shufti_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("sufix")->nullable();
            $table->text("first_name")->nullable();
            $table->text("middle_name")->nullable();
            $table->text("last_name")->nullable();
            $table->text("email")->nullable();
            $table->dateTime("birth_date")->nullable();
            $table->string("gender")->nullable();
            $table->string("civil_status")->nullable();
            $table->unsignedBigInteger("birth_province")->nullable();
            $table->unsignedBigInteger("birth_city")->nullable();
            $table->string("citizenship")->nullable();
            $table->text("tin")->nullable();
            $table->integer("family_members")->nullable();
            $table->integer("monthly_income")->nullable();
            $table->integer("cards_owned")->nullable();
            $table->integer("cars_owned")->nullable();
            $table->string("cbpo_user")->nullable();
            $table->text("reference")->nullable();
            $table->string("status")->nullable();
            $table->text("message")->nullable();
            $table->integer("is_deleted")->default(0);
            $table->timestamps();
        });

        Schema::create('shufti_user_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id");
            $table->text("file_name");
            $table->text("file_type")->nullable();
            $table->text("file_category")->nullable();
            $table->text("file_path")->nullable();
            $table->integer("is_deleted")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shufti_users');
        Schema::dropIfExists('shufti_user_documents');
    }
}
