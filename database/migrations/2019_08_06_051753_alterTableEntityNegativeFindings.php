<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEntityNegativeFindings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->text('negative_findings')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('negative_findings');
		});
	}

}
