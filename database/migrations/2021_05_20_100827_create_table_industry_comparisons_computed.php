<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIndustryComparisonsComputed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_industry_comparisons_computed', function (Blueprint $table) {
        	$table->bigIncrements('id');
            $table->string('industry_code')->nullable();
            $table->decimal('gross_revenue_growth', $precision = 8, $scale = 4)->nullable();
            $table->decimal('net_income_growth', $precision = 8, $scale = 4)->nullable();
            $table->decimal('gross_profit_margin', $precision = 8, $scale = 4)->nullable();
            $table->decimal('current_ratio', $precision = 8, $scale = 4)->nullable();
            $table->decimal('debt_equity_ratio', $precision = 8, $scale = 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_industry_comparisons_computed');
    }
}
