<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblReadyRationScore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblreadyrationscore', function(Blueprint $table)
		{
			$table->increments('readyrationscoreid');
			$table->float('score_range');
			$table->integer('score_value');
			$table->integer('score_financial_value');
			$table->string('score_sign',50);
			$table->string('score_description',100);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblreadyrationscore');
	}

}
