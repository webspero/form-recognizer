<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingDetailsToTbllogin extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tbllogin', function(Blueprint $table) {
			$table->string('street_address', 255);
			$table->string('city', 255);
			$table->string('province', 255);
			$table->string('zipcode', 255);
			$table->string('phone', 15);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tbllogin', function(Blueprint $table) {
            $table->dropColumn('street_address');
            $table->dropColumn('city');
            $table->dropColumn('province');
			$table->dropColumn('zipcode');
			$table->dropColumn('phone');
        });
	}

}
