<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTbleducationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbleducation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbleducation` MODIFY `educ_type` int(3) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbleducation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbleducation` MODIFY `educ_type` int(11) NOT NULL;');
		});
	}

}
