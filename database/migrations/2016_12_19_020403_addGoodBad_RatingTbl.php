<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoodBadRatingTbl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			$table->text('positive_points')->after('operating_cashflow_ratio')->default('');
			$table->text('negative_points')->after('positive_points')->default('');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			$table->dropColumn('positive_points');
			$table->dropColumn('negative_points');
        });
	}

}
