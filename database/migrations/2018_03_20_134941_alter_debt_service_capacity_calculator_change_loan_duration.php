<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDebtServiceCapacityCalculatorChangeLoanDuration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('debt_service_capacity_calculator');

		Schema::create('debt_service_capacity_calculator', function(Blueprint $table)
		{
			$table->integer('entity_id')->primary();
			$table->decimal('dscr', 10, 1)->default(0);
			$table->integer('loan_duration')->default(0);
			$table->decimal('net_operating_income', 20, 2)->default(0);
			$table->decimal('loan_amount', 20, 2)->default(0);
			$table->decimal('working_interest_rate_local', 20, 4)->default(0);
			$table->decimal('working_interest_rate_foreign', 20, 4)->default(0);
			$table->decimal('term_interest_rate_local', 20, 4)->default(0);
			$table->decimal('term_interest_rate_foreign', 20, 4)->default(0);
			$table->integer('period')->default(0);
	        $table->decimal('payment_per_period_local', 20, 2)->default(0);
	        $table->decimal('payment_per_period_foreign', 20, 2)->default(0);
	        $table->tinyinteger('is_custom')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('debt_service_capacity_calculator');
	}

}
