<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableReportMatchingSme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_matching_sme', function (Blueprint $table) {
            $table->text('regions_served');
            $table->enum('loan_approved',['Yes','No'])->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_matching_sme', function (Blueprint $table) {
            $table->dropColumn('regions_served');
            $table->dropColumn('loan_approved');
        });
    }
}
