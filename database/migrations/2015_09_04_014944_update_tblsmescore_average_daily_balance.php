<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblsmescoreAverageDailyBalance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			$table->decimal('average_daily_balance', 12, 2)->after('score_sf');
			$table->decimal('operating_cashflow_margin', 5, 2)->after('average_daily_balance');
			$table->decimal('operating_cashflow_ratio', 5, 2)->after('operating_cashflow_margin');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			$table->dropColumn('average_daily_balance');
			$table->dropColumn('operating_cashflow_margin');
			$table->dropColumn('operating_cashflow_ratio');
		});
	}

}
