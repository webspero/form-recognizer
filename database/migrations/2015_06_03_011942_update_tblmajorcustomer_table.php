<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmajorcustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_share_sales` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_years_doing_business` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_settlement` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_order_frequency` varchar(30) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_share_sales` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_years_doing_business` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_settlement` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorcustomer` MODIFY `customer_order_frequency` varchar(200) NOT NULL;');
		});
	}

}
