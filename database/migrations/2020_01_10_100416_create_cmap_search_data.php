<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmapSearchData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmap_search_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('data_table_name');
            $table->string('data_name');
            $table->string('data_type');
            $table->smallInteger('data_id');
            $table->string('data_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cmap_search_data');
    }
}
