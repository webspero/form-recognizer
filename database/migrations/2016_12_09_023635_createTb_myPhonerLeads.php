<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMyPhonerLeads extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('myphoner_leads', function(Blueprint $table)
		{
			$table->increments('lead_id');
			$table->string('myphoner_id', 100);
			$table->string('institute_name', 250);
			$table->string('email_address', 250);
			$table->string('primary_contact_dm', 250);
			$table->string('contact_no', 250);
			$table->string('trial_start_date', 250)->default(STR_EMPTY);
			$table->tinyInteger('status');
			$table->datetime('created_date');
			$table->datetime('updated_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('myphoner_leads');
	}

}
