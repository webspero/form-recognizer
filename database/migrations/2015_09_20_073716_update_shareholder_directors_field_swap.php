<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShareholderDirectorsFieldSwap extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbldirectors', function(Blueprint $table)
		{
			$table->decimal('percentage_share', 6, 2)->default(0);
		});
		
		Schema::table('tblshareholders', function(Blueprint $table)
		{
			$table->string('id_no');
			$table->string('nationality');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbldirectors', function(Blueprint $table)
		{
			$table->dropColumn('percentage_share');
		});
		
		Schema::table('tblshareholders', function(Blueprint $table)
		{
			$table->dropColumn('id_no');
			$table->dropColumn('nationality');
		});
	}

}
