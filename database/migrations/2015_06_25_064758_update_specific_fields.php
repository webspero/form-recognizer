<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSpecificFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblentity` MODIFY `total_assets` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblprojectcompleted` MODIFY `projectcompleted_cost` decimal(15,2) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `tblentity` MODIFY `total_assets` int(20) NOT NULL;');
		DB::statement('ALTER TABLE `tblprojectcompleted` MODIFY `projectcompleted_cost` int(20) NOT NULL;');
	}

}
