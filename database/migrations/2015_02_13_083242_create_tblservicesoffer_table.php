<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblservicesofferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblservicesoffer', function(Blueprint $table)
		{
			$table->increments('servicesofferid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('servicesoffer_name', 200);
			$table->string('servicesoffer_share_revenue', 200);
			$table->string('servicesoffer_seasonality', 200);
			$table->string('servicesoffer_targetmarket', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblservicesoffer');
	}

}
