<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpenSanctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_sanctions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("firstname",100);
            $table->string('lastname',100);
            $table->string('source',250);
            $table->string('nationality',100)->nullable();
            $table->datetime('birth_date')->nullable();
            $table->string('birthplace',250)->nullable();
            $table->string('sex',10)->nullable();
            $table->text("charges")->nullable();
            $table->integer("is_deleted")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_sanctions');
    }
}
