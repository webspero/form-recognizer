<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmanagebusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmanagebus', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmanagebus` MODIFY `bus_location` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmanagebus', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmanagebus` MODIFY `bus_location` int(11) NOT NULL;');
		});
	}

}
