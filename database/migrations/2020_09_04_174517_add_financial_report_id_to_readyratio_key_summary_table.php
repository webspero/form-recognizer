<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinancialReportIdToReadyratioKeySummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('readyratio_key_summary', function (Blueprint $table) {
            $table->integer('financial_report_id')->after('T5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('readyratio_key_summary', function (Blueprint $table) {
            $table->dropColumn('financial_report_id');
        });
    }
}
