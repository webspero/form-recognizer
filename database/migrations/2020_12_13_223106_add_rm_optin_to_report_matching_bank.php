<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRmOptinToReportMatchingBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_matching_bank', function (Blueprint $table) {
            $table->enum('matching_optin',['Yes','No'])->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_matching_bank', function (Blueprint $table) {
            $table->dropColumn('matching_optin');
        });
    }
}
