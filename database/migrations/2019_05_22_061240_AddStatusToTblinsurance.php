<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToTblinsurance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tblinsurance', function(Blueprint $table)
		{
			$table->integer('is_deleted')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tblinsurance', function(Blueprint $table)
		{
			$table->dropColumn('is_deleted');
		});
	}

}
