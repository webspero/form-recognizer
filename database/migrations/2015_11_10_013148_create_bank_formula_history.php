<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankFormulaHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_formula_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->integer('bank_id');
			$table->string('bcc', 30);
			$table->string('mq', 30);
			$table->string('fa', 30);
			$table->smallInteger('boost');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_formula_history');
	}

}
