<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblpaymentEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblpayment', function(Blueprint $table)
		{
			$table->string('email')->after('currencycode')->nullable();
			$table->smallInteger('batch')->after('email')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblpayment', function(Blueprint $table)
		{
			$table->dropColumn('email');
			$table->dropColumn('batch');
		});
	}

}
