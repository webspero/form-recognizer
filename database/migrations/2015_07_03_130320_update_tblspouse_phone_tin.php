<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblspousePhoneTin extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblspouse` MODIFY `phone` VARCHAR(128) NOT NULL;');
		DB::statement('ALTER TABLE `tblspouse` MODIFY `tin_num` VARCHAR(64) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblspouse', function(Blueprint $table)
		{
			// no rollback wrong datatype
		});
	}

}
