<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFsTemplateV7 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fin_cashflow', function(Blueprint $table)
		{
			$table->decimal('NetCashByOperatingActivities', 15, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fin_cashflow', function(Blueprint $table)
		{
			$table->dropColumn('NetCashByOperatingActivities');
		});
	}

}
