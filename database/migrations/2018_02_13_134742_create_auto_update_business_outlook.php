<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoUpdateBusinessOutlook extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_outlook_update', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('quarter');
            $table->integer('year');
            $table->tinyInteger('status')->default(0);
            $table->date('executed');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_outlook_update');
	}

}
