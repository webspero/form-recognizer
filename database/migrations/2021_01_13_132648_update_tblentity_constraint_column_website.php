<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblentityConstraintColumnWebsite extends Migration
{
    public function __construct(){
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->string('website')->nullable()->change();
            $table->string('email')->nullable()->change();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->string('website')->nullable()->change();
            $table->string('email')->nullable()->change();
	    });
    }
}
