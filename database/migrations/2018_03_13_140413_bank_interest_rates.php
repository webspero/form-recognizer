<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BankInterestRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_interest_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('low', 20, 2)->default(0);
			$table->decimal('mid', 20, 2)->default(0);
			$table->decimal('high', 20, 2)->default(0);
			$table->string('type', 20)->default('local');
			$table->date('executed');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_interest_rates');
	}

}
