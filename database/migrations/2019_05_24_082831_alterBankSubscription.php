<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBankSubscription extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblbanksubscriptions', function(Blueprint $table)
		{
			$table->integer('status')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblbanksubscriptions', function(Blueprint $table)
		{
			$table->dropColumn('status');
			$table->dropColumn('updated_by');
		});
	}

}
