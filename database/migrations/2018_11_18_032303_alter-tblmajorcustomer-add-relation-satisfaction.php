<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblmajorcustomerAddRelationSatisfaction extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table) {
			$table->tinyInteger('relationship_satisfaction')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table) {
			$table->dropColumn('relationship_satisfaction');
		});
	}

}
