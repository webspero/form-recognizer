<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentReceipts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_receipts', function(Blueprint $table)
		{
			$table->decimal('discount', 15, 2)->nullable();
			$table->decimal('price', 15, 2)->nullable();
			$table->decimal('amount', 15, 2)->nullable();
			$table->decimal('subtotal', 15, 2)->nullable();
			// $table->float('vatable_amount')->nullable();
		});

		// /*Modify vatable_amount to nullable*/
		DB::statement('ALTER TABLE `payment_receipts` MODIFY `vatable_amount` float NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('payment_receipts', function(Blueprint $table)
		{
			$table->dropColumn('discount');
			$table->dropColumn('price');
			$table->dropColumn('amount');
			$table->dropColumn('subtotal');
			// $table->float('vatable_amount')->nullable(false);
		});

		DB::statement('ALTER TABLE `payment_receipts` MODIFY `vatable_amount` float NOT NULL;');
	}

}
