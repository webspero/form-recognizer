<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCmapToTblbank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblbank', function (Blueprint $table) {
            $table->integer('is_cmap')->default(0);
            $table->integer('cmap_is_used')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblbank', function (Blueprint $table) {
			$table->dropColumn('is_cmap');
			$table->dropColumn('cmap_is_used');
        });
    }
}
