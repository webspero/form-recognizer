<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinCashflowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fin_cashflow', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('financial_report_id');
			$table->smallInteger('index_count');
			$table->decimal('Amortization', 15, 2);
			$table->decimal('Depreciation', 15, 2);
			$table->decimal('InterestExpense', 15, 2);
			$table->decimal('NonCashExpenses', 15, 2);
			$table->decimal('NetOperatingIncome', 15, 2);
			$table->decimal('PrincipalPayments', 15, 2);
			$table->decimal('InterestPayments', 15, 2);
			$table->decimal('LeasePayments', 15, 2);
			$table->decimal('DebtServiceCapacity', 15, 2);
			$table->decimal('DebtServiceCapacityRatio', 15, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fin_cashflow');
	}

}
