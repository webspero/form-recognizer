<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblindustryclassForecast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblindustryclass_forecast', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('class_code', 5);
            $table->year('year');
            $table->float('gross_revenue_growth', 10, 4);
            $table->float('net_income', 10, 4);
            $table->float('gross_profit_margin', 10, 4);
            $table->float('current_ratio', 10, 4);
            $table->float('debt_equity_ratio', 10, 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblindustryclass_forecast');
    }
}
