<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbackFinalRatings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feedback_final_ratings', function(Blueprint $table)
        {
            $table->increments('final_rating_id');
            $table->integer('transaction_id');
            $table->tinyInteger('ease_of_experience');
            $table->tinyInteger('report_satisfaction');
            $table->tinyInteger('our_understanding');
            $table->tinyInteger('will_recommend');
            $table->tinyInteger('status');
            $table->date('date_rated');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feedback_final_ratings');
	}

}
