<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblplanfacilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `cash_deposit_amount` decimal(15,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `securities_estimated_value` decimal(15,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `property_estimated_value` decimal(15,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `chattel_estimated_value` decimal(15,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `others_estimated_value` decimal(15,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `purpose_credit_facility` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `cash_deposit_amount` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `securities_estimated_value` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `property_estimated_value` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `chattel_estimated_value` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `others_estimated_value` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblplanfacility` MODIFY `purpose_credit_facility` varchar(255) NOT NULL;');
		});
	}

}
