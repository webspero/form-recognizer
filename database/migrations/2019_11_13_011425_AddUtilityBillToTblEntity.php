<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUtilityBillToTblEntity extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->integer('utility_bill')->after('negative_findings');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('utility_bill');
		});
	}

}
