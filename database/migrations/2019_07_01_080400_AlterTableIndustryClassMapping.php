<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableIndustryClassMapping extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('industry_class', function(Blueprint $table)
		{
			$table->text('mapping')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('industry_class', function(Blueprint $table)
		{
			$table->dropColumn('mapping');
		});
	}
	

}
