<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChecklistFilesColApprovedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('checklist_files', function (Blueprint $table) {
            $table->renameColumn( 'approved_by', 'reviewed_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('checklist_files', function (Blueprint $table) {
            $table->renameColumn('reviewed_by', 'approved_by');
            //
        });
    }
}
