<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndustryGrowthForecast extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->string('gf_year1', 250);
			$table->string('gf_year2', 250);
			$table->string('gf_year3', 250);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->dropColumn('gf_year1');
			$table->dropColumn('gf_year2');
			$table->dropColumn('gf_year3');
		});
	}

}
