<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('auditlogs', function(Blueprint $table)
		{
			$table->increments('auditLogId');
			$table->integer('entity_id');
			$table->integer('login_id');
			$table->string('activity', 255);
			$table->text('error_code')->default(null)->nullable();
			$table->text('stack_trace')->default(null)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('auditlogs');
	}

}
