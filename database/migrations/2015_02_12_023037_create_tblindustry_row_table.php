<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblindustryRowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblindustry_row', function(Blueprint $table)
		{
			$table->increments('industry_row_id');
			$table->integer('industry_sub_id')->index();
			$table->string('row_code', 100);
			$table->string('row_title', 200);
			$table->integer('row_status')->index();
			$table->timestamps();
		});
	} 

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblindustry_row');
	}

}
