<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblcreditlinesInvestigationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcreditlines_investigation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcreditlines_investigation` MODIFY `banking_credit_experience` SMALLINT NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines_investigation` MODIFY `credit_facility_ready` SMALLINT NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcreditlines_investigation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcreditlines_investigation` MODIFY `banking_credit_experience` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines_investigation` MODIFY `credit_facility_ready` varchar(200) NOT NULL;');
		});
	}

}
