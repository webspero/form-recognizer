<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('currency');
		Schema::create('currency', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('symbol', 50);
			$table->string('iso_code', 10);
			$table->string('fraction_unit', 20);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currency');
		Schema::create('currency', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('symbol', 5);
			$table->string('iso_code', 3);
			$table->string('fraction_unit', 20);
		});
	}
}
