<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMajorSupplierItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			$table->string('item_1');
			$table->string('item_2');
			$table->string('item_3');
			$table->decimal('average_monthly_volume', 15, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			$table->dropColumn('item_1');
			$table->dropColumn('item_2');
			$table->dropColumn('item_3');
			$table->dropColumn('average_monthly_volume');
		});
	}

}
