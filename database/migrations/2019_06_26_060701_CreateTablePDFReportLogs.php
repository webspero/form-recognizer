<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePDFReportLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('generate_report_pdf_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entity_id');
			$table->integer('status');
			$table->string('error',250)->nullable();
			$table->timestamps();
			$table->integer("is_deleted");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop("generate_report_pdf_logs");
	}

}
