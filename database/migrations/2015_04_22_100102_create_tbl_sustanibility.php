<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSustanibility extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblsustainability', function(Blueprint $table)
		{
			$table->increments('sustainabilityid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->smallInteger('succession_plan')->nullable();
			$table->smallInteger('succession_timeframe')->nullable();
			$table->string('competition_details', 200)->nullable();
			$table->smallInteger('competition_landscape')->nullable();
			$table->smallInteger('competition_timeframe')->nullable();
			$table->smallInteger('ev_susceptibility_economic_recession')->nullable();
			$table->smallInteger('ev_foreign_exchange_interest_sensitivity')->nullable();
			$table->smallInteger('ev_commodity_price_volatility')->nullable();
			$table->smallInteger('ev_governement_regulation')->nullable();
			$table->smallInteger('cg_structure')->nullable();
			$table->smallInteger('mt_executive_team')->nullable();
			$table->smallInteger('mt_middle_management')->nullable();
			$table->smallInteger('mt_staff')->nullable();
			$table->smallInteger('crm_Structure')->nullable();
			$table->smallInteger('bpsf_Regular_cycle_business')->nullable();
			$table->smallInteger('tax_payments')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblsustainability');
	}

}
