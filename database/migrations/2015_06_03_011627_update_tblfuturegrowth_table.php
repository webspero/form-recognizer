<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblfuturegrowthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblfuturegrowth', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_name` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_estimated_cost` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_implementation_date` DATE NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_source_capital` varchar(100) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblfuturegrowth', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_name` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_estimated_cost` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_implementation_date` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_source_capital` varchar(200) NOT NULL;');
		});
	}

}
