<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmajorlocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorlocation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorlocation` MODIFY `marketlocation` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorlocation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorlocation` MODIFY `marketlocation` int(11) NOT NULL;');
		});
	}

}
