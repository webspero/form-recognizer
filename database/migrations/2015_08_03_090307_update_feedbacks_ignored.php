<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFeedbacksIgnored extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('feedback_tab_ratings')
            ->where('user_difficulty', '')
            ->where('ignored', 0)
            ->update(array(
                    'ignored' => 1
                )
            );
        
        DB::table('feedback_tab_ratings')
            ->where('user_difficulty', '!=', '')
            ->where('ignored', 1)
            ->update(array(
                    'ignored' => 0
                )
            );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
