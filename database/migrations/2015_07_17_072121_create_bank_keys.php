<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_keys', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bank_id');
			$table->string('serial_key');
			$table->smallInteger('is_used')->default(0);
			$table->integer('login_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_keys');
	}

}
