<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblsmescoreGrossRevenueGrowth extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `tblreadyrationdata` MODIFY COLUMN `gross_revenue_growth1` DECIMAL(15,2) NOT NULL DEFAULT 0");
		DB::statement("ALTER TABLE `tblreadyrationdata` MODIFY COLUMN `gross_revenue_growth2` DECIMAL(15,2) NOT NULL DEFAULT 0");
		DB::statement("ALTER TABLE `tblreadyrationdata` MODIFY COLUMN `net_income_growth1` DECIMAL(15,2) NOT NULL DEFAULT 0");
		DB::statement("ALTER TABLE `tblreadyrationdata` MODIFY COLUMN `net_income_growth2` DECIMAL(15,2) NOT NULL DEFAULT 0");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
