<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAveragedailybalanceToTblentity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->string('average_daily_balance')->after('utility_bill');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->dropColumn('average_daily_balance');
        });
    }
}
