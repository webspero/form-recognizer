<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPreviousLoanApplication extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblpreviousloanapplication', function(Blueprint $table)
		{
			$table->increments('ploanapplicationid');
			$table->integer('loginid')->index();
			$table->string('ploan_name',200);
			$table->string('ploan_products',200);
			$table->date('ploan_date');
			$table->string('ploan_status',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblpreviousloanapplication');
	}

}
