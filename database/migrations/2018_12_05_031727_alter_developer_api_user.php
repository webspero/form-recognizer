<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeveloperApiUser extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('developer_api_users');
		Schema::create('developer_api_users', function(Blueprint $table) {
			$table->increments('client_id');
			$table->string('firstname', 100);
			$table->string('lastname', 100);
			$table->string('email', 100);
			$table->string('password', 100);
			$table->string('secret_key', 100);
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('developer_api_users');
		Schema::create('developer_api_users', function(Blueprint $table) {
			$table->increments('client_id');
			$table->string('firstname', 100);
			$table->string('lastname', 100);
			$table->string('email', 100);
			$table->string('password', 100);
			$table->string('secret_key', 100);
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}
}
