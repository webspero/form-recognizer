<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePsic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // sections and Industry Main
        Schema::create('psic_section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->integer('main_id')->nullable();
            $table->string('status');
            $table->string('is_deleted');
            $table->timestamps();
        });

        // divisions and industry sub
        Schema::create('psic_division', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->integer('main_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('section_id')->nullable();
            $table->string('status');
            $table->text('description')->nullable();
            $table->string('is_deleted');
            $table->timestamps();
        });

        // groups and industry rows
        Schema::create('psic_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->integer('main_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('row_id')->nullable();
            $table->integer('division_id')->nullable();
            $table->string('status');
            $table->text('description')->nullable();
            $table->string('is_deleted');
            $table->timestamps();
        });

        // class and industry rows
        Schema::create('psic_class', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->integer('main_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('row_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->string('status');
            $table->text('description')->nullable();
            $table->string('is_deleted');
            $table->timestamps();
        });

        // subclass and industry rows
        Schema::create('psic_subclass', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->integer('main_id')->nullable();
            $table->integer('sub_id')->nullable();
            $table->integer('row_id')->nullable();
            $table->integer('subclass_id')->nullable();
            $table->string('status');
            $table->string('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psic_section');
        Schema::dropIfExists('psic_division');
        Schema::dropIfExists('psic_group');
        Schema::dropIfExists('psic_class');
        Schema::dropIfExists('psic_subclass');
    }
}
