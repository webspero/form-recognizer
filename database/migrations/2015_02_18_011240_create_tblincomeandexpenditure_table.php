<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblincomeandexpenditureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblincomeandexpenditure', function(Blueprint $table)
		{
			$table->increments('incomeandexpenditureid');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->bigInteger('total_income');
			$table->bigInteger('living_expenses');
			$table->bigInteger('education_medical');
			$table->bigInteger('insurance_pension');
			$table->bigInteger('rent_amortization');
			$table->bigInteger('transportation_utilities');
			$table->bigInteger('credit_cards');
			$table->bigInteger('other_loans');
			$table->bigInteger('miscellaneous');
			$table->bigInteger('total_expenses');
			$table->bigInteger('net_monthly_income');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblincomeandexpenditure');
	}

}
