<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigatorTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investigator_tag', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('entity_id')->index();
			$table->string('group', 50)->index();
			$table->string('item', 50)->index();
			$table->enum('status', array('verified', 'incomplete'));
			$table->string('notes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investigator_tag');
	}

}
