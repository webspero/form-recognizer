<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbldropboxfspdf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldropboxfspdf', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('entity_id');
            $table->integer('login_id');
            $table->string('file_url');
            $table->string('file_name');
            $table->string('file_download');
            $table->string('is_deleted');
            $table->string('type');
            $table->integer('file_count');
            $table->string('orig_filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldropboxfspdf');
    }
}
