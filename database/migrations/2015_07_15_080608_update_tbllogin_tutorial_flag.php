<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblloginTutorialFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->smallInteger('tutorial_flag')->default(0);
			$table->integer('transactionid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('tutorial_flag');
		});
	}

}
