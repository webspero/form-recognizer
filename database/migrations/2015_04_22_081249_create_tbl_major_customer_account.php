<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMajorCustomerAccount extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmajorcustomeraccount', function(Blueprint $table)
		{
			$table->increments('majorcustomeraccountid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('is_agree');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmajorcustomeraccount');
	}

}
