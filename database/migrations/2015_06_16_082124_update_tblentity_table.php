<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblentityTableThree extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->datetime('completed_date')->nullable()->after('is_independent');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('completed_date');
		});
	}

}
