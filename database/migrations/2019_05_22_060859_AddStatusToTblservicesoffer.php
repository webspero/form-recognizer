<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToTblservicesoffer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tblservicesoffer', function(Blueprint $table)
		{
			$table->integer('is_deleted')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tblservicesoffer', function(Blueprint $table)
		{
			$table->dropColumn('is_deleted');
		});
	}

}
