<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePseCompanyFinancialReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pse_company_financial_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('pse_company_id');
            $table->string('edge_no');
            $table->string('attachment_file_name');
            $table->string('attachment_file_id');
            $table->string('attachment_file_url');
            $table->integer("is_deleted")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pse_company_financial_reports');
    }
}
