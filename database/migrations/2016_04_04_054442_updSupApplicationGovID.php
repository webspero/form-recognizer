<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdSupApplicationGovID extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->string('valid_id_picture', 250)->after('product_features');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->dropColumn('valid_id_picture');
		});
	}

}
