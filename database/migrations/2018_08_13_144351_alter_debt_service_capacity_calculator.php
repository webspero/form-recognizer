<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDebtServiceCapacityCalculator extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('debt_service_capacity_calculator', function($table) {
	        $table->string('source', 100);
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('debt_service_capacity_calculator', function($table) {
	        $table->dropColumn('source');
	    });
	}

}
