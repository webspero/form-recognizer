<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPseExtractionTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pse_companies', function(Blueprint $table)
        {
            $table->smallInteger('pse_id');
            $table->string('company_name', 45);
            $table->string('pse_code', 10);
            $table->tinyInteger('status');
            $table->datetime('date_updated');
        });
        
        Schema::create('pse_balance_sheets', function(Blueprint $table)
        {
            $table->increments('pse_balsheet_id');
            $table->smallInteger('pse_id');
            $table->string('year', 4);
            $table->string('current_asset', 45);
            $table->string('total_asset', 45);
            $table->string('current_liabilities', 45);
            $table->string('total_liabilities', 45);
            $table->string('retained_earnings', 45);
            $table->string('stockholders_equity', 45);
            $table->string('parent_stockholders_equity', 45);
            $table->string('book_value', 45);
            $table->tinyInteger('status');
            $table->datetime('date_updated');
        });
        
        Schema::create('pse_income_statement', function(Blueprint $table)
        {
            $table->increments('pse_income_id');
            $table->smallInteger('pse_id');
            $table->string('year', 4);
            $table->string('gross_revenue', 45);
            $table->string('gross_expense', 45);
            $table->string('net_income_before_tax', 45);
            $table->string('net_income_after_tax', 45);
            $table->string('net_income_parent_attrib', 45);
            $table->string('basic_earnings_per_share', 45);
            $table->string('diluted_earnings_per_share', 45);
            $table->tinyInteger('status');
            $table->datetime('date_updated');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pse_companies');
		Schema::drop('pse_balance_sheets');
		Schema::drop('pse_income_statement');
	}

}
