<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblplanfacilityrequestedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblplanfacilityrequested', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('plan_credit_availment');
			$table->string('cash_deposit_details');
			$table->decimal('cash_deposit_amount', 15, 2);
			$table->string('securities_details');
			$table->decimal('securities_estimated_value', 15,2);
			$table->string('property_details');
			$table->decimal('property_estimated_value',15,2);
			$table->string('chattel_details');
			$table->decimal('chattel_estimated_value',15,2);
			$table->string('others_details');
			$table->decimal('others_estimated_value',15,2);
			$table->smallInteger('purpose_credit_facility');
			$table->string('purpose_credit_facility_others');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblplanfacilityrequested');
	}

}
