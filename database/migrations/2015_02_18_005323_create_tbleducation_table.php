<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbleducationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbleducation', function(Blueprint $table)
		{
			$table->increments('educationid');
			$table->integer('entity_id');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->integer('educ_type')->index();
			$table->string('school_name', 200);
			$table->string('educ_degree', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbleducation');
	}

}
