<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntityDscrInterestRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('debt_service_capacity_calculator', function(Blueprint $table)
		{
			$table->integer('entityid')->primary();
			$table->decimal('dscr', 10, 4)->default(0);
			$table->decimal('loan_duration', 20, 12)->default(0);
			$table->decimal('net_operating_income', 20, 2)->default(0);
			$table->decimal('loan_amount', 20, 2)->default(0);
			$table->decimal('working_interest_rate_local', 20, 2)->default(0);
			$table->decimal('working_interest_rate_foreign', 20, 2)->default(0);
			$table->decimal('term_interest_rate_local', 20, 2)->default(0);
			$table->decimal('term_interest_rate_foreign', 20, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('debt_service_capacity_calculator');
	}

}
