<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDirectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldirectors', function (Blueprint $table) {
            $table->date('birthDate')-> after('name');
            $table->string('lastName')->after('name');
            $table->string('middleName')->after('name');
            $table->string('firstName')->after('name');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldirectors', function (Blueprint $table) {
            $table->dropColumn('firstName');
            $table->dropColumn('middleName');
            $table->dropColumn('lastName');
            $table->dropColumn('birthDate');
            //
        });
    }
}
