<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAuthTokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_auth_token', function(Blueprint $table)
        {
			$table->increments('id');
			$table->integer('login_id');
			$table->integer('client_id');
			$table->string('access_token', 100);
			$table->string('refresh_token', 100);
			$table->dateTime('expires_in');
			$table->tinyInteger('status')->default(1);
			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_auth_tokens');
	}

}
