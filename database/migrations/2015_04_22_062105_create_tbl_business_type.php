<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBusinessType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbusinesstype', function(Blueprint $table)
		{
			$table->increments('businesstypeid');
			$table->integer('loginid')->index();
			$table->integer('is_import');
			$table->integer('is_export');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbusinesstype');
	}

}
