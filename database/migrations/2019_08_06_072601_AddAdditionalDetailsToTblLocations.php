<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalDetailsToTblLocations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tbllocations', function(Blueprint $table)
		{
			$table->integer('floor_count');
			$table->string("address", 255);
			$table->string("material", 255);
			$table->string("color", 255);
			$table->string("map", 255);
			$table->string("location_photo", 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tbllocations', function(Blueprint $table)
		{
			$table->dropColumn('floor_count');
			$table->dropColumn('address');
			$table->dropColumn('material');
			$table->dropColumn('color');
			$table->dropColumn("map");
			$table->dropColumn("location_photo");
		});
	}

}
