<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDocumentaryRequirements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function($table)
		{
			$table->string('permit_to_operate', 45);
			$table->string('authority_to_borrow', 45);
			$table->string('authorized_signatory', 45);
			$table->string('tax_registration', 45);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function($table)
		{
			$table->dropColumn('permit_to_operate');
			$table->dropColumn('authority_to_borrow');
			$table->dropColumn('authorized_signatory');
			$table->dropColumn('tax_registration');
		});
	}

}
