<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMajorSupplierAccount extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmajorsupplieraccount', function(Blueprint $table)
        {
            $table->increments('majorsupplieraccountid');
            $table->integer('entity_id');
            $table->tinyInteger('is_agree');
            $table->date('created_at');
            $table->date('updated_at');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmajorsupplieraccount');
	}

}
