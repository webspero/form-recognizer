<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmapTableHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmap_table_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cmap_table_name');
            $table->string('cmap_table_header');
            $table->smallInteger('header_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cmap_table_headers');
    }
}
