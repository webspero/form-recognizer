<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFutureGrowthTb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblfuturegrowth', function($table)
		{
			$table->string('planned_proj_goal', 20);
			$table->string('planned_goal_increase', 20);
			$table->date('proj_benefit_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblfuturegrowth', function($table)
		{
			$table->dropColumn('planned_proj_goal');
			$table->dropColumn('planned_goal_increase');
			$table->dropColumn('proj_benefit_date');
		});
	}

}
