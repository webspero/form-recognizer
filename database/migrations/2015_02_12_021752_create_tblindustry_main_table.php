<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblindustryMainTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblindustry_main', function(Blueprint $table)
		{
			$table->increments('industry_main_id');
			$table->string('main_title', 200);
			$table->integer('business_group');
			$table->integer('management_group');
			$table->integer('financial_group');
			$table->string('industry_trend', 200);
			$table->integer('industry_score');
			$table->integer('main_status')->index();
			$table->float('gross_revenue_growth',10,2);
			$table->float('net_income_growth',10,2);
			$table->float('gross_profit_margin',10,2);
			$table->float('net_profit_margin',10,2);
			$table->float('net_cash_margin',10,2);
			$table->float('current_ratio',10,2);
			$table->float('debt_equity_ratio',10,2);
			$table->float('industry_capacity_utilization',10,2);
			$table->decimal('net_cash',15,2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblindustry_main');
	}

}
