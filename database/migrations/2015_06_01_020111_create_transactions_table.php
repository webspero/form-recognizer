<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbltransactions', function($table)
		{
			$table->increments('id');
			$table->integer('loginid')->index();
			$table->decimal('amount', 8, 2);
			$table->string('ccy', 4);
			$table->string('description')->nullable();
			$table->string('refno', 20)->nullable();
			$table->string('status', 10)->nullable()->index();
			$table->string('message')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbltransactions');
	}

}
