<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblkeymanagers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblkeymanagers', function(Blueprint $table)
		{
			$table->increments('keymanagerid');
			$table->integer('ownerid');
			$table->integer('entity_id');
			$table->integer('loginid');
			$table->string('firstname', 100);
			$table->string('middlename', 100);
			$table->string('lastname', 100);
			$table->string('nationality', 100);
			$table->date('birthdate');
			$table->string('civilstatus', 50);
			$table->string('profession', 200);
			$table->string('email');
			$table->string('address1');
			$table->string('address2');
			$table->smallInteger('cityid');
			$table->smallInteger('province');
			$table->smallInteger('zipcode');
			$table->smallInteger('present_address_status');
			$table->smallInteger('no_yrs_present_address');
			$table->integer('phone');
			$table->string('tin_num', 20);
			$table->smallInteger('percent_of_ownership');
			$table->smallInteger('number_of_year_engaged');
			$table->string('position');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblkeymanagers');
	}

}
