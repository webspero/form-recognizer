<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoginAttempt extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_login_attempts', function(Blueprint $table)
        {
            $table->increments('login_attempt_id');
            $table->integer('login_id');
            $table->integer('attempts');
            $table->string('country_code', 3);
            $table->tinyInteger('status');
            $table->datetime('last_login');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_login_attempts');
	}

}
