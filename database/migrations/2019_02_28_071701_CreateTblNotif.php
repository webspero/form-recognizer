<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNotif extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('tblnotif', function(Blueprint $table) {
			$table->increments('notif_id');
			$table->integer('bank_id');
			$table->tinyInteger('notif_new_report');
			$table->tinyInteger('notif_client_key_use');
			$table->tinyInteger('notif_keys_available');
			$table->tinyInteger('notif_keys_almost_out');
			$table->tinyInteger('notif_no_more_keys');
			$table->integer('keys_number_trigger');
			$table->datetime('created_date');
			$table->datetime('updated_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('tblnotif');
	}

}
