<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentUploadType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbldocument', function(Blueprint $table)
		{
			$table->tinyInteger('upload_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbldocument', function(Blueprint $table)
		{
			$table->dropColumn('upload_type');
		});
	}

}
