<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblnetworkinfluenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblnetworkinfluence', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblnetworkinfluence` MODIFY `network_type` int(3) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblnetworkinfluence', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblnetworkinfluence` MODIFY `network_type` varchar(100) NOT NULL;');
		});
	}

}
