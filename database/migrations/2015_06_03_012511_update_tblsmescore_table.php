<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblsmescoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_rm` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_cd` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_sd` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_bois` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_boe` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_mte` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_bdms` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_sp` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_ppfi` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblsmescore', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_rm` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_cd` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_sd` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_bois` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_boe` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_mte` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_bdms` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_sp` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblsmescore` MODIFY `score_ppfi` int(10) NOT NULL;');
		});
	}

}
