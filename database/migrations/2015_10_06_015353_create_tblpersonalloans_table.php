<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpersonalloansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblpersonalloans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entity_id');
			$table->integer('owner_id');
			$table->string('purpose', 100);
			$table->decimal('balance', 15, 2);
			$table->date('as_of');
			$table->decimal('monthly_amortization', 15, 2);
			$table->string('creditor', 100);
			$table->date('due_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblpersonalloans');
	}

}
