<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblloginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbllogin', function(Blueprint $table)
		{
			$table->increments('loginid', 11);
			$table->string('email', 100);
			$table->string('password', 100);
			$table->string('job_title', 100);
			$table->string('firstname', 100);
			$table->string('lastname', 100);
			$table->integer('activation_code');
			$table->date('activation_date');
			$table->integer('role')->index();
			$table->integer('status')->index();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbllogin');
	}

}
