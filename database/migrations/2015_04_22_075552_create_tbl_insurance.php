<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInsurance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblinsurance', function(Blueprint $table)
		{
			$table->increments('insuranceid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->timestamps();
			$table->string('insurance_type',200);
            $table->string('insured_amount',200);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblinsurance');
	}

}
