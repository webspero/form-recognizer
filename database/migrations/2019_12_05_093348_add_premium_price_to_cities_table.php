<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPremiumPriceToCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refcitymun', function (Blueprint $table) {
            $table->integer('premiumReportPrice')->after('citymunCode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refcitymun', function (Blueprint $table) {
            $table->dropColumn('premiumReportPrice');
        });
    }
}
