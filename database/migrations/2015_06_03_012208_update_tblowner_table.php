<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblownerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblowner', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblowner` MODIFY `cityid` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `province` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `present_address_status` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `no_yrs_present_address` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `percent_of_ownership` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `number_of_year_engaged` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `position` varchar(50) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblowner', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblowner` MODIFY `cityid` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `province` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `present_address_status` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `no_yrs_present_address` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `percent_of_ownership` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `number_of_year_engaged` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblowner` MODIFY `position` varchar(200) NOT NULL;');
		});
	}

}
