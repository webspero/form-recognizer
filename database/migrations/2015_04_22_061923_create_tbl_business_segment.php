<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBusinessSegment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbusinesssegment', function(Blueprint $table)
		{
			$table->increments('businesssegmentid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('businesssegment_name',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbusinesssegment');
	}

}
