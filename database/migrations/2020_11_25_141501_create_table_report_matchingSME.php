<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportMatchingSME extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_matching_sme', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('email',100);
            $table->string('company',255);
            $table->string('job_title',255);
            $table->enum('business_classification',['Micro','Small','Medium','Large']);
            $table->enum('business_loan_plan',['3 months','6 months', '1 year']);
            $table->enum('talked_to_bank',['No','Yes'])->default('No');
            $table->bigInteger('loginid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_matching_sme');
    }
}
