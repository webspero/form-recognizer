<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblindustryMainTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `business_group` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `management_group` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `financial_group` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `industry_trend` varchar(30) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `industry_score` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `main_status` int(3) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `business_group` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `management_group` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `financial_group` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `industry_trend` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `industry_score` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblindustry_main` MODIFY `main_status` int(11) NOT NULL;');
		});
	}

}
