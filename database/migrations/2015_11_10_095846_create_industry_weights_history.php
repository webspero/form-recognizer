<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndustryWeightsHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbankindustryweightshistory', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->integer('bank_id');
			$table->text('setting');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbankindustryweightshistory');
	}

}
