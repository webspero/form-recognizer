<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEntityidToReportMatchingSme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_matching_sme', function (Blueprint $table) {
            $table->bigInteger('entityid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_matching_sme', function (Blueprint $table) {
            $table->dropColumn('entityid');
        });
    }
}
