<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTlbMajorSupplier extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmajorsupplier', function(Blueprint $table)
		{
			$table->increments('majorsupplierid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('supplier_name',200);
			$table->string('supplier_share_sales',200);
			$table->string('supplier_address',200);
			$table->string('supplier_contact_person',200);
			$table->string('supplier_contact_details',200);
			$table->integer('supplier_experience');
			$table->integer('supplier_started_years');
			$table->string('supplier_years_doing_business',200);
			$table->string('supplier_settlement',200);
			$table->string('supplier_order_frequency',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmajorsupplier');
	}

}
