<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUploadErrors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_errors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_type', 100);
            $table->string('loginid', 100);
            $table->string('entityid', 100);
            $table->text('error_details');
            $table->dateTime('date_uploading');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_errors');
    }
}
