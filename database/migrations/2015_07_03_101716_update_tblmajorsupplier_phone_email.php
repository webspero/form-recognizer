<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmajorsupplierPhoneEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			$table->string('supplier_phone', 128)->nullable()->after('supplier_contact_details');
			$table->string('supplier_email')->nullable()->after('supplier_contact_details');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			$table->dropColumn('supplier_phone');
			$table->dropColumn('supplier_email');
		});
	}

}
