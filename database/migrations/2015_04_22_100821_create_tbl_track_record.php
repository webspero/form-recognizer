<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTrackRecord extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbltrackrecord', function(Blueprint $table)
		{
			$table->increments('trackrecordid');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('bus_owner',200);
			$table->string('bus_spouse',200);
			$table->string('bus_relative',200);
			$table->string('bus_profmanager',200);
			$table->string('bus_other',200);
			$table->string('bus_other_specify',200);
			$table->string('hmr_status',200);
			$table->string('exposure_position',200);
			$table->string('exposure_type',200);
			$table->string('cr_position',200);
			$table->string('cr_type',200);
			$table->string('dr_position',200);
			$table->string('dr_type',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbltrackrecord');
	}

}
