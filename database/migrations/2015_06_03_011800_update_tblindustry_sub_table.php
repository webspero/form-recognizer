<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblindustrySubTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblindustry_sub', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_sub` MODIFY `sub_status` int(1) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblindustry_sub', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_sub` MODIFY `sub_status` int(11) NOT NULL;');
		});
	}

}
