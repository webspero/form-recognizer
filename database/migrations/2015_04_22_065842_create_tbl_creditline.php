<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCreditline extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcreditlines', function(Blueprint $table)
		{
			$table->increments('creditlineid');
			$table->integer('loginid')->index();
			$table->integer('bank_type');
			$table->string('bank_name',200);
			$table->string('bank_product',200);
			$table->string('bank_collateral',200);
			$table->integer('bank_initial_availment');
			$table->string('bank_credit_term',100); 
			$table->integer('bank_outstanding_amount');
			$table->date('bank_outstanding_date');
			$table->integer('bank_volume_trade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcreditlines');
	}

}
