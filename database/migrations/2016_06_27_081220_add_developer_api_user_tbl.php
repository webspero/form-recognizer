<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeveloperApiUserTbl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('developer_api_users', function(Blueprint $table)
		{
			$table->increments('developer_id');
			$table->string('firstname', 250);
			$table->string('lastname', 250);
			$table->string('email', 250);
			$table->string('password', 250);
			$table->tinyInteger('status')->default(0);
			$table->date('date_registered');
			$table->date('last_activity');
            
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('developer_api_users');
	}

}
