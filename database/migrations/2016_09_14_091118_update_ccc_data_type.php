<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCccDataType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover1` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover2` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover3` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover1` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover2` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover3` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover1` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover2` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover3` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle1` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle2` decimal(10,1) NOT NULL;');
		DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle3` decimal(10,1) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
