<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadyratioKeySummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('readyratio_key_summary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('entityid')->index();
            $table->integer('Year');
            $table->double('IntangibleAsset');
            $table->double('NetTangibleAsset');
            $table->double('NetAsset');
            $table->double('FinancialLeverage');
            $table->double('DebtRatio');
            $table->double('LTDtoE');
            $table->double('NCAtoNW');
            $table->double('FixedAsset');
            $table->double('FAtoNW');
            $table->double('CLiabilityRatio');
            $table->double('Capitalization');
            $table->double('NWC');
            $table->double('WCD');
            $table->double('InventoryNWC');
            $table->double('CurrentRatio');
            $table->double('QuickRatio');
            $table->double('CashRatio');
            $table->double('OIEeFC');
            $table->double('EBIT');
            $table->double('EBITDA');
            $table->double('GrossMargin');
            $table->double('ROS');
            $table->double('ProfitMargin');
            $table->double('ICR');
            $table->double('ROE');
            $table->double('ROE_CI');
            $table->double('ROA');
            $table->double('ROA_CI');
            $table->double('ROCE');
            $table->double('ReceivablesTurnover');
            $table->double('PayableTurnover');
            $table->double('InventoryTurnover');
            $table->double('AssetTurnover');
            $table->double('CAssetTurnover');
            $table->double('CapitalTurnover');
            $table->double('CCC');
            $table->double('T1');
            $table->double('T2');
            $table->double('T3');
            $table->double('T4');
            $table->double('T5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('readyratio_key_summary');
    }
}
