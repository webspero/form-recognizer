<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupervisorApplication extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supervisor_application', function(Blueprint $table)
		{
			$table->increments('application_id');
			$table->integer('bank_id');
			$table->tinyInteger('role');
			$table->tinyInteger('product_features');
			$table->string('email', 250);
			$table->string('firstname', 150);
			$table->string('lastname', 150);
			$table->string('password', 30);
			$table->string('other', 150);
            $table->tinyInteger('status');
			$table->date('application_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supervisor_application');
	}

}
