<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaypalTransactionsAddReturnUrl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('paypal_transactions', function(Blueprint $table) {
			$table->string('client_return', 250);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('paypal_transactions', function(Blueprint $table) {
			$table->dropColumn('client_return');
		});
	}

}
