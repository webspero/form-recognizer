<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BatchRemoveLoginid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbladverserecord', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblbranches', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblbusinessdriver', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblbusinesssegment', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblbusinesstype', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblcapacityexpansion', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblcapital', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblcertifications', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblcompetitor', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblcycledetails', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tbldirectors', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tbldocument', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tbleducation', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblfuturegrowth', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblfuturesource', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblinsurance', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblkeymanagers', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tbllocations', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblmajorcustomeraccount', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblmajorlocation', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblmanagebus', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblowner', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblplanfacilityrequested', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblprojectcompleted', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblrelatedcompanies', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblrevenuegrowthpotential', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblriskassessment', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblservicesoffer', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblshareholders', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblspouse', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
		
		Schema::table('tblsustainability', function(Blueprint $table)
		{
			$table->dropColumn('loginid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
