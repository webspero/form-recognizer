<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Altertabledropboxesfilecount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldropboxentity', function (Blueprint $table) {
            $table->integer('file_count')->after('type');
            $table->string('orig_filename')->after('file_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldropboxentity', function (Blueprint $table) {
            $table->dropColumn('file_count');
            $table->dropColumn('orig_filename');
        });
    }
}
