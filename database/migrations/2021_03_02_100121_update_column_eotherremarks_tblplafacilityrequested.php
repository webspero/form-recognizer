<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnEotherremarksTblplafacilityrequested extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblplanfacilityrequested', function (Blueprint $table) {
            $table->string('other_remarks')->nullable()->change();
            $table->string('cash_deposit_details')->nullable()->change();
            $table->string('cash_deposit_amount')->nullable()->change();
            $table->string('securities_details')->nullable()->change();
            $table->string('securities_estimated_value')->nullable()->change();
            $table->string('property_details')->nullable()->change();
            $table->string('property_estimated_value')->nullable()->change();
            $table->string('chattel_details')->nullable()->change();
            $table->string('chattel_estimated_value')->nullable()->change();
            $table->string('others_details')->nullable()->change();
            $table->string('others_estimated_value')->nullable()->change();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
