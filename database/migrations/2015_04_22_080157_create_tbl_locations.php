<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLocations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbllocations', function(Blueprint $table)
		{
			$table->increments('locationid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('location_size');
			$table->string('location_type',200);
			$table->string('location_map',200);
			$table->string('location_used',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbllocations');
	}

}
