<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDocument extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbldocument', function(Blueprint $table)
		{
			$table->increments('documentid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('is_origin')->index();
			$table->integer('document_type');
			$table->integer('document_group');
			$table->string('document_orig',200);
			$table->string('document_rename',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbldocument');
	}

}
