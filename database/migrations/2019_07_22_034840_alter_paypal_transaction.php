<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaypalTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('paypal_transactions', function(Blueprint $table)
		{
			$table->text('token')->nullable();
			$table->text('entity_id')->nullable();
			$table->datetime('created_at');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('paypal_transactions', function(Blueprint $table)
		{
			$table->dropColumn('token');
			$table->dropColumn('entity_id');
			$table->dropColumn('created_at');
		});
    }
}
