<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCreditFacilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcreditfacilitiesreasons', function(Blueprint $table)
		{
			$table->increments('creditfacilitiesreasonid');
			$table->integer('loginid')->index();
			$table->tinyInteger('credit_facilities_type');
			$table->longText('credit_facilities_reasons');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcreditfacilitiesreasons');
	}

}
