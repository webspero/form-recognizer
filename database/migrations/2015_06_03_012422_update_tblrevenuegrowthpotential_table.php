<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblrevenuegrowthpotentialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblrevenuegrowthpotential', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblrevenuegrowthpotential` MODIFY `current_market` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblrevenuegrowthpotential` MODIFY `new_market` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblrevenuegrowthpotential', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblrevenuegrowthpotential` MODIFY `current_market` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblrevenuegrowthpotential` MODIFY `new_market` int(11) NOT NULL;');
		});
	}

}
