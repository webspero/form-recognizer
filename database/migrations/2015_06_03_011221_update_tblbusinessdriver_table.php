<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblbusinessdriverTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblbusinessdriver', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblbusinessdriver` MODIFY `businessdriver_total_sales` int(10) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblbusinessdriver', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblbusinessdriver` MODIFY `businessdriver_total_sales` varchar(200) NOT NULL;');
		});
	}

}
