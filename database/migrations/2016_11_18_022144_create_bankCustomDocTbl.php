<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankCustomDocTbl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_custom_docs', function(Blueprint $table)
		{
			$table->increments('document_id');
			$table->integer('bank_id');
			$table->string('document_label', 100);
			$table->tinyInteger('bank_required');
			$table->tinyInteger('status');
			$table->datetime('created_date');
			$table->datetime('updated_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_custom_docs');
	}

}
