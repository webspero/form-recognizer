<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFstemplateFilename extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('financial_reports', function(Blueprint $table)
		{
			$table->string('fstemplate_file', 250);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('financial_reports', function(Blueprint $table)
		{
			$table->dropColumn('fstemplate_file');
		});
	}

}
