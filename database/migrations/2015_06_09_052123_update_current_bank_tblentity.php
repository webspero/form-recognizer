<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCurrentBankTblentityTwo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement("UPDATE  `tblentity` SET `current_bank` = NULL");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			//
		});
	}

}
