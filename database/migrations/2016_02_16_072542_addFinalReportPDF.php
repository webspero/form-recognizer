<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalReportPDF extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tblentity', function(Blueprint $table)
		{
			$table->string('cbpo_pdf', 250);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('cbpo_pdf');
		});
	}

}
