<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCreditlineInvestigation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcreditlines_investigation', function(Blueprint $table)
		{
			$table->increments('ccinvestigationid');
			$table->integer('loginid')->index();
			$table->string('banking_credit_experience',200);
			$table->string('credit_facility_ready',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcreditlines_investigation');
	}

}
