<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinancialAnalysisPdfTlToTblentity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->text('financial_analysis_pdf_tl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->dropColumn('financial_analysis_pdf_tl');
        });
    }
}
