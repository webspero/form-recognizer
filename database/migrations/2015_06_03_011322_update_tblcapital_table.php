<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblcapitalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcapital', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_authorized` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_issued` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_paid_up` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_ordinary_shares` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_par_value` decimal(10,2) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcapital', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_authorized` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_issued` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_paid_up` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_ordinary_shares` int(20) NOT NULL;');
			DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_par_value` int(20) NOT NULL;');
		});
	}

}
