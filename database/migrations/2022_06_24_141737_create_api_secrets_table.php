<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiSecretsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_secrets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('api_key');
            $table->text('end_point');
            $table->text('model_id');
            $table->text('container_name');
            $table->text('container_url');
            $table->text('sas_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_secrets');
    }
}
