<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRiskAssessment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblriskassessment', function(Blueprint $table)
		{
			$table->increments('riskassessmentid');
			$table->integer('loginid')->index();
			$table->string('risk_assessment_name',200);
			$table->string('risk_assessment_solution',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblriskassessment');
	}

}
