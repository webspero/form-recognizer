<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblnotif', function (Blueprint $table) {$date = new DateTime();
            $table->tinyInteger('notif_daily')->after('keys_number_trigger');
            $table->tinyInteger('notif_reports_started')->after('notif_daily');
            $table->tinyInteger('notif_reports_completed')->after('notif_reports_started');
            $table->time('time_trigger')->nullable()->default(null)->after('notif_reports_completed');
            $table->string('timezone')->after('time_trigger');
            $table->time('original_time')->nullable()->default(null)->after('timezone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblnotif', function (Blueprint $table) {
            $table->dropColumn('notif_daily');
            $table->dropColumn('notif_reports_started');
            $table->dropColumn('notif_reports_completed');
            $table->dropColumn('time_trigger');
            $table->dropColumn('timezone');
            $table->dropColumn('original_time');
        });
    }
}
