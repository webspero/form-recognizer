<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnPriceTblbank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblbank', function (Blueprint $table) {
            $table->float('simplified_price', 12, 2)->default('1767.5')->change();
            $table->float('standalone_price', 12, 2)->default('3535')->change();
            $table->float('premium_zone1_price', 12, 2)->default('4343')->change();
            $table->float('premium_zone2_price', 12, 2)->default('5353')->change();
            $table->float('premium_zone3_price', 12, 2)->default('6363')->change();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
