<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomPricingForTblbank extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->integer('is_custom')->default(0);
			$table->integer('custom_price')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->dropColumn('is_custom');
			$table->dropColumn('custom_price');
		});
	}

}
