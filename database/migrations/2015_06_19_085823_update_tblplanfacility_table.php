<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblplanfacilityTableThree extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			$table->string('lending_institution')->nullable()->after('loginid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			$table->dropColumn('lending_institution');
		});
	}

}
