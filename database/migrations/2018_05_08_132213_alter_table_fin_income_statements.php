<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFinIncomeStatements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fin_income_statements', function(Blueprint $table) {
			$table->decimal('ImpairmentLoss', 15, 2)->default(0);
			$table->decimal('OtherIncomeFromSubsidiaries', 15, 2)->default(0);
			$table->decimal('CumulativeGainPreviouslyRecognized', 15, 2)->default(0);
			$table->decimal('HedgingGainsForHedge', 15, 2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fin_income_statements', function(Blueprint $table) {
	        $table->dropColumn('ImpairmentLoss');
	        $table->dropColumn('OtherIncomeFromSubsidiaries');
	        $table->dropColumn('CumulativeGainPreviouslyRecognized');
	        $table->dropColumn('HedgingGainsForHedge');
	    });
	}

}
