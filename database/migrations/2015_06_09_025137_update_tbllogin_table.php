<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblloginTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->integer('bank_id')->default(0);
			$table->smallInteger('can_view_free_sme')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('bank_id');
			$table->dropColumn('can_view_free_sme');
		});
	}

}
