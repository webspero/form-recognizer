<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableIndustryClassTier extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('industry_class', function(Blueprint $table)
		{
			$table->text('industry_sub')->nullable();
			$table->text('industry_main')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('industry_class', function(Blueprint $table)
		{
			$table->dropColumn('industry_sub');
			$table->dropColumn('industry_main');
		});
	}

}
