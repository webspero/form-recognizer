<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmajorcustomerPhoneEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			$table->string('customer_phone', 128)->nullable()->after('customer_contact_details');
			$table->string('customer_email')->nullable()->after('customer_contact_details');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			$table->dropColumn('customer_phone');
			$table->dropColumn('customer_email');
		});
	}

}
