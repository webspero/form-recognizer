<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPremiumPriceToZipcodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('zipcodes', function(Blueprint $table)
		{
			$table->integer('premiumReportPrice');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('zipcodes', function(Blueprint $table)
		{
			$table->dropColumn('premiumReportPrice');
		});
	}

}
