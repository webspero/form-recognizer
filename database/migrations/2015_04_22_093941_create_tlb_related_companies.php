<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTlbRelatedCompanies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblrelatedcompanies', function(Blueprint $table)
		{
			$table->increments('relatedcompaniesid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('related_company_name',200);
			$table->string('related_company_address1',200);
			$table->string('related_company_address2',200);
			$table->string('related_company_cityid',200);
			$table->string('related_company_province',200);
			$table->string('related_company_zipcode',200);
			$table->string('related_company_phone',200);
			$table->string('related_company_email',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblrelatedcompanies');
	}

}
