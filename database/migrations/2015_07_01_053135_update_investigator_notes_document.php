<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvestigatorNotesDocument extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investigator_notes', function(Blueprint $table)
		{
			$table->string('required_docs')->nullable()->after('notes');
			$table->string('others_docs')->nullable()->after('required_docs');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investigator_notes', function(Blueprint $table)
		{
			$table->dropColumn('required_docs');
			$table->dropColumn('others_docs');
		});
	}

}
