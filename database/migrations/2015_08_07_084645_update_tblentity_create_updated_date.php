<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblentityCreateUpdatedDate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->date('updated_date')->nullable()->after('employee_size');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('updated_date');
		});
	}

}
