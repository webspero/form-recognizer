<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajorLocations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmajorlocation', function(Blueprint $table)
		{
			$table->increments('marketlocationid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('marketlocation')->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmajorlocation');
	}

}
