<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblexternalTranscriberAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblexternal_transcriber_account', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name', 250);
			$table->string('email', 100);
			$table->integer('is_deleted');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblexternal_transcriber_account');
    }
}
