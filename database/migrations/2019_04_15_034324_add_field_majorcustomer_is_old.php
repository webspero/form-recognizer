<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldMajorcustomerIsOld extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			$table->integer('is_old',false, 10)->after('customer_order_frequency');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorcustomer', function(Blueprint $table)
		{
			//
			$table->dropColumn('is_old');
		});
	}

}
