<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmanagementturnoverTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmanagementturnover', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `executive_team` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `middle_management` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `staff` int(3) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmanagementturnover', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `executive_team` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `middle_management` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmanagementturnover` MODIFY `staff` varchar(200) NOT NULL;');
		});
	}

}
