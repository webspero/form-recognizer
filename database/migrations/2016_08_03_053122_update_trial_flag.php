<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTrialFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->smallInteger('trial_flag')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('trial_flag');
		});
	}

}
