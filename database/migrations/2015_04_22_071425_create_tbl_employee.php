<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmployee extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblemployee', function(Blueprint $table)
		{
			$table->increments('employeeid');
			$table->integer('loginid')->index();
			$table->string('employee_name',200);
			$table->string('employee_address',200);
			$table->string('employee_contact_details',200);
			$table->tinyInteger('employee_score');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblemployee');
	}

}
