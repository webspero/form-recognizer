<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblpayment', function(Blueprint $table)
		{
			$table->integer('entity_id')->nullable()->after('loginid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblpayment', function(Blueprint $table)
		{
			$table->dropColumn('entity_id');
		});
	}

}
