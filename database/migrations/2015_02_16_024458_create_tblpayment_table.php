<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblpaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblpayment', function(Blueprint $table)
		{
			$table->increments('paymentid');
			$table->integer('loginid')->index();
			$table->string('transactionid', 200);
			$table->string('transactiontype', 200);
			$table->string('paymenttype', 200);
			$table->string('paymentamount', 200);
			$table->string('currencycode', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblpayment');
	}

}
