<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblDocumentValidity extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbldocument', function(Blueprint $table)
		{
			$table->smallInteger('is_valid')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbldocument', function(Blueprint $table)
		{
			$table->dropColumn('is_valid');
		});
	}

}
