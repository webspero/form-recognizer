<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescIndustryColumnsInPseCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pse_companies', function (Blueprint $table) {
            $table->integer('pse_company_industry_main_id')->nullable(); // FK ID of tblindustry_main
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pse_companies', function (Blueprint $table) {
            //
        });
    }
}
