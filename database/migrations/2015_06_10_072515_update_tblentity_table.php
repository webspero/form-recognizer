<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblentityTableTwo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement("UPDATE  `tblentity` SET `is_independent` = 0");
			DB::statement("ALTER TABLE `tblentity` MODIFY COLUMN `is_independent` TINYINT NOT NULL DEFAULT 0");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE `tblentity` MODIFY COLUMN `is_independent` ENUM('0','1') NULL DEFAULT NULL");
			DB::statement("UPDATE  `tblentity` SET `is_independent` = NULL");
		});
	}

}
