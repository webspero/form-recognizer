<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblbankindustryweightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbankindustryweights', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bank_id')->index();
			$table->integer('industry_id')->index();
			$table->tinyInteger('business_group');
			$table->tinyInteger('management_group');
			$table->tinyInteger('financial_group');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbankindustryweights');
	}

}
