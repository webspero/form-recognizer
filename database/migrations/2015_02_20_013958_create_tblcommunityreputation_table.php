<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblcommunityreputationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcommunityreputation', function(Blueprint $table)
		{
			$table->increments('communityreputationid');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('assoc_name', 200);
			$table->string('assoc_jurisdiction', 200);
			$table->string('assoc_type', 200);
			$table->string('assoc_dues', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcommunityreputation');
	}

}
