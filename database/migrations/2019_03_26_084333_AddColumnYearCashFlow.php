<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnYearCashFlow extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fin_cashflow', function(Blueprint $table)
		{
			$table->integer('Year')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fin_cashflow', function(Blueprint $table)
		{
			$table->dropColumn('Year');
		});
	}

}
