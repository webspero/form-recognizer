<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblcreditlinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcreditlines', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_type` SMALLINT NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_initial_availment` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_outstanding_amount` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_volume_trade` decimal(10,2) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcreditlines', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_type` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_initial_availment` int(10) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_outstanding_amount` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_volume_trade` int(11) NOT NULL;');
		});
	}

}
