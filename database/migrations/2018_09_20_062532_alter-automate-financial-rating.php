<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAutomateFinancialRating extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('automate_standalone_rating');
		Schema::create('automate_standalone_rating', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('entity_id')->index();
			$table->smallInteger('status')->default(0)->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
