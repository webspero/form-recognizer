<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblSustainability extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblsustainability', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `succession_plan` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `succession_timeframe` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_details` VARCHAR(200) NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_landscape` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_timeframe` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_susceptibility_economic_recession` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_foreign_exchange_interest_sensitivity` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_commodity_price_volatility` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_governement_regulation` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `cg_structure` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_executive_team` SMALLINT NULL DEFAULT 0;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_middle_management` SMALLINT NULL DEFAULT 0;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_staff` SMALLINT NULL DEFAULT 0;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `crm_Structure` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `bpsf_Regular_cycle_business` SMALLINT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `tax_payments` SMALLINT NULL;');
			/* $table->smallInteger('succession_plan')->nullable()->change();
			$table->smallInteger('succession_timeframe')->nullable()->change();
			$table->string('competition_details')->nullable()-change();
			$table->smallInteger('competition_landscape')->nullable()->change();
			$table->smallInteger('competition_timeframe')->nullable()->change();
			$table->smallInteger('ev_susceptibility_economic_recession')->nullable()->change();
			$table->smallInteger('ev_foreign_exchange_interest_sensitivity')->nullable()->change();
			$table->smallInteger('ev_commodity_price_volatility')->nullable()->change();
			$table->smallInteger('ev_governement_regulation')->nullable()->change();
			$table->smallInteger('cg_structure')->nullable()->change();
			$table->smallInteger('mt_executive_team')->nullable()->default(0)->change();
			$table->smallInteger('mt_middle_management')->nullable()->default(0)->change();
			$table->smallInteger('mt_staff')->nullable()->default(0)->change();
			$table->smallInteger('crm_Structure')->nullable()->change();
			$table->smallInteger('bpsf_Regular_cycle_business')->nullable()->change();
			$table->smallInteger('tax_payments')->nullable()->change(); */
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblsustainability', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `succession_plan` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `succession_timeframe` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_details` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_landscape` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `competition_timeframe` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_susceptibility_economic_recession` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_foreign_exchange_interest_sensitivity` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_commodity_price_volatility` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `ev_governement_regulation` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `cg_structure` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_executive_team` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_middle_management` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `mt_staff` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `crm_Structure` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `bpsf_Regular_cycle_business` VARCHAR(255) NOT NULL;');
			DB::statement('ALTER TABLE `creditbpo_db`.`tblsustainability` MODIFY `tax_payments` VARCHAR(255) NOT NULL;');
			/* $table->string('succession_plan')->change();
			$table->string('succession_timeframe')->change();
			$table->string('competition_details')->change();
			$table->string('competition_landscape')->change();
			$table->string('competition_timeframe')->change();
			$table->string('ev_susceptibility_economic_recession')->change();
			$table->string('ev_foreign_exchange_interest_sensitivity')->change();
			$table->string('ev_commodity_price_volatility')->change();
			$table->string('ev_governement_regulation')->change();
			$table->string('cg_structure')->change();
			$table->string('mt_executive_team')->change();
			$table->string('mt_middle_management')->change();
			$table->string('mt_staff')->change();
			$table->string('crm_Structure')->change();
			$table->string('bpsf_Regular_cycle_business')->change();
			$table->string('tax_payments')->change(); */
		});
	}

}
