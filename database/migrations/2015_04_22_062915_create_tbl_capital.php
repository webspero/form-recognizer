<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCapital extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcapital', function(Blueprint $table)
		{
			$table->increments('capitalid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->bigInteger('capital_authorized');
			$table->bigInteger('capital_issued');
			$table->bigInteger('capital_paid_up');
			$table->bigInteger('capital_ordinary_shares');
			$table->bigInteger('capital_par_value');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcapital');
	}

}
