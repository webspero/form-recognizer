<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDebtServiceCapacityCalculatorAddColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('debt_service_capacity_calculator', function($table) {
	        $table->tinyinteger('period')->default(0);
	        $table->decimal('payment_per_period', 20, 12)->default(0);
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('debt_service_capacity_calculator', function($table) {
	        $table->dropColumn('period');
	        $table->dropColumn('payment_per_period');
	    });
	}

}
