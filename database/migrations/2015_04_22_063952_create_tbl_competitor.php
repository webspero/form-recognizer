<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCompetitor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcompetitor', function(Blueprint $table)
		{
			$table->increments('competitorid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('competitor_name',200);
			$table->string('competitor_address',200);
			$table->string('competitor_contact_details',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcompetitor');
	}

}
