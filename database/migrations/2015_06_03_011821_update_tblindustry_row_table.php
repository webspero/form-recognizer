<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblindustryRowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblindustry_row', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_row` MODIFY `row_status` int(1) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblindustry_row', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblindustry_row` MODIFY `row_status` int(11) NOT NULL;');
		});
	}

}
