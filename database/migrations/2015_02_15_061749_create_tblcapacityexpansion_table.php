<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblcapacityexpansionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcapacityexpansion', function(Blueprint $table)
		{
			$table->increments('capacityexpansionid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('current_year');
			$table->integer('new_year');
			$table->integer('within_five_years');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcapacityexpansion');
	}

}
