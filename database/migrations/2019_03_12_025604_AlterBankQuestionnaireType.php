<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBankQuestionnaireType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->integer('questionnaire_type')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->dropColumn('questionnaire_type');
		});
	}

}
