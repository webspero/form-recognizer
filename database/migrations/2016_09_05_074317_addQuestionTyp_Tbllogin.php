<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionTypTbllogin extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->tinyInteger('question_type')->after('role')->default(0);
			$table->tinyInteger('ecf_included')->after('question_type')->default(1);
			$table->tinyInteger('rcl_included')->after('ecf_included')->default(1);
		});
        
        Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->tinyInteger('question_type')->after('role')->default(0);
			$table->tinyInteger('ecf_included')->after('question_type')->default(1);
			$table->tinyInteger('rcl_included')->after('ecf_included')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->dropColumn('question_type');
			$table->dropColumn('ecf_included');
			$table->dropColumn('rcl_included');
		});
        
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('question_type');
            $table->dropColumn('ecf_included');
			$table->dropColumn('rcl_included');
		});
	}

}
