<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEcfRclInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			$table->string('credit_terms', 250);
			$table->string('outstanding_balance', 250);
			$table->smallInteger('experience');
			$table->text('other_remarks');
		});
        
        Schema::table('tblplanfacilityrequested', function(Blueprint $table)
		{
			$table->string('credit_terms', 250);
			$table->string('outstanding_balance', 250);
			$table->smallInteger('experience');
			$table->text('other_remarks');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblplanfacility', function(Blueprint $table)
		{
			$table->dropColumn('credit_terms');
			$table->dropColumn('outstanding_balance');
			$table->dropColumn('experience');
			$table->dropColumn('other_remarks');
		});
        
        Schema::table('tblplanfacilityrequested', function(Blueprint $table)
		{
			$table->dropColumn('credit_terms');
			$table->dropColumn('outstanding_balance');
			$table->dropColumn('experience');
			$table->dropColumn('other_remarks');
		});
	}

}
