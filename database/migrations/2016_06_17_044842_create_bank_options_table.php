<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_doc_options', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bank_id');
			$table->string('name');
			$table->smallInteger('is_active')->default(1);
			$table->smallInteger('corp_sec_reg')->default(1);
			$table->smallInteger('corp_sec_gen')->default(1);
			$table->smallInteger('corp_business_license')->default(1);
			$table->smallInteger('corp_board_resolution')->default(0);
			$table->smallInteger('corp_board_resolution_pres')->default(0);
			$table->smallInteger('corp_tax_reg')->default(1);
			$table->smallInteger('corp_itr')->default(1);
			$table->smallInteger('sole_business_license')->default(1);
			$table->smallInteger('sole_business_owner')->default(0);
			$table->smallInteger('sole_tax_reg')->default(1);
			$table->smallInteger('sole_itr')->default(1);
			$table->smallInteger('boi')->default(0);
			$table->smallInteger('peza')->default(0);
			$table->smallInteger('fs_template')->default(1);
			$table->smallInteger('balance_sheet')->default(0);
			$table->smallInteger('income_statement')->default(0);
			$table->smallInteger('cashflow_statement')->default(0);
			$table->smallInteger('bank_statement')->default(3);
			$table->smallInteger('utility_bill')->default(3);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_doc_options');
	}

}
