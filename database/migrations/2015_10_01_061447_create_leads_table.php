<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_leads', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('first_name', 50);
			$table->string('last_name', 50);
			$table->datetime('submitted_date');
			$table->integer('last_id');
			$table->smallInteger('is_new')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_leads');
	}

}
