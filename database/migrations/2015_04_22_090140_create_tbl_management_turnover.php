<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblManagementTurnover extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmanagementturnover', function(Blueprint $table)
		{
			$table->increments('mturnoverid');
			$table->integer('loginid')->index();
			$table->string('executive_team',200);
			$table->string('middle_management',200);
			$table->string('staff',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmanagementturnover');
	}

}
