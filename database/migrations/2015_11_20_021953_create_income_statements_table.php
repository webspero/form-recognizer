<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeStatementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fin_income_statements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('financial_report_id');
			$table->smallInteger('index_count');
			$table->decimal('Revenue', 15, 2);
			$table->decimal('CostOfSales', 15, 2);
			$table->decimal('GrossProfit', 15, 2);
			$table->decimal('OtherIncome', 15, 2);
			$table->decimal('DistributionCosts', 15, 2);
			$table->decimal('AdministrativeExpense', 15, 2);
			$table->decimal('OtherExpenseByFunction', 15, 2);
			$table->decimal('OtherGainsLosses', 15, 2);
			$table->decimal('ProfitLossFromOperatingActivities', 15, 2);
			$table->decimal('DifferenceBetweenCarryingAmountOfDividendsPayable', 15, 2);
			$table->decimal('GainsLossesOnNetMonetaryPosition', 15, 2);
			$table->decimal('GainLossArisingFromDerecognitionOfFinancialAssets', 15, 2);
			$table->decimal('FinanceIncome', 15, 2);
			$table->decimal('FinanceCosts', 15, 2);
			$table->decimal('ShareOfProfitLossOfAssociates', 15, 2);
			$table->decimal('GainsLossesArisingFromDifference', 15, 2);
			$table->decimal('ProfitLossBeforeTax', 15, 2);
			$table->decimal('IncomeTaxExpenseContinuingOperations', 15, 2);
			$table->decimal('ProfitLossFromContinuingOperations', 15, 2);
			$table->decimal('ProfitLossFromDiscontinuedOperations', 15, 2);
			$table->decimal('ProfitLoss', 15, 2);
			$table->decimal('OtherComprehensiveIncome', 15, 2);
			$table->decimal('ComprehensiveIncome', 15, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fin_income_statements');
	}

}
