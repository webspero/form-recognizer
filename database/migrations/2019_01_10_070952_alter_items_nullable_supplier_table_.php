<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItemsNullableSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_1` VARCHAR(255) NULL;');
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_2` VARCHAR(255) NULL;');
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_3` VARCHAR(255) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_1` VARCHAR(255) NOT  NULL;');
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_2` VARCHAR(255) NOT  NULL;');
		DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `item_3` VARCHAR(255) NOT  NULL;');
	}

}
