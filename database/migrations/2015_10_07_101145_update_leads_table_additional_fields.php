<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLeadsTableAdditionalFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('email_leads', function(Blueprint $table)
		{
			$table->string('company_name', 60)->after('last_name');
			$table->string('contact_number', 30)->after('company_name');
			$table->string('job_title', 30)->after('contact_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('email_leads', function(Blueprint $table)
		{
			$table->dropColumn('company_name');
			$table->dropColumn('contact_number');
			$table->dropColumn('job_title');
		});
	}

}
