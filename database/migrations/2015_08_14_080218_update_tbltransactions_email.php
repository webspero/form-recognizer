<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTbltransactionsEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbltransactions', function(Blueprint $table)
		{
			$table->string('email')->after('message')->nullable();
			$table->smallInteger('batch')->after('email')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbltransactions', function(Blueprint $table)
		{
			$table->dropColumn('email');
			$table->dropColumn('batch');
		});
	}

}
