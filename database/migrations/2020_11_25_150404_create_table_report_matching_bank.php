<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportMatchingBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_matching_bank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('bank_name',255);
            $table->string('bank_id',255);
            $table->string('job_title',255);
            $table->enum('bank_classification',['Universal','Commercial','Thrift','Rural']);
            $table->integer('business_loans');
            $table->bigInteger('loginid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_matching_bank');
    }
}
