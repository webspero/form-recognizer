<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbankproducts', function(Blueprint $table)
		{
			$table->increments('bankproductid');
			$table->integer('loginid')->index();
			$table->integer('bank_type')->index();
			$table->string('bank_name',200);
			$table->string('bank_product',200);
			$table->string('bank_account_number',200);
			$table->integer('bank_outstanding_amount');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbankproducts');
	}

}
