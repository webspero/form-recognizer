<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblfuturesourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblfuturesource', function(Blueprint $table)
		{
			$table->increments('futuresourceid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('opg_per_year');
			$table->string('opg_horizontal_year');
			$table->string('family_per_year');
			$table->string('family_horizon_year');
			$table->string('bp_per_year');
			$table->string('bp_horizon_year');
			$table->string('vcc_per_year');
			$table->string('vcc_horizon_year');
			$table->string('ipo_per_year');
			$table->string('ipo_horizon_year');
			$table->string('others_per_year');
			$table->string('others_horizon_year');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblfuturesource');
	}

}
