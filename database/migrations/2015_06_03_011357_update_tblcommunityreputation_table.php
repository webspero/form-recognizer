<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblcommunityreputationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcommunityreputation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_jurisdiction` varchar(50) NOT NULL;');
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_type` varchar(50) NOT NULL;');
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_dues` varchar(50) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcommunityreputation', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_jurisdiction` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_type` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblcommunityreputation` MODIFY `assoc_dues` varchar(200) NOT NULL;');
		});
	}

}
