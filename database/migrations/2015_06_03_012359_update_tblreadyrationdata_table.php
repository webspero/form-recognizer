<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblreadyrationdataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblreadyrationdata', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_revenue_growth1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_revenue_growth2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_income_growth1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_income_growth2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_profit_margin1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_profit_margin2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_profit_margin1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_profit_margin2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_cash_margin1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_cash_margin2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `current_ratio1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `current_ratio2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `debt_equity_ratio1` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `debt_equity_ratio2` float(6,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover1` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover2` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover1` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover2` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover1` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover2` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle1` int(11) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle2` int(11) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblreadyrationdata', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_revenue_growth1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_revenue_growth2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_income_growth1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_income_growth2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_profit_margin1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `gross_profit_margin2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_profit_margin1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_profit_margin2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_cash_margin1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `net_cash_margin2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `current_ratio1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `current_ratio2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `debt_equity_ratio1` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `debt_equity_ratio2` float(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover1` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `receivables_turnover2` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover1` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `inventory_turnover2` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover1` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `accounts_payable_turnover2` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle1` int(100) NOT NULL;');
			DB::statement('ALTER TABLE `tblreadyrationdata` MODIFY `cash_conversion_cycle2` int(100) NOT NULL;');
		});
	}

}
